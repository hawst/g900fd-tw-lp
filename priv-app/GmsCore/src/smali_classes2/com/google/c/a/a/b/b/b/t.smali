.class final Lcom/google/c/a/a/b/b/b/t;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/c/a/a/b/b/b/s;


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 137
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/c/a/a/b/b/b/y;Lcom/google/c/a/a/b/b/b/y;)V
    .locals 12

    .prologue
    const/4 v3, 0x0

    const/4 v11, 0x0

    .line 140
    invoke-interface {p1}, Lcom/google/c/a/a/b/b/b/y;->c()Lcom/google/c/b/a/c;

    move-result-object v0

    check-cast v0, Lcom/google/c/a/a/b/b/a/c/s;

    .line 141
    invoke-interface {p2}, Lcom/google/c/a/a/b/b/b/y;->c()Lcom/google/c/b/a/c;

    move-result-object v1

    check-cast v1, Lcom/google/c/a/a/b/b/a/c/s;

    .line 142
    invoke-virtual {v0}, Lcom/google/c/a/a/b/b/a/c/s;->e()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1}, Lcom/google/c/a/a/b/b/a/c/s;->e()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 202
    :goto_0
    return-void

    .line 149
    :cond_0
    invoke-virtual {v0}, Lcom/google/c/a/a/b/b/a/c/s;->d()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    .line 150
    invoke-virtual {v0}, Lcom/google/c/a/a/b/b/a/c/s;->c()I

    move-result v4

    .line 151
    add-int/2addr v2, v4

    add-int/lit8 v5, v2, -0x1

    .line 152
    invoke-virtual {v1}, Lcom/google/c/a/a/b/b/a/c/s;->d()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v6

    .line 153
    invoke-virtual {v1}, Lcom/google/c/a/a/b/b/a/c/s;->c()I

    move-result v7

    .line 154
    add-int v2, v7, v6

    add-int/lit8 v8, v2, -0x1

    .line 156
    invoke-virtual {v1}, Lcom/google/c/a/a/b/b/a/c/s;->d()Ljava/util/List;

    move-result-object v9

    .line 158
    if-ge v7, v4, :cond_5

    .line 163
    sub-int v2, v8, v4

    add-int/lit8 v2, v2, 0x1

    invoke-static {v11, v2}, Ljava/lang/Math;->max(II)I

    move-result v4

    .line 165
    new-instance v2, Lcom/google/c/a/a/b/b/a/c/s;

    invoke-virtual {v1}, Lcom/google/c/a/a/b/b/a/c/s;->e()Ljava/lang/String;

    move-result-object v10

    sub-int v4, v6, v4

    invoke-interface {v9, v11, v4}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v4

    invoke-direct {v2, v10, v7, v4}, Lcom/google/c/a/a/b/b/a/c/s;-><init>(Ljava/lang/String;ILjava/util/List;)V

    .line 171
    :goto_1
    if-le v8, v5, :cond_4

    .line 177
    sub-int v3, v5, v7

    add-int/lit8 v3, v3, 0x1

    invoke-static {v11, v3}, Ljava/lang/Math;->max(II)I

    move-result v4

    .line 179
    new-instance v3, Lcom/google/c/a/a/b/b/a/c/s;

    invoke-virtual {v1}, Lcom/google/c/a/a/b/b/a/c/s;->e()Ljava/lang/String;

    move-result-object v1

    add-int v5, v7, v4

    invoke-interface {v9, v4, v6}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v4

    invoke-direct {v3, v1, v5, v4}, Lcom/google/c/a/a/b/b/a/c/s;-><init>(Ljava/lang/String;ILjava/util/List;)V

    move-object v1, v3

    .line 186
    :goto_2
    if-eqz v2, :cond_2

    if-eqz v1, :cond_2

    .line 187
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 188
    invoke-interface {v3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 189
    invoke-interface {v3, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 190
    new-instance v1, Lcom/google/c/a/a/b/b/a/c/o;

    invoke-direct {v1}, Lcom/google/c/a/a/b/b/a/c/o;-><init>()V

    invoke-virtual {v1, v3}, Lcom/google/c/a/a/b/b/a/c/o;->a(Ljava/util/Collection;)Lcom/google/c/a/a/b/b/a/c/o;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/c/a/a/b/b/a/c/o;->a()Lcom/google/c/a/a/b/b/a/c/n;

    move-result-object v1

    .line 200
    :cond_1
    :goto_3
    invoke-interface {p1, v0}, Lcom/google/c/a/a/b/b/b/y;->a(Lcom/google/c/b/a/c;)V

    .line 201
    invoke-interface {p2, v1}, Lcom/google/c/a/a/b/b/b/y;->a(Lcom/google/c/b/a/c;)V

    goto :goto_0

    .line 192
    :cond_2
    if-eqz v2, :cond_3

    move-object v1, v2

    .line 193
    goto :goto_3

    .line 194
    :cond_3
    if-nez v1, :cond_1

    .line 197
    invoke-static {}, Lcom/google/c/b/a/d;->a()Lcom/google/c/b/a/d;

    move-result-object v1

    goto :goto_3

    :cond_4
    move-object v1, v3

    goto :goto_2

    :cond_5
    move-object v2, v3

    goto :goto_1
.end method
