.class public abstract Lcom/google/c/a/a/b/b/a/c/b;
.super Lcom/google/c/a/a/b/b/a/c/d;
.source "SourceFile"

# interfaces
.implements Lcom/google/c/a/a/b/b/a/c/l;


# instance fields
.field final a:I

.field final b:Ljava/util/List;

.field private d:Ljava/util/Set;


# direct methods
.method public constructor <init>(Ljava/lang/String;ILjava/util/List;)V
    .locals 2

    .prologue
    .line 29
    invoke-direct {p0, p1}, Lcom/google/c/a/a/b/b/a/c/d;-><init>(Ljava/lang/String;)V

    .line 30
    invoke-interface {p3}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "At least one value must be specified for an insert mutation."

    invoke-static {v0, v1}, Lcom/google/c/a/a/b/h/c;->a(ZLjava/lang/String;)V

    .line 32
    iput p2, p0, Lcom/google/c/a/a/b/b/a/c/b;->a:I

    .line 33
    iput-object p3, p0, Lcom/google/c/a/a/b/b/a/c/b;->b:Ljava/util/List;

    .line 34
    return-void

    .line 30
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lcom/google/c/a/a/b/b/a/c/b;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public final b()Ljava/util/Set;
    .locals 1

    .prologue
    .line 70
    iget-object v0, p0, Lcom/google/c/a/a/b/b/a/c/b;->d:Ljava/util/Set;

    if-nez v0, :cond_0

    .line 71
    iget-object v0, p0, Lcom/google/c/a/a/b/b/a/c/b;->b:Ljava/util/List;

    invoke-static {v0}, Lcom/google/c/a/a/b/b/a/c/q;->b(Ljava/util/List;)Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, Lcom/google/c/a/a/b/b/a/c/b;->d:Ljava/util/Set;

    .line 73
    :cond_0
    iget-object v0, p0, Lcom/google/c/a/a/b/b/a/c/b;->d:Ljava/util/Set;

    return-object v0
.end method

.method public final c()I
    .locals 1

    .prologue
    .line 42
    iget v0, p0, Lcom/google/c/a/a/b/b/a/c/b;->a:I

    return v0
.end method

.method public final d()Ljava/util/List;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lcom/google/c/a/a/b/b/a/c/b;->b:Ljava/util/List;

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 59
    instance-of v1, p1, Lcom/google/c/a/a/b/b/a/c/b;

    if-nez v1, :cond_0

    .line 63
    :goto_0
    return v0

    .line 62
    :cond_0
    check-cast p1, Lcom/google/c/a/a/b/b/a/c/b;

    .line 63
    const/4 v1, 0x6

    new-array v1, v1, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/google/c/a/a/b/b/a/c/d;->c:Ljava/lang/String;

    aput-object v2, v1, v0

    const/4 v0, 0x1

    iget-object v2, p1, Lcom/google/c/a/a/b/b/a/c/d;->c:Ljava/lang/String;

    aput-object v2, v1, v0

    const/4 v0, 0x2

    iget v2, p0, Lcom/google/c/a/a/b/b/a/c/b;->a:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v0

    const/4 v0, 0x3

    iget v2, p1, Lcom/google/c/a/a/b/b/a/c/b;->a:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v0

    const/4 v0, 0x4

    iget-object v2, p0, Lcom/google/c/a/a/b/b/a/c/b;->b:Ljava/util/List;

    aput-object v2, v1, v0

    const/4 v0, 0x5

    iget-object v2, p1, Lcom/google/c/a/a/b/b/a/c/b;->b:Ljava/util/List;

    aput-object v2, v1, v0

    invoke-static {v1}, Lcom/google/c/a/a/b/h/b;->a([Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 55
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/c/a/a/b/b/a/c/d;->c:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget v2, p0, Lcom/google/c/a/a/b/b/a/c/b;->a:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/c/a/a/b/b/a/c/b;->b:Ljava/util/List;

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 51
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "AbstractInsertMutation @ "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/c/a/a/b/b/a/c/d;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/c/a/a/b/b/a/c/b;->a:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/c/a/a/b/b/a/c/b;->b:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
