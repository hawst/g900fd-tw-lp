.class public final Lcom/google/c/a/a/b/b/a/b/h;
.super Lcom/google/c/a/a/b/b/a/b/a;
.source "SourceFile"

# interfaces
.implements Lcom/google/c/a/a/b/b/a/e;


# instance fields
.field private final b:Ljava/util/List;

.field private c:I


# direct methods
.method public constructor <init>(Lcom/google/c/a/a/b/b/a/b/w;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 56
    invoke-direct {p0, p1, p2}, Lcom/google/c/a/a/b/b/a/b/a;-><init>(Lcom/google/c/a/a/b/b/a/b/w;Ljava/lang/String;)V

    .line 52
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/c/a/a/b/b/a/b/h;->b:Ljava/util/List;

    .line 53
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/c/a/a/b/b/a/b/h;->c:I

    .line 57
    return-void
.end method

.method private a(Lcom/google/c/a/a/b/b/a/c/k;)Lcom/google/c/a/a/b/b/a/c/r;
    .locals 4

    .prologue
    .line 140
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 141
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p1}, Lcom/google/c/a/a/b/b/a/c/k;->a()I

    move-result v2

    if-ge v0, v2, :cond_0

    .line 142
    invoke-virtual {p1}, Lcom/google/c/a/a/b/b/a/c/k;->c()I

    move-result v2

    add-int/2addr v2, v0

    .line 143
    invoke-direct {p0, v2}, Lcom/google/c/a/a/b/b/a/b/h;->a(I)Lcom/google/c/a/a/b/b/a/r;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 141
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 145
    :cond_0
    new-instance v0, Lcom/google/c/a/a/b/b/a/c/i;

    invoke-virtual {p1}, Lcom/google/c/a/a/b/b/a/c/k;->e()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lcom/google/c/a/a/b/b/a/c/k;->c()I

    move-result v3

    invoke-direct {v0, v2, v3, v1}, Lcom/google/c/a/a/b/b/a/c/i;-><init>(Ljava/lang/String;ILjava/util/List;)V

    return-object v0
.end method

.method private a(Lcom/google/c/a/a/b/b/a/c/s;)Lcom/google/c/a/a/b/b/a/c/r;
    .locals 4

    .prologue
    .line 165
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 166
    invoke-virtual {p1}, Lcom/google/c/a/a/b/b/a/c/s;->c()I

    move-result v2

    .line 167
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p1}, Lcom/google/c/a/a/b/b/a/c/s;->d()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-ge v0, v3, :cond_0

    .line 168
    add-int v3, v2, v0

    invoke-direct {p0, v3}, Lcom/google/c/a/a/b/b/a/b/h;->a(I)Lcom/google/c/a/a/b/b/a/r;

    move-result-object v3

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 167
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 170
    :cond_0
    new-instance v0, Lcom/google/c/a/a/b/b/a/c/s;

    invoke-virtual {p1}, Lcom/google/c/a/a/b/b/a/c/s;->e()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lcom/google/c/a/a/b/b/a/c/s;->c()I

    move-result v3

    invoke-direct {v0, v2, v3, v1}, Lcom/google/c/a/a/b/b/a/c/s;-><init>(Ljava/lang/String;ILjava/util/List;)V

    return-object v0
.end method

.method private a(I)Lcom/google/c/a/a/b/b/a/r;
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Lcom/google/c/a/a/b/b/a/b/h;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/c/a/a/b/b/a/r;

    return-object v0
.end method

.method private a(Lcom/google/c/a/a/b/b/a/c/b;Lcom/google/c/a/a/b/b/a/o;ZLcom/google/c/a/a/b/b/a/b/y;Lcom/google/c/a/a/b/b/a/k;)V
    .locals 9

    .prologue
    .line 175
    invoke-virtual {p1}, Lcom/google/c/a/a/b/b/a/c/b;->c()I

    move-result v0

    .line 176
    const/4 v8, 0x0

    .line 177
    if-eqz p3, :cond_0

    .line 178
    new-instance v8, Ljava/util/ArrayList;

    invoke-virtual {p1}, Lcom/google/c/a/a/b/b/a/c/b;->d()Ljava/util/List;

    move-result-object v1

    invoke-direct {v8, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 181
    :cond_0
    invoke-virtual {p1}, Lcom/google/c/a/a/b/b/a/c/b;->d()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v1, v0

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/c/a/a/b/b/a/r;

    .line 182
    if-eqz v0, :cond_1

    .line 183
    iget v2, p0, Lcom/google/c/a/a/b/b/a/b/h;->c:I

    invoke-interface {v0}, Lcom/google/c/a/a/b/b/a/r;->b()I

    move-result v4

    add-int/2addr v2, v4

    iput v2, p0, Lcom/google/c/a/a/b/b/a/b/h;->c:I

    .line 185
    :cond_1
    iget-object v4, p0, Lcom/google/c/a/a/b/b/a/b/h;->b:Ljava/util/List;

    add-int/lit8 v2, v1, 0x1

    invoke-interface {v4, v1, v0}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 186
    invoke-virtual {p0, v0}, Lcom/google/c/a/a/b/b/a/b/h;->a(Lcom/google/c/a/a/b/b/a/r;)V

    move v1, v2

    .line 187
    goto :goto_0

    .line 189
    :cond_2
    if-eqz p3, :cond_3

    .line 190
    new-instance v0, Lcom/google/c/a/a/b/b/a/a/h;

    invoke-virtual {p2}, Lcom/google/c/a/a/b/b/a/o;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p2}, Lcom/google/c/a/a/b/b/a/o;->c()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p2}, Lcom/google/c/a/a/b/b/a/o;->d()Ljava/util/List;

    move-result-object v4

    invoke-virtual {p2}, Lcom/google/c/a/a/b/b/a/o;->a()Z

    move-result v5

    invoke-virtual {p1}, Lcom/google/c/a/a/b/b/a/c/b;->c()I

    move-result v7

    move-object v1, p0

    move-object v6, p5

    invoke-direct/range {v0 .. v8}, Lcom/google/c/a/a/b/b/a/a/h;-><init>(Lcom/google/c/a/a/b/b/a/e;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;ZLcom/google/c/a/a/b/b/a/k;ILjava/util/List;)V

    .line 193
    invoke-virtual {p4, v0}, Lcom/google/c/a/a/b/b/a/b/y;->a(Lcom/google/c/a/a/b/b/a/a/a;)V

    .line 195
    :cond_3
    return-void
.end method

.method private a(Lcom/google/c/a/a/b/b/a/c/s;Lcom/google/c/a/a/b/b/a/o;ZLcom/google/c/a/a/b/b/a/b/y;Lcom/google/c/a/a/b/b/a/k;)V
    .locals 10

    .prologue
    const/4 v9, 0x0

    const/4 v3, 0x0

    .line 199
    invoke-virtual {p1}, Lcom/google/c/a/a/b/b/a/c/s;->c()I

    move-result v0

    .line 202
    if-eqz p3, :cond_5

    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    .line 203
    :goto_0
    if-eqz p3, :cond_0

    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    .line 205
    :cond_0
    invoke-virtual {p1}, Lcom/google/c/a/a/b/b/a/c/s;->d()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    move v2, v3

    move v1, v0

    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/c/a/a/b/b/a/r;

    .line 206
    iget-object v6, p0, Lcom/google/c/a/a/b/b/a/b/h;->b:Ljava/util/List;

    add-int/lit8 v4, v1, 0x1

    invoke-interface {v6, v1, v0}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/c/a/a/b/b/a/r;

    .line 207
    if-eqz v1, :cond_1

    .line 208
    iget v6, p0, Lcom/google/c/a/a/b/b/a/b/h;->c:I

    invoke-interface {v1}, Lcom/google/c/a/a/b/b/a/r;->b()I

    move-result v7

    sub-int/2addr v6, v7

    iput v6, p0, Lcom/google/c/a/a/b/b/a/b/h;->c:I

    .line 210
    :cond_1
    if-eqz v0, :cond_2

    .line 211
    iget v6, p0, Lcom/google/c/a/a/b/b/a/b/h;->c:I

    invoke-interface {v0}, Lcom/google/c/a/a/b/b/a/r;->b()I

    move-result v7

    add-int/2addr v6, v7

    iput v6, p0, Lcom/google/c/a/a/b/b/a/b/h;->c:I

    .line 214
    :cond_2
    invoke-virtual {p0, v0}, Lcom/google/c/a/a/b/b/a/b/h;->a(Lcom/google/c/a/a/b/b/a/r;)V

    .line 215
    invoke-virtual {p0, v1}, Lcom/google/c/a/a/b/b/a/b/h;->b(Lcom/google/c/a/a/b/b/a/r;)Z

    move-result v6

    if-nez v6, :cond_3

    if-eqz v2, :cond_6

    :cond_3
    const/4 v2, 0x1

    .line 217
    :goto_2
    if-eqz v9, :cond_4

    if-eqz v8, :cond_4

    .line 218
    invoke-interface {v8, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 219
    invoke-interface {v9, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_4
    move v1, v4

    .line 221
    goto :goto_1

    :cond_5
    move-object v8, v9

    .line 202
    goto :goto_0

    :cond_6
    move v2, v3

    .line 215
    goto :goto_2

    .line 223
    :cond_7
    if-eqz v2, :cond_8

    .line 224
    invoke-virtual {p4}, Lcom/google/c/a/a/b/b/a/b/y;->a()V

    .line 226
    :cond_8
    if-eqz p3, :cond_9

    .line 227
    new-instance v0, Lcom/google/c/a/a/b/b/a/a/j;

    invoke-virtual {p2}, Lcom/google/c/a/a/b/b/a/o;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p2}, Lcom/google/c/a/a/b/b/a/o;->c()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p2}, Lcom/google/c/a/a/b/b/a/o;->d()Ljava/util/List;

    move-result-object v4

    invoke-virtual {p2}, Lcom/google/c/a/a/b/b/a/o;->a()Z

    move-result v5

    invoke-virtual {p1}, Lcom/google/c/a/a/b/b/a/c/s;->c()I

    move-result v7

    move-object v1, p0

    move-object v6, p5

    invoke-direct/range {v0 .. v9}, Lcom/google/c/a/a/b/b/a/a/j;-><init>(Lcom/google/c/a/a/b/b/a/e;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;ZLcom/google/c/a/a/b/b/a/k;ILjava/util/List;Ljava/util/List;)V

    .line 230
    invoke-virtual {p4, v0}, Lcom/google/c/a/a/b/b/a/b/y;->a(Lcom/google/c/a/a/b/b/a/a/a;)V

    .line 232
    :cond_9
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lcom/google/c/a/a/b/b/a/b/h;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public final a(Lcom/google/c/a/a/b/b/a/i;)I
    .locals 3

    .prologue
    .line 432
    const/16 v0, 0x11

    .line 435
    iget-object v1, p0, Lcom/google/c/a/a/b/b/a/b/h;->b:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/c/a/a/b/b/a/r;

    .line 436
    mul-int/lit8 v1, v1, 0x25

    invoke-interface {p1, v0}, Lcom/google/c/a/a/b/b/a/i;->a(Lcom/google/c/a/a/b/b/a/r;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    add-int/2addr v0, v1

    move v1, v0

    .line 437
    goto :goto_0

    .line 439
    :cond_0
    return v1
.end method

.method public final a(II)Lcom/google/c/a/a/b/b/a/a;
    .locals 3

    .prologue
    .line 377
    new-instance v0, Lcom/google/c/a/a/b/b/a/c/f;

    invoke-super {p0}, Lcom/google/c/a/a/b/b/a/b/a;->d()Ljava/lang/String;

    move-result-object v1

    sub-int v2, p2, p1

    invoke-direct {v0, v1, p1, v2}, Lcom/google/c/a/a/b/b/a/c/f;-><init>(Ljava/lang/String;II)V

    .line 378
    invoke-super {p0}, Lcom/google/c/a/a/b/b/a/b/a;->j()Lcom/google/c/a/a/b/b/a/b;

    move-result-object v1

    iget-object v2, p0, Lcom/google/c/a/a/b/b/a/b/h;->d_:Lcom/google/c/a/a/b/b/a/b/w;

    iget-object v2, v2, Lcom/google/c/a/a/b/b/a/b/w;->a:Lcom/google/c/a/a/b/b/a/b/ab;

    invoke-static {v2, v0}, Lcom/google/c/a/a/b/b/a/b/s;->a(Lcom/google/c/a/a/b/b/a/b/ab;Lcom/google/c/b/a/c;)Lcom/google/c/b/a/c;

    move-result-object v0

    sget-object v2, Lcom/google/c/a/a/b/b/a/k;->a:Lcom/google/c/a/a/b/b/a/k;

    invoke-interface {v1, v0, v2}, Lcom/google/c/a/a/b/b/a/b;->a(Lcom/google/c/b/a/c;Lcom/google/c/a/a/b/b/a/k;)Lcom/google/c/a/a/b/b/a/a;

    move-result-object v0

    return-object v0
.end method

.method public final a(ILcom/google/c/a/a/b/b/a/e;I)Lcom/google/c/a/a/b/b/a/a;
    .locals 4

    .prologue
    .line 396
    if-ltz p1, :cond_0

    invoke-virtual {p0}, Lcom/google/c/a/a/b/b/a/b/h;->a()I

    move-result v0

    if-ge p1, v0, :cond_0

    invoke-interface {p2}, Lcom/google/c/a/a/b/b/a/e;->a()I

    move-result v0

    if-le p3, v0, :cond_1

    .line 397
    :cond_0
    new-instance v0, Ljava/lang/IndexOutOfBoundsException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "srcIndex: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " srcSize: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/c/a/a/b/b/a/b/h;->a()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "destIndex: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "destSize: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-interface {p2}, Lcom/google/c/a/a/b/b/a/e;->a()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 400
    :cond_1
    invoke-super {p0}, Lcom/google/c/a/a/b/b/a/b/a;->j()Lcom/google/c/a/a/b/b/a/b;

    move-result-object v0

    new-instance v1, Lcom/google/c/a/a/b/b/a/c/m;

    invoke-super {p0}, Lcom/google/c/a/a/b/b/a/b/a;->d()Ljava/lang/String;

    move-result-object v2

    invoke-interface {p2}, Lcom/google/c/a/a/b/b/a/e;->d()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, p1, v3, p3}, Lcom/google/c/a/a/b/b/a/c/m;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    sget-object v2, Lcom/google/c/a/a/b/b/a/k;->a:Lcom/google/c/a/a/b/b/a/k;

    invoke-interface {v0, v1, v2}, Lcom/google/c/a/a/b/b/a/b;->a(Lcom/google/c/b/a/c;Lcom/google/c/a/a/b/b/a/k;)Lcom/google/c/a/a/b/b/a/a;

    move-result-object v0

    return-object v0
.end method

.method public final a(ILjava/util/Collection;)Lcom/google/c/a/a/b/b/a/a;
    .locals 4

    .prologue
    .line 332
    invoke-interface {p2}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 333
    invoke-super {p0}, Lcom/google/c/a/a/b/b/a/b/a;->j()Lcom/google/c/a/a/b/b/a/b;

    move-result-object v0

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1, p2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    new-instance v2, Lcom/google/c/a/a/b/b/a/c/i;

    invoke-super {p0}, Lcom/google/c/a/a/b/b/a/b/a;->d()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3, p1, v1}, Lcom/google/c/a/a/b/b/a/c/i;-><init>(Ljava/lang/String;ILjava/util/List;)V

    sget-object v1, Lcom/google/c/a/a/b/b/a/k;->a:Lcom/google/c/a/a/b/b/a/k;

    invoke-interface {v0, v2, v1}, Lcom/google/c/a/a/b/b/a/b;->a(Lcom/google/c/b/a/c;Lcom/google/c/a/a/b/b/a/k;)Lcom/google/c/a/a/b/b/a/a;

    move-result-object v0

    .line 336
    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lcom/google/c/a/a/b/b/a/a;->f()Lcom/google/c/a/a/b/b/a/a;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(ILjava/util/List;)Lcom/google/c/a/a/b/b/a/a;
    .locals 4

    .prologue
    .line 385
    invoke-super {p0}, Lcom/google/c/a/a/b/b/a/b/a;->j()Lcom/google/c/a/a/b/b/a/b;

    move-result-object v0

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1, p2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    new-instance v2, Lcom/google/c/a/a/b/b/a/c/s;

    invoke-super {p0}, Lcom/google/c/a/a/b/b/a/b/a;->d()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3, p1, v1}, Lcom/google/c/a/a/b/b/a/c/s;-><init>(Ljava/lang/String;ILjava/util/List;)V

    sget-object v1, Lcom/google/c/a/a/b/b/a/k;->a:Lcom/google/c/a/a/b/b/a/k;

    invoke-interface {v0, v2, v1}, Lcom/google/c/a/a/b/b/a/b;->a(Lcom/google/c/b/a/c;Lcom/google/c/a/a/b/b/a/k;)Lcom/google/c/a/a/b/b/a/a;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/c/a/a/b/b/a/c/r;)Lcom/google/c/a/a/b/b/a/c/r;
    .locals 4

    .prologue
    .line 117
    instance-of v0, p1, Lcom/google/c/a/a/b/b/a/c/f;

    if-eqz v0, :cond_1

    .line 118
    check-cast p1, Lcom/google/c/a/a/b/b/a/c/f;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p1}, Lcom/google/c/a/a/b/b/a/c/f;->a()I

    move-result v2

    if-ge v0, v2, :cond_0

    invoke-virtual {p1}, Lcom/google/c/a/a/b/b/a/c/f;->c()I

    move-result v2

    add-int/2addr v2, v0

    invoke-direct {p0, v2}, Lcom/google/c/a/a/b/b/a/b/h;->a(I)Lcom/google/c/a/a/b/b/a/r;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    new-instance v0, Lcom/google/c/a/a/b/b/a/c/j;

    invoke-virtual {p1}, Lcom/google/c/a/a/b/b/a/c/f;->e()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lcom/google/c/a/a/b/b/a/c/f;->c()I

    move-result v3

    invoke-direct {v0, v2, v3, v1}, Lcom/google/c/a/a/b/b/a/c/j;-><init>(Ljava/lang/String;ILjava/util/List;)V

    .line 126
    :goto_1
    return-object v0

    .line 119
    :cond_1
    instance-of v0, p1, Lcom/google/c/a/a/b/b/a/c/j;

    if-eqz v0, :cond_2

    .line 120
    check-cast p1, Lcom/google/c/a/a/b/b/a/c/j;

    new-instance v0, Lcom/google/c/a/a/b/b/a/c/f;

    invoke-virtual {p1}, Lcom/google/c/a/a/b/b/a/c/j;->e()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/c/a/a/b/b/a/c/j;->c()I

    move-result v2

    invoke-virtual {p1}, Lcom/google/c/a/a/b/b/a/c/j;->d()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    invoke-direct {v0, v1, v2, v3}, Lcom/google/c/a/a/b/b/a/c/f;-><init>(Ljava/lang/String;II)V

    goto :goto_1

    .line 121
    :cond_2
    instance-of v0, p1, Lcom/google/c/a/a/b/b/a/c/i;

    if-eqz v0, :cond_3

    .line 122
    check-cast p1, Lcom/google/c/a/a/b/b/a/c/i;

    new-instance v0, Lcom/google/c/a/a/b/b/a/c/k;

    invoke-virtual {p1}, Lcom/google/c/a/a/b/b/a/c/i;->e()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/c/a/a/b/b/a/c/i;->c()I

    move-result v2

    invoke-virtual {p1}, Lcom/google/c/a/a/b/b/a/c/i;->d()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    invoke-direct {v0, v1, v2, v3}, Lcom/google/c/a/a/b/b/a/c/k;-><init>(Ljava/lang/String;II)V

    goto :goto_1

    .line 123
    :cond_3
    instance-of v0, p1, Lcom/google/c/a/a/b/b/a/c/k;

    if-eqz v0, :cond_4

    .line 124
    check-cast p1, Lcom/google/c/a/a/b/b/a/c/k;

    invoke-direct {p0, p1}, Lcom/google/c/a/a/b/b/a/b/h;->a(Lcom/google/c/a/a/b/b/a/c/k;)Lcom/google/c/a/a/b/b/a/c/r;

    move-result-object v0

    goto :goto_1

    .line 125
    :cond_4
    instance-of v0, p1, Lcom/google/c/a/a/b/b/a/c/s;

    if-eqz v0, :cond_5

    .line 126
    check-cast p1, Lcom/google/c/a/a/b/b/a/c/s;

    invoke-direct {p0, p1}, Lcom/google/c/a/a/b/b/a/b/h;->a(Lcom/google/c/a/a/b/b/a/c/s;)Lcom/google/c/a/a/b/b/a/c/r;

    move-result-object v0

    goto :goto_1

    .line 128
    :cond_5
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Only insert and delete mutations can be applied to arrays."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final a(Lcom/google/c/a/a/b/b/a/c/m;Lcom/google/c/a/a/b/b/a/o;ZLcom/google/c/a/a/b/b/a/b/y;Lcom/google/c/a/a/b/b/a/k;)V
    .locals 11

    .prologue
    .line 241
    invoke-super {p0}, Lcom/google/c/a/a/b/b/a/b/a;->j()Lcom/google/c/a/a/b/b/a/b;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/c/a/a/b/b/a/c/m;->a()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/c/a/a/b/b/a/b;->a(Ljava/lang/String;)Lcom/google/c/a/a/b/b/a/g;

    move-result-object v9

    check-cast v9, Lcom/google/c/a/a/b/b/a/b/h;

    .line 243
    new-instance v8, Ljava/util/ArrayList;

    iget-object v0, v9, Lcom/google/c/a/a/b/b/a/b/h;->b:Ljava/util/List;

    invoke-virtual {p1}, Lcom/google/c/a/a/b/b/a/c/m;->c()I

    move-result v1

    invoke-virtual {p1}, Lcom/google/c/a/a/b/b/a/c/m;->c()I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    invoke-interface {v0, v1, v2}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v0

    invoke-direct {v8, v0}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 245
    iget-object v0, p0, Lcom/google/c/a/a/b/b/a/b/h;->b:Ljava/util/List;

    invoke-virtual {p1}, Lcom/google/c/a/a/b/b/a/c/m;->e()I

    move-result v1

    invoke-interface {v0, v1, v8}, Ljava/util/List;->addAll(ILjava/util/Collection;)Z

    .line 246
    invoke-interface {v8}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/c/a/a/b/b/a/r;

    .line 247
    if-eqz v0, :cond_0

    .line 248
    iget v2, p0, Lcom/google/c/a/a/b/b/a/b/h;->c:I

    invoke-interface {v0}, Lcom/google/c/a/a/b/b/a/r;->b()I

    move-result v3

    add-int/2addr v2, v3

    iput v2, p0, Lcom/google/c/a/a/b/b/a/b/h;->c:I

    .line 250
    :cond_0
    invoke-virtual {p0, v0}, Lcom/google/c/a/a/b/b/a/b/h;->a(Lcom/google/c/a/a/b/b/a/r;)V

    goto :goto_0

    .line 252
    :cond_1
    if-eqz p3, :cond_2

    .line 253
    new-instance v0, Lcom/google/c/a/a/b/b/a/a/h;

    invoke-virtual {p2}, Lcom/google/c/a/a/b/b/a/o;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p2}, Lcom/google/c/a/a/b/b/a/o;->c()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p2}, Lcom/google/c/a/a/b/b/a/o;->d()Ljava/util/List;

    move-result-object v4

    invoke-virtual {p2}, Lcom/google/c/a/a/b/b/a/o;->a()Z

    move-result v5

    invoke-virtual {p1}, Lcom/google/c/a/a/b/b/a/c/m;->e()I

    move-result v7

    invoke-virtual {p1}, Lcom/google/c/a/a/b/b/a/c/m;->c()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    move-object v1, p0

    move-object/from16 v6, p5

    invoke-direct/range {v0 .. v10}, Lcom/google/c/a/a/b/b/a/a/h;-><init>(Lcom/google/c/a/a/b/b/a/e;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;ZLcom/google/c/a/a/b/b/a/k;ILjava/util/List;Lcom/google/c/a/a/b/b/a/e;Ljava/lang/Integer;)V

    .line 256
    invoke-virtual {p4, v0}, Lcom/google/c/a/a/b/b/a/b/y;->a(Lcom/google/c/a/a/b/b/a/a/a;)V

    .line 258
    :cond_2
    return-void
.end method

.method public final a(Lcom/google/c/a/a/b/b/a/c/p;Lcom/google/c/a/a/b/b/a/o;ZLcom/google/c/a/a/b/b/a/b/y;Lcom/google/c/a/a/b/b/a/k;)V
    .locals 9

    .prologue
    const/4 v2, 0x0

    .line 72
    instance-of v0, p1, Lcom/google/c/a/a/b/b/a/c/a;

    if-eqz v0, :cond_7

    .line 73
    check-cast p1, Lcom/google/c/a/a/b/b/a/c/a;

    iget-object v0, p0, Lcom/google/c/a/a/b/b/a/b/h;->b:Ljava/util/List;

    invoke-virtual {p1}, Lcom/google/c/a/a/b/b/a/c/a;->c()I

    move-result v1

    invoke-virtual {p1}, Lcom/google/c/a/a/b/b/a/c/a;->c()I

    move-result v3

    invoke-virtual {p1}, Lcom/google/c/a/a/b/b/a/c/a;->a()I

    move-result v4

    add-int/2addr v3, v4

    invoke-interface {v0, v1, v3}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move v1, v2

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/c/a/a/b/b/a/r;

    if-eqz v0, :cond_0

    iget v5, p0, Lcom/google/c/a/a/b/b/a/b/h;->c:I

    invoke-interface {v0}, Lcom/google/c/a/a/b/b/a/r;->b()I

    move-result v6

    sub-int/2addr v5, v6

    iput v5, p0, Lcom/google/c/a/a/b/b/a/b/h;->c:I

    :cond_0
    invoke-virtual {p0, v0}, Lcom/google/c/a/a/b/b/a/b/h;->b(Lcom/google/c/a/a/b/b/a/r;)Z

    move-result v0

    if-nez v0, :cond_1

    if-eqz v1, :cond_2

    :cond_1
    const/4 v0, 0x1

    :goto_1
    move v1, v0

    goto :goto_0

    :cond_2
    move v0, v2

    goto :goto_1

    :cond_3
    const/4 v8, 0x0

    if-eqz p3, :cond_4

    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    :cond_4
    invoke-interface {v3}, Ljava/util/List;->clear()V

    if-eqz v1, :cond_5

    invoke-virtual {p4}, Lcom/google/c/a/a/b/b/a/b/y;->a()V

    :cond_5
    if-eqz p3, :cond_6

    new-instance v0, Lcom/google/c/a/a/b/b/a/a/i;

    invoke-virtual {p2}, Lcom/google/c/a/a/b/b/a/o;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p2}, Lcom/google/c/a/a/b/b/a/o;->c()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p2}, Lcom/google/c/a/a/b/b/a/o;->d()Ljava/util/List;

    move-result-object v4

    invoke-virtual {p2}, Lcom/google/c/a/a/b/b/a/o;->a()Z

    move-result v5

    invoke-virtual {p1}, Lcom/google/c/a/a/b/b/a/c/a;->c()I

    move-result v7

    move-object v1, p0

    move-object v6, p5

    invoke-direct/range {v0 .. v8}, Lcom/google/c/a/a/b/b/a/a/i;-><init>(Lcom/google/c/a/a/b/b/a/e;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;ZLcom/google/c/a/a/b/b/a/k;ILjava/util/List;)V

    invoke-virtual {p4, v0}, Lcom/google/c/a/a/b/b/a/b/y;->a(Lcom/google/c/a/a/b/b/a/a/a;)V

    .line 79
    :cond_6
    :goto_2
    return-void

    .line 75
    :cond_7
    instance-of v0, p1, Lcom/google/c/a/a/b/b/a/c/b;

    if-eqz v0, :cond_8

    move-object v1, p1

    .line 76
    check-cast v1, Lcom/google/c/a/a/b/b/a/c/b;

    move-object v0, p0

    move-object v2, p2

    move v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-direct/range {v0 .. v5}, Lcom/google/c/a/a/b/b/a/b/h;->a(Lcom/google/c/a/a/b/b/a/c/b;Lcom/google/c/a/a/b/b/a/o;ZLcom/google/c/a/a/b/b/a/b/y;Lcom/google/c/a/a/b/b/a/k;)V

    goto :goto_2

    .line 78
    :cond_8
    instance-of v0, p1, Lcom/google/c/a/a/b/b/a/c/s;

    if-eqz v0, :cond_9

    move-object v1, p1

    .line 79
    check-cast v1, Lcom/google/c/a/a/b/b/a/c/s;

    move-object v0, p0

    move-object v2, p2

    move v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-direct/range {v0 .. v5}, Lcom/google/c/a/a/b/b/a/b/h;->a(Lcom/google/c/a/a/b/b/a/c/s;Lcom/google/c/a/a/b/b/a/o;ZLcom/google/c/a/a/b/b/a/b/y;Lcom/google/c/a/a/b/b/a/k;)V

    goto :goto_2

    .line 81
    :cond_9
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Only insert and delete mutations can be applied to arrays."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final a(Lcom/google/c/a/a/b/b/a/j;Lcom/google/c/a/a/b/b/a/g;)Z
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 412
    instance-of v0, p2, Lcom/google/c/a/a/b/b/a/b/h;

    if-nez v0, :cond_1

    .line 427
    :cond_0
    :goto_0
    return v1

    .line 415
    :cond_1
    check-cast p2, Lcom/google/c/a/a/b/b/a/b/h;

    .line 417
    invoke-virtual {p0}, Lcom/google/c/a/a/b/b/a/b/h;->a()I

    move-result v0

    invoke-virtual {p2}, Lcom/google/c/a/a/b/b/a/b/h;->a()I

    move-result v2

    if-ne v0, v2, :cond_0

    move v0, v1

    .line 421
    :goto_1
    invoke-virtual {p0}, Lcom/google/c/a/a/b/b/a/b/h;->a()I

    move-result v2

    if-ge v0, v2, :cond_2

    .line 422
    invoke-direct {p0, v0}, Lcom/google/c/a/a/b/b/a/b/h;->a(I)Lcom/google/c/a/a/b/b/a/r;

    move-result-object v2

    invoke-direct {p2, v0}, Lcom/google/c/a/a/b/b/a/b/h;->a(I)Lcom/google/c/a/a/b/b/a/r;

    move-result-object v3

    invoke-super {p0}, Lcom/google/c/a/a/b/b/a/b/a;->j()Lcom/google/c/a/a/b/b/a/b;

    move-result-object v4

    invoke-super {p2}, Lcom/google/c/a/a/b/b/a/b/a;->j()Lcom/google/c/a/a/b/b/a/b;

    move-result-object v5

    invoke-interface {p1, v2, v3, v4, v5}, Lcom/google/c/a/a/b/b/a/j;->a(Lcom/google/c/a/a/b/b/a/r;Lcom/google/c/a/a/b/b/a/r;Lcom/google/c/a/a/b/b/a/b;Lcom/google/c/a/a/b/b/a/b;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 421
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 427
    :cond_2
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public final b(Lcom/google/c/a/a/b/b/a/i;)Ljava/lang/String;
    .locals 5

    .prologue
    .line 443
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 444
    const-string v0, "["

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 445
    const/4 v0, 0x1

    .line 446
    iget-object v1, p0, Lcom/google/c/a/a/b/b/a/b/h;->b:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v1, v0

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/c/a/a/b/b/a/r;

    .line 447
    if-nez v1, :cond_0

    .line 448
    const-string v4, ", "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 452
    :goto_1
    invoke-interface {p1, v0}, Lcom/google/c/a/a/b/b/a/i;->a(Lcom/google/c/a/a/b/b/a/r;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 450
    :cond_0
    const/4 v1, 0x0

    goto :goto_1

    .line 454
    :cond_1
    const-string v0, "]"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 455
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final b()Ljava/util/List;
    .locals 1

    .prologue
    .line 479
    iget-object v0, p0, Lcom/google/c/a/a/b/b/a/b/h;->b:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public final b(Lcom/google/c/a/a/b/b/a/c/m;Lcom/google/c/a/a/b/b/a/o;ZLcom/google/c/a/a/b/b/a/b/y;Lcom/google/c/a/a/b/b/a/k;)V
    .locals 11

    .prologue
    .line 267
    invoke-virtual {p1}, Lcom/google/c/a/a/b/b/a/c/m;->c()I

    move-result v7

    .line 268
    invoke-virtual {p1}, Lcom/google/c/a/a/b/b/a/c/m;->e()I

    move-result v1

    .line 269
    invoke-virtual {p1}, Lcom/google/c/a/a/b/b/a/c/m;->d()Ljava/lang/String;

    move-result-object v0

    .line 270
    invoke-super {p0}, Lcom/google/c/a/a/b/b/a/b/a;->j()Lcom/google/c/a/a/b/b/a/b;

    move-result-object v2

    invoke-interface {v2, v0}, Lcom/google/c/a/a/b/b/a/b;->a(Ljava/lang/String;)Lcom/google/c/a/a/b/b/a/g;

    move-result-object v9

    check-cast v9, Lcom/google/c/a/a/b/b/a/b/h;

    .line 272
    invoke-virtual {p1}, Lcom/google/c/a/a/b/b/a/c/m;->d()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/c/a/a/b/b/a/c/m;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    if-ge v1, v7, :cond_0

    .line 277
    add-int/lit8 v7, v7, 0x1

    .line 279
    :cond_0
    iget-object v0, p0, Lcom/google/c/a/a/b/b/a/b/h;->b:Ljava/util/List;

    add-int/lit8 v2, v7, 0x1

    invoke-interface {v0, v7, v2}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v2

    .line 280
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_1
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/c/a/a/b/b/a/r;

    .line 281
    if-eqz v0, :cond_2

    .line 282
    iget v4, p0, Lcom/google/c/a/a/b/b/a/b/h;->c:I

    invoke-interface {v0}, Lcom/google/c/a/a/b/b/a/r;->b()I

    move-result v5

    sub-int/2addr v4, v5

    iput v4, p0, Lcom/google/c/a/a/b/b/a/b/h;->c:I

    .line 284
    :cond_2
    invoke-virtual {p0, v0}, Lcom/google/c/a/a/b/b/a/b/h;->b(Lcom/google/c/a/a/b/b/a/r;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 285
    invoke-virtual {p4}, Lcom/google/c/a/a/b/b/a/b/y;->a()V

    goto :goto_0

    .line 288
    :cond_3
    const/4 v8, 0x0

    .line 289
    if-eqz p3, :cond_4

    .line 290
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 292
    :cond_4
    invoke-interface {v2}, Ljava/util/List;->clear()V

    .line 293
    if-eqz p3, :cond_5

    .line 294
    new-instance v0, Lcom/google/c/a/a/b/b/a/a/i;

    invoke-virtual {p2}, Lcom/google/c/a/a/b/b/a/o;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p2}, Lcom/google/c/a/a/b/b/a/o;->c()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p2}, Lcom/google/c/a/a/b/b/a/o;->d()Ljava/util/List;

    move-result-object v4

    invoke-virtual {p2}, Lcom/google/c/a/a/b/b/a/o;->a()Z

    move-result v5

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    move-object v1, p0

    move-object/from16 v6, p5

    invoke-direct/range {v0 .. v10}, Lcom/google/c/a/a/b/b/a/a/i;-><init>(Lcom/google/c/a/a/b/b/a/e;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;ZLcom/google/c/a/a/b/b/a/k;ILjava/util/List;Lcom/google/c/a/a/b/b/a/e;Ljava/lang/Integer;)V

    .line 297
    invoke-virtual {p4, v0}, Lcom/google/c/a/a/b/b/a/b/y;->a(Lcom/google/c/a/a/b/b/a/a/a;)V

    .line 299
    :cond_5
    return-void
.end method

.method public final bridge synthetic d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 49
    invoke-super {p0}, Lcom/google/c/a/a/b/b/a/b/a;->d()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic e()Lcom/google/c/a/a/b/b/a/p;
    .locals 1

    .prologue
    .line 49
    invoke-super {p0}, Lcom/google/c/a/a/b/b/a/b/a;->e()Lcom/google/c/a/a/b/b/a/p;

    move-result-object v0

    return-object v0
.end method

.method public final f()Ljava/lang/String;
    .locals 1

    .prologue
    .line 469
    sget-object v0, Lcom/google/c/a/a/b/b/a/b/c;->b:Lcom/google/c/a/a/b/b/a/b/c;

    invoke-virtual {v0}, Lcom/google/c/a/a/b/b/a/b/c;->a()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic g()Ljava/util/List;
    .locals 1

    .prologue
    .line 49
    invoke-super {p0}, Lcom/google/c/a/a/b/b/a/b/a;->g()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public final i()Lcom/google/c/a/a/b/b/a/c/n;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 342
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 343
    invoke-virtual {p0}, Lcom/google/c/a/a/b/b/a/b/h;->a()I

    move-result v1

    if-lez v1, :cond_0

    .line 344
    new-instance v1, Lcom/google/c/a/a/b/b/a/c/f;

    invoke-super {p0}, Lcom/google/c/a/a/b/b/a/b/a;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0}, Lcom/google/c/a/a/b/b/a/b/h;->a()I

    move-result v3

    invoke-direct {v1, v2, v4, v3}, Lcom/google/c/a/a/b/b/a/c/f;-><init>(Ljava/lang/String;II)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 346
    :cond_0
    new-instance v1, Lcom/google/c/a/a/b/b/a/c/o;

    invoke-direct {v1}, Lcom/google/c/a/a/b/b/a/c/o;-><init>()V

    invoke-virtual {v1, v0}, Lcom/google/c/a/a/b/b/a/c/o;->a(Ljava/util/Collection;)Lcom/google/c/a/a/b/b/a/c/o;

    move-result-object v0

    iput-boolean v4, v0, Lcom/google/c/a/a/b/b/a/c/o;->d:Z

    invoke-virtual {v0}, Lcom/google/c/a/a/b/b/a/c/o;->a()Lcom/google/c/a/a/b/b/a/c/n;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic j()Lcom/google/c/a/a/b/b/a/b;
    .locals 1

    .prologue
    .line 49
    invoke-super {p0}, Lcom/google/c/a/a/b/b/a/b/a;->j()Lcom/google/c/a/a/b/b/a/b;

    move-result-object v0

    return-object v0
.end method

.method public final k()Ljava/util/List;
    .locals 4

    .prologue
    .line 303
    invoke-virtual {p0}, Lcom/google/c/a/a/b/b/a/b/h;->a()I

    move-result v0

    if-nez v0, :cond_0

    .line 304
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    .line 306
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/google/c/a/a/b/b/a/c/i;

    invoke-super {p0}, Lcom/google/c/a/a/b/b/a/b/a;->d()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/c/a/a/b/b/a/b/h;->b:Ljava/util/List;

    invoke-direct {v0, v1, v2, v3}, Lcom/google/c/a/a/b/b/a/c/i;-><init>(Ljava/lang/String;ILjava/util/List;)V

    invoke-static {v0}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    goto :goto_0
.end method

.method public final l()I
    .locals 1

    .prologue
    .line 464
    iget v0, p0, Lcom/google/c/a/a/b/b/a/b/h;->c:I

    add-int/lit8 v0, v0, 0x18

    return v0
.end method

.method public final bridge synthetic m()V
    .locals 0

    .prologue
    .line 49
    invoke-super {p0}, Lcom/google/c/a/a/b/b/a/b/a;->m()V

    return-void
.end method

.method protected final n()Ljava/lang/Iterable;
    .locals 1

    .prologue
    .line 474
    iget-object v0, p0, Lcom/google/c/a/a/b/b/a/b/h;->b:Ljava/util/List;

    return-object v0
.end method
