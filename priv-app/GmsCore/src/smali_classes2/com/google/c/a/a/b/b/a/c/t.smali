.class public Lcom/google/c/a/a/b/b/a/c/t;
.super Lcom/google/c/a/a/b/b/a/c/d;
.source "SourceFile"


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:Lcom/google/c/a/a/b/b/a/r;

.field private d:Ljava/util/Set;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Lcom/google/c/a/a/b/b/a/r;)V
    .locals 1

    .prologue
    .line 24
    invoke-direct {p0, p1}, Lcom/google/c/a/a/b/b/a/c/d;-><init>(Ljava/lang/String;)V

    .line 25
    const-string v0, "property cannot be null."

    invoke-static {p2, v0}, Lcom/google/c/a/a/b/h/c;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/google/c/a/a/b/b/a/c/t;->a:Ljava/lang/String;

    .line 26
    iput-object p3, p0, Lcom/google/c/a/a/b/b/a/c/t;->b:Lcom/google/c/a/a/b/b/a/r;

    .line 27
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lcom/google/c/a/a/b/b/a/c/t;->a:Ljava/lang/String;

    return-object v0
.end method

.method public final b()Ljava/util/Set;
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Lcom/google/c/a/a/b/b/a/c/t;->d:Ljava/util/Set;

    if-nez v0, :cond_0

    .line 58
    iget-object v0, p0, Lcom/google/c/a/a/b/b/a/c/t;->b:Lcom/google/c/a/a/b/b/a/r;

    instance-of v0, v0, Lcom/google/c/a/a/b/b/a/p;

    if-eqz v0, :cond_1

    .line 59
    iget-object v0, p0, Lcom/google/c/a/a/b/b/a/c/t;->b:Lcom/google/c/a/a/b/b/a/r;

    check-cast v0, Lcom/google/c/a/a/b/b/a/p;

    invoke-virtual {v0}, Lcom/google/c/a/a/b/b/a/p;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Collections;->singleton(Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, Lcom/google/c/a/a/b/b/a/c/t;->d:Ljava/util/Set;

    .line 64
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/google/c/a/a/b/b/a/c/t;->d:Ljava/util/Set;

    return-object v0

    .line 61
    :cond_1
    invoke-static {}, Ljava/util/Collections;->emptySet()Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, Lcom/google/c/a/a/b/b/a/c/t;->d:Ljava/util/Set;

    goto :goto_0
.end method

.method public final c()Lcom/google/c/a/a/b/b/a/r;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lcom/google/c/a/a/b/b/a/c/t;->b:Lcom/google/c/a/a/b/b/a/r;

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 42
    instance-of v1, p1, Lcom/google/c/a/a/b/b/a/c/t;

    if-nez v1, :cond_0

    .line 46
    :goto_0
    return v0

    .line 45
    :cond_0
    check-cast p1, Lcom/google/c/a/a/b/b/a/c/t;

    .line 46
    const/4 v1, 0x6

    new-array v1, v1, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/google/c/a/a/b/b/a/c/d;->c:Ljava/lang/String;

    aput-object v2, v1, v0

    const/4 v0, 0x1

    iget-object v2, p1, Lcom/google/c/a/a/b/b/a/c/d;->c:Ljava/lang/String;

    aput-object v2, v1, v0

    const/4 v0, 0x2

    iget-object v2, p0, Lcom/google/c/a/a/b/b/a/c/t;->a:Ljava/lang/String;

    aput-object v2, v1, v0

    const/4 v0, 0x3

    iget-object v2, p1, Lcom/google/c/a/a/b/b/a/c/t;->a:Ljava/lang/String;

    aput-object v2, v1, v0

    const/4 v0, 0x4

    iget-object v2, p0, Lcom/google/c/a/a/b/b/a/c/t;->b:Lcom/google/c/a/a/b/b/a/r;

    aput-object v2, v1, v0

    const/4 v0, 0x5

    iget-object v2, p1, Lcom/google/c/a/a/b/b/a/c/t;->b:Lcom/google/c/a/a/b/b/a/r;

    aput-object v2, v1, v0

    invoke-static {v1}, Lcom/google/c/a/a/b/h/b;->a([Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 38
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/c/a/a/b/b/a/c/d;->c:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/c/a/a/b/b/a/c/t;->a:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/c/a/a/b/b/a/c/t;->b:Lcom/google/c/a/a/b/b/a/r;

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 52
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "UpdateMutation: [propertyName="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/c/a/a/b/b/a/c/t;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", value="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/c/a/a/b/b/a/c/t;->b:Lcom/google/c/a/a/b/b/a/r;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
