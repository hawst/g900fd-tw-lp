.class public final enum Lcom/google/c/a/a/b/e/g;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field public static final enum a:Lcom/google/c/a/a/b/e/g;

.field public static final enum b:Lcom/google/c/a/a/b/e/g;

.field private static final synthetic c:[Lcom/google/c/a/a/b/e/g;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 71
    new-instance v0, Lcom/google/c/a/a/b/e/g;

    const-string v1, "SYNCHRONOUS"

    invoke-direct {v0, v1, v2}, Lcom/google/c/a/a/b/e/g;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/c/a/a/b/e/g;->a:Lcom/google/c/a/a/b/e/g;

    .line 77
    new-instance v0, Lcom/google/c/a/a/b/e/g;

    const-string v1, "ASYNCHRONOUS"

    invoke-direct {v0, v1, v3}, Lcom/google/c/a/a/b/e/g;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/c/a/a/b/e/g;->b:Lcom/google/c/a/a/b/e/g;

    .line 65
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/google/c/a/a/b/e/g;

    sget-object v1, Lcom/google/c/a/a/b/e/g;->a:Lcom/google/c/a/a/b/e/g;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/c/a/a/b/e/g;->b:Lcom/google/c/a/a/b/e/g;

    aput-object v1, v0, v3

    sput-object v0, Lcom/google/c/a/a/b/e/g;->c:[Lcom/google/c/a/a/b/e/g;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 65
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/c/a/a/b/e/g;
    .locals 1

    .prologue
    .line 65
    const-class v0, Lcom/google/c/a/a/b/e/g;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/c/a/a/b/e/g;

    return-object v0
.end method

.method public static values()[Lcom/google/c/a/a/b/e/g;
    .locals 1

    .prologue
    .line 65
    sget-object v0, Lcom/google/c/a/a/b/e/g;->c:[Lcom/google/c/a/a/b/e/g;

    invoke-virtual {v0}, [Lcom/google/c/a/a/b/e/g;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/c/a/a/b/e/g;

    return-object v0
.end method
