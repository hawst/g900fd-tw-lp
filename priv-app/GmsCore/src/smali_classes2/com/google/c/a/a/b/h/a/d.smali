.class public final Lcom/google/c/a/a/b/h/a/d;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public final a:Ljava/util/List;

.field private final b:Ljava/lang/String;

.field private c:I

.field private d:Lcom/google/c/a/a/b/b/a/c/i;

.field private e:Lcom/google/c/a/a/b/b/a/c/f;

.field private f:Lcom/google/c/a/a/b/h/a/e;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 321
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 309
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/c/a/a/b/h/a/d;->c:I

    .line 310
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/c/a/a/b/h/a/d;->a:Ljava/util/List;

    .line 313
    sget-object v0, Lcom/google/c/a/a/b/h/a/e;->c:Lcom/google/c/a/a/b/h/a/e;

    iput-object v0, p0, Lcom/google/c/a/a/b/h/a/d;->f:Lcom/google/c/a/a/b/h/a/e;

    .line 322
    iput-object p1, p0, Lcom/google/c/a/a/b/h/a/d;->b:Ljava/lang/String;

    .line 323
    return-void
.end method

.method static synthetic a(Lcom/google/c/a/a/b/h/a/d;Lcom/google/c/a/a/b/h/a/d;)V
    .locals 4

    .prologue
    .line 306
    const/4 v0, 0x0

    iget-object v1, p1, Lcom/google/c/a/a/b/h/a/d;->a:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v1, v0

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/c/a/a/b/b/a/c/l;

    invoke-interface {v0}, Lcom/google/c/a/a/b/b/a/c/l;->c()I

    move-result v2

    if-ge v1, v2, :cond_4

    invoke-interface {v0}, Lcom/google/c/a/a/b/b/a/c/l;->c()I

    move-result v2

    sub-int/2addr v2, v1

    invoke-virtual {p0, v2}, Lcom/google/c/a/a/b/h/a/d;->a(I)V

    add-int/2addr v2, v1

    :goto_1
    instance-of v1, v0, Lcom/google/c/a/a/b/b/a/c/i;

    if-eqz v1, :cond_0

    move-object v1, v0

    check-cast v1, Lcom/google/c/a/a/b/b/a/c/i;

    invoke-virtual {v1}, Lcom/google/c/a/a/b/b/a/c/i;->d()Ljava/util/List;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/google/c/a/a/b/h/a/d;->a(Ljava/util/Collection;)V

    invoke-interface {v0}, Lcom/google/c/a/a/b/b/a/c/l;->a()I

    move-result v0

    add-int/2addr v0, v2

    move v1, v0

    goto :goto_0

    :cond_0
    instance-of v1, v0, Lcom/google/c/a/a/b/b/a/c/f;

    if-eqz v1, :cond_1

    invoke-interface {v0}, Lcom/google/c/a/a/b/b/a/c/l;->a()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/c/a/a/b/h/a/d;->b(I)V

    :cond_1
    move v1, v2

    goto :goto_0

    :cond_2
    iget v0, p1, Lcom/google/c/a/a/b/h/a/d;->c:I

    if-ge v1, v0, :cond_3

    iget v0, p1, Lcom/google/c/a/a/b/h/a/d;->c:I

    sub-int/2addr v0, v1

    invoke-virtual {p0, v0}, Lcom/google/c/a/a/b/h/a/d;->a(I)V

    :cond_3
    return-void

    :cond_4
    move v2, v1

    goto :goto_1
.end method

.method static synthetic a(Lcom/google/c/a/a/b/h/a/d;Lcom/google/c/a/a/b/h/a/h;)V
    .locals 3

    .prologue
    .line 306
    new-instance v1, Ljava/util/ArrayList;

    invoke-virtual {p1}, Lcom/google/c/a/a/b/h/a/h;->a()I

    move-result v0

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(I)V

    invoke-virtual {p1}, Lcom/google/c/a/a/b/h/a/h;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/c/a/a/b/b/a/r;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    invoke-direct {p0, v1}, Lcom/google/c/a/a/b/h/a/d;->a(Ljava/util/Collection;)V

    return-void
.end method

.method private a(Ljava/util/Collection;)V
    .locals 4

    .prologue
    .line 338
    invoke-interface {p1}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 339
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "valuesToInsert must not be empty"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 343
    :cond_0
    iget-object v0, p0, Lcom/google/c/a/a/b/h/a/d;->f:Lcom/google/c/a/a/b/h/a/e;

    sget-object v1, Lcom/google/c/a/a/b/h/a/e;->a:Lcom/google/c/a/a/b/h/a/e;

    if-ne v0, v1, :cond_3

    .line 344
    new-instance v1, Ljava/util/ArrayList;

    invoke-interface {p1}, Ljava/util/Collection;->size()I

    move-result v0

    iget-object v2, p0, Lcom/google/c/a/a/b/h/a/d;->d:Lcom/google/c/a/a/b/b/a/c/i;

    invoke-virtual {v2}, Lcom/google/c/a/a/b/b/a/c/i;->a()I

    move-result v2

    add-int/2addr v0, v2

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 347
    iget-object v0, p0, Lcom/google/c/a/a/b/h/a/d;->d:Lcom/google/c/a/a/b/b/a/c/i;

    invoke-virtual {v0}, Lcom/google/c/a/a/b/b/a/c/i;->d()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/c/a/a/b/b/a/r;

    .line 348
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 351
    :cond_1
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/c/a/a/b/b/a/r;

    .line 352
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 354
    :cond_2
    new-instance v0, Lcom/google/c/a/a/b/b/a/c/i;

    iget-object v2, p0, Lcom/google/c/a/a/b/h/a/d;->b:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/c/a/a/b/h/a/d;->d:Lcom/google/c/a/a/b/b/a/c/i;

    invoke-virtual {v3}, Lcom/google/c/a/a/b/b/a/c/i;->c()I

    move-result v3

    invoke-direct {v0, v2, v3, v1}, Lcom/google/c/a/a/b/b/a/c/i;-><init>(Ljava/lang/String;ILjava/util/List;)V

    iput-object v0, p0, Lcom/google/c/a/a/b/h/a/d;->d:Lcom/google/c/a/a/b/b/a/c/i;

    .line 355
    iget-object v0, p0, Lcom/google/c/a/a/b/h/a/d;->a:Ljava/util/List;

    iget-object v1, p0, Lcom/google/c/a/a/b/h/a/d;->a:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    iget-object v2, p0, Lcom/google/c/a/a/b/h/a/d;->d:Lcom/google/c/a/a/b/b/a/c/i;

    invoke-interface {v0, v1, v2}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 366
    :goto_2
    iget v0, p0, Lcom/google/c/a/a/b/h/a/d;->c:I

    invoke-interface {p1}, Ljava/util/Collection;->size()I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lcom/google/c/a/a/b/h/a/d;->c:I

    .line 367
    sget-object v0, Lcom/google/c/a/a/b/h/a/e;->a:Lcom/google/c/a/a/b/h/a/e;

    iput-object v0, p0, Lcom/google/c/a/a/b/h/a/d;->f:Lcom/google/c/a/a/b/h/a/e;

    .line 368
    return-void

    .line 357
    :cond_3
    new-instance v1, Ljava/util/ArrayList;

    invoke-interface {p1}, Ljava/util/Collection;->size()I

    move-result v0

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 359
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_3
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/c/a/a/b/b/a/r;

    .line 360
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 362
    :cond_4
    new-instance v0, Lcom/google/c/a/a/b/b/a/c/i;

    iget-object v2, p0, Lcom/google/c/a/a/b/h/a/d;->b:Ljava/lang/String;

    iget v3, p0, Lcom/google/c/a/a/b/h/a/d;->c:I

    invoke-direct {v0, v2, v3, v1}, Lcom/google/c/a/a/b/b/a/c/i;-><init>(Ljava/lang/String;ILjava/util/List;)V

    iput-object v0, p0, Lcom/google/c/a/a/b/h/a/d;->d:Lcom/google/c/a/a/b/b/a/c/i;

    .line 363
    iget-object v0, p0, Lcom/google/c/a/a/b/h/a/d;->a:Ljava/util/List;

    iget-object v1, p0, Lcom/google/c/a/a/b/h/a/d;->d:Lcom/google/c/a/a/b/b/a/c/i;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2
.end method


# virtual methods
.method final a(I)V
    .locals 1

    .prologue
    .line 375
    iget v0, p0, Lcom/google/c/a/a/b/h/a/d;->c:I

    add-int/2addr v0, p1

    iput v0, p0, Lcom/google/c/a/a/b/h/a/d;->c:I

    .line 376
    sget-object v0, Lcom/google/c/a/a/b/h/a/e;->c:Lcom/google/c/a/a/b/h/a/e;

    iput-object v0, p0, Lcom/google/c/a/a/b/h/a/d;->f:Lcom/google/c/a/a/b/h/a/e;

    .line 377
    return-void
.end method

.method final b(I)V
    .locals 4

    .prologue
    .line 384
    iget-object v0, p0, Lcom/google/c/a/a/b/h/a/d;->f:Lcom/google/c/a/a/b/h/a/e;

    sget-object v1, Lcom/google/c/a/a/b/h/a/e;->b:Lcom/google/c/a/a/b/h/a/e;

    if-ne v0, v1, :cond_0

    .line 385
    new-instance v0, Lcom/google/c/a/a/b/b/a/c/f;

    iget-object v1, p0, Lcom/google/c/a/a/b/h/a/d;->b:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/c/a/a/b/h/a/d;->e:Lcom/google/c/a/a/b/b/a/c/f;

    invoke-virtual {v2}, Lcom/google/c/a/a/b/b/a/c/f;->c()I

    move-result v2

    iget-object v3, p0, Lcom/google/c/a/a/b/h/a/d;->e:Lcom/google/c/a/a/b/b/a/c/f;

    invoke-virtual {v3}, Lcom/google/c/a/a/b/b/a/c/f;->a()I

    move-result v3

    add-int/2addr v3, p1

    invoke-direct {v0, v1, v2, v3}, Lcom/google/c/a/a/b/b/a/c/f;-><init>(Ljava/lang/String;II)V

    iput-object v0, p0, Lcom/google/c/a/a/b/h/a/d;->e:Lcom/google/c/a/a/b/b/a/c/f;

    .line 386
    iget-object v0, p0, Lcom/google/c/a/a/b/h/a/d;->a:Ljava/util/List;

    iget-object v1, p0, Lcom/google/c/a/a/b/h/a/d;->a:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    iget-object v2, p0, Lcom/google/c/a/a/b/h/a/d;->e:Lcom/google/c/a/a/b/b/a/c/f;

    invoke-interface {v0, v1, v2}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 391
    :goto_0
    sget-object v0, Lcom/google/c/a/a/b/h/a/e;->b:Lcom/google/c/a/a/b/h/a/e;

    iput-object v0, p0, Lcom/google/c/a/a/b/h/a/d;->f:Lcom/google/c/a/a/b/h/a/e;

    .line 392
    return-void

    .line 388
    :cond_0
    new-instance v0, Lcom/google/c/a/a/b/b/a/c/f;

    iget-object v1, p0, Lcom/google/c/a/a/b/h/a/d;->b:Ljava/lang/String;

    iget v2, p0, Lcom/google/c/a/a/b/h/a/d;->c:I

    invoke-direct {v0, v1, v2, p1}, Lcom/google/c/a/a/b/b/a/c/f;-><init>(Ljava/lang/String;II)V

    iput-object v0, p0, Lcom/google/c/a/a/b/h/a/d;->e:Lcom/google/c/a/a/b/b/a/c/f;

    .line 389
    iget-object v0, p0, Lcom/google/c/a/a/b/h/a/d;->a:Ljava/util/List;

    iget-object v1, p0, Lcom/google/c/a/a/b/h/a/d;->e:Lcom/google/c/a/a/b/b/a/c/f;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method
