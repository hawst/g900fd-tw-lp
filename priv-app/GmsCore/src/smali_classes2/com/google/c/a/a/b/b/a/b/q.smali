.class abstract Lcom/google/c/a/a/b/b/a/b/q;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/c/a/a/b/b/a/i;


# instance fields
.field private final a:Ljava/util/Set;

.field final synthetic b:Lcom/google/c/a/a/b/b/a/b/l;


# direct methods
.method constructor <init>(Lcom/google/c/a/a/b/b/a/b/l;)V
    .locals 1

    .prologue
    .line 83
    iput-object p1, p0, Lcom/google/c/a/a/b/b/a/b/q;->b:Lcom/google/c/a/a/b/b/a/b/l;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 84
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/c/a/a/b/b/a/b/q;->a:Ljava/util/Set;

    return-void
.end method


# virtual methods
.method protected abstract a(Lcom/google/c/a/a/b/b/a/g;)Ljava/lang/Object;
.end method

.method public final a(Lcom/google/c/a/a/b/b/a/r;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 87
    instance-of v0, p1, Lcom/google/c/a/a/b/b/a/p;

    if-eqz v0, :cond_1

    .line 88
    check-cast p1, Lcom/google/c/a/a/b/b/a/p;

    .line 89
    iget-object v0, p0, Lcom/google/c/a/a/b/b/a/b/q;->a:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 90
    iget-object v0, p0, Lcom/google/c/a/a/b/b/a/b/q;->b:Lcom/google/c/a/a/b/b/a/b/l;

    invoke-virtual {v0}, Lcom/google/c/a/a/b/b/a/b/l;->j()Lcom/google/c/a/a/b/b/a/b;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/c/a/a/b/b/a/p;->a()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/c/a/a/b/b/a/b;->a(Ljava/lang/String;)Lcom/google/c/a/a/b/b/a/g;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/c/a/a/b/b/a/b/q;->b(Lcom/google/c/a/a/b/b/a/g;)Ljava/lang/Object;

    move-result-object v0

    .line 96
    :goto_0
    return-object v0

    .line 92
    :cond_0
    iget-object v0, p0, Lcom/google/c/a/a/b/b/a/b/q;->a:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 93
    iget-object v0, p0, Lcom/google/c/a/a/b/b/a/b/q;->b:Lcom/google/c/a/a/b/b/a/b/l;

    invoke-virtual {v0}, Lcom/google/c/a/a/b/b/a/b/l;->j()Lcom/google/c/a/a/b/b/a/b;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/c/a/a/b/b/a/p;->a()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/c/a/a/b/b/a/b;->a(Ljava/lang/String;)Lcom/google/c/a/a/b/b/a/g;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/c/a/a/b/b/a/b/q;->a(Lcom/google/c/a/a/b/b/a/g;)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0

    .line 96
    :cond_1
    invoke-virtual {p0, p1}, Lcom/google/c/a/a/b/b/a/b/q;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0
.end method

.method protected abstract a(Ljava/lang/Object;)Ljava/lang/Object;
.end method

.method protected abstract b(Lcom/google/c/a/a/b/b/a/g;)Ljava/lang/Object;
.end method
