.class public final enum Lcom/google/c/a/a/b/f/a/aw;
.super Ljava/lang/Enum;
.source "SourceFile"

# interfaces
.implements Lcom/google/c/a/a/b/f/a/k;


# static fields
.field public static final enum a:Lcom/google/c/a/a/b/f/a/aw;

.field public static final enum b:Lcom/google/c/a/a/b/f/a/aw;

.field public static final enum c:Lcom/google/c/a/a/b/f/a/aw;

.field public static final enum d:Lcom/google/c/a/a/b/f/a/aw;

.field public static final enum e:Lcom/google/c/a/a/b/f/a/aw;

.field public static final enum f:Lcom/google/c/a/a/b/f/a/aw;

.field public static final enum g:Lcom/google/c/a/a/b/f/a/aw;

.field private static final synthetic i:[Lcom/google/c/a/a/b/f/a/aw;


# instance fields
.field private final h:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 13
    new-instance v0, Lcom/google/c/a/a/b/f/a/aw;

    const-string v1, "SESSION_ID"

    invoke-direct {v0, v1, v4, v4}, Lcom/google/c/a/a/b/f/a/aw;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/c/a/a/b/f/a/aw;->a:Lcom/google/c/a/a/b/f/a/aw;

    .line 14
    new-instance v0, Lcom/google/c/a/a/b/f/a/aw;

    const-string v1, "USER_ID"

    invoke-direct {v0, v1, v5, v5}, Lcom/google/c/a/a/b/f/a/aw;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/c/a/a/b/f/a/aw;->b:Lcom/google/c/a/a/b/f/a/aw;

    .line 15
    new-instance v0, Lcom/google/c/a/a/b/f/a/aw;

    const-string v1, "IS_ANONYMOUS"

    invoke-direct {v0, v1, v6, v6}, Lcom/google/c/a/a/b/f/a/aw;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/c/a/a/b/f/a/aw;->c:Lcom/google/c/a/a/b/f/a/aw;

    .line 16
    new-instance v0, Lcom/google/c/a/a/b/f/a/aw;

    const-string v1, "IS_ME"

    invoke-direct {v0, v1, v7, v7}, Lcom/google/c/a/a/b/f/a/aw;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/c/a/a/b/f/a/aw;->d:Lcom/google/c/a/a/b/f/a/aw;

    .line 17
    new-instance v0, Lcom/google/c/a/a/b/f/a/aw;

    const-string v1, "DISPLAY_NAME"

    invoke-direct {v0, v1, v8, v8}, Lcom/google/c/a/a/b/f/a/aw;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/c/a/a/b/f/a/aw;->e:Lcom/google/c/a/a/b/f/a/aw;

    .line 18
    new-instance v0, Lcom/google/c/a/a/b/f/a/aw;

    const-string v1, "COLOR"

    const/4 v2, 0x5

    const/4 v3, 0x5

    invoke-direct {v0, v1, v2, v3}, Lcom/google/c/a/a/b/f/a/aw;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/c/a/a/b/f/a/aw;->f:Lcom/google/c/a/a/b/f/a/aw;

    .line 19
    new-instance v0, Lcom/google/c/a/a/b/f/a/aw;

    const-string v1, "PHOTO_URL"

    const/4 v2, 0x6

    const/4 v3, 0x6

    invoke-direct {v0, v1, v2, v3}, Lcom/google/c/a/a/b/f/a/aw;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/c/a/a/b/f/a/aw;->g:Lcom/google/c/a/a/b/f/a/aw;

    .line 12
    const/4 v0, 0x7

    new-array v0, v0, [Lcom/google/c/a/a/b/f/a/aw;

    sget-object v1, Lcom/google/c/a/a/b/f/a/aw;->a:Lcom/google/c/a/a/b/f/a/aw;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/c/a/a/b/f/a/aw;->b:Lcom/google/c/a/a/b/f/a/aw;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/c/a/a/b/f/a/aw;->c:Lcom/google/c/a/a/b/f/a/aw;

    aput-object v1, v0, v6

    sget-object v1, Lcom/google/c/a/a/b/f/a/aw;->d:Lcom/google/c/a/a/b/f/a/aw;

    aput-object v1, v0, v7

    sget-object v1, Lcom/google/c/a/a/b/f/a/aw;->e:Lcom/google/c/a/a/b/f/a/aw;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lcom/google/c/a/a/b/f/a/aw;->f:Lcom/google/c/a/a/b/f/a/aw;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/google/c/a/a/b/f/a/aw;->g:Lcom/google/c/a/a/b/f/a/aw;

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/c/a/a/b/f/a/aw;->i:[Lcom/google/c/a/a/b/f/a/aw;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 24
    iput p3, p0, Lcom/google/c/a/a/b/f/a/aw;->h:I

    .line 25
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/c/a/a/b/f/a/aw;
    .locals 1

    .prologue
    .line 12
    const-class v0, Lcom/google/c/a/a/b/f/a/aw;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/c/a/a/b/f/a/aw;

    return-object v0
.end method

.method public static values()[Lcom/google/c/a/a/b/f/a/aw;
    .locals 1

    .prologue
    .line 12
    sget-object v0, Lcom/google/c/a/a/b/f/a/aw;->i:[Lcom/google/c/a/a/b/f/a/aw;

    invoke-virtual {v0}, [Lcom/google/c/a/a/b/f/a/aw;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/c/a/a/b/f/a/aw;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 29
    iget v0, p0, Lcom/google/c/a/a/b/f/a/aw;->h:I

    return v0
.end method
