.class public Lcom/google/c/a/a/b/c/d;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Comparable;


# instance fields
.field private final a:Lcom/google/c/b/a/c;

.field private final b:J

.field private final c:Ljava/lang/String;

.field private final d:I

.field private final e:Ljava/lang/Long;

.field private final f:I

.field private final g:Ljava/lang/Long;


# direct methods
.method public constructor <init>(Lcom/google/c/b/a/c;JLjava/lang/String;ILjava/lang/Long;ILjava/lang/Long;)V
    .locals 4

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    if-eqz p6, :cond_0

    invoke-virtual {p6}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-ltz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    const-string v1, "sessionNumber must be null or nonnegative"

    invoke-static {v0, v1}, Lcom/google/c/a/a/b/h/c;->a(ZLjava/lang/String;)V

    .line 36
    const-string v0, "command"

    invoke-static {p1, v0}, Lcom/google/c/a/a/b/h/c;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/c/b/a/c;

    iput-object v0, p0, Lcom/google/c/a/a/b/c/d;->a:Lcom/google/c/b/a/c;

    .line 37
    iput-wide p2, p0, Lcom/google/c/a/a/b/c/d;->b:J

    .line 38
    const-string v0, "userId"

    invoke-static {p4, v0}, Lcom/google/c/a/a/b/h/c;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/google/c/a/a/b/c/d;->c:Ljava/lang/String;

    .line 39
    if-ltz p5, :cond_2

    const/4 v0, 0x1

    :goto_1
    const-string v1, "revision must be nonnegative"

    invoke-static {v0, v1}, Lcom/google/c/a/a/b/h/c;->a(ZLjava/lang/String;)V

    .line 40
    iput p5, p0, Lcom/google/c/a/a/b/c/d;->d:I

    .line 41
    iput-object p6, p0, Lcom/google/c/a/a/b/c/d;->e:Ljava/lang/Long;

    .line 42
    iput p7, p0, Lcom/google/c/a/a/b/c/d;->f:I

    .line 43
    iput-object p8, p0, Lcom/google/c/a/a/b/c/d;->g:Ljava/lang/Long;

    .line 44
    return-void

    .line 33
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 39
    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public constructor <init>(Lcom/google/c/b/a/c;Ljava/lang/String;ILjava/lang/Long;Ljava/lang/Long;)V
    .locals 9

    .prologue
    .line 57
    const-wide/16 v2, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v4, p2

    move v5, p3

    move-object v6, p4

    move v7, p3

    move-object v8, p5

    invoke-direct/range {v0 .. v8}, Lcom/google/c/a/a/b/c/d;-><init>(Lcom/google/c/b/a/c;JLjava/lang/String;ILjava/lang/Long;ILjava/lang/Long;)V

    .line 58
    return-void
.end method


# virtual methods
.method public final a()Lcom/google/c/b/a/c;
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Lcom/google/c/a/a/b/c/d;->a:Lcom/google/c/b/a/c;

    return-object v0
.end method

.method public final b()J
    .locals 2

    .prologue
    .line 65
    iget-wide v0, p0, Lcom/google/c/a/a/b/c/d;->b:J

    return-wide v0
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 69
    iget-object v0, p0, Lcom/google/c/a/a/b/c/d;->c:Ljava/lang/String;

    return-object v0
.end method

.method public bridge synthetic compareTo(Ljava/lang/Object;)I
    .locals 2

    .prologue
    .line 12
    check-cast p1, Lcom/google/c/a/a/b/c/d;

    iget v0, p0, Lcom/google/c/a/a/b/c/d;->d:I

    iget v1, p1, Lcom/google/c/a/a/b/c/d;->d:I

    sub-int/2addr v0, v1

    return v0
.end method

.method public final d()I
    .locals 1

    .prologue
    .line 76
    iget v0, p0, Lcom/google/c/a/a/b/c/d;->d:I

    return v0
.end method

.method public final e()I
    .locals 1

    .prologue
    .line 83
    iget v0, p0, Lcom/google/c/a/a/b/c/d;->f:I

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 111
    instance-of v1, p1, Lcom/google/c/a/a/b/c/d;

    if-nez v1, :cond_0

    .line 115
    :goto_0
    return v0

    .line 114
    :cond_0
    check-cast p1, Lcom/google/c/a/a/b/c/d;

    .line 115
    const/16 v1, 0xa

    new-array v1, v1, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/google/c/a/a/b/c/d;->a:Lcom/google/c/b/a/c;

    aput-object v2, v1, v0

    const/4 v0, 0x1

    iget-object v2, p1, Lcom/google/c/a/a/b/c/d;->a:Lcom/google/c/b/a/c;

    aput-object v2, v1, v0

    const/4 v0, 0x2

    iget-wide v2, p0, Lcom/google/c/a/a/b/c/d;->b:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v1, v0

    const/4 v0, 0x3

    iget-wide v2, p1, Lcom/google/c/a/a/b/c/d;->b:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v1, v0

    const/4 v0, 0x4

    iget-object v2, p0, Lcom/google/c/a/a/b/c/d;->c:Ljava/lang/String;

    aput-object v2, v1, v0

    const/4 v0, 0x5

    iget-object v2, p1, Lcom/google/c/a/a/b/c/d;->c:Ljava/lang/String;

    aput-object v2, v1, v0

    const/4 v0, 0x6

    iget v2, p0, Lcom/google/c/a/a/b/c/d;->d:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v0

    const/4 v0, 0x7

    iget v2, p1, Lcom/google/c/a/a/b/c/d;->d:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v0

    const/16 v0, 0x8

    iget-object v2, p0, Lcom/google/c/a/a/b/c/d;->e:Ljava/lang/Long;

    aput-object v2, v1, v0

    const/16 v0, 0x9

    iget-object v2, p1, Lcom/google/c/a/a/b/c/d;->e:Ljava/lang/Long;

    aput-object v2, v1, v0

    invoke-static {v1}, Lcom/google/c/a/a/b/h/b;->a([Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public final f()Ljava/lang/Long;
    .locals 1

    .prologue
    .line 87
    iget-object v0, p0, Lcom/google/c/a/a/b/c/d;->e:Ljava/lang/Long;

    return-object v0
.end method

.method public final g()Ljava/lang/Long;
    .locals 1

    .prologue
    .line 94
    iget-object v0, p0, Lcom/google/c/a/a/b/c/d;->g:Ljava/lang/Long;

    return-object v0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    .line 99
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/c/a/a/b/c/d;->a:Lcom/google/c/b/a/c;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-wide v2, p0, Lcom/google/c/a/a/b/c/d;->b:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/c/a/a/b/c/d;->c:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget v2, p0, Lcom/google/c/a/a/b/c/d;->d:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/c/a/a/b/c/d;->e:Ljava/lang/Long;

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 105
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "ClientChange [command="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/c/a/a/b/c/d;->a:Lcom/google/c/b/a/c;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", timestamp="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lcom/google/c/a/a/b/c/d;->b:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", userId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/c/a/a/b/c/d;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", revision="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/c/a/a/b/c/d;->d:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", sessionNumber="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/c/a/a/b/c/d;->e:Ljava/lang/Long;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
