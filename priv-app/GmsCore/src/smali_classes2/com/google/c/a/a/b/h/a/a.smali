.class public abstract Lcom/google/c/a/a/b/h/a/a;
.super Lcom/google/c/a/a/b/h/a/h;
.source "SourceFile"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 12
    invoke-direct {p0}, Lcom/google/c/a/a/b/h/a/h;-><init>()V

    return-void
.end method


# virtual methods
.method protected abstract a(I)C
.end method

.method public final bridge synthetic a(ILcom/google/c/a/a/b/h/a/h;I)Z
    .locals 2

    .prologue
    .line 12
    check-cast p2, Lcom/google/c/a/a/b/h/a/a;

    invoke-virtual {p0, p1}, Lcom/google/c/a/a/b/h/a/a;->a(I)C

    move-result v0

    invoke-virtual {p2, p3}, Lcom/google/c/a/a/b/h/a/a;->a(I)C

    move-result v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 47
    if-ne p0, p1, :cond_1

    move v1, v2

    .line 65
    :cond_0
    :goto_0
    return v1

    .line 50
    :cond_1
    if-eqz p1, :cond_0

    .line 53
    instance-of v0, p1, Lcom/google/c/a/a/b/h/a/a;

    if-eqz v0, :cond_0

    .line 56
    check-cast p1, Lcom/google/c/a/a/b/h/a/a;

    .line 57
    invoke-virtual {p1}, Lcom/google/c/a/a/b/h/a/a;->a()I

    move-result v0

    invoke-virtual {p0}, Lcom/google/c/a/a/b/h/a/a;->a()I

    move-result v3

    if-ne v0, v3, :cond_0

    move v0, v1

    .line 60
    :goto_1
    invoke-virtual {p1}, Lcom/google/c/a/a/b/h/a/a;->a()I

    move-result v3

    if-ge v0, v3, :cond_2

    .line 61
    invoke-virtual {p1, v0}, Lcom/google/c/a/a/b/h/a/a;->a(I)C

    move-result v3

    invoke-virtual {p0, v0}, Lcom/google/c/a/a/b/h/a/a;->a(I)C

    move-result v4

    if-ne v3, v4, :cond_0

    .line 60
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_2
    move v1, v2

    .line 65
    goto :goto_0
.end method

.method public iterator()Ljava/util/Iterator;
    .locals 1

    .prologue
    .line 20
    new-instance v0, Lcom/google/c/a/a/b/h/a/b;

    invoke-direct {v0, p0}, Lcom/google/c/a/a/b/h/a/b;-><init>(Lcom/google/c/a/a/b/h/a/a;)V

    return-object v0
.end method
