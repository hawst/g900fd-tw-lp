.class public final Lcom/google/c/a/a/b/b/a/k;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static final a:Lcom/google/c/a/a/b/b/a/k;


# instance fields
.field private final b:Z

.field private final c:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 14
    new-instance v0, Lcom/google/c/a/a/b/b/a/k;

    invoke-direct {v0, v1, v1}, Lcom/google/c/a/a/b/b/a/k;-><init>(ZZ)V

    sput-object v0, Lcom/google/c/a/a/b/b/a/k;->a:Lcom/google/c/a/a/b/b/a/k;

    return-void
.end method

.method public constructor <init>(ZZ)V
    .locals 2

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    .line 23
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "A mutation cannot be caused by bothan undo and redo."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 26
    :cond_0
    iput-boolean p1, p0, Lcom/google/c/a/a/b/b/a/k;->b:Z

    .line 27
    iput-boolean p2, p0, Lcom/google/c/a/a/b/b/a/k;->c:Z

    .line 28
    return-void
.end method


# virtual methods
.method public final a()Z
    .locals 1

    .prologue
    .line 34
    iget-boolean v0, p0, Lcom/google/c/a/a/b/b/a/k;->b:Z

    return v0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 41
    iget-boolean v0, p0, Lcom/google/c/a/a/b/b/a/k;->c:Z

    return v0
.end method
