.class public final Lcom/google/c/a/a/b/f/a/y;
.super Lcom/google/c/a/a/b/f/b;
.source "SourceFile"


# direct methods
.method public constructor <init>(Z)V
    .locals 1

    .prologue
    .line 41
    const-class v0, Lcom/google/c/a/a/b/c/f;

    invoke-direct {p0, v0, p1}, Lcom/google/c/a/a/b/f/b;-><init>(Ljava/lang/Class;Z)V

    .line 42
    return-void
.end method


# virtual methods
.method protected final synthetic a(Ljava/lang/Object;Ljava/lang/Object;Lcom/google/c/a/a/b/d/b;)V
    .locals 2

    .prologue
    .line 17
    check-cast p1, Lcom/google/c/a/a/b/c/f;

    sget-object v0, Lcom/google/c/a/a/b/f/a/z;->a:Lcom/google/c/a/a/b/f/a/z;

    invoke-virtual {p1}, Lcom/google/c/a/a/b/c/f;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, p2, p3, v0, v1}, Lcom/google/c/a/a/b/f/a/y;->a(Ljava/lang/Object;Lcom/google/c/a/a/b/d/b;Lcom/google/c/a/a/b/f/a/k;Ljava/lang/Object;)V

    sget-object v0, Lcom/google/c/a/a/b/f/a/z;->b:Lcom/google/c/a/a/b/f/a/z;

    invoke-virtual {p1}, Lcom/google/c/a/a/b/c/f;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, p2, p3, v0, v1}, Lcom/google/c/a/a/b/f/a/y;->a(Ljava/lang/Object;Lcom/google/c/a/a/b/d/b;Lcom/google/c/a/a/b/f/a/k;Ljava/lang/Object;)V

    sget-object v0, Lcom/google/c/a/a/b/f/a/z;->d:Lcom/google/c/a/a/b/f/a/z;

    invoke-virtual {p1}, Lcom/google/c/a/a/b/c/f;->d()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, p2, p3, v0, v1}, Lcom/google/c/a/a/b/f/a/y;->a(Ljava/lang/Object;Lcom/google/c/a/a/b/d/b;Lcom/google/c/a/a/b/f/a/k;Ljava/lang/Object;)V

    sget-object v0, Lcom/google/c/a/a/b/f/a/z;->c:Lcom/google/c/a/a/b/f/a/z;

    invoke-virtual {p1}, Lcom/google/c/a/a/b/c/f;->c()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {p0, p2, p3, v0, v1}, Lcom/google/c/a/a/b/f/a/y;->a(Ljava/lang/Object;Lcom/google/c/a/a/b/d/b;Lcom/google/c/a/a/b/f/a/k;Ljava/lang/Object;)V

    sget-object v0, Lcom/google/c/a/a/b/f/a/z;->e:Lcom/google/c/a/a/b/f/a/z;

    invoke-virtual {p1}, Lcom/google/c/a/a/b/c/f;->e()Ljava/util/List;

    move-result-object v1

    invoke-virtual {p0, p2, p3, v0, v1}, Lcom/google/c/a/a/b/f/a/y;->a(Ljava/lang/Object;Lcom/google/c/a/a/b/d/b;Lcom/google/c/a/a/b/f/a/k;Ljava/lang/Object;)V

    sget-object v0, Lcom/google/c/a/a/b/f/a/z;->g:Lcom/google/c/a/a/b/f/a/z;

    invoke-virtual {p1}, Lcom/google/c/a/a/b/c/f;->g()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, p2, p3, v0, v1}, Lcom/google/c/a/a/b/f/a/y;->a(Ljava/lang/Object;Lcom/google/c/a/a/b/d/b;Lcom/google/c/a/a/b/f/a/k;Ljava/lang/Object;)V

    new-instance v0, Ljava/util/ArrayList;

    invoke-virtual {p1}, Lcom/google/c/a/a/b/c/f;->f()Ljava/util/Collection;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    sget-object v1, Lcom/google/c/a/a/b/f/a/z;->f:Lcom/google/c/a/a/b/f/a/z;

    invoke-virtual {p0, p2, p3, v1, v0}, Lcom/google/c/a/a/b/f/a/y;->a(Ljava/lang/Object;Lcom/google/c/a/a/b/d/b;Lcom/google/c/a/a/b/f/a/k;Ljava/lang/Object;)V

    return-void
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 88
    instance-of v0, p1, Lcom/google/c/a/a/b/c/f;

    return v0
.end method

.method public final synthetic b(Ljava/lang/Object;Lcom/google/c/a/a/b/d/b;)Ljava/lang/Object;
    .locals 8

    .prologue
    .line 17
    sget-object v0, Lcom/google/c/a/a/b/f/a/z;->a:Lcom/google/c/a/a/b/f/a/z;

    const-class v1, Ljava/lang/String;

    invoke-virtual {p0, p1, p2, v0, v1}, Lcom/google/c/a/a/b/f/a/y;->a(Ljava/lang/Object;Lcom/google/c/a/a/b/d/b;Lcom/google/c/a/a/b/f/a/k;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    sget-object v0, Lcom/google/c/a/a/b/f/a/z;->b:Lcom/google/c/a/a/b/f/a/z;

    const-class v2, Ljava/lang/String;

    invoke-virtual {p0, p1, p2, v0, v2}, Lcom/google/c/a/a/b/f/a/y;->a(Ljava/lang/Object;Lcom/google/c/a/a/b/d/b;Lcom/google/c/a/a/b/f/a/k;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    sget-object v0, Lcom/google/c/a/a/b/f/a/z;->c:Lcom/google/c/a/a/b/f/a/z;

    const-class v3, Ljava/lang/Boolean;

    invoke-virtual {p0, p1, p2, v0, v3}, Lcom/google/c/a/a/b/f/a/y;->a(Ljava/lang/Object;Lcom/google/c/a/a/b/d/b;Lcom/google/c/a/a/b/f/a/k;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    sget-object v0, Lcom/google/c/a/a/b/f/a/z;->d:Lcom/google/c/a/a/b/f/a/z;

    const-class v4, Ljava/lang/Integer;

    invoke-virtual {p0, p1, p2, v0, v4}, Lcom/google/c/a/a/b/f/a/y;->a(Ljava/lang/Object;Lcom/google/c/a/a/b/d/b;Lcom/google/c/a/a/b/f/a/k;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v4

    sget-object v0, Lcom/google/c/a/a/b/f/a/z;->f:Lcom/google/c/a/a/b/f/a/z;

    const-class v5, Ljava/util/List;

    invoke-virtual {p0, p1, p2, v0, v5}, Lcom/google/c/a/a/b/f/a/y;->a(Ljava/lang/Object;Lcom/google/c/a/a/b/d/b;Lcom/google/c/a/a/b/f/a/k;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    if-nez v0, :cond_0

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v6

    :goto_0
    sget-object v0, Lcom/google/c/a/a/b/f/a/z;->e:Lcom/google/c/a/a/b/f/a/z;

    const-class v5, Ljava/util/List;

    invoke-virtual {p0, p1, p2, v0, v5}, Lcom/google/c/a/a/b/f/a/y;->a(Ljava/lang/Object;Lcom/google/c/a/a/b/d/b;Lcom/google/c/a/a/b/f/a/k;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/util/List;

    sget-object v0, Lcom/google/c/a/a/b/f/a/z;->g:Lcom/google/c/a/a/b/f/a/z;

    const-class v7, Ljava/lang/String;

    invoke-virtual {p0, p1, p2, v0, v7}, Lcom/google/c/a/a/b/f/a/y;->a(Ljava/lang/Object;Lcom/google/c/a/a/b/d/b;Lcom/google/c/a/a/b/f/a/k;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    new-instance v0, Lcom/google/c/a/a/b/c/f;

    invoke-direct/range {v0 .. v7}, Lcom/google/c/a/a/b/c/f;-><init>(Ljava/lang/String;Ljava/lang/String;ZILjava/util/List;Ljava/util/Collection;Ljava/lang/String;)V

    return-object v0

    :cond_0
    move-object v6, v0

    goto :goto_0
.end method
