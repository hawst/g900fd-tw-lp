.class public final Lcom/google/c/a/a/b/f/a/aj;
.super Lcom/google/c/a/a/b/f/b;
.source "SourceFile"


# direct methods
.method public constructor <init>(Z)V
    .locals 1

    .prologue
    .line 35
    const-class v0, Lcom/google/c/a/a/b/b/a/c/m;

    invoke-direct {p0, v0, p1}, Lcom/google/c/a/a/b/f/b;-><init>(Ljava/lang/Class;Z)V

    .line 36
    return-void
.end method


# virtual methods
.method protected final synthetic a(Ljava/lang/Object;Ljava/lang/Object;Lcom/google/c/a/a/b/d/b;)V
    .locals 2

    .prologue
    .line 11
    check-cast p1, Lcom/google/c/a/a/b/b/a/c/m;

    sget-object v0, Lcom/google/c/a/a/b/f/a/ak;->a:Lcom/google/c/a/a/b/f/a/ak;

    invoke-virtual {p1}, Lcom/google/c/a/a/b/b/a/c/m;->c()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, p2, p3, v0, v1}, Lcom/google/c/a/a/b/f/a/aj;->a(Ljava/lang/Object;Lcom/google/c/a/a/b/d/b;Lcom/google/c/a/a/b/f/a/k;Ljava/lang/Object;)V

    sget-object v0, Lcom/google/c/a/a/b/f/a/ak;->b:Lcom/google/c/a/a/b/f/a/ak;

    invoke-virtual {p1}, Lcom/google/c/a/a/b/b/a/c/m;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, p2, p3, v0, v1}, Lcom/google/c/a/a/b/f/a/aj;->a(Ljava/lang/Object;Lcom/google/c/a/a/b/d/b;Lcom/google/c/a/a/b/f/a/k;Ljava/lang/Object;)V

    sget-object v0, Lcom/google/c/a/a/b/f/a/ak;->c:Lcom/google/c/a/a/b/f/a/ak;

    invoke-virtual {p1}, Lcom/google/c/a/a/b/b/a/c/m;->e()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, p2, p3, v0, v1}, Lcom/google/c/a/a/b/f/a/aj;->a(Ljava/lang/Object;Lcom/google/c/a/a/b/d/b;Lcom/google/c/a/a/b/f/a/k;Ljava/lang/Object;)V

    invoke-virtual {p1}, Lcom/google/c/a/a/b/b/a/c/m;->d()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/c/a/a/b/b/a/c/m;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/google/c/a/a/b/f/a/ak;->d:Lcom/google/c/a/a/b/f/a/ak;

    invoke-virtual {p1}, Lcom/google/c/a/a/b/b/a/c/m;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, p2, p3, v0, v1}, Lcom/google/c/a/a/b/f/a/aj;->a(Ljava/lang/Object;Lcom/google/c/a/a/b/d/b;Lcom/google/c/a/a/b/f/a/k;Ljava/lang/Object;)V

    :cond_0
    return-void
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 62
    instance-of v0, p1, Lcom/google/c/a/a/b/b/a/c/m;

    return v0
.end method

.method public final synthetic b(Ljava/lang/Object;Lcom/google/c/a/a/b/d/b;)Ljava/lang/Object;
    .locals 5

    .prologue
    .line 11
    sget-object v0, Lcom/google/c/a/a/b/f/a/ak;->a:Lcom/google/c/a/a/b/f/a/ak;

    const-class v1, Ljava/lang/Integer;

    invoke-virtual {p0, p1, p2, v0, v1}, Lcom/google/c/a/a/b/f/a/aj;->a(Ljava/lang/Object;Lcom/google/c/a/a/b/d/b;Lcom/google/c/a/a/b/f/a/k;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v2

    sget-object v0, Lcom/google/c/a/a/b/f/a/ak;->b:Lcom/google/c/a/a/b/f/a/ak;

    const-class v1, Ljava/lang/String;

    invoke-virtual {p0, p1, p2, v0, v1}, Lcom/google/c/a/a/b/f/a/aj;->a(Ljava/lang/Object;Lcom/google/c/a/a/b/d/b;Lcom/google/c/a/a/b/f/a/k;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    sget-object v1, Lcom/google/c/a/a/b/f/a/ak;->c:Lcom/google/c/a/a/b/f/a/ak;

    const-class v3, Ljava/lang/Integer;

    invoke-virtual {p0, p1, p2, v1, v3}, Lcom/google/c/a/a/b/f/a/aj;->a(Ljava/lang/Object;Lcom/google/c/a/a/b/d/b;Lcom/google/c/a/a/b/f/a/k;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v3

    sget-object v1, Lcom/google/c/a/a/b/f/a/ak;->d:Lcom/google/c/a/a/b/f/a/ak;

    const-class v4, Ljava/lang/String;

    invoke-virtual {p0, p1, p2, v1, v4}, Lcom/google/c/a/a/b/f/a/aj;->a(Ljava/lang/Object;Lcom/google/c/a/a/b/d/b;Lcom/google/c/a/a/b/f/a/k;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    if-nez v1, :cond_0

    move-object v1, v0

    :cond_0
    new-instance v4, Lcom/google/c/a/a/b/b/a/c/m;

    invoke-direct {v4, v0, v2, v1, v3}, Lcom/google/c/a/a/b/b/a/c/m;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    return-object v4
.end method
