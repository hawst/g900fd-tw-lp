.class public Lcom/google/c/a/a/b/c/g;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:I

.field private final b:J

.field private final c:Ljava/util/List;


# direct methods
.method public constructor <init>(IJLjava/util/Collection;)V
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    iput p1, p0, Lcom/google/c/a/a/b/c/g;->a:I

    .line 33
    iput-wide p2, p0, Lcom/google/c/a/a/b/c/g;->b:J

    .line 34
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, p4}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/c/a/a/b/c/g;->c:Ljava/util/List;

    .line 36
    if-ltz p1, :cond_0

    move v0, v1

    :goto_0
    const-string v3, "revision must be non-negative"

    invoke-static {v0, v3}, Lcom/google/c/a/a/b/h/c;->a(ZLjava/lang/String;)V

    .line 37
    const-wide/16 v4, 0x0

    cmp-long v0, p2, v4

    if-ltz v0, :cond_1

    move v0, v1

    :goto_1
    const-string v3, "request number must be non-negative"

    invoke-static {v0, v3}, Lcom/google/c/a/a/b/h/c;->a(ZLjava/lang/String;)V

    .line 38
    const-string v0, "changes"

    invoke-static {p4, v0}, Lcom/google/c/a/a/b/h/c;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 39
    invoke-interface {p4}, Ljava/util/Collection;->size()I

    move-result v0

    if-lez v0, :cond_2

    :goto_2
    const-string v0, "at least one change is required"

    invoke-static {v1, v0}, Lcom/google/c/a/a/b/h/c;->a(ZLjava/lang/String;)V

    .line 40
    return-void

    :cond_0
    move v0, v2

    .line 36
    goto :goto_0

    :cond_1
    move v0, v2

    .line 37
    goto :goto_1

    :cond_2
    move v1, v2

    .line 39
    goto :goto_2
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 46
    iget v0, p0, Lcom/google/c/a/a/b/c/g;->a:I

    return v0
.end method

.method public final b()J
    .locals 2

    .prologue
    .line 54
    iget-wide v0, p0, Lcom/google/c/a/a/b/c/g;->b:J

    return-wide v0
.end method

.method public final c()Ljava/util/List;
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Lcom/google/c/a/a/b/c/g;->c:Ljava/util/List;

    return-object v0
.end method
