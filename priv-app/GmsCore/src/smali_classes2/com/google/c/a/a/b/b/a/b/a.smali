.class abstract Lcom/google/c/a/a/b/b/a/b/a;
.super Lcom/google/c/a/a/b/b/a/b/l;
.source "SourceFile"


# instance fields
.field private final b:Lcom/google/c/a/a/b/b/a/p;

.field private c:Lcom/google/c/a/a/b/b/a/b/a;

.field private d:Ljava/util/Map;

.field protected d_:Lcom/google/c/a/a/b/b/a/b/w;

.field private e:Z

.field private f:Z

.field private g:Ljava/util/Set;


# direct methods
.method protected constructor <init>(Lcom/google/c/a/a/b/b/a/b/w;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 33
    invoke-direct {p0}, Lcom/google/c/a/a/b/b/a/b/l;-><init>()V

    .line 34
    iput-object p1, p0, Lcom/google/c/a/a/b/b/a/b/a;->d_:Lcom/google/c/a/a/b/b/a/b/w;

    .line 35
    new-instance v0, Lcom/google/c/a/a/b/b/a/p;

    invoke-direct {v0, p2}, Lcom/google/c/a/a/b/b/a/p;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/c/a/a/b/b/a/b/a;->b:Lcom/google/c/a/a/b/b/a/p;

    .line 36
    return-void
.end method

.method private o()V
    .locals 4

    .prologue
    .line 189
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    .line 190
    new-instance v2, Ljava/util/LinkedList;

    invoke-virtual {p0}, Lcom/google/c/a/a/b/b/a/b/a;->h()Ljava/util/Set;

    move-result-object v0

    invoke-direct {v2, v0}, Ljava/util/LinkedList;-><init>(Ljava/util/Collection;)V

    .line 191
    invoke-virtual {v2}, Ljava/util/LinkedList;->poll()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    :goto_0
    if-eqz v0, :cond_1

    .line 192
    invoke-interface {v1, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 193
    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 196
    iget-object v3, p0, Lcom/google/c/a/a/b/b/a/b/a;->d_:Lcom/google/c/a/a/b/b/a/b/w;

    invoke-virtual {v3, v0}, Lcom/google/c/a/a/b/b/a/b/w;->c(Ljava/lang/String;)Lcom/google/c/a/a/b/b/a/b/a;

    move-result-object v0

    .line 197
    iget-boolean v3, v0, Lcom/google/c/a/a/b/b/a/b/a;->f:Z

    if-nez v3, :cond_0

    .line 198
    const/4 v3, 0x1

    iput-boolean v3, v0, Lcom/google/c/a/a/b/b/a/b/a;->f:Z

    .line 201
    invoke-virtual {v0}, Lcom/google/c/a/a/b/b/a/b/a;->h()Ljava/util/Set;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/util/LinkedList;->addAll(Ljava/util/Collection;)Z

    .line 191
    :cond_0
    invoke-virtual {v2}, Ljava/util/LinkedList;->poll()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    goto :goto_0

    .line 203
    :cond_1
    return-void
.end method


# virtual methods
.method abstract a(Lcom/google/c/a/a/b/b/a/c/p;Lcom/google/c/a/a/b/b/a/o;ZLcom/google/c/a/a/b/b/a/b/y;Lcom/google/c/a/a/b/b/a/k;)V
.end method

.method protected final a(Lcom/google/c/a/a/b/b/a/r;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 143
    instance-of v0, p1, Lcom/google/c/a/a/b/b/a/p;

    if-eqz v0, :cond_2

    .line 144
    iget-object v0, p0, Lcom/google/c/a/a/b/b/a/b/a;->d_:Lcom/google/c/a/a/b/b/a/b/w;

    check-cast p1, Lcom/google/c/a/a/b/b/a/p;

    invoke-virtual {p1}, Lcom/google/c/a/a/b/b/a/p;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/c/a/a/b/b/a/b/w;->c(Ljava/lang/String;)Lcom/google/c/a/a/b/b/a/b/a;

    move-result-object v1

    iget-boolean v0, p0, Lcom/google/c/a/a/b/b/a/b/a;->f:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/google/c/a/a/b/b/a/b/a;->e:Z

    if-eqz v0, :cond_1

    :cond_0
    iget-boolean v0, v1, Lcom/google/c/a/a/b/b/a/b/a;->f:Z

    if-nez v0, :cond_1

    iput-boolean v3, v1, Lcom/google/c/a/a/b/b/a/b/a;->f:Z

    invoke-direct {v1}, Lcom/google/c/a/a/b/b/a/b/a;->o()V

    :cond_1
    iget-object v0, v1, Lcom/google/c/a/a/b/b/a/b/a;->d:Ljava/util/Map;

    if-nez v0, :cond_3

    iget-object v0, v1, Lcom/google/c/a/a/b/b/a/b/a;->c:Lcom/google/c/a/a/b/b/a/b/a;

    if-nez v0, :cond_3

    iput-object p0, v1, Lcom/google/c/a/a/b/b/a/b/a;->c:Lcom/google/c/a/a/b/b/a/b/a;

    .line 145
    :goto_0
    iput-object v4, p0, Lcom/google/c/a/a/b/b/a/b/a;->g:Ljava/util/Set;

    .line 147
    :cond_2
    return-void

    .line 144
    :cond_3
    iget-object v0, v1, Lcom/google/c/a/a/b/b/a/b/a;->c:Lcom/google/c/a/a/b/b/a/b/a;

    if-eqz v0, :cond_4

    new-instance v0, Ljava/util/IdentityHashMap;

    invoke-direct {v0}, Ljava/util/IdentityHashMap;-><init>()V

    iput-object v0, v1, Lcom/google/c/a/a/b/b/a/b/a;->d:Ljava/util/Map;

    iget-object v0, v1, Lcom/google/c/a/a/b/b/a/b/a;->d:Ljava/util/Map;

    iget-object v2, v1, Lcom/google/c/a/a/b/b/a/b/a;->c:Lcom/google/c/a/a/b/b/a/b/a;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v0, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iput-object v4, v1, Lcom/google/c/a/a/b/b/a/b/a;->c:Lcom/google/c/a/a/b/b/a/b/a;

    :cond_4
    iget-object v0, v1, Lcom/google/c/a/a/b/b/a/b/a;->d:Ljava/util/Map;

    invoke-interface {v0, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    if-nez v0, :cond_5

    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    :cond_5
    iget-object v1, v1, Lcom/google/c/a/a/b/b/a/b/a;->d:Ljava/util/Map;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v1, p0, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method protected final b(Lcom/google/c/a/a/b/b/a/r;)Z
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v1, 0x1

    .line 156
    instance-of v0, p1, Lcom/google/c/a/a/b/b/a/p;

    if-eqz v0, :cond_5

    .line 157
    iget-object v0, p0, Lcom/google/c/a/a/b/b/a/b/a;->d_:Lcom/google/c/a/a/b/b/a/b/w;

    check-cast p1, Lcom/google/c/a/a/b/b/a/p;

    invoke-virtual {p1}, Lcom/google/c/a/a/b/b/a/p;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/c/a/a/b/b/a/b/w;->c(Ljava/lang/String;)Lcom/google/c/a/a/b/b/a/b/a;

    move-result-object v2

    iget-object v0, v2, Lcom/google/c/a/a/b/b/a/b/a;->c:Lcom/google/c/a/a/b/b/a/b/a;

    if-eqz v0, :cond_1

    iget-object v0, v2, Lcom/google/c/a/a/b/b/a/b/a;->c:Lcom/google/c/a/a/b/b/a/b/a;

    if-ne v0, p0, :cond_1

    iput-object v4, v2, Lcom/google/c/a/a/b/b/a/b/a;->c:Lcom/google/c/a/a/b/b/a/b/a;

    .line 158
    :cond_0
    :goto_0
    iput-object v4, p0, Lcom/google/c/a/a/b/b/a/b/a;->g:Ljava/util/Set;

    move v0, v1

    .line 161
    :goto_1
    return v0

    .line 157
    :cond_1
    iget-object v0, v2, Lcom/google/c/a/a/b/b/a/b/a;->d:Ljava/util/Map;

    if-eqz v0, :cond_4

    iget-object v0, v2, Lcom/google/c/a/a/b/b/a/b/a;->d:Ljava/util/Map;

    invoke-interface {v0, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    if-nez v0, :cond_2

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Removed non-existent parent "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v3

    if-ne v3, v1, :cond_3

    iget-object v0, v2, Lcom/google/c/a/a/b/b/a/b/a;->d:Ljava/util/Map;

    invoke-interface {v0, p0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    :goto_2
    iget-object v0, v2, Lcom/google/c/a/a/b/b/a/b/a;->d:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v0

    if-ne v0, v1, :cond_0

    iget-object v0, v2, Lcom/google/c/a/a/b/b/a/b/a;->d:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-ne v0, v1, :cond_0

    iget-object v0, v2, Lcom/google/c/a/a/b/b/a/b/a;->d:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/c/a/a/b/b/a/b/a;

    iput-object v0, v2, Lcom/google/c/a/a/b/b/a/b/a;->c:Lcom/google/c/a/a/b/b/a/b/a;

    iput-object v4, v2, Lcom/google/c/a/a/b/b/a/b/a;->d:Ljava/util/Map;

    goto :goto_0

    :cond_3
    iget-object v3, v2, Lcom/google/c/a/a/b/b/a/b/a;->d:Ljava/util/Map;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v3, p0, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_2

    :cond_4
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Removed non-existent parent "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 161
    :cond_5
    const/4 v0, 0x0

    goto/16 :goto_1
.end method

.method public d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 136
    invoke-virtual {p0}, Lcom/google/c/a/a/b/b/a/b/a;->e()Lcom/google/c/a/a/b/b/a/p;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/c/a/a/b/b/a/p;->a()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public e()Lcom/google/c/a/a/b/b/a/p;
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lcom/google/c/a/a/b/b/a/b/a;->b:Lcom/google/c/a/a/b/b/a/p;

    return-object v0
.end method

.method public g()Ljava/util/List;
    .locals 6

    .prologue
    .line 207
    iget-boolean v0, p0, Lcom/google/c/a/a/b/b/a/b/a;->e:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/google/c/a/a/b/b/a/b/a;->f:Z

    if-nez v0, :cond_0

    .line 212
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    .line 237
    :goto_0
    return-object v0

    .line 215
    :cond_0
    new-instance v0, Ljava/util/IdentityHashMap;

    invoke-direct {v0}, Ljava/util/IdentityHashMap;-><init>()V

    invoke-static {v0}, Ljava/util/Collections;->newSetFromMap(Ljava/util/Map;)Ljava/util/Set;

    move-result-object v1

    .line 217
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 218
    new-instance v3, Ljava/util/Stack;

    invoke-direct {v3}, Ljava/util/Stack;-><init>()V

    .line 220
    invoke-virtual {v3, p0}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    .line 221
    invoke-interface {v1, p0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 222
    :cond_1
    invoke-virtual {v3}, Ljava/util/Stack;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_6

    .line 223
    invoke-virtual {v3}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/c/a/a/b/b/a/b/a;

    .line 224
    iget-boolean v4, v0, Lcom/google/c/a/a/b/b/a/b/a;->e:Z

    if-eqz v4, :cond_2

    .line 225
    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 227
    :cond_2
    iget-boolean v4, v0, Lcom/google/c/a/a/b/b/a/b/a;->f:Z

    if-eqz v4, :cond_1

    .line 228
    iget-object v4, v0, Lcom/google/c/a/a/b/b/a/b/a;->c:Lcom/google/c/a/a/b/b/a/b/a;

    if-nez v4, :cond_4

    iget-object v4, v0, Lcom/google/c/a/a/b/b/a/b/a;->d:Ljava/util/Map;

    if-nez v4, :cond_4

    invoke-static {}, Ljava/util/Collections;->emptySet()Ljava/util/Set;

    move-result-object v0

    :goto_1
    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_3
    :goto_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/c/a/a/b/b/a/b/a;

    .line 229
    invoke-interface {v1, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_3

    .line 230
    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 233
    invoke-virtual {v3, v0}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_2

    .line 228
    :cond_4
    iget-object v4, v0, Lcom/google/c/a/a/b/b/a/b/a;->d:Ljava/util/Map;

    if-nez v4, :cond_5

    iget-object v0, v0, Lcom/google/c/a/a/b/b/a/b/a;->c:Lcom/google/c/a/a/b/b/a/b/a;

    invoke-static {v0}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    goto :goto_1

    :cond_5
    iget-object v0, v0, Lcom/google/c/a/a/b/b/a/b/a;->d:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableCollection(Ljava/util/Collection;)Ljava/util/Collection;

    move-result-object v0

    goto :goto_1

    .line 237
    :cond_6
    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    goto :goto_0
.end method

.method public final h()Ljava/util/Set;
    .locals 4

    .prologue
    .line 167
    iget-object v0, p0, Lcom/google/c/a/a/b/b/a/b/a;->g:Ljava/util/Set;

    if-nez v0, :cond_2

    .line 168
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    .line 169
    invoke-virtual {p0}, Lcom/google/c/a/a/b/b/a/b/a;->n()Ljava/lang/Iterable;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    .line 170
    instance-of v3, v0, Lcom/google/c/a/a/b/b/a/p;

    if-eqz v3, :cond_0

    .line 171
    check-cast v0, Lcom/google/c/a/a/b/b/a/p;

    invoke-virtual {v0}, Lcom/google/c/a/a/b/b/a/p;->a()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 174
    :cond_1
    invoke-static {v1}, Ljava/util/Collections;->unmodifiableSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, Lcom/google/c/a/a/b/b/a/b/a;->g:Ljava/util/Set;

    .line 176
    :cond_2
    iget-object v0, p0, Lcom/google/c/a/a/b/b/a/b/a;->g:Ljava/util/Set;

    return-object v0
.end method

.method public j()Lcom/google/c/a/a/b/b/a/b;
    .locals 1

    .prologue
    .line 132
    iget-object v0, p0, Lcom/google/c/a/a/b/b/a/b/a;->d_:Lcom/google/c/a/a/b/b/a/b/w;

    return-object v0
.end method

.method public m()V
    .locals 1

    .prologue
    .line 181
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/c/a/a/b/b/a/b/a;->e:Z

    .line 182
    invoke-direct {p0}, Lcom/google/c/a/a/b/b/a/b/a;->o()V

    .line 183
    return-void
.end method

.method protected abstract n()Ljava/lang/Iterable;
.end method
