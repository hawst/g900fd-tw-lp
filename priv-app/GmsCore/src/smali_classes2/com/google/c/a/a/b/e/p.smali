.class public final Lcom/google/c/a/a/b/e/p;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final a:Ljava/util/Random;


# instance fields
.field private b:Ljava/util/LinkedList;

.field private c:I

.field private d:I

.field private e:J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 26
    new-instance v0, Ljava/util/Random;

    invoke-direct {v0}, Ljava/util/Random;-><init>()V

    sput-object v0, Lcom/google/c/a/a/b/e/p;->a:Ljava/util/Random;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/google/c/a/a/b/e/p;->b:Ljava/util/LinkedList;

    .line 29
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/c/a/a/b/e/p;->c:I

    .line 31
    sget-object v0, Lcom/google/c/a/a/b/e/p;->a:Ljava/util/Random;

    invoke-virtual {v0}, Ljava/util/Random;->nextLong()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Math;->abs(J)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/c/a/a/b/e/p;->e:J

    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 51
    iget v0, p0, Lcom/google/c/a/a/b/e/p;->c:I

    return v0
.end method

.method public final a(Lcom/google/c/b/a/c;)Lcom/google/c/a/a/b/c/e;
    .locals 4

    .prologue
    .line 97
    new-instance v0, Lcom/google/c/a/a/b/c/e;

    iget-wide v2, p0, Lcom/google/c/a/a/b/e/p;->e:J

    invoke-direct {v0, v2, v3, p1}, Lcom/google/c/a/a/b/c/e;-><init>(JLcom/google/c/b/a/c;)V

    .line 98
    iget-object v1, p0, Lcom/google/c/a/a/b/e/p;->b:Ljava/util/LinkedList;

    invoke-virtual {v1, v0}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 99
    return-object v0
.end method

.method public final a(I)V
    .locals 2

    .prologue
    .line 34
    iget v0, p0, Lcom/google/c/a/a/b/e/p;->c:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 35
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Initial revision has already been set."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 37
    :cond_0
    iput p1, p0, Lcom/google/c/a/a/b/e/p;->c:I

    .line 38
    return-void
.end method

.method public final a(Lcom/google/c/a/a/b/a/f;)V
    .locals 2

    .prologue
    .line 44
    new-instance v0, Ljava/util/LinkedList;

    iget-object v1, p1, Lcom/google/c/a/a/b/a/f;->d:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/LinkedList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/google/c/a/a/b/e/p;->b:Ljava/util/LinkedList;

    .line 45
    return-void
.end method

.method public final a(Ljava/util/List;I)V
    .locals 1

    .prologue
    .line 88
    iput p2, p0, Lcom/google/c/a/a/b/e/p;->c:I

    .line 89
    iget-object v0, p0, Lcom/google/c/a/a/b/e/p;->b:Ljava/util/LinkedList;

    invoke-static {v0, p1}, Lcom/google/c/a/a/b/b/b/u;->b(Ljava/util/List;Ljava/util/List;)V

    .line 90
    return-void
.end method

.method public final a(Lcom/google/c/a/a/b/c/d;)Z
    .locals 8

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 132
    invoke-virtual {p1}, Lcom/google/c/a/a/b/c/d;->d()I

    move-result v0

    iget v3, p0, Lcom/google/c/a/a/b/e/p;->c:I

    add-int/lit8 v3, v3, 0x1

    if-ne v0, v3, :cond_1

    move v0, v1

    :goto_0
    const-string v3, "Out of sequence acknowledgment."

    invoke-static {v0, v3}, Lcom/google/c/a/a/b/h/c;->b(ZLjava/lang/String;)V

    .line 134
    iget-object v0, p0, Lcom/google/c/a/a/b/e/p;->b:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->peek()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/c/a/a/b/c/e;

    .line 135
    if-nez v0, :cond_2

    .line 146
    :cond_0
    :goto_1
    return v2

    :cond_1
    move v0, v2

    .line 132
    goto :goto_0

    .line 138
    :cond_2
    invoke-virtual {p1}, Lcom/google/c/a/a/b/c/d;->g()Ljava/lang/Long;

    move-result-object v3

    if-eqz v3, :cond_3

    .line 139
    invoke-virtual {p1}, Lcom/google/c/a/a/b/c/d;->g()Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-virtual {v0}, Lcom/google/c/a/a/b/c/e;->a()J

    move-result-wide v6

    cmp-long v0, v4, v6

    if-nez v0, :cond_0

    .line 143
    :cond_3
    iget-object v0, p0, Lcom/google/c/a/a/b/e/p;->b:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->pop()Ljava/lang/Object;

    .line 144
    iget v0, p0, Lcom/google/c/a/a/b/e/p;->d:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/google/c/a/a/b/e/p;->d:I

    .line 145
    iget v0, p0, Lcom/google/c/a/a/b/e/p;->c:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/c/a/a/b/e/p;->c:I

    move v2, v1

    .line 146
    goto :goto_1
.end method

.method public final b()Z
    .locals 2

    .prologue
    .line 58
    iget-object v0, p0, Lcom/google/c/a/a/b/e/p;->b:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->size()I

    move-result v0

    iget v1, p0, Lcom/google/c/a/a/b/e/p;->d:I

    if-le v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 65
    iget v0, p0, Lcom/google/c/a/a/b/e/p;->d:I

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method final d()Ljava/util/List;
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lcom/google/c/a/a/b/e/p;->b:Ljava/util/LinkedList;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public final e()Z
    .locals 2

    .prologue
    .line 79
    invoke-virtual {p0}, Lcom/google/c/a/a/b/e/p;->c()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/c/a/a/b/e/p;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/google/c/a/a/b/e/p;->c:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final f()Lcom/google/c/a/a/b/c/g;
    .locals 8

    .prologue
    .line 107
    invoke-virtual {p0}, Lcom/google/c/a/a/b/e/p;->e()Z

    move-result v0

    if-nez v0, :cond_0

    .line 108
    const/4 v0, 0x0

    .line 121
    :goto_0
    return-object v0

    .line 110
    :cond_0
    iget-object v0, p0, Lcom/google/c/a/a/b/e/p;->b:Ljava/util/LinkedList;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/c/a/a/b/c/e;

    invoke-virtual {v0}, Lcom/google/c/a/a/b/c/e;->a()J

    move-result-wide v2

    .line 111
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 112
    iget-object v0, p0, Lcom/google/c/a/a/b/e/p;->b:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/c/a/a/b/c/e;

    .line 113
    invoke-virtual {v0}, Lcom/google/c/a/a/b/c/e;->a()J

    move-result-wide v6

    cmp-long v5, v6, v2

    if-nez v5, :cond_1

    .line 114
    invoke-virtual {v0}, Lcom/google/c/a/a/b/c/e;->b()Lcom/google/c/b/a/c;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 118
    :cond_1
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v0

    iput v0, p0, Lcom/google/c/a/a/b/e/p;->d:I

    .line 119
    new-instance v0, Lcom/google/c/a/a/b/c/g;

    iget v4, p0, Lcom/google/c/a/a/b/e/p;->c:I

    invoke-direct {v0, v4, v2, v3, v1}, Lcom/google/c/a/a/b/c/g;-><init>(IJLjava/util/Collection;)V

    .line 120
    iget-wide v2, p0, Lcom/google/c/a/a/b/e/p;->e:J

    const-wide/16 v4, 0x1

    add-long/2addr v2, v4

    iput-wide v2, p0, Lcom/google/c/a/a/b/e/p;->e:J

    goto :goto_0
.end method
