.class public final Lcom/google/c/a/a/b/b/a/a;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final a:Lcom/google/c/a/a/b/b/a/a;


# instance fields
.field private final b:Ljava/util/List;

.field private final c:Ljava/util/Collection;

.field private final d:Ljava/util/Set;

.field private final e:Lcom/google/c/a/a/b/b/a/q;

.field private final f:Lcom/google/c/a/a/b/b/a/c/p;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    .line 17
    new-instance v0, Lcom/google/c/a/a/b/b/a/a;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v1

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v2

    invoke-static {}, Ljava/util/Collections;->emptySet()Ljava/util/Set;

    move-result-object v3

    const/4 v4, 0x0

    invoke-static {}, Lcom/google/c/a/a/b/b/a/q;->a()Lcom/google/c/a/a/b/b/a/q;

    move-result-object v5

    invoke-direct/range {v0 .. v5}, Lcom/google/c/a/a/b/b/a/a;-><init>(Ljava/util/List;Ljava/util/Collection;Ljava/util/Set;Lcom/google/c/a/a/b/b/a/c/p;Lcom/google/c/a/a/b/b/a/q;)V

    sput-object v0, Lcom/google/c/a/a/b/b/a/a;->a:Lcom/google/c/a/a/b/b/a/a;

    return-void
.end method

.method public constructor <init>(Ljava/util/List;Ljava/util/Collection;Ljava/util/Set;Lcom/google/c/a/a/b/b/a/c/p;Lcom/google/c/a/a/b/b/a/q;)V
    .locals 1

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    const-string v0, "events"

    invoke-static {p1, v0}, Lcom/google/c/a/a/b/h/c;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    iput-object v0, p0, Lcom/google/c/a/a/b/b/a/a;->b:Ljava/util/List;

    .line 34
    iput-object p2, p0, Lcom/google/c/a/a/b/b/a/a;->c:Ljava/util/Collection;

    .line 35
    iput-object p3, p0, Lcom/google/c/a/a/b/b/a/a;->d:Ljava/util/Set;

    .line 36
    iput-object p4, p0, Lcom/google/c/a/a/b/b/a/a;->f:Lcom/google/c/a/a/b/b/a/c/p;

    .line 37
    iput-object p5, p0, Lcom/google/c/a/a/b/b/a/a;->e:Lcom/google/c/a/a/b/b/a/q;

    .line 38
    return-void
.end method

.method public static a(Ljava/util/Set;)Lcom/google/c/a/a/b/b/a/a;
    .locals 6

    .prologue
    .line 89
    new-instance v0, Lcom/google/c/a/a/b/b/a/a;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v1

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v2

    const/4 v4, 0x0

    invoke-static {}, Lcom/google/c/a/a/b/b/a/q;->a()Lcom/google/c/a/a/b/b/a/q;

    move-result-object v5

    move-object v3, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/c/a/a/b/b/a/a;-><init>(Ljava/util/List;Ljava/util/Collection;Ljava/util/Set;Lcom/google/c/a/a/b/b/a/c/p;Lcom/google/c/a/a/b/b/a/q;)V

    return-object v0
.end method

.method public static f()Lcom/google/c/a/a/b/b/a/a;
    .locals 1

    .prologue
    .line 82
    sget-object v0, Lcom/google/c/a/a/b/b/a/a;->a:Lcom/google/c/a/a/b/b/a/a;

    return-object v0
.end method


# virtual methods
.method public final a()Ljava/util/List;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcom/google/c/a/a/b/b/a/a;->b:Ljava/util/List;

    return-object v0
.end method

.method public final b()Ljava/util/Collection;
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lcom/google/c/a/a/b/b/a/a;->c:Ljava/util/Collection;

    return-object v0
.end method

.method public final c()Lcom/google/c/a/a/b/b/a/q;
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lcom/google/c/a/a/b/b/a/a;->e:Lcom/google/c/a/a/b/b/a/q;

    return-object v0
.end method

.method public final d()Ljava/util/Set;
    .locals 1

    .prologue
    .line 67
    iget-object v0, p0, Lcom/google/c/a/a/b/b/a/a;->d:Ljava/util/Set;

    return-object v0
.end method

.method public final e()Lcom/google/c/a/a/b/b/a/c/p;
    .locals 1

    .prologue
    .line 75
    iget-object v0, p0, Lcom/google/c/a/a/b/b/a/a;->f:Lcom/google/c/a/a/b/b/a/c/p;

    return-object v0
.end method
