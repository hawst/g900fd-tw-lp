.class public final enum Lcom/google/c/a/a/b/f/a/ak;
.super Ljava/lang/Enum;
.source "SourceFile"

# interfaces
.implements Lcom/google/c/a/a/b/f/a/k;


# static fields
.field public static final enum a:Lcom/google/c/a/a/b/f/a/ak;

.field public static final enum b:Lcom/google/c/a/a/b/f/a/ak;

.field public static final enum c:Lcom/google/c/a/a/b/f/a/ak;

.field public static final enum d:Lcom/google/c/a/a/b/f/a/ak;

.field private static final synthetic f:[Lcom/google/c/a/a/b/f/a/ak;


# instance fields
.field private e:I


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 17
    new-instance v0, Lcom/google/c/a/a/b/f/a/ak;

    const-string v1, "SOURCE_INDEX"

    invoke-direct {v0, v1, v2, v2}, Lcom/google/c/a/a/b/f/a/ak;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/c/a/a/b/f/a/ak;->a:Lcom/google/c/a/a/b/f/a/ak;

    .line 18
    new-instance v0, Lcom/google/c/a/a/b/f/a/ak;

    const-string v1, "SOURCE_ID"

    invoke-direct {v0, v1, v3, v3}, Lcom/google/c/a/a/b/f/a/ak;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/c/a/a/b/f/a/ak;->b:Lcom/google/c/a/a/b/f/a/ak;

    .line 19
    new-instance v0, Lcom/google/c/a/a/b/f/a/ak;

    const-string v1, "DESTINATION_INDEX"

    invoke-direct {v0, v1, v4, v4}, Lcom/google/c/a/a/b/f/a/ak;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/c/a/a/b/f/a/ak;->c:Lcom/google/c/a/a/b/f/a/ak;

    .line 20
    new-instance v0, Lcom/google/c/a/a/b/f/a/ak;

    const-string v1, "DESTINATION_ID"

    invoke-direct {v0, v1, v5, v5}, Lcom/google/c/a/a/b/f/a/ak;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/c/a/a/b/f/a/ak;->d:Lcom/google/c/a/a/b/f/a/ak;

    .line 16
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/google/c/a/a/b/f/a/ak;

    sget-object v1, Lcom/google/c/a/a/b/f/a/ak;->a:Lcom/google/c/a/a/b/f/a/ak;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/c/a/a/b/f/a/ak;->b:Lcom/google/c/a/a/b/f/a/ak;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/c/a/a/b/f/a/ak;->c:Lcom/google/c/a/a/b/f/a/ak;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/c/a/a/b/f/a/ak;->d:Lcom/google/c/a/a/b/f/a/ak;

    aput-object v1, v0, v5

    sput-object v0, Lcom/google/c/a/a/b/f/a/ak;->f:[Lcom/google/c/a/a/b/f/a/ak;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 25
    iput p3, p0, Lcom/google/c/a/a/b/f/a/ak;->e:I

    .line 26
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/c/a/a/b/f/a/ak;
    .locals 1

    .prologue
    .line 16
    const-class v0, Lcom/google/c/a/a/b/f/a/ak;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/c/a/a/b/f/a/ak;

    return-object v0
.end method

.method public static values()[Lcom/google/c/a/a/b/f/a/ak;
    .locals 1

    .prologue
    .line 16
    sget-object v0, Lcom/google/c/a/a/b/f/a/ak;->f:[Lcom/google/c/a/a/b/f/a/ak;

    invoke-virtual {v0}, [Lcom/google/c/a/a/b/f/a/ak;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/c/a/a/b/f/a/ak;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 30
    iget v0, p0, Lcom/google/c/a/a/b/f/a/ak;->e:I

    return v0
.end method
