.class public final enum Lcom/google/c/a/a/a/c/a/d/c;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field public static final enum a:Lcom/google/c/a/a/a/c/a/d/c;

.field public static final enum b:Lcom/google/c/a/a/a/c/a/d/c;

.field public static final enum c:Lcom/google/c/a/a/a/c/a/d/c;

.field public static final enum d:Lcom/google/c/a/a/a/c/a/d/c;

.field private static final synthetic e:[Lcom/google/c/a/a/a/c/a/d/c;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 67
    new-instance v0, Lcom/google/c/a/a/a/c/a/d/c;

    const-string v1, "CLOSED"

    invoke-direct {v0, v1, v2}, Lcom/google/c/a/a/a/c/a/d/c;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/c/a/a/a/c/a/d/c;->a:Lcom/google/c/a/a/a/c/a/d/c;

    .line 72
    new-instance v0, Lcom/google/c/a/a/a/c/a/d/c;

    const-string v1, "CONNECTING"

    invoke-direct {v0, v1, v3}, Lcom/google/c/a/a/a/c/a/d/c;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/c/a/a/a/c/a/d/c;->b:Lcom/google/c/a/a/a/c/a/d/c;

    .line 77
    new-instance v0, Lcom/google/c/a/a/a/c/a/d/c;

    const-string v1, "NEW"

    invoke-direct {v0, v1, v4}, Lcom/google/c/a/a/a/c/a/d/c;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/c/a/a/a/c/a/d/c;->c:Lcom/google/c/a/a/a/c/a/d/c;

    .line 82
    new-instance v0, Lcom/google/c/a/a/a/c/a/d/c;

    const-string v1, "OPEN"

    invoke-direct {v0, v1, v5}, Lcom/google/c/a/a/a/c/a/d/c;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/c/a/a/a/c/a/d/c;->d:Lcom/google/c/a/a/a/c/a/d/c;

    .line 59
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/google/c/a/a/a/c/a/d/c;

    sget-object v1, Lcom/google/c/a/a/a/c/a/d/c;->a:Lcom/google/c/a/a/a/c/a/d/c;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/c/a/a/a/c/a/d/c;->b:Lcom/google/c/a/a/a/c/a/d/c;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/c/a/a/a/c/a/d/c;->c:Lcom/google/c/a/a/a/c/a/d/c;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/c/a/a/a/c/a/d/c;->d:Lcom/google/c/a/a/a/c/a/d/c;

    aput-object v1, v0, v5

    sput-object v0, Lcom/google/c/a/a/a/c/a/d/c;->e:[Lcom/google/c/a/a/a/c/a/d/c;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 59
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/c/a/a/a/c/a/d/c;
    .locals 1

    .prologue
    .line 59
    const-class v0, Lcom/google/c/a/a/a/c/a/d/c;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/c/a/a/a/c/a/d/c;

    return-object v0
.end method

.method public static values()[Lcom/google/c/a/a/a/c/a/d/c;
    .locals 1

    .prologue
    .line 59
    sget-object v0, Lcom/google/c/a/a/a/c/a/d/c;->e:[Lcom/google/c/a/a/a/c/a/d/c;

    invoke-virtual {v0}, [Lcom/google/c/a/a/a/c/a/d/c;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/c/a/a/a/c/a/d/c;

    return-object v0
.end method
