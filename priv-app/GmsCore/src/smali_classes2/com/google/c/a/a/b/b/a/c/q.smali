.class public final Lcom/google/c/a/a/b/b/a/c/q;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method public static a(Ljava/util/List;Ljava/lang/String;)Lcom/google/c/b/a/c;
    .locals 2

    .prologue
    .line 39
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 40
    const/4 v0, 0x0

    invoke-interface {p0, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/c/b/a/c;

    .line 45
    :goto_0
    return-object v0

    .line 42
    :cond_0
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_1

    .line 43
    invoke-static {}, Lcom/google/c/b/a/d;->a()Lcom/google/c/b/a/d;

    move-result-object v0

    goto :goto_0

    .line 45
    :cond_1
    new-instance v0, Lcom/google/c/a/a/b/b/a/c/o;

    invoke-direct {v0}, Lcom/google/c/a/a/b/b/a/c/o;-><init>()V

    iput-object p1, v0, Lcom/google/c/a/a/b/b/a/c/o;->a:Ljava/lang/String;

    invoke-virtual {v0, p0}, Lcom/google/c/a/a/b/b/a/c/o;->a(Ljava/util/Collection;)Lcom/google/c/a/a/b/b/a/c/o;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/c/a/a/b/b/a/c/o;->a()Lcom/google/c/a/a/b/b/a/c/n;

    move-result-object v0

    goto :goto_0
.end method

.method private static a(Lcom/google/c/a/a/b/b/a/c/n;Ljava/util/List;Z)Ljava/util/List;
    .locals 3

    .prologue
    .line 111
    invoke-virtual {p0}, Lcom/google/c/a/a/b/b/a/c/n;->c()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/c/b/a/c;

    .line 112
    instance-of v2, v0, Lcom/google/c/a/a/b/b/a/c/n;

    if-eqz v2, :cond_1

    .line 113
    check-cast v0, Lcom/google/c/a/a/b/b/a/c/n;

    invoke-static {v0, p1, p2}, Lcom/google/c/a/a/b/b/a/c/q;->a(Lcom/google/c/a/a/b/b/a/c/n;Ljava/util/List;Z)Ljava/util/List;

    goto :goto_0

    .line 114
    :cond_1
    instance-of v2, v0, Lcom/google/c/b/a/d;

    if-eqz v2, :cond_2

    .line 115
    if-eqz p2, :cond_0

    .line 116
    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 119
    :cond_2
    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 122
    :cond_3
    return-object p1
.end method

.method public static a(Ljava/util/List;)Ljava/util/List;
    .locals 4

    .prologue
    .line 65
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 66
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/c/b/a/c;

    .line 67
    instance-of v3, v0, Lcom/google/c/a/a/b/b/a/c/n;

    if-eqz v3, :cond_1

    check-cast v0, Lcom/google/c/a/a/b/b/a/c/n;

    const/4 v3, 0x0

    invoke-static {v0, v1, v3}, Lcom/google/c/a/a/b/b/a/c/q;->a(Lcom/google/c/a/a/b/b/a/c/n;Ljava/util/List;Z)Ljava/util/List;

    goto :goto_0

    :cond_1
    instance-of v3, v0, Lcom/google/c/b/a/d;

    if-nez v3, :cond_0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 79
    :cond_2
    return-object v1
.end method

.method public static b(Ljava/util/List;)Ljava/util/Set;
    .locals 4

    .prologue
    .line 133
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    .line 134
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/c/a/a/b/b/a/r;

    .line 135
    instance-of v3, v0, Lcom/google/c/a/a/b/b/a/p;

    if-eqz v3, :cond_0

    .line 136
    check-cast v0, Lcom/google/c/a/a/b/b/a/p;

    invoke-virtual {v0}, Lcom/google/c/a/a/b/b/a/p;->a()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 139
    :cond_1
    invoke-static {v1}, Ljava/util/Collections;->unmodifiableSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method
