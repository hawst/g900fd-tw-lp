.class public final Lcom/google/c/a/a/b/h/a/c;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method private static a(Lcom/google/c/a/a/b/h/a/h;Lcom/google/c/a/a/b/h/a/h;)I
    .locals 3

    .prologue
    .line 276
    invoke-virtual {p0}, Lcom/google/c/a/a/b/h/a/h;->a()I

    move-result v0

    invoke-virtual {p1}, Lcom/google/c/a/a/b/h/a/h;->a()I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v1

    .line 277
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_1

    .line 278
    invoke-virtual {p0, v0, p1, v0}, Lcom/google/c/a/a/b/h/a/h;->a(ILcom/google/c/a/a/b/h/a/h;I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 282
    :goto_1
    return v0

    .line 277
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    move v0, v1

    .line 282
    goto :goto_1
.end method

.method private static a(Lcom/google/c/a/a/b/h/a/h;Lcom/google/c/a/a/b/h/a/h;IILjava/lang/String;)Lcom/google/c/a/a/b/h/a/d;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 260
    invoke-virtual {p0, v1, p2}, Lcom/google/c/a/a/b/h/a/h;->a(II)Lcom/google/c/a/a/b/h/a/h;

    move-result-object v0

    .line 261
    invoke-virtual {p1, v1, p3}, Lcom/google/c/a/a/b/h/a/h;->a(II)Lcom/google/c/a/a/b/h/a/h;

    move-result-object v1

    .line 262
    invoke-virtual {p0}, Lcom/google/c/a/a/b/h/a/h;->a()I

    move-result v2

    invoke-virtual {p0, p2, v2}, Lcom/google/c/a/a/b/h/a/h;->a(II)Lcom/google/c/a/a/b/h/a/h;

    move-result-object v2

    .line 263
    invoke-virtual {p1}, Lcom/google/c/a/a/b/h/a/h;->a()I

    move-result v3

    invoke-virtual {p1, p3, v3}, Lcom/google/c/a/a/b/h/a/h;->a(II)Lcom/google/c/a/a/b/h/a/h;

    move-result-object v3

    .line 265
    invoke-static {v0, v1, p4}, Lcom/google/c/a/a/b/h/a/c;->a(Lcom/google/c/a/a/b/h/a/h;Lcom/google/c/a/a/b/h/a/h;Ljava/lang/String;)Lcom/google/c/a/a/b/h/a/d;

    move-result-object v0

    .line 266
    invoke-static {v2, v3, p4}, Lcom/google/c/a/a/b/h/a/c;->a(Lcom/google/c/a/a/b/h/a/h;Lcom/google/c/a/a/b/h/a/h;Ljava/lang/String;)Lcom/google/c/a/a/b/h/a/d;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/c/a/a/b/h/a/d;->a(Lcom/google/c/a/a/b/h/a/d;Lcom/google/c/a/a/b/h/a/d;)V

    .line 267
    return-object v0
.end method

.method public static a(Lcom/google/c/a/a/b/h/a/h;Lcom/google/c/a/a/b/h/a/h;Ljava/lang/String;)Lcom/google/c/a/a/b/h/a/d;
    .locals 13

    .prologue
    const/4 v12, 0x0

    .line 38
    if-eqz p0, :cond_0

    if-nez p1, :cond_1

    .line 39
    :cond_0
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 43
    :cond_1
    invoke-virtual {p0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 44
    new-instance v0, Lcom/google/c/a/a/b/h/a/d;

    invoke-direct {v0, p2}, Lcom/google/c/a/a/b/h/a/d;-><init>(Ljava/lang/String;)V

    .line 45
    invoke-virtual {p0}, Lcom/google/c/a/a/b/h/a/h;->a()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/c/a/a/b/h/a/d;->a(I)V

    .line 75
    :cond_2
    :goto_0
    return-object v0

    .line 50
    :cond_3
    invoke-static {p0, p1}, Lcom/google/c/a/a/b/h/a/c;->a(Lcom/google/c/a/a/b/h/a/h;Lcom/google/c/a/a/b/h/a/h;)I

    move-result v5

    .line 51
    invoke-virtual {p0}, Lcom/google/c/a/a/b/h/a/h;->a()I

    move-result v0

    invoke-virtual {p0, v5, v0}, Lcom/google/c/a/a/b/h/a/h;->a(II)Lcom/google/c/a/a/b/h/a/h;

    move-result-object v0

    .line 52
    invoke-virtual {p1}, Lcom/google/c/a/a/b/h/a/h;->a()I

    move-result v1

    invoke-virtual {p1, v5, v1}, Lcom/google/c/a/a/b/h/a/h;->a(II)Lcom/google/c/a/a/b/h/a/h;

    move-result-object v2

    .line 55
    invoke-static {v0, v2}, Lcom/google/c/a/a/b/h/a/c;->b(Lcom/google/c/a/a/b/h/a/h;Lcom/google/c/a/a/b/h/a/h;)I

    move-result v6

    .line 56
    invoke-virtual {v0}, Lcom/google/c/a/a/b/h/a/h;->a()I

    move-result v1

    sub-int/2addr v1, v6

    invoke-virtual {v0, v12, v1}, Lcom/google/c/a/a/b/h/a/h;->a(II)Lcom/google/c/a/a/b/h/a/h;

    move-result-object v1

    .line 57
    invoke-virtual {v2}, Lcom/google/c/a/a/b/h/a/h;->a()I

    move-result v0

    sub-int/2addr v0, v6

    invoke-virtual {v2, v12, v0}, Lcom/google/c/a/a/b/h/a/h;->a(II)Lcom/google/c/a/a/b/h/a/h;

    move-result-object v2

    .line 60
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    const-wide/16 v10, 0xc8

    add-long/2addr v8, v10

    invoke-virtual {v1}, Lcom/google/c/a/a/b/h/a/h;->b()Z

    move-result v0

    if-eqz v0, :cond_4

    new-instance v1, Lcom/google/c/a/a/b/h/a/d;

    invoke-direct {v1, p2}, Lcom/google/c/a/a/b/h/a/d;-><init>(Ljava/lang/String;)V

    invoke-static {v1, v2}, Lcom/google/c/a/a/b/h/a/d;->a(Lcom/google/c/a/a/b/h/a/d;Lcom/google/c/a/a/b/h/a/h;)V

    .line 64
    :goto_1
    if-lez v5, :cond_b

    .line 65
    new-instance v0, Lcom/google/c/a/a/b/h/a/d;

    invoke-direct {v0, p2}, Lcom/google/c/a/a/b/h/a/d;-><init>(Ljava/lang/String;)V

    .line 66
    invoke-virtual {v0, v5}, Lcom/google/c/a/a/b/h/a/d;->a(I)V

    .line 67
    invoke-static {v0, v1}, Lcom/google/c/a/a/b/h/a/d;->a(Lcom/google/c/a/a/b/h/a/d;Lcom/google/c/a/a/b/h/a/d;)V

    .line 71
    :goto_2
    if-lez v6, :cond_2

    .line 72
    invoke-virtual {v0, v6}, Lcom/google/c/a/a/b/h/a/d;->a(I)V

    goto :goto_0

    .line 60
    :cond_4
    invoke-virtual {v2}, Lcom/google/c/a/a/b/h/a/h;->b()Z

    move-result v0

    if-eqz v0, :cond_5

    new-instance v0, Lcom/google/c/a/a/b/h/a/d;

    invoke-direct {v0, p2}, Lcom/google/c/a/a/b/h/a/d;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Lcom/google/c/a/a/b/h/a/h;->a()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/c/a/a/b/h/a/d;->b(I)V

    move-object v1, v0

    goto :goto_1

    :cond_5
    invoke-virtual {v1}, Lcom/google/c/a/a/b/h/a/h;->a()I

    move-result v0

    invoke-virtual {v2}, Lcom/google/c/a/a/b/h/a/h;->a()I

    move-result v3

    if-le v0, v3, :cond_6

    move-object v0, v1

    :goto_3
    invoke-virtual {v1}, Lcom/google/c/a/a/b/h/a/h;->a()I

    move-result v3

    invoke-virtual {v2}, Lcom/google/c/a/a/b/h/a/h;->a()I

    move-result v4

    if-le v3, v4, :cond_7

    move-object v3, v2

    :goto_4
    invoke-virtual {v0, v3}, Lcom/google/c/a/a/b/h/a/h;->a(Lcom/google/c/a/a/b/h/a/h;)I

    move-result v7

    const/4 v4, -0x1

    if-eq v7, v4, :cond_9

    new-instance v4, Lcom/google/c/a/a/b/h/a/d;

    invoke-direct {v4, p2}, Lcom/google/c/a/a/b/h/a/d;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Lcom/google/c/a/a/b/h/a/h;->a()I

    move-result v1

    invoke-virtual {v2}, Lcom/google/c/a/a/b/h/a/h;->a()I

    move-result v2

    if-le v1, v2, :cond_8

    invoke-virtual {v4, v7}, Lcom/google/c/a/a/b/h/a/d;->b(I)V

    invoke-virtual {v3}, Lcom/google/c/a/a/b/h/a/h;->a()I

    move-result v1

    invoke-virtual {v4, v1}, Lcom/google/c/a/a/b/h/a/d;->a(I)V

    invoke-virtual {v0}, Lcom/google/c/a/a/b/h/a/h;->a()I

    move-result v0

    invoke-virtual {v3}, Lcom/google/c/a/a/b/h/a/h;->a()I

    move-result v1

    add-int/2addr v1, v7

    sub-int/2addr v0, v1

    invoke-virtual {v4, v0}, Lcom/google/c/a/a/b/h/a/d;->b(I)V

    :goto_5
    move-object v1, v4

    goto :goto_1

    :cond_6
    move-object v0, v2

    goto :goto_3

    :cond_7
    move-object v3, v1

    goto :goto_4

    :cond_8
    invoke-virtual {v0, v12, v7}, Lcom/google/c/a/a/b/h/a/h;->a(II)Lcom/google/c/a/a/b/h/a/h;

    move-result-object v1

    invoke-static {v4, v1}, Lcom/google/c/a/a/b/h/a/d;->a(Lcom/google/c/a/a/b/h/a/d;Lcom/google/c/a/a/b/h/a/h;)V

    invoke-virtual {v3}, Lcom/google/c/a/a/b/h/a/h;->a()I

    move-result v1

    invoke-virtual {v4, v1}, Lcom/google/c/a/a/b/h/a/d;->a(I)V

    invoke-virtual {v3}, Lcom/google/c/a/a/b/h/a/h;->a()I

    move-result v1

    add-int/2addr v1, v7

    invoke-virtual {v0}, Lcom/google/c/a/a/b/h/a/h;->a()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/google/c/a/a/b/h/a/h;->a(II)Lcom/google/c/a/a/b/h/a/h;

    move-result-object v0

    invoke-static {v4, v0}, Lcom/google/c/a/a/b/h/a/d;->a(Lcom/google/c/a/a/b/h/a/d;Lcom/google/c/a/a/b/h/a/h;)V

    goto :goto_5

    :cond_9
    invoke-virtual {v3}, Lcom/google/c/a/a/b/h/a/h;->a()I

    move-result v0

    const/4 v3, 0x1

    if-ne v0, v3, :cond_a

    new-instance v0, Lcom/google/c/a/a/b/h/a/d;

    invoke-direct {v0, p2}, Lcom/google/c/a/a/b/h/a/d;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Lcom/google/c/a/a/b/h/a/h;->a()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/c/a/a/b/h/a/d;->b(I)V

    invoke-static {v0, v2}, Lcom/google/c/a/a/b/h/a/d;->a(Lcom/google/c/a/a/b/h/a/d;Lcom/google/c/a/a/b/h/a/h;)V

    move-object v1, v0

    goto/16 :goto_1

    :cond_a
    invoke-static {v1, v2, p2, v8, v9}, Lcom/google/c/a/a/b/h/a/c;->a(Lcom/google/c/a/a/b/h/a/h;Lcom/google/c/a/a/b/h/a/h;Ljava/lang/String;J)Lcom/google/c/a/a/b/h/a/d;

    move-result-object v1

    goto/16 :goto_1

    :cond_b
    move-object v0, v1

    goto/16 :goto_2
.end method

.method private static a(Lcom/google/c/a/a/b/h/a/h;Lcom/google/c/a/a/b/h/a/h;Ljava/lang/String;J)Lcom/google/c/a/a/b/h/a/d;
    .locals 25

    .prologue
    .line 145
    invoke-virtual/range {p0 .. p0}, Lcom/google/c/a/a/b/h/a/h;->a()I

    move-result v13

    .line 146
    invoke-virtual/range {p1 .. p1}, Lcom/google/c/a/a/b/h/a/h;->a()I

    move-result v14

    .line 147
    add-int v4, v13, v14

    add-int/lit8 v4, v4, 0x1

    div-int/lit8 v15, v4, 0x2

    .line 148
    mul-int/lit8 v16, v15, 0x2

    .line 150
    move/from16 v0, v16

    new-array v0, v0, [I

    move-object/from16 v17, v0

    .line 151
    move/from16 v0, v16

    new-array v0, v0, [I

    move-object/from16 v18, v0

    .line 152
    const/4 v4, 0x0

    :goto_0
    move/from16 v0, v16

    if-ge v4, v0, :cond_0

    .line 153
    const/4 v5, -0x1

    aput v5, v17, v4

    .line 154
    const/4 v5, -0x1

    aput v5, v18, v4

    .line 152
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 156
    :cond_0
    add-int/lit8 v4, v15, 0x1

    const/4 v5, 0x0

    aput v5, v17, v4

    .line 157
    add-int/lit8 v4, v15, 0x1

    const/4 v5, 0x0

    aput v5, v18, v4

    .line 158
    sub-int v19, v13, v14

    .line 161
    rem-int/lit8 v4, v19, 0x2

    if-eqz v4, :cond_3

    const/4 v4, 0x1

    .line 164
    :goto_1
    const/4 v7, 0x0

    .line 165
    const/4 v6, 0x0

    .line 166
    const/4 v9, 0x0

    .line 167
    const/4 v8, 0x0

    .line 168
    const/4 v5, 0x0

    move v12, v5

    :goto_2
    if-ge v12, v15, :cond_f

    .line 170
    rem-int/lit8 v5, v12, 0x64

    if-nez v5, :cond_1

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v10

    cmp-long v5, v10, p3

    if-gtz v5, :cond_f

    .line 171
    :cond_1
    neg-int v5, v12

    add-int/2addr v5, v7

    move v11, v5

    :goto_3
    sub-int v5, v12, v6

    if-gt v11, v5, :cond_8

    .line 176
    add-int v20, v15, v11

    .line 178
    neg-int v5, v12

    if-eq v11, v5, :cond_2

    if-eq v11, v12, :cond_4

    add-int/lit8 v5, v20, -0x1

    aget v5, v17, v5

    add-int/lit8 v10, v20, 0x1

    aget v10, v17, v10

    if-ge v5, v10, :cond_4

    .line 179
    :cond_2
    add-int/lit8 v5, v20, 0x1

    aget v5, v17, v5

    .line 183
    :goto_4
    sub-int v10, v5, v11

    move/from16 v23, v10

    move v10, v5

    move/from16 v5, v23

    .line 184
    :goto_5
    if-ge v10, v13, :cond_5

    if-ge v5, v14, :cond_5

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-virtual {v0, v10, v1, v5}, Lcom/google/c/a/a/b/h/a/h;->a(ILcom/google/c/a/a/b/h/a/h;I)Z

    move-result v21

    if-eqz v21, :cond_5

    .line 185
    add-int/lit8 v10, v10, 0x1

    .line 186
    add-int/lit8 v5, v5, 0x1

    goto :goto_5

    .line 161
    :cond_3
    const/4 v4, 0x0

    goto :goto_1

    .line 181
    :cond_4
    add-int/lit8 v5, v20, -0x1

    aget v5, v17, v5

    add-int/lit8 v5, v5, 0x1

    goto :goto_4

    .line 188
    :cond_5
    aput v10, v17, v20

    .line 189
    if-le v10, v13, :cond_6

    .line 191
    add-int/lit8 v5, v6, 0x2

    move v6, v7

    .line 175
    :goto_6
    add-int/lit8 v7, v11, 0x2

    move v11, v7

    move v7, v6

    move v6, v5

    goto :goto_3

    .line 192
    :cond_6
    if-le v5, v14, :cond_7

    .line 194
    add-int/lit8 v5, v7, 0x2

    move/from16 v23, v6

    move v6, v5

    move/from16 v5, v23

    goto :goto_6

    .line 195
    :cond_7
    if-eqz v4, :cond_11

    .line 196
    add-int v20, v15, v19

    sub-int v20, v20, v11

    .line 197
    if-ltz v20, :cond_11

    move/from16 v0, v20

    move/from16 v1, v16

    if-ge v0, v1, :cond_11

    aget v21, v18, v20

    const/16 v22, -0x1

    move/from16 v0, v21

    move/from16 v1, v22

    if-eq v0, v1, :cond_11

    .line 199
    aget v20, v18, v20

    sub-int v20, v13, v20

    .line 200
    move/from16 v0, v20

    if-lt v10, v0, :cond_11

    .line 202
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    invoke-static {v0, v1, v10, v5, v2}, Lcom/google/c/a/a/b/h/a/c;->a(Lcom/google/c/a/a/b/h/a/h;Lcom/google/c/a/a/b/h/a/h;IILjava/lang/String;)Lcom/google/c/a/a/b/h/a/d;

    move-result-object v4

    .line 250
    :goto_7
    return-object v4

    .line 209
    :cond_8
    neg-int v5, v12

    add-int/2addr v5, v9

    move v11, v5

    :goto_8
    sub-int v5, v12, v8

    if-gt v11, v5, :cond_e

    .line 210
    add-int v20, v15, v11

    .line 212
    neg-int v5, v12

    if-eq v11, v5, :cond_9

    if-eq v11, v12, :cond_a

    add-int/lit8 v5, v20, -0x1

    aget v5, v18, v5

    add-int/lit8 v10, v20, 0x1

    aget v10, v18, v10

    if-ge v5, v10, :cond_a

    .line 213
    :cond_9
    add-int/lit8 v5, v20, 0x1

    aget v5, v18, v5

    .line 217
    :goto_9
    sub-int v10, v5, v11

    move/from16 v23, v10

    move v10, v5

    move/from16 v5, v23

    .line 218
    :goto_a
    if-ge v10, v13, :cond_b

    if-ge v5, v14, :cond_b

    sub-int v21, v13, v10

    add-int/lit8 v21, v21, -0x1

    sub-int v22, v14, v5

    add-int/lit8 v22, v22, -0x1

    move-object/from16 v0, p0

    move/from16 v1, v21

    move-object/from16 v2, p1

    move/from16 v3, v22

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/c/a/a/b/h/a/h;->a(ILcom/google/c/a/a/b/h/a/h;I)Z

    move-result v21

    if-eqz v21, :cond_b

    .line 220
    add-int/lit8 v10, v10, 0x1

    .line 221
    add-int/lit8 v5, v5, 0x1

    goto :goto_a

    .line 215
    :cond_a
    add-int/lit8 v5, v20, -0x1

    aget v5, v18, v5

    add-int/lit8 v5, v5, 0x1

    goto :goto_9

    .line 223
    :cond_b
    aput v10, v18, v20

    .line 224
    if-le v10, v13, :cond_c

    .line 226
    add-int/lit8 v5, v8, 0x2

    move v8, v9

    .line 209
    :goto_b
    add-int/lit8 v9, v11, 0x2

    move v11, v9

    move v9, v8

    move v8, v5

    goto :goto_8

    .line 227
    :cond_c
    if-le v5, v14, :cond_d

    .line 229
    add-int/lit8 v5, v9, 0x2

    move/from16 v23, v8

    move v8, v5

    move/from16 v5, v23

    goto :goto_b

    .line 230
    :cond_d
    if-nez v4, :cond_10

    .line 231
    add-int v5, v15, v19

    sub-int/2addr v5, v11

    .line 232
    if-ltz v5, :cond_10

    move/from16 v0, v16

    if-ge v5, v0, :cond_10

    aget v20, v17, v5

    const/16 v21, -0x1

    move/from16 v0, v20

    move/from16 v1, v21

    if-eq v0, v1, :cond_10

    .line 233
    aget v20, v17, v5

    .line 234
    add-int v21, v15, v20

    sub-int v5, v21, v5

    .line 236
    sub-int v10, v13, v10

    .line 237
    move/from16 v0, v20

    if-lt v0, v10, :cond_10

    .line 239
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, v20

    move-object/from16 v3, p2

    invoke-static {v0, v1, v2, v5, v3}, Lcom/google/c/a/a/b/h/a/c;->a(Lcom/google/c/a/a/b/h/a/h;Lcom/google/c/a/a/b/h/a/h;IILjava/lang/String;)Lcom/google/c/a/a/b/h/a/d;

    move-result-object v4

    goto/16 :goto_7

    .line 168
    :cond_e
    add-int/lit8 v5, v12, 0x1

    move v12, v5

    goto/16 :goto_2

    .line 247
    :cond_f
    new-instance v4, Lcom/google/c/a/a/b/h/a/d;

    move-object/from16 v0, p2

    invoke-direct {v4, v0}, Lcom/google/c/a/a/b/h/a/d;-><init>(Ljava/lang/String;)V

    .line 248
    invoke-virtual {v4, v13}, Lcom/google/c/a/a/b/h/a/d;->b(I)V

    .line 249
    move-object/from16 v0, p1

    invoke-static {v4, v0}, Lcom/google/c/a/a/b/h/a/d;->a(Lcom/google/c/a/a/b/h/a/d;Lcom/google/c/a/a/b/h/a/h;)V

    goto/16 :goto_7

    :cond_10
    move v5, v8

    move v8, v9

    goto :goto_b

    :cond_11
    move v5, v6

    move v6, v7

    goto/16 :goto_6
.end method

.method private static b(Lcom/google/c/a/a/b/h/a/h;Lcom/google/c/a/a/b/h/a/h;)I
    .locals 6

    .prologue
    .line 291
    invoke-virtual {p0}, Lcom/google/c/a/a/b/h/a/h;->a()I

    move-result v2

    .line 292
    invoke-virtual {p1}, Lcom/google/c/a/a/b/h/a/h;->a()I

    move-result v3

    .line 293
    invoke-static {v2, v3}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 294
    const/4 v1, 0x1

    :goto_0
    if-gt v1, v0, :cond_0

    .line 295
    sub-int v4, v2, v1

    sub-int v5, v3, v1

    invoke-virtual {p0, v4, p1, v5}, Lcom/google/c/a/a/b/h/a/h;->a(ILcom/google/c/a/a/b/h/a/h;I)Z

    move-result v4

    if-nez v4, :cond_1

    .line 296
    add-int/lit8 v0, v1, -0x1

    .line 299
    :cond_0
    return v0

    .line 294
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method
