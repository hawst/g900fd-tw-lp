.class final Lcom/google/c/a/a/b/b/a/b/x;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final a:Lcom/google/c/a/a/b/b/a/b/ab;

.field final b:Ljava/util/List;

.field c:Lcom/google/c/a/a/b/b/a/c/p;

.field d:Z

.field private final e:Lcom/google/c/a/a/b/b/a/b/ad;

.field private final f:Lcom/google/c/a/a/b/b/a/k;

.field private g:Z

.field private final h:Ljava/util/List;

.field private i:Ljava/util/Set;

.field private final j:Lcom/google/c/a/a/b/b/a/b/y;


# direct methods
.method public constructor <init>(Lcom/google/c/a/a/b/b/a/b/ab;Lcom/google/c/a/a/b/b/a/b/ad;Lcom/google/c/a/a/b/b/a/o;ZLcom/google/c/a/a/b/b/a/k;)V
    .locals 2

    .prologue
    .line 49
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/c/a/a/b/b/a/b/x;->b:Ljava/util/List;

    .line 40
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/c/a/a/b/b/a/b/x;->h:Ljava/util/List;

    .line 43
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/c/a/a/b/b/a/b/x;->d:Z

    .line 44
    new-instance v0, Ljava/util/IdentityHashMap;

    invoke-direct {v0}, Ljava/util/IdentityHashMap;-><init>()V

    invoke-static {v0}, Ljava/util/Collections;->newSetFromMap(Ljava/util/Map;)Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, Lcom/google/c/a/a/b/b/a/b/x;->i:Ljava/util/Set;

    .line 46
    new-instance v0, Lcom/google/c/a/a/b/b/a/b/y;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/c/a/a/b/b/a/b/y;-><init>(Lcom/google/c/a/a/b/b/a/b/x;B)V

    iput-object v0, p0, Lcom/google/c/a/a/b/b/a/b/x;->j:Lcom/google/c/a/a/b/b/a/b/y;

    .line 50
    iput-object p1, p0, Lcom/google/c/a/a/b/b/a/b/x;->a:Lcom/google/c/a/a/b/b/a/b/ab;

    .line 51
    iput-object p2, p0, Lcom/google/c/a/a/b/b/a/b/x;->e:Lcom/google/c/a/a/b/b/a/b/ad;

    .line 52
    iput-boolean p4, p0, Lcom/google/c/a/a/b/b/a/b/x;->g:Z

    .line 53
    iput-object p5, p0, Lcom/google/c/a/a/b/b/a/b/x;->f:Lcom/google/c/a/a/b/b/a/k;

    .line 54
    iget-object v0, p0, Lcom/google/c/a/a/b/b/a/b/x;->h:Ljava/util/List;

    invoke-interface {v0, p3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 55
    return-void
.end method

.method private a(Ljava/lang/String;Lcom/google/c/a/a/b/b/a/c/p;Lcom/google/c/a/a/b/b/a/o;)V
    .locals 7

    .prologue
    .line 265
    iget-object v0, p0, Lcom/google/c/a/a/b/b/a/b/x;->a:Lcom/google/c/a/a/b/b/a/b/ab;

    invoke-virtual {v0, p1}, Lcom/google/c/a/a/b/b/a/b/ab;->b(Ljava/lang/String;)Ljava/util/Map;

    move-result-object v0

    .line 266
    if-eqz v0, :cond_0

    .line 269
    new-instance v1, Ljava/util/ArrayList;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 270
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/c/a/a/b/b/a/b/r;

    .line 271
    iget-object v1, p0, Lcom/google/c/a/a/b/b/a/b/x;->i:Ljava/util/Set;

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 272
    iget-boolean v3, p0, Lcom/google/c/a/a/b/b/a/b/x;->g:Z

    iget-object v4, p0, Lcom/google/c/a/a/b/b/a/b/x;->j:Lcom/google/c/a/a/b/b/a/b/y;

    iget-object v5, p0, Lcom/google/c/a/a/b/b/a/b/x;->f:Lcom/google/c/a/a/b/b/a/k;

    move-object v1, p2

    move-object v2, p3

    invoke-virtual/range {v0 .. v5}, Lcom/google/c/a/a/b/b/a/b/r;->a(Lcom/google/c/a/a/b/b/a/c/p;Lcom/google/c/a/a/b/b/a/o;ZLcom/google/c/a/a/b/b/a/b/y;Lcom/google/c/a/a/b/b/a/k;)V

    goto :goto_0

    .line 275
    :cond_0
    return-void
.end method

.method private b(Lcom/google/c/a/a/b/b/a/c/p;)V
    .locals 12

    .prologue
    const/4 v9, 0x0

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 162
    move-object v0, p1

    :goto_0
    instance-of v1, v0, Lcom/google/c/a/a/b/b/a/c/n;

    if-eqz v1, :cond_3

    .line 163
    check-cast v0, Lcom/google/c/a/a/b/b/a/c/n;

    invoke-virtual {v0}, Lcom/google/c/a/a/b/b/a/c/n;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/c/a/a/b/b/a/b/x;->a(Ljava/lang/String;)V

    iget-boolean v1, p0, Lcom/google/c/a/a/b/b/a/b/x;->g:Z

    invoke-virtual {v0}, Lcom/google/c/a/a/b/b/a/c/n;->e()Z

    move-result v2

    and-int/2addr v2, v1

    iput-boolean v2, p0, Lcom/google/c/a/a/b/b/a/b/x;->g:Z

    invoke-virtual {v0}, Lcom/google/c/a/a/b/b/a/c/n;->c()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/c/b/a/c;

    instance-of v3, v0, Lcom/google/c/a/a/b/b/a/c/p;

    if-eqz v3, :cond_0

    check-cast v0, Lcom/google/c/a/a/b/b/a/c/p;

    invoke-direct {p0, v0}, Lcom/google/c/a/a/b/b/a/b/x;->b(Lcom/google/c/a/a/b/b/a/c/p;)V

    goto :goto_1

    :cond_1
    iput-boolean v1, p0, Lcom/google/c/a/a/b/b/a/b/x;->g:Z

    invoke-virtual {p0}, Lcom/google/c/a/a/b/b/a/b/x;->c()V

    .line 182
    :cond_2
    :goto_2
    return-void

    .line 167
    :cond_3
    iget-boolean v1, p0, Lcom/google/c/a/a/b/b/a/b/x;->d:Z

    if-eqz v1, :cond_6

    instance-of v1, v0, Lcom/google/c/a/a/b/b/a/c/e;

    if-nez v1, :cond_6

    instance-of v1, v0, Lcom/google/c/a/a/b/b/a/c/g;

    if-nez v1, :cond_6

    invoke-direct {p0}, Lcom/google/c/a/a/b/b/a/b/x;->d()Lcom/google/c/a/a/b/b/a/o;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/c/a/a/b/b/a/o;->a()Z

    move-result v1

    if-eqz v1, :cond_6

    move v1, v2

    .line 171
    :goto_3
    iget-object v6, p0, Lcom/google/c/a/a/b/b/a/b/x;->e:Lcom/google/c/a/a/b/b/a/b/ad;

    iput-object v9, v6, Lcom/google/c/a/a/b/b/a/b/ad;->g:Lcom/google/c/b/a/c;

    iput-object v9, v6, Lcom/google/c/a/a/b/b/a/b/ad;->h:Lcom/google/c/b/a/c;

    if-nez v1, :cond_9

    iget-object v1, v6, Lcom/google/c/a/a/b/b/a/b/ad;->b:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_4

    iget-object v1, v6, Lcom/google/c/a/a/b/b/a/b/ad;->c:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_5

    :cond_4
    iget-boolean v1, v6, Lcom/google/c/a/a/b/b/a/b/ad;->f:Z

    if-eqz v1, :cond_7

    .line 173
    :cond_5
    :goto_4
    instance-of v1, v0, Lcom/google/c/a/a/b/b/a/c/e;

    if-eqz v1, :cond_10

    .line 174
    check-cast v0, Lcom/google/c/a/a/b/b/a/c/e;

    invoke-virtual {v0}, Lcom/google/c/a/a/b/b/a/c/e;->a()Ljava/lang/String;

    move-result-object v4

    iget-object v1, p0, Lcom/google/c/a/a/b/b/a/b/x;->a:Lcom/google/c/a/a/b/b/a/b/ab;

    invoke-virtual {v1, v4}, Lcom/google/c/a/a/b/b/a/b/ab;->a(Ljava/lang/String;)Lcom/google/c/a/a/b/b/a/b/a;

    move-result-object v1

    if-nez v1, :cond_f

    iget-object v5, p0, Lcom/google/c/a/a/b/b/a/b/x;->a:Lcom/google/c/a/a/b/b/a/b/ab;

    invoke-virtual {v0}, Lcom/google/c/a/a/b/b/a/c/e;->c()Ljava/lang/String;

    move-result-object v6

    iget-object v1, v5, Lcom/google/c/a/a/b/b/a/b/ab;->a:Ljava/util/Map;

    invoke-interface {v1, v4}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_e

    iget-object v1, v5, Lcom/google/c/a/a/b/b/a/b/ab;->b:Ljava/util/Map;

    invoke-interface {v1, v4}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_e

    move v1, v2

    :goto_5
    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "Object with ID "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " already exists."

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v1, v7}, Lcom/google/c/a/a/b/h/c;->b(ZLjava/lang/String;)V

    iget-object v1, v5, Lcom/google/c/a/a/b/b/a/b/ab;->e:Lcom/google/c/a/a/b/b/a/b/aa;

    invoke-virtual {v1, v6, v4}, Lcom/google/c/a/a/b/b/a/b/aa;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/c/a/a/b/b/a/b/a;

    move-result-object v1

    iget-object v6, v5, Lcom/google/c/a/a/b/b/a/b/ab;->a:Ljava/util/Map;

    invoke-interface {v6, v4, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget v4, v5, Lcom/google/c/a/a/b/b/a/b/ab;->g:I

    invoke-virtual {v1}, Lcom/google/c/a/a/b/b/a/b/a;->l()I

    move-result v1

    add-int/2addr v1, v4

    iput v1, v5, Lcom/google/c/a/a/b/b/a/b/ab;->g:I

    :goto_6
    invoke-virtual {v0}, Lcom/google/c/a/a/b/b/a/c/e;->d()Lcom/google/c/a/a/b/b/a/c/n;

    move-result-object v1

    if-eqz v1, :cond_2

    invoke-virtual {v0}, Lcom/google/c/a/a/b/b/a/c/e;->d()Lcom/google/c/a/a/b/b/a/c/n;

    move-result-object v0

    goto/16 :goto_0

    :cond_6
    move v1, v3

    .line 167
    goto :goto_3

    .line 171
    :cond_7
    new-instance v1, Lcom/google/c/a/a/b/b/a/b/k;

    invoke-direct {v1, v0}, Lcom/google/c/a/a/b/b/a/b/k;-><init>(Lcom/google/c/a/a/b/b/a/c/p;)V

    iget-object v4, v6, Lcom/google/c/a/a/b/b/a/b/ad;->b:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_8

    iput-object v1, v6, Lcom/google/c/a/a/b/b/a/b/ad;->g:Lcom/google/c/b/a/c;

    iget-object v4, v6, Lcom/google/c/a/a/b/b/a/b/ad;->b:Ljava/util/List;

    invoke-interface {v4, v3, v1}, Ljava/util/List;->add(ILjava/lang/Object;)V

    :cond_8
    iget-object v4, v6, Lcom/google/c/a/a/b/b/a/b/ad;->c:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_5

    iput-object v1, v6, Lcom/google/c/a/a/b/b/a/b/ad;->h:Lcom/google/c/b/a/c;

    iget-object v4, v6, Lcom/google/c/a/a/b/b/a/b/ad;->c:Ljava/util/List;

    invoke-interface {v4, v3, v1}, Ljava/util/List;->add(ILjava/lang/Object;)V

    goto/16 :goto_4

    :cond_9
    instance-of v1, v0, Lcom/google/c/a/a/b/b/a/c/r;

    if-eqz v1, :cond_a

    move-object v1, v0

    check-cast v1, Lcom/google/c/a/a/b/b/a/c/r;

    iget-object v4, v6, Lcom/google/c/a/a/b/b/a/b/ad;->a:Lcom/google/c/a/a/b/b/a/b;

    invoke-interface {v1}, Lcom/google/c/a/a/b/b/a/c/r;->e()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v4, v5}, Lcom/google/c/a/a/b/b/a/b;->a(Ljava/lang/String;)Lcom/google/c/a/a/b/b/a/g;

    move-result-object v4

    invoke-interface {v4, v1}, Lcom/google/c/a/a/b/b/a/g;->a(Lcom/google/c/a/a/b/b/a/c/r;)Lcom/google/c/a/a/b/b/a/c/r;

    move-result-object v1

    :goto_7
    iget-object v4, v6, Lcom/google/c/a/a/b/b/a/b/ad;->k:Ljava/util/List;

    invoke-interface {v4, v3, v1}, Ljava/util/List;->add(ILjava/lang/Object;)V

    goto/16 :goto_4

    :cond_a
    instance-of v1, v0, Lcom/google/c/a/a/b/b/a/c/m;

    if-eqz v1, :cond_d

    move-object v1, v0

    check-cast v1, Lcom/google/c/a/a/b/b/a/c/m;

    invoke-virtual {v1}, Lcom/google/c/a/a/b/b/a/c/m;->d()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v1}, Lcom/google/c/a/a/b/b/a/c/m;->e()I

    move-result v4

    invoke-virtual {v1}, Lcom/google/c/a/a/b/b/a/c/m;->a()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v1}, Lcom/google/c/a/a/b/b/a/c/m;->c()I

    move-result v1

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_b

    if-le v4, v1, :cond_c

    add-int/lit8 v4, v4, -0x1

    :cond_b
    :goto_8
    new-instance v5, Lcom/google/c/a/a/b/b/a/c/m;

    invoke-direct {v5, v7, v4, v8, v1}, Lcom/google/c/a/a/b/b/a/c/m;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    move-object v1, v5

    goto :goto_7

    :cond_c
    if-ge v4, v1, :cond_b

    add-int/lit8 v1, v1, 0x1

    goto :goto_8

    :cond_d
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Cannot invert mutation with illegal type "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_e
    move v1, v3

    .line 174
    goto/16 :goto_5

    :cond_f
    invoke-virtual {v1}, Lcom/google/c/a/a/b/b/a/b/a;->i()Lcom/google/c/a/a/b/b/a/c/n;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/google/c/a/a/b/b/a/b/x;->b(Lcom/google/c/a/a/b/b/a/c/p;)V

    goto/16 :goto_6

    .line 175
    :cond_10
    instance-of v1, v0, Lcom/google/c/a/a/b/b/a/c/g;

    if-eqz v1, :cond_11

    .line 176
    iget-object v1, p0, Lcom/google/c/a/a/b/b/a/b/x;->a:Lcom/google/c/a/a/b/b/a/b/ab;

    check-cast v0, Lcom/google/c/a/a/b/b/a/c/g;

    invoke-virtual {v0}, Lcom/google/c/a/a/b/b/a/c/g;->a()Ljava/lang/String;

    move-result-object v0

    iget-object v2, v1, Lcom/google/c/a/a/b/b/a/b/ab;->b:Ljava/util/Map;

    iget-object v1, v1, Lcom/google/c/a/a/b/b/a/b/ab;->a:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-interface {v2, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_2

    .line 177
    :cond_11
    instance-of v1, v0, Lcom/google/c/a/a/b/b/a/c/r;

    if-eqz v1, :cond_13

    move-object v1, v0

    .line 178
    check-cast v1, Lcom/google/c/a/a/b/b/a/c/r;

    invoke-interface {v1}, Lcom/google/c/a/a/b/b/a/c/r;->e()Ljava/lang/String;

    move-result-object v6

    iget-object v0, p0, Lcom/google/c/a/a/b/b/a/b/x;->a:Lcom/google/c/a/a/b/b/a/b/ab;

    invoke-interface {v1}, Lcom/google/c/a/a/b/b/a/c/r;->e()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/c/a/a/b/b/a/b/ab;->a(Ljava/lang/String;)Lcom/google/c/a/a/b/b/a/b/a;

    move-result-object v0

    iget-object v2, p0, Lcom/google/c/a/a/b/b/a/b/x;->i:Ljava/util/Set;

    invoke-interface {v2, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    invoke-virtual {v0}, Lcom/google/c/a/a/b/b/a/b/a;->l()I

    move-result v7

    invoke-direct {p0}, Lcom/google/c/a/a/b/b/a/b/x;->d()Lcom/google/c/a/a/b/b/a/o;

    move-result-object v2

    iget-boolean v3, p0, Lcom/google/c/a/a/b/b/a/b/x;->g:Z

    iget-object v4, p0, Lcom/google/c/a/a/b/b/a/b/x;->j:Lcom/google/c/a/a/b/b/a/b/y;

    iget-object v5, p0, Lcom/google/c/a/a/b/b/a/b/x;->f:Lcom/google/c/a/a/b/b/a/k;

    invoke-virtual/range {v0 .. v5}, Lcom/google/c/a/a/b/b/a/b/a;->a(Lcom/google/c/a/a/b/b/a/c/p;Lcom/google/c/a/a/b/b/a/o;ZLcom/google/c/a/a/b/b/a/b/y;Lcom/google/c/a/a/b/b/a/k;)V

    invoke-virtual {v0}, Lcom/google/c/a/a/b/b/a/b/a;->l()I

    move-result v0

    invoke-direct {p0}, Lcom/google/c/a/a/b/b/a/b/x;->d()Lcom/google/c/a/a/b/b/a/o;

    move-result-object v2

    invoke-direct {p0, v6, v1, v2}, Lcom/google/c/a/a/b/b/a/b/x;->a(Ljava/lang/String;Lcom/google/c/a/a/b/b/a/c/p;Lcom/google/c/a/a/b/b/a/o;)V

    iget-object v2, p0, Lcom/google/c/a/a/b/b/a/b/x;->a:Lcom/google/c/a/a/b/b/a/b/ab;

    sub-int/2addr v0, v7

    iget-boolean v3, v2, Lcom/google/c/a/a/b/b/a/b/ab;->f:Z

    if-nez v3, :cond_12

    iget-object v3, v2, Lcom/google/c/a/a/b/b/a/b/ab;->d:Ljava/util/Set;

    invoke-interface {v1}, Lcom/google/c/a/a/b/b/a/c/r;->e()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_12

    iget v3, v2, Lcom/google/c/a/a/b/b/a/b/ab;->h:I

    add-int/2addr v3, v0

    iput v3, v2, Lcom/google/c/a/a/b/b/a/b/ab;->h:I

    invoke-virtual {v2, v1}, Lcom/google/c/a/a/b/b/a/b/ab;->a(Lcom/google/c/a/a/b/b/a/c/p;)V

    :cond_12
    iget v1, v2, Lcom/google/c/a/a/b/b/a/b/ab;->g:I

    add-int/2addr v0, v1

    iput v0, v2, Lcom/google/c/a/a/b/b/a/b/ab;->g:I

    goto/16 :goto_2

    .line 179
    :cond_13
    instance-of v1, v0, Lcom/google/c/a/a/b/b/a/c/m;

    if-eqz v1, :cond_2

    move-object v1, v0

    .line 180
    check-cast v1, Lcom/google/c/a/a/b/b/a/c/m;

    invoke-direct {p0}, Lcom/google/c/a/a/b/b/a/b/x;->d()Lcom/google/c/a/a/b/b/a/o;

    move-result-object v2

    invoke-virtual {v1}, Lcom/google/c/a/a/b/b/a/c/m;->a()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v1}, Lcom/google/c/a/a/b/b/a/c/m;->d()Ljava/lang/String;

    move-result-object v11

    iget-object v0, p0, Lcom/google/c/a/a/b/b/a/b/x;->a:Lcom/google/c/a/a/b/b/a/b/ab;

    invoke-virtual {v0, v10}, Lcom/google/c/a/a/b/b/a/b/ab;->a(Ljava/lang/String;)Lcom/google/c/a/a/b/b/a/b/a;

    move-result-object v0

    move-object v9, v0

    check-cast v9, Lcom/google/c/a/a/b/b/a/b/h;

    iget-object v0, p0, Lcom/google/c/a/a/b/b/a/b/x;->a:Lcom/google/c/a/a/b/b/a/b/ab;

    invoke-virtual {v0, v11}, Lcom/google/c/a/a/b/b/a/b/ab;->a(Ljava/lang/String;)Lcom/google/c/a/a/b/b/a/b/a;

    move-result-object v0

    check-cast v0, Lcom/google/c/a/a/b/b/a/b/h;

    iget-object v3, p0, Lcom/google/c/a/a/b/b/a/b/x;->i:Ljava/util/Set;

    invoke-interface {v3, v9}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    iget-object v3, p0, Lcom/google/c/a/a/b/b/a/b/x;->i:Ljava/util/Set;

    invoke-interface {v3, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    iget-boolean v3, p0, Lcom/google/c/a/a/b/b/a/b/x;->g:Z

    iget-object v4, p0, Lcom/google/c/a/a/b/b/a/b/x;->j:Lcom/google/c/a/a/b/b/a/b/y;

    iget-object v5, p0, Lcom/google/c/a/a/b/b/a/b/x;->f:Lcom/google/c/a/a/b/b/a/k;

    invoke-virtual/range {v0 .. v5}, Lcom/google/c/a/a/b/b/a/b/h;->a(Lcom/google/c/a/a/b/b/a/c/m;Lcom/google/c/a/a/b/b/a/o;ZLcom/google/c/a/a/b/b/a/b/y;Lcom/google/c/a/a/b/b/a/k;)V

    iget-boolean v6, p0, Lcom/google/c/a/a/b/b/a/b/x;->g:Z

    iget-object v7, p0, Lcom/google/c/a/a/b/b/a/b/x;->j:Lcom/google/c/a/a/b/b/a/b/y;

    iget-object v8, p0, Lcom/google/c/a/a/b/b/a/b/x;->f:Lcom/google/c/a/a/b/b/a/k;

    move-object v3, v9

    move-object v4, v1

    move-object v5, v2

    invoke-virtual/range {v3 .. v8}, Lcom/google/c/a/a/b/b/a/b/h;->b(Lcom/google/c/a/a/b/b/a/c/m;Lcom/google/c/a/a/b/b/a/o;ZLcom/google/c/a/a/b/b/a/b/y;Lcom/google/c/a/a/b/b/a/k;)V

    invoke-virtual {v11, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_14

    invoke-direct {p0, v11, v1, v2}, Lcom/google/c/a/a/b/b/a/b/x;->a(Ljava/lang/String;Lcom/google/c/a/a/b/b/a/c/p;Lcom/google/c/a/a/b/b/a/o;)V

    :cond_14
    invoke-direct {p0, v10, v1, v2}, Lcom/google/c/a/a/b/b/a/b/x;->a(Ljava/lang/String;Lcom/google/c/a/a/b/b/a/c/p;Lcom/google/c/a/a/b/b/a/o;)V

    if-eq v9, v0, :cond_2

    iget-object v0, p0, Lcom/google/c/a/a/b/b/a/b/x;->a:Lcom/google/c/a/a/b/b/a/b/ab;

    iget-boolean v1, v0, Lcom/google/c/a/a/b/b/a/b/ab;->f:Z

    if-eqz v1, :cond_15

    invoke-virtual {v0}, Lcom/google/c/a/a/b/b/a/b/ab;->a()V

    :cond_15
    iget-object v0, v0, Lcom/google/c/a/a/b/b/a/b/ab;->d:Ljava/util/Set;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0, v10}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    invoke-interface {v0, v11}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eq v1, v0, :cond_2

    iget-object v0, p0, Lcom/google/c/a/a/b/b/a/b/x;->j:Lcom/google/c/a/a/b/b/a/b/y;

    invoke-virtual {v0}, Lcom/google/c/a/a/b/b/a/b/y;->a()V

    goto/16 :goto_2
.end method

.method private d()Lcom/google/c/a/a/b/b/a/o;
    .locals 2

    .prologue
    .line 292
    iget-object v0, p0, Lcom/google/c/a/a/b/b/a/b/x;->h:Ljava/util/List;

    iget-object v1, p0, Lcom/google/c/a/a/b/b/a/b/x;->h:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/c/a/a/b/b/a/o;

    return-object v0
.end method


# virtual methods
.method public final a(Lcom/google/c/a/a/b/b/a/c/p;)Lcom/google/c/a/a/b/b/a/a;
    .locals 2

    .prologue
    .line 62
    iget-object v0, p0, Lcom/google/c/a/a/b/b/a/b/x;->c:Lcom/google/c/a/a/b/b/a/c/p;

    if-eqz v0, :cond_0

    .line 63
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Cannot apply multiple local mutations while not in a compound operation."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 67
    :cond_0
    new-instance v0, Ljava/util/IdentityHashMap;

    invoke-direct {v0}, Ljava/util/IdentityHashMap;-><init>()V

    invoke-static {v0}, Ljava/util/Collections;->newSetFromMap(Ljava/util/Map;)Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, Lcom/google/c/a/a/b/b/a/b/x;->i:Ljava/util/Set;

    .line 69
    invoke-direct {p0, p1}, Lcom/google/c/a/a/b/b/a/b/x;->b(Lcom/google/c/a/a/b/b/a/c/p;)V

    .line 71
    invoke-direct {p0}, Lcom/google/c/a/a/b/b/a/b/x;->d()Lcom/google/c/a/a/b/b/a/o;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/c/a/a/b/b/a/o;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 72
    invoke-virtual {p0}, Lcom/google/c/a/a/b/b/a/b/x;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 73
    iget-object v0, p0, Lcom/google/c/a/a/b/b/a/b/x;->b:Ljava/util/List;

    iget-object v1, p0, Lcom/google/c/a/a/b/b/a/b/x;->b:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/c/a/a/b/b/a/c/o;

    invoke-virtual {v0, p1}, Lcom/google/c/a/a/b/b/a/c/o;->a(Lcom/google/c/b/a/c;)Lcom/google/c/a/a/b/b/a/c/o;

    .line 79
    :cond_1
    :goto_0
    invoke-virtual {p0}, Lcom/google/c/a/a/b/b/a/b/x;->a()Z

    move-result v0

    if-nez v0, :cond_3

    .line 80
    invoke-virtual {p0}, Lcom/google/c/a/a/b/b/a/b/x;->b()Lcom/google/c/a/a/b/b/a/a;

    move-result-object v0

    .line 82
    :goto_1
    return-object v0

    .line 75
    :cond_2
    iput-object p1, p0, Lcom/google/c/a/a/b/b/a/b/x;->c:Lcom/google/c/a/a/b/b/a/c/p;

    goto :goto_0

    .line 82
    :cond_3
    iget-object v0, p0, Lcom/google/c/a/a/b/b/a/b/x;->i:Ljava/util/Set;

    invoke-static {v0}, Lcom/google/c/a/a/b/b/a/a;->a(Ljava/util/Set;)Lcom/google/c/a/a/b/b/a/a;

    move-result-object v0

    goto :goto_1
.end method

.method final a(Ljava/lang/String;)V
    .locals 6

    .prologue
    .line 278
    invoke-direct {p0}, Lcom/google/c/a/a/b/b/a/b/x;->d()Lcom/google/c/a/a/b/b/a/o;

    move-result-object v0

    .line 279
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 280
    invoke-virtual {v0}, Lcom/google/c/a/a/b/b/a/o;->d()Ljava/util/List;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 281
    invoke-interface {v1, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 282
    iget-object v2, p0, Lcom/google/c/a/a/b/b/a/b/x;->h:Ljava/util/List;

    new-instance v3, Lcom/google/c/a/a/b/b/a/o;

    invoke-virtual {v0}, Lcom/google/c/a/a/b/b/a/o;->b()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0}, Lcom/google/c/a/a/b/b/a/o;->c()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0}, Lcom/google/c/a/a/b/b/a/o;->a()Z

    move-result v0

    invoke-direct {v3, v4, v5, v1, v0}, Lcom/google/c/a/a/b/b/a/o;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Z)V

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 284
    return-void
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 151
    iget-object v0, p0, Lcom/google/c/a/a/b/b/a/b/x;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method final b()Lcom/google/c/a/a/b/b/a/a;
    .locals 8

    .prologue
    const/4 v0, 0x1

    const/4 v6, 0x0

    .line 155
    iget-object v7, p0, Lcom/google/c/a/a/b/b/a/b/x;->e:Lcom/google/c/a/a/b/b/a/b/ad;

    iget-object v1, v7, Lcom/google/c/a/a/b/b/a/b/ad;->k:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_8

    iget-object v1, v7, Lcom/google/c/a/a/b/b/a/b/ad;->k:Ljava/util/List;

    const-string v2, ""

    invoke-static {v1, v2}, Lcom/google/c/a/a/b/b/a/c/q;->a(Ljava/util/List;Ljava/lang/String;)Lcom/google/c/b/a/c;

    move-result-object v1

    iget-boolean v2, v7, Lcom/google/c/a/a/b/b/a/b/ad;->d:Z

    if-eqz v2, :cond_2

    invoke-virtual {v7}, Lcom/google/c/a/a/b/b/a/b/ad;->b()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v7}, Lcom/google/c/a/a/b/b/a/b/ad;->a()Z

    move-result v2

    if-nez v2, :cond_1

    :cond_0
    :goto_0
    iget-object v2, v7, Lcom/google/c/a/a/b/b/a/b/ad;->c:Ljava/util/List;

    invoke-interface {v2, v6, v1}, Ljava/util/List;->add(ILjava/lang/Object;)V

    iput-object v1, v7, Lcom/google/c/a/a/b/b/a/b/ad;->h:Lcom/google/c/b/a/c;

    :goto_1
    new-instance v1, Ljava/util/LinkedList;

    invoke-direct {v1}, Ljava/util/LinkedList;-><init>()V

    iput-object v1, v7, Lcom/google/c/a/a/b/b/a/b/ad;->k:Ljava/util/List;

    move v1, v0

    :goto_2
    new-instance v0, Lcom/google/c/a/a/b/b/a/q;

    iget v2, v7, Lcom/google/c/a/a/b/b/a/b/ad;->i:I

    iget-object v3, v7, Lcom/google/c/a/a/b/b/a/b/ad;->g:Lcom/google/c/b/a/c;

    iget v4, v7, Lcom/google/c/a/a/b/b/a/b/ad;->j:I

    iget-object v5, v7, Lcom/google/c/a/a/b/b/a/b/ad;->h:Lcom/google/c/b/a/c;

    invoke-direct/range {v0 .. v5}, Lcom/google/c/a/a/b/b/a/q;-><init>(ZILcom/google/c/b/a/c;ILcom/google/c/b/a/c;)V

    iput v6, v7, Lcom/google/c/a/a/b/b/a/b/ad;->i:I

    iput v6, v7, Lcom/google/c/a/a/b/b/a/b/ad;->j:I

    .line 156
    new-instance v1, Lcom/google/c/a/a/b/b/a/a;

    iget-object v2, p0, Lcom/google/c/a/a/b/b/a/b/x;->j:Lcom/google/c/a/a/b/b/a/b/y;

    iget-object v2, v2, Lcom/google/c/a/a/b/b/a/b/y;->a:Ljava/util/List;

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    iget-object v3, p0, Lcom/google/c/a/a/b/b/a/b/x;->j:Lcom/google/c/a/a/b/b/a/b/y;

    invoke-virtual {v3}, Lcom/google/c/a/a/b/b/a/b/y;->b()Ljava/util/Collection;

    move-result-object v3

    iget-object v4, p0, Lcom/google/c/a/a/b/b/a/b/x;->i:Ljava/util/Set;

    invoke-static {v4}, Ljava/util/Collections;->unmodifiableSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v4

    iget-object v5, p0, Lcom/google/c/a/a/b/b/a/b/x;->c:Lcom/google/c/a/a/b/b/a/c/p;

    move-object v6, v0

    invoke-direct/range {v1 .. v6}, Lcom/google/c/a/a/b/b/a/a;-><init>(Ljava/util/List;Ljava/util/Collection;Ljava/util/Set;Lcom/google/c/a/a/b/b/a/c/p;Lcom/google/c/a/a/b/b/a/q;)V

    return-object v1

    :cond_1
    move v0, v6

    .line 155
    goto :goto_0

    :cond_2
    iget-boolean v2, v7, Lcom/google/c/a/a/b/b/a/b/ad;->e:Z

    if-eqz v2, :cond_5

    invoke-virtual {v7}, Lcom/google/c/a/a/b/b/a/b/ad;->a()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-virtual {v7}, Lcom/google/c/a/a/b/b/a/b/ad;->b()Z

    move-result v2

    if-nez v2, :cond_4

    :cond_3
    :goto_3
    iget-object v2, v7, Lcom/google/c/a/a/b/b/a/b/ad;->b:Ljava/util/List;

    invoke-interface {v2, v6, v1}, Ljava/util/List;->add(ILjava/lang/Object;)V

    iput-object v1, v7, Lcom/google/c/a/a/b/b/a/b/ad;->g:Lcom/google/c/b/a/c;

    goto :goto_1

    :cond_4
    move v0, v6

    goto :goto_3

    :cond_5
    invoke-virtual {v7}, Lcom/google/c/a/a/b/b/a/b/ad;->a()Z

    move-result v2

    if-eqz v2, :cond_6

    invoke-virtual {v7}, Lcom/google/c/a/a/b/b/a/b/ad;->b()Z

    move-result v2

    if-eqz v2, :cond_7

    :cond_6
    :goto_4
    iget-object v2, v7, Lcom/google/c/a/a/b/b/a/b/ad;->c:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->clear()V

    goto :goto_3

    :cond_7
    move v0, v6

    goto :goto_4

    :cond_8
    move v1, v6

    goto :goto_2
.end method

.method final c()V
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 287
    iget-object v1, p0, Lcom/google/c/a/a/b/b/a/b/x;->h:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-le v1, v0, :cond_0

    :goto_0
    const-string v1, "Cannot pop root compound operation name."

    invoke-static {v0, v1}, Lcom/google/c/a/a/b/h/c;->b(ZLjava/lang/String;)V

    .line 288
    iget-object v0, p0, Lcom/google/c/a/a/b/b/a/b/x;->h:Ljava/util/List;

    iget-object v1, p0, Lcom/google/c/a/a/b/b/a/b/x;->h:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-interface {v0, v1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 289
    return-void

    .line 287
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
