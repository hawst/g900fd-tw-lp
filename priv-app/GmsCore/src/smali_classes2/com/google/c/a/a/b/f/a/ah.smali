.class public final Lcom/google/c/a/a/b/f/a/ah;
.super Lcom/google/c/a/a/b/f/c;
.source "SourceFile"


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 18
    const-class v0, Lcom/google/c/a/a/b/b/a/n;

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Lcom/google/c/a/a/b/f/c;-><init>(Ljava/lang/Class;Z)V

    .line 19
    return-void
.end method

.method private static a(Lcom/google/c/a/a/b/b/a/n;Lcom/google/c/a/a/b/d/b;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 24
    :try_start_0
    invoke-virtual {p0}, Lcom/google/c/a/a/b/b/a/n;->a()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/google/c/a/a/b/d/b;->b(Ljava/lang/String;)Ljava/lang/Object;
    :try_end_0
    .catch Lcom/google/c/a/a/b/d/a; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    return-object v0

    .line 26
    :catch_0
    move-exception v0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "cannot serialize invalid JSON value: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/c/a/a/b/b/a/n;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private static f(Ljava/lang/Object;Lcom/google/c/a/a/b/d/b;)Lcom/google/c/a/a/b/b/a/n;
    .locals 3

    .prologue
    .line 34
    :try_start_0
    invoke-interface {p1, p0}, Lcom/google/c/a/a/b/d/b;->d(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/c/a/a/b/b/a/n;->a(Ljava/lang/String;)Lcom/google/c/a/a/b/b/a/n;
    :try_end_0
    .catch Lcom/google/c/a/a/b/d/a; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    return-object v0

    .line 36
    :catch_0
    move-exception v0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "cannot deserialize invalid JSON value: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method


# virtual methods
.method protected final synthetic c(Ljava/lang/Object;Lcom/google/c/a/a/b/d/b;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 16
    check-cast p1, Lcom/google/c/a/a/b/b/a/n;

    invoke-static {p1, p2}, Lcom/google/c/a/a/b/f/a/ah;->a(Lcom/google/c/a/a/b/b/a/n;Lcom/google/c/a/a/b/d/b;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method protected final synthetic d(Ljava/lang/Object;Lcom/google/c/a/a/b/d/b;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 16
    invoke-static {p1, p2}, Lcom/google/c/a/a/b/f/a/ah;->f(Ljava/lang/Object;Lcom/google/c/a/a/b/d/b;)Lcom/google/c/a/a/b/b/a/n;

    move-result-object v0

    return-object v0
.end method
