.class final Lcom/google/c/a/a/b/b/a/b/r;
.super Lcom/google/c/a/a/b/b/a/b/a;
.source "SourceFile"

# interfaces
.implements Lcom/google/c/a/a/b/b/a/l;
.implements Ljava/lang/Comparable;


# instance fields
.field private b:I

.field private c:Ljava/lang/String;

.field private d:Z


# direct methods
.method public constructor <init>(Lcom/google/c/a/a/b/b/a/b/w;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 48
    invoke-direct {p0, p1, p2}, Lcom/google/c/a/a/b/b/a/b/a;-><init>(Lcom/google/c/a/a/b/b/a/b/w;Ljava/lang/String;)V

    .line 49
    return-void
.end method

.method private a(ILcom/google/c/a/a/b/b/a/o;ZLjava/lang/String;Lcom/google/c/a/a/b/b/a/b/y;Lcom/google/c/a/a/b/b/a/k;)V
    .locals 12

    .prologue
    .line 186
    iget v1, p0, Lcom/google/c/a/a/b/b/a/b/r;->b:I

    if-ne v1, p1, :cond_1

    iget-object v1, p0, Lcom/google/c/a/a/b/b/a/b/r;->c:Ljava/lang/String;

    move-object/from16 v0, p4

    invoke-static {v1, v0}, Lcom/google/c/a/a/b/h/b;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 213
    :cond_0
    :goto_0
    return-void

    .line 189
    :cond_1
    iget-object v11, p0, Lcom/google/c/a/a/b/b/a/b/r;->c:Ljava/lang/String;

    .line 190
    iget v10, p0, Lcom/google/c/a/a/b/b/a/b/r;->b:I

    .line 191
    iput p1, p0, Lcom/google/c/a/a/b/b/a/b/r;->b:I

    .line 192
    const/4 v1, 0x0

    .line 193
    iget-object v2, p0, Lcom/google/c/a/a/b/b/a/b/r;->c:Ljava/lang/String;

    move-object/from16 v0, p4

    invoke-static {v0, v2}, Lcom/google/c/a/a/b/h/b;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_7

    .line 194
    iget-object v2, p0, Lcom/google/c/a/a/b/b/a/b/r;->c:Ljava/lang/String;

    if-eqz v2, :cond_6

    .line 195
    invoke-virtual {p0}, Lcom/google/c/a/a/b/b/a/b/r;->j()Lcom/google/c/a/a/b/b/a/b;

    move-result-object v1

    iget-object v2, p0, Lcom/google/c/a/a/b/b/a/b/r;->c:Ljava/lang/String;

    invoke-interface {v1, v2}, Lcom/google/c/a/a/b/b/a/b;->a(Ljava/lang/String;)Lcom/google/c/a/a/b/b/a/g;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/c/a/a/b/b/a/g;->e()Lcom/google/c/a/a/b/b/a/p;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/c/a/a/b/b/a/b/r;->b(Lcom/google/c/a/a/b/b/a/r;)Z

    move-result v1

    move v2, v1

    .line 197
    :goto_1
    iget-object v1, p0, Lcom/google/c/a/a/b/b/a/b/r;->d_:Lcom/google/c/a/a/b/b/a/b/w;

    iget-object v1, v1, Lcom/google/c/a/a/b/b/a/b/w;->a:Lcom/google/c/a/a/b/b/a/b/ab;

    iget-object v1, v1, Lcom/google/c/a/a/b/b/a/b/ab;->c:Ljava/util/Map;

    iget-object v3, p0, Lcom/google/c/a/a/b/b/a/b/r;->c:Ljava/lang/String;

    invoke-interface {v1, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map;

    if-eqz v1, :cond_2

    invoke-virtual {p0}, Lcom/google/c/a/a/b/b/a/b/r;->d()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v3}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 198
    :cond_2
    move-object/from16 v0, p4

    iput-object v0, p0, Lcom/google/c/a/a/b/b/a/b/r;->c:Ljava/lang/String;

    .line 199
    iget-object v1, p0, Lcom/google/c/a/a/b/b/a/b/r;->c:Ljava/lang/String;

    if-eqz v1, :cond_4

    .line 200
    invoke-virtual {p0}, Lcom/google/c/a/a/b/b/a/b/r;->j()Lcom/google/c/a/a/b/b/a/b;

    move-result-object v1

    iget-object v3, p0, Lcom/google/c/a/a/b/b/a/b/r;->c:Ljava/lang/String;

    invoke-interface {v1, v3}, Lcom/google/c/a/a/b/b/a/b;->a(Ljava/lang/String;)Lcom/google/c/a/a/b/b/a/g;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/c/a/a/b/b/a/g;->e()Lcom/google/c/a/a/b/b/a/p;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/c/a/a/b/b/a/b/r;->a(Lcom/google/c/a/a/b/b/a/r;)V

    .line 201
    iget-object v1, p0, Lcom/google/c/a/a/b/b/a/b/r;->d_:Lcom/google/c/a/a/b/b/a/b/w;

    iget-object v3, v1, Lcom/google/c/a/a/b/b/a/b/w;->a:Lcom/google/c/a/a/b/b/a/b/ab;

    iget-object v1, v3, Lcom/google/c/a/a/b/b/a/b/ab;->c:Ljava/util/Map;

    iget-object v4, p0, Lcom/google/c/a/a/b/b/a/b/r;->c:Ljava/lang/String;

    invoke-interface {v1, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map;

    if-nez v1, :cond_3

    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    iget-object v3, v3, Lcom/google/c/a/a/b/b/a/b/ab;->c:Ljava/util/Map;

    iget-object v4, p0, Lcom/google/c/a/a/b/b/a/b/r;->c:Ljava/lang/String;

    invoke-interface {v3, v4, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_3
    invoke-virtual {p0}, Lcom/google/c/a/a/b/b/a/b/r;->d()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v3, p0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 204
    :cond_4
    :goto_2
    if-eqz v2, :cond_5

    .line 205
    invoke-virtual/range {p5 .. p5}, Lcom/google/c/a/a/b/b/a/b/y;->a()V

    .line 207
    :cond_5
    if-eqz p3, :cond_0

    .line 208
    new-instance v1, Lcom/google/c/a/a/b/b/a/a/d;

    invoke-virtual {p2}, Lcom/google/c/a/a/b/b/a/o;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p2}, Lcom/google/c/a/a/b/b/a/o;->c()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p2}, Lcom/google/c/a/a/b/b/a/o;->d()Ljava/util/List;

    move-result-object v5

    invoke-virtual {p2}, Lcom/google/c/a/a/b/b/a/o;->a()Z

    move-result v6

    iget-object v8, p0, Lcom/google/c/a/a/b/b/a/b/r;->c:Ljava/lang/String;

    iget v9, p0, Lcom/google/c/a/a/b/b/a/b/r;->b:I

    move-object v2, p0

    move-object/from16 v7, p6

    invoke-direct/range {v1 .. v11}, Lcom/google/c/a/a/b/b/a/a/d;-><init>(Lcom/google/c/a/a/b/b/a/l;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;ZLcom/google/c/a/a/b/b/a/k;Ljava/lang/String;IILjava/lang/String;)V

    .line 211
    move-object/from16 v0, p5

    invoke-virtual {v0, v1}, Lcom/google/c/a/a/b/b/a/b/y;->a(Lcom/google/c/a/a/b/b/a/a/a;)V

    goto/16 :goto_0

    :cond_6
    move v2, v1

    goto :goto_1

    :cond_7
    move v2, v1

    goto :goto_2
.end method


# virtual methods
.method public final a(Lcom/google/c/a/a/b/b/a/i;)I
    .locals 3

    .prologue
    .line 282
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/google/c/a/a/b/b/a/b/r;->d()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget v2, p0, Lcom/google/c/a/a/b/b/a/b/r;->b:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-boolean v2, p0, Lcom/google/c/a/a/b/b/a/b/r;->d:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final a(I)Lcom/google/c/a/a/b/b/a/a;
    .locals 8

    .prologue
    .line 244
    invoke-virtual {p0}, Lcom/google/c/a/a/b/b/a/b/r;->j()Lcom/google/c/a/a/b/b/a/b;

    move-result-object v7

    new-instance v0, Lcom/google/c/a/a/b/b/a/c/h;

    invoke-virtual {p0}, Lcom/google/c/a/a/b/b/a/b/r;->d()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/c/a/a/b/b/a/b/r;->c:Ljava/lang/String;

    iget-boolean v4, p0, Lcom/google/c/a/a/b/b/a/b/r;->d:Z

    iget-object v5, p0, Lcom/google/c/a/a/b/b/a/b/r;->c:Ljava/lang/String;

    iget v6, p0, Lcom/google/c/a/a/b/b/a/b/r;->b:I

    move v3, p1

    invoke-direct/range {v0 .. v6}, Lcom/google/c/a/a/b/b/a/c/h;-><init>(Ljava/lang/String;Ljava/lang/String;IZLjava/lang/String;I)V

    sget-object v1, Lcom/google/c/a/a/b/b/a/k;->a:Lcom/google/c/a/a/b/b/a/k;

    invoke-interface {v7, v0, v1}, Lcom/google/c/a/a/b/b/a/b;->a(Lcom/google/c/b/a/c;Lcom/google/c/a/a/b/b/a/k;)Lcom/google/c/a/a/b/b/a/a;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/c/a/a/b/b/a/c/r;)Lcom/google/c/a/a/b/b/a/c/r;
    .locals 7

    .prologue
    .line 217
    instance-of v0, p1, Lcom/google/c/a/a/b/b/a/c/h;

    if-eqz v0, :cond_0

    .line 218
    check-cast p1, Lcom/google/c/a/a/b/b/a/c/h;

    .line 219
    new-instance v0, Lcom/google/c/a/a/b/b/a/c/h;

    invoke-virtual {p1}, Lcom/google/c/a/a/b/b/a/c/h;->e()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/c/a/a/b/b/a/c/h;->g()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lcom/google/c/a/a/b/b/a/c/h;->f()I

    move-result v3

    invoke-virtual {p1}, Lcom/google/c/a/a/b/b/a/c/h;->d()Z

    move-result v4

    invoke-virtual {p1}, Lcom/google/c/a/a/b/b/a/c/h;->a()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1}, Lcom/google/c/a/a/b/b/a/c/h;->c()I

    move-result v6

    invoke-direct/range {v0 .. v6}, Lcom/google/c/a/a/b/b/a/c/h;-><init>(Ljava/lang/String;Ljava/lang/String;IZLjava/lang/String;I)V

    return-object v0

    .line 227
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Index references can only get inverse of IndexMutation"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 63
    iget-object v0, p0, Lcom/google/c/a/a/b/b/a/b/r;->c:Ljava/lang/String;

    return-object v0
.end method

.method public final a(Lcom/google/c/a/a/b/b/a/c/p;Lcom/google/c/a/a/b/b/a/o;ZLcom/google/c/a/a/b/b/a/b/y;Lcom/google/c/a/a/b/b/a/k;)V
    .locals 7

    .prologue
    const/4 v4, 0x1

    .line 74
    instance-of v0, p1, Lcom/google/c/a/a/b/b/a/c/h;

    if-eqz v0, :cond_1

    .line 75
    check-cast p1, Lcom/google/c/a/a/b/b/a/c/h;

    invoke-virtual {p1}, Lcom/google/c/a/a/b/b/a/c/h;->d()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/c/a/a/b/b/a/b/r;->d:Z

    invoke-virtual {p1}, Lcom/google/c/a/a/b/b/a/c/h;->c()I

    move-result v1

    invoke-virtual {p1}, Lcom/google/c/a/a/b/b/a/c/h;->a()Ljava/lang/String;

    move-result-object v4

    move-object v0, p0

    move-object v2, p2

    move v3, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/google/c/a/a/b/b/a/b/r;->a(ILcom/google/c/a/a/b/b/a/o;ZLjava/lang/String;Lcom/google/c/a/a/b/b/a/b/y;Lcom/google/c/a/a/b/b/a/k;)V

    .line 90
    :cond_0
    :goto_0
    return-void

    .line 76
    :cond_1
    instance-of v0, p1, Lcom/google/c/a/a/b/b/a/c/b;

    if-eqz v0, :cond_3

    .line 77
    check-cast p1, Lcom/google/c/a/a/b/b/a/c/b;

    invoke-virtual {p1}, Lcom/google/c/a/a/b/b/a/c/b;->e()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/c/a/a/b/b/a/b/r;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    instance-of v0, p1, Lcom/google/c/a/a/b/b/a/c/j;

    if-eqz v0, :cond_2

    iget v0, p0, Lcom/google/c/a/a/b/b/a/b/r;->b:I

    invoke-virtual {p1}, Lcom/google/c/a/a/b/b/a/c/b;->c()I

    move-result v1

    if-lt v0, v1, :cond_2

    iget v0, p0, Lcom/google/c/a/a/b/b/a/b/r;->b:I

    invoke-virtual {p1}, Lcom/google/c/a/a/b/b/a/c/b;->c()I

    move-result v1

    invoke-virtual {p1}, Lcom/google/c/a/a/b/b/a/c/b;->a()I

    move-result v2

    add-int/2addr v1, v2

    if-lt v0, v1, :cond_0

    :cond_2
    iget v0, p0, Lcom/google/c/a/a/b/b/a/b/r;->b:I

    invoke-virtual {p1}, Lcom/google/c/a/a/b/b/a/c/b;->c()I

    move-result v1

    invoke-virtual {p1}, Lcom/google/c/a/a/b/b/a/c/b;->d()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    iget-boolean v3, p0, Lcom/google/c/a/a/b/b/a/b/r;->d:Z

    invoke-static {v0, v1, v2, v3, v4}, Lcom/google/c/a/a/b/h/d;->a(IIIZZ)I

    move-result v1

    iget-object v4, p0, Lcom/google/c/a/a/b/b/a/b/r;->c:Ljava/lang/String;

    move-object v0, p0

    move-object v2, p2

    move v3, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/google/c/a/a/b/b/a/b/r;->a(ILcom/google/c/a/a/b/b/a/o;ZLjava/lang/String;Lcom/google/c/a/a/b/b/a/b/y;Lcom/google/c/a/a/b/b/a/k;)V

    goto :goto_0

    .line 79
    :cond_3
    instance-of v0, p1, Lcom/google/c/a/a/b/b/a/c/a;

    if-eqz v0, :cond_5

    .line 80
    check-cast p1, Lcom/google/c/a/a/b/b/a/c/a;

    invoke-virtual {p1}, Lcom/google/c/a/a/b/b/a/c/a;->e()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/c/a/a/b/b/a/b/r;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/google/c/a/a/b/b/a/b/r;->b:I

    invoke-virtual {p1}, Lcom/google/c/a/a/b/b/a/c/a;->c()I

    move-result v1

    invoke-virtual {p1}, Lcom/google/c/a/a/b/b/a/c/a;->a()I

    move-result v2

    mul-int/lit8 v2, v2, -0x1

    invoke-static {v0, v1, v2, v4, v4}, Lcom/google/c/a/a/b/h/d;->a(IIIZZ)I

    move-result v1

    if-gez v1, :cond_4

    iget-boolean v0, p0, Lcom/google/c/a/a/b/b/a/b/r;->d:Z

    if-eqz v0, :cond_0

    :cond_4
    iget-object v4, p0, Lcom/google/c/a/a/b/b/a/b/r;->c:Ljava/lang/String;

    move-object v0, p0

    move-object v2, p2

    move v3, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/google/c/a/a/b/b/a/b/r;->a(ILcom/google/c/a/a/b/b/a/o;ZLjava/lang/String;Lcom/google/c/a/a/b/b/a/b/y;Lcom/google/c/a/a/b/b/a/k;)V

    goto :goto_0

    .line 82
    :cond_5
    instance-of v0, p1, Lcom/google/c/a/a/b/b/a/c/m;

    if-eqz v0, :cond_9

    .line 83
    check-cast p1, Lcom/google/c/a/a/b/b/a/c/m;

    iget v0, p0, Lcom/google/c/a/a/b/b/a/b/r;->b:I

    invoke-virtual {p1}, Lcom/google/c/a/a/b/b/a/c/m;->a()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/c/a/a/b/b/a/b/r;->c:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_8

    invoke-virtual {p1}, Lcom/google/c/a/a/b/b/a/c/m;->c()I

    move-result v1

    iget v2, p0, Lcom/google/c/a/a/b/b/a/b/r;->b:I

    if-ne v1, v2, :cond_7

    invoke-virtual {p1}, Lcom/google/c/a/a/b/b/a/c/m;->e()I

    move-result v1

    invoke-virtual {p1}, Lcom/google/c/a/a/b/b/a/c/m;->e()I

    move-result v0

    invoke-virtual {p1}, Lcom/google/c/a/a/b/b/a/c/m;->c()I

    move-result v2

    if-le v0, v2, :cond_6

    invoke-virtual {p1}, Lcom/google/c/a/a/b/b/a/c/m;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/c/a/a/b/b/a/c/m;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    add-int/lit8 v1, v1, -0x1

    :cond_6
    invoke-virtual {p1}, Lcom/google/c/a/a/b/b/a/c/m;->d()Ljava/lang/String;

    move-result-object v4

    move-object v0, p0

    move-object v2, p2

    move v3, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/google/c/a/a/b/b/a/b/r;->a(ILcom/google/c/a/a/b/b/a/o;ZLjava/lang/String;Lcom/google/c/a/a/b/b/a/b/y;Lcom/google/c/a/a/b/b/a/k;)V

    goto/16 :goto_0

    :cond_7
    invoke-virtual {p1}, Lcom/google/c/a/a/b/b/a/c/m;->c()I

    move-result v1

    iget v2, p0, Lcom/google/c/a/a/b/b/a/b/r;->b:I

    if-ge v1, v2, :cond_8

    add-int/lit8 v0, v0, -0x1

    :cond_8
    invoke-virtual {p1}, Lcom/google/c/a/a/b/b/a/c/m;->d()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/c/a/a/b/b/a/b/r;->c:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_a

    invoke-virtual {p1}, Lcom/google/c/a/a/b/b/a/c/m;->e()I

    move-result v1

    iget v2, p0, Lcom/google/c/a/a/b/b/a/b/r;->b:I

    if-gt v1, v2, :cond_a

    add-int/lit8 v0, v0, 0x1

    move v1, v0

    :goto_1
    iget v0, p0, Lcom/google/c/a/a/b/b/a/b/r;->b:I

    if-eq v1, v0, :cond_0

    iget-object v4, p0, Lcom/google/c/a/a/b/b/a/b/r;->c:Ljava/lang/String;

    move-object v0, p0

    move-object v2, p2

    move v3, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/google/c/a/a/b/b/a/b/r;->a(ILcom/google/c/a/a/b/b/a/o;ZLjava/lang/String;Lcom/google/c/a/a/b/b/a/b/y;Lcom/google/c/a/a/b/b/a/k;)V

    goto/16 :goto_0

    .line 84
    :cond_9
    instance-of v0, p1, Lcom/google/c/a/a/b/b/a/c/s;

    if-nez v0, :cond_0

    .line 87
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Muations of type "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " cannot be applied to IndexReferences."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_a
    move v1, v0

    goto :goto_1
.end method

.method public final a(Lcom/google/c/a/a/b/b/a/g;IZ)V
    .locals 8

    .prologue
    .line 233
    iget-object v0, p0, Lcom/google/c/a/a/b/b/a/b/r;->c:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 234
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Index reference has already been initialized"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 236
    :cond_0
    invoke-virtual {p0}, Lcom/google/c/a/a/b/b/a/b/r;->j()Lcom/google/c/a/a/b/b/a/b;

    move-result-object v7

    new-instance v0, Lcom/google/c/a/a/b/b/a/c/h;

    invoke-virtual {p0}, Lcom/google/c/a/a/b/b/a/b/r;->d()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1}, Lcom/google/c/a/a/b/b/a/g;->d()Ljava/lang/String;

    move-result-object v2

    const/4 v5, 0x0

    const/4 v6, -0x1

    move v3, p2

    move v4, p3

    invoke-direct/range {v0 .. v6}, Lcom/google/c/a/a/b/b/a/c/h;-><init>(Ljava/lang/String;Ljava/lang/String;IZLjava/lang/String;I)V

    sget-object v1, Lcom/google/c/a/a/b/b/a/k;->a:Lcom/google/c/a/a/b/b/a/k;

    invoke-interface {v7, v0, v1}, Lcom/google/c/a/a/b/b/a/b;->a(Lcom/google/c/b/a/c;Lcom/google/c/a/a/b/b/a/k;)Lcom/google/c/a/a/b/b/a/a;

    .line 240
    return-void
.end method

.method public final a(Lcom/google/c/a/a/b/b/a/j;Lcom/google/c/a/a/b/b/a/g;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 270
    instance-of v1, p2, Lcom/google/c/a/a/b/b/a/l;

    if-nez v1, :cond_0

    .line 274
    :goto_0
    return v0

    .line 273
    :cond_0
    check-cast p2, Lcom/google/c/a/a/b/b/a/l;

    .line 274
    const/4 v1, 0x6

    new-array v1, v1, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/google/c/a/a/b/b/a/b/r;->c:Ljava/lang/String;

    aput-object v2, v1, v0

    const/4 v0, 0x1

    invoke-interface {p2}, Lcom/google/c/a/a/b/b/a/l;->a()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v0

    const/4 v0, 0x2

    iget v2, p0, Lcom/google/c/a/a/b/b/a/b/r;->b:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v0

    const/4 v0, 0x3

    invoke-interface {p2}, Lcom/google/c/a/a/b/b/a/l;->b()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v0

    const/4 v0, 0x4

    iget-boolean v2, p0, Lcom/google/c/a/a/b/b/a/b/r;->d:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v1, v0

    const/4 v0, 0x5

    invoke-interface {p2}, Lcom/google/c/a/a/b/b/a/l;->c()Z

    move-result v2

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v1, v0

    invoke-static {v1}, Lcom/google/c/a/a/b/h/b;->a([Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 58
    iget v0, p0, Lcom/google/c/a/a/b/b/a/b/r;->b:I

    return v0
.end method

.method public final b(Lcom/google/c/a/a/b/b/a/i;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 287
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "DefaultIndexReference [id="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/c/a/a/b/b/a/b/r;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", objectId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/c/a/a/b/b/a/b/r;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", index="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/c/a/a/b/b/a/b/r;->b:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", canBeDeleted="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/google/c/a/a/b/b/a/b/r;->d:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 53
    iget-boolean v0, p0, Lcom/google/c/a/a/b/b/a/b/r;->d:Z

    return v0
.end method

.method public final synthetic compareTo(Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 41
    check-cast p1, Lcom/google/c/a/a/b/b/a/b/r;

    sget-object v0, Lcom/google/c/a/a/b/b/a/l;->a:Ljava/util/Comparator;

    invoke-interface {v0, p0, p1}, Ljava/util/Comparator;->compare(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final f()Ljava/lang/String;
    .locals 1

    .prologue
    .line 303
    sget-object v0, Lcom/google/c/a/a/b/b/a/b/c;->d:Lcom/google/c/a/a/b/b/a/b/c;

    invoke-virtual {v0}, Lcom/google/c/a/a/b/b/a/b/c;->a()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final i()Lcom/google/c/a/a/b/b/a/c/n;
    .locals 8

    .prologue
    const/4 v3, 0x0

    .line 251
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 252
    iget-object v0, p0, Lcom/google/c/a/a/b/b/a/b/r;->c:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 253
    new-instance v0, Lcom/google/c/a/a/b/b/a/c/h;

    invoke-virtual {p0}, Lcom/google/c/a/a/b/b/a/b/r;->d()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    iget-object v5, p0, Lcom/google/c/a/a/b/b/a/b/r;->c:Ljava/lang/String;

    iget v6, p0, Lcom/google/c/a/a/b/b/a/b/r;->b:I

    move v4, v3

    invoke-direct/range {v0 .. v6}, Lcom/google/c/a/a/b/b/a/c/h;-><init>(Ljava/lang/String;Ljava/lang/String;IZLjava/lang/String;I)V

    invoke-interface {v7, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 255
    :cond_0
    new-instance v0, Lcom/google/c/a/a/b/b/a/c/o;

    invoke-direct {v0}, Lcom/google/c/a/a/b/b/a/c/o;-><init>()V

    invoke-virtual {v0, v7}, Lcom/google/c/a/a/b/b/a/c/o;->a(Ljava/util/Collection;)Lcom/google/c/a/a/b/b/a/c/o;

    move-result-object v0

    iput-boolean v3, v0, Lcom/google/c/a/a/b/b/a/c/o;->d:Z

    invoke-virtual {v0}, Lcom/google/c/a/a/b/b/a/c/o;->a()Lcom/google/c/a/a/b/b/a/c/n;

    move-result-object v0

    return-object v0
.end method

.method public final k()Ljava/util/List;
    .locals 8

    .prologue
    .line 263
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 264
    new-instance v0, Lcom/google/c/a/a/b/b/a/c/h;

    invoke-virtual {p0}, Lcom/google/c/a/a/b/b/a/b/r;->d()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/c/a/a/b/b/a/b/r;->c:Ljava/lang/String;

    iget v3, p0, Lcom/google/c/a/a/b/b/a/b/r;->b:I

    iget-boolean v4, p0, Lcom/google/c/a/a/b/b/a/b/r;->d:Z

    iget-object v5, p0, Lcom/google/c/a/a/b/b/a/b/r;->c:Ljava/lang/String;

    iget v6, p0, Lcom/google/c/a/a/b/b/a/b/r;->b:I

    invoke-direct/range {v0 .. v6}, Lcom/google/c/a/a/b/b/a/c/h;-><init>(Ljava/lang/String;Ljava/lang/String;IZLjava/lang/String;I)V

    invoke-interface {v7, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 265
    return-object v7
.end method

.method public final l()I
    .locals 1

    .prologue
    .line 298
    iget-object v0, p0, Lcom/google/c/a/a/b/b/a/b/r;->c:Ljava/lang/String;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    add-int/lit8 v0, v0, 0x20

    return v0

    :cond_0
    iget-object v0, p0, Lcom/google/c/a/a/b/b/a/b/r;->c:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    goto :goto_0
.end method

.method public final n()Ljava/lang/Iterable;
    .locals 2

    .prologue
    .line 308
    iget-object v0, p0, Lcom/google/c/a/a/b/b/a/b/r;->c:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 309
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    .line 311
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Lcom/google/c/a/a/b/b/a/b/r;->j()Lcom/google/c/a/a/b/b/a/b;

    move-result-object v0

    iget-object v1, p0, Lcom/google/c/a/a/b/b/a/b/r;->c:Ljava/lang/String;

    invoke-interface {v0, v1}, Lcom/google/c/a/a/b/b/a/b;->a(Ljava/lang/String;)Lcom/google/c/a/a/b/b/a/g;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/c/a/a/b/b/a/g;->e()Lcom/google/c/a/a/b/b/a/p;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    goto :goto_0
.end method
