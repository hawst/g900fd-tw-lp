.class final Lcom/google/c/a/a/b/b/a/b/m;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/c/a/a/b/b/a/j;


# instance fields
.field private final a:Ljava/util/Set;


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 168
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 169
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/c/a/a/b/b/a/b/m;->a:Ljava/util/Set;

    .line 206
    return-void
.end method

.method synthetic constructor <init>(B)V
    .locals 0

    .prologue
    .line 168
    invoke-direct {p0}, Lcom/google/c/a/a/b/b/a/b/m;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/c/a/a/b/b/a/r;Lcom/google/c/a/a/b/b/a/r;Lcom/google/c/a/a/b/b/a/b;Lcom/google/c/a/a/b/b/a/b;)Z
    .locals 5

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 173
    if-eqz p1, :cond_0

    if-nez p2, :cond_3

    .line 174
    :cond_0
    if-nez p1, :cond_2

    if-nez p2, :cond_2

    .line 197
    :cond_1
    :goto_0
    return v0

    :cond_2
    move v0, v1

    .line 174
    goto :goto_0

    .line 177
    :cond_3
    instance-of v2, p1, Lcom/google/c/a/a/b/b/a/p;

    if-eqz v2, :cond_4

    instance-of v2, p2, Lcom/google/c/a/a/b/b/a/p;

    if-eqz v2, :cond_4

    .line 178
    check-cast p1, Lcom/google/c/a/a/b/b/a/p;

    .line 179
    check-cast p2, Lcom/google/c/a/a/b/b/a/p;

    .line 180
    invoke-virtual {p1}, Lcom/google/c/a/a/b/b/a/p;->a()Ljava/lang/String;

    move-result-object v2

    invoke-interface {p3, v2}, Lcom/google/c/a/a/b/b/a/b;->a(Ljava/lang/String;)Lcom/google/c/a/a/b/b/a/g;

    move-result-object v2

    .line 181
    invoke-virtual {p2}, Lcom/google/c/a/a/b/b/a/p;->a()Ljava/lang/String;

    move-result-object v3

    invoke-interface {p4, v3}, Lcom/google/c/a/a/b/b/a/b;->a(Ljava/lang/String;)Lcom/google/c/a/a/b/b/a/g;

    move-result-object v3

    .line 182
    new-instance v4, Lcom/google/c/a/a/b/b/a/b/n;

    invoke-direct {v4, v2, v3, v1}, Lcom/google/c/a/a/b/b/a/b/n;-><init>(Lcom/google/c/a/a/b/b/a/g;Lcom/google/c/a/a/b/b/a/g;B)V

    .line 183
    iget-object v1, p0, Lcom/google/c/a/a/b/b/a/b/m;->a:Ljava/util/Set;

    invoke-interface {v1, v4}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 186
    iget-object v0, p0, Lcom/google/c/a/a/b/b/a/b/m;->a:Ljava/util/Set;

    invoke-interface {v0, v4}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 187
    invoke-interface {v2, p0, v3}, Lcom/google/c/a/a/b/b/a/g;->a(Lcom/google/c/a/a/b/b/a/j;Lcom/google/c/a/a/b/b/a/g;)Z

    move-result v0

    goto :goto_0

    .line 193
    :cond_4
    if-eq p1, p2, :cond_1

    .line 197
    invoke-virtual {p1, p2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method
