.class public abstract enum Lcom/google/c/a/a/b/b/a/b/c;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field public static final enum a:Lcom/google/c/a/a/b/b/a/b/c;

.field public static final enum b:Lcom/google/c/a/a/b/b/a/b/c;

.field public static final enum c:Lcom/google/c/a/a/b/b/a/b/c;

.field public static final enum d:Lcom/google/c/a/a/b/b/a/b/c;

.field private static final synthetic g:[Lcom/google/c/a/a/b/b/a/b/c;


# instance fields
.field private final e:Ljava/lang/String;

.field private final f:I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 9
    new-instance v0, Lcom/google/c/a/a/b/b/a/b/d;

    const-string v1, "COLLABORATIVE_MAP"

    const-string v2, "Map"

    invoke-direct {v0, v1, v2}, Lcom/google/c/a/a/b/b/a/b/d;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/google/c/a/a/b/b/a/b/c;->a:Lcom/google/c/a/a/b/b/a/b/c;

    .line 16
    new-instance v0, Lcom/google/c/a/a/b/b/a/b/e;

    const-string v1, "COLLABORATIVE_LIST"

    const-string v2, "List"

    invoke-direct {v0, v1, v2}, Lcom/google/c/a/a/b/b/a/b/e;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/google/c/a/a/b/b/a/b/c;->b:Lcom/google/c/a/a/b/b/a/b/c;

    .line 23
    new-instance v0, Lcom/google/c/a/a/b/b/a/b/f;

    const-string v1, "COLLABORATIVE_STRING"

    const-string v2, "EditableString"

    invoke-direct {v0, v1, v2}, Lcom/google/c/a/a/b/b/a/b/f;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/google/c/a/a/b/b/a/b/c;->c:Lcom/google/c/a/a/b/b/a/b/c;

    .line 30
    new-instance v0, Lcom/google/c/a/a/b/b/a/b/g;

    const-string v1, "INDEX_REFERENCE"

    const-string v2, "IndexReference"

    invoke-direct {v0, v1, v2}, Lcom/google/c/a/a/b/b/a/b/g;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/google/c/a/a/b/b/a/b/c;->d:Lcom/google/c/a/a/b/b/a/b/c;

    .line 8
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/google/c/a/a/b/b/a/b/c;

    const/4 v1, 0x0

    sget-object v2, Lcom/google/c/a/a/b/b/a/b/c;->a:Lcom/google/c/a/a/b/b/a/b/c;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    sget-object v2, Lcom/google/c/a/a/b/b/a/b/c;->b:Lcom/google/c/a/a/b/b/a/b/c;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    sget-object v2, Lcom/google/c/a/a/b/b/a/b/c;->c:Lcom/google/c/a/a/b/b/a/b/c;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    sget-object v2, Lcom/google/c/a/a/b/b/a/b/c;->d:Lcom/google/c/a/a/b/b/a/b/c;

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/c/a/a/b/b/a/b/c;->g:[Lcom/google/c/a/a/b/b/a/b/c;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;I)V
    .locals 0

    .prologue
    .line 40
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 41
    iput-object p3, p0, Lcom/google/c/a/a/b/b/a/b/c;->e:Ljava/lang/String;

    .line 42
    iput p4, p0, Lcom/google/c/a/a/b/b/a/b/c;->f:I

    .line 43
    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;ILjava/lang/String;IB)V
    .locals 0

    .prologue
    .line 8
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/c/a/a/b/b/a/b/c;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/c/a/a/b/b/a/b/c;
    .locals 1

    .prologue
    .line 8
    const-class v0, Lcom/google/c/a/a/b/b/a/b/c;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/c/a/a/b/b/a/b/c;

    return-object v0
.end method

.method public static values()[Lcom/google/c/a/a/b/b/a/b/c;
    .locals 1

    .prologue
    .line 8
    sget-object v0, Lcom/google/c/a/a/b/b/a/b/c;->g:[Lcom/google/c/a/a/b/b/a/b/c;

    invoke-virtual {v0}, [Lcom/google/c/a/a/b/b/a/b/c;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/c/a/a/b/b/a/b/c;

    return-object v0
.end method


# virtual methods
.method public abstract a(Lcom/google/c/a/a/b/b/a/b/w;Ljava/lang/String;)Lcom/google/c/a/a/b/b/a/b/a;
.end method

.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lcom/google/c/a/a/b/b/a/b/c;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 61
    iget v0, p0, Lcom/google/c/a/a/b/b/a/b/c;->f:I

    return v0
.end method
