.class abstract Lcom/google/c/a/a/b/b/a/b/l;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/c/a/a/b/b/a/g;


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 168
    return-void
.end method


# virtual methods
.method protected a(Lcom/google/c/a/a/b/b/a/g;)Z
    .locals 5

    .prologue
    .line 45
    new-instance v0, Lcom/google/c/a/a/b/b/a/b/m;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/google/c/a/a/b/b/a/b/m;-><init>(B)V

    invoke-virtual {p0}, Lcom/google/c/a/a/b/b/a/b/l;->e()Lcom/google/c/a/a/b/b/a/p;

    move-result-object v1

    invoke-interface {p1}, Lcom/google/c/a/a/b/b/a/g;->e()Lcom/google/c/a/a/b/b/a/p;

    move-result-object v2

    invoke-virtual {p0}, Lcom/google/c/a/a/b/b/a/b/l;->j()Lcom/google/c/a/a/b/b/a/b;

    move-result-object v3

    invoke-interface {p1}, Lcom/google/c/a/a/b/b/a/g;->j()Lcom/google/c/a/a/b/b/a/b;

    move-result-object v4

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/google/c/a/a/b/b/a/b/m;->a(Lcom/google/c/a/a/b/b/a/r;Lcom/google/c/a/a/b/b/a/r;Lcom/google/c/a/a/b/b/a/b;Lcom/google/c/a/a/b/b/a/b;)Z

    move-result v0

    return v0
.end method

.method protected aA_()Ljava/lang/String;
    .locals 2

    .prologue
    .line 76
    new-instance v0, Lcom/google/c/a/a/b/b/a/b/p;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/c/a/a/b/b/a/b/p;-><init>(Lcom/google/c/a/a/b/b/a/b/l;B)V

    invoke-virtual {p0}, Lcom/google/c/a/a/b/b/a/b/l;->e()Lcom/google/c/a/a/b/b/a/p;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/c/a/a/b/b/a/b/p;->a(Lcom/google/c/a/a/b/b/a/r;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method protected az_()I
    .locals 2

    .prologue
    .line 61
    new-instance v0, Lcom/google/c/a/a/b/b/a/b/o;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/c/a/a/b/b/a/b/o;-><init>(Lcom/google/c/a/a/b/b/a/b/l;B)V

    invoke-virtual {p0}, Lcom/google/c/a/a/b/b/a/b/l;->e()Lcom/google/c/a/a/b/b/a/p;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/c/a/a/b/b/a/b/o;->a(Lcom/google/c/a/a/b/b/a/r;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 24
    if-nez p1, :cond_1

    .line 34
    :cond_0
    :goto_0
    return v0

    .line 27
    :cond_1
    if-ne p1, p0, :cond_2

    .line 28
    const/4 v0, 0x1

    goto :goto_0

    .line 30
    :cond_2
    instance-of v1, p1, Lcom/google/c/a/a/b/b/a/g;

    if-eqz v1, :cond_0

    .line 33
    check-cast p1, Lcom/google/c/a/a/b/b/a/g;

    .line 34
    invoke-virtual {p0, p1}, Lcom/google/c/a/a/b/b/a/b/l;->a(Lcom/google/c/a/a/b/b/a/g;)Z

    move-result v0

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 50
    invoke-virtual {p0}, Lcom/google/c/a/a/b/b/a/b/l;->az_()I

    move-result v0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 65
    invoke-virtual {p0}, Lcom/google/c/a/a/b/b/a/b/l;->aA_()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
