.class public final enum Lcom/google/c/a/a/b/f/a/v;
.super Ljava/lang/Enum;
.source "SourceFile"

# interfaces
.implements Lcom/google/c/a/a/b/f/a/k;


# static fields
.field public static final enum a:Lcom/google/c/a/a/b/f/a/v;

.field public static final enum b:Lcom/google/c/a/a/b/f/a/v;

.field private static final synthetic d:[Lcom/google/c/a/a/b/f/a/v;


# instance fields
.field private final c:I


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 18
    new-instance v0, Lcom/google/c/a/a/b/f/a/v;

    const-string v1, "REQUEST_NUMBER"

    invoke-direct {v0, v1, v2, v2}, Lcom/google/c/a/a/b/f/a/v;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/c/a/a/b/f/a/v;->a:Lcom/google/c/a/a/b/f/a/v;

    .line 19
    new-instance v0, Lcom/google/c/a/a/b/f/a/v;

    const-string v1, "COMMAND"

    invoke-direct {v0, v1, v3, v3}, Lcom/google/c/a/a/b/f/a/v;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/c/a/a/b/f/a/v;->b:Lcom/google/c/a/a/b/f/a/v;

    .line 17
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/google/c/a/a/b/f/a/v;

    sget-object v1, Lcom/google/c/a/a/b/f/a/v;->a:Lcom/google/c/a/a/b/f/a/v;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/c/a/a/b/f/a/v;->b:Lcom/google/c/a/a/b/f/a/v;

    aput-object v1, v0, v3

    sput-object v0, Lcom/google/c/a/a/b/f/a/v;->d:[Lcom/google/c/a/a/b/f/a/v;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 24
    iput p3, p0, Lcom/google/c/a/a/b/f/a/v;->c:I

    .line 25
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/c/a/a/b/f/a/v;
    .locals 1

    .prologue
    .line 17
    const-class v0, Lcom/google/c/a/a/b/f/a/v;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/c/a/a/b/f/a/v;

    return-object v0
.end method

.method public static values()[Lcom/google/c/a/a/b/f/a/v;
    .locals 1

    .prologue
    .line 17
    sget-object v0, Lcom/google/c/a/a/b/f/a/v;->d:[Lcom/google/c/a/a/b/f/a/v;

    invoke-virtual {v0}, [Lcom/google/c/a/a/b/f/a/v;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/c/a/a/b/f/a/v;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 29
    iget v0, p0, Lcom/google/c/a/a/b/f/a/v;->c:I

    return v0
.end method
