.class final Lcom/google/c/a/a/a/c/h;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:I

.field final synthetic b:Lcom/google/c/a/a/a/c/a;


# direct methods
.method constructor <init>(Lcom/google/c/a/a/a/c/a;I)V
    .locals 0

    .prologue
    .line 322
    iput-object p1, p0, Lcom/google/c/a/a/a/c/h;->b:Lcom/google/c/a/a/a/c/a;

    iput p2, p0, Lcom/google/c/a/a/a/c/h;->a:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 3

    .prologue
    .line 325
    iget v0, p0, Lcom/google/c/a/a/a/c/h;->a:I

    sparse-switch v0, :sswitch_data_0

    .line 339
    iget-object v0, p0, Lcom/google/c/a/a/a/c/h;->b:Lcom/google/c/a/a/a/c/a;

    iget-object v0, v0, Lcom/google/c/a/a/a/c/a;->b:Lcom/google/c/a/a/a/c/s;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Server returned unexpected HTTP status: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/google/c/a/a/a/c/h;->a:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/c/a/a/a/c/s;->d(Ljava/lang/String;)V

    .line 340
    :goto_0
    return-void

    .line 327
    :sswitch_0
    iget-object v0, p0, Lcom/google/c/a/a/a/c/h;->b:Lcom/google/c/a/a/a/c/a;

    iget-object v0, v0, Lcom/google/c/a/a/a/c/a;->b:Lcom/google/c/a/a/a/c/s;

    const-string v1, "File not found."

    invoke-interface {v0, v1}, Lcom/google/c/a/a/a/c/s;->b(Ljava/lang/String;)V

    goto :goto_0

    .line 330
    :sswitch_1
    iget-object v0, p0, Lcom/google/c/a/a/a/c/h;->b:Lcom/google/c/a/a/a/c/a;

    iget-object v0, v0, Lcom/google/c/a/a/a/c/a;->b:Lcom/google/c/a/a/a/c/s;

    const-string v1, "Access denied to file."

    invoke-interface {v0, v1}, Lcom/google/c/a/a/a/c/s;->c(Ljava/lang/String;)V

    goto :goto_0

    .line 333
    :sswitch_2
    iget-object v0, p0, Lcom/google/c/a/a/a/c/h;->b:Lcom/google/c/a/a/a/c/a;

    iget-object v0, v0, Lcom/google/c/a/a/a/c/a;->b:Lcom/google/c/a/a/a/c/s;

    const-string v1, "A different session already created the file."

    invoke-interface {v0, v1}, Lcom/google/c/a/a/a/c/s;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 336
    :sswitch_3
    iget-object v0, p0, Lcom/google/c/a/a/a/c/h;->b:Lcom/google/c/a/a/a/c/a;

    iget-object v0, v0, Lcom/google/c/a/a/a/c/a;->b:Lcom/google/c/a/a/a/c/s;

    const-string v1, "The OAuth token must be refreshed."

    invoke-interface {v0, v1}, Lcom/google/c/a/a/a/c/s;->e(Ljava/lang/String;)V

    goto :goto_0

    .line 325
    :sswitch_data_0
    .sparse-switch
        0x191 -> :sswitch_3
        0x193 -> :sswitch_1
        0x194 -> :sswitch_0
        0x1a7 -> :sswitch_2
    .end sparse-switch
.end method
