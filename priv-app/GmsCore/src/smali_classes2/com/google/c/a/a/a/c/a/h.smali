.class final Lcom/google/c/a/a/a/c/a/h;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lcom/google/c/a/a/a/c/a/a;


# direct methods
.method private constructor <init>(Lcom/google/c/a/a/a/c/a/a;)V
    .locals 0

    .prologue
    .line 232
    iput-object p1, p0, Lcom/google/c/a/a/a/c/a/h;->a:Lcom/google/c/a/a/a/c/a/a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/c/a/a/a/c/a/a;B)V
    .locals 0

    .prologue
    .line 232
    invoke-direct {p0, p1}, Lcom/google/c/a/a/a/c/a/h;-><init>(Lcom/google/c/a/a/a/c/a/a;)V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 3

    .prologue
    .line 235
    iget-object v0, p0, Lcom/google/c/a/a/a/c/a/h;->a:Lcom/google/c/a/a/a/c/a/a;

    invoke-static {v0}, Lcom/google/c/a/a/a/c/a/a;->a(Lcom/google/c/a/a/a/c/a/a;)Lcom/google/c/a/a/a/c/a/d;

    move-result-object v1

    monitor-enter v1

    .line 236
    :try_start_0
    iget-object v0, p0, Lcom/google/c/a/a/a/c/a/h;->a:Lcom/google/c/a/a/a/c/a/a;

    invoke-static {v0}, Lcom/google/c/a/a/a/c/a/a;->a(Lcom/google/c/a/a/a/c/a/a;)Lcom/google/c/a/a/a/c/a/d;

    move-result-object v0

    sget-object v2, Lcom/google/c/a/a/a/c/a/d;->b:Lcom/google/c/a/a/a/c/a/d;

    if-ne v0, v2, :cond_0

    .line 237
    invoke-static {}, Lcom/google/c/a/a/a/c/a/a;->c()Ljava/util/logging/Logger;

    move-result-object v0

    const-string v2, "Starting connection streaming test."

    invoke-virtual {v0, v2}, Ljava/util/logging/Logger;->info(Ljava/lang/String;)V

    .line 238
    iget-object v0, p0, Lcom/google/c/a/a/a/c/a/h;->a:Lcom/google/c/a/a/a/c/a/a;

    invoke-static {v0}, Lcom/google/c/a/a/a/c/a/a;->c(Lcom/google/c/a/a/a/c/a/a;)Lcom/google/c/a/a/a/c/a/c/f;

    move-result-object v0

    iget-object v2, p0, Lcom/google/c/a/a/a/c/a/h;->a:Lcom/google/c/a/a/a/c/a/a;

    invoke-static {v2}, Lcom/google/c/a/a/a/c/a/a;->b(Lcom/google/c/a/a/a/c/a/a;)Lcom/google/c/a/a/a/c/a/b/d;

    move-result-object v2

    invoke-interface {v0, v2}, Lcom/google/c/a/a/a/c/a/c/f;->a(Lcom/google/c/a/a/a/c/a/b/d;)Lcom/google/c/a/a/a/c/a/c/b;

    move-result-object v0

    .line 239
    iget-object v2, p0, Lcom/google/c/a/a/a/c/a/h;->a:Lcom/google/c/a/a/a/c/a/a;

    invoke-static {v2}, Lcom/google/c/a/a/a/c/a/a;->d(Lcom/google/c/a/a/a/c/a/a;)Lcom/google/c/a/a/a/c/a/f;

    move-result-object v2

    invoke-interface {v0, v2}, Lcom/google/c/a/a/a/c/a/c/b;->a(Lcom/google/c/a/a/a/c/a/c/c;)V

    .line 240
    invoke-interface {v0}, Lcom/google/c/a/a/a/c/a/c/b;->a()V

    .line 242
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method
