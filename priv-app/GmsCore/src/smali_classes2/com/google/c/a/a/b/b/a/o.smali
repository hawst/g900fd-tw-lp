.class public final Lcom/google/c/a/a/b/b/a/o;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static final a:Lcom/google/c/a/a/b/b/a/o;


# instance fields
.field private final b:Z

.field private final c:Ljava/lang/String;

.field private final d:Ljava/lang/String;

.field private final e:Ljava/util/List;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 20
    new-instance v0, Lcom/google/c/a/a/b/b/a/o;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    const/4 v2, 0x0

    invoke-direct {v0, v3, v3, v1, v2}, Lcom/google/c/a/a/b/b/a/o;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Z)V

    sput-object v0, Lcom/google/c/a/a/b/b/a/o;->a:Lcom/google/c/a/a/b/b/a/o;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Z)V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    iput-object p1, p0, Lcom/google/c/a/a/b/b/a/o;->c:Ljava/lang/String;

    .line 31
    iput-object p2, p0, Lcom/google/c/a/a/b/b/a/o;->d:Ljava/lang/String;

    .line 32
    iput-object p3, p0, Lcom/google/c/a/a/b/b/a/o;->e:Ljava/util/List;

    .line 33
    iput-boolean p4, p0, Lcom/google/c/a/a/b/b/a/o;->b:Z

    .line 34
    return-void
.end method


# virtual methods
.method public final a()Z
    .locals 1

    .prologue
    .line 41
    iget-boolean v0, p0, Lcom/google/c/a/a/b/b/a/o;->b:Z

    return v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Lcom/google/c/a/a/b/b/a/o;->c:Ljava/lang/String;

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 63
    iget-object v0, p0, Lcom/google/c/a/a/b/b/a/o;->d:Ljava/lang/String;

    return-object v0
.end method

.method public final d()Ljava/util/List;
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lcom/google/c/a/a/b/b/a/o;->e:Ljava/util/List;

    return-object v0
.end method
