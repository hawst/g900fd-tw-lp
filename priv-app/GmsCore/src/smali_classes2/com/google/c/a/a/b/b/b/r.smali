.class final Lcom/google/c/a/a/b/b/b/r;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/c/a/a/b/b/b/s;


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static a(Lcom/google/c/a/a/b/b/a/c/n;Ljava/util/List;)Lcom/google/c/b/a/c;
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 53
    if-eqz p0, :cond_0

    .line 54
    new-instance v0, Lcom/google/c/a/a/b/b/a/c/n;

    invoke-virtual {p0}, Lcom/google/c/a/a/b/b/a/c/n;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/c/a/a/b/b/a/c/n;->d()Z

    move-result v2

    invoke-virtual {p0}, Lcom/google/c/a/a/b/b/a/c/n;->e()Z

    move-result v3

    invoke-direct {v0, v1, p1, v2, v3}, Lcom/google/c/a/a/b/b/a/c/n;-><init>(Ljava/lang/String;Ljava/util/List;ZZ)V

    .line 62
    :goto_0
    return-object v0

    .line 59
    :cond_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-ne v0, v3, :cond_1

    .line 60
    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/c/b/a/c;

    goto :goto_0

    .line 62
    :cond_1
    new-instance v0, Lcom/google/c/a/a/b/b/a/c/n;

    const-string v1, ""

    invoke-direct {v0, v1, p1, v2, v3}, Lcom/google/c/a/a/b/b/a/c/n;-><init>(Ljava/lang/String;Ljava/util/List;ZZ)V

    goto :goto_0
.end method


# virtual methods
.method public final a(Lcom/google/c/a/a/b/b/b/y;Lcom/google/c/a/a/b/b/b/y;)V
    .locals 6

    .prologue
    const/4 v3, 0x0

    .line 24
    invoke-interface {p1}, Lcom/google/c/a/a/b/b/b/y;->c()Lcom/google/c/b/a/c;

    move-result-object v0

    .line 25
    invoke-interface {p2}, Lcom/google/c/a/a/b/b/b/y;->c()Lcom/google/c/b/a/c;

    move-result-object v1

    .line 26
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 27
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 30
    instance-of v2, v0, Lcom/google/c/a/a/b/b/a/c/n;

    if-eqz v2, :cond_0

    .line 31
    check-cast v0, Lcom/google/c/a/a/b/b/a/c/n;

    .line 32
    invoke-virtual {v0}, Lcom/google/c/a/a/b/b/a/c/n;->c()Ljava/util/List;

    move-result-object v2

    invoke-interface {v4, v2}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    move-object v2, v0

    .line 37
    :goto_0
    instance-of v0, v1, Lcom/google/c/a/a/b/b/a/c/n;

    if-eqz v0, :cond_1

    move-object v0, v1

    .line 38
    check-cast v0, Lcom/google/c/a/a/b/b/a/c/n;

    .line 39
    invoke-virtual {v0}, Lcom/google/c/a/a/b/b/a/c/n;->c()Ljava/util/List;

    move-result-object v1

    invoke-interface {v5, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 44
    :goto_1
    invoke-static {v4, v5}, Lcom/google/c/a/a/b/b/b/u;->a(Ljava/util/List;Ljava/util/List;)V

    .line 45
    invoke-static {v2, v4}, Lcom/google/c/a/a/b/b/b/r;->a(Lcom/google/c/a/a/b/b/a/c/n;Ljava/util/List;)Lcom/google/c/b/a/c;

    move-result-object v1

    .line 46
    invoke-static {v0, v5}, Lcom/google/c/a/a/b/b/b/r;->a(Lcom/google/c/a/a/b/b/a/c/n;Ljava/util/List;)Lcom/google/c/b/a/c;

    move-result-object v0

    .line 48
    invoke-interface {p1, v1}, Lcom/google/c/a/a/b/b/b/y;->a(Lcom/google/c/b/a/c;)V

    .line 49
    invoke-interface {p2, v0}, Lcom/google/c/a/a/b/b/b/y;->a(Lcom/google/c/b/a/c;)V

    .line 50
    return-void

    .line 35
    :cond_0
    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move-object v2, v3

    goto :goto_0

    .line 42
    :cond_1
    invoke-interface {v5, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move-object v0, v3

    goto :goto_1
.end method
