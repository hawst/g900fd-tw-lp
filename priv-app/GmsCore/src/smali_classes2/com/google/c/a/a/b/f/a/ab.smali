.class public final enum Lcom/google/c/a/a/b/f/a/ab;
.super Ljava/lang/Enum;
.source "SourceFile"

# interfaces
.implements Lcom/google/c/a/a/b/f/a/k;


# static fields
.field public static final enum a:Lcom/google/c/a/a/b/f/a/ab;

.field public static final enum b:Lcom/google/c/a/a/b/f/a/ab;

.field public static final enum c:Lcom/google/c/a/a/b/f/a/ab;

.field private static final synthetic e:[Lcom/google/c/a/a/b/f/a/ab;


# instance fields
.field private d:I


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 21
    new-instance v0, Lcom/google/c/a/a/b/f/a/ab;

    const-string v1, "ID"

    invoke-direct {v0, v1, v2, v2}, Lcom/google/c/a/a/b/f/a/ab;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/c/a/a/b/f/a/ab;->a:Lcom/google/c/a/a/b/f/a/ab;

    .line 22
    new-instance v0, Lcom/google/c/a/a/b/f/a/ab;

    const-string v1, "INDEX"

    invoke-direct {v0, v1, v3, v3}, Lcom/google/c/a/a/b/f/a/ab;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/c/a/a/b/f/a/ab;->b:Lcom/google/c/a/a/b/f/a/ab;

    .line 23
    new-instance v0, Lcom/google/c/a/a/b/f/a/ab;

    const-string v1, "VALUES"

    invoke-direct {v0, v1, v4, v4}, Lcom/google/c/a/a/b/f/a/ab;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/c/a/a/b/f/a/ab;->c:Lcom/google/c/a/a/b/f/a/ab;

    .line 20
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/google/c/a/a/b/f/a/ab;

    sget-object v1, Lcom/google/c/a/a/b/f/a/ab;->a:Lcom/google/c/a/a/b/f/a/ab;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/c/a/a/b/f/a/ab;->b:Lcom/google/c/a/a/b/f/a/ab;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/c/a/a/b/f/a/ab;->c:Lcom/google/c/a/a/b/f/a/ab;

    aput-object v1, v0, v4

    sput-object v0, Lcom/google/c/a/a/b/f/a/ab;->e:[Lcom/google/c/a/a/b/f/a/ab;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 28
    iput p3, p0, Lcom/google/c/a/a/b/f/a/ab;->d:I

    .line 29
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/c/a/a/b/f/a/ab;
    .locals 1

    .prologue
    .line 20
    const-class v0, Lcom/google/c/a/a/b/f/a/ab;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/c/a/a/b/f/a/ab;

    return-object v0
.end method

.method public static values()[Lcom/google/c/a/a/b/f/a/ab;
    .locals 1

    .prologue
    .line 20
    sget-object v0, Lcom/google/c/a/a/b/f/a/ab;->e:[Lcom/google/c/a/a/b/f/a/ab;

    invoke-virtual {v0}, [Lcom/google/c/a/a/b/f/a/ab;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/c/a/a/b/f/a/ab;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 33
    iget v0, p0, Lcom/google/c/a/a/b/f/a/ab;->d:I

    return v0
.end method
