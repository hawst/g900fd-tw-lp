.class public final Lcom/google/c/a/a/b/b/a/b/w;
.super Lcom/google/c/b/a/b;
.source "SourceFile"

# interfaces
.implements Lcom/google/c/a/a/b/b/a/b;


# instance fields
.field final a:Lcom/google/c/a/a/b/b/a/b/ab;

.field private final b:Ljava/util/List;

.field private final c:Z

.field private d:Z

.field private final e:Z

.field private final f:Lcom/google/c/a/a/b/b/a/b/ad;

.field private final g:Lcom/google/c/a/a/b/b/a/o;

.field private h:Lcom/google/c/a/a/b/b/a/b/x;

.field private final i:I


# direct methods
.method public constructor <init>()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 71
    new-instance v0, Lcom/google/c/a/a/b/b/a/b/u;

    invoke-direct {v0}, Lcom/google/c/a/a/b/b/a/b/u;-><init>()V

    invoke-virtual {v0}, Lcom/google/c/a/a/b/b/a/b/u;->a()Lcom/google/c/a/a/b/b/a/b/t;

    move-result-object v0

    new-instance v1, Lcom/google/c/a/a/b/b/a/o;

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    const/4 v3, 0x1

    invoke-direct {v1, v4, v4, v2, v3}, Lcom/google/c/a/a/b/b/a/o;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Z)V

    invoke-direct {p0, v0, v1}, Lcom/google/c/a/a/b/b/a/b/w;-><init>(Lcom/google/c/a/a/b/b/a/b/t;Lcom/google/c/a/a/b/b/a/o;)V

    .line 73
    return-void
.end method

.method public constructor <init>(Lcom/google/c/a/a/b/b/a/b/t;Lcom/google/c/a/a/b/b/a/o;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 54
    invoke-direct {p0}, Lcom/google/c/b/a/b;-><init>()V

    .line 40
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/c/a/a/b/b/a/b/w;->b:Ljava/util/List;

    .line 43
    iput-boolean v4, p0, Lcom/google/c/a/a/b/b/a/b/w;->d:Z

    .line 55
    new-instance v0, Lcom/google/c/a/a/b/b/a/b/ab;

    new-instance v1, Lcom/google/c/a/a/b/b/a/b/aa;

    invoke-interface {p1}, Lcom/google/c/a/a/b/b/a/b/t;->f()Z

    move-result v2

    invoke-interface {p1}, Lcom/google/c/a/a/b/b/a/b/t;->e()Ljava/util/Set;

    move-result-object v3

    invoke-direct {v1, p0, v2, v3}, Lcom/google/c/a/a/b/b/a/b/aa;-><init>(Lcom/google/c/a/a/b/b/a/b/w;ZLjava/util/Set;)V

    invoke-direct {v0, v1}, Lcom/google/c/a/a/b/b/a/b/ab;-><init>(Lcom/google/c/a/a/b/b/a/b/aa;)V

    iput-object v0, p0, Lcom/google/c/a/a/b/b/a/b/w;->a:Lcom/google/c/a/a/b/b/a/b/ab;

    .line 57
    invoke-interface {p1}, Lcom/google/c/a/a/b/b/a/b/t;->b()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/c/a/a/b/b/a/b/w;->c:Z

    .line 58
    invoke-interface {p1}, Lcom/google/c/a/a/b/b/a/b/t;->g()I

    move-result v0

    iput v0, p0, Lcom/google/c/a/a/b/b/a/b/w;->i:I

    .line 59
    invoke-interface {p1}, Lcom/google/c/a/a/b/b/a/b/t;->h()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/c/a/a/b/b/a/b/w;->e:Z

    .line 60
    new-instance v0, Lcom/google/c/a/a/b/b/a/b/ad;

    invoke-interface {p1}, Lcom/google/c/a/a/b/b/a/b/t;->c()Ljava/util/List;

    move-result-object v1

    invoke-interface {p1}, Lcom/google/c/a/a/b/b/a/b/t;->d()Ljava/util/List;

    move-result-object v2

    invoke-direct {v0, p0, v1, v2}, Lcom/google/c/a/a/b/b/a/b/ad;-><init>(Lcom/google/c/a/a/b/b/a/b;Ljava/util/List;Ljava/util/List;)V

    iput-object v0, p0, Lcom/google/c/a/a/b/b/a/b/w;->f:Lcom/google/c/a/a/b/b/a/b/ad;

    .line 61
    iput-object p2, p0, Lcom/google/c/a/a/b/b/a/b/w;->g:Lcom/google/c/a/a/b/b/a/o;

    .line 62
    iget-object v0, p0, Lcom/google/c/a/a/b/b/a/b/w;->f:Lcom/google/c/a/a/b/b/a/b/ad;

    iput-boolean v4, v0, Lcom/google/c/a/a/b/b/a/b/ad;->f:Z

    .line 63
    invoke-interface {p1}, Lcom/google/c/a/a/b/b/a/b/t;->a()Ljava/lang/Iterable;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/c/a/a/b/b/a/b/w;->a(Ljava/lang/Iterable;)V

    .line 64
    iget-object v0, p0, Lcom/google/c/a/a/b/b/a/b/w;->f:Lcom/google/c/a/a/b/b/a/b/ad;

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/google/c/a/a/b/b/a/b/ad;->f:Z

    .line 65
    return-void
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/c/a/a/b/b/a/g;
    .locals 3

    .prologue
    .line 252
    new-instance v0, Lcom/google/c/a/a/b/b/a/c/e;

    invoke-direct {v0, p2, p1}, Lcom/google/c/a/a/b/b/a/c/e;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/c/a/a/b/b/a/b/w;->g:Lcom/google/c/a/a/b/b/a/o;

    sget-object v2, Lcom/google/c/a/a/b/b/a/k;->a:Lcom/google/c/a/a/b/b/a/k;

    invoke-virtual {p0, v0, v1, v2}, Lcom/google/c/a/a/b/b/a/b/w;->a(Lcom/google/c/b/a/c;Lcom/google/c/a/a/b/b/a/o;Lcom/google/c/a/a/b/b/a/k;)Lcom/google/c/a/a/b/b/a/a;

    .line 253
    invoke-virtual {p0, p2}, Lcom/google/c/a/a/b/b/a/b/w;->c(Ljava/lang/String;)Lcom/google/c/a/a/b/b/a/b/a;

    move-result-object v0

    return-object v0
.end method

.method private a(Lcom/google/c/a/a/b/b/a/a;)V
    .locals 4

    .prologue
    .line 188
    iget-object v0, p0, Lcom/google/c/a/a/b/b/a/b/w;->h:Lcom/google/c/a/a/b/b/a/b/x;

    invoke-virtual {v0}, Lcom/google/c/a/a/b/b/a/b/x;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 199
    :goto_0
    return-void

    .line 191
    :cond_0
    invoke-virtual {p1}, Lcom/google/c/a/a/b/b/a/a;->e()Lcom/google/c/a/a/b/b/a/c/p;

    move-result-object v1

    .line 192
    if-eqz v1, :cond_1

    .line 193
    iget-object v0, p0, Lcom/google/c/a/a/b/b/a/b/w;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/c/a/a/b/b/a/a/c;

    .line 194
    invoke-virtual {p1}, Lcom/google/c/a/a/b/b/a/a;->c()Lcom/google/c/a/a/b/b/a/q;

    move-result-object v3

    invoke-interface {v0, v1, v3}, Lcom/google/c/a/a/b/b/a/a/c;->a(Lcom/google/c/a/a/b/b/a/c/p;Lcom/google/c/a/a/b/b/a/q;)V

    goto :goto_1

    .line 198
    :cond_1
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/c/a/a/b/b/a/b/w;->h:Lcom/google/c/a/a/b/b/a/b/x;

    goto :goto_0
.end method

.method private a(Ljava/lang/Iterable;)V
    .locals 6

    .prologue
    const/4 v4, 0x0

    .line 84
    new-instance v0, Lcom/google/c/a/a/b/b/a/b/x;

    iget-object v1, p0, Lcom/google/c/a/a/b/b/a/b/w;->a:Lcom/google/c/a/a/b/b/a/b/ab;

    iget-object v2, p0, Lcom/google/c/a/a/b/b/a/b/w;->f:Lcom/google/c/a/a/b/b/a/b/ad;

    sget-object v3, Lcom/google/c/a/a/b/b/a/o;->a:Lcom/google/c/a/a/b/b/a/o;

    sget-object v5, Lcom/google/c/a/a/b/b/a/k;->a:Lcom/google/c/a/a/b/b/a/k;

    invoke-direct/range {v0 .. v5}, Lcom/google/c/a/a/b/b/a/b/x;-><init>(Lcom/google/c/a/a/b/b/a/b/ab;Lcom/google/c/a/a/b/b/a/b/ad;Lcom/google/c/a/a/b/b/a/o;ZLcom/google/c/a/a/b/b/a/k;)V

    .line 86
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/c/b/a/c;

    .line 87
    instance-of v3, v1, Lcom/google/c/a/a/b/b/a/c/p;

    if-eqz v3, :cond_0

    .line 88
    check-cast v1, Lcom/google/c/a/a/b/b/a/c/p;

    invoke-virtual {v0, v1}, Lcom/google/c/a/a/b/b/a/b/x;->a(Lcom/google/c/a/a/b/b/a/c/p;)Lcom/google/c/a/a/b/b/a/a;

    .line 89
    iput-boolean v4, p0, Lcom/google/c/a/a/b/b/a/b/w;->d:Z

    goto :goto_0

    .line 92
    :cond_1
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/c/b/a/c;Lcom/google/c/a/a/b/b/a/k;)Lcom/google/c/a/a/b/b/a/a;
    .locals 1

    .prologue
    .line 306
    iget-object v0, p0, Lcom/google/c/a/a/b/b/a/b/w;->g:Lcom/google/c/a/a/b/b/a/o;

    invoke-virtual {p0, p1, v0, p2}, Lcom/google/c/a/a/b/b/a/b/w;->a(Lcom/google/c/b/a/c;Lcom/google/c/a/a/b/b/a/o;Lcom/google/c/a/a/b/b/a/k;)Lcom/google/c/a/a/b/b/a/a;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/c/b/a/c;Lcom/google/c/a/a/b/b/a/o;Lcom/google/c/a/a/b/b/a/k;)Lcom/google/c/a/a/b/b/a/a;
    .locals 6

    .prologue
    const/4 v4, 0x1

    .line 150
    instance-of v0, p1, Lcom/google/c/a/a/b/b/a/c/p;

    if-eqz v0, :cond_7

    .line 151
    invoke-virtual {p2}, Lcom/google/c/a/a/b/b/a/o;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 152
    iget-boolean v0, p0, Lcom/google/c/a/a/b/b/a/b/w;->c:Z

    const-string v1, "Unable to apply local mutation because document is in read-only mode."

    invoke-static {v0, v1}, Lcom/google/c/a/a/b/h/c;->b(ZLjava/lang/String;)V

    .line 158
    :goto_0
    iget-object v0, p0, Lcom/google/c/a/a/b/b/a/b/w;->h:Lcom/google/c/a/a/b/b/a/b/x;

    if-nez v0, :cond_4

    .line 159
    new-instance v0, Lcom/google/c/a/a/b/b/a/b/x;

    iget-object v1, p0, Lcom/google/c/a/a/b/b/a/b/w;->a:Lcom/google/c/a/a/b/b/a/b/ab;

    iget-object v2, p0, Lcom/google/c/a/a/b/b/a/b/w;->f:Lcom/google/c/a/a/b/b/a/b/ad;

    move-object v3, p2

    move-object v5, p3

    invoke-direct/range {v0 .. v5}, Lcom/google/c/a/a/b/b/a/b/x;-><init>(Lcom/google/c/a/a/b/b/a/b/ab;Lcom/google/c/a/a/b/b/a/b/ad;Lcom/google/c/a/a/b/b/a/o;ZLcom/google/c/a/a/b/b/a/k;)V

    iput-object v0, p0, Lcom/google/c/a/a/b/b/a/b/w;->h:Lcom/google/c/a/a/b/b/a/b/x;

    .line 167
    :cond_0
    iget-object v0, p0, Lcom/google/c/a/a/b/b/a/b/w;->a:Lcom/google/c/a/a/b/b/a/b/ab;

    iget v1, v0, Lcom/google/c/a/a/b/b/a/b/ab;->g:I

    .line 168
    iget-object v0, p0, Lcom/google/c/a/a/b/b/a/b/w;->h:Lcom/google/c/a/a/b/b/a/b/x;

    check-cast p1, Lcom/google/c/a/a/b/b/a/c/p;

    invoke-virtual {v0, p1}, Lcom/google/c/a/a/b/b/a/b/x;->a(Lcom/google/c/a/a/b/b/a/c/p;)Lcom/google/c/a/a/b/b/a/a;

    move-result-object v0

    .line 169
    invoke-virtual {p2}, Lcom/google/c/a/a/b/b/a/o;->a()Z

    move-result v2

    if-nez v2, :cond_1

    iget-boolean v2, p0, Lcom/google/c/a/a/b/b/a/b/w;->e:Z

    if-eqz v2, :cond_6

    .line 170
    :cond_1
    iget-object v2, p0, Lcom/google/c/a/a/b/b/a/b/w;->a:Lcom/google/c/a/a/b/b/a/b/ab;

    iget v2, v2, Lcom/google/c/a/a/b/b/a/b/ab;->g:I

    .line 171
    iget-object v3, p0, Lcom/google/c/a/a/b/b/a/b/w;->a:Lcom/google/c/a/a/b/b/a/b/ab;

    iget v3, v3, Lcom/google/c/a/a/b/b/a/b/ab;->g:I

    iget v4, p0, Lcom/google/c/a/a/b/b/a/b/w;->i:I

    if-le v3, v4, :cond_6

    if-le v2, v1, :cond_6

    .line 172
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "Model size limit has been exceeded. Used: "

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " bytes; Limit: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/google/c/a/a/b/b/a/b/w;->i:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " bytes"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 155
    :cond_2
    iget-object v0, p0, Lcom/google/c/a/a/b/b/a/b/w;->h:Lcom/google/c/a/a/b/b/a/b/x;

    if-nez v0, :cond_3

    move v0, v4

    :goto_1
    const-string v1, "Cannot receive a server mutation in the middle of a compound operation."

    invoke-static {v0, v1}, Lcom/google/c/a/a/b/h/c;->b(ZLjava/lang/String;)V

    goto :goto_0

    :cond_3
    const/4 v0, 0x0

    goto :goto_1

    .line 162
    :cond_4
    invoke-virtual {p3}, Lcom/google/c/a/a/b/b/a/k;->a()Z

    move-result v0

    if-nez v0, :cond_5

    invoke-virtual {p3}, Lcom/google/c/a/a/b/b/a/k;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 163
    :cond_5
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Undo and Redo calls are not permitted inside a compound operation."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 176
    :cond_6
    invoke-direct {p0, v0}, Lcom/google/c/a/a/b/b/a/b/w;->a(Lcom/google/c/a/a/b/b/a/a;)V

    .line 180
    :goto_2
    return-object v0

    .line 178
    :cond_7
    instance-of v0, p1, Lcom/google/c/b/a/d;

    if-eqz v0, :cond_8

    .line 180
    invoke-static {}, Lcom/google/c/a/a/b/b/a/a;->f()Lcom/google/c/a/a/b/b/a/a;

    move-result-object v0

    goto :goto_2

    .line 182
    :cond_8
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unsupported mutation type: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final synthetic a(Ljava/lang/String;)Lcom/google/c/a/a/b/b/a/g;
    .locals 1

    .prologue
    .line 34
    invoke-virtual {p0, p1}, Lcom/google/c/a/a/b/b/a/b/w;->c(Ljava/lang/String;)Lcom/google/c/a/a/b/b/a/b/a;

    move-result-object v0

    return-object v0
.end method

.method public final a()Ljava/util/List;
    .locals 10

    .prologue
    .line 206
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 207
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 208
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 209
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 210
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 212
    iget-object v0, p0, Lcom/google/c/a/a/b/b/a/b/w;->a:Lcom/google/c/a/a/b/b/a/b/ab;

    iget-object v0, v0, Lcom/google/c/a/a/b/b/a/b/ab;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 213
    invoke-virtual {p0, v0}, Lcom/google/c/a/a/b/b/a/b/w;->c(Ljava/lang/String;)Lcom/google/c/a/a/b/b/a/b/a;

    move-result-object v7

    .line 214
    new-instance v8, Lcom/google/c/a/a/b/b/a/c/e;

    invoke-interface {v7}, Lcom/google/c/a/a/b/b/a/g;->f()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v8, v0, v9}, Lcom/google/c/a/a/b/b/a/c/e;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 215
    invoke-interface {v7}, Lcom/google/c/a/a/b/b/a/g;->k()Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Lcom/google/c/a/a/b/b/a/c/q;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/c/b/a/c;

    .line 216
    instance-of v8, v0, Lcom/google/c/a/a/b/b/a/c/i;

    if-eqz v8, :cond_1

    .line 217
    check-cast v0, Lcom/google/c/a/a/b/b/a/c/i;

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 218
    :cond_1
    instance-of v8, v0, Lcom/google/c/a/a/b/b/a/c/t;

    if-eqz v8, :cond_2

    .line 219
    check-cast v0, Lcom/google/c/a/a/b/b/a/c/t;

    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 220
    :cond_2
    instance-of v8, v0, Lcom/google/c/a/a/b/b/a/c/h;

    if-eqz v8, :cond_3

    .line 221
    check-cast v0, Lcom/google/c/a/a/b/b/a/c/h;

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 223
    :cond_3
    new-instance v1, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unexpected mutation type "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 227
    :cond_4
    new-instance v0, Ljava/util/ArrayList;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v6

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v7

    add-int/2addr v6, v7

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v7

    add-int/2addr v6, v7

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v7

    add-int/2addr v6, v7

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v7

    add-int/2addr v6, v7

    invoke-direct {v0, v6}, Ljava/util/ArrayList;-><init>(I)V

    .line 229
    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 230
    invoke-interface {v0, v4}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 231
    invoke-interface {v0, v5}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 232
    invoke-interface {v0, v2}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 233
    invoke-interface {v0, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 234
    return-object v0
.end method

.method public final a(Lcom/google/c/a/a/b/b/a/a/c;)V
    .locals 1

    .prologue
    .line 101
    iget-object v0, p0, Lcom/google/c/a/a/b/b/a/b/w;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 102
    return-void
.end method

.method final a(Ljava/lang/String;Lcom/google/c/a/a/b/b/a/b/z;)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v4, 0x1

    .line 132
    iget-object v0, p0, Lcom/google/c/a/a/b/b/a/b/w;->h:Lcom/google/c/a/a/b/b/a/b/x;

    if-nez v0, :cond_0

    .line 133
    new-instance v0, Lcom/google/c/a/a/b/b/a/b/x;

    iget-object v1, p0, Lcom/google/c/a/a/b/b/a/b/w;->a:Lcom/google/c/a/a/b/b/a/b/ab;

    iget-object v2, p0, Lcom/google/c/a/a/b/b/a/b/w;->f:Lcom/google/c/a/a/b/b/a/b/ad;

    iget-object v3, p0, Lcom/google/c/a/a/b/b/a/b/w;->g:Lcom/google/c/a/a/b/b/a/o;

    sget-object v5, Lcom/google/c/a/a/b/b/a/k;->a:Lcom/google/c/a/a/b/b/a/k;

    invoke-direct/range {v0 .. v5}, Lcom/google/c/a/a/b/b/a/b/x;-><init>(Lcom/google/c/a/a/b/b/a/b/ab;Lcom/google/c/a/a/b/b/a/b/ad;Lcom/google/c/a/a/b/b/a/o;ZLcom/google/c/a/a/b/b/a/k;)V

    iput-object v0, p0, Lcom/google/c/a/a/b/b/a/b/w;->h:Lcom/google/c/a/a/b/b/a/b/x;

    .line 136
    :cond_0
    iget-object v1, p0, Lcom/google/c/a/a/b/b/a/b/w;->h:Lcom/google/c/a/a/b/b/a/b/x;

    iget-object v0, v1, Lcom/google/c/a/a/b/b/a/b/x;->c:Lcom/google/c/a/a/b/b/a/c/p;

    if-nez v0, :cond_3

    move v0, v4

    :goto_0
    const-string v2, "Cannot begin a compound operation after applying changes."

    invoke-static {v0, v2}, Lcom/google/c/a/a/b/h/c;->b(ZLjava/lang/String;)V

    invoke-virtual {v1, p1}, Lcom/google/c/a/a/b/b/a/b/x;->a(Ljava/lang/String;)V

    sget-object v0, Lcom/google/c/a/a/b/b/a/b/z;->a:Lcom/google/c/a/a/b/b/a/b/z;

    if-ne p2, v0, :cond_1

    invoke-virtual {v1}, Lcom/google/c/a/a/b/b/a/b/x;->a()Z

    move-result v0

    if-nez v0, :cond_4

    move v0, v4

    :goto_1
    const-string v2, "Creation compound operations cannot be nested inside another compound operation."

    invoke-static {v0, v2}, Lcom/google/c/a/a/b/h/c;->b(ZLjava/lang/String;)V

    :cond_1
    invoke-virtual {v1}, Lcom/google/c/a/a/b/b/a/b/x;->a()Z

    move-result v0

    if-eqz v0, :cond_5

    iget-boolean v0, v1, Lcom/google/c/a/a/b/b/a/b/x;->d:Z

    if-eqz v0, :cond_2

    invoke-virtual {p2}, Lcom/google/c/a/a/b/b/a/b/z;->a()Z

    move-result v0

    const-string v2, "A non-undoable compound operation cannot be nested in an undoable compound operation"

    invoke-static {v0, v2}, Lcom/google/c/a/a/b/h/c;->b(ZLjava/lang/String;)V

    :cond_2
    :goto_2
    iget-object v1, v1, Lcom/google/c/a/a/b/b/a/b/x;->b:Ljava/util/List;

    new-instance v2, Lcom/google/c/a/a/b/b/a/c/o;

    invoke-direct {v2}, Lcom/google/c/a/a/b/b/a/c/o;-><init>()V

    const-string v0, "Compound operation name cannot be null."

    invoke-static {p1, v0}, Lcom/google/c/a/a/b/h/c;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, v2, Lcom/google/c/a/a/b/b/a/c/o;->a:Ljava/lang/String;

    sget-object v0, Lcom/google/c/a/a/b/b/a/b/z;->a:Lcom/google/c/a/a/b/b/a/b/z;

    if-ne p2, v0, :cond_6

    :goto_3
    iput-boolean v4, v2, Lcom/google/c/a/a/b/b/a/c/o;->c:Z

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 137
    return-void

    :cond_3
    move v0, v6

    .line 136
    goto :goto_0

    :cond_4
    move v0, v6

    goto :goto_1

    :cond_5
    invoke-virtual {p2}, Lcom/google/c/a/a/b/b/a/b/z;->a()Z

    move-result v0

    iput-boolean v0, v1, Lcom/google/c/a/a/b/b/a/b/x;->d:Z

    goto :goto_2

    :cond_6
    move v4, v6

    goto :goto_3
.end method

.method public final a(Ljava/lang/String;Z)V
    .locals 1

    .prologue
    .line 126
    if-eqz p2, :cond_0

    sget-object v0, Lcom/google/c/a/a/b/b/a/b/z;->b:Lcom/google/c/a/a/b/b/a/b/z;

    .line 128
    :goto_0
    invoke-virtual {p0, p1, v0}, Lcom/google/c/a/a/b/b/a/b/w;->a(Ljava/lang/String;Lcom/google/c/a/a/b/b/a/b/z;)V

    .line 129
    return-void

    .line 126
    :cond_0
    sget-object v0, Lcom/google/c/a/a/b/b/a/b/z;->c:Lcom/google/c/a/a/b/b/a/b/z;

    goto :goto_0
.end method

.method public final b(Ljava/lang/String;)Lcom/google/c/a/a/b/b/a/g;
    .locals 3

    .prologue
    .line 248
    iget-object v1, p0, Lcom/google/c/a/a/b/b/a/b/w;->a:Lcom/google/c/a/a/b/b/a/b/ab;

    invoke-static {}, Lcom/google/c/a/a/b/h/a;->a()Ljava/lang/String;

    move-result-object v0

    :goto_0
    iget-object v2, v1, Lcom/google/c/a/a/b/b/a/b/ab;->a:Ljava/util/Map;

    invoke-interface {v2, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, v1, Lcom/google/c/a/a/b/b/a/b/ab;->b:Ljava/util/Map;

    invoke-interface {v2, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    :cond_0
    invoke-static {}, Lcom/google/c/a/a/b/h/a;->a()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_1
    invoke-direct {p0, p1, v0}, Lcom/google/c/a/a/b/b/a/b/w;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/c/a/a/b/b/a/g;

    move-result-object v0

    return-object v0
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 121
    const-string v0, ""

    sget-object v1, Lcom/google/c/a/a/b/b/a/b/z;->a:Lcom/google/c/a/a/b/b/a/b/z;

    invoke-virtual {p0, v0, v1}, Lcom/google/c/a/a/b/b/a/b/w;->a(Ljava/lang/String;Lcom/google/c/a/a/b/b/a/b/z;)V

    .line 122
    return-void
.end method

.method public final c()Lcom/google/c/a/a/b/b/a/a;
    .locals 4

    .prologue
    .line 141
    iget-object v0, p0, Lcom/google/c/a/a/b/b/a/b/w;->h:Lcom/google/c/a/a/b/b/a/b/x;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "Not in a compound operation."

    invoke-static {v0, v1}, Lcom/google/c/a/a/b/h/c;->b(ZLjava/lang/String;)V

    .line 142
    iget-object v1, p0, Lcom/google/c/a/a/b/b/a/b/w;->h:Lcom/google/c/a/a/b/b/a/b/x;

    invoke-virtual {v1}, Lcom/google/c/a/a/b/b/a/b/x;->a()Z

    move-result v0

    const-string v2, "No compound operation to end."

    invoke-static {v0, v2}, Lcom/google/c/a/a/b/h/c;->b(ZLjava/lang/String;)V

    invoke-virtual {v1}, Lcom/google/c/a/a/b/b/a/b/x;->c()V

    iget-object v0, v1, Lcom/google/c/a/a/b/b/a/b/x;->b:Ljava/util/List;

    iget-object v2, v1, Lcom/google/c/a/a/b/b/a/b/x;->b:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-interface {v0, v2}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/c/a/a/b/b/a/c/o;

    invoke-virtual {v0}, Lcom/google/c/a/a/b/b/a/c/o;->a()Lcom/google/c/a/a/b/b/a/c/n;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/c/a/a/b/b/a/c/n;->c()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, v1, Lcom/google/c/a/a/b/b/a/b/x;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    iput-object v2, v1, Lcom/google/c/a/a/b/b/a/b/x;->c:Lcom/google/c/a/a/b/b/a/c/p;

    :goto_1
    invoke-virtual {v1}, Lcom/google/c/a/a/b/b/a/b/x;->a()Z

    move-result v0

    if-nez v0, :cond_2

    invoke-virtual {v1}, Lcom/google/c/a/a/b/b/a/b/x;->b()Lcom/google/c/a/a/b/b/a/a;

    move-result-object v0

    .line 143
    :goto_2
    invoke-direct {p0, v0}, Lcom/google/c/a/a/b/b/a/b/w;->a(Lcom/google/c/a/a/b/b/a/a;)V

    .line 144
    return-object v0

    .line 141
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 142
    :cond_1
    iget-object v0, v1, Lcom/google/c/a/a/b/b/a/b/x;->b:Ljava/util/List;

    iget-object v3, v1, Lcom/google/c/a/a/b/b/a/b/x;->b:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/c/a/a/b/b/a/c/o;

    invoke-virtual {v0, v2}, Lcom/google/c/a/a/b/b/a/c/o;->a(Lcom/google/c/b/a/c;)Lcom/google/c/a/a/b/b/a/c/o;

    goto :goto_1

    :cond_2
    invoke-static {}, Lcom/google/c/a/a/b/b/a/a;->f()Lcom/google/c/a/a/b/b/a/a;

    move-result-object v0

    goto :goto_2
.end method

.method public final c(Ljava/lang/String;)Lcom/google/c/a/a/b/b/a/b/a;
    .locals 1

    .prologue
    .line 106
    iget-object v0, p0, Lcom/google/c/a/a/b/b/a/b/w;->a:Lcom/google/c/a/a/b/b/a/b/ab;

    invoke-virtual {v0, p1}, Lcom/google/c/a/a/b/b/a/b/ab;->a(Ljava/lang/String;)Lcom/google/c/a/a/b/b/a/b/a;

    move-result-object v0

    return-object v0
.end method

.method public final d()Z
    .locals 2

    .prologue
    .line 76
    iget-boolean v0, p0, Lcom/google/c/a/a/b/b/a/b/w;->d:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/c/a/a/b/b/a/b/w;->c:Z

    if-eqz v0, :cond_0

    .line 78
    sget-object v0, Lcom/google/c/a/a/b/b/a/b/c;->a:Lcom/google/c/a/a/b/b/a/b/c;

    invoke-virtual {v0}, Lcom/google/c/a/a/b/b/a/b/c;->a()Ljava/lang/String;

    move-result-object v0

    const-string v1, "root"

    invoke-direct {p0, v0, v1}, Lcom/google/c/a/a/b/b/a/b/w;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/c/a/a/b/b/a/g;

    .line 80
    :cond_0
    iget-boolean v0, p0, Lcom/google/c/a/a/b/b/a/b/w;->d:Z

    return v0
.end method

.method public final e()Z
    .locals 1

    .prologue
    .line 96
    iget-boolean v0, p0, Lcom/google/c/a/a/b/b/a/b/w;->d:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 283
    instance-of v1, p1, Lcom/google/c/a/a/b/b/a/b/w;

    if-nez v1, :cond_0

    .line 287
    :goto_0
    return v0

    .line 286
    :cond_0
    check-cast p1, Lcom/google/c/a/a/b/b/a/b/w;

    .line 287
    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/google/c/a/a/b/b/a/b/w;->a:Lcom/google/c/a/a/b/b/a/b/ab;

    invoke-virtual {v2}, Lcom/google/c/a/a/b/b/a/b/ab;->b()Lcom/google/c/a/a/b/b/a/f;

    move-result-object v2

    aput-object v2, v1, v0

    const/4 v0, 0x1

    iget-object v2, p1, Lcom/google/c/a/a/b/b/a/b/w;->a:Lcom/google/c/a/a/b/b/a/b/ab;

    invoke-virtual {v2}, Lcom/google/c/a/a/b/b/a/b/ab;->b()Lcom/google/c/a/a/b/b/a/f;

    move-result-object v2

    aput-object v2, v1, v0

    invoke-static {v1}, Lcom/google/c/a/a/b/h/b;->a([Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public final f()Lcom/google/c/a/a/b/b/a/l;
    .locals 1

    .prologue
    .line 275
    sget-object v0, Lcom/google/c/a/a/b/b/a/b/c;->d:Lcom/google/c/a/a/b/b/a/b/c;

    invoke-virtual {v0}, Lcom/google/c/a/a/b/b/a/b/c;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/c/a/a/b/b/a/b/w;->b(Ljava/lang/String;)Lcom/google/c/a/a/b/b/a/g;

    move-result-object v0

    check-cast v0, Lcom/google/c/a/a/b/b/a/l;

    return-object v0
.end method

.method public final g()I
    .locals 1

    .prologue
    .line 311
    iget-object v0, p0, Lcom/google/c/a/a/b/b/a/b/w;->a:Lcom/google/c/a/a/b/b/a/b/ab;

    iget v0, v0, Lcom/google/c/a/a/b/b/a/b/ab;->g:I

    return v0
.end method

.method public final h()Lcom/google/c/a/a/b/b/a/a;
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 321
    iget-object v3, p0, Lcom/google/c/a/a/b/b/a/b/w;->f:Lcom/google/c/a/a/b/b/a/b/ad;

    invoke-virtual {v3}, Lcom/google/c/a/a/b/b/a/b/ad;->a()Z

    move-result v0

    const-string v4, "Undo stack is empty."

    invoke-static {v0, v4}, Lcom/google/c/a/a/b/h/c;->b(ZLjava/lang/String;)V

    iget-boolean v0, v3, Lcom/google/c/a/a/b/b/a/b/ad;->d:Z

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    const-string v4, "Recursive undo call."

    invoke-static {v0, v4}, Lcom/google/c/a/a/b/h/c;->b(ZLjava/lang/String;)V

    iget-object v0, v3, Lcom/google/c/a/a/b/b/a/b/ad;->b:Ljava/util/List;

    invoke-virtual {v3, v0, v2}, Lcom/google/c/a/a/b/b/a/b/ad;->a(Ljava/util/List;I)Lcom/google/c/b/a/c;

    move-result-object v0

    iput-boolean v1, v3, Lcom/google/c/a/a/b/b/a/b/ad;->d:Z

    new-instance v4, Lcom/google/c/a/a/b/b/a/k;

    invoke-direct {v4, v1, v2}, Lcom/google/c/a/a/b/b/a/k;-><init>(ZZ)V

    invoke-virtual {v3, v0, v4}, Lcom/google/c/a/a/b/b/a/b/ad;->a(Lcom/google/c/b/a/c;Lcom/google/c/a/a/b/b/a/k;)Lcom/google/c/a/a/b/b/a/a;

    move-result-object v0

    iput-boolean v2, v3, Lcom/google/c/a/a/b/b/a/b/ad;->d:Z

    return-object v0

    :cond_0
    move v0, v2

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    .line 279
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/c/a/a/b/b/a/b/w;->a:Lcom/google/c/a/a/b/b/a/b/ab;

    invoke-virtual {v2}, Lcom/google/c/a/a/b/b/a/b/ab;->b()Lcom/google/c/a/a/b/b/a/f;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final i()Lcom/google/c/a/a/b/b/a/a;
    .locals 5

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 326
    iget-object v3, p0, Lcom/google/c/a/a/b/b/a/b/w;->f:Lcom/google/c/a/a/b/b/a/b/ad;

    invoke-virtual {v3}, Lcom/google/c/a/a/b/b/a/b/ad;->b()Z

    move-result v0

    const-string v4, "Redo stack is empty."

    invoke-static {v0, v4}, Lcom/google/c/a/a/b/h/c;->b(ZLjava/lang/String;)V

    iget-boolean v0, v3, Lcom/google/c/a/a/b/b/a/b/ad;->e:Z

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    const-string v4, "Recursive redo call."

    invoke-static {v0, v4}, Lcom/google/c/a/a/b/h/c;->b(ZLjava/lang/String;)V

    iget-object v0, v3, Lcom/google/c/a/a/b/b/a/b/ad;->c:Ljava/util/List;

    invoke-virtual {v3, v0, v1}, Lcom/google/c/a/a/b/b/a/b/ad;->a(Ljava/util/List;I)Lcom/google/c/b/a/c;

    move-result-object v0

    iput-boolean v1, v3, Lcom/google/c/a/a/b/b/a/b/ad;->e:Z

    new-instance v4, Lcom/google/c/a/a/b/b/a/k;

    invoke-direct {v4, v2, v1}, Lcom/google/c/a/a/b/b/a/k;-><init>(ZZ)V

    invoke-virtual {v3, v0, v4}, Lcom/google/c/a/a/b/b/a/b/ad;->a(Lcom/google/c/b/a/c;Lcom/google/c/a/a/b/b/a/k;)Lcom/google/c/a/a/b/b/a/a;

    move-result-object v0

    iput-boolean v2, v3, Lcom/google/c/a/a/b/b/a/b/ad;->e:Z

    return-object v0

    :cond_0
    move v0, v2

    goto :goto_0
.end method

.method public final j()Z
    .locals 1

    .prologue
    .line 331
    iget-object v0, p0, Lcom/google/c/a/a/b/b/a/b/w;->f:Lcom/google/c/a/a/b/b/a/b/ad;

    invoke-virtual {v0}, Lcom/google/c/a/a/b/b/a/b/ad;->a()Z

    move-result v0

    return v0
.end method

.method public final k()Z
    .locals 1

    .prologue
    .line 336
    iget-object v0, p0, Lcom/google/c/a/a/b/b/a/b/w;->f:Lcom/google/c/a/a/b/b/a/b/ad;

    invoke-virtual {v0}, Lcom/google/c/a/a/b/b/a/b/ad;->b()Z

    move-result v0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 292
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "ModelImpl (root = "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/c/a/a/b/b/a/b/w;->a:Lcom/google/c/a/a/b/b/a/b/ab;

    invoke-virtual {v1}, Lcom/google/c/a/a/b/b/a/b/ab;->b()Lcom/google/c/a/a/b/b/a/f;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
