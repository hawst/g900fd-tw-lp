.class public final Lcom/google/c/a/a/b/f/a/al;
.super Lcom/google/c/a/a/b/f/b;
.source "SourceFile"


# direct methods
.method public constructor <init>(Z)V
    .locals 1

    .prologue
    .line 41
    const-class v0, Lcom/google/c/a/a/b/b/a/c/n;

    invoke-direct {p0, v0, p1}, Lcom/google/c/a/a/b/f/b;-><init>(Ljava/lang/Class;Z)V

    .line 42
    return-void
.end method


# virtual methods
.method protected final synthetic a(Ljava/lang/Object;Ljava/lang/Object;Lcom/google/c/a/a/b/d/b;)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 19
    check-cast p1, Lcom/google/c/a/a/b/b/a/c/n;

    sget-object v0, Lcom/google/c/a/a/b/f/a/am;->a:Lcom/google/c/a/a/b/f/a/am;

    invoke-virtual {p1}, Lcom/google/c/a/a/b/b/a/c/n;->c()Ljava/util/List;

    move-result-object v2

    invoke-virtual {p0, p2, p3, v0, v2}, Lcom/google/c/a/a/b/f/a/al;->a(Ljava/lang/Object;Lcom/google/c/a/a/b/d/b;Lcom/google/c/a/a/b/f/a/k;Ljava/lang/Object;)V

    invoke-virtual {p1}, Lcom/google/c/a/a/b/b/a/c/n;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_0

    move-object v0, v1

    :cond_0
    sget-object v2, Lcom/google/c/a/a/b/f/a/am;->b:Lcom/google/c/a/a/b/f/a/am;

    invoke-virtual {p0, p2, p3, v2, v0}, Lcom/google/c/a/a/b/f/a/al;->a(Ljava/lang/Object;Lcom/google/c/a/a/b/d/b;Lcom/google/c/a/a/b/f/a/k;Ljava/lang/Object;)V

    invoke-virtual {p1}, Lcom/google/c/a/a/b/b/a/c/n;->d()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-nez v2, :cond_1

    :goto_0
    sget-object v0, Lcom/google/c/a/a/b/f/a/am;->c:Lcom/google/c/a/a/b/f/a/am;

    invoke-virtual {p0, p2, p3, v0, v1}, Lcom/google/c/a/a/b/f/a/al;->a(Ljava/lang/Object;Lcom/google/c/a/a/b/d/b;Lcom/google/c/a/a/b/f/a/k;Ljava/lang/Object;)V

    invoke-virtual {p1}, Lcom/google/c/a/a/b/b/a/c/n;->e()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sget-object v1, Lcom/google/c/a/a/b/f/a/am;->d:Lcom/google/c/a/a/b/f/a/am;

    invoke-virtual {p0, p2, p3, v1, v0}, Lcom/google/c/a/a/b/f/a/al;->a(Ljava/lang/Object;Lcom/google/c/a/a/b/d/b;Lcom/google/c/a/a/b/f/a/k;Ljava/lang/Object;)V

    return-void

    :cond_1
    move-object v1, v0

    goto :goto_0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 103
    instance-of v0, p1, Lcom/google/c/a/a/b/b/a/c/n;

    return v0
.end method

.method public final synthetic b(Ljava/lang/Object;Lcom/google/c/a/a/b/d/b;)Ljava/lang/Object;
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 19
    sget-object v0, Lcom/google/c/a/a/b/f/a/am;->a:Lcom/google/c/a/a/b/f/a/am;

    const-class v1, Ljava/util/List;

    invoke-virtual {p0, p1, p2, v0, v1}, Lcom/google/c/a/a/b/f/a/al;->a(Ljava/lang/Object;Lcom/google/c/a/a/b/d/b;Lcom/google/c/a/a/b/f/a/k;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    sget-object v1, Lcom/google/c/a/a/b/f/a/am;->b:Lcom/google/c/a/a/b/f/a/am;

    const-class v2, Ljava/lang/String;

    invoke-virtual {p0, p1, p2, v1, v2}, Lcom/google/c/a/a/b/f/a/al;->a(Ljava/lang/Object;Lcom/google/c/a/a/b/d/b;Lcom/google/c/a/a/b/f/a/k;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    if-nez v1, :cond_3

    const-string v1, ""

    move-object v2, v1

    :goto_0
    sget-object v1, Lcom/google/c/a/a/b/f/a/am;->c:Lcom/google/c/a/a/b/f/a/am;

    const-class v3, Ljava/lang/Boolean;

    invoke-virtual {p0, p1, p2, v1, v3}, Lcom/google/c/a/a/b/f/a/al;->a(Ljava/lang/Object;Lcom/google/c/a/a/b/d/b;Lcom/google/c/a/a/b/f/a/k;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    if-nez v1, :cond_2

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    move-object v3, v1

    :goto_1
    sget-object v1, Lcom/google/c/a/a/b/f/a/am;->d:Lcom/google/c/a/a/b/f/a/am;

    const-class v4, Ljava/lang/Boolean;

    invoke-virtual {p0, p1, p2, v1, v4}, Lcom/google/c/a/a/b/f/a/al;->a(Ljava/lang/Object;Lcom/google/c/a/a/b/d/b;Lcom/google/c/a/a/b/f/a/k;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    if-nez v1, :cond_1

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    move-object v4, v1

    :goto_2
    sget-object v1, Lcom/google/c/a/a/b/f/a/am;->e:Lcom/google/c/a/a/b/f/a/am;

    const-class v5, Ljava/lang/Boolean;

    invoke-virtual {p0, p1, p2, v1, v5}, Lcom/google/c/a/a/b/f/a/al;->a(Ljava/lang/Object;Lcom/google/c/a/a/b/d/b;Lcom/google/c/a/a/b/f/a/k;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    if-nez v1, :cond_0

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    :cond_0
    new-instance v1, Lcom/google/c/a/a/b/b/a/c/o;

    invoke-direct {v1}, Lcom/google/c/a/a/b/b/a/c/o;-><init>()V

    iput-object v2, v1, Lcom/google/c/a/a/b/b/a/c/o;->a:Ljava/lang/String;

    invoke-virtual {v1, v0}, Lcom/google/c/a/a/b/b/a/c/o;->a(Ljava/util/Collection;)Lcom/google/c/a/a/b/b/a/c/o;

    move-result-object v0

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    iput-boolean v1, v0, Lcom/google/c/a/a/b/b/a/c/o;->c:Z

    invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    iput-boolean v1, v0, Lcom/google/c/a/a/b/b/a/c/o;->d:Z

    invoke-virtual {v0}, Lcom/google/c/a/a/b/b/a/c/o;->a()Lcom/google/c/a/a/b/b/a/c/n;

    move-result-object v0

    return-object v0

    :cond_1
    move-object v4, v1

    goto :goto_2

    :cond_2
    move-object v3, v1

    goto :goto_1

    :cond_3
    move-object v2, v1

    goto :goto_0
.end method
