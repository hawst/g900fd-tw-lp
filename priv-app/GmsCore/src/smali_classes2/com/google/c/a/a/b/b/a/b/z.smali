.class public final enum Lcom/google/c/a/a/b/b/a/b/z;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field public static final enum a:Lcom/google/c/a/a/b/b/a/b/z;

.field public static final enum b:Lcom/google/c/a/a/b/b/a/b/z;

.field public static final enum c:Lcom/google/c/a/a/b/b/a/b/z;

.field private static final synthetic e:[Lcom/google/c/a/a/b/b/a/b/z;


# instance fields
.field private final d:Z


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 358
    new-instance v0, Lcom/google/c/a/a/b/b/a/b/z;

    const-string v1, "CREATION"

    invoke-direct {v0, v1, v2, v2}, Lcom/google/c/a/a/b/b/a/b/z;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, Lcom/google/c/a/a/b/b/a/b/z;->a:Lcom/google/c/a/a/b/b/a/b/z;

    .line 363
    new-instance v0, Lcom/google/c/a/a/b/b/a/b/z;

    const-string v1, "NORMAL"

    invoke-direct {v0, v1, v3, v3}, Lcom/google/c/a/a/b/b/a/b/z;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, Lcom/google/c/a/a/b/b/a/b/z;->b:Lcom/google/c/a/a/b/b/a/b/z;

    .line 368
    new-instance v0, Lcom/google/c/a/a/b/b/a/b/z;

    const-string v1, "NORMAL_NO_UNDO"

    invoke-direct {v0, v1, v4, v2}, Lcom/google/c/a/a/b/b/a/b/z;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, Lcom/google/c/a/a/b/b/a/b/z;->c:Lcom/google/c/a/a/b/b/a/b/z;

    .line 353
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/google/c/a/a/b/b/a/b/z;

    sget-object v1, Lcom/google/c/a/a/b/b/a/b/z;->a:Lcom/google/c/a/a/b/b/a/b/z;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/c/a/a/b/b/a/b/z;->b:Lcom/google/c/a/a/b/b/a/b/z;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/c/a/a/b/b/a/b/z;->c:Lcom/google/c/a/a/b/b/a/b/z;

    aput-object v1, v0, v4

    sput-object v0, Lcom/google/c/a/a/b/b/a/b/z;->e:[Lcom/google/c/a/a/b/b/a/b/z;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IZ)V
    .locals 0

    .prologue
    .line 372
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 373
    iput-boolean p3, p0, Lcom/google/c/a/a/b/b/a/b/z;->d:Z

    .line 374
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/c/a/a/b/b/a/b/z;
    .locals 1

    .prologue
    .line 353
    const-class v0, Lcom/google/c/a/a/b/b/a/b/z;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/c/a/a/b/b/a/b/z;

    return-object v0
.end method

.method public static values()[Lcom/google/c/a/a/b/b/a/b/z;
    .locals 1

    .prologue
    .line 353
    sget-object v0, Lcom/google/c/a/a/b/b/a/b/z;->e:[Lcom/google/c/a/a/b/b/a/b/z;

    invoke-virtual {v0}, [Lcom/google/c/a/a/b/b/a/b/z;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/c/a/a/b/b/a/b/z;

    return-object v0
.end method


# virtual methods
.method public final a()Z
    .locals 1

    .prologue
    .line 380
    iget-boolean v0, p0, Lcom/google/c/a/a/b/b/a/b/z;->d:Z

    return v0
.end method
