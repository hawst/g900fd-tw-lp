.class public final Lcom/google/c/a/a/a/c/a/d/d;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public final a:Lcom/google/c/a/a/a/c/a/d/j;


# direct methods
.method public constructor <init>(Lcom/google/c/a/a/a/c/a/d/j;)V
    .locals 0

    .prologue
    .line 91
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 92
    iput-object p1, p0, Lcom/google/c/a/a/a/c/a/d/d;->a:Lcom/google/c/a/a/a/c/a/d/j;

    .line 93
    return-void
.end method


# virtual methods
.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 112
    if-ne p0, p1, :cond_1

    .line 129
    :cond_0
    :goto_0
    return v0

    .line 115
    :cond_1
    if-nez p1, :cond_2

    move v0, v1

    .line 116
    goto :goto_0

    .line 118
    :cond_2
    instance-of v2, p1, Lcom/google/c/a/a/a/c/a/d/d;

    if-nez v2, :cond_3

    move v0, v1

    .line 119
    goto :goto_0

    .line 121
    :cond_3
    check-cast p1, Lcom/google/c/a/a/a/c/a/d/d;

    .line 122
    iget-object v2, p0, Lcom/google/c/a/a/a/c/a/d/d;->a:Lcom/google/c/a/a/a/c/a/d/j;

    if-nez v2, :cond_4

    .line 123
    iget-object v2, p1, Lcom/google/c/a/a/a/c/a/d/d;->a:Lcom/google/c/a/a/a/c/a/d/j;

    if-eqz v2, :cond_0

    move v0, v1

    .line 124
    goto :goto_0

    .line 126
    :cond_4
    iget-object v2, p0, Lcom/google/c/a/a/a/c/a/d/d;->a:Lcom/google/c/a/a/a/c/a/d/j;

    iget-object v3, p1, Lcom/google/c/a/a/a/c/a/d/d;->a:Lcom/google/c/a/a/a/c/a/d/j;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    .line 127
    goto :goto_0
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 104
    iget-object v0, p0, Lcom/google/c/a/a/a/c/a/d/d;->a:Lcom/google/c/a/a/a/c/a/d/j;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    add-int/lit8 v0, v0, 0x1f

    .line 107
    return v0

    .line 104
    :cond_0
    iget-object v0, p0, Lcom/google/c/a/a/a/c/a/d/d;->a:Lcom/google/c/a/a/a/c/a/d/j;

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0
.end method
