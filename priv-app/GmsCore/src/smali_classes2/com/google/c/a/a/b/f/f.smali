.class public final Lcom/google/c/a/a/b/f/f;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static a:Ljava/util/Map;

.field private static b:Ljava/util/List;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    .line 21
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/google/c/a/a/b/f/f;->a:Ljava/util/Map;

    .line 27
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/google/c/a/a/b/f/f;->b:Ljava/util/List;

    .line 31
    invoke-static {}, Lcom/google/c/a/a/b/f/e;->values()[Lcom/google/c/a/a/b/f/e;

    move-result-object v2

    array-length v3, v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_3

    aget-object v4, v2, v1

    .line 32
    invoke-virtual {v4}, Lcom/google/c/a/a/b/f/e;->a()Lcom/google/c/a/a/b/f/a;

    move-result-object v0

    .line 33
    if-nez v0, :cond_0

    .line 34
    invoke-virtual {v4}, Lcom/google/c/a/a/b/f/e;->b()Lcom/google/c/a/a/b/f/a;

    move-result-object v0

    .line 36
    :cond_0
    invoke-interface {v0}, Lcom/google/c/a/a/b/f/a;->a()Ljava/lang/Class;

    move-result-object v5

    .line 37
    if-eqz v5, :cond_2

    .line 38
    sget-object v0, Lcom/google/c/a/a/b/f/f;->a:Ljava/util/Map;

    invoke-interface {v0, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 39
    if-nez v0, :cond_1

    .line 40
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 42
    :cond_1
    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 43
    sget-object v4, Lcom/google/c/a/a/b/f/f;->a:Ljava/util/Map;

    invoke-interface {v4, v5, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 31
    :goto_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 45
    :cond_2
    sget-object v0, Lcom/google/c/a/a/b/f/f;->b:Ljava/util/List;

    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 48
    :cond_3
    return-void
.end method

.method public static a(Ljava/lang/Object;Lcom/google/c/a/a/b/d/b;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 163
    const-class v0, Ljava/lang/Object;

    invoke-static {p0, p1, v0}, Lcom/google/c/a/a/b/f/f;->a(Ljava/lang/Object;Lcom/google/c/a/a/b/d/b;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public static a(Ljava/lang/Object;Lcom/google/c/a/a/b/d/b;Ljava/lang/Class;)Ljava/lang/Object;
    .locals 7

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x0

    .line 178
    :try_start_0
    invoke-interface {p1, p0}, Lcom/google/c/a/a/b/d/b;->g(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    move-object v0, v1

    .line 194
    :goto_0
    return-object v0

    .line 182
    :cond_0
    sget-object v0, Lcom/google/c/a/a/b/f/f;->a:Ljava/util/Map;

    invoke-interface {v0, p2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    if-nez v0, :cond_2

    .line 183
    :cond_1
    :goto_1
    if-eqz v1, :cond_3

    invoke-virtual {v1}, Lcom/google/c/a/a/b/f/e;->b()Lcom/google/c/a/a/b/f/a;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 186
    invoke-virtual {v1}, Lcom/google/c/a/a/b/f/e;->b()Lcom/google/c/a/a/b/f/a;

    move-result-object v0

    invoke-interface {v0, p0, p1}, Lcom/google/c/a/a/b/f/a;->a(Ljava/lang/Object;Lcom/google/c/a/a/b/d/b;)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0

    .line 182
    :cond_2
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v3

    const/4 v4, 0x1

    if-ne v3, v4, :cond_1

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/c/a/a/b/f/e;

    move-object v1, v0

    goto :goto_1

    .line 191
    :cond_3
    invoke-interface {p1, p0}, Lcom/google/c/a/a/b/d/b;->f(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 192
    invoke-interface {p1, p0}, Lcom/google/c/a/a/b/d/b;->e(Ljava/lang/Object;)I

    move-result v0

    if-gtz v0, :cond_4

    new-instance v0, Lcom/google/c/a/a/b/f/d;

    const-string v1, "Serialized object is missing type field."

    invoke-direct {v0, v1}, Lcom/google/c/a/a/b/f/d;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    .line 197
    :catch_0
    move-exception v0

    .line 198
    new-instance v1, Lcom/google/c/a/a/b/f/d;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unexpected runtime exception while deserializing value: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Lcom/google/c/a/a/b/f/d;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 192
    :cond_4
    const/4 v0, 0x0

    :try_start_1
    invoke-interface {p1, p0, v0}, Lcom/google/c/a/a/b/d/b;->a(Ljava/lang/Object;I)Ljava/lang/Object;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/google/c/a/a/b/d/b;->b(Ljava/lang/Object;)D

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v1

    if-nez v1, :cond_5

    new-instance v0, Lcom/google/c/a/a/b/f/d;

    const-string v1, "Serialized object type field is not a number."

    invoke-direct {v0, v1}, Lcom/google/c/a/a/b/f/d;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_5
    invoke-virtual {v1}, Ljava/lang/Number;->intValue()I

    move-result v3

    invoke-static {}, Lcom/google/c/a/a/b/f/e;->values()[Lcom/google/c/a/a/b/f/e;

    move-result-object v4

    array-length v5, v4

    move v0, v2

    :goto_2
    if-ge v0, v5, :cond_7

    aget-object v2, v4, v0

    invoke-virtual {v2}, Lcom/google/c/a/a/b/f/e;->c()I

    move-result v6

    if-ne v6, v3, :cond_6

    invoke-virtual {v2}, Lcom/google/c/a/a/b/f/e;->a()Lcom/google/c/a/a/b/f/a;

    move-result-object v0

    if-nez v0, :cond_8

    new-instance v0, Lcom/google/c/a/a/b/f/d;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "No serializer found to deserialize object with type code: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/lang/Number;->intValue()I

    move-result v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/c/a/a/b/f/d;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_6
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_7
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Found unknown type "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_8
    invoke-interface {v0, p0, p1}, Lcom/google/c/a/a/b/f/a;->a(Ljava/lang/Object;Lcom/google/c/a/a/b/d/b;)Ljava/lang/Object;

    move-result-object v0

    goto/16 :goto_0

    .line 194
    :cond_9
    invoke-interface {p1, p0}, Lcom/google/c/a/a/b/d/b;->h(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_a

    invoke-interface {p1, p0}, Lcom/google/c/a/a/b/d/b;->c(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    :cond_a
    invoke-interface {p1, p0}, Lcom/google/c/a/a/b/d/b;->i(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_b

    invoke-interface {p1, p0}, Lcom/google/c/a/a/b/d/b;->b(Ljava/lang/Object;)D

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    goto/16 :goto_0

    :cond_b
    invoke-interface {p1, p0}, Lcom/google/c/a/a/b/d/b;->j(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_c

    invoke-interface {p1, p0}, Lcom/google/c/a/a/b/d/b;->a(Ljava/lang/Object;)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto/16 :goto_0

    :cond_c
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Encoded value is not a primitive but also does not contain required type code."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_1
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_0
.end method

.method public static a(Ljava/lang/Object;Lcom/google/c/a/a/b/d/b;Z)Ljava/lang/Object;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 72
    if-nez p0, :cond_0

    move-object v0, v1

    .line 80
    :goto_0
    return-object v0

    .line 76
    :cond_0
    sget-object v0, Lcom/google/c/a/a/b/f/f;->a:Ljava/util/Map;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    if-nez v0, :cond_1

    sget-object v0, Lcom/google/c/a/a/b/f/f;->b:Ljava/util/List;

    :cond_1
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/c/a/a/b/f/e;

    invoke-virtual {v0, p0}, Lcom/google/c/a/a/b/f/e;->a(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    move-object v1, v0

    .line 77
    :cond_3
    if-nez v1, :cond_4

    .line 78
    new-instance v0, Lcom/google/c/a/a/b/f/d;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "No encoder for type: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/c/a/a/b/f/d;-><init>(Ljava/lang/String;)V

    throw v0

    .line 80
    :cond_4
    if-eqz p2, :cond_6

    invoke-virtual {v1}, Lcom/google/c/a/a/b/f/e;->a()Lcom/google/c/a/a/b/f/a;

    move-result-object v0

    if-nez v0, :cond_5

    invoke-virtual {v1}, Lcom/google/c/a/a/b/f/e;->b()Lcom/google/c/a/a/b/f/a;

    move-result-object v0

    :cond_5
    :goto_1
    invoke-interface {v0, p0, p1, v1}, Lcom/google/c/a/a/b/f/a;->a(Ljava/lang/Object;Lcom/google/c/a/a/b/d/b;Lcom/google/c/a/a/b/f/e;)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0

    :cond_6
    invoke-virtual {v1}, Lcom/google/c/a/a/b/f/e;->b()Lcom/google/c/a/a/b/f/a;

    move-result-object v0

    if-nez v0, :cond_5

    invoke-virtual {v1}, Lcom/google/c/a/a/b/f/e;->a()Lcom/google/c/a/a/b/f/a;

    move-result-object v0

    goto :goto_1
.end method
