.class final Lcom/google/c/a/a/a/c/a/b;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lcom/google/c/a/a/a/c/a/a;

.field private final b:Lcom/google/c/a/a/a/c/a/a/n;


# direct methods
.method private constructor <init>(Lcom/google/c/a/a/a/c/a/a;Lcom/google/c/a/a/a/c/a/a/n;)V
    .locals 0

    .prologue
    .line 252
    iput-object p1, p0, Lcom/google/c/a/a/a/c/a/b;->a:Lcom/google/c/a/a/a/c/a/a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 253
    iput-object p2, p0, Lcom/google/c/a/a/a/c/a/b;->b:Lcom/google/c/a/a/a/c/a/a/n;

    .line 254
    return-void
.end method

.method synthetic constructor <init>(Lcom/google/c/a/a/a/c/a/a;Lcom/google/c/a/a/a/c/a/a/n;B)V
    .locals 0

    .prologue
    .line 249
    invoke-direct {p0, p1, p2}, Lcom/google/c/a/a/a/c/a/b;-><init>(Lcom/google/c/a/a/a/c/a/a;Lcom/google/c/a/a/a/c/a/a/n;)V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 5

    .prologue
    .line 258
    iget-object v0, p0, Lcom/google/c/a/a/a/c/a/b;->a:Lcom/google/c/a/a/a/c/a/a;

    invoke-static {v0}, Lcom/google/c/a/a/a/c/a/a;->a(Lcom/google/c/a/a/a/c/a/a;)Lcom/google/c/a/a/a/c/a/d;

    move-result-object v1

    monitor-enter v1

    .line 259
    :try_start_0
    iget-object v0, p0, Lcom/google/c/a/a/a/c/a/b;->a:Lcom/google/c/a/a/a/c/a/a;

    invoke-static {v0}, Lcom/google/c/a/a/a/c/a/a;->a(Lcom/google/c/a/a/a/c/a/a;)Lcom/google/c/a/a/a/c/a/d;

    move-result-object v0

    sget-object v2, Lcom/google/c/a/a/a/c/a/d;->c:Lcom/google/c/a/a/a/c/a/d;

    if-eq v0, v2, :cond_0

    iget-object v0, p0, Lcom/google/c/a/a/a/c/a/b;->a:Lcom/google/c/a/a/a/c/a/a;

    invoke-static {v0}, Lcom/google/c/a/a/a/c/a/a;->a(Lcom/google/c/a/a/a/c/a/a;)Lcom/google/c/a/a/a/c/a/d;

    move-result-object v0

    sget-object v2, Lcom/google/c/a/a/a/c/a/d;->d:Lcom/google/c/a/a/a/c/a/d;

    if-ne v0, v2, :cond_1

    .line 260
    :cond_0
    invoke-static {}, Lcom/google/c/a/a/a/c/a/a;->c()Ljava/util/logging/Logger;

    move-result-object v0

    const-string v2, "Opening bind request to server."

    invoke-virtual {v0, v2}, Ljava/util/logging/Logger;->info(Ljava/lang/String;)V

    .line 261
    iget-object v0, p0, Lcom/google/c/a/a/a/c/a/b;->a:Lcom/google/c/a/a/a/c/a/a;

    invoke-static {v0}, Lcom/google/c/a/a/a/c/a/a;->f(Lcom/google/c/a/a/a/c/a/a;)Lcom/google/c/a/a/a/c/a/a/m;

    move-result-object v0

    iget-object v2, p0, Lcom/google/c/a/a/a/c/a/b;->a:Lcom/google/c/a/a/a/c/a/a;

    invoke-static {v2}, Lcom/google/c/a/a/a/c/a/a;->b(Lcom/google/c/a/a/a/c/a/a;)Lcom/google/c/a/a/a/c/a/b/d;

    move-result-object v2

    iget-object v3, p0, Lcom/google/c/a/a/a/c/a/b;->b:Lcom/google/c/a/a/a/c/a/a/n;

    iget-object v4, p0, Lcom/google/c/a/a/a/c/a/b;->a:Lcom/google/c/a/a/a/c/a/a;

    invoke-static {v4}, Lcom/google/c/a/a/a/c/a/a;->e(Lcom/google/c/a/a/a/c/a/a;)Z

    move-result v4

    invoke-interface {v0, v2, v3, v4}, Lcom/google/c/a/a/a/c/a/a/m;->a(Lcom/google/c/a/a/a/c/a/b/d;Lcom/google/c/a/a/a/c/a/a/n;Z)Lcom/google/c/a/a/a/c/a/a/e;

    move-result-object v0

    .line 263
    iget-object v2, p0, Lcom/google/c/a/a/a/c/a/b;->a:Lcom/google/c/a/a/a/c/a/a;

    invoke-static {v2, v0}, Lcom/google/c/a/a/a/c/a/a;->a(Lcom/google/c/a/a/a/c/a/a;Lcom/google/c/a/a/a/c/a/a/e;)V

    .line 264
    iget-object v2, p0, Lcom/google/c/a/a/a/c/a/b;->a:Lcom/google/c/a/a/a/c/a/a;

    invoke-static {v2}, Lcom/google/c/a/a/a/c/a/a;->d(Lcom/google/c/a/a/a/c/a/a;)Lcom/google/c/a/a/a/c/a/f;

    move-result-object v2

    invoke-interface {v0, v2}, Lcom/google/c/a/a/a/c/a/a/e;->a(Lcom/google/c/a/a/a/c/a/a/j;)V

    .line 265
    iget-object v2, p0, Lcom/google/c/a/a/a/c/a/b;->a:Lcom/google/c/a/a/a/c/a/a;

    invoke-static {v2}, Lcom/google/c/a/a/a/c/a/a;->d(Lcom/google/c/a/a/a/c/a/a;)Lcom/google/c/a/a/a/c/a/f;

    move-result-object v2

    invoke-interface {v0, v2}, Lcom/google/c/a/a/a/c/a/a/e;->a(Lcom/google/c/a/a/a/c/a/a/h;)V

    .line 266
    iget-object v2, p0, Lcom/google/c/a/a/a/c/a/b;->a:Lcom/google/c/a/a/a/c/a/a;

    invoke-static {v2}, Lcom/google/c/a/a/a/c/a/a;->d(Lcom/google/c/a/a/a/c/a/a;)Lcom/google/c/a/a/a/c/a/f;

    move-result-object v2

    invoke-interface {v0, v2}, Lcom/google/c/a/a/a/c/a/a/e;->a(Lcom/google/c/a/a/a/c/a/a/i;)V

    .line 267
    iget-object v2, p0, Lcom/google/c/a/a/a/c/a/b;->a:Lcom/google/c/a/a/a/c/a/a;

    invoke-static {v2}, Lcom/google/c/a/a/a/c/a/a;->d(Lcom/google/c/a/a/a/c/a/a;)Lcom/google/c/a/a/a/c/a/f;

    move-result-object v2

    invoke-interface {v0, v2}, Lcom/google/c/a/a/a/c/a/a/e;->a(Lcom/google/c/a/a/a/c/a/a/k;)V

    .line 268
    invoke-interface {v0}, Lcom/google/c/a/a/a/c/a/a/e;->c()V

    .line 270
    :cond_1
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method
