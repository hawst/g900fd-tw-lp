.class public final Lcom/google/c/a/a/b/b/a/a/g;
.super Lcom/google/c/a/a/b/b/a/a/a;
.source "SourceFile"


# instance fields
.field public final g:Ljava/lang/String;

.field public final h:Lcom/google/c/a/a/b/b/a/r;

.field public final i:Lcom/google/c/a/a/b/b/a/r;


# direct methods
.method public constructor <init>(Lcom/google/c/a/a/b/b/a/f;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;ZLcom/google/c/a/a/b/b/a/k;Ljava/lang/String;Lcom/google/c/a/a/b/b/a/r;Lcom/google/c/a/a/b/b/a/r;)V
    .locals 0

    .prologue
    .line 32
    invoke-direct/range {p0 .. p6}, Lcom/google/c/a/a/b/b/a/a/a;-><init>(Lcom/google/c/a/a/b/b/a/g;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;ZLcom/google/c/a/a/b/b/a/k;)V

    .line 33
    iput-object p7, p0, Lcom/google/c/a/a/b/b/a/a/g;->g:Ljava/lang/String;

    .line 34
    iput-object p8, p0, Lcom/google/c/a/a/b/b/a/a/g;->i:Lcom/google/c/a/a/b/b/a/r;

    .line 35
    iput-object p9, p0, Lcom/google/c/a/a/b/b/a/a/g;->h:Lcom/google/c/a/a/b/b/a/r;

    .line 36
    return-void
.end method


# virtual methods
.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 66
    if-ne p0, p1, :cond_0

    .line 77
    :goto_0
    return v0

    .line 69
    :cond_0
    invoke-super {p0, p1}, Lcom/google/c/a/a/b/b/a/a/a;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    move v0, v1

    .line 70
    goto :goto_0

    .line 72
    :cond_1
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_2

    move v0, v1

    .line 73
    goto :goto_0

    .line 75
    :cond_2
    check-cast p1, Lcom/google/c/a/a/b/b/a/a/g;

    .line 77
    const/4 v2, 0x6

    new-array v2, v2, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/google/c/a/a/b/b/a/a/g;->g:Ljava/lang/String;

    aput-object v3, v2, v1

    iget-object v1, p1, Lcom/google/c/a/a/b/b/a/a/g;->g:Ljava/lang/String;

    aput-object v1, v2, v0

    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/c/a/a/b/b/a/a/g;->i:Lcom/google/c/a/a/b/b/a/r;

    aput-object v1, v2, v0

    const/4 v0, 0x3

    iget-object v1, p1, Lcom/google/c/a/a/b/b/a/a/g;->i:Lcom/google/c/a/a/b/b/a/r;

    aput-object v1, v2, v0

    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/c/a/a/b/b/a/a/g;->h:Lcom/google/c/a/a/b/b/a/r;

    aput-object v1, v2, v0

    const/4 v0, 0x5

    iget-object v1, p1, Lcom/google/c/a/a/b/b/a/a/g;->h:Lcom/google/c/a/a/b/b/a/r;

    aput-object v1, v2, v0

    invoke-static {v2}, Lcom/google/c/a/a/b/h/b;->a([Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    .line 61
    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    invoke-super {p0}, Lcom/google/c/a/a/b/b/a/a/a;->hashCode()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/c/a/a/b/b/a/a/g;->g:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/c/a/a/b/b/a/a/g;->h:Lcom/google/c/a/a/b/b/a/r;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/c/a/a/b/b/a/a/g;->i:Lcom/google/c/a/a/b/b/a/r;

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method
