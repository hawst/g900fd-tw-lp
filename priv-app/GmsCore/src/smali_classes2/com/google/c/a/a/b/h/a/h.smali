.class abstract Lcom/google/c/a/a/b/h/a/h;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Iterable;


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract a()I
.end method

.method final a(Lcom/google/c/a/a/b/h/a/h;)I
    .locals 8

    .prologue
    const/4 v0, -0x1

    const/4 v3, 0x0

    .line 39
    invoke-virtual {p1}, Lcom/google/c/a/a/b/h/a/h;->a()I

    move-result v5

    .line 40
    invoke-virtual {p0}, Lcom/google/c/a/a/b/h/a/h;->a()I

    move-result v1

    if-ge v5, v1, :cond_1

    .line 56
    :cond_0
    :goto_0
    return v0

    .line 43
    :cond_1
    invoke-virtual {p0}, Lcom/google/c/a/a/b/h/a/h;->a()I

    move-result v1

    sub-int v6, v1, v5

    move v1, v3

    .line 44
    :goto_1
    if-ge v1, v6, :cond_0

    .line 45
    const/4 v2, 0x1

    move v4, v3

    .line 46
    :goto_2
    if-ge v4, v5, :cond_2

    .line 47
    add-int v7, v1, v4

    invoke-virtual {p0, v7, p1, v4}, Lcom/google/c/a/a/b/h/a/h;->a(ILcom/google/c/a/a/b/h/a/h;I)Z

    move-result v7

    if-nez v7, :cond_3

    move v2, v3

    .line 52
    :cond_2
    if-eqz v2, :cond_4

    move v0, v1

    .line 53
    goto :goto_0

    .line 46
    :cond_3
    add-int/lit8 v4, v4, 0x1

    goto :goto_2

    .line 44
    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_1
.end method

.method public abstract a(II)Lcom/google/c/a/a/b/h/a/h;
.end method

.method public abstract a(ILcom/google/c/a/a/b/h/a/h;I)Z
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 60
    invoke-virtual {p0}, Lcom/google/c/a/a/b/h/a/h;->a()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public abstract iterator()Ljava/util/Iterator;
.end method
