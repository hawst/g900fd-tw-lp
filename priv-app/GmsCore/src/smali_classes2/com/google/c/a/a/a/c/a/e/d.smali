.class public final enum Lcom/google/c/a/a/a/c/a/e/d;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field public static final enum a:Lcom/google/c/a/a/a/c/a/e/d;

.field public static final enum b:Lcom/google/c/a/a/a/c/a/e/d;

.field public static final enum c:Lcom/google/c/a/a/a/c/a/e/d;

.field private static final synthetic d:[Lcom/google/c/a/a/a/c/a/e/d;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 23
    new-instance v0, Lcom/google/c/a/a/a/c/a/e/d;

    const-string v1, "SUCCESS"

    invoke-direct {v0, v1, v2}, Lcom/google/c/a/a/a/c/a/e/d;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/c/a/a/a/c/a/e/d;->a:Lcom/google/c/a/a/a/c/a/e/d;

    .line 28
    new-instance v0, Lcom/google/c/a/a/a/c/a/e/d;

    const-string v1, "FAILURE"

    invoke-direct {v0, v1, v3}, Lcom/google/c/a/a/a/c/a/e/d;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/c/a/a/a/c/a/e/d;->b:Lcom/google/c/a/a/a/c/a/e/d;

    .line 33
    new-instance v0, Lcom/google/c/a/a/a/c/a/e/d;

    const-string v1, "BACKOFF"

    invoke-direct {v0, v1, v4}, Lcom/google/c/a/a/a/c/a/e/d;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/c/a/a/a/c/a/e/d;->c:Lcom/google/c/a/a/a/c/a/e/d;

    .line 19
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/google/c/a/a/a/c/a/e/d;

    sget-object v1, Lcom/google/c/a/a/a/c/a/e/d;->a:Lcom/google/c/a/a/a/c/a/e/d;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/c/a/a/a/c/a/e/d;->b:Lcom/google/c/a/a/a/c/a/e/d;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/c/a/a/a/c/a/e/d;->c:Lcom/google/c/a/a/a/c/a/e/d;

    aput-object v1, v0, v4

    sput-object v0, Lcom/google/c/a/a/a/c/a/e/d;->d:[Lcom/google/c/a/a/a/c/a/e/d;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/c/a/a/a/c/a/e/d;
    .locals 1

    .prologue
    .line 19
    const-class v0, Lcom/google/c/a/a/a/c/a/e/d;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/c/a/a/a/c/a/e/d;

    return-object v0
.end method

.method public static values()[Lcom/google/c/a/a/a/c/a/e/d;
    .locals 1

    .prologue
    .line 19
    sget-object v0, Lcom/google/c/a/a/a/c/a/e/d;->d:[Lcom/google/c/a/a/a/c/a/e/d;

    invoke-virtual {v0}, [Lcom/google/c/a/a/a/c/a/e/d;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/c/a/a/a/c/a/e/d;

    return-object v0
.end method
