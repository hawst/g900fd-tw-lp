.class public final Lcom/google/c/a/a/a/c/a/a/g;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public final a:Lcom/google/c/a/a/a/c/a/e/e;

.field public final b:Ljava/lang/Exception;

.field public final c:I

.field public final d:Lcom/google/c/a/a/a/c/a/a/n;


# direct methods
.method public constructor <init>(Lcom/google/c/a/a/a/c/a/e/e;Ljava/lang/Exception;ILcom/google/c/a/a/a/c/a/a/n;)V
    .locals 1

    .prologue
    .line 104
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 105
    iput-object p1, p0, Lcom/google/c/a/a/a/c/a/a/g;->a:Lcom/google/c/a/a/a/c/a/e/e;

    .line 106
    iput-object p2, p0, Lcom/google/c/a/a/a/c/a/a/g;->b:Ljava/lang/Exception;

    .line 107
    iput p3, p0, Lcom/google/c/a/a/a/c/a/a/g;->c:I

    .line 108
    iput-object p4, p0, Lcom/google/c/a/a/a/c/a/a/g;->d:Lcom/google/c/a/a/a/c/a/a/n;

    .line 109
    if-eqz p2, :cond_0

    if-nez p1, :cond_1

    .line 110
    :cond_0
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 112
    :cond_1
    return-void
.end method

.method public constructor <init>(Lcom/google/c/a/a/a/c/a/e/e;Ljava/lang/Exception;Lcom/google/c/a/a/a/c/a/a/n;)V
    .locals 1

    .prologue
    .line 100
    const/4 v0, -0x1

    invoke-direct {p0, p1, p2, v0, p3}, Lcom/google/c/a/a/a/c/a/a/g;-><init>(Lcom/google/c/a/a/a/c/a/e/e;Ljava/lang/Exception;ILcom/google/c/a/a/a/c/a/a/n;)V

    .line 101
    return-void
.end method


# virtual methods
.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 150
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "BindErrorEvent [errorType="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/c/a/a/a/c/a/a/g;->a:Lcom/google/c/a/a/a/c/a/e/e;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", cause="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/c/a/a/a/c/a/a/g;->b:Ljava/lang/Exception;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", statusCode="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/c/a/a/a/c/a/a/g;->c:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
