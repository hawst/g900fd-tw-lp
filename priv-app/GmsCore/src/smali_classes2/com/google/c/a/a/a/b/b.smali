.class public final Lcom/google/c/a/a/a/b/b;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final a:Ljava/util/Map;

.field public b:Lcom/google/c/a/a/b/g/d;

.field public c:Lcom/google/c/a/a/a/b/a;


# direct methods
.method public constructor <init>(Lcom/google/c/a/a/a/c/i;)V
    .locals 2

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/c/a/a/a/b/b;->a:Ljava/util/Map;

    .line 30
    const-class v0, Lcom/google/c/a/a/b/g/b;

    new-instance v1, Lcom/google/c/a/a/a/b/c;

    invoke-direct {v1, p0}, Lcom/google/c/a/a/a/b/c;-><init>(Lcom/google/c/a/a/a/b/b;)V

    invoke-interface {p1, v0, v1}, Lcom/google/c/a/a/a/c/i;->a(Ljava/lang/Class;Lcom/google/c/a/a/a/c/r;)V

    .line 36
    const-class v0, Lcom/google/c/a/a/b/g/c;

    new-instance v1, Lcom/google/c/a/a/a/b/d;

    invoke-direct {v1, p0}, Lcom/google/c/a/a/a/b/d;-><init>(Lcom/google/c/a/a/a/b/b;)V

    invoke-interface {p1, v0, v1}, Lcom/google/c/a/a/a/c/i;->a(Ljava/lang/Class;Lcom/google/c/a/a/a/c/r;)V

    .line 43
    const-class v0, Lcom/google/c/a/a/b/g/a;

    new-instance v1, Lcom/google/c/a/a/a/b/e;

    invoke-direct {v1, p0}, Lcom/google/c/a/a/a/b/e;-><init>(Lcom/google/c/a/a/a/b/b;)V

    invoke-interface {p1, v0, v1}, Lcom/google/c/a/a/a/c/i;->a(Ljava/lang/Class;Lcom/google/c/a/a/a/c/r;)V

    .line 49
    return-void
.end method


# virtual methods
.method public final a()Ljava/util/Collection;
    .locals 3

    .prologue
    .line 130
    iget-object v1, p0, Lcom/google/c/a/a/a/b/b;->a:Ljava/util/Map;

    monitor-enter v1

    .line 131
    :try_start_0
    new-instance v0, Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/google/c/a/a/a/b/b;->a:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v0

    .line 132
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method final a(Lcom/google/c/a/a/b/g/d;)V
    .locals 1

    .prologue
    .line 105
    iget-object v0, p0, Lcom/google/c/a/a/a/b/b;->c:Lcom/google/c/a/a/a/b/a;

    if-eqz v0, :cond_0

    .line 106
    iget-object v0, p0, Lcom/google/c/a/a/a/b/b;->c:Lcom/google/c/a/a/a/b/a;

    invoke-interface {v0, p1}, Lcom/google/c/a/a/a/b/a;->a(Lcom/google/c/a/a/b/g/d;)V

    .line 108
    :cond_0
    return-void
.end method

.method public final a(Ljava/lang/Iterable;)V
    .locals 8

    .prologue
    .line 57
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    .line 58
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/c/a/a/b/g/d;

    .line 59
    invoke-virtual {v0}, Lcom/google/c/a/a/b/g/d;->a()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 62
    :cond_0
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 63
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 64
    iget-object v4, p0, Lcom/google/c/a/a/a/b/b;->a:Ljava/util/Map;

    monitor-enter v4

    .line 65
    :try_start_0
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/c/a/a/b/g/d;

    .line 66
    invoke-virtual {v0}, Lcom/google/c/a/a/b/g/d;->c()Z

    move-result v6

    if-eqz v6, :cond_1

    .line 67
    iput-object v0, p0, Lcom/google/c/a/a/a/b/b;->b:Lcom/google/c/a/a/b/g/d;

    .line 69
    :cond_1
    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 70
    iget-object v6, p0, Lcom/google/c/a/a/a/b/b;->a:Ljava/util/Map;

    invoke-virtual {v0}, Lcom/google/c/a/a/b/g/d;->a()Ljava/lang/String;

    move-result-object v7

    invoke-interface {v6, v7, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    .line 80
    :catchall_0
    move-exception v0

    monitor-exit v4

    throw v0

    .line 72
    :cond_2
    :try_start_1
    iget-object v0, p0, Lcom/google/c/a/a/a/b/b;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_3
    :goto_2
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/c/a/a/b/g/d;

    .line 73
    invoke-virtual {v0}, Lcom/google/c/a/a/b/g/d;->a()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v1, v6}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_3

    .line 74
    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 77
    :cond_4
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/c/a/a/b/g/d;

    .line 78
    iget-object v5, p0, Lcom/google/c/a/a/a/b/b;->a:Ljava/util/Map;

    invoke-virtual {v0}, Lcom/google/c/a/a/b/g/d;->a()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v5, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_3

    .line 80
    :cond_5
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 82
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_4
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/c/a/a/b/g/d;

    .line 83
    invoke-virtual {p0, v0}, Lcom/google/c/a/a/a/b/b;->a(Lcom/google/c/a/a/b/g/d;)V

    goto :goto_4

    .line 85
    :cond_6
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_5
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/c/a/a/b/g/d;

    .line 86
    invoke-virtual {p0, v0}, Lcom/google/c/a/a/a/b/b;->b(Lcom/google/c/a/a/b/g/d;)V

    goto :goto_5

    .line 88
    :cond_7
    return-void
.end method

.method final b(Lcom/google/c/a/a/b/g/d;)V
    .locals 1

    .prologue
    .line 121
    iget-object v0, p0, Lcom/google/c/a/a/a/b/b;->c:Lcom/google/c/a/a/a/b/a;

    if-eqz v0, :cond_0

    .line 122
    iget-object v0, p0, Lcom/google/c/a/a/a/b/b;->c:Lcom/google/c/a/a/a/b/a;

    invoke-interface {v0, p1}, Lcom/google/c/a/a/a/b/a;->b(Lcom/google/c/a/a/b/g/d;)V

    .line 124
    :cond_0
    return-void
.end method
