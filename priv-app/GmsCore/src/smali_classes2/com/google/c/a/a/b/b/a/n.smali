.class public final Lcom/google/c/a/a/b/b/a/n;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/c/a/a/b/b/a/r;


# instance fields
.field private final a:Ljava/lang/String;


# direct methods
.method private constructor <init>(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 13
    const-string v0, "serializedValue"

    invoke-static {p1, v0}, Lcom/google/c/a/a/b/h/c;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/google/c/a/a/b/b/a/n;->a:Ljava/lang/String;

    .line 14
    return-void
.end method

.method public static a(Ljava/lang/String;)Lcom/google/c/a/a/b/b/a/n;
    .locals 1

    .prologue
    .line 27
    new-instance v0, Lcom/google/c/a/a/b/b/a/n;

    invoke-direct {v0, p0}, Lcom/google/c/a/a/b/b/a/n;-><init>(Ljava/lang/String;)V

    return-object v0
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 20
    iget-object v0, p0, Lcom/google/c/a/a/b/b/a/n;->a:Ljava/lang/String;

    return-object v0
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Lcom/google/c/a/a/b/b/a/n;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    add-int/lit8 v0, v0, 0x18

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 42
    if-ne p0, p1, :cond_1

    .line 43
    const/4 v0, 0x1

    .line 52
    :cond_0
    :goto_0
    return v0

    .line 45
    :cond_1
    if-eqz p1, :cond_0

    .line 48
    instance-of v1, p1, Lcom/google/c/a/a/b/b/a/n;

    if-eqz v1, :cond_0

    .line 51
    check-cast p1, Lcom/google/c/a/a/b/b/a/n;

    .line 52
    iget-object v0, p0, Lcom/google/c/a/a/b/b/a/n;->a:Ljava/lang/String;

    iget-object v1, p1, Lcom/google/c/a/a/b/b/a/n;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lcom/google/c/a/a/b/b/a/n;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 32
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "[JsonValue "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/c/a/a/b/b/a/n;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
