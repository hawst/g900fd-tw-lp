.class final Lcom/google/c/a/a/a/c/a/c/g;
.super Lcom/google/c/a/a/a/c/a/c/a;
.source "SourceFile"


# instance fields
.field a:I

.field b:J

.field c:J

.field private d:Lcom/google/c/a/a/a/c/a/c/i;

.field private final e:Lcom/google/c/a/a/a/c/a/d/b;


# direct methods
.method constructor <init>(Lcom/google/c/a/a/a/c/a/b/d;Lcom/google/c/a/a/a/c/a/d/k;)V
    .locals 5

    .prologue
    const-wide/16 v2, 0x0

    const/4 v4, 0x0

    .line 46
    invoke-direct {p0}, Lcom/google/c/a/a/a/c/a/c/a;-><init>()V

    .line 33
    sget-object v0, Lcom/google/c/a/a/a/c/a/c/i;->a:Lcom/google/c/a/a/a/c/a/c/i;

    iput-object v0, p0, Lcom/google/c/a/a/a/c/a/c/g;->d:Lcom/google/c/a/a/a/c/a/c/i;

    .line 34
    iput v4, p0, Lcom/google/c/a/a/a/c/a/c/g;->a:I

    .line 37
    iput-wide v2, p0, Lcom/google/c/a/a/a/c/a/c/g;->b:J

    .line 38
    iput-wide v2, p0, Lcom/google/c/a/a/a/c/a/c/g;->c:J

    .line 48
    iget-object v0, p1, Lcom/google/c/a/a/a/c/a/b/d;->d:Lcom/google/c/a/a/a/c/a/b/a;

    sget-object v1, Lcom/google/c/a/a/a/c/a/b/c;->a:Lcom/google/c/a/a/a/c/a/b/c;

    invoke-virtual {v0, v1}, Lcom/google/c/a/a/a/c/a/b/a;->a(Lcom/google/c/a/a/a/c/a/b/c;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "VER"

    const-string v2, "8"

    invoke-static {v1, v2}, Ljava/util/Collections;->singletonMap(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/Map;

    move-result-object v1

    sget-object v2, Lcom/google/c/a/a/a/c/a/d/r;->a:Lcom/google/c/a/a/a/c/a/d/r;

    const/4 v3, 0x0

    invoke-static {p1, v0, v1, v2, v3}, Lcom/google/c/a/a/a/c/a/d/p;->a(Lcom/google/c/a/a/a/c/a/b/d;Ljava/lang/String;Ljava/util/Map;Lcom/google/c/a/a/a/c/a/d/r;Ljava/lang/String;)Lcom/google/c/a/a/a/c/a/d/p;

    move-result-object v0

    .line 56
    invoke-interface {p2, v0, v4}, Lcom/google/c/a/a/a/c/a/d/k;->a(Lcom/google/c/a/a/a/c/a/d/p;Z)Lcom/google/c/a/a/a/c/a/d/b;

    move-result-object v0

    iput-object v0, p0, Lcom/google/c/a/a/a/c/a/c/g;->e:Lcom/google/c/a/a/a/c/a/d/b;

    .line 57
    new-instance v0, Lcom/google/c/a/a/a/c/a/c/h;

    invoke-direct {v0, p0, v4}, Lcom/google/c/a/a/a/c/a/c/h;-><init>(Lcom/google/c/a/a/a/c/a/c/g;B)V

    .line 58
    iget-object v1, p0, Lcom/google/c/a/a/a/c/a/c/g;->e:Lcom/google/c/a/a/a/c/a/d/b;

    invoke-interface {v1, v0}, Lcom/google/c/a/a/a/c/a/d/b;->a(Lcom/google/c/a/a/a/c/a/d/f;)V

    .line 59
    iget-object v1, p0, Lcom/google/c/a/a/a/c/a/c/g;->e:Lcom/google/c/a/a/a/c/a/d/b;

    invoke-interface {v1, v0}, Lcom/google/c/a/a/a/c/a/d/b;->a(Lcom/google/c/a/a/a/c/a/d/g;)V

    .line 60
    iget-object v1, p0, Lcom/google/c/a/a/a/c/a/c/g;->e:Lcom/google/c/a/a/a/c/a/d/b;

    invoke-interface {v1, v0}, Lcom/google/c/a/a/a/c/a/d/b;->a(Lcom/google/c/a/a/a/c/a/d/h;)V

    .line 61
    iget-object v1, p0, Lcom/google/c/a/a/a/c/a/c/g;->e:Lcom/google/c/a/a/a/c/a/d/b;

    invoke-interface {v1, v0}, Lcom/google/c/a/a/a/c/a/d/b;->a(Lcom/google/c/a/a/a/c/a/d/i;)V

    .line 62
    return-void
.end method

.method private declared-synchronized a(Lcom/google/c/a/a/a/c/a/c/e;)V
    .locals 1

    .prologue
    .line 80
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/c/a/a/a/c/a/c/g;->e:Lcom/google/c/a/a/a/c/a/d/b;

    invoke-interface {v0}, Lcom/google/c/a/a/a/c/a/d/b;->b()V

    .line 81
    sget-object v0, Lcom/google/c/a/a/a/c/a/c/i;->f:Lcom/google/c/a/a/a/c/a/c/i;

    iput-object v0, p0, Lcom/google/c/a/a/a/c/a/c/g;->d:Lcom/google/c/a/a/a/c/a/c/i;

    .line 82
    new-instance v0, Lcom/google/c/a/a/a/c/a/c/d;

    invoke-direct {v0, p1}, Lcom/google/c/a/a/a/c/a/c/d;-><init>(Lcom/google/c/a/a/a/c/a/c/e;)V

    invoke-virtual {p0, v0}, Lcom/google/c/a/a/a/c/a/c/g;->a(Lcom/google/c/a/a/a/c/a/c/d;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 83
    monitor-exit p0

    return-void

    .line 80
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public final declared-synchronized a()V
    .locals 2

    .prologue
    .line 66
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/c/a/a/a/c/a/c/g;->d:Lcom/google/c/a/a/a/c/a/c/i;

    sget-object v1, Lcom/google/c/a/a/a/c/a/c/i;->a:Lcom/google/c/a/a/a/c/a/c/i;

    if-eq v0, v1, :cond_0

    .line 67
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Test has already been run."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 66
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 69
    :cond_0
    :try_start_1
    sget-object v0, Lcom/google/c/a/a/a/c/a/c/i;->b:Lcom/google/c/a/a/a/c/a/c/i;

    iput-object v0, p0, Lcom/google/c/a/a/a/c/a/c/g;->d:Lcom/google/c/a/a/a/c/a/c/i;

    .line 70
    iget-object v0, p0, Lcom/google/c/a/a/a/c/a/c/g;->e:Lcom/google/c/a/a/a/c/a/d/b;

    invoke-interface {v0}, Lcom/google/c/a/a/a/c/a/d/b;->a()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 71
    monitor-exit p0

    return-void
.end method

.method final declared-synchronized a(Lcom/google/c/a/a/a/c/a/c/i;)V
    .locals 4

    .prologue
    .line 86
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/c/a/a/a/c/a/c/g;->d:Lcom/google/c/a/a/a/c/a/c/i;

    .line 87
    iput-object p1, p0, Lcom/google/c/a/a/a/c/a/c/g;->d:Lcom/google/c/a/a/a/c/a/c/i;

    .line 88
    invoke-virtual {v0}, Lcom/google/c/a/a/a/c/a/c/i;->a()I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    invoke-virtual {p1}, Lcom/google/c/a/a/a/c/a/c/i;->a()I

    move-result v2

    if-eq v1, v2, :cond_0

    .line 89
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Out-of sequence response. Server sent response for stage "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", but actual stage was "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 91
    sget-object v1, Lcom/google/c/a/a/a/c/a/e/e;->d:Lcom/google/c/a/a/a/c/a/e/e;

    new-instance v2, Ljava/lang/IllegalStateException;

    invoke-direct {v2, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    const/4 v0, -0x1

    invoke-virtual {p0, v1, v2, v0}, Lcom/google/c/a/a/a/c/a/c/g;->a(Lcom/google/c/a/a/a/c/a/e/e;Ljava/lang/Exception;I)V

    .line 93
    :cond_0
    sget-object v0, Lcom/google/c/a/a/a/c/a/c/i;->f:Lcom/google/c/a/a/a/c/a/c/i;

    if-ne p1, v0, :cond_1

    .line 94
    iget-wide v0, p0, Lcom/google/c/a/a/a/c/a/c/g;->c:J

    iget-wide v2, p0, Lcom/google/c/a/a/a/c/a/c/g;->b:J

    sub-long/2addr v0, v2

    .line 95
    const-wide/16 v2, 0x1f4

    cmp-long v0, v0, v2

    if-lez v0, :cond_2

    .line 96
    sget-object v0, Lcom/google/c/a/a/a/c/a/c/e;->a:Lcom/google/c/a/a/a/c/a/c/e;

    invoke-direct {p0, v0}, Lcom/google/c/a/a/a/c/a/c/g;->a(Lcom/google/c/a/a/a/c/a/c/e;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 101
    :cond_1
    :goto_0
    monitor-exit p0

    return-void

    .line 98
    :cond_2
    :try_start_1
    sget-object v0, Lcom/google/c/a/a/a/c/a/c/e;->b:Lcom/google/c/a/a/a/c/a/c/e;

    invoke-direct {p0, v0}, Lcom/google/c/a/a/a/c/a/c/g;->a(Lcom/google/c/a/a/a/c/a/c/e;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 86
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method final declared-synchronized a(Lcom/google/c/a/a/a/c/a/e/e;Ljava/lang/Exception;I)V
    .locals 2

    .prologue
    .line 74
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/c/a/a/a/c/a/c/g;->e:Lcom/google/c/a/a/a/c/a/d/b;

    invoke-interface {v0}, Lcom/google/c/a/a/a/c/a/d/b;->b()V

    .line 75
    sget-object v0, Lcom/google/c/a/a/a/c/a/c/i;->f:Lcom/google/c/a/a/a/c/a/c/i;

    iput-object v0, p0, Lcom/google/c/a/a/a/c/a/c/g;->d:Lcom/google/c/a/a/a/c/a/c/i;

    .line 76
    new-instance v0, Lcom/google/c/a/a/a/c/a/c/d;

    sget-object v1, Lcom/google/c/a/a/a/c/a/c/e;->c:Lcom/google/c/a/a/a/c/a/c/e;

    invoke-direct {v0, v1, p1, p2, p3}, Lcom/google/c/a/a/a/c/a/c/d;-><init>(Lcom/google/c/a/a/a/c/a/c/e;Lcom/google/c/a/a/a/c/a/e/e;Ljava/lang/Exception;I)V

    invoke-virtual {p0, v0}, Lcom/google/c/a/a/a/c/a/c/g;->a(Lcom/google/c/a/a/a/c/a/c/d;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 77
    monitor-exit p0

    return-void

    .line 74
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
