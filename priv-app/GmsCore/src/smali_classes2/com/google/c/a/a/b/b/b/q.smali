.class final Lcom/google/c/a/a/b/b/b/q;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/c/a/a/b/b/b/s;


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static a(ILcom/google/c/a/a/b/b/a/c/m;)I
    .locals 2

    .prologue
    .line 109
    invoke-virtual {p1}, Lcom/google/c/a/a/b/b/a/c/m;->d()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/c/a/a/b/b/a/c/m;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/google/c/a/a/b/b/a/c/m;->e()I

    move-result v0

    invoke-virtual {p1}, Lcom/google/c/a/a/b/b/a/c/m;->c()I

    move-result v1

    if-le v0, v1, :cond_0

    .line 111
    add-int/lit8 p0, p0, -0x1

    .line 113
    :cond_0
    return p0
.end method

.method private static a(Ljava/lang/String;ILjava/util/List;)Lcom/google/c/b/a/c;
    .locals 1

    .prologue
    .line 101
    invoke-interface {p2}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 102
    invoke-static {}, Lcom/google/c/b/a/d;->a()Lcom/google/c/b/a/d;

    move-result-object v0

    .line 104
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/google/c/a/a/b/b/a/c/s;

    invoke-direct {v0, p0, p1, p2}, Lcom/google/c/a/a/b/b/a/c/s;-><init>(Ljava/lang/String;ILjava/util/List;)V

    goto :goto_0
.end method


# virtual methods
.method public final a(Lcom/google/c/a/a/b/b/b/y;Lcom/google/c/a/a/b/b/b/y;)V
    .locals 9

    .prologue
    const/4 v8, 0x0

    .line 22
    invoke-interface {p1}, Lcom/google/c/a/a/b/b/b/y;->c()Lcom/google/c/b/a/c;

    move-result-object v0

    check-cast v0, Lcom/google/c/a/a/b/b/a/c/m;

    .line 23
    invoke-interface {p2}, Lcom/google/c/a/a/b/b/b/y;->c()Lcom/google/c/b/a/c;

    move-result-object v1

    check-cast v1, Lcom/google/c/a/a/b/b/a/c/s;

    .line 24
    invoke-virtual {v0}, Lcom/google/c/a/a/b/b/a/c/m;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1}, Lcom/google/c/a/a/b/b/a/c/s;->e()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    invoke-virtual {v0}, Lcom/google/c/a/a/b/b/a/c/m;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1}, Lcom/google/c/a/a/b/b/a/c/s;->e()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 97
    :goto_0
    return-void

    .line 29
    :cond_0
    invoke-virtual {v0}, Lcom/google/c/a/a/b/b/a/c/m;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1}, Lcom/google/c/a/a/b/b/a/c/s;->e()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {v0}, Lcom/google/c/a/a/b/b/a/c/m;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1}, Lcom/google/c/a/a/b/b/a/c/s;->e()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {v0}, Lcom/google/c/a/a/b/b/a/c/m;->c()I

    move-result v2

    invoke-virtual {v1}, Lcom/google/c/a/a/b/b/a/c/s;->c()I

    move-result v3

    if-lt v2, v3, :cond_1

    invoke-virtual {v0}, Lcom/google/c/a/a/b/b/a/c/m;->c()I

    move-result v2

    invoke-virtual {v1}, Lcom/google/c/a/a/b/b/a/c/s;->c()I

    move-result v3

    invoke-virtual {v1}, Lcom/google/c/a/a/b/b/a/c/s;->a()I

    move-result v4

    add-int/2addr v3, v4

    if-ge v2, v3, :cond_1

    invoke-virtual {v0}, Lcom/google/c/a/a/b/b/a/c/m;->e()I

    move-result v2

    invoke-virtual {v1}, Lcom/google/c/a/a/b/b/a/c/s;->c()I

    move-result v3

    if-le v2, v3, :cond_1

    invoke-virtual {v0}, Lcom/google/c/a/a/b/b/a/c/m;->e()I

    move-result v2

    invoke-virtual {v1}, Lcom/google/c/a/a/b/b/a/c/s;->c()I

    move-result v3

    invoke-virtual {v1}, Lcom/google/c/a/a/b/b/a/c/s;->a()I

    move-result v4

    add-int/2addr v3, v4

    if-ge v2, v3, :cond_1

    .line 36
    invoke-virtual {v0}, Lcom/google/c/a/a/b/b/a/c/m;->c()I

    move-result v2

    invoke-virtual {v1}, Lcom/google/c/a/a/b/b/a/c/s;->c()I

    move-result v3

    sub-int v3, v2, v3

    .line 37
    invoke-virtual {v0}, Lcom/google/c/a/a/b/b/a/c/m;->e()I

    move-result v0

    invoke-virtual {v1}, Lcom/google/c/a/a/b/b/a/c/s;->c()I

    move-result v2

    sub-int/2addr v0, v2

    .line 38
    if-le v0, v3, :cond_8

    .line 39
    add-int/lit8 v0, v0, -0x1

    move v2, v0

    .line 41
    :goto_1
    new-instance v4, Ljava/util/ArrayList;

    invoke-virtual {v1}, Lcom/google/c/a/a/b/b/a/c/s;->d()Ljava/util/List;

    move-result-object v0

    invoke-direct {v4, v0}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 42
    invoke-interface {v4, v3}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/c/a/a/b/b/a/r;

    .line 43
    invoke-interface {v4, v2, v0}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 44
    invoke-virtual {v1}, Lcom/google/c/a/a/b/b/a/c/s;->e()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1}, Lcom/google/c/a/a/b/b/a/c/s;->c()I

    move-result v1

    invoke-static {v0, v1, v4}, Lcom/google/c/a/a/b/b/b/q;->a(Ljava/lang/String;ILjava/util/List;)Lcom/google/c/b/a/c;

    move-result-object v0

    .line 45
    invoke-interface {p2, v0}, Lcom/google/c/a/a/b/b/b/y;->a(Lcom/google/c/b/a/c;)V

    goto/16 :goto_0

    .line 49
    :cond_1
    invoke-virtual {v1}, Lcom/google/c/a/a/b/b/a/c/s;->c()I

    move-result v4

    .line 50
    new-instance v5, Ljava/util/ArrayList;

    invoke-virtual {v1}, Lcom/google/c/a/a/b/b/a/c/s;->d()Ljava/util/List;

    move-result-object v2

    invoke-direct {v5, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 51
    invoke-static {}, Lcom/google/c/b/a/d;->a()Lcom/google/c/b/a/d;

    move-result-object v2

    .line 53
    invoke-virtual {v0}, Lcom/google/c/a/a/b/b/a/c/m;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1}, Lcom/google/c/a/a/b/b/a/c/s;->e()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_7

    .line 54
    invoke-virtual {v0}, Lcom/google/c/a/a/b/b/a/c/m;->c()I

    move-result v3

    invoke-virtual {v1}, Lcom/google/c/a/a/b/b/a/c/s;->c()I

    move-result v6

    if-lt v3, v6, :cond_2

    invoke-virtual {v0}, Lcom/google/c/a/a/b/b/a/c/m;->c()I

    move-result v3

    invoke-virtual {v1}, Lcom/google/c/a/a/b/b/a/c/s;->c()I

    move-result v6

    invoke-virtual {v1}, Lcom/google/c/a/a/b/b/a/c/s;->a()I

    move-result v7

    add-int/2addr v6, v7

    if-ge v3, v6, :cond_2

    .line 57
    invoke-virtual {v0}, Lcom/google/c/a/a/b/b/a/c/m;->c()I

    move-result v2

    invoke-virtual {v1}, Lcom/google/c/a/a/b/b/a/c/s;->c()I

    move-result v3

    sub-int/2addr v2, v3

    .line 60
    invoke-interface {v5, v2}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/c/a/a/b/b/a/r;

    .line 64
    invoke-virtual {v0}, Lcom/google/c/a/a/b/b/a/c/m;->e()I

    move-result v3

    invoke-static {v3, v0}, Lcom/google/c/a/a/b/b/b/q;->a(ILcom/google/c/a/a/b/b/a/c/m;)I

    move-result v6

    .line 65
    new-instance v3, Lcom/google/c/a/a/b/b/a/c/s;

    invoke-virtual {v0}, Lcom/google/c/a/a/b/b/a/c/m;->d()Ljava/lang/String;

    move-result-object v7

    invoke-static {v2}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    invoke-direct {v3, v7, v6, v2}, Lcom/google/c/a/a/b/b/a/c/s;-><init>(Ljava/lang/String;ILjava/util/List;)V

    move-object v2, v3

    move v3, v4

    .line 71
    :goto_2
    invoke-virtual {v0}, Lcom/google/c/a/a/b/b/a/c/m;->d()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1}, Lcom/google/c/a/a/b/b/a/c/s;->e()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_6

    .line 72
    invoke-virtual {v0}, Lcom/google/c/a/a/b/b/a/c/m;->e()I

    move-result v4

    invoke-virtual {v1}, Lcom/google/c/a/a/b/b/a/c/s;->c()I

    move-result v6

    if-le v4, v6, :cond_3

    invoke-virtual {v0}, Lcom/google/c/a/a/b/b/a/c/m;->e()I

    move-result v4

    invoke-virtual {v1}, Lcom/google/c/a/a/b/b/a/c/s;->c()I

    move-result v6

    invoke-virtual {v1}, Lcom/google/c/a/a/b/b/a/c/s;->a()I

    move-result v7

    add-int/2addr v6, v7

    if-ge v4, v6, :cond_3

    .line 75
    invoke-virtual {v0}, Lcom/google/c/a/a/b/b/a/c/m;->e()I

    move-result v2

    invoke-virtual {v1}, Lcom/google/c/a/a/b/b/a/c/s;->c()I

    move-result v4

    sub-int v4, v2, v4

    .line 76
    invoke-virtual {v0}, Lcom/google/c/a/a/b/b/a/c/m;->e()I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    invoke-static {v2, v0}, Lcom/google/c/a/a/b/b/b/q;->a(ILcom/google/c/a/a/b/b/a/c/m;)I

    move-result v0

    .line 77
    invoke-virtual {v1}, Lcom/google/c/a/a/b/b/a/c/s;->e()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v6

    invoke-interface {v5, v4, v6}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v6

    invoke-static {v2, v0, v6}, Lcom/google/c/a/a/b/b/b/q;->a(Ljava/lang/String;ILjava/util/List;)Lcom/google/c/b/a/c;

    move-result-object v2

    .line 79
    invoke-interface {v5, v8, v4}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v0

    .line 85
    :goto_3
    invoke-virtual {v1}, Lcom/google/c/a/a/b/b/a/c/s;->e()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, v3, v0}, Lcom/google/c/a/a/b/b/b/q;->a(Ljava/lang/String;ILjava/util/List;)Lcom/google/c/b/a/c;

    move-result-object v0

    .line 86
    instance-of v1, v2, Lcom/google/c/b/a/d;

    if-nez v1, :cond_5

    .line 87
    instance-of v1, v0, Lcom/google/c/b/a/d;

    if-eqz v1, :cond_4

    .line 96
    :goto_4
    invoke-interface {p2, v2}, Lcom/google/c/a/a/b/b/b/y;->a(Lcom/google/c/b/a/c;)V

    goto/16 :goto_0

    .line 67
    :cond_2
    invoke-virtual {v0}, Lcom/google/c/a/a/b/b/a/c/m;->c()I

    move-result v3

    invoke-virtual {v1}, Lcom/google/c/a/a/b/b/a/c/s;->c()I

    move-result v6

    if-ge v3, v6, :cond_7

    .line 68
    add-int/lit8 v3, v4, -0x1

    goto :goto_2

    .line 80
    :cond_3
    invoke-virtual {v0}, Lcom/google/c/a/a/b/b/a/c/m;->e()I

    move-result v0

    invoke-virtual {v1}, Lcom/google/c/a/a/b/b/a/c/s;->c()I

    move-result v4

    if-gt v0, v4, :cond_6

    .line 81
    add-int/lit8 v3, v3, 0x1

    move-object v0, v5

    goto :goto_3

    .line 90
    :cond_4
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 91
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 92
    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 93
    new-instance v2, Lcom/google/c/a/a/b/b/a/c/n;

    const-string v0, ""

    const/4 v3, 0x1

    invoke-direct {v2, v0, v1, v8, v3}, Lcom/google/c/a/a/b/b/a/c/n;-><init>(Ljava/lang/String;Ljava/util/List;ZZ)V

    goto :goto_4

    :cond_5
    move-object v2, v0

    goto :goto_4

    :cond_6
    move-object v0, v5

    goto :goto_3

    :cond_7
    move v3, v4

    goto/16 :goto_2

    :cond_8
    move v2, v0

    goto/16 :goto_1
.end method
