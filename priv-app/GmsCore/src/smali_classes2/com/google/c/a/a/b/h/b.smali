.class public final Lcom/google/c/a/a/b/h/b;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method public static a(Ljava/lang/Object;Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 20
    if-ne p0, p1, :cond_0

    .line 21
    const/4 v0, 0x1

    .line 26
    :goto_0
    return v0

    .line 23
    :cond_0
    if-eqz p0, :cond_1

    if-nez p1, :cond_2

    .line 24
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 26
    :cond_2
    invoke-virtual {p0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public static varargs a([Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 30
    array-length v0, p0

    rem-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_0

    .line 31
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "There must be an even number of fields to compare."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    move v0, v1

    .line 33
    :goto_0
    array-length v2, p0

    if-ge v0, v2, :cond_2

    .line 34
    aget-object v2, p0, v0

    add-int/lit8 v3, v0, 0x1

    aget-object v3, p0, v3

    invoke-static {v2, v3}, Lcom/google/c/a/a/b/h/b;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 38
    :goto_1
    return v1

    .line 33
    :cond_1
    add-int/lit8 v0, v0, 0x2

    goto :goto_0

    .line 38
    :cond_2
    const/4 v1, 0x1

    goto :goto_1
.end method
