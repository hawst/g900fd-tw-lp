.class public Lcom/google/c/a/a/b/g/b;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/c/a/a/b/g/d;


# direct methods
.method public constructor <init>(Lcom/google/c/a/a/b/g/d;)V
    .locals 1

    .prologue
    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 12
    const-string v0, "metadata"

    invoke-static {p1, v0}, Lcom/google/c/a/a/b/h/c;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/c/a/a/b/g/d;

    iput-object v0, p0, Lcom/google/c/a/a/b/g/b;->a:Lcom/google/c/a/a/b/g/d;

    .line 13
    return-void
.end method


# virtual methods
.method public final a()Lcom/google/c/a/a/b/g/d;
    .locals 1

    .prologue
    .line 19
    iget-object v0, p0, Lcom/google/c/a/a/b/g/b;->a:Lcom/google/c/a/a/b/g/d;

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 34
    instance-of v0, p1, Lcom/google/c/a/a/b/g/b;

    if-nez v0, :cond_0

    .line 35
    const/4 v0, 0x0

    .line 38
    :goto_0
    return v0

    .line 37
    :cond_0
    check-cast p1, Lcom/google/c/a/a/b/g/b;

    .line 38
    iget-object v0, p0, Lcom/google/c/a/a/b/g/b;->a:Lcom/google/c/a/a/b/g/d;

    iget-object v1, p1, Lcom/google/c/a/a/b/g/b;->a:Lcom/google/c/a/a/b/g/d;

    invoke-virtual {v0, v1}, Lcom/google/c/a/a/b/g/d;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 29
    iget-object v0, p0, Lcom/google/c/a/a/b/g/b;->a:Lcom/google/c/a/a/b/g/d;

    invoke-virtual {v0}, Lcom/google/c/a/a/b/g/d;->hashCode()I

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 24
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "SessionJoinEvent [metadata="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/c/a/a/b/g/b;->a:Lcom/google/c/a/a/b/g/d;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
