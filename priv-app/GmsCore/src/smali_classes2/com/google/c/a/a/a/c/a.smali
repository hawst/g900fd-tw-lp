.class public final Lcom/google/c/a/a/a/c/a;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/c/a/a/a/c/a/a/k;
.implements Lcom/google/c/a/a/a/c/a/c;
.implements Lcom/google/c/a/a/a/c/a/g;
.implements Lcom/google/c/a/a/a/c/i;


# instance fields
.field final a:Lcom/google/c/a/a/b/d/b;

.field final b:Lcom/google/c/a/a/a/c/s;

.field final c:Ljava/util/Map;

.field d:Ljava/lang/String;

.field e:Ljava/lang/String;

.field volatile f:J

.field final g:Lcom/google/c/a/a/a/c/a/e/f;

.field private final h:Lcom/google/c/a/a/a/c/a/e/c;

.field private final i:Lcom/google/c/a/a/a/c/a/e/a;

.field private j:Ljava/lang/String;

.field private k:Lcom/google/c/a/a/a/c/a/a;

.field private final l:Lcom/google/c/a/a/a/c/u;

.field private final m:Lcom/google/c/a/a/a/c/a/d/k;

.field private final n:Ljava/lang/String;

.field private final o:Ljava/lang/String;

.field private final p:Ljava/lang/String;

.field private q:I

.field private r:Lcom/google/c/a/a/a/c/a/b/f;

.field private s:Lcom/google/c/a/a/a/c/a/b/d;

.field private t:Lcom/google/c/a/a/a/c/a/g;


# direct methods
.method public constructor <init>(Lcom/google/c/a/a/b/d/b;Lcom/google/c/a/a/a/c/s;Lcom/google/c/a/a/a/c/u;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/c/a/a/a/c/a/d/k;Lcom/google/c/a/a/a/c/a/e/b;Lcom/google/c/a/a/a/c/a/e/f;)V
    .locals 2

    .prologue
    .line 105
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 75
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/c/a/a/a/c/a;->c:Ljava/util/Map;

    .line 77
    new-instance v0, Lcom/google/c/a/a/a/c/a/e/j;

    invoke-direct {v0}, Lcom/google/c/a/a/a/c/a/e/j;-><init>()V

    iput-object v0, p0, Lcom/google/c/a/a/a/c/a;->h:Lcom/google/c/a/a/a/c/a/e/c;

    .line 87
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/c/a/a/a/c/a;->f:J

    .line 106
    iput-object p1, p0, Lcom/google/c/a/a/a/c/a;->a:Lcom/google/c/a/a/b/d/b;

    .line 107
    iput-object p2, p0, Lcom/google/c/a/a/a/c/a;->b:Lcom/google/c/a/a/a/c/s;

    .line 108
    iput-object p3, p0, Lcom/google/c/a/a/a/c/a;->l:Lcom/google/c/a/a/a/c/u;

    .line 109
    iput-object p4, p0, Lcom/google/c/a/a/a/c/a;->n:Ljava/lang/String;

    .line 110
    iput-object p5, p0, Lcom/google/c/a/a/a/c/a;->o:Ljava/lang/String;

    .line 111
    iput-object p6, p0, Lcom/google/c/a/a/a/c/a;->p:Ljava/lang/String;

    .line 112
    new-instance v0, Lcom/google/c/a/a/a/c/b;

    invoke-direct {v0, p0}, Lcom/google/c/a/a/a/c/b;-><init>(Lcom/google/c/a/a/a/c/a;)V

    iput-object v0, p0, Lcom/google/c/a/a/a/c/a;->r:Lcom/google/c/a/a/a/c/a/b/f;

    .line 123
    invoke-direct {p0}, Lcom/google/c/a/a/a/c/a;->d()Lcom/google/c/a/a/a/c/a/b/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/c/a/a/a/c/a/b/e;->a()Lcom/google/c/a/a/a/c/a/b/d;

    move-result-object v0

    iput-object v0, p0, Lcom/google/c/a/a/a/c/a;->s:Lcom/google/c/a/a/a/c/a/b/d;

    .line 124
    iput-object p7, p0, Lcom/google/c/a/a/a/c/a;->m:Lcom/google/c/a/a/a/c/a/d/k;

    .line 125
    iget-object v0, p0, Lcom/google/c/a/a/a/c/a;->h:Lcom/google/c/a/a/a/c/a/e/c;

    invoke-interface {p8, v0}, Lcom/google/c/a/a/a/c/a/e/b;->a(Lcom/google/c/a/a/a/c/a/e/c;)Lcom/google/c/a/a/a/c/a/e/a;

    move-result-object v0

    iput-object v0, p0, Lcom/google/c/a/a/a/c/a;->i:Lcom/google/c/a/a/a/c/a/e/a;

    .line 126
    iput-object p9, p0, Lcom/google/c/a/a/a/c/a;->g:Lcom/google/c/a/a/a/c/a/e/f;

    .line 127
    return-void
.end method

.method private final d()Lcom/google/c/a/a/a/c/a/b/e;
    .locals 3

    .prologue
    .line 130
    new-instance v0, Lcom/google/c/a/a/a/c/a/b/e;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/google/c/a/a/a/c/a/b/e;-><init>(B)V

    iget-object v1, p0, Lcom/google/c/a/a/a/c/a;->n:Ljava/lang/String;

    const-string v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    iput-object v1, v0, Lcom/google/c/a/a/a/c/a/b/e;->a:Ljava/lang/String;

    :goto_0
    iget-object v1, p0, Lcom/google/c/a/a/a/c/a;->r:Lcom/google/c/a/a/a/c/a/b/f;

    iput-object v1, v0, Lcom/google/c/a/a/a/c/a/b/e;->e:Lcom/google/c/a/a/a/c/a/b/f;

    const-string v1, "rctype"

    iget-object v2, p0, Lcom/google/c/a/a/a/c/a;->o:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/google/c/a/a/a/c/a/b/e;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/c/a/a/a/c/a/b/e;

    move-result-object v0

    const-string v1, "rcver"

    iget-object v2, p0, Lcom/google/c/a/a/a/c/a;->p:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/google/c/a/a/a/c/a/b/e;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/c/a/a/a/c/a/b/e;

    move-result-object v0

    return-object v0

    :cond_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/google/c/a/a/a/c/a/b/e;->a:Ljava/lang/String;

    goto :goto_0
.end method


# virtual methods
.method public final declared-synchronized a()V
    .locals 2

    .prologue
    const/4 v1, 0x3

    .line 287
    monitor-enter p0

    :try_start_0
    iget v0, p0, Lcom/google/c/a/a/a/c/a;->q:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-ne v0, v1, :cond_1

    .line 294
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 290
    :cond_1
    const/4 v0, 0x3

    :try_start_1
    iput v0, p0, Lcom/google/c/a/a/a/c/a;->q:I

    .line 291
    iget-object v0, p0, Lcom/google/c/a/a/a/c/a;->k:Lcom/google/c/a/a/a/c/a/a;

    if-eqz v0, :cond_0

    .line 292
    iget-object v0, p0, Lcom/google/c/a/a/a/c/a;->k:Lcom/google/c/a/a/a/c/a/a;

    invoke-virtual {v0}, Lcom/google/c/a/a/a/c/a/a;->b()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 287
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method final a(I)V
    .locals 2

    .prologue
    .line 317
    if-nez p1, :cond_0

    .line 344
    :goto_0
    return-void

    .line 322
    :cond_0
    iget-object v0, p0, Lcom/google/c/a/a/a/c/a;->g:Lcom/google/c/a/a/a/c/a/e/f;

    new-instance v1, Lcom/google/c/a/a/a/c/h;

    invoke-direct {v1, p0, p1}, Lcom/google/c/a/a/a/c/h;-><init>(Lcom/google/c/a/a/a/c/a;I)V

    invoke-interface {v0, v1}, Lcom/google/c/a/a/a/c/a/e/f;->a(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method public final a(Lcom/google/c/a/a/a/c/a/a/l;)V
    .locals 6

    .prologue
    .line 229
    iget-object v0, p1, Lcom/google/c/a/a/a/c/a/a/l;->b:Ljava/lang/Object;

    .line 230
    iget-object v1, p0, Lcom/google/c/a/a/a/c/a;->a:Lcom/google/c/a/a/b/d/b;

    iget-object v2, p1, Lcom/google/c/a/a/a/c/a/a/l;->a:Ljava/lang/Object;

    invoke-interface {v1, v2}, Lcom/google/c/a/a/b/d/b;->b(Ljava/lang/Object;)D

    move-result-wide v2

    const-wide/high16 v4, 0x4008000000000000L    # 3.0

    cmpl-double v1, v2, v4

    if-nez v1, :cond_0

    .line 231
    monitor-enter p0

    .line 232
    :try_start_0
    iget-object v1, p0, Lcom/google/c/a/a/a/c/a;->a:Lcom/google/c/a/a/b/d/b;

    iget-object v2, p0, Lcom/google/c/a/a/a/c/a;->a:Lcom/google/c/a/a/b/d/b;

    const/4 v3, 0x0

    invoke-interface {v2, v0, v3}, Lcom/google/c/a/a/b/d/b;->a(Ljava/lang/Object;I)Ljava/lang/Object;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/google/c/a/a/b/d/b;->b(Ljava/lang/Object;)D

    move-result-wide v2

    double-to-long v2, v2

    .line 233
    iget-wide v4, p0, Lcom/google/c/a/a/a/c/a;->f:J

    cmp-long v1, v2, v4

    if-gtz v1, :cond_1

    .line 235
    monitor-exit p0

    .line 243
    :cond_0
    :goto_0
    return-void

    .line 237
    :cond_1
    iput-wide v2, p0, Lcom/google/c/a/a/a/c/a;->f:J

    .line 238
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 239
    iget-object v1, p0, Lcom/google/c/a/a/a/c/a;->a:Lcom/google/c/a/a/b/d/b;

    const/4 v2, 0x1

    invoke-interface {v1, v0, v2}, Lcom/google/c/a/a/b/d/b;->a(Ljava/lang/Object;I)Ljava/lang/Object;

    move-result-object v0

    .line 240
    iget-object v1, p0, Lcom/google/c/a/a/a/c/a;->a:Lcom/google/c/a/a/b/d/b;

    invoke-static {v0, v1}, Lcom/google/c/a/a/b/f/f;->a(Ljava/lang/Object;Lcom/google/c/a/a/b/d/b;)Ljava/lang/Object;

    move-result-object v0

    .line 241
    iget-object v1, p0, Lcom/google/c/a/a/a/c/a;->g:Lcom/google/c/a/a/a/c/a/e/f;

    new-instance v2, Lcom/google/c/a/a/a/c/g;

    invoke-direct {v2, p0, v0}, Lcom/google/c/a/a/a/c/g;-><init>(Lcom/google/c/a/a/a/c/a;Ljava/lang/Object;)V

    invoke-interface {v1, v2}, Lcom/google/c/a/a/a/c/a/e/f;->a(Ljava/lang/Runnable;)V

    goto :goto_0

    .line 238
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final a(Lcom/google/c/a/a/a/c/a/e/e;I)V
    .locals 1

    .prologue
    .line 270
    sget-object v0, Lcom/google/c/a/a/a/c/a/e/e;->b:Lcom/google/c/a/a/a/c/a/e/e;

    if-ne p1, v0, :cond_0

    .line 271
    invoke-virtual {p0, p2}, Lcom/google/c/a/a/a/c/a;->a(I)V

    .line 273
    :cond_0
    return-void
.end method

.method public final declared-synchronized a(Lcom/google/c/a/a/a/c/a/e;)V
    .locals 3

    .prologue
    .line 247
    monitor-enter p0

    :try_start_0
    iget v0, p0, Lcom/google/c/a/a/a/c/a;->q:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/4 v1, 0x1

    if-eq v0, v1, :cond_1

    .line 265
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 250
    :cond_1
    :try_start_1
    iget-object v0, p0, Lcom/google/c/a/a/a/c/a;->t:Lcom/google/c/a/a/a/c/a/g;

    if-eqz v0, :cond_2

    .line 251
    iget-object v0, p0, Lcom/google/c/a/a/a/c/a;->t:Lcom/google/c/a/a/a/c/a/g;

    invoke-interface {v0, p1}, Lcom/google/c/a/a/a/c/a/g;->a(Lcom/google/c/a/a/a/c/a/e;)V

    .line 253
    :cond_2
    iget-object v0, p1, Lcom/google/c/a/a/a/c/a/e;->a:Lcom/google/c/a/a/a/c/a/d;

    sget-object v1, Lcom/google/c/a/a/a/c/a/d;->d:Lcom/google/c/a/a/a/c/a/d;

    if-ne v0, v1, :cond_3

    .line 254
    iget-object v0, p0, Lcom/google/c/a/a/a/c/a;->h:Lcom/google/c/a/a/a/c/a/e/c;

    sget-object v1, Lcom/google/c/a/a/a/c/a/e/d;->a:Lcom/google/c/a/a/a/c/a/e/d;

    invoke-interface {v0, v1}, Lcom/google/c/a/a/a/c/a/e/c;->a(Lcom/google/c/a/a/a/c/a/e/d;)D
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 247
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 255
    :cond_3
    :try_start_2
    iget-object v0, p1, Lcom/google/c/a/a/a/c/a/e;->a:Lcom/google/c/a/a/a/c/a/d;

    sget-object v1, Lcom/google/c/a/a/a/c/a/d;->e:Lcom/google/c/a/a/a/c/a/d;

    if-ne v0, v1, :cond_0

    .line 256
    const/4 v0, 0x4

    iput v0, p0, Lcom/google/c/a/a/a/c/a;->q:I

    .line 258
    iget-object v0, p0, Lcom/google/c/a/a/a/c/a;->i:Lcom/google/c/a/a/a/c/a/e/a;

    sget-object v1, Lcom/google/c/a/a/a/c/a/e/d;->b:Lcom/google/c/a/a/a/c/a/e/d;

    new-instance v2, Lcom/google/c/a/a/a/c/f;

    invoke-direct {v2, p0}, Lcom/google/c/a/a/a/c/f;-><init>(Lcom/google/c/a/a/a/c/a;)V

    invoke-interface {v0, v1, v2}, Lcom/google/c/a/a/a/c/a/e/a;->a(Lcom/google/c/a/a/a/c/a/e/d;Ljava/lang/Runnable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method

.method public final declared-synchronized a(Lcom/google/c/a/a/a/c/a/g;)V
    .locals 1

    .prologue
    .line 348
    monitor-enter p0

    :try_start_0
    iput-object p1, p0, Lcom/google/c/a/a/a/c/a;->t:Lcom/google/c/a/a/a/c/a/g;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 349
    monitor-exit p0

    return-void

    .line 348
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final a(Ljava/lang/Class;Lcom/google/c/a/a/a/c/r;)V
    .locals 2

    .prologue
    .line 277
    iget-object v0, p0, Lcom/google/c/a/a/a/c/a;->c:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 278
    if-nez v0, :cond_0

    .line 279
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 280
    iget-object v1, p0, Lcom/google/c/a/a/a/c/a;->c:Ljava/util/Map;

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 282
    :cond_0
    invoke-interface {v0, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 283
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 383
    iput-object p1, p0, Lcom/google/c/a/a/a/c/a;->j:Ljava/lang/String;

    .line 384
    return-void
.end method

.method public final declared-synchronized a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 5

    .prologue
    .line 199
    monitor-enter p0

    :try_start_0
    iget v0, p0, Lcom/google/c/a/a/a/c/a;->q:I

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/google/c/a/a/a/c/a;->q:I

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    iget v0, p0, Lcom/google/c/a/a/a/c/a;->q:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/4 v1, 0x4

    if-eq v0, v1, :cond_0

    .line 225
    :goto_0
    monitor-exit p0

    return-void

    .line 202
    :cond_0
    const/4 v0, 0x1

    :try_start_1
    iput v0, p0, Lcom/google/c/a/a/a/c/a;->q:I

    .line 204
    iput-object p1, p0, Lcom/google/c/a/a/a/c/a;->d:Ljava/lang/String;

    .line 205
    iput-object p2, p0, Lcom/google/c/a/a/a/c/a;->e:Ljava/lang/String;

    .line 207
    iget-object v0, p0, Lcom/google/c/a/a/a/c/a;->k:Lcom/google/c/a/a/a/c/a/a;

    if-eqz v0, :cond_1

    .line 208
    iget-object v0, p0, Lcom/google/c/a/a/a/c/a;->k:Lcom/google/c/a/a/a/c/a/a;

    invoke-virtual {v0}, Lcom/google/c/a/a/a/c/a/a;->b()V

    .line 209
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/c/a/a/a/c/a;->k:Lcom/google/c/a/a/a/c/a/a;

    .line 212
    :cond_1
    invoke-direct {p0}, Lcom/google/c/a/a/a/c/a;->d()Lcom/google/c/a/a/a/c/a/b/e;

    move-result-object v0

    const-string v1, "id"

    invoke-virtual {v0, v1, p1}, Lcom/google/c/a/a/a/c/a/b/e;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/c/a/a/a/c/a/b/e;

    move-result-object v0

    const-string v1, "sid"

    invoke-virtual {v0, v1, p2}, Lcom/google/c/a/a/a/c/a/b/e;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/c/a/a/a/c/a/b/e;

    move-result-object v0

    .line 215
    new-instance v1, Lcom/google/c/a/a/a/c/a/c/j;

    iget-object v2, p0, Lcom/google/c/a/a/a/c/a;->m:Lcom/google/c/a/a/a/c/a/d/k;

    invoke-direct {v1, v2}, Lcom/google/c/a/a/a/c/a/c/j;-><init>(Lcom/google/c/a/a/a/c/a/d/k;)V

    .line 217
    new-instance v2, Lcom/google/c/a/a/a/c/a/a/d;

    iget-object v3, p0, Lcom/google/c/a/a/a/c/a;->a:Lcom/google/c/a/a/b/d/b;

    iget-object v4, p0, Lcom/google/c/a/a/a/c/a;->m:Lcom/google/c/a/a/a/c/a/d/k;

    invoke-direct {v2, v3, v4}, Lcom/google/c/a/a/a/c/a/a/d;-><init>(Lcom/google/c/a/a/b/d/b;Lcom/google/c/a/a/a/c/a/d/k;)V

    .line 219
    new-instance v3, Lcom/google/c/a/a/a/c/a/a;

    invoke-virtual {v0}, Lcom/google/c/a/a/a/c/a/b/e;->a()Lcom/google/c/a/a/a/c/a/b/d;

    move-result-object v0

    iget-object v4, p0, Lcom/google/c/a/a/a/c/a;->i:Lcom/google/c/a/a/a/c/a/e/a;

    invoke-direct {v3, v0, v4, v1, v2}, Lcom/google/c/a/a/a/c/a/a;-><init>(Lcom/google/c/a/a/a/c/a/b/d;Lcom/google/c/a/a/a/c/a/e/a;Lcom/google/c/a/a/a/c/a/c/f;Lcom/google/c/a/a/a/c/a/a/m;)V

    iput-object v3, p0, Lcom/google/c/a/a/a/c/a;->k:Lcom/google/c/a/a/a/c/a/a;

    .line 221
    iget-object v0, p0, Lcom/google/c/a/a/a/c/a;->k:Lcom/google/c/a/a/a/c/a/a;

    invoke-virtual {v0, p0}, Lcom/google/c/a/a/a/c/a/a;->a(Lcom/google/c/a/a/a/c/a/a/k;)V

    .line 222
    iget-object v0, p0, Lcom/google/c/a/a/a/c/a;->k:Lcom/google/c/a/a/a/c/a/a;

    invoke-virtual {v0, p0}, Lcom/google/c/a/a/a/c/a/a;->a(Lcom/google/c/a/a/a/c/a/c;)V

    .line 223
    iget-object v0, p0, Lcom/google/c/a/a/a/c/a;->k:Lcom/google/c/a/a/a/c/a/a;

    invoke-virtual {v0, p0}, Lcom/google/c/a/a/a/c/a/a;->a(Lcom/google/c/a/a/a/c/a/g;)V

    .line 224
    iget-object v0, p0, Lcom/google/c/a/a/a/c/a;->k:Lcom/google/c/a/a/a/c/a/a;

    invoke-virtual {v0}, Lcom/google/c/a/a/a/c/a/a;->a()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 199
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final a(Ljava/lang/String;Ljava/util/Map;Ljava/lang/Object;ZLcom/google/c/a/a/b/e/f;Ljava/lang/Class;)V
    .locals 4

    .prologue
    .line 141
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0, p2}, Ljava/util/HashMap;-><init>(Ljava/util/Map;)V

    .line 142
    invoke-virtual {p0, v0}, Lcom/google/c/a/a/a/c/a;->a(Ljava/util/Map;)V

    .line 144
    const/4 v1, 0x0

    .line 146
    if-eqz p3, :cond_0

    .line 147
    sget-object v0, Lcom/google/c/a/a/a/c/a/d/r;->b:Lcom/google/c/a/a/a/c/a/d/r;

    .line 149
    :try_start_0
    iget-object v1, p0, Lcom/google/c/a/a/a/c/a;->a:Lcom/google/c/a/a/b/d/b;

    iget-object v2, p0, Lcom/google/c/a/a/a/c/a;->a:Lcom/google/c/a/a/b/d/b;

    const/4 v3, 0x0

    invoke-static {p3, v2, v3}, Lcom/google/c/a/a/b/f/f;->a(Ljava/lang/Object;Lcom/google/c/a/a/b/d/b;Z)Ljava/lang/Object;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/google/c/a/a/b/d/b;->d(Ljava/lang/Object;)Ljava/lang/String;
    :try_end_0
    .catch Lcom/google/c/a/a/b/d/a; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 156
    :goto_0
    iget-object v2, p0, Lcom/google/c/a/a/a/c/a;->s:Lcom/google/c/a/a/a/c/a/b/d;

    invoke-static {v2, p1, p2, v0, v1}, Lcom/google/c/a/a/a/c/a/d/p;->a(Lcom/google/c/a/a/a/c/a/b/d;Ljava/lang/String;Ljava/util/Map;Lcom/google/c/a/a/a/c/a/d/r;Ljava/lang/String;)Lcom/google/c/a/a/a/c/a/d/p;

    move-result-object v0

    .line 158
    new-instance v1, Lcom/google/c/a/a/a/c/a/d/l;

    iget-object v2, p0, Lcom/google/c/a/a/a/c/a;->m:Lcom/google/c/a/a/a/c/a/d/k;

    invoke-interface {v2, v0, p4}, Lcom/google/c/a/a/a/c/a/d/k;->a(Lcom/google/c/a/a/a/c/a/d/p;Z)Lcom/google/c/a/a/a/c/a/d/b;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/c/a/a/a/c/a/d/l;-><init>(Lcom/google/c/a/a/a/c/a/d/b;)V

    .line 159
    new-instance v0, Lcom/google/c/a/a/a/c/c;

    invoke-direct {v0, p0, p6, p5}, Lcom/google/c/a/a/a/c/c;-><init>(Lcom/google/c/a/a/a/c/a;Ljava/lang/Class;Lcom/google/c/a/a/b/e/f;)V

    iput-object v0, v1, Lcom/google/c/a/a/a/c/a/d/l;->d:Lcom/google/c/a/a/a/c/a/d/n;

    .line 194
    invoke-virtual {v1}, Lcom/google/c/a/a/a/c/a/d/l;->a()V

    .line 195
    return-void

    .line 150
    :catch_0
    move-exception v0

    .line 151
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 154
    :cond_0
    sget-object v0, Lcom/google/c/a/a/a/c/a/d/r;->a:Lcom/google/c/a/a/a/c/a/d/r;

    goto :goto_0
.end method

.method final a(Ljava/util/Map;)V
    .locals 2

    .prologue
    .line 373
    iget-object v0, p0, Lcom/google/c/a/a/a/c/a;->l:Lcom/google/c/a/a/a/c/u;

    invoke-interface {v0}, Lcom/google/c/a/a/a/c/u;->a()Ljava/lang/String;

    move-result-object v0

    .line 374
    if-eqz v0, :cond_1

    .line 375
    const-string v0, "access_token"

    iget-object v1, p0, Lcom/google/c/a/a/a/c/a;->l:Lcom/google/c/a/a/a/c/u;

    invoke-interface {v1}, Lcom/google/c/a/a/a/c/u;->a()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 379
    :cond_0
    :goto_0
    return-void

    .line 376
    :cond_1
    iget-object v0, p0, Lcom/google/c/a/a/a/c/a;->j:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 377
    const-string v0, "token"

    iget-object v1, p0, Lcom/google/c/a/a/a/c/a;->j:Ljava/lang/String;

    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method public final declared-synchronized b()V
    .locals 2

    .prologue
    .line 353
    monitor-enter p0

    :try_start_0
    iget v0, p0, Lcom/google/c/a/a/a/c/a;->q:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 354
    const/4 v0, 0x2

    iput v0, p0, Lcom/google/c/a/a/a/c/a;->q:I

    .line 355
    iget-object v0, p0, Lcom/google/c/a/a/a/c/a;->k:Lcom/google/c/a/a/a/c/a/a;

    invoke-virtual {v0}, Lcom/google/c/a/a/a/c/a/a;->b()V

    .line 356
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/c/a/a/a/c/a;->k:Lcom/google/c/a/a/a/c/a/a;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 360
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 357
    :cond_1
    :try_start_1
    iget v0, p0, Lcom/google/c/a/a/a/c/a;->q:I

    if-nez v0, :cond_0

    .line 358
    const/4 v0, 0x2

    iput v0, p0, Lcom/google/c/a/a/a/c/a;->q:I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 353
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized c()V
    .locals 2

    .prologue
    .line 364
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/c/a/a/a/c/a;->d:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 365
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Must call initBrowserChannel() first."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 364
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 367
    :cond_0
    :try_start_1
    iget v0, p0, Lcom/google/c/a/a/a/c/a;->q:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    .line 368
    iget-object v0, p0, Lcom/google/c/a/a/a/c/a;->d:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/c/a/a/a/c/a;->e:Ljava/lang/String;

    invoke-virtual {p0, v0, v1}, Lcom/google/c/a/a/a/c/a;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 370
    :cond_1
    monitor-exit p0

    return-void
.end method
