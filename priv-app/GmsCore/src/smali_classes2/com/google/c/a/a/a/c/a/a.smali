.class public final Lcom/google/c/a/a/a/c/a/a;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final a:Ljava/util/logging/Logger;


# instance fields
.field private final b:Lcom/google/c/a/a/a/c/a/h;

.field private final c:Lcom/google/c/a/a/a/c/a/f;

.field private final d:Lcom/google/c/a/a/a/c/a/b/d;

.field private final e:Lcom/google/c/a/a/a/c/a/e/a;

.field private final f:Lcom/google/c/a/a/a/c/a/c/f;

.field private final g:Lcom/google/c/a/a/a/c/a/a/m;

.field private h:Lcom/google/c/a/a/a/c/a/a/k;

.field private i:Lcom/google/c/a/a/a/c/a/g;

.field private j:Lcom/google/c/a/a/a/c/a/c;

.field private k:Lcom/google/c/a/a/a/c/a/a/e;

.field private volatile l:Lcom/google/c/a/a/a/c/a/d;

.field private m:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 35
    const-string v0, "BrowserChannel"

    invoke-static {v0}, Ljava/util/logging/Logger;->getLogger(Ljava/lang/String;)Ljava/util/logging/Logger;

    move-result-object v0

    sput-object v0, Lcom/google/c/a/a/a/c/a/a;->a:Ljava/util/logging/Logger;

    return-void
.end method

.method public constructor <init>(Lcom/google/c/a/a/a/c/a/b/d;Lcom/google/c/a/a/a/c/a/e/a;Lcom/google/c/a/a/a/c/a/c/f;Lcom/google/c/a/a/a/c/a/a/m;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 65
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    new-instance v0, Lcom/google/c/a/a/a/c/a/h;

    invoke-direct {v0, p0, v1}, Lcom/google/c/a/a/a/c/a/h;-><init>(Lcom/google/c/a/a/a/c/a/a;B)V

    iput-object v0, p0, Lcom/google/c/a/a/a/c/a/a;->b:Lcom/google/c/a/a/a/c/a/h;

    .line 38
    new-instance v0, Lcom/google/c/a/a/a/c/a/f;

    invoke-direct {v0, p0, v1}, Lcom/google/c/a/a/a/c/a/f;-><init>(Lcom/google/c/a/a/a/c/a/a;B)V

    iput-object v0, p0, Lcom/google/c/a/a/a/c/a/a;->c:Lcom/google/c/a/a/a/c/a/f;

    .line 48
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/c/a/a/a/c/a/a;->k:Lcom/google/c/a/a/a/c/a/a/e;

    .line 49
    sget-object v0, Lcom/google/c/a/a/a/c/a/d;->a:Lcom/google/c/a/a/a/c/a/d;

    iput-object v0, p0, Lcom/google/c/a/a/a/c/a/a;->l:Lcom/google/c/a/a/a/c/a/d;

    .line 66
    iput-object p1, p0, Lcom/google/c/a/a/a/c/a/a;->d:Lcom/google/c/a/a/a/c/a/b/d;

    .line 67
    iput-object p2, p0, Lcom/google/c/a/a/a/c/a/a;->e:Lcom/google/c/a/a/a/c/a/e/a;

    .line 68
    iput-object p3, p0, Lcom/google/c/a/a/a/c/a/a;->f:Lcom/google/c/a/a/a/c/a/c/f;

    .line 69
    iput-object p4, p0, Lcom/google/c/a/a/a/c/a/a;->g:Lcom/google/c/a/a/a/c/a/a/m;

    .line 70
    return-void
.end method

.method static synthetic a(Lcom/google/c/a/a/a/c/a/a;)Lcom/google/c/a/a/a/c/a/d;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lcom/google/c/a/a/a/c/a/a;->l:Lcom/google/c/a/a/a/c/a/d;

    return-object v0
.end method

.method private declared-synchronized a(Lcom/google/c/a/a/a/c/a/a/e;)V
    .locals 1

    .prologue
    .line 146
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/c/a/a/a/c/a/a;->k:Lcom/google/c/a/a/a/c/a/a/e;

    if-eqz v0, :cond_0

    .line 147
    iget-object v0, p0, Lcom/google/c/a/a/a/c/a/a;->k:Lcom/google/c/a/a/a/c/a/a/e;

    invoke-interface {v0}, Lcom/google/c/a/a/a/c/a/a/e;->a()V

    .line 149
    :cond_0
    iput-object p1, p0, Lcom/google/c/a/a/a/c/a/a;->k:Lcom/google/c/a/a/a/c/a/a/e;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 150
    monitor-exit p0

    return-void

    .line 146
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method static synthetic a(Lcom/google/c/a/a/a/c/a/a;Lcom/google/c/a/a/a/c/a/a/e;)V
    .locals 0

    .prologue
    .line 34
    invoke-direct {p0, p1}, Lcom/google/c/a/a/a/c/a/a;->a(Lcom/google/c/a/a/a/c/a/a/e;)V

    return-void
.end method

.method static synthetic a(Lcom/google/c/a/a/a/c/a/a;Lcom/google/c/a/a/a/c/a/d;)V
    .locals 0

    .prologue
    .line 34
    invoke-direct {p0, p1}, Lcom/google/c/a/a/a/c/a/a;->a(Lcom/google/c/a/a/a/c/a/d;)V

    return-void
.end method

.method private a(Lcom/google/c/a/a/a/c/a/d;)V
    .locals 5

    .prologue
    .line 128
    iget-object v1, p0, Lcom/google/c/a/a/a/c/a/a;->l:Lcom/google/c/a/a/a/c/a/d;

    monitor-enter v1

    .line 129
    :try_start_0
    iget-object v0, p0, Lcom/google/c/a/a/a/c/a/a;->l:Lcom/google/c/a/a/a/c/a/d;

    .line 130
    iput-object p1, p0, Lcom/google/c/a/a/a/c/a/a;->l:Lcom/google/c/a/a/a/c/a/d;

    .line 131
    if-eq p1, v0, :cond_0

    .line 132
    sget-object v2, Lcom/google/c/a/a/a/c/a/a;->a:Ljava/util/logging/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Channel state changed from "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " to "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "."

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/logging/Logger;->info(Ljava/lang/String;)V

    .line 133
    iget-object v2, p0, Lcom/google/c/a/a/a/c/a/a;->i:Lcom/google/c/a/a/a/c/a/g;

    if-eqz v2, :cond_0

    .line 134
    iget-object v2, p0, Lcom/google/c/a/a/a/c/a/a;->i:Lcom/google/c/a/a/a/c/a/g;

    new-instance v3, Lcom/google/c/a/a/a/c/a/e;

    invoke-direct {v3, v0, p1}, Lcom/google/c/a/a/a/c/a/e;-><init>(Lcom/google/c/a/a/a/c/a/d;Lcom/google/c/a/a/a/c/a/d;)V

    invoke-interface {v2, v3}, Lcom/google/c/a/a/a/c/a/g;->a(Lcom/google/c/a/a/a/c/a/e;)V

    .line 138
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method static synthetic a(Lcom/google/c/a/a/a/c/a/a;Z)Z
    .locals 0

    .prologue
    .line 34
    iput-boolean p1, p0, Lcom/google/c/a/a/a/c/a/a;->m:Z

    return p1
.end method

.method static synthetic b(Lcom/google/c/a/a/a/c/a/a;)Lcom/google/c/a/a/a/c/a/b/d;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lcom/google/c/a/a/a/c/a/a;->d:Lcom/google/c/a/a/a/c/a/b/d;

    return-object v0
.end method

.method static synthetic c(Lcom/google/c/a/a/a/c/a/a;)Lcom/google/c/a/a/a/c/a/c/f;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lcom/google/c/a/a/a/c/a/a;->f:Lcom/google/c/a/a/a/c/a/c/f;

    return-object v0
.end method

.method static synthetic c()Ljava/util/logging/Logger;
    .locals 1

    .prologue
    .line 34
    sget-object v0, Lcom/google/c/a/a/a/c/a/a;->a:Ljava/util/logging/Logger;

    return-object v0
.end method

.method static synthetic d(Lcom/google/c/a/a/a/c/a/a;)Lcom/google/c/a/a/a/c/a/f;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lcom/google/c/a/a/a/c/a/a;->c:Lcom/google/c/a/a/a/c/a/f;

    return-object v0
.end method

.method static synthetic e(Lcom/google/c/a/a/a/c/a/a;)Z
    .locals 1

    .prologue
    .line 34
    iget-boolean v0, p0, Lcom/google/c/a/a/a/c/a/a;->m:Z

    return v0
.end method

.method static synthetic f(Lcom/google/c/a/a/a/c/a/a;)Lcom/google/c/a/a/a/c/a/a/m;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lcom/google/c/a/a/a/c/a/a;->g:Lcom/google/c/a/a/a/c/a/a/m;

    return-object v0
.end method

.method static synthetic g(Lcom/google/c/a/a/a/c/a/a;)Lcom/google/c/a/a/a/c/a/e/a;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lcom/google/c/a/a/a/c/a/a;->e:Lcom/google/c/a/a/a/c/a/e/a;

    return-object v0
.end method

.method static synthetic h(Lcom/google/c/a/a/a/c/a/a;)Lcom/google/c/a/a/a/c/a/h;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lcom/google/c/a/a/a/c/a/a;->b:Lcom/google/c/a/a/a/c/a/h;

    return-object v0
.end method

.method static synthetic i(Lcom/google/c/a/a/a/c/a/a;)Lcom/google/c/a/a/a/c/a/c;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lcom/google/c/a/a/a/c/a/a;->j:Lcom/google/c/a/a/a/c/a/c;

    return-object v0
.end method

.method static synthetic j(Lcom/google/c/a/a/a/c/a/a;)Lcom/google/c/a/a/a/c/a/a/k;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lcom/google/c/a/a/a/c/a/a;->h:Lcom/google/c/a/a/a/c/a/a/k;

    return-object v0
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    .line 76
    iget-object v1, p0, Lcom/google/c/a/a/a/c/a/a;->l:Lcom/google/c/a/a/a/c/a/d;

    monitor-enter v1

    .line 77
    :try_start_0
    iget-object v0, p0, Lcom/google/c/a/a/a/c/a/a;->l:Lcom/google/c/a/a/a/c/a/d;

    sget-object v2, Lcom/google/c/a/a/a/c/a/d;->a:Lcom/google/c/a/a/a/c/a/d;

    if-eq v0, v2, :cond_0

    .line 78
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v2, "Cannot open if channel is not in state NEW."

    invoke-direct {v0, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 82
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 80
    :cond_0
    :try_start_1
    sget-object v0, Lcom/google/c/a/a/a/c/a/d;->b:Lcom/google/c/a/a/a/c/a/d;

    invoke-direct {p0, v0}, Lcom/google/c/a/a/a/c/a/a;->a(Lcom/google/c/a/a/a/c/a/d;)V

    .line 81
    iget-object v0, p0, Lcom/google/c/a/a/a/c/a/a;->b:Lcom/google/c/a/a/a/c/a/h;

    invoke-virtual {v0}, Lcom/google/c/a/a/a/c/a/h;->run()V

    .line 82
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void
.end method

.method public final a(Lcom/google/c/a/a/a/c/a/a/k;)V
    .locals 0

    .prologue
    .line 106
    iput-object p1, p0, Lcom/google/c/a/a/a/c/a/a;->h:Lcom/google/c/a/a/a/c/a/a/k;

    .line 107
    return-void
.end method

.method public final a(Lcom/google/c/a/a/a/c/a/c;)V
    .locals 0

    .prologue
    .line 121
    iput-object p1, p0, Lcom/google/c/a/a/a/c/a/a;->j:Lcom/google/c/a/a/a/c/a/c;

    .line 122
    return-void
.end method

.method public final a(Lcom/google/c/a/a/a/c/a/g;)V
    .locals 0

    .prologue
    .line 114
    iput-object p1, p0, Lcom/google/c/a/a/a/c/a/a;->i:Lcom/google/c/a/a/a/c/a/g;

    .line 115
    return-void
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 89
    iget-object v1, p0, Lcom/google/c/a/a/a/c/a/a;->l:Lcom/google/c/a/a/a/c/a/d;

    monitor-enter v1

    .line 90
    :try_start_0
    sget-object v0, Lcom/google/c/a/a/a/c/a/d;->e:Lcom/google/c/a/a/a/c/a/d;

    invoke-direct {p0, v0}, Lcom/google/c/a/a/a/c/a/a;->a(Lcom/google/c/a/a/a/c/a/d;)V

    .line 91
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/c/a/a/a/c/a/a;->a(Lcom/google/c/a/a/a/c/a/a/e;)V

    .line 92
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method
