.class public abstract Lcom/google/c/a/a/b/f/h;
.super Lcom/google/c/a/a/b/f/g;
.source "SourceFile"


# instance fields
.field private final c:Ljava/lang/Class;

.field private final d:Z


# direct methods
.method public constructor <init>(Ljava/lang/Class;Ljava/lang/Class;Z)V
    .locals 1

    .prologue
    .line 33
    invoke-direct {p0, p1, p3}, Lcom/google/c/a/a/b/f/g;-><init>(Ljava/lang/Class;Z)V

    .line 34
    iput-object p2, p0, Lcom/google/c/a/a/b/f/h;->c:Ljava/lang/Class;

    .line 35
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/c/a/a/b/f/h;->d:Z

    .line 36
    return-void
.end method


# virtual methods
.method protected final a(Ljava/lang/Object;Ljava/lang/Object;Lcom/google/c/a/a/b/d/b;)V
    .locals 2

    .prologue
    .line 44
    invoke-virtual {p0, p1}, Lcom/google/c/a/a/b/f/h;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    iget-boolean v1, p0, Lcom/google/c/a/a/b/f/h;->d:Z

    invoke-static {v0, p3, v1}, Lcom/google/c/a/a/b/f/f;->a(Ljava/lang/Object;Lcom/google/c/a/a/b/d/b;Z)Ljava/lang/Object;

    move-result-object v0

    .line 45
    const/4 v1, 0x0

    invoke-virtual {p0, p2, v1, v0, p3}, Lcom/google/c/a/a/b/f/h;->a(Ljava/lang/Object;ILjava/lang/Object;Lcom/google/c/a/a/b/d/b;)V

    .line 46
    return-void
.end method

.method public a(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 39
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    iget-object v1, p0, Lcom/google/c/a/a/b/f/h;->b:Ljava/lang/Class;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method protected abstract b(Ljava/lang/Object;)Ljava/lang/Object;
.end method

.method protected final b(Ljava/lang/Object;Lcom/google/c/a/a/b/d/b;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 49
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0, p2}, Lcom/google/c/a/a/b/f/h;->a(Ljava/lang/Object;ILcom/google/c/a/a/b/d/b;)Ljava/lang/Object;

    move-result-object v0

    .line 50
    iget-object v1, p0, Lcom/google/c/a/a/b/f/h;->c:Ljava/lang/Class;

    invoke-static {v0, p2, v1}, Lcom/google/c/a/a/b/f/f;->a(Ljava/lang/Object;Lcom/google/c/a/a/b/d/b;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/c/a/a/b/f/h;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method protected abstract c(Ljava/lang/Object;)Ljava/lang/Object;
.end method
