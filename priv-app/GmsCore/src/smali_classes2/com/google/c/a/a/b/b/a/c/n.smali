.class public Lcom/google/c/a/a/b/b/a/c/n;
.super Lcom/google/c/a/a/b/b/a/c/c;
.source "SourceFile"


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:Ljava/util/List;

.field private final c:Z

.field private final d:Z


# direct methods
.method private constructor <init>(Lcom/google/c/a/a/b/b/a/c/o;)V
    .locals 4

    .prologue
    .line 26
    iget-object v0, p1, Lcom/google/c/a/a/b/b/a/c/o;->a:Ljava/lang/String;

    iget-object v1, p1, Lcom/google/c/a/a/b/b/a/c/o;->b:Ljava/util/List;

    iget-boolean v2, p1, Lcom/google/c/a/a/b/b/a/c/o;->c:Z

    iget-boolean v3, p1, Lcom/google/c/a/a/b/b/a/c/o;->d:Z

    invoke-direct {p0, v0, v1, v2, v3}, Lcom/google/c/a/a/b/b/a/c/n;-><init>(Ljava/lang/String;Ljava/util/List;ZZ)V

    .line 27
    return-void
.end method

.method synthetic constructor <init>(Lcom/google/c/a/a/b/b/a/c/o;B)V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0, p1}, Lcom/google/c/a/a/b/b/a/c/n;-><init>(Lcom/google/c/a/a/b/b/a/c/o;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/util/List;ZZ)V
    .locals 1

    .prologue
    .line 30
    invoke-direct {p0}, Lcom/google/c/a/a/b/b/a/c/c;-><init>()V

    .line 31
    const-string v0, "MultiMutation name cannot be null."

    invoke-static {p1, v0}, Lcom/google/c/a/a/b/h/c;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/google/c/a/a/b/b/a/c/n;->a:Ljava/lang/String;

    .line 32
    invoke-static {p2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/c/a/a/b/b/a/c/n;->b:Ljava/util/List;

    .line 33
    iput-boolean p3, p0, Lcom/google/c/a/a/b/b/a/c/n;->c:Z

    .line 34
    iput-boolean p4, p0, Lcom/google/c/a/a/b/b/a/c/n;->d:Z

    .line 35
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lcom/google/c/a/a/b/b/a/c/n;->a:Ljava/lang/String;

    return-object v0
.end method

.method public final b()Ljava/util/Set;
    .locals 4

    .prologue
    .line 133
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    .line 134
    iget-object v0, p0, Lcom/google/c/a/a/b/b/a/c/n;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/c/b/a/c;

    .line 135
    instance-of v3, v0, Lcom/google/c/a/a/b/b/a/c/p;

    if-eqz v3, :cond_0

    .line 136
    check-cast v0, Lcom/google/c/a/a/b/b/a/c/p;

    invoke-interface {v0}, Lcom/google/c/a/a/b/b/a/c/p;->b()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    goto :goto_0

    .line 139
    :cond_1
    return-object v1
.end method

.method public final c()Ljava/util/List;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/google/c/a/a/b/b/a/c/n;->b:Ljava/util/List;

    return-object v0
.end method

.method public final d()Z
    .locals 1

    .prologue
    .line 50
    iget-boolean v0, p0, Lcom/google/c/a/a/b/b/a/c/n;->c:Z

    return v0
.end method

.method public final e()Z
    .locals 1

    .prologue
    .line 58
    iget-boolean v0, p0, Lcom/google/c/a/a/b/b/a/c/n;->d:Z

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 68
    instance-of v1, p1, Lcom/google/c/a/a/b/b/a/c/n;

    if-nez v1, :cond_0

    .line 72
    :goto_0
    return v0

    .line 71
    :cond_0
    check-cast p1, Lcom/google/c/a/a/b/b/a/c/n;

    .line 72
    const/16 v1, 0x8

    new-array v1, v1, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/google/c/a/a/b/b/a/c/n;->a:Ljava/lang/String;

    aput-object v2, v1, v0

    const/4 v0, 0x1

    iget-object v2, p1, Lcom/google/c/a/a/b/b/a/c/n;->a:Ljava/lang/String;

    aput-object v2, v1, v0

    const/4 v0, 0x2

    iget-object v2, p0, Lcom/google/c/a/a/b/b/a/c/n;->b:Ljava/util/List;

    aput-object v2, v1, v0

    const/4 v0, 0x3

    iget-object v2, p1, Lcom/google/c/a/a/b/b/a/c/n;->b:Ljava/util/List;

    aput-object v2, v1, v0

    const/4 v0, 0x4

    iget-boolean v2, p0, Lcom/google/c/a/a/b/b/a/c/n;->c:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v1, v0

    const/4 v0, 0x5

    iget-boolean v2, p1, Lcom/google/c/a/a/b/b/a/c/n;->c:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v1, v0

    const/4 v0, 0x6

    iget-boolean v2, p0, Lcom/google/c/a/a/b/b/a/c/n;->d:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v1, v0

    const/4 v0, 0x7

    iget-boolean v2, p1, Lcom/google/c/a/a/b/b/a/c/n;->d:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v1, v0

    invoke-static {v1}, Lcom/google/c/a/a/b/h/b;->a([Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 63
    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/c/a/a/b/b/a/c/n;->a:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/c/a/a/b/b/a/c/n;->b:Ljava/util/List;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-boolean v2, p0, Lcom/google/c/a/a/b/b/a/c/n;->c:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-boolean v2, p0, Lcom/google/c/a/a/b/b/a/c/n;->d:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 81
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "MultiMutation [name="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/c/a/a/b/b/a/c/n;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mutations="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/c/a/a/b/b/a/c/n;->b:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", isCreation="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/google/c/a/a/b/b/a/c/n;->c:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", fireEventListeners="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/google/c/a/a/b/b/a/c/n;->d:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
