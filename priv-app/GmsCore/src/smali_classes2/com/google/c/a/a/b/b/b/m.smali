.class final Lcom/google/c/a/a/b/b/b/m;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/c/a/a/b/b/b/s;


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static a(Ljava/lang/String;II)Lcom/google/c/b/a/c;
    .locals 1

    .prologue
    .line 137
    if-nez p2, :cond_0

    .line 138
    invoke-static {}, Lcom/google/c/b/a/d;->a()Lcom/google/c/b/a/d;

    move-result-object v0

    .line 140
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/google/c/a/a/b/b/a/c/f;

    invoke-direct {v0, p0, p1, p2}, Lcom/google/c/a/a/b/b/a/c/f;-><init>(Ljava/lang/String;II)V

    goto :goto_0
.end method

.method private static a(Ljava/lang/String;ILjava/lang/String;I)Lcom/google/c/b/a/c;
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 120
    invoke-virtual {p0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 121
    invoke-static {p0, p1, v4}, Lcom/google/c/a/a/b/b/b/m;->a(Ljava/lang/String;II)Lcom/google/c/b/a/c;

    move-result-object v0

    .line 126
    :goto_0
    return-object v0

    .line 123
    :cond_0
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 124
    invoke-static {p0, p1, p2, p3}, Lcom/google/c/a/a/b/b/b/m;->b(Ljava/lang/String;ILjava/lang/String;I)Lcom/google/c/b/a/c;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 125
    invoke-static {p2, p3, v4}, Lcom/google/c/a/a/b/b/b/m;->a(Ljava/lang/String;II)Lcom/google/c/b/a/c;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 126
    new-instance v0, Lcom/google/c/a/a/b/b/a/c/n;

    const-string v2, ""

    const/4 v3, 0x0

    invoke-direct {v0, v2, v1, v3, v4}, Lcom/google/c/a/a/b/b/a/c/n;-><init>(Ljava/lang/String;Ljava/util/List;ZZ)V

    goto :goto_0
.end method

.method private static b(Ljava/lang/String;ILjava/lang/String;I)Lcom/google/c/b/a/c;
    .locals 1

    .prologue
    .line 130
    const/4 v0, -0x1

    if-eq p1, v0, :cond_0

    invoke-virtual {p0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    if-ne p1, p3, :cond_1

    .line 131
    :cond_0
    invoke-static {}, Lcom/google/c/b/a/d;->a()Lcom/google/c/b/a/d;

    move-result-object v0

    .line 133
    :goto_0
    return-object v0

    :cond_1
    new-instance v0, Lcom/google/c/a/a/b/b/a/c/m;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/google/c/a/a/b/b/a/c/m;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    goto :goto_0
.end method


# virtual methods
.method public final a(Lcom/google/c/a/a/b/b/b/y;Lcom/google/c/a/a/b/b/b/y;)V
    .locals 12

    .prologue
    .line 35
    invoke-interface {p1}, Lcom/google/c/a/a/b/b/b/y;->c()Lcom/google/c/b/a/c;

    move-result-object v0

    check-cast v0, Lcom/google/c/a/a/b/b/a/c/m;

    .line 36
    invoke-interface {p2}, Lcom/google/c/a/a/b/b/b/y;->c()Lcom/google/c/b/a/c;

    move-result-object v1

    check-cast v1, Lcom/google/c/a/a/b/b/a/c/f;

    .line 37
    invoke-virtual {v0}, Lcom/google/c/a/a/b/b/a/c/m;->c()I

    move-result v6

    .line 38
    invoke-virtual {v0}, Lcom/google/c/a/a/b/b/a/c/m;->e()I

    move-result v3

    .line 39
    invoke-virtual {v1}, Lcom/google/c/a/a/b/b/a/c/f;->c()I

    move-result v5

    .line 40
    invoke-virtual {v1}, Lcom/google/c/a/a/b/b/a/c/f;->a()I

    move-result v4

    .line 41
    add-int v2, v5, v4

    add-int/lit8 v8, v2, -0x1

    .line 42
    const/4 v2, -0x1

    .line 43
    const/4 v7, 0x0

    .line 44
    invoke-virtual {v0}, Lcom/google/c/a/a/b/b/a/c/m;->a()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v1}, Lcom/google/c/a/a/b/b/a/c/f;->e()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_0

    invoke-virtual {v0}, Lcom/google/c/a/a/b/b/a/c/m;->d()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v1}, Lcom/google/c/a/a/b/b/a/c/f;->e()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_0

    if-lt v6, v5, :cond_0

    if-gt v6, v8, :cond_0

    if-lt v3, v5, :cond_0

    if-gt v3, v8, :cond_0

    .line 49
    invoke-static {}, Lcom/google/c/b/a/d;->a()Lcom/google/c/b/a/d;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/google/c/a/a/b/b/b/y;->a(Lcom/google/c/b/a/c;)V

    .line 109
    :goto_0
    return-void

    .line 52
    :cond_0
    invoke-virtual {v0}, Lcom/google/c/a/a/b/b/a/c/m;->a()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v1}, Lcom/google/c/a/a/b/b/a/c/f;->e()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_1

    .line 53
    invoke-virtual {v0}, Lcom/google/c/a/a/b/b/a/c/m;->c()I

    move-result v9

    invoke-virtual {v1}, Lcom/google/c/a/a/b/b/a/c/f;->c()I

    move-result v10

    if-ge v9, v10, :cond_2

    .line 55
    add-int/lit8 v5, v5, -0x1

    .line 74
    :cond_1
    :goto_1
    invoke-virtual {v0}, Lcom/google/c/a/a/b/b/a/c/m;->d()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v1}, Lcom/google/c/a/a/b/b/a/c/f;->e()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_8

    .line 75
    invoke-virtual {v1}, Lcom/google/c/a/a/b/b/a/c/f;->c()I

    move-result v9

    if-gt v3, v9, :cond_4

    .line 77
    add-int/lit8 v5, v5, 0x1

    move v11, v7

    move v7, v3

    move v3, v2

    move v2, v11

    .line 93
    :goto_2
    if-eqz v2, :cond_6

    .line 94
    invoke-virtual {v0}, Lcom/google/c/a/a/b/b/a/c/m;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, Lcom/google/c/a/a/b/b/a/c/m;->d()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0}, Lcom/google/c/a/a/b/b/a/c/m;->e()I

    move-result v8

    invoke-static {v2, v6, v7, v8}, Lcom/google/c/a/a/b/b/b/m;->a(Ljava/lang/String;ILjava/lang/String;I)Lcom/google/c/b/a/c;

    move-result-object v2

    .line 99
    :goto_3
    invoke-virtual {v1}, Lcom/google/c/a/a/b/b/a/c/f;->e()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, v5, v4}, Lcom/google/c/a/a/b/b/b/m;->a(Ljava/lang/String;II)Lcom/google/c/b/a/c;

    move-result-object v1

    .line 100
    if-ltz v3, :cond_7

    .line 101
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 102
    invoke-interface {v4, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 103
    invoke-virtual {v0}, Lcom/google/c/a/a/b/b/a/c/m;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Lcom/google/c/a/a/b/b/a/c/m;->a()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0}, Lcom/google/c/a/a/b/b/a/c/m;->c()I

    move-result v0

    invoke-static {v1, v3, v5, v0}, Lcom/google/c/a/a/b/b/b/m;->a(Ljava/lang/String;ILjava/lang/String;I)Lcom/google/c/b/a/c;

    move-result-object v0

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 105
    new-instance v0, Lcom/google/c/a/a/b/b/a/c/n;

    const-string v1, ""

    const/4 v3, 0x0

    const/4 v5, 0x1

    invoke-direct {v0, v1, v4, v3, v5}, Lcom/google/c/a/a/b/b/a/c/n;-><init>(Ljava/lang/String;Ljava/util/List;ZZ)V

    .line 107
    :goto_4
    invoke-interface {p1, v2}, Lcom/google/c/a/a/b/b/b/y;->a(Lcom/google/c/b/a/c;)V

    .line 108
    invoke-interface {p2, v0}, Lcom/google/c/a/a/b/b/b/y;->a(Lcom/google/c/b/a/c;)V

    goto :goto_0

    .line 56
    :cond_2
    if-lt v6, v5, :cond_3

    if-gt v6, v8, :cond_3

    .line 59
    add-int/lit8 v4, v4, -0x1

    .line 63
    invoke-virtual {v0}, Lcom/google/c/a/a/b/b/a/c/m;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, Lcom/google/c/a/a/b/b/a/c/m;->a()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v2, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_9

    if-ge v6, v3, :cond_9

    .line 64
    add-int/lit8 v2, v3, -0x1

    .line 68
    :goto_5
    const/4 v6, -0x1

    goto :goto_1

    .line 71
    :cond_3
    sub-int/2addr v6, v4

    goto :goto_1

    .line 78
    :cond_4
    if-le v3, v8, :cond_5

    .line 81
    sub-int/2addr v3, v4

    .line 82
    sub-int/2addr v2, v4

    move v11, v7

    move v7, v3

    move v3, v2

    move v2, v11

    goto :goto_2

    .line 86
    :cond_5
    const/4 v7, 0x1

    .line 89
    add-int/lit8 v4, v4, 0x1

    move v11, v7

    move v7, v3

    move v3, v2

    move v2, v11

    goto :goto_2

    .line 97
    :cond_6
    invoke-virtual {v0}, Lcom/google/c/a/a/b/b/a/c/m;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, Lcom/google/c/a/a/b/b/a/c/m;->d()Ljava/lang/String;

    move-result-object v8

    invoke-static {v2, v6, v8, v7}, Lcom/google/c/a/a/b/b/b/m;->b(Ljava/lang/String;ILjava/lang/String;I)Lcom/google/c/b/a/c;

    move-result-object v2

    goto :goto_3

    :cond_7
    move-object v0, v1

    goto :goto_4

    :cond_8
    move v11, v7

    move v7, v3

    move v3, v2

    move v2, v11

    goto/16 :goto_2

    :cond_9
    move v2, v3

    goto :goto_5
.end method
