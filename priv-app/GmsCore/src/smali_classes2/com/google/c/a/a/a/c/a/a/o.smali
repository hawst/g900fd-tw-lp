.class final Lcom/google/c/a/a/a/c/a/a/o;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final a:I

.field final b:Ljava/lang/Object;

.field final c:Ljava/lang/Object;

.field private final d:Lcom/google/c/a/a/b/d/b;


# direct methods
.method private constructor <init>(Lcom/google/c/a/a/b/d/b;ILjava/lang/Object;Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    iput-object p1, p0, Lcom/google/c/a/a/a/c/a/a/o;->d:Lcom/google/c/a/a/b/d/b;

    .line 31
    iput p2, p0, Lcom/google/c/a/a/a/c/a/a/o;->a:I

    .line 32
    iput-object p3, p0, Lcom/google/c/a/a/a/c/a/a/o;->b:Ljava/lang/Object;

    .line 33
    iput-object p4, p0, Lcom/google/c/a/a/a/c/a/a/o;->c:Ljava/lang/Object;

    .line 34
    if-nez p1, :cond_0

    .line 35
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "jsonDriver"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 37
    :cond_0
    if-nez p3, :cond_1

    .line 38
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "payload"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 40
    :cond_1
    if-nez p4, :cond_2

    .line 41
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "payloadType"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 43
    :cond_2
    if-gez p2, :cond_3

    .line 44
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "envelopeId < 0"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 46
    :cond_3
    return-void
.end method

.method public static a(Lcom/google/c/a/a/b/d/b;Ljava/lang/Object;)Lcom/google/c/a/a/a/c/a/a/o;
    .locals 7

    .prologue
    const/4 v0, 0x1

    const/4 v4, 0x0

    .line 100
    invoke-interface {p0, p1}, Lcom/google/c/a/a/b/d/b;->f(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 101
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "non-array envelope"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 103
    :cond_0
    invoke-interface {p0, p1}, Lcom/google/c/a/a/b/d/b;->e(Ljava/lang/Object;)I

    move-result v1

    const/4 v2, 0x2

    if-eq v1, v2, :cond_1

    .line 104
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "invalid envelope size"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 106
    :cond_1
    invoke-interface {p0, p1, v4}, Lcom/google/c/a/a/b/d/b;->a(Ljava/lang/Object;I)Ljava/lang/Object;

    move-result-object v1

    .line 107
    invoke-interface {p0, v1}, Lcom/google/c/a/a/b/d/b;->i(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 108
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "non-numeric envelope ID"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 110
    :cond_2
    invoke-interface {p0, v1}, Lcom/google/c/a/a/b/d/b;->b(Ljava/lang/Object;)D

    move-result-wide v2

    double-to-int v1, v2

    .line 111
    invoke-interface {p0, p1, v0}, Lcom/google/c/a/a/b/d/b;->a(Ljava/lang/Object;I)Ljava/lang/Object;

    move-result-object v2

    .line 112
    invoke-interface {p0, v2}, Lcom/google/c/a/a/b/d/b;->f(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_3

    .line 113
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "non-array envelope contents"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 115
    :cond_3
    invoke-interface {p0, v2}, Lcom/google/c/a/a/b/d/b;->e(Ljava/lang/Object;)I

    move-result v3

    if-nez v3, :cond_4

    .line 116
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "empty envelope contents"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 118
    :cond_4
    invoke-interface {p0, v2, v4}, Lcom/google/c/a/a/b/d/b;->a(Ljava/lang/Object;I)Ljava/lang/Object;

    move-result-object v3

    .line 119
    invoke-interface {p0, v3}, Lcom/google/c/a/a/b/d/b;->i(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_5

    invoke-interface {p0, v3}, Lcom/google/c/a/a/b/d/b;->h(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_5

    .line 120
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "payload type must be a number/string"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 122
    :cond_5
    invoke-interface {p0}, Lcom/google/c/a/a/b/d/b;->a()Ljava/lang/Object;

    move-result-object v4

    .line 123
    :goto_0
    invoke-interface {p0, v2}, Lcom/google/c/a/a/b/d/b;->e(Ljava/lang/Object;)I

    move-result v5

    if-ge v0, v5, :cond_6

    .line 124
    add-int/lit8 v5, v0, -0x1

    invoke-interface {p0, v2, v0}, Lcom/google/c/a/a/b/d/b;->a(Ljava/lang/Object;I)Ljava/lang/Object;

    move-result-object v6

    invoke-interface {p0, v4, v5, v6}, Lcom/google/c/a/a/b/d/b;->a(Ljava/lang/Object;ILjava/lang/Object;)V

    .line 123
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 126
    :cond_6
    new-instance v0, Lcom/google/c/a/a/a/c/a/a/o;

    invoke-direct {v0, p0, v1, v4, v3}, Lcom/google/c/a/a/a/c/a/a/o;-><init>(Lcom/google/c/a/a/b/d/b;ILjava/lang/Object;Ljava/lang/Object;)V

    return-object v0
.end method


# virtual methods
.method final a(Ljava/lang/String;)Z
    .locals 2

    .prologue
    .line 87
    iget-object v0, p0, Lcom/google/c/a/a/a/c/a/a/o;->d:Lcom/google/c/a/a/b/d/b;

    iget-object v1, p0, Lcom/google/c/a/a/a/c/a/a/o;->c:Ljava/lang/Object;

    invoke-interface {v0, v1}, Lcom/google/c/a/a/b/d/b;->h(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/c/a/a/a/c/a/a/o;->d:Lcom/google/c/a/a/b/d/b;

    iget-object v1, p0, Lcom/google/c/a/a/a/c/a/a/o;->c:Ljava/lang/Object;

    invoke-interface {v0, v1}, Lcom/google/c/a/a/b/d/b;->c(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 92
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Envelope [envelopeId="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v1, p0, Lcom/google/c/a/a/a/c/a/a/o;->a:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", payload="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/c/a/a/a/c/a/a/o;->b:Ljava/lang/Object;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", messageType="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/c/a/a/a/c/a/a/o;->c:Ljava/lang/Object;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
