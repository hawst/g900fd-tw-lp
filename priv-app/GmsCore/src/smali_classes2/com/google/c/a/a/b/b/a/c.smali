.class public final Lcom/google/c/a/a/b/b/a/c;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/c/a/a/b/b/a/r;


# static fields
.field private static final a:[Lcom/google/c/a/a/b/b/a/c;


# instance fields
.field private final b:C


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 10
    const/16 v0, 0x100

    new-array v0, v0, [Lcom/google/c/a/a/b/b/a/c;

    sput-object v0, Lcom/google/c/a/a/b/b/a/c;->a:[Lcom/google/c/a/a/b/b/a/c;

    .line 13
    const/4 v0, 0x0

    :goto_0
    sget-object v1, Lcom/google/c/a/a/b/b/a/c;->a:[Lcom/google/c/a/a/b/b/a/c;

    array-length v1, v1

    if-ge v0, v1, :cond_0

    .line 14
    sget-object v1, Lcom/google/c/a/a/b/b/a/c;->a:[Lcom/google/c/a/a/b/b/a/c;

    new-instance v2, Lcom/google/c/a/a/b/b/a/c;

    int-to-char v3, v0

    invoke-direct {v2, v3}, Lcom/google/c/a/a/b/b/a/c;-><init>(C)V

    aput-object v2, v1, v0

    .line 13
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 16
    :cond_0
    return-void
.end method

.method private constructor <init>(C)V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    iput-char p1, p0, Lcom/google/c/a/a/b/b/a/c;->b:C

    .line 22
    return-void
.end method

.method synthetic constructor <init>(CB)V
    .locals 0

    .prologue
    .line 6
    invoke-direct {p0, p1}, Lcom/google/c/a/a/b/b/a/c;-><init>(C)V

    return-void
.end method

.method public static a(C)Lcom/google/c/a/a/b/b/a/c;
    .locals 1

    .prologue
    .line 50
    sget-object v0, Lcom/google/c/a/a/b/b/a/c;->a:[Lcom/google/c/a/a/b/b/a/c;

    array-length v0, v0

    if-ge p0, v0, :cond_0

    .line 51
    sget-object v0, Lcom/google/c/a/a/b/b/a/c;->a:[Lcom/google/c/a/a/b/b/a/c;

    aget-object v0, v0, p0

    .line 53
    :goto_0
    return-object v0

    :cond_0
    invoke-static {p0}, Lcom/google/c/a/a/b/b/a/d;->a(C)Lcom/google/c/a/a/b/b/a/c;

    move-result-object v0

    goto :goto_0
.end method

.method static synthetic c()[Lcom/google/c/a/a/b/b/a/c;
    .locals 1

    .prologue
    .line 6
    sget-object v0, Lcom/google/c/a/a/b/b/a/c;->a:[Lcom/google/c/a/a/b/b/a/c;

    return-object v0
.end method


# virtual methods
.method public final a()C
    .locals 1

    .prologue
    .line 25
    iget-char v0, p0, Lcom/google/c/a/a/b/b/a/c;->b:C

    return v0
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 60
    const/16 v0, 0xa

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 46
    if-ne p1, p0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 39
    iget-char v0, p0, Lcom/google/c/a/a/b/b/a/c;->b:C

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 34
    iget-char v0, p0, Lcom/google/c/a/a/b/b/a/c;->b:C

    invoke-static {v0}, Ljava/lang/String;->valueOf(C)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
