.class public final Lcom/google/c/a/a/a/a/a;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public final a:Lcom/google/c/a/a/b/e/e;

.field public final b:Lcom/google/c/a/a/a/b/b;

.field public c:Lcom/google/c/a/a/b/a/e;

.field public d:Lcom/google/c/a/a/b/e/c;

.field public e:Lcom/google/c/a/a/b/e/i;

.field public f:Lcom/google/c/a/a/b/b/a/b;

.field public g:Z

.field private final h:Lcom/google/c/a/a/b/e/n;

.field private final i:Ljava/util/Set;

.field private final j:Z

.field private final k:Lcom/google/c/a/a/a/c/i;

.field private l:Z

.field private m:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/util/Set;Lcom/google/c/a/a/a/c/i;Lcom/google/c/a/a/b/e/e;Lcom/google/c/a/a/b/e/n;)V
    .locals 1

    .prologue
    .line 72
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 73
    iput-object p1, p0, Lcom/google/c/a/a/a/a/a;->i:Ljava/util/Set;

    .line 74
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/c/a/a/a/a/a;->j:Z

    .line 75
    iput-object p3, p0, Lcom/google/c/a/a/a/a/a;->a:Lcom/google/c/a/a/b/e/e;

    .line 76
    new-instance v0, Lcom/google/c/a/a/a/b/b;

    invoke-direct {v0, p2}, Lcom/google/c/a/a/a/b/b;-><init>(Lcom/google/c/a/a/a/c/i;)V

    iput-object v0, p0, Lcom/google/c/a/a/a/a/a;->b:Lcom/google/c/a/a/a/b/b;

    .line 77
    iput-object p4, p0, Lcom/google/c/a/a/a/a/a;->h:Lcom/google/c/a/a/b/e/n;

    .line 78
    iput-object p2, p0, Lcom/google/c/a/a/a/a/a;->k:Lcom/google/c/a/a/a/c/i;

    .line 79
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/c/a/a/b/c/f;Ljava/util/List;Ljava/util/List;)V
    .locals 8

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 162
    iget-object v0, p0, Lcom/google/c/a/a/a/a/a;->k:Lcom/google/c/a/a/a/c/i;

    invoke-virtual {p1}, Lcom/google/c/a/a/b/c/f;->g()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v3}, Lcom/google/c/a/a/a/c/i;->a(Ljava/lang/String;)V

    .line 163
    invoke-virtual {p1}, Lcom/google/c/a/a/b/c/f;->c()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/c/a/a/a/a/a;->g:Z

    .line 164
    iget-object v0, p0, Lcom/google/c/a/a/a/a/a;->b:Lcom/google/c/a/a/a/b/b;

    invoke-virtual {p1}, Lcom/google/c/a/a/b/c/f;->f()Ljava/util/Collection;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/c/a/a/a/b/b;->a(Ljava/lang/Iterable;)V

    .line 165
    invoke-virtual {p1}, Lcom/google/c/a/a/b/c/f;->a()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/c/a/a/a/a/a;->m:Ljava/lang/String;

    .line 166
    iget-boolean v0, p0, Lcom/google/c/a/a/a/a/a;->g:Z

    if-nez v0, :cond_1

    move v0, v1

    :goto_0
    invoke-virtual {p1}, Lcom/google/c/a/a/b/c/f;->e()Ljava/util/List;

    move-result-object v3

    iget-object v4, p0, Lcom/google/c/a/a/a/a/a;->b:Lcom/google/c/a/a/a/b/b;

    iget-object v4, v4, Lcom/google/c/a/a/a/b/b;->b:Lcom/google/c/a/a/b/g/d;

    new-instance v5, Lcom/google/c/a/a/b/b/a/o;

    invoke-virtual {v4}, Lcom/google/c/a/a/b/g/d;->a()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4}, Lcom/google/c/a/a/b/g/d;->d()Ljava/lang/String;

    move-result-object v4

    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    invoke-direct {v5, v6, v4, v7, v1}, Lcom/google/c/a/a/b/b/a/o;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Z)V

    new-instance v1, Lcom/google/c/a/a/b/b/a/b/w;

    new-instance v4, Lcom/google/c/a/a/b/b/a/b/u;

    invoke-direct {v4}, Lcom/google/c/a/a/b/b/a/b/u;-><init>()V

    iput-object v3, v4, Lcom/google/c/a/a/b/b/a/b/u;->a:Ljava/lang/Iterable;

    iput-boolean v0, v4, Lcom/google/c/a/a/b/b/a/b/u;->b:Z

    iput-object p2, v4, Lcom/google/c/a/a/b/b/a/b/u;->c:Ljava/util/List;

    iput-object p3, v4, Lcom/google/c/a/a/b/b/a/b/u;->d:Ljava/util/List;

    iget-object v0, p0, Lcom/google/c/a/a/a/a/a;->i:Ljava/util/Set;

    iput-object v0, v4, Lcom/google/c/a/a/b/b/a/b/u;->f:Ljava/util/Set;

    iget-boolean v0, p0, Lcom/google/c/a/a/a/a/a;->j:Z

    iput-boolean v0, v4, Lcom/google/c/a/a/b/b/a/b/u;->e:Z

    const/high16 v0, 0xa00000

    iput v0, v4, Lcom/google/c/a/a/b/b/a/b/u;->g:I

    iput-boolean v2, v4, Lcom/google/c/a/a/b/b/a/b/u;->h:Z

    invoke-virtual {v4}, Lcom/google/c/a/a/b/b/a/b/u;->a()Lcom/google/c/a/a/b/b/a/b/t;

    move-result-object v0

    invoke-direct {v1, v0, v5}, Lcom/google/c/a/a/b/b/a/b/w;-><init>(Lcom/google/c/a/a/b/b/a/b/t;Lcom/google/c/a/a/b/b/a/o;)V

    iput-object v1, p0, Lcom/google/c/a/a/a/a/a;->f:Lcom/google/c/a/a/b/b/a/b;

    iget-object v0, p0, Lcom/google/c/a/a/a/a/a;->f:Lcom/google/c/a/a/b/b/a/b;

    new-instance v1, Lcom/google/c/a/a/a/a/c;

    invoke-direct {v1, p0}, Lcom/google/c/a/a/a/a/c;-><init>(Lcom/google/c/a/a/a/a/a;)V

    invoke-interface {v0, v1}, Lcom/google/c/a/a/b/b/a/b;->a(Lcom/google/c/a/a/b/b/a/a/c;)V

    new-instance v0, Lcom/google/c/a/a/b/e/i;

    iget-object v1, p0, Lcom/google/c/a/a/a/a/a;->a:Lcom/google/c/a/a/b/e/e;

    iget-object v2, p0, Lcom/google/c/a/a/a/a/a;->h:Lcom/google/c/a/a/b/e/n;

    iget-object v3, p0, Lcom/google/c/a/a/a/a/a;->f:Lcom/google/c/a/a/b/b/a/b;

    iget-object v4, p0, Lcom/google/c/a/a/a/a/a;->d:Lcom/google/c/a/a/b/e/c;

    invoke-virtual {p1}, Lcom/google/c/a/a/b/c/f;->b()Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lcom/google/c/a/a/a/a/a;->c:Lcom/google/c/a/a/b/a/e;

    invoke-direct/range {v0 .. v6}, Lcom/google/c/a/a/b/e/i;-><init>(Lcom/google/c/a/a/b/e/e;Lcom/google/c/a/a/b/e/n;Lcom/google/c/a/a/b/b/a/b;Lcom/google/c/a/a/b/e/c;Ljava/lang/String;Lcom/google/c/a/a/b/a/e;)V

    iput-object v0, p0, Lcom/google/c/a/a/a/a/a;->e:Lcom/google/c/a/a/b/e/i;

    invoke-virtual {p1}, Lcom/google/c/a/a/b/c/f;->d()I

    move-result v0

    iget-object v1, p0, Lcom/google/c/a/a/a/a/a;->e:Lcom/google/c/a/a/b/e/i;

    invoke-virtual {v1, v0}, Lcom/google/c/a/a/b/e/i;->a(I)V

    iget-object v0, p0, Lcom/google/c/a/a/a/a/a;->c:Lcom/google/c/a/a/b/a/e;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/c/a/a/a/a/a;->c:Lcom/google/c/a/a/b/a/e;

    invoke-interface {v0, p1}, Lcom/google/c/a/a/b/a/e;->a(Lcom/google/c/a/a/b/c/f;)V

    .line 167
    :cond_0
    return-void

    :cond_1
    move v0, v2

    .line 166
    goto :goto_0
.end method

.method public final a()Z
    .locals 2

    .prologue
    .line 222
    iget-object v0, p0, Lcom/google/c/a/a/a/a/a;->e:Lcom/google/c/a/a/b/e/i;

    if-eqz v0, :cond_0

    .line 223
    iget-object v0, p0, Lcom/google/c/a/a/a/a/a;->e:Lcom/google/c/a/a/b/e/i;

    invoke-virtual {v0}, Lcom/google/c/a/a/b/e/i;->b()Z

    move-result v0

    return v0

    .line 225
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "OT manager has not been initialized"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final declared-synchronized b()V
    .locals 1

    .prologue
    .line 254
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/google/c/a/a/a/a/a;->l:Z

    if-nez v0, :cond_0

    .line 255
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/c/a/a/a/a/a;->l:Z

    .line 256
    iget-object v0, p0, Lcom/google/c/a/a/a/a/a;->a:Lcom/google/c/a/a/b/e/e;

    invoke-interface {v0}, Lcom/google/c/a/a/b/e/e;->a()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 258
    :cond_0
    monitor-exit p0

    return-void

    .line 254
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized c()V
    .locals 1

    .prologue
    .line 265
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/google/c/a/a/a/a/a;->l:Z

    if-eqz v0, :cond_0

    .line 266
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/c/a/a/a/a/a;->l:Z

    .line 267
    iget-object v0, p0, Lcom/google/c/a/a/a/a/a;->a:Lcom/google/c/a/a/b/e/e;

    invoke-interface {v0}, Lcom/google/c/a/a/b/e/e;->b()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 269
    :cond_0
    monitor-exit p0

    return-void

    .line 265
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
