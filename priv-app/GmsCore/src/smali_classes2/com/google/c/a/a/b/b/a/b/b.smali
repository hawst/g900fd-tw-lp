.class abstract Lcom/google/c/a/a/b/b/a/b/b;
.super Lcom/google/c/a/a/b/b/a/b/a;
.source "SourceFile"

# interfaces
.implements Lcom/google/c/a/a/b/b/a/f;


# instance fields
.field private final b:Ljava/util/Map;

.field private c:I


# direct methods
.method public constructor <init>(Lcom/google/c/a/a/b/b/a/b/w;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 56
    invoke-direct {p0, p1, p2}, Lcom/google/c/a/a/b/b/a/b/a;-><init>(Lcom/google/c/a/a/b/b/a/b/w;Ljava/lang/String;)V

    .line 52
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/c/a/a/b/b/a/b/b;->b:Ljava/util/Map;

    .line 53
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/c/a/a/b/b/a/b/b;->c:I

    .line 57
    return-void
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 181
    iget-object v0, p0, Lcom/google/c/a/a/b/b/a/b/b;->b:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v0

    return v0
.end method

.method public a(Lcom/google/c/a/a/b/b/a/i;)I
    .locals 4

    .prologue
    .line 226
    const/16 v0, 0x11

    .line 229
    invoke-virtual {p0}, Lcom/google/c/a/a/b/b/a/b/b;->c()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 230
    mul-int/lit8 v3, v1, 0x25

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    add-int/2addr v1, v3

    .line 231
    mul-int/lit8 v1, v1, 0x25

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/c/a/a/b/b/a/r;

    invoke-interface {p1, v0}, Lcom/google/c/a/a/b/b/a/i;->a(Lcom/google/c/a/a/b/b/a/r;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    add-int/2addr v0, v1

    move v1, v0

    .line 232
    goto :goto_0

    .line 234
    :cond_0
    return v1
.end method

.method public a(Ljava/lang/String;)Lcom/google/c/a/a/b/b/a/a;
    .locals 4

    .prologue
    .line 171
    invoke-virtual {p0, p1}, Lcom/google/c/a/a/b/b/a/b/b;->a(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 172
    invoke-virtual {p0}, Lcom/google/c/a/a/b/b/a/b/b;->j()Lcom/google/c/a/a/b/b/a/b;

    move-result-object v0

    new-instance v1, Lcom/google/c/a/a/b/b/a/c/t;

    invoke-virtual {p0}, Lcom/google/c/a/a/b/b/a/b/b;->d()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-direct {v1, v2, p1, v3}, Lcom/google/c/a/a/b/b/a/c/t;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/google/c/a/a/b/b/a/r;)V

    sget-object v2, Lcom/google/c/a/a/b/b/a/k;->a:Lcom/google/c/a/a/b/b/a/k;

    invoke-interface {v0, v1, v2}, Lcom/google/c/a/a/b/b/a/b;->a(Lcom/google/c/b/a/c;Lcom/google/c/a/a/b/b/a/k;)Lcom/google/c/a/a/b/b/a/a;

    move-result-object v0

    .line 175
    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lcom/google/c/a/a/b/b/a/a;->f()Lcom/google/c/a/a/b/b/a/a;

    move-result-object v0

    goto :goto_0
.end method

.method public a(Ljava/util/Map;)Lcom/google/c/a/a/b/b/a/a;
    .locals 6

    .prologue
    .line 161
    invoke-virtual {p0}, Lcom/google/c/a/a/b/b/a/b/b;->d()Ljava/lang/String;

    move-result-object v2

    .line 162
    new-instance v3, Lcom/google/c/a/a/b/b/a/c/o;

    invoke-direct {v3}, Lcom/google/c/a/a/b/b/a/c/o;-><init>()V

    .line 163
    invoke-interface {p1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 164
    new-instance v5, Lcom/google/c/a/a/b/b/a/c/t;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/c/a/a/b/b/a/r;

    invoke-direct {v5, v2, v1, v0}, Lcom/google/c/a/a/b/b/a/c/t;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/google/c/a/a/b/b/a/r;)V

    invoke-virtual {v3, v5}, Lcom/google/c/a/a/b/b/a/c/o;->a(Lcom/google/c/b/a/c;)Lcom/google/c/a/a/b/b/a/c/o;

    goto :goto_0

    .line 166
    :cond_0
    invoke-virtual {p0}, Lcom/google/c/a/a/b/b/a/b/b;->j()Lcom/google/c/a/a/b/b/a/b;

    move-result-object v0

    invoke-virtual {v3}, Lcom/google/c/a/a/b/b/a/c/o;->a()Lcom/google/c/a/a/b/b/a/c/n;

    move-result-object v1

    sget-object v2, Lcom/google/c/a/a/b/b/a/k;->a:Lcom/google/c/a/a/b/b/a/k;

    invoke-interface {v0, v1, v2}, Lcom/google/c/a/a/b/b/a/b;->a(Lcom/google/c/b/a/c;Lcom/google/c/a/a/b/b/a/k;)Lcom/google/c/a/a/b/b/a/a;

    move-result-object v0

    return-object v0
.end method

.method public a(Lcom/google/c/a/a/b/b/a/c/r;)Lcom/google/c/a/a/b/b/a/c/r;
    .locals 4

    .prologue
    .line 112
    instance-of v0, p1, Lcom/google/c/a/a/b/b/a/c/t;

    const-string v1, "Only update mutations can be applied to maps."

    invoke-static {v0, v1}, Lcom/google/c/a/a/b/h/c;->a(ZLjava/lang/String;)V

    .line 114
    check-cast p1, Lcom/google/c/a/a/b/b/a/c/t;

    .line 115
    invoke-virtual {p1}, Lcom/google/c/a/a/b/b/a/c/t;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/c/a/a/b/b/a/b/b;->b(Ljava/lang/Object;)Lcom/google/c/a/a/b/b/a/r;

    move-result-object v0

    .line 116
    new-instance v1, Lcom/google/c/a/a/b/b/a/c/t;

    invoke-virtual {p1}, Lcom/google/c/a/a/b/b/a/c/t;->e()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lcom/google/c/a/a/b/b/a/c/t;->a()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3, v0}, Lcom/google/c/a/a/b/b/a/c/t;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/google/c/a/a/b/b/a/r;)V

    return-object v1
.end method

.method public a(Lcom/google/c/a/a/b/b/a/c/p;Lcom/google/c/a/a/b/b/a/o;ZLcom/google/c/a/a/b/b/a/b/y;Lcom/google/c/a/a/b/b/a/k;)V
    .locals 10

    .prologue
    .line 62
    instance-of v0, p1, Lcom/google/c/a/a/b/b/a/c/t;

    const-string v1, "Only update mutations can be applied to maps."

    invoke-static {v0, v1}, Lcom/google/c/a/a/b/h/c;->a(ZLjava/lang/String;)V

    .line 64
    check-cast p1, Lcom/google/c/a/a/b/b/a/c/t;

    .line 65
    invoke-virtual {p1}, Lcom/google/c/a/a/b/b/a/c/t;->c()Lcom/google/c/a/a/b/b/a/r;

    move-result-object v8

    .line 67
    invoke-virtual {p1}, Lcom/google/c/a/a/b/b/a/c/t;->a()Ljava/lang/String;

    move-result-object v1

    .line 68
    if-nez v8, :cond_1

    .line 69
    iget-object v0, p0, Lcom/google/c/a/a/b/b/a/b/b;->b:Ljava/util/Map;

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/c/a/a/b/b/a/r;

    .line 72
    iget v2, p0, Lcom/google/c/a/a/b/b/a/b/b;->c:I

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, 0x8

    sub-int v1, v2, v1

    iput v1, p0, Lcom/google/c/a/a/b/b/a/b/b;->c:I

    .line 73
    if-eqz v0, :cond_4

    .line 74
    iget v1, p0, Lcom/google/c/a/a/b/b/a/b/b;->c:I

    invoke-interface {v0}, Lcom/google/c/a/a/b/b/a/r;->b()I

    move-result v2

    sub-int/2addr v1, v2

    iput v1, p0, Lcom/google/c/a/a/b/b/a/b/b;->c:I

    move-object v9, v0

    .line 93
    :goto_0
    invoke-static {v9, v8}, Lcom/google/c/a/a/b/h/b;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 108
    :cond_0
    :goto_1
    return-void

    .line 77
    :cond_1
    iget-object v0, p0, Lcom/google/c/a/a/b/b/a/b/b;->b:Ljava/util/Map;

    invoke-interface {v0, v1, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/c/a/a/b/b/a/r;

    .line 79
    if-nez v0, :cond_2

    .line 82
    iget v2, p0, Lcom/google/c/a/a/b/b/a/b/b;->c:I

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, 0x8

    add-int/2addr v1, v2

    iput v1, p0, Lcom/google/c/a/a/b/b/a/b/b;->c:I

    .line 85
    :cond_2
    if-eqz v0, :cond_3

    .line 87
    iget v1, p0, Lcom/google/c/a/a/b/b/a/b/b;->c:I

    invoke-interface {v0}, Lcom/google/c/a/a/b/b/a/r;->b()I

    move-result v2

    sub-int/2addr v1, v2

    iput v1, p0, Lcom/google/c/a/a/b/b/a/b/b;->c:I

    .line 91
    :cond_3
    iget v1, p0, Lcom/google/c/a/a/b/b/a/b/b;->c:I

    invoke-virtual {p1}, Lcom/google/c/a/a/b/b/a/c/t;->c()Lcom/google/c/a/a/b/b/a/r;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/c/a/a/b/b/a/r;->b()I

    move-result v2

    add-int/2addr v1, v2

    iput v1, p0, Lcom/google/c/a/a/b/b/a/b/b;->c:I

    :cond_4
    move-object v9, v0

    goto :goto_0

    .line 96
    :cond_5
    invoke-virtual {p0, v8}, Lcom/google/c/a/a/b/b/a/b/b;->a(Lcom/google/c/a/a/b/b/a/r;)V

    .line 97
    invoke-virtual {p0, v9}, Lcom/google/c/a/a/b/b/a/b/b;->b(Lcom/google/c/a/a/b/b/a/r;)Z

    move-result v0

    .line 98
    if-eqz v0, :cond_6

    .line 99
    invoke-virtual {p4}, Lcom/google/c/a/a/b/b/a/b/y;->a()V

    .line 102
    :cond_6
    if-eqz p3, :cond_0

    .line 103
    new-instance v0, Lcom/google/c/a/a/b/b/a/a/g;

    invoke-virtual {p2}, Lcom/google/c/a/a/b/b/a/o;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p2}, Lcom/google/c/a/a/b/b/a/o;->c()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p2}, Lcom/google/c/a/a/b/b/a/o;->d()Ljava/util/List;

    move-result-object v4

    invoke-virtual {p2}, Lcom/google/c/a/a/b/b/a/o;->a()Z

    move-result v5

    invoke-virtual {p1}, Lcom/google/c/a/a/b/b/a/c/t;->a()Ljava/lang/String;

    move-result-object v7

    move-object v1, p0

    move-object v6, p5

    invoke-direct/range {v0 .. v9}, Lcom/google/c/a/a/b/b/a/a/g;-><init>(Lcom/google/c/a/a/b/b/a/f;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;ZLcom/google/c/a/a/b/b/a/k;Ljava/lang/String;Lcom/google/c/a/a/b/b/a/r;Lcom/google/c/a/a/b/b/a/r;)V

    .line 106
    invoke-virtual {p4, v0}, Lcom/google/c/a/a/b/b/a/b/y;->a(Lcom/google/c/a/a/b/b/a/a/a;)V

    goto :goto_1
.end method

.method public a(Lcom/google/c/a/a/b/b/a/j;Lcom/google/c/a/a/b/b/a/g;)Z
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 200
    instance-of v0, p2, Lcom/google/c/a/a/b/b/a/f;

    if-nez v0, :cond_0

    move v0, v2

    .line 221
    :goto_0
    return v0

    .line 203
    :cond_0
    check-cast p2, Lcom/google/c/a/a/b/b/a/f;

    .line 205
    invoke-virtual {p0}, Lcom/google/c/a/a/b/b/a/b/b;->a()I

    move-result v0

    invoke-interface {p2}, Lcom/google/c/a/a/b/b/a/f;->a()I

    move-result v1

    if-eq v0, v1, :cond_1

    move v0, v2

    .line 206
    goto :goto_0

    .line 209
    :cond_1
    invoke-virtual {p0}, Lcom/google/c/a/a/b/b/a/b/b;->c()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 210
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 212
    invoke-interface {p2, v1}, Lcom/google/c/a/a/b/b/a/f;->a(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_3

    move v0, v2

    .line 213
    goto :goto_0

    .line 216
    :cond_3
    invoke-interface {p2, v1}, Lcom/google/c/a/a/b/b/a/f;->b(Ljava/lang/Object;)Lcom/google/c/a/a/b/b/a/r;

    move-result-object v1

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/c/a/a/b/b/a/r;

    invoke-virtual {p0}, Lcom/google/c/a/a/b/b/a/b/b;->j()Lcom/google/c/a/a/b/b/a/b;

    move-result-object v4

    invoke-interface {p2}, Lcom/google/c/a/a/b/b/a/f;->j()Lcom/google/c/a/a/b/b/a/b;

    move-result-object v5

    invoke-interface {p1, v1, v0, v4, v5}, Lcom/google/c/a/a/b/b/a/j;->a(Lcom/google/c/a/a/b/b/a/r;Lcom/google/c/a/a/b/b/a/r;Lcom/google/c/a/a/b/b/a/b;Lcom/google/c/a/a/b/b/a/b;)Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v2

    .line 217
    goto :goto_0

    .line 221
    :cond_4
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 135
    iget-object v0, p0, Lcom/google/c/a/a/b/b/a/b/b;->b:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public b()Lcom/google/c/a/a/b/b/a/a;
    .locals 6

    .prologue
    .line 121
    invoke-virtual {p0}, Lcom/google/c/a/a/b/b/a/b/b;->a()I

    move-result v0

    if-lez v0, :cond_1

    .line 122
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 123
    iget-object v0, p0, Lcom/google/c/a/a/b/b/a/b/b;->b:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 124
    new-instance v3, Lcom/google/c/a/a/b/b/a/c/t;

    invoke-virtual {p0}, Lcom/google/c/a/a/b/b/a/b/b;->d()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    invoke-direct {v3, v4, v0, v5}, Lcom/google/c/a/a/b/b/a/c/t;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/google/c/a/a/b/b/a/r;)V

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 126
    :cond_0
    invoke-virtual {p0}, Lcom/google/c/a/a/b/b/a/b/b;->j()Lcom/google/c/a/a/b/b/a/b;

    move-result-object v0

    new-instance v2, Lcom/google/c/a/a/b/b/a/c/o;

    invoke-direct {v2}, Lcom/google/c/a/a/b/b/a/c/o;-><init>()V

    invoke-virtual {v2, v1}, Lcom/google/c/a/a/b/b/a/c/o;->a(Ljava/util/Collection;)Lcom/google/c/a/a/b/b/a/c/o;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/c/a/a/b/b/a/c/o;->a()Lcom/google/c/a/a/b/b/a/c/n;

    move-result-object v1

    sget-object v2, Lcom/google/c/a/a/b/b/a/k;->a:Lcom/google/c/a/a/b/b/a/k;

    invoke-interface {v0, v1, v2}, Lcom/google/c/a/a/b/b/a/b;->a(Lcom/google/c/b/a/c;Lcom/google/c/a/a/b/b/a/k;)Lcom/google/c/a/a/b/b/a/a;

    move-result-object v0

    .line 129
    :goto_1
    return-object v0

    :cond_1
    invoke-static {}, Lcom/google/c/a/a/b/b/a/a;->f()Lcom/google/c/a/a/b/b/a/a;

    move-result-object v0

    goto :goto_1
.end method

.method public b(Ljava/lang/Object;)Lcom/google/c/a/a/b/b/a/r;
    .locals 1

    .prologue
    .line 145
    iget-object v0, p0, Lcom/google/c/a/a/b/b/a/b/b;->b:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/c/a/a/b/b/a/r;

    return-object v0
.end method

.method public b(Lcom/google/c/a/a/b/b/a/i;)Ljava/lang/String;
    .locals 5

    .prologue
    .line 239
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 241
    const-string v0, "{"

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 242
    const/4 v0, 0x1

    .line 243
    invoke-virtual {p0}, Lcom/google/c/a/a/b/b/a/b/b;->c()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move v1, v0

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 244
    if-nez v1, :cond_0

    .line 245
    const-string v1, ", "

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 247
    :cond_0
    const/4 v2, 0x0

    .line 249
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 250
    const-string v1, ": "

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 251
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/c/a/a/b/b/a/r;

    invoke-interface {p1, v0}, Lcom/google/c/a/a/b/b/a/i;->a(Lcom/google/c/a/a/b/b/a/r;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move v1, v2

    .line 252
    goto :goto_0

    .line 253
    :cond_1
    const-string v0, "}"

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 255
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public c()Ljava/util/Set;
    .locals 1

    .prologue
    .line 140
    iget-object v0, p0, Lcom/google/c/a/a/b/b/a/b/b;->b:Ljava/util/Map;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public i()Lcom/google/c/a/a/b/b/a/c/n;
    .locals 6

    .prologue
    .line 260
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 261
    iget-object v0, p0, Lcom/google/c/a/a/b/b/a/b/b;->b:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 262
    new-instance v3, Lcom/google/c/a/a/b/b/a/c/t;

    invoke-virtual {p0}, Lcom/google/c/a/a/b/b/a/b/b;->d()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    invoke-direct {v3, v4, v0, v5}, Lcom/google/c/a/a/b/b/a/c/t;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/google/c/a/a/b/b/a/r;)V

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 264
    :cond_0
    new-instance v0, Lcom/google/c/a/a/b/b/a/c/o;

    invoke-direct {v0}, Lcom/google/c/a/a/b/b/a/c/o;-><init>()V

    invoke-virtual {v0, v1}, Lcom/google/c/a/a/b/b/a/c/o;->a(Ljava/util/Collection;)Lcom/google/c/a/a/b/b/a/c/o;

    move-result-object v0

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/google/c/a/a/b/b/a/c/o;->d:Z

    invoke-virtual {v0}, Lcom/google/c/a/a/b/b/a/c/o;->a()Lcom/google/c/a/a/b/b/a/c/n;

    move-result-object v0

    return-object v0
.end method

.method public k()Ljava/util/List;
    .locals 6

    .prologue
    .line 191
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 192
    iget-object v0, p0, Lcom/google/c/a/a/b/b/a/b/b;->b:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 193
    new-instance v4, Lcom/google/c/a/a/b/b/a/c/t;

    invoke-virtual {p0}, Lcom/google/c/a/a/b/b/a/b/b;->d()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/c/a/a/b/b/a/r;

    invoke-direct {v4, v5, v1, v0}, Lcom/google/c/a/a/b/b/a/c/t;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/google/c/a/a/b/b/a/r;)V

    invoke-interface {v2, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 195
    :cond_0
    return-object v2
.end method

.method public l()I
    .locals 1

    .prologue
    .line 276
    iget v0, p0, Lcom/google/c/a/a/b/b/a/b/b;->c:I

    add-int/lit8 v0, v0, 0x18

    return v0
.end method

.method public n()Ljava/lang/Iterable;
    .locals 1

    .prologue
    .line 281
    iget-object v0, p0, Lcom/google/c/a/a/b/b/a/b/b;->b:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    return-object v0
.end method
