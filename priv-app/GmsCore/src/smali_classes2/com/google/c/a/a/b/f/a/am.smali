.class public final enum Lcom/google/c/a/a/b/f/a/am;
.super Ljava/lang/Enum;
.source "SourceFile"

# interfaces
.implements Lcom/google/c/a/a/b/f/a/k;


# static fields
.field public static final enum a:Lcom/google/c/a/a/b/f/a/am;

.field public static final enum b:Lcom/google/c/a/a/b/f/a/am;

.field public static final enum c:Lcom/google/c/a/a/b/f/a/am;

.field public static final enum d:Lcom/google/c/a/a/b/f/a/am;

.field public static final enum e:Lcom/google/c/a/a/b/f/a/am;

.field private static final synthetic g:[Lcom/google/c/a/a/b/f/a/am;


# instance fields
.field private f:I


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 21
    new-instance v0, Lcom/google/c/a/a/b/f/a/am;

    const-string v1, "MUTATIONS"

    invoke-direct {v0, v1, v2, v2}, Lcom/google/c/a/a/b/f/a/am;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/c/a/a/b/f/a/am;->a:Lcom/google/c/a/a/b/f/a/am;

    .line 22
    new-instance v0, Lcom/google/c/a/a/b/f/a/am;

    const-string v1, "NAME"

    invoke-direct {v0, v1, v3, v3}, Lcom/google/c/a/a/b/f/a/am;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/c/a/a/b/f/a/am;->b:Lcom/google/c/a/a/b/f/a/am;

    .line 23
    new-instance v0, Lcom/google/c/a/a/b/f/a/am;

    const-string v1, "IS_CREATION"

    invoke-direct {v0, v1, v4, v4}, Lcom/google/c/a/a/b/f/a/am;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/c/a/a/b/f/a/am;->c:Lcom/google/c/a/a/b/f/a/am;

    .line 24
    new-instance v0, Lcom/google/c/a/a/b/f/a/am;

    const-string v1, "FIRE_EVENT_LISTENERS"

    invoke-direct {v0, v1, v5, v5}, Lcom/google/c/a/a/b/f/a/am;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/c/a/a/b/f/a/am;->d:Lcom/google/c/a/a/b/f/a/am;

    .line 26
    new-instance v0, Lcom/google/c/a/a/b/f/a/am;

    const-string v1, "IS_UNDOABLE"

    invoke-direct {v0, v1, v6, v6}, Lcom/google/c/a/a/b/f/a/am;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/c/a/a/b/f/a/am;->e:Lcom/google/c/a/a/b/f/a/am;

    .line 20
    const/4 v0, 0x5

    new-array v0, v0, [Lcom/google/c/a/a/b/f/a/am;

    sget-object v1, Lcom/google/c/a/a/b/f/a/am;->a:Lcom/google/c/a/a/b/f/a/am;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/c/a/a/b/f/a/am;->b:Lcom/google/c/a/a/b/f/a/am;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/c/a/a/b/f/a/am;->c:Lcom/google/c/a/a/b/f/a/am;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/c/a/a/b/f/a/am;->d:Lcom/google/c/a/a/b/f/a/am;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/c/a/a/b/f/a/am;->e:Lcom/google/c/a/a/b/f/a/am;

    aput-object v1, v0, v6

    sput-object v0, Lcom/google/c/a/a/b/f/a/am;->g:[Lcom/google/c/a/a/b/f/a/am;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 31
    iput p3, p0, Lcom/google/c/a/a/b/f/a/am;->f:I

    .line 32
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/c/a/a/b/f/a/am;
    .locals 1

    .prologue
    .line 20
    const-class v0, Lcom/google/c/a/a/b/f/a/am;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/c/a/a/b/f/a/am;

    return-object v0
.end method

.method public static values()[Lcom/google/c/a/a/b/f/a/am;
    .locals 1

    .prologue
    .line 20
    sget-object v0, Lcom/google/c/a/a/b/f/a/am;->g:[Lcom/google/c/a/a/b/f/a/am;

    invoke-virtual {v0}, [Lcom/google/c/a/a/b/f/a/am;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/c/a/a/b/f/a/am;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 36
    iget v0, p0, Lcom/google/c/a/a/b/f/a/am;->f:I

    return v0
.end method
