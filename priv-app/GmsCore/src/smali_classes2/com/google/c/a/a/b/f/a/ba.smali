.class public final Lcom/google/c/a/a/b/f/a/ba;
.super Lcom/google/c/a/a/b/f/g;
.source "SourceFile"


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 22
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Lcom/google/c/a/a/b/f/g;-><init>(Ljava/lang/Class;Z)V

    .line 23
    return-void
.end method


# virtual methods
.method protected final synthetic a(Ljava/lang/Object;Ljava/lang/Object;Lcom/google/c/a/a/b/d/b;)V
    .locals 3

    .prologue
    .line 20
    check-cast p1, Ljava/util/List;

    const/4 v0, 0x0

    :goto_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    const/4 v2, 0x1

    invoke-static {v1, p3, v2}, Lcom/google/c/a/a/b/f/f;->a(Ljava/lang/Object;Lcom/google/c/a/a/b/d/b;Z)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {p0, p2, v0, v1, p3}, Lcom/google/c/a/a/b/f/a/ba;->a(Ljava/lang/Object;ILjava/lang/Object;Lcom/google/c/a/a/b/d/b;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 41
    instance-of v1, p1, Ljava/util/List;

    if-nez v1, :cond_1

    .line 44
    :cond_0
    :goto_0
    return v0

    :cond_1
    check-cast p1, Ljava/util/List;

    invoke-static {p1}, Lcom/google/c/a/a/b/f/a/h;->a(Ljava/util/List;)Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final synthetic b(Ljava/lang/Object;Lcom/google/c/a/a/b/d/b;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 20
    invoke-virtual {p0, p1, p2}, Lcom/google/c/a/a/b/f/a/ba;->e(Ljava/lang/Object;Lcom/google/c/a/a/b/d/b;)I

    move-result v1

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2, v1}, Ljava/util/ArrayList;-><init>(I)V

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    invoke-virtual {p0, p1, v0, p2}, Lcom/google/c/a/a/b/f/a/ba;->a(Ljava/lang/Object;ILcom/google/c/a/a/b/d/b;)Ljava/lang/Object;

    move-result-object v3

    invoke-static {v3, p2}, Lcom/google/c/a/a/b/f/f;->a(Ljava/lang/Object;Lcom/google/c/a/a/b/d/b;)Ljava/lang/Object;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method
