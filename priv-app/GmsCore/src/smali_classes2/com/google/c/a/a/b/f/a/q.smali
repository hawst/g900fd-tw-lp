.class public final enum Lcom/google/c/a/a/b/f/a/q;
.super Ljava/lang/Enum;
.source "SourceFile"

# interfaces
.implements Lcom/google/c/a/a/b/f/a/k;


# static fields
.field public static final enum a:Lcom/google/c/a/a/b/f/a/q;

.field public static final enum b:Lcom/google/c/a/a/b/f/a/q;

.field public static final enum c:Lcom/google/c/a/a/b/f/a/q;

.field private static final synthetic e:[Lcom/google/c/a/a/b/f/a/q;


# instance fields
.field private d:I


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 18
    new-instance v0, Lcom/google/c/a/a/b/f/a/q;

    const-string v1, "ID"

    invoke-direct {v0, v1, v2, v2}, Lcom/google/c/a/a/b/f/a/q;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/c/a/a/b/f/a/q;->a:Lcom/google/c/a/a/b/f/a/q;

    .line 19
    new-instance v0, Lcom/google/c/a/a/b/f/a/q;

    const-string v1, "INDEX"

    invoke-direct {v0, v1, v3, v3}, Lcom/google/c/a/a/b/f/a/q;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/c/a/a/b/f/a/q;->b:Lcom/google/c/a/a/b/f/a/q;

    .line 20
    new-instance v0, Lcom/google/c/a/a/b/f/a/q;

    const-string v1, "COUNT"

    invoke-direct {v0, v1, v4, v4}, Lcom/google/c/a/a/b/f/a/q;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/c/a/a/b/f/a/q;->c:Lcom/google/c/a/a/b/f/a/q;

    .line 17
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/google/c/a/a/b/f/a/q;

    sget-object v1, Lcom/google/c/a/a/b/f/a/q;->a:Lcom/google/c/a/a/b/f/a/q;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/c/a/a/b/f/a/q;->b:Lcom/google/c/a/a/b/f/a/q;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/c/a/a/b/f/a/q;->c:Lcom/google/c/a/a/b/f/a/q;

    aput-object v1, v0, v4

    sput-object v0, Lcom/google/c/a/a/b/f/a/q;->e:[Lcom/google/c/a/a/b/f/a/q;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 25
    iput p3, p0, Lcom/google/c/a/a/b/f/a/q;->d:I

    .line 26
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/c/a/a/b/f/a/q;
    .locals 1

    .prologue
    .line 17
    const-class v0, Lcom/google/c/a/a/b/f/a/q;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/c/a/a/b/f/a/q;

    return-object v0
.end method

.method public static values()[Lcom/google/c/a/a/b/f/a/q;
    .locals 1

    .prologue
    .line 17
    sget-object v0, Lcom/google/c/a/a/b/f/a/q;->e:[Lcom/google/c/a/a/b/f/a/q;

    invoke-virtual {v0}, [Lcom/google/c/a/a/b/f/a/q;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/c/a/a/b/f/a/q;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 30
    iget v0, p0, Lcom/google/c/a/a/b/f/a/q;->d:I

    return v0
.end method
