.class public final Lcom/google/c/a/a/a/c/a/d/e;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final a:Ljava/util/Map;

.field public final b:I


# direct methods
.method public constructor <init>(ILjava/util/Map;)V
    .locals 0

    .prologue
    .line 140
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 141
    iput p1, p0, Lcom/google/c/a/a/a/c/a/d/e;->b:I

    .line 142
    iput-object p2, p0, Lcom/google/c/a/a/a/c/a/d/e;->a:Ljava/util/Map;

    .line 143
    return-void
.end method


# virtual methods
.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 171
    if-ne p0, p1, :cond_1

    .line 191
    :cond_0
    :goto_0
    return v0

    .line 174
    :cond_1
    if-nez p1, :cond_2

    move v0, v1

    .line 175
    goto :goto_0

    .line 177
    :cond_2
    instance-of v2, p1, Lcom/google/c/a/a/a/c/a/d/e;

    if-nez v2, :cond_3

    move v0, v1

    .line 178
    goto :goto_0

    .line 180
    :cond_3
    check-cast p1, Lcom/google/c/a/a/a/c/a/d/e;

    .line 181
    iget-object v2, p0, Lcom/google/c/a/a/a/c/a/d/e;->a:Ljava/util/Map;

    if-nez v2, :cond_4

    .line 182
    iget-object v2, p1, Lcom/google/c/a/a/a/c/a/d/e;->a:Ljava/util/Map;

    if-eqz v2, :cond_5

    move v0, v1

    .line 183
    goto :goto_0

    .line 185
    :cond_4
    iget-object v2, p0, Lcom/google/c/a/a/a/c/a/d/e;->a:Ljava/util/Map;

    iget-object v3, p1, Lcom/google/c/a/a/a/c/a/d/e;->a:Ljava/util/Map;

    invoke-interface {v2, v3}, Ljava/util/Map;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_5

    move v0, v1

    .line 186
    goto :goto_0

    .line 188
    :cond_5
    iget v2, p0, Lcom/google/c/a/a/a/c/a/d/e;->b:I

    iget v3, p1, Lcom/google/c/a/a/a/c/a/d/e;->b:I

    if-eq v2, v3, :cond_0

    move v0, v1

    .line 189
    goto :goto_0
.end method

.method public final hashCode()I
    .locals 2

    .prologue
    .line 162
    iget-object v0, p0, Lcom/google/c/a/a/a/c/a/d/e;->a:Ljava/util/Map;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    add-int/lit8 v0, v0, 0x1f

    .line 165
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/google/c/a/a/a/c/a/d/e;->b:I

    add-int/2addr v0, v1

    .line 166
    return v0

    .line 162
    :cond_0
    iget-object v0, p0, Lcom/google/c/a/a/a/c/a/d/e;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->hashCode()I

    move-result v0

    goto :goto_0
.end method
