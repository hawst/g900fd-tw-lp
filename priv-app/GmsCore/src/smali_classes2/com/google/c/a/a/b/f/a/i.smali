.class public final Lcom/google/c/a/a/b/f/a/i;
.super Lcom/google/c/a/a/b/f/b;
.source "SourceFile"


# direct methods
.method public constructor <init>(Z)V
    .locals 1

    .prologue
    .line 35
    const-class v0, Lcom/google/c/a/a/b/c/d;

    invoke-direct {p0, v0, p1}, Lcom/google/c/a/a/b/f/b;-><init>(Ljava/lang/Class;Z)V

    .line 36
    return-void
.end method


# virtual methods
.method protected final synthetic a(Ljava/lang/Object;Ljava/lang/Object;Lcom/google/c/a/a/b/d/b;)V
    .locals 6

    .prologue
    .line 12
    check-cast p1, Lcom/google/c/a/a/b/c/d;

    sget-object v0, Lcom/google/c/a/a/b/f/a/j;->a:Lcom/google/c/a/a/b/f/a/j;

    invoke-virtual {p1}, Lcom/google/c/a/a/b/c/d;->d()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, p2, p3, v0, v1}, Lcom/google/c/a/a/b/f/a/i;->a(Ljava/lang/Object;Lcom/google/c/a/a/b/d/b;Lcom/google/c/a/a/b/f/a/k;Ljava/lang/Object;)V

    sget-object v0, Lcom/google/c/a/a/b/f/a/j;->b:Lcom/google/c/a/a/b/f/a/j;

    invoke-virtual {p1}, Lcom/google/c/a/a/b/c/d;->b()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {p0, p2, p3, v0, v1}, Lcom/google/c/a/a/b/f/a/i;->a(Ljava/lang/Object;Lcom/google/c/a/a/b/d/b;Lcom/google/c/a/a/b/f/a/k;Ljava/lang/Object;)V

    sget-object v0, Lcom/google/c/a/a/b/f/a/j;->c:Lcom/google/c/a/a/b/f/a/j;

    invoke-virtual {p1}, Lcom/google/c/a/a/b/c/d;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, p2, p3, v0, v1}, Lcom/google/c/a/a/b/f/a/i;->a(Ljava/lang/Object;Lcom/google/c/a/a/b/d/b;Lcom/google/c/a/a/b/f/a/k;Ljava/lang/Object;)V

    invoke-virtual {p1}, Lcom/google/c/a/a/b/c/d;->f()Ljava/lang/Long;

    move-result-object v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/google/c/a/a/b/f/a/j;->d:Lcom/google/c/a/a/b/f/a/j;

    invoke-virtual {p1}, Lcom/google/c/a/a/b/c/d;->f()Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->toHexString(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, p2, p3, v0, v1}, Lcom/google/c/a/a/b/f/a/i;->a(Ljava/lang/Object;Lcom/google/c/a/a/b/d/b;Lcom/google/c/a/a/b/f/a/k;Ljava/lang/Object;)V

    :cond_0
    sget-object v3, Lcom/google/c/a/a/b/f/a/j;->e:Lcom/google/c/a/a/b/f/a/j;

    invoke-virtual {p1}, Lcom/google/c/a/a/b/c/d;->a()Lcom/google/c/b/a/c;

    move-result-object v4

    const/4 v5, 0x1

    move-object v0, p0

    move-object v1, p2

    move-object v2, p3

    invoke-virtual/range {v0 .. v5}, Lcom/google/c/a/a/b/f/a/i;->a(Ljava/lang/Object;Lcom/google/c/a/a/b/d/b;Lcom/google/c/a/a/b/f/a/k;Ljava/lang/Object;Z)V

    invoke-virtual {p1}, Lcom/google/c/a/a/b/c/d;->e()I

    move-result v0

    invoke-virtual {p1}, Lcom/google/c/a/a/b/c/d;->d()I

    move-result v1

    if-eq v0, v1, :cond_1

    sget-object v0, Lcom/google/c/a/a/b/f/a/j;->f:Lcom/google/c/a/a/b/f/a/j;

    invoke-virtual {p1}, Lcom/google/c/a/a/b/c/d;->e()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, p2, p3, v0, v1}, Lcom/google/c/a/a/b/f/a/i;->a(Ljava/lang/Object;Lcom/google/c/a/a/b/d/b;Lcom/google/c/a/a/b/f/a/k;Ljava/lang/Object;)V

    :cond_1
    invoke-virtual {p1}, Lcom/google/c/a/a/b/c/d;->g()Ljava/lang/Long;

    move-result-object v0

    if-eqz v0, :cond_2

    sget-object v0, Lcom/google/c/a/a/b/f/a/j;->g:Lcom/google/c/a/a/b/f/a/j;

    invoke-virtual {p1}, Lcom/google/c/a/a/b/c/d;->g()Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {p0, p2, p3, v0, v1}, Lcom/google/c/a/a/b/f/a/i;->a(Ljava/lang/Object;Lcom/google/c/a/a/b/d/b;Lcom/google/c/a/a/b/f/a/k;Ljava/lang/Object;)V

    :cond_2
    return-void
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 78
    instance-of v0, p1, Lcom/google/c/a/a/b/c/d;

    return v0
.end method

.method public final synthetic b(Ljava/lang/Object;Lcom/google/c/a/a/b/d/b;)Ljava/lang/Object;
    .locals 9

    .prologue
    .line 12
    sget-object v0, Lcom/google/c/a/a/b/f/a/j;->a:Lcom/google/c/a/a/b/f/a/j;

    const-class v1, Ljava/lang/Integer;

    invoke-virtual {p0, p1, p2, v0, v1}, Lcom/google/c/a/a/b/f/a/i;->a(Ljava/lang/Object;Lcom/google/c/a/a/b/d/b;Lcom/google/c/a/a/b/f/a/k;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Ljava/lang/Integer;

    sget-object v0, Lcom/google/c/a/a/b/f/a/j;->b:Lcom/google/c/a/a/b/f/a/j;

    const-class v1, Ljava/lang/Long;

    invoke-virtual {p0, p1, p2, v0, v1}, Lcom/google/c/a/a/b/f/a/i;->a(Ljava/lang/Object;Lcom/google/c/a/a/b/d/b;Lcom/google/c/a/a/b/f/a/k;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Ljava/lang/Long;

    sget-object v0, Lcom/google/c/a/a/b/f/a/j;->c:Lcom/google/c/a/a/b/f/a/j;

    const-class v1, Ljava/lang/String;

    invoke-virtual {p0, p1, p2, v0, v1}, Lcom/google/c/a/a/b/f/a/i;->a(Ljava/lang/Object;Lcom/google/c/a/a/b/d/b;Lcom/google/c/a/a/b/f/a/k;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    sget-object v0, Lcom/google/c/a/a/b/f/a/j;->d:Lcom/google/c/a/a/b/f/a/j;

    const-class v1, Ljava/lang/Long;

    invoke-virtual {p0, p1, p2, v0, v1}, Lcom/google/c/a/a/b/f/a/i;->a(Ljava/lang/Object;Lcom/google/c/a/a/b/d/b;Lcom/google/c/a/a/b/f/a/k;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Long;

    sget-object v0, Lcom/google/c/a/a/b/f/a/j;->e:Lcom/google/c/a/a/b/f/a/j;

    invoke-virtual {p0, p1, p2, v0}, Lcom/google/c/a/a/b/f/a/i;->a(Ljava/lang/Object;Lcom/google/c/a/a/b/d/b;Lcom/google/c/a/a/b/f/a/k;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/c/b/a/c;

    sget-object v0, Lcom/google/c/a/a/b/f/a/j;->f:Lcom/google/c/a/a/b/f/a/j;

    const-class v3, Ljava/lang/Integer;

    invoke-virtual {p0, p1, p2, v0, v3}, Lcom/google/c/a/a/b/f/a/i;->a(Ljava/lang/Object;Lcom/google/c/a/a/b/d/b;Lcom/google/c/a/a/b/f/a/k;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    if-nez v0, :cond_0

    move-object v7, v5

    :goto_0
    sget-object v0, Lcom/google/c/a/a/b/f/a/j;->g:Lcom/google/c/a/a/b/f/a/j;

    const-class v3, Ljava/lang/Long;

    invoke-virtual {p0, p1, p2, v0, v3}, Lcom/google/c/a/a/b/f/a/i;->a(Ljava/lang/Object;Lcom/google/c/a/a/b/d/b;Lcom/google/c/a/a/b/f/a/k;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/Long;

    new-instance v0, Lcom/google/c/a/a/b/c/d;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v7

    invoke-direct/range {v0 .. v8}, Lcom/google/c/a/a/b/c/d;-><init>(Lcom/google/c/b/a/c;JLjava/lang/String;ILjava/lang/Long;ILjava/lang/Long;)V

    return-object v0

    :cond_0
    move-object v7, v0

    goto :goto_0
.end method
