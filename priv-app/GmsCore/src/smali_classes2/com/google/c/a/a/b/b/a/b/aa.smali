.class final Lcom/google/c/a/a/b/b/a/b/aa;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/c/a/a/b/b/a/b/w;

.field private final b:Z

.field private final c:Ljava/util/Set;


# direct methods
.method public constructor <init>(Lcom/google/c/a/a/b/b/a/b/w;ZLjava/util/Set;)V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    iput-object p1, p0, Lcom/google/c/a/a/b/b/a/b/aa;->a:Lcom/google/c/a/a/b/b/a/b/w;

    .line 27
    iput-boolean p2, p0, Lcom/google/c/a/a/b/b/a/b/aa;->b:Z

    .line 28
    iput-object p3, p0, Lcom/google/c/a/a/b/b/a/b/aa;->c:Ljava/util/Set;

    .line 29
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/c/a/a/b/b/a/b/a;
    .locals 5

    .prologue
    .line 38
    invoke-static {}, Lcom/google/c/a/a/b/b/a/b/c;->values()[Lcom/google/c/a/a/b/b/a/b/c;

    move-result-object v1

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    .line 39
    invoke-virtual {v3}, Lcom/google/c/a/a/b/b/a/b/c;->a()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 40
    iget-object v0, p0, Lcom/google/c/a/a/b/b/a/b/aa;->a:Lcom/google/c/a/a/b/b/a/b/w;

    invoke-virtual {v3, v0, p2}, Lcom/google/c/a/a/b/b/a/b/c;->a(Lcom/google/c/a/a/b/b/a/b/w;Ljava/lang/String;)Lcom/google/c/a/a/b/b/a/b/a;

    move-result-object v0

    .line 49
    :goto_1
    return-object v0

    .line 38
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 44
    :cond_1
    iget-boolean v0, p0, Lcom/google/c/a/a/b/b/a/b/aa;->b:Z

    if-nez v0, :cond_2

    .line 45
    iget-object v0, p0, Lcom/google/c/a/a/b/b/a/b/aa;->c:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Cannnot create collaborative object with unregistered type: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/c/a/a/b/h/c;->a(ZLjava/lang/String;)V

    .line 48
    :cond_2
    new-instance v0, Lcom/google/c/a/a/b/b/a/b/ac;

    iget-object v1, p0, Lcom/google/c/a/a/b/b/a/b/aa;->a:Lcom/google/c/a/a/b/b/a/b/w;

    invoke-direct {v0, p1, v1, p2}, Lcom/google/c/a/a/b/b/a/b/ac;-><init>(Ljava/lang/String;Lcom/google/c/a/a/b/b/a/b/w;Ljava/lang/String;)V

    goto :goto_1
.end method
