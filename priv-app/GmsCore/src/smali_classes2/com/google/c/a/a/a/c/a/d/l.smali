.class public final Lcom/google/c/a/a/a/c/a/d/l;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field a:I

.field b:Ljava/util/Map;

.field final c:Ljava/lang/StringBuilder;

.field public d:Lcom/google/c/a/a/a/c/a/d/n;

.field e:Ljava/lang/String;

.field private f:Z

.field private final g:Lcom/google/c/a/a/a/c/a/d/b;


# direct methods
.method public constructor <init>(Lcom/google/c/a/a/a/c/a/d/b;)V
    .locals 2

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iput-object v0, p0, Lcom/google/c/a/a/a/c/a/d/l;->c:Ljava/lang/StringBuilder;

    .line 26
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/c/a/a/a/c/a/d/l;->e:Ljava/lang/String;

    .line 32
    invoke-interface {p1}, Lcom/google/c/a/a/a/c/a/d/b;->c()Lcom/google/c/a/a/a/c/a/d/c;

    move-result-object v0

    sget-object v1, Lcom/google/c/a/a/a/c/a/d/c;->c:Lcom/google/c/a/a/a/c/a/d/c;

    if-eq v0, v1, :cond_0

    .line 33
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    .line 35
    :cond_0
    iput-object p1, p0, Lcom/google/c/a/a/a/c/a/d/l;->g:Lcom/google/c/a/a/a/c/a/d/b;

    .line 36
    new-instance v0, Lcom/google/c/a/a/a/c/a/d/m;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/c/a/a/a/c/a/d/m;-><init>(Lcom/google/c/a/a/a/c/a/d/l;B)V

    .line 37
    invoke-interface {p1, v0}, Lcom/google/c/a/a/a/c/a/d/b;->a(Lcom/google/c/a/a/a/c/a/d/f;)V

    .line 38
    invoke-interface {p1, v0}, Lcom/google/c/a/a/a/c/a/d/b;->a(Lcom/google/c/a/a/a/c/a/d/g;)V

    .line 39
    invoke-interface {p1, v0}, Lcom/google/c/a/a/a/c/a/d/b;->a(Lcom/google/c/a/a/a/c/a/d/i;)V

    .line 40
    return-void
.end method


# virtual methods
.method public final declared-synchronized a()V
    .locals 2

    .prologue
    .line 51
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/google/c/a/a/a/c/a/d/l;->f:Z

    if-eqz v0, :cond_0

    .line 52
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Request has already been sent."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 51
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 54
    :cond_0
    const/4 v0, 0x1

    :try_start_1
    iput-boolean v0, p0, Lcom/google/c/a/a/a/c/a/d/l;->f:Z

    .line 55
    iget-object v0, p0, Lcom/google/c/a/a/a/c/a/d/l;->g:Lcom/google/c/a/a/a/c/a/d/b;

    invoke-interface {v0}, Lcom/google/c/a/a/a/c/a/d/b;->a()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 56
    monitor-exit p0

    return-void
.end method
