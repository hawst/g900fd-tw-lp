.class final Lcom/google/c/a/a/b/b/a/b/ab;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final a:Ljava/util/Map;

.field final b:Ljava/util/Map;

.field final c:Ljava/util/Map;

.field d:Ljava/util/Set;

.field final e:Lcom/google/c/a/a/b/b/a/b/aa;

.field f:Z

.field g:I

.field h:I

.field private i:Lcom/google/c/a/a/b/b/a/f;


# direct methods
.method public constructor <init>(Lcom/google/c/a/a/b/b/a/b/aa;)V
    .locals 2

    .prologue
    .line 52
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/c/a/a/b/b/a/b/ab;->a:Ljava/util/Map;

    .line 29
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/c/a/a/b/b/a/b/ab;->b:Ljava/util/Map;

    .line 30
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/c/a/a/b/b/a/b/ab;->c:Ljava/util/Map;

    .line 32
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/c/a/a/b/b/a/b/ab;->d:Ljava/util/Set;

    .line 38
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/c/a/a/b/b/a/b/ab;->f:Z

    .line 53
    iput-object p1, p0, Lcom/google/c/a/a/b/b/a/b/ab;->e:Lcom/google/c/a/a/b/b/a/b/aa;

    .line 54
    const/16 v0, 0x18

    iput v0, p0, Lcom/google/c/a/a/b/b/a/b/ab;->h:I

    .line 55
    iget-object v0, p0, Lcom/google/c/a/a/b/b/a/b/ab;->d:Ljava/util/Set;

    const-string v1, "root"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 56
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Lcom/google/c/a/a/b/b/a/b/a;
    .locals 2

    .prologue
    .line 63
    iget-object v0, p0, Lcom/google/c/a/a/b/b/a/b/ab;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/c/a/a/b/b/a/b/a;

    .line 64
    if-nez v0, :cond_0

    .line 65
    iget-object v0, p0, Lcom/google/c/a/a/b/b/a/b/ab;->b:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/c/a/a/b/b/a/b/a;

    .line 66
    if-eqz v0, :cond_0

    .line 67
    iget-object v1, p0, Lcom/google/c/a/a/b/b/a/b/ab;->a:Ljava/util/Map;

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 70
    :cond_0
    return-object v0
.end method

.method final a()V
    .locals 6

    .prologue
    .line 140
    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    .line 141
    invoke-virtual {p0}, Lcom/google/c/a/a/b/b/a/b/ab;->b()Lcom/google/c/a/a/b/b/a/f;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/c/a/a/b/b/a/f;->l()I

    move-result v0

    .line 142
    new-instance v3, Ljava/util/LinkedList;

    invoke-direct {v3}, Ljava/util/LinkedList;-><init>()V

    .line 143
    const-string v1, "root"

    invoke-interface {v3, v1}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    .line 144
    const-string v1, "root"

    invoke-interface {v2, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    move v1, v0

    .line 146
    :cond_0
    invoke-interface {v3}, Ljava/util/Queue;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    .line 147
    invoke-interface {v3}, Ljava/util/Queue;->poll()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 148
    invoke-virtual {p0, v0}, Lcom/google/c/a/a/b/b/a/b/ab;->a(Ljava/lang/String;)Lcom/google/c/a/a/b/b/a/b/a;

    move-result-object v0

    .line 149
    invoke-interface {v0}, Lcom/google/c/a/a/b/b/a/g;->h()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_1
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 150
    invoke-interface {v2, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_1

    .line 151
    invoke-virtual {p0, v0}, Lcom/google/c/a/a/b/b/a/b/ab;->a(Ljava/lang/String;)Lcom/google/c/a/a/b/b/a/b/a;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/c/a/a/b/b/a/b/a;->l()I

    move-result v5

    add-int/2addr v1, v5

    .line 152
    invoke-interface {v2, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 153
    invoke-interface {v3, v0}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 160
    :cond_2
    iput-object v2, p0, Lcom/google/c/a/a/b/b/a/b/ab;->d:Ljava/util/Set;

    .line 161
    iput v1, p0, Lcom/google/c/a/a/b/b/a/b/ab;->h:I

    .line 162
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/c/a/a/b/b/a/b/ab;->f:Z

    .line 163
    return-void
.end method

.method final a(Lcom/google/c/a/a/b/b/a/c/p;)V
    .locals 5

    .prologue
    .line 111
    new-instance v1, Ljava/util/LinkedList;

    invoke-interface {p1}, Lcom/google/c/a/a/b/b/a/c/p;->b()Ljava/util/Set;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/util/LinkedList;-><init>(Ljava/util/Collection;)V

    .line 112
    invoke-interface {v1}, Ljava/util/Queue;->poll()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    :goto_0
    if-eqz v0, :cond_1

    .line 113
    iget-object v2, p0, Lcom/google/c/a/a/b/b/a/b/ab;->d:Ljava/util/Set;

    invoke-interface {v2, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 114
    invoke-virtual {p0, v0}, Lcom/google/c/a/a/b/b/a/b/ab;->a(Ljava/lang/String;)Lcom/google/c/a/a/b/b/a/b/a;

    move-result-object v2

    .line 115
    iget v3, p0, Lcom/google/c/a/a/b/b/a/b/ab;->h:I

    invoke-interface {v2}, Lcom/google/c/a/a/b/b/a/g;->l()I

    move-result v4

    add-int/2addr v3, v4

    iput v3, p0, Lcom/google/c/a/a/b/b/a/b/ab;->h:I

    .line 116
    iget-object v3, p0, Lcom/google/c/a/a/b/b/a/b/ab;->d:Ljava/util/Set;

    invoke-interface {v3, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 117
    invoke-interface {v2}, Lcom/google/c/a/a/b/b/a/g;->h()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Queue;->addAll(Ljava/util/Collection;)Z

    .line 112
    :cond_0
    invoke-interface {v1}, Ljava/util/Queue;->poll()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    goto :goto_0

    .line 120
    :cond_1
    return-void
.end method

.method public final b()Lcom/google/c/a/a/b/b/a/f;
    .locals 1

    .prologue
    .line 199
    iget-object v0, p0, Lcom/google/c/a/a/b/b/a/b/ab;->i:Lcom/google/c/a/a/b/b/a/f;

    if-eqz v0, :cond_0

    .line 200
    iget-object v0, p0, Lcom/google/c/a/a/b/b/a/b/ab;->i:Lcom/google/c/a/a/b/b/a/f;

    .line 203
    :goto_0
    return-object v0

    .line 202
    :cond_0
    const-string v0, "root"

    invoke-virtual {p0, v0}, Lcom/google/c/a/a/b/b/a/b/ab;->a(Ljava/lang/String;)Lcom/google/c/a/a/b/b/a/b/a;

    move-result-object v0

    check-cast v0, Lcom/google/c/a/a/b/b/a/f;

    iput-object v0, p0, Lcom/google/c/a/a/b/b/a/b/ab;->i:Lcom/google/c/a/a/b/b/a/f;

    .line 203
    iget-object v0, p0, Lcom/google/c/a/a/b/b/a/b/ab;->i:Lcom/google/c/a/a/b/b/a/f;

    goto :goto_0
.end method

.method public final b(Ljava/lang/String;)Ljava/util/Map;
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Lcom/google/c/a/a/b/b/a/b/ab;->c:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    .line 80
    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Ljava/util/Collections;->emptyMap()Ljava/util/Map;

    move-result-object v0

    goto :goto_0
.end method
