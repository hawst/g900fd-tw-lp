.class public final enum Lcom/google/c/a/a/b/f/a/bc;
.super Ljava/lang/Enum;
.source "SourceFile"

# interfaces
.implements Lcom/google/c/a/a/b/f/a/k;


# static fields
.field public static final enum a:Lcom/google/c/a/a/b/f/a/bc;

.field public static final enum b:Lcom/google/c/a/a/b/f/a/bc;

.field public static final enum c:Lcom/google/c/a/a/b/f/a/bc;

.field private static final synthetic e:[Lcom/google/c/a/a/b/f/a/bc;


# instance fields
.field private d:I


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 19
    new-instance v0, Lcom/google/c/a/a/b/f/a/bc;

    const-string v1, "ID"

    invoke-direct {v0, v1, v2, v2}, Lcom/google/c/a/a/b/f/a/bc;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/c/a/a/b/f/a/bc;->a:Lcom/google/c/a/a/b/f/a/bc;

    .line 20
    new-instance v0, Lcom/google/c/a/a/b/f/a/bc;

    const-string v1, "PROPERTY"

    invoke-direct {v0, v1, v3, v3}, Lcom/google/c/a/a/b/f/a/bc;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/c/a/a/b/f/a/bc;->b:Lcom/google/c/a/a/b/f/a/bc;

    .line 21
    new-instance v0, Lcom/google/c/a/a/b/f/a/bc;

    const-string v1, "VALUE"

    invoke-direct {v0, v1, v4, v4}, Lcom/google/c/a/a/b/f/a/bc;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/c/a/a/b/f/a/bc;->c:Lcom/google/c/a/a/b/f/a/bc;

    .line 18
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/google/c/a/a/b/f/a/bc;

    sget-object v1, Lcom/google/c/a/a/b/f/a/bc;->a:Lcom/google/c/a/a/b/f/a/bc;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/c/a/a/b/f/a/bc;->b:Lcom/google/c/a/a/b/f/a/bc;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/c/a/a/b/f/a/bc;->c:Lcom/google/c/a/a/b/f/a/bc;

    aput-object v1, v0, v4

    sput-object v0, Lcom/google/c/a/a/b/f/a/bc;->e:[Lcom/google/c/a/a/b/f/a/bc;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 26
    iput p3, p0, Lcom/google/c/a/a/b/f/a/bc;->d:I

    .line 27
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/c/a/a/b/f/a/bc;
    .locals 1

    .prologue
    .line 18
    const-class v0, Lcom/google/c/a/a/b/f/a/bc;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/c/a/a/b/f/a/bc;

    return-object v0
.end method

.method public static values()[Lcom/google/c/a/a/b/f/a/bc;
    .locals 1

    .prologue
    .line 18
    sget-object v0, Lcom/google/c/a/a/b/f/a/bc;->e:[Lcom/google/c/a/a/b/f/a/bc;

    invoke-virtual {v0}, [Lcom/google/c/a/a/b/f/a/bc;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/c/a/a/b/f/a/bc;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 31
    iget v0, p0, Lcom/google/c/a/a/b/f/a/bc;->d:I

    return v0
.end method
