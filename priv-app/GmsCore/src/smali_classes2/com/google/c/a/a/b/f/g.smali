.class public abstract Lcom/google/c/a/a/b/f/g;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/c/a/a/b/f/a;


# instance fields
.field protected final a:Z

.field protected final b:Ljava/lang/Class;


# direct methods
.method public constructor <init>(Ljava/lang/Class;Z)V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    iput-object p1, p0, Lcom/google/c/a/a/b/f/g;->b:Ljava/lang/Class;

    .line 29
    iput-boolean p2, p0, Lcom/google/c/a/a/b/f/g;->a:Z

    .line 30
    return-void
.end method

.method private a(I)I
    .locals 1

    .prologue
    .line 130
    iget-boolean v0, p0, Lcom/google/c/a/a/b/f/g;->a:Z

    if-eqz v0, :cond_0

    .line 131
    add-int/lit8 p1, p1, 0x1

    .line 133
    :cond_0
    return p1
.end method


# virtual methods
.method public final a()Ljava/lang/Class;
    .locals 1

    .prologue
    .line 77
    iget-object v0, p0, Lcom/google/c/a/a/b/f/g;->b:Ljava/lang/Class;

    return-object v0
.end method

.method protected final a(Ljava/lang/Object;ILcom/google/c/a/a/b/d/b;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 101
    invoke-direct {p0, p2}, Lcom/google/c/a/a/b/f/g;->a(I)I

    move-result v0

    .line 102
    invoke-interface {p3, p1}, Lcom/google/c/a/a/b/d/b;->e(Ljava/lang/Object;)I

    move-result v1

    if-lt v0, v1, :cond_0

    .line 103
    const/4 v0, 0x0

    .line 105
    :goto_0
    return-object v0

    :cond_0
    invoke-interface {p3, p1, v0}, Lcom/google/c/a/a/b/d/b;->a(Ljava/lang/Object;I)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(Ljava/lang/Object;Lcom/google/c/a/a/b/d/b;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 62
    invoke-interface {p2, p1}, Lcom/google/c/a/a/b/d/b;->g(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 63
    const/4 v0, 0x0

    .line 72
    :goto_0
    return-object v0

    .line 65
    :cond_0
    iget-boolean v0, p0, Lcom/google/c/a/a/b/f/g;->a:Z

    if-nez v0, :cond_1

    invoke-interface {p2, p1}, Lcom/google/c/a/a/b/d/b;->f(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 68
    invoke-interface {p2}, Lcom/google/c/a/a/b/d/b;->a()Ljava/lang/Object;

    move-result-object v0

    .line 69
    const/4 v1, 0x0

    invoke-interface {p2, v0, v1, p1}, Lcom/google/c/a/a/b/d/b;->a(Ljava/lang/Object;ILjava/lang/Object;)V

    .line 70
    invoke-virtual {p0, v0, p2}, Lcom/google/c/a/a/b/f/g;->b(Ljava/lang/Object;Lcom/google/c/a/a/b/d/b;)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0

    .line 72
    :cond_1
    invoke-virtual {p0, p1, p2}, Lcom/google/c/a/a/b/f/g;->b(Ljava/lang/Object;Lcom/google/c/a/a/b/d/b;)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(Ljava/lang/Object;Lcom/google/c/a/a/b/d/b;Lcom/google/c/a/a/b/f/e;)Ljava/lang/Object;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 42
    if-nez p1, :cond_1

    .line 43
    const/4 v0, 0x0

    .line 58
    :cond_0
    :goto_0
    return-object v0

    .line 45
    :cond_1
    invoke-interface {p2}, Lcom/google/c/a/a/b/d/b;->a()Ljava/lang/Object;

    move-result-object v1

    .line 46
    iget-boolean v0, p0, Lcom/google/c/a/a/b/f/g;->a:Z

    if-eqz v0, :cond_2

    .line 47
    invoke-virtual {p3}, Lcom/google/c/a/a/b/f/e;->c()I

    move-result v0

    int-to-double v2, v0

    invoke-interface {p2, v2, v3}, Lcom/google/c/a/a/b/d/b;->a(D)Ljava/lang/Object;

    move-result-object v0

    invoke-interface {p2, v1, v4, v0}, Lcom/google/c/a/a/b/d/b;->a(Ljava/lang/Object;ILjava/lang/Object;)V

    .line 49
    :cond_2
    invoke-virtual {p0, p1, v1, p2}, Lcom/google/c/a/a/b/f/g;->a(Ljava/lang/Object;Ljava/lang/Object;Lcom/google/c/a/a/b/d/b;)V

    .line 50
    iget-boolean v0, p0, Lcom/google/c/a/a/b/f/g;->a:Z

    if-nez v0, :cond_3

    invoke-interface {p2, v1}, Lcom/google/c/a/a/b/d/b;->e(Ljava/lang/Object;)I

    move-result v0

    const/4 v2, 0x1

    if-ne v0, v2, :cond_3

    .line 53
    invoke-interface {p2, v1, v4}, Lcom/google/c/a/a/b/d/b;->a(Ljava/lang/Object;I)Ljava/lang/Object;

    move-result-object v0

    .line 54
    invoke-interface {p2, v0}, Lcom/google/c/a/a/b/d/b;->f(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    :cond_3
    move-object v0, v1

    .line 58
    goto :goto_0
.end method

.method protected final a(Ljava/lang/Object;ILjava/lang/Object;Lcom/google/c/a/a/b/d/b;)V
    .locals 1

    .prologue
    .line 120
    invoke-direct {p0, p2}, Lcom/google/c/a/a/b/f/g;->a(I)I

    move-result v0

    invoke-interface {p4, p1, v0, p3}, Lcom/google/c/a/a/b/d/b;->a(Ljava/lang/Object;ILjava/lang/Object;)V

    .line 121
    return-void
.end method

.method protected abstract a(Ljava/lang/Object;Ljava/lang/Object;Lcom/google/c/a/a/b/d/b;)V
.end method

.method protected abstract b(Ljava/lang/Object;Lcom/google/c/a/a/b/d/b;)Ljava/lang/Object;
.end method

.method protected final e(Ljava/lang/Object;Lcom/google/c/a/a/b/d/b;)I
    .locals 2

    .prologue
    .line 142
    invoke-interface {p2, p1}, Lcom/google/c/a/a/b/d/b;->e(Ljava/lang/Object;)I

    move-result v0

    .line 143
    iget-boolean v1, p0, Lcom/google/c/a/a/b/f/g;->a:Z

    if-eqz v1, :cond_0

    .line 144
    add-int/lit8 v0, v0, -0x1

    .line 146
    :cond_0
    return v0
.end method
