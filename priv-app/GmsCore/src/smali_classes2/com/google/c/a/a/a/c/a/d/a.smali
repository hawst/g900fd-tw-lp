.class public abstract Lcom/google/c/a/a/a/c/a/d/a;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/c/a/a/a/c/a/d/b;


# instance fields
.field protected a:Lcom/google/c/a/a/a/c/a/d/c;

.field b:Lcom/google/c/a/a/a/c/a/d/g;

.field c:Lcom/google/c/a/a/a/c/a/d/h;

.field private d:Lcom/google/c/a/a/a/c/a/d/f;

.field private e:Lcom/google/c/a/a/a/c/a/d/i;

.field private f:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 8
    sget-object v0, Lcom/google/c/a/a/a/c/a/d/c;->c:Lcom/google/c/a/a/a/c/a/d/c;

    iput-object v0, p0, Lcom/google/c/a/a/a/c/a/d/a;->a:Lcom/google/c/a/a/a/c/a/d/c;

    return-void
.end method


# virtual methods
.method public final declared-synchronized a()V
    .locals 2

    .prologue
    .line 18
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/c/a/a/a/c/a/d/a;->a:Lcom/google/c/a/a/a/c/a/d/c;

    sget-object v1, Lcom/google/c/a/a/a/c/a/d/c;->c:Lcom/google/c/a/a/a/c/a/d/c;

    if-eq v0, v1, :cond_0

    .line 19
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Cannot open if connection is not in state NEW."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 18
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 21
    :cond_0
    :try_start_1
    sget-object v0, Lcom/google/c/a/a/a/c/a/d/c;->b:Lcom/google/c/a/a/a/c/a/d/c;

    iput-object v0, p0, Lcom/google/c/a/a/a/c/a/d/a;->a:Lcom/google/c/a/a/a/c/a/d/c;

    .line 22
    invoke-virtual {p0}, Lcom/google/c/a/a/a/c/a/d/a;->d()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 23
    monitor-exit p0

    return-void
.end method

.method protected final declared-synchronized a(Lcom/google/c/a/a/a/c/a/d/e;)V
    .locals 1

    .prologue
    .line 103
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/google/c/a/a/a/c/a/d/a;->f:Z

    if-nez v0, :cond_0

    .line 104
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/c/a/a/a/c/a/d/a;->f:Z

    .line 105
    iget-object v0, p0, Lcom/google/c/a/a/a/c/a/d/a;->e:Lcom/google/c/a/a/a/c/a/d/i;

    if-eqz v0, :cond_0

    .line 106
    iget-object v0, p0, Lcom/google/c/a/a/a/c/a/d/a;->e:Lcom/google/c/a/a/a/c/a/d/i;

    invoke-interface {v0, p1}, Lcom/google/c/a/a/a/c/a/d/i;->a(Lcom/google/c/a/a/a/c/a/d/e;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 109
    :cond_0
    monitor-exit p0

    return-void

    .line 103
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final a(Lcom/google/c/a/a/a/c/a/d/f;)V
    .locals 0

    .prologue
    .line 44
    iput-object p1, p0, Lcom/google/c/a/a/a/c/a/d/a;->d:Lcom/google/c/a/a/a/c/a/d/f;

    .line 45
    return-void
.end method

.method public final a(Lcom/google/c/a/a/a/c/a/d/g;)V
    .locals 0

    .prologue
    .line 49
    iput-object p1, p0, Lcom/google/c/a/a/a/c/a/d/a;->b:Lcom/google/c/a/a/a/c/a/d/g;

    .line 50
    return-void
.end method

.method public final a(Lcom/google/c/a/a/a/c/a/d/h;)V
    .locals 0

    .prologue
    .line 54
    iput-object p1, p0, Lcom/google/c/a/a/a/c/a/d/a;->c:Lcom/google/c/a/a/a/c/a/d/h;

    .line 55
    return-void
.end method

.method public final a(Lcom/google/c/a/a/a/c/a/d/i;)V
    .locals 0

    .prologue
    .line 59
    iput-object p1, p0, Lcom/google/c/a/a/a/c/a/d/a;->e:Lcom/google/c/a/a/a/c/a/d/i;

    .line 60
    return-void
.end method

.method public final declared-synchronized b()V
    .locals 2

    .prologue
    .line 27
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/c/a/a/a/c/a/d/a;->a:Lcom/google/c/a/a/a/c/a/d/c;

    sget-object v1, Lcom/google/c/a/a/a/c/a/d/c;->a:Lcom/google/c/a/a/a/c/a/d/c;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-ne v0, v1, :cond_1

    .line 35
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 30
    :cond_1
    :try_start_1
    sget-object v0, Lcom/google/c/a/a/a/c/a/d/c;->a:Lcom/google/c/a/a/a/c/a/d/c;

    iput-object v0, p0, Lcom/google/c/a/a/a/c/a/d/a;->a:Lcom/google/c/a/a/a/c/a/d/c;

    .line 31
    invoke-virtual {p0}, Lcom/google/c/a/a/a/c/a/d/a;->e()V

    .line 32
    iget-object v0, p0, Lcom/google/c/a/a/a/c/a/d/a;->d:Lcom/google/c/a/a/a/c/a/d/f;

    if-eqz v0, :cond_0

    .line 33
    iget-object v0, p0, Lcom/google/c/a/a/a/c/a/d/a;->d:Lcom/google/c/a/a/a/c/a/d/f;

    invoke-interface {v0}, Lcom/google/c/a/a/a/c/a/d/f;->a()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 27
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final c()Lcom/google/c/a/a/a/c/a/d/c;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lcom/google/c/a/a/a/c/a/d/a;->a:Lcom/google/c/a/a/a/c/a/d/c;

    return-object v0
.end method

.method protected abstract d()V
.end method

.method protected abstract e()V
.end method
