.class public final Lcom/google/c/a/a/a/c/a/d/p;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final a:Lcom/google/c/a/a/a/c/a/d/r;

.field final b:Ljava/lang/String;

.field final c:Ljava/util/Map;

.field final d:Ljava/lang/String;


# direct methods
.method private constructor <init>(Lcom/google/c/a/a/a/c/a/d/q;)V
    .locals 2

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    iget-object v0, p1, Lcom/google/c/a/a/a/c/a/d/q;->a:Lcom/google/c/a/a/a/c/a/d/r;

    iput-object v0, p0, Lcom/google/c/a/a/a/c/a/d/p;->a:Lcom/google/c/a/a/a/c/a/d/r;

    .line 22
    iget-object v0, p1, Lcom/google/c/a/a/a/c/a/d/q;->b:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/c/a/a/a/c/a/d/p;->b:Ljava/lang/String;

    .line 23
    iget-object v0, p1, Lcom/google/c/a/a/a/c/a/d/q;->c:Ljava/util/Map;

    iput-object v0, p0, Lcom/google/c/a/a/a/c/a/d/p;->c:Ljava/util/Map;

    .line 24
    iget-object v0, p1, Lcom/google/c/a/a/a/c/a/d/q;->d:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/c/a/a/a/c/a/d/p;->d:Ljava/lang/String;

    .line 26
    iget-object v0, p0, Lcom/google/c/a/a/a/c/a/d/p;->b:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/c/a/a/a/c/a/d/p;->a:Lcom/google/c/a/a/a/c/a/d/r;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/c/a/a/a/c/a/d/p;->d:Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/c/a/a/a/c/a/d/p;->a:Lcom/google/c/a/a/a/c/a/d/r;

    sget-object v1, Lcom/google/c/a/a/a/c/a/d/r;->b:Lcom/google/c/a/a/a/c/a/d/r;

    if-eq v0, v1, :cond_1

    .line 27
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 29
    :cond_1
    return-void
.end method

.method private synthetic constructor <init>(Lcom/google/c/a/a/a/c/a/d/q;B)V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0, p1}, Lcom/google/c/a/a/a/c/a/d/p;-><init>(Lcom/google/c/a/a/a/c/a/d/q;)V

    return-void
.end method

.method public static a(Lcom/google/c/a/a/a/c/a/b/d;Ljava/lang/String;Ljava/util/Map;Lcom/google/c/a/a/a/c/a/d/r;Ljava/lang/String;)Lcom/google/c/a/a/a/c/a/d/p;
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 99
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/google/c/a/a/a/c/a/b/d;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 100
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 101
    iget-object v1, p0, Lcom/google/c/a/a/a/c/a/b/d;->e:Lcom/google/c/a/a/a/c/a/b/f;

    iget-object v3, p0, Lcom/google/c/a/a/a/c/a/b/d;->b:Ljava/util/Map;

    invoke-virtual {v1, v3}, Lcom/google/c/a/a/a/c/a/b/f;->a(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    .line 102
    invoke-interface {v0, p2}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    .line 104
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 105
    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 106
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->length()I

    move-result v1

    if-nez v1, :cond_0

    .line 107
    const-string v1, "?"

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 111
    :goto_1
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 112
    const-string v1, "="

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 113
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 109
    :cond_0
    const-string v1, "&"

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 116
    :cond_1
    new-instance v0, Lcom/google/c/a/a/a/c/a/d/q;

    invoke-direct {v0, v5}, Lcom/google/c/a/a/a/c/a/d/q;-><init>(B)V

    if-nez p3, :cond_2

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_2
    iput-object p3, v0, Lcom/google/c/a/a/a/c/a/d/q;->a:Lcom/google/c/a/a/a/c/a/d/r;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_3

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_3
    iput-object v1, v0, Lcom/google/c/a/a/a/c/a/d/q;->b:Ljava/lang/String;

    iput-object p4, v0, Lcom/google/c/a/a/a/c/a/d/q;->d:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/c/a/a/a/c/a/b/d;->e:Lcom/google/c/a/a/a/c/a/b/f;

    iget-object v1, p0, Lcom/google/c/a/a/a/c/a/b/d;->c:Ljava/util/Map;

    iget-object v2, v0, Lcom/google/c/a/a/a/c/a/d/q;->c:Ljava/util/Map;

    invoke-interface {v2, v1}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    new-instance v1, Lcom/google/c/a/a/a/c/a/d/p;

    invoke-direct {v1, v0, v5}, Lcom/google/c/a/a/a/c/a/d/p;-><init>(Lcom/google/c/a/a/a/c/a/d/q;B)V

    return-object v1
.end method


# virtual methods
.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 137
    if-ne p0, p1, :cond_1

    .line 171
    :cond_0
    :goto_0
    return v0

    .line 140
    :cond_1
    if-nez p1, :cond_2

    move v0, v1

    .line 141
    goto :goto_0

    .line 143
    :cond_2
    instance-of v2, p1, Lcom/google/c/a/a/a/c/a/d/p;

    if-nez v2, :cond_3

    move v0, v1

    .line 144
    goto :goto_0

    .line 146
    :cond_3
    check-cast p1, Lcom/google/c/a/a/a/c/a/d/p;

    .line 147
    iget-object v2, p0, Lcom/google/c/a/a/a/c/a/d/p;->c:Ljava/util/Map;

    if-nez v2, :cond_4

    .line 148
    iget-object v2, p1, Lcom/google/c/a/a/a/c/a/d/p;->c:Ljava/util/Map;

    if-eqz v2, :cond_5

    move v0, v1

    .line 149
    goto :goto_0

    .line 151
    :cond_4
    iget-object v2, p0, Lcom/google/c/a/a/a/c/a/d/p;->c:Ljava/util/Map;

    iget-object v3, p1, Lcom/google/c/a/a/a/c/a/d/p;->c:Ljava/util/Map;

    invoke-interface {v2, v3}, Ljava/util/Map;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_5

    move v0, v1

    .line 152
    goto :goto_0

    .line 154
    :cond_5
    iget-object v2, p0, Lcom/google/c/a/a/a/c/a/d/p;->a:Lcom/google/c/a/a/a/c/a/d/r;

    iget-object v3, p1, Lcom/google/c/a/a/a/c/a/d/p;->a:Lcom/google/c/a/a/a/c/a/d/r;

    if-eq v2, v3, :cond_6

    move v0, v1

    .line 155
    goto :goto_0

    .line 157
    :cond_6
    iget-object v2, p0, Lcom/google/c/a/a/a/c/a/d/p;->d:Ljava/lang/String;

    if-nez v2, :cond_7

    .line 158
    iget-object v2, p1, Lcom/google/c/a/a/a/c/a/d/p;->d:Ljava/lang/String;

    if-eqz v2, :cond_8

    move v0, v1

    .line 159
    goto :goto_0

    .line 161
    :cond_7
    iget-object v2, p0, Lcom/google/c/a/a/a/c/a/d/p;->d:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/c/a/a/a/c/a/d/p;->d:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_8

    move v0, v1

    .line 162
    goto :goto_0

    .line 164
    :cond_8
    iget-object v2, p0, Lcom/google/c/a/a/a/c/a/d/p;->b:Ljava/lang/String;

    if-nez v2, :cond_9

    .line 165
    iget-object v2, p1, Lcom/google/c/a/a/a/c/a/d/p;->b:Ljava/lang/String;

    if-eqz v2, :cond_0

    move v0, v1

    .line 166
    goto :goto_0

    .line 168
    :cond_9
    iget-object v2, p0, Lcom/google/c/a/a/a/c/a/d/p;->b:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/c/a/a/a/c/a/d/p;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    .line 169
    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 126
    iget-object v0, p0, Lcom/google/c/a/a/a/c/a/d/p;->c:Ljava/util/Map;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/lit8 v0, v0, 0x1f

    .line 129
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/c/a/a/a/c/a/d/p;->a:Lcom/google/c/a/a/a/c/a/d/r;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    .line 130
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/c/a/a/a/c/a/d/p;->d:Ljava/lang/String;

    if-nez v0, :cond_2

    move v0, v1

    :goto_2
    add-int/2addr v0, v2

    .line 131
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/c/a/a/a/c/a/d/p;->b:Ljava/lang/String;

    if-nez v2, :cond_3

    :goto_3
    add-int/2addr v0, v1

    .line 132
    return v0

    .line 126
    :cond_0
    iget-object v0, p0, Lcom/google/c/a/a/a/c/a/d/p;->c:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->hashCode()I

    move-result v0

    goto :goto_0

    .line 129
    :cond_1
    iget-object v0, p0, Lcom/google/c/a/a/a/c/a/d/p;->a:Lcom/google/c/a/a/a/c/a/d/r;

    invoke-virtual {v0}, Lcom/google/c/a/a/a/c/a/d/r;->hashCode()I

    move-result v0

    goto :goto_1

    .line 130
    :cond_2
    iget-object v0, p0, Lcom/google/c/a/a/a/c/a/d/p;->d:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_2

    .line 131
    :cond_3
    iget-object v1, p0, Lcom/google/c/a/a/a/c/a/d/p;->b:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_3
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 176
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "HttpRequestParams [method="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/c/a/a/a/c/a/d/p;->a:Lcom/google/c/a/a/a/c/a/d/r;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", url="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/c/a/a/a/c/a/d/p;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", headers="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/c/a/a/a/c/a/d/p;->c:Ljava/util/Map;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", postdata="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/c/a/a/a/c/a/d/p;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
