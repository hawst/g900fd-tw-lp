.class final Lcom/google/c/a/a/b/b/b/w;
.super Lcom/google/c/a/a/b/b/b/v;
.source "SourceFile"


# instance fields
.field private final a:Ljava/util/List;

.field private b:Ljava/util/ListIterator;

.field private c:Lcom/google/c/a/a/b/c/e;


# direct methods
.method public constructor <init>(Ljava/util/List;)V
    .locals 1

    .prologue
    .line 286
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/c/a/a/b/b/b/v;-><init>(B)V

    .line 287
    iput-object p1, p0, Lcom/google/c/a/a/b/b/b/w;->a:Ljava/util/List;

    .line 288
    invoke-virtual {p0}, Lcom/google/c/a/a/b/b/b/w;->b()V

    .line 289
    return-void
.end method


# virtual methods
.method final a()V
    .locals 1

    .prologue
    .line 305
    iget-object v0, p0, Lcom/google/c/a/a/b/b/b/w;->b:Ljava/util/ListIterator;

    invoke-interface {v0}, Ljava/util/ListIterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 306
    iget-object v0, p0, Lcom/google/c/a/a/b/b/b/w;->b:Ljava/util/ListIterator;

    invoke-interface {v0}, Ljava/util/ListIterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/c/a/a/b/c/e;

    iput-object v0, p0, Lcom/google/c/a/a/b/b/b/w;->c:Lcom/google/c/a/a/b/c/e;

    .line 310
    :goto_0
    return-void

    .line 308
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/c/a/a/b/b/b/w;->c:Lcom/google/c/a/a/b/c/e;

    goto :goto_0
.end method

.method public final a(Lcom/google/c/b/a/c;)V
    .locals 4

    .prologue
    .line 298
    iget-object v0, p0, Lcom/google/c/a/a/b/b/b/w;->c:Lcom/google/c/a/a/b/c/e;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "No current command."

    invoke-static {v0, v1}, Lcom/google/c/a/a/b/h/c;->b(ZLjava/lang/String;)V

    .line 299
    new-instance v0, Lcom/google/c/a/a/b/c/e;

    iget-object v1, p0, Lcom/google/c/a/a/b/b/b/w;->c:Lcom/google/c/a/a/b/c/e;

    invoke-virtual {v1}, Lcom/google/c/a/a/b/c/e;->a()J

    move-result-wide v2

    invoke-direct {v0, v2, v3, p1}, Lcom/google/c/a/a/b/c/e;-><init>(JLcom/google/c/b/a/c;)V

    iput-object v0, p0, Lcom/google/c/a/a/b/b/b/w;->c:Lcom/google/c/a/a/b/c/e;

    .line 300
    iget-object v0, p0, Lcom/google/c/a/a/b/b/b/w;->b:Ljava/util/ListIterator;

    iget-object v1, p0, Lcom/google/c/a/a/b/b/b/w;->c:Lcom/google/c/a/a/b/c/e;

    invoke-interface {v0, v1}, Ljava/util/ListIterator;->set(Ljava/lang/Object;)V

    .line 301
    return-void

    .line 298
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method final b()V
    .locals 1

    .prologue
    .line 314
    iget-object v0, p0, Lcom/google/c/a/a/b/b/b/w;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->listIterator()Ljava/util/ListIterator;

    move-result-object v0

    iput-object v0, p0, Lcom/google/c/a/a/b/b/b/w;->b:Ljava/util/ListIterator;

    .line 315
    invoke-virtual {p0}, Lcom/google/c/a/a/b/b/b/w;->a()V

    .line 316
    return-void
.end method

.method public final c()Lcom/google/c/b/a/c;
    .locals 1

    .prologue
    .line 293
    iget-object v0, p0, Lcom/google/c/a/a/b/b/b/w;->c:Lcom/google/c/a/a/b/c/e;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/c/a/a/b/b/b/w;->c:Lcom/google/c/a/a/b/c/e;

    invoke-virtual {v0}, Lcom/google/c/a/a/b/c/e;->b()Lcom/google/c/b/a/c;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
