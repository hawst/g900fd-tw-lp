.class public final enum Lcom/google/c/a/a/a/c/a/e/e;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field public static final enum a:Lcom/google/c/a/a/a/c/a/e/e;

.field public static final enum b:Lcom/google/c/a/a/a/c/a/e/e;

.field public static final enum c:Lcom/google/c/a/a/a/c/a/e/e;

.field public static final enum d:Lcom/google/c/a/a/a/c/a/e/e;

.field public static final enum e:Lcom/google/c/a/a/a/c/a/e/e;

.field private static final synthetic f:[Lcom/google/c/a/a/a/c/a/e/e;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 10
    new-instance v0, Lcom/google/c/a/a/a/c/a/e/e;

    const-string v1, "NETWORK"

    invoke-direct {v0, v1, v2}, Lcom/google/c/a/a/a/c/a/e/e;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/c/a/a/a/c/a/e/e;->a:Lcom/google/c/a/a/a/c/a/e/e;

    .line 15
    new-instance v0, Lcom/google/c/a/a/a/c/a/e/e;

    const-string v1, "SERVER_ERROR"

    invoke-direct {v0, v1, v3}, Lcom/google/c/a/a/a/c/a/e/e;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/c/a/a/a/c/a/e/e;->b:Lcom/google/c/a/a/a/c/a/e/e;

    .line 20
    new-instance v0, Lcom/google/c/a/a/a/c/a/e/e;

    const-string v1, "SERVER_BACKOFF"

    invoke-direct {v0, v1, v4}, Lcom/google/c/a/a/a/c/a/e/e;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/c/a/a/a/c/a/e/e;->c:Lcom/google/c/a/a/a/c/a/e/e;

    .line 25
    new-instance v0, Lcom/google/c/a/a/a/c/a/e/e;

    const-string v1, "INVALID_RESPONSE"

    invoke-direct {v0, v1, v5}, Lcom/google/c/a/a/a/c/a/e/e;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/c/a/a/a/c/a/e/e;->d:Lcom/google/c/a/a/a/c/a/e/e;

    .line 30
    new-instance v0, Lcom/google/c/a/a/a/c/a/e/e;

    const-string v1, "UNKNOWN_SID"

    invoke-direct {v0, v1, v6}, Lcom/google/c/a/a/a/c/a/e/e;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/c/a/a/a/c/a/e/e;->e:Lcom/google/c/a/a/a/c/a/e/e;

    .line 6
    const/4 v0, 0x5

    new-array v0, v0, [Lcom/google/c/a/a/a/c/a/e/e;

    sget-object v1, Lcom/google/c/a/a/a/c/a/e/e;->a:Lcom/google/c/a/a/a/c/a/e/e;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/c/a/a/a/c/a/e/e;->b:Lcom/google/c/a/a/a/c/a/e/e;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/c/a/a/a/c/a/e/e;->c:Lcom/google/c/a/a/a/c/a/e/e;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/c/a/a/a/c/a/e/e;->d:Lcom/google/c/a/a/a/c/a/e/e;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/c/a/a/a/c/a/e/e;->e:Lcom/google/c/a/a/a/c/a/e/e;

    aput-object v1, v0, v6

    sput-object v0, Lcom/google/c/a/a/a/c/a/e/e;->f:[Lcom/google/c/a/a/a/c/a/e/e;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 6
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/c/a/a/a/c/a/e/e;
    .locals 1

    .prologue
    .line 6
    const-class v0, Lcom/google/c/a/a/a/c/a/e/e;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/c/a/a/a/c/a/e/e;

    return-object v0
.end method

.method public static values()[Lcom/google/c/a/a/a/c/a/e/e;
    .locals 1

    .prologue
    .line 6
    sget-object v0, Lcom/google/c/a/a/a/c/a/e/e;->f:[Lcom/google/c/a/a/a/c/a/e/e;

    invoke-virtual {v0}, [Lcom/google/c/a/a/a/c/a/e/e;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/c/a/a/a/c/a/e/e;

    return-object v0
.end method
