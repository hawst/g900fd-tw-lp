.class public final Lcom/google/c/a/a/a/c/a/e/h;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/c/a/a/a/c/a/e/a;


# instance fields
.field private final a:Lcom/google/c/a/a/a/c/a/e/c;

.field private final b:Ljava/util/concurrent/ScheduledExecutorService;


# direct methods
.method public constructor <init>(Lcom/google/c/a/a/a/c/a/e/c;Ljava/util/concurrent/ScheduledExecutorService;)V
    .locals 1

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    .line 21
    :cond_0
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 23
    :cond_1
    iput-object p1, p0, Lcom/google/c/a/a/a/c/a/e/h;->a:Lcom/google/c/a/a/a/c/a/e/c;

    .line 24
    iput-object p2, p0, Lcom/google/c/a/a/a/c/a/e/h;->b:Ljava/util/concurrent/ScheduledExecutorService;

    .line 25
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/c/a/a/a/c/a/e/d;Ljava/lang/Runnable;)V
    .locals 4

    .prologue
    .line 29
    sget-object v0, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0x1

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    long-to-double v0, v0

    iget-object v2, p0, Lcom/google/c/a/a/a/c/a/e/h;->a:Lcom/google/c/a/a/a/c/a/e/c;

    invoke-interface {v2, p1}, Lcom/google/c/a/a/a/c/a/e/c;->a(Lcom/google/c/a/a/a/c/a/e/d;)D

    move-result-wide v2

    mul-double/2addr v0, v2

    double-to-long v0, v0

    .line 31
    iget-object v2, p0, Lcom/google/c/a/a/a/c/a/e/h;->b:Ljava/util/concurrent/ScheduledExecutorService;

    sget-object v3, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v2, p2, v0, v1, v3}, Ljava/util/concurrent/ScheduledExecutorService;->schedule(Ljava/lang/Runnable;JLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;

    .line 32
    return-void
.end method
