.class public final Lcom/google/c/a/a/a/c/a/b/d;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Ljava/util/Map;

.field public final c:Ljava/util/Map;

.field public final d:Lcom/google/c/a/a/a/c/a/b/a;

.field public final e:Lcom/google/c/a/a/a/c/a/b/f;


# direct methods
.method private constructor <init>(Lcom/google/c/a/a/a/c/a/b/e;)V
    .locals 2

    .prologue
    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 18
    iget-object v0, p1, Lcom/google/c/a/a/a/c/a/b/e;->a:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 19
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "baseUri"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 22
    :cond_0
    iget-object v0, p1, Lcom/google/c/a/a/a/c/a/b/e;->a:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/c/a/a/a/c/a/b/d;->a:Ljava/lang/String;

    .line 23
    iget-object v0, p1, Lcom/google/c/a/a/a/c/a/b/e;->b:Ljava/util/Map;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, Lcom/google/c/a/a/a/c/a/b/d;->b:Ljava/util/Map;

    .line 24
    iget-object v0, p1, Lcom/google/c/a/a/a/c/a/b/e;->c:Ljava/util/Map;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, Lcom/google/c/a/a/a/c/a/b/d;->c:Ljava/util/Map;

    .line 25
    iget-object v0, p1, Lcom/google/c/a/a/a/c/a/b/e;->d:Lcom/google/c/a/a/a/c/a/b/a;

    iput-object v0, p0, Lcom/google/c/a/a/a/c/a/b/d;->d:Lcom/google/c/a/a/a/c/a/b/a;

    .line 26
    iget-object v0, p1, Lcom/google/c/a/a/a/c/a/b/e;->e:Lcom/google/c/a/a/a/c/a/b/f;

    if-eqz v0, :cond_1

    iget-object v0, p1, Lcom/google/c/a/a/a/c/a/b/e;->e:Lcom/google/c/a/a/a/c/a/b/f;

    :goto_0
    iput-object v0, p0, Lcom/google/c/a/a/a/c/a/b/d;->e:Lcom/google/c/a/a/a/c/a/b/f;

    .line 27
    return-void

    .line 26
    :cond_1
    new-instance v0, Lcom/google/c/a/a/a/c/a/b/f;

    invoke-direct {v0}, Lcom/google/c/a/a/a/c/a/b/f;-><init>()V

    goto :goto_0
.end method

.method synthetic constructor <init>(Lcom/google/c/a/a/a/c/a/b/e;B)V
    .locals 0

    .prologue
    .line 10
    invoke-direct {p0, p1}, Lcom/google/c/a/a/a/c/a/b/d;-><init>(Lcom/google/c/a/a/a/c/a/b/e;)V

    return-void
.end method
