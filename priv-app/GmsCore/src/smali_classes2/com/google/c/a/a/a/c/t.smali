.class public final Lcom/google/c/a/a/a/c/t;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/c/a/a/a/c/i;


# instance fields
.field private final a:Ljava/util/Map;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/c/a/a/a/c/t;->a:Ljava/util/Map;

    return-void
.end method


# virtual methods
.method public final declared-synchronized a()V
    .locals 0

    .prologue
    .line 92
    monitor-enter p0

    monitor-exit p0

    return-void
.end method

.method public final a(Lcom/google/c/a/a/a/c/a/g;)V
    .locals 0

    .prologue
    .line 107
    return-void
.end method

.method public final a(Ljava/lang/Class;Lcom/google/c/a/a/a/c/r;)V
    .locals 2

    .prologue
    .line 66
    iget-object v0, p0, Lcom/google/c/a/a/a/c/t;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 67
    if-nez v0, :cond_0

    .line 68
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 69
    iget-object v1, p0, Lcom/google/c/a/a/a/c/t;->a:Ljava/util/Map;

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 71
    :cond_0
    invoke-interface {v0, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 72
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 112
    return-void
.end method

.method public final declared-synchronized a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 62
    monitor-enter p0

    monitor-exit p0

    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/util/Map;Ljava/lang/Object;ZLcom/google/c/a/a/b/e/f;Ljava/lang/Class;)V
    .locals 9

    .prologue
    const/4 v7, 0x0

    const/4 v8, 0x0

    .line 37
    const-string v0, "gs"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 38
    const-string v0, "0"

    const-string v1, "fakeUserId"

    const-string v2, "Test User"

    const-string v3, "black"

    const/4 v4, 0x1

    const-string v5, "https://www.example.com"

    invoke-static/range {v0 .. v5}, Lcom/google/c/a/a/b/g/d;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;)Lcom/google/c/a/a/b/g/d;

    move-result-object v3

    .line 40
    new-instance v0, Lcom/google/c/a/a/b/c/f;

    const-string v1, "object-id"

    const-string v2, "0"

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v5

    invoke-static {v3}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v6

    move v3, v8

    move v4, v8

    invoke-direct/range {v0 .. v7}, Lcom/google/c/a/a/b/c/f;-><init>(Ljava/lang/String;Ljava/lang/String;ZILjava/util/List;Ljava/util/Collection;Ljava/lang/String;)V

    .line 48
    invoke-interface {p5, v0}, Lcom/google/c/a/a/b/e/f;->a(Ljava/lang/Object;)V

    .line 57
    :cond_0
    return-void

    .line 49
    :cond_1
    const-string v0, "save"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 50
    check-cast p3, Lcom/google/c/a/a/b/c/g;

    .line 51
    invoke-interface {p5, v7}, Lcom/google/c/a/a/b/e/f;->a(Ljava/lang/Object;)V

    .line 52
    new-instance v0, Lcom/google/c/a/a/b/c/d;

    invoke-static {}, Lcom/google/c/b/a/d;->a()Lcom/google/c/b/a/d;

    move-result-object v1

    const-string v2, "uid"

    invoke-virtual {p3}, Lcom/google/c/a/a/b/c/g;->a()I

    move-result v3

    add-int/lit8 v3, v3, 0x1

    const-wide/16 v4, 0x0

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {p3}, Lcom/google/c/a/a/b/c/g;->b()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-direct/range {v0 .. v5}, Lcom/google/c/a/a/b/c/d;-><init>(Lcom/google/c/b/a/c;Ljava/lang/String;ILjava/lang/Long;Ljava/lang/Long;)V

    .line 55
    new-instance v1, Lcom/google/c/a/a/b/c/c;

    invoke-static {v0}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/c/a/a/b/c/c;-><init>(Ljava/util/List;)V

    iget-object v0, p0, Lcom/google/c/a/a/a/c/t;->a:Ljava/util/Map;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/c/a/a/a/c/r;

    invoke-interface {v0, v1}, Lcom/google/c/a/a/a/c/r;->a(Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public final b()V
    .locals 0

    .prologue
    .line 97
    return-void
.end method

.method public final c()V
    .locals 0

    .prologue
    .line 102
    return-void
.end method
