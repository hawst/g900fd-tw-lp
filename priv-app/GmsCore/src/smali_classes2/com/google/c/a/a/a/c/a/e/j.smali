.class public final Lcom/google/c/a/a/a/c/a/e/j;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/c/a/a/a/c/a/e/c;


# instance fields
.field private volatile a:I

.field private final b:D

.field private final c:D


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    const-wide/high16 v0, 0x3ff0000000000000L    # 1.0

    iput-wide v0, p0, Lcom/google/c/a/a/a/c/a/e/j;->b:D

    .line 41
    const-wide/high16 v0, 0x403e000000000000L    # 30.0

    iput-wide v0, p0, Lcom/google/c/a/a/a/c/a/e/j;->c:D

    .line 43
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/c/a/a/a/c/a/e/d;)D
    .locals 6

    .prologue
    const-wide/high16 v4, 0x4000000000000000L    # 2.0

    .line 50
    sget-object v0, Lcom/google/c/a/a/a/c/a/e/d;->a:Lcom/google/c/a/a/a/c/a/e/d;

    if-ne p1, v0, :cond_0

    .line 51
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/c/a/a/a/c/a/e/j;->a:I

    .line 52
    const-wide/16 v0, 0x0

    .line 67
    :goto_0
    return-wide v0

    .line 54
    :cond_0
    iget-wide v0, p0, Lcom/google/c/a/a/a/c/a/e/j;->b:D

    iget v2, p0, Lcom/google/c/a/a/a/c/a/e/j;->a:I

    invoke-static {v0, v1, v2}, Ljava/lang/Math;->scalb(DI)D

    move-result-wide v0

    .line 55
    iget v2, p0, Lcom/google/c/a/a/a/c/a/e/j;->a:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/google/c/a/a/a/c/a/e/j;->a:I

    .line 60
    iget-wide v2, p0, Lcom/google/c/a/a/a/c/a/e/j;->c:D

    div-double/2addr v2, v4

    cmpl-double v2, v0, v2

    if-lez v2, :cond_1

    .line 61
    iget-wide v0, p0, Lcom/google/c/a/a/a/c/a/e/j;->c:D

    div-double/2addr v0, v4

    .line 65
    :cond_1
    invoke-static {}, Ljava/lang/Math;->random()D

    move-result-wide v2

    mul-double/2addr v2, v0

    add-double/2addr v0, v2

    .line 67
    goto :goto_0
.end method
