.class public final Lcom/google/c/a/a/b/f/a/n;
.super Lcom/google/c/a/a/b/f/b;
.source "SourceFile"


# static fields
.field private static final c:Ljava/util/Map;

.field private static final d:Ljava/util/Map;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    .line 38
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/google/c/a/a/b/f/a/n;->c:Ljava/util/Map;

    .line 39
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/google/c/a/a/b/f/a/n;->d:Ljava/util/Map;

    .line 42
    invoke-static {}, Lcom/google/c/a/a/b/b/a/b/c;->values()[Lcom/google/c/a/a/b/b/a/b/c;

    move-result-object v1

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    .line 43
    sget-object v4, Lcom/google/c/a/a/b/f/a/n;->c:Ljava/util/Map;

    invoke-virtual {v3}, Lcom/google/c/a/a/b/b/a/b/c;->a()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3}, Lcom/google/c/a/a/b/b/a/b/c;->b()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-interface {v4, v5, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 44
    sget-object v4, Lcom/google/c/a/a/b/f/a/n;->d:Ljava/util/Map;

    invoke-virtual {v3}, Lcom/google/c/a/a/b/b/a/b/c;->b()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3}, Lcom/google/c/a/a/b/b/a/b/c;->a()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v4, v5, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 42
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 46
    :cond_0
    return-void
.end method

.method public constructor <init>(Z)V
    .locals 1

    .prologue
    .line 49
    const-class v0, Lcom/google/c/a/a/b/b/a/c/e;

    invoke-direct {p0, v0, p1}, Lcom/google/c/a/a/b/f/b;-><init>(Ljava/lang/Class;Z)V

    .line 50
    return-void
.end method


# virtual methods
.method protected final synthetic a(Ljava/lang/Object;Ljava/lang/Object;Lcom/google/c/a/a/b/d/b;)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 20
    check-cast p1, Lcom/google/c/a/a/b/b/a/c/e;

    sget-object v0, Lcom/google/c/a/a/b/f/a/o;->a:Lcom/google/c/a/a/b/f/a/o;

    invoke-virtual {p1}, Lcom/google/c/a/a/b/b/a/c/e;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, p2, p3, v0, v1}, Lcom/google/c/a/a/b/f/a/n;->a(Ljava/lang/Object;Lcom/google/c/a/a/b/d/b;Lcom/google/c/a/a/b/f/a/k;Ljava/lang/Object;)V

    invoke-virtual {p1}, Lcom/google/c/a/a/b/b/a/c/e;->c()Ljava/lang/String;

    move-result-object v4

    sget-object v0, Lcom/google/c/a/a/b/f/a/n;->c:Ljava/util/Map;

    invoke-interface {v0, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    if-eqz v0, :cond_0

    sget-object v3, Lcom/google/c/a/a/b/f/a/o;->b:Lcom/google/c/a/a/b/f/a/o;

    invoke-virtual {v0}, Ljava/lang/Integer;->doubleValue()D

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v4

    move-object v0, p0

    move-object v1, p2

    move-object v2, p3

    invoke-virtual/range {v0 .. v5}, Lcom/google/c/a/a/b/f/a/n;->a(Ljava/lang/Object;Lcom/google/c/a/a/b/d/b;Lcom/google/c/a/a/b/f/a/k;Ljava/lang/Object;Z)V

    :goto_0
    sget-object v0, Lcom/google/c/a/a/b/f/a/o;->c:Lcom/google/c/a/a/b/f/a/o;

    invoke-virtual {p1}, Lcom/google/c/a/a/b/b/a/c/e;->d()Lcom/google/c/a/a/b/b/a/c/n;

    move-result-object v1

    invoke-virtual {p0, p2, p3, v0, v1}, Lcom/google/c/a/a/b/f/a/n;->a(Ljava/lang/Object;Lcom/google/c/a/a/b/d/b;Lcom/google/c/a/a/b/f/a/k;Ljava/lang/Object;)V

    return-void

    :cond_0
    sget-object v3, Lcom/google/c/a/a/b/f/a/o;->b:Lcom/google/c/a/a/b/f/a/o;

    move-object v0, p0

    move-object v1, p2

    move-object v2, p3

    invoke-virtual/range {v0 .. v5}, Lcom/google/c/a/a/b/f/a/n;->a(Ljava/lang/Object;Lcom/google/c/a/a/b/d/b;Lcom/google/c/a/a/b/f/a/k;Ljava/lang/Object;Z)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 82
    instance-of v0, p1, Lcom/google/c/a/a/b/b/a/c/e;

    return v0
.end method

.method public final synthetic b(Ljava/lang/Object;Lcom/google/c/a/a/b/d/b;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 20
    sget-object v0, Lcom/google/c/a/a/b/f/a/o;->a:Lcom/google/c/a/a/b/f/a/o;

    const-class v1, Ljava/lang/String;

    invoke-virtual {p0, p1, p2, v0, v1}, Lcom/google/c/a/a/b/f/a/n;->a(Ljava/lang/Object;Lcom/google/c/a/a/b/d/b;Lcom/google/c/a/a/b/f/a/k;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    sget-object v1, Lcom/google/c/a/a/b/f/a/o;->b:Lcom/google/c/a/a/b/f/a/o;

    invoke-virtual {p0, p1, p2, v1}, Lcom/google/c/a/a/b/f/a/n;->a(Ljava/lang/Object;Lcom/google/c/a/a/b/d/b;Lcom/google/c/a/a/b/f/a/k;)Ljava/lang/Object;

    move-result-object v1

    instance-of v2, v1, Ljava/lang/String;

    if-eqz v2, :cond_0

    check-cast v1, Ljava/lang/String;

    move-object v2, v1

    :goto_0
    sget-object v1, Lcom/google/c/a/a/b/f/a/o;->c:Lcom/google/c/a/a/b/f/a/o;

    const-class v3, Lcom/google/c/a/a/b/b/a/c/n;

    invoke-virtual {p0, p1, p2, v1, v3}, Lcom/google/c/a/a/b/f/a/n;->a(Ljava/lang/Object;Lcom/google/c/a/a/b/d/b;Lcom/google/c/a/a/b/f/a/k;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/c/a/a/b/b/a/c/n;

    new-instance v3, Lcom/google/c/a/a/b/b/a/c/e;

    invoke-direct {v3, v0, v2, v1}, Lcom/google/c/a/a/b/b/a/c/e;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/google/c/a/a/b/b/a/c/n;)V

    return-object v3

    :cond_0
    check-cast v1, Ljava/lang/Double;

    sget-object v2, Lcom/google/c/a/a/b/f/a/n;->d:Ljava/util/Map;

    invoke-virtual {v1}, Ljava/lang/Double;->intValue()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v2, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    move-object v2, v1

    goto :goto_0
.end method
