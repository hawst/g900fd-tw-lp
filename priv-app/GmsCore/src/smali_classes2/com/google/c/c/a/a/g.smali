.class public final Lcom/google/c/c/a/a/g;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/c/c/a/a/c;


# instance fields
.field private final a:I

.field private final b:Ljava/io/OutputStream;

.field private final c:[B

.field private d:I

.field private e:Lcom/google/c/c/a/a/a;


# direct methods
.method public constructor <init>(Ljava/io/OutputStream;I)V
    .locals 1

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/c/c/a/a/g;->d:I

    .line 28
    iput-object p1, p0, Lcom/google/c/c/a/a/g;->b:Ljava/io/OutputStream;

    .line 29
    iput p2, p0, Lcom/google/c/c/a/a/g;->a:I

    .line 30
    new-array v0, p2, [B

    iput-object v0, p0, Lcom/google/c/c/a/a/g;->c:[B

    .line 31
    return-void
.end method

.method private b()V
    .locals 5

    .prologue
    const/16 v4, 0xa

    const/4 v3, 0x0

    .line 74
    iget-object v0, p0, Lcom/google/c/c/a/a/g;->b:Ljava/io/OutputStream;

    iget v1, p0, Lcom/google/c/c/a/a/g;->d:I

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    sget-object v2, Ljava/nio/charset/StandardCharsets;->UTF_8:Ljava/nio/charset/Charset;

    invoke-virtual {v1, v2}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/io/OutputStream;->write([B)V

    .line 75
    iget-object v0, p0, Lcom/google/c/c/a/a/g;->b:Ljava/io/OutputStream;

    invoke-virtual {v0, v4}, Ljava/io/OutputStream;->write(I)V

    .line 76
    iget-object v0, p0, Lcom/google/c/c/a/a/g;->b:Ljava/io/OutputStream;

    iget-object v1, p0, Lcom/google/c/c/a/a/g;->c:[B

    iget v2, p0, Lcom/google/c/c/a/a/g;->d:I

    invoke-virtual {v0, v1, v3, v2}, Ljava/io/OutputStream;->write([BII)V

    .line 77
    iget-object v0, p0, Lcom/google/c/c/a/a/g;->b:Ljava/io/OutputStream;

    invoke-virtual {v0, v4}, Ljava/io/OutputStream;->write(I)V

    .line 78
    iput v3, p0, Lcom/google/c/c/a/a/g;->d:I

    .line 79
    return-void
.end method

.method private c()V
    .locals 6

    .prologue
    .line 82
    iget-object v0, p0, Lcom/google/c/c/a/a/g;->e:Lcom/google/c/c/a/a/a;

    if-eqz v0, :cond_0

    .line 83
    iget-object v0, p0, Lcom/google/c/c/a/a/g;->b:Ljava/io/OutputStream;

    const-string v1, "%d-%d"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/c/c/a/a/g;->e:Lcom/google/c/c/a/a/a;

    iget-wide v4, v4, Lcom/google/c/c/a/a/a;->a:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    iget-object v4, p0, Lcom/google/c/c/a/a/g;->e:Lcom/google/c/c/a/a/a;

    iget-wide v4, v4, Lcom/google/c/c/a/a/a;->b:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    sget-object v2, Ljava/nio/charset/StandardCharsets;->UTF_8:Ljava/nio/charset/Charset;

    invoke-virtual {v1, v2}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/io/OutputStream;->write([B)V

    .line 85
    iget-object v0, p0, Lcom/google/c/c/a/a/g;->b:Ljava/io/OutputStream;

    const/16 v1, 0xa

    invoke-virtual {v0, v1}, Ljava/io/OutputStream;->write(I)V

    .line 86
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/c/c/a/a/g;->e:Lcom/google/c/c/a/a/a;

    .line 88
    :cond_0
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 63
    iget v0, p0, Lcom/google/c/c/a/a/g;->d:I

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/c/c/a/a/g;->e:Lcom/google/c/c/a/a/a;

    if-nez v0, :cond_3

    :cond_0
    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/k/a/ah;->b(Z)V

    .line 64
    iget v0, p0, Lcom/google/c/c/a/a/g;->d:I

    if-eqz v0, :cond_1

    .line 65
    invoke-direct {p0}, Lcom/google/c/c/a/a/g;->b()V

    .line 67
    :cond_1
    iget-object v0, p0, Lcom/google/c/c/a/a/g;->e:Lcom/google/c/c/a/a/a;

    if-eqz v0, :cond_2

    .line 68
    invoke-direct {p0}, Lcom/google/c/c/a/a/g;->c()V

    .line 70
    :cond_2
    iget-object v0, p0, Lcom/google/c/c/a/a/g;->b:Ljava/io/OutputStream;

    invoke-virtual {v0}, Ljava/io/OutputStream;->flush()V

    .line 71
    return-void

    .line 63
    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(B)V
    .locals 3

    .prologue
    .line 35
    iget-object v0, p0, Lcom/google/c/c/a/a/g;->e:Lcom/google/c/c/a/a/a;

    if-eqz v0, :cond_0

    .line 36
    invoke-direct {p0}, Lcom/google/c/c/a/a/g;->c()V

    .line 38
    :cond_0
    iget-object v0, p0, Lcom/google/c/c/a/a/g;->c:[B

    iget v1, p0, Lcom/google/c/c/a/a/g;->d:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lcom/google/c/c/a/a/g;->d:I

    aput-byte p1, v0, v1

    .line 39
    iget v0, p0, Lcom/google/c/c/a/a/g;->d:I

    iget v1, p0, Lcom/google/c/c/a/a/g;->a:I

    if-ne v0, v1, :cond_1

    .line 40
    invoke-direct {p0}, Lcom/google/c/c/a/a/g;->b()V

    .line 42
    :cond_1
    return-void
.end method

.method public final a(JI)V
    .locals 11

    .prologue
    const-wide/16 v8, 0x1

    const-wide/16 v6, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 46
    cmp-long v0, p1, v6

    if-ltz v0, :cond_1

    move v0, v1

    :goto_0
    invoke-static {v0}, Lcom/google/k/a/ah;->a(Z)V

    .line 47
    if-lez p3, :cond_2

    move v0, v1

    :goto_1
    invoke-static {v0}, Lcom/google/k/a/ah;->a(Z)V

    .line 48
    iget v0, p0, Lcom/google/c/c/a/a/g;->d:I

    if-eqz v0, :cond_0

    .line 49
    invoke-direct {p0}, Lcom/google/c/c/a/a/g;->b()V

    .line 52
    :cond_0
    iget-object v0, p0, Lcom/google/c/c/a/a/g;->e:Lcom/google/c/c/a/a/a;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/google/c/c/a/a/g;->e:Lcom/google/c/c/a/a/a;

    iget-wide v4, v0, Lcom/google/c/c/a/a/a;->b:J

    add-long/2addr v4, v8

    cmp-long v0, v4, p1

    if-nez v0, :cond_4

    .line 54
    iget-object v0, p0, Lcom/google/c/c/a/a/g;->e:Lcom/google/c/c/a/a/a;

    int-to-long v4, p3

    cmp-long v3, v4, v6

    if-lez v3, :cond_3

    :goto_2
    invoke-static {v1}, Lcom/google/k/a/ah;->a(Z)V

    new-instance v1, Lcom/google/c/c/a/a/a;

    iget-wide v2, v0, Lcom/google/c/c/a/a/a;->a:J

    iget-wide v6, v0, Lcom/google/c/c/a/a/a;->b:J

    add-long/2addr v4, v6

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/google/c/c/a/a/a;-><init>(JJ)V

    iput-object v1, p0, Lcom/google/c/c/a/a/g;->e:Lcom/google/c/c/a/a/a;

    .line 59
    :goto_3
    return-void

    :cond_1
    move v0, v2

    .line 46
    goto :goto_0

    :cond_2
    move v0, v2

    .line 47
    goto :goto_1

    :cond_3
    move v1, v2

    .line 54
    goto :goto_2

    .line 56
    :cond_4
    invoke-direct {p0}, Lcom/google/c/c/a/a/g;->c()V

    .line 57
    new-instance v0, Lcom/google/c/c/a/a/a;

    int-to-long v2, p3

    add-long/2addr v2, p1

    sub-long/2addr v2, v8

    invoke-direct {v0, p1, p2, v2, v3}, Lcom/google/c/c/a/a/a;-><init>(JJ)V

    iput-object v0, p0, Lcom/google/c/c/a/a/g;->e:Lcom/google/c/c/a/a/a;

    goto :goto_3
.end method
