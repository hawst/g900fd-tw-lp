.class final Lcom/google/c/c/a/a/a;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final a:J

.field final b:J


# direct methods
.method constructor <init>(JJ)V
    .locals 7

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    const-wide/16 v4, 0x0

    cmp-long v0, p1, v4

    if-ltz v0, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, Lcom/google/k/a/ah;->a(Z)V

    .line 18
    cmp-long v0, p3, p1

    if-ltz v0, :cond_1

    :goto_1
    invoke-static {v1}, Lcom/google/k/a/ah;->a(Z)V

    .line 19
    iput-wide p1, p0, Lcom/google/c/c/a/a/a;->a:J

    .line 20
    iput-wide p3, p0, Lcom/google/c/c/a/a/a;->b:J

    .line 21
    return-void

    :cond_0
    move v0, v2

    .line 17
    goto :goto_0

    :cond_1
    move v1, v2

    .line 18
    goto :goto_1
.end method


# virtual methods
.method public final equals(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 55
    if-ne p0, p1, :cond_1

    .line 63
    :cond_0
    :goto_0
    return v0

    .line 58
    :cond_1
    instance-of v2, p1, Lcom/google/c/c/a/a/a;

    if-nez v2, :cond_2

    move v0, v1

    .line 59
    goto :goto_0

    .line 62
    :cond_2
    check-cast p1, Lcom/google/c/c/a/a/a;

    .line 63
    iget-wide v2, p0, Lcom/google/c/c/a/a/a;->b:J

    iget-wide v4, p1, Lcom/google/c/c/a/a/a;->b:J

    cmp-long v2, v2, v4

    if-nez v2, :cond_3

    iget-wide v2, p0, Lcom/google/c/c/a/a/a;->a:J

    iget-wide v4, p1, Lcom/google/c/c/a/a/a;->a:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 7

    .prologue
    const/16 v6, 0x20

    .line 68
    iget-wide v0, p0, Lcom/google/c/c/a/a/a;->a:J

    iget-wide v2, p0, Lcom/google/c/c/a/a/a;->a:J

    ushr-long/2addr v2, v6

    xor-long/2addr v0, v2

    long-to-int v0, v0

    add-int/lit16 v0, v0, 0x20f

    .line 70
    mul-int/lit8 v0, v0, 0x1f

    iget-wide v2, p0, Lcom/google/c/c/a/a/a;->b:J

    iget-wide v4, p0, Lcom/google/c/c/a/a/a;->b:J

    ushr-long/2addr v4, v6

    xor-long/2addr v2, v4

    long-to-int v1, v2

    add-int/2addr v0, v1

    .line 71
    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 6

    .prologue
    .line 76
    const-string v0, "ByteRange{start=%d, end=%d}"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-wide v4, p0, Lcom/google/c/c/a/a/a;->a:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-wide v4, p0, Lcom/google/c/c/a/a/a;->b:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
