.class public final Lcom/google/c/e/b/d/a/a/b;
.super Lcom/google/protobuf/nano/d;
.source "SourceFile"


# instance fields
.field public a:[Ljava/lang/String;

.field public b:[Ljava/lang/String;

.field public c:[Lcom/google/c/e/b/d/a/a/c;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 237
    invoke-direct {p0}, Lcom/google/protobuf/nano/d;-><init>()V

    .line 238
    sget-object v0, Lcom/google/protobuf/nano/m;->f:[Ljava/lang/String;

    iput-object v0, p0, Lcom/google/c/e/b/d/a/a/b;->a:[Ljava/lang/String;

    sget-object v0, Lcom/google/protobuf/nano/m;->f:[Ljava/lang/String;

    iput-object v0, p0, Lcom/google/c/e/b/d/a/a/b;->b:[Ljava/lang/String;

    invoke-static {}, Lcom/google/c/e/b/d/a/a/c;->a()[Lcom/google/c/e/b/d/a/a/c;

    move-result-object v0

    iput-object v0, p0, Lcom/google/c/e/b/d/a/a/b;->c:[Lcom/google/c/e/b/d/a/a/c;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/c/e/b/d/a/a/b;->unknownFieldData:Lcom/google/protobuf/nano/f;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/c/e/b/d/a/a/b;->cachedSize:I

    .line 239
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 282
    invoke-super {p0}, Lcom/google/protobuf/nano/d;->computeSerializedSize()I

    move-result v4

    .line 283
    iget-object v0, p0, Lcom/google/c/e/b/d/a/a/b;->a:[Ljava/lang/String;

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/google/c/e/b/d/a/a/b;->a:[Ljava/lang/String;

    array-length v0, v0

    if-lez v0, :cond_7

    move v0, v1

    move v2, v1

    move v3, v1

    .line 286
    :goto_0
    iget-object v5, p0, Lcom/google/c/e/b/d/a/a/b;->a:[Ljava/lang/String;

    array-length v5, v5

    if-ge v0, v5, :cond_1

    .line 287
    iget-object v5, p0, Lcom/google/c/e/b/d/a/a/b;->a:[Ljava/lang/String;

    aget-object v5, v5, v0

    .line 288
    if-eqz v5, :cond_0

    .line 289
    add-int/lit8 v3, v3, 0x1

    .line 290
    invoke-static {v5}, Lcom/google/protobuf/nano/b;->a(Ljava/lang/String;)I

    move-result v5

    add-int/2addr v2, v5

    .line 286
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 294
    :cond_1
    add-int v0, v4, v2

    .line 295
    mul-int/lit8 v2, v3, 0x1

    add-int/2addr v0, v2

    .line 297
    :goto_1
    iget-object v2, p0, Lcom/google/c/e/b/d/a/a/b;->b:[Ljava/lang/String;

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/google/c/e/b/d/a/a/b;->b:[Ljava/lang/String;

    array-length v2, v2

    if-lez v2, :cond_4

    move v2, v1

    move v3, v1

    move v4, v1

    .line 300
    :goto_2
    iget-object v5, p0, Lcom/google/c/e/b/d/a/a/b;->b:[Ljava/lang/String;

    array-length v5, v5

    if-ge v2, v5, :cond_3

    .line 301
    iget-object v5, p0, Lcom/google/c/e/b/d/a/a/b;->b:[Ljava/lang/String;

    aget-object v5, v5, v2

    .line 302
    if-eqz v5, :cond_2

    .line 303
    add-int/lit8 v4, v4, 0x1

    .line 304
    invoke-static {v5}, Lcom/google/protobuf/nano/b;->a(Ljava/lang/String;)I

    move-result v5

    add-int/2addr v3, v5

    .line 300
    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 308
    :cond_3
    add-int/2addr v0, v3

    .line 309
    mul-int/lit8 v2, v4, 0x1

    add-int/2addr v0, v2

    .line 311
    :cond_4
    iget-object v2, p0, Lcom/google/c/e/b/d/a/a/b;->c:[Lcom/google/c/e/b/d/a/a/c;

    if-eqz v2, :cond_6

    iget-object v2, p0, Lcom/google/c/e/b/d/a/a/b;->c:[Lcom/google/c/e/b/d/a/a/c;

    array-length v2, v2

    if-lez v2, :cond_6

    .line 312
    :goto_3
    iget-object v2, p0, Lcom/google/c/e/b/d/a/a/b;->c:[Lcom/google/c/e/b/d/a/a/c;

    array-length v2, v2

    if-ge v1, v2, :cond_6

    .line 313
    iget-object v2, p0, Lcom/google/c/e/b/d/a/a/b;->c:[Lcom/google/c/e/b/d/a/a/c;

    aget-object v2, v2, v1

    .line 314
    if-eqz v2, :cond_5

    .line 315
    const/4 v3, 0x3

    invoke-static {v3, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v2

    add-int/2addr v0, v2

    .line 312
    :cond_5
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 320
    :cond_6
    return v0

    :cond_7
    move v0, v4

    goto :goto_1
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 96
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/c/e/b/d/a/a/b;->storeUnknownField(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    const/16 v0, 0xa

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->b(Lcom/google/protobuf/nano/a;I)I

    move-result v2

    iget-object v0, p0, Lcom/google/c/e/b/d/a/a/b;->a:[Ljava/lang/String;

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v3, p0, Lcom/google/c/e/b/d/a/a/b;->a:[Ljava/lang/String;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_1
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_3

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    iget-object v0, p0, Lcom/google/c/e/b/d/a/a/b;->a:[Ljava/lang/String;

    array-length v0, v0

    goto :goto_1

    :cond_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    iput-object v2, p0, Lcom/google/c/e/b/d/a/a/b;->a:[Ljava/lang/String;

    goto :goto_0

    :sswitch_2
    const/16 v0, 0x12

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->b(Lcom/google/protobuf/nano/a;I)I

    move-result v2

    iget-object v0, p0, Lcom/google/c/e/b/d/a/a/b;->b:[Ljava/lang/String;

    if-nez v0, :cond_5

    move v0, v1

    :goto_3
    add-int/2addr v2, v0

    new-array v2, v2, [Ljava/lang/String;

    if-eqz v0, :cond_4

    iget-object v3, p0, Lcom/google/c/e/b/d/a/a/b;->b:[Ljava/lang/String;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_4
    :goto_4
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_6

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_5
    iget-object v0, p0, Lcom/google/c/e/b/d/a/a/b;->b:[Ljava/lang/String;

    array-length v0, v0

    goto :goto_3

    :cond_6
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    iput-object v2, p0, Lcom/google/c/e/b/d/a/a/b;->b:[Ljava/lang/String;

    goto :goto_0

    :sswitch_3
    const/16 v0, 0x1a

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->b(Lcom/google/protobuf/nano/a;I)I

    move-result v2

    iget-object v0, p0, Lcom/google/c/e/b/d/a/a/b;->c:[Lcom/google/c/e/b/d/a/a/c;

    if-nez v0, :cond_8

    move v0, v1

    :goto_5
    add-int/2addr v2, v0

    new-array v2, v2, [Lcom/google/c/e/b/d/a/a/c;

    if-eqz v0, :cond_7

    iget-object v3, p0, Lcom/google/c/e/b/d/a/a/b;->c:[Lcom/google/c/e/b/d/a/a/c;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_7
    :goto_6
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_9

    new-instance v3, Lcom/google/c/e/b/d/a/a/c;

    invoke-direct {v3}, Lcom/google/c/e/b/d/a/a/c;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_6

    :cond_8
    iget-object v0, p0, Lcom/google/c/e/b/d/a/a/b;->c:[Lcom/google/c/e/b/d/a/a/c;

    array-length v0, v0

    goto :goto_5

    :cond_9
    new-instance v3, Lcom/google/c/e/b/d/a/a/c;

    invoke-direct {v3}, Lcom/google/c/e/b/d/a/a/c;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    iput-object v2, p0, Lcom/google/c/e/b/d/a/a/b;->c:[Lcom/google/c/e/b/d/a/a/c;

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 253
    iget-object v0, p0, Lcom/google/c/e/b/d/a/a/b;->a:[Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/c/e/b/d/a/a/b;->a:[Ljava/lang/String;

    array-length v0, v0

    if-lez v0, :cond_1

    move v0, v1

    .line 254
    :goto_0
    iget-object v2, p0, Lcom/google/c/e/b/d/a/a/b;->a:[Ljava/lang/String;

    array-length v2, v2

    if-ge v0, v2, :cond_1

    .line 255
    iget-object v2, p0, Lcom/google/c/e/b/d/a/a/b;->a:[Ljava/lang/String;

    aget-object v2, v2, v0

    .line 256
    if-eqz v2, :cond_0

    .line 257
    const/4 v3, 0x1

    invoke-virtual {p1, v3, v2}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 254
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 261
    :cond_1
    iget-object v0, p0, Lcom/google/c/e/b/d/a/a/b;->b:[Ljava/lang/String;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/c/e/b/d/a/a/b;->b:[Ljava/lang/String;

    array-length v0, v0

    if-lez v0, :cond_3

    move v0, v1

    .line 262
    :goto_1
    iget-object v2, p0, Lcom/google/c/e/b/d/a/a/b;->b:[Ljava/lang/String;

    array-length v2, v2

    if-ge v0, v2, :cond_3

    .line 263
    iget-object v2, p0, Lcom/google/c/e/b/d/a/a/b;->b:[Ljava/lang/String;

    aget-object v2, v2, v0

    .line 264
    if-eqz v2, :cond_2

    .line 265
    const/4 v3, 0x2

    invoke-virtual {p1, v3, v2}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 262
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 269
    :cond_3
    iget-object v0, p0, Lcom/google/c/e/b/d/a/a/b;->c:[Lcom/google/c/e/b/d/a/a/c;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/google/c/e/b/d/a/a/b;->c:[Lcom/google/c/e/b/d/a/a/c;

    array-length v0, v0

    if-lez v0, :cond_5

    .line 270
    :goto_2
    iget-object v0, p0, Lcom/google/c/e/b/d/a/a/b;->c:[Lcom/google/c/e/b/d/a/a/c;

    array-length v0, v0

    if-ge v1, v0, :cond_5

    .line 271
    iget-object v0, p0, Lcom/google/c/e/b/d/a/a/b;->c:[Lcom/google/c/e/b/d/a/a/c;

    aget-object v0, v0, v1

    .line 272
    if-eqz v0, :cond_4

    .line 273
    const/4 v2, 0x3

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 270
    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 277
    :cond_5
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/d;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 278
    return-void
.end method
