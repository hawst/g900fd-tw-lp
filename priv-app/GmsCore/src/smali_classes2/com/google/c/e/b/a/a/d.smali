.class public final Lcom/google/c/e/b/a/a/d;
.super Lcom/google/protobuf/nano/d;
.source "SourceFile"


# instance fields
.field public a:Lcom/google/ac/c/a/a/a/q;

.field public apiHeader:Lcom/google/c/e/b/a/a/b;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 321
    invoke-direct {p0}, Lcom/google/protobuf/nano/d;-><init>()V

    .line 322
    iput-object v0, p0, Lcom/google/c/e/b/a/a/d;->apiHeader:Lcom/google/c/e/b/a/a/b;

    iput-object v0, p0, Lcom/google/c/e/b/a/a/d;->a:Lcom/google/ac/c/a/a/a/q;

    iput-object v0, p0, Lcom/google/c/e/b/a/a/d;->unknownFieldData:Lcom/google/protobuf/nano/f;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/c/e/b/a/a/d;->cachedSize:I

    .line 323
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 347
    invoke-super {p0}, Lcom/google/protobuf/nano/d;->computeSerializedSize()I

    move-result v0

    .line 348
    iget-object v1, p0, Lcom/google/c/e/b/a/a/d;->apiHeader:Lcom/google/c/e/b/a/a/b;

    if-eqz v1, :cond_0

    .line 349
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/c/e/b/a/a/d;->apiHeader:Lcom/google/c/e/b/a/a/b;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 352
    :cond_0
    iget-object v1, p0, Lcom/google/c/e/b/a/a/d;->a:Lcom/google/ac/c/a/a/a/q;

    if-eqz v1, :cond_1

    .line 353
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/c/e/b/a/a/d;->a:Lcom/google/ac/c/a/a/a/q;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 356
    :cond_1
    return v0
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 298
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/c/e/b/a/a/d;->storeUnknownField(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Lcom/google/c/e/b/a/a/d;->apiHeader:Lcom/google/c/e/b/a/a/b;

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/c/e/b/a/a/b;

    invoke-direct {v0}, Lcom/google/c/e/b/a/a/b;-><init>()V

    iput-object v0, p0, Lcom/google/c/e/b/a/a/d;->apiHeader:Lcom/google/c/e/b/a/a/b;

    :cond_1
    iget-object v0, p0, Lcom/google/c/e/b/a/a/d;->apiHeader:Lcom/google/c/e/b/a/a/b;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Lcom/google/c/e/b/a/a/d;->a:Lcom/google/ac/c/a/a/a/q;

    if-nez v0, :cond_2

    new-instance v0, Lcom/google/ac/c/a/a/a/q;

    invoke-direct {v0}, Lcom/google/ac/c/a/a/a/q;-><init>()V

    iput-object v0, p0, Lcom/google/c/e/b/a/a/d;->a:Lcom/google/ac/c/a/a/a/q;

    :cond_2
    iget-object v0, p0, Lcom/google/c/e/b/a/a/d;->a:Lcom/google/ac/c/a/a/a/q;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 336
    iget-object v0, p0, Lcom/google/c/e/b/a/a/d;->apiHeader:Lcom/google/c/e/b/a/a/b;

    if-eqz v0, :cond_0

    .line 337
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/c/e/b/a/a/d;->apiHeader:Lcom/google/c/e/b/a/a/b;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 339
    :cond_0
    iget-object v0, p0, Lcom/google/c/e/b/a/a/d;->a:Lcom/google/ac/c/a/a/a/q;

    if-eqz v0, :cond_1

    .line 340
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/c/e/b/a/a/d;->a:Lcom/google/ac/c/a/a/a/q;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 342
    :cond_1
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/d;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 343
    return-void
.end method
