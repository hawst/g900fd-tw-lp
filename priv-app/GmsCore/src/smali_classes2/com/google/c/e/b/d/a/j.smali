.class public final Lcom/google/c/e/b/d/a/j;
.super Lcom/google/protobuf/nano/d;
.source "SourceFile"


# instance fields
.field public a:Ljava/lang/String;

.field public b:[Ljava/lang/String;

.field public c:[Lcom/google/c/e/b/d/a/k;

.field public d:Ljava/lang/Boolean;

.field public e:Ljava/lang/Integer;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 283
    invoke-direct {p0}, Lcom/google/protobuf/nano/d;-><init>()V

    .line 284
    iput-object v1, p0, Lcom/google/c/e/b/d/a/j;->a:Ljava/lang/String;

    sget-object v0, Lcom/google/protobuf/nano/m;->f:[Ljava/lang/String;

    iput-object v0, p0, Lcom/google/c/e/b/d/a/j;->b:[Ljava/lang/String;

    invoke-static {}, Lcom/google/c/e/b/d/a/k;->a()[Lcom/google/c/e/b/d/a/k;

    move-result-object v0

    iput-object v0, p0, Lcom/google/c/e/b/d/a/j;->c:[Lcom/google/c/e/b/d/a/k;

    iput-object v1, p0, Lcom/google/c/e/b/d/a/j;->d:Ljava/lang/Boolean;

    iput-object v1, p0, Lcom/google/c/e/b/d/a/j;->e:Ljava/lang/Integer;

    iput-object v1, p0, Lcom/google/c/e/b/d/a/j;->unknownFieldData:Lcom/google/protobuf/nano/f;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/c/e/b/d/a/j;->cachedSize:I

    .line 285
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 329
    invoke-super {p0}, Lcom/google/protobuf/nano/d;->computeSerializedSize()I

    move-result v0

    .line 330
    const/4 v2, 0x1

    iget-object v3, p0, Lcom/google/c/e/b/d/a/j;->a:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v2

    add-int v4, v0, v2

    .line 332
    iget-object v0, p0, Lcom/google/c/e/b/d/a/j;->b:[Ljava/lang/String;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/google/c/e/b/d/a/j;->b:[Ljava/lang/String;

    array-length v0, v0

    if-lez v0, :cond_6

    move v0, v1

    move v2, v1

    move v3, v1

    .line 335
    :goto_0
    iget-object v5, p0, Lcom/google/c/e/b/d/a/j;->b:[Ljava/lang/String;

    array-length v5, v5

    if-ge v0, v5, :cond_1

    .line 336
    iget-object v5, p0, Lcom/google/c/e/b/d/a/j;->b:[Ljava/lang/String;

    aget-object v5, v5, v0

    .line 337
    if-eqz v5, :cond_0

    .line 338
    add-int/lit8 v3, v3, 0x1

    .line 339
    invoke-static {v5}, Lcom/google/protobuf/nano/b;->a(Ljava/lang/String;)I

    move-result v5

    add-int/2addr v2, v5

    .line 335
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 343
    :cond_1
    add-int v0, v4, v2

    .line 344
    mul-int/lit8 v2, v3, 0x1

    add-int/2addr v0, v2

    .line 346
    :goto_1
    iget-object v2, p0, Lcom/google/c/e/b/d/a/j;->d:Ljava/lang/Boolean;

    if-eqz v2, :cond_2

    .line 347
    const/4 v2, 0x3

    iget-object v3, p0, Lcom/google/c/e/b/d/a/j;->d:Ljava/lang/Boolean;

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v2}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 350
    :cond_2
    iget-object v2, p0, Lcom/google/c/e/b/d/a/j;->e:Ljava/lang/Integer;

    if-eqz v2, :cond_3

    .line 351
    const/4 v2, 0x4

    iget-object v3, p0, Lcom/google/c/e/b/d/a/j;->e:Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 354
    :cond_3
    iget-object v2, p0, Lcom/google/c/e/b/d/a/j;->c:[Lcom/google/c/e/b/d/a/k;

    if-eqz v2, :cond_5

    iget-object v2, p0, Lcom/google/c/e/b/d/a/j;->c:[Lcom/google/c/e/b/d/a/k;

    array-length v2, v2

    if-lez v2, :cond_5

    .line 355
    :goto_2
    iget-object v2, p0, Lcom/google/c/e/b/d/a/j;->c:[Lcom/google/c/e/b/d/a/k;

    array-length v2, v2

    if-ge v1, v2, :cond_5

    .line 356
    iget-object v2, p0, Lcom/google/c/e/b/d/a/j;->c:[Lcom/google/c/e/b/d/a/k;

    aget-object v2, v2, v1

    .line 357
    if-eqz v2, :cond_4

    .line 358
    const/4 v3, 0x5

    invoke-static {v3, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v2

    add-int/2addr v0, v2

    .line 355
    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 363
    :cond_5
    return v0

    :cond_6
    move v0, v4

    goto :goto_1
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 129
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/c/e/b/d/a/j;->storeUnknownField(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/c/e/b/d/a/j;->a:Ljava/lang/String;

    goto :goto_0

    :sswitch_2
    const/16 v0, 0x12

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->b(Lcom/google/protobuf/nano/a;I)I

    move-result v2

    iget-object v0, p0, Lcom/google/c/e/b/d/a/j;->b:[Ljava/lang/String;

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v3, p0, Lcom/google/c/e/b/d/a/j;->b:[Ljava/lang/String;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_1
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_3

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    iget-object v0, p0, Lcom/google/c/e/b/d/a/j;->b:[Ljava/lang/String;

    array-length v0, v0

    goto :goto_1

    :cond_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    iput-object v2, p0, Lcom/google/c/e/b/d/a/j;->b:[Ljava/lang/String;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/c/e/b/d/a/j;->d:Ljava/lang/Boolean;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/c/e/b/d/a/j;->e:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_5
    const/16 v0, 0x2a

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->b(Lcom/google/protobuf/nano/a;I)I

    move-result v2

    iget-object v0, p0, Lcom/google/c/e/b/d/a/j;->c:[Lcom/google/c/e/b/d/a/k;

    if-nez v0, :cond_5

    move v0, v1

    :goto_3
    add-int/2addr v2, v0

    new-array v2, v2, [Lcom/google/c/e/b/d/a/k;

    if-eqz v0, :cond_4

    iget-object v3, p0, Lcom/google/c/e/b/d/a/j;->c:[Lcom/google/c/e/b/d/a/k;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_4
    :goto_4
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_6

    new-instance v3, Lcom/google/c/e/b/d/a/k;

    invoke-direct {v3}, Lcom/google/c/e/b/d/a/k;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_5
    iget-object v0, p0, Lcom/google/c/e/b/d/a/j;->c:[Lcom/google/c/e/b/d/a/k;

    array-length v0, v0

    goto :goto_3

    :cond_6
    new-instance v3, Lcom/google/c/e/b/d/a/k;

    invoke-direct {v3}, Lcom/google/c/e/b/d/a/k;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    iput-object v2, p0, Lcom/google/c/e/b/d/a/j;->c:[Lcom/google/c/e/b/d/a/k;

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x2a -> :sswitch_5
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 301
    const/4 v0, 0x1

    iget-object v2, p0, Lcom/google/c/e/b/d/a/j;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v2}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 302
    iget-object v0, p0, Lcom/google/c/e/b/d/a/j;->b:[Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/c/e/b/d/a/j;->b:[Ljava/lang/String;

    array-length v0, v0

    if-lez v0, :cond_1

    move v0, v1

    .line 303
    :goto_0
    iget-object v2, p0, Lcom/google/c/e/b/d/a/j;->b:[Ljava/lang/String;

    array-length v2, v2

    if-ge v0, v2, :cond_1

    .line 304
    iget-object v2, p0, Lcom/google/c/e/b/d/a/j;->b:[Ljava/lang/String;

    aget-object v2, v2, v0

    .line 305
    if-eqz v2, :cond_0

    .line 306
    const/4 v3, 0x2

    invoke-virtual {p1, v3, v2}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 303
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 310
    :cond_1
    iget-object v0, p0, Lcom/google/c/e/b/d/a/j;->d:Ljava/lang/Boolean;

    if-eqz v0, :cond_2

    .line 311
    const/4 v0, 0x3

    iget-object v2, p0, Lcom/google/c/e/b/d/a/j;->d:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-virtual {p1, v0, v2}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 313
    :cond_2
    iget-object v0, p0, Lcom/google/c/e/b/d/a/j;->e:Ljava/lang/Integer;

    if-eqz v0, :cond_3

    .line 314
    const/4 v0, 0x4

    iget-object v2, p0, Lcom/google/c/e/b/d/a/j;->e:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {p1, v0, v2}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 316
    :cond_3
    iget-object v0, p0, Lcom/google/c/e/b/d/a/j;->c:[Lcom/google/c/e/b/d/a/k;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/google/c/e/b/d/a/j;->c:[Lcom/google/c/e/b/d/a/k;

    array-length v0, v0

    if-lez v0, :cond_5

    .line 317
    :goto_1
    iget-object v0, p0, Lcom/google/c/e/b/d/a/j;->c:[Lcom/google/c/e/b/d/a/k;

    array-length v0, v0

    if-ge v1, v0, :cond_5

    .line 318
    iget-object v0, p0, Lcom/google/c/e/b/d/a/j;->c:[Lcom/google/c/e/b/d/a/k;

    aget-object v0, v0, v1

    .line 319
    if-eqz v0, :cond_4

    .line 320
    const/4 v2, 0x5

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 317
    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 324
    :cond_5
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/d;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 325
    return-void
.end method
