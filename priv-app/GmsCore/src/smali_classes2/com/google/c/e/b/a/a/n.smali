.class public final Lcom/google/c/e/b/a/a/n;
.super Lcom/google/protobuf/nano/d;
.source "SourceFile"


# instance fields
.field public a:Lcom/google/ac/d/a/h;

.field public b:Lcom/google/ac/d/a/j;

.field public c:Ljava/lang/String;

.field public d:Lf/a/a/e;

.field public e:Lcom/google/c/d/a/a/b;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 173
    invoke-direct {p0}, Lcom/google/protobuf/nano/d;-><init>()V

    .line 174
    iput-object v0, p0, Lcom/google/c/e/b/a/a/n;->a:Lcom/google/ac/d/a/h;

    iput-object v0, p0, Lcom/google/c/e/b/a/a/n;->b:Lcom/google/ac/d/a/j;

    iput-object v0, p0, Lcom/google/c/e/b/a/a/n;->c:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/c/e/b/a/a/n;->d:Lf/a/a/e;

    iput-object v0, p0, Lcom/google/c/e/b/a/a/n;->e:Lcom/google/c/d/a/a/b;

    iput-object v0, p0, Lcom/google/c/e/b/a/a/n;->unknownFieldData:Lcom/google/protobuf/nano/f;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/c/e/b/a/a/n;->cachedSize:I

    .line 175
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 211
    invoke-super {p0}, Lcom/google/protobuf/nano/d;->computeSerializedSize()I

    move-result v0

    .line 212
    iget-object v1, p0, Lcom/google/c/e/b/a/a/n;->a:Lcom/google/ac/d/a/h;

    if-eqz v1, :cond_0

    .line 213
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/c/e/b/a/a/n;->a:Lcom/google/ac/d/a/h;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 216
    :cond_0
    iget-object v1, p0, Lcom/google/c/e/b/a/a/n;->b:Lcom/google/ac/d/a/j;

    if-eqz v1, :cond_1

    .line 217
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/c/e/b/a/a/n;->b:Lcom/google/ac/d/a/j;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 220
    :cond_1
    iget-object v1, p0, Lcom/google/c/e/b/a/a/n;->c:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 221
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/c/e/b/a/a/n;->c:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 224
    :cond_2
    iget-object v1, p0, Lcom/google/c/e/b/a/a/n;->d:Lf/a/a/e;

    if-eqz v1, :cond_3

    .line 225
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/c/e/b/a/a/n;->d:Lf/a/a/e;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 228
    :cond_3
    iget-object v1, p0, Lcom/google/c/e/b/a/a/n;->e:Lcom/google/c/d/a/a/b;

    if-eqz v1, :cond_4

    .line 229
    const/4 v1, 0x5

    iget-object v2, p0, Lcom/google/c/e/b/a/a/n;->e:Lcom/google/c/d/a/a/b;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 232
    :cond_4
    return v0
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 141
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/c/e/b/a/a/n;->storeUnknownField(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Lcom/google/c/e/b/a/a/n;->a:Lcom/google/ac/d/a/h;

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/ac/d/a/h;

    invoke-direct {v0}, Lcom/google/ac/d/a/h;-><init>()V

    iput-object v0, p0, Lcom/google/c/e/b/a/a/n;->a:Lcom/google/ac/d/a/h;

    :cond_1
    iget-object v0, p0, Lcom/google/c/e/b/a/a/n;->a:Lcom/google/ac/d/a/h;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Lcom/google/c/e/b/a/a/n;->b:Lcom/google/ac/d/a/j;

    if-nez v0, :cond_2

    new-instance v0, Lcom/google/ac/d/a/j;

    invoke-direct {v0}, Lcom/google/ac/d/a/j;-><init>()V

    iput-object v0, p0, Lcom/google/c/e/b/a/a/n;->b:Lcom/google/ac/d/a/j;

    :cond_2
    iget-object v0, p0, Lcom/google/c/e/b/a/a/n;->b:Lcom/google/ac/d/a/j;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/c/e/b/a/a/n;->c:Ljava/lang/String;

    goto :goto_0

    :sswitch_4
    iget-object v0, p0, Lcom/google/c/e/b/a/a/n;->d:Lf/a/a/e;

    if-nez v0, :cond_3

    new-instance v0, Lf/a/a/e;

    invoke-direct {v0}, Lf/a/a/e;-><init>()V

    iput-object v0, p0, Lcom/google/c/e/b/a/a/n;->d:Lf/a/a/e;

    :cond_3
    iget-object v0, p0, Lcom/google/c/e/b/a/a/n;->d:Lf/a/a/e;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_5
    iget-object v0, p0, Lcom/google/c/e/b/a/a/n;->e:Lcom/google/c/d/a/a/b;

    if-nez v0, :cond_4

    new-instance v0, Lcom/google/c/d/a/a/b;

    invoke-direct {v0}, Lcom/google/c/d/a/a/b;-><init>()V

    iput-object v0, p0, Lcom/google/c/e/b/a/a/n;->e:Lcom/google/c/d/a/a/b;

    :cond_4
    iget-object v0, p0, Lcom/google/c/e/b/a/a/n;->e:Lcom/google/c/d/a/a/b;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 191
    iget-object v0, p0, Lcom/google/c/e/b/a/a/n;->a:Lcom/google/ac/d/a/h;

    if-eqz v0, :cond_0

    .line 192
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/c/e/b/a/a/n;->a:Lcom/google/ac/d/a/h;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 194
    :cond_0
    iget-object v0, p0, Lcom/google/c/e/b/a/a/n;->b:Lcom/google/ac/d/a/j;

    if-eqz v0, :cond_1

    .line 195
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/c/e/b/a/a/n;->b:Lcom/google/ac/d/a/j;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 197
    :cond_1
    iget-object v0, p0, Lcom/google/c/e/b/a/a/n;->c:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 198
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/c/e/b/a/a/n;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 200
    :cond_2
    iget-object v0, p0, Lcom/google/c/e/b/a/a/n;->d:Lf/a/a/e;

    if-eqz v0, :cond_3

    .line 201
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/c/e/b/a/a/n;->d:Lf/a/a/e;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 203
    :cond_3
    iget-object v0, p0, Lcom/google/c/e/b/a/a/n;->e:Lcom/google/c/d/a/a/b;

    if-eqz v0, :cond_4

    .line 204
    const/4 v0, 0x5

    iget-object v1, p0, Lcom/google/c/e/b/a/a/n;->e:Lcom/google/c/d/a/a/b;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 206
    :cond_4
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/d;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 207
    return-void
.end method
