.class public final Lcom/google/c/e/b/d/a/f;
.super Lcom/google/protobuf/nano/d;
.source "SourceFile"


# instance fields
.field public a:Lcom/google/c/f/a/a/e;

.field public b:Ljava/lang/Boolean;

.field public c:Lcom/google/c/f/a/a/g;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 477
    invoke-direct {p0}, Lcom/google/protobuf/nano/d;-><init>()V

    .line 478
    iput-object v0, p0, Lcom/google/c/e/b/d/a/f;->a:Lcom/google/c/f/a/a/e;

    iput-object v0, p0, Lcom/google/c/e/b/d/a/f;->b:Ljava/lang/Boolean;

    iput-object v0, p0, Lcom/google/c/e/b/d/a/f;->c:Lcom/google/c/f/a/a/g;

    iput-object v0, p0, Lcom/google/c/e/b/d/a/f;->unknownFieldData:Lcom/google/protobuf/nano/f;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/c/e/b/d/a/f;->cachedSize:I

    .line 479
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 507
    invoke-super {p0}, Lcom/google/protobuf/nano/d;->computeSerializedSize()I

    move-result v0

    .line 508
    iget-object v1, p0, Lcom/google/c/e/b/d/a/f;->a:Lcom/google/c/f/a/a/e;

    if-eqz v1, :cond_0

    .line 509
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/c/e/b/d/a/f;->a:Lcom/google/c/f/a/a/e;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 512
    :cond_0
    iget-object v1, p0, Lcom/google/c/e/b/d/a/f;->b:Ljava/lang/Boolean;

    if-eqz v1, :cond_1

    .line 513
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/c/e/b/d/a/f;->b:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 516
    :cond_1
    iget-object v1, p0, Lcom/google/c/e/b/d/a/f;->c:Lcom/google/c/f/a/a/g;

    if-eqz v1, :cond_2

    .line 517
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/c/e/b/d/a/f;->c:Lcom/google/c/f/a/a/g;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 520
    :cond_2
    return v0
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 451
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/c/e/b/d/a/f;->storeUnknownField(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Lcom/google/c/e/b/d/a/f;->a:Lcom/google/c/f/a/a/e;

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/c/f/a/a/e;

    invoke-direct {v0}, Lcom/google/c/f/a/a/e;-><init>()V

    iput-object v0, p0, Lcom/google/c/e/b/d/a/f;->a:Lcom/google/c/f/a/a/e;

    :cond_1
    iget-object v0, p0, Lcom/google/c/e/b/d/a/f;->a:Lcom/google/c/f/a/a/e;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/c/e/b/d/a/f;->b:Ljava/lang/Boolean;

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Lcom/google/c/e/b/d/a/f;->c:Lcom/google/c/f/a/a/g;

    if-nez v0, :cond_2

    new-instance v0, Lcom/google/c/f/a/a/g;

    invoke-direct {v0}, Lcom/google/c/f/a/a/g;-><init>()V

    iput-object v0, p0, Lcom/google/c/e/b/d/a/f;->c:Lcom/google/c/f/a/a/g;

    :cond_2
    iget-object v0, p0, Lcom/google/c/e/b/d/a/f;->c:Lcom/google/c/f/a/a/g;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x22 -> :sswitch_3
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 493
    iget-object v0, p0, Lcom/google/c/e/b/d/a/f;->a:Lcom/google/c/f/a/a/e;

    if-eqz v0, :cond_0

    .line 494
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/c/e/b/d/a/f;->a:Lcom/google/c/f/a/a/e;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 496
    :cond_0
    iget-object v0, p0, Lcom/google/c/e/b/d/a/f;->b:Ljava/lang/Boolean;

    if-eqz v0, :cond_1

    .line 497
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/c/e/b/d/a/f;->b:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 499
    :cond_1
    iget-object v0, p0, Lcom/google/c/e/b/d/a/f;->c:Lcom/google/c/f/a/a/g;

    if-eqz v0, :cond_2

    .line 500
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/c/e/b/d/a/f;->c:Lcom/google/c/f/a/a/g;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 502
    :cond_2
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/d;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 503
    return-void
.end method
