.class public final Lcom/google/c/e/b/a/a/o;
.super Lcom/google/protobuf/nano/d;
.source "SourceFile"


# instance fields
.field public a:Lcom/google/c/e/b/d/a/j;

.field public apiHeader:Lcom/google/c/e/b/a/a/m;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1169
    invoke-direct {p0}, Lcom/google/protobuf/nano/d;-><init>()V

    .line 1170
    iput-object v0, p0, Lcom/google/c/e/b/a/a/o;->apiHeader:Lcom/google/c/e/b/a/a/m;

    iput-object v0, p0, Lcom/google/c/e/b/a/a/o;->a:Lcom/google/c/e/b/d/a/j;

    iput-object v0, p0, Lcom/google/c/e/b/a/a/o;->unknownFieldData:Lcom/google/protobuf/nano/f;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/c/e/b/a/a/o;->cachedSize:I

    .line 1171
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 1195
    invoke-super {p0}, Lcom/google/protobuf/nano/d;->computeSerializedSize()I

    move-result v0

    .line 1196
    iget-object v1, p0, Lcom/google/c/e/b/a/a/o;->apiHeader:Lcom/google/c/e/b/a/a/m;

    if-eqz v1, :cond_0

    .line 1197
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/c/e/b/a/a/o;->apiHeader:Lcom/google/c/e/b/a/a/m;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1200
    :cond_0
    iget-object v1, p0, Lcom/google/c/e/b/a/a/o;->a:Lcom/google/c/e/b/d/a/j;

    if-eqz v1, :cond_1

    .line 1201
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/c/e/b/a/a/o;->a:Lcom/google/c/e/b/d/a/j;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1204
    :cond_1
    return v0
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 1146
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/c/e/b/a/a/o;->storeUnknownField(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Lcom/google/c/e/b/a/a/o;->apiHeader:Lcom/google/c/e/b/a/a/m;

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/c/e/b/a/a/m;

    invoke-direct {v0}, Lcom/google/c/e/b/a/a/m;-><init>()V

    iput-object v0, p0, Lcom/google/c/e/b/a/a/o;->apiHeader:Lcom/google/c/e/b/a/a/m;

    :cond_1
    iget-object v0, p0, Lcom/google/c/e/b/a/a/o;->apiHeader:Lcom/google/c/e/b/a/a/m;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Lcom/google/c/e/b/a/a/o;->a:Lcom/google/c/e/b/d/a/j;

    if-nez v0, :cond_2

    new-instance v0, Lcom/google/c/e/b/d/a/j;

    invoke-direct {v0}, Lcom/google/c/e/b/d/a/j;-><init>()V

    iput-object v0, p0, Lcom/google/c/e/b/a/a/o;->a:Lcom/google/c/e/b/d/a/j;

    :cond_2
    iget-object v0, p0, Lcom/google/c/e/b/a/a/o;->a:Lcom/google/c/e/b/d/a/j;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 1184
    iget-object v0, p0, Lcom/google/c/e/b/a/a/o;->apiHeader:Lcom/google/c/e/b/a/a/m;

    if-eqz v0, :cond_0

    .line 1185
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/c/e/b/a/a/o;->apiHeader:Lcom/google/c/e/b/a/a/m;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 1187
    :cond_0
    iget-object v0, p0, Lcom/google/c/e/b/a/a/o;->a:Lcom/google/c/e/b/d/a/j;

    if-eqz v0, :cond_1

    .line 1188
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/c/e/b/a/a/o;->a:Lcom/google/c/e/b/d/a/j;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 1190
    :cond_1
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/d;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 1191
    return-void
.end method
