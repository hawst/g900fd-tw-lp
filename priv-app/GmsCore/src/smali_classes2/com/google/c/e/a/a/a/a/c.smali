.class public final Lcom/google/c/e/a/a/a/a/c;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# static fields
.field private static volatile g:[Lcom/google/c/e/a/a/a/a/c;


# instance fields
.field public a:Lcom/google/c/e/a/a/a/a/d;

.field public b:Lcom/google/c/e/a/a/a/a/b;

.field public c:Z

.field public d:Lcom/google/c/e/a/a/a/a/f;

.field public e:Lcom/google/c/e/a/a/a/a/g;

.field public f:Lcom/google/c/e/a/a/a/a/e;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 41
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 42
    iput-object v1, p0, Lcom/google/c/e/a/a/a/a/c;->a:Lcom/google/c/e/a/a/a/a/d;

    iput-object v1, p0, Lcom/google/c/e/a/a/a/a/c;->b:Lcom/google/c/e/a/a/a/a/b;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/c/e/a/a/a/a/c;->c:Z

    iput-object v1, p0, Lcom/google/c/e/a/a/a/a/c;->d:Lcom/google/c/e/a/a/a/a/f;

    iput-object v1, p0, Lcom/google/c/e/a/a/a/a/c;->e:Lcom/google/c/e/a/a/a/a/g;

    iput-object v1, p0, Lcom/google/c/e/a/a/a/a/c;->f:Lcom/google/c/e/a/a/a/a/e;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/c/e/a/a/a/a/c;->cachedSize:I

    .line 43
    return-void
.end method

.method public static a()[Lcom/google/c/e/a/a/a/a/c;
    .locals 2

    .prologue
    .line 12
    sget-object v0, Lcom/google/c/e/a/a/a/a/c;->g:[Lcom/google/c/e/a/a/a/a/c;

    if-nez v0, :cond_1

    .line 13
    sget-object v1, Lcom/google/protobuf/nano/h;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 15
    :try_start_0
    sget-object v0, Lcom/google/c/e/a/a/a/a/c;->g:[Lcom/google/c/e/a/a/a/a/c;

    if-nez v0, :cond_0

    .line 16
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/c/e/a/a/a/a/c;

    sput-object v0, Lcom/google/c/e/a/a/a/a/c;->g:[Lcom/google/c/e/a/a/a/a/c;

    .line 18
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 20
    :cond_1
    sget-object v0, Lcom/google/c/e/a/a/a/a/c;->g:[Lcom/google/c/e/a/a/a/a/c;

    return-object v0

    .line 18
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 159
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->computeSerializedSize()I

    move-result v0

    .line 160
    iget-object v1, p0, Lcom/google/c/e/a/a/a/a/c;->a:Lcom/google/c/e/a/a/a/a/d;

    if-eqz v1, :cond_0

    .line 161
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/c/e/a/a/a/a/c;->a:Lcom/google/c/e/a/a/a/a/d;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 164
    :cond_0
    iget-object v1, p0, Lcom/google/c/e/a/a/a/a/c;->b:Lcom/google/c/e/a/a/a/a/b;

    if-eqz v1, :cond_1

    .line 165
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/c/e/a/a/a/a/c;->b:Lcom/google/c/e/a/a/a/a/b;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 168
    :cond_1
    iget-boolean v1, p0, Lcom/google/c/e/a/a/a/a/c;->c:Z

    if-eqz v1, :cond_2

    .line 169
    const/4 v1, 0x3

    iget-boolean v2, p0, Lcom/google/c/e/a/a/a/a/c;->c:Z

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 172
    :cond_2
    iget-object v1, p0, Lcom/google/c/e/a/a/a/a/c;->d:Lcom/google/c/e/a/a/a/a/f;

    if-eqz v1, :cond_3

    .line 173
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/c/e/a/a/a/a/c;->d:Lcom/google/c/e/a/a/a/a/f;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 176
    :cond_3
    iget-object v1, p0, Lcom/google/c/e/a/a/a/a/c;->e:Lcom/google/c/e/a/a/a/a/g;

    if-eqz v1, :cond_4

    .line 177
    const/4 v1, 0x5

    iget-object v2, p0, Lcom/google/c/e/a/a/a/a/c;->e:Lcom/google/c/e/a/a/a/a/g;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 180
    :cond_4
    iget-object v1, p0, Lcom/google/c/e/a/a/a/a/c;->f:Lcom/google/c/e/a/a/a/a/e;

    if-eqz v1, :cond_5

    .line 181
    const/4 v1, 0x6

    iget-object v2, p0, Lcom/google/c/e/a/a/a/a/c;->f:Lcom/google/c/e/a/a/a/a/e;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 184
    :cond_5
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 58
    if-ne p1, p0, :cond_1

    .line 113
    :cond_0
    :goto_0
    return v0

    .line 61
    :cond_1
    instance-of v2, p1, Lcom/google/c/e/a/a/a/a/c;

    if-nez v2, :cond_2

    move v0, v1

    .line 62
    goto :goto_0

    .line 64
    :cond_2
    check-cast p1, Lcom/google/c/e/a/a/a/a/c;

    .line 65
    iget-object v2, p0, Lcom/google/c/e/a/a/a/a/c;->a:Lcom/google/c/e/a/a/a/a/d;

    if-nez v2, :cond_3

    .line 66
    iget-object v2, p1, Lcom/google/c/e/a/a/a/a/c;->a:Lcom/google/c/e/a/a/a/a/d;

    if-eqz v2, :cond_4

    move v0, v1

    .line 67
    goto :goto_0

    .line 70
    :cond_3
    iget-object v2, p0, Lcom/google/c/e/a/a/a/a/c;->a:Lcom/google/c/e/a/a/a/a/d;

    iget-object v3, p1, Lcom/google/c/e/a/a/a/a/c;->a:Lcom/google/c/e/a/a/a/a/d;

    invoke-virtual {v2, v3}, Lcom/google/c/e/a/a/a/a/d;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    move v0, v1

    .line 71
    goto :goto_0

    .line 74
    :cond_4
    iget-object v2, p0, Lcom/google/c/e/a/a/a/a/c;->b:Lcom/google/c/e/a/a/a/a/b;

    if-nez v2, :cond_5

    .line 75
    iget-object v2, p1, Lcom/google/c/e/a/a/a/a/c;->b:Lcom/google/c/e/a/a/a/a/b;

    if-eqz v2, :cond_6

    move v0, v1

    .line 76
    goto :goto_0

    .line 79
    :cond_5
    iget-object v2, p0, Lcom/google/c/e/a/a/a/a/c;->b:Lcom/google/c/e/a/a/a/a/b;

    iget-object v3, p1, Lcom/google/c/e/a/a/a/a/c;->b:Lcom/google/c/e/a/a/a/a/b;

    invoke-virtual {v2, v3}, Lcom/google/c/e/a/a/a/a/b;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    move v0, v1

    .line 80
    goto :goto_0

    .line 83
    :cond_6
    iget-boolean v2, p0, Lcom/google/c/e/a/a/a/a/c;->c:Z

    iget-boolean v3, p1, Lcom/google/c/e/a/a/a/a/c;->c:Z

    if-eq v2, v3, :cond_7

    move v0, v1

    .line 84
    goto :goto_0

    .line 86
    :cond_7
    iget-object v2, p0, Lcom/google/c/e/a/a/a/a/c;->d:Lcom/google/c/e/a/a/a/a/f;

    if-nez v2, :cond_8

    .line 87
    iget-object v2, p1, Lcom/google/c/e/a/a/a/a/c;->d:Lcom/google/c/e/a/a/a/a/f;

    if-eqz v2, :cond_9

    move v0, v1

    .line 88
    goto :goto_0

    .line 91
    :cond_8
    iget-object v2, p0, Lcom/google/c/e/a/a/a/a/c;->d:Lcom/google/c/e/a/a/a/a/f;

    iget-object v3, p1, Lcom/google/c/e/a/a/a/a/c;->d:Lcom/google/c/e/a/a/a/a/f;

    invoke-virtual {v2, v3}, Lcom/google/c/e/a/a/a/a/f;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    move v0, v1

    .line 92
    goto :goto_0

    .line 95
    :cond_9
    iget-object v2, p0, Lcom/google/c/e/a/a/a/a/c;->e:Lcom/google/c/e/a/a/a/a/g;

    if-nez v2, :cond_a

    .line 96
    iget-object v2, p1, Lcom/google/c/e/a/a/a/a/c;->e:Lcom/google/c/e/a/a/a/a/g;

    if-eqz v2, :cond_b

    move v0, v1

    .line 97
    goto :goto_0

    .line 100
    :cond_a
    iget-object v2, p0, Lcom/google/c/e/a/a/a/a/c;->e:Lcom/google/c/e/a/a/a/a/g;

    iget-object v3, p1, Lcom/google/c/e/a/a/a/a/c;->e:Lcom/google/c/e/a/a/a/a/g;

    invoke-virtual {v2, v3}, Lcom/google/c/e/a/a/a/a/g;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_b

    move v0, v1

    .line 101
    goto :goto_0

    .line 104
    :cond_b
    iget-object v2, p0, Lcom/google/c/e/a/a/a/a/c;->f:Lcom/google/c/e/a/a/a/a/e;

    if-nez v2, :cond_c

    .line 105
    iget-object v2, p1, Lcom/google/c/e/a/a/a/a/c;->f:Lcom/google/c/e/a/a/a/a/e;

    if-eqz v2, :cond_0

    move v0, v1

    .line 106
    goto :goto_0

    .line 109
    :cond_c
    iget-object v2, p0, Lcom/google/c/e/a/a/a/a/c;->f:Lcom/google/c/e/a/a/a/a/e;

    iget-object v3, p1, Lcom/google/c/e/a/a/a/a/c;->f:Lcom/google/c/e/a/a/a/a/e;

    invoke-virtual {v2, v3}, Lcom/google/c/e/a/a/a/a/e;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    .line 110
    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 118
    iget-object v0, p0, Lcom/google/c/e/a/a/a/a/c;->a:Lcom/google/c/e/a/a/a/a/d;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    .line 121
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/c/e/a/a/a/a/c;->b:Lcom/google/c/e/a/a/a/a/b;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    .line 123
    mul-int/lit8 v2, v0, 0x1f

    iget-boolean v0, p0, Lcom/google/c/e/a/a/a/a/c;->c:Z

    if-eqz v0, :cond_2

    const/16 v0, 0x4cf

    :goto_2
    add-int/2addr v0, v2

    .line 124
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/c/e/a/a/a/a/c;->d:Lcom/google/c/e/a/a/a/a/f;

    if-nez v0, :cond_3

    move v0, v1

    :goto_3
    add-int/2addr v0, v2

    .line 126
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/c/e/a/a/a/a/c;->e:Lcom/google/c/e/a/a/a/a/g;

    if-nez v0, :cond_4

    move v0, v1

    :goto_4
    add-int/2addr v0, v2

    .line 128
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/c/e/a/a/a/a/c;->f:Lcom/google/c/e/a/a/a/a/e;

    if-nez v2, :cond_5

    :goto_5
    add-int/2addr v0, v1

    .line 130
    return v0

    .line 118
    :cond_0
    iget-object v0, p0, Lcom/google/c/e/a/a/a/a/c;->a:Lcom/google/c/e/a/a/a/a/d;

    invoke-virtual {v0}, Lcom/google/c/e/a/a/a/a/d;->hashCode()I

    move-result v0

    goto :goto_0

    .line 121
    :cond_1
    iget-object v0, p0, Lcom/google/c/e/a/a/a/a/c;->b:Lcom/google/c/e/a/a/a/a/b;

    invoke-virtual {v0}, Lcom/google/c/e/a/a/a/a/b;->hashCode()I

    move-result v0

    goto :goto_1

    .line 123
    :cond_2
    const/16 v0, 0x4d5

    goto :goto_2

    .line 124
    :cond_3
    iget-object v0, p0, Lcom/google/c/e/a/a/a/a/c;->d:Lcom/google/c/e/a/a/a/a/f;

    invoke-virtual {v0}, Lcom/google/c/e/a/a/a/a/f;->hashCode()I

    move-result v0

    goto :goto_3

    .line 126
    :cond_4
    iget-object v0, p0, Lcom/google/c/e/a/a/a/a/c;->e:Lcom/google/c/e/a/a/a/a/g;

    invoke-virtual {v0}, Lcom/google/c/e/a/a/a/a/g;->hashCode()I

    move-result v0

    goto :goto_4

    .line 128
    :cond_5
    iget-object v1, p0, Lcom/google/c/e/a/a/a/a/c;->f:Lcom/google/c/e/a/a/a/a/e;

    invoke-virtual {v1}, Lcom/google/c/e/a/a/a/a/e;->hashCode()I

    move-result v1

    goto :goto_5
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 5
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Lcom/google/c/e/a/a/a/a/c;->a:Lcom/google/c/e/a/a/a/a/d;

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/c/e/a/a/a/a/d;

    invoke-direct {v0}, Lcom/google/c/e/a/a/a/a/d;-><init>()V

    iput-object v0, p0, Lcom/google/c/e/a/a/a/a/c;->a:Lcom/google/c/e/a/a/a/a/d;

    :cond_1
    iget-object v0, p0, Lcom/google/c/e/a/a/a/a/c;->a:Lcom/google/c/e/a/a/a/a/d;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Lcom/google/c/e/a/a/a/a/c;->b:Lcom/google/c/e/a/a/a/a/b;

    if-nez v0, :cond_2

    new-instance v0, Lcom/google/c/e/a/a/a/a/b;

    invoke-direct {v0}, Lcom/google/c/e/a/a/a/a/b;-><init>()V

    iput-object v0, p0, Lcom/google/c/e/a/a/a/a/c;->b:Lcom/google/c/e/a/a/a/a/b;

    :cond_2
    iget-object v0, p0, Lcom/google/c/e/a/a/a/a/c;->b:Lcom/google/c/e/a/a/a/a/b;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/c/e/a/a/a/a/c;->c:Z

    goto :goto_0

    :sswitch_4
    iget-object v0, p0, Lcom/google/c/e/a/a/a/a/c;->d:Lcom/google/c/e/a/a/a/a/f;

    if-nez v0, :cond_3

    new-instance v0, Lcom/google/c/e/a/a/a/a/f;

    invoke-direct {v0}, Lcom/google/c/e/a/a/a/a/f;-><init>()V

    iput-object v0, p0, Lcom/google/c/e/a/a/a/a/c;->d:Lcom/google/c/e/a/a/a/a/f;

    :cond_3
    iget-object v0, p0, Lcom/google/c/e/a/a/a/a/c;->d:Lcom/google/c/e/a/a/a/a/f;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_5
    iget-object v0, p0, Lcom/google/c/e/a/a/a/a/c;->e:Lcom/google/c/e/a/a/a/a/g;

    if-nez v0, :cond_4

    new-instance v0, Lcom/google/c/e/a/a/a/a/g;

    invoke-direct {v0}, Lcom/google/c/e/a/a/a/a/g;-><init>()V

    iput-object v0, p0, Lcom/google/c/e/a/a/a/a/c;->e:Lcom/google/c/e/a/a/a/a/g;

    :cond_4
    iget-object v0, p0, Lcom/google/c/e/a/a/a/a/c;->e:Lcom/google/c/e/a/a/a/a/g;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_6
    iget-object v0, p0, Lcom/google/c/e/a/a/a/a/c;->f:Lcom/google/c/e/a/a/a/a/e;

    if-nez v0, :cond_5

    new-instance v0, Lcom/google/c/e/a/a/a/a/e;

    invoke-direct {v0}, Lcom/google/c/e/a/a/a/a/e;-><init>()V

    iput-object v0, p0, Lcom/google/c/e/a/a/a/a/c;->f:Lcom/google/c/e/a/a/a/a/e;

    :cond_5
    iget-object v0, p0, Lcom/google/c/e/a/a/a/a/c;->f:Lcom/google/c/e/a/a/a/a/e;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 136
    iget-object v0, p0, Lcom/google/c/e/a/a/a/a/c;->a:Lcom/google/c/e/a/a/a/a/d;

    if-eqz v0, :cond_0

    .line 137
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/c/e/a/a/a/a/c;->a:Lcom/google/c/e/a/a/a/a/d;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 139
    :cond_0
    iget-object v0, p0, Lcom/google/c/e/a/a/a/a/c;->b:Lcom/google/c/e/a/a/a/a/b;

    if-eqz v0, :cond_1

    .line 140
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/c/e/a/a/a/a/c;->b:Lcom/google/c/e/a/a/a/a/b;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 142
    :cond_1
    iget-boolean v0, p0, Lcom/google/c/e/a/a/a/a/c;->c:Z

    if-eqz v0, :cond_2

    .line 143
    const/4 v0, 0x3

    iget-boolean v1, p0, Lcom/google/c/e/a/a/a/a/c;->c:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 145
    :cond_2
    iget-object v0, p0, Lcom/google/c/e/a/a/a/a/c;->d:Lcom/google/c/e/a/a/a/a/f;

    if-eqz v0, :cond_3

    .line 146
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/c/e/a/a/a/a/c;->d:Lcom/google/c/e/a/a/a/a/f;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 148
    :cond_3
    iget-object v0, p0, Lcom/google/c/e/a/a/a/a/c;->e:Lcom/google/c/e/a/a/a/a/g;

    if-eqz v0, :cond_4

    .line 149
    const/4 v0, 0x5

    iget-object v1, p0, Lcom/google/c/e/a/a/a/a/c;->e:Lcom/google/c/e/a/a/a/a/g;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 151
    :cond_4
    iget-object v0, p0, Lcom/google/c/e/a/a/a/a/c;->f:Lcom/google/c/e/a/a/a/a/e;

    if-eqz v0, :cond_5

    .line 152
    const/4 v0, 0x6

    iget-object v1, p0, Lcom/google/c/e/a/a/a/a/c;->f:Lcom/google/c/e/a/a/a/a/e;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 154
    :cond_5
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 155
    return-void
.end method
