.class public final Lcom/google/c/e/b/a/a/e;
.super Lcom/google/protobuf/nano/d;
.source "SourceFile"


# instance fields
.field public a:Lcom/google/ac/c/a/a/a/r;

.field public apiHeader:Lcom/google/c/e/b/a/a/c;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 427
    invoke-direct {p0}, Lcom/google/protobuf/nano/d;-><init>()V

    .line 428
    iput-object v0, p0, Lcom/google/c/e/b/a/a/e;->apiHeader:Lcom/google/c/e/b/a/a/c;

    iput-object v0, p0, Lcom/google/c/e/b/a/a/e;->a:Lcom/google/ac/c/a/a/a/r;

    iput-object v0, p0, Lcom/google/c/e/b/a/a/e;->unknownFieldData:Lcom/google/protobuf/nano/f;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/c/e/b/a/a/e;->cachedSize:I

    .line 429
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 453
    invoke-super {p0}, Lcom/google/protobuf/nano/d;->computeSerializedSize()I

    move-result v0

    .line 454
    iget-object v1, p0, Lcom/google/c/e/b/a/a/e;->apiHeader:Lcom/google/c/e/b/a/a/c;

    if-eqz v1, :cond_0

    .line 455
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/c/e/b/a/a/e;->apiHeader:Lcom/google/c/e/b/a/a/c;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 458
    :cond_0
    iget-object v1, p0, Lcom/google/c/e/b/a/a/e;->a:Lcom/google/ac/c/a/a/a/r;

    if-eqz v1, :cond_1

    .line 459
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/c/e/b/a/a/e;->a:Lcom/google/ac/c/a/a/a/r;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 462
    :cond_1
    return v0
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 404
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/c/e/b/a/a/e;->storeUnknownField(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Lcom/google/c/e/b/a/a/e;->apiHeader:Lcom/google/c/e/b/a/a/c;

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/c/e/b/a/a/c;

    invoke-direct {v0}, Lcom/google/c/e/b/a/a/c;-><init>()V

    iput-object v0, p0, Lcom/google/c/e/b/a/a/e;->apiHeader:Lcom/google/c/e/b/a/a/c;

    :cond_1
    iget-object v0, p0, Lcom/google/c/e/b/a/a/e;->apiHeader:Lcom/google/c/e/b/a/a/c;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Lcom/google/c/e/b/a/a/e;->a:Lcom/google/ac/c/a/a/a/r;

    if-nez v0, :cond_2

    new-instance v0, Lcom/google/ac/c/a/a/a/r;

    invoke-direct {v0}, Lcom/google/ac/c/a/a/a/r;-><init>()V

    iput-object v0, p0, Lcom/google/c/e/b/a/a/e;->a:Lcom/google/ac/c/a/a/a/r;

    :cond_2
    iget-object v0, p0, Lcom/google/c/e/b/a/a/e;->a:Lcom/google/ac/c/a/a/a/r;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 442
    iget-object v0, p0, Lcom/google/c/e/b/a/a/e;->apiHeader:Lcom/google/c/e/b/a/a/c;

    if-eqz v0, :cond_0

    .line 443
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/c/e/b/a/a/e;->apiHeader:Lcom/google/c/e/b/a/a/c;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 445
    :cond_0
    iget-object v0, p0, Lcom/google/c/e/b/a/a/e;->a:Lcom/google/ac/c/a/a/a/r;

    if-eqz v0, :cond_1

    .line 446
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/c/e/b/a/a/e;->a:Lcom/google/ac/c/a/a/a/r;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 448
    :cond_1
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/d;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 449
    return-void
.end method
