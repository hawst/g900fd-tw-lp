.class public final Lcom/google/c/e/b/b/a/a/c;
.super Lcom/google/protobuf/nano/d;
.source "SourceFile"


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/lang/Integer;

.field public c:Lcom/google/c/e/b/b/a/a/d;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 373
    invoke-direct {p0}, Lcom/google/protobuf/nano/d;-><init>()V

    .line 374
    iput-object v0, p0, Lcom/google/c/e/b/b/a/a/c;->a:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/c/e/b/b/a/a/c;->b:Ljava/lang/Integer;

    iput-object v0, p0, Lcom/google/c/e/b/b/a/a/c;->c:Lcom/google/c/e/b/b/a/a/d;

    iput-object v0, p0, Lcom/google/c/e/b/b/a/a/c;->unknownFieldData:Lcom/google/protobuf/nano/f;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/c/e/b/b/a/a/c;->cachedSize:I

    .line 375
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 403
    invoke-super {p0}, Lcom/google/protobuf/nano/d;->computeSerializedSize()I

    move-result v0

    .line 404
    iget-object v1, p0, Lcom/google/c/e/b/b/a/a/c;->a:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 405
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/c/e/b/b/a/a/c;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 408
    :cond_0
    iget-object v1, p0, Lcom/google/c/e/b/b/a/a/c;->b:Ljava/lang/Integer;

    if-eqz v1, :cond_1

    .line 409
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/c/e/b/b/a/a/c;->b:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 412
    :cond_1
    iget-object v1, p0, Lcom/google/c/e/b/b/a/a/c;->c:Lcom/google/c/e/b/b/a/a/d;

    if-eqz v1, :cond_2

    .line 413
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/c/e/b/b/a/a/c;->c:Lcom/google/c/e/b/b/a/a/d;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 416
    :cond_2
    return v0
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 208
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/c/e/b/b/a/a/c;->storeUnknownField(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/c/e/b/b/a/a/c;->a:Ljava/lang/String;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/c/e/b/b/a/a/c;->b:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Lcom/google/c/e/b/b/a/a/c;->c:Lcom/google/c/e/b/b/a/a/d;

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/c/e/b/b/a/a/d;

    invoke-direct {v0}, Lcom/google/c/e/b/b/a/a/d;-><init>()V

    iput-object v0, p0, Lcom/google/c/e/b/b/a/a/c;->c:Lcom/google/c/e/b/b/a/a/d;

    :cond_1
    iget-object v0, p0, Lcom/google/c/e/b/b/a/a/c;->c:Lcom/google/c/e/b/b/a/a/d;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 389
    iget-object v0, p0, Lcom/google/c/e/b/b/a/a/c;->a:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 390
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/c/e/b/b/a/a/c;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 392
    :cond_0
    iget-object v0, p0, Lcom/google/c/e/b/b/a/a/c;->b:Ljava/lang/Integer;

    if-eqz v0, :cond_1

    .line 393
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/c/e/b/b/a/a/c;->b:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 395
    :cond_1
    iget-object v0, p0, Lcom/google/c/e/b/b/a/a/c;->c:Lcom/google/c/e/b/b/a/a/d;

    if-eqz v0, :cond_2

    .line 396
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/c/e/b/b/a/a/c;->c:Lcom/google/c/e/b/b/a/a/d;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 398
    :cond_2
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/d;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 399
    return-void
.end method
