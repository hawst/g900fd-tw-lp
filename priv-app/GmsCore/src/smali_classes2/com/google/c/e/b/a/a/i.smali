.class public final Lcom/google/c/e/b/a/a/i;
.super Lcom/google/protobuf/nano/d;
.source "SourceFile"


# instance fields
.field public a:Lcom/google/c/e/b/c/a/a/a/b/b;

.field public apiHeader:Lcom/google/c/e/b/a/a/c;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 851
    invoke-direct {p0}, Lcom/google/protobuf/nano/d;-><init>()V

    .line 852
    iput-object v0, p0, Lcom/google/c/e/b/a/a/i;->apiHeader:Lcom/google/c/e/b/a/a/c;

    iput-object v0, p0, Lcom/google/c/e/b/a/a/i;->a:Lcom/google/c/e/b/c/a/a/a/b/b;

    iput-object v0, p0, Lcom/google/c/e/b/a/a/i;->unknownFieldData:Lcom/google/protobuf/nano/f;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/c/e/b/a/a/i;->cachedSize:I

    .line 853
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 877
    invoke-super {p0}, Lcom/google/protobuf/nano/d;->computeSerializedSize()I

    move-result v0

    .line 878
    iget-object v1, p0, Lcom/google/c/e/b/a/a/i;->apiHeader:Lcom/google/c/e/b/a/a/c;

    if-eqz v1, :cond_0

    .line 879
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/c/e/b/a/a/i;->apiHeader:Lcom/google/c/e/b/a/a/c;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 882
    :cond_0
    iget-object v1, p0, Lcom/google/c/e/b/a/a/i;->a:Lcom/google/c/e/b/c/a/a/a/b/b;

    if-eqz v1, :cond_1

    .line 883
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/c/e/b/a/a/i;->a:Lcom/google/c/e/b/c/a/a/a/b/b;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 886
    :cond_1
    return v0
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 828
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/c/e/b/a/a/i;->storeUnknownField(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Lcom/google/c/e/b/a/a/i;->apiHeader:Lcom/google/c/e/b/a/a/c;

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/c/e/b/a/a/c;

    invoke-direct {v0}, Lcom/google/c/e/b/a/a/c;-><init>()V

    iput-object v0, p0, Lcom/google/c/e/b/a/a/i;->apiHeader:Lcom/google/c/e/b/a/a/c;

    :cond_1
    iget-object v0, p0, Lcom/google/c/e/b/a/a/i;->apiHeader:Lcom/google/c/e/b/a/a/c;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Lcom/google/c/e/b/a/a/i;->a:Lcom/google/c/e/b/c/a/a/a/b/b;

    if-nez v0, :cond_2

    new-instance v0, Lcom/google/c/e/b/c/a/a/a/b/b;

    invoke-direct {v0}, Lcom/google/c/e/b/c/a/a/a/b/b;-><init>()V

    iput-object v0, p0, Lcom/google/c/e/b/a/a/i;->a:Lcom/google/c/e/b/c/a/a/a/b/b;

    :cond_2
    iget-object v0, p0, Lcom/google/c/e/b/a/a/i;->a:Lcom/google/c/e/b/c/a/a/a/b/b;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 866
    iget-object v0, p0, Lcom/google/c/e/b/a/a/i;->apiHeader:Lcom/google/c/e/b/a/a/c;

    if-eqz v0, :cond_0

    .line 867
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/c/e/b/a/a/i;->apiHeader:Lcom/google/c/e/b/a/a/c;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 869
    :cond_0
    iget-object v0, p0, Lcom/google/c/e/b/a/a/i;->a:Lcom/google/c/e/b/c/a/a/a/b/b;

    if-eqz v0, :cond_1

    .line 870
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/c/e/b/a/a/i;->a:Lcom/google/c/e/b/c/a/a/a/b/b;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 872
    :cond_1
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/d;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 873
    return-void
.end method
