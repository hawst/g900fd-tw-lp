.class public final Lcom/google/c/e/a/a/a/a/g;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# instance fields
.field public a:Lcom/google/c/e/a/a/a/a/h;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 26
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 27
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/c/e/a/a/a/a/g;->a:Lcom/google/c/e/a/a/a/a/h;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/c/e/a/a/a/a/g;->cachedSize:I

    .line 28
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 76
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->computeSerializedSize()I

    move-result v0

    .line 77
    iget-object v1, p0, Lcom/google/c/e/a/a/a/a/g;->a:Lcom/google/c/e/a/a/a/a/h;

    if-eqz v1, :cond_0

    .line 78
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/c/e/a/a/a/a/g;->a:Lcom/google/c/e/a/a/a/a/h;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 81
    :cond_0
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 38
    if-ne p1, p0, :cond_1

    .line 54
    :cond_0
    :goto_0
    return v0

    .line 41
    :cond_1
    instance-of v2, p1, Lcom/google/c/e/a/a/a/a/g;

    if-nez v2, :cond_2

    move v0, v1

    .line 42
    goto :goto_0

    .line 44
    :cond_2
    check-cast p1, Lcom/google/c/e/a/a/a/a/g;

    .line 45
    iget-object v2, p0, Lcom/google/c/e/a/a/a/a/g;->a:Lcom/google/c/e/a/a/a/a/h;

    if-nez v2, :cond_3

    .line 46
    iget-object v2, p1, Lcom/google/c/e/a/a/a/a/g;->a:Lcom/google/c/e/a/a/a/a/h;

    if-eqz v2, :cond_0

    move v0, v1

    .line 47
    goto :goto_0

    .line 50
    :cond_3
    iget-object v2, p0, Lcom/google/c/e/a/a/a/a/g;->a:Lcom/google/c/e/a/a/a/a/h;

    iget-object v3, p1, Lcom/google/c/e/a/a/a/a/g;->a:Lcom/google/c/e/a/a/a/a/h;

    invoke-virtual {v2, v3}, Lcom/google/c/e/a/a/a/a/h;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    .line 51
    goto :goto_0
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lcom/google/c/e/a/a/a/a/g;->a:Lcom/google/c/e/a/a/a/a/h;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    .line 62
    return v0

    .line 59
    :cond_0
    iget-object v0, p0, Lcom/google/c/e/a/a/a/a/g;->a:Lcom/google/c/e/a/a/a/a/h;

    invoke-virtual {v0}, Lcom/google/c/e/a/a/a/a/h;->hashCode()I

    move-result v0

    goto :goto_0
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 5
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Lcom/google/c/e/a/a/a/a/g;->a:Lcom/google/c/e/a/a/a/a/h;

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/c/e/a/a/a/a/h;

    invoke-direct {v0}, Lcom/google/c/e/a/a/a/a/h;-><init>()V

    iput-object v0, p0, Lcom/google/c/e/a/a/a/a/g;->a:Lcom/google/c/e/a/a/a/a/h;

    :cond_1
    iget-object v0, p0, Lcom/google/c/e/a/a/a/a/g;->a:Lcom/google/c/e/a/a/a/a/h;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 68
    iget-object v0, p0, Lcom/google/c/e/a/a/a/a/g;->a:Lcom/google/c/e/a/a/a/a/h;

    if-eqz v0, :cond_0

    .line 69
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/c/e/a/a/a/a/g;->a:Lcom/google/c/e/a/a/a/a/h;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 71
    :cond_0
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 72
    return-void
.end method
