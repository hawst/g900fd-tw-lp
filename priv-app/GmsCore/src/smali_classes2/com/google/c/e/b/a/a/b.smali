.class public final Lcom/google/c/e/b/a/a/b;
.super Lcom/google/protobuf/nano/d;
.source "SourceFile"


# instance fields
.field public a:Ljava/lang/Boolean;

.field public b:Lf/a/a/b;

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 37
    invoke-direct {p0}, Lcom/google/protobuf/nano/d;-><init>()V

    .line 38
    iput-object v0, p0, Lcom/google/c/e/b/a/a/b;->a:Ljava/lang/Boolean;

    iput-object v0, p0, Lcom/google/c/e/b/a/a/b;->b:Lf/a/a/b;

    iput-object v0, p0, Lcom/google/c/e/b/a/a/b;->c:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/c/e/b/a/a/b;->d:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/c/e/b/a/a/b;->unknownFieldData:Lcom/google/protobuf/nano/f;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/c/e/b/a/a/b;->cachedSize:I

    .line 39
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 71
    invoke-super {p0}, Lcom/google/protobuf/nano/d;->computeSerializedSize()I

    move-result v0

    .line 72
    iget-object v1, p0, Lcom/google/c/e/b/a/a/b;->a:Ljava/lang/Boolean;

    if-eqz v1, :cond_0

    .line 73
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/c/e/b/a/a/b;->a:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 76
    :cond_0
    iget-object v1, p0, Lcom/google/c/e/b/a/a/b;->b:Lf/a/a/b;

    if-eqz v1, :cond_1

    .line 77
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/c/e/b/a/a/b;->b:Lf/a/a/b;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 80
    :cond_1
    iget-object v1, p0, Lcom/google/c/e/b/a/a/b;->c:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 81
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/c/e/b/a/a/b;->c:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 84
    :cond_2
    iget-object v1, p0, Lcom/google/c/e/b/a/a/b;->d:Ljava/lang/String;

    if-eqz v1, :cond_3

    .line 85
    const/4 v1, 0x5

    iget-object v2, p0, Lcom/google/c/e/b/a/a/b;->d:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 88
    :cond_3
    return v0
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 8
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/c/e/b/a/a/b;->storeUnknownField(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/c/e/b/a/a/b;->a:Ljava/lang/Boolean;

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Lcom/google/c/e/b/a/a/b;->b:Lf/a/a/b;

    if-nez v0, :cond_1

    new-instance v0, Lf/a/a/b;

    invoke-direct {v0}, Lf/a/a/b;-><init>()V

    iput-object v0, p0, Lcom/google/c/e/b/a/a/b;->b:Lf/a/a/b;

    :cond_1
    iget-object v0, p0, Lcom/google/c/e/b/a/a/b;->b:Lf/a/a/b;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/c/e/b/a/a/b;->c:Ljava/lang/String;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/c/e/b/a/a/b;->d:Ljava/lang/String;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x10 -> :sswitch_1
        0x1a -> :sswitch_2
        0x22 -> :sswitch_3
        0x2a -> :sswitch_4
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 54
    iget-object v0, p0, Lcom/google/c/e/b/a/a/b;->a:Ljava/lang/Boolean;

    if-eqz v0, :cond_0

    .line 55
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/c/e/b/a/a/b;->a:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 57
    :cond_0
    iget-object v0, p0, Lcom/google/c/e/b/a/a/b;->b:Lf/a/a/b;

    if-eqz v0, :cond_1

    .line 58
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/c/e/b/a/a/b;->b:Lf/a/a/b;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 60
    :cond_1
    iget-object v0, p0, Lcom/google/c/e/b/a/a/b;->c:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 61
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/c/e/b/a/a/b;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 63
    :cond_2
    iget-object v0, p0, Lcom/google/c/e/b/a/a/b;->d:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 64
    const/4 v0, 0x5

    iget-object v1, p0, Lcom/google/c/e/b/a/a/b;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 66
    :cond_3
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/d;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 67
    return-void
.end method
