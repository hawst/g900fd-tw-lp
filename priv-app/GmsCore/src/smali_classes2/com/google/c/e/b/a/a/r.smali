.class public final Lcom/google/c/e/b/a/a/r;
.super Lcom/google/protobuf/nano/d;
.source "SourceFile"


# instance fields
.field public a:Lcom/google/c/e/b/d/a/f;

.field public apiHeader:Lcom/google/c/e/b/a/a/n;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 639
    invoke-direct {p0}, Lcom/google/protobuf/nano/d;-><init>()V

    .line 640
    iput-object v0, p0, Lcom/google/c/e/b/a/a/r;->apiHeader:Lcom/google/c/e/b/a/a/n;

    iput-object v0, p0, Lcom/google/c/e/b/a/a/r;->a:Lcom/google/c/e/b/d/a/f;

    iput-object v0, p0, Lcom/google/c/e/b/a/a/r;->unknownFieldData:Lcom/google/protobuf/nano/f;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/c/e/b/a/a/r;->cachedSize:I

    .line 641
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/protobuf/nano/a;)Lcom/google/c/e/b/a/a/r;
    .locals 1

    .prologue
    .line 682
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    .line 683
    sparse-switch v0, :sswitch_data_0

    .line 687
    invoke-virtual {p0, p1, v0}, Lcom/google/c/e/b/a/a/r;->storeUnknownField(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 688
    :sswitch_0
    return-object p0

    .line 693
    :sswitch_1
    iget-object v0, p0, Lcom/google/c/e/b/a/a/r;->apiHeader:Lcom/google/c/e/b/a/a/n;

    if-nez v0, :cond_1

    .line 694
    new-instance v0, Lcom/google/c/e/b/a/a/n;

    invoke-direct {v0}, Lcom/google/c/e/b/a/a/n;-><init>()V

    iput-object v0, p0, Lcom/google/c/e/b/a/a/r;->apiHeader:Lcom/google/c/e/b/a/a/n;

    .line 696
    :cond_1
    iget-object v0, p0, Lcom/google/c/e/b/a/a/r;->apiHeader:Lcom/google/c/e/b/a/a/n;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    .line 700
    :sswitch_2
    iget-object v0, p0, Lcom/google/c/e/b/a/a/r;->a:Lcom/google/c/e/b/d/a/f;

    if-nez v0, :cond_2

    .line 701
    new-instance v0, Lcom/google/c/e/b/d/a/f;

    invoke-direct {v0}, Lcom/google/c/e/b/d/a/f;-><init>()V

    iput-object v0, p0, Lcom/google/c/e/b/a/a/r;->a:Lcom/google/c/e/b/d/a/f;

    .line 703
    :cond_2
    iget-object v0, p0, Lcom/google/c/e/b/a/a/r;->a:Lcom/google/c/e/b/d/a/f;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    .line 683
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 665
    invoke-super {p0}, Lcom/google/protobuf/nano/d;->computeSerializedSize()I

    move-result v0

    .line 666
    iget-object v1, p0, Lcom/google/c/e/b/a/a/r;->apiHeader:Lcom/google/c/e/b/a/a/n;

    if-eqz v1, :cond_0

    .line 667
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/c/e/b/a/a/r;->apiHeader:Lcom/google/c/e/b/a/a/n;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 670
    :cond_0
    iget-object v1, p0, Lcom/google/c/e/b/a/a/r;->a:Lcom/google/c/e/b/d/a/f;

    if-eqz v1, :cond_1

    .line 671
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/c/e/b/a/a/r;->a:Lcom/google/c/e/b/d/a/f;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 674
    :cond_1
    return v0
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 616
    invoke-virtual {p0, p1}, Lcom/google/c/e/b/a/a/r;->a(Lcom/google/protobuf/nano/a;)Lcom/google/c/e/b/a/a/r;

    move-result-object v0

    return-object v0
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 654
    iget-object v0, p0, Lcom/google/c/e/b/a/a/r;->apiHeader:Lcom/google/c/e/b/a/a/n;

    if-eqz v0, :cond_0

    .line 655
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/c/e/b/a/a/r;->apiHeader:Lcom/google/c/e/b/a/a/n;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 657
    :cond_0
    iget-object v0, p0, Lcom/google/c/e/b/a/a/r;->a:Lcom/google/c/e/b/d/a/f;

    if-eqz v0, :cond_1

    .line 658
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/c/e/b/a/a/r;->a:Lcom/google/c/e/b/d/a/f;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 660
    :cond_1
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/d;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 661
    return-void
.end method
