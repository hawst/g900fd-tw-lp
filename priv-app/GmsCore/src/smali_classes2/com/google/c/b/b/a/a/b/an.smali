.class public final Lcom/google/c/b/b/a/a/b/an;
.super Lcom/google/protobuf/nano/d;
.source "SourceFile"


# instance fields
.field public a:Lcom/google/c/b/b/a/a/b/ar;

.field public b:Lcom/google/c/b/b/a/a/b/ao;

.field public c:Lcom/google/c/b/b/a/a/b/ao;

.field public d:Lcom/google/c/b/b/a/a/b/aq;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 532
    invoke-direct {p0}, Lcom/google/protobuf/nano/d;-><init>()V

    .line 533
    iput-object v0, p0, Lcom/google/c/b/b/a/a/b/an;->a:Lcom/google/c/b/b/a/a/b/ar;

    iput-object v0, p0, Lcom/google/c/b/b/a/a/b/an;->b:Lcom/google/c/b/b/a/a/b/ao;

    iput-object v0, p0, Lcom/google/c/b/b/a/a/b/an;->c:Lcom/google/c/b/b/a/a/b/ao;

    iput-object v0, p0, Lcom/google/c/b/b/a/a/b/an;->d:Lcom/google/c/b/b/a/a/b/aq;

    iput-object v0, p0, Lcom/google/c/b/b/a/a/b/an;->unknownFieldData:Lcom/google/protobuf/nano/f;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/c/b/b/a/a/b/an;->cachedSize:I

    .line 534
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 566
    invoke-super {p0}, Lcom/google/protobuf/nano/d;->computeSerializedSize()I

    move-result v0

    .line 567
    iget-object v1, p0, Lcom/google/c/b/b/a/a/b/an;->a:Lcom/google/c/b/b/a/a/b/ar;

    if-eqz v1, :cond_0

    .line 568
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/c/b/b/a/a/b/an;->a:Lcom/google/c/b/b/a/a/b/ar;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 571
    :cond_0
    iget-object v1, p0, Lcom/google/c/b/b/a/a/b/an;->b:Lcom/google/c/b/b/a/a/b/ao;

    if-eqz v1, :cond_1

    .line 572
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/c/b/b/a/a/b/an;->b:Lcom/google/c/b/b/a/a/b/ao;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 575
    :cond_1
    iget-object v1, p0, Lcom/google/c/b/b/a/a/b/an;->c:Lcom/google/c/b/b/a/a/b/ao;

    if-eqz v1, :cond_2

    .line 576
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/c/b/b/a/a/b/an;->c:Lcom/google/c/b/b/a/a/b/ao;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 579
    :cond_2
    iget-object v1, p0, Lcom/google/c/b/b/a/a/b/an;->d:Lcom/google/c/b/b/a/a/b/aq;

    if-eqz v1, :cond_3

    .line 580
    const/4 v1, 0x5

    iget-object v2, p0, Lcom/google/c/b/b/a/a/b/an;->d:Lcom/google/c/b/b/a/a/b/aq;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 583
    :cond_3
    return v0
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 5
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/c/b/b/a/a/b/an;->storeUnknownField(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Lcom/google/c/b/b/a/a/b/an;->a:Lcom/google/c/b/b/a/a/b/ar;

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/c/b/b/a/a/b/ar;

    invoke-direct {v0}, Lcom/google/c/b/b/a/a/b/ar;-><init>()V

    iput-object v0, p0, Lcom/google/c/b/b/a/a/b/an;->a:Lcom/google/c/b/b/a/a/b/ar;

    :cond_1
    iget-object v0, p0, Lcom/google/c/b/b/a/a/b/an;->a:Lcom/google/c/b/b/a/a/b/ar;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Lcom/google/c/b/b/a/a/b/an;->b:Lcom/google/c/b/b/a/a/b/ao;

    if-nez v0, :cond_2

    new-instance v0, Lcom/google/c/b/b/a/a/b/ao;

    invoke-direct {v0}, Lcom/google/c/b/b/a/a/b/ao;-><init>()V

    iput-object v0, p0, Lcom/google/c/b/b/a/a/b/an;->b:Lcom/google/c/b/b/a/a/b/ao;

    :cond_2
    iget-object v0, p0, Lcom/google/c/b/b/a/a/b/an;->b:Lcom/google/c/b/b/a/a/b/ao;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Lcom/google/c/b/b/a/a/b/an;->c:Lcom/google/c/b/b/a/a/b/ao;

    if-nez v0, :cond_3

    new-instance v0, Lcom/google/c/b/b/a/a/b/ao;

    invoke-direct {v0}, Lcom/google/c/b/b/a/a/b/ao;-><init>()V

    iput-object v0, p0, Lcom/google/c/b/b/a/a/b/an;->c:Lcom/google/c/b/b/a/a/b/ao;

    :cond_3
    iget-object v0, p0, Lcom/google/c/b/b/a/a/b/an;->c:Lcom/google/c/b/b/a/a/b/ao;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_4
    iget-object v0, p0, Lcom/google/c/b/b/a/a/b/an;->d:Lcom/google/c/b/b/a/a/b/aq;

    if-nez v0, :cond_4

    new-instance v0, Lcom/google/c/b/b/a/a/b/aq;

    invoke-direct {v0}, Lcom/google/c/b/b/a/a/b/aq;-><init>()V

    iput-object v0, p0, Lcom/google/c/b/b/a/a/b/an;->d:Lcom/google/c/b/b/a/a/b/aq;

    :cond_4
    iget-object v0, p0, Lcom/google/c/b/b/a/a/b/an;->d:Lcom/google/c/b/b/a/a/b/aq;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x1a -> :sswitch_2
        0x22 -> :sswitch_3
        0x2a -> :sswitch_4
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 549
    iget-object v0, p0, Lcom/google/c/b/b/a/a/b/an;->a:Lcom/google/c/b/b/a/a/b/ar;

    if-eqz v0, :cond_0

    .line 550
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/c/b/b/a/a/b/an;->a:Lcom/google/c/b/b/a/a/b/ar;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 552
    :cond_0
    iget-object v0, p0, Lcom/google/c/b/b/a/a/b/an;->b:Lcom/google/c/b/b/a/a/b/ao;

    if-eqz v0, :cond_1

    .line 553
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/c/b/b/a/a/b/an;->b:Lcom/google/c/b/b/a/a/b/ao;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 555
    :cond_1
    iget-object v0, p0, Lcom/google/c/b/b/a/a/b/an;->c:Lcom/google/c/b/b/a/a/b/ao;

    if-eqz v0, :cond_2

    .line 556
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/c/b/b/a/a/b/an;->c:Lcom/google/c/b/b/a/a/b/ao;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 558
    :cond_2
    iget-object v0, p0, Lcom/google/c/b/b/a/a/b/an;->d:Lcom/google/c/b/b/a/a/b/aq;

    if-eqz v0, :cond_3

    .line 559
    const/4 v0, 0x5

    iget-object v1, p0, Lcom/google/c/b/b/a/a/b/an;->d:Lcom/google/c/b/b/a/a/b/aq;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 561
    :cond_3
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/d;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 562
    return-void
.end method
