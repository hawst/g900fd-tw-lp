.class public final Lcom/google/c/b/b/a/a/b/h;
.super Lcom/google/protobuf/nano/d;
.source "SourceFile"


# instance fields
.field public a:Lcom/google/c/b/b/a/a/b/i;

.field public b:Ljava/lang/Boolean;

.field public c:[I

.field public d:Lcom/google/c/b/b/a/a/b/a;

.field public e:Ljava/lang/Integer;

.field public f:Ljava/lang/Integer;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 180
    invoke-direct {p0}, Lcom/google/protobuf/nano/d;-><init>()V

    .line 181
    iput-object v1, p0, Lcom/google/c/b/b/a/a/b/h;->a:Lcom/google/c/b/b/a/a/b/i;

    iput-object v1, p0, Lcom/google/c/b/b/a/a/b/h;->b:Ljava/lang/Boolean;

    sget-object v0, Lcom/google/protobuf/nano/m;->a:[I

    iput-object v0, p0, Lcom/google/c/b/b/a/a/b/h;->c:[I

    iput-object v1, p0, Lcom/google/c/b/b/a/a/b/h;->d:Lcom/google/c/b/b/a/a/b/a;

    iput-object v1, p0, Lcom/google/c/b/b/a/a/b/h;->e:Ljava/lang/Integer;

    iput-object v1, p0, Lcom/google/c/b/b/a/a/b/h;->f:Ljava/lang/Integer;

    iput-object v1, p0, Lcom/google/c/b/b/a/a/b/h;->unknownFieldData:Lcom/google/protobuf/nano/f;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/c/b/b/a/a/b/h;->cachedSize:I

    .line 182
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 224
    invoke-super {p0}, Lcom/google/protobuf/nano/d;->computeSerializedSize()I

    move-result v0

    .line 225
    iget-object v2, p0, Lcom/google/c/b/b/a/a/b/h;->a:Lcom/google/c/b/b/a/a/b/i;

    if-eqz v2, :cond_0

    .line 226
    const/4 v2, 0x1

    iget-object v3, p0, Lcom/google/c/b/b/a/a/b/h;->a:Lcom/google/c/b/b/a/a/b/i;

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v2

    add-int/2addr v0, v2

    .line 229
    :cond_0
    iget-object v2, p0, Lcom/google/c/b/b/a/a/b/h;->b:Ljava/lang/Boolean;

    if-eqz v2, :cond_1

    .line 230
    const/4 v2, 0x2

    iget-object v3, p0, Lcom/google/c/b/b/a/a/b/h;->b:Ljava/lang/Boolean;

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v2}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 233
    :cond_1
    iget-object v2, p0, Lcom/google/c/b/b/a/a/b/h;->c:[I

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/google/c/b/b/a/a/b/h;->c:[I

    array-length v2, v2

    if-lez v2, :cond_3

    move v2, v1

    .line 235
    :goto_0
    iget-object v3, p0, Lcom/google/c/b/b/a/a/b/h;->c:[I

    array-length v3, v3

    if-ge v1, v3, :cond_2

    .line 236
    iget-object v3, p0, Lcom/google/c/b/b/a/a/b/h;->c:[I

    aget v3, v3, v1

    .line 237
    invoke-static {v3}, Lcom/google/protobuf/nano/b;->b(I)I

    move-result v3

    add-int/2addr v2, v3

    .line 235
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 240
    :cond_2
    add-int/2addr v0, v2

    .line 241
    iget-object v1, p0, Lcom/google/c/b/b/a/a/b/h;->c:[I

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 243
    :cond_3
    iget-object v1, p0, Lcom/google/c/b/b/a/a/b/h;->d:Lcom/google/c/b/b/a/a/b/a;

    if-eqz v1, :cond_4

    .line 244
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/c/b/b/a/a/b/h;->d:Lcom/google/c/b/b/a/a/b/a;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 247
    :cond_4
    iget-object v1, p0, Lcom/google/c/b/b/a/a/b/h;->e:Ljava/lang/Integer;

    if-eqz v1, :cond_5

    .line 248
    const/4 v1, 0x5

    iget-object v2, p0, Lcom/google/c/b/b/a/a/b/h;->e:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 251
    :cond_5
    iget-object v1, p0, Lcom/google/c/b/b/a/a/b/h;->f:Ljava/lang/Integer;

    if-eqz v1, :cond_6

    .line 252
    const/4 v1, 0x6

    iget-object v2, p0, Lcom/google/c/b/b/a/a/b/h;->f:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 255
    :cond_6
    return v0
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 5
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/c/b/b/a/a/b/h;->storeUnknownField(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Lcom/google/c/b/b/a/a/b/h;->a:Lcom/google/c/b/b/a/a/b/i;

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/c/b/b/a/a/b/i;

    invoke-direct {v0}, Lcom/google/c/b/b/a/a/b/i;-><init>()V

    iput-object v0, p0, Lcom/google/c/b/b/a/a/b/h;->a:Lcom/google/c/b/b/a/a/b/i;

    :cond_1
    iget-object v0, p0, Lcom/google/c/b/b/a/a/b/h;->a:Lcom/google/c/b/b/a/a/b/i;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/c/b/b/a/a/b/h;->b:Ljava/lang/Boolean;

    goto :goto_0

    :sswitch_3
    const/16 v0, 0x18

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->b(Lcom/google/protobuf/nano/a;I)I

    move-result v4

    new-array v5, v4, [I

    move v3, v2

    move v1, v2

    :goto_1
    if-ge v3, v4, :cond_3

    if-eqz v3, :cond_2

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    :cond_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v6

    packed-switch v6, :pswitch_data_0

    move v0, v1

    :goto_2
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    move v1, v0

    goto :goto_1

    :pswitch_0
    add-int/lit8 v0, v1, 0x1

    aput v6, v5, v1

    goto :goto_2

    :cond_3
    if-eqz v1, :cond_0

    iget-object v0, p0, Lcom/google/c/b/b/a/a/b/h;->c:[I

    if-nez v0, :cond_4

    move v0, v2

    :goto_3
    if-nez v0, :cond_5

    array-length v3, v5

    if-ne v1, v3, :cond_5

    iput-object v5, p0, Lcom/google/c/b/b/a/a/b/h;->c:[I

    goto :goto_0

    :cond_4
    iget-object v0, p0, Lcom/google/c/b/b/a/a/b/h;->c:[I

    array-length v0, v0

    goto :goto_3

    :cond_5
    add-int v3, v0, v1

    new-array v3, v3, [I

    if-eqz v0, :cond_6

    iget-object v4, p0, Lcom/google/c/b/b/a/a/b/h;->c:[I

    invoke-static {v4, v2, v3, v2, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_6
    invoke-static {v5, v2, v3, v0, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iput-object v3, p0, Lcom/google/c/b/b/a/a/b/h;->c:[I

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->c(I)I

    move-result v3

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->o()I

    move-result v1

    move v0, v2

    :goto_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->m()I

    move-result v4

    if-lez v4, :cond_7

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v4

    packed-switch v4, :pswitch_data_1

    goto :goto_4

    :pswitch_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_7
    if-eqz v0, :cond_b

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/a;->e(I)V

    iget-object v1, p0, Lcom/google/c/b/b/a/a/b/h;->c:[I

    if-nez v1, :cond_9

    move v1, v2

    :goto_5
    add-int/2addr v0, v1

    new-array v4, v0, [I

    if-eqz v1, :cond_8

    iget-object v0, p0, Lcom/google/c/b/b/a/a/b/h;->c:[I

    invoke-static {v0, v2, v4, v2, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_8
    :goto_6
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->m()I

    move-result v0

    if-lez v0, :cond_a

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v5

    packed-switch v5, :pswitch_data_2

    goto :goto_6

    :pswitch_2
    add-int/lit8 v0, v1, 0x1

    aput v5, v4, v1

    move v1, v0

    goto :goto_6

    :cond_9
    iget-object v1, p0, Lcom/google/c/b/b/a/a/b/h;->c:[I

    array-length v1, v1

    goto :goto_5

    :cond_a
    iput-object v4, p0, Lcom/google/c/b/b/a/a/b/h;->c:[I

    :cond_b
    invoke-virtual {p1, v3}, Lcom/google/protobuf/nano/a;->d(I)V

    goto/16 :goto_0

    :sswitch_5
    iget-object v0, p0, Lcom/google/c/b/b/a/a/b/h;->d:Lcom/google/c/b/b/a/a/b/a;

    if-nez v0, :cond_c

    new-instance v0, Lcom/google/c/b/b/a/a/b/a;

    invoke-direct {v0}, Lcom/google/c/b/b/a/a/b/a;-><init>()V

    iput-object v0, p0, Lcom/google/c/b/b/a/a/b/h;->d:Lcom/google/c/b/b/a/a/b/a;

    :cond_c
    iget-object v0, p0, Lcom/google/c/b/b/a/a/b/h;->d:Lcom/google/c/b/b/a/a/b/a;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto/16 :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    packed-switch v0, :pswitch_data_3

    goto/16 :goto_0

    :pswitch_3
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/c/b/b/a/a/b/h;->e:Ljava/lang/Integer;

    goto/16 :goto_0

    :sswitch_7
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    packed-switch v0, :pswitch_data_4

    goto/16 :goto_0

    :pswitch_4
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/c/b/b/a/a/b/h;->f:Ljava/lang/Integer;

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x1a -> :sswitch_4
        0x22 -> :sswitch_5
        0x28 -> :sswitch_6
        0x30 -> :sswitch_7
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x0
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
    .end packed-switch

    :pswitch_data_3
    .packed-switch 0x0
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
    .end packed-switch

    :pswitch_data_4
    .packed-switch 0x0
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_4
    .end packed-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 3

    .prologue
    .line 199
    iget-object v0, p0, Lcom/google/c/b/b/a/a/b/h;->a:Lcom/google/c/b/b/a/a/b/i;

    if-eqz v0, :cond_0

    .line 200
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/c/b/b/a/a/b/h;->a:Lcom/google/c/b/b/a/a/b/i;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 202
    :cond_0
    iget-object v0, p0, Lcom/google/c/b/b/a/a/b/h;->b:Ljava/lang/Boolean;

    if-eqz v0, :cond_1

    .line 203
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/c/b/b/a/a/b/h;->b:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 205
    :cond_1
    iget-object v0, p0, Lcom/google/c/b/b/a/a/b/h;->c:[I

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/c/b/b/a/a/b/h;->c:[I

    array-length v0, v0

    if-lez v0, :cond_2

    .line 206
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/google/c/b/b/a/a/b/h;->c:[I

    array-length v1, v1

    if-ge v0, v1, :cond_2

    .line 207
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/c/b/b/a/a/b/h;->c:[I

    aget v2, v2, v0

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 206
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 210
    :cond_2
    iget-object v0, p0, Lcom/google/c/b/b/a/a/b/h;->d:Lcom/google/c/b/b/a/a/b/a;

    if-eqz v0, :cond_3

    .line 211
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/c/b/b/a/a/b/h;->d:Lcom/google/c/b/b/a/a/b/a;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 213
    :cond_3
    iget-object v0, p0, Lcom/google/c/b/b/a/a/b/h;->e:Ljava/lang/Integer;

    if-eqz v0, :cond_4

    .line 214
    const/4 v0, 0x5

    iget-object v1, p0, Lcom/google/c/b/b/a/a/b/h;->e:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 216
    :cond_4
    iget-object v0, p0, Lcom/google/c/b/b/a/a/b/h;->f:Ljava/lang/Integer;

    if-eqz v0, :cond_5

    .line 217
    const/4 v0, 0x6

    iget-object v1, p0, Lcom/google/c/b/b/a/a/b/h;->f:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 219
    :cond_5
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/d;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 220
    return-void
.end method
