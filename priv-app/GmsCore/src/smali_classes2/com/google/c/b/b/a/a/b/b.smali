.class public final Lcom/google/c/b/b/a/a/b/b;
.super Lcom/google/protobuf/nano/d;
.source "SourceFile"


# instance fields
.field public a:Ljava/lang/Double;

.field public b:Ljava/lang/Double;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 419
    invoke-direct {p0}, Lcom/google/protobuf/nano/d;-><init>()V

    .line 420
    iput-object v0, p0, Lcom/google/c/b/b/a/a/b/b;->a:Ljava/lang/Double;

    iput-object v0, p0, Lcom/google/c/b/b/a/a/b/b;->b:Ljava/lang/Double;

    iput-object v0, p0, Lcom/google/c/b/b/a/a/b/b;->unknownFieldData:Lcom/google/protobuf/nano/f;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/c/b/b/a/a/b/b;->cachedSize:I

    .line 421
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 445
    invoke-super {p0}, Lcom/google/protobuf/nano/d;->computeSerializedSize()I

    move-result v0

    .line 446
    iget-object v1, p0, Lcom/google/c/b/b/a/a/b/b;->a:Ljava/lang/Double;

    if-eqz v1, :cond_0

    .line 447
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/c/b/b/a/a/b/b;->a:Ljava/lang/Double;

    invoke-virtual {v2}, Ljava/lang/Double;->doubleValue()D

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x8

    add-int/2addr v0, v1

    .line 450
    :cond_0
    iget-object v1, p0, Lcom/google/c/b/b/a/a/b/b;->b:Ljava/lang/Double;

    if-eqz v1, :cond_1

    .line 451
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/c/b/b/a/a/b/b;->b:Ljava/lang/Double;

    invoke-virtual {v2}, Ljava/lang/Double;->doubleValue()D

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x8

    add-int/2addr v0, v1

    .line 454
    :cond_1
    return v0
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 2

    .prologue
    .line 396
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/c/b/b/a/a/b/b;->storeUnknownField(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->l()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Double;->longBitsToDouble(J)D

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    iput-object v0, p0, Lcom/google/c/b/b/a/a/b/b;->a:Ljava/lang/Double;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->l()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Double;->longBitsToDouble(J)D

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    iput-object v0, p0, Lcom/google/c/b/b/a/a/b/b;->b:Ljava/lang/Double;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x9 -> :sswitch_1
        0x11 -> :sswitch_2
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 4

    .prologue
    .line 434
    iget-object v0, p0, Lcom/google/c/b/b/a/a/b/b;->a:Ljava/lang/Double;

    if-eqz v0, :cond_0

    .line 435
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/c/b/b/a/a/b/b;->a:Ljava/lang/Double;

    invoke-virtual {v1}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/b;->a(ID)V

    .line 437
    :cond_0
    iget-object v0, p0, Lcom/google/c/b/b/a/a/b/b;->b:Ljava/lang/Double;

    if-eqz v0, :cond_1

    .line 438
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/c/b/b/a/a/b/b;->b:Ljava/lang/Double;

    invoke-virtual {v1}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/b;->a(ID)V

    .line 440
    :cond_1
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/d;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 441
    return-void
.end method
