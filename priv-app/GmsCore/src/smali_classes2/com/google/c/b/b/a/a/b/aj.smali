.class public final Lcom/google/c/b/b/a/a/b/aj;
.super Lcom/google/protobuf/nano/d;
.source "SourceFile"


# instance fields
.field public a:Lcom/google/c/b/b/a/a/b/ak;

.field public b:Lcom/google/c/b/b/a/a/b/ak;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 159
    invoke-direct {p0}, Lcom/google/protobuf/nano/d;-><init>()V

    .line 160
    iput-object v0, p0, Lcom/google/c/b/b/a/a/b/aj;->a:Lcom/google/c/b/b/a/a/b/ak;

    iput-object v0, p0, Lcom/google/c/b/b/a/a/b/aj;->b:Lcom/google/c/b/b/a/a/b/ak;

    iput-object v0, p0, Lcom/google/c/b/b/a/a/b/aj;->unknownFieldData:Lcom/google/protobuf/nano/f;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/c/b/b/a/a/b/aj;->cachedSize:I

    .line 161
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 185
    invoke-super {p0}, Lcom/google/protobuf/nano/d;->computeSerializedSize()I

    move-result v0

    .line 186
    iget-object v1, p0, Lcom/google/c/b/b/a/a/b/aj;->a:Lcom/google/c/b/b/a/a/b/ak;

    if-eqz v1, :cond_0

    .line 187
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/c/b/b/a/a/b/aj;->a:Lcom/google/c/b/b/a/a/b/ak;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 190
    :cond_0
    iget-object v1, p0, Lcom/google/c/b/b/a/a/b/aj;->b:Lcom/google/c/b/b/a/a/b/ak;

    if-eqz v1, :cond_1

    .line 191
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/c/b/b/a/a/b/aj;->b:Lcom/google/c/b/b/a/a/b/ak;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 194
    :cond_1
    return v0
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 5
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/c/b/b/a/a/b/aj;->storeUnknownField(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Lcom/google/c/b/b/a/a/b/aj;->a:Lcom/google/c/b/b/a/a/b/ak;

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/c/b/b/a/a/b/ak;

    invoke-direct {v0}, Lcom/google/c/b/b/a/a/b/ak;-><init>()V

    iput-object v0, p0, Lcom/google/c/b/b/a/a/b/aj;->a:Lcom/google/c/b/b/a/a/b/ak;

    :cond_1
    iget-object v0, p0, Lcom/google/c/b/b/a/a/b/aj;->a:Lcom/google/c/b/b/a/a/b/ak;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Lcom/google/c/b/b/a/a/b/aj;->b:Lcom/google/c/b/b/a/a/b/ak;

    if-nez v0, :cond_2

    new-instance v0, Lcom/google/c/b/b/a/a/b/ak;

    invoke-direct {v0}, Lcom/google/c/b/b/a/a/b/ak;-><init>()V

    iput-object v0, p0, Lcom/google/c/b/b/a/a/b/aj;->b:Lcom/google/c/b/b/a/a/b/ak;

    :cond_2
    iget-object v0, p0, Lcom/google/c/b/b/a/a/b/aj;->b:Lcom/google/c/b/b/a/a/b/ak;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 174
    iget-object v0, p0, Lcom/google/c/b/b/a/a/b/aj;->a:Lcom/google/c/b/b/a/a/b/ak;

    if-eqz v0, :cond_0

    .line 175
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/c/b/b/a/a/b/aj;->a:Lcom/google/c/b/b/a/a/b/ak;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 177
    :cond_0
    iget-object v0, p0, Lcom/google/c/b/b/a/a/b/aj;->b:Lcom/google/c/b/b/a/a/b/ak;

    if-eqz v0, :cond_1

    .line 178
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/c/b/b/a/a/b/aj;->b:Lcom/google/c/b/b/a/a/b/ak;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 180
    :cond_1
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/d;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 181
    return-void
.end method
