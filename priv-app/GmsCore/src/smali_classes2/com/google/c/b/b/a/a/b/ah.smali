.class public final Lcom/google/c/b/b/a/a/b/ah;
.super Lcom/google/protobuf/nano/d;
.source "SourceFile"


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/lang/String;

.field public c:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 869
    invoke-direct {p0}, Lcom/google/protobuf/nano/d;-><init>()V

    .line 870
    iput-object v0, p0, Lcom/google/c/b/b/a/a/b/ah;->a:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/c/b/b/a/a/b/ah;->b:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/c/b/b/a/a/b/ah;->c:Ljava/lang/Boolean;

    iput-object v0, p0, Lcom/google/c/b/b/a/a/b/ah;->unknownFieldData:Lcom/google/protobuf/nano/f;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/c/b/b/a/a/b/ah;->cachedSize:I

    .line 871
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 899
    invoke-super {p0}, Lcom/google/protobuf/nano/d;->computeSerializedSize()I

    move-result v0

    .line 900
    iget-object v1, p0, Lcom/google/c/b/b/a/a/b/ah;->a:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 901
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/c/b/b/a/a/b/ah;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 904
    :cond_0
    iget-object v1, p0, Lcom/google/c/b/b/a/a/b/ah;->b:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 905
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/c/b/b/a/a/b/ah;->b:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 908
    :cond_1
    iget-object v1, p0, Lcom/google/c/b/b/a/a/b/ah;->c:Ljava/lang/Boolean;

    if-eqz v1, :cond_2

    .line 909
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/c/b/b/a/a/b/ah;->c:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 912
    :cond_2
    return v0
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 843
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/c/b/b/a/a/b/ah;->storeUnknownField(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/c/b/b/a/a/b/ah;->a:Ljava/lang/String;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/c/b/b/a/a/b/ah;->b:Ljava/lang/String;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/c/b/b/a/a/b/ah;->c:Ljava/lang/Boolean;

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 885
    iget-object v0, p0, Lcom/google/c/b/b/a/a/b/ah;->a:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 886
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/c/b/b/a/a/b/ah;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 888
    :cond_0
    iget-object v0, p0, Lcom/google/c/b/b/a/a/b/ah;->b:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 889
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/c/b/b/a/a/b/ah;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 891
    :cond_1
    iget-object v0, p0, Lcom/google/c/b/b/a/a/b/ah;->c:Ljava/lang/Boolean;

    if-eqz v0, :cond_2

    .line 892
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/c/b/b/a/a/b/ah;->c:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 894
    :cond_2
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/d;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 895
    return-void
.end method
