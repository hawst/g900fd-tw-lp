.class public final Lcom/google/c/b/b/a/a/f;
.super Lcom/google/protobuf/nano/d;
.source "SourceFile"


# instance fields
.field public a:[Lcom/google/c/b/b/a/a/e;

.field public b:Lcom/google/c/b/b/a/a/i;

.field public c:Lcom/google/c/b/b/a/a/a/a;

.field public d:Lcom/google/c/b/b/a/a/g;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 35
    invoke-direct {p0}, Lcom/google/protobuf/nano/d;-><init>()V

    .line 36
    invoke-static {}, Lcom/google/c/b/b/a/a/e;->a()[Lcom/google/c/b/b/a/a/e;

    move-result-object v0

    iput-object v0, p0, Lcom/google/c/b/b/a/a/f;->a:[Lcom/google/c/b/b/a/a/e;

    iput-object v1, p0, Lcom/google/c/b/b/a/a/f;->b:Lcom/google/c/b/b/a/a/i;

    iput-object v1, p0, Lcom/google/c/b/b/a/a/f;->c:Lcom/google/c/b/b/a/a/a/a;

    iput-object v1, p0, Lcom/google/c/b/b/a/a/f;->d:Lcom/google/c/b/b/a/a/g;

    iput-object v1, p0, Lcom/google/c/b/b/a/a/f;->unknownFieldData:Lcom/google/protobuf/nano/f;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/c/b/b/a/a/f;->cachedSize:I

    .line 37
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 4

    .prologue
    .line 74
    invoke-super {p0}, Lcom/google/protobuf/nano/d;->computeSerializedSize()I

    move-result v1

    .line 75
    iget-object v0, p0, Lcom/google/c/b/b/a/a/f;->a:[Lcom/google/c/b/b/a/a/e;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/c/b/b/a/a/f;->a:[Lcom/google/c/b/b/a/a/e;

    array-length v0, v0

    if-lez v0, :cond_1

    .line 76
    const/4 v0, 0x0

    :goto_0
    iget-object v2, p0, Lcom/google/c/b/b/a/a/f;->a:[Lcom/google/c/b/b/a/a/e;

    array-length v2, v2

    if-ge v0, v2, :cond_1

    .line 77
    iget-object v2, p0, Lcom/google/c/b/b/a/a/f;->a:[Lcom/google/c/b/b/a/a/e;

    aget-object v2, v2, v0

    .line 78
    if-eqz v2, :cond_0

    .line 79
    const/4 v3, 0x1

    invoke-static {v3, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v2

    add-int/2addr v1, v2

    .line 76
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 84
    :cond_1
    iget-object v0, p0, Lcom/google/c/b/b/a/a/f;->b:Lcom/google/c/b/b/a/a/i;

    if-eqz v0, :cond_2

    .line 85
    const/4 v0, 0x2

    iget-object v2, p0, Lcom/google/c/b/b/a/a/f;->b:Lcom/google/c/b/b/a/a/i;

    invoke-static {v0, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v0

    add-int/2addr v1, v0

    .line 88
    :cond_2
    iget-object v0, p0, Lcom/google/c/b/b/a/a/f;->c:Lcom/google/c/b/b/a/a/a/a;

    if-eqz v0, :cond_3

    .line 89
    const/4 v0, 0x3

    iget-object v2, p0, Lcom/google/c/b/b/a/a/f;->c:Lcom/google/c/b/b/a/a/a/a;

    invoke-static {v0, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v0

    add-int/2addr v1, v0

    .line 92
    :cond_3
    iget-object v0, p0, Lcom/google/c/b/b/a/a/f;->d:Lcom/google/c/b/b/a/a/g;

    if-eqz v0, :cond_4

    .line 93
    const/4 v0, 0x4

    iget-object v2, p0, Lcom/google/c/b/b/a/a/f;->d:Lcom/google/c/b/b/a/a/g;

    invoke-static {v0, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v0

    add-int/2addr v1, v0

    .line 96
    :cond_4
    return v1
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 5
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/c/b/b/a/a/f;->storeUnknownField(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    const/16 v0, 0xa

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->b(Lcom/google/protobuf/nano/a;I)I

    move-result v2

    iget-object v0, p0, Lcom/google/c/b/b/a/a/f;->a:[Lcom/google/c/b/b/a/a/e;

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lcom/google/c/b/b/a/a/e;

    if-eqz v0, :cond_1

    iget-object v3, p0, Lcom/google/c/b/b/a/a/f;->a:[Lcom/google/c/b/b/a/a/e;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_1
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_3

    new-instance v3, Lcom/google/c/b/b/a/a/e;

    invoke-direct {v3}, Lcom/google/c/b/b/a/a/e;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    iget-object v0, p0, Lcom/google/c/b/b/a/a/f;->a:[Lcom/google/c/b/b/a/a/e;

    array-length v0, v0

    goto :goto_1

    :cond_3
    new-instance v3, Lcom/google/c/b/b/a/a/e;

    invoke-direct {v3}, Lcom/google/c/b/b/a/a/e;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    iput-object v2, p0, Lcom/google/c/b/b/a/a/f;->a:[Lcom/google/c/b/b/a/a/e;

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Lcom/google/c/b/b/a/a/f;->b:Lcom/google/c/b/b/a/a/i;

    if-nez v0, :cond_4

    new-instance v0, Lcom/google/c/b/b/a/a/i;

    invoke-direct {v0}, Lcom/google/c/b/b/a/a/i;-><init>()V

    iput-object v0, p0, Lcom/google/c/b/b/a/a/f;->b:Lcom/google/c/b/b/a/a/i;

    :cond_4
    iget-object v0, p0, Lcom/google/c/b/b/a/a/f;->b:Lcom/google/c/b/b/a/a/i;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Lcom/google/c/b/b/a/a/f;->c:Lcom/google/c/b/b/a/a/a/a;

    if-nez v0, :cond_5

    new-instance v0, Lcom/google/c/b/b/a/a/a/a;

    invoke-direct {v0}, Lcom/google/c/b/b/a/a/a/a;-><init>()V

    iput-object v0, p0, Lcom/google/c/b/b/a/a/f;->c:Lcom/google/c/b/b/a/a/a/a;

    :cond_5
    iget-object v0, p0, Lcom/google/c/b/b/a/a/f;->c:Lcom/google/c/b/b/a/a/a/a;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_4
    iget-object v0, p0, Lcom/google/c/b/b/a/a/f;->d:Lcom/google/c/b/b/a/a/g;

    if-nez v0, :cond_6

    new-instance v0, Lcom/google/c/b/b/a/a/g;

    invoke-direct {v0}, Lcom/google/c/b/b/a/a/g;-><init>()V

    iput-object v0, p0, Lcom/google/c/b/b/a/a/f;->d:Lcom/google/c/b/b/a/a/g;

    :cond_6
    iget-object v0, p0, Lcom/google/c/b/b/a/a/f;->d:Lcom/google/c/b/b/a/a/g;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 3

    .prologue
    .line 52
    iget-object v0, p0, Lcom/google/c/b/b/a/a/f;->a:[Lcom/google/c/b/b/a/a/e;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/c/b/b/a/a/f;->a:[Lcom/google/c/b/b/a/a/e;

    array-length v0, v0

    if-lez v0, :cond_1

    .line 53
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/google/c/b/b/a/a/f;->a:[Lcom/google/c/b/b/a/a/e;

    array-length v1, v1

    if-ge v0, v1, :cond_1

    .line 54
    iget-object v1, p0, Lcom/google/c/b/b/a/a/f;->a:[Lcom/google/c/b/b/a/a/e;

    aget-object v1, v1, v0

    .line 55
    if-eqz v1, :cond_0

    .line 56
    const/4 v2, 0x1

    invoke-virtual {p1, v2, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 53
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 60
    :cond_1
    iget-object v0, p0, Lcom/google/c/b/b/a/a/f;->b:Lcom/google/c/b/b/a/a/i;

    if-eqz v0, :cond_2

    .line 61
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/c/b/b/a/a/f;->b:Lcom/google/c/b/b/a/a/i;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 63
    :cond_2
    iget-object v0, p0, Lcom/google/c/b/b/a/a/f;->c:Lcom/google/c/b/b/a/a/a/a;

    if-eqz v0, :cond_3

    .line 64
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/c/b/b/a/a/f;->c:Lcom/google/c/b/b/a/a/a/a;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 66
    :cond_3
    iget-object v0, p0, Lcom/google/c/b/b/a/a/f;->d:Lcom/google/c/b/b/a/a/g;

    if-eqz v0, :cond_4

    .line 67
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/c/b/b/a/a/f;->d:Lcom/google/c/b/b/a/a/g;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 69
    :cond_4
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/d;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 70
    return-void
.end method
