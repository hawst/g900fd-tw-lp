.class public final Lcom/google/c/b/b/a/a/b/al;
.super Lcom/google/protobuf/nano/d;
.source "SourceFile"


# instance fields
.field public a:Lcom/google/c/b/b/a/a/b/h;

.field public b:Lcom/google/c/b/b/a/a/b/aj;

.field public c:Lcom/google/c/b/b/a/a/b/am;

.field public d:Lcom/google/c/b/b/a/a/b/an;

.field public e:Lcom/google/c/b/b/a/a/b/v;

.field public f:Lcom/google/c/b/b/a/a/b/j;

.field public g:Lcom/google/c/b/b/a/a/b/t;

.field public h:Lcom/google/c/b/b/a/a/b/as;

.field public i:Lcom/google/c/b/b/a/a/b/au;

.field public j:Lcom/google/c/b/b/a/a/b/z;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 53
    invoke-direct {p0}, Lcom/google/protobuf/nano/d;-><init>()V

    .line 54
    iput-object v0, p0, Lcom/google/c/b/b/a/a/b/al;->a:Lcom/google/c/b/b/a/a/b/h;

    iput-object v0, p0, Lcom/google/c/b/b/a/a/b/al;->b:Lcom/google/c/b/b/a/a/b/aj;

    iput-object v0, p0, Lcom/google/c/b/b/a/a/b/al;->c:Lcom/google/c/b/b/a/a/b/am;

    iput-object v0, p0, Lcom/google/c/b/b/a/a/b/al;->d:Lcom/google/c/b/b/a/a/b/an;

    iput-object v0, p0, Lcom/google/c/b/b/a/a/b/al;->e:Lcom/google/c/b/b/a/a/b/v;

    iput-object v0, p0, Lcom/google/c/b/b/a/a/b/al;->f:Lcom/google/c/b/b/a/a/b/j;

    iput-object v0, p0, Lcom/google/c/b/b/a/a/b/al;->g:Lcom/google/c/b/b/a/a/b/t;

    iput-object v0, p0, Lcom/google/c/b/b/a/a/b/al;->h:Lcom/google/c/b/b/a/a/b/as;

    iput-object v0, p0, Lcom/google/c/b/b/a/a/b/al;->i:Lcom/google/c/b/b/a/a/b/au;

    iput-object v0, p0, Lcom/google/c/b/b/a/a/b/al;->j:Lcom/google/c/b/b/a/a/b/z;

    iput-object v0, p0, Lcom/google/c/b/b/a/a/b/al;->unknownFieldData:Lcom/google/protobuf/nano/f;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/c/b/b/a/a/b/al;->cachedSize:I

    .line 55
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 111
    invoke-super {p0}, Lcom/google/protobuf/nano/d;->computeSerializedSize()I

    move-result v0

    .line 112
    iget-object v1, p0, Lcom/google/c/b/b/a/a/b/al;->a:Lcom/google/c/b/b/a/a/b/h;

    if-eqz v1, :cond_0

    .line 113
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/c/b/b/a/a/b/al;->a:Lcom/google/c/b/b/a/a/b/h;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 116
    :cond_0
    iget-object v1, p0, Lcom/google/c/b/b/a/a/b/al;->b:Lcom/google/c/b/b/a/a/b/aj;

    if-eqz v1, :cond_1

    .line 117
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/c/b/b/a/a/b/al;->b:Lcom/google/c/b/b/a/a/b/aj;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 120
    :cond_1
    iget-object v1, p0, Lcom/google/c/b/b/a/a/b/al;->c:Lcom/google/c/b/b/a/a/b/am;

    if-eqz v1, :cond_2

    .line 121
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/c/b/b/a/a/b/al;->c:Lcom/google/c/b/b/a/a/b/am;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 124
    :cond_2
    iget-object v1, p0, Lcom/google/c/b/b/a/a/b/al;->d:Lcom/google/c/b/b/a/a/b/an;

    if-eqz v1, :cond_3

    .line 125
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/c/b/b/a/a/b/al;->d:Lcom/google/c/b/b/a/a/b/an;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 128
    :cond_3
    iget-object v1, p0, Lcom/google/c/b/b/a/a/b/al;->e:Lcom/google/c/b/b/a/a/b/v;

    if-eqz v1, :cond_4

    .line 129
    const/4 v1, 0x5

    iget-object v2, p0, Lcom/google/c/b/b/a/a/b/al;->e:Lcom/google/c/b/b/a/a/b/v;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 132
    :cond_4
    iget-object v1, p0, Lcom/google/c/b/b/a/a/b/al;->f:Lcom/google/c/b/b/a/a/b/j;

    if-eqz v1, :cond_5

    .line 133
    const/4 v1, 0x6

    iget-object v2, p0, Lcom/google/c/b/b/a/a/b/al;->f:Lcom/google/c/b/b/a/a/b/j;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 136
    :cond_5
    iget-object v1, p0, Lcom/google/c/b/b/a/a/b/al;->g:Lcom/google/c/b/b/a/a/b/t;

    if-eqz v1, :cond_6

    .line 137
    const/4 v1, 0x7

    iget-object v2, p0, Lcom/google/c/b/b/a/a/b/al;->g:Lcom/google/c/b/b/a/a/b/t;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 140
    :cond_6
    iget-object v1, p0, Lcom/google/c/b/b/a/a/b/al;->h:Lcom/google/c/b/b/a/a/b/as;

    if-eqz v1, :cond_7

    .line 141
    const/16 v1, 0x8

    iget-object v2, p0, Lcom/google/c/b/b/a/a/b/al;->h:Lcom/google/c/b/b/a/a/b/as;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 144
    :cond_7
    iget-object v1, p0, Lcom/google/c/b/b/a/a/b/al;->i:Lcom/google/c/b/b/a/a/b/au;

    if-eqz v1, :cond_8

    .line 145
    const/16 v1, 0x9

    iget-object v2, p0, Lcom/google/c/b/b/a/a/b/al;->i:Lcom/google/c/b/b/a/a/b/au;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 148
    :cond_8
    iget-object v1, p0, Lcom/google/c/b/b/a/a/b/al;->j:Lcom/google/c/b/b/a/a/b/z;

    if-eqz v1, :cond_9

    .line 149
    const/16 v1, 0xa

    iget-object v2, p0, Lcom/google/c/b/b/a/a/b/al;->j:Lcom/google/c/b/b/a/a/b/z;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 152
    :cond_9
    return v0
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 5
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/c/b/b/a/a/b/al;->storeUnknownField(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Lcom/google/c/b/b/a/a/b/al;->a:Lcom/google/c/b/b/a/a/b/h;

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/c/b/b/a/a/b/h;

    invoke-direct {v0}, Lcom/google/c/b/b/a/a/b/h;-><init>()V

    iput-object v0, p0, Lcom/google/c/b/b/a/a/b/al;->a:Lcom/google/c/b/b/a/a/b/h;

    :cond_1
    iget-object v0, p0, Lcom/google/c/b/b/a/a/b/al;->a:Lcom/google/c/b/b/a/a/b/h;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Lcom/google/c/b/b/a/a/b/al;->b:Lcom/google/c/b/b/a/a/b/aj;

    if-nez v0, :cond_2

    new-instance v0, Lcom/google/c/b/b/a/a/b/aj;

    invoke-direct {v0}, Lcom/google/c/b/b/a/a/b/aj;-><init>()V

    iput-object v0, p0, Lcom/google/c/b/b/a/a/b/al;->b:Lcom/google/c/b/b/a/a/b/aj;

    :cond_2
    iget-object v0, p0, Lcom/google/c/b/b/a/a/b/al;->b:Lcom/google/c/b/b/a/a/b/aj;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Lcom/google/c/b/b/a/a/b/al;->c:Lcom/google/c/b/b/a/a/b/am;

    if-nez v0, :cond_3

    new-instance v0, Lcom/google/c/b/b/a/a/b/am;

    invoke-direct {v0}, Lcom/google/c/b/b/a/a/b/am;-><init>()V

    iput-object v0, p0, Lcom/google/c/b/b/a/a/b/al;->c:Lcom/google/c/b/b/a/a/b/am;

    :cond_3
    iget-object v0, p0, Lcom/google/c/b/b/a/a/b/al;->c:Lcom/google/c/b/b/a/a/b/am;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_4
    iget-object v0, p0, Lcom/google/c/b/b/a/a/b/al;->d:Lcom/google/c/b/b/a/a/b/an;

    if-nez v0, :cond_4

    new-instance v0, Lcom/google/c/b/b/a/a/b/an;

    invoke-direct {v0}, Lcom/google/c/b/b/a/a/b/an;-><init>()V

    iput-object v0, p0, Lcom/google/c/b/b/a/a/b/al;->d:Lcom/google/c/b/b/a/a/b/an;

    :cond_4
    iget-object v0, p0, Lcom/google/c/b/b/a/a/b/al;->d:Lcom/google/c/b/b/a/a/b/an;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_5
    iget-object v0, p0, Lcom/google/c/b/b/a/a/b/al;->e:Lcom/google/c/b/b/a/a/b/v;

    if-nez v0, :cond_5

    new-instance v0, Lcom/google/c/b/b/a/a/b/v;

    invoke-direct {v0}, Lcom/google/c/b/b/a/a/b/v;-><init>()V

    iput-object v0, p0, Lcom/google/c/b/b/a/a/b/al;->e:Lcom/google/c/b/b/a/a/b/v;

    :cond_5
    iget-object v0, p0, Lcom/google/c/b/b/a/a/b/al;->e:Lcom/google/c/b/b/a/a/b/v;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_6
    iget-object v0, p0, Lcom/google/c/b/b/a/a/b/al;->f:Lcom/google/c/b/b/a/a/b/j;

    if-nez v0, :cond_6

    new-instance v0, Lcom/google/c/b/b/a/a/b/j;

    invoke-direct {v0}, Lcom/google/c/b/b/a/a/b/j;-><init>()V

    iput-object v0, p0, Lcom/google/c/b/b/a/a/b/al;->f:Lcom/google/c/b/b/a/a/b/j;

    :cond_6
    iget-object v0, p0, Lcom/google/c/b/b/a/a/b/al;->f:Lcom/google/c/b/b/a/a/b/j;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_7
    iget-object v0, p0, Lcom/google/c/b/b/a/a/b/al;->g:Lcom/google/c/b/b/a/a/b/t;

    if-nez v0, :cond_7

    new-instance v0, Lcom/google/c/b/b/a/a/b/t;

    invoke-direct {v0}, Lcom/google/c/b/b/a/a/b/t;-><init>()V

    iput-object v0, p0, Lcom/google/c/b/b/a/a/b/al;->g:Lcom/google/c/b/b/a/a/b/t;

    :cond_7
    iget-object v0, p0, Lcom/google/c/b/b/a/a/b/al;->g:Lcom/google/c/b/b/a/a/b/t;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto/16 :goto_0

    :sswitch_8
    iget-object v0, p0, Lcom/google/c/b/b/a/a/b/al;->h:Lcom/google/c/b/b/a/a/b/as;

    if-nez v0, :cond_8

    new-instance v0, Lcom/google/c/b/b/a/a/b/as;

    invoke-direct {v0}, Lcom/google/c/b/b/a/a/b/as;-><init>()V

    iput-object v0, p0, Lcom/google/c/b/b/a/a/b/al;->h:Lcom/google/c/b/b/a/a/b/as;

    :cond_8
    iget-object v0, p0, Lcom/google/c/b/b/a/a/b/al;->h:Lcom/google/c/b/b/a/a/b/as;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto/16 :goto_0

    :sswitch_9
    iget-object v0, p0, Lcom/google/c/b/b/a/a/b/al;->i:Lcom/google/c/b/b/a/a/b/au;

    if-nez v0, :cond_9

    new-instance v0, Lcom/google/c/b/b/a/a/b/au;

    invoke-direct {v0}, Lcom/google/c/b/b/a/a/b/au;-><init>()V

    iput-object v0, p0, Lcom/google/c/b/b/a/a/b/al;->i:Lcom/google/c/b/b/a/a/b/au;

    :cond_9
    iget-object v0, p0, Lcom/google/c/b/b/a/a/b/al;->i:Lcom/google/c/b/b/a/a/b/au;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto/16 :goto_0

    :sswitch_a
    iget-object v0, p0, Lcom/google/c/b/b/a/a/b/al;->j:Lcom/google/c/b/b/a/a/b/z;

    if-nez v0, :cond_a

    new-instance v0, Lcom/google/c/b/b/a/a/b/z;

    invoke-direct {v0}, Lcom/google/c/b/b/a/a/b/z;-><init>()V

    iput-object v0, p0, Lcom/google/c/b/b/a/a/b/al;->j:Lcom/google/c/b/b/a/a/b/z;

    :cond_a
    iget-object v0, p0, Lcom/google/c/b/b/a/a/b/al;->j:Lcom/google/c/b/b/a/a/b/z;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x42 -> :sswitch_8
        0x4a -> :sswitch_9
        0x52 -> :sswitch_a
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 76
    iget-object v0, p0, Lcom/google/c/b/b/a/a/b/al;->a:Lcom/google/c/b/b/a/a/b/h;

    if-eqz v0, :cond_0

    .line 77
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/c/b/b/a/a/b/al;->a:Lcom/google/c/b/b/a/a/b/h;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 79
    :cond_0
    iget-object v0, p0, Lcom/google/c/b/b/a/a/b/al;->b:Lcom/google/c/b/b/a/a/b/aj;

    if-eqz v0, :cond_1

    .line 80
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/c/b/b/a/a/b/al;->b:Lcom/google/c/b/b/a/a/b/aj;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 82
    :cond_1
    iget-object v0, p0, Lcom/google/c/b/b/a/a/b/al;->c:Lcom/google/c/b/b/a/a/b/am;

    if-eqz v0, :cond_2

    .line 83
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/c/b/b/a/a/b/al;->c:Lcom/google/c/b/b/a/a/b/am;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 85
    :cond_2
    iget-object v0, p0, Lcom/google/c/b/b/a/a/b/al;->d:Lcom/google/c/b/b/a/a/b/an;

    if-eqz v0, :cond_3

    .line 86
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/c/b/b/a/a/b/al;->d:Lcom/google/c/b/b/a/a/b/an;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 88
    :cond_3
    iget-object v0, p0, Lcom/google/c/b/b/a/a/b/al;->e:Lcom/google/c/b/b/a/a/b/v;

    if-eqz v0, :cond_4

    .line 89
    const/4 v0, 0x5

    iget-object v1, p0, Lcom/google/c/b/b/a/a/b/al;->e:Lcom/google/c/b/b/a/a/b/v;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 91
    :cond_4
    iget-object v0, p0, Lcom/google/c/b/b/a/a/b/al;->f:Lcom/google/c/b/b/a/a/b/j;

    if-eqz v0, :cond_5

    .line 92
    const/4 v0, 0x6

    iget-object v1, p0, Lcom/google/c/b/b/a/a/b/al;->f:Lcom/google/c/b/b/a/a/b/j;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 94
    :cond_5
    iget-object v0, p0, Lcom/google/c/b/b/a/a/b/al;->g:Lcom/google/c/b/b/a/a/b/t;

    if-eqz v0, :cond_6

    .line 95
    const/4 v0, 0x7

    iget-object v1, p0, Lcom/google/c/b/b/a/a/b/al;->g:Lcom/google/c/b/b/a/a/b/t;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 97
    :cond_6
    iget-object v0, p0, Lcom/google/c/b/b/a/a/b/al;->h:Lcom/google/c/b/b/a/a/b/as;

    if-eqz v0, :cond_7

    .line 98
    const/16 v0, 0x8

    iget-object v1, p0, Lcom/google/c/b/b/a/a/b/al;->h:Lcom/google/c/b/b/a/a/b/as;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 100
    :cond_7
    iget-object v0, p0, Lcom/google/c/b/b/a/a/b/al;->i:Lcom/google/c/b/b/a/a/b/au;

    if-eqz v0, :cond_8

    .line 101
    const/16 v0, 0x9

    iget-object v1, p0, Lcom/google/c/b/b/a/a/b/al;->i:Lcom/google/c/b/b/a/a/b/au;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 103
    :cond_8
    iget-object v0, p0, Lcom/google/c/b/b/a/a/b/al;->j:Lcom/google/c/b/b/a/a/b/z;

    if-eqz v0, :cond_9

    .line 104
    const/16 v0, 0xa

    iget-object v1, p0, Lcom/google/c/b/b/a/a/b/al;->j:Lcom/google/c/b/b/a/a/b/z;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 106
    :cond_9
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/d;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 107
    return-void
.end method
