.class public final Lcom/google/c/b/b/a/a/b/r;
.super Lcom/google/protobuf/nano/d;
.source "SourceFile"


# instance fields
.field public a:Ljava/lang/Integer;

.field public b:Ljava/lang/Integer;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 903
    invoke-direct {p0}, Lcom/google/protobuf/nano/d;-><init>()V

    .line 904
    iput-object v0, p0, Lcom/google/c/b/b/a/a/b/r;->a:Ljava/lang/Integer;

    iput-object v0, p0, Lcom/google/c/b/b/a/a/b/r;->b:Ljava/lang/Integer;

    iput-object v0, p0, Lcom/google/c/b/b/a/a/b/r;->unknownFieldData:Lcom/google/protobuf/nano/f;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/c/b/b/a/a/b/r;->cachedSize:I

    .line 905
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 929
    invoke-super {p0}, Lcom/google/protobuf/nano/d;->computeSerializedSize()I

    move-result v0

    .line 930
    iget-object v1, p0, Lcom/google/c/b/b/a/a/b/r;->a:Ljava/lang/Integer;

    if-eqz v1, :cond_0

    .line 931
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/c/b/b/a/a/b/r;->a:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->g(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 934
    :cond_0
    iget-object v1, p0, Lcom/google/c/b/b/a/a/b/r;->b:Ljava/lang/Integer;

    if-eqz v1, :cond_1

    .line 935
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/c/b/b/a/a/b/r;->b:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->g(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 938
    :cond_1
    return v0
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 880
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/c/b/b/a/a/b/r;->storeUnknownField(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/c/b/b/a/a/b/r;->a:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/c/b/b/a/a/b/r;->b:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 918
    iget-object v0, p0, Lcom/google/c/b/b/a/a/b/r;->a:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 919
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/c/b/b/a/a/b/r;->a:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->c(II)V

    .line 921
    :cond_0
    iget-object v0, p0, Lcom/google/c/b/b/a/a/b/r;->b:Ljava/lang/Integer;

    if-eqz v0, :cond_1

    .line 922
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/c/b/b/a/a/b/r;->b:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->c(II)V

    .line 924
    :cond_1
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/d;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 925
    return-void
.end method
