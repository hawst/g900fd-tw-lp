.class public final Lcom/google/c/b/b/a/a/b/ai;
.super Lcom/google/protobuf/nano/d;
.source "SourceFile"


# instance fields
.field public a:Ljava/lang/Integer;

.field public b:Ljava/lang/Long;

.field public c:Ljava/lang/Integer;

.field public d:Ljava/lang/Integer;

.field public e:Ljava/lang/Integer;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 990
    invoke-direct {p0}, Lcom/google/protobuf/nano/d;-><init>()V

    .line 991
    iput-object v0, p0, Lcom/google/c/b/b/a/a/b/ai;->a:Ljava/lang/Integer;

    iput-object v0, p0, Lcom/google/c/b/b/a/a/b/ai;->b:Ljava/lang/Long;

    iput-object v0, p0, Lcom/google/c/b/b/a/a/b/ai;->c:Ljava/lang/Integer;

    iput-object v0, p0, Lcom/google/c/b/b/a/a/b/ai;->d:Ljava/lang/Integer;

    iput-object v0, p0, Lcom/google/c/b/b/a/a/b/ai;->e:Ljava/lang/Integer;

    iput-object v0, p0, Lcom/google/c/b/b/a/a/b/ai;->unknownFieldData:Lcom/google/protobuf/nano/f;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/c/b/b/a/a/b/ai;->cachedSize:I

    .line 992
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 4

    .prologue
    .line 1028
    invoke-super {p0}, Lcom/google/protobuf/nano/d;->computeSerializedSize()I

    move-result v0

    .line 1029
    iget-object v1, p0, Lcom/google/c/b/b/a/a/b/ai;->a:Ljava/lang/Integer;

    if-eqz v1, :cond_0

    .line 1030
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/c/b/b/a/a/b/ai;->a:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1033
    :cond_0
    iget-object v1, p0, Lcom/google/c/b/b/a/a/b/ai;->b:Ljava/lang/Long;

    if-eqz v1, :cond_1

    .line 1034
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/c/b/b/a/a/b/ai;->b:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/b;->e(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 1037
    :cond_1
    iget-object v1, p0, Lcom/google/c/b/b/a/a/b/ai;->c:Ljava/lang/Integer;

    if-eqz v1, :cond_2

    .line 1038
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/c/b/b/a/a/b/ai;->c:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->g(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1041
    :cond_2
    iget-object v1, p0, Lcom/google/c/b/b/a/a/b/ai;->d:Ljava/lang/Integer;

    if-eqz v1, :cond_3

    .line 1042
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/c/b/b/a/a/b/ai;->d:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->g(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1045
    :cond_3
    iget-object v1, p0, Lcom/google/c/b/b/a/a/b/ai;->e:Ljava/lang/Integer;

    if-eqz v1, :cond_4

    .line 1046
    const/4 v1, 0x5

    iget-object v2, p0, Lcom/google/c/b/b/a/a/b/ai;->e:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1049
    :cond_4
    return v0
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 2

    .prologue
    .line 958
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/c/b/b/a/a/b/ai;->storeUnknownField(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/c/b/b/a/a/b/ai;->a:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->j()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/google/c/b/b/a/a/b/ai;->b:Ljava/lang/Long;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/c/b/b/a/a/b/ai;->c:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/c/b/b/a/a/b/ai;->d:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    packed-switch v0, :pswitch_data_1

    goto :goto_0

    :pswitch_1
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/c/b/b/a/a/b/ai;->e:Ljava/lang/Integer;

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 4

    .prologue
    .line 1008
    iget-object v0, p0, Lcom/google/c/b/b/a/a/b/ai;->a:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 1009
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/c/b/b/a/a/b/ai;->a:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 1011
    :cond_0
    iget-object v0, p0, Lcom/google/c/b/b/a/a/b/ai;->b:Ljava/lang/Long;

    if-eqz v0, :cond_1

    .line 1012
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/c/b/b/a/a/b/ai;->b:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/b;->a(IJ)V

    .line 1014
    :cond_1
    iget-object v0, p0, Lcom/google/c/b/b/a/a/b/ai;->c:Ljava/lang/Integer;

    if-eqz v0, :cond_2

    .line 1015
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/c/b/b/a/a/b/ai;->c:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->c(II)V

    .line 1017
    :cond_2
    iget-object v0, p0, Lcom/google/c/b/b/a/a/b/ai;->d:Ljava/lang/Integer;

    if-eqz v0, :cond_3

    .line 1018
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/c/b/b/a/a/b/ai;->d:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->c(II)V

    .line 1020
    :cond_3
    iget-object v0, p0, Lcom/google/c/b/b/a/a/b/ai;->e:Ljava/lang/Integer;

    if-eqz v0, :cond_4

    .line 1021
    const/4 v0, 0x5

    iget-object v1, p0, Lcom/google/c/b/b/a/a/b/ai;->e:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 1023
    :cond_4
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/d;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 1024
    return-void
.end method
