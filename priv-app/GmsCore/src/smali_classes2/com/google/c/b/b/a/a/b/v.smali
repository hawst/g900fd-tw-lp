.class public final Lcom/google/c/b/b/a/a/b/v;
.super Lcom/google/protobuf/nano/d;
.source "SourceFile"


# instance fields
.field public a:Ljava/lang/Integer;

.field public b:Lcom/google/c/b/b/a/a/b/y;

.field public c:Lcom/google/c/b/b/a/a/b/x;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 362
    invoke-direct {p0}, Lcom/google/protobuf/nano/d;-><init>()V

    .line 363
    iput-object v0, p0, Lcom/google/c/b/b/a/a/b/v;->a:Ljava/lang/Integer;

    iput-object v0, p0, Lcom/google/c/b/b/a/a/b/v;->b:Lcom/google/c/b/b/a/a/b/y;

    iput-object v0, p0, Lcom/google/c/b/b/a/a/b/v;->c:Lcom/google/c/b/b/a/a/b/x;

    iput-object v0, p0, Lcom/google/c/b/b/a/a/b/v;->unknownFieldData:Lcom/google/protobuf/nano/f;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/c/b/b/a/a/b/v;->cachedSize:I

    .line 364
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 392
    invoke-super {p0}, Lcom/google/protobuf/nano/d;->computeSerializedSize()I

    move-result v0

    .line 393
    iget-object v1, p0, Lcom/google/c/b/b/a/a/b/v;->a:Ljava/lang/Integer;

    if-eqz v1, :cond_0

    .line 394
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/c/b/b/a/a/b/v;->a:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 397
    :cond_0
    iget-object v1, p0, Lcom/google/c/b/b/a/a/b/v;->b:Lcom/google/c/b/b/a/a/b/y;

    if-eqz v1, :cond_1

    .line 398
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/c/b/b/a/a/b/v;->b:Lcom/google/c/b/b/a/a/b/y;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 401
    :cond_1
    iget-object v1, p0, Lcom/google/c/b/b/a/a/b/v;->c:Lcom/google/c/b/b/a/a/b/x;

    if-eqz v1, :cond_2

    .line 402
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/c/b/b/a/a/b/v;->c:Lcom/google/c/b/b/a/a/b/x;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 405
    :cond_2
    return v0
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 5
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/c/b/b/a/a/b/v;->storeUnknownField(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/c/b/b/a/a/b/v;->a:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Lcom/google/c/b/b/a/a/b/v;->b:Lcom/google/c/b/b/a/a/b/y;

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/c/b/b/a/a/b/y;

    invoke-direct {v0}, Lcom/google/c/b/b/a/a/b/y;-><init>()V

    iput-object v0, p0, Lcom/google/c/b/b/a/a/b/v;->b:Lcom/google/c/b/b/a/a/b/y;

    :cond_1
    iget-object v0, p0, Lcom/google/c/b/b/a/a/b/v;->b:Lcom/google/c/b/b/a/a/b/y;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Lcom/google/c/b/b/a/a/b/v;->c:Lcom/google/c/b/b/a/a/b/x;

    if-nez v0, :cond_2

    new-instance v0, Lcom/google/c/b/b/a/a/b/x;

    invoke-direct {v0}, Lcom/google/c/b/b/a/a/b/x;-><init>()V

    iput-object v0, p0, Lcom/google/c/b/b/a/a/b/v;->c:Lcom/google/c/b/b/a/a/b/x;

    :cond_2
    iget-object v0, p0, Lcom/google/c/b/b/a/a/b/v;->c:Lcom/google/c/b/b/a/a/b/x;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 378
    iget-object v0, p0, Lcom/google/c/b/b/a/a/b/v;->a:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 379
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/c/b/b/a/a/b/v;->a:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 381
    :cond_0
    iget-object v0, p0, Lcom/google/c/b/b/a/a/b/v;->b:Lcom/google/c/b/b/a/a/b/y;

    if-eqz v0, :cond_1

    .line 382
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/c/b/b/a/a/b/v;->b:Lcom/google/c/b/b/a/a/b/y;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 384
    :cond_1
    iget-object v0, p0, Lcom/google/c/b/b/a/a/b/v;->c:Lcom/google/c/b/b/a/a/b/x;

    if-eqz v0, :cond_2

    .line 385
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/c/b/b/a/a/b/v;->c:Lcom/google/c/b/b/a/a/b/x;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 387
    :cond_2
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/d;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 388
    return-void
.end method
