.class public final Lcom/google/c/b/b/a/a/b/j;
.super Lcom/google/protobuf/nano/d;
.source "SourceFile"


# instance fields
.field public a:Lcom/google/c/b/b/a/a/b/s;

.field public b:Lcom/google/c/b/b/a/a/b/k;

.field public c:Lcom/google/c/b/b/a/a/b/k;

.field public d:[I

.field public e:[I

.field public f:[I

.field public g:Lcom/google/c/b/b/a/a/b/o;

.field public h:Lcom/google/c/b/b/a/a/b/l;

.field public i:Lcom/google/c/b/b/a/a/b/q;

.field public j:Lcom/google/c/b/b/a/a/b/r;

.field public k:Lcom/google/c/b/b/a/a/b/m;

.field public l:Lcom/google/c/b/b/a/a/b/p;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1351
    invoke-direct {p0}, Lcom/google/protobuf/nano/d;-><init>()V

    .line 1352
    iput-object v1, p0, Lcom/google/c/b/b/a/a/b/j;->a:Lcom/google/c/b/b/a/a/b/s;

    iput-object v1, p0, Lcom/google/c/b/b/a/a/b/j;->b:Lcom/google/c/b/b/a/a/b/k;

    iput-object v1, p0, Lcom/google/c/b/b/a/a/b/j;->c:Lcom/google/c/b/b/a/a/b/k;

    sget-object v0, Lcom/google/protobuf/nano/m;->a:[I

    iput-object v0, p0, Lcom/google/c/b/b/a/a/b/j;->d:[I

    sget-object v0, Lcom/google/protobuf/nano/m;->a:[I

    iput-object v0, p0, Lcom/google/c/b/b/a/a/b/j;->e:[I

    sget-object v0, Lcom/google/protobuf/nano/m;->a:[I

    iput-object v0, p0, Lcom/google/c/b/b/a/a/b/j;->f:[I

    iput-object v1, p0, Lcom/google/c/b/b/a/a/b/j;->g:Lcom/google/c/b/b/a/a/b/o;

    iput-object v1, p0, Lcom/google/c/b/b/a/a/b/j;->h:Lcom/google/c/b/b/a/a/b/l;

    iput-object v1, p0, Lcom/google/c/b/b/a/a/b/j;->i:Lcom/google/c/b/b/a/a/b/q;

    iput-object v1, p0, Lcom/google/c/b/b/a/a/b/j;->j:Lcom/google/c/b/b/a/a/b/r;

    iput-object v1, p0, Lcom/google/c/b/b/a/a/b/j;->k:Lcom/google/c/b/b/a/a/b/m;

    iput-object v1, p0, Lcom/google/c/b/b/a/a/b/j;->l:Lcom/google/c/b/b/a/a/b/p;

    iput-object v1, p0, Lcom/google/c/b/b/a/a/b/j;->unknownFieldData:Lcom/google/protobuf/nano/f;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/c/b/b/a/a/b/j;->cachedSize:I

    .line 1353
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 1423
    invoke-super {p0}, Lcom/google/protobuf/nano/d;->computeSerializedSize()I

    move-result v0

    .line 1424
    iget-object v1, p0, Lcom/google/c/b/b/a/a/b/j;->a:Lcom/google/c/b/b/a/a/b/s;

    if-eqz v1, :cond_0

    .line 1425
    const/4 v1, 0x1

    iget-object v3, p0, Lcom/google/c/b/b/a/a/b/j;->a:Lcom/google/c/b/b/a/a/b/s;

    invoke-static {v1, v3}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1428
    :cond_0
    iget-object v1, p0, Lcom/google/c/b/b/a/a/b/j;->b:Lcom/google/c/b/b/a/a/b/k;

    if-eqz v1, :cond_1

    .line 1429
    const/4 v1, 0x2

    iget-object v3, p0, Lcom/google/c/b/b/a/a/b/j;->b:Lcom/google/c/b/b/a/a/b/k;

    invoke-static {v1, v3}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1432
    :cond_1
    iget-object v1, p0, Lcom/google/c/b/b/a/a/b/j;->c:Lcom/google/c/b/b/a/a/b/k;

    if-eqz v1, :cond_2

    .line 1433
    const/4 v1, 0x3

    iget-object v3, p0, Lcom/google/c/b/b/a/a/b/j;->c:Lcom/google/c/b/b/a/a/b/k;

    invoke-static {v1, v3}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1436
    :cond_2
    iget-object v1, p0, Lcom/google/c/b/b/a/a/b/j;->d:[I

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/google/c/b/b/a/a/b/j;->d:[I

    array-length v1, v1

    if-lez v1, :cond_4

    move v1, v2

    move v3, v2

    .line 1438
    :goto_0
    iget-object v4, p0, Lcom/google/c/b/b/a/a/b/j;->d:[I

    array-length v4, v4

    if-ge v1, v4, :cond_3

    .line 1439
    iget-object v4, p0, Lcom/google/c/b/b/a/a/b/j;->d:[I

    aget v4, v4, v1

    .line 1440
    invoke-static {v4}, Lcom/google/protobuf/nano/b;->b(I)I

    move-result v4

    add-int/2addr v3, v4

    .line 1438
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1443
    :cond_3
    add-int/2addr v0, v3

    .line 1444
    iget-object v1, p0, Lcom/google/c/b/b/a/a/b/j;->d:[I

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 1446
    :cond_4
    iget-object v1, p0, Lcom/google/c/b/b/a/a/b/j;->e:[I

    if-eqz v1, :cond_6

    iget-object v1, p0, Lcom/google/c/b/b/a/a/b/j;->e:[I

    array-length v1, v1

    if-lez v1, :cond_6

    move v1, v2

    move v3, v2

    .line 1448
    :goto_1
    iget-object v4, p0, Lcom/google/c/b/b/a/a/b/j;->e:[I

    array-length v4, v4

    if-ge v1, v4, :cond_5

    .line 1449
    iget-object v4, p0, Lcom/google/c/b/b/a/a/b/j;->e:[I

    aget v4, v4, v1

    .line 1450
    invoke-static {v4}, Lcom/google/protobuf/nano/b;->b(I)I

    move-result v4

    add-int/2addr v3, v4

    .line 1448
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 1453
    :cond_5
    add-int/2addr v0, v3

    .line 1454
    iget-object v1, p0, Lcom/google/c/b/b/a/a/b/j;->e:[I

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 1456
    :cond_6
    iget-object v1, p0, Lcom/google/c/b/b/a/a/b/j;->f:[I

    if-eqz v1, :cond_8

    iget-object v1, p0, Lcom/google/c/b/b/a/a/b/j;->f:[I

    array-length v1, v1

    if-lez v1, :cond_8

    move v1, v2

    .line 1458
    :goto_2
    iget-object v3, p0, Lcom/google/c/b/b/a/a/b/j;->f:[I

    array-length v3, v3

    if-ge v2, v3, :cond_7

    .line 1459
    iget-object v3, p0, Lcom/google/c/b/b/a/a/b/j;->f:[I

    aget v3, v3, v2

    .line 1460
    invoke-static {v3}, Lcom/google/protobuf/nano/b;->b(I)I

    move-result v3

    add-int/2addr v1, v3

    .line 1458
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 1463
    :cond_7
    add-int/2addr v0, v1

    .line 1464
    iget-object v1, p0, Lcom/google/c/b/b/a/a/b/j;->f:[I

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 1466
    :cond_8
    iget-object v1, p0, Lcom/google/c/b/b/a/a/b/j;->g:Lcom/google/c/b/b/a/a/b/o;

    if-eqz v1, :cond_9

    .line 1467
    const/4 v1, 0x7

    iget-object v2, p0, Lcom/google/c/b/b/a/a/b/j;->g:Lcom/google/c/b/b/a/a/b/o;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1470
    :cond_9
    iget-object v1, p0, Lcom/google/c/b/b/a/a/b/j;->h:Lcom/google/c/b/b/a/a/b/l;

    if-eqz v1, :cond_a

    .line 1471
    const/16 v1, 0x8

    iget-object v2, p0, Lcom/google/c/b/b/a/a/b/j;->h:Lcom/google/c/b/b/a/a/b/l;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1474
    :cond_a
    iget-object v1, p0, Lcom/google/c/b/b/a/a/b/j;->i:Lcom/google/c/b/b/a/a/b/q;

    if-eqz v1, :cond_b

    .line 1475
    const/16 v1, 0x9

    iget-object v2, p0, Lcom/google/c/b/b/a/a/b/j;->i:Lcom/google/c/b/b/a/a/b/q;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1478
    :cond_b
    iget-object v1, p0, Lcom/google/c/b/b/a/a/b/j;->j:Lcom/google/c/b/b/a/a/b/r;

    if-eqz v1, :cond_c

    .line 1479
    const/16 v1, 0xa

    iget-object v2, p0, Lcom/google/c/b/b/a/a/b/j;->j:Lcom/google/c/b/b/a/a/b/r;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1482
    :cond_c
    iget-object v1, p0, Lcom/google/c/b/b/a/a/b/j;->k:Lcom/google/c/b/b/a/a/b/m;

    if-eqz v1, :cond_d

    .line 1483
    const/16 v1, 0xb

    iget-object v2, p0, Lcom/google/c/b/b/a/a/b/j;->k:Lcom/google/c/b/b/a/a/b/m;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1486
    :cond_d
    iget-object v1, p0, Lcom/google/c/b/b/a/a/b/j;->l:Lcom/google/c/b/b/a/a/b/p;

    if-eqz v1, :cond_e

    .line 1487
    const/16 v1, 0xc

    iget-object v2, p0, Lcom/google/c/b/b/a/a/b/j;->l:Lcom/google/c/b/b/a/a/b/p;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1490
    :cond_e
    return v0
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 5
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/c/b/b/a/a/b/j;->storeUnknownField(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Lcom/google/c/b/b/a/a/b/j;->a:Lcom/google/c/b/b/a/a/b/s;

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/c/b/b/a/a/b/s;

    invoke-direct {v0}, Lcom/google/c/b/b/a/a/b/s;-><init>()V

    iput-object v0, p0, Lcom/google/c/b/b/a/a/b/j;->a:Lcom/google/c/b/b/a/a/b/s;

    :cond_1
    iget-object v0, p0, Lcom/google/c/b/b/a/a/b/j;->a:Lcom/google/c/b/b/a/a/b/s;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Lcom/google/c/b/b/a/a/b/j;->b:Lcom/google/c/b/b/a/a/b/k;

    if-nez v0, :cond_2

    new-instance v0, Lcom/google/c/b/b/a/a/b/k;

    invoke-direct {v0}, Lcom/google/c/b/b/a/a/b/k;-><init>()V

    iput-object v0, p0, Lcom/google/c/b/b/a/a/b/j;->b:Lcom/google/c/b/b/a/a/b/k;

    :cond_2
    iget-object v0, p0, Lcom/google/c/b/b/a/a/b/j;->b:Lcom/google/c/b/b/a/a/b/k;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Lcom/google/c/b/b/a/a/b/j;->c:Lcom/google/c/b/b/a/a/b/k;

    if-nez v0, :cond_3

    new-instance v0, Lcom/google/c/b/b/a/a/b/k;

    invoke-direct {v0}, Lcom/google/c/b/b/a/a/b/k;-><init>()V

    iput-object v0, p0, Lcom/google/c/b/b/a/a/b/j;->c:Lcom/google/c/b/b/a/a/b/k;

    :cond_3
    iget-object v0, p0, Lcom/google/c/b/b/a/a/b/j;->c:Lcom/google/c/b/b/a/a/b/k;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_4
    const/16 v0, 0x20

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->b(Lcom/google/protobuf/nano/a;I)I

    move-result v4

    new-array v5, v4, [I

    move v3, v2

    move v1, v2

    :goto_1
    if-ge v3, v4, :cond_5

    if-eqz v3, :cond_4

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    :cond_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v6

    packed-switch v6, :pswitch_data_0

    move v0, v1

    :goto_2
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    move v1, v0

    goto :goto_1

    :pswitch_0
    add-int/lit8 v0, v1, 0x1

    aput v6, v5, v1

    goto :goto_2

    :cond_5
    if-eqz v1, :cond_0

    iget-object v0, p0, Lcom/google/c/b/b/a/a/b/j;->d:[I

    if-nez v0, :cond_6

    move v0, v2

    :goto_3
    if-nez v0, :cond_7

    array-length v3, v5

    if-ne v1, v3, :cond_7

    iput-object v5, p0, Lcom/google/c/b/b/a/a/b/j;->d:[I

    goto :goto_0

    :cond_6
    iget-object v0, p0, Lcom/google/c/b/b/a/a/b/j;->d:[I

    array-length v0, v0

    goto :goto_3

    :cond_7
    add-int v3, v0, v1

    new-array v3, v3, [I

    if-eqz v0, :cond_8

    iget-object v4, p0, Lcom/google/c/b/b/a/a/b/j;->d:[I

    invoke-static {v4, v2, v3, v2, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_8
    invoke-static {v5, v2, v3, v0, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iput-object v3, p0, Lcom/google/c/b/b/a/a/b/j;->d:[I

    goto/16 :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->c(I)I

    move-result v3

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->o()I

    move-result v1

    move v0, v2

    :goto_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->m()I

    move-result v4

    if-lez v4, :cond_9

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v4

    packed-switch v4, :pswitch_data_1

    goto :goto_4

    :pswitch_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_9
    if-eqz v0, :cond_d

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/a;->e(I)V

    iget-object v1, p0, Lcom/google/c/b/b/a/a/b/j;->d:[I

    if-nez v1, :cond_b

    move v1, v2

    :goto_5
    add-int/2addr v0, v1

    new-array v4, v0, [I

    if-eqz v1, :cond_a

    iget-object v0, p0, Lcom/google/c/b/b/a/a/b/j;->d:[I

    invoke-static {v0, v2, v4, v2, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_a
    :goto_6
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->m()I

    move-result v0

    if-lez v0, :cond_c

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v5

    packed-switch v5, :pswitch_data_2

    goto :goto_6

    :pswitch_2
    add-int/lit8 v0, v1, 0x1

    aput v5, v4, v1

    move v1, v0

    goto :goto_6

    :cond_b
    iget-object v1, p0, Lcom/google/c/b/b/a/a/b/j;->d:[I

    array-length v1, v1

    goto :goto_5

    :cond_c
    iput-object v4, p0, Lcom/google/c/b/b/a/a/b/j;->d:[I

    :cond_d
    invoke-virtual {p1, v3}, Lcom/google/protobuf/nano/a;->d(I)V

    goto/16 :goto_0

    :sswitch_6
    const/16 v0, 0x28

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->b(Lcom/google/protobuf/nano/a;I)I

    move-result v4

    new-array v5, v4, [I

    move v3, v2

    move v1, v2

    :goto_7
    if-ge v3, v4, :cond_f

    if-eqz v3, :cond_e

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    :cond_e
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v6

    packed-switch v6, :pswitch_data_3

    move v0, v1

    :goto_8
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    move v1, v0

    goto :goto_7

    :pswitch_3
    add-int/lit8 v0, v1, 0x1

    aput v6, v5, v1

    goto :goto_8

    :cond_f
    if-eqz v1, :cond_0

    iget-object v0, p0, Lcom/google/c/b/b/a/a/b/j;->e:[I

    if-nez v0, :cond_10

    move v0, v2

    :goto_9
    if-nez v0, :cond_11

    array-length v3, v5

    if-ne v1, v3, :cond_11

    iput-object v5, p0, Lcom/google/c/b/b/a/a/b/j;->e:[I

    goto/16 :goto_0

    :cond_10
    iget-object v0, p0, Lcom/google/c/b/b/a/a/b/j;->e:[I

    array-length v0, v0

    goto :goto_9

    :cond_11
    add-int v3, v0, v1

    new-array v3, v3, [I

    if-eqz v0, :cond_12

    iget-object v4, p0, Lcom/google/c/b/b/a/a/b/j;->e:[I

    invoke-static {v4, v2, v3, v2, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_12
    invoke-static {v5, v2, v3, v0, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iput-object v3, p0, Lcom/google/c/b/b/a/a/b/j;->e:[I

    goto/16 :goto_0

    :sswitch_7
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->c(I)I

    move-result v3

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->o()I

    move-result v1

    move v0, v2

    :goto_a
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->m()I

    move-result v4

    if-lez v4, :cond_13

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v4

    packed-switch v4, :pswitch_data_4

    goto :goto_a

    :pswitch_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_a

    :cond_13
    if-eqz v0, :cond_17

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/a;->e(I)V

    iget-object v1, p0, Lcom/google/c/b/b/a/a/b/j;->e:[I

    if-nez v1, :cond_15

    move v1, v2

    :goto_b
    add-int/2addr v0, v1

    new-array v4, v0, [I

    if-eqz v1, :cond_14

    iget-object v0, p0, Lcom/google/c/b/b/a/a/b/j;->e:[I

    invoke-static {v0, v2, v4, v2, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_14
    :goto_c
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->m()I

    move-result v0

    if-lez v0, :cond_16

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v5

    packed-switch v5, :pswitch_data_5

    goto :goto_c

    :pswitch_5
    add-int/lit8 v0, v1, 0x1

    aput v5, v4, v1

    move v1, v0

    goto :goto_c

    :cond_15
    iget-object v1, p0, Lcom/google/c/b/b/a/a/b/j;->e:[I

    array-length v1, v1

    goto :goto_b

    :cond_16
    iput-object v4, p0, Lcom/google/c/b/b/a/a/b/j;->e:[I

    :cond_17
    invoke-virtual {p1, v3}, Lcom/google/protobuf/nano/a;->d(I)V

    goto/16 :goto_0

    :sswitch_8
    const/16 v0, 0x30

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->b(Lcom/google/protobuf/nano/a;I)I

    move-result v4

    new-array v5, v4, [I

    move v3, v2

    move v1, v2

    :goto_d
    if-ge v3, v4, :cond_19

    if-eqz v3, :cond_18

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    :cond_18
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v6

    packed-switch v6, :pswitch_data_6

    move v0, v1

    :goto_e
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    move v1, v0

    goto :goto_d

    :pswitch_6
    add-int/lit8 v0, v1, 0x1

    aput v6, v5, v1

    goto :goto_e

    :cond_19
    if-eqz v1, :cond_0

    iget-object v0, p0, Lcom/google/c/b/b/a/a/b/j;->f:[I

    if-nez v0, :cond_1a

    move v0, v2

    :goto_f
    if-nez v0, :cond_1b

    array-length v3, v5

    if-ne v1, v3, :cond_1b

    iput-object v5, p0, Lcom/google/c/b/b/a/a/b/j;->f:[I

    goto/16 :goto_0

    :cond_1a
    iget-object v0, p0, Lcom/google/c/b/b/a/a/b/j;->f:[I

    array-length v0, v0

    goto :goto_f

    :cond_1b
    add-int v3, v0, v1

    new-array v3, v3, [I

    if-eqz v0, :cond_1c

    iget-object v4, p0, Lcom/google/c/b/b/a/a/b/j;->f:[I

    invoke-static {v4, v2, v3, v2, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_1c
    invoke-static {v5, v2, v3, v0, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iput-object v3, p0, Lcom/google/c/b/b/a/a/b/j;->f:[I

    goto/16 :goto_0

    :sswitch_9
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->c(I)I

    move-result v3

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->o()I

    move-result v1

    move v0, v2

    :goto_10
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->m()I

    move-result v4

    if-lez v4, :cond_1d

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v4

    packed-switch v4, :pswitch_data_7

    goto :goto_10

    :pswitch_7
    add-int/lit8 v0, v0, 0x1

    goto :goto_10

    :cond_1d
    if-eqz v0, :cond_21

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/a;->e(I)V

    iget-object v1, p0, Lcom/google/c/b/b/a/a/b/j;->f:[I

    if-nez v1, :cond_1f

    move v1, v2

    :goto_11
    add-int/2addr v0, v1

    new-array v4, v0, [I

    if-eqz v1, :cond_1e

    iget-object v0, p0, Lcom/google/c/b/b/a/a/b/j;->f:[I

    invoke-static {v0, v2, v4, v2, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_1e
    :goto_12
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->m()I

    move-result v0

    if-lez v0, :cond_20

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v5

    packed-switch v5, :pswitch_data_8

    goto :goto_12

    :pswitch_8
    add-int/lit8 v0, v1, 0x1

    aput v5, v4, v1

    move v1, v0

    goto :goto_12

    :cond_1f
    iget-object v1, p0, Lcom/google/c/b/b/a/a/b/j;->f:[I

    array-length v1, v1

    goto :goto_11

    :cond_20
    iput-object v4, p0, Lcom/google/c/b/b/a/a/b/j;->f:[I

    :cond_21
    invoke-virtual {p1, v3}, Lcom/google/protobuf/nano/a;->d(I)V

    goto/16 :goto_0

    :sswitch_a
    iget-object v0, p0, Lcom/google/c/b/b/a/a/b/j;->g:Lcom/google/c/b/b/a/a/b/o;

    if-nez v0, :cond_22

    new-instance v0, Lcom/google/c/b/b/a/a/b/o;

    invoke-direct {v0}, Lcom/google/c/b/b/a/a/b/o;-><init>()V

    iput-object v0, p0, Lcom/google/c/b/b/a/a/b/j;->g:Lcom/google/c/b/b/a/a/b/o;

    :cond_22
    iget-object v0, p0, Lcom/google/c/b/b/a/a/b/j;->g:Lcom/google/c/b/b/a/a/b/o;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto/16 :goto_0

    :sswitch_b
    iget-object v0, p0, Lcom/google/c/b/b/a/a/b/j;->h:Lcom/google/c/b/b/a/a/b/l;

    if-nez v0, :cond_23

    new-instance v0, Lcom/google/c/b/b/a/a/b/l;

    invoke-direct {v0}, Lcom/google/c/b/b/a/a/b/l;-><init>()V

    iput-object v0, p0, Lcom/google/c/b/b/a/a/b/j;->h:Lcom/google/c/b/b/a/a/b/l;

    :cond_23
    iget-object v0, p0, Lcom/google/c/b/b/a/a/b/j;->h:Lcom/google/c/b/b/a/a/b/l;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto/16 :goto_0

    :sswitch_c
    iget-object v0, p0, Lcom/google/c/b/b/a/a/b/j;->i:Lcom/google/c/b/b/a/a/b/q;

    if-nez v0, :cond_24

    new-instance v0, Lcom/google/c/b/b/a/a/b/q;

    invoke-direct {v0}, Lcom/google/c/b/b/a/a/b/q;-><init>()V

    iput-object v0, p0, Lcom/google/c/b/b/a/a/b/j;->i:Lcom/google/c/b/b/a/a/b/q;

    :cond_24
    iget-object v0, p0, Lcom/google/c/b/b/a/a/b/j;->i:Lcom/google/c/b/b/a/a/b/q;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto/16 :goto_0

    :sswitch_d
    iget-object v0, p0, Lcom/google/c/b/b/a/a/b/j;->j:Lcom/google/c/b/b/a/a/b/r;

    if-nez v0, :cond_25

    new-instance v0, Lcom/google/c/b/b/a/a/b/r;

    invoke-direct {v0}, Lcom/google/c/b/b/a/a/b/r;-><init>()V

    iput-object v0, p0, Lcom/google/c/b/b/a/a/b/j;->j:Lcom/google/c/b/b/a/a/b/r;

    :cond_25
    iget-object v0, p0, Lcom/google/c/b/b/a/a/b/j;->j:Lcom/google/c/b/b/a/a/b/r;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto/16 :goto_0

    :sswitch_e
    iget-object v0, p0, Lcom/google/c/b/b/a/a/b/j;->k:Lcom/google/c/b/b/a/a/b/m;

    if-nez v0, :cond_26

    new-instance v0, Lcom/google/c/b/b/a/a/b/m;

    invoke-direct {v0}, Lcom/google/c/b/b/a/a/b/m;-><init>()V

    iput-object v0, p0, Lcom/google/c/b/b/a/a/b/j;->k:Lcom/google/c/b/b/a/a/b/m;

    :cond_26
    iget-object v0, p0, Lcom/google/c/b/b/a/a/b/j;->k:Lcom/google/c/b/b/a/a/b/m;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto/16 :goto_0

    :sswitch_f
    iget-object v0, p0, Lcom/google/c/b/b/a/a/b/j;->l:Lcom/google/c/b/b/a/a/b/p;

    if-nez v0, :cond_27

    new-instance v0, Lcom/google/c/b/b/a/a/b/p;

    invoke-direct {v0}, Lcom/google/c/b/b/a/a/b/p;-><init>()V

    iput-object v0, p0, Lcom/google/c/b/b/a/a/b/j;->l:Lcom/google/c/b/b/a/a/b/p;

    :cond_27
    iget-object v0, p0, Lcom/google/c/b/b/a/a/b/j;->l:Lcom/google/c/b/b/a/a/b/p;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
        0x22 -> :sswitch_5
        0x28 -> :sswitch_6
        0x2a -> :sswitch_7
        0x30 -> :sswitch_8
        0x32 -> :sswitch_9
        0x3a -> :sswitch_a
        0x42 -> :sswitch_b
        0x4a -> :sswitch_c
        0x52 -> :sswitch_d
        0x5a -> :sswitch_e
        0x62 -> :sswitch_f
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x0
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
    .end packed-switch

    :pswitch_data_3
    .packed-switch 0x0
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
    .end packed-switch

    :pswitch_data_4
    .packed-switch 0x0
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_4
    .end packed-switch

    :pswitch_data_5
    .packed-switch 0x0
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
    .end packed-switch

    :pswitch_data_6
    .packed-switch 0x0
        :pswitch_6
        :pswitch_6
        :pswitch_6
        :pswitch_6
        :pswitch_6
        :pswitch_6
        :pswitch_6
        :pswitch_6
        :pswitch_6
    .end packed-switch

    :pswitch_data_7
    .packed-switch 0x0
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_7
    .end packed-switch

    :pswitch_data_8
    .packed-switch 0x0
        :pswitch_8
        :pswitch_8
        :pswitch_8
        :pswitch_8
        :pswitch_8
        :pswitch_8
        :pswitch_8
        :pswitch_8
        :pswitch_8
    .end packed-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 1376
    iget-object v0, p0, Lcom/google/c/b/b/a/a/b/j;->a:Lcom/google/c/b/b/a/a/b/s;

    if-eqz v0, :cond_0

    .line 1377
    const/4 v0, 0x1

    iget-object v2, p0, Lcom/google/c/b/b/a/a/b/j;->a:Lcom/google/c/b/b/a/a/b/s;

    invoke-virtual {p1, v0, v2}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 1379
    :cond_0
    iget-object v0, p0, Lcom/google/c/b/b/a/a/b/j;->b:Lcom/google/c/b/b/a/a/b/k;

    if-eqz v0, :cond_1

    .line 1380
    const/4 v0, 0x2

    iget-object v2, p0, Lcom/google/c/b/b/a/a/b/j;->b:Lcom/google/c/b/b/a/a/b/k;

    invoke-virtual {p1, v0, v2}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 1382
    :cond_1
    iget-object v0, p0, Lcom/google/c/b/b/a/a/b/j;->c:Lcom/google/c/b/b/a/a/b/k;

    if-eqz v0, :cond_2

    .line 1383
    const/4 v0, 0x3

    iget-object v2, p0, Lcom/google/c/b/b/a/a/b/j;->c:Lcom/google/c/b/b/a/a/b/k;

    invoke-virtual {p1, v0, v2}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 1385
    :cond_2
    iget-object v0, p0, Lcom/google/c/b/b/a/a/b/j;->d:[I

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/c/b/b/a/a/b/j;->d:[I

    array-length v0, v0

    if-lez v0, :cond_3

    move v0, v1

    .line 1386
    :goto_0
    iget-object v2, p0, Lcom/google/c/b/b/a/a/b/j;->d:[I

    array-length v2, v2

    if-ge v0, v2, :cond_3

    .line 1387
    const/4 v2, 0x4

    iget-object v3, p0, Lcom/google/c/b/b/a/a/b/j;->d:[I

    aget v3, v3, v0

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 1386
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1390
    :cond_3
    iget-object v0, p0, Lcom/google/c/b/b/a/a/b/j;->e:[I

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/google/c/b/b/a/a/b/j;->e:[I

    array-length v0, v0

    if-lez v0, :cond_4

    move v0, v1

    .line 1391
    :goto_1
    iget-object v2, p0, Lcom/google/c/b/b/a/a/b/j;->e:[I

    array-length v2, v2

    if-ge v0, v2, :cond_4

    .line 1392
    const/4 v2, 0x5

    iget-object v3, p0, Lcom/google/c/b/b/a/a/b/j;->e:[I

    aget v3, v3, v0

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 1391
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 1395
    :cond_4
    iget-object v0, p0, Lcom/google/c/b/b/a/a/b/j;->f:[I

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/google/c/b/b/a/a/b/j;->f:[I

    array-length v0, v0

    if-lez v0, :cond_5

    .line 1396
    :goto_2
    iget-object v0, p0, Lcom/google/c/b/b/a/a/b/j;->f:[I

    array-length v0, v0

    if-ge v1, v0, :cond_5

    .line 1397
    const/4 v0, 0x6

    iget-object v2, p0, Lcom/google/c/b/b/a/a/b/j;->f:[I

    aget v2, v2, v1

    invoke-virtual {p1, v0, v2}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 1396
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 1400
    :cond_5
    iget-object v0, p0, Lcom/google/c/b/b/a/a/b/j;->g:Lcom/google/c/b/b/a/a/b/o;

    if-eqz v0, :cond_6

    .line 1401
    const/4 v0, 0x7

    iget-object v1, p0, Lcom/google/c/b/b/a/a/b/j;->g:Lcom/google/c/b/b/a/a/b/o;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 1403
    :cond_6
    iget-object v0, p0, Lcom/google/c/b/b/a/a/b/j;->h:Lcom/google/c/b/b/a/a/b/l;

    if-eqz v0, :cond_7

    .line 1404
    const/16 v0, 0x8

    iget-object v1, p0, Lcom/google/c/b/b/a/a/b/j;->h:Lcom/google/c/b/b/a/a/b/l;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 1406
    :cond_7
    iget-object v0, p0, Lcom/google/c/b/b/a/a/b/j;->i:Lcom/google/c/b/b/a/a/b/q;

    if-eqz v0, :cond_8

    .line 1407
    const/16 v0, 0x9

    iget-object v1, p0, Lcom/google/c/b/b/a/a/b/j;->i:Lcom/google/c/b/b/a/a/b/q;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 1409
    :cond_8
    iget-object v0, p0, Lcom/google/c/b/b/a/a/b/j;->j:Lcom/google/c/b/b/a/a/b/r;

    if-eqz v0, :cond_9

    .line 1410
    const/16 v0, 0xa

    iget-object v1, p0, Lcom/google/c/b/b/a/a/b/j;->j:Lcom/google/c/b/b/a/a/b/r;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 1412
    :cond_9
    iget-object v0, p0, Lcom/google/c/b/b/a/a/b/j;->k:Lcom/google/c/b/b/a/a/b/m;

    if-eqz v0, :cond_a

    .line 1413
    const/16 v0, 0xb

    iget-object v1, p0, Lcom/google/c/b/b/a/a/b/j;->k:Lcom/google/c/b/b/a/a/b/m;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 1415
    :cond_a
    iget-object v0, p0, Lcom/google/c/b/b/a/a/b/j;->l:Lcom/google/c/b/b/a/a/b/p;

    if-eqz v0, :cond_b

    .line 1416
    const/16 v0, 0xc

    iget-object v1, p0, Lcom/google/c/b/b/a/a/b/j;->l:Lcom/google/c/b/b/a/a/b/p;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 1418
    :cond_b
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/d;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 1419
    return-void
.end method
