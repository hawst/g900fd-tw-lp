.class public final Lcom/google/c/b/b/a/a/b/q;
.super Lcom/google/protobuf/nano/d;
.source "SourceFile"


# instance fields
.field public a:Ljava/lang/Integer;

.field public b:Ljava/lang/Integer;

.field public c:Ljava/lang/Integer;

.field public d:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 779
    invoke-direct {p0}, Lcom/google/protobuf/nano/d;-><init>()V

    .line 780
    iput-object v0, p0, Lcom/google/c/b/b/a/a/b/q;->a:Ljava/lang/Integer;

    iput-object v0, p0, Lcom/google/c/b/b/a/a/b/q;->b:Ljava/lang/Integer;

    iput-object v0, p0, Lcom/google/c/b/b/a/a/b/q;->c:Ljava/lang/Integer;

    iput-object v0, p0, Lcom/google/c/b/b/a/a/b/q;->d:Ljava/lang/Boolean;

    iput-object v0, p0, Lcom/google/c/b/b/a/a/b/q;->unknownFieldData:Lcom/google/protobuf/nano/f;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/c/b/b/a/a/b/q;->cachedSize:I

    .line 781
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 813
    invoke-super {p0}, Lcom/google/protobuf/nano/d;->computeSerializedSize()I

    move-result v0

    .line 814
    iget-object v1, p0, Lcom/google/c/b/b/a/a/b/q;->a:Ljava/lang/Integer;

    if-eqz v1, :cond_0

    .line 815
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/c/b/b/a/a/b/q;->a:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->g(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 818
    :cond_0
    iget-object v1, p0, Lcom/google/c/b/b/a/a/b/q;->b:Ljava/lang/Integer;

    if-eqz v1, :cond_1

    .line 819
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/c/b/b/a/a/b/q;->b:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->g(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 822
    :cond_1
    iget-object v1, p0, Lcom/google/c/b/b/a/a/b/q;->c:Ljava/lang/Integer;

    if-eqz v1, :cond_2

    .line 823
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/c/b/b/a/a/b/q;->c:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->g(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 826
    :cond_2
    iget-object v1, p0, Lcom/google/c/b/b/a/a/b/q;->d:Ljava/lang/Boolean;

    if-eqz v1, :cond_3

    .line 827
    const/4 v1, 0x6

    iget-object v2, p0, Lcom/google/c/b/b/a/a/b/q;->d:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 830
    :cond_3
    return v0
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 750
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/c/b/b/a/a/b/q;->storeUnknownField(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/c/b/b/a/a/b/q;->a:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/c/b/b/a/a/b/q;->b:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/c/b/b/a/a/b/q;->c:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/c/b/b/a/a/b/q;->d:Ljava/lang/Boolean;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x30 -> :sswitch_4
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 796
    iget-object v0, p0, Lcom/google/c/b/b/a/a/b/q;->a:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 797
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/c/b/b/a/a/b/q;->a:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->c(II)V

    .line 799
    :cond_0
    iget-object v0, p0, Lcom/google/c/b/b/a/a/b/q;->b:Ljava/lang/Integer;

    if-eqz v0, :cond_1

    .line 800
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/c/b/b/a/a/b/q;->b:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->c(II)V

    .line 802
    :cond_1
    iget-object v0, p0, Lcom/google/c/b/b/a/a/b/q;->c:Ljava/lang/Integer;

    if-eqz v0, :cond_2

    .line 803
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/c/b/b/a/a/b/q;->c:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->c(II)V

    .line 805
    :cond_2
    iget-object v0, p0, Lcom/google/c/b/b/a/a/b/q;->d:Ljava/lang/Boolean;

    if-eqz v0, :cond_3

    .line 806
    const/4 v0, 0x6

    iget-object v1, p0, Lcom/google/c/b/b/a/a/b/q;->d:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 808
    :cond_3
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/d;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 809
    return-void
.end method
