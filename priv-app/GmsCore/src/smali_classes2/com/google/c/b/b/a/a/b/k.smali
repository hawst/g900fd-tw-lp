.class public final Lcom/google/c/b/b/a/a/b/k;
.super Lcom/google/protobuf/nano/d;
.source "SourceFile"


# instance fields
.field public a:Ljava/lang/Integer;

.field public b:Ljava/lang/Integer;

.field public c:Ljava/lang/Integer;

.field public d:Ljava/lang/Integer;

.field public e:Ljava/lang/Boolean;

.field public f:[I


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 327
    invoke-direct {p0}, Lcom/google/protobuf/nano/d;-><init>()V

    .line 328
    iput-object v1, p0, Lcom/google/c/b/b/a/a/b/k;->a:Ljava/lang/Integer;

    iput-object v1, p0, Lcom/google/c/b/b/a/a/b/k;->b:Ljava/lang/Integer;

    iput-object v1, p0, Lcom/google/c/b/b/a/a/b/k;->c:Ljava/lang/Integer;

    iput-object v1, p0, Lcom/google/c/b/b/a/a/b/k;->d:Ljava/lang/Integer;

    iput-object v1, p0, Lcom/google/c/b/b/a/a/b/k;->e:Ljava/lang/Boolean;

    sget-object v0, Lcom/google/protobuf/nano/m;->a:[I

    iput-object v0, p0, Lcom/google/c/b/b/a/a/b/k;->f:[I

    iput-object v1, p0, Lcom/google/c/b/b/a/a/b/k;->unknownFieldData:Lcom/google/protobuf/nano/f;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/c/b/b/a/a/b/k;->cachedSize:I

    .line 329
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 371
    invoke-super {p0}, Lcom/google/protobuf/nano/d;->computeSerializedSize()I

    move-result v0

    .line 372
    iget-object v2, p0, Lcom/google/c/b/b/a/a/b/k;->a:Ljava/lang/Integer;

    if-eqz v2, :cond_0

    .line 373
    const/4 v2, 0x1

    iget-object v3, p0, Lcom/google/c/b/b/a/a/b/k;->a:Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/b;->g(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 376
    :cond_0
    iget-object v2, p0, Lcom/google/c/b/b/a/a/b/k;->b:Ljava/lang/Integer;

    if-eqz v2, :cond_1

    .line 377
    const/4 v2, 0x2

    iget-object v3, p0, Lcom/google/c/b/b/a/a/b/k;->b:Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/b;->g(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 380
    :cond_1
    iget-object v2, p0, Lcom/google/c/b/b/a/a/b/k;->c:Ljava/lang/Integer;

    if-eqz v2, :cond_2

    .line 381
    const/4 v2, 0x3

    iget-object v3, p0, Lcom/google/c/b/b/a/a/b/k;->c:Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/b;->g(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 384
    :cond_2
    iget-object v2, p0, Lcom/google/c/b/b/a/a/b/k;->d:Ljava/lang/Integer;

    if-eqz v2, :cond_3

    .line 385
    const/4 v2, 0x4

    iget-object v3, p0, Lcom/google/c/b/b/a/a/b/k;->d:Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/b;->g(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 388
    :cond_3
    iget-object v2, p0, Lcom/google/c/b/b/a/a/b/k;->e:Ljava/lang/Boolean;

    if-eqz v2, :cond_4

    .line 389
    const/4 v2, 0x5

    iget-object v3, p0, Lcom/google/c/b/b/a/a/b/k;->e:Ljava/lang/Boolean;

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v2}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 392
    :cond_4
    iget-object v2, p0, Lcom/google/c/b/b/a/a/b/k;->f:[I

    if-eqz v2, :cond_6

    iget-object v2, p0, Lcom/google/c/b/b/a/a/b/k;->f:[I

    array-length v2, v2

    if-lez v2, :cond_6

    move v2, v1

    .line 394
    :goto_0
    iget-object v3, p0, Lcom/google/c/b/b/a/a/b/k;->f:[I

    array-length v3, v3

    if-ge v1, v3, :cond_5

    .line 395
    iget-object v3, p0, Lcom/google/c/b/b/a/a/b/k;->f:[I

    aget v3, v3, v1

    .line 396
    invoke-static {v3}, Lcom/google/protobuf/nano/b;->b(I)I

    move-result v3

    add-int/2addr v2, v3

    .line 394
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 399
    :cond_5
    add-int/2addr v0, v2

    .line 400
    iget-object v1, p0, Lcom/google/c/b/b/a/a/b/k;->f:[I

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 402
    :cond_6
    return v0
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 292
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/c/b/b/a/a/b/k;->storeUnknownField(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/c/b/b/a/a/b/k;->a:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/c/b/b/a/a/b/k;->b:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/c/b/b/a/a/b/k;->c:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/c/b/b/a/a/b/k;->d:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/c/b/b/a/a/b/k;->e:Ljava/lang/Boolean;

    goto :goto_0

    :sswitch_6
    const/16 v0, 0x30

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->b(Lcom/google/protobuf/nano/a;I)I

    move-result v4

    new-array v5, v4, [I

    move v3, v2

    move v1, v2

    :goto_1
    if-ge v3, v4, :cond_2

    if-eqz v3, :cond_1

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    :cond_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v6

    packed-switch v6, :pswitch_data_0

    move v0, v1

    :goto_2
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    move v1, v0

    goto :goto_1

    :pswitch_0
    add-int/lit8 v0, v1, 0x1

    aput v6, v5, v1

    goto :goto_2

    :cond_2
    if-eqz v1, :cond_0

    iget-object v0, p0, Lcom/google/c/b/b/a/a/b/k;->f:[I

    if-nez v0, :cond_3

    move v0, v2

    :goto_3
    if-nez v0, :cond_4

    array-length v3, v5

    if-ne v1, v3, :cond_4

    iput-object v5, p0, Lcom/google/c/b/b/a/a/b/k;->f:[I

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/google/c/b/b/a/a/b/k;->f:[I

    array-length v0, v0

    goto :goto_3

    :cond_4
    add-int v3, v0, v1

    new-array v3, v3, [I

    if-eqz v0, :cond_5

    iget-object v4, p0, Lcom/google/c/b/b/a/a/b/k;->f:[I

    invoke-static {v4, v2, v3, v2, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_5
    invoke-static {v5, v2, v3, v0, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iput-object v3, p0, Lcom/google/c/b/b/a/a/b/k;->f:[I

    goto/16 :goto_0

    :sswitch_7
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->c(I)I

    move-result v3

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->o()I

    move-result v1

    move v0, v2

    :goto_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->m()I

    move-result v4

    if-lez v4, :cond_6

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v4

    packed-switch v4, :pswitch_data_1

    goto :goto_4

    :pswitch_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_6
    if-eqz v0, :cond_a

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/a;->e(I)V

    iget-object v1, p0, Lcom/google/c/b/b/a/a/b/k;->f:[I

    if-nez v1, :cond_8

    move v1, v2

    :goto_5
    add-int/2addr v0, v1

    new-array v4, v0, [I

    if-eqz v1, :cond_7

    iget-object v0, p0, Lcom/google/c/b/b/a/a/b/k;->f:[I

    invoke-static {v0, v2, v4, v2, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_7
    :goto_6
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->m()I

    move-result v0

    if-lez v0, :cond_9

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v5

    packed-switch v5, :pswitch_data_2

    goto :goto_6

    :pswitch_2
    add-int/lit8 v0, v1, 0x1

    aput v5, v4, v1

    move v1, v0

    goto :goto_6

    :cond_8
    iget-object v1, p0, Lcom/google/c/b/b/a/a/b/k;->f:[I

    array-length v1, v1

    goto :goto_5

    :cond_9
    iput-object v4, p0, Lcom/google/c/b/b/a/a/b/k;->f:[I

    :cond_a
    invoke-virtual {p1, v3}, Lcom/google/protobuf/nano/a;->d(I)V

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
        0x30 -> :sswitch_6
        0x32 -> :sswitch_7
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x0
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
    .end packed-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 3

    .prologue
    .line 346
    iget-object v0, p0, Lcom/google/c/b/b/a/a/b/k;->a:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 347
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/c/b/b/a/a/b/k;->a:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->c(II)V

    .line 349
    :cond_0
    iget-object v0, p0, Lcom/google/c/b/b/a/a/b/k;->b:Ljava/lang/Integer;

    if-eqz v0, :cond_1

    .line 350
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/c/b/b/a/a/b/k;->b:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->c(II)V

    .line 352
    :cond_1
    iget-object v0, p0, Lcom/google/c/b/b/a/a/b/k;->c:Ljava/lang/Integer;

    if-eqz v0, :cond_2

    .line 353
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/c/b/b/a/a/b/k;->c:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->c(II)V

    .line 355
    :cond_2
    iget-object v0, p0, Lcom/google/c/b/b/a/a/b/k;->d:Ljava/lang/Integer;

    if-eqz v0, :cond_3

    .line 356
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/c/b/b/a/a/b/k;->d:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->c(II)V

    .line 358
    :cond_3
    iget-object v0, p0, Lcom/google/c/b/b/a/a/b/k;->e:Ljava/lang/Boolean;

    if-eqz v0, :cond_4

    .line 359
    const/4 v0, 0x5

    iget-object v1, p0, Lcom/google/c/b/b/a/a/b/k;->e:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 361
    :cond_4
    iget-object v0, p0, Lcom/google/c/b/b/a/a/b/k;->f:[I

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/google/c/b/b/a/a/b/k;->f:[I

    array-length v0, v0

    if-lez v0, :cond_5

    .line 362
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/google/c/b/b/a/a/b/k;->f:[I

    array-length v1, v1

    if-ge v0, v1, :cond_5

    .line 363
    const/4 v1, 0x6

    iget-object v2, p0, Lcom/google/c/b/b/a/a/b/k;->f:[I

    aget v2, v2, v0

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 362
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 366
    :cond_5
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/d;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 367
    return-void
.end method
