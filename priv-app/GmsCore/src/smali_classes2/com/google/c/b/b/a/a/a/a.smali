.class public final Lcom/google/c/b/b/a/a/a/a;
.super Lcom/google/protobuf/nano/d;
.source "SourceFile"


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field public d:Lcom/google/c/b/c/a/a;

.field public e:Lcom/google/c/b/b/a/a/a/b;

.field public f:Lcom/google/c/b/b/a/a/a/d;

.field public g:Ljava/lang/Boolean;

.field public h:Ljava/lang/String;

.field public i:Lcom/google/c/b/b/a/a/a/e;

.field public j:Ljava/lang/Integer;

.field public k:Lcom/google/c/b/b/a/a/a/c;

.field public l:Ljava/lang/Integer;

.field public m:Lb/a/a/a/c;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 162
    invoke-direct {p0}, Lcom/google/protobuf/nano/d;-><init>()V

    .line 163
    iput-object v0, p0, Lcom/google/c/b/b/a/a/a/a;->a:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/c/b/b/a/a/a/a;->b:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/c/b/b/a/a/a/a;->c:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/c/b/b/a/a/a/a;->d:Lcom/google/c/b/c/a/a;

    iput-object v0, p0, Lcom/google/c/b/b/a/a/a/a;->e:Lcom/google/c/b/b/a/a/a/b;

    iput-object v0, p0, Lcom/google/c/b/b/a/a/a/a;->f:Lcom/google/c/b/b/a/a/a/d;

    iput-object v0, p0, Lcom/google/c/b/b/a/a/a/a;->g:Ljava/lang/Boolean;

    iput-object v0, p0, Lcom/google/c/b/b/a/a/a/a;->h:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/c/b/b/a/a/a/a;->i:Lcom/google/c/b/b/a/a/a/e;

    iput-object v0, p0, Lcom/google/c/b/b/a/a/a/a;->j:Ljava/lang/Integer;

    iput-object v0, p0, Lcom/google/c/b/b/a/a/a/a;->k:Lcom/google/c/b/b/a/a/a/c;

    iput-object v0, p0, Lcom/google/c/b/b/a/a/a/a;->l:Ljava/lang/Integer;

    iput-object v0, p0, Lcom/google/c/b/b/a/a/a/a;->m:Lb/a/a/a/c;

    iput-object v0, p0, Lcom/google/c/b/b/a/a/a/a;->unknownFieldData:Lcom/google/protobuf/nano/f;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/c/b/b/a/a/a/a;->cachedSize:I

    .line 164
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 232
    invoke-super {p0}, Lcom/google/protobuf/nano/d;->computeSerializedSize()I

    move-result v0

    .line 233
    iget-object v1, p0, Lcom/google/c/b/b/a/a/a/a;->a:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 234
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/c/b/b/a/a/a/a;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 237
    :cond_0
    iget-object v1, p0, Lcom/google/c/b/b/a/a/a/a;->b:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 238
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/c/b/b/a/a/a/a;->b:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 241
    :cond_1
    iget-object v1, p0, Lcom/google/c/b/b/a/a/a/a;->c:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 242
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/c/b/b/a/a/a/a;->c:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 245
    :cond_2
    iget-object v1, p0, Lcom/google/c/b/b/a/a/a/a;->f:Lcom/google/c/b/b/a/a/a/d;

    if-eqz v1, :cond_3

    .line 246
    const/4 v1, 0x5

    iget-object v2, p0, Lcom/google/c/b/b/a/a/a/a;->f:Lcom/google/c/b/b/a/a/a/d;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 249
    :cond_3
    iget-object v1, p0, Lcom/google/c/b/b/a/a/a/a;->g:Ljava/lang/Boolean;

    if-eqz v1, :cond_4

    .line 250
    const/4 v1, 0x6

    iget-object v2, p0, Lcom/google/c/b/b/a/a/a/a;->g:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 253
    :cond_4
    iget-object v1, p0, Lcom/google/c/b/b/a/a/a/a;->h:Ljava/lang/String;

    if-eqz v1, :cond_5

    .line 254
    const/4 v1, 0x7

    iget-object v2, p0, Lcom/google/c/b/b/a/a/a/a;->h:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 257
    :cond_5
    iget-object v1, p0, Lcom/google/c/b/b/a/a/a/a;->i:Lcom/google/c/b/b/a/a/a/e;

    if-eqz v1, :cond_6

    .line 258
    const/16 v1, 0x8

    iget-object v2, p0, Lcom/google/c/b/b/a/a/a/a;->i:Lcom/google/c/b/b/a/a/a/e;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 261
    :cond_6
    iget-object v1, p0, Lcom/google/c/b/b/a/a/a/a;->j:Ljava/lang/Integer;

    if-eqz v1, :cond_7

    .line 262
    const/16 v1, 0x9

    iget-object v2, p0, Lcom/google/c/b/b/a/a/a/a;->j:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 265
    :cond_7
    iget-object v1, p0, Lcom/google/c/b/b/a/a/a/a;->k:Lcom/google/c/b/b/a/a/a/c;

    if-eqz v1, :cond_8

    .line 266
    const/16 v1, 0xa

    iget-object v2, p0, Lcom/google/c/b/b/a/a/a/a;->k:Lcom/google/c/b/b/a/a/a/c;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 269
    :cond_8
    iget-object v1, p0, Lcom/google/c/b/b/a/a/a/a;->l:Ljava/lang/Integer;

    if-eqz v1, :cond_9

    .line 270
    const/16 v1, 0xb

    iget-object v2, p0, Lcom/google/c/b/b/a/a/a/a;->l:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 273
    :cond_9
    iget-object v1, p0, Lcom/google/c/b/b/a/a/a/a;->d:Lcom/google/c/b/c/a/a;

    if-eqz v1, :cond_a

    .line 274
    const/16 v1, 0xc

    iget-object v2, p0, Lcom/google/c/b/b/a/a/a/a;->d:Lcom/google/c/b/c/a/a;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 277
    :cond_a
    iget-object v1, p0, Lcom/google/c/b/b/a/a/a/a;->e:Lcom/google/c/b/b/a/a/a/b;

    if-eqz v1, :cond_b

    .line 278
    const/16 v1, 0xd

    iget-object v2, p0, Lcom/google/c/b/b/a/a/a/a;->e:Lcom/google/c/b/b/a/a/a/b;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 281
    :cond_b
    iget-object v1, p0, Lcom/google/c/b/b/a/a/a/a;->m:Lb/a/a/a/c;

    if-eqz v1, :cond_c

    .line 282
    const/16 v1, 0xe

    iget-object v2, p0, Lcom/google/c/b/b/a/a/a/a;->m:Lb/a/a/a/c;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 285
    :cond_c
    return v0
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 5
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/c/b/b/a/a/a/a;->storeUnknownField(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/c/b/b/a/a/a/a;->a:Ljava/lang/String;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/c/b/b/a/a/a/a;->b:Ljava/lang/String;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/c/b/b/a/a/a/a;->c:Ljava/lang/String;

    goto :goto_0

    :sswitch_4
    iget-object v0, p0, Lcom/google/c/b/b/a/a/a/a;->f:Lcom/google/c/b/b/a/a/a/d;

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/c/b/b/a/a/a/d;

    invoke-direct {v0}, Lcom/google/c/b/b/a/a/a/d;-><init>()V

    iput-object v0, p0, Lcom/google/c/b/b/a/a/a/a;->f:Lcom/google/c/b/b/a/a/a/d;

    :cond_1
    iget-object v0, p0, Lcom/google/c/b/b/a/a/a/a;->f:Lcom/google/c/b/b/a/a/a/d;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/c/b/b/a/a/a/a;->g:Ljava/lang/Boolean;

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/c/b/b/a/a/a/a;->h:Ljava/lang/String;

    goto :goto_0

    :sswitch_7
    iget-object v0, p0, Lcom/google/c/b/b/a/a/a/a;->i:Lcom/google/c/b/b/a/a/a/e;

    if-nez v0, :cond_2

    new-instance v0, Lcom/google/c/b/b/a/a/a/e;

    invoke-direct {v0}, Lcom/google/c/b/b/a/a/a/e;-><init>()V

    iput-object v0, p0, Lcom/google/c/b/b/a/a/a/a;->i:Lcom/google/c/b/b/a/a/a/e;

    :cond_2
    iget-object v0, p0, Lcom/google/c/b/b/a/a/a/a;->i:Lcom/google/c/b/b/a/a/a/e;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_8
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/c/b/b/a/a/a/a;->j:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_9
    iget-object v0, p0, Lcom/google/c/b/b/a/a/a/a;->k:Lcom/google/c/b/b/a/a/a/c;

    if-nez v0, :cond_3

    new-instance v0, Lcom/google/c/b/b/a/a/a/c;

    invoke-direct {v0}, Lcom/google/c/b/b/a/a/a/c;-><init>()V

    iput-object v0, p0, Lcom/google/c/b/b/a/a/a/a;->k:Lcom/google/c/b/b/a/a/a/c;

    :cond_3
    iget-object v0, p0, Lcom/google/c/b/b/a/a/a/a;->k:Lcom/google/c/b/b/a/a/a/c;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_a
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/c/b/b/a/a/a/a;->l:Ljava/lang/Integer;

    goto/16 :goto_0

    :sswitch_b
    iget-object v0, p0, Lcom/google/c/b/b/a/a/a/a;->d:Lcom/google/c/b/c/a/a;

    if-nez v0, :cond_4

    new-instance v0, Lcom/google/c/b/c/a/a;

    invoke-direct {v0}, Lcom/google/c/b/c/a/a;-><init>()V

    iput-object v0, p0, Lcom/google/c/b/b/a/a/a/a;->d:Lcom/google/c/b/c/a/a;

    :cond_4
    iget-object v0, p0, Lcom/google/c/b/b/a/a/a/a;->d:Lcom/google/c/b/c/a/a;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto/16 :goto_0

    :sswitch_c
    iget-object v0, p0, Lcom/google/c/b/b/a/a/a/a;->e:Lcom/google/c/b/b/a/a/a/b;

    if-nez v0, :cond_5

    new-instance v0, Lcom/google/c/b/b/a/a/a/b;

    invoke-direct {v0}, Lcom/google/c/b/b/a/a/a/b;-><init>()V

    iput-object v0, p0, Lcom/google/c/b/b/a/a/a/a;->e:Lcom/google/c/b/b/a/a/a/b;

    :cond_5
    iget-object v0, p0, Lcom/google/c/b/b/a/a/a/a;->e:Lcom/google/c/b/b/a/a/a/b;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto/16 :goto_0

    :sswitch_d
    iget-object v0, p0, Lcom/google/c/b/b/a/a/a/a;->m:Lb/a/a/a/c;

    if-nez v0, :cond_6

    new-instance v0, Lb/a/a/a/c;

    invoke-direct {v0}, Lb/a/a/a/c;-><init>()V

    iput-object v0, p0, Lcom/google/c/b/b/a/a/a/a;->m:Lb/a/a/a/c;

    :cond_6
    iget-object v0, p0, Lcom/google/c/b/b/a/a/a/a;->m:Lb/a/a/a/c;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x2a -> :sswitch_4
        0x30 -> :sswitch_5
        0x3a -> :sswitch_6
        0x42 -> :sswitch_7
        0x48 -> :sswitch_8
        0x52 -> :sswitch_9
        0x58 -> :sswitch_a
        0x62 -> :sswitch_b
        0x6a -> :sswitch_c
        0x72 -> :sswitch_d
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 188
    iget-object v0, p0, Lcom/google/c/b/b/a/a/a/a;->a:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 189
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/c/b/b/a/a/a/a;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 191
    :cond_0
    iget-object v0, p0, Lcom/google/c/b/b/a/a/a/a;->b:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 192
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/c/b/b/a/a/a/a;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 194
    :cond_1
    iget-object v0, p0, Lcom/google/c/b/b/a/a/a/a;->c:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 195
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/c/b/b/a/a/a/a;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 197
    :cond_2
    iget-object v0, p0, Lcom/google/c/b/b/a/a/a/a;->f:Lcom/google/c/b/b/a/a/a/d;

    if-eqz v0, :cond_3

    .line 198
    const/4 v0, 0x5

    iget-object v1, p0, Lcom/google/c/b/b/a/a/a/a;->f:Lcom/google/c/b/b/a/a/a/d;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 200
    :cond_3
    iget-object v0, p0, Lcom/google/c/b/b/a/a/a/a;->g:Ljava/lang/Boolean;

    if-eqz v0, :cond_4

    .line 201
    const/4 v0, 0x6

    iget-object v1, p0, Lcom/google/c/b/b/a/a/a/a;->g:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 203
    :cond_4
    iget-object v0, p0, Lcom/google/c/b/b/a/a/a/a;->h:Ljava/lang/String;

    if-eqz v0, :cond_5

    .line 204
    const/4 v0, 0x7

    iget-object v1, p0, Lcom/google/c/b/b/a/a/a/a;->h:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 206
    :cond_5
    iget-object v0, p0, Lcom/google/c/b/b/a/a/a/a;->i:Lcom/google/c/b/b/a/a/a/e;

    if-eqz v0, :cond_6

    .line 207
    const/16 v0, 0x8

    iget-object v1, p0, Lcom/google/c/b/b/a/a/a/a;->i:Lcom/google/c/b/b/a/a/a/e;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 209
    :cond_6
    iget-object v0, p0, Lcom/google/c/b/b/a/a/a/a;->j:Ljava/lang/Integer;

    if-eqz v0, :cond_7

    .line 210
    const/16 v0, 0x9

    iget-object v1, p0, Lcom/google/c/b/b/a/a/a/a;->j:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 212
    :cond_7
    iget-object v0, p0, Lcom/google/c/b/b/a/a/a/a;->k:Lcom/google/c/b/b/a/a/a/c;

    if-eqz v0, :cond_8

    .line 213
    const/16 v0, 0xa

    iget-object v1, p0, Lcom/google/c/b/b/a/a/a/a;->k:Lcom/google/c/b/b/a/a/a/c;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 215
    :cond_8
    iget-object v0, p0, Lcom/google/c/b/b/a/a/a/a;->l:Ljava/lang/Integer;

    if-eqz v0, :cond_9

    .line 216
    const/16 v0, 0xb

    iget-object v1, p0, Lcom/google/c/b/b/a/a/a/a;->l:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 218
    :cond_9
    iget-object v0, p0, Lcom/google/c/b/b/a/a/a/a;->d:Lcom/google/c/b/c/a/a;

    if-eqz v0, :cond_a

    .line 219
    const/16 v0, 0xc

    iget-object v1, p0, Lcom/google/c/b/b/a/a/a/a;->d:Lcom/google/c/b/c/a/a;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 221
    :cond_a
    iget-object v0, p0, Lcom/google/c/b/b/a/a/a/a;->e:Lcom/google/c/b/b/a/a/a/b;

    if-eqz v0, :cond_b

    .line 222
    const/16 v0, 0xd

    iget-object v1, p0, Lcom/google/c/b/b/a/a/a/a;->e:Lcom/google/c/b/b/a/a/a/b;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 224
    :cond_b
    iget-object v0, p0, Lcom/google/c/b/b/a/a/a/a;->m:Lb/a/a/a/c;

    if-eqz v0, :cond_c

    .line 225
    const/16 v0, 0xe

    iget-object v1, p0, Lcom/google/c/b/b/a/a/a/a;->m:Lb/a/a/a/c;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 227
    :cond_c
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/d;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 228
    return-void
.end method
