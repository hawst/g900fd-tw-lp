.class public final Lcom/google/c/b/b/a/a/b/x;
.super Lcom/google/protobuf/nano/d;
.source "SourceFile"


# instance fields
.field public a:Lcom/google/c/b/b/a/a/b/w;

.field public b:Lcom/google/c/b/b/a/a/b/w;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 256
    invoke-direct {p0}, Lcom/google/protobuf/nano/d;-><init>()V

    .line 257
    iput-object v0, p0, Lcom/google/c/b/b/a/a/b/x;->a:Lcom/google/c/b/b/a/a/b/w;

    iput-object v0, p0, Lcom/google/c/b/b/a/a/b/x;->b:Lcom/google/c/b/b/a/a/b/w;

    iput-object v0, p0, Lcom/google/c/b/b/a/a/b/x;->unknownFieldData:Lcom/google/protobuf/nano/f;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/c/b/b/a/a/b/x;->cachedSize:I

    .line 258
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 282
    invoke-super {p0}, Lcom/google/protobuf/nano/d;->computeSerializedSize()I

    move-result v0

    .line 283
    iget-object v1, p0, Lcom/google/c/b/b/a/a/b/x;->a:Lcom/google/c/b/b/a/a/b/w;

    if-eqz v1, :cond_0

    .line 284
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/c/b/b/a/a/b/x;->a:Lcom/google/c/b/b/a/a/b/w;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 287
    :cond_0
    iget-object v1, p0, Lcom/google/c/b/b/a/a/b/x;->b:Lcom/google/c/b/b/a/a/b/w;

    if-eqz v1, :cond_1

    .line 288
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/c/b/b/a/a/b/x;->b:Lcom/google/c/b/b/a/a/b/w;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 291
    :cond_1
    return v0
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 233
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/c/b/b/a/a/b/x;->storeUnknownField(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Lcom/google/c/b/b/a/a/b/x;->a:Lcom/google/c/b/b/a/a/b/w;

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/c/b/b/a/a/b/w;

    invoke-direct {v0}, Lcom/google/c/b/b/a/a/b/w;-><init>()V

    iput-object v0, p0, Lcom/google/c/b/b/a/a/b/x;->a:Lcom/google/c/b/b/a/a/b/w;

    :cond_1
    iget-object v0, p0, Lcom/google/c/b/b/a/a/b/x;->a:Lcom/google/c/b/b/a/a/b/w;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Lcom/google/c/b/b/a/a/b/x;->b:Lcom/google/c/b/b/a/a/b/w;

    if-nez v0, :cond_2

    new-instance v0, Lcom/google/c/b/b/a/a/b/w;

    invoke-direct {v0}, Lcom/google/c/b/b/a/a/b/w;-><init>()V

    iput-object v0, p0, Lcom/google/c/b/b/a/a/b/x;->b:Lcom/google/c/b/b/a/a/b/w;

    :cond_2
    iget-object v0, p0, Lcom/google/c/b/b/a/a/b/x;->b:Lcom/google/c/b/b/a/a/b/w;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 271
    iget-object v0, p0, Lcom/google/c/b/b/a/a/b/x;->a:Lcom/google/c/b/b/a/a/b/w;

    if-eqz v0, :cond_0

    .line 272
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/c/b/b/a/a/b/x;->a:Lcom/google/c/b/b/a/a/b/w;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 274
    :cond_0
    iget-object v0, p0, Lcom/google/c/b/b/a/a/b/x;->b:Lcom/google/c/b/b/a/a/b/w;

    if-eqz v0, :cond_1

    .line 275
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/c/b/b/a/a/b/x;->b:Lcom/google/c/b/b/a/a/b/w;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 277
    :cond_1
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/d;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 278
    return-void
.end method
