.class public final Lcom/google/c/b/b/a/a/a/e;
.super Lcom/google/protobuf/nano/d;
.source "SourceFile"


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field public d:[Ljava/lang/String;

.field public e:[Ljava/lang/String;

.field public f:Ljava/lang/Boolean;

.field public g:Ljava/lang/Boolean;

.field public h:Ljava/lang/Integer;

.field public i:Ljava/lang/Integer;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 50
    invoke-direct {p0}, Lcom/google/protobuf/nano/d;-><init>()V

    .line 51
    iput-object v1, p0, Lcom/google/c/b/b/a/a/a/e;->a:Ljava/lang/String;

    iput-object v1, p0, Lcom/google/c/b/b/a/a/a/e;->b:Ljava/lang/String;

    iput-object v1, p0, Lcom/google/c/b/b/a/a/a/e;->c:Ljava/lang/String;

    sget-object v0, Lcom/google/protobuf/nano/m;->f:[Ljava/lang/String;

    iput-object v0, p0, Lcom/google/c/b/b/a/a/a/e;->d:[Ljava/lang/String;

    sget-object v0, Lcom/google/protobuf/nano/m;->f:[Ljava/lang/String;

    iput-object v0, p0, Lcom/google/c/b/b/a/a/a/e;->e:[Ljava/lang/String;

    iput-object v1, p0, Lcom/google/c/b/b/a/a/a/e;->f:Ljava/lang/Boolean;

    iput-object v1, p0, Lcom/google/c/b/b/a/a/a/e;->g:Ljava/lang/Boolean;

    iput-object v1, p0, Lcom/google/c/b/b/a/a/a/e;->h:Ljava/lang/Integer;

    iput-object v1, p0, Lcom/google/c/b/b/a/a/a/e;->i:Ljava/lang/Integer;

    iput-object v1, p0, Lcom/google/c/b/b/a/a/a/e;->unknownFieldData:Lcom/google/protobuf/nano/f;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/c/b/b/a/a/a/e;->cachedSize:I

    .line 52
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 114
    invoke-super {p0}, Lcom/google/protobuf/nano/d;->computeSerializedSize()I

    move-result v0

    .line 115
    iget-object v1, p0, Lcom/google/c/b/b/a/a/a/e;->a:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 116
    const/4 v1, 0x1

    iget-object v3, p0, Lcom/google/c/b/b/a/a/a/e;->a:Ljava/lang/String;

    invoke-static {v1, v3}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 119
    :cond_0
    iget-object v1, p0, Lcom/google/c/b/b/a/a/a/e;->b:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 120
    const/4 v1, 0x2

    iget-object v3, p0, Lcom/google/c/b/b/a/a/a/e;->b:Ljava/lang/String;

    invoke-static {v1, v3}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 123
    :cond_1
    iget-object v1, p0, Lcom/google/c/b/b/a/a/a/e;->c:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 124
    const/4 v1, 0x3

    iget-object v3, p0, Lcom/google/c/b/b/a/a/a/e;->c:Ljava/lang/String;

    invoke-static {v1, v3}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 127
    :cond_2
    iget-object v1, p0, Lcom/google/c/b/b/a/a/a/e;->d:[Ljava/lang/String;

    if-eqz v1, :cond_5

    iget-object v1, p0, Lcom/google/c/b/b/a/a/a/e;->d:[Ljava/lang/String;

    array-length v1, v1

    if-lez v1, :cond_5

    move v1, v2

    move v3, v2

    move v4, v2

    .line 130
    :goto_0
    iget-object v5, p0, Lcom/google/c/b/b/a/a/a/e;->d:[Ljava/lang/String;

    array-length v5, v5

    if-ge v1, v5, :cond_4

    .line 131
    iget-object v5, p0, Lcom/google/c/b/b/a/a/a/e;->d:[Ljava/lang/String;

    aget-object v5, v5, v1

    .line 132
    if-eqz v5, :cond_3

    .line 133
    add-int/lit8 v4, v4, 0x1

    .line 134
    invoke-static {v5}, Lcom/google/protobuf/nano/b;->a(Ljava/lang/String;)I

    move-result v5

    add-int/2addr v3, v5

    .line 130
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 138
    :cond_4
    add-int/2addr v0, v3

    .line 139
    mul-int/lit8 v1, v4, 0x1

    add-int/2addr v0, v1

    .line 141
    :cond_5
    iget-object v1, p0, Lcom/google/c/b/b/a/a/a/e;->e:[Ljava/lang/String;

    if-eqz v1, :cond_8

    iget-object v1, p0, Lcom/google/c/b/b/a/a/a/e;->e:[Ljava/lang/String;

    array-length v1, v1

    if-lez v1, :cond_8

    move v1, v2

    move v3, v2

    .line 144
    :goto_1
    iget-object v4, p0, Lcom/google/c/b/b/a/a/a/e;->e:[Ljava/lang/String;

    array-length v4, v4

    if-ge v2, v4, :cond_7

    .line 145
    iget-object v4, p0, Lcom/google/c/b/b/a/a/a/e;->e:[Ljava/lang/String;

    aget-object v4, v4, v2

    .line 146
    if-eqz v4, :cond_6

    .line 147
    add-int/lit8 v3, v3, 0x1

    .line 148
    invoke-static {v4}, Lcom/google/protobuf/nano/b;->a(Ljava/lang/String;)I

    move-result v4

    add-int/2addr v1, v4

    .line 144
    :cond_6
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 152
    :cond_7
    add-int/2addr v0, v1

    .line 153
    mul-int/lit8 v1, v3, 0x1

    add-int/2addr v0, v1

    .line 155
    :cond_8
    iget-object v1, p0, Lcom/google/c/b/b/a/a/a/e;->f:Ljava/lang/Boolean;

    if-eqz v1, :cond_9

    .line 156
    const/4 v1, 0x6

    iget-object v2, p0, Lcom/google/c/b/b/a/a/a/e;->f:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 159
    :cond_9
    iget-object v1, p0, Lcom/google/c/b/b/a/a/a/e;->g:Ljava/lang/Boolean;

    if-eqz v1, :cond_a

    .line 160
    const/4 v1, 0x7

    iget-object v2, p0, Lcom/google/c/b/b/a/a/a/e;->g:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 163
    :cond_a
    iget-object v1, p0, Lcom/google/c/b/b/a/a/a/e;->h:Ljava/lang/Integer;

    if-eqz v1, :cond_b

    .line 164
    const/16 v1, 0x8

    iget-object v2, p0, Lcom/google/c/b/b/a/a/a/e;->h:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 167
    :cond_b
    iget-object v1, p0, Lcom/google/c/b/b/a/a/a/e;->i:Ljava/lang/Integer;

    if-eqz v1, :cond_c

    .line 168
    const/16 v1, 0x9

    iget-object v2, p0, Lcom/google/c/b/b/a/a/a/e;->i:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 171
    :cond_c
    return v0
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 5
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/c/b/b/a/a/a/e;->storeUnknownField(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/c/b/b/a/a/a/e;->a:Ljava/lang/String;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/c/b/b/a/a/a/e;->b:Ljava/lang/String;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/c/b/b/a/a/a/e;->c:Ljava/lang/String;

    goto :goto_0

    :sswitch_4
    const/16 v0, 0x22

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->b(Lcom/google/protobuf/nano/a;I)I

    move-result v2

    iget-object v0, p0, Lcom/google/c/b/b/a/a/a/e;->d:[Ljava/lang/String;

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v3, p0, Lcom/google/c/b/b/a/a/a/e;->d:[Ljava/lang/String;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_1
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_3

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    iget-object v0, p0, Lcom/google/c/b/b/a/a/a/e;->d:[Ljava/lang/String;

    array-length v0, v0

    goto :goto_1

    :cond_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    iput-object v2, p0, Lcom/google/c/b/b/a/a/a/e;->d:[Ljava/lang/String;

    goto :goto_0

    :sswitch_5
    const/16 v0, 0x2a

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->b(Lcom/google/protobuf/nano/a;I)I

    move-result v2

    iget-object v0, p0, Lcom/google/c/b/b/a/a/a/e;->e:[Ljava/lang/String;

    if-nez v0, :cond_5

    move v0, v1

    :goto_3
    add-int/2addr v2, v0

    new-array v2, v2, [Ljava/lang/String;

    if-eqz v0, :cond_4

    iget-object v3, p0, Lcom/google/c/b/b/a/a/a/e;->e:[Ljava/lang/String;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_4
    :goto_4
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_6

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_5
    iget-object v0, p0, Lcom/google/c/b/b/a/a/a/e;->e:[Ljava/lang/String;

    array-length v0, v0

    goto :goto_3

    :cond_6
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    iput-object v2, p0, Lcom/google/c/b/b/a/a/a/e;->e:[Ljava/lang/String;

    goto/16 :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/c/b/b/a/a/a/e;->f:Ljava/lang/Boolean;

    goto/16 :goto_0

    :sswitch_7
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/c/b/b/a/a/a/e;->g:Ljava/lang/Boolean;

    goto/16 :goto_0

    :sswitch_8
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/c/b/b/a/a/a/e;->h:Ljava/lang/Integer;

    goto/16 :goto_0

    :sswitch_9
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/c/b/b/a/a/a/e;->i:Ljava/lang/Integer;

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x30 -> :sswitch_6
        0x38 -> :sswitch_7
        0x40 -> :sswitch_8
        0x48 -> :sswitch_9
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 72
    iget-object v0, p0, Lcom/google/c/b/b/a/a/a/e;->a:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 73
    const/4 v0, 0x1

    iget-object v2, p0, Lcom/google/c/b/b/a/a/a/e;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v2}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 75
    :cond_0
    iget-object v0, p0, Lcom/google/c/b/b/a/a/a/e;->b:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 76
    const/4 v0, 0x2

    iget-object v2, p0, Lcom/google/c/b/b/a/a/a/e;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v2}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 78
    :cond_1
    iget-object v0, p0, Lcom/google/c/b/b/a/a/a/e;->c:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 79
    const/4 v0, 0x3

    iget-object v2, p0, Lcom/google/c/b/b/a/a/a/e;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v2}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 81
    :cond_2
    iget-object v0, p0, Lcom/google/c/b/b/a/a/a/e;->d:[Ljava/lang/String;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/google/c/b/b/a/a/a/e;->d:[Ljava/lang/String;

    array-length v0, v0

    if-lez v0, :cond_4

    move v0, v1

    .line 82
    :goto_0
    iget-object v2, p0, Lcom/google/c/b/b/a/a/a/e;->d:[Ljava/lang/String;

    array-length v2, v2

    if-ge v0, v2, :cond_4

    .line 83
    iget-object v2, p0, Lcom/google/c/b/b/a/a/a/e;->d:[Ljava/lang/String;

    aget-object v2, v2, v0

    .line 84
    if-eqz v2, :cond_3

    .line 85
    const/4 v3, 0x4

    invoke-virtual {p1, v3, v2}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 82
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 89
    :cond_4
    iget-object v0, p0, Lcom/google/c/b/b/a/a/a/e;->e:[Ljava/lang/String;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/google/c/b/b/a/a/a/e;->e:[Ljava/lang/String;

    array-length v0, v0

    if-lez v0, :cond_6

    .line 90
    :goto_1
    iget-object v0, p0, Lcom/google/c/b/b/a/a/a/e;->e:[Ljava/lang/String;

    array-length v0, v0

    if-ge v1, v0, :cond_6

    .line 91
    iget-object v0, p0, Lcom/google/c/b/b/a/a/a/e;->e:[Ljava/lang/String;

    aget-object v0, v0, v1

    .line 92
    if-eqz v0, :cond_5

    .line 93
    const/4 v2, 0x5

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 90
    :cond_5
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 97
    :cond_6
    iget-object v0, p0, Lcom/google/c/b/b/a/a/a/e;->f:Ljava/lang/Boolean;

    if-eqz v0, :cond_7

    .line 98
    const/4 v0, 0x6

    iget-object v1, p0, Lcom/google/c/b/b/a/a/a/e;->f:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 100
    :cond_7
    iget-object v0, p0, Lcom/google/c/b/b/a/a/a/e;->g:Ljava/lang/Boolean;

    if-eqz v0, :cond_8

    .line 101
    const/4 v0, 0x7

    iget-object v1, p0, Lcom/google/c/b/b/a/a/a/e;->g:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 103
    :cond_8
    iget-object v0, p0, Lcom/google/c/b/b/a/a/a/e;->h:Ljava/lang/Integer;

    if-eqz v0, :cond_9

    .line 104
    const/16 v0, 0x8

    iget-object v1, p0, Lcom/google/c/b/b/a/a/a/e;->h:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 106
    :cond_9
    iget-object v0, p0, Lcom/google/c/b/b/a/a/a/e;->i:Ljava/lang/Integer;

    if-eqz v0, :cond_a

    .line 107
    const/16 v0, 0x9

    iget-object v1, p0, Lcom/google/c/b/b/a/a/a/e;->i:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 109
    :cond_a
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/d;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 110
    return-void
.end method
