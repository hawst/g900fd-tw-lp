.class public final Lcom/google/c/b/b/a/a/b/m;
.super Lcom/google/protobuf/nano/d;
.source "SourceFile"


# instance fields
.field public a:Lcom/google/c/b/b/a/a/b/n;

.field public b:Lcom/google/c/b/b/a/a/b/n;

.field public c:Ljava/lang/Boolean;

.field public d:Ljava/lang/Integer;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1109
    invoke-direct {p0}, Lcom/google/protobuf/nano/d;-><init>()V

    .line 1110
    iput-object v0, p0, Lcom/google/c/b/b/a/a/b/m;->a:Lcom/google/c/b/b/a/a/b/n;

    iput-object v0, p0, Lcom/google/c/b/b/a/a/b/m;->b:Lcom/google/c/b/b/a/a/b/n;

    iput-object v0, p0, Lcom/google/c/b/b/a/a/b/m;->c:Ljava/lang/Boolean;

    iput-object v0, p0, Lcom/google/c/b/b/a/a/b/m;->d:Ljava/lang/Integer;

    iput-object v0, p0, Lcom/google/c/b/b/a/a/b/m;->unknownFieldData:Lcom/google/protobuf/nano/f;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/c/b/b/a/a/b/m;->cachedSize:I

    .line 1111
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 1143
    invoke-super {p0}, Lcom/google/protobuf/nano/d;->computeSerializedSize()I

    move-result v0

    .line 1144
    iget-object v1, p0, Lcom/google/c/b/b/a/a/b/m;->a:Lcom/google/c/b/b/a/a/b/n;

    if-eqz v1, :cond_0

    .line 1145
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/c/b/b/a/a/b/m;->a:Lcom/google/c/b/b/a/a/b/n;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1148
    :cond_0
    iget-object v1, p0, Lcom/google/c/b/b/a/a/b/m;->b:Lcom/google/c/b/b/a/a/b/n;

    if-eqz v1, :cond_1

    .line 1149
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/c/b/b/a/a/b/m;->b:Lcom/google/c/b/b/a/a/b/n;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1152
    :cond_1
    iget-object v1, p0, Lcom/google/c/b/b/a/a/b/m;->c:Ljava/lang/Boolean;

    if-eqz v1, :cond_2

    .line 1153
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/c/b/b/a/a/b/m;->c:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 1156
    :cond_2
    iget-object v1, p0, Lcom/google/c/b/b/a/a/b/m;->d:Ljava/lang/Integer;

    if-eqz v1, :cond_3

    .line 1157
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/c/b/b/a/a/b/m;->d:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->g(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1160
    :cond_3
    return v0
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 1080
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/c/b/b/a/a/b/m;->storeUnknownField(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Lcom/google/c/b/b/a/a/b/m;->a:Lcom/google/c/b/b/a/a/b/n;

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/c/b/b/a/a/b/n;

    invoke-direct {v0}, Lcom/google/c/b/b/a/a/b/n;-><init>()V

    iput-object v0, p0, Lcom/google/c/b/b/a/a/b/m;->a:Lcom/google/c/b/b/a/a/b/n;

    :cond_1
    iget-object v0, p0, Lcom/google/c/b/b/a/a/b/m;->a:Lcom/google/c/b/b/a/a/b/n;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Lcom/google/c/b/b/a/a/b/m;->b:Lcom/google/c/b/b/a/a/b/n;

    if-nez v0, :cond_2

    new-instance v0, Lcom/google/c/b/b/a/a/b/n;

    invoke-direct {v0}, Lcom/google/c/b/b/a/a/b/n;-><init>()V

    iput-object v0, p0, Lcom/google/c/b/b/a/a/b/m;->b:Lcom/google/c/b/b/a/a/b/n;

    :cond_2
    iget-object v0, p0, Lcom/google/c/b/b/a/a/b/m;->b:Lcom/google/c/b/b/a/a/b/n;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/c/b/b/a/a/b/m;->c:Ljava/lang/Boolean;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/c/b/b/a/a/b/m;->d:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 1126
    iget-object v0, p0, Lcom/google/c/b/b/a/a/b/m;->a:Lcom/google/c/b/b/a/a/b/n;

    if-eqz v0, :cond_0

    .line 1127
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/c/b/b/a/a/b/m;->a:Lcom/google/c/b/b/a/a/b/n;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 1129
    :cond_0
    iget-object v0, p0, Lcom/google/c/b/b/a/a/b/m;->b:Lcom/google/c/b/b/a/a/b/n;

    if-eqz v0, :cond_1

    .line 1130
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/c/b/b/a/a/b/m;->b:Lcom/google/c/b/b/a/a/b/n;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 1132
    :cond_1
    iget-object v0, p0, Lcom/google/c/b/b/a/a/b/m;->c:Ljava/lang/Boolean;

    if-eqz v0, :cond_2

    .line 1133
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/c/b/b/a/a/b/m;->c:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 1135
    :cond_2
    iget-object v0, p0, Lcom/google/c/b/b/a/a/b/m;->d:Ljava/lang/Integer;

    if-eqz v0, :cond_3

    .line 1136
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/c/b/b/a/a/b/m;->d:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->c(II)V

    .line 1138
    :cond_3
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/d;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 1139
    return-void
.end method
