.class public final Lcom/google/c/b/b/a/a/b/z;
.super Lcom/google/protobuf/nano/d;
.source "SourceFile"


# instance fields
.field public a:Lcom/google/c/b/b/a/a/b/aa;

.field public b:Lcom/google/c/b/b/a/a/b/ab;

.field public c:Lcom/google/c/b/b/a/a/b/ac;

.field public d:Ljava/lang/Integer;

.field public e:Lcom/google/c/b/b/a/a/b/ad;

.field public f:Ljava/lang/Integer;

.field public g:Ljava/lang/Integer;

.field public h:Ljava/lang/Boolean;

.field public i:Lcom/google/c/b/b/a/a/b/ae;

.field public j:Ljava/lang/Integer;

.field public k:Lcom/google/c/b/b/a/a/b/af;

.field public l:Lcom/google/c/b/b/a/a/b/ah;

.field public m:Lcom/google/c/b/b/a/a/b/ai;

.field public n:Ljava/lang/Integer;

.field public o:Lcom/google/c/b/b/a/a/b/ag;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1369
    invoke-direct {p0}, Lcom/google/protobuf/nano/d;-><init>()V

    .line 1370
    iput-object v0, p0, Lcom/google/c/b/b/a/a/b/z;->a:Lcom/google/c/b/b/a/a/b/aa;

    iput-object v0, p0, Lcom/google/c/b/b/a/a/b/z;->b:Lcom/google/c/b/b/a/a/b/ab;

    iput-object v0, p0, Lcom/google/c/b/b/a/a/b/z;->c:Lcom/google/c/b/b/a/a/b/ac;

    iput-object v0, p0, Lcom/google/c/b/b/a/a/b/z;->d:Ljava/lang/Integer;

    iput-object v0, p0, Lcom/google/c/b/b/a/a/b/z;->e:Lcom/google/c/b/b/a/a/b/ad;

    iput-object v0, p0, Lcom/google/c/b/b/a/a/b/z;->f:Ljava/lang/Integer;

    iput-object v0, p0, Lcom/google/c/b/b/a/a/b/z;->g:Ljava/lang/Integer;

    iput-object v0, p0, Lcom/google/c/b/b/a/a/b/z;->h:Ljava/lang/Boolean;

    iput-object v0, p0, Lcom/google/c/b/b/a/a/b/z;->i:Lcom/google/c/b/b/a/a/b/ae;

    iput-object v0, p0, Lcom/google/c/b/b/a/a/b/z;->j:Ljava/lang/Integer;

    iput-object v0, p0, Lcom/google/c/b/b/a/a/b/z;->k:Lcom/google/c/b/b/a/a/b/af;

    iput-object v0, p0, Lcom/google/c/b/b/a/a/b/z;->l:Lcom/google/c/b/b/a/a/b/ah;

    iput-object v0, p0, Lcom/google/c/b/b/a/a/b/z;->m:Lcom/google/c/b/b/a/a/b/ai;

    iput-object v0, p0, Lcom/google/c/b/b/a/a/b/z;->n:Ljava/lang/Integer;

    iput-object v0, p0, Lcom/google/c/b/b/a/a/b/z;->o:Lcom/google/c/b/b/a/a/b/ag;

    iput-object v0, p0, Lcom/google/c/b/b/a/a/b/z;->unknownFieldData:Lcom/google/protobuf/nano/f;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/c/b/b/a/a/b/z;->cachedSize:I

    .line 1371
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 1447
    invoke-super {p0}, Lcom/google/protobuf/nano/d;->computeSerializedSize()I

    move-result v0

    .line 1448
    iget-object v1, p0, Lcom/google/c/b/b/a/a/b/z;->a:Lcom/google/c/b/b/a/a/b/aa;

    if-eqz v1, :cond_0

    .line 1449
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/c/b/b/a/a/b/z;->a:Lcom/google/c/b/b/a/a/b/aa;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1452
    :cond_0
    iget-object v1, p0, Lcom/google/c/b/b/a/a/b/z;->b:Lcom/google/c/b/b/a/a/b/ab;

    if-eqz v1, :cond_1

    .line 1453
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/c/b/b/a/a/b/z;->b:Lcom/google/c/b/b/a/a/b/ab;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1456
    :cond_1
    iget-object v1, p0, Lcom/google/c/b/b/a/a/b/z;->c:Lcom/google/c/b/b/a/a/b/ac;

    if-eqz v1, :cond_2

    .line 1457
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/c/b/b/a/a/b/z;->c:Lcom/google/c/b/b/a/a/b/ac;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1460
    :cond_2
    iget-object v1, p0, Lcom/google/c/b/b/a/a/b/z;->d:Ljava/lang/Integer;

    if-eqz v1, :cond_3

    .line 1461
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/c/b/b/a/a/b/z;->d:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1464
    :cond_3
    iget-object v1, p0, Lcom/google/c/b/b/a/a/b/z;->e:Lcom/google/c/b/b/a/a/b/ad;

    if-eqz v1, :cond_4

    .line 1465
    const/4 v1, 0x5

    iget-object v2, p0, Lcom/google/c/b/b/a/a/b/z;->e:Lcom/google/c/b/b/a/a/b/ad;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1468
    :cond_4
    iget-object v1, p0, Lcom/google/c/b/b/a/a/b/z;->f:Ljava/lang/Integer;

    if-eqz v1, :cond_5

    .line 1469
    const/4 v1, 0x6

    iget-object v2, p0, Lcom/google/c/b/b/a/a/b/z;->f:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1472
    :cond_5
    iget-object v1, p0, Lcom/google/c/b/b/a/a/b/z;->g:Ljava/lang/Integer;

    if-eqz v1, :cond_6

    .line 1473
    const/4 v1, 0x7

    iget-object v2, p0, Lcom/google/c/b/b/a/a/b/z;->g:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1476
    :cond_6
    iget-object v1, p0, Lcom/google/c/b/b/a/a/b/z;->h:Ljava/lang/Boolean;

    if-eqz v1, :cond_7

    .line 1477
    const/16 v1, 0x8

    iget-object v2, p0, Lcom/google/c/b/b/a/a/b/z;->h:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 1480
    :cond_7
    iget-object v1, p0, Lcom/google/c/b/b/a/a/b/z;->i:Lcom/google/c/b/b/a/a/b/ae;

    if-eqz v1, :cond_8

    .line 1481
    const/16 v1, 0x9

    iget-object v2, p0, Lcom/google/c/b/b/a/a/b/z;->i:Lcom/google/c/b/b/a/a/b/ae;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1484
    :cond_8
    iget-object v1, p0, Lcom/google/c/b/b/a/a/b/z;->j:Ljava/lang/Integer;

    if-eqz v1, :cond_9

    .line 1485
    const/16 v1, 0xa

    iget-object v2, p0, Lcom/google/c/b/b/a/a/b/z;->j:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1488
    :cond_9
    iget-object v1, p0, Lcom/google/c/b/b/a/a/b/z;->k:Lcom/google/c/b/b/a/a/b/af;

    if-eqz v1, :cond_a

    .line 1489
    const/16 v1, 0xb

    iget-object v2, p0, Lcom/google/c/b/b/a/a/b/z;->k:Lcom/google/c/b/b/a/a/b/af;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1492
    :cond_a
    iget-object v1, p0, Lcom/google/c/b/b/a/a/b/z;->l:Lcom/google/c/b/b/a/a/b/ah;

    if-eqz v1, :cond_b

    .line 1493
    const/16 v1, 0xc

    iget-object v2, p0, Lcom/google/c/b/b/a/a/b/z;->l:Lcom/google/c/b/b/a/a/b/ah;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1496
    :cond_b
    iget-object v1, p0, Lcom/google/c/b/b/a/a/b/z;->m:Lcom/google/c/b/b/a/a/b/ai;

    if-eqz v1, :cond_c

    .line 1497
    const/16 v1, 0xd

    iget-object v2, p0, Lcom/google/c/b/b/a/a/b/z;->m:Lcom/google/c/b/b/a/a/b/ai;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1500
    :cond_c
    iget-object v1, p0, Lcom/google/c/b/b/a/a/b/z;->n:Ljava/lang/Integer;

    if-eqz v1, :cond_d

    .line 1501
    const/16 v1, 0xe

    iget-object v2, p0, Lcom/google/c/b/b/a/a/b/z;->n:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1504
    :cond_d
    iget-object v1, p0, Lcom/google/c/b/b/a/a/b/z;->o:Lcom/google/c/b/b/a/a/b/ag;

    if-eqz v1, :cond_e

    .line 1505
    const/16 v1, 0xf

    iget-object v2, p0, Lcom/google/c/b/b/a/a/b/z;->o:Lcom/google/c/b/b/a/a/b/ag;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1508
    :cond_e
    return v0
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 5
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/c/b/b/a/a/b/z;->storeUnknownField(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Lcom/google/c/b/b/a/a/b/z;->a:Lcom/google/c/b/b/a/a/b/aa;

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/c/b/b/a/a/b/aa;

    invoke-direct {v0}, Lcom/google/c/b/b/a/a/b/aa;-><init>()V

    iput-object v0, p0, Lcom/google/c/b/b/a/a/b/z;->a:Lcom/google/c/b/b/a/a/b/aa;

    :cond_1
    iget-object v0, p0, Lcom/google/c/b/b/a/a/b/z;->a:Lcom/google/c/b/b/a/a/b/aa;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Lcom/google/c/b/b/a/a/b/z;->b:Lcom/google/c/b/b/a/a/b/ab;

    if-nez v0, :cond_2

    new-instance v0, Lcom/google/c/b/b/a/a/b/ab;

    invoke-direct {v0}, Lcom/google/c/b/b/a/a/b/ab;-><init>()V

    iput-object v0, p0, Lcom/google/c/b/b/a/a/b/z;->b:Lcom/google/c/b/b/a/a/b/ab;

    :cond_2
    iget-object v0, p0, Lcom/google/c/b/b/a/a/b/z;->b:Lcom/google/c/b/b/a/a/b/ab;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Lcom/google/c/b/b/a/a/b/z;->c:Lcom/google/c/b/b/a/a/b/ac;

    if-nez v0, :cond_3

    new-instance v0, Lcom/google/c/b/b/a/a/b/ac;

    invoke-direct {v0}, Lcom/google/c/b/b/a/a/b/ac;-><init>()V

    iput-object v0, p0, Lcom/google/c/b/b/a/a/b/z;->c:Lcom/google/c/b/b/a/a/b/ac;

    :cond_3
    iget-object v0, p0, Lcom/google/c/b/b/a/a/b/z;->c:Lcom/google/c/b/b/a/a/b/ac;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/c/b/b/a/a/b/z;->d:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_5
    iget-object v0, p0, Lcom/google/c/b/b/a/a/b/z;->e:Lcom/google/c/b/b/a/a/b/ad;

    if-nez v0, :cond_4

    new-instance v0, Lcom/google/c/b/b/a/a/b/ad;

    invoke-direct {v0}, Lcom/google/c/b/b/a/a/b/ad;-><init>()V

    iput-object v0, p0, Lcom/google/c/b/b/a/a/b/z;->e:Lcom/google/c/b/b/a/a/b/ad;

    :cond_4
    iget-object v0, p0, Lcom/google/c/b/b/a/a/b/z;->e:Lcom/google/c/b/b/a/a/b/ad;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    packed-switch v0, :pswitch_data_1

    goto :goto_0

    :pswitch_1
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/c/b/b/a/a/b/z;->f:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_7
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    packed-switch v0, :pswitch_data_2

    goto :goto_0

    :pswitch_2
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/c/b/b/a/a/b/z;->g:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_8
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/c/b/b/a/a/b/z;->h:Ljava/lang/Boolean;

    goto/16 :goto_0

    :sswitch_9
    iget-object v0, p0, Lcom/google/c/b/b/a/a/b/z;->i:Lcom/google/c/b/b/a/a/b/ae;

    if-nez v0, :cond_5

    new-instance v0, Lcom/google/c/b/b/a/a/b/ae;

    invoke-direct {v0}, Lcom/google/c/b/b/a/a/b/ae;-><init>()V

    iput-object v0, p0, Lcom/google/c/b/b/a/a/b/z;->i:Lcom/google/c/b/b/a/a/b/ae;

    :cond_5
    iget-object v0, p0, Lcom/google/c/b/b/a/a/b/z;->i:Lcom/google/c/b/b/a/a/b/ae;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto/16 :goto_0

    :sswitch_a
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    packed-switch v0, :pswitch_data_3

    goto/16 :goto_0

    :pswitch_3
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/c/b/b/a/a/b/z;->j:Ljava/lang/Integer;

    goto/16 :goto_0

    :sswitch_b
    iget-object v0, p0, Lcom/google/c/b/b/a/a/b/z;->k:Lcom/google/c/b/b/a/a/b/af;

    if-nez v0, :cond_6

    new-instance v0, Lcom/google/c/b/b/a/a/b/af;

    invoke-direct {v0}, Lcom/google/c/b/b/a/a/b/af;-><init>()V

    iput-object v0, p0, Lcom/google/c/b/b/a/a/b/z;->k:Lcom/google/c/b/b/a/a/b/af;

    :cond_6
    iget-object v0, p0, Lcom/google/c/b/b/a/a/b/z;->k:Lcom/google/c/b/b/a/a/b/af;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto/16 :goto_0

    :sswitch_c
    iget-object v0, p0, Lcom/google/c/b/b/a/a/b/z;->l:Lcom/google/c/b/b/a/a/b/ah;

    if-nez v0, :cond_7

    new-instance v0, Lcom/google/c/b/b/a/a/b/ah;

    invoke-direct {v0}, Lcom/google/c/b/b/a/a/b/ah;-><init>()V

    iput-object v0, p0, Lcom/google/c/b/b/a/a/b/z;->l:Lcom/google/c/b/b/a/a/b/ah;

    :cond_7
    iget-object v0, p0, Lcom/google/c/b/b/a/a/b/z;->l:Lcom/google/c/b/b/a/a/b/ah;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto/16 :goto_0

    :sswitch_d
    iget-object v0, p0, Lcom/google/c/b/b/a/a/b/z;->m:Lcom/google/c/b/b/a/a/b/ai;

    if-nez v0, :cond_8

    new-instance v0, Lcom/google/c/b/b/a/a/b/ai;

    invoke-direct {v0}, Lcom/google/c/b/b/a/a/b/ai;-><init>()V

    iput-object v0, p0, Lcom/google/c/b/b/a/a/b/z;->m:Lcom/google/c/b/b/a/a/b/ai;

    :cond_8
    iget-object v0, p0, Lcom/google/c/b/b/a/a/b/z;->m:Lcom/google/c/b/b/a/a/b/ai;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto/16 :goto_0

    :sswitch_e
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    packed-switch v0, :pswitch_data_4

    goto/16 :goto_0

    :pswitch_4
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/c/b/b/a/a/b/z;->n:Ljava/lang/Integer;

    goto/16 :goto_0

    :sswitch_f
    iget-object v0, p0, Lcom/google/c/b/b/a/a/b/z;->o:Lcom/google/c/b/b/a/a/b/ag;

    if-nez v0, :cond_9

    new-instance v0, Lcom/google/c/b/b/a/a/b/ag;

    invoke-direct {v0}, Lcom/google/c/b/b/a/a/b/ag;-><init>()V

    iput-object v0, p0, Lcom/google/c/b/b/a/a/b/z;->o:Lcom/google/c/b/b/a/a/b/ag;

    :cond_9
    iget-object v0, p0, Lcom/google/c/b/b/a/a/b/z;->o:Lcom/google/c/b/b/a/a/b/ag;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
        0x2a -> :sswitch_5
        0x30 -> :sswitch_6
        0x38 -> :sswitch_7
        0x40 -> :sswitch_8
        0x4a -> :sswitch_9
        0x50 -> :sswitch_a
        0x5a -> :sswitch_b
        0x62 -> :sswitch_c
        0x6a -> :sswitch_d
        0x70 -> :sswitch_e
        0x7a -> :sswitch_f
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x0
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
    .end packed-switch

    :pswitch_data_3
    .packed-switch 0x0
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
    .end packed-switch

    :pswitch_data_4
    .packed-switch 0x0
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_4
    .end packed-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 1397
    iget-object v0, p0, Lcom/google/c/b/b/a/a/b/z;->a:Lcom/google/c/b/b/a/a/b/aa;

    if-eqz v0, :cond_0

    .line 1398
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/c/b/b/a/a/b/z;->a:Lcom/google/c/b/b/a/a/b/aa;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 1400
    :cond_0
    iget-object v0, p0, Lcom/google/c/b/b/a/a/b/z;->b:Lcom/google/c/b/b/a/a/b/ab;

    if-eqz v0, :cond_1

    .line 1401
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/c/b/b/a/a/b/z;->b:Lcom/google/c/b/b/a/a/b/ab;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 1403
    :cond_1
    iget-object v0, p0, Lcom/google/c/b/b/a/a/b/z;->c:Lcom/google/c/b/b/a/a/b/ac;

    if-eqz v0, :cond_2

    .line 1404
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/c/b/b/a/a/b/z;->c:Lcom/google/c/b/b/a/a/b/ac;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 1406
    :cond_2
    iget-object v0, p0, Lcom/google/c/b/b/a/a/b/z;->d:Ljava/lang/Integer;

    if-eqz v0, :cond_3

    .line 1407
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/c/b/b/a/a/b/z;->d:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 1409
    :cond_3
    iget-object v0, p0, Lcom/google/c/b/b/a/a/b/z;->e:Lcom/google/c/b/b/a/a/b/ad;

    if-eqz v0, :cond_4

    .line 1410
    const/4 v0, 0x5

    iget-object v1, p0, Lcom/google/c/b/b/a/a/b/z;->e:Lcom/google/c/b/b/a/a/b/ad;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 1412
    :cond_4
    iget-object v0, p0, Lcom/google/c/b/b/a/a/b/z;->f:Ljava/lang/Integer;

    if-eqz v0, :cond_5

    .line 1413
    const/4 v0, 0x6

    iget-object v1, p0, Lcom/google/c/b/b/a/a/b/z;->f:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 1415
    :cond_5
    iget-object v0, p0, Lcom/google/c/b/b/a/a/b/z;->g:Ljava/lang/Integer;

    if-eqz v0, :cond_6

    .line 1416
    const/4 v0, 0x7

    iget-object v1, p0, Lcom/google/c/b/b/a/a/b/z;->g:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 1418
    :cond_6
    iget-object v0, p0, Lcom/google/c/b/b/a/a/b/z;->h:Ljava/lang/Boolean;

    if-eqz v0, :cond_7

    .line 1419
    const/16 v0, 0x8

    iget-object v1, p0, Lcom/google/c/b/b/a/a/b/z;->h:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 1421
    :cond_7
    iget-object v0, p0, Lcom/google/c/b/b/a/a/b/z;->i:Lcom/google/c/b/b/a/a/b/ae;

    if-eqz v0, :cond_8

    .line 1422
    const/16 v0, 0x9

    iget-object v1, p0, Lcom/google/c/b/b/a/a/b/z;->i:Lcom/google/c/b/b/a/a/b/ae;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 1424
    :cond_8
    iget-object v0, p0, Lcom/google/c/b/b/a/a/b/z;->j:Ljava/lang/Integer;

    if-eqz v0, :cond_9

    .line 1425
    const/16 v0, 0xa

    iget-object v1, p0, Lcom/google/c/b/b/a/a/b/z;->j:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 1427
    :cond_9
    iget-object v0, p0, Lcom/google/c/b/b/a/a/b/z;->k:Lcom/google/c/b/b/a/a/b/af;

    if-eqz v0, :cond_a

    .line 1428
    const/16 v0, 0xb

    iget-object v1, p0, Lcom/google/c/b/b/a/a/b/z;->k:Lcom/google/c/b/b/a/a/b/af;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 1430
    :cond_a
    iget-object v0, p0, Lcom/google/c/b/b/a/a/b/z;->l:Lcom/google/c/b/b/a/a/b/ah;

    if-eqz v0, :cond_b

    .line 1431
    const/16 v0, 0xc

    iget-object v1, p0, Lcom/google/c/b/b/a/a/b/z;->l:Lcom/google/c/b/b/a/a/b/ah;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 1433
    :cond_b
    iget-object v0, p0, Lcom/google/c/b/b/a/a/b/z;->m:Lcom/google/c/b/b/a/a/b/ai;

    if-eqz v0, :cond_c

    .line 1434
    const/16 v0, 0xd

    iget-object v1, p0, Lcom/google/c/b/b/a/a/b/z;->m:Lcom/google/c/b/b/a/a/b/ai;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 1436
    :cond_c
    iget-object v0, p0, Lcom/google/c/b/b/a/a/b/z;->n:Ljava/lang/Integer;

    if-eqz v0, :cond_d

    .line 1437
    const/16 v0, 0xe

    iget-object v1, p0, Lcom/google/c/b/b/a/a/b/z;->n:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 1439
    :cond_d
    iget-object v0, p0, Lcom/google/c/b/b/a/a/b/z;->o:Lcom/google/c/b/b/a/a/b/ag;

    if-eqz v0, :cond_e

    .line 1440
    const/16 v0, 0xf

    iget-object v1, p0, Lcom/google/c/b/b/a/a/b/z;->o:Lcom/google/c/b/b/a/a/b/ag;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 1442
    :cond_e
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/d;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 1443
    return-void
.end method
