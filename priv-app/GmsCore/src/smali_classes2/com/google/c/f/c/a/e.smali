.class public final Lcom/google/c/f/c/a/e;
.super Lcom/google/protobuf/nano/d;
.source "SourceFile"


# static fields
.field private static volatile g:[Lcom/google/c/f/c/a/e;


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/String;

.field public e:Ljava/lang/String;

.field public f:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 172
    invoke-direct {p0}, Lcom/google/protobuf/nano/d;-><init>()V

    .line 173
    iput-object v0, p0, Lcom/google/c/f/c/a/e;->a:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/c/f/c/a/e;->b:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/c/f/c/a/e;->c:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/c/f/c/a/e;->d:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/c/f/c/a/e;->e:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/c/f/c/a/e;->f:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/c/f/c/a/e;->unknownFieldData:Lcom/google/protobuf/nano/f;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/c/f/c/a/e;->cachedSize:I

    .line 174
    return-void
.end method

.method public static a()[Lcom/google/c/f/c/a/e;
    .locals 2

    .prologue
    .line 143
    sget-object v0, Lcom/google/c/f/c/a/e;->g:[Lcom/google/c/f/c/a/e;

    if-nez v0, :cond_1

    .line 144
    sget-object v1, Lcom/google/protobuf/nano/h;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 146
    :try_start_0
    sget-object v0, Lcom/google/c/f/c/a/e;->g:[Lcom/google/c/f/c/a/e;

    if-nez v0, :cond_0

    .line 147
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/c/f/c/a/e;

    sput-object v0, Lcom/google/c/f/c/a/e;->g:[Lcom/google/c/f/c/a/e;

    .line 149
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 151
    :cond_1
    sget-object v0, Lcom/google/c/f/c/a/e;->g:[Lcom/google/c/f/c/a/e;

    return-object v0

    .line 149
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 214
    invoke-super {p0}, Lcom/google/protobuf/nano/d;->computeSerializedSize()I

    move-result v0

    .line 215
    iget-object v1, p0, Lcom/google/c/f/c/a/e;->a:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 216
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/c/f/c/a/e;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 219
    :cond_0
    iget-object v1, p0, Lcom/google/c/f/c/a/e;->b:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 220
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/c/f/c/a/e;->b:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 223
    :cond_1
    iget-object v1, p0, Lcom/google/c/f/c/a/e;->c:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 224
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/c/f/c/a/e;->c:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 227
    :cond_2
    iget-object v1, p0, Lcom/google/c/f/c/a/e;->d:Ljava/lang/String;

    if-eqz v1, :cond_3

    .line 228
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/c/f/c/a/e;->d:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 231
    :cond_3
    iget-object v1, p0, Lcom/google/c/f/c/a/e;->e:Ljava/lang/String;

    if-eqz v1, :cond_4

    .line 232
    const/4 v1, 0x5

    iget-object v2, p0, Lcom/google/c/f/c/a/e;->e:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 235
    :cond_4
    iget-object v1, p0, Lcom/google/c/f/c/a/e;->f:Ljava/lang/String;

    if-eqz v1, :cond_5

    .line 236
    const/4 v1, 0x6

    iget-object v2, p0, Lcom/google/c/f/c/a/e;->f:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 239
    :cond_5
    return v0
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 137
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/c/f/c/a/e;->storeUnknownField(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/c/f/c/a/e;->a:Ljava/lang/String;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/c/f/c/a/e;->b:Ljava/lang/String;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/c/f/c/a/e;->c:Ljava/lang/String;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/c/f/c/a/e;->d:Ljava/lang/String;

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/c/f/c/a/e;->e:Ljava/lang/String;

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/c/f/c/a/e;->f:Ljava/lang/String;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 191
    iget-object v0, p0, Lcom/google/c/f/c/a/e;->a:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 192
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/c/f/c/a/e;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 194
    :cond_0
    iget-object v0, p0, Lcom/google/c/f/c/a/e;->b:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 195
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/c/f/c/a/e;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 197
    :cond_1
    iget-object v0, p0, Lcom/google/c/f/c/a/e;->c:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 198
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/c/f/c/a/e;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 200
    :cond_2
    iget-object v0, p0, Lcom/google/c/f/c/a/e;->d:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 201
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/c/f/c/a/e;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 203
    :cond_3
    iget-object v0, p0, Lcom/google/c/f/c/a/e;->e:Ljava/lang/String;

    if-eqz v0, :cond_4

    .line 204
    const/4 v0, 0x5

    iget-object v1, p0, Lcom/google/c/f/c/a/e;->e:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 206
    :cond_4
    iget-object v0, p0, Lcom/google/c/f/c/a/e;->f:Ljava/lang/String;

    if-eqz v0, :cond_5

    .line 207
    const/4 v0, 0x6

    iget-object v1, p0, Lcom/google/c/f/c/a/e;->f:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 209
    :cond_5
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/d;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 210
    return-void
.end method
