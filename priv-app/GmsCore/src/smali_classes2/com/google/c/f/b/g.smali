.class public final Lcom/google/c/f/b/g;
.super Lcom/google/protobuf/nano/d;
.source "SourceFile"


# static fields
.field private static volatile e:[Lcom/google/c/f/b/g;


# instance fields
.field public a:Lcom/google/c/f/b/j;

.field public b:Lcom/google/c/f/b/k;

.field public c:[Lcom/google/c/f/b/h;

.field public d:[Lcom/google/c/f/b/i;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1493
    invoke-direct {p0}, Lcom/google/protobuf/nano/d;-><init>()V

    .line 1494
    iput-object v1, p0, Lcom/google/c/f/b/g;->a:Lcom/google/c/f/b/j;

    iput-object v1, p0, Lcom/google/c/f/b/g;->b:Lcom/google/c/f/b/k;

    invoke-static {}, Lcom/google/c/f/b/h;->a()[Lcom/google/c/f/b/h;

    move-result-object v0

    iput-object v0, p0, Lcom/google/c/f/b/g;->c:[Lcom/google/c/f/b/h;

    invoke-static {}, Lcom/google/c/f/b/i;->a()[Lcom/google/c/f/b/i;

    move-result-object v0

    iput-object v0, p0, Lcom/google/c/f/b/g;->d:[Lcom/google/c/f/b/i;

    iput-object v1, p0, Lcom/google/c/f/b/g;->unknownFieldData:Lcom/google/protobuf/nano/f;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/c/f/b/g;->cachedSize:I

    .line 1495
    return-void
.end method

.method public static a()[Lcom/google/c/f/b/g;
    .locals 2

    .prologue
    .line 1470
    sget-object v0, Lcom/google/c/f/b/g;->e:[Lcom/google/c/f/b/g;

    if-nez v0, :cond_1

    .line 1471
    sget-object v1, Lcom/google/protobuf/nano/h;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 1473
    :try_start_0
    sget-object v0, Lcom/google/c/f/b/g;->e:[Lcom/google/c/f/b/g;

    if-nez v0, :cond_0

    .line 1474
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/c/f/b/g;

    sput-object v0, Lcom/google/c/f/b/g;->e:[Lcom/google/c/f/b/g;

    .line 1476
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1478
    :cond_1
    sget-object v0, Lcom/google/c/f/b/g;->e:[Lcom/google/c/f/b/g;

    return-object v0

    .line 1476
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 1537
    invoke-super {p0}, Lcom/google/protobuf/nano/d;->computeSerializedSize()I

    move-result v0

    .line 1538
    iget-object v2, p0, Lcom/google/c/f/b/g;->a:Lcom/google/c/f/b/j;

    if-eqz v2, :cond_0

    .line 1539
    const/4 v2, 0x1

    iget-object v3, p0, Lcom/google/c/f/b/g;->a:Lcom/google/c/f/b/j;

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v2

    add-int/2addr v0, v2

    .line 1542
    :cond_0
    iget-object v2, p0, Lcom/google/c/f/b/g;->b:Lcom/google/c/f/b/k;

    if-eqz v2, :cond_1

    .line 1543
    const/4 v2, 0x2

    iget-object v3, p0, Lcom/google/c/f/b/g;->b:Lcom/google/c/f/b/k;

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v2

    add-int/2addr v0, v2

    .line 1546
    :cond_1
    iget-object v2, p0, Lcom/google/c/f/b/g;->c:[Lcom/google/c/f/b/h;

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/google/c/f/b/g;->c:[Lcom/google/c/f/b/h;

    array-length v2, v2

    if-lez v2, :cond_4

    move v2, v0

    move v0, v1

    .line 1547
    :goto_0
    iget-object v3, p0, Lcom/google/c/f/b/g;->c:[Lcom/google/c/f/b/h;

    array-length v3, v3

    if-ge v0, v3, :cond_3

    .line 1548
    iget-object v3, p0, Lcom/google/c/f/b/g;->c:[Lcom/google/c/f/b/h;

    aget-object v3, v3, v0

    .line 1549
    if-eqz v3, :cond_2

    .line 1550
    const/4 v4, 0x3

    invoke-static {v4, v3}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v3

    add-int/2addr v2, v3

    .line 1547
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_3
    move v0, v2

    .line 1555
    :cond_4
    iget-object v2, p0, Lcom/google/c/f/b/g;->d:[Lcom/google/c/f/b/i;

    if-eqz v2, :cond_6

    iget-object v2, p0, Lcom/google/c/f/b/g;->d:[Lcom/google/c/f/b/i;

    array-length v2, v2

    if-lez v2, :cond_6

    .line 1556
    :goto_1
    iget-object v2, p0, Lcom/google/c/f/b/g;->d:[Lcom/google/c/f/b/i;

    array-length v2, v2

    if-ge v1, v2, :cond_6

    .line 1557
    iget-object v2, p0, Lcom/google/c/f/b/g;->d:[Lcom/google/c/f/b/i;

    aget-object v2, v2, v1

    .line 1558
    if-eqz v2, :cond_5

    .line 1559
    const/4 v3, 0x4

    invoke-static {v3, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v2

    add-int/2addr v0, v2

    .line 1556
    :cond_5
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 1564
    :cond_6
    return v0
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 1464
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/c/f/b/g;->storeUnknownField(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Lcom/google/c/f/b/g;->a:Lcom/google/c/f/b/j;

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/c/f/b/j;

    invoke-direct {v0}, Lcom/google/c/f/b/j;-><init>()V

    iput-object v0, p0, Lcom/google/c/f/b/g;->a:Lcom/google/c/f/b/j;

    :cond_1
    iget-object v0, p0, Lcom/google/c/f/b/g;->a:Lcom/google/c/f/b/j;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Lcom/google/c/f/b/g;->b:Lcom/google/c/f/b/k;

    if-nez v0, :cond_2

    new-instance v0, Lcom/google/c/f/b/k;

    invoke-direct {v0}, Lcom/google/c/f/b/k;-><init>()V

    iput-object v0, p0, Lcom/google/c/f/b/g;->b:Lcom/google/c/f/b/k;

    :cond_2
    iget-object v0, p0, Lcom/google/c/f/b/g;->b:Lcom/google/c/f/b/k;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_3
    const/16 v0, 0x1a

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->b(Lcom/google/protobuf/nano/a;I)I

    move-result v2

    iget-object v0, p0, Lcom/google/c/f/b/g;->c:[Lcom/google/c/f/b/h;

    if-nez v0, :cond_4

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lcom/google/c/f/b/h;

    if-eqz v0, :cond_3

    iget-object v3, p0, Lcom/google/c/f/b/g;->c:[Lcom/google/c/f/b/h;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_3
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_5

    new-instance v3, Lcom/google/c/f/b/h;

    invoke-direct {v3}, Lcom/google/c/f/b/h;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_4
    iget-object v0, p0, Lcom/google/c/f/b/g;->c:[Lcom/google/c/f/b/h;

    array-length v0, v0

    goto :goto_1

    :cond_5
    new-instance v3, Lcom/google/c/f/b/h;

    invoke-direct {v3}, Lcom/google/c/f/b/h;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    iput-object v2, p0, Lcom/google/c/f/b/g;->c:[Lcom/google/c/f/b/h;

    goto :goto_0

    :sswitch_4
    const/16 v0, 0x22

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->b(Lcom/google/protobuf/nano/a;I)I

    move-result v2

    iget-object v0, p0, Lcom/google/c/f/b/g;->d:[Lcom/google/c/f/b/i;

    if-nez v0, :cond_7

    move v0, v1

    :goto_3
    add-int/2addr v2, v0

    new-array v2, v2, [Lcom/google/c/f/b/i;

    if-eqz v0, :cond_6

    iget-object v3, p0, Lcom/google/c/f/b/g;->d:[Lcom/google/c/f/b/i;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_6
    :goto_4
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_8

    new-instance v3, Lcom/google/c/f/b/i;

    invoke-direct {v3}, Lcom/google/c/f/b/i;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_7
    iget-object v0, p0, Lcom/google/c/f/b/g;->d:[Lcom/google/c/f/b/i;

    array-length v0, v0

    goto :goto_3

    :cond_8
    new-instance v3, Lcom/google/c/f/b/i;

    invoke-direct {v3}, Lcom/google/c/f/b/i;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    iput-object v2, p0, Lcom/google/c/f/b/g;->d:[Lcom/google/c/f/b/i;

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 1510
    iget-object v0, p0, Lcom/google/c/f/b/g;->a:Lcom/google/c/f/b/j;

    if-eqz v0, :cond_0

    .line 1511
    const/4 v0, 0x1

    iget-object v2, p0, Lcom/google/c/f/b/g;->a:Lcom/google/c/f/b/j;

    invoke-virtual {p1, v0, v2}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 1513
    :cond_0
    iget-object v0, p0, Lcom/google/c/f/b/g;->b:Lcom/google/c/f/b/k;

    if-eqz v0, :cond_1

    .line 1514
    const/4 v0, 0x2

    iget-object v2, p0, Lcom/google/c/f/b/g;->b:Lcom/google/c/f/b/k;

    invoke-virtual {p1, v0, v2}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 1516
    :cond_1
    iget-object v0, p0, Lcom/google/c/f/b/g;->c:[Lcom/google/c/f/b/h;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/c/f/b/g;->c:[Lcom/google/c/f/b/h;

    array-length v0, v0

    if-lez v0, :cond_3

    move v0, v1

    .line 1517
    :goto_0
    iget-object v2, p0, Lcom/google/c/f/b/g;->c:[Lcom/google/c/f/b/h;

    array-length v2, v2

    if-ge v0, v2, :cond_3

    .line 1518
    iget-object v2, p0, Lcom/google/c/f/b/g;->c:[Lcom/google/c/f/b/h;

    aget-object v2, v2, v0

    .line 1519
    if-eqz v2, :cond_2

    .line 1520
    const/4 v3, 0x3

    invoke-virtual {p1, v3, v2}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 1517
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1524
    :cond_3
    iget-object v0, p0, Lcom/google/c/f/b/g;->d:[Lcom/google/c/f/b/i;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/google/c/f/b/g;->d:[Lcom/google/c/f/b/i;

    array-length v0, v0

    if-lez v0, :cond_5

    .line 1525
    :goto_1
    iget-object v0, p0, Lcom/google/c/f/b/g;->d:[Lcom/google/c/f/b/i;

    array-length v0, v0

    if-ge v1, v0, :cond_5

    .line 1526
    iget-object v0, p0, Lcom/google/c/f/b/g;->d:[Lcom/google/c/f/b/i;

    aget-object v0, v0, v1

    .line 1527
    if-eqz v0, :cond_4

    .line 1528
    const/4 v2, 0x4

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 1525
    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 1532
    :cond_5
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/d;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 1533
    return-void
.end method
