.class public final Lcom/google/c/f/c/a/r;
.super Lcom/google/protobuf/nano/d;
.source "SourceFile"


# static fields
.field private static volatile g:[Lcom/google/c/f/c/a/r;


# instance fields
.field public a:Ljava/lang/Integer;

.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/Boolean;

.field public e:Ljava/lang/String;

.field public f:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1340
    invoke-direct {p0}, Lcom/google/protobuf/nano/d;-><init>()V

    .line 1341
    iput-object v0, p0, Lcom/google/c/f/c/a/r;->a:Ljava/lang/Integer;

    iput-object v0, p0, Lcom/google/c/f/c/a/r;->b:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/c/f/c/a/r;->c:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/c/f/c/a/r;->d:Ljava/lang/Boolean;

    iput-object v0, p0, Lcom/google/c/f/c/a/r;->e:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/c/f/c/a/r;->f:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/c/f/c/a/r;->unknownFieldData:Lcom/google/protobuf/nano/f;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/c/f/c/a/r;->cachedSize:I

    .line 1342
    return-void
.end method

.method public static a()[Lcom/google/c/f/c/a/r;
    .locals 2

    .prologue
    .line 1311
    sget-object v0, Lcom/google/c/f/c/a/r;->g:[Lcom/google/c/f/c/a/r;

    if-nez v0, :cond_1

    .line 1312
    sget-object v1, Lcom/google/protobuf/nano/h;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 1314
    :try_start_0
    sget-object v0, Lcom/google/c/f/c/a/r;->g:[Lcom/google/c/f/c/a/r;

    if-nez v0, :cond_0

    .line 1315
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/c/f/c/a/r;

    sput-object v0, Lcom/google/c/f/c/a/r;->g:[Lcom/google/c/f/c/a/r;

    .line 1317
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1319
    :cond_1
    sget-object v0, Lcom/google/c/f/c/a/r;->g:[Lcom/google/c/f/c/a/r;

    return-object v0

    .line 1317
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 1380
    invoke-super {p0}, Lcom/google/protobuf/nano/d;->computeSerializedSize()I

    move-result v0

    .line 1381
    iget-object v1, p0, Lcom/google/c/f/c/a/r;->a:Ljava/lang/Integer;

    if-eqz v1, :cond_0

    .line 1382
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/c/f/c/a/r;->a:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1385
    :cond_0
    iget-object v1, p0, Lcom/google/c/f/c/a/r;->b:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 1386
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/c/f/c/a/r;->b:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1389
    :cond_1
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/c/f/c/a/r;->c:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1391
    iget-object v1, p0, Lcom/google/c/f/c/a/r;->d:Ljava/lang/Boolean;

    if-eqz v1, :cond_2

    .line 1392
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/c/f/c/a/r;->d:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 1395
    :cond_2
    iget-object v1, p0, Lcom/google/c/f/c/a/r;->e:Ljava/lang/String;

    if-eqz v1, :cond_3

    .line 1396
    const/4 v1, 0x5

    iget-object v2, p0, Lcom/google/c/f/c/a/r;->e:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1399
    :cond_3
    iget-object v1, p0, Lcom/google/c/f/c/a/r;->f:Ljava/lang/String;

    if-eqz v1, :cond_4

    .line 1400
    const/4 v1, 0x6

    iget-object v2, p0, Lcom/google/c/f/c/a/r;->f:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1403
    :cond_4
    return v0
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 1305
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/c/f/c/a/r;->storeUnknownField(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/c/f/c/a/r;->a:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/c/f/c/a/r;->b:Ljava/lang/String;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/c/f/c/a/r;->c:Ljava/lang/String;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/c/f/c/a/r;->d:Ljava/lang/Boolean;

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/c/f/c/a/r;->e:Ljava/lang/String;

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/c/f/c/a/r;->f:Ljava/lang/String;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 1359
    iget-object v0, p0, Lcom/google/c/f/c/a/r;->a:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 1360
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/c/f/c/a/r;->a:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 1362
    :cond_0
    iget-object v0, p0, Lcom/google/c/f/c/a/r;->b:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 1363
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/c/f/c/a/r;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 1365
    :cond_1
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/c/f/c/a/r;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 1366
    iget-object v0, p0, Lcom/google/c/f/c/a/r;->d:Ljava/lang/Boolean;

    if-eqz v0, :cond_2

    .line 1367
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/c/f/c/a/r;->d:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 1369
    :cond_2
    iget-object v0, p0, Lcom/google/c/f/c/a/r;->e:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 1370
    const/4 v0, 0x5

    iget-object v1, p0, Lcom/google/c/f/c/a/r;->e:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 1372
    :cond_3
    iget-object v0, p0, Lcom/google/c/f/c/a/r;->f:Ljava/lang/String;

    if-eqz v0, :cond_4

    .line 1373
    const/4 v0, 0x6

    iget-object v1, p0, Lcom/google/c/f/c/a/r;->f:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 1375
    :cond_4
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/d;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 1376
    return-void
.end method
