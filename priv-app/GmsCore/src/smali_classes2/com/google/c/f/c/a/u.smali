.class public final Lcom/google/c/f/c/a/u;
.super Lcom/google/protobuf/nano/d;
.source "SourceFile"


# instance fields
.field public a:Ljava/lang/String;

.field public b:Lcom/google/c/f/c/a/v;

.field public c:Ljava/lang/Integer;

.field public d:Lcom/google/c/f/c/a/t;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 641
    invoke-direct {p0}, Lcom/google/protobuf/nano/d;-><init>()V

    .line 642
    iput-object v0, p0, Lcom/google/c/f/c/a/u;->a:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/c/f/c/a/u;->b:Lcom/google/c/f/c/a/v;

    iput-object v0, p0, Lcom/google/c/f/c/a/u;->c:Ljava/lang/Integer;

    iput-object v0, p0, Lcom/google/c/f/c/a/u;->d:Lcom/google/c/f/c/a/t;

    iput-object v0, p0, Lcom/google/c/f/c/a/u;->unknownFieldData:Lcom/google/protobuf/nano/f;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/c/f/c/a/u;->cachedSize:I

    .line 643
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 675
    invoke-super {p0}, Lcom/google/protobuf/nano/d;->computeSerializedSize()I

    move-result v0

    .line 676
    iget-object v1, p0, Lcom/google/c/f/c/a/u;->a:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 677
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/c/f/c/a/u;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 680
    :cond_0
    iget-object v1, p0, Lcom/google/c/f/c/a/u;->b:Lcom/google/c/f/c/a/v;

    if-eqz v1, :cond_1

    .line 681
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/c/f/c/a/u;->b:Lcom/google/c/f/c/a/v;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 684
    :cond_1
    iget-object v1, p0, Lcom/google/c/f/c/a/u;->c:Ljava/lang/Integer;

    if-eqz v1, :cond_2

    .line 685
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/c/f/c/a/u;->c:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 688
    :cond_2
    iget-object v1, p0, Lcom/google/c/f/c/a/u;->d:Lcom/google/c/f/c/a/t;

    if-eqz v1, :cond_3

    .line 689
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/c/f/c/a/u;->d:Lcom/google/c/f/c/a/t;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 692
    :cond_3
    return v0
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 612
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/c/f/c/a/u;->storeUnknownField(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/c/f/c/a/u;->a:Ljava/lang/String;

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Lcom/google/c/f/c/a/u;->b:Lcom/google/c/f/c/a/v;

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/c/f/c/a/v;

    invoke-direct {v0}, Lcom/google/c/f/c/a/v;-><init>()V

    iput-object v0, p0, Lcom/google/c/f/c/a/u;->b:Lcom/google/c/f/c/a/v;

    :cond_1
    iget-object v0, p0, Lcom/google/c/f/c/a/u;->b:Lcom/google/c/f/c/a/v;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/c/f/c/a/u;->c:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_4
    iget-object v0, p0, Lcom/google/c/f/c/a/u;->d:Lcom/google/c/f/c/a/t;

    if-nez v0, :cond_2

    new-instance v0, Lcom/google/c/f/c/a/t;

    invoke-direct {v0}, Lcom/google/c/f/c/a/t;-><init>()V

    iput-object v0, p0, Lcom/google/c/f/c/a/u;->d:Lcom/google/c/f/c/a/t;

    :cond_2
    iget-object v0, p0, Lcom/google/c/f/c/a/u;->d:Lcom/google/c/f/c/a/t;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 658
    iget-object v0, p0, Lcom/google/c/f/c/a/u;->a:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 659
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/c/f/c/a/u;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 661
    :cond_0
    iget-object v0, p0, Lcom/google/c/f/c/a/u;->b:Lcom/google/c/f/c/a/v;

    if-eqz v0, :cond_1

    .line 662
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/c/f/c/a/u;->b:Lcom/google/c/f/c/a/v;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 664
    :cond_1
    iget-object v0, p0, Lcom/google/c/f/c/a/u;->c:Ljava/lang/Integer;

    if-eqz v0, :cond_2

    .line 665
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/c/f/c/a/u;->c:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 667
    :cond_2
    iget-object v0, p0, Lcom/google/c/f/c/a/u;->d:Lcom/google/c/f/c/a/t;

    if-eqz v0, :cond_3

    .line 668
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/c/f/c/a/u;->d:Lcom/google/c/f/c/a/t;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 670
    :cond_3
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/d;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 671
    return-void
.end method
