.class public final Lcom/google/c/f/c/a/h;
.super Lcom/google/protobuf/nano/d;
.source "SourceFile"


# instance fields
.field public a:Ljava/lang/Integer;

.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/Integer;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1506
    invoke-direct {p0}, Lcom/google/protobuf/nano/d;-><init>()V

    .line 1507
    iput-object v0, p0, Lcom/google/c/f/c/a/h;->a:Ljava/lang/Integer;

    iput-object v0, p0, Lcom/google/c/f/c/a/h;->b:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/c/f/c/a/h;->c:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/c/f/c/a/h;->d:Ljava/lang/Integer;

    iput-object v0, p0, Lcom/google/c/f/c/a/h;->unknownFieldData:Lcom/google/protobuf/nano/f;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/c/f/c/a/h;->cachedSize:I

    .line 1508
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 1540
    invoke-super {p0}, Lcom/google/protobuf/nano/d;->computeSerializedSize()I

    move-result v0

    .line 1541
    iget-object v1, p0, Lcom/google/c/f/c/a/h;->a:Ljava/lang/Integer;

    if-eqz v1, :cond_0

    .line 1542
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/c/f/c/a/h;->a:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1545
    :cond_0
    iget-object v1, p0, Lcom/google/c/f/c/a/h;->b:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 1546
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/c/f/c/a/h;->b:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1549
    :cond_1
    iget-object v1, p0, Lcom/google/c/f/c/a/h;->c:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 1550
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/c/f/c/a/h;->c:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1553
    :cond_2
    iget-object v1, p0, Lcom/google/c/f/c/a/h;->d:Ljava/lang/Integer;

    if-eqz v1, :cond_3

    .line 1554
    const/4 v1, 0x5

    iget-object v2, p0, Lcom/google/c/f/c/a/h;->d:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1557
    :cond_3
    return v0
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 1477
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/c/f/c/a/h;->storeUnknownField(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/c/f/c/a/h;->a:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/c/f/c/a/h;->b:Ljava/lang/String;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/c/f/c/a/h;->c:Ljava/lang/String;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/c/f/c/a/h;->d:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x28 -> :sswitch_4
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 1523
    iget-object v0, p0, Lcom/google/c/f/c/a/h;->a:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 1524
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/c/f/c/a/h;->a:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 1526
    :cond_0
    iget-object v0, p0, Lcom/google/c/f/c/a/h;->b:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 1527
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/c/f/c/a/h;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 1529
    :cond_1
    iget-object v0, p0, Lcom/google/c/f/c/a/h;->c:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 1530
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/c/f/c/a/h;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 1532
    :cond_2
    iget-object v0, p0, Lcom/google/c/f/c/a/h;->d:Ljava/lang/Integer;

    if-eqz v0, :cond_3

    .line 1533
    const/4 v0, 0x5

    iget-object v1, p0, Lcom/google/c/f/c/a/h;->d:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 1535
    :cond_3
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/d;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 1536
    return-void
.end method
