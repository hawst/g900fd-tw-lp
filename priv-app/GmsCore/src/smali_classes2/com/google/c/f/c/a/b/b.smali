.class public final Lcom/google/c/f/c/a/b/b;
.super Lcom/google/protobuf/nano/d;
.source "SourceFile"


# instance fields
.field public a:[Lcom/google/c/f/c/a/c;

.field public b:[Lcom/google/c/f/c/a/i;

.field public c:Lcom/google/c/f/c/a/m;

.field public d:Ljava/lang/Integer;

.field public e:Ljava/lang/Long;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 40
    invoke-direct {p0}, Lcom/google/protobuf/nano/d;-><init>()V

    .line 41
    invoke-static {}, Lcom/google/c/f/c/a/c;->a()[Lcom/google/c/f/c/a/c;

    move-result-object v0

    iput-object v0, p0, Lcom/google/c/f/c/a/b/b;->a:[Lcom/google/c/f/c/a/c;

    invoke-static {}, Lcom/google/c/f/c/a/i;->a()[Lcom/google/c/f/c/a/i;

    move-result-object v0

    iput-object v0, p0, Lcom/google/c/f/c/a/b/b;->b:[Lcom/google/c/f/c/a/i;

    iput-object v1, p0, Lcom/google/c/f/c/a/b/b;->c:Lcom/google/c/f/c/a/m;

    iput-object v1, p0, Lcom/google/c/f/c/a/b/b;->d:Ljava/lang/Integer;

    iput-object v1, p0, Lcom/google/c/f/c/a/b/b;->e:Ljava/lang/Long;

    iput-object v1, p0, Lcom/google/c/f/c/a/b/b;->unknownFieldData:Lcom/google/protobuf/nano/f;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/c/f/c/a/b/b;->cachedSize:I

    .line 42
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 88
    invoke-super {p0}, Lcom/google/protobuf/nano/d;->computeSerializedSize()I

    move-result v0

    .line 89
    iget-object v2, p0, Lcom/google/c/f/c/a/b/b;->a:[Lcom/google/c/f/c/a/c;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/google/c/f/c/a/b/b;->a:[Lcom/google/c/f/c/a/c;

    array-length v2, v2

    if-lez v2, :cond_2

    move v2, v0

    move v0, v1

    .line 90
    :goto_0
    iget-object v3, p0, Lcom/google/c/f/c/a/b/b;->a:[Lcom/google/c/f/c/a/c;

    array-length v3, v3

    if-ge v0, v3, :cond_1

    .line 91
    iget-object v3, p0, Lcom/google/c/f/c/a/b/b;->a:[Lcom/google/c/f/c/a/c;

    aget-object v3, v3, v0

    .line 92
    if-eqz v3, :cond_0

    .line 93
    const/4 v4, 0x1

    invoke-static {v4, v3}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v3

    add-int/2addr v2, v3

    .line 90
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    move v0, v2

    .line 98
    :cond_2
    iget-object v2, p0, Lcom/google/c/f/c/a/b/b;->b:[Lcom/google/c/f/c/a/i;

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/google/c/f/c/a/b/b;->b:[Lcom/google/c/f/c/a/i;

    array-length v2, v2

    if-lez v2, :cond_4

    .line 99
    :goto_1
    iget-object v2, p0, Lcom/google/c/f/c/a/b/b;->b:[Lcom/google/c/f/c/a/i;

    array-length v2, v2

    if-ge v1, v2, :cond_4

    .line 100
    iget-object v2, p0, Lcom/google/c/f/c/a/b/b;->b:[Lcom/google/c/f/c/a/i;

    aget-object v2, v2, v1

    .line 101
    if-eqz v2, :cond_3

    .line 102
    const/4 v3, 0x2

    invoke-static {v3, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v2

    add-int/2addr v0, v2

    .line 99
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 107
    :cond_4
    iget-object v1, p0, Lcom/google/c/f/c/a/b/b;->c:Lcom/google/c/f/c/a/m;

    if-eqz v1, :cond_5

    .line 108
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/c/f/c/a/b/b;->c:Lcom/google/c/f/c/a/m;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 111
    :cond_5
    iget-object v1, p0, Lcom/google/c/f/c/a/b/b;->d:Ljava/lang/Integer;

    if-eqz v1, :cond_6

    .line 112
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/c/f/c/a/b/b;->d:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 115
    :cond_6
    iget-object v1, p0, Lcom/google/c/f/c/a/b/b;->e:Ljava/lang/Long;

    if-eqz v1, :cond_7

    .line 116
    const/4 v1, 0x5

    iget-object v2, p0, Lcom/google/c/f/c/a/b/b;->e:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/b;->f(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 119
    :cond_7
    return v0
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 8
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/c/f/c/a/b/b;->storeUnknownField(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    const/16 v0, 0xa

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->b(Lcom/google/protobuf/nano/a;I)I

    move-result v2

    iget-object v0, p0, Lcom/google/c/f/c/a/b/b;->a:[Lcom/google/c/f/c/a/c;

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lcom/google/c/f/c/a/c;

    if-eqz v0, :cond_1

    iget-object v3, p0, Lcom/google/c/f/c/a/b/b;->a:[Lcom/google/c/f/c/a/c;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_1
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_3

    new-instance v3, Lcom/google/c/f/c/a/c;

    invoke-direct {v3}, Lcom/google/c/f/c/a/c;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    iget-object v0, p0, Lcom/google/c/f/c/a/b/b;->a:[Lcom/google/c/f/c/a/c;

    array-length v0, v0

    goto :goto_1

    :cond_3
    new-instance v3, Lcom/google/c/f/c/a/c;

    invoke-direct {v3}, Lcom/google/c/f/c/a/c;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    iput-object v2, p0, Lcom/google/c/f/c/a/b/b;->a:[Lcom/google/c/f/c/a/c;

    goto :goto_0

    :sswitch_2
    const/16 v0, 0x12

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->b(Lcom/google/protobuf/nano/a;I)I

    move-result v2

    iget-object v0, p0, Lcom/google/c/f/c/a/b/b;->b:[Lcom/google/c/f/c/a/i;

    if-nez v0, :cond_5

    move v0, v1

    :goto_3
    add-int/2addr v2, v0

    new-array v2, v2, [Lcom/google/c/f/c/a/i;

    if-eqz v0, :cond_4

    iget-object v3, p0, Lcom/google/c/f/c/a/b/b;->b:[Lcom/google/c/f/c/a/i;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_4
    :goto_4
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_6

    new-instance v3, Lcom/google/c/f/c/a/i;

    invoke-direct {v3}, Lcom/google/c/f/c/a/i;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_5
    iget-object v0, p0, Lcom/google/c/f/c/a/b/b;->b:[Lcom/google/c/f/c/a/i;

    array-length v0, v0

    goto :goto_3

    :cond_6
    new-instance v3, Lcom/google/c/f/c/a/i;

    invoke-direct {v3}, Lcom/google/c/f/c/a/i;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    iput-object v2, p0, Lcom/google/c/f/c/a/b/b;->b:[Lcom/google/c/f/c/a/i;

    goto/16 :goto_0

    :sswitch_3
    iget-object v0, p0, Lcom/google/c/f/c/a/b/b;->c:Lcom/google/c/f/c/a/m;

    if-nez v0, :cond_7

    new-instance v0, Lcom/google/c/f/c/a/m;

    invoke-direct {v0}, Lcom/google/c/f/c/a/m;-><init>()V

    iput-object v0, p0, Lcom/google/c/f/c/a/b/b;->c:Lcom/google/c/f/c/a/m;

    :cond_7
    iget-object v0, p0, Lcom/google/c/f/c/a/b/b;->c:Lcom/google/c/f/c/a/m;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto/16 :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/c/f/c/a/b/b;->d:Ljava/lang/Integer;

    goto/16 :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->j()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/google/c/f/c/a/b/b;->e:Ljava/lang/Long;

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 58
    iget-object v0, p0, Lcom/google/c/f/c/a/b/b;->a:[Lcom/google/c/f/c/a/c;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/c/f/c/a/b/b;->a:[Lcom/google/c/f/c/a/c;

    array-length v0, v0

    if-lez v0, :cond_1

    move v0, v1

    .line 59
    :goto_0
    iget-object v2, p0, Lcom/google/c/f/c/a/b/b;->a:[Lcom/google/c/f/c/a/c;

    array-length v2, v2

    if-ge v0, v2, :cond_1

    .line 60
    iget-object v2, p0, Lcom/google/c/f/c/a/b/b;->a:[Lcom/google/c/f/c/a/c;

    aget-object v2, v2, v0

    .line 61
    if-eqz v2, :cond_0

    .line 62
    const/4 v3, 0x1

    invoke-virtual {p1, v3, v2}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 59
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 66
    :cond_1
    iget-object v0, p0, Lcom/google/c/f/c/a/b/b;->b:[Lcom/google/c/f/c/a/i;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/c/f/c/a/b/b;->b:[Lcom/google/c/f/c/a/i;

    array-length v0, v0

    if-lez v0, :cond_3

    .line 67
    :goto_1
    iget-object v0, p0, Lcom/google/c/f/c/a/b/b;->b:[Lcom/google/c/f/c/a/i;

    array-length v0, v0

    if-ge v1, v0, :cond_3

    .line 68
    iget-object v0, p0, Lcom/google/c/f/c/a/b/b;->b:[Lcom/google/c/f/c/a/i;

    aget-object v0, v0, v1

    .line 69
    if-eqz v0, :cond_2

    .line 70
    const/4 v2, 0x2

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 67
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 74
    :cond_3
    iget-object v0, p0, Lcom/google/c/f/c/a/b/b;->c:Lcom/google/c/f/c/a/m;

    if-eqz v0, :cond_4

    .line 75
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/c/f/c/a/b/b;->c:Lcom/google/c/f/c/a/m;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 77
    :cond_4
    iget-object v0, p0, Lcom/google/c/f/c/a/b/b;->d:Ljava/lang/Integer;

    if-eqz v0, :cond_5

    .line 78
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/c/f/c/a/b/b;->d:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 80
    :cond_5
    iget-object v0, p0, Lcom/google/c/f/c/a/b/b;->e:Ljava/lang/Long;

    if-eqz v0, :cond_6

    .line 81
    const/4 v0, 0x5

    iget-object v1, p0, Lcom/google/c/f/c/a/b/b;->e:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/b;->b(IJ)V

    .line 83
    :cond_6
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/d;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 84
    return-void
.end method
