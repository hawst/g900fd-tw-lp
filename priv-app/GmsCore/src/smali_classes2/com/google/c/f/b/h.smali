.class public final Lcom/google/c/f/b/h;
.super Lcom/google/protobuf/nano/d;
.source "SourceFile"


# static fields
.field private static volatile b:[Lcom/google/c/f/b/h;


# instance fields
.field public a:Ljava/lang/Integer;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1270
    invoke-direct {p0}, Lcom/google/protobuf/nano/d;-><init>()V

    .line 1271
    iput-object v0, p0, Lcom/google/c/f/b/h;->a:Ljava/lang/Integer;

    iput-object v0, p0, Lcom/google/c/f/b/h;->unknownFieldData:Lcom/google/protobuf/nano/f;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/c/f/b/h;->cachedSize:I

    .line 1272
    return-void
.end method

.method public static a()[Lcom/google/c/f/b/h;
    .locals 2

    .prologue
    .line 1256
    sget-object v0, Lcom/google/c/f/b/h;->b:[Lcom/google/c/f/b/h;

    if-nez v0, :cond_1

    .line 1257
    sget-object v1, Lcom/google/protobuf/nano/h;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 1259
    :try_start_0
    sget-object v0, Lcom/google/c/f/b/h;->b:[Lcom/google/c/f/b/h;

    if-nez v0, :cond_0

    .line 1260
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/c/f/b/h;

    sput-object v0, Lcom/google/c/f/b/h;->b:[Lcom/google/c/f/b/h;

    .line 1262
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1264
    :cond_1
    sget-object v0, Lcom/google/c/f/b/h;->b:[Lcom/google/c/f/b/h;

    return-object v0

    .line 1262
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 1292
    invoke-super {p0}, Lcom/google/protobuf/nano/d;->computeSerializedSize()I

    move-result v0

    .line 1293
    iget-object v1, p0, Lcom/google/c/f/b/h;->a:Ljava/lang/Integer;

    if-eqz v1, :cond_0

    .line 1294
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/c/f/b/h;->a:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1297
    :cond_0
    return v0
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 1250
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/c/f/b/h;->storeUnknownField(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/c/f/b/h;->a:Ljava/lang/Integer;

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 1284
    iget-object v0, p0, Lcom/google/c/f/b/h;->a:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 1285
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/c/f/b/h;->a:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 1287
    :cond_0
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/d;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 1288
    return-void
.end method
