.class public final Lcom/google/c/f/c/a/s;
.super Lcom/google/protobuf/nano/d;
.source "SourceFile"


# instance fields
.field public a:Ljava/lang/Integer;

.field public b:[Ljava/lang/String;

.field public c:Lcom/google/c/f/c/a/u;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 777
    invoke-direct {p0}, Lcom/google/protobuf/nano/d;-><init>()V

    .line 778
    iput-object v1, p0, Lcom/google/c/f/c/a/s;->a:Ljava/lang/Integer;

    sget-object v0, Lcom/google/protobuf/nano/m;->f:[Ljava/lang/String;

    iput-object v0, p0, Lcom/google/c/f/c/a/s;->b:[Ljava/lang/String;

    iput-object v1, p0, Lcom/google/c/f/c/a/s;->c:Lcom/google/c/f/c/a/u;

    iput-object v1, p0, Lcom/google/c/f/c/a/s;->unknownFieldData:Lcom/google/protobuf/nano/f;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/c/f/c/a/s;->cachedSize:I

    .line 779
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 812
    invoke-super {p0}, Lcom/google/protobuf/nano/d;->computeSerializedSize()I

    move-result v0

    .line 813
    iget-object v2, p0, Lcom/google/c/f/c/a/s;->a:Ljava/lang/Integer;

    if-eqz v2, :cond_0

    .line 814
    const/4 v2, 0x1

    iget-object v3, p0, Lcom/google/c/f/c/a/s;->a:Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 817
    :cond_0
    iget-object v2, p0, Lcom/google/c/f/c/a/s;->b:[Ljava/lang/String;

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/google/c/f/c/a/s;->b:[Ljava/lang/String;

    array-length v2, v2

    if-lez v2, :cond_3

    move v2, v1

    move v3, v1

    .line 820
    :goto_0
    iget-object v4, p0, Lcom/google/c/f/c/a/s;->b:[Ljava/lang/String;

    array-length v4, v4

    if-ge v1, v4, :cond_2

    .line 821
    iget-object v4, p0, Lcom/google/c/f/c/a/s;->b:[Ljava/lang/String;

    aget-object v4, v4, v1

    .line 822
    if-eqz v4, :cond_1

    .line 823
    add-int/lit8 v3, v3, 0x1

    .line 824
    invoke-static {v4}, Lcom/google/protobuf/nano/b;->a(Ljava/lang/String;)I

    move-result v4

    add-int/2addr v2, v4

    .line 820
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 828
    :cond_2
    add-int/2addr v0, v2

    .line 829
    mul-int/lit8 v1, v3, 0x1

    add-int/2addr v0, v1

    .line 831
    :cond_3
    iget-object v1, p0, Lcom/google/c/f/c/a/s;->c:Lcom/google/c/f/c/a/u;

    if-eqz v1, :cond_4

    .line 832
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/c/f/c/a/s;->c:Lcom/google/c/f/c/a/u;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 835
    :cond_4
    return v0
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 366
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/c/f/c/a/s;->storeUnknownField(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/c/f/c/a/s;->a:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_2
    const/16 v0, 0x12

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->b(Lcom/google/protobuf/nano/a;I)I

    move-result v2

    iget-object v0, p0, Lcom/google/c/f/c/a/s;->b:[Ljava/lang/String;

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v3, p0, Lcom/google/c/f/c/a/s;->b:[Ljava/lang/String;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_1
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_3

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    iget-object v0, p0, Lcom/google/c/f/c/a/s;->b:[Ljava/lang/String;

    array-length v0, v0

    goto :goto_1

    :cond_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    iput-object v2, p0, Lcom/google/c/f/c/a/s;->b:[Ljava/lang/String;

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Lcom/google/c/f/c/a/s;->c:Lcom/google/c/f/c/a/u;

    if-nez v0, :cond_4

    new-instance v0, Lcom/google/c/f/c/a/u;

    invoke-direct {v0}, Lcom/google/c/f/c/a/u;-><init>()V

    iput-object v0, p0, Lcom/google/c/f/c/a/s;->c:Lcom/google/c/f/c/a/u;

    :cond_4
    iget-object v0, p0, Lcom/google/c/f/c/a/s;->c:Lcom/google/c/f/c/a/u;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 3

    .prologue
    .line 793
    iget-object v0, p0, Lcom/google/c/f/c/a/s;->a:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 794
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/c/f/c/a/s;->a:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 796
    :cond_0
    iget-object v0, p0, Lcom/google/c/f/c/a/s;->b:[Ljava/lang/String;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/c/f/c/a/s;->b:[Ljava/lang/String;

    array-length v0, v0

    if-lez v0, :cond_2

    .line 797
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/google/c/f/c/a/s;->b:[Ljava/lang/String;

    array-length v1, v1

    if-ge v0, v1, :cond_2

    .line 798
    iget-object v1, p0, Lcom/google/c/f/c/a/s;->b:[Ljava/lang/String;

    aget-object v1, v1, v0

    .line 799
    if-eqz v1, :cond_1

    .line 800
    const/4 v2, 0x2

    invoke-virtual {p1, v2, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 797
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 804
    :cond_2
    iget-object v0, p0, Lcom/google/c/f/c/a/s;->c:Lcom/google/c/f/c/a/u;

    if-eqz v0, :cond_3

    .line 805
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/c/f/c/a/s;->c:Lcom/google/c/f/c/a/u;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 807
    :cond_3
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/d;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 808
    return-void
.end method
