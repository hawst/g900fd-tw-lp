.class public final Lcom/google/c/f/c/a/g;
.super Lcom/google/protobuf/nano/d;
.source "SourceFile"


# static fields
.field private static volatile k:[Lcom/google/c/f/c/a/g;


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/String;

.field public e:Ljava/lang/String;

.field public f:Ljava/lang/String;

.field public g:Ljava/lang/String;

.field public h:Ljava/lang/String;

.field public i:Ljava/lang/Integer;

.field public j:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1654
    invoke-direct {p0}, Lcom/google/protobuf/nano/d;-><init>()V

    .line 1655
    iput-object v0, p0, Lcom/google/c/f/c/a/g;->a:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/c/f/c/a/g;->b:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/c/f/c/a/g;->c:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/c/f/c/a/g;->d:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/c/f/c/a/g;->e:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/c/f/c/a/g;->f:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/c/f/c/a/g;->g:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/c/f/c/a/g;->h:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/c/f/c/a/g;->i:Ljava/lang/Integer;

    iput-object v0, p0, Lcom/google/c/f/c/a/g;->j:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/c/f/c/a/g;->unknownFieldData:Lcom/google/protobuf/nano/f;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/c/f/c/a/g;->cachedSize:I

    .line 1656
    return-void
.end method

.method public static a()[Lcom/google/c/f/c/a/g;
    .locals 2

    .prologue
    .line 1613
    sget-object v0, Lcom/google/c/f/c/a/g;->k:[Lcom/google/c/f/c/a/g;

    if-nez v0, :cond_1

    .line 1614
    sget-object v1, Lcom/google/protobuf/nano/h;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 1616
    :try_start_0
    sget-object v0, Lcom/google/c/f/c/a/g;->k:[Lcom/google/c/f/c/a/g;

    if-nez v0, :cond_0

    .line 1617
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/c/f/c/a/g;

    sput-object v0, Lcom/google/c/f/c/a/g;->k:[Lcom/google/c/f/c/a/g;

    .line 1619
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1621
    :cond_1
    sget-object v0, Lcom/google/c/f/c/a/g;->k:[Lcom/google/c/f/c/a/g;

    return-object v0

    .line 1619
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 1712
    invoke-super {p0}, Lcom/google/protobuf/nano/d;->computeSerializedSize()I

    move-result v0

    .line 1713
    iget-object v1, p0, Lcom/google/c/f/c/a/g;->a:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 1714
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/c/f/c/a/g;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1717
    :cond_0
    iget-object v1, p0, Lcom/google/c/f/c/a/g;->b:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 1718
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/c/f/c/a/g;->b:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1721
    :cond_1
    iget-object v1, p0, Lcom/google/c/f/c/a/g;->c:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 1722
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/c/f/c/a/g;->c:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1725
    :cond_2
    iget-object v1, p0, Lcom/google/c/f/c/a/g;->d:Ljava/lang/String;

    if-eqz v1, :cond_3

    .line 1726
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/c/f/c/a/g;->d:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1729
    :cond_3
    iget-object v1, p0, Lcom/google/c/f/c/a/g;->e:Ljava/lang/String;

    if-eqz v1, :cond_4

    .line 1730
    const/4 v1, 0x5

    iget-object v2, p0, Lcom/google/c/f/c/a/g;->e:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1733
    :cond_4
    iget-object v1, p0, Lcom/google/c/f/c/a/g;->f:Ljava/lang/String;

    if-eqz v1, :cond_5

    .line 1734
    const/4 v1, 0x6

    iget-object v2, p0, Lcom/google/c/f/c/a/g;->f:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1737
    :cond_5
    iget-object v1, p0, Lcom/google/c/f/c/a/g;->g:Ljava/lang/String;

    if-eqz v1, :cond_6

    .line 1738
    const/4 v1, 0x7

    iget-object v2, p0, Lcom/google/c/f/c/a/g;->g:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1741
    :cond_6
    iget-object v1, p0, Lcom/google/c/f/c/a/g;->h:Ljava/lang/String;

    if-eqz v1, :cond_7

    .line 1742
    const/16 v1, 0x8

    iget-object v2, p0, Lcom/google/c/f/c/a/g;->h:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1745
    :cond_7
    iget-object v1, p0, Lcom/google/c/f/c/a/g;->i:Ljava/lang/Integer;

    if-eqz v1, :cond_8

    .line 1746
    const/16 v1, 0x9

    iget-object v2, p0, Lcom/google/c/f/c/a/g;->i:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1749
    :cond_8
    iget-object v1, p0, Lcom/google/c/f/c/a/g;->j:Ljava/lang/String;

    if-eqz v1, :cond_9

    .line 1750
    const/16 v1, 0xa

    iget-object v2, p0, Lcom/google/c/f/c/a/g;->j:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1753
    :cond_9
    return v0
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 1607
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/c/f/c/a/g;->storeUnknownField(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/c/f/c/a/g;->a:Ljava/lang/String;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/c/f/c/a/g;->b:Ljava/lang/String;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/c/f/c/a/g;->c:Ljava/lang/String;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/c/f/c/a/g;->d:Ljava/lang/String;

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/c/f/c/a/g;->e:Ljava/lang/String;

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/c/f/c/a/g;->f:Ljava/lang/String;

    goto :goto_0

    :sswitch_7
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/c/f/c/a/g;->g:Ljava/lang/String;

    goto :goto_0

    :sswitch_8
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/c/f/c/a/g;->h:Ljava/lang/String;

    goto :goto_0

    :sswitch_9
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/c/f/c/a/g;->i:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_a
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/c/f/c/a/g;->j:Ljava/lang/String;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x42 -> :sswitch_8
        0x48 -> :sswitch_9
        0x52 -> :sswitch_a
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 1677
    iget-object v0, p0, Lcom/google/c/f/c/a/g;->a:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 1678
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/c/f/c/a/g;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 1680
    :cond_0
    iget-object v0, p0, Lcom/google/c/f/c/a/g;->b:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 1681
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/c/f/c/a/g;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 1683
    :cond_1
    iget-object v0, p0, Lcom/google/c/f/c/a/g;->c:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 1684
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/c/f/c/a/g;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 1686
    :cond_2
    iget-object v0, p0, Lcom/google/c/f/c/a/g;->d:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 1687
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/c/f/c/a/g;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 1689
    :cond_3
    iget-object v0, p0, Lcom/google/c/f/c/a/g;->e:Ljava/lang/String;

    if-eqz v0, :cond_4

    .line 1690
    const/4 v0, 0x5

    iget-object v1, p0, Lcom/google/c/f/c/a/g;->e:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 1692
    :cond_4
    iget-object v0, p0, Lcom/google/c/f/c/a/g;->f:Ljava/lang/String;

    if-eqz v0, :cond_5

    .line 1693
    const/4 v0, 0x6

    iget-object v1, p0, Lcom/google/c/f/c/a/g;->f:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 1695
    :cond_5
    iget-object v0, p0, Lcom/google/c/f/c/a/g;->g:Ljava/lang/String;

    if-eqz v0, :cond_6

    .line 1696
    const/4 v0, 0x7

    iget-object v1, p0, Lcom/google/c/f/c/a/g;->g:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 1698
    :cond_6
    iget-object v0, p0, Lcom/google/c/f/c/a/g;->h:Ljava/lang/String;

    if-eqz v0, :cond_7

    .line 1699
    const/16 v0, 0x8

    iget-object v1, p0, Lcom/google/c/f/c/a/g;->h:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 1701
    :cond_7
    iget-object v0, p0, Lcom/google/c/f/c/a/g;->i:Ljava/lang/Integer;

    if-eqz v0, :cond_8

    .line 1702
    const/16 v0, 0x9

    iget-object v1, p0, Lcom/google/c/f/c/a/g;->i:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 1704
    :cond_8
    iget-object v0, p0, Lcom/google/c/f/c/a/g;->j:Ljava/lang/String;

    if-eqz v0, :cond_9

    .line 1705
    const/16 v0, 0xa

    iget-object v1, p0, Lcom/google/c/f/c/a/g;->j:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 1707
    :cond_9
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/d;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 1708
    return-void
.end method
