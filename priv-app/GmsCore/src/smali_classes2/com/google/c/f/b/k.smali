.class public final Lcom/google/c/f/b/k;
.super Lcom/google/protobuf/nano/d;
.source "SourceFile"


# instance fields
.field public a:[Lcom/google/c/f/b/m;

.field public b:Lcom/google/c/f/b/m;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 535
    invoke-direct {p0}, Lcom/google/protobuf/nano/d;-><init>()V

    .line 536
    invoke-static {}, Lcom/google/c/f/b/m;->a()[Lcom/google/c/f/b/m;

    move-result-object v0

    iput-object v0, p0, Lcom/google/c/f/b/k;->a:[Lcom/google/c/f/b/m;

    iput-object v1, p0, Lcom/google/c/f/b/k;->b:Lcom/google/c/f/b/m;

    iput-object v1, p0, Lcom/google/c/f/b/k;->unknownFieldData:Lcom/google/protobuf/nano/f;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/c/f/b/k;->cachedSize:I

    .line 537
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 4

    .prologue
    .line 566
    invoke-super {p0}, Lcom/google/protobuf/nano/d;->computeSerializedSize()I

    move-result v1

    .line 567
    iget-object v0, p0, Lcom/google/c/f/b/k;->a:[Lcom/google/c/f/b/m;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/c/f/b/k;->a:[Lcom/google/c/f/b/m;

    array-length v0, v0

    if-lez v0, :cond_1

    .line 568
    const/4 v0, 0x0

    :goto_0
    iget-object v2, p0, Lcom/google/c/f/b/k;->a:[Lcom/google/c/f/b/m;

    array-length v2, v2

    if-ge v0, v2, :cond_1

    .line 569
    iget-object v2, p0, Lcom/google/c/f/b/k;->a:[Lcom/google/c/f/b/m;

    aget-object v2, v2, v0

    .line 570
    if-eqz v2, :cond_0

    .line 571
    const/4 v3, 0x1

    invoke-static {v3, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v2

    add-int/2addr v1, v2

    .line 568
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 576
    :cond_1
    iget-object v0, p0, Lcom/google/c/f/b/k;->b:Lcom/google/c/f/b/m;

    if-eqz v0, :cond_2

    .line 577
    const/4 v0, 0x2

    iget-object v2, p0, Lcom/google/c/f/b/k;->b:Lcom/google/c/f/b/m;

    invoke-static {v0, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v0

    add-int/2addr v1, v0

    .line 580
    :cond_2
    return v1
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 512
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/c/f/b/k;->storeUnknownField(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    const/16 v0, 0xa

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->b(Lcom/google/protobuf/nano/a;I)I

    move-result v2

    iget-object v0, p0, Lcom/google/c/f/b/k;->a:[Lcom/google/c/f/b/m;

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lcom/google/c/f/b/m;

    if-eqz v0, :cond_1

    iget-object v3, p0, Lcom/google/c/f/b/k;->a:[Lcom/google/c/f/b/m;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_1
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_3

    new-instance v3, Lcom/google/c/f/b/m;

    invoke-direct {v3}, Lcom/google/c/f/b/m;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    iget-object v0, p0, Lcom/google/c/f/b/k;->a:[Lcom/google/c/f/b/m;

    array-length v0, v0

    goto :goto_1

    :cond_3
    new-instance v3, Lcom/google/c/f/b/m;

    invoke-direct {v3}, Lcom/google/c/f/b/m;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    iput-object v2, p0, Lcom/google/c/f/b/k;->a:[Lcom/google/c/f/b/m;

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Lcom/google/c/f/b/k;->b:Lcom/google/c/f/b/m;

    if-nez v0, :cond_4

    new-instance v0, Lcom/google/c/f/b/m;

    invoke-direct {v0}, Lcom/google/c/f/b/m;-><init>()V

    iput-object v0, p0, Lcom/google/c/f/b/k;->b:Lcom/google/c/f/b/m;

    :cond_4
    iget-object v0, p0, Lcom/google/c/f/b/k;->b:Lcom/google/c/f/b/m;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 3

    .prologue
    .line 550
    iget-object v0, p0, Lcom/google/c/f/b/k;->a:[Lcom/google/c/f/b/m;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/c/f/b/k;->a:[Lcom/google/c/f/b/m;

    array-length v0, v0

    if-lez v0, :cond_1

    .line 551
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/google/c/f/b/k;->a:[Lcom/google/c/f/b/m;

    array-length v1, v1

    if-ge v0, v1, :cond_1

    .line 552
    iget-object v1, p0, Lcom/google/c/f/b/k;->a:[Lcom/google/c/f/b/m;

    aget-object v1, v1, v0

    .line 553
    if-eqz v1, :cond_0

    .line 554
    const/4 v2, 0x1

    invoke-virtual {p1, v2, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 551
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 558
    :cond_1
    iget-object v0, p0, Lcom/google/c/f/b/k;->b:Lcom/google/c/f/b/m;

    if-eqz v0, :cond_2

    .line 559
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/c/f/b/k;->b:Lcom/google/c/f/b/m;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 561
    :cond_2
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/d;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 562
    return-void
.end method
