.class public final Lcom/google/c/f/c/a/i;
.super Lcom/google/protobuf/nano/d;
.source "SourceFile"


# static fields
.field private static volatile f:[Lcom/google/c/f/c/a/i;


# instance fields
.field public a:Lcom/google/c/f/c/a/e;

.field public b:[Lcom/google/c/f/c/a/e;

.field public c:Lcom/google/c/f/c/a/f;

.field public d:[Lcom/google/c/f/c/a/q;

.field public e:Lcom/google/c/f/c/a/k;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 3001
    invoke-direct {p0}, Lcom/google/protobuf/nano/d;-><init>()V

    .line 3002
    iput-object v1, p0, Lcom/google/c/f/c/a/i;->a:Lcom/google/c/f/c/a/e;

    invoke-static {}, Lcom/google/c/f/c/a/e;->a()[Lcom/google/c/f/c/a/e;

    move-result-object v0

    iput-object v0, p0, Lcom/google/c/f/c/a/i;->b:[Lcom/google/c/f/c/a/e;

    iput-object v1, p0, Lcom/google/c/f/c/a/i;->c:Lcom/google/c/f/c/a/f;

    invoke-static {}, Lcom/google/c/f/c/a/q;->a()[Lcom/google/c/f/c/a/q;

    move-result-object v0

    iput-object v0, p0, Lcom/google/c/f/c/a/i;->d:[Lcom/google/c/f/c/a/q;

    iput-object v1, p0, Lcom/google/c/f/c/a/i;->e:Lcom/google/c/f/c/a/k;

    iput-object v1, p0, Lcom/google/c/f/c/a/i;->unknownFieldData:Lcom/google/protobuf/nano/f;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/c/f/c/a/i;->cachedSize:I

    .line 3003
    return-void
.end method

.method public static a()[Lcom/google/c/f/c/a/i;
    .locals 2

    .prologue
    .line 2975
    sget-object v0, Lcom/google/c/f/c/a/i;->f:[Lcom/google/c/f/c/a/i;

    if-nez v0, :cond_1

    .line 2976
    sget-object v1, Lcom/google/protobuf/nano/h;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 2978
    :try_start_0
    sget-object v0, Lcom/google/c/f/c/a/i;->f:[Lcom/google/c/f/c/a/i;

    if-nez v0, :cond_0

    .line 2979
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/c/f/c/a/i;

    sput-object v0, Lcom/google/c/f/c/a/i;->f:[Lcom/google/c/f/c/a/i;

    .line 2981
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2983
    :cond_1
    sget-object v0, Lcom/google/c/f/c/a/i;->f:[Lcom/google/c/f/c/a/i;

    return-object v0

    .line 2981
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 3049
    invoke-super {p0}, Lcom/google/protobuf/nano/d;->computeSerializedSize()I

    move-result v0

    .line 3050
    iget-object v2, p0, Lcom/google/c/f/c/a/i;->a:Lcom/google/c/f/c/a/e;

    if-eqz v2, :cond_0

    .line 3051
    const/4 v2, 0x1

    iget-object v3, p0, Lcom/google/c/f/c/a/i;->a:Lcom/google/c/f/c/a/e;

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v2

    add-int/2addr v0, v2

    .line 3054
    :cond_0
    iget-object v2, p0, Lcom/google/c/f/c/a/i;->b:[Lcom/google/c/f/c/a/e;

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/google/c/f/c/a/i;->b:[Lcom/google/c/f/c/a/e;

    array-length v2, v2

    if-lez v2, :cond_3

    move v2, v0

    move v0, v1

    .line 3055
    :goto_0
    iget-object v3, p0, Lcom/google/c/f/c/a/i;->b:[Lcom/google/c/f/c/a/e;

    array-length v3, v3

    if-ge v0, v3, :cond_2

    .line 3056
    iget-object v3, p0, Lcom/google/c/f/c/a/i;->b:[Lcom/google/c/f/c/a/e;

    aget-object v3, v3, v0

    .line 3057
    if-eqz v3, :cond_1

    .line 3058
    const/4 v4, 0x2

    invoke-static {v4, v3}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v3

    add-int/2addr v2, v3

    .line 3055
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    move v0, v2

    .line 3063
    :cond_3
    iget-object v2, p0, Lcom/google/c/f/c/a/i;->c:Lcom/google/c/f/c/a/f;

    if-eqz v2, :cond_4

    .line 3064
    const/4 v2, 0x3

    iget-object v3, p0, Lcom/google/c/f/c/a/i;->c:Lcom/google/c/f/c/a/f;

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v2

    add-int/2addr v0, v2

    .line 3067
    :cond_4
    iget-object v2, p0, Lcom/google/c/f/c/a/i;->d:[Lcom/google/c/f/c/a/q;

    if-eqz v2, :cond_6

    iget-object v2, p0, Lcom/google/c/f/c/a/i;->d:[Lcom/google/c/f/c/a/q;

    array-length v2, v2

    if-lez v2, :cond_6

    .line 3068
    :goto_1
    iget-object v2, p0, Lcom/google/c/f/c/a/i;->d:[Lcom/google/c/f/c/a/q;

    array-length v2, v2

    if-ge v1, v2, :cond_6

    .line 3069
    iget-object v2, p0, Lcom/google/c/f/c/a/i;->d:[Lcom/google/c/f/c/a/q;

    aget-object v2, v2, v1

    .line 3070
    if-eqz v2, :cond_5

    .line 3071
    const/4 v3, 0x4

    invoke-static {v3, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v2

    add-int/2addr v0, v2

    .line 3068
    :cond_5
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 3076
    :cond_6
    iget-object v1, p0, Lcom/google/c/f/c/a/i;->e:Lcom/google/c/f/c/a/k;

    if-eqz v1, :cond_7

    .line 3077
    const/4 v1, 0x5

    iget-object v2, p0, Lcom/google/c/f/c/a/i;->e:Lcom/google/c/f/c/a/k;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3080
    :cond_7
    return v0
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 2969
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/c/f/c/a/i;->storeUnknownField(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Lcom/google/c/f/c/a/i;->a:Lcom/google/c/f/c/a/e;

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/c/f/c/a/e;

    invoke-direct {v0}, Lcom/google/c/f/c/a/e;-><init>()V

    iput-object v0, p0, Lcom/google/c/f/c/a/i;->a:Lcom/google/c/f/c/a/e;

    :cond_1
    iget-object v0, p0, Lcom/google/c/f/c/a/i;->a:Lcom/google/c/f/c/a/e;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_2
    const/16 v0, 0x12

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->b(Lcom/google/protobuf/nano/a;I)I

    move-result v2

    iget-object v0, p0, Lcom/google/c/f/c/a/i;->b:[Lcom/google/c/f/c/a/e;

    if-nez v0, :cond_3

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lcom/google/c/f/c/a/e;

    if-eqz v0, :cond_2

    iget-object v3, p0, Lcom/google/c/f/c/a/i;->b:[Lcom/google/c/f/c/a/e;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_2
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_4

    new-instance v3, Lcom/google/c/f/c/a/e;

    invoke-direct {v3}, Lcom/google/c/f/c/a/e;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_3
    iget-object v0, p0, Lcom/google/c/f/c/a/i;->b:[Lcom/google/c/f/c/a/e;

    array-length v0, v0

    goto :goto_1

    :cond_4
    new-instance v3, Lcom/google/c/f/c/a/e;

    invoke-direct {v3}, Lcom/google/c/f/c/a/e;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    iput-object v2, p0, Lcom/google/c/f/c/a/i;->b:[Lcom/google/c/f/c/a/e;

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Lcom/google/c/f/c/a/i;->c:Lcom/google/c/f/c/a/f;

    if-nez v0, :cond_5

    new-instance v0, Lcom/google/c/f/c/a/f;

    invoke-direct {v0}, Lcom/google/c/f/c/a/f;-><init>()V

    iput-object v0, p0, Lcom/google/c/f/c/a/i;->c:Lcom/google/c/f/c/a/f;

    :cond_5
    iget-object v0, p0, Lcom/google/c/f/c/a/i;->c:Lcom/google/c/f/c/a/f;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_4
    const/16 v0, 0x22

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->b(Lcom/google/protobuf/nano/a;I)I

    move-result v2

    iget-object v0, p0, Lcom/google/c/f/c/a/i;->d:[Lcom/google/c/f/c/a/q;

    if-nez v0, :cond_7

    move v0, v1

    :goto_3
    add-int/2addr v2, v0

    new-array v2, v2, [Lcom/google/c/f/c/a/q;

    if-eqz v0, :cond_6

    iget-object v3, p0, Lcom/google/c/f/c/a/i;->d:[Lcom/google/c/f/c/a/q;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_6
    :goto_4
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_8

    new-instance v3, Lcom/google/c/f/c/a/q;

    invoke-direct {v3}, Lcom/google/c/f/c/a/q;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_7
    iget-object v0, p0, Lcom/google/c/f/c/a/i;->d:[Lcom/google/c/f/c/a/q;

    array-length v0, v0

    goto :goto_3

    :cond_8
    new-instance v3, Lcom/google/c/f/c/a/q;

    invoke-direct {v3}, Lcom/google/c/f/c/a/q;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    iput-object v2, p0, Lcom/google/c/f/c/a/i;->d:[Lcom/google/c/f/c/a/q;

    goto/16 :goto_0

    :sswitch_5
    iget-object v0, p0, Lcom/google/c/f/c/a/i;->e:Lcom/google/c/f/c/a/k;

    if-nez v0, :cond_9

    new-instance v0, Lcom/google/c/f/c/a/k;

    invoke-direct {v0}, Lcom/google/c/f/c/a/k;-><init>()V

    iput-object v0, p0, Lcom/google/c/f/c/a/i;->e:Lcom/google/c/f/c/a/k;

    :cond_9
    iget-object v0, p0, Lcom/google/c/f/c/a/i;->e:Lcom/google/c/f/c/a/k;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 3019
    iget-object v0, p0, Lcom/google/c/f/c/a/i;->a:Lcom/google/c/f/c/a/e;

    if-eqz v0, :cond_0

    .line 3020
    const/4 v0, 0x1

    iget-object v2, p0, Lcom/google/c/f/c/a/i;->a:Lcom/google/c/f/c/a/e;

    invoke-virtual {p1, v0, v2}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 3022
    :cond_0
    iget-object v0, p0, Lcom/google/c/f/c/a/i;->b:[Lcom/google/c/f/c/a/e;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/c/f/c/a/i;->b:[Lcom/google/c/f/c/a/e;

    array-length v0, v0

    if-lez v0, :cond_2

    move v0, v1

    .line 3023
    :goto_0
    iget-object v2, p0, Lcom/google/c/f/c/a/i;->b:[Lcom/google/c/f/c/a/e;

    array-length v2, v2

    if-ge v0, v2, :cond_2

    .line 3024
    iget-object v2, p0, Lcom/google/c/f/c/a/i;->b:[Lcom/google/c/f/c/a/e;

    aget-object v2, v2, v0

    .line 3025
    if-eqz v2, :cond_1

    .line 3026
    const/4 v3, 0x2

    invoke-virtual {p1, v3, v2}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 3023
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 3030
    :cond_2
    iget-object v0, p0, Lcom/google/c/f/c/a/i;->c:Lcom/google/c/f/c/a/f;

    if-eqz v0, :cond_3

    .line 3031
    const/4 v0, 0x3

    iget-object v2, p0, Lcom/google/c/f/c/a/i;->c:Lcom/google/c/f/c/a/f;

    invoke-virtual {p1, v0, v2}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 3033
    :cond_3
    iget-object v0, p0, Lcom/google/c/f/c/a/i;->d:[Lcom/google/c/f/c/a/q;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/google/c/f/c/a/i;->d:[Lcom/google/c/f/c/a/q;

    array-length v0, v0

    if-lez v0, :cond_5

    .line 3034
    :goto_1
    iget-object v0, p0, Lcom/google/c/f/c/a/i;->d:[Lcom/google/c/f/c/a/q;

    array-length v0, v0

    if-ge v1, v0, :cond_5

    .line 3035
    iget-object v0, p0, Lcom/google/c/f/c/a/i;->d:[Lcom/google/c/f/c/a/q;

    aget-object v0, v0, v1

    .line 3036
    if-eqz v0, :cond_4

    .line 3037
    const/4 v2, 0x4

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 3034
    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 3041
    :cond_5
    iget-object v0, p0, Lcom/google/c/f/c/a/i;->e:Lcom/google/c/f/c/a/k;

    if-eqz v0, :cond_6

    .line 3042
    const/4 v0, 0x5

    iget-object v1, p0, Lcom/google/c/f/c/a/i;->e:Lcom/google/c/f/c/a/k;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 3044
    :cond_6
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/d;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 3045
    return-void
.end method
