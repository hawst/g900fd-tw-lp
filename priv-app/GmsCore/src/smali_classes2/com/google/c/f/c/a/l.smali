.class public final Lcom/google/c/f/c/a/l;
.super Lcom/google/protobuf/nano/d;
.source "SourceFile"


# static fields
.field private static volatile b:[Lcom/google/c/f/c/a/l;


# instance fields
.field public a:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 2778
    invoke-direct {p0}, Lcom/google/protobuf/nano/d;-><init>()V

    .line 2779
    iput-object v0, p0, Lcom/google/c/f/c/a/l;->a:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/c/f/c/a/l;->unknownFieldData:Lcom/google/protobuf/nano/f;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/c/f/c/a/l;->cachedSize:I

    .line 2780
    return-void
.end method

.method public static a()[Lcom/google/c/f/c/a/l;
    .locals 2

    .prologue
    .line 2764
    sget-object v0, Lcom/google/c/f/c/a/l;->b:[Lcom/google/c/f/c/a/l;

    if-nez v0, :cond_1

    .line 2765
    sget-object v1, Lcom/google/protobuf/nano/h;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 2767
    :try_start_0
    sget-object v0, Lcom/google/c/f/c/a/l;->b:[Lcom/google/c/f/c/a/l;

    if-nez v0, :cond_0

    .line 2768
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/c/f/c/a/l;

    sput-object v0, Lcom/google/c/f/c/a/l;->b:[Lcom/google/c/f/c/a/l;

    .line 2770
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2772
    :cond_1
    sget-object v0, Lcom/google/c/f/c/a/l;->b:[Lcom/google/c/f/c/a/l;

    return-object v0

    .line 2770
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 2800
    invoke-super {p0}, Lcom/google/protobuf/nano/d;->computeSerializedSize()I

    move-result v0

    .line 2801
    iget-object v1, p0, Lcom/google/c/f/c/a/l;->a:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 2802
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/c/f/c/a/l;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2805
    :cond_0
    return v0
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 2758
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/c/f/c/a/l;->storeUnknownField(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/c/f/c/a/l;->a:Ljava/lang/String;

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 2792
    iget-object v0, p0, Lcom/google/c/f/c/a/l;->a:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 2793
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/c/f/c/a/l;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 2795
    :cond_0
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/d;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 2796
    return-void
.end method
