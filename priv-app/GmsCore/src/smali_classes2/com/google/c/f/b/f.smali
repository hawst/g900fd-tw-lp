.class public final Lcom/google/c/f/b/f;
.super Lcom/google/protobuf/nano/d;
.source "SourceFile"


# instance fields
.field public a:[Lcom/google/c/f/b/g;

.field public b:[Lcom/google/c/f/b/b;

.field public c:[Lcom/google/c/f/b/l;

.field public d:Lcom/google/c/f/b/d;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1826
    invoke-direct {p0}, Lcom/google/protobuf/nano/d;-><init>()V

    .line 1827
    invoke-static {}, Lcom/google/c/f/b/g;->a()[Lcom/google/c/f/b/g;

    move-result-object v0

    iput-object v0, p0, Lcom/google/c/f/b/f;->a:[Lcom/google/c/f/b/g;

    invoke-static {}, Lcom/google/c/f/b/b;->a()[Lcom/google/c/f/b/b;

    move-result-object v0

    iput-object v0, p0, Lcom/google/c/f/b/f;->b:[Lcom/google/c/f/b/b;

    invoke-static {}, Lcom/google/c/f/b/l;->a()[Lcom/google/c/f/b/l;

    move-result-object v0

    iput-object v0, p0, Lcom/google/c/f/b/f;->c:[Lcom/google/c/f/b/l;

    iput-object v1, p0, Lcom/google/c/f/b/f;->d:Lcom/google/c/f/b/d;

    iput-object v1, p0, Lcom/google/c/f/b/f;->unknownFieldData:Lcom/google/protobuf/nano/f;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/c/f/b/f;->cachedSize:I

    .line 1828
    return-void
.end method

.method public static a([B)Lcom/google/c/f/b/f;
    .locals 1

    .prologue
    .line 1998
    new-instance v0, Lcom/google/c/f/b/f;

    invoke-direct {v0}, Lcom/google/c/f/b/f;-><init>()V

    invoke-static {v0, p0}, Lcom/google/protobuf/nano/j;->mergeFrom(Lcom/google/protobuf/nano/j;[B)Lcom/google/protobuf/nano/j;

    move-result-object v0

    check-cast v0, Lcom/google/c/f/b/f;

    return-object v0
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 1875
    invoke-super {p0}, Lcom/google/protobuf/nano/d;->computeSerializedSize()I

    move-result v0

    .line 1876
    iget-object v2, p0, Lcom/google/c/f/b/f;->a:[Lcom/google/c/f/b/g;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/google/c/f/b/f;->a:[Lcom/google/c/f/b/g;

    array-length v2, v2

    if-lez v2, :cond_2

    move v2, v0

    move v0, v1

    .line 1877
    :goto_0
    iget-object v3, p0, Lcom/google/c/f/b/f;->a:[Lcom/google/c/f/b/g;

    array-length v3, v3

    if-ge v0, v3, :cond_1

    .line 1878
    iget-object v3, p0, Lcom/google/c/f/b/f;->a:[Lcom/google/c/f/b/g;

    aget-object v3, v3, v0

    .line 1879
    if-eqz v3, :cond_0

    .line 1880
    const/4 v4, 0x1

    invoke-static {v4, v3}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v3

    add-int/2addr v2, v3

    .line 1877
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    move v0, v2

    .line 1885
    :cond_2
    iget-object v2, p0, Lcom/google/c/f/b/f;->b:[Lcom/google/c/f/b/b;

    if-eqz v2, :cond_5

    iget-object v2, p0, Lcom/google/c/f/b/f;->b:[Lcom/google/c/f/b/b;

    array-length v2, v2

    if-lez v2, :cond_5

    move v2, v0

    move v0, v1

    .line 1886
    :goto_1
    iget-object v3, p0, Lcom/google/c/f/b/f;->b:[Lcom/google/c/f/b/b;

    array-length v3, v3

    if-ge v0, v3, :cond_4

    .line 1887
    iget-object v3, p0, Lcom/google/c/f/b/f;->b:[Lcom/google/c/f/b/b;

    aget-object v3, v3, v0

    .line 1888
    if-eqz v3, :cond_3

    .line 1889
    const/4 v4, 0x2

    invoke-static {v4, v3}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v3

    add-int/2addr v2, v3

    .line 1886
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_4
    move v0, v2

    .line 1894
    :cond_5
    iget-object v2, p0, Lcom/google/c/f/b/f;->c:[Lcom/google/c/f/b/l;

    if-eqz v2, :cond_7

    iget-object v2, p0, Lcom/google/c/f/b/f;->c:[Lcom/google/c/f/b/l;

    array-length v2, v2

    if-lez v2, :cond_7

    .line 1895
    :goto_2
    iget-object v2, p0, Lcom/google/c/f/b/f;->c:[Lcom/google/c/f/b/l;

    array-length v2, v2

    if-ge v1, v2, :cond_7

    .line 1896
    iget-object v2, p0, Lcom/google/c/f/b/f;->c:[Lcom/google/c/f/b/l;

    aget-object v2, v2, v1

    .line 1897
    if-eqz v2, :cond_6

    .line 1898
    const/4 v3, 0x3

    invoke-static {v3, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v2

    add-int/2addr v0, v2

    .line 1895
    :cond_6
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 1903
    :cond_7
    iget-object v1, p0, Lcom/google/c/f/b/f;->d:Lcom/google/c/f/b/d;

    if-eqz v1, :cond_8

    .line 1904
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/c/f/b/f;->d:Lcom/google/c/f/b/d;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1907
    :cond_8
    return v0
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 1797
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/c/f/b/f;->storeUnknownField(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    const/16 v0, 0xa

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->b(Lcom/google/protobuf/nano/a;I)I

    move-result v2

    iget-object v0, p0, Lcom/google/c/f/b/f;->a:[Lcom/google/c/f/b/g;

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lcom/google/c/f/b/g;

    if-eqz v0, :cond_1

    iget-object v3, p0, Lcom/google/c/f/b/f;->a:[Lcom/google/c/f/b/g;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_1
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_3

    new-instance v3, Lcom/google/c/f/b/g;

    invoke-direct {v3}, Lcom/google/c/f/b/g;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    iget-object v0, p0, Lcom/google/c/f/b/f;->a:[Lcom/google/c/f/b/g;

    array-length v0, v0

    goto :goto_1

    :cond_3
    new-instance v3, Lcom/google/c/f/b/g;

    invoke-direct {v3}, Lcom/google/c/f/b/g;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    iput-object v2, p0, Lcom/google/c/f/b/f;->a:[Lcom/google/c/f/b/g;

    goto :goto_0

    :sswitch_2
    const/16 v0, 0x12

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->b(Lcom/google/protobuf/nano/a;I)I

    move-result v2

    iget-object v0, p0, Lcom/google/c/f/b/f;->b:[Lcom/google/c/f/b/b;

    if-nez v0, :cond_5

    move v0, v1

    :goto_3
    add-int/2addr v2, v0

    new-array v2, v2, [Lcom/google/c/f/b/b;

    if-eqz v0, :cond_4

    iget-object v3, p0, Lcom/google/c/f/b/f;->b:[Lcom/google/c/f/b/b;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_4
    :goto_4
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_6

    new-instance v3, Lcom/google/c/f/b/b;

    invoke-direct {v3}, Lcom/google/c/f/b/b;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_5
    iget-object v0, p0, Lcom/google/c/f/b/f;->b:[Lcom/google/c/f/b/b;

    array-length v0, v0

    goto :goto_3

    :cond_6
    new-instance v3, Lcom/google/c/f/b/b;

    invoke-direct {v3}, Lcom/google/c/f/b/b;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    iput-object v2, p0, Lcom/google/c/f/b/f;->b:[Lcom/google/c/f/b/b;

    goto/16 :goto_0

    :sswitch_3
    const/16 v0, 0x1a

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->b(Lcom/google/protobuf/nano/a;I)I

    move-result v2

    iget-object v0, p0, Lcom/google/c/f/b/f;->c:[Lcom/google/c/f/b/l;

    if-nez v0, :cond_8

    move v0, v1

    :goto_5
    add-int/2addr v2, v0

    new-array v2, v2, [Lcom/google/c/f/b/l;

    if-eqz v0, :cond_7

    iget-object v3, p0, Lcom/google/c/f/b/f;->c:[Lcom/google/c/f/b/l;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_7
    :goto_6
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_9

    new-instance v3, Lcom/google/c/f/b/l;

    invoke-direct {v3}, Lcom/google/c/f/b/l;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_6

    :cond_8
    iget-object v0, p0, Lcom/google/c/f/b/f;->c:[Lcom/google/c/f/b/l;

    array-length v0, v0

    goto :goto_5

    :cond_9
    new-instance v3, Lcom/google/c/f/b/l;

    invoke-direct {v3}, Lcom/google/c/f/b/l;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    iput-object v2, p0, Lcom/google/c/f/b/f;->c:[Lcom/google/c/f/b/l;

    goto/16 :goto_0

    :sswitch_4
    iget-object v0, p0, Lcom/google/c/f/b/f;->d:Lcom/google/c/f/b/d;

    if-nez v0, :cond_a

    new-instance v0, Lcom/google/c/f/b/d;

    invoke-direct {v0}, Lcom/google/c/f/b/d;-><init>()V

    iput-object v0, p0, Lcom/google/c/f/b/f;->d:Lcom/google/c/f/b/d;

    :cond_a
    iget-object v0, p0, Lcom/google/c/f/b/f;->d:Lcom/google/c/f/b/d;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 1843
    iget-object v0, p0, Lcom/google/c/f/b/f;->a:[Lcom/google/c/f/b/g;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/c/f/b/f;->a:[Lcom/google/c/f/b/g;

    array-length v0, v0

    if-lez v0, :cond_1

    move v0, v1

    .line 1844
    :goto_0
    iget-object v2, p0, Lcom/google/c/f/b/f;->a:[Lcom/google/c/f/b/g;

    array-length v2, v2

    if-ge v0, v2, :cond_1

    .line 1845
    iget-object v2, p0, Lcom/google/c/f/b/f;->a:[Lcom/google/c/f/b/g;

    aget-object v2, v2, v0

    .line 1846
    if-eqz v2, :cond_0

    .line 1847
    const/4 v3, 0x1

    invoke-virtual {p1, v3, v2}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 1844
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1851
    :cond_1
    iget-object v0, p0, Lcom/google/c/f/b/f;->b:[Lcom/google/c/f/b/b;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/c/f/b/f;->b:[Lcom/google/c/f/b/b;

    array-length v0, v0

    if-lez v0, :cond_3

    move v0, v1

    .line 1852
    :goto_1
    iget-object v2, p0, Lcom/google/c/f/b/f;->b:[Lcom/google/c/f/b/b;

    array-length v2, v2

    if-ge v0, v2, :cond_3

    .line 1853
    iget-object v2, p0, Lcom/google/c/f/b/f;->b:[Lcom/google/c/f/b/b;

    aget-object v2, v2, v0

    .line 1854
    if-eqz v2, :cond_2

    .line 1855
    const/4 v3, 0x2

    invoke-virtual {p1, v3, v2}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 1852
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 1859
    :cond_3
    iget-object v0, p0, Lcom/google/c/f/b/f;->c:[Lcom/google/c/f/b/l;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/google/c/f/b/f;->c:[Lcom/google/c/f/b/l;

    array-length v0, v0

    if-lez v0, :cond_5

    .line 1860
    :goto_2
    iget-object v0, p0, Lcom/google/c/f/b/f;->c:[Lcom/google/c/f/b/l;

    array-length v0, v0

    if-ge v1, v0, :cond_5

    .line 1861
    iget-object v0, p0, Lcom/google/c/f/b/f;->c:[Lcom/google/c/f/b/l;

    aget-object v0, v0, v1

    .line 1862
    if-eqz v0, :cond_4

    .line 1863
    const/4 v2, 0x3

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 1860
    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 1867
    :cond_5
    iget-object v0, p0, Lcom/google/c/f/b/f;->d:Lcom/google/c/f/b/d;

    if-eqz v0, :cond_6

    .line 1868
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/c/f/b/f;->d:Lcom/google/c/f/b/d;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 1870
    :cond_6
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/d;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 1871
    return-void
.end method
