.class public final Lcom/google/c/f/b/m;
.super Lcom/google/protobuf/nano/d;
.source "SourceFile"


# static fields
.field private static volatile g:[Lcom/google/c/f/b/m;


# instance fields
.field public a:Lcom/google/c/f/c/a/e;

.field public b:Ljava/lang/String;

.field public c:Ljava/lang/Integer;

.field public d:Lcom/google/c/f/b/n;

.field public e:Lcom/google/c/f/b/e;

.field public f:Lcom/google/c/f/b/o;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 79
    invoke-direct {p0}, Lcom/google/protobuf/nano/d;-><init>()V

    .line 80
    iput-object v0, p0, Lcom/google/c/f/b/m;->a:Lcom/google/c/f/c/a/e;

    iput-object v0, p0, Lcom/google/c/f/b/m;->b:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/c/f/b/m;->c:Ljava/lang/Integer;

    iput-object v0, p0, Lcom/google/c/f/b/m;->d:Lcom/google/c/f/b/n;

    iput-object v0, p0, Lcom/google/c/f/b/m;->e:Lcom/google/c/f/b/e;

    iput-object v0, p0, Lcom/google/c/f/b/m;->f:Lcom/google/c/f/b/o;

    iput-object v0, p0, Lcom/google/c/f/b/m;->unknownFieldData:Lcom/google/protobuf/nano/f;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/c/f/b/m;->cachedSize:I

    .line 81
    return-void
.end method

.method public static a()[Lcom/google/c/f/b/m;
    .locals 2

    .prologue
    .line 50
    sget-object v0, Lcom/google/c/f/b/m;->g:[Lcom/google/c/f/b/m;

    if-nez v0, :cond_1

    .line 51
    sget-object v1, Lcom/google/protobuf/nano/h;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 53
    :try_start_0
    sget-object v0, Lcom/google/c/f/b/m;->g:[Lcom/google/c/f/b/m;

    if-nez v0, :cond_0

    .line 54
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/c/f/b/m;

    sput-object v0, Lcom/google/c/f/b/m;->g:[Lcom/google/c/f/b/m;

    .line 56
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 58
    :cond_1
    sget-object v0, Lcom/google/c/f/b/m;->g:[Lcom/google/c/f/b/m;

    return-object v0

    .line 56
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 121
    invoke-super {p0}, Lcom/google/protobuf/nano/d;->computeSerializedSize()I

    move-result v0

    .line 122
    iget-object v1, p0, Lcom/google/c/f/b/m;->a:Lcom/google/c/f/c/a/e;

    if-eqz v1, :cond_0

    .line 123
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/c/f/b/m;->a:Lcom/google/c/f/c/a/e;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 126
    :cond_0
    iget-object v1, p0, Lcom/google/c/f/b/m;->b:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 127
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/c/f/b/m;->b:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 130
    :cond_1
    iget-object v1, p0, Lcom/google/c/f/b/m;->c:Ljava/lang/Integer;

    if-eqz v1, :cond_2

    .line 131
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/c/f/b/m;->c:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 134
    :cond_2
    iget-object v1, p0, Lcom/google/c/f/b/m;->d:Lcom/google/c/f/b/n;

    if-eqz v1, :cond_3

    .line 135
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/c/f/b/m;->d:Lcom/google/c/f/b/n;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 138
    :cond_3
    iget-object v1, p0, Lcom/google/c/f/b/m;->e:Lcom/google/c/f/b/e;

    if-eqz v1, :cond_4

    .line 139
    const/4 v1, 0x5

    iget-object v2, p0, Lcom/google/c/f/b/m;->e:Lcom/google/c/f/b/e;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 142
    :cond_4
    iget-object v1, p0, Lcom/google/c/f/b/m;->f:Lcom/google/c/f/b/o;

    if-eqz v1, :cond_5

    .line 143
    const/4 v1, 0x6

    iget-object v2, p0, Lcom/google/c/f/b/m;->f:Lcom/google/c/f/b/o;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 146
    :cond_5
    return v0
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 44
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/c/f/b/m;->storeUnknownField(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Lcom/google/c/f/b/m;->a:Lcom/google/c/f/c/a/e;

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/c/f/c/a/e;

    invoke-direct {v0}, Lcom/google/c/f/c/a/e;-><init>()V

    iput-object v0, p0, Lcom/google/c/f/b/m;->a:Lcom/google/c/f/c/a/e;

    :cond_1
    iget-object v0, p0, Lcom/google/c/f/b/m;->a:Lcom/google/c/f/c/a/e;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/c/f/b/m;->b:Ljava/lang/String;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/c/f/b/m;->c:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_4
    iget-object v0, p0, Lcom/google/c/f/b/m;->d:Lcom/google/c/f/b/n;

    if-nez v0, :cond_2

    new-instance v0, Lcom/google/c/f/b/n;

    invoke-direct {v0}, Lcom/google/c/f/b/n;-><init>()V

    iput-object v0, p0, Lcom/google/c/f/b/m;->d:Lcom/google/c/f/b/n;

    :cond_2
    iget-object v0, p0, Lcom/google/c/f/b/m;->d:Lcom/google/c/f/b/n;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_5
    iget-object v0, p0, Lcom/google/c/f/b/m;->e:Lcom/google/c/f/b/e;

    if-nez v0, :cond_3

    new-instance v0, Lcom/google/c/f/b/e;

    invoke-direct {v0}, Lcom/google/c/f/b/e;-><init>()V

    iput-object v0, p0, Lcom/google/c/f/b/m;->e:Lcom/google/c/f/b/e;

    :cond_3
    iget-object v0, p0, Lcom/google/c/f/b/m;->e:Lcom/google/c/f/b/e;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_6
    iget-object v0, p0, Lcom/google/c/f/b/m;->f:Lcom/google/c/f/b/o;

    if-nez v0, :cond_4

    new-instance v0, Lcom/google/c/f/b/o;

    invoke-direct {v0}, Lcom/google/c/f/b/o;-><init>()V

    iput-object v0, p0, Lcom/google/c/f/b/m;->f:Lcom/google/c/f/b/o;

    :cond_4
    iget-object v0, p0, Lcom/google/c/f/b/m;->f:Lcom/google/c/f/b/o;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 98
    iget-object v0, p0, Lcom/google/c/f/b/m;->a:Lcom/google/c/f/c/a/e;

    if-eqz v0, :cond_0

    .line 99
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/c/f/b/m;->a:Lcom/google/c/f/c/a/e;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 101
    :cond_0
    iget-object v0, p0, Lcom/google/c/f/b/m;->b:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 102
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/c/f/b/m;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 104
    :cond_1
    iget-object v0, p0, Lcom/google/c/f/b/m;->c:Ljava/lang/Integer;

    if-eqz v0, :cond_2

    .line 105
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/c/f/b/m;->c:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 107
    :cond_2
    iget-object v0, p0, Lcom/google/c/f/b/m;->d:Lcom/google/c/f/b/n;

    if-eqz v0, :cond_3

    .line 108
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/c/f/b/m;->d:Lcom/google/c/f/b/n;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 110
    :cond_3
    iget-object v0, p0, Lcom/google/c/f/b/m;->e:Lcom/google/c/f/b/e;

    if-eqz v0, :cond_4

    .line 111
    const/4 v0, 0x5

    iget-object v1, p0, Lcom/google/c/f/b/m;->e:Lcom/google/c/f/b/e;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 113
    :cond_4
    iget-object v0, p0, Lcom/google/c/f/b/m;->f:Lcom/google/c/f/b/o;

    if-eqz v0, :cond_5

    .line 114
    const/4 v0, 0x6

    iget-object v1, p0, Lcom/google/c/f/b/m;->f:Lcom/google/c/f/b/o;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 116
    :cond_5
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/d;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 117
    return-void
.end method
