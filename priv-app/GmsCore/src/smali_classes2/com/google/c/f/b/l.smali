.class public final Lcom/google/c/f/b/l;
.super Lcom/google/protobuf/nano/d;
.source "SourceFile"


# static fields
.field private static volatile e:[Lcom/google/c/f/b/l;


# instance fields
.field public a:Lcom/google/c/f/b/m;

.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field public d:Lcom/google/c/f/b/c;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 922
    invoke-direct {p0}, Lcom/google/protobuf/nano/d;-><init>()V

    .line 923
    iput-object v0, p0, Lcom/google/c/f/b/l;->a:Lcom/google/c/f/b/m;

    iput-object v0, p0, Lcom/google/c/f/b/l;->b:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/c/f/b/l;->c:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/c/f/b/l;->d:Lcom/google/c/f/b/c;

    iput-object v0, p0, Lcom/google/c/f/b/l;->unknownFieldData:Lcom/google/protobuf/nano/f;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/c/f/b/l;->cachedSize:I

    .line 924
    return-void
.end method

.method public static a()[Lcom/google/c/f/b/l;
    .locals 2

    .prologue
    .line 899
    sget-object v0, Lcom/google/c/f/b/l;->e:[Lcom/google/c/f/b/l;

    if-nez v0, :cond_1

    .line 900
    sget-object v1, Lcom/google/protobuf/nano/h;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 902
    :try_start_0
    sget-object v0, Lcom/google/c/f/b/l;->e:[Lcom/google/c/f/b/l;

    if-nez v0, :cond_0

    .line 903
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/c/f/b/l;

    sput-object v0, Lcom/google/c/f/b/l;->e:[Lcom/google/c/f/b/l;

    .line 905
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 907
    :cond_1
    sget-object v0, Lcom/google/c/f/b/l;->e:[Lcom/google/c/f/b/l;

    return-object v0

    .line 905
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 956
    invoke-super {p0}, Lcom/google/protobuf/nano/d;->computeSerializedSize()I

    move-result v0

    .line 957
    iget-object v1, p0, Lcom/google/c/f/b/l;->a:Lcom/google/c/f/b/m;

    if-eqz v1, :cond_0

    .line 958
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/c/f/b/l;->a:Lcom/google/c/f/b/m;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 961
    :cond_0
    iget-object v1, p0, Lcom/google/c/f/b/l;->b:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 962
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/c/f/b/l;->b:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 965
    :cond_1
    iget-object v1, p0, Lcom/google/c/f/b/l;->c:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 966
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/c/f/b/l;->c:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 969
    :cond_2
    iget-object v1, p0, Lcom/google/c/f/b/l;->d:Lcom/google/c/f/b/c;

    if-eqz v1, :cond_3

    .line 970
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/c/f/b/l;->d:Lcom/google/c/f/b/c;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 973
    :cond_3
    return v0
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 893
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/c/f/b/l;->storeUnknownField(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Lcom/google/c/f/b/l;->a:Lcom/google/c/f/b/m;

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/c/f/b/m;

    invoke-direct {v0}, Lcom/google/c/f/b/m;-><init>()V

    iput-object v0, p0, Lcom/google/c/f/b/l;->a:Lcom/google/c/f/b/m;

    :cond_1
    iget-object v0, p0, Lcom/google/c/f/b/l;->a:Lcom/google/c/f/b/m;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/c/f/b/l;->b:Ljava/lang/String;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/c/f/b/l;->c:Ljava/lang/String;

    goto :goto_0

    :sswitch_4
    iget-object v0, p0, Lcom/google/c/f/b/l;->d:Lcom/google/c/f/b/c;

    if-nez v0, :cond_2

    new-instance v0, Lcom/google/c/f/b/c;

    invoke-direct {v0}, Lcom/google/c/f/b/c;-><init>()V

    iput-object v0, p0, Lcom/google/c/f/b/l;->d:Lcom/google/c/f/b/c;

    :cond_2
    iget-object v0, p0, Lcom/google/c/f/b/l;->d:Lcom/google/c/f/b/c;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 939
    iget-object v0, p0, Lcom/google/c/f/b/l;->a:Lcom/google/c/f/b/m;

    if-eqz v0, :cond_0

    .line 940
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/c/f/b/l;->a:Lcom/google/c/f/b/m;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 942
    :cond_0
    iget-object v0, p0, Lcom/google/c/f/b/l;->b:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 943
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/c/f/b/l;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 945
    :cond_1
    iget-object v0, p0, Lcom/google/c/f/b/l;->c:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 946
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/c/f/b/l;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 948
    :cond_2
    iget-object v0, p0, Lcom/google/c/f/b/l;->d:Lcom/google/c/f/b/c;

    if-eqz v0, :cond_3

    .line 949
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/c/f/b/l;->d:Lcom/google/c/f/b/c;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 951
    :cond_3
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/d;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 952
    return-void
.end method
