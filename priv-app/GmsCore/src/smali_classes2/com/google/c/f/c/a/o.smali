.class public final Lcom/google/c/f/c/a/o;
.super Lcom/google/protobuf/nano/d;
.source "SourceFile"


# instance fields
.field public a:Lcom/google/c/f/c/a/s;

.field public b:Ljava/lang/String;

.field public c:[Ljava/lang/String;

.field public d:Lcom/google/c/f/c/a/p;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1044
    invoke-direct {p0}, Lcom/google/protobuf/nano/d;-><init>()V

    .line 1045
    iput-object v1, p0, Lcom/google/c/f/c/a/o;->a:Lcom/google/c/f/c/a/s;

    iput-object v1, p0, Lcom/google/c/f/c/a/o;->b:Ljava/lang/String;

    sget-object v0, Lcom/google/protobuf/nano/m;->f:[Ljava/lang/String;

    iput-object v0, p0, Lcom/google/c/f/c/a/o;->c:[Ljava/lang/String;

    iput-object v1, p0, Lcom/google/c/f/c/a/o;->d:Lcom/google/c/f/c/a/p;

    iput-object v1, p0, Lcom/google/c/f/c/a/o;->unknownFieldData:Lcom/google/protobuf/nano/f;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/c/f/c/a/o;->cachedSize:I

    .line 1046
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 1083
    invoke-super {p0}, Lcom/google/protobuf/nano/d;->computeSerializedSize()I

    move-result v0

    .line 1084
    iget-object v2, p0, Lcom/google/c/f/c/a/o;->a:Lcom/google/c/f/c/a/s;

    if-eqz v2, :cond_0

    .line 1085
    const/4 v2, 0x1

    iget-object v3, p0, Lcom/google/c/f/c/a/o;->a:Lcom/google/c/f/c/a/s;

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v2

    add-int/2addr v0, v2

    .line 1088
    :cond_0
    iget-object v2, p0, Lcom/google/c/f/c/a/o;->b:Ljava/lang/String;

    if-eqz v2, :cond_1

    .line 1089
    const/4 v2, 0x2

    iget-object v3, p0, Lcom/google/c/f/c/a/o;->b:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 1092
    :cond_1
    iget-object v2, p0, Lcom/google/c/f/c/a/o;->c:[Ljava/lang/String;

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/google/c/f/c/a/o;->c:[Ljava/lang/String;

    array-length v2, v2

    if-lez v2, :cond_4

    move v2, v1

    move v3, v1

    .line 1095
    :goto_0
    iget-object v4, p0, Lcom/google/c/f/c/a/o;->c:[Ljava/lang/String;

    array-length v4, v4

    if-ge v1, v4, :cond_3

    .line 1096
    iget-object v4, p0, Lcom/google/c/f/c/a/o;->c:[Ljava/lang/String;

    aget-object v4, v4, v1

    .line 1097
    if-eqz v4, :cond_2

    .line 1098
    add-int/lit8 v3, v3, 0x1

    .line 1099
    invoke-static {v4}, Lcom/google/protobuf/nano/b;->a(Ljava/lang/String;)I

    move-result v4

    add-int/2addr v2, v4

    .line 1095
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1103
    :cond_3
    add-int/2addr v0, v2

    .line 1104
    mul-int/lit8 v1, v3, 0x1

    add-int/2addr v0, v1

    .line 1106
    :cond_4
    iget-object v1, p0, Lcom/google/c/f/c/a/o;->d:Lcom/google/c/f/c/a/p;

    if-eqz v1, :cond_5

    .line 1107
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/c/f/c/a/o;->d:Lcom/google/c/f/c/a/p;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1110
    :cond_5
    return v0
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 904
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/c/f/c/a/o;->storeUnknownField(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Lcom/google/c/f/c/a/o;->a:Lcom/google/c/f/c/a/s;

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/c/f/c/a/s;

    invoke-direct {v0}, Lcom/google/c/f/c/a/s;-><init>()V

    iput-object v0, p0, Lcom/google/c/f/c/a/o;->a:Lcom/google/c/f/c/a/s;

    :cond_1
    iget-object v0, p0, Lcom/google/c/f/c/a/o;->a:Lcom/google/c/f/c/a/s;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/c/f/c/a/o;->b:Ljava/lang/String;

    goto :goto_0

    :sswitch_3
    const/16 v0, 0x1a

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->b(Lcom/google/protobuf/nano/a;I)I

    move-result v2

    iget-object v0, p0, Lcom/google/c/f/c/a/o;->c:[Ljava/lang/String;

    if-nez v0, :cond_3

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Ljava/lang/String;

    if-eqz v0, :cond_2

    iget-object v3, p0, Lcom/google/c/f/c/a/o;->c:[Ljava/lang/String;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_2
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_4

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_3
    iget-object v0, p0, Lcom/google/c/f/c/a/o;->c:[Ljava/lang/String;

    array-length v0, v0

    goto :goto_1

    :cond_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    iput-object v2, p0, Lcom/google/c/f/c/a/o;->c:[Ljava/lang/String;

    goto :goto_0

    :sswitch_4
    iget-object v0, p0, Lcom/google/c/f/c/a/o;->d:Lcom/google/c/f/c/a/p;

    if-nez v0, :cond_5

    new-instance v0, Lcom/google/c/f/c/a/p;

    invoke-direct {v0}, Lcom/google/c/f/c/a/p;-><init>()V

    iput-object v0, p0, Lcom/google/c/f/c/a/o;->d:Lcom/google/c/f/c/a/p;

    :cond_5
    iget-object v0, p0, Lcom/google/c/f/c/a/o;->d:Lcom/google/c/f/c/a/p;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 3

    .prologue
    .line 1061
    iget-object v0, p0, Lcom/google/c/f/c/a/o;->a:Lcom/google/c/f/c/a/s;

    if-eqz v0, :cond_0

    .line 1062
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/c/f/c/a/o;->a:Lcom/google/c/f/c/a/s;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 1064
    :cond_0
    iget-object v0, p0, Lcom/google/c/f/c/a/o;->b:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 1065
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/c/f/c/a/o;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 1067
    :cond_1
    iget-object v0, p0, Lcom/google/c/f/c/a/o;->c:[Ljava/lang/String;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/c/f/c/a/o;->c:[Ljava/lang/String;

    array-length v0, v0

    if-lez v0, :cond_3

    .line 1068
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/google/c/f/c/a/o;->c:[Ljava/lang/String;

    array-length v1, v1

    if-ge v0, v1, :cond_3

    .line 1069
    iget-object v1, p0, Lcom/google/c/f/c/a/o;->c:[Ljava/lang/String;

    aget-object v1, v1, v0

    .line 1070
    if-eqz v1, :cond_2

    .line 1071
    const/4 v2, 0x3

    invoke-virtual {p1, v2, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 1068
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1075
    :cond_3
    iget-object v0, p0, Lcom/google/c/f/c/a/o;->d:Lcom/google/c/f/c/a/p;

    if-eqz v0, :cond_4

    .line 1076
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/c/f/c/a/o;->d:Lcom/google/c/f/c/a/p;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 1078
    :cond_4
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/d;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 1079
    return-void
.end method
