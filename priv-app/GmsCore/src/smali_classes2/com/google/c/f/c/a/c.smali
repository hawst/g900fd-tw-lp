.class public final Lcom/google/c/f/c/a/c;
.super Lcom/google/protobuf/nano/d;
.source "SourceFile"


# static fields
.field private static volatile e:[Lcom/google/c/f/c/a/c;


# instance fields
.field public a:Lcom/google/c/f/c/a/d;

.field public b:Lcom/google/c/f/c/a/j;

.field public c:Ljava/lang/Boolean;

.field public d:Lcom/google/c/f/c/a/m;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 3536
    invoke-direct {p0}, Lcom/google/protobuf/nano/d;-><init>()V

    .line 3537
    iput-object v0, p0, Lcom/google/c/f/c/a/c;->a:Lcom/google/c/f/c/a/d;

    iput-object v0, p0, Lcom/google/c/f/c/a/c;->b:Lcom/google/c/f/c/a/j;

    iput-object v0, p0, Lcom/google/c/f/c/a/c;->c:Ljava/lang/Boolean;

    iput-object v0, p0, Lcom/google/c/f/c/a/c;->d:Lcom/google/c/f/c/a/m;

    iput-object v0, p0, Lcom/google/c/f/c/a/c;->unknownFieldData:Lcom/google/protobuf/nano/f;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/c/f/c/a/c;->cachedSize:I

    .line 3538
    return-void
.end method

.method public static a()[Lcom/google/c/f/c/a/c;
    .locals 2

    .prologue
    .line 3513
    sget-object v0, Lcom/google/c/f/c/a/c;->e:[Lcom/google/c/f/c/a/c;

    if-nez v0, :cond_1

    .line 3514
    sget-object v1, Lcom/google/protobuf/nano/h;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 3516
    :try_start_0
    sget-object v0, Lcom/google/c/f/c/a/c;->e:[Lcom/google/c/f/c/a/c;

    if-nez v0, :cond_0

    .line 3517
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/c/f/c/a/c;

    sput-object v0, Lcom/google/c/f/c/a/c;->e:[Lcom/google/c/f/c/a/c;

    .line 3519
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 3521
    :cond_1
    sget-object v0, Lcom/google/c/f/c/a/c;->e:[Lcom/google/c/f/c/a/c;

    return-object v0

    .line 3519
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 3570
    invoke-super {p0}, Lcom/google/protobuf/nano/d;->computeSerializedSize()I

    move-result v0

    .line 3571
    iget-object v1, p0, Lcom/google/c/f/c/a/c;->a:Lcom/google/c/f/c/a/d;

    if-eqz v1, :cond_0

    .line 3572
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/c/f/c/a/c;->a:Lcom/google/c/f/c/a/d;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3575
    :cond_0
    iget-object v1, p0, Lcom/google/c/f/c/a/c;->b:Lcom/google/c/f/c/a/j;

    if-eqz v1, :cond_1

    .line 3576
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/c/f/c/a/c;->b:Lcom/google/c/f/c/a/j;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3579
    :cond_1
    iget-object v1, p0, Lcom/google/c/f/c/a/c;->c:Ljava/lang/Boolean;

    if-eqz v1, :cond_2

    .line 3580
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/c/f/c/a/c;->c:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 3583
    :cond_2
    iget-object v1, p0, Lcom/google/c/f/c/a/c;->d:Lcom/google/c/f/c/a/m;

    if-eqz v1, :cond_3

    .line 3584
    const/4 v1, 0x5

    iget-object v2, p0, Lcom/google/c/f/c/a/c;->d:Lcom/google/c/f/c/a/m;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3587
    :cond_3
    return v0
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 3507
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/c/f/c/a/c;->storeUnknownField(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Lcom/google/c/f/c/a/c;->a:Lcom/google/c/f/c/a/d;

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/c/f/c/a/d;

    invoke-direct {v0}, Lcom/google/c/f/c/a/d;-><init>()V

    iput-object v0, p0, Lcom/google/c/f/c/a/c;->a:Lcom/google/c/f/c/a/d;

    :cond_1
    iget-object v0, p0, Lcom/google/c/f/c/a/c;->a:Lcom/google/c/f/c/a/d;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Lcom/google/c/f/c/a/c;->b:Lcom/google/c/f/c/a/j;

    if-nez v0, :cond_2

    new-instance v0, Lcom/google/c/f/c/a/j;

    invoke-direct {v0}, Lcom/google/c/f/c/a/j;-><init>()V

    iput-object v0, p0, Lcom/google/c/f/c/a/c;->b:Lcom/google/c/f/c/a/j;

    :cond_2
    iget-object v0, p0, Lcom/google/c/f/c/a/c;->b:Lcom/google/c/f/c/a/j;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/c/f/c/a/c;->c:Ljava/lang/Boolean;

    goto :goto_0

    :sswitch_4
    iget-object v0, p0, Lcom/google/c/f/c/a/c;->d:Lcom/google/c/f/c/a/m;

    if-nez v0, :cond_3

    new-instance v0, Lcom/google/c/f/c/a/m;

    invoke-direct {v0}, Lcom/google/c/f/c/a/m;-><init>()V

    iput-object v0, p0, Lcom/google/c/f/c/a/c;->d:Lcom/google/c/f/c/a/m;

    :cond_3
    iget-object v0, p0, Lcom/google/c/f/c/a/c;->d:Lcom/google/c/f/c/a/m;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x2a -> :sswitch_4
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 3553
    iget-object v0, p0, Lcom/google/c/f/c/a/c;->a:Lcom/google/c/f/c/a/d;

    if-eqz v0, :cond_0

    .line 3554
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/c/f/c/a/c;->a:Lcom/google/c/f/c/a/d;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 3556
    :cond_0
    iget-object v0, p0, Lcom/google/c/f/c/a/c;->b:Lcom/google/c/f/c/a/j;

    if-eqz v0, :cond_1

    .line 3557
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/c/f/c/a/c;->b:Lcom/google/c/f/c/a/j;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 3559
    :cond_1
    iget-object v0, p0, Lcom/google/c/f/c/a/c;->c:Ljava/lang/Boolean;

    if-eqz v0, :cond_2

    .line 3560
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/c/f/c/a/c;->c:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 3562
    :cond_2
    iget-object v0, p0, Lcom/google/c/f/c/a/c;->d:Lcom/google/c/f/c/a/m;

    if-eqz v0, :cond_3

    .line 3563
    const/4 v0, 0x5

    iget-object v1, p0, Lcom/google/c/f/c/a/c;->d:Lcom/google/c/f/c/a/m;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 3565
    :cond_3
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/d;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 3566
    return-void
.end method
