.class public final Lcom/google/c/f/a/a/e;
.super Lcom/google/protobuf/nano/d;
.source "SourceFile"


# instance fields
.field public a:Lcom/google/c/f/a/a/d;

.field public b:Ljava/lang/String;

.field public c:Lcom/google/c/f/a/a/c;

.field public d:Ljava/lang/Double;

.field public e:Ljava/lang/Double;

.field public f:Ljava/lang/Integer;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 179
    invoke-direct {p0}, Lcom/google/protobuf/nano/d;-><init>()V

    .line 180
    iput-object v0, p0, Lcom/google/c/f/a/a/e;->a:Lcom/google/c/f/a/a/d;

    iput-object v0, p0, Lcom/google/c/f/a/a/e;->b:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/c/f/a/a/e;->c:Lcom/google/c/f/a/a/c;

    iput-object v0, p0, Lcom/google/c/f/a/a/e;->d:Ljava/lang/Double;

    iput-object v0, p0, Lcom/google/c/f/a/a/e;->e:Ljava/lang/Double;

    iput-object v0, p0, Lcom/google/c/f/a/a/e;->f:Ljava/lang/Integer;

    iput-object v0, p0, Lcom/google/c/f/a/a/e;->unknownFieldData:Lcom/google/protobuf/nano/f;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/c/f/a/a/e;->cachedSize:I

    .line 181
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 221
    invoke-super {p0}, Lcom/google/protobuf/nano/d;->computeSerializedSize()I

    move-result v0

    .line 222
    iget-object v1, p0, Lcom/google/c/f/a/a/e;->a:Lcom/google/c/f/a/a/d;

    if-eqz v1, :cond_0

    .line 223
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/c/f/a/a/e;->a:Lcom/google/c/f/a/a/d;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 226
    :cond_0
    iget-object v1, p0, Lcom/google/c/f/a/a/e;->b:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 227
    const/4 v1, 0x6

    iget-object v2, p0, Lcom/google/c/f/a/a/e;->b:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 230
    :cond_1
    iget-object v1, p0, Lcom/google/c/f/a/a/e;->c:Lcom/google/c/f/a/a/c;

    if-eqz v1, :cond_2

    .line 231
    const/16 v1, 0xc

    iget-object v2, p0, Lcom/google/c/f/a/a/e;->c:Lcom/google/c/f/a/a/c;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 234
    :cond_2
    iget-object v1, p0, Lcom/google/c/f/a/a/e;->d:Ljava/lang/Double;

    if-eqz v1, :cond_3

    .line 235
    const/16 v1, 0x10

    iget-object v2, p0, Lcom/google/c/f/a/a/e;->d:Ljava/lang/Double;

    invoke-virtual {v2}, Ljava/lang/Double;->doubleValue()D

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x8

    add-int/2addr v0, v1

    .line 238
    :cond_3
    iget-object v1, p0, Lcom/google/c/f/a/a/e;->e:Ljava/lang/Double;

    if-eqz v1, :cond_4

    .line 239
    const/16 v1, 0x14

    iget-object v2, p0, Lcom/google/c/f/a/a/e;->e:Ljava/lang/Double;

    invoke-virtual {v2}, Ljava/lang/Double;->doubleValue()D

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x8

    add-int/2addr v0, v1

    .line 242
    :cond_4
    iget-object v1, p0, Lcom/google/c/f/a/a/e;->f:Ljava/lang/Integer;

    if-eqz v1, :cond_5

    .line 243
    const/16 v1, 0x23

    iget-object v2, p0, Lcom/google/c/f/a/a/e;->f:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 246
    :cond_5
    return v0
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 2

    .prologue
    .line 137
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/c/f/a/a/e;->storeUnknownField(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Lcom/google/c/f/a/a/e;->a:Lcom/google/c/f/a/a/d;

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/c/f/a/a/d;

    invoke-direct {v0}, Lcom/google/c/f/a/a/d;-><init>()V

    iput-object v0, p0, Lcom/google/c/f/a/a/e;->a:Lcom/google/c/f/a/a/d;

    :cond_1
    iget-object v0, p0, Lcom/google/c/f/a/a/e;->a:Lcom/google/c/f/a/a/d;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/c/f/a/a/e;->b:Ljava/lang/String;

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Lcom/google/c/f/a/a/e;->c:Lcom/google/c/f/a/a/c;

    if-nez v0, :cond_2

    new-instance v0, Lcom/google/c/f/a/a/c;

    invoke-direct {v0}, Lcom/google/c/f/a/a/c;-><init>()V

    iput-object v0, p0, Lcom/google/c/f/a/a/e;->c:Lcom/google/c/f/a/a/c;

    :cond_2
    iget-object v0, p0, Lcom/google/c/f/a/a/e;->c:Lcom/google/c/f/a/a/c;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->l()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Double;->longBitsToDouble(J)D

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    iput-object v0, p0, Lcom/google/c/f/a/a/e;->d:Ljava/lang/Double;

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->l()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Double;->longBitsToDouble(J)D

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    iput-object v0, p0, Lcom/google/c/f/a/a/e;->e:Ljava/lang/Double;

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/c/f/a/a/e;->f:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x1a -> :sswitch_1
        0x32 -> :sswitch_2
        0x62 -> :sswitch_3
        0x81 -> :sswitch_4
        0xa1 -> :sswitch_5
        0x118 -> :sswitch_6
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 4

    .prologue
    .line 198
    iget-object v0, p0, Lcom/google/c/f/a/a/e;->a:Lcom/google/c/f/a/a/d;

    if-eqz v0, :cond_0

    .line 199
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/c/f/a/a/e;->a:Lcom/google/c/f/a/a/d;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 201
    :cond_0
    iget-object v0, p0, Lcom/google/c/f/a/a/e;->b:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 202
    const/4 v0, 0x6

    iget-object v1, p0, Lcom/google/c/f/a/a/e;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 204
    :cond_1
    iget-object v0, p0, Lcom/google/c/f/a/a/e;->c:Lcom/google/c/f/a/a/c;

    if-eqz v0, :cond_2

    .line 205
    const/16 v0, 0xc

    iget-object v1, p0, Lcom/google/c/f/a/a/e;->c:Lcom/google/c/f/a/a/c;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 207
    :cond_2
    iget-object v0, p0, Lcom/google/c/f/a/a/e;->d:Ljava/lang/Double;

    if-eqz v0, :cond_3

    .line 208
    const/16 v0, 0x10

    iget-object v1, p0, Lcom/google/c/f/a/a/e;->d:Ljava/lang/Double;

    invoke-virtual {v1}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/b;->a(ID)V

    .line 210
    :cond_3
    iget-object v0, p0, Lcom/google/c/f/a/a/e;->e:Ljava/lang/Double;

    if-eqz v0, :cond_4

    .line 211
    const/16 v0, 0x14

    iget-object v1, p0, Lcom/google/c/f/a/a/e;->e:Ljava/lang/Double;

    invoke-virtual {v1}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/b;->a(ID)V

    .line 213
    :cond_4
    iget-object v0, p0, Lcom/google/c/f/a/a/e;->f:Ljava/lang/Integer;

    if-eqz v0, :cond_5

    .line 214
    const/16 v0, 0x23

    iget-object v1, p0, Lcom/google/c/f/a/a/e;->f:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 216
    :cond_5
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/d;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 217
    return-void
.end method
