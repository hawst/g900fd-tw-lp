.class public final Lcom/google/c/f/c/a/k;
.super Lcom/google/protobuf/nano/d;
.source "SourceFile"


# instance fields
.field public a:Ljava/lang/String;

.field public b:[Lcom/google/c/f/c/a/l;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2866
    invoke-direct {p0}, Lcom/google/protobuf/nano/d;-><init>()V

    .line 2867
    iput-object v1, p0, Lcom/google/c/f/c/a/k;->a:Ljava/lang/String;

    invoke-static {}, Lcom/google/c/f/c/a/l;->a()[Lcom/google/c/f/c/a/l;

    move-result-object v0

    iput-object v0, p0, Lcom/google/c/f/c/a/k;->b:[Lcom/google/c/f/c/a/l;

    iput-object v1, p0, Lcom/google/c/f/c/a/k;->unknownFieldData:Lcom/google/protobuf/nano/f;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/c/f/c/a/k;->cachedSize:I

    .line 2868
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 5

    .prologue
    .line 2897
    invoke-super {p0}, Lcom/google/protobuf/nano/d;->computeSerializedSize()I

    move-result v0

    .line 2898
    iget-object v1, p0, Lcom/google/c/f/c/a/k;->a:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 2899
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/c/f/c/a/k;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2902
    :cond_0
    iget-object v1, p0, Lcom/google/c/f/c/a/k;->b:[Lcom/google/c/f/c/a/l;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/google/c/f/c/a/k;->b:[Lcom/google/c/f/c/a/l;

    array-length v1, v1

    if-lez v1, :cond_3

    .line 2903
    const/4 v1, 0x0

    move v4, v1

    move v1, v0

    move v0, v4

    :goto_0
    iget-object v2, p0, Lcom/google/c/f/c/a/k;->b:[Lcom/google/c/f/c/a/l;

    array-length v2, v2

    if-ge v0, v2, :cond_2

    .line 2904
    iget-object v2, p0, Lcom/google/c/f/c/a/k;->b:[Lcom/google/c/f/c/a/l;

    aget-object v2, v2, v0

    .line 2905
    if-eqz v2, :cond_1

    .line 2906
    const/4 v3, 0x2

    invoke-static {v3, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v2

    add-int/2addr v1, v2

    .line 2903
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    move v0, v1

    .line 2911
    :cond_3
    return v0
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 2843
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/c/f/c/a/k;->storeUnknownField(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/c/f/c/a/k;->a:Ljava/lang/String;

    goto :goto_0

    :sswitch_2
    const/16 v0, 0x12

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->b(Lcom/google/protobuf/nano/a;I)I

    move-result v2

    iget-object v0, p0, Lcom/google/c/f/c/a/k;->b:[Lcom/google/c/f/c/a/l;

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lcom/google/c/f/c/a/l;

    if-eqz v0, :cond_1

    iget-object v3, p0, Lcom/google/c/f/c/a/k;->b:[Lcom/google/c/f/c/a/l;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_1
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_3

    new-instance v3, Lcom/google/c/f/c/a/l;

    invoke-direct {v3}, Lcom/google/c/f/c/a/l;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    iget-object v0, p0, Lcom/google/c/f/c/a/k;->b:[Lcom/google/c/f/c/a/l;

    array-length v0, v0

    goto :goto_1

    :cond_3
    new-instance v3, Lcom/google/c/f/c/a/l;

    invoke-direct {v3}, Lcom/google/c/f/c/a/l;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    iput-object v2, p0, Lcom/google/c/f/c/a/k;->b:[Lcom/google/c/f/c/a/l;

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 3

    .prologue
    .line 2881
    iget-object v0, p0, Lcom/google/c/f/c/a/k;->a:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 2882
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/c/f/c/a/k;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 2884
    :cond_0
    iget-object v0, p0, Lcom/google/c/f/c/a/k;->b:[Lcom/google/c/f/c/a/l;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/c/f/c/a/k;->b:[Lcom/google/c/f/c/a/l;

    array-length v0, v0

    if-lez v0, :cond_2

    .line 2885
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/google/c/f/c/a/k;->b:[Lcom/google/c/f/c/a/l;

    array-length v1, v1

    if-ge v0, v1, :cond_2

    .line 2886
    iget-object v1, p0, Lcom/google/c/f/c/a/k;->b:[Lcom/google/c/f/c/a/l;

    aget-object v1, v1, v0

    .line 2887
    if-eqz v1, :cond_1

    .line 2888
    const/4 v2, 0x2

    invoke-virtual {p1, v2, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 2885
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2892
    :cond_2
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/d;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 2893
    return-void
.end method
