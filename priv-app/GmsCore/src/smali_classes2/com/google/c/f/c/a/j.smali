.class public final Lcom/google/c/f/c/a/j;
.super Lcom/google/protobuf/nano/d;
.source "SourceFile"


# instance fields
.field public a:Ljava/lang/Integer;

.field public b:Ljava/lang/Integer;

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/String;

.field public e:Ljava/lang/String;

.field public f:Ljava/lang/String;

.field public g:Ljava/lang/Double;

.field public h:Ljava/lang/String;

.field public i:Ljava/lang/Integer;

.field public j:Ljava/lang/Integer;

.field public k:Ljava/lang/Boolean;

.field public l:Ljava/lang/Boolean;

.field public m:Ljava/lang/String;

.field public n:Ljava/lang/Integer;

.field public o:Ljava/lang/Boolean;

.field public p:Ljava/lang/Boolean;

.field public q:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 3243
    invoke-direct {p0}, Lcom/google/protobuf/nano/d;-><init>()V

    .line 3244
    iput-object v0, p0, Lcom/google/c/f/c/a/j;->a:Ljava/lang/Integer;

    iput-object v0, p0, Lcom/google/c/f/c/a/j;->b:Ljava/lang/Integer;

    iput-object v0, p0, Lcom/google/c/f/c/a/j;->c:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/c/f/c/a/j;->d:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/c/f/c/a/j;->e:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/c/f/c/a/j;->f:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/c/f/c/a/j;->g:Ljava/lang/Double;

    iput-object v0, p0, Lcom/google/c/f/c/a/j;->h:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/c/f/c/a/j;->i:Ljava/lang/Integer;

    iput-object v0, p0, Lcom/google/c/f/c/a/j;->j:Ljava/lang/Integer;

    iput-object v0, p0, Lcom/google/c/f/c/a/j;->k:Ljava/lang/Boolean;

    iput-object v0, p0, Lcom/google/c/f/c/a/j;->l:Ljava/lang/Boolean;

    iput-object v0, p0, Lcom/google/c/f/c/a/j;->m:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/c/f/c/a/j;->n:Ljava/lang/Integer;

    iput-object v0, p0, Lcom/google/c/f/c/a/j;->o:Ljava/lang/Boolean;

    iput-object v0, p0, Lcom/google/c/f/c/a/j;->p:Ljava/lang/Boolean;

    iput-object v0, p0, Lcom/google/c/f/c/a/j;->q:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/c/f/c/a/j;->unknownFieldData:Lcom/google/protobuf/nano/f;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/c/f/c/a/j;->cachedSize:I

    .line 3245
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 3329
    invoke-super {p0}, Lcom/google/protobuf/nano/d;->computeSerializedSize()I

    move-result v0

    .line 3330
    iget-object v1, p0, Lcom/google/c/f/c/a/j;->c:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 3331
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/c/f/c/a/j;->c:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3334
    :cond_0
    iget-object v1, p0, Lcom/google/c/f/c/a/j;->d:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 3335
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/c/f/c/a/j;->d:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3338
    :cond_1
    iget-object v1, p0, Lcom/google/c/f/c/a/j;->e:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 3339
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/c/f/c/a/j;->e:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3342
    :cond_2
    iget-object v1, p0, Lcom/google/c/f/c/a/j;->f:Ljava/lang/String;

    if-eqz v1, :cond_3

    .line 3343
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/c/f/c/a/j;->f:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3346
    :cond_3
    iget-object v1, p0, Lcom/google/c/f/c/a/j;->g:Ljava/lang/Double;

    if-eqz v1, :cond_4

    .line 3347
    const/4 v1, 0x5

    iget-object v2, p0, Lcom/google/c/f/c/a/j;->g:Ljava/lang/Double;

    invoke-virtual {v2}, Ljava/lang/Double;->doubleValue()D

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x8

    add-int/2addr v0, v1

    .line 3350
    :cond_4
    iget-object v1, p0, Lcom/google/c/f/c/a/j;->h:Ljava/lang/String;

    if-eqz v1, :cond_5

    .line 3351
    const/4 v1, 0x6

    iget-object v2, p0, Lcom/google/c/f/c/a/j;->h:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3354
    :cond_5
    iget-object v1, p0, Lcom/google/c/f/c/a/j;->i:Ljava/lang/Integer;

    if-eqz v1, :cond_6

    .line 3355
    const/4 v1, 0x7

    iget-object v2, p0, Lcom/google/c/f/c/a/j;->i:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 3358
    :cond_6
    iget-object v1, p0, Lcom/google/c/f/c/a/j;->j:Ljava/lang/Integer;

    if-eqz v1, :cond_7

    .line 3359
    const/16 v1, 0x8

    iget-object v2, p0, Lcom/google/c/f/c/a/j;->j:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 3362
    :cond_7
    iget-object v1, p0, Lcom/google/c/f/c/a/j;->k:Ljava/lang/Boolean;

    if-eqz v1, :cond_8

    .line 3363
    const/16 v1, 0x9

    iget-object v2, p0, Lcom/google/c/f/c/a/j;->k:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 3366
    :cond_8
    iget-object v1, p0, Lcom/google/c/f/c/a/j;->a:Ljava/lang/Integer;

    if-eqz v1, :cond_9

    .line 3367
    const/16 v1, 0xa

    iget-object v2, p0, Lcom/google/c/f/c/a/j;->a:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 3370
    :cond_9
    iget-object v1, p0, Lcom/google/c/f/c/a/j;->b:Ljava/lang/Integer;

    if-eqz v1, :cond_a

    .line 3371
    const/16 v1, 0xb

    iget-object v2, p0, Lcom/google/c/f/c/a/j;->b:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 3374
    :cond_a
    iget-object v1, p0, Lcom/google/c/f/c/a/j;->l:Ljava/lang/Boolean;

    if-eqz v1, :cond_b

    .line 3375
    const/16 v1, 0xc

    iget-object v2, p0, Lcom/google/c/f/c/a/j;->l:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 3378
    :cond_b
    iget-object v1, p0, Lcom/google/c/f/c/a/j;->m:Ljava/lang/String;

    if-eqz v1, :cond_c

    .line 3379
    const/16 v1, 0xd

    iget-object v2, p0, Lcom/google/c/f/c/a/j;->m:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3382
    :cond_c
    iget-object v1, p0, Lcom/google/c/f/c/a/j;->n:Ljava/lang/Integer;

    if-eqz v1, :cond_d

    .line 3383
    const/16 v1, 0xe

    iget-object v2, p0, Lcom/google/c/f/c/a/j;->n:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 3386
    :cond_d
    iget-object v1, p0, Lcom/google/c/f/c/a/j;->o:Ljava/lang/Boolean;

    if-eqz v1, :cond_e

    .line 3387
    const/16 v1, 0xf

    iget-object v2, p0, Lcom/google/c/f/c/a/j;->o:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 3390
    :cond_e
    iget-object v1, p0, Lcom/google/c/f/c/a/j;->p:Ljava/lang/Boolean;

    if-eqz v1, :cond_f

    .line 3391
    const/16 v1, 0x10

    iget-object v2, p0, Lcom/google/c/f/c/a/j;->p:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 3394
    :cond_f
    iget-object v1, p0, Lcom/google/c/f/c/a/j;->q:Ljava/lang/String;

    if-eqz v1, :cond_10

    .line 3395
    const/16 v1, 0x11

    iget-object v2, p0, Lcom/google/c/f/c/a/j;->q:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3398
    :cond_10
    return v0
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 2

    .prologue
    .line 3175
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/c/f/c/a/j;->storeUnknownField(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/c/f/c/a/j;->c:Ljava/lang/String;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/c/f/c/a/j;->d:Ljava/lang/String;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/c/f/c/a/j;->e:Ljava/lang/String;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/c/f/c/a/j;->f:Ljava/lang/String;

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->l()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Double;->longBitsToDouble(J)D

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    iput-object v0, p0, Lcom/google/c/f/c/a/j;->g:Ljava/lang/Double;

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/c/f/c/a/j;->h:Ljava/lang/String;

    goto :goto_0

    :sswitch_7
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/c/f/c/a/j;->i:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_8
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/c/f/c/a/j;->j:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_9
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/c/f/c/a/j;->k:Ljava/lang/Boolean;

    goto :goto_0

    :sswitch_a
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/c/f/c/a/j;->a:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_b
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/c/f/c/a/j;->b:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_c
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/c/f/c/a/j;->l:Ljava/lang/Boolean;

    goto/16 :goto_0

    :sswitch_d
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/c/f/c/a/j;->m:Ljava/lang/String;

    goto/16 :goto_0

    :sswitch_e
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/c/f/c/a/j;->n:Ljava/lang/Integer;

    goto/16 :goto_0

    :sswitch_f
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/c/f/c/a/j;->o:Ljava/lang/Boolean;

    goto/16 :goto_0

    :sswitch_10
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/c/f/c/a/j;->p:Ljava/lang/Boolean;

    goto/16 :goto_0

    :sswitch_11
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/c/f/c/a/j;->q:Ljava/lang/String;

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x29 -> :sswitch_5
        0x32 -> :sswitch_6
        0x38 -> :sswitch_7
        0x40 -> :sswitch_8
        0x48 -> :sswitch_9
        0x50 -> :sswitch_a
        0x58 -> :sswitch_b
        0x60 -> :sswitch_c
        0x6a -> :sswitch_d
        0x70 -> :sswitch_e
        0x78 -> :sswitch_f
        0x80 -> :sswitch_10
        0x8a -> :sswitch_11
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 4

    .prologue
    .line 3273
    iget-object v0, p0, Lcom/google/c/f/c/a/j;->c:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 3274
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/c/f/c/a/j;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 3276
    :cond_0
    iget-object v0, p0, Lcom/google/c/f/c/a/j;->d:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 3277
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/c/f/c/a/j;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 3279
    :cond_1
    iget-object v0, p0, Lcom/google/c/f/c/a/j;->e:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 3280
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/c/f/c/a/j;->e:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 3282
    :cond_2
    iget-object v0, p0, Lcom/google/c/f/c/a/j;->f:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 3283
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/c/f/c/a/j;->f:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 3285
    :cond_3
    iget-object v0, p0, Lcom/google/c/f/c/a/j;->g:Ljava/lang/Double;

    if-eqz v0, :cond_4

    .line 3286
    const/4 v0, 0x5

    iget-object v1, p0, Lcom/google/c/f/c/a/j;->g:Ljava/lang/Double;

    invoke-virtual {v1}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/b;->a(ID)V

    .line 3288
    :cond_4
    iget-object v0, p0, Lcom/google/c/f/c/a/j;->h:Ljava/lang/String;

    if-eqz v0, :cond_5

    .line 3289
    const/4 v0, 0x6

    iget-object v1, p0, Lcom/google/c/f/c/a/j;->h:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 3291
    :cond_5
    iget-object v0, p0, Lcom/google/c/f/c/a/j;->i:Ljava/lang/Integer;

    if-eqz v0, :cond_6

    .line 3292
    const/4 v0, 0x7

    iget-object v1, p0, Lcom/google/c/f/c/a/j;->i:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 3294
    :cond_6
    iget-object v0, p0, Lcom/google/c/f/c/a/j;->j:Ljava/lang/Integer;

    if-eqz v0, :cond_7

    .line 3295
    const/16 v0, 0x8

    iget-object v1, p0, Lcom/google/c/f/c/a/j;->j:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 3297
    :cond_7
    iget-object v0, p0, Lcom/google/c/f/c/a/j;->k:Ljava/lang/Boolean;

    if-eqz v0, :cond_8

    .line 3298
    const/16 v0, 0x9

    iget-object v1, p0, Lcom/google/c/f/c/a/j;->k:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 3300
    :cond_8
    iget-object v0, p0, Lcom/google/c/f/c/a/j;->a:Ljava/lang/Integer;

    if-eqz v0, :cond_9

    .line 3301
    const/16 v0, 0xa

    iget-object v1, p0, Lcom/google/c/f/c/a/j;->a:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 3303
    :cond_9
    iget-object v0, p0, Lcom/google/c/f/c/a/j;->b:Ljava/lang/Integer;

    if-eqz v0, :cond_a

    .line 3304
    const/16 v0, 0xb

    iget-object v1, p0, Lcom/google/c/f/c/a/j;->b:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 3306
    :cond_a
    iget-object v0, p0, Lcom/google/c/f/c/a/j;->l:Ljava/lang/Boolean;

    if-eqz v0, :cond_b

    .line 3307
    const/16 v0, 0xc

    iget-object v1, p0, Lcom/google/c/f/c/a/j;->l:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 3309
    :cond_b
    iget-object v0, p0, Lcom/google/c/f/c/a/j;->m:Ljava/lang/String;

    if-eqz v0, :cond_c

    .line 3310
    const/16 v0, 0xd

    iget-object v1, p0, Lcom/google/c/f/c/a/j;->m:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 3312
    :cond_c
    iget-object v0, p0, Lcom/google/c/f/c/a/j;->n:Ljava/lang/Integer;

    if-eqz v0, :cond_d

    .line 3313
    const/16 v0, 0xe

    iget-object v1, p0, Lcom/google/c/f/c/a/j;->n:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 3315
    :cond_d
    iget-object v0, p0, Lcom/google/c/f/c/a/j;->o:Ljava/lang/Boolean;

    if-eqz v0, :cond_e

    .line 3316
    const/16 v0, 0xf

    iget-object v1, p0, Lcom/google/c/f/c/a/j;->o:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 3318
    :cond_e
    iget-object v0, p0, Lcom/google/c/f/c/a/j;->p:Ljava/lang/Boolean;

    if-eqz v0, :cond_f

    .line 3319
    const/16 v0, 0x10

    iget-object v1, p0, Lcom/google/c/f/c/a/j;->p:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 3321
    :cond_f
    iget-object v0, p0, Lcom/google/c/f/c/a/j;->q:Ljava/lang/String;

    if-eqz v0, :cond_10

    .line 3322
    const/16 v0, 0x11

    iget-object v1, p0, Lcom/google/c/f/c/a/j;->q:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 3324
    :cond_10
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/d;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 3325
    return-void
.end method
