.class public final Lcom/google/c/f/a/a/f;
.super Lcom/google/protobuf/nano/d;
.source "SourceFile"


# instance fields
.field public a:Ljava/lang/Boolean;

.field public b:Ljava/lang/Boolean;

.field public c:Ljava/lang/Boolean;

.field public d:Ljava/lang/Boolean;

.field public e:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 441
    invoke-direct {p0}, Lcom/google/protobuf/nano/d;-><init>()V

    .line 442
    iput-object v0, p0, Lcom/google/c/f/a/a/f;->a:Ljava/lang/Boolean;

    iput-object v0, p0, Lcom/google/c/f/a/a/f;->b:Ljava/lang/Boolean;

    iput-object v0, p0, Lcom/google/c/f/a/a/f;->c:Ljava/lang/Boolean;

    iput-object v0, p0, Lcom/google/c/f/a/a/f;->d:Ljava/lang/Boolean;

    iput-object v0, p0, Lcom/google/c/f/a/a/f;->e:Ljava/lang/Boolean;

    iput-object v0, p0, Lcom/google/c/f/a/a/f;->unknownFieldData:Lcom/google/protobuf/nano/f;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/c/f/a/a/f;->cachedSize:I

    .line 443
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 479
    invoke-super {p0}, Lcom/google/protobuf/nano/d;->computeSerializedSize()I

    move-result v0

    .line 480
    iget-object v1, p0, Lcom/google/c/f/a/a/f;->a:Ljava/lang/Boolean;

    if-eqz v1, :cond_0

    .line 481
    const/16 v1, 0x8

    iget-object v2, p0, Lcom/google/c/f/a/a/f;->a:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 484
    :cond_0
    iget-object v1, p0, Lcom/google/c/f/a/a/f;->b:Ljava/lang/Boolean;

    if-eqz v1, :cond_1

    .line 485
    const/16 v1, 0x9

    iget-object v2, p0, Lcom/google/c/f/a/a/f;->b:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 488
    :cond_1
    iget-object v1, p0, Lcom/google/c/f/a/a/f;->c:Ljava/lang/Boolean;

    if-eqz v1, :cond_2

    .line 489
    const/16 v1, 0x10

    iget-object v2, p0, Lcom/google/c/f/a/a/f;->c:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 492
    :cond_2
    iget-object v1, p0, Lcom/google/c/f/a/a/f;->d:Ljava/lang/Boolean;

    if-eqz v1, :cond_3

    .line 493
    const/16 v1, 0x16

    iget-object v2, p0, Lcom/google/c/f/a/a/f;->d:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 496
    :cond_3
    iget-object v1, p0, Lcom/google/c/f/a/a/f;->e:Ljava/lang/Boolean;

    if-eqz v1, :cond_4

    .line 497
    const/16 v1, 0x17

    iget-object v2, p0, Lcom/google/c/f/a/a/f;->e:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 500
    :cond_4
    return v0
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 409
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/c/f/a/a/f;->storeUnknownField(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/c/f/a/a/f;->a:Ljava/lang/Boolean;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/c/f/a/a/f;->b:Ljava/lang/Boolean;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/c/f/a/a/f;->c:Ljava/lang/Boolean;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/c/f/a/a/f;->d:Ljava/lang/Boolean;

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/c/f/a/a/f;->e:Ljava/lang/Boolean;

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x40 -> :sswitch_1
        0x48 -> :sswitch_2
        0x80 -> :sswitch_3
        0xb0 -> :sswitch_4
        0xb8 -> :sswitch_5
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 459
    iget-object v0, p0, Lcom/google/c/f/a/a/f;->a:Ljava/lang/Boolean;

    if-eqz v0, :cond_0

    .line 460
    const/16 v0, 0x8

    iget-object v1, p0, Lcom/google/c/f/a/a/f;->a:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 462
    :cond_0
    iget-object v0, p0, Lcom/google/c/f/a/a/f;->b:Ljava/lang/Boolean;

    if-eqz v0, :cond_1

    .line 463
    const/16 v0, 0x9

    iget-object v1, p0, Lcom/google/c/f/a/a/f;->b:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 465
    :cond_1
    iget-object v0, p0, Lcom/google/c/f/a/a/f;->c:Ljava/lang/Boolean;

    if-eqz v0, :cond_2

    .line 466
    const/16 v0, 0x10

    iget-object v1, p0, Lcom/google/c/f/a/a/f;->c:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 468
    :cond_2
    iget-object v0, p0, Lcom/google/c/f/a/a/f;->d:Ljava/lang/Boolean;

    if-eqz v0, :cond_3

    .line 469
    const/16 v0, 0x16

    iget-object v1, p0, Lcom/google/c/f/a/a/f;->d:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 471
    :cond_3
    iget-object v0, p0, Lcom/google/c/f/a/a/f;->e:Ljava/lang/Boolean;

    if-eqz v0, :cond_4

    .line 472
    const/16 v0, 0x17

    iget-object v1, p0, Lcom/google/c/f/a/a/f;->e:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 474
    :cond_4
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/d;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 475
    return-void
.end method
