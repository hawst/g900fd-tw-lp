.class public final Lcom/google/c/f/c/a/q;
.super Lcom/google/protobuf/nano/d;
.source "SourceFile"


# static fields
.field private static volatile e:[Lcom/google/c/f/c/a/q;


# instance fields
.field public a:Lcom/google/c/f/c/a/d;

.field public b:Ljava/lang/Integer;

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 2645
    invoke-direct {p0}, Lcom/google/protobuf/nano/d;-><init>()V

    .line 2646
    iput-object v0, p0, Lcom/google/c/f/c/a/q;->a:Lcom/google/c/f/c/a/d;

    iput-object v0, p0, Lcom/google/c/f/c/a/q;->b:Ljava/lang/Integer;

    iput-object v0, p0, Lcom/google/c/f/c/a/q;->c:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/c/f/c/a/q;->d:Ljava/lang/Boolean;

    iput-object v0, p0, Lcom/google/c/f/c/a/q;->unknownFieldData:Lcom/google/protobuf/nano/f;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/c/f/c/a/q;->cachedSize:I

    .line 2647
    return-void
.end method

.method public static a()[Lcom/google/c/f/c/a/q;
    .locals 2

    .prologue
    .line 2622
    sget-object v0, Lcom/google/c/f/c/a/q;->e:[Lcom/google/c/f/c/a/q;

    if-nez v0, :cond_1

    .line 2623
    sget-object v1, Lcom/google/protobuf/nano/h;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 2625
    :try_start_0
    sget-object v0, Lcom/google/c/f/c/a/q;->e:[Lcom/google/c/f/c/a/q;

    if-nez v0, :cond_0

    .line 2626
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/c/f/c/a/q;

    sput-object v0, Lcom/google/c/f/c/a/q;->e:[Lcom/google/c/f/c/a/q;

    .line 2628
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2630
    :cond_1
    sget-object v0, Lcom/google/c/f/c/a/q;->e:[Lcom/google/c/f/c/a/q;

    return-object v0

    .line 2628
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 2679
    invoke-super {p0}, Lcom/google/protobuf/nano/d;->computeSerializedSize()I

    move-result v0

    .line 2680
    iget-object v1, p0, Lcom/google/c/f/c/a/q;->b:Ljava/lang/Integer;

    if-eqz v1, :cond_0

    .line 2681
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/c/f/c/a/q;->b:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 2684
    :cond_0
    iget-object v1, p0, Lcom/google/c/f/c/a/q;->c:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 2685
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/c/f/c/a/q;->c:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2688
    :cond_1
    iget-object v1, p0, Lcom/google/c/f/c/a/q;->a:Lcom/google/c/f/c/a/d;

    if-eqz v1, :cond_2

    .line 2689
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/c/f/c/a/q;->a:Lcom/google/c/f/c/a/d;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2692
    :cond_2
    iget-object v1, p0, Lcom/google/c/f/c/a/q;->d:Ljava/lang/Boolean;

    if-eqz v1, :cond_3

    .line 2693
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/c/f/c/a/q;->d:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 2696
    :cond_3
    return v0
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 2616
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/c/f/c/a/q;->storeUnknownField(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/c/f/c/a/q;->b:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/c/f/c/a/q;->c:Ljava/lang/String;

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Lcom/google/c/f/c/a/q;->a:Lcom/google/c/f/c/a/d;

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/c/f/c/a/d;

    invoke-direct {v0}, Lcom/google/c/f/c/a/d;-><init>()V

    iput-object v0, p0, Lcom/google/c/f/c/a/q;->a:Lcom/google/c/f/c/a/d;

    :cond_1
    iget-object v0, p0, Lcom/google/c/f/c/a/q;->a:Lcom/google/c/f/c/a/d;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/c/f/c/a/q;->d:Ljava/lang/Boolean;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 2662
    iget-object v0, p0, Lcom/google/c/f/c/a/q;->b:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 2663
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/c/f/c/a/q;->b:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 2665
    :cond_0
    iget-object v0, p0, Lcom/google/c/f/c/a/q;->c:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 2666
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/c/f/c/a/q;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 2668
    :cond_1
    iget-object v0, p0, Lcom/google/c/f/c/a/q;->a:Lcom/google/c/f/c/a/d;

    if-eqz v0, :cond_2

    .line 2669
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/c/f/c/a/q;->a:Lcom/google/c/f/c/a/d;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 2671
    :cond_2
    iget-object v0, p0, Lcom/google/c/f/c/a/q;->d:Ljava/lang/Boolean;

    if-eqz v0, :cond_3

    .line 2672
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/c/f/c/a/q;->d:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 2674
    :cond_3
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/d;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 2675
    return-void
.end method
