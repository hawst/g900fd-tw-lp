.class public final Lcom/google/c/f/c/a/n;
.super Lcom/google/protobuf/nano/d;
.source "SourceFile"


# static fields
.field private static volatile e:[Lcom/google/c/f/c/a/n;


# instance fields
.field public a:Ljava/lang/Integer;

.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1208
    invoke-direct {p0}, Lcom/google/protobuf/nano/d;-><init>()V

    .line 1209
    iput-object v0, p0, Lcom/google/c/f/c/a/n;->a:Ljava/lang/Integer;

    iput-object v0, p0, Lcom/google/c/f/c/a/n;->b:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/c/f/c/a/n;->c:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/c/f/c/a/n;->d:Ljava/lang/Boolean;

    iput-object v0, p0, Lcom/google/c/f/c/a/n;->unknownFieldData:Lcom/google/protobuf/nano/f;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/c/f/c/a/n;->cachedSize:I

    .line 1210
    return-void
.end method

.method public static a()[Lcom/google/c/f/c/a/n;
    .locals 2

    .prologue
    .line 1185
    sget-object v0, Lcom/google/c/f/c/a/n;->e:[Lcom/google/c/f/c/a/n;

    if-nez v0, :cond_1

    .line 1186
    sget-object v1, Lcom/google/protobuf/nano/h;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 1188
    :try_start_0
    sget-object v0, Lcom/google/c/f/c/a/n;->e:[Lcom/google/c/f/c/a/n;

    if-nez v0, :cond_0

    .line 1189
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/c/f/c/a/n;

    sput-object v0, Lcom/google/c/f/c/a/n;->e:[Lcom/google/c/f/c/a/n;

    .line 1191
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1193
    :cond_1
    sget-object v0, Lcom/google/c/f/c/a/n;->e:[Lcom/google/c/f/c/a/n;

    return-object v0

    .line 1191
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 1240
    invoke-super {p0}, Lcom/google/protobuf/nano/d;->computeSerializedSize()I

    move-result v0

    .line 1241
    iget-object v1, p0, Lcom/google/c/f/c/a/n;->a:Ljava/lang/Integer;

    if-eqz v1, :cond_0

    .line 1242
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/c/f/c/a/n;->a:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1245
    :cond_0
    iget-object v1, p0, Lcom/google/c/f/c/a/n;->b:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 1246
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/c/f/c/a/n;->b:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1249
    :cond_1
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/c/f/c/a/n;->c:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1251
    iget-object v1, p0, Lcom/google/c/f/c/a/n;->d:Ljava/lang/Boolean;

    if-eqz v1, :cond_2

    .line 1252
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/c/f/c/a/n;->d:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 1255
    :cond_2
    return v0
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 1179
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/c/f/c/a/n;->storeUnknownField(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/c/f/c/a/n;->a:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/c/f/c/a/n;->b:Ljava/lang/String;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/c/f/c/a/n;->c:Ljava/lang/String;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/c/f/c/a/n;->d:Ljava/lang/Boolean;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 1225
    iget-object v0, p0, Lcom/google/c/f/c/a/n;->a:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 1226
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/c/f/c/a/n;->a:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 1228
    :cond_0
    iget-object v0, p0, Lcom/google/c/f/c/a/n;->b:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 1229
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/c/f/c/a/n;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 1231
    :cond_1
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/c/f/c/a/n;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 1232
    iget-object v0, p0, Lcom/google/c/f/c/a/n;->d:Ljava/lang/Boolean;

    if-eqz v0, :cond_2

    .line 1233
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/c/f/c/a/n;->d:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 1235
    :cond_2
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/d;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 1236
    return-void
.end method
