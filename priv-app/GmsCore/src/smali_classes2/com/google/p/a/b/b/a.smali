.class public final Lcom/google/p/a/b/b/a;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static final a:Ljava/lang/Boolean;

.field public static final b:Ljava/lang/Boolean;

.field public static final c:[B

.field private static final h:Lcom/google/p/a/b/b/b;


# instance fields
.field private d:Lcom/google/p/a/b/b/c;

.field private final e:Lcom/google/p/a/e/d;

.field private f:Lcom/google/p/a/e/d;

.field private g:I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 57
    new-instance v0, Ljava/lang/Boolean;

    invoke-direct {v0, v2}, Ljava/lang/Boolean;-><init>(Z)V

    sput-object v0, Lcom/google/p/a/b/b/a;->a:Ljava/lang/Boolean;

    .line 58
    new-instance v0, Ljava/lang/Boolean;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/lang/Boolean;-><init>(Z)V

    sput-object v0, Lcom/google/p/a/b/b/a;->b:Ljava/lang/Boolean;

    .line 59
    new-array v0, v2, [B

    sput-object v0, Lcom/google/p/a/b/b/a;->c:[B

    .line 437
    new-instance v0, Lcom/google/p/a/b/b/b;

    invoke-direct {v0, v2}, Lcom/google/p/a/b/b/b;-><init>(B)V

    sput-object v0, Lcom/google/p/a/b/b/a;->h:Lcom/google/p/a/b/b/b;

    return-void
.end method

.method public constructor <init>(Lcom/google/p/a/b/b/c;)V
    .locals 1

    .prologue
    .line 103
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 96
    const/high16 v0, -0x80000000

    iput v0, p0, Lcom/google/p/a/b/b/a;->g:I

    .line 104
    iput-object p1, p0, Lcom/google/p/a/b/b/a;->d:Lcom/google/p/a/b/b/c;

    .line 106
    if-nez p1, :cond_0

    .line 107
    new-instance v0, Lcom/google/p/a/e/d;

    invoke-direct {v0}, Lcom/google/p/a/e/d;-><init>()V

    iput-object v0, p0, Lcom/google/p/a/b/b/a;->e:Lcom/google/p/a/e/d;

    .line 111
    :goto_0
    return-void

    .line 109
    :cond_0
    invoke-virtual {p1}, Lcom/google/p/a/b/b/c;->a()Lcom/google/p/a/e/d;

    move-result-object v0

    iput-object v0, p0, Lcom/google/p/a/b/b/a;->e:Lcom/google/p/a/e/d;

    goto :goto_0
.end method

.method public constructor <init>(Lcom/google/p/a/b/b/c;I)V
    .locals 2

    .prologue
    .line 117
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 96
    const/high16 v0, -0x80000000

    iput v0, p0, Lcom/google/p/a/b/b/a;->g:I

    .line 118
    iput-object p1, p0, Lcom/google/p/a/b/b/a;->d:Lcom/google/p/a/b/b/c;

    .line 121
    new-instance v0, Lcom/google/p/a/e/d;

    add-int/lit8 v1, p2, 0x1

    invoke-direct {v0, v1}, Lcom/google/p/a/e/d;-><init>(I)V

    iput-object v0, p0, Lcom/google/p/a/b/b/a;->e:Lcom/google/p/a/e/d;

    .line 122
    return-void
.end method

.method private static a(J)I
    .locals 4

    .prologue
    .line 767
    const-wide/16 v0, 0x0

    cmp-long v0, p0, v0

    if-gez v0, :cond_1

    .line 768
    const/16 v0, 0xa

    .line 775
    :cond_0
    return v0

    .line 770
    :cond_1
    const/4 v0, 0x1

    .line 771
    :goto_0
    const-wide/16 v2, 0x80

    cmp-long v1, p0, v2

    if-ltz v1, :cond_0

    .line 772
    add-int/lit8 v0, v0, 0x1

    .line 773
    const/4 v1, 0x7

    shr-long/2addr p0, v1

    goto :goto_0
.end method

.method private a(Lcom/google/p/a/b/j;)I
    .locals 21

    .prologue
    .line 845
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/p/a/b/b/a;->e:Lcom/google/p/a/e/d;

    invoke-virtual {v2}, Lcom/google/p/a/e/d;->a()Lcom/google/p/a/e/e;

    move-result-object v11

    .line 846
    const/4 v2, 0x0

    move v3, v2

    .line 847
    :goto_0
    invoke-virtual {v11}, Lcom/google/p/a/e/e;->a()Z

    move-result v2

    if-eqz v2, :cond_7

    .line 848
    invoke-virtual {v11}, Lcom/google/p/a/e/e;->b()I

    move-result v12

    .line 849
    move-object/from16 v0, p0

    invoke-virtual {v0, v12}, Lcom/google/p/a/b/b/a;->k(I)I

    move-result v13

    move-object/from16 v0, p0

    invoke-direct {v0, v12}, Lcom/google/p/a/b/b/a;->o(I)I

    move-result v14

    shl-int/lit8 v2, v12, 0x3

    or-int v15, v2, v14

    const/4 v4, 0x0

    const/4 v2, 0x0

    move v10, v2

    move v2, v4

    :goto_1
    if-ge v10, v13, :cond_6

    int-to-long v4, v15

    move-object/from16 v0, p1

    invoke-static {v0, v4, v5}, Lcom/google/p/a/b/b/a;->a(Ljava/io/OutputStream;J)I

    move-result v4

    add-int v7, v2, v4

    const/4 v6, 0x0

    invoke-virtual/range {p1 .. p1}, Lcom/google/p/a/b/j;->b()I

    move-result v16

    packed-switch v14, :pswitch_data_0

    :pswitch_0
    new-instance v2, Ljava/lang/IllegalArgumentException;

    invoke-direct {v2}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v2

    :pswitch_1
    const/16 v2, 0x13

    move-object/from16 v0, p0

    invoke-direct {v0, v12, v10, v2}, Lcom/google/p/a/b/b/a;->a(III)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    const/4 v2, 0x5

    if-ne v14, v2, :cond_0

    const/4 v2, 0x4

    :goto_2
    const/4 v4, 0x0

    :goto_3
    if-ge v4, v2, :cond_1

    const-wide/16 v18, 0xff

    and-long v18, v18, v8

    move-wide/from16 v0, v18

    long-to-int v5, v0

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Lcom/google/p/a/b/j;->write(I)V

    const/16 v5, 0x8

    shr-long/2addr v8, v5

    add-int/lit8 v4, v4, 0x1

    goto :goto_3

    :cond_0
    const/16 v2, 0x8

    goto :goto_2

    :cond_1
    move v4, v6

    move v2, v7

    :goto_4
    if-nez v4, :cond_2

    invoke-virtual/range {p1 .. p1}, Lcom/google/p/a/b/j;->b()I

    move-result v4

    sub-int v4, v4, v16

    add-int/2addr v2, v4

    :cond_2
    add-int/lit8 v4, v10, 0x1

    move v10, v4

    goto :goto_1

    :pswitch_2
    const/16 v2, 0x13

    move-object/from16 v0, p0

    invoke-direct {v0, v12, v10, v2}, Lcom/google/p/a/b/b/a;->a(III)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    move-object/from16 v0, p0

    invoke-direct {v0, v12}, Lcom/google/p/a/b/b/a;->m(I)Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-static {v4, v5}, Lcom/google/p/a/b/b/a;->b(J)J

    move-result-wide v4

    :cond_3
    move-object/from16 v0, p1

    invoke-static {v0, v4, v5}, Lcom/google/p/a/b/b/a;->a(Ljava/io/OutputStream;J)I

    move v4, v6

    move v2, v7

    goto :goto_4

    :pswitch_3
    move-object/from16 v0, p0

    invoke-direct {v0, v12}, Lcom/google/p/a/b/b/a;->l(I)I

    move-result v2

    const/16 v4, 0x1b

    if-ne v2, v4, :cond_4

    const/16 v2, 0x10

    :goto_5
    move-object/from16 v0, p0

    invoke-direct {v0, v12, v10, v2}, Lcom/google/p/a/b/b/a;->a(III)Ljava/lang/Object;

    move-result-object v2

    instance-of v4, v2, [B

    if-eqz v4, :cond_5

    check-cast v2, [B

    array-length v4, v2

    int-to-long v4, v4

    move-object/from16 v0, p1

    invoke-static {v0, v4, v5}, Lcom/google/p/a/b/b/a;->a(Ljava/io/OutputStream;J)I

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Lcom/google/p/a/b/j;->write([B)V

    move v4, v6

    move v2, v7

    goto :goto_4

    :cond_4
    const/16 v2, 0x19

    goto :goto_5

    :cond_5
    invoke-virtual/range {p1 .. p1}, Lcom/google/p/a/b/j;->b()I

    move-result v4

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Lcom/google/p/a/b/j;->b(I)V

    invoke-virtual/range {p1 .. p1}, Lcom/google/p/a/b/j;->a()I

    move-result v4

    const/4 v5, -0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Lcom/google/p/a/b/j;->b(I)V

    check-cast v2, Lcom/google/p/a/b/b/a;

    move-object/from16 v0, p1

    invoke-direct {v2, v0}, Lcom/google/p/a/b/b/a;->a(Lcom/google/p/a/b/j;)I

    move-result v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v2}, Lcom/google/p/a/b/j;->a(II)V

    int-to-long v4, v2

    invoke-static {v4, v5}, Lcom/google/p/a/b/b/a;->a(J)I

    move-result v4

    add-int/2addr v2, v4

    add-int v4, v7, v2

    const/4 v2, 0x1

    move/from16 v20, v2

    move v2, v4

    move/from16 v4, v20

    goto/16 :goto_4

    :pswitch_4
    const/16 v2, 0x1a

    move-object/from16 v0, p0

    invoke-direct {v0, v12, v10, v2}, Lcom/google/p/a/b/b/a;->a(III)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/p/a/b/b/a;

    move-object/from16 v0, p1

    invoke-direct {v2, v0}, Lcom/google/p/a/b/b/a;->a(Lcom/google/p/a/b/j;)I

    move-result v2

    add-int/2addr v2, v7

    shl-int/lit8 v4, v12, 0x3

    or-int/lit8 v4, v4, 0x4

    int-to-long v4, v4

    move-object/from16 v0, p1

    invoke-static {v0, v4, v5}, Lcom/google/p/a/b/b/a;->a(Ljava/io/OutputStream;J)I

    move-result v4

    add-int/2addr v4, v2

    const/4 v2, 0x1

    move/from16 v20, v2

    move v2, v4

    move/from16 v4, v20

    goto/16 :goto_4

    :cond_6
    add-int/2addr v2, v3

    move v3, v2

    .line 850
    goto/16 :goto_0

    .line 851
    :cond_7
    return v3

    .line 849
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_1
        :pswitch_3
        :pswitch_4
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private a(Ljava/io/InputStream;I)I
    .locals 3

    .prologue
    .line 452
    const/4 v0, 0x1

    new-instance v1, Lcom/google/p/a/b/b/b;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lcom/google/p/a/b/b/b;-><init>(B)V

    invoke-direct {p0, p1, p2, v0, v1}, Lcom/google/p/a/b/b/a;->a(Ljava/io/InputStream;IZLcom/google/p/a/b/b/b;)I

    move-result v0

    return v0
.end method

.method private a(Ljava/io/InputStream;IZLcom/google/p/a/b/b/b;)I
    .locals 11

    .prologue
    .line 471
    if-eqz p3, :cond_2

    .line 472
    iget-object v1, p0, Lcom/google/p/a/b/b/a;->e:Lcom/google/p/a/e/d;

    const/4 v0, 0x0

    :goto_0
    iget-object v2, v1, Lcom/google/p/a/e/d;->a:[Ljava/lang/Object;

    array-length v2, v2

    if-ge v0, v2, :cond_0

    iget-object v2, v1, Lcom/google/p/a/e/d;->a:[Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object v3, v2, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    iget-object v0, v1, Lcom/google/p/a/e/d;->b:Ljava/util/HashMap;

    if-eqz v0, :cond_1

    iget-object v0, v1, Lcom/google/p/a/e/d;->b:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    :cond_1
    const/high16 v0, -0x80000000

    iput v0, v1, Lcom/google/p/a/e/d;->d:I

    const/high16 v0, -0x80000000

    iput v0, v1, Lcom/google/p/a/e/d;->c:I

    const/4 v0, 0x0

    iput v0, v1, Lcom/google/p/a/e/d;->e:I

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/p/a/b/b/a;->f:Lcom/google/p/a/e/d;

    :cond_2
    move v0, p2

    .line 474
    :goto_1
    if-lez v0, :cond_10

    .line 475
    const/4 v1, 0x1

    invoke-static {p1, v1, p4}, Lcom/google/p/a/b/b/a;->a(Ljava/io/InputStream;ZLcom/google/p/a/b/b/b;)J

    move-result-wide v4

    .line 477
    const-wide/16 v2, -0x1

    cmp-long v1, v4, v2

    if-eqz v1, :cond_10

    .line 478
    iget v1, p4, Lcom/google/p/a/b/b/b;->a:I

    sub-int v2, v0, v1

    .line 481
    long-to-int v0, v4

    and-int/lit8 v0, v0, 0x7

    .line 482
    const/4 v1, 0x4

    if-eq v0, v1, :cond_c

    .line 483
    const/4 v1, 0x3

    ushr-long/2addr v4, v1

    long-to-int v6, v4

    .line 487
    invoke-direct {p0, v6}, Lcom/google/p/a/b/b/a;->l(I)I

    move-result v1

    .line 488
    const/16 v3, 0x10

    if-ne v1, v3, :cond_4

    .line 489
    iget-object v1, p0, Lcom/google/p/a/b/b/a;->f:Lcom/google/p/a/e/d;

    if-nez v1, :cond_3

    .line 490
    new-instance v1, Lcom/google/p/a/e/d;

    invoke-direct {v1}, Lcom/google/p/a/e/d;-><init>()V

    iput-object v1, p0, Lcom/google/p/a/b/b/a;->f:Lcom/google/p/a/e/d;

    .line 492
    :cond_3
    iget-object v1, p0, Lcom/google/p/a/b/b/a;->f:Lcom/google/p/a/e/d;

    invoke-static {v0}, Lcom/google/p/a/e/h;->a(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v6, v3}, Lcom/google/p/a/e/d;->a(ILjava/lang/Object;)V

    .line 493
    :cond_4
    packed-switch v0, :pswitch_data_0

    .line 552
    :pswitch_0
    new-instance v1, Ljava/io/IOException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unknown wire type "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", reading garbage data?"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 500
    :pswitch_1
    const/4 v0, 0x0

    invoke-static {p1, v0, p4}, Lcom/google/p/a/b/b/a;->a(Ljava/io/InputStream;ZLcom/google/p/a/b/b/b;)J

    move-result-wide v0

    .line 501
    iget v3, p4, Lcom/google/p/a/b/b/b;->a:I

    sub-int/2addr v2, v3

    .line 502
    invoke-direct {p0, v6}, Lcom/google/p/a/b/b/a;->m(I)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 503
    const/4 v3, 0x1

    ushr-long v4, v0, v3

    const-wide/16 v8, 0x1

    and-long/2addr v0, v8

    neg-long v0, v0

    xor-long/2addr v0, v4

    .line 505
    :cond_5
    invoke-static {v0, v1}, Lcom/google/p/a/e/h;->a(J)Ljava/lang/Long;

    move-result-object v0

    move v1, v2

    .line 555
    :cond_6
    :goto_2
    invoke-direct {p0, v6, v0}, Lcom/google/p/a/b/b/a;->a(ILjava/lang/Object;)V

    move v0, v1

    .line 556
    goto :goto_1

    .line 511
    :pswitch_2
    const-wide/16 v4, 0x0

    .line 512
    const/4 v3, 0x0

    .line 513
    const/4 v1, 0x5

    if-ne v0, v1, :cond_7

    const/4 v0, 0x4

    .line 514
    :goto_3
    sub-int v1, v2, v0

    .line 516
    :goto_4
    add-int/lit8 v2, v0, -0x1

    if-lez v0, :cond_8

    .line 517
    invoke-virtual {p1}, Ljava/io/InputStream;->read()I

    move-result v0

    int-to-long v8, v0

    .line 518
    shl-long/2addr v8, v3

    or-long/2addr v4, v8

    .line 519
    add-int/lit8 v0, v3, 0x8

    move v3, v0

    move v0, v2

    .line 520
    goto :goto_4

    .line 513
    :cond_7
    const/16 v0, 0x8

    goto :goto_3

    .line 522
    :cond_8
    invoke-static {v4, v5}, Lcom/google/p/a/e/h;->a(J)Ljava/lang/Long;

    move-result-object v0

    goto :goto_2

    .line 526
    :pswitch_3
    const/4 v0, 0x0

    invoke-static {p1, v0, p4}, Lcom/google/p/a/b/b/a;->a(Ljava/io/InputStream;ZLcom/google/p/a/b/b/b;)J

    move-result-wide v0

    long-to-int v3, v0

    .line 527
    iget v0, p4, Lcom/google/p/a/b/b/b;->a:I

    sub-int v0, v2, v0

    .line 528
    sub-int v1, v0, v3

    .line 530
    if-nez v3, :cond_9

    sget-object v0, Lcom/google/p/a/b/b/a;->c:[B

    .line 531
    :goto_5
    const/4 v2, 0x0

    .line 532
    :goto_6
    if-ge v2, v3, :cond_6

    .line 533
    sub-int v4, v3, v2

    invoke-virtual {p1, v0, v2, v4}, Ljava/io/InputStream;->read([BII)I

    move-result v4

    .line 534
    if-gtz v4, :cond_a

    .line 535
    new-instance v0, Ljava/io/IOException;

    const-string v1, "Unexp.EOF"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 530
    :cond_9
    new-array v0, v3, [B

    goto :goto_5

    .line 537
    :cond_a
    add-int/2addr v2, v4

    goto :goto_6

    .line 543
    :pswitch_4
    new-instance v1, Lcom/google/p/a/b/b/a;

    iget-object v0, p0, Lcom/google/p/a/b/b/a;->d:Lcom/google/p/a/b/b/c;

    if-nez v0, :cond_b

    const/4 v0, 0x0

    :goto_7
    invoke-direct {v1, v0}, Lcom/google/p/a/b/b/a;-><init>(Lcom/google/p/a/b/b/c;)V

    .line 547
    const/4 v0, 0x0

    invoke-direct {v1, p1, v2, v0, p4}, Lcom/google/p/a/b/b/a;->a(Ljava/io/InputStream;IZLcom/google/p/a/b/b/b;)I

    move-result v0

    move-object v10, v1

    move v1, v0

    move-object v0, v10

    .line 549
    goto :goto_2

    .line 543
    :cond_b
    iget-object v0, p0, Lcom/google/p/a/b/b/a;->d:Lcom/google/p/a/b/b/c;

    invoke-virtual {v0, v6}, Lcom/google/p/a/b/b/c;->b(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/p/a/b/b/c;

    goto :goto_7

    :cond_c
    move v1, v2

    .line 558
    :goto_8
    if-gez v1, :cond_d

    .line 559
    new-instance v0, Ljava/io/IOException;

    invoke-direct {v0}, Ljava/io/IOException;-><init>()V

    throw v0

    .line 563
    :cond_d
    iget-object v0, p0, Lcom/google/p/a/b/b/a;->f:Lcom/google/p/a/e/d;

    if-eqz v0, :cond_e

    .line 564
    iget-object v2, p0, Lcom/google/p/a/b/b/a;->f:Lcom/google/p/a/e/d;

    iget v0, v2, Lcom/google/p/a/e/d;->c:I

    if-gtz v0, :cond_f

    const/4 v0, 0x1

    :goto_9
    new-array v3, v0, [Ljava/lang/Object;

    iget-object v4, v2, Lcom/google/p/a/e/d;->a:[Ljava/lang/Object;

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-static {v4, v5, v3, v6, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v0, v2, Lcom/google/p/a/e/d;->a:[Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {v0, v4}, Ljava/util/Arrays;->fill([Ljava/lang/Object;Ljava/lang/Object;)V

    iput-object v3, v2, Lcom/google/p/a/e/d;->a:[Ljava/lang/Object;

    .line 567
    :cond_e
    return v1

    .line 564
    :cond_f
    iget v0, v2, Lcom/google/p/a/e/d;->c:I

    add-int/lit8 v0, v0, 0x1

    goto :goto_9

    :cond_10
    move v1, v0

    goto :goto_8

    .line 493
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method private static a(Ljava/io/OutputStream;J)I
    .locals 5

    .prologue
    .line 1785
    const/4 v0, 0x0

    :goto_0
    const/16 v1, 0xa

    if-ge v0, v1, :cond_0

    .line 1787
    const-wide/16 v2, 0x7f

    and-long/2addr v2, p1

    long-to-int v1, v2

    .line 1789
    const/4 v2, 0x7

    ushr-long/2addr p1, v2

    .line 1791
    const-wide/16 v2, 0x0

    cmp-long v2, p1, v2

    if-nez v2, :cond_1

    .line 1792
    invoke-virtual {p0, v1}, Ljava/io/OutputStream;->write(I)V

    .line 1793
    add-int/lit8 v0, v0, 0x1

    .line 1798
    :cond_0
    return v0

    .line 1795
    :cond_1
    or-int/lit16 v1, v1, 0x80

    invoke-virtual {p0, v1}, Ljava/io/OutputStream;->write(I)V

    .line 1785
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method private static a(Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 587
    if-nez p0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    instance-of v0, p0, Ljava/util/Vector;

    if-eqz v0, :cond_1

    check-cast p0, Ljava/util/Vector;

    invoke-virtual {p0}, Ljava/util/Vector;->size()I

    move-result v0

    goto :goto_0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private a(Z)I
    .locals 10

    .prologue
    const/4 v1, 0x0

    .line 656
    iget v0, p0, Lcom/google/p/a/b/b/a;->g:I

    const/high16 v2, -0x80000000

    if-eq v0, v2, :cond_0

    if-eqz p1, :cond_0

    .line 657
    iget v0, p0, Lcom/google/p/a/b/b/a;->g:I

    .line 670
    :goto_0
    return v0

    .line 660
    :cond_0
    iget-object v0, p0, Lcom/google/p/a/b/b/a;->e:Lcom/google/p/a/e/d;

    invoke-virtual {v0}, Lcom/google/p/a/e/d;->a()Lcom/google/p/a/e/e;

    move-result-object v6

    move v0, v1

    .line 661
    :goto_1
    invoke-virtual {v6}, Lcom/google/p/a/e/e;->a()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 662
    invoke-virtual {v6}, Lcom/google/p/a/e/e;->b()I

    move-result v7

    .line 663
    invoke-virtual {p0, v7}, Lcom/google/p/a/b/b/a;->k(I)I

    move-result v8

    move v3, v1

    move v2, v0

    .line 664
    :goto_2
    if-ge v3, v8, :cond_4

    .line 665
    shl-int/lit8 v0, v7, 0x3

    int-to-long v4, v0

    invoke-static {v4, v5}, Lcom/google/p/a/b/b/a;->a(J)I

    move-result v9

    invoke-direct {p0, v7}, Lcom/google/p/a/b/b/a;->o(I)I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :pswitch_0
    const/16 v0, 0x10

    invoke-direct {p0, v7, v3, v0}, Lcom/google/p/a/b/b/a;->a(III)Ljava/lang/Object;

    move-result-object v0

    instance-of v4, v0, [B

    if-eqz v4, :cond_2

    check-cast v0, [B

    array-length v0, v0

    :goto_3
    int-to-long v4, v0

    invoke-static {v4, v5}, Lcom/google/p/a/b/b/a;->a(J)I

    move-result v4

    add-int/2addr v4, v9

    add-int/2addr v0, v4

    :goto_4
    add-int/2addr v2, v0

    .line 664
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_2

    .line 665
    :pswitch_1
    add-int/lit8 v0, v9, 0x4

    goto :goto_4

    :pswitch_2
    add-int/lit8 v0, v9, 0x8

    goto :goto_4

    :pswitch_3
    invoke-virtual {p0, v7, v3}, Lcom/google/p/a/b/b/a;->c(II)J

    move-result-wide v4

    invoke-direct {p0, v7}, Lcom/google/p/a/b/b/a;->m(I)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-static {v4, v5}, Lcom/google/p/a/b/b/a;->b(J)J

    move-result-wide v4

    :cond_1
    invoke-static {v4, v5}, Lcom/google/p/a/b/b/a;->a(J)I

    move-result v0

    add-int/2addr v0, v9

    goto :goto_4

    :pswitch_4
    invoke-virtual {p0, v7, v3}, Lcom/google/p/a/b/b/a;->d(II)Lcom/google/p/a/b/b/a;

    move-result-object v0

    invoke-direct {v0, p1}, Lcom/google/p/a/b/b/a;->a(Z)I

    move-result v0

    add-int/2addr v0, v9

    add-int/2addr v0, v9

    goto :goto_4

    :cond_2
    instance-of v4, v0, Ljava/lang/String;

    if-eqz v4, :cond_3

    check-cast v0, Ljava/lang/String;

    const/4 v4, 0x0

    invoke-static {v0, v4, v1}, Lcom/google/p/a/b/o;->a(Ljava/lang/String;[BI)I

    move-result v0

    goto :goto_3

    :cond_3
    check-cast v0, Lcom/google/p/a/b/b/a;

    invoke-direct {v0, p1}, Lcom/google/p/a/b/b/a;->a(Z)I

    move-result v0

    goto :goto_3

    :cond_4
    move v0, v2

    .line 667
    goto :goto_1

    .line 668
    :cond_5
    iput v0, p0, Lcom/google/p/a/b/b/a;->g:I

    .line 670
    iget v0, p0, Lcom/google/p/a/b/b/a;->g:I

    goto :goto_0

    .line 665
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_3
        :pswitch_2
        :pswitch_0
        :pswitch_4
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private static a(Ljava/io/InputStream;ZLcom/google/p/a/b/b/b;)J
    .locals 10

    .prologue
    const/4 v0, 0x0

    .line 1669
    const-wide/16 v2, 0x0

    .line 1672
    iput v0, p2, Lcom/google/p/a/b/b/b;->a:I

    move v8, v0

    move v9, v0

    move-wide v0, v2

    move v2, v8

    move v3, v9

    .line 1676
    :goto_0
    const/16 v4, 0xa

    if-ge v2, v4, :cond_2

    .line 1677
    invoke-virtual {p0}, Ljava/io/InputStream;->read()I

    move-result v4

    .line 1679
    const/4 v5, -0x1

    if-ne v4, v5, :cond_1

    .line 1680
    if-nez v2, :cond_0

    if-eqz p1, :cond_0

    .line 1681
    const-wide/16 v0, -0x1

    .line 1695
    :goto_1
    return-wide v0

    .line 1683
    :cond_0
    new-instance v0, Ljava/io/IOException;

    const-string v1, "EOF"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1686
    :cond_1
    and-int/lit8 v5, v4, 0x7f

    int-to-long v6, v5

    shl-long/2addr v6, v3

    or-long/2addr v0, v6

    .line 1688
    and-int/lit16 v4, v4, 0x80

    if-eqz v4, :cond_2

    .line 1689
    add-int/lit8 v3, v3, 0x7

    .line 1676
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 1694
    :cond_2
    add-int/lit8 v2, v2, 0x1

    iput v2, p2, Lcom/google/p/a/b/b/b;->a:I

    goto :goto_1
.end method

.method private a(III)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 1433
    iget-object v0, p0, Lcom/google/p/a/b/b/a;->e:Lcom/google/p/a/e/d;

    invoke-virtual {v0, p1}, Lcom/google/p/a/e/d;->a(I)Ljava/lang/Object;

    move-result-object v0

    .line 1435
    invoke-static {v0}, Lcom/google/p/a/b/b/a;->a(Ljava/lang/Object;)I

    move-result v1

    .line 1437
    if-lt p2, v1, :cond_0

    .line 1438
    new-instance v0, Ljava/lang/ArrayIndexOutOfBoundsException;

    invoke-direct {v0}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>()V

    throw v0

    .line 1440
    :cond_0
    invoke-direct {p0, p1, p2, p3, v0}, Lcom/google/p/a/b/b/a;->a(IIILjava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method private a(IIILjava/lang/Object;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 1449
    const/4 v0, 0x0

    .line 1450
    instance-of v1, p4, Ljava/util/Vector;

    if-eqz v1, :cond_2

    .line 1451
    check-cast p4, Ljava/util/Vector;

    .line 1452
    invoke-virtual {p4, p2}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    .line 1455
    :goto_0
    invoke-direct {p0, v0, p3, p1}, Lcom/google/p/a/b/b/a;->a(Ljava/lang/Object;II)Ljava/lang/Object;

    move-result-object v1

    .line 1456
    if-eq v1, v0, :cond_0

    if-eqz v0, :cond_0

    .line 1457
    if-nez p4, :cond_1

    .line 1458
    invoke-direct {p0, p1, v1}, Lcom/google/p/a/b/b/a;->b(ILjava/lang/Object;)Lcom/google/p/a/b/b/a;

    .line 1463
    :cond_0
    :goto_1
    return-object v1

    .line 1460
    :cond_1
    invoke-virtual {p4, v1, p2}, Ljava/util/Vector;->setElementAt(Ljava/lang/Object;I)V

    goto :goto_1

    :cond_2
    move-object v2, v0

    move-object v0, p4

    move-object p4, v2

    goto :goto_0
.end method

.method private a(Ljava/lang/Object;II)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 1561
    packed-switch p2, :pswitch_data_0

    .line 1632
    :pswitch_0
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Unsupp.Type"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1566
    :pswitch_1
    instance-of v0, p1, Ljava/lang/Boolean;

    if-eqz v0, :cond_1

    .line 1629
    :cond_0
    :goto_0
    :pswitch_2
    return-object p1

    .line 1569
    :cond_1
    check-cast p1, Ljava/lang/Long;

    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    long-to-int v0, v0

    packed-switch v0, :pswitch_data_1

    .line 1575
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Type mismatch"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1571
    :pswitch_3
    sget-object p1, Lcom/google/p/a/b/b/a;->a:Ljava/lang/Boolean;

    goto :goto_0

    .line 1573
    :pswitch_4
    sget-object p1, Lcom/google/p/a/b/b/a;->b:Ljava/lang/Boolean;

    goto :goto_0

    .line 1588
    :pswitch_5
    instance-of v0, p1, Ljava/lang/Boolean;

    if-eqz v0, :cond_0

    .line 1589
    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_2

    const-wide/16 v0, 0x1

    :goto_1
    invoke-static {v0, v1}, Lcom/google/p/a/e/h;->a(J)Ljava/lang/Long;

    move-result-object p1

    goto :goto_0

    :cond_2
    const-wide/16 v0, 0x0

    goto :goto_1

    .line 1594
    :pswitch_6
    instance-of v0, p1, Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 1595
    check-cast p1, Ljava/lang/String;

    invoke-static {p1}, Lcom/google/p/a/b/o;->a(Ljava/lang/String;)[B

    move-result-object p1

    goto :goto_0

    .line 1596
    :cond_3
    instance-of v0, p1, Lcom/google/p/a/b/b/a;

    if-eqz v0, :cond_0

    .line 1597
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 1599
    :try_start_0
    check-cast p1, Lcom/google/p/a/b/b/a;

    invoke-virtual {p1, v0}, Lcom/google/p/a/b/b/a;->a(Ljava/io/OutputStream;)V

    .line 1600
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object p1

    goto :goto_0

    .line 1601
    :catch_0
    move-exception v0

    .line 1602
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-virtual {v0}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 1608
    :pswitch_7
    instance-of v0, p1, [B

    if-eqz v0, :cond_0

    .line 1609
    check-cast p1, [B

    .line 1610
    array-length v0, p1

    invoke-static {p1, v0}, Lcom/google/p/a/b/o;->a([BI)Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    .line 1615
    :pswitch_8
    instance-of v0, p1, [B

    if-eqz v0, :cond_0

    .line 1618
    if-lez p3, :cond_4

    :try_start_1
    iget-object v0, p0, Lcom/google/p/a/b/b/a;->d:Lcom/google/p/a/b/b/c;

    if-eqz v0, :cond_4

    .line 1619
    new-instance v1, Lcom/google/p/a/b/b/a;

    iget-object v0, p0, Lcom/google/p/a/b/b/a;->d:Lcom/google/p/a/b/b/c;

    invoke-virtual {v0, p3}, Lcom/google/p/a/b/b/c;->b(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/p/a/b/b/c;

    invoke-direct {v1, v0}, Lcom/google/p/a/b/b/a;-><init>(Lcom/google/p/a/b/b/c;)V

    move-object v0, v1

    .line 1624
    :goto_2
    check-cast p1, [B

    invoke-virtual {v0, p1}, Lcom/google/p/a/b/b/a;->b([B)Lcom/google/p/a/b/b/a;

    move-result-object p1

    goto/16 :goto_0

    .line 1621
    :cond_4
    new-instance v0, Lcom/google/p/a/b/b/a;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/google/p/a/b/b/a;-><init>(Lcom/google/p/a/b/b/c;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_2

    .line 1625
    :catch_1
    move-exception v0

    .line 1626
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-virtual {v0}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 1561
    nop

    :pswitch_data_0
    .packed-switch 0x10
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_1
        :pswitch_6
        :pswitch_8
        :pswitch_8
        :pswitch_7
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch

    .line 1569
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method private a(ILjava/lang/Object;)V
    .locals 3

    .prologue
    .line 1553
    iget-object v0, p0, Lcom/google/p/a/b/b/a;->e:Lcom/google/p/a/e/d;

    invoke-virtual {v0, p1}, Lcom/google/p/a/e/d;->a(I)Ljava/lang/Object;

    move-result-object v1

    const/4 v0, 0x0

    instance-of v2, v1, Ljava/util/Vector;

    if-eqz v2, :cond_0

    move-object v0, v1

    check-cast v0, Ljava/util/Vector;

    :cond_0
    if-eqz v1, :cond_1

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v2

    if-nez v2, :cond_2

    :cond_1
    invoke-direct {p0, p1, p2}, Lcom/google/p/a/b/b/a;->b(ILjava/lang/Object;)Lcom/google/p/a/b/b/a;

    .line 1554
    :goto_0
    return-void

    .line 1553
    :cond_2
    if-nez v0, :cond_3

    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    invoke-virtual {v0, v1}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    iget-object v1, p0, Lcom/google/p/a/b/b/a;->e:Lcom/google/p/a/e/d;

    invoke-virtual {v1, p1, v0}, Lcom/google/p/a/e/d;->a(ILjava/lang/Object;)V

    :cond_3
    invoke-virtual {v0, p2}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    goto :goto_0
.end method

.method private a(Ljava/io/Writer;)V
    .locals 1

    .prologue
    .line 996
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/p/a/b/b/a;->a(Ljava/io/Writer;I)V

    .line 997
    return-void
.end method

.method private a(Ljava/io/Writer;I)V
    .locals 7

    .prologue
    const/16 v6, 0x20

    const/4 v1, 0x0

    .line 1000
    mul-int/lit8 v2, p2, 0x2

    new-instance v3, Ljava/lang/StringBuffer;

    invoke-direct {v3}, Ljava/lang/StringBuffer;-><init>()V

    move v0, v1

    :goto_0
    if-ge v0, v2, :cond_0

    invoke-virtual {v3, v6}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    invoke-virtual {v3}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v3

    move v2, v1

    .line 1001
    :goto_1
    iget-object v0, p0, Lcom/google/p/a/b/b/a;->e:Lcom/google/p/a/e/d;

    iget v0, v0, Lcom/google/p/a/e/d;->d:I

    if-gt v2, v0, :cond_3

    move v0, v1

    .line 1002
    :goto_2
    invoke-virtual {p0, v2}, Lcom/google/p/a/b/b/a;->k(I)I

    move-result v4

    if-ge v0, v4, :cond_2

    .line 1003
    invoke-virtual {p1, v3}, Ljava/io/Writer;->append(Ljava/lang/CharSequence;)Ljava/io/Writer;

    move-result-object v4

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/io/Writer;->append(Ljava/lang/CharSequence;)Ljava/io/Writer;

    invoke-direct {p0, v2}, Lcom/google/p/a/b/b/a;->l(I)I

    move-result v4

    const/16 v5, 0x1a

    if-ne v4, v5, :cond_1

    invoke-virtual {p1, v6}, Ljava/io/Writer;->append(C)Ljava/io/Writer;

    :goto_3
    packed-switch v4, :pswitch_data_0

    const-string v5, "UNSUPPORTED TYPE: "

    invoke-virtual {p1, v5}, Ljava/io/Writer;->append(Ljava/lang/CharSequence;)Ljava/io/Writer;

    move-result-object v5

    invoke-static {v4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v5, v4}, Ljava/io/Writer;->append(Ljava/lang/CharSequence;)Ljava/io/Writer;

    :goto_4
    const/16 v4, 0xa

    invoke-virtual {p1, v4}, Ljava/io/Writer;->append(C)Ljava/io/Writer;

    .line 1002
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 1003
    :cond_1
    const/16 v5, 0x3a

    invoke-virtual {p1, v5}, Ljava/io/Writer;->append(C)Ljava/io/Writer;

    goto :goto_3

    :pswitch_0
    const-string v4, "{\n"

    invoke-virtual {p1, v4}, Ljava/io/Writer;->append(Ljava/lang/CharSequence;)Ljava/io/Writer;

    invoke-virtual {p0, v2, v0}, Lcom/google/p/a/b/b/a;->d(II)Lcom/google/p/a/b/b/a;

    move-result-object v4

    add-int/lit8 v5, p2, 0x1

    invoke-direct {v4, p1, v5}, Lcom/google/p/a/b/b/a;->a(Ljava/io/Writer;I)V

    invoke-virtual {p1, v3}, Ljava/io/Writer;->append(Ljava/lang/CharSequence;)Ljava/io/Writer;

    move-result-object v4

    const/16 v5, 0x7d

    invoke-virtual {v4, v5}, Ljava/io/Writer;->append(C)Ljava/io/Writer;

    goto :goto_4

    :pswitch_1
    invoke-virtual {p0, v2, v0}, Lcom/google/p/a/b/b/a;->b(II)I

    move-result v4

    invoke-static {v4}, Ljava/lang/Float;->intBitsToFloat(I)F

    move-result v4

    invoke-static {v4}, Ljava/lang/Float;->toString(F)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, Ljava/io/Writer;->append(Ljava/lang/CharSequence;)Ljava/io/Writer;

    goto :goto_4

    :pswitch_2
    invoke-virtual {p0, v2, v0}, Lcom/google/p/a/b/b/a;->c(II)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Double;->longBitsToDouble(J)D

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Double;->toString(D)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, Ljava/io/Writer;->append(Ljava/lang/CharSequence;)Ljava/io/Writer;

    goto :goto_4

    :pswitch_3
    invoke-direct {p0, v2, v0, v4}, Lcom/google/p/a/b/b/a;->a(III)Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, Ljava/io/Writer;->append(Ljava/lang/CharSequence;)Ljava/io/Writer;

    goto :goto_4

    :pswitch_4
    invoke-virtual {p0, v2, v0}, Lcom/google/p/a/b/b/a;->a(II)[B

    move-result-object v4

    invoke-static {v4}, Lcom/google/p/a/e/c;->a([B)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, Ljava/io/Writer;->append(Ljava/lang/CharSequence;)Ljava/io/Writer;

    goto :goto_4

    .line 1001
    :cond_2
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto/16 :goto_1

    .line 1006
    :cond_3
    return-void

    .line 1003
    nop

    :pswitch_data_0
    .packed-switch 0x11
        :pswitch_2
        :pswitch_1
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_4
        :pswitch_0
        :pswitch_0
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_4
        :pswitch_3
    .end packed-switch
.end method

.method private static b(J)J
    .locals 4

    .prologue
    .line 959
    const/4 v0, 0x1

    shl-long v0, p0, v0

    const/16 v2, 0x3f

    ushr-long v2, p0, v2

    neg-long v2, v2

    xor-long/2addr v0, v2

    .line 960
    return-wide v0
.end method

.method private b(ILjava/lang/Object;)Lcom/google/p/a/b/b/a;
    .locals 1

    .prologue
    .line 1703
    if-gez p1, :cond_0

    .line 1704
    new-instance v0, Ljava/lang/ArrayIndexOutOfBoundsException;

    invoke-direct {v0}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>()V

    throw v0

    .line 1706
    :cond_0
    iget-object v0, p0, Lcom/google/p/a/b/b/a;->e:Lcom/google/p/a/e/d;

    invoke-virtual {v0, p1, p2}, Lcom/google/p/a/e/d;->a(ILjava/lang/Object;)V

    .line 1710
    return-object p0
.end method

.method private g(II)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 1413
    iget-object v0, p0, Lcom/google/p/a/b/b/a;->e:Lcom/google/p/a/e/d;

    invoke-virtual {v0, p1}, Lcom/google/p/a/e/d;->a(I)Ljava/lang/Object;

    move-result-object v0

    .line 1415
    invoke-static {v0}, Lcom/google/p/a/b/b/a;->a(Ljava/lang/Object;)I

    move-result v1

    .line 1417
    if-nez v1, :cond_0

    .line 1418
    invoke-direct {p0, p1}, Lcom/google/p/a/b/b/a;->n(I)Ljava/lang/Object;

    move-result-object v0

    .line 1424
    :goto_0
    return-object v0

    .line 1421
    :cond_0
    const/4 v2, 0x1

    if-le v1, v2, :cond_1

    .line 1422
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 1424
    :cond_1
    const/4 v1, 0x0

    invoke-direct {p0, p1, v1, p2, v0}, Lcom/google/p/a/b/b/a;->a(IIILjava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0
.end method

.method private l(I)I
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/16 v3, 0x10

    .line 613
    .line 614
    iget-object v0, p0, Lcom/google/p/a/b/b/a;->d:Lcom/google/p/a/b/b/c;

    if-eqz v0, :cond_5

    .line 615
    iget-object v0, p0, Lcom/google/p/a/b/b/a;->d:Lcom/google/p/a/b/b/c;

    invoke-virtual {v0, p1}, Lcom/google/p/a/b/b/c;->a(I)I

    move-result v2

    .line 618
    :goto_0
    if-ne v2, v3, :cond_4

    .line 619
    iget-object v0, p0, Lcom/google/p/a/b/b/a;->f:Lcom/google/p/a/e/d;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/p/a/b/b/a;->f:Lcom/google/p/a/e/d;

    invoke-virtual {v0, p1}, Lcom/google/p/a/e/d;->a(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 620
    :goto_1
    if-eqz v0, :cond_4

    .line 621
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 625
    :goto_2
    if-ne v0, v3, :cond_1

    invoke-virtual {p0, p1}, Lcom/google/p/a/b/b/a;->k(I)I

    move-result v2

    if-lez v2, :cond_1

    .line 626
    invoke-direct {p0, p1, v1, v3}, Lcom/google/p/a/b/b/a;->a(III)Ljava/lang/Object;

    move-result-object v0

    .line 628
    instance-of v2, v0, Ljava/lang/Long;

    if-nez v2, :cond_0

    instance-of v0, v0, Ljava/lang/Boolean;

    if-eqz v0, :cond_3

    :cond_0
    move v0, v1

    .line 632
    :cond_1
    :goto_3
    return v0

    .line 619
    :cond_2
    const/4 v0, 0x0

    goto :goto_1

    .line 628
    :cond_3
    const/4 v0, 0x2

    goto :goto_3

    :cond_4
    move v0, v2

    goto :goto_2

    :cond_5
    move v2, v3

    goto :goto_0
.end method

.method private m(I)Z
    .locals 2

    .prologue
    .line 950
    invoke-direct {p0, p1}, Lcom/google/p/a/b/b/a;->l(I)I

    move-result v0

    .line 951
    const/16 v1, 0x21

    if-eq v0, v1, :cond_0

    const/16 v1, 0x22

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private n(I)Ljava/lang/Object;
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 1387
    invoke-direct {p0, p1}, Lcom/google/p/a/b/b/a;->l(I)I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    .line 1393
    iget-object v1, p0, Lcom/google/p/a/b/b/a;->d:Lcom/google/p/a/b/b/c;

    if-nez v1, :cond_0

    :goto_0
    :sswitch_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/p/a/b/b/a;->d:Lcom/google/p/a/b/b/c;

    invoke-virtual {v0, p1}, Lcom/google/p/a/b/b/c;->b(I)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0

    .line 1387
    :sswitch_data_0
    .sparse-switch
        0x10 -> :sswitch_0
        0x1a -> :sswitch_0
        0x1b -> :sswitch_0
    .end sparse-switch
.end method

.method private final o(I)I
    .locals 5

    .prologue
    const/16 v4, 0x2f

    .line 1473
    invoke-direct {p0, p1}, Lcom/google/p/a/b/b/a;->l(I)I

    move-result v0

    .line 1475
    packed-switch v0, :pswitch_data_0

    .line 1510
    :pswitch_0
    new-instance v1, Ljava/lang/RuntimeException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unsupp.Type:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/google/p/a/b/b/a;->d:Lcom/google/p/a/b/b/c;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 1492
    :pswitch_1
    const/4 v0, 0x0

    .line 1508
    :goto_0
    :pswitch_2
    return v0

    .line 1498
    :pswitch_3
    const/4 v0, 0x2

    goto :goto_0

    .line 1502
    :pswitch_4
    const/4 v0, 0x1

    goto :goto_0

    .line 1506
    :pswitch_5
    const/4 v0, 0x5

    goto :goto_0

    .line 1508
    :pswitch_6
    const/4 v0, 0x3

    goto :goto_0

    .line 1475
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_0
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_4
        :pswitch_5
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_4
        :pswitch_5
        :pswitch_1
        :pswitch_3
        :pswitch_6
        :pswitch_3
        :pswitch_3
        :pswitch_1
        :pswitch_1
        :pswitch_5
        :pswitch_4
        :pswitch_1
        :pswitch_1
        :pswitch_3
        :pswitch_3
    .end packed-switch
.end method


# virtual methods
.method public final a()Lcom/google/p/a/b/b/a;
    .locals 2

    .prologue
    .line 132
    :try_start_0
    new-instance v0, Lcom/google/p/a/b/b/a;

    iget-object v1, p0, Lcom/google/p/a/b/b/a;->d:Lcom/google/p/a/b/b/c;

    invoke-direct {v0, v1}, Lcom/google/p/a/b/b/a;-><init>(Lcom/google/p/a/b/b/c;)V

    .line 133
    invoke-virtual {p0}, Lcom/google/p/a/b/b/a;->f()[B

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/p/a/b/b/a;->b([B)Lcom/google/p/a/b/b/a;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 134
    return-object v0

    .line 136
    :catch_0
    move-exception v0

    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Could not serialize and parse ProtoBuf."

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final a(IF)Lcom/google/p/a/b/b/a;
    .locals 2

    .prologue
    .line 1172
    invoke-static {p2}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v0

    int-to-long v0, v0

    invoke-virtual {p0, p1, v0, v1}, Lcom/google/p/a/b/b/a;->b(IJ)Lcom/google/p/a/b/b/a;

    move-result-object v0

    return-object v0
.end method

.method public final a(ILjava/lang/String;)Lcom/google/p/a/b/b/a;
    .locals 1

    .prologue
    .line 1238
    invoke-direct {p0, p1, p2}, Lcom/google/p/a/b/b/a;->b(ILjava/lang/Object;)Lcom/google/p/a/b/b/a;

    move-result-object v0

    return-object v0
.end method

.method public final a(IZ)Lcom/google/p/a/b/b/a;
    .locals 1

    .prologue
    .line 1087
    if-eqz p2, :cond_0

    sget-object v0, Lcom/google/p/a/b/b/a;->b:Ljava/lang/Boolean;

    :goto_0
    invoke-direct {p0, p1, v0}, Lcom/google/p/a/b/b/a;->b(ILjava/lang/Object;)Lcom/google/p/a/b/b/a;

    move-result-object v0

    return-object v0

    :cond_0
    sget-object v0, Lcom/google/p/a/b/b/a;->a:Ljava/lang/Boolean;

    goto :goto_0
.end method

.method public final a(Ljava/io/InputStream;)Lcom/google/p/a/b/b/a;
    .locals 1

    .prologue
    .line 422
    const v0, 0x7fffffff

    invoke-direct {p0, p1, v0}, Lcom/google/p/a/b/b/a;->a(Ljava/io/InputStream;I)I

    .line 423
    return-object p0
.end method

.method public final a(I)V
    .locals 4

    .prologue
    .line 173
    const/4 v0, 0x6

    int-to-long v2, p1

    invoke-virtual {p0, v0, v2, v3}, Lcom/google/p/a/b/b/a;->a(IJ)V

    .line 174
    return-void
.end method

.method public final a(ID)V
    .locals 2

    .prologue
    .line 194
    invoke-static {p2, p3}, Ljava/lang/Double;->doubleToLongBits(D)J

    move-result-wide v0

    invoke-virtual {p0, p1, v0, v1}, Lcom/google/p/a/b/b/a;->a(IJ)V

    .line 195
    return-void
.end method

.method public final a(IJ)V
    .locals 2

    .prologue
    .line 180
    invoke-static {p2, p3}, Lcom/google/p/a/e/h;->a(J)Ljava/lang/Long;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lcom/google/p/a/b/b/a;->a(ILjava/lang/Object;)V

    .line 181
    return-void
.end method

.method public final a(ILcom/google/p/a/b/b/a;)V
    .locals 0

    .prologue
    .line 201
    invoke-direct {p0, p1, p2}, Lcom/google/p/a/b/b/a;->a(ILjava/lang/Object;)V

    .line 202
    return-void
.end method

.method public final a(Ljava/io/OutputStream;)V
    .locals 8

    .prologue
    const/4 v0, 0x0

    .line 797
    new-instance v3, Lcom/google/p/a/b/j;

    invoke-direct {v3}, Lcom/google/p/a/b/j;-><init>()V

    invoke-direct {p0, v3}, Lcom/google/p/a/b/b/a;->a(Lcom/google/p/a/b/j;)I

    invoke-virtual {v3}, Lcom/google/p/a/b/j;->a()I

    move-result v4

    move v1, v0

    :goto_0
    if-ge v0, v4, :cond_0

    invoke-virtual {v3, v0}, Lcom/google/p/a/b/j;->a(I)I

    move-result v2

    sub-int v5, v2, v1

    invoke-virtual {v3, p1, v1, v5}, Lcom/google/p/a/b/j;->a(Ljava/io/OutputStream;II)V

    add-int/lit8 v1, v0, 0x1

    invoke-virtual {v3, v1}, Lcom/google/p/a/b/j;->a(I)I

    move-result v1

    int-to-long v6, v1

    invoke-static {p1, v6, v7}, Lcom/google/p/a/b/b/a;->a(Ljava/io/OutputStream;J)I

    add-int/lit8 v0, v0, 0x2

    move v1, v2

    goto :goto_0

    :cond_0
    invoke-virtual {v3}, Lcom/google/p/a/b/j;->b()I

    move-result v0

    if-ge v1, v0, :cond_1

    invoke-virtual {v3}, Lcom/google/p/a/b/j;->b()I

    move-result v0

    sub-int/2addr v0, v1

    invoke-virtual {v3, p1, v1, v0}, Lcom/google/p/a/b/j;->a(Ljava/io/OutputStream;II)V

    .line 798
    :cond_1
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 208
    const/4 v0, 0x5

    invoke-direct {p0, v0, p1}, Lcom/google/p/a/b/b/a;->a(ILjava/lang/Object;)V

    .line 209
    return-void
.end method

.method public final a([B)V
    .locals 1

    .prologue
    .line 166
    const/4 v0, 0x7

    invoke-direct {p0, v0, p1}, Lcom/google/p/a/b/b/a;->a(ILjava/lang/Object;)V

    .line 167
    return-void
.end method

.method public final a(II)[B
    .locals 1

    .prologue
    .line 238
    const/16 v0, 0x19

    invoke-direct {p0, p1, p2, v0}, Lcom/google/p/a/b/b/a;->a(III)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B

    return-object v0
.end method

.method public final b(II)I
    .locals 2

    .prologue
    .line 252
    const/16 v0, 0x15

    invoke-direct {p0, p1, p2, v0}, Lcom/google/p/a/b/b/a;->a(III)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    long-to-int v0, v0

    return v0
.end method

.method public final b(ID)Lcom/google/p/a/b/b/a;
    .locals 2

    .prologue
    .line 1155
    invoke-static {p2, p3}, Ljava/lang/Double;->doubleToLongBits(D)J

    move-result-wide v0

    invoke-virtual {p0, p1, v0, v1}, Lcom/google/p/a/b/b/a;->b(IJ)Lcom/google/p/a/b/b/a;

    move-result-object v0

    return-object v0
.end method

.method public final b(IJ)Lcom/google/p/a/b/b/a;
    .locals 2

    .prologue
    .line 1138
    invoke-static {p2, p3}, Lcom/google/p/a/e/h;->a(J)Ljava/lang/Long;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lcom/google/p/a/b/b/a;->b(ILjava/lang/Object;)Lcom/google/p/a/b/b/a;

    move-result-object v0

    return-object v0
.end method

.method public final b(ILcom/google/p/a/b/b/a;)Lcom/google/p/a/b/b/a;
    .locals 1

    .prologue
    .line 1189
    invoke-direct {p0, p1, p2}, Lcom/google/p/a/b/b/a;->b(ILjava/lang/Object;)Lcom/google/p/a/b/b/a;

    move-result-object v0

    return-object v0
.end method

.method public final b([B)Lcom/google/p/a/b/b/a;
    .locals 2

    .prologue
    .line 407
    new-instance v0, Ljava/io/ByteArrayInputStream;

    invoke-direct {v0, p1}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    array-length v1, p1

    invoke-direct {p0, v0, v1}, Lcom/google/p/a/b/b/a;->a(Ljava/io/InputStream;I)I

    .line 408
    return-object p0
.end method

.method public final b(I)Z
    .locals 1

    .prologue
    .line 215
    const/16 v0, 0x18

    invoke-direct {p0, p1, v0}, Lcom/google/p/a/b/b/a;->g(II)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method

.method public final b()[B
    .locals 2

    .prologue
    .line 231
    const/4 v0, 0x3

    const/16 v1, 0x19

    invoke-direct {p0, v0, v1}, Lcom/google/p/a/b/b/a;->g(II)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B

    return-object v0
.end method

.method public final c(I)I
    .locals 2

    .prologue
    .line 245
    const/16 v0, 0x15

    invoke-direct {p0, p1, v0}, Lcom/google/p/a/b/b/a;->g(II)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    long-to-int v0, v0

    return v0
.end method

.method public final c(II)J
    .locals 2

    .prologue
    .line 267
    const/16 v0, 0x13

    invoke-direct {p0, p1, p2, v0}, Lcom/google/p/a/b/b/a;->a(III)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    return-wide v0
.end method

.method public final c()Lcom/google/p/a/b/b/c;
    .locals 1

    .prologue
    .line 348
    iget-object v0, p0, Lcom/google/p/a/b/b/a;->d:Lcom/google/p/a/b/b/c;

    return-object v0
.end method

.method public final synthetic clone()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 55
    invoke-virtual {p0}, Lcom/google/p/a/b/b/a;->a()Lcom/google/p/a/b/b/a;

    move-result-object v0

    return-object v0
.end method

.method public final d(I)J
    .locals 2

    .prologue
    .line 260
    const/16 v0, 0x13

    invoke-direct {p0, p1, v0}, Lcom/google/p/a/b/b/a;->g(II)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    return-wide v0
.end method

.method public final d(II)Lcom/google/p/a/b/b/a;
    .locals 1

    .prologue
    .line 310
    const/16 v0, 0x1a

    invoke-direct {p0, p1, p2, v0}, Lcom/google/p/a/b/b/a;->a(III)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/p/a/b/b/a;

    return-object v0
.end method

.method public final d()Z
    .locals 1

    .prologue
    .line 375
    iget-object v0, p0, Lcom/google/p/a/b/b/a;->d:Lcom/google/p/a/b/b/c;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/p/a/b/b/a;->d:Lcom/google/p/a/b/b/c;

    invoke-virtual {v0, p0}, Lcom/google/p/a/b/b/c;->a(Lcom/google/p/a/b/b/a;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final e(I)F
    .locals 1

    .prologue
    .line 274
    invoke-virtual {p0, p1}, Lcom/google/p/a/b/b/a;->c(I)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->intBitsToFloat(I)F

    move-result v0

    return v0
.end method

.method public final e()I
    .locals 1

    .prologue
    .line 639
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/p/a/b/b/a;->a(Z)I

    move-result v0

    return v0
.end method

.method public final e(II)Ljava/lang/String;
    .locals 1

    .prologue
    .line 341
    const/16 v0, 0x1c

    invoke-direct {p0, p1, p2, v0}, Lcom/google/p/a/b/b/a;->a(III)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public final f(I)Lcom/google/p/a/b/b/a;
    .locals 1

    .prologue
    .line 302
    const/16 v0, 0x1a

    invoke-direct {p0, p1, v0}, Lcom/google/p/a/b/b/a;->g(II)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/p/a/b/b/a;

    return-object v0
.end method

.method public final f(II)Lcom/google/p/a/b/b/a;
    .locals 2

    .prologue
    .line 1121
    int-to-long v0, p2

    invoke-virtual {p0, p1, v0, v1}, Lcom/google/p/a/b/b/a;->b(IJ)Lcom/google/p/a/b/b/a;

    move-result-object v0

    return-object v0
.end method

.method public final f()[B
    .locals 1

    .prologue
    .line 977
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 978
    invoke-virtual {p0, v0}, Lcom/google/p/a/b/b/a;->a(Ljava/io/OutputStream;)V

    .line 979
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v0

    return-object v0
.end method

.method public final g(I)Ljava/lang/String;
    .locals 1

    .prologue
    .line 333
    const/16 v0, 0x1c

    invoke-direct {p0, p1, v0}, Lcom/google/p/a/b/b/a;->g(II)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public final h(I)Z
    .locals 1

    .prologue
    .line 382
    invoke-virtual {p0, p1}, Lcom/google/p/a/b/b/a;->k(I)I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final i(I)Z
    .locals 1

    .prologue
    .line 394
    invoke-virtual {p0, p1}, Lcom/google/p/a/b/b/a;->h(I)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0, p1}, Lcom/google/p/a/b/b/a;->n(I)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final j(I)V
    .locals 2

    .prologue
    .line 574
    invoke-virtual {p0, p1}, Lcom/google/p/a/b/b/a;->k(I)I

    move-result v0

    .line 575
    if-gtz v0, :cond_0

    .line 576
    new-instance v0, Ljava/lang/ArrayIndexOutOfBoundsException;

    invoke-direct {v0}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>()V

    throw v0

    .line 578
    :cond_0
    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 579
    iget-object v0, p0, Lcom/google/p/a/b/b/a;->e:Lcom/google/p/a/e/d;

    invoke-virtual {v0, p1}, Lcom/google/p/a/e/d;->b(I)Ljava/lang/Object;

    .line 584
    :goto_0
    return-void

    .line 581
    :cond_1
    iget-object v0, p0, Lcom/google/p/a/b/b/a;->e:Lcom/google/p/a/e/d;

    invoke-virtual {v0, p1}, Lcom/google/p/a/e/d;->a(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Vector;

    .line 582
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/Vector;->removeElementAt(I)V

    goto :goto_0
.end method

.method public final k(I)I
    .locals 1

    .prologue
    .line 602
    iget-object v0, p0, Lcom/google/p/a/b/b/a;->e:Lcom/google/p/a/e/d;

    invoke-virtual {v0, p1}, Lcom/google/p/a/e/d;->a(I)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lcom/google/p/a/b/b/a;->a(Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1769
    new-instance v0, Ljava/io/StringWriter;

    invoke-direct {v0}, Ljava/io/StringWriter;-><init>()V

    .line 1771
    :try_start_0
    invoke-direct {p0, v0}, Lcom/google/p/a/b/b/a;->a(Ljava/io/Writer;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1775
    :goto_0
    invoke-virtual {v0}, Ljava/io/StringWriter;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 1772
    :catch_0
    move-exception v1

    .line 1773
    invoke-virtual {v1}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/io/StringWriter;->write(Ljava/lang/String;)V

    goto :goto_0
.end method
