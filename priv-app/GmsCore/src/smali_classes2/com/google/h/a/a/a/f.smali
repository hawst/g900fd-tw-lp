.class public final Lcom/google/h/a/a/a/f;
.super Lcom/google/protobuf/nano/d;
.source "SourceFile"


# instance fields
.field public a:[Lcom/google/h/a/a/a/b;

.field public b:[J

.field public c:[Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2686
    invoke-direct {p0}, Lcom/google/protobuf/nano/d;-><init>()V

    .line 2687
    invoke-static {}, Lcom/google/h/a/a/a/b;->a()[Lcom/google/h/a/a/a/b;

    move-result-object v0

    iput-object v0, p0, Lcom/google/h/a/a/a/f;->a:[Lcom/google/h/a/a/a/b;

    sget-object v0, Lcom/google/protobuf/nano/m;->b:[J

    iput-object v0, p0, Lcom/google/h/a/a/a/f;->b:[J

    sget-object v0, Lcom/google/protobuf/nano/m;->f:[Ljava/lang/String;

    iput-object v0, p0, Lcom/google/h/a/a/a/f;->c:[Ljava/lang/String;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/h/a/a/a/f;->unknownFieldData:Lcom/google/protobuf/nano/f;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/h/a/a/a/f;->cachedSize:I

    .line 2688
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 2728
    invoke-super {p0}, Lcom/google/protobuf/nano/d;->computeSerializedSize()I

    move-result v0

    .line 2729
    iget-object v2, p0, Lcom/google/h/a/a/a/f;->a:[Lcom/google/h/a/a/a/b;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/google/h/a/a/a/f;->a:[Lcom/google/h/a/a/a/b;

    array-length v2, v2

    if-lez v2, :cond_2

    move v2, v0

    move v0, v1

    .line 2730
    :goto_0
    iget-object v3, p0, Lcom/google/h/a/a/a/f;->a:[Lcom/google/h/a/a/a/b;

    array-length v3, v3

    if-ge v0, v3, :cond_1

    .line 2731
    iget-object v3, p0, Lcom/google/h/a/a/a/f;->a:[Lcom/google/h/a/a/a/b;

    aget-object v3, v3, v0

    .line 2732
    if-eqz v3, :cond_0

    .line 2733
    const/4 v4, 0x1

    invoke-static {v4, v3}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v3

    add-int/2addr v2, v3

    .line 2730
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    move v0, v2

    .line 2738
    :cond_2
    iget-object v2, p0, Lcom/google/h/a/a/a/f;->b:[J

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/google/h/a/a/a/f;->b:[J

    array-length v2, v2

    if-lez v2, :cond_4

    move v2, v1

    move v3, v1

    .line 2740
    :goto_1
    iget-object v4, p0, Lcom/google/h/a/a/a/f;->b:[J

    array-length v4, v4

    if-ge v2, v4, :cond_3

    .line 2741
    iget-object v4, p0, Lcom/google/h/a/a/a/f;->b:[J

    aget-wide v4, v4, v2

    .line 2742
    invoke-static {v4, v5}, Lcom/google/protobuf/nano/b;->a(J)I

    move-result v4

    add-int/2addr v3, v4

    .line 2740
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 2745
    :cond_3
    add-int/2addr v0, v3

    .line 2746
    iget-object v2, p0, Lcom/google/h/a/a/a/f;->b:[J

    array-length v2, v2

    mul-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 2748
    :cond_4
    iget-object v2, p0, Lcom/google/h/a/a/a/f;->c:[Ljava/lang/String;

    if-eqz v2, :cond_7

    iget-object v2, p0, Lcom/google/h/a/a/a/f;->c:[Ljava/lang/String;

    array-length v2, v2

    if-lez v2, :cond_7

    move v2, v1

    move v3, v1

    .line 2751
    :goto_2
    iget-object v4, p0, Lcom/google/h/a/a/a/f;->c:[Ljava/lang/String;

    array-length v4, v4

    if-ge v1, v4, :cond_6

    .line 2752
    iget-object v4, p0, Lcom/google/h/a/a/a/f;->c:[Ljava/lang/String;

    aget-object v4, v4, v1

    .line 2753
    if-eqz v4, :cond_5

    .line 2754
    add-int/lit8 v3, v3, 0x1

    .line 2755
    invoke-static {v4}, Lcom/google/protobuf/nano/b;->a(Ljava/lang/String;)I

    move-result v4

    add-int/2addr v2, v4

    .line 2751
    :cond_5
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 2759
    :cond_6
    add-int/2addr v0, v2

    .line 2760
    mul-int/lit8 v1, v3, 0x1

    add-int/2addr v0, v1

    .line 2762
    :cond_7
    return v0
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 2660
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/h/a/a/a/f;->storeUnknownField(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    const/16 v0, 0xa

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->b(Lcom/google/protobuf/nano/a;I)I

    move-result v2

    iget-object v0, p0, Lcom/google/h/a/a/a/f;->a:[Lcom/google/h/a/a/a/b;

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lcom/google/h/a/a/a/b;

    if-eqz v0, :cond_1

    iget-object v3, p0, Lcom/google/h/a/a/a/f;->a:[Lcom/google/h/a/a/a/b;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_1
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_3

    new-instance v3, Lcom/google/h/a/a/a/b;

    invoke-direct {v3}, Lcom/google/h/a/a/a/b;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    iget-object v0, p0, Lcom/google/h/a/a/a/f;->a:[Lcom/google/h/a/a/a/b;

    array-length v0, v0

    goto :goto_1

    :cond_3
    new-instance v3, Lcom/google/h/a/a/a/b;

    invoke-direct {v3}, Lcom/google/h/a/a/a/b;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    iput-object v2, p0, Lcom/google/h/a/a/a/f;->a:[Lcom/google/h/a/a/a/b;

    goto :goto_0

    :sswitch_2
    const/16 v0, 0x10

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->b(Lcom/google/protobuf/nano/a;I)I

    move-result v2

    iget-object v0, p0, Lcom/google/h/a/a/a/f;->b:[J

    if-nez v0, :cond_5

    move v0, v1

    :goto_3
    add-int/2addr v2, v0

    new-array v2, v2, [J

    if-eqz v0, :cond_4

    iget-object v3, p0, Lcom/google/h/a/a/a/f;->b:[J

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_4
    :goto_4
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_6

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->j()J

    move-result-wide v4

    aput-wide v4, v2, v0

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_5
    iget-object v0, p0, Lcom/google/h/a/a/a/f;->b:[J

    array-length v0, v0

    goto :goto_3

    :cond_6
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->j()J

    move-result-wide v4

    aput-wide v4, v2, v0

    iput-object v2, p0, Lcom/google/h/a/a/a/f;->b:[J

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->c(I)I

    move-result v3

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->o()I

    move-result v2

    move v0, v1

    :goto_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->m()I

    move-result v4

    if-lez v4, :cond_7

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->j()J

    add-int/lit8 v0, v0, 0x1

    goto :goto_5

    :cond_7
    invoke-virtual {p1, v2}, Lcom/google/protobuf/nano/a;->e(I)V

    iget-object v2, p0, Lcom/google/h/a/a/a/f;->b:[J

    if-nez v2, :cond_9

    move v2, v1

    :goto_6
    add-int/2addr v0, v2

    new-array v0, v0, [J

    if-eqz v2, :cond_8

    iget-object v4, p0, Lcom/google/h/a/a/a/f;->b:[J

    invoke-static {v4, v1, v0, v1, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_8
    :goto_7
    array-length v4, v0

    if-ge v2, v4, :cond_a

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->j()J

    move-result-wide v4

    aput-wide v4, v0, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_7

    :cond_9
    iget-object v2, p0, Lcom/google/h/a/a/a/f;->b:[J

    array-length v2, v2

    goto :goto_6

    :cond_a
    iput-object v0, p0, Lcom/google/h/a/a/a/f;->b:[J

    invoke-virtual {p1, v3}, Lcom/google/protobuf/nano/a;->d(I)V

    goto/16 :goto_0

    :sswitch_4
    const/16 v0, 0x1a

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->b(Lcom/google/protobuf/nano/a;I)I

    move-result v2

    iget-object v0, p0, Lcom/google/h/a/a/a/f;->c:[Ljava/lang/String;

    if-nez v0, :cond_c

    move v0, v1

    :goto_8
    add-int/2addr v2, v0

    new-array v2, v2, [Ljava/lang/String;

    if-eqz v0, :cond_b

    iget-object v3, p0, Lcom/google/h/a/a/a/f;->c:[Ljava/lang/String;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_b
    :goto_9
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_d

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_9

    :cond_c
    iget-object v0, p0, Lcom/google/h/a/a/a/f;->c:[Ljava/lang/String;

    array-length v0, v0

    goto :goto_8

    :cond_d
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    iput-object v2, p0, Lcom/google/h/a/a/a/f;->c:[Ljava/lang/String;

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x12 -> :sswitch_3
        0x1a -> :sswitch_4
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 2702
    iget-object v0, p0, Lcom/google/h/a/a/a/f;->a:[Lcom/google/h/a/a/a/b;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/h/a/a/a/f;->a:[Lcom/google/h/a/a/a/b;

    array-length v0, v0

    if-lez v0, :cond_1

    move v0, v1

    .line 2703
    :goto_0
    iget-object v2, p0, Lcom/google/h/a/a/a/f;->a:[Lcom/google/h/a/a/a/b;

    array-length v2, v2

    if-ge v0, v2, :cond_1

    .line 2704
    iget-object v2, p0, Lcom/google/h/a/a/a/f;->a:[Lcom/google/h/a/a/a/b;

    aget-object v2, v2, v0

    .line 2705
    if-eqz v2, :cond_0

    .line 2706
    const/4 v3, 0x1

    invoke-virtual {p1, v3, v2}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 2703
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2710
    :cond_1
    iget-object v0, p0, Lcom/google/h/a/a/a/f;->b:[J

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/h/a/a/a/f;->b:[J

    array-length v0, v0

    if-lez v0, :cond_2

    move v0, v1

    .line 2711
    :goto_1
    iget-object v2, p0, Lcom/google/h/a/a/a/f;->b:[J

    array-length v2, v2

    if-ge v0, v2, :cond_2

    .line 2712
    const/4 v2, 0x2

    iget-object v3, p0, Lcom/google/h/a/a/a/f;->b:[J

    aget-wide v4, v3, v0

    invoke-virtual {p1, v2, v4, v5}, Lcom/google/protobuf/nano/b;->b(IJ)V

    .line 2711
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 2715
    :cond_2
    iget-object v0, p0, Lcom/google/h/a/a/a/f;->c:[Ljava/lang/String;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/google/h/a/a/a/f;->c:[Ljava/lang/String;

    array-length v0, v0

    if-lez v0, :cond_4

    .line 2716
    :goto_2
    iget-object v0, p0, Lcom/google/h/a/a/a/f;->c:[Ljava/lang/String;

    array-length v0, v0

    if-ge v1, v0, :cond_4

    .line 2717
    iget-object v0, p0, Lcom/google/h/a/a/a/f;->c:[Ljava/lang/String;

    aget-object v0, v0, v1

    .line 2718
    if-eqz v0, :cond_3

    .line 2719
    const/4 v2, 0x3

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 2716
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 2723
    :cond_4
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/d;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 2724
    return-void
.end method
