.class public final Lcom/google/h/a/a/a/i;
.super Lcom/google/protobuf/nano/d;
.source "SourceFile"


# instance fields
.field public a:[Lcom/google/h/a/a/a/j;

.field public b:Ljava/lang/Long;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2999
    invoke-direct {p0}, Lcom/google/protobuf/nano/d;-><init>()V

    .line 3000
    invoke-static {}, Lcom/google/h/a/a/a/j;->a()[Lcom/google/h/a/a/a/j;

    move-result-object v0

    iput-object v0, p0, Lcom/google/h/a/a/a/i;->a:[Lcom/google/h/a/a/a/j;

    iput-object v1, p0, Lcom/google/h/a/a/a/i;->b:Ljava/lang/Long;

    iput-object v1, p0, Lcom/google/h/a/a/a/i;->unknownFieldData:Lcom/google/protobuf/nano/f;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/h/a/a/a/i;->cachedSize:I

    .line 3001
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 4

    .prologue
    .line 3030
    invoke-super {p0}, Lcom/google/protobuf/nano/d;->computeSerializedSize()I

    move-result v1

    .line 3031
    iget-object v0, p0, Lcom/google/h/a/a/a/i;->a:[Lcom/google/h/a/a/a/j;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/h/a/a/a/i;->a:[Lcom/google/h/a/a/a/j;

    array-length v0, v0

    if-lez v0, :cond_1

    .line 3032
    const/4 v0, 0x0

    :goto_0
    iget-object v2, p0, Lcom/google/h/a/a/a/i;->a:[Lcom/google/h/a/a/a/j;

    array-length v2, v2

    if-ge v0, v2, :cond_1

    .line 3033
    iget-object v2, p0, Lcom/google/h/a/a/a/i;->a:[Lcom/google/h/a/a/a/j;

    aget-object v2, v2, v0

    .line 3034
    if-eqz v2, :cond_0

    .line 3035
    const/4 v3, 0x1

    invoke-static {v3, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v2

    add-int/2addr v1, v2

    .line 3032
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 3040
    :cond_1
    iget-object v0, p0, Lcom/google/h/a/a/a/i;->b:Ljava/lang/Long;

    if-eqz v0, :cond_2

    .line 3041
    const/4 v0, 0x2

    iget-object v2, p0, Lcom/google/h/a/a/a/i;->b:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v0, v2, v3}, Lcom/google/protobuf/nano/b;->f(IJ)I

    move-result v0

    add-int/2addr v1, v0

    .line 3044
    :cond_2
    return v1
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 2976
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/h/a/a/a/i;->storeUnknownField(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    const/16 v0, 0xa

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->b(Lcom/google/protobuf/nano/a;I)I

    move-result v2

    iget-object v0, p0, Lcom/google/h/a/a/a/i;->a:[Lcom/google/h/a/a/a/j;

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lcom/google/h/a/a/a/j;

    if-eqz v0, :cond_1

    iget-object v3, p0, Lcom/google/h/a/a/a/i;->a:[Lcom/google/h/a/a/a/j;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_1
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_3

    new-instance v3, Lcom/google/h/a/a/a/j;

    invoke-direct {v3}, Lcom/google/h/a/a/a/j;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    iget-object v0, p0, Lcom/google/h/a/a/a/i;->a:[Lcom/google/h/a/a/a/j;

    array-length v0, v0

    goto :goto_1

    :cond_3
    new-instance v3, Lcom/google/h/a/a/a/j;

    invoke-direct {v3}, Lcom/google/h/a/a/a/j;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    iput-object v2, p0, Lcom/google/h/a/a/a/i;->a:[Lcom/google/h/a/a/a/j;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->j()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/google/h/a/a/a/i;->b:Ljava/lang/Long;

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 4

    .prologue
    .line 3014
    iget-object v0, p0, Lcom/google/h/a/a/a/i;->a:[Lcom/google/h/a/a/a/j;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/h/a/a/a/i;->a:[Lcom/google/h/a/a/a/j;

    array-length v0, v0

    if-lez v0, :cond_1

    .line 3015
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/google/h/a/a/a/i;->a:[Lcom/google/h/a/a/a/j;

    array-length v1, v1

    if-ge v0, v1, :cond_1

    .line 3016
    iget-object v1, p0, Lcom/google/h/a/a/a/i;->a:[Lcom/google/h/a/a/a/j;

    aget-object v1, v1, v0

    .line 3017
    if-eqz v1, :cond_0

    .line 3018
    const/4 v2, 0x1

    invoke-virtual {p1, v2, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 3015
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 3022
    :cond_1
    iget-object v0, p0, Lcom/google/h/a/a/a/i;->b:Ljava/lang/Long;

    if-eqz v0, :cond_2

    .line 3023
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/h/a/a/a/i;->b:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/b;->b(IJ)V

    .line 3025
    :cond_2
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/d;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 3026
    return-void
.end method
