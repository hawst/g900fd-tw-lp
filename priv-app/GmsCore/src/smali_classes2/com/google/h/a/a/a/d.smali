.class public final Lcom/google/h/a/a/a/d;
.super Lcom/google/protobuf/nano/d;
.source "SourceFile"


# instance fields
.field public a:Ljava/lang/String;

.field public b:[Lcom/google/h/a/a/a/g;

.field public c:[Ljava/lang/String;

.field public d:Ljava/lang/String;

.field public e:Ljava/lang/Integer;

.field public f:Ljava/lang/Integer;

.field public g:Ljava/lang/Long;

.field public h:Ljava/lang/Long;

.field public i:Ljava/lang/String;

.field public j:Ljava/lang/String;

.field public k:Ljava/lang/String;

.field public l:Ljava/lang/Integer;

.field public m:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 447
    invoke-direct {p0}, Lcom/google/protobuf/nano/d;-><init>()V

    .line 448
    iput-object v1, p0, Lcom/google/h/a/a/a/d;->a:Ljava/lang/String;

    invoke-static {}, Lcom/google/h/a/a/a/g;->a()[Lcom/google/h/a/a/a/g;

    move-result-object v0

    iput-object v0, p0, Lcom/google/h/a/a/a/d;->b:[Lcom/google/h/a/a/a/g;

    sget-object v0, Lcom/google/protobuf/nano/m;->f:[Ljava/lang/String;

    iput-object v0, p0, Lcom/google/h/a/a/a/d;->c:[Ljava/lang/String;

    iput-object v1, p0, Lcom/google/h/a/a/a/d;->d:Ljava/lang/String;

    iput-object v1, p0, Lcom/google/h/a/a/a/d;->e:Ljava/lang/Integer;

    iput-object v1, p0, Lcom/google/h/a/a/a/d;->f:Ljava/lang/Integer;

    iput-object v1, p0, Lcom/google/h/a/a/a/d;->g:Ljava/lang/Long;

    iput-object v1, p0, Lcom/google/h/a/a/a/d;->h:Ljava/lang/Long;

    iput-object v1, p0, Lcom/google/h/a/a/a/d;->i:Ljava/lang/String;

    iput-object v1, p0, Lcom/google/h/a/a/a/d;->j:Ljava/lang/String;

    iput-object v1, p0, Lcom/google/h/a/a/a/d;->k:Ljava/lang/String;

    iput-object v1, p0, Lcom/google/h/a/a/a/d;->l:Ljava/lang/Integer;

    iput-object v1, p0, Lcom/google/h/a/a/a/d;->m:Ljava/lang/Boolean;

    iput-object v1, p0, Lcom/google/h/a/a/a/d;->unknownFieldData:Lcom/google/protobuf/nano/f;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/h/a/a/a/d;->cachedSize:I

    .line 449
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 525
    invoke-super {p0}, Lcom/google/protobuf/nano/d;->computeSerializedSize()I

    move-result v0

    .line 526
    const/4 v2, 0x1

    iget-object v3, p0, Lcom/google/h/a/a/a/d;->a:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v2

    add-int v4, v0, v2

    .line 528
    iget-object v0, p0, Lcom/google/h/a/a/a/d;->c:[Ljava/lang/String;

    if-eqz v0, :cond_e

    iget-object v0, p0, Lcom/google/h/a/a/a/d;->c:[Ljava/lang/String;

    array-length v0, v0

    if-lez v0, :cond_e

    move v0, v1

    move v2, v1

    move v3, v1

    .line 531
    :goto_0
    iget-object v5, p0, Lcom/google/h/a/a/a/d;->c:[Ljava/lang/String;

    array-length v5, v5

    if-ge v0, v5, :cond_1

    .line 532
    iget-object v5, p0, Lcom/google/h/a/a/a/d;->c:[Ljava/lang/String;

    aget-object v5, v5, v0

    .line 533
    if-eqz v5, :cond_0

    .line 534
    add-int/lit8 v3, v3, 0x1

    .line 535
    invoke-static {v5}, Lcom/google/protobuf/nano/b;->a(Ljava/lang/String;)I

    move-result v5

    add-int/2addr v2, v5

    .line 531
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 539
    :cond_1
    add-int v0, v4, v2

    .line 540
    mul-int/lit8 v2, v3, 0x1

    add-int/2addr v0, v2

    .line 542
    :goto_1
    iget-object v2, p0, Lcom/google/h/a/a/a/d;->d:Ljava/lang/String;

    if-eqz v2, :cond_2

    .line 543
    const/16 v2, 0x8

    iget-object v3, p0, Lcom/google/h/a/a/a/d;->d:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 546
    :cond_2
    iget-object v2, p0, Lcom/google/h/a/a/a/d;->g:Ljava/lang/Long;

    if-eqz v2, :cond_3

    .line 547
    const/16 v2, 0x9

    iget-object v3, p0, Lcom/google/h/a/a/a/d;->g:Ljava/lang/Long;

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-static {v2, v4, v5}, Lcom/google/protobuf/nano/b;->e(IJ)I

    move-result v2

    add-int/2addr v0, v2

    .line 550
    :cond_3
    iget-object v2, p0, Lcom/google/h/a/a/a/d;->b:[Lcom/google/h/a/a/a/g;

    if-eqz v2, :cond_5

    iget-object v2, p0, Lcom/google/h/a/a/a/d;->b:[Lcom/google/h/a/a/a/g;

    array-length v2, v2

    if-lez v2, :cond_5

    .line 551
    :goto_2
    iget-object v2, p0, Lcom/google/h/a/a/a/d;->b:[Lcom/google/h/a/a/a/g;

    array-length v2, v2

    if-ge v1, v2, :cond_5

    .line 552
    iget-object v2, p0, Lcom/google/h/a/a/a/d;->b:[Lcom/google/h/a/a/a/g;

    aget-object v2, v2, v1

    .line 553
    if-eqz v2, :cond_4

    .line 554
    const/16 v3, 0xa

    invoke-static {v3, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v2

    add-int/2addr v0, v2

    .line 551
    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 559
    :cond_5
    iget-object v1, p0, Lcom/google/h/a/a/a/d;->f:Ljava/lang/Integer;

    if-eqz v1, :cond_6

    .line 560
    const/16 v1, 0xb

    iget-object v2, p0, Lcom/google/h/a/a/a/d;->f:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->g(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 563
    :cond_6
    iget-object v1, p0, Lcom/google/h/a/a/a/d;->e:Ljava/lang/Integer;

    if-eqz v1, :cond_7

    .line 564
    const/16 v1, 0xc

    iget-object v2, p0, Lcom/google/h/a/a/a/d;->e:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->g(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 567
    :cond_7
    iget-object v1, p0, Lcom/google/h/a/a/a/d;->h:Ljava/lang/Long;

    if-eqz v1, :cond_8

    .line 568
    const/16 v1, 0xd

    iget-object v2, p0, Lcom/google/h/a/a/a/d;->h:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/b;->e(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 571
    :cond_8
    iget-object v1, p0, Lcom/google/h/a/a/a/d;->i:Ljava/lang/String;

    if-eqz v1, :cond_9

    .line 572
    const/16 v1, 0xe

    iget-object v2, p0, Lcom/google/h/a/a/a/d;->i:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 575
    :cond_9
    iget-object v1, p0, Lcom/google/h/a/a/a/d;->j:Ljava/lang/String;

    if-eqz v1, :cond_a

    .line 576
    const/16 v1, 0xf

    iget-object v2, p0, Lcom/google/h/a/a/a/d;->j:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 579
    :cond_a
    iget-object v1, p0, Lcom/google/h/a/a/a/d;->k:Ljava/lang/String;

    if-eqz v1, :cond_b

    .line 580
    const/16 v1, 0x10

    iget-object v2, p0, Lcom/google/h/a/a/a/d;->k:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 583
    :cond_b
    iget-object v1, p0, Lcom/google/h/a/a/a/d;->l:Ljava/lang/Integer;

    if-eqz v1, :cond_c

    .line 584
    const/16 v1, 0x11

    iget-object v2, p0, Lcom/google/h/a/a/a/d;->l:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 587
    :cond_c
    iget-object v1, p0, Lcom/google/h/a/a/a/d;->m:Ljava/lang/Boolean;

    if-eqz v1, :cond_d

    .line 588
    const/16 v1, 0x12

    iget-object v2, p0, Lcom/google/h/a/a/a/d;->m:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 591
    :cond_d
    return v0

    :cond_e
    move v0, v4

    goto/16 :goto_1
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 391
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/h/a/a/a/d;->storeUnknownField(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/h/a/a/a/d;->a:Ljava/lang/String;

    goto :goto_0

    :sswitch_2
    const/16 v0, 0x22

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->b(Lcom/google/protobuf/nano/a;I)I

    move-result v2

    iget-object v0, p0, Lcom/google/h/a/a/a/d;->c:[Ljava/lang/String;

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v3, p0, Lcom/google/h/a/a/a/d;->c:[Ljava/lang/String;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_1
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_3

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    iget-object v0, p0, Lcom/google/h/a/a/a/d;->c:[Ljava/lang/String;

    array-length v0, v0

    goto :goto_1

    :cond_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    iput-object v2, p0, Lcom/google/h/a/a/a/d;->c:[Ljava/lang/String;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/h/a/a/a/d;->d:Ljava/lang/String;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->j()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/google/h/a/a/a/d;->g:Ljava/lang/Long;

    goto :goto_0

    :sswitch_5
    const/16 v0, 0x52

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->b(Lcom/google/protobuf/nano/a;I)I

    move-result v2

    iget-object v0, p0, Lcom/google/h/a/a/a/d;->b:[Lcom/google/h/a/a/a/g;

    if-nez v0, :cond_5

    move v0, v1

    :goto_3
    add-int/2addr v2, v0

    new-array v2, v2, [Lcom/google/h/a/a/a/g;

    if-eqz v0, :cond_4

    iget-object v3, p0, Lcom/google/h/a/a/a/d;->b:[Lcom/google/h/a/a/a/g;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_4
    :goto_4
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_6

    new-instance v3, Lcom/google/h/a/a/a/g;

    invoke-direct {v3}, Lcom/google/h/a/a/a/g;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_5
    iget-object v0, p0, Lcom/google/h/a/a/a/d;->b:[Lcom/google/h/a/a/a/g;

    array-length v0, v0

    goto :goto_3

    :cond_6
    new-instance v3, Lcom/google/h/a/a/a/g;

    invoke-direct {v3}, Lcom/google/h/a/a/a/g;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    iput-object v2, p0, Lcom/google/h/a/a/a/d;->b:[Lcom/google/h/a/a/a/g;

    goto/16 :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/h/a/a/a/d;->f:Ljava/lang/Integer;

    goto/16 :goto_0

    :sswitch_7
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/h/a/a/a/d;->e:Ljava/lang/Integer;

    goto/16 :goto_0

    :sswitch_8
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->j()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/google/h/a/a/a/d;->h:Ljava/lang/Long;

    goto/16 :goto_0

    :sswitch_9
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/h/a/a/a/d;->i:Ljava/lang/String;

    goto/16 :goto_0

    :sswitch_a
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/h/a/a/a/d;->j:Ljava/lang/String;

    goto/16 :goto_0

    :sswitch_b
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/h/a/a/a/d;->k:Ljava/lang/String;

    goto/16 :goto_0

    :sswitch_c
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto/16 :goto_0

    :pswitch_0
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/h/a/a/a/d;->l:Ljava/lang/Integer;

    goto/16 :goto_0

    :sswitch_d
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/h/a/a/a/d;->m:Ljava/lang/Boolean;

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x22 -> :sswitch_2
        0x42 -> :sswitch_3
        0x48 -> :sswitch_4
        0x52 -> :sswitch_5
        0x58 -> :sswitch_6
        0x60 -> :sswitch_7
        0x68 -> :sswitch_8
        0x72 -> :sswitch_9
        0x7a -> :sswitch_a
        0x82 -> :sswitch_b
        0x88 -> :sswitch_c
        0x90 -> :sswitch_d
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 473
    const/4 v0, 0x1

    iget-object v2, p0, Lcom/google/h/a/a/a/d;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v2}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 474
    iget-object v0, p0, Lcom/google/h/a/a/a/d;->c:[Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/h/a/a/a/d;->c:[Ljava/lang/String;

    array-length v0, v0

    if-lez v0, :cond_1

    move v0, v1

    .line 475
    :goto_0
    iget-object v2, p0, Lcom/google/h/a/a/a/d;->c:[Ljava/lang/String;

    array-length v2, v2

    if-ge v0, v2, :cond_1

    .line 476
    iget-object v2, p0, Lcom/google/h/a/a/a/d;->c:[Ljava/lang/String;

    aget-object v2, v2, v0

    .line 477
    if-eqz v2, :cond_0

    .line 478
    const/4 v3, 0x4

    invoke-virtual {p1, v3, v2}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 475
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 482
    :cond_1
    iget-object v0, p0, Lcom/google/h/a/a/a/d;->d:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 483
    const/16 v0, 0x8

    iget-object v2, p0, Lcom/google/h/a/a/a/d;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v2}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 485
    :cond_2
    iget-object v0, p0, Lcom/google/h/a/a/a/d;->g:Ljava/lang/Long;

    if-eqz v0, :cond_3

    .line 486
    const/16 v0, 0x9

    iget-object v2, p0, Lcom/google/h/a/a/a/d;->g:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/b;->a(IJ)V

    .line 488
    :cond_3
    iget-object v0, p0, Lcom/google/h/a/a/a/d;->b:[Lcom/google/h/a/a/a/g;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/google/h/a/a/a/d;->b:[Lcom/google/h/a/a/a/g;

    array-length v0, v0

    if-lez v0, :cond_5

    .line 489
    :goto_1
    iget-object v0, p0, Lcom/google/h/a/a/a/d;->b:[Lcom/google/h/a/a/a/g;

    array-length v0, v0

    if-ge v1, v0, :cond_5

    .line 490
    iget-object v0, p0, Lcom/google/h/a/a/a/d;->b:[Lcom/google/h/a/a/a/g;

    aget-object v0, v0, v1

    .line 491
    if-eqz v0, :cond_4

    .line 492
    const/16 v2, 0xa

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 489
    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 496
    :cond_5
    iget-object v0, p0, Lcom/google/h/a/a/a/d;->f:Ljava/lang/Integer;

    if-eqz v0, :cond_6

    .line 497
    const/16 v0, 0xb

    iget-object v1, p0, Lcom/google/h/a/a/a/d;->f:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->c(II)V

    .line 499
    :cond_6
    iget-object v0, p0, Lcom/google/h/a/a/a/d;->e:Ljava/lang/Integer;

    if-eqz v0, :cond_7

    .line 500
    const/16 v0, 0xc

    iget-object v1, p0, Lcom/google/h/a/a/a/d;->e:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->c(II)V

    .line 502
    :cond_7
    iget-object v0, p0, Lcom/google/h/a/a/a/d;->h:Ljava/lang/Long;

    if-eqz v0, :cond_8

    .line 503
    const/16 v0, 0xd

    iget-object v1, p0, Lcom/google/h/a/a/a/d;->h:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/b;->a(IJ)V

    .line 505
    :cond_8
    iget-object v0, p0, Lcom/google/h/a/a/a/d;->i:Ljava/lang/String;

    if-eqz v0, :cond_9

    .line 506
    const/16 v0, 0xe

    iget-object v1, p0, Lcom/google/h/a/a/a/d;->i:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 508
    :cond_9
    iget-object v0, p0, Lcom/google/h/a/a/a/d;->j:Ljava/lang/String;

    if-eqz v0, :cond_a

    .line 509
    const/16 v0, 0xf

    iget-object v1, p0, Lcom/google/h/a/a/a/d;->j:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 511
    :cond_a
    iget-object v0, p0, Lcom/google/h/a/a/a/d;->k:Ljava/lang/String;

    if-eqz v0, :cond_b

    .line 512
    const/16 v0, 0x10

    iget-object v1, p0, Lcom/google/h/a/a/a/d;->k:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 514
    :cond_b
    iget-object v0, p0, Lcom/google/h/a/a/a/d;->l:Ljava/lang/Integer;

    if-eqz v0, :cond_c

    .line 515
    const/16 v0, 0x11

    iget-object v1, p0, Lcom/google/h/a/a/a/d;->l:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 517
    :cond_c
    iget-object v0, p0, Lcom/google/h/a/a/a/d;->m:Ljava/lang/Boolean;

    if-eqz v0, :cond_d

    .line 518
    const/16 v0, 0x12

    iget-object v1, p0, Lcom/google/h/a/a/a/d;->m:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 520
    :cond_d
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/d;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 521
    return-void
.end method
