.class public final Lcom/google/h/a/a/a/j;
.super Lcom/google/protobuf/nano/d;
.source "SourceFile"


# static fields
.field private static volatile d:[Lcom/google/h/a/a/a/j;


# instance fields
.field public a:Ljava/lang/Integer;

.field public b:Ljava/lang/Integer;

.field public c:Ljava/lang/Integer;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 2899
    invoke-direct {p0}, Lcom/google/protobuf/nano/d;-><init>()V

    .line 2900
    iput-object v0, p0, Lcom/google/h/a/a/a/j;->a:Ljava/lang/Integer;

    iput-object v0, p0, Lcom/google/h/a/a/a/j;->b:Ljava/lang/Integer;

    iput-object v0, p0, Lcom/google/h/a/a/a/j;->c:Ljava/lang/Integer;

    iput-object v0, p0, Lcom/google/h/a/a/a/j;->unknownFieldData:Lcom/google/protobuf/nano/f;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/h/a/a/a/j;->cachedSize:I

    .line 2901
    return-void
.end method

.method public static a()[Lcom/google/h/a/a/a/j;
    .locals 2

    .prologue
    .line 2879
    sget-object v0, Lcom/google/h/a/a/a/j;->d:[Lcom/google/h/a/a/a/j;

    if-nez v0, :cond_1

    .line 2880
    sget-object v1, Lcom/google/protobuf/nano/h;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 2882
    :try_start_0
    sget-object v0, Lcom/google/h/a/a/a/j;->d:[Lcom/google/h/a/a/a/j;

    if-nez v0, :cond_0

    .line 2883
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/h/a/a/a/j;

    sput-object v0, Lcom/google/h/a/a/a/j;->d:[Lcom/google/h/a/a/a/j;

    .line 2885
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2887
    :cond_1
    sget-object v0, Lcom/google/h/a/a/a/j;->d:[Lcom/google/h/a/a/a/j;

    return-object v0

    .line 2885
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 2923
    invoke-super {p0}, Lcom/google/protobuf/nano/d;->computeSerializedSize()I

    move-result v0

    .line 2924
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/h/a/a/a/j;->a:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 2926
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/h/a/a/a/j;->b:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 2928
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/h/a/a/a/j;->c:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 2930
    return v0
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 2873
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/h/a/a/a/j;->storeUnknownField(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/h/a/a/a/j;->a:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/h/a/a/a/j;->b:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/h/a/a/a/j;->c:Ljava/lang/Integer;

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 2915
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/h/a/a/a/j;->a:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 2916
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/h/a/a/a/j;->b:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 2917
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/h/a/a/a/j;->c:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 2918
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/d;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 2919
    return-void
.end method
