.class public final Lcom/google/h/a/a/a/l;
.super Lcom/google/protobuf/nano/d;
.source "SourceFile"


# instance fields
.field public a:[B

.field public b:[B

.field public c:Ljava/lang/Integer;

.field public d:Ljava/lang/Boolean;

.field public e:Ljava/lang/Long;

.field public f:Ljava/lang/Long;

.field public g:Ljava/lang/Integer;

.field public h:Ljava/lang/Boolean;

.field public i:Ljava/lang/Long;

.field public j:Ljava/lang/Long;

.field public k:Ljava/lang/String;

.field public l:Ljava/lang/String;

.field public m:Ljava/lang/String;

.field public n:Ljava/lang/String;

.field public o:Ljava/lang/String;

.field public p:Ljava/lang/Integer;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 777
    invoke-direct {p0}, Lcom/google/protobuf/nano/d;-><init>()V

    .line 778
    iput-object v0, p0, Lcom/google/h/a/a/a/l;->a:[B

    iput-object v0, p0, Lcom/google/h/a/a/a/l;->b:[B

    iput-object v0, p0, Lcom/google/h/a/a/a/l;->c:Ljava/lang/Integer;

    iput-object v0, p0, Lcom/google/h/a/a/a/l;->d:Ljava/lang/Boolean;

    iput-object v0, p0, Lcom/google/h/a/a/a/l;->e:Ljava/lang/Long;

    iput-object v0, p0, Lcom/google/h/a/a/a/l;->f:Ljava/lang/Long;

    iput-object v0, p0, Lcom/google/h/a/a/a/l;->g:Ljava/lang/Integer;

    iput-object v0, p0, Lcom/google/h/a/a/a/l;->h:Ljava/lang/Boolean;

    iput-object v0, p0, Lcom/google/h/a/a/a/l;->i:Ljava/lang/Long;

    iput-object v0, p0, Lcom/google/h/a/a/a/l;->j:Ljava/lang/Long;

    iput-object v0, p0, Lcom/google/h/a/a/a/l;->k:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/h/a/a/a/l;->l:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/h/a/a/a/l;->m:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/h/a/a/a/l;->n:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/h/a/a/a/l;->o:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/h/a/a/a/l;->p:Ljava/lang/Integer;

    iput-object v0, p0, Lcom/google/h/a/a/a/l;->unknownFieldData:Lcom/google/protobuf/nano/f;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/h/a/a/a/l;->cachedSize:I

    .line 779
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 4

    .prologue
    .line 855
    invoke-super {p0}, Lcom/google/protobuf/nano/d;->computeSerializedSize()I

    move-result v0

    .line 856
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/h/a/a/a/l;->a:[B

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(I[B)I

    move-result v1

    add-int/2addr v0, v1

    .line 858
    iget-object v1, p0, Lcom/google/h/a/a/a/l;->b:[B

    if-eqz v1, :cond_0

    .line 859
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/h/a/a/a/l;->b:[B

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(I[B)I

    move-result v1

    add-int/2addr v0, v1

    .line 862
    :cond_0
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/h/a/a/a/l;->c:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->g(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 864
    iget-object v1, p0, Lcom/google/h/a/a/a/l;->d:Ljava/lang/Boolean;

    if-eqz v1, :cond_1

    .line 865
    const/4 v1, 0x5

    iget-object v2, p0, Lcom/google/h/a/a/a/l;->d:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 868
    :cond_1
    iget-object v1, p0, Lcom/google/h/a/a/a/l;->i:Ljava/lang/Long;

    if-eqz v1, :cond_2

    .line 869
    const/4 v1, 0x6

    iget-object v2, p0, Lcom/google/h/a/a/a/l;->i:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/b;->e(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 872
    :cond_2
    iget-object v1, p0, Lcom/google/h/a/a/a/l;->f:Ljava/lang/Long;

    if-eqz v1, :cond_3

    .line 873
    const/16 v1, 0x8

    iget-object v2, p0, Lcom/google/h/a/a/a/l;->f:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/b;->e(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 876
    :cond_3
    iget-object v1, p0, Lcom/google/h/a/a/a/l;->e:Ljava/lang/Long;

    if-eqz v1, :cond_4

    .line 877
    const/16 v1, 0x9

    iget-object v2, p0, Lcom/google/h/a/a/a/l;->e:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/b;->e(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 880
    :cond_4
    iget-object v1, p0, Lcom/google/h/a/a/a/l;->g:Ljava/lang/Integer;

    if-eqz v1, :cond_5

    .line 881
    const/16 v1, 0xa

    iget-object v2, p0, Lcom/google/h/a/a/a/l;->g:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->g(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 884
    :cond_5
    iget-object v1, p0, Lcom/google/h/a/a/a/l;->j:Ljava/lang/Long;

    if-eqz v1, :cond_6

    .line 885
    const/16 v1, 0xb

    iget-object v2, p0, Lcom/google/h/a/a/a/l;->j:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/b;->e(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 888
    :cond_6
    iget-object v1, p0, Lcom/google/h/a/a/a/l;->k:Ljava/lang/String;

    if-eqz v1, :cond_7

    .line 889
    const/16 v1, 0xc

    iget-object v2, p0, Lcom/google/h/a/a/a/l;->k:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 892
    :cond_7
    iget-object v1, p0, Lcom/google/h/a/a/a/l;->l:Ljava/lang/String;

    if-eqz v1, :cond_8

    .line 893
    const/16 v1, 0xd

    iget-object v2, p0, Lcom/google/h/a/a/a/l;->l:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 896
    :cond_8
    iget-object v1, p0, Lcom/google/h/a/a/a/l;->m:Ljava/lang/String;

    if-eqz v1, :cond_9

    .line 897
    const/16 v1, 0xe

    iget-object v2, p0, Lcom/google/h/a/a/a/l;->m:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 900
    :cond_9
    iget-object v1, p0, Lcom/google/h/a/a/a/l;->h:Ljava/lang/Boolean;

    if-eqz v1, :cond_a

    .line 901
    const/16 v1, 0xf

    iget-object v2, p0, Lcom/google/h/a/a/a/l;->h:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 904
    :cond_a
    iget-object v1, p0, Lcom/google/h/a/a/a/l;->n:Ljava/lang/String;

    if-eqz v1, :cond_b

    .line 905
    const/16 v1, 0x10

    iget-object v2, p0, Lcom/google/h/a/a/a/l;->n:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 908
    :cond_b
    iget-object v1, p0, Lcom/google/h/a/a/a/l;->o:Ljava/lang/String;

    if-eqz v1, :cond_c

    .line 909
    const/16 v1, 0x11

    iget-object v2, p0, Lcom/google/h/a/a/a/l;->o:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 912
    :cond_c
    iget-object v1, p0, Lcom/google/h/a/a/a/l;->p:Ljava/lang/Integer;

    if-eqz v1, :cond_d

    .line 913
    const/16 v1, 0x12

    iget-object v2, p0, Lcom/google/h/a/a/a/l;->p:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->g(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 916
    :cond_d
    return v0
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 2

    .prologue
    .line 712
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/h/a/a/a/l;->storeUnknownField(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->f()[B

    move-result-object v0

    iput-object v0, p0, Lcom/google/h/a/a/a/l;->a:[B

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->f()[B

    move-result-object v0

    iput-object v0, p0, Lcom/google/h/a/a/a/l;->b:[B

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/h/a/a/a/l;->c:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/h/a/a/a/l;->d:Ljava/lang/Boolean;

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->j()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/google/h/a/a/a/l;->i:Ljava/lang/Long;

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->j()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/google/h/a/a/a/l;->f:Ljava/lang/Long;

    goto :goto_0

    :sswitch_7
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->j()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/google/h/a/a/a/l;->e:Ljava/lang/Long;

    goto :goto_0

    :sswitch_8
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/h/a/a/a/l;->g:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_9
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->j()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/google/h/a/a/a/l;->j:Ljava/lang/Long;

    goto :goto_0

    :sswitch_a
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/h/a/a/a/l;->k:Ljava/lang/String;

    goto :goto_0

    :sswitch_b
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/h/a/a/a/l;->l:Ljava/lang/String;

    goto :goto_0

    :sswitch_c
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/h/a/a/a/l;->m:Ljava/lang/String;

    goto :goto_0

    :sswitch_d
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/h/a/a/a/l;->h:Ljava/lang/Boolean;

    goto/16 :goto_0

    :sswitch_e
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/h/a/a/a/l;->n:Ljava/lang/String;

    goto/16 :goto_0

    :sswitch_f
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/h/a/a/a/l;->o:Ljava/lang/String;

    goto/16 :goto_0

    :sswitch_10
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/h/a/a/a/l;->p:Ljava/lang/Integer;

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x28 -> :sswitch_4
        0x30 -> :sswitch_5
        0x40 -> :sswitch_6
        0x48 -> :sswitch_7
        0x50 -> :sswitch_8
        0x58 -> :sswitch_9
        0x62 -> :sswitch_a
        0x6a -> :sswitch_b
        0x72 -> :sswitch_c
        0x78 -> :sswitch_d
        0x82 -> :sswitch_e
        0x8a -> :sswitch_f
        0x90 -> :sswitch_10
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 4

    .prologue
    .line 806
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/h/a/a/a/l;->a:[B

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(I[B)V

    .line 807
    iget-object v0, p0, Lcom/google/h/a/a/a/l;->b:[B

    if-eqz v0, :cond_0

    .line 808
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/h/a/a/a/l;->b:[B

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(I[B)V

    .line 810
    :cond_0
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/h/a/a/a/l;->c:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->c(II)V

    .line 811
    iget-object v0, p0, Lcom/google/h/a/a/a/l;->d:Ljava/lang/Boolean;

    if-eqz v0, :cond_1

    .line 812
    const/4 v0, 0x5

    iget-object v1, p0, Lcom/google/h/a/a/a/l;->d:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 814
    :cond_1
    iget-object v0, p0, Lcom/google/h/a/a/a/l;->i:Ljava/lang/Long;

    if-eqz v0, :cond_2

    .line 815
    const/4 v0, 0x6

    iget-object v1, p0, Lcom/google/h/a/a/a/l;->i:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/b;->a(IJ)V

    .line 817
    :cond_2
    iget-object v0, p0, Lcom/google/h/a/a/a/l;->f:Ljava/lang/Long;

    if-eqz v0, :cond_3

    .line 818
    const/16 v0, 0x8

    iget-object v1, p0, Lcom/google/h/a/a/a/l;->f:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/b;->a(IJ)V

    .line 820
    :cond_3
    iget-object v0, p0, Lcom/google/h/a/a/a/l;->e:Ljava/lang/Long;

    if-eqz v0, :cond_4

    .line 821
    const/16 v0, 0x9

    iget-object v1, p0, Lcom/google/h/a/a/a/l;->e:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/b;->a(IJ)V

    .line 823
    :cond_4
    iget-object v0, p0, Lcom/google/h/a/a/a/l;->g:Ljava/lang/Integer;

    if-eqz v0, :cond_5

    .line 824
    const/16 v0, 0xa

    iget-object v1, p0, Lcom/google/h/a/a/a/l;->g:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->c(II)V

    .line 826
    :cond_5
    iget-object v0, p0, Lcom/google/h/a/a/a/l;->j:Ljava/lang/Long;

    if-eqz v0, :cond_6

    .line 827
    const/16 v0, 0xb

    iget-object v1, p0, Lcom/google/h/a/a/a/l;->j:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/b;->a(IJ)V

    .line 829
    :cond_6
    iget-object v0, p0, Lcom/google/h/a/a/a/l;->k:Ljava/lang/String;

    if-eqz v0, :cond_7

    .line 830
    const/16 v0, 0xc

    iget-object v1, p0, Lcom/google/h/a/a/a/l;->k:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 832
    :cond_7
    iget-object v0, p0, Lcom/google/h/a/a/a/l;->l:Ljava/lang/String;

    if-eqz v0, :cond_8

    .line 833
    const/16 v0, 0xd

    iget-object v1, p0, Lcom/google/h/a/a/a/l;->l:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 835
    :cond_8
    iget-object v0, p0, Lcom/google/h/a/a/a/l;->m:Ljava/lang/String;

    if-eqz v0, :cond_9

    .line 836
    const/16 v0, 0xe

    iget-object v1, p0, Lcom/google/h/a/a/a/l;->m:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 838
    :cond_9
    iget-object v0, p0, Lcom/google/h/a/a/a/l;->h:Ljava/lang/Boolean;

    if-eqz v0, :cond_a

    .line 839
    const/16 v0, 0xf

    iget-object v1, p0, Lcom/google/h/a/a/a/l;->h:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 841
    :cond_a
    iget-object v0, p0, Lcom/google/h/a/a/a/l;->n:Ljava/lang/String;

    if-eqz v0, :cond_b

    .line 842
    const/16 v0, 0x10

    iget-object v1, p0, Lcom/google/h/a/a/a/l;->n:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 844
    :cond_b
    iget-object v0, p0, Lcom/google/h/a/a/a/l;->o:Ljava/lang/String;

    if-eqz v0, :cond_c

    .line 845
    const/16 v0, 0x11

    iget-object v1, p0, Lcom/google/h/a/a/a/l;->o:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 847
    :cond_c
    iget-object v0, p0, Lcom/google/h/a/a/a/l;->p:Ljava/lang/Integer;

    if-eqz v0, :cond_d

    .line 848
    const/16 v0, 0x12

    iget-object v1, p0, Lcom/google/h/a/a/a/l;->p:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->c(II)V

    .line 850
    :cond_d
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/d;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 851
    return-void
.end method
