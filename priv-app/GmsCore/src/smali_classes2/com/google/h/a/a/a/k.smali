.class public final Lcom/google/h/a/a/a/k;
.super Lcom/google/protobuf/nano/d;
.source "SourceFile"


# static fields
.field private static volatile e:[Lcom/google/h/a/a/a/k;


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/lang/String;

.field public c:Ljava/lang/Long;

.field public d:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 3131
    invoke-direct {p0}, Lcom/google/protobuf/nano/d;-><init>()V

    .line 3132
    iput-object v0, p0, Lcom/google/h/a/a/a/k;->a:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/h/a/a/a/k;->b:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/h/a/a/a/k;->c:Ljava/lang/Long;

    iput-object v0, p0, Lcom/google/h/a/a/a/k;->d:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/h/a/a/a/k;->unknownFieldData:Lcom/google/protobuf/nano/f;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/h/a/a/a/k;->cachedSize:I

    .line 3133
    return-void
.end method

.method public static a()[Lcom/google/h/a/a/a/k;
    .locals 2

    .prologue
    .line 3108
    sget-object v0, Lcom/google/h/a/a/a/k;->e:[Lcom/google/h/a/a/a/k;

    if-nez v0, :cond_1

    .line 3109
    sget-object v1, Lcom/google/protobuf/nano/h;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 3111
    :try_start_0
    sget-object v0, Lcom/google/h/a/a/a/k;->e:[Lcom/google/h/a/a/a/k;

    if-nez v0, :cond_0

    .line 3112
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/h/a/a/a/k;

    sput-object v0, Lcom/google/h/a/a/a/k;->e:[Lcom/google/h/a/a/a/k;

    .line 3114
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 3116
    :cond_1
    sget-object v0, Lcom/google/h/a/a/a/k;->e:[Lcom/google/h/a/a/a/k;

    return-object v0

    .line 3114
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 4

    .prologue
    .line 3165
    invoke-super {p0}, Lcom/google/protobuf/nano/d;->computeSerializedSize()I

    move-result v0

    .line 3166
    iget-object v1, p0, Lcom/google/h/a/a/a/k;->a:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 3167
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/h/a/a/a/k;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3170
    :cond_0
    iget-object v1, p0, Lcom/google/h/a/a/a/k;->b:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 3171
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/h/a/a/a/k;->b:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3174
    :cond_1
    iget-object v1, p0, Lcom/google/h/a/a/a/k;->c:Ljava/lang/Long;

    if-eqz v1, :cond_2

    .line 3175
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/h/a/a/a/k;->c:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/b;->f(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 3178
    :cond_2
    iget-object v1, p0, Lcom/google/h/a/a/a/k;->d:Ljava/lang/String;

    if-eqz v1, :cond_3

    .line 3179
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/h/a/a/a/k;->d:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3182
    :cond_3
    return v0
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 2

    .prologue
    .line 3102
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/h/a/a/a/k;->storeUnknownField(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/h/a/a/a/k;->a:Ljava/lang/String;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/h/a/a/a/k;->b:Ljava/lang/String;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->j()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/google/h/a/a/a/k;->c:Ljava/lang/Long;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/h/a/a/a/k;->d:Ljava/lang/String;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 4

    .prologue
    .line 3148
    iget-object v0, p0, Lcom/google/h/a/a/a/k;->a:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 3149
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/h/a/a/a/k;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 3151
    :cond_0
    iget-object v0, p0, Lcom/google/h/a/a/a/k;->b:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 3152
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/h/a/a/a/k;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 3154
    :cond_1
    iget-object v0, p0, Lcom/google/h/a/a/a/k;->c:Ljava/lang/Long;

    if-eqz v0, :cond_2

    .line 3155
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/h/a/a/a/k;->c:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/b;->b(IJ)V

    .line 3157
    :cond_2
    iget-object v0, p0, Lcom/google/h/a/a/a/k;->d:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 3158
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/h/a/a/a/k;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 3160
    :cond_3
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/d;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 3161
    return-void
.end method
