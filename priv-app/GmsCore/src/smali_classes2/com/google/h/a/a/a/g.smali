.class public final Lcom/google/h/a/a/a/g;
.super Lcom/google/protobuf/nano/d;
.source "SourceFile"


# static fields
.field private static volatile k:[Lcom/google/h/a/a/a/g;


# instance fields
.field public a:Ljava/lang/String;

.field public b:[I

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/String;

.field public e:Ljava/lang/Boolean;

.field public f:Ljava/lang/Boolean;

.field public g:Ljava/lang/Boolean;

.field public h:Ljava/lang/Integer;

.field public i:Ljava/lang/Boolean;

.field public j:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 174
    invoke-direct {p0}, Lcom/google/protobuf/nano/d;-><init>()V

    .line 175
    iput-object v1, p0, Lcom/google/h/a/a/a/g;->a:Ljava/lang/String;

    sget-object v0, Lcom/google/protobuf/nano/m;->a:[I

    iput-object v0, p0, Lcom/google/h/a/a/a/g;->b:[I

    iput-object v1, p0, Lcom/google/h/a/a/a/g;->c:Ljava/lang/String;

    iput-object v1, p0, Lcom/google/h/a/a/a/g;->d:Ljava/lang/String;

    iput-object v1, p0, Lcom/google/h/a/a/a/g;->e:Ljava/lang/Boolean;

    iput-object v1, p0, Lcom/google/h/a/a/a/g;->f:Ljava/lang/Boolean;

    iput-object v1, p0, Lcom/google/h/a/a/a/g;->g:Ljava/lang/Boolean;

    iput-object v1, p0, Lcom/google/h/a/a/a/g;->h:Ljava/lang/Integer;

    iput-object v1, p0, Lcom/google/h/a/a/a/g;->i:Ljava/lang/Boolean;

    iput-object v1, p0, Lcom/google/h/a/a/a/g;->j:Ljava/lang/Boolean;

    iput-object v1, p0, Lcom/google/h/a/a/a/g;->unknownFieldData:Lcom/google/protobuf/nano/f;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/h/a/a/a/g;->cachedSize:I

    .line 176
    return-void
.end method

.method public static a()[Lcom/google/h/a/a/a/g;
    .locals 2

    .prologue
    .line 133
    sget-object v0, Lcom/google/h/a/a/a/g;->k:[Lcom/google/h/a/a/a/g;

    if-nez v0, :cond_1

    .line 134
    sget-object v1, Lcom/google/protobuf/nano/h;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 136
    :try_start_0
    sget-object v0, Lcom/google/h/a/a/a/g;->k:[Lcom/google/h/a/a/a/g;

    if-nez v0, :cond_0

    .line 137
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/h/a/a/a/g;

    sput-object v0, Lcom/google/h/a/a/a/g;->k:[Lcom/google/h/a/a/a/g;

    .line 139
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 141
    :cond_1
    sget-object v0, Lcom/google/h/a/a/a/g;->k:[Lcom/google/h/a/a/a/g;

    return-object v0

    .line 139
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 234
    invoke-super {p0}, Lcom/google/protobuf/nano/d;->computeSerializedSize()I

    move-result v0

    .line 235
    iget-object v2, p0, Lcom/google/h/a/a/a/g;->a:Ljava/lang/String;

    if-eqz v2, :cond_0

    .line 236
    const/4 v2, 0x1

    iget-object v3, p0, Lcom/google/h/a/a/a/g;->a:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 239
    :cond_0
    iget-object v2, p0, Lcom/google/h/a/a/a/g;->d:Ljava/lang/String;

    if-eqz v2, :cond_1

    .line 240
    const/4 v2, 0x2

    iget-object v3, p0, Lcom/google/h/a/a/a/g;->d:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 243
    :cond_1
    iget-object v2, p0, Lcom/google/h/a/a/a/g;->e:Ljava/lang/Boolean;

    if-eqz v2, :cond_2

    .line 244
    const/4 v2, 0x3

    iget-object v3, p0, Lcom/google/h/a/a/a/g;->e:Ljava/lang/Boolean;

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v2}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 247
    :cond_2
    iget-object v2, p0, Lcom/google/h/a/a/a/g;->f:Ljava/lang/Boolean;

    if-eqz v2, :cond_3

    .line 248
    const/4 v2, 0x4

    iget-object v3, p0, Lcom/google/h/a/a/a/g;->f:Ljava/lang/Boolean;

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v2}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 251
    :cond_3
    iget-object v2, p0, Lcom/google/h/a/a/a/g;->g:Ljava/lang/Boolean;

    if-eqz v2, :cond_4

    .line 252
    const/4 v2, 0x5

    iget-object v3, p0, Lcom/google/h/a/a/a/g;->g:Ljava/lang/Boolean;

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v2}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 255
    :cond_4
    iget-object v2, p0, Lcom/google/h/a/a/a/g;->h:Ljava/lang/Integer;

    if-eqz v2, :cond_5

    .line 256
    const/4 v2, 0x6

    iget-object v3, p0, Lcom/google/h/a/a/a/g;->h:Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 259
    :cond_5
    iget-object v2, p0, Lcom/google/h/a/a/a/g;->i:Ljava/lang/Boolean;

    if-eqz v2, :cond_6

    .line 260
    const/4 v2, 0x7

    iget-object v3, p0, Lcom/google/h/a/a/a/g;->i:Ljava/lang/Boolean;

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v2}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 263
    :cond_6
    iget-object v2, p0, Lcom/google/h/a/a/a/g;->b:[I

    if-eqz v2, :cond_8

    iget-object v2, p0, Lcom/google/h/a/a/a/g;->b:[I

    array-length v2, v2

    if-lez v2, :cond_8

    move v2, v1

    .line 265
    :goto_0
    iget-object v3, p0, Lcom/google/h/a/a/a/g;->b:[I

    array-length v3, v3

    if-ge v1, v3, :cond_7

    .line 266
    iget-object v3, p0, Lcom/google/h/a/a/a/g;->b:[I

    aget v3, v3, v1

    .line 267
    invoke-static {v3}, Lcom/google/protobuf/nano/b;->b(I)I

    move-result v3

    add-int/2addr v2, v3

    .line 265
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 270
    :cond_7
    add-int/2addr v0, v2

    .line 271
    iget-object v1, p0, Lcom/google/h/a/a/a/g;->b:[I

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 273
    :cond_8
    iget-object v1, p0, Lcom/google/h/a/a/a/g;->j:Ljava/lang/Boolean;

    if-eqz v1, :cond_9

    .line 274
    const/16 v1, 0x9

    iget-object v2, p0, Lcom/google/h/a/a/a/g;->j:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 277
    :cond_9
    iget-object v1, p0, Lcom/google/h/a/a/a/g;->c:Ljava/lang/String;

    if-eqz v1, :cond_a

    .line 278
    const/16 v1, 0xa

    iget-object v2, p0, Lcom/google/h/a/a/a/g;->c:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 281
    :cond_a
    return v0
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 127
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/h/a/a/a/g;->storeUnknownField(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/h/a/a/a/g;->a:Ljava/lang/String;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/h/a/a/a/g;->d:Ljava/lang/String;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/h/a/a/a/g;->e:Ljava/lang/Boolean;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/h/a/a/a/g;->f:Ljava/lang/Boolean;

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/h/a/a/a/g;->g:Ljava/lang/Boolean;

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/h/a/a/a/g;->h:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_7
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/h/a/a/a/g;->i:Ljava/lang/Boolean;

    goto :goto_0

    :sswitch_8
    const/16 v0, 0x40

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->b(Lcom/google/protobuf/nano/a;I)I

    move-result v2

    iget-object v0, p0, Lcom/google/h/a/a/a/g;->b:[I

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [I

    if-eqz v0, :cond_1

    iget-object v3, p0, Lcom/google/h/a/a/a/g;->b:[I

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_1
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_3

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v3

    aput v3, v2, v0

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    iget-object v0, p0, Lcom/google/h/a/a/a/g;->b:[I

    array-length v0, v0

    goto :goto_1

    :cond_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v3

    aput v3, v2, v0

    iput-object v2, p0, Lcom/google/h/a/a/a/g;->b:[I

    goto/16 :goto_0

    :sswitch_9
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->c(I)I

    move-result v3

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->o()I

    move-result v2

    move v0, v1

    :goto_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->m()I

    move-result v4

    if-lez v4, :cond_4

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    :cond_4
    invoke-virtual {p1, v2}, Lcom/google/protobuf/nano/a;->e(I)V

    iget-object v2, p0, Lcom/google/h/a/a/a/g;->b:[I

    if-nez v2, :cond_6

    move v2, v1

    :goto_4
    add-int/2addr v0, v2

    new-array v0, v0, [I

    if-eqz v2, :cond_5

    iget-object v4, p0, Lcom/google/h/a/a/a/g;->b:[I

    invoke-static {v4, v1, v0, v1, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_5
    :goto_5
    array-length v4, v0

    if-ge v2, v4, :cond_7

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v4

    aput v4, v0, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_5

    :cond_6
    iget-object v2, p0, Lcom/google/h/a/a/a/g;->b:[I

    array-length v2, v2

    goto :goto_4

    :cond_7
    iput-object v0, p0, Lcom/google/h/a/a/a/g;->b:[I

    invoke-virtual {p1, v3}, Lcom/google/protobuf/nano/a;->d(I)V

    goto/16 :goto_0

    :sswitch_a
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/h/a/a/a/g;->j:Ljava/lang/Boolean;

    goto/16 :goto_0

    :sswitch_b
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/h/a/a/a/g;->c:Ljava/lang/String;

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
        0x30 -> :sswitch_6
        0x38 -> :sswitch_7
        0x40 -> :sswitch_8
        0x42 -> :sswitch_9
        0x48 -> :sswitch_a
        0x52 -> :sswitch_b
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 3

    .prologue
    .line 197
    iget-object v0, p0, Lcom/google/h/a/a/a/g;->a:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 198
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/h/a/a/a/g;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 200
    :cond_0
    iget-object v0, p0, Lcom/google/h/a/a/a/g;->d:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 201
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/h/a/a/a/g;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 203
    :cond_1
    iget-object v0, p0, Lcom/google/h/a/a/a/g;->e:Ljava/lang/Boolean;

    if-eqz v0, :cond_2

    .line 204
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/h/a/a/a/g;->e:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 206
    :cond_2
    iget-object v0, p0, Lcom/google/h/a/a/a/g;->f:Ljava/lang/Boolean;

    if-eqz v0, :cond_3

    .line 207
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/h/a/a/a/g;->f:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 209
    :cond_3
    iget-object v0, p0, Lcom/google/h/a/a/a/g;->g:Ljava/lang/Boolean;

    if-eqz v0, :cond_4

    .line 210
    const/4 v0, 0x5

    iget-object v1, p0, Lcom/google/h/a/a/a/g;->g:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 212
    :cond_4
    iget-object v0, p0, Lcom/google/h/a/a/a/g;->h:Ljava/lang/Integer;

    if-eqz v0, :cond_5

    .line 213
    const/4 v0, 0x6

    iget-object v1, p0, Lcom/google/h/a/a/a/g;->h:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 215
    :cond_5
    iget-object v0, p0, Lcom/google/h/a/a/a/g;->i:Ljava/lang/Boolean;

    if-eqz v0, :cond_6

    .line 216
    const/4 v0, 0x7

    iget-object v1, p0, Lcom/google/h/a/a/a/g;->i:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 218
    :cond_6
    iget-object v0, p0, Lcom/google/h/a/a/a/g;->b:[I

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/google/h/a/a/a/g;->b:[I

    array-length v0, v0

    if-lez v0, :cond_7

    .line 219
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/google/h/a/a/a/g;->b:[I

    array-length v1, v1

    if-ge v0, v1, :cond_7

    .line 220
    const/16 v1, 0x8

    iget-object v2, p0, Lcom/google/h/a/a/a/g;->b:[I

    aget v2, v2, v0

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 219
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 223
    :cond_7
    iget-object v0, p0, Lcom/google/h/a/a/a/g;->j:Ljava/lang/Boolean;

    if-eqz v0, :cond_8

    .line 224
    const/16 v0, 0x9

    iget-object v1, p0, Lcom/google/h/a/a/a/g;->j:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 226
    :cond_8
    iget-object v0, p0, Lcom/google/h/a/a/a/g;->c:Ljava/lang/String;

    if-eqz v0, :cond_9

    .line 227
    const/16 v0, 0xa

    iget-object v1, p0, Lcom/google/h/a/a/a/g;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 229
    :cond_9
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/d;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 230
    return-void
.end method
