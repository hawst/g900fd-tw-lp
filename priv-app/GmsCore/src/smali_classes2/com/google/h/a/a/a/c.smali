.class public final Lcom/google/h/a/a/a/c;
.super Lcom/google/protobuf/nano/d;
.source "SourceFile"


# static fields
.field private static volatile i:[Lcom/google/h/a/a/a/c;


# instance fields
.field public a:Ljava/lang/Integer;

.field public b:Ljava/lang/String;

.field public c:Ljava/lang/Integer;

.field public d:Ljava/lang/String;

.field public e:Ljava/lang/Integer;

.field public f:Ljava/lang/Boolean;

.field public g:Ljava/lang/Boolean;

.field public h:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1055
    invoke-direct {p0}, Lcom/google/protobuf/nano/d;-><init>()V

    .line 1056
    iput-object v0, p0, Lcom/google/h/a/a/a/c;->a:Ljava/lang/Integer;

    iput-object v0, p0, Lcom/google/h/a/a/a/c;->b:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/h/a/a/a/c;->c:Ljava/lang/Integer;

    iput-object v0, p0, Lcom/google/h/a/a/a/c;->d:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/h/a/a/a/c;->e:Ljava/lang/Integer;

    iput-object v0, p0, Lcom/google/h/a/a/a/c;->f:Ljava/lang/Boolean;

    iput-object v0, p0, Lcom/google/h/a/a/a/c;->g:Ljava/lang/Boolean;

    iput-object v0, p0, Lcom/google/h/a/a/a/c;->h:Ljava/lang/Boolean;

    iput-object v0, p0, Lcom/google/h/a/a/a/c;->unknownFieldData:Lcom/google/protobuf/nano/f;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/h/a/a/a/c;->cachedSize:I

    .line 1057
    return-void
.end method

.method public static a()[Lcom/google/h/a/a/a/c;
    .locals 2

    .prologue
    .line 1020
    sget-object v0, Lcom/google/h/a/a/a/c;->i:[Lcom/google/h/a/a/a/c;

    if-nez v0, :cond_1

    .line 1021
    sget-object v1, Lcom/google/protobuf/nano/h;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 1023
    :try_start_0
    sget-object v0, Lcom/google/h/a/a/a/c;->i:[Lcom/google/h/a/a/a/c;

    if-nez v0, :cond_0

    .line 1024
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/h/a/a/a/c;

    sput-object v0, Lcom/google/h/a/a/a/c;->i:[Lcom/google/h/a/a/a/c;

    .line 1026
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1028
    :cond_1
    sget-object v0, Lcom/google/h/a/a/a/c;->i:[Lcom/google/h/a/a/a/c;

    return-object v0

    .line 1026
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 1105
    invoke-super {p0}, Lcom/google/protobuf/nano/d;->computeSerializedSize()I

    move-result v0

    .line 1106
    iget-object v1, p0, Lcom/google/h/a/a/a/c;->a:Ljava/lang/Integer;

    if-eqz v1, :cond_0

    .line 1107
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/h/a/a/a/c;->a:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1110
    :cond_0
    iget-object v1, p0, Lcom/google/h/a/a/a/c;->b:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 1111
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/h/a/a/a/c;->b:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1114
    :cond_1
    iget-object v1, p0, Lcom/google/h/a/a/a/c;->c:Ljava/lang/Integer;

    if-eqz v1, :cond_2

    .line 1115
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/h/a/a/a/c;->c:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1118
    :cond_2
    iget-object v1, p0, Lcom/google/h/a/a/a/c;->d:Ljava/lang/String;

    if-eqz v1, :cond_3

    .line 1119
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/h/a/a/a/c;->d:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1122
    :cond_3
    iget-object v1, p0, Lcom/google/h/a/a/a/c;->e:Ljava/lang/Integer;

    if-eqz v1, :cond_4

    .line 1123
    const/4 v1, 0x5

    iget-object v2, p0, Lcom/google/h/a/a/a/c;->e:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1126
    :cond_4
    iget-object v1, p0, Lcom/google/h/a/a/a/c;->f:Ljava/lang/Boolean;

    if-eqz v1, :cond_5

    .line 1127
    const/4 v1, 0x6

    iget-object v2, p0, Lcom/google/h/a/a/a/c;->f:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 1130
    :cond_5
    iget-object v1, p0, Lcom/google/h/a/a/a/c;->g:Ljava/lang/Boolean;

    if-eqz v1, :cond_6

    .line 1131
    const/4 v1, 0x7

    iget-object v2, p0, Lcom/google/h/a/a/a/c;->g:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 1134
    :cond_6
    iget-object v1, p0, Lcom/google/h/a/a/a/c;->h:Ljava/lang/Boolean;

    if-eqz v1, :cond_7

    .line 1135
    const/16 v1, 0x8

    iget-object v2, p0, Lcom/google/h/a/a/a/c;->h:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 1138
    :cond_7
    return v0
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 1014
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/h/a/a/a/c;->storeUnknownField(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/h/a/a/a/c;->a:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/h/a/a/a/c;->b:Ljava/lang/String;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/h/a/a/a/c;->c:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/h/a/a/a/c;->d:Ljava/lang/String;

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/h/a/a/a/c;->e:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/h/a/a/a/c;->f:Ljava/lang/Boolean;

    goto :goto_0

    :sswitch_7
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/h/a/a/a/c;->g:Ljava/lang/Boolean;

    goto :goto_0

    :sswitch_8
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/h/a/a/a/c;->h:Ljava/lang/Boolean;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
        0x28 -> :sswitch_5
        0x30 -> :sswitch_6
        0x38 -> :sswitch_7
        0x40 -> :sswitch_8
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 1076
    iget-object v0, p0, Lcom/google/h/a/a/a/c;->a:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 1077
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/h/a/a/a/c;->a:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 1079
    :cond_0
    iget-object v0, p0, Lcom/google/h/a/a/a/c;->b:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 1080
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/h/a/a/a/c;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 1082
    :cond_1
    iget-object v0, p0, Lcom/google/h/a/a/a/c;->c:Ljava/lang/Integer;

    if-eqz v0, :cond_2

    .line 1083
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/h/a/a/a/c;->c:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 1085
    :cond_2
    iget-object v0, p0, Lcom/google/h/a/a/a/c;->d:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 1086
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/h/a/a/a/c;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 1088
    :cond_3
    iget-object v0, p0, Lcom/google/h/a/a/a/c;->e:Ljava/lang/Integer;

    if-eqz v0, :cond_4

    .line 1089
    const/4 v0, 0x5

    iget-object v1, p0, Lcom/google/h/a/a/a/c;->e:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 1091
    :cond_4
    iget-object v0, p0, Lcom/google/h/a/a/a/c;->f:Ljava/lang/Boolean;

    if-eqz v0, :cond_5

    .line 1092
    const/4 v0, 0x6

    iget-object v1, p0, Lcom/google/h/a/a/a/c;->f:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 1094
    :cond_5
    iget-object v0, p0, Lcom/google/h/a/a/a/c;->g:Ljava/lang/Boolean;

    if-eqz v0, :cond_6

    .line 1095
    const/4 v0, 0x7

    iget-object v1, p0, Lcom/google/h/a/a/a/c;->g:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 1097
    :cond_6
    iget-object v0, p0, Lcom/google/h/a/a/a/c;->h:Ljava/lang/Boolean;

    if-eqz v0, :cond_7

    .line 1098
    const/16 v0, 0x8

    iget-object v1, p0, Lcom/google/h/a/a/a/c;->h:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 1100
    :cond_7
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/d;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 1101
    return-void
.end method
