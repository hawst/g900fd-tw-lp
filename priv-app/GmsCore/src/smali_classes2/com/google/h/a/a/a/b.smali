.class public final Lcom/google/h/a/a/a/b;
.super Lcom/google/protobuf/nano/d;
.source "SourceFile"


# static fields
.field private static volatile l:[Lcom/google/h/a/a/a/b;


# instance fields
.field public a:Ljava/lang/Integer;

.field public b:Ljava/lang/Long;

.field public c:Ljava/lang/Long;

.field public d:Ljava/lang/Integer;

.field public e:Ljava/lang/Integer;

.field public f:Ljava/lang/Integer;

.field public g:Ljava/lang/Boolean;

.field public h:Ljava/lang/Boolean;

.field public i:Ljava/lang/Boolean;

.field public j:Ljava/lang/Boolean;

.field public k:Ljava/lang/Integer;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 2470
    invoke-direct {p0}, Lcom/google/protobuf/nano/d;-><init>()V

    .line 2471
    iput-object v0, p0, Lcom/google/h/a/a/a/b;->a:Ljava/lang/Integer;

    iput-object v0, p0, Lcom/google/h/a/a/a/b;->b:Ljava/lang/Long;

    iput-object v0, p0, Lcom/google/h/a/a/a/b;->c:Ljava/lang/Long;

    iput-object v0, p0, Lcom/google/h/a/a/a/b;->d:Ljava/lang/Integer;

    iput-object v0, p0, Lcom/google/h/a/a/a/b;->e:Ljava/lang/Integer;

    iput-object v0, p0, Lcom/google/h/a/a/a/b;->f:Ljava/lang/Integer;

    iput-object v0, p0, Lcom/google/h/a/a/a/b;->g:Ljava/lang/Boolean;

    iput-object v0, p0, Lcom/google/h/a/a/a/b;->h:Ljava/lang/Boolean;

    iput-object v0, p0, Lcom/google/h/a/a/a/b;->i:Ljava/lang/Boolean;

    iput-object v0, p0, Lcom/google/h/a/a/a/b;->j:Ljava/lang/Boolean;

    iput-object v0, p0, Lcom/google/h/a/a/a/b;->k:Ljava/lang/Integer;

    iput-object v0, p0, Lcom/google/h/a/a/a/b;->unknownFieldData:Lcom/google/protobuf/nano/f;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/h/a/a/a/b;->cachedSize:I

    .line 2472
    return-void
.end method

.method public static a()[Lcom/google/h/a/a/a/b;
    .locals 2

    .prologue
    .line 2426
    sget-object v0, Lcom/google/h/a/a/a/b;->l:[Lcom/google/h/a/a/a/b;

    if-nez v0, :cond_1

    .line 2427
    sget-object v1, Lcom/google/protobuf/nano/h;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 2429
    :try_start_0
    sget-object v0, Lcom/google/h/a/a/a/b;->l:[Lcom/google/h/a/a/a/b;

    if-nez v0, :cond_0

    .line 2430
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/h/a/a/a/b;

    sput-object v0, Lcom/google/h/a/a/a/b;->l:[Lcom/google/h/a/a/a/b;

    .line 2432
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2434
    :cond_1
    sget-object v0, Lcom/google/h/a/a/a/b;->l:[Lcom/google/h/a/a/a/b;

    return-object v0

    .line 2432
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 4

    .prologue
    .line 2530
    invoke-super {p0}, Lcom/google/protobuf/nano/d;->computeSerializedSize()I

    move-result v0

    .line 2531
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/h/a/a/a/b;->a:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 2533
    iget-object v1, p0, Lcom/google/h/a/a/a/b;->b:Ljava/lang/Long;

    if-eqz v1, :cond_0

    .line 2534
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/h/a/a/a/b;->b:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/b;->f(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 2537
    :cond_0
    iget-object v1, p0, Lcom/google/h/a/a/a/b;->e:Ljava/lang/Integer;

    if-eqz v1, :cond_1

    .line 2538
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/h/a/a/a/b;->e:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 2541
    :cond_1
    iget-object v1, p0, Lcom/google/h/a/a/a/b;->f:Ljava/lang/Integer;

    if-eqz v1, :cond_2

    .line 2542
    const/4 v1, 0x5

    iget-object v2, p0, Lcom/google/h/a/a/a/b;->f:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 2545
    :cond_2
    iget-object v1, p0, Lcom/google/h/a/a/a/b;->g:Ljava/lang/Boolean;

    if-eqz v1, :cond_3

    .line 2546
    const/4 v1, 0x6

    iget-object v2, p0, Lcom/google/h/a/a/a/b;->g:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 2549
    :cond_3
    iget-object v1, p0, Lcom/google/h/a/a/a/b;->h:Ljava/lang/Boolean;

    if-eqz v1, :cond_4

    .line 2550
    const/4 v1, 0x7

    iget-object v2, p0, Lcom/google/h/a/a/a/b;->h:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 2553
    :cond_4
    iget-object v1, p0, Lcom/google/h/a/a/a/b;->i:Ljava/lang/Boolean;

    if-eqz v1, :cond_5

    .line 2554
    const/16 v1, 0x8

    iget-object v2, p0, Lcom/google/h/a/a/a/b;->i:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 2557
    :cond_5
    iget-object v1, p0, Lcom/google/h/a/a/a/b;->k:Ljava/lang/Integer;

    if-eqz v1, :cond_6

    .line 2558
    const/16 v1, 0xb

    iget-object v2, p0, Lcom/google/h/a/a/a/b;->k:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 2561
    :cond_6
    iget-object v1, p0, Lcom/google/h/a/a/a/b;->d:Ljava/lang/Integer;

    if-eqz v1, :cond_7

    .line 2562
    const/16 v1, 0xc

    iget-object v2, p0, Lcom/google/h/a/a/a/b;->d:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 2565
    :cond_7
    iget-object v1, p0, Lcom/google/h/a/a/a/b;->j:Ljava/lang/Boolean;

    if-eqz v1, :cond_8

    .line 2566
    const/16 v1, 0xd

    iget-object v2, p0, Lcom/google/h/a/a/a/b;->j:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 2569
    :cond_8
    iget-object v1, p0, Lcom/google/h/a/a/a/b;->c:Ljava/lang/Long;

    if-eqz v1, :cond_9

    .line 2570
    const/16 v1, 0xe

    iget-object v2, p0, Lcom/google/h/a/a/a/b;->c:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/b;->f(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 2573
    :cond_9
    return v0
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 2

    .prologue
    .line 2411
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/h/a/a/a/b;->storeUnknownField(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :pswitch_0
    goto :goto_0

    :pswitch_1
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/h/a/a/a/b;->a:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->j()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/google/h/a/a/a/b;->b:Ljava/lang/Long;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/h/a/a/a/b;->e:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/h/a/a/a/b;->f:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/h/a/a/a/b;->g:Ljava/lang/Boolean;

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/h/a/a/a/b;->h:Ljava/lang/Boolean;

    goto :goto_0

    :sswitch_7
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/h/a/a/a/b;->i:Ljava/lang/Boolean;

    goto :goto_0

    :sswitch_8
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/h/a/a/a/b;->k:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_9
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/h/a/a/a/b;->d:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_a
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/h/a/a/a/b;->j:Ljava/lang/Boolean;

    goto :goto_0

    :sswitch_b
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->j()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/google/h/a/a/a/b;->c:Ljava/lang/Long;

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x20 -> :sswitch_3
        0x28 -> :sswitch_4
        0x30 -> :sswitch_5
        0x38 -> :sswitch_6
        0x40 -> :sswitch_7
        0x58 -> :sswitch_8
        0x60 -> :sswitch_9
        0x68 -> :sswitch_a
        0x70 -> :sswitch_b
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 4

    .prologue
    .line 2494
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/h/a/a/a/b;->a:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 2495
    iget-object v0, p0, Lcom/google/h/a/a/a/b;->b:Ljava/lang/Long;

    if-eqz v0, :cond_0

    .line 2496
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/h/a/a/a/b;->b:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/b;->b(IJ)V

    .line 2498
    :cond_0
    iget-object v0, p0, Lcom/google/h/a/a/a/b;->e:Ljava/lang/Integer;

    if-eqz v0, :cond_1

    .line 2499
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/h/a/a/a/b;->e:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 2501
    :cond_1
    iget-object v0, p0, Lcom/google/h/a/a/a/b;->f:Ljava/lang/Integer;

    if-eqz v0, :cond_2

    .line 2502
    const/4 v0, 0x5

    iget-object v1, p0, Lcom/google/h/a/a/a/b;->f:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 2504
    :cond_2
    iget-object v0, p0, Lcom/google/h/a/a/a/b;->g:Ljava/lang/Boolean;

    if-eqz v0, :cond_3

    .line 2505
    const/4 v0, 0x6

    iget-object v1, p0, Lcom/google/h/a/a/a/b;->g:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 2507
    :cond_3
    iget-object v0, p0, Lcom/google/h/a/a/a/b;->h:Ljava/lang/Boolean;

    if-eqz v0, :cond_4

    .line 2508
    const/4 v0, 0x7

    iget-object v1, p0, Lcom/google/h/a/a/a/b;->h:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 2510
    :cond_4
    iget-object v0, p0, Lcom/google/h/a/a/a/b;->i:Ljava/lang/Boolean;

    if-eqz v0, :cond_5

    .line 2511
    const/16 v0, 0x8

    iget-object v1, p0, Lcom/google/h/a/a/a/b;->i:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 2513
    :cond_5
    iget-object v0, p0, Lcom/google/h/a/a/a/b;->k:Ljava/lang/Integer;

    if-eqz v0, :cond_6

    .line 2514
    const/16 v0, 0xb

    iget-object v1, p0, Lcom/google/h/a/a/a/b;->k:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 2516
    :cond_6
    iget-object v0, p0, Lcom/google/h/a/a/a/b;->d:Ljava/lang/Integer;

    if-eqz v0, :cond_7

    .line 2517
    const/16 v0, 0xc

    iget-object v1, p0, Lcom/google/h/a/a/a/b;->d:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 2519
    :cond_7
    iget-object v0, p0, Lcom/google/h/a/a/a/b;->j:Ljava/lang/Boolean;

    if-eqz v0, :cond_8

    .line 2520
    const/16 v0, 0xd

    iget-object v1, p0, Lcom/google/h/a/a/a/b;->j:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 2522
    :cond_8
    iget-object v0, p0, Lcom/google/h/a/a/a/b;->c:Ljava/lang/Long;

    if-eqz v0, :cond_9

    .line 2523
    const/16 v0, 0xe

    iget-object v1, p0, Lcom/google/h/a/a/a/b;->c:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/b;->b(IJ)V

    .line 2525
    :cond_9
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/d;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 2526
    return-void
.end method
