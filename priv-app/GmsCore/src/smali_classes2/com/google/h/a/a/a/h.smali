.class public final Lcom/google/h/a/a/a/h;
.super Lcom/google/protobuf/nano/d;
.source "SourceFile"


# instance fields
.field public a:[Lcom/google/h/a/a/a/k;

.field public b:Ljava/lang/String;

.field public c:[Ljava/lang/String;

.field public d:Ljava/lang/String;

.field public e:Ljava/lang/Integer;

.field public f:Ljava/lang/Integer;

.field public g:Ljava/lang/Integer;

.field public h:Ljava/lang/Integer;

.field public i:Ljava/lang/Integer;

.field public j:Ljava/lang/Integer;

.field public k:Ljava/lang/Integer;

.field public l:Ljava/lang/Integer;

.field public m:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 3288
    invoke-direct {p0}, Lcom/google/protobuf/nano/d;-><init>()V

    .line 3289
    invoke-static {}, Lcom/google/h/a/a/a/k;->a()[Lcom/google/h/a/a/a/k;

    move-result-object v0

    iput-object v0, p0, Lcom/google/h/a/a/a/h;->a:[Lcom/google/h/a/a/a/k;

    iput-object v1, p0, Lcom/google/h/a/a/a/h;->b:Ljava/lang/String;

    sget-object v0, Lcom/google/protobuf/nano/m;->f:[Ljava/lang/String;

    iput-object v0, p0, Lcom/google/h/a/a/a/h;->c:[Ljava/lang/String;

    iput-object v1, p0, Lcom/google/h/a/a/a/h;->d:Ljava/lang/String;

    iput-object v1, p0, Lcom/google/h/a/a/a/h;->e:Ljava/lang/Integer;

    iput-object v1, p0, Lcom/google/h/a/a/a/h;->f:Ljava/lang/Integer;

    iput-object v1, p0, Lcom/google/h/a/a/a/h;->g:Ljava/lang/Integer;

    iput-object v1, p0, Lcom/google/h/a/a/a/h;->h:Ljava/lang/Integer;

    iput-object v1, p0, Lcom/google/h/a/a/a/h;->i:Ljava/lang/Integer;

    iput-object v1, p0, Lcom/google/h/a/a/a/h;->j:Ljava/lang/Integer;

    iput-object v1, p0, Lcom/google/h/a/a/a/h;->k:Ljava/lang/Integer;

    iput-object v1, p0, Lcom/google/h/a/a/a/h;->l:Ljava/lang/Integer;

    iput-object v1, p0, Lcom/google/h/a/a/a/h;->m:Ljava/lang/String;

    iput-object v1, p0, Lcom/google/h/a/a/a/h;->unknownFieldData:Lcom/google/protobuf/nano/f;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/h/a/a/a/h;->cachedSize:I

    .line 3290
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 3368
    invoke-super {p0}, Lcom/google/protobuf/nano/d;->computeSerializedSize()I

    move-result v0

    .line 3369
    iget-object v2, p0, Lcom/google/h/a/a/a/h;->a:[Lcom/google/h/a/a/a/k;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/google/h/a/a/a/h;->a:[Lcom/google/h/a/a/a/k;

    array-length v2, v2

    if-lez v2, :cond_2

    move v2, v0

    move v0, v1

    .line 3370
    :goto_0
    iget-object v3, p0, Lcom/google/h/a/a/a/h;->a:[Lcom/google/h/a/a/a/k;

    array-length v3, v3

    if-ge v0, v3, :cond_1

    .line 3371
    iget-object v3, p0, Lcom/google/h/a/a/a/h;->a:[Lcom/google/h/a/a/a/k;

    aget-object v3, v3, v0

    .line 3372
    if-eqz v3, :cond_0

    .line 3373
    const/4 v4, 0x1

    invoke-static {v4, v3}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v3

    add-int/2addr v2, v3

    .line 3370
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    move v0, v2

    .line 3378
    :cond_2
    iget-object v2, p0, Lcom/google/h/a/a/a/h;->b:Ljava/lang/String;

    if-eqz v2, :cond_3

    .line 3379
    const/4 v2, 0x2

    iget-object v3, p0, Lcom/google/h/a/a/a/h;->b:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 3382
    :cond_3
    iget-object v2, p0, Lcom/google/h/a/a/a/h;->c:[Ljava/lang/String;

    if-eqz v2, :cond_6

    iget-object v2, p0, Lcom/google/h/a/a/a/h;->c:[Ljava/lang/String;

    array-length v2, v2

    if-lez v2, :cond_6

    move v2, v1

    move v3, v1

    .line 3385
    :goto_1
    iget-object v4, p0, Lcom/google/h/a/a/a/h;->c:[Ljava/lang/String;

    array-length v4, v4

    if-ge v1, v4, :cond_5

    .line 3386
    iget-object v4, p0, Lcom/google/h/a/a/a/h;->c:[Ljava/lang/String;

    aget-object v4, v4, v1

    .line 3387
    if-eqz v4, :cond_4

    .line 3388
    add-int/lit8 v3, v3, 0x1

    .line 3389
    invoke-static {v4}, Lcom/google/protobuf/nano/b;->a(Ljava/lang/String;)I

    move-result v4

    add-int/2addr v2, v4

    .line 3385
    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 3393
    :cond_5
    add-int/2addr v0, v2

    .line 3394
    mul-int/lit8 v1, v3, 0x1

    add-int/2addr v0, v1

    .line 3396
    :cond_6
    iget-object v1, p0, Lcom/google/h/a/a/a/h;->d:Ljava/lang/String;

    if-eqz v1, :cond_7

    .line 3397
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/h/a/a/a/h;->d:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3400
    :cond_7
    iget-object v1, p0, Lcom/google/h/a/a/a/h;->e:Ljava/lang/Integer;

    if-eqz v1, :cond_8

    .line 3401
    const/4 v1, 0x5

    iget-object v2, p0, Lcom/google/h/a/a/a/h;->e:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 3404
    :cond_8
    iget-object v1, p0, Lcom/google/h/a/a/a/h;->f:Ljava/lang/Integer;

    if-eqz v1, :cond_9

    .line 3405
    const/4 v1, 0x6

    iget-object v2, p0, Lcom/google/h/a/a/a/h;->f:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 3408
    :cond_9
    iget-object v1, p0, Lcom/google/h/a/a/a/h;->g:Ljava/lang/Integer;

    if-eqz v1, :cond_a

    .line 3409
    const/4 v1, 0x7

    iget-object v2, p0, Lcom/google/h/a/a/a/h;->g:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 3412
    :cond_a
    iget-object v1, p0, Lcom/google/h/a/a/a/h;->h:Ljava/lang/Integer;

    if-eqz v1, :cond_b

    .line 3413
    const/16 v1, 0x8

    iget-object v2, p0, Lcom/google/h/a/a/a/h;->h:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 3416
    :cond_b
    iget-object v1, p0, Lcom/google/h/a/a/a/h;->i:Ljava/lang/Integer;

    if-eqz v1, :cond_c

    .line 3417
    const/16 v1, 0x9

    iget-object v2, p0, Lcom/google/h/a/a/a/h;->i:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 3420
    :cond_c
    iget-object v1, p0, Lcom/google/h/a/a/a/h;->j:Ljava/lang/Integer;

    if-eqz v1, :cond_d

    .line 3421
    const/16 v1, 0xa

    iget-object v2, p0, Lcom/google/h/a/a/a/h;->j:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 3424
    :cond_d
    iget-object v1, p0, Lcom/google/h/a/a/a/h;->k:Ljava/lang/Integer;

    if-eqz v1, :cond_e

    .line 3425
    const/16 v1, 0xb

    iget-object v2, p0, Lcom/google/h/a/a/a/h;->k:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 3428
    :cond_e
    iget-object v1, p0, Lcom/google/h/a/a/a/h;->l:Ljava/lang/Integer;

    if-eqz v1, :cond_f

    .line 3429
    const/16 v1, 0xc

    iget-object v2, p0, Lcom/google/h/a/a/a/h;->l:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 3432
    :cond_f
    iget-object v1, p0, Lcom/google/h/a/a/a/h;->m:Ljava/lang/String;

    if-eqz v1, :cond_10

    .line 3433
    const/16 v1, 0xd

    iget-object v2, p0, Lcom/google/h/a/a/a/h;->m:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3436
    :cond_10
    return v0
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 3232
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/h/a/a/a/h;->storeUnknownField(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    const/16 v0, 0xa

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->b(Lcom/google/protobuf/nano/a;I)I

    move-result v2

    iget-object v0, p0, Lcom/google/h/a/a/a/h;->a:[Lcom/google/h/a/a/a/k;

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lcom/google/h/a/a/a/k;

    if-eqz v0, :cond_1

    iget-object v3, p0, Lcom/google/h/a/a/a/h;->a:[Lcom/google/h/a/a/a/k;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_1
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_3

    new-instance v3, Lcom/google/h/a/a/a/k;

    invoke-direct {v3}, Lcom/google/h/a/a/a/k;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    iget-object v0, p0, Lcom/google/h/a/a/a/h;->a:[Lcom/google/h/a/a/a/k;

    array-length v0, v0

    goto :goto_1

    :cond_3
    new-instance v3, Lcom/google/h/a/a/a/k;

    invoke-direct {v3}, Lcom/google/h/a/a/a/k;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    iput-object v2, p0, Lcom/google/h/a/a/a/h;->a:[Lcom/google/h/a/a/a/k;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/h/a/a/a/h;->b:Ljava/lang/String;

    goto :goto_0

    :sswitch_3
    const/16 v0, 0x1a

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->b(Lcom/google/protobuf/nano/a;I)I

    move-result v2

    iget-object v0, p0, Lcom/google/h/a/a/a/h;->c:[Ljava/lang/String;

    if-nez v0, :cond_5

    move v0, v1

    :goto_3
    add-int/2addr v2, v0

    new-array v2, v2, [Ljava/lang/String;

    if-eqz v0, :cond_4

    iget-object v3, p0, Lcom/google/h/a/a/a/h;->c:[Ljava/lang/String;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_4
    :goto_4
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_6

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_5
    iget-object v0, p0, Lcom/google/h/a/a/a/h;->c:[Ljava/lang/String;

    array-length v0, v0

    goto :goto_3

    :cond_6
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    iput-object v2, p0, Lcom/google/h/a/a/a/h;->c:[Ljava/lang/String;

    goto/16 :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/h/a/a/a/h;->d:Ljava/lang/String;

    goto/16 :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/h/a/a/a/h;->e:Ljava/lang/Integer;

    goto/16 :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/h/a/a/a/h;->f:Ljava/lang/Integer;

    goto/16 :goto_0

    :sswitch_7
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/h/a/a/a/h;->g:Ljava/lang/Integer;

    goto/16 :goto_0

    :sswitch_8
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/h/a/a/a/h;->h:Ljava/lang/Integer;

    goto/16 :goto_0

    :sswitch_9
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/h/a/a/a/h;->i:Ljava/lang/Integer;

    goto/16 :goto_0

    :sswitch_a
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/h/a/a/a/h;->j:Ljava/lang/Integer;

    goto/16 :goto_0

    :sswitch_b
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/h/a/a/a/h;->k:Ljava/lang/Integer;

    goto/16 :goto_0

    :sswitch_c
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/h/a/a/a/h;->l:Ljava/lang/Integer;

    goto/16 :goto_0

    :sswitch_d
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/h/a/a/a/h;->m:Ljava/lang/String;

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x28 -> :sswitch_5
        0x30 -> :sswitch_6
        0x38 -> :sswitch_7
        0x40 -> :sswitch_8
        0x48 -> :sswitch_9
        0x50 -> :sswitch_a
        0x58 -> :sswitch_b
        0x60 -> :sswitch_c
        0x6a -> :sswitch_d
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 3314
    iget-object v0, p0, Lcom/google/h/a/a/a/h;->a:[Lcom/google/h/a/a/a/k;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/h/a/a/a/h;->a:[Lcom/google/h/a/a/a/k;

    array-length v0, v0

    if-lez v0, :cond_1

    move v0, v1

    .line 3315
    :goto_0
    iget-object v2, p0, Lcom/google/h/a/a/a/h;->a:[Lcom/google/h/a/a/a/k;

    array-length v2, v2

    if-ge v0, v2, :cond_1

    .line 3316
    iget-object v2, p0, Lcom/google/h/a/a/a/h;->a:[Lcom/google/h/a/a/a/k;

    aget-object v2, v2, v0

    .line 3317
    if-eqz v2, :cond_0

    .line 3318
    const/4 v3, 0x1

    invoke-virtual {p1, v3, v2}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 3315
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 3322
    :cond_1
    iget-object v0, p0, Lcom/google/h/a/a/a/h;->b:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 3323
    const/4 v0, 0x2

    iget-object v2, p0, Lcom/google/h/a/a/a/h;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v2}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 3325
    :cond_2
    iget-object v0, p0, Lcom/google/h/a/a/a/h;->c:[Ljava/lang/String;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/google/h/a/a/a/h;->c:[Ljava/lang/String;

    array-length v0, v0

    if-lez v0, :cond_4

    .line 3326
    :goto_1
    iget-object v0, p0, Lcom/google/h/a/a/a/h;->c:[Ljava/lang/String;

    array-length v0, v0

    if-ge v1, v0, :cond_4

    .line 3327
    iget-object v0, p0, Lcom/google/h/a/a/a/h;->c:[Ljava/lang/String;

    aget-object v0, v0, v1

    .line 3328
    if-eqz v0, :cond_3

    .line 3329
    const/4 v2, 0x3

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 3326
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 3333
    :cond_4
    iget-object v0, p0, Lcom/google/h/a/a/a/h;->d:Ljava/lang/String;

    if-eqz v0, :cond_5

    .line 3334
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/h/a/a/a/h;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 3336
    :cond_5
    iget-object v0, p0, Lcom/google/h/a/a/a/h;->e:Ljava/lang/Integer;

    if-eqz v0, :cond_6

    .line 3337
    const/4 v0, 0x5

    iget-object v1, p0, Lcom/google/h/a/a/a/h;->e:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 3339
    :cond_6
    iget-object v0, p0, Lcom/google/h/a/a/a/h;->f:Ljava/lang/Integer;

    if-eqz v0, :cond_7

    .line 3340
    const/4 v0, 0x6

    iget-object v1, p0, Lcom/google/h/a/a/a/h;->f:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 3342
    :cond_7
    iget-object v0, p0, Lcom/google/h/a/a/a/h;->g:Ljava/lang/Integer;

    if-eqz v0, :cond_8

    .line 3343
    const/4 v0, 0x7

    iget-object v1, p0, Lcom/google/h/a/a/a/h;->g:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 3345
    :cond_8
    iget-object v0, p0, Lcom/google/h/a/a/a/h;->h:Ljava/lang/Integer;

    if-eqz v0, :cond_9

    .line 3346
    const/16 v0, 0x8

    iget-object v1, p0, Lcom/google/h/a/a/a/h;->h:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 3348
    :cond_9
    iget-object v0, p0, Lcom/google/h/a/a/a/h;->i:Ljava/lang/Integer;

    if-eqz v0, :cond_a

    .line 3349
    const/16 v0, 0x9

    iget-object v1, p0, Lcom/google/h/a/a/a/h;->i:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 3351
    :cond_a
    iget-object v0, p0, Lcom/google/h/a/a/a/h;->j:Ljava/lang/Integer;

    if-eqz v0, :cond_b

    .line 3352
    const/16 v0, 0xa

    iget-object v1, p0, Lcom/google/h/a/a/a/h;->j:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 3354
    :cond_b
    iget-object v0, p0, Lcom/google/h/a/a/a/h;->k:Ljava/lang/Integer;

    if-eqz v0, :cond_c

    .line 3355
    const/16 v0, 0xb

    iget-object v1, p0, Lcom/google/h/a/a/a/h;->k:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 3357
    :cond_c
    iget-object v0, p0, Lcom/google/h/a/a/a/h;->l:Ljava/lang/Integer;

    if-eqz v0, :cond_d

    .line 3358
    const/16 v0, 0xc

    iget-object v1, p0, Lcom/google/h/a/a/a/h;->l:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 3360
    :cond_d
    iget-object v0, p0, Lcom/google/h/a/a/a/h;->m:Ljava/lang/String;

    if-eqz v0, :cond_e

    .line 3361
    const/16 v0, 0xd

    iget-object v1, p0, Lcom/google/h/a/a/a/h;->m:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 3363
    :cond_e
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/d;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 3364
    return-void
.end method
