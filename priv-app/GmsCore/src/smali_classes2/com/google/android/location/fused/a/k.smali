.class final Lcom/google/android/location/fused/a/k;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/location/fused/a/i;


# instance fields
.field final synthetic a:Lcom/google/android/location/fused/a/g;


# direct methods
.method private constructor <init>(Lcom/google/android/location/fused/a/g;)V
    .locals 0

    .prologue
    .line 247
    iput-object p1, p0, Lcom/google/android/location/fused/a/k;->a:Lcom/google/android/location/fused/a/g;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/location/fused/a/g;B)V
    .locals 0

    .prologue
    .line 247
    invoke-direct {p0, p1}, Lcom/google/android/location/fused/a/k;-><init>(Lcom/google/android/location/fused/a/g;)V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 8

    .prologue
    .line 250
    iget-object v0, p0, Lcom/google/android/location/fused/a/k;->a:Lcom/google/android/location/fused/a/g;

    invoke-static {v0}, Lcom/google/android/location/fused/a/g;->e(Lcom/google/android/location/fused/a/g;)Lcom/google/android/location/fused/ar;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/location/fused/ar;->a()Landroid/location/LocationManager;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/location/os/real/bg;->a(Landroid/location/LocationManager;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 251
    iget-object v0, p0, Lcom/google/android/location/fused/a/k;->a:Lcom/google/android/location/fused/a/g;

    invoke-static {v0}, Lcom/google/android/location/fused/a/g;->e(Lcom/google/android/location/fused/a/g;)Lcom/google/android/location/fused/ar;

    move-result-object v0

    const-string v1, "gps"

    const-wide/16 v2, 0x0

    iget-object v4, p0, Lcom/google/android/location/fused/a/k;->a:Lcom/google/android/location/fused/a/g;

    iget-object v5, p0, Lcom/google/android/location/fused/a/k;->a:Lcom/google/android/location/fused/a/g;

    invoke-static {v5}, Lcom/google/android/location/fused/a/g;->f(Lcom/google/android/location/fused/a/g;)Landroid/os/Handler;

    move-result-object v5

    invoke-virtual {v5}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;

    move-result-object v5

    iget-object v6, p0, Lcom/google/android/location/fused/a/k;->a:Lcom/google/android/location/fused/a/g;

    iget-object v6, v6, Lcom/google/android/location/fused/a/q;->j:Ljava/util/Collection;

    const/4 v7, 0x1

    invoke-virtual/range {v0 .. v7}, Lcom/google/android/location/fused/ar;->a(Ljava/lang/String;JLandroid/location/LocationListener;Landroid/os/Looper;Ljava/util/Collection;Z)V

    .line 265
    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/google/android/location/fused/a/k;->a:Lcom/google/android/location/fused/a/g;

    sget-object v0, Lcom/google/android/location/x;->F:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lcom/google/android/location/fused/a/g;->a(Lcom/google/android/location/fused/a/g;J)V

    .line 266
    return-void

    .line 262
    :cond_1
    const-string v0, "GCoreFlp"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 263
    const-string v0, "Gps not enabled because no gps device exists"

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/google/android/location/fused/ax;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 270
    iget-object v0, p0, Lcom/google/android/location/fused/a/k;->a:Lcom/google/android/location/fused/a/g;

    invoke-static {v0}, Lcom/google/android/location/fused/a/g;->e(Lcom/google/android/location/fused/a/g;)Lcom/google/android/location/fused/ar;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/location/fused/ar;->a()Landroid/location/LocationManager;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/location/fused/a/k;->a:Lcom/google/android/location/fused/a/g;

    invoke-virtual {v0, v1}, Landroid/location/LocationManager;->removeUpdates(Landroid/location/LocationListener;)V

    .line 272
    return-void
.end method

.method public final c()V
    .locals 2

    .prologue
    .line 276
    iget-object v0, p0, Lcom/google/android/location/fused/a/k;->a:Lcom/google/android/location/fused/a/g;

    iget-object v1, p0, Lcom/google/android/location/fused/a/k;->a:Lcom/google/android/location/fused/a/g;

    invoke-static {v1}, Lcom/google/android/location/fused/a/g;->g(Lcom/google/android/location/fused/a/g;)Lcom/google/android/location/fused/a/i;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/location/fused/a/g;->a(Lcom/google/android/location/fused/a/g;Lcom/google/android/location/fused/a/i;)V

    .line 277
    return-void
.end method

.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 281
    const-string v0, "pulsing"

    return-object v0
.end method
