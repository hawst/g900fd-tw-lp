.class final Lcom/google/android/location/reporting/ble/b;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Ljava/lang/String;

.field final synthetic b:Landroid/os/PowerManager$WakeLock;

.field final synthetic c:Lcom/google/android/location/reporting/ble/a;


# direct methods
.method constructor <init>(Lcom/google/android/location/reporting/ble/a;Ljava/lang/String;Landroid/os/PowerManager$WakeLock;)V
    .locals 0

    .prologue
    .line 175
    iput-object p1, p0, Lcom/google/android/location/reporting/ble/b;->c:Lcom/google/android/location/reporting/ble/a;

    iput-object p2, p0, Lcom/google/android/location/reporting/ble/b;->a:Ljava/lang/String;

    iput-object p3, p0, Lcom/google/android/location/reporting/ble/b;->b:Landroid/os/PowerManager$WakeLock;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 6

    .prologue
    .line 179
    :try_start_0
    iget-object v0, p0, Lcom/google/android/location/reporting/ble/b;->c:Lcom/google/android/location/reporting/ble/a;

    iget-object v0, v0, Lcom/google/android/location/reporting/ble/a;->c:Lcom/google/android/gms/common/util/p;

    invoke-interface {v0}, Lcom/google/android/gms/common/util/p;->b()J

    move-result-wide v2

    .line 180
    sget-object v0, Lcom/google/android/location/reporting/service/ab;->t:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    .line 181
    iget-object v4, p0, Lcom/google/android/location/reporting/ble/b;->c:Lcom/google/android/location/reporting/ble/a;

    invoke-static {v4}, Lcom/google/android/location/reporting/ble/a;->a(Lcom/google/android/location/reporting/ble/a;)J

    move-result-wide v4

    sub-long v4, v2, v4

    cmp-long v0, v4, v0

    if-lez v0, :cond_3

    const/4 v0, 0x1

    .line 183
    :goto_0
    iget-object v1, p0, Lcom/google/android/location/reporting/ble/b;->c:Lcom/google/android/location/reporting/ble/a;

    monitor-enter v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 184
    :try_start_1
    iget-object v4, p0, Lcom/google/android/location/reporting/ble/b;->c:Lcom/google/android/location/reporting/ble/a;

    invoke-static {v4}, Lcom/google/android/location/reporting/ble/a;->b(Lcom/google/android/location/reporting/ble/a;)Z

    move-result v4

    if-nez v4, :cond_4

    if-eqz v0, :cond_4

    iget-object v4, p0, Lcom/google/android/location/reporting/ble/b;->c:Lcom/google/android/location/reporting/ble/a;

    invoke-virtual {v4}, Lcom/google/android/location/reporting/ble/a;->c()Z

    move-result v4

    if-eqz v4, :cond_4

    .line 185
    const-string v0, "GCoreUlr"

    const/4 v4, 0x4

    invoke-static {v0, v4}, Lcom/google/android/location/reporting/b/d;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 186
    const-string v0, "GCoreUlr"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "BLE: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v5, p0, Lcom/google/android/location/reporting/ble/b;->a:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ": scannerActive=false, enoughTimeElapsed=true; starting background scan..."

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, v4}, Lcom/google/android/location/reporting/b/d;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 189
    :cond_0
    iget-object v0, p0, Lcom/google/android/location/reporting/ble/b;->c:Lcom/google/android/location/reporting/ble/a;

    invoke-static {v0, v2, v3}, Lcom/google/android/location/reporting/ble/a;->a(Lcom/google/android/location/reporting/ble/a;J)J

    .line 190
    iget-object v0, p0, Lcom/google/android/location/reporting/ble/b;->c:Lcom/google/android/location/reporting/ble/a;

    invoke-static {v0}, Lcom/google/android/location/reporting/ble/a;->c(Lcom/google/android/location/reporting/ble/a;)V

    .line 191
    const-string v0, "GCoreUlr"

    const/4 v2, 0x4

    invoke-static {v0, v2}, Lcom/google/android/location/reporting/b/d;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 192
    const-string v0, "GCoreUlr"

    const-string v2, "BLE:   ...background scan complete."

    invoke-static {v0, v2}, Lcom/google/android/location/reporting/b/d;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 194
    :cond_1
    iget-object v0, p0, Lcom/google/android/location/reporting/ble/b;->c:Lcom/google/android/location/reporting/ble/a;

    invoke-virtual {v0}, Lcom/google/android/location/reporting/ble/a;->d()V

    .line 202
    :cond_2
    :goto_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 204
    iget-object v0, p0, Lcom/google/android/location/reporting/ble/b;->b:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V

    .line 206
    return-void

    .line 181
    :cond_3
    const/4 v0, 0x0

    goto :goto_0

    .line 196
    :cond_4
    :try_start_2
    const-string v2, "GCoreUlr"

    const/4 v3, 0x4

    invoke-static {v2, v3}, Lcom/google/android/location/reporting/b/d;->a(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 197
    const-string v2, "GCoreUlr"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "BLE: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/google/android/location/reporting/ble/b;->a:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ": scannerActive="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/location/reporting/ble/b;->c:Lcom/google/android/location/reporting/ble/a;

    invoke-static {v4}, Lcom/google/android/location/reporting/ble/a;->b(Lcom/google/android/location/reporting/ble/a;)Z

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", enoughTimeElapsed="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "; scan not started."

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/google/android/location/reporting/b/d;->c(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    .line 202
    :catchall_0
    move-exception v0

    :try_start_3
    monitor-exit v1

    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 204
    :catchall_1
    move-exception v0

    iget-object v1, p0, Lcom/google/android/location/reporting/ble/b;->b:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->release()V

    throw v0
.end method
