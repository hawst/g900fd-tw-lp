.class public final Lcom/google/android/location/places/k;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/v/b/n;


# instance fields
.field public a:Lcom/google/android/gms/location/places/internal/PlacesParams;

.field public b:Lcom/google/android/location/places/aj;

.field public c:Ljava/lang/Exception;

.field public d:Lcom/google/v/b/o;

.field public e:Lcom/google/android/location/places/o;

.field public f:J

.field final synthetic g:Lcom/google/android/location/places/f;


# direct methods
.method public constructor <init>(Lcom/google/android/location/places/f;Lcom/google/android/gms/location/places/internal/PlacesParams;Lcom/google/android/location/places/aj;Lcom/google/android/location/places/o;)V
    .locals 2

    .prologue
    .line 348
    iput-object p1, p0, Lcom/google/android/location/places/k;->g:Lcom/google/android/location/places/f;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 349
    iput-object p2, p0, Lcom/google/android/location/places/k;->a:Lcom/google/android/gms/location/places/internal/PlacesParams;

    .line 350
    iput-object p3, p0, Lcom/google/android/location/places/k;->b:Lcom/google/android/location/places/aj;

    .line 351
    iput-object p4, p0, Lcom/google/android/location/places/k;->e:Lcom/google/android/location/places/o;

    .line 352
    iget-object v0, p1, Lcom/google/android/location/places/f;->c:Lcom/google/android/location/n/ai;

    invoke-virtual {v0}, Lcom/google/android/location/n/ai;->a()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/location/places/k;->f:J

    .line 353
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/v/b/m;Lcom/google/v/b/o;)V
    .locals 2

    .prologue
    .line 373
    iput-object p2, p0, Lcom/google/android/location/places/k;->d:Lcom/google/v/b/o;

    .line 374
    iget-object v0, p0, Lcom/google/android/location/places/k;->g:Lcom/google/android/location/places/f;

    iget-object v0, v0, Lcom/google/android/location/places/f;->a:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/location/places/m;

    invoke-direct {v1, p0}, Lcom/google/android/location/places/m;-><init>(Lcom/google/android/location/places/k;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 380
    return-void
.end method

.method public final a(Lcom/google/v/b/m;Ljava/lang/Exception;)V
    .locals 2

    .prologue
    .line 357
    iput-object p2, p0, Lcom/google/android/location/places/k;->c:Ljava/lang/Exception;

    .line 358
    const-string v0, "Places"

    const/4 v1, 0x6

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 359
    const-string v0, "Places"

    const-string v1, "masf request failed"

    invoke-static {v0, v1, p2}, Lcom/google/android/location/n/aa;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 361
    :cond_0
    iget-object v0, p0, Lcom/google/android/location/places/k;->g:Lcom/google/android/location/places/f;

    iget-object v0, v0, Lcom/google/android/location/places/f;->a:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/location/places/l;

    invoke-direct {v1, p0}, Lcom/google/android/location/places/l;-><init>(Lcom/google/android/location/places/k;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 369
    return-void
.end method
