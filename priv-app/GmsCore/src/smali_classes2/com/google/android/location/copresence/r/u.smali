.class final Lcom/google/android/location/copresence/r/u;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/net/wifi/p2p/WifiP2pManager$GroupInfoListener;


# instance fields
.field final synthetic a:Lcom/google/android/location/copresence/r/t;


# direct methods
.method constructor <init>(Lcom/google/android/location/copresence/r/t;)V
    .locals 0

    .prologue
    .line 301
    iput-object p1, p0, Lcom/google/android/location/copresence/r/u;->a:Lcom/google/android/location/copresence/r/t;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onGroupInfoAvailable(Landroid/net/wifi/p2p/WifiP2pGroup;)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    .line 304
    if-nez p1, :cond_0

    .line 305
    iget-object v0, p0, Lcom/google/android/location/copresence/r/u;->a:Lcom/google/android/location/copresence/r/t;

    iget-object v0, v0, Lcom/google/android/location/copresence/r/t;->b:Lcom/google/android/location/copresence/r/p;

    invoke-static {v0}, Lcom/google/android/location/copresence/r/p;->i(Lcom/google/android/location/copresence/r/p;)V

    .line 306
    iget-object v0, p0, Lcom/google/android/location/copresence/r/u;->a:Lcom/google/android/location/copresence/r/t;

    iget-object v0, v0, Lcom/google/android/location/copresence/r/t;->b:Lcom/google/android/location/copresence/r/p;

    invoke-static {v0}, Lcom/google/android/location/copresence/r/p;->g(Lcom/google/android/location/copresence/r/p;)Lcom/google/android/location/copresence/k/a;

    move-result-object v0

    const/4 v1, 0x0

    new-array v1, v1, [Lcom/google/android/location/copresence/k/f;

    invoke-virtual {v0, v1}, Lcom/google/android/location/copresence/k/a;->b([Lcom/google/android/location/copresence/k/f;)V

    .line 327
    :goto_0
    return-void

    .line 309
    :cond_0
    const/4 v0, 0x3

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 310
    const-string v0, "WifiDirectStates2: Removing current group"

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->b(Ljava/lang/String;)I

    .line 312
    :cond_1
    iget-object v0, p0, Lcom/google/android/location/copresence/r/u;->a:Lcom/google/android/location/copresence/r/t;

    iget-object v0, v0, Lcom/google/android/location/copresence/r/t;->b:Lcom/google/android/location/copresence/r/p;

    invoke-static {v0}, Lcom/google/android/location/copresence/r/p;->b(Lcom/google/android/location/copresence/r/p;)Lcom/google/android/location/copresence/r/ah;

    move-result-object v0

    new-instance v1, Lcom/google/android/location/copresence/r/v;

    invoke-direct {v1, p0}, Lcom/google/android/location/copresence/r/v;-><init>(Lcom/google/android/location/copresence/r/u;)V

    invoke-virtual {v0}, Lcom/google/android/location/copresence/r/ah;->a()V

    invoke-static {v3}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v2

    if-eqz v2, :cond_2

    const-string v2, "WifiP2pManager: Remove group."

    invoke-static {v2}, Lcom/google/android/location/copresence/ag;->a(Ljava/lang/String;)I

    :cond_2
    invoke-static {v3}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v2

    if-eqz v2, :cond_3

    const-string v2, "WifiP2pManager: Removing group."

    invoke-static {v2}, Lcom/google/android/location/copresence/ag;->a(Ljava/lang/String;)I

    :cond_3
    iget-object v2, v0, Lcom/google/android/location/copresence/r/ah;->a:Landroid/net/wifi/p2p/WifiP2pManager;

    iget-object v0, v0, Lcom/google/android/location/copresence/r/ah;->e:Landroid/net/wifi/p2p/WifiP2pManager$Channel;

    invoke-virtual {v2, v0, v1}, Landroid/net/wifi/p2p/WifiP2pManager;->removeGroup(Landroid/net/wifi/p2p/WifiP2pManager$Channel;Landroid/net/wifi/p2p/WifiP2pManager$ActionListener;)V

    goto :goto_0
.end method
