.class final Lcom/google/android/location/internal/c;
.super Lcom/google/android/gms/common/internal/b;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/location/internal/GoogleLocationManagerService;


# direct methods
.method private constructor <init>(Lcom/google/android/location/internal/GoogleLocationManagerService;Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 205
    iput-object p1, p0, Lcom/google/android/location/internal/c;->a:Lcom/google/android/location/internal/GoogleLocationManagerService;

    .line 206
    invoke-direct {p0, p2}, Lcom/google/android/gms/common/internal/b;-><init>(Landroid/content/Context;)V

    .line 207
    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/location/internal/GoogleLocationManagerService;Landroid/content/Context;B)V
    .locals 0

    .prologue
    .line 203
    invoke-direct {p0, p1, p2}, Lcom/google/android/location/internal/c;-><init>(Lcom/google/android/location/internal/GoogleLocationManagerService;Landroid/content/Context;)V

    return-void
.end method


# virtual methods
.method protected final a(Lcom/google/android/gms/common/internal/bg;Lcom/google/android/gms/common/internal/GetServiceRequest;)V
    .locals 3

    .prologue
    .line 234
    invoke-virtual {p2}, Lcom/google/android/gms/common/internal/GetServiceRequest;->b()I

    move-result v0

    const/16 v1, 0x41

    if-eq v0, v1, :cond_0

    .line 235
    invoke-super {p0, p1, p2}, Lcom/google/android/gms/common/internal/b;->a(Lcom/google/android/gms/common/internal/bg;Lcom/google/android/gms/common/internal/GetServiceRequest;)V

    .line 238
    :cond_0
    const-string v0, "GLMS"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 239
    const-string v0, "GLMS"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Generating Places binder for: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p2}, Lcom/google/android/gms/common/internal/GetServiceRequest;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 242
    :cond_1
    new-instance v0, Lcom/google/android/location/internal/g;

    iget-object v1, p0, Lcom/google/android/location/internal/c;->a:Lcom/google/android/location/internal/GoogleLocationManagerService;

    invoke-static {v1}, Lcom/google/android/location/internal/GoogleLocationManagerService;->a(Lcom/google/android/location/internal/GoogleLocationManagerService;)Lcom/google/android/location/internal/e;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/location/internal/g;-><init>(Lcom/google/android/location/internal/e;)V

    .line 244
    const/4 v1, 0x0

    invoke-virtual {v0}, Lcom/google/android/location/internal/g;->asBinder()Landroid/os/IBinder;

    move-result-object v0

    const/4 v2, 0x0

    invoke-interface {p1, v1, v0, v2}, Lcom/google/android/gms/common/internal/bg;->a(ILandroid/os/IBinder;Landroid/os/Bundle;)V

    .line 245
    return-void
.end method

.method public final i(Lcom/google/android/gms/common/internal/bg;ILjava/lang/String;Landroid/os/Bundle;)V
    .locals 7

    .prologue
    .line 214
    iget-object v0, p0, Lcom/google/android/location/internal/c;->a:Lcom/google/android/location/internal/GoogleLocationManagerService;

    invoke-static {p4}, Lcom/google/android/location/internal/GoogleLocationManagerService;->a(Landroid/os/Bundle;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 217
    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    move-result v4

    .line 218
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v5

    .line 219
    iget-object v0, p0, Lcom/google/android/location/internal/c;->a:Lcom/google/android/location/internal/GoogleLocationManagerService;

    invoke-static {v0}, Lcom/google/android/location/internal/GoogleLocationManagerService;->b(Lcom/google/android/location/internal/GoogleLocationManagerService;)Landroid/os/Handler;

    move-result-object v6

    new-instance v0, Lcom/google/android/location/internal/d;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p3

    invoke-direct/range {v0 .. v5}, Lcom/google/android/location/internal/d;-><init>(Lcom/google/android/location/internal/c;Lcom/google/android/gms/common/internal/bg;Ljava/lang/String;II)V

    invoke-virtual {v6, v0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 229
    :goto_0
    return-void

    .line 227
    :cond_0
    iget-object v0, p0, Lcom/google/android/location/internal/c;->a:Lcom/google/android/location/internal/GoogleLocationManagerService;

    invoke-static {v0, p1, p3}, Lcom/google/android/location/internal/GoogleLocationManagerService;->a(Lcom/google/android/location/internal/GoogleLocationManagerService;Lcom/google/android/gms/common/internal/bg;Ljava/lang/String;)V

    goto :goto_0
.end method
