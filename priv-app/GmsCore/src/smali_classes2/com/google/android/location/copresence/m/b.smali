.class public final Lcom/google/android/location/copresence/m/b;
.super Lcom/google/android/gms/location/copresence/internal/m;
.source "SourceFile"


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Landroid/content/pm/PackageManager;

.field private final c:Lcom/google/android/location/copresence/ap;

.field private d:Lcom/google/android/location/copresence/r;


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 56
    invoke-direct {p0}, Lcom/google/android/gms/location/copresence/internal/m;-><init>()V

    .line 57
    iput-object p1, p0, Lcom/google/android/location/copresence/m/b;->a:Landroid/content/Context;

    .line 58
    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/copresence/m/b;->b:Landroid/content/pm/PackageManager;

    .line 59
    invoke-static {p1}, Lcom/google/android/location/copresence/ap;->a(Landroid/content/Context;)Lcom/google/android/location/copresence/ap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/copresence/m/b;->c:Lcom/google/android/location/copresence/ap;

    .line 60
    iget-object v0, p0, Lcom/google/android/location/copresence/m/b;->c:Lcom/google/android/location/copresence/ap;

    new-instance v1, Lcom/google/android/location/copresence/m/c;

    invoke-direct {v1, p0}, Lcom/google/android/location/copresence/m/c;-><init>(Lcom/google/android/location/copresence/m/b;)V

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/google/android/location/copresence/ap;->a(Ljava/lang/Runnable;Landroid/os/WorkSource;)V

    .line 68
    return-void
.end method

.method static synthetic a(Lcom/google/android/location/copresence/m/b;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lcom/google/android/location/copresence/m/b;->a:Landroid/content/Context;

    return-object v0
.end method

.method public static a(Landroid/content/Context;)Lcom/google/android/location/copresence/m/b;
    .locals 2

    .prologue
    .line 45
    const/4 v1, 0x0

    .line 47
    :try_start_0
    new-instance v0, Lcom/google/android/location/copresence/m/b;

    invoke-direct {v0, p0}, Lcom/google/android/location/copresence/m/b;-><init>(Landroid/content/Context;)V
    :try_end_0
    .catch Ljava/lang/ExceptionInInitializerError; {:try_start_0 .. :try_end_0} :catch_0

    .line 53
    :goto_0
    return-object v0

    .line 49
    :catch_0
    move-exception v0

    const/4 v0, 0x6

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 50
    const-string v0, "CopresenceServiceImpl: Failed to initialize CopresenceServiceImpl"

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->e(Ljava/lang/String;)I

    :cond_0
    move-object v0, v1

    goto :goto_0
.end method

.method static synthetic a(Lcom/google/android/location/copresence/m/b;Lcom/google/android/location/copresence/r;)Lcom/google/android/location/copresence/r;
    .locals 0

    .prologue
    .line 36
    iput-object p1, p0, Lcom/google/android/location/copresence/m/b;->d:Lcom/google/android/location/copresence/r;

    return-object p1
.end method

.method static synthetic b(Lcom/google/android/location/copresence/m/b;)Lcom/google/android/location/copresence/r;
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lcom/google/android/location/copresence/m/b;->d:Lcom/google/android/location/copresence/r;

    return-object v0
.end method

.method public static b(Landroid/content/Intent;)V
    .locals 0

    .prologue
    .line 183
    invoke-static {p0}, Lcom/google/android/location/copresence/GcmBroadcastReceiver;->a(Landroid/content/Intent;)Z

    .line 184
    return-void
.end method

.method private b()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 170
    iget-object v1, p0, Lcom/google/android/location/copresence/m/b;->b:Landroid/content/pm/PackageManager;

    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/content/pm/PackageManager;->getPackagesForUid(I)[Ljava/lang/String;

    move-result-object v1

    array-length v2, v1

    if-lez v2, :cond_0

    iget-object v2, p0, Lcom/google/android/location/copresence/m/b;->b:Landroid/content/pm/PackageManager;

    aget-object v0, v1, v0

    invoke-static {v2, v0}, Lcom/google/android/gms/common/ew;->b(Landroid/content/pm/PackageManager;Ljava/lang/String;)Z

    move-result v0

    :cond_0
    return v0
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    .line 153
    iget-object v0, p0, Lcom/google/android/location/copresence/m/b;->c:Lcom/google/android/location/copresence/ap;

    new-instance v1, Lcom/google/android/location/copresence/m/i;

    invoke-direct {v1, p0}, Lcom/google/android/location/copresence/m/i;-><init>(Lcom/google/android/location/copresence/m/b;)V

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/google/android/location/copresence/ap;->a(Ljava/lang/Runnable;Landroid/os/WorkSource;)V

    .line 159
    return-void
.end method

.method public final a(Landroid/content/Intent;)V
    .locals 3

    .prologue
    .line 144
    iget-object v0, p0, Lcom/google/android/location/copresence/m/b;->c:Lcom/google/android/location/copresence/ap;

    new-instance v1, Lcom/google/android/location/copresence/m/h;

    invoke-direct {v1, p0, p1}, Lcom/google/android/location/copresence/m/h;-><init>(Lcom/google/android/location/copresence/m/b;Landroid/content/Intent;)V

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/google/android/location/copresence/ap;->a(Ljava/lang/Runnable;Landroid/os/WorkSource;)V

    .line 150
    return-void
.end method

.method public final a(Landroid/os/IBinder;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/location/copresence/internal/BatchImpl;Lcom/google/android/gms/location/copresence/internal/i;)V
    .locals 9

    .prologue
    .line 75
    const/4 v0, 0x3

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 76
    const-string v0, "CopresenceServiceImpl: copresenceBatch"

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->b(Ljava/lang/String;)I

    .line 78
    :cond_0
    invoke-direct {p0}, Lcom/google/android/location/copresence/m/b;->b()Z

    move-result v0

    if-nez v0, :cond_1

    .line 79
    new-instance v0, Ljava/lang/SecurityException;

    invoke-direct {v0}, Ljava/lang/SecurityException;-><init>()V

    throw v0

    .line 81
    :cond_1
    iget-object v0, p0, Lcom/google/android/location/copresence/m/b;->a:Landroid/content/Context;

    invoke-static {v0, p2}, Lcom/google/android/gms/common/util/e;->c(Landroid/content/Context;Ljava/lang/String;)V

    .line 82
    iget-object v8, p0, Lcom/google/android/location/copresence/m/b;->c:Lcom/google/android/location/copresence/ap;

    new-instance v0, Lcom/google/android/location/copresence/m/d;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object v7, p6

    invoke-direct/range {v0 .. v7}, Lcom/google/android/location/copresence/m/d;-><init>(Lcom/google/android/location/copresence/m/b;Landroid/os/IBinder;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/location/copresence/internal/BatchImpl;Lcom/google/android/gms/location/copresence/internal/i;)V

    const/4 v1, 0x0

    invoke-virtual {v8, v0, v1}, Lcom/google/android/location/copresence/ap;->a(Ljava/lang/Runnable;Landroid/os/WorkSource;)V

    .line 89
    return-void
.end method

.method public final a(Ljava/lang/String;Lcom/google/android/gms/location/copresence/CopresenceSettings;Lcom/google/android/gms/location/copresence/internal/i;)V
    .locals 3

    .prologue
    .line 99
    invoke-direct {p0}, Lcom/google/android/location/copresence/m/b;->b()Z

    move-result v0

    if-nez v0, :cond_0

    .line 100
    new-instance v0, Ljava/lang/SecurityException;

    const-string v1, "You have no access to copresences settings."

    invoke-direct {v0, v1}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 102
    :cond_0
    iget-object v0, p0, Lcom/google/android/location/copresence/m/b;->c:Lcom/google/android/location/copresence/ap;

    new-instance v1, Lcom/google/android/location/copresence/m/e;

    invoke-direct {v1, p0, p1, p2, p3}, Lcom/google/android/location/copresence/m/e;-><init>(Lcom/google/android/location/copresence/m/b;Ljava/lang/String;Lcom/google/android/gms/location/copresence/CopresenceSettings;Lcom/google/android/gms/location/copresence/internal/i;)V

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/google/android/location/copresence/ap;->a(Ljava/lang/Runnable;Landroid/os/WorkSource;)V

    .line 109
    return-void
.end method

.method public final a(Ljava/lang/String;Lcom/google/android/gms/location/copresence/debug/CopresenceDebugPokeRequest;Lcom/google/android/gms/location/copresence/internal/i;)V
    .locals 3

    .prologue
    .line 132
    invoke-direct {p0}, Lcom/google/android/location/copresence/m/b;->b()Z

    move-result v0

    if-nez v0, :cond_0

    .line 133
    new-instance v0, Ljava/lang/SecurityException;

    const-string v1, ""

    invoke-direct {v0, v1}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 135
    :cond_0
    iget-object v0, p0, Lcom/google/android/location/copresence/m/b;->c:Lcom/google/android/location/copresence/ap;

    new-instance v1, Lcom/google/android/location/copresence/m/g;

    invoke-direct {v1, p0, p1, p2, p3}, Lcom/google/android/location/copresence/m/g;-><init>(Lcom/google/android/location/copresence/m/b;Ljava/lang/String;Lcom/google/android/gms/location/copresence/debug/CopresenceDebugPokeRequest;Lcom/google/android/gms/location/copresence/internal/i;)V

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/google/android/location/copresence/ap;->a(Ljava/lang/Runnable;Landroid/os/WorkSource;)V

    .line 141
    return-void
.end method

.method public final a(Ljava/lang/String;Lcom/google/android/gms/location/copresence/internal/i;)V
    .locals 3

    .prologue
    .line 116
    const-string v0, "CopresenceServiceImpl: "

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "GoogleLocationManagerService: getCopresenceSettings: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 117
    invoke-direct {p0}, Lcom/google/android/location/copresence/m/b;->b()Z

    move-result v0

    if-nez v0, :cond_0

    .line 118
    new-instance v0, Ljava/lang/SecurityException;

    const-string v1, "You have no access to copresences settings."

    invoke-direct {v0, v1}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 120
    :cond_0
    iget-object v0, p0, Lcom/google/android/location/copresence/m/b;->c:Lcom/google/android/location/copresence/ap;

    new-instance v1, Lcom/google/android/location/copresence/m/f;

    invoke-direct {v1, p0, p1, p2}, Lcom/google/android/location/copresence/m/f;-><init>(Lcom/google/android/location/copresence/m/b;Ljava/lang/String;Lcom/google/android/gms/location/copresence/internal/i;)V

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/google/android/location/copresence/ap;->a(Ljava/lang/Runnable;Landroid/os/WorkSource;)V

    .line 127
    return-void
.end method

.method public final dump(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 162
    iget-object v0, p0, Lcom/google/android/location/copresence/m/b;->d:Lcom/google/android/location/copresence/r;

    if-eqz v0, :cond_0

    .line 163
    const-string v0, "\nCopresence State:"

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 164
    iget-object v0, p0, Lcom/google/android/location/copresence/m/b;->d:Lcom/google/android/location/copresence/r;

    invoke-static {p2}, Lcom/google/android/location/copresence/ag;->a(Ljava/io/PrintWriter;)V

    new-instance v1, Ljava/util/concurrent/CountDownLatch;

    const/4 v2, 0x1

    invoke-direct {v1, v2}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V

    iget-object v2, v0, Lcom/google/android/location/copresence/r;->a:Lcom/google/android/location/copresence/ap;

    new-instance v3, Lcom/google/android/location/copresence/u;

    invoke-direct {v3, v0, p2, v1}, Lcom/google/android/location/copresence/u;-><init>(Lcom/google/android/location/copresence/r;Ljava/io/PrintWriter;Ljava/util/concurrent/CountDownLatch;)V

    const/4 v0, 0x0

    invoke-virtual {v2, v3, v0}, Lcom/google/android/location/copresence/ap;->a(Ljava/lang/Runnable;Landroid/os/WorkSource;)V

    const-wide/16 v2, 0x5

    :try_start_0
    sget-object v0, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v1, v2, v3, v0}, Ljava/util/concurrent/CountDownLatch;->await(JLjava/util/concurrent/TimeUnit;)Z
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 165
    :goto_0
    const-string v0, "\nEnd Copresence State\n"

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 167
    :cond_0
    return-void

    .line 164
    :catch_0
    move-exception v0

    const-string v0, "Dump interrupted."

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    goto :goto_0
.end method
