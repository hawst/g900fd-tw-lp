.class public final Lcom/google/android/location/places/ui/be;
.super Landroid/support/v4/app/Fragment;
.source "SourceFile"

# interfaces
.implements Landroid/text/TextWatcher;
.implements Landroid/view/View$OnKeyListener;
.implements Landroid/widget/AdapterView$OnItemClickListener;
.implements Landroid/widget/TextView$OnEditorActionListener;


# instance fields
.field private a:Landroid/view/View;

.field private b:Landroid/view/View;

.field private c:Landroid/widget/EditText;

.field private d:Landroid/widget/ImageButton;

.field private e:Landroid/widget/ListView;

.field private f:Lcom/google/android/location/places/ui/bi;

.field private g:Lcom/google/android/location/places/ui/bk;

.field private h:Lcom/google/android/location/places/ui/bh;

.field private i:Lcom/google/android/location/places/ui/r;

.field private j:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 36
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    .line 367
    return-void
.end method

.method static synthetic a(Lcom/google/android/location/places/ui/be;)Landroid/widget/EditText;
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lcom/google/android/location/places/ui/be;->c:Landroid/widget/EditText;

    return-object v0
.end method

.method public static a()Lcom/google/android/location/places/ui/be;
    .locals 1

    .prologue
    .line 64
    new-instance v0, Lcom/google/android/location/places/ui/be;

    invoke-direct {v0}, Lcom/google/android/location/places/ui/be;-><init>()V

    .line 65
    return-object v0
.end method

.method private a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 144
    iget-object v0, p0, Lcom/google/android/location/places/ui/be;->f:Lcom/google/android/location/places/ui/bi;

    invoke-virtual {v0}, Lcom/google/android/location/places/ui/bi;->clear()V

    .line 146
    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 147
    iget-object v0, p0, Lcom/google/android/location/places/ui/be;->h:Lcom/google/android/location/places/ui/bh;

    if-eqz v0, :cond_0

    .line 148
    iget-object v0, p0, Lcom/google/android/location/places/ui/be;->h:Lcom/google/android/location/places/ui/bh;

    invoke-interface {v0}, Lcom/google/android/location/places/ui/bh;->k()V

    .line 156
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/google/android/location/places/ui/be;->c:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->clearFocus()V

    .line 157
    invoke-virtual {p0}, Lcom/google/android/location/places/ui/be;->g()V

    .line 158
    return-void

    .line 151
    :cond_1
    iget-object v0, p0, Lcom/google/android/location/places/ui/be;->h:Lcom/google/android/location/places/ui/bh;

    if-eqz v0, :cond_0

    .line 152
    iget-object v0, p0, Lcom/google/android/location/places/ui/be;->h:Lcom/google/android/location/places/ui/bh;

    invoke-interface {v0}, Lcom/google/android/location/places/ui/bh;->l()V

    goto :goto_0
.end method

.method static synthetic b(Lcom/google/android/location/places/ui/be;)Landroid/view/View;
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lcom/google/android/location/places/ui/be;->a:Landroid/view/View;

    return-object v0
.end method

.method static synthetic c(Lcom/google/android/location/places/ui/be;)Landroid/widget/ListView;
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lcom/google/android/location/places/ui/be;->e:Landroid/widget/ListView;

    return-object v0
.end method

.method static synthetic d(Lcom/google/android/location/places/ui/be;)Lcom/google/android/location/places/ui/r;
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lcom/google/android/location/places/ui/be;->i:Lcom/google/android/location/places/ui/r;

    return-object v0
.end method

.method static synthetic e(Lcom/google/android/location/places/ui/be;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 36
    iget-boolean v0, p0, Lcom/google/android/location/places/ui/be;->j:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/location/places/ui/be;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    sget v1, Lcom/google/android/gms/b;->t:I

    invoke-static {v0, v1}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setFillAfter(Z)V

    iget-object v1, p0, Lcom/google/android/location/places/ui/be;->a:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    iget-object v0, p0, Lcom/google/android/location/places/ui/be;->a:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    iput-boolean v2, p0, Lcom/google/android/location/places/ui/be;->j:Z

    :cond_0
    return-void
.end method

.method static synthetic f(Lcom/google/android/location/places/ui/be;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 36
    iget-boolean v0, p0, Lcom/google/android/location/places/ui/be;->j:Z

    if-nez v0, :cond_0

    iput-boolean v2, p0, Lcom/google/android/location/places/ui/be;->j:Z

    invoke-virtual {p0}, Lcom/google/android/location/places/ui/be;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    sget v1, Lcom/google/android/gms/b;->z:I

    invoke-static {v0, v1}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/animation/Animation;->setFillAfter(Z)V

    new-instance v1, Lcom/google/android/location/places/ui/bg;

    invoke-direct {v1, p0}, Lcom/google/android/location/places/ui/bg;-><init>(Lcom/google/android/location/places/ui/be;)V

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    iget-object v1, p0, Lcom/google/android/location/places/ui/be;->a:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    :cond_0
    return-void
.end method

.method static synthetic g(Lcom/google/android/location/places/ui/be;)Lcom/google/android/location/places/ui/bi;
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lcom/google/android/location/places/ui/be;->f:Lcom/google/android/location/places/ui/bi;

    return-object v0
.end method


# virtual methods
.method public final a(Lcom/google/android/location/places/ui/bh;)Lcom/google/android/location/places/ui/be;
    .locals 0

    .prologue
    .line 74
    iput-object p1, p0, Lcom/google/android/location/places/ui/be;->h:Lcom/google/android/location/places/ui/bh;

    .line 75
    return-object p0
.end method

.method public final a(Lcom/google/android/location/places/ui/r;)Lcom/google/android/location/places/ui/be;
    .locals 0

    .prologue
    .line 69
    iput-object p1, p0, Lcom/google/android/location/places/ui/be;->i:Lcom/google/android/location/places/ui/r;

    .line 70
    return-object p0
.end method

.method public final a(Landroid/widget/ListView;)V
    .locals 1

    .prologue
    .line 132
    iget-object v0, p0, Lcom/google/android/location/places/ui/be;->g:Lcom/google/android/location/places/ui/bk;

    invoke-virtual {p1, v0}, Landroid/widget/ListView;->setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    .line 133
    return-void
.end method

.method public final a(Ljava/lang/String;Z)V
    .locals 1

    .prologue
    .line 136
    iget-object v0, p0, Lcom/google/android/location/places/ui/be;->c:Landroid/widget/EditText;

    invoke-virtual {v0, p1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 138
    if-eqz p2, :cond_0

    .line 139
    invoke-direct {p0, p1}, Lcom/google/android/location/places/ui/be;->a(Ljava/lang/String;)V

    .line 141
    :cond_0
    return-void
.end method

.method public final a([Lcom/google/android/gms/location/places/AutocompletePrediction;)V
    .locals 4

    .prologue
    .line 267
    iget-object v0, p0, Lcom/google/android/location/places/ui/be;->c:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->hasFocus()Z

    move-result v0

    if-nez v0, :cond_1

    .line 279
    :cond_0
    :goto_0
    return-void

    .line 271
    :cond_1
    iget-object v0, p0, Lcom/google/android/location/places/ui/be;->f:Lcom/google/android/location/places/ui/bi;

    invoke-virtual {v0}, Lcom/google/android/location/places/ui/bi;->clear()V

    .line 272
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_2

    .line 273
    iget-object v0, p0, Lcom/google/android/location/places/ui/be;->f:Lcom/google/android/location/places/ui/bi;

    invoke-virtual {v0, p1}, Lcom/google/android/location/places/ui/bi;->addAll([Ljava/lang/Object;)V

    goto :goto_0

    .line 275
    :cond_2
    array-length v1, p1

    const/4 v0, 0x0

    :goto_1
    if-ge v0, v1, :cond_0

    aget-object v2, p1, v0

    .line 276
    iget-object v3, p0, Lcom/google/android/location/places/ui/be;->f:Lcom/google/android/location/places/ui/bi;

    invoke-virtual {v3, v2}, Lcom/google/android/location/places/ui/bi;->add(Ljava/lang/Object;)V

    .line 275
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method

.method public final afterTextChanged(Landroid/text/Editable;)V
    .locals 0

    .prologue
    .line 255
    return-void
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 123
    iget-object v0, p0, Lcom/google/android/location/places/ui/be;->c:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    .line 124
    if-eqz v0, :cond_0

    .line 125
    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    .line 127
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 233
    return-void
.end method

.method public final c()V
    .locals 2

    .prologue
    .line 161
    const-string v0, ""

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/google/android/location/places/ui/be;->a(Ljava/lang/String;Z)V

    .line 162
    return-void
.end method

.method public final d()Z
    .locals 1

    .prologue
    .line 169
    iget-object v0, p0, Lcom/google/android/location/places/ui/be;->c:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->hasFocus()Z

    move-result v0

    return v0
.end method

.method public final e()V
    .locals 1

    .prologue
    .line 282
    iget-object v0, p0, Lcom/google/android/location/places/ui/be;->c:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->clearFocus()V

    .line 283
    return-void
.end method

.method public final f()V
    .locals 1

    .prologue
    .line 286
    iget-object v0, p0, Lcom/google/android/location/places/ui/be;->f:Lcom/google/android/location/places/ui/bi;

    invoke-virtual {v0}, Lcom/google/android/location/places/ui/bi;->clear()V

    .line 287
    return-void
.end method

.method public final g()V
    .locals 2

    .prologue
    .line 290
    invoke-virtual {p0}, Lcom/google/android/location/places/ui/be;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/location/places/ui/be;->c:Landroid/widget/EditText;

    invoke-static {v0, v1}, Lcom/google/android/location/places/ui/bm;->a(Landroid/content/Context;Landroid/view/View;)V

    .line 291
    return-void
.end method

.method public final onActivityCreated(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 87
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 89
    invoke-virtual {p0}, Lcom/google/android/location/places/ui/be;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    sget v1, Lcom/google/android/gms/j;->oC:I

    invoke-virtual {v0, v1}, Landroid/support/v4/app/q;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/places/ui/be;->a:Landroid/view/View;

    .line 90
    iget-object v0, p0, Lcom/google/android/location/places/ui/be;->b:Landroid/view/View;

    sget v1, Lcom/google/android/gms/j;->oD:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/google/android/location/places/ui/be;->c:Landroid/widget/EditText;

    .line 91
    iget-object v0, p0, Lcom/google/android/location/places/ui/be;->b:Landroid/view/View;

    sget v1, Lcom/google/android/gms/j;->oA:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/google/android/location/places/ui/be;->d:Landroid/widget/ImageButton;

    .line 92
    iget-object v0, p0, Lcom/google/android/location/places/ui/be;->b:Landroid/view/View;

    sget v1, Lcom/google/android/gms/j;->oE:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/google/android/location/places/ui/be;->e:Landroid/widget/ListView;

    .line 93
    new-instance v0, Lcom/google/android/location/places/ui/bi;

    invoke-virtual {p0}, Lcom/google/android/location/places/ui/be;->getActivity()Landroid/support/v4/app/q;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/google/android/location/places/ui/bi;-><init>(Lcom/google/android/location/places/ui/be;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/location/places/ui/be;->f:Lcom/google/android/location/places/ui/bi;

    .line 94
    new-instance v0, Lcom/google/android/location/places/ui/bk;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/android/location/places/ui/bk;-><init>(Lcom/google/android/location/places/ui/be;B)V

    iput-object v0, p0, Lcom/google/android/location/places/ui/be;->g:Lcom/google/android/location/places/ui/bk;

    .line 97
    iget-object v0, p0, Lcom/google/android/location/places/ui/be;->e:Landroid/widget/ListView;

    new-instance v1, Landroid/view/View;

    invoke-virtual {p0}, Lcom/google/android/location/places/ui/be;->getActivity()Landroid/support/v4/app/q;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    const/4 v2, 0x0

    const/4 v3, 0x1

    invoke-virtual {v0, v1, v2, v3}, Landroid/widget/ListView;->addHeaderView(Landroid/view/View;Ljava/lang/Object;Z)V

    .line 98
    iget-object v0, p0, Lcom/google/android/location/places/ui/be;->e:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/google/android/location/places/ui/be;->f:Lcom/google/android/location/places/ui/bi;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 99
    iget-object v0, p0, Lcom/google/android/location/places/ui/be;->e:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/google/android/location/places/ui/be;->f:Lcom/google/android/location/places/ui/bi;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 100
    iget-object v0, p0, Lcom/google/android/location/places/ui/be;->e:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/google/android/location/places/ui/be;->f:Lcom/google/android/location/places/ui/bi;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 102
    iget-object v0, p0, Lcom/google/android/location/places/ui/be;->c:Landroid/widget/EditText;

    invoke-virtual {v0, p0}, Landroid/widget/EditText;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    .line 103
    iget-object v0, p0, Lcom/google/android/location/places/ui/be;->c:Landroid/widget/EditText;

    invoke-virtual {v0, p0}, Landroid/widget/EditText;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 104
    iget-object v0, p0, Lcom/google/android/location/places/ui/be;->c:Landroid/widget/EditText;

    invoke-virtual {v0, p0}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 106
    iget-object v0, p0, Lcom/google/android/location/places/ui/be;->d:Landroid/widget/ImageButton;

    new-instance v1, Lcom/google/android/location/places/ui/bf;

    invoke-direct {v1, p0}, Lcom/google/android/location/places/ui/bf;-><init>(Lcom/google/android/location/places/ui/be;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 113
    iget-object v0, p0, Lcom/google/android/location/places/ui/be;->d:Landroid/widget/ImageButton;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 114
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2

    .prologue
    .line 80
    sget v0, Lcom/google/android/gms/l;->de:I

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 81
    sget v1, Lcom/google/android/gms/j;->oB:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/location/places/ui/be;->b:Landroid/view/View;

    .line 82
    return-object v0
.end method

.method public final onDestroyView()V
    .locals 1

    .prologue
    .line 118
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/location/places/ui/be;->e:Landroid/widget/ListView;

    .line 119
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onDestroyView()V

    .line 120
    return-void
.end method

.method public final onEditorAction(Landroid/widget/TextView;ILandroid/view/KeyEvent;)Z
    .locals 2

    .prologue
    .line 207
    invoke-virtual {p1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    .line 208
    const/4 v1, 0x3

    if-ne p2, v1, :cond_0

    .line 209
    invoke-direct {p0, v0}, Lcom/google/android/location/places/ui/be;->a(Ljava/lang/String;)V

    .line 210
    const/4 v0, 0x1

    .line 212
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 2

    .prologue
    .line 260
    invoke-virtual {p1, p3}, Landroid/widget/AdapterView;->getItemAtPosition(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 261
    if-eqz v0, :cond_0

    .line 262
    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/location/places/ui/be;->a(Ljava/lang/String;Z)V

    .line 264
    :cond_0
    return-void
.end method

.method public final onKey(Landroid/view/View;ILandroid/view/KeyEvent;)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 217
    iget-object v1, p0, Lcom/google/android/location/places/ui/be;->c:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    .line 218
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v2

    if-ne v2, v0, :cond_0

    .line 219
    const/16 v2, 0x42

    if-ne p2, v2, :cond_0

    .line 220
    invoke-virtual {p1}, Landroid/view/View;->cancelLongPress()V

    .line 223
    invoke-direct {p0, v1}, Lcom/google/android/location/places/ui/be;->a(Ljava/lang/String;)V

    .line 227
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 3

    .prologue
    .line 237
    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    .line 238
    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 239
    iget-object v0, p0, Lcom/google/android/location/places/ui/be;->d:Landroid/widget/ImageButton;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 240
    iget-object v0, p0, Lcom/google/android/location/places/ui/be;->f:Lcom/google/android/location/places/ui/bi;

    invoke-virtual {v0}, Lcom/google/android/location/places/ui/bi;->clear()V

    .line 241
    iget-object v0, p0, Lcom/google/android/location/places/ui/be;->h:Lcom/google/android/location/places/ui/bh;

    if-eqz v0, :cond_0

    .line 242
    iget-object v0, p0, Lcom/google/android/location/places/ui/be;->h:Lcom/google/android/location/places/ui/bh;

    invoke-interface {v0}, Lcom/google/android/location/places/ui/bh;->l()V

    .line 250
    :cond_0
    :goto_0
    return-void

    .line 245
    :cond_1
    iget-object v1, p0, Lcom/google/android/location/places/ui/be;->d:Landroid/widget/ImageButton;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 246
    iget-object v1, p0, Lcom/google/android/location/places/ui/be;->h:Lcom/google/android/location/places/ui/bh;

    if-eqz v1, :cond_0

    .line 247
    iget-object v1, p0, Lcom/google/android/location/places/ui/be;->h:Lcom/google/android/location/places/ui/bh;

    invoke-interface {v1, v0}, Lcom/google/android/location/places/ui/bh;->a(Ljava/lang/String;)V

    goto :goto_0
.end method
