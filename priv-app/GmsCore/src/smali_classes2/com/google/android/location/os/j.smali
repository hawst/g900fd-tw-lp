.class public final Lcom/google/android/location/os/j;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public final a:Lcom/google/android/location/os/at;

.field private final b:Ljava/util/LinkedList;

.field private final c:Lcom/google/android/location/o/a/b;

.field private final d:Ljava/io/PrintWriter;

.field private final e:Ljava/util/Date;

.field private final f:Ljava/lang/StringBuffer;

.field private final g:Ljava/text/FieldPosition;

.field private final h:Ljava/text/SimpleDateFormat;

.field private final i:Lcom/google/android/location/os/b;

.field private final j:Ljava/util/EnumSet;

.field private final k:[Lcom/google/android/location/os/as;

.field private final l:[I

.field private final m:[J

.field private n:J

.field private final o:J

.field private p:I


# direct methods
.method public constructor <init>(Lcom/google/android/location/os/at;Lcom/google/android/location/o/a/b;Ljava/io/PrintWriter;Lcom/google/android/location/os/b;)V
    .locals 4

    .prologue
    .line 130
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 92
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/google/android/location/os/j;->b:Ljava/util/LinkedList;

    .line 97
    new-instance v0, Ljava/util/Date;

    invoke-direct {v0}, Ljava/util/Date;-><init>()V

    iput-object v0, p0, Lcom/google/android/location/os/j;->e:Ljava/util/Date;

    .line 98
    new-instance v0, Ljava/lang/StringBuffer;

    const/16 v1, 0x14

    invoke-direct {v0, v1}, Ljava/lang/StringBuffer;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/location/os/j;->f:Ljava/lang/StringBuffer;

    .line 99
    new-instance v0, Ljava/text/FieldPosition;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/text/FieldPosition;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/location/os/j;->g:Ljava/text/FieldPosition;

    .line 100
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v1, "yyyy.MM.dd HH:mm:ss "

    invoke-direct {v0, v1}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/location/os/j;->h:Ljava/text/SimpleDateFormat;

    .line 103
    sget-object v0, Lcom/google/android/location/os/au;->i:Lcom/google/android/location/os/au;

    sget-object v1, Lcom/google/android/location/os/au;->Q:Lcom/google/android/location/os/au;

    invoke-static {v0, v1}, Ljava/util/EnumSet;->of(Ljava/lang/Enum;Ljava/lang/Enum;)Ljava/util/EnumSet;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/os/j;->j:Ljava/util/EnumSet;

    .line 109
    invoke-static {}, Lcom/google/android/location/os/au;->values()[Lcom/google/android/location/os/au;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [Lcom/google/android/location/os/as;

    iput-object v0, p0, Lcom/google/android/location/os/j;->k:[Lcom/google/android/location/os/as;

    .line 111
    invoke-static {}, Lcom/google/android/location/os/au;->values()[Lcom/google/android/location/os/au;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/google/android/location/os/j;->l:[I

    .line 113
    invoke-static {}, Lcom/google/android/location/os/au;->values()[Lcom/google/android/location/os/au;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [J

    iput-object v0, p0, Lcom/google/android/location/os/j;->m:[J

    .line 114
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/location/os/j;->n:J

    .line 122
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/location/os/j;->p:I

    .line 131
    iput-object p1, p0, Lcom/google/android/location/os/j;->a:Lcom/google/android/location/os/at;

    .line 132
    iput-object p2, p0, Lcom/google/android/location/os/j;->c:Lcom/google/android/location/o/a/b;

    .line 133
    iput-object p3, p0, Lcom/google/android/location/os/j;->d:Ljava/io/PrintWriter;

    .line 134
    iput-object p4, p0, Lcom/google/android/location/os/j;->i:Lcom/google/android/location/os/b;

    .line 135
    iget-object v0, p0, Lcom/google/android/location/os/j;->m:[J

    const-wide/16 v2, -0x1

    invoke-static {v0, v2, v3}, Ljava/util/Arrays;->fill([JJ)V

    .line 136
    invoke-interface {p1}, Lcom/google/android/location/os/at;->a()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/location/os/j;->o:J

    .line 137
    new-instance v0, Lcom/google/android/location/os/as;

    sget-object v1, Lcom/google/android/location/os/au;->c:Lcom/google/android/location/os/au;

    iget-wide v2, p0, Lcom/google/android/location/os/j;->o:J

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/location/os/as;-><init>(Lcom/google/android/location/os/au;J)V

    invoke-virtual {p0, v0}, Lcom/google/android/location/os/j;->a(Lcom/google/android/location/os/as;)V

    .line 138
    return-void
.end method

.method private declared-synchronized a(Ljava/io/PrintWriter;)V
    .locals 4

    .prologue
    .line 680
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/location/os/j;->b:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/os/as;

    .line 681
    iget-object v2, v0, Lcom/google/android/location/os/as;->f:Lcom/google/android/location/os/au;

    iget-object v2, v2, Lcom/google/android/location/os/au;->ao:Lcom/google/android/location/os/av;

    sget-object v3, Lcom/google/android/location/os/av;->a:Lcom/google/android/location/os/av;

    if-ne v2, v3, :cond_0

    .line 682
    const/16 v2, 0xa

    invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->print(C)V

    .line 684
    :cond_0
    invoke-virtual {v0, p1}, Lcom/google/android/location/os/as;->b(Ljava/io/PrintWriter;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 680
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 686
    :cond_1
    monitor-exit p0

    return-void
.end method


# virtual methods
.method public final a(I)V
    .locals 7

    .prologue
    .line 511
    new-instance v1, Lcom/google/android/location/os/aa;

    sget-object v3, Lcom/google/android/location/os/au;->X:Lcom/google/android/location/os/au;

    iget-object v0, p0, Lcom/google/android/location/os/j;->a:Lcom/google/android/location/os/at;

    invoke-interface {v0}, Lcom/google/android/location/os/at;->a()J

    move-result-wide v4

    move-object v2, p0

    move v6, p1

    invoke-direct/range {v1 .. v6}, Lcom/google/android/location/os/aa;-><init>(Lcom/google/android/location/os/j;Lcom/google/android/location/os/au;JI)V

    invoke-virtual {p0, v1, p1}, Lcom/google/android/location/os/j;->a(Lcom/google/android/location/os/as;I)V

    .line 517
    return-void
.end method

.method public final a(ILjava/lang/String;)V
    .locals 9

    .prologue
    const/4 v8, -0x1

    .line 564
    new-instance v1, Lcom/google/android/location/os/af;

    sget-object v3, Lcom/google/android/location/os/au;->aj:Lcom/google/android/location/os/au;

    iget-object v0, p0, Lcom/google/android/location/os/j;->a:Lcom/google/android/location/os/at;

    invoke-interface {v0}, Lcom/google/android/location/os/at;->a()J

    move-result-wide v4

    move-object v2, p0

    move v6, p1

    move-object v7, p2

    invoke-direct/range {v1 .. v7}, Lcom/google/android/location/os/af;-><init>(Lcom/google/android/location/os/j;Lcom/google/android/location/os/au;JILjava/lang/String;)V

    move-object v0, p0

    move v2, p1

    move v3, v8

    move v4, v8

    move-object v5, p2

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/location/os/j;->a(Lcom/google/android/location/os/as;IIILjava/lang/String;)V

    .line 571
    return-void
.end method

.method public final a(Lcom/google/android/gms/location/ActivityRecognitionResult;Z)V
    .locals 8

    .prologue
    .line 499
    new-instance v1, Lcom/google/android/location/os/z;

    sget-object v3, Lcom/google/android/location/os/au;->W:Lcom/google/android/location/os/au;

    iget-object v0, p0, Lcom/google/android/location/os/j;->a:Lcom/google/android/location/os/at;

    invoke-interface {v0}, Lcom/google/android/location/os/at;->a()J

    move-result-wide v4

    move-object v2, p0

    move-object v6, p1

    move v7, p2

    invoke-direct/range {v1 .. v7}, Lcom/google/android/location/os/z;-><init>(Lcom/google/android/location/os/j;Lcom/google/android/location/os/au;JLcom/google/android/gms/location/ActivityRecognitionResult;Z)V

    invoke-virtual {p1}, Lcom/google/android/gms/location/ActivityRecognitionResult;->b()Lcom/google/android/gms/location/DetectedActivity;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/location/DetectedActivity;->a()I

    move-result v2

    invoke-virtual {p1}, Lcom/google/android/gms/location/ActivityRecognitionResult;->b()Lcom/google/android/gms/location/DetectedActivity;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/location/DetectedActivity;->b()I

    move-result v3

    if-eqz p2, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p0, v1, v2, v3, v0}, Lcom/google/android/location/os/j;->a(Lcom/google/android/location/os/as;III)V

    .line 508
    return-void

    .line 499
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Lcom/google/android/location/e/h;)V
    .locals 7

    .prologue
    .line 323
    new-instance v1, Lcom/google/android/location/os/ao;

    sget-object v3, Lcom/google/android/location/os/au;->h:Lcom/google/android/location/os/au;

    iget-object v0, p0, Lcom/google/android/location/os/j;->a:Lcom/google/android/location/os/at;

    invoke-interface {v0}, Lcom/google/android/location/os/at;->a()J

    move-result-wide v4

    move-object v2, p0

    move-object v6, p1

    invoke-direct/range {v1 .. v6}, Lcom/google/android/location/os/ao;-><init>(Lcom/google/android/location/os/j;Lcom/google/android/location/os/au;JLcom/google/android/location/e/h;)V

    invoke-virtual {p0, v1}, Lcom/google/android/location/os/j;->a(Lcom/google/android/location/os/as;)V

    .line 329
    return-void
.end method

.method public final declared-synchronized a(Lcom/google/android/location/os/as;)V
    .locals 6

    .prologue
    .line 180
    monitor-enter p0

    const/4 v2, -0x1

    const/4 v3, -0x1

    const/4 v4, -0x1

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    :try_start_0
    invoke-virtual/range {v0 .. v5}, Lcom/google/android/location/os/j;->a(Lcom/google/android/location/os/as;IIILjava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 181
    monitor-exit p0

    return-void

    .line 180
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Lcom/google/android/location/os/as;I)V
    .locals 6

    .prologue
    .line 184
    monitor-enter p0

    const/4 v3, -0x1

    const/4 v4, -0x1

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    :try_start_0
    invoke-virtual/range {v0 .. v5}, Lcom/google/android/location/os/j;->a(Lcom/google/android/location/os/as;IIILjava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 185
    monitor-exit p0

    return-void

    .line 184
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Lcom/google/android/location/os/as;II)V
    .locals 6

    .prologue
    .line 188
    monitor-enter p0

    const/4 v4, -0x1

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move v3, p3

    :try_start_0
    invoke-virtual/range {v0 .. v5}, Lcom/google/android/location/os/j;->a(Lcom/google/android/location/os/as;IIILjava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 189
    monitor-exit p0

    return-void

    .line 188
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Lcom/google/android/location/os/as;III)V
    .locals 6

    .prologue
    .line 192
    monitor-enter p0

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    :try_start_0
    invoke-virtual/range {v0 .. v5}, Lcom/google/android/location/os/j;->a(Lcom/google/android/location/os/as;IIILjava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 193
    monitor-exit p0

    return-void

    .line 192
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Lcom/google/android/location/os/as;IIILjava/lang/String;)V
    .locals 8

    .prologue
    .line 204
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/location/os/j;->b:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 205
    iget-object v0, p0, Lcom/google/android/location/os/j;->b:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->getLast()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/os/as;

    .line 206
    iget-object v1, p0, Lcom/google/android/location/os/j;->m:[J

    iget-object v0, v0, Lcom/google/android/location/os/as;->f:Lcom/google/android/location/os/au;

    invoke-virtual {v0}, Lcom/google/android/location/os/au;->ordinal()I

    move-result v0

    iget-wide v2, p1, Lcom/google/android/location/os/as;->g:J

    aput-wide v2, v1, v0

    .line 208
    :cond_0
    iget-object v0, p0, Lcom/google/android/location/os/j;->k:[Lcom/google/android/location/os/as;

    iget-object v1, p1, Lcom/google/android/location/os/as;->f:Lcom/google/android/location/os/au;

    invoke-virtual {v1}, Lcom/google/android/location/os/au;->ordinal()I

    move-result v1

    aput-object p1, v0, v1

    .line 209
    iget-object v0, p0, Lcom/google/android/location/os/j;->l:[I

    iget-object v1, p1, Lcom/google/android/location/os/as;->f:Lcom/google/android/location/os/au;

    invoke-virtual {v1}, Lcom/google/android/location/os/au;->ordinal()I

    move-result v1

    aget v2, v0, v1

    add-int/lit8 v2, v2, 0x1

    aput v2, v0, v1

    .line 212
    iget-object v0, p1, Lcom/google/android/location/os/as;->f:Lcom/google/android/location/os/au;

    sget-object v1, Lcom/google/android/location/os/au;->o:Lcom/google/android/location/os/au;

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lcom/google/android/location/os/j;->b:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->size()I

    move-result v0

    const/4 v1, 0x2

    if-lt v0, v1, :cond_1

    iget-object v0, p0, Lcom/google/android/location/os/j;->b:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->getLast()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/os/as;

    iget-object v0, v0, Lcom/google/android/location/os/as;->f:Lcom/google/android/location/os/au;

    sget-object v1, Lcom/google/android/location/os/au;->o:Lcom/google/android/location/os/au;

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lcom/google/android/location/os/j;->b:Ljava/util/LinkedList;

    iget-object v1, p0, Lcom/google/android/location/os/j;->b:Ljava/util/LinkedList;

    invoke-virtual {v1}, Ljava/util/LinkedList;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x2

    invoke-virtual {v0, v1}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/os/as;

    iget-object v0, v0, Lcom/google/android/location/os/as;->f:Lcom/google/android/location/os/au;

    sget-object v1, Lcom/google/android/location/os/au;->o:Lcom/google/android/location/os/au;

    if-ne v0, v1, :cond_1

    .line 215
    iget-object v0, p0, Lcom/google/android/location/os/j;->b:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->removeLast()Ljava/lang/Object;

    .line 217
    :cond_1
    iget-object v0, p0, Lcom/google/android/location/os/j;->b:Ljava/util/LinkedList;

    invoke-virtual {v0, p1}, Ljava/util/LinkedList;->addLast(Ljava/lang/Object;)V

    .line 218
    :goto_0
    iget-object v0, p0, Lcom/google/android/location/os/j;->b:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->size()I

    move-result v0

    const/16 v1, 0xc8

    if-le v0, v1, :cond_2

    .line 219
    iget-object v0, p0, Lcom/google/android/location/os/j;->b:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->removeFirst()Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 204
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 221
    :cond_2
    :try_start_1
    iget-object v0, p1, Lcom/google/android/location/os/as;->f:Lcom/google/android/location/os/au;

    iget-object v0, v0, Lcom/google/android/location/os/au;->ao:Lcom/google/android/location/os/av;

    sget-object v1, Lcom/google/android/location/os/av;->a:Lcom/google/android/location/os/av;

    if-ne v0, v1, :cond_3

    .line 222
    iget-wide v0, p1, Lcom/google/android/location/os/as;->g:J

    iput-wide v0, p0, Lcom/google/android/location/os/j;->n:J

    .line 224
    :cond_3
    iget-object v0, p1, Lcom/google/android/location/os/as;->f:Lcom/google/android/location/os/au;

    .line 225
    sget-object v1, Lcom/google/android/location/os/au;->z:Lcom/google/android/location/os/au;

    if-eq v0, v1, :cond_4

    sget-object v1, Lcom/google/android/location/os/au;->x:Lcom/google/android/location/os/au;

    if-eq v0, v1, :cond_4

    sget-object v1, Lcom/google/android/location/os/au;->f:Lcom/google/android/location/os/au;

    if-eq v0, v1, :cond_4

    sget-object v1, Lcom/google/android/location/os/au;->R:Lcom/google/android/location/os/au;

    if-eq v0, v1, :cond_4

    sget-object v1, Lcom/google/android/location/os/au;->S:Lcom/google/android/location/os/au;

    if-ne v0, v1, :cond_8

    .line 228
    :cond_4
    iput p2, p0, Lcom/google/android/location/os/j;->p:I

    .line 232
    :goto_1
    iget-object v0, p0, Lcom/google/android/location/os/j;->c:Lcom/google/android/location/o/a/b;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/google/android/location/os/j;->j:Ljava/util/EnumSet;

    iget-object v1, p1, Lcom/google/android/location/os/as;->f:Lcom/google/android/location/os/au;

    invoke-virtual {v0, v1}, Ljava/util/EnumSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_5

    iget-object v0, p0, Lcom/google/android/location/os/j;->c:Lcom/google/android/location/o/a/b;

    const-string v1, "gmmNlpEventLog"

    const/4 v2, 0x3

    invoke-interface {v0, v1, v2}, Lcom/google/android/location/o/a/b;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 234
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 235
    new-instance v1, Ljava/io/PrintWriter;

    invoke-direct {v1, v0}, Ljava/io/PrintWriter;-><init>(Ljava/io/OutputStream;)V

    .line 236
    invoke-virtual {p1, v1}, Lcom/google/android/location/os/as;->b(Ljava/io/PrintWriter;)V

    .line 237
    invoke-virtual {v1}, Ljava/io/PrintWriter;->close()V

    .line 238
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toString()Ljava/lang/String;

    move-result-object v0

    .line 242
    iget-object v1, p0, Lcom/google/android/location/os/j;->c:Lcom/google/android/location/o/a/b;

    const-string v2, "gmmNlpEventLog"

    invoke-interface {v1, v2, v0}, Lcom/google/android/location/o/a/b;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 245
    :cond_5
    iget-object v0, p0, Lcom/google/android/location/os/j;->d:Ljava/io/PrintWriter;

    if-eqz v0, :cond_6

    .line 246
    iget-object v0, p0, Lcom/google/android/location/os/j;->e:Ljava/util/Date;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Ljava/util/Date;->setTime(J)V

    .line 247
    iget-object v0, p0, Lcom/google/android/location/os/j;->f:Ljava/lang/StringBuffer;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->setLength(I)V

    .line 248
    iget-object v0, p0, Lcom/google/android/location/os/j;->h:Ljava/text/SimpleDateFormat;

    iget-object v1, p0, Lcom/google/android/location/os/j;->e:Ljava/util/Date;

    iget-object v2, p0, Lcom/google/android/location/os/j;->f:Ljava/lang/StringBuffer;

    iget-object v3, p0, Lcom/google/android/location/os/j;->g:Ljava/text/FieldPosition;

    invoke-virtual {v0, v1, v2, v3}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;Ljava/lang/StringBuffer;Ljava/text/FieldPosition;)Ljava/lang/StringBuffer;

    .line 249
    iget-object v0, p0, Lcom/google/android/location/os/j;->d:Ljava/io/PrintWriter;

    iget-object v1, p0, Lcom/google/android/location/os/j;->f:Ljava/lang/StringBuffer;

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 250
    iget-object v0, p0, Lcom/google/android/location/os/j;->d:Ljava/io/PrintWriter;

    invoke-virtual {p1, v0}, Lcom/google/android/location/os/as;->b(Ljava/io/PrintWriter;)V

    .line 251
    iget-object v0, p0, Lcom/google/android/location/os/j;->d:Ljava/io/PrintWriter;

    invoke-virtual {v0}, Ljava/io/PrintWriter;->flush()V

    .line 253
    :cond_6
    iget-object v0, p0, Lcom/google/android/location/os/j;->i:Lcom/google/android/location/os/b;

    if-eqz v0, :cond_7

    iget-object v0, p1, Lcom/google/android/location/os/as;->f:Lcom/google/android/location/os/au;

    sget-object v1, Lcom/google/android/location/os/au;->Q:Lcom/google/android/location/os/au;

    if-eq v0, v1, :cond_7

    .line 254
    iget-object v0, p0, Lcom/google/android/location/os/j;->i:Lcom/google/android/location/os/b;

    iget-object v1, p1, Lcom/google/android/location/os/as;->f:Lcom/google/android/location/os/au;

    iget-wide v2, p1, Lcom/google/android/location/os/as;->g:J

    move v4, p2

    move v5, p3

    move v6, p4

    move-object v7, p5

    invoke-virtual/range {v0 .. v7}, Lcom/google/android/location/os/b;->a(Lcom/google/android/location/os/au;JIIILjava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 256
    :cond_7
    monitor-exit p0

    return-void

    .line 230
    :cond_8
    const/4 v0, -0x1

    :try_start_2
    iput v0, p0, Lcom/google/android/location/os/j;->p:I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_1
.end method

.method public final a(Lcom/google/android/location/os/au;)V
    .locals 4

    .prologue
    .line 259
    new-instance v0, Lcom/google/android/location/os/as;

    iget-object v1, p0, Lcom/google/android/location/os/j;->a:Lcom/google/android/location/os/at;

    invoke-interface {v1}, Lcom/google/android/location/os/at;->a()J

    move-result-wide v2

    invoke-direct {v0, p1, v2, v3}, Lcom/google/android/location/os/as;-><init>(Lcom/google/android/location/os/au;J)V

    invoke-virtual {p0, v0}, Lcom/google/android/location/os/j;->a(Lcom/google/android/location/os/as;)V

    .line 260
    return-void
.end method

.method public final a(Lcom/google/android/location/os/aw;)V
    .locals 10

    .prologue
    const-wide v8, 0x416312d000000000L    # 1.0E7

    .line 361
    new-instance v1, Lcom/google/android/location/os/l;

    sget-object v3, Lcom/google/android/location/os/au;->o:Lcom/google/android/location/os/au;

    iget-object v0, p0, Lcom/google/android/location/os/j;->a:Lcom/google/android/location/os/at;

    invoke-interface {v0}, Lcom/google/android/location/os/at;->a()J

    move-result-wide v4

    move-object v2, p0

    move-object v6, p1

    invoke-direct/range {v1 .. v6}, Lcom/google/android/location/os/l;-><init>(Lcom/google/android/location/os/j;Lcom/google/android/location/os/au;JLcom/google/android/location/os/aw;)V

    invoke-interface {p1}, Lcom/google/android/location/os/aw;->b()D

    move-result-wide v2

    mul-double/2addr v2, v8

    invoke-static {v2, v3}, Ljava/lang/Math;->round(D)J

    move-result-wide v2

    long-to-int v0, v2

    invoke-interface {p1}, Lcom/google/android/location/os/aw;->c()D

    move-result-wide v2

    mul-double/2addr v2, v8

    invoke-static {v2, v3}, Ljava/lang/Math;->round(D)J

    move-result-wide v2

    long-to-int v2, v2

    invoke-interface {p1}, Lcom/google/android/location/os/aw;->a()F

    move-result v3

    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    move-result v3

    invoke-virtual {p0, v1, v0, v2, v3}, Lcom/google/android/location/os/j;->a(Lcom/google/android/location/os/as;III)V

    .line 368
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 9

    .prologue
    .line 655
    new-instance v1, Lcom/google/android/location/os/al;

    sget-object v3, Lcom/google/android/location/os/au;->Q:Lcom/google/android/location/os/au;

    iget-object v0, p0, Lcom/google/android/location/os/j;->a:Lcom/google/android/location/os/at;

    invoke-interface {v0}, Lcom/google/android/location/os/at;->a()J

    move-result-wide v4

    move-object v2, p0

    move-object v6, p3

    move-object v7, p1

    move-object v8, p2

    invoke-direct/range {v1 .. v8}, Lcom/google/android/location/os/al;-><init>(Lcom/google/android/location/os/j;Lcom/google/android/location/os/au;JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0, v1}, Lcom/google/android/location/os/j;->a(Lcom/google/android/location/os/as;)V

    .line 665
    return-void
.end method

.method public final declared-synchronized a(Ljava/text/Format;JLjava/io/PrintWriter;)V
    .locals 10

    .prologue
    .line 696
    monitor-enter p0

    :try_start_0
    sget-boolean v0, Lcom/google/android/location/i/a;->c:Z

    if-eqz v0, :cond_0

    const-string v0, "gmmNlpEventLog"

    const-string v1, "dump"

    invoke-static {v0, v1}, Lcom/google/android/location/o/a/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 697
    :cond_0
    iget-object v0, p0, Lcom/google/android/location/os/j;->a:Lcom/google/android/location/os/at;

    invoke-interface {v0}, Lcom/google/android/location/os/at;->a()J

    move-result-wide v0

    .line 698
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "elapsedRealtime "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p4, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 699
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Process restart time: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v4, p0, Lcom/google/android/location/os/j;->o:J

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p4, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 700
    invoke-virtual {p4}, Ljava/io/PrintWriter;->flush()V

    .line 701
    new-instance v2, Ljava/util/Date;

    const-wide/16 v4, 0x0

    invoke-direct {v2, v4, v5}, Ljava/util/Date;-><init>(J)V

    .line 702
    const-wide/32 v4, 0x927c0

    sub-long v4, v0, v4

    .line 703
    iget-object v0, p0, Lcom/google/android/location/os/j;->b:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/os/as;

    .line 704
    iget-wide v6, v0, Lcom/google/android/location/os/as;->g:J

    cmp-long v3, v6, v4

    if-ltz v3, :cond_1

    .line 705
    iget-object v3, v0, Lcom/google/android/location/os/as;->f:Lcom/google/android/location/os/au;

    iget-object v3, v3, Lcom/google/android/location/os/au;->ao:Lcom/google/android/location/os/av;

    sget-object v6, Lcom/google/android/location/os/av;->a:Lcom/google/android/location/os/av;

    if-ne v3, v6, :cond_2

    .line 708
    const/16 v3, 0xa

    invoke-virtual {p4, v3}, Ljava/io/PrintWriter;->print(C)V

    .line 710
    :cond_2
    invoke-virtual {v2, p2, p3}, Ljava/util/Date;->setTime(J)V

    .line 711
    invoke-virtual {v2}, Ljava/util/Date;->getTime()J

    move-result-wide v6

    iget-wide v8, v0, Lcom/google/android/location/os/as;->g:J

    add-long/2addr v6, v8

    invoke-virtual {v2, v6, v7}, Ljava/util/Date;->setTime(J)V

    invoke-virtual {p1, v2}, Ljava/text/Format;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p4, v3}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const/16 v3, 0x20

    invoke-virtual {p4, v3}, Ljava/io/PrintWriter;->print(C)V

    invoke-virtual {v0, p4}, Lcom/google/android/location/os/as;->b(Ljava/io/PrintWriter;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 696
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 713
    :cond_3
    :try_start_1
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "elapsedRealtime "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/location/os/j;->a:Lcom/google/android/location/os/at;

    invoke-interface {v1}, Lcom/google/android/location/os/at;->a()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p4, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 714
    invoke-virtual {p4}, Ljava/io/PrintWriter;->flush()V

    .line 717
    iget-object v0, p0, Lcom/google/android/location/os/j;->i:Lcom/google/android/location/os/b;

    if-eqz v0, :cond_4

    .line 718
    iget-object v0, p0, Lcom/google/android/location/os/j;->i:Lcom/google/android/location/os/b;

    iget-object v1, p0, Lcom/google/android/location/os/j;->a:Lcom/google/android/location/os/at;

    invoke-virtual {v0, p4, v1}, Lcom/google/android/location/os/b;->a(Ljava/io/PrintWriter;Lcom/google/android/location/os/at;)V

    .line 719
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "elapsedRealtime "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/location/os/j;->a:Lcom/google/android/location/os/at;

    invoke-interface {v1}, Lcom/google/android/location/os/at;->a()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p4, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 721
    :cond_4
    monitor-exit p0

    return-void
.end method

.method public final a(Z)V
    .locals 7

    .prologue
    .line 291
    new-instance v1, Lcom/google/android/location/os/ag;

    sget-object v3, Lcom/google/android/location/os/au;->e:Lcom/google/android/location/os/au;

    iget-object v0, p0, Lcom/google/android/location/os/j;->a:Lcom/google/android/location/os/at;

    invoke-interface {v0}, Lcom/google/android/location/os/at;->a()J

    move-result-wide v4

    move-object v2, p0

    move v6, p1

    invoke-direct/range {v1 .. v6}, Lcom/google/android/location/os/ag;-><init>(Lcom/google/android/location/os/j;Lcom/google/android/location/os/au;JZ)V

    if-eqz p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p0, v1, v0}, Lcom/google/android/location/os/j;->a(Lcom/google/android/location/os/as;I)V

    .line 297
    return-void

    .line 291
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(ZZI)V
    .locals 10

    .prologue
    const/4 v0, 0x1

    const/4 v9, 0x0

    .line 351
    new-instance v1, Lcom/google/android/location/os/ar;

    sget-object v3, Lcom/google/android/location/os/au;->p:Lcom/google/android/location/os/au;

    iget-object v2, p0, Lcom/google/android/location/os/j;->a:Lcom/google/android/location/os/at;

    invoke-interface {v2}, Lcom/google/android/location/os/at;->a()J

    move-result-wide v4

    move-object v2, p0

    move v6, p1

    move v7, p2

    move v8, p3

    invoke-direct/range {v1 .. v8}, Lcom/google/android/location/os/ar;-><init>(Lcom/google/android/location/os/j;Lcom/google/android/location/os/au;JZZI)V

    if-eqz p1, :cond_0

    move v2, v0

    :goto_0
    if-eqz p2, :cond_1

    :goto_1
    invoke-virtual {p0, v1, v2, v0, p3}, Lcom/google/android/location/os/j;->a(Lcom/google/android/location/os/as;III)V

    .line 358
    return-void

    :cond_0
    move v2, v9

    .line 351
    goto :goto_0

    :cond_1
    move v0, v9

    goto :goto_1
.end method

.method public final b(Z)V
    .locals 7

    .prologue
    .line 371
    new-instance v1, Lcom/google/android/location/os/m;

    sget-object v3, Lcom/google/android/location/os/au;->r:Lcom/google/android/location/os/au;

    iget-object v0, p0, Lcom/google/android/location/os/j;->a:Lcom/google/android/location/os/at;

    invoke-interface {v0}, Lcom/google/android/location/os/at;->a()J

    move-result-wide v4

    move-object v2, p0

    move v6, p1

    invoke-direct/range {v1 .. v6}, Lcom/google/android/location/os/m;-><init>(Lcom/google/android/location/os/j;Lcom/google/android/location/os/au;JZ)V

    if-eqz p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p0, v1, v0}, Lcom/google/android/location/os/j;->a(Lcom/google/android/location/os/as;I)V

    .line 377
    return-void

    .line 371
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c(Z)V
    .locals 7

    .prologue
    .line 389
    new-instance v1, Lcom/google/android/location/os/o;

    sget-object v3, Lcom/google/android/location/os/au;->t:Lcom/google/android/location/os/au;

    iget-object v0, p0, Lcom/google/android/location/os/j;->a:Lcom/google/android/location/os/at;

    invoke-interface {v0}, Lcom/google/android/location/os/at;->a()J

    move-result-wide v4

    move-object v2, p0

    move v6, p1

    invoke-direct/range {v1 .. v6}, Lcom/google/android/location/os/o;-><init>(Lcom/google/android/location/os/j;Lcom/google/android/location/os/au;JZ)V

    if-eqz p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p0, v1, v0}, Lcom/google/android/location/os/j;->a(Lcom/google/android/location/os/as;I)V

    .line 395
    return-void

    .line 389
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final d(Z)V
    .locals 7

    .prologue
    .line 529
    new-instance v1, Lcom/google/android/location/os/ac;

    sget-object v3, Lcom/google/android/location/os/au;->w:Lcom/google/android/location/os/au;

    iget-object v0, p0, Lcom/google/android/location/os/j;->a:Lcom/google/android/location/os/at;

    invoke-interface {v0}, Lcom/google/android/location/os/at;->a()J

    move-result-wide v4

    move-object v2, p0

    move v6, p1

    invoke-direct/range {v1 .. v6}, Lcom/google/android/location/os/ac;-><init>(Lcom/google/android/location/os/j;Lcom/google/android/location/os/au;JZ)V

    if-eqz p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p0, v1, v0}, Lcom/google/android/location/os/j;->a(Lcom/google/android/location/os/as;I)V

    .line 535
    return-void

    .line 529
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 672
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    const/16 v1, 0x2710

    invoke-direct {v0, v1}, Ljava/io/ByteArrayOutputStream;-><init>(I)V

    .line 673
    new-instance v1, Ljava/io/PrintWriter;

    invoke-direct {v1, v0}, Ljava/io/PrintWriter;-><init>(Ljava/io/OutputStream;)V

    .line 674
    invoke-direct {p0, v1}, Lcom/google/android/location/os/j;->a(Ljava/io/PrintWriter;)V

    .line 675
    invoke-virtual {v1}, Ljava/io/PrintWriter;->close()V

    .line 676
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
