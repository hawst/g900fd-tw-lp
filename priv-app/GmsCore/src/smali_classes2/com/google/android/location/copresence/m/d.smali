.class final Lcom/google/android/location/copresence/m/d;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Landroid/os/IBinder;

.field final synthetic b:Ljava/lang/String;

.field final synthetic c:Ljava/lang/String;

.field final synthetic d:Ljava/lang/String;

.field final synthetic e:Lcom/google/android/gms/location/copresence/internal/BatchImpl;

.field final synthetic f:Lcom/google/android/gms/location/copresence/internal/i;

.field final synthetic g:Lcom/google/android/location/copresence/m/b;


# direct methods
.method constructor <init>(Lcom/google/android/location/copresence/m/b;Landroid/os/IBinder;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/location/copresence/internal/BatchImpl;Lcom/google/android/gms/location/copresence/internal/i;)V
    .locals 0

    .prologue
    .line 82
    iput-object p1, p0, Lcom/google/android/location/copresence/m/d;->g:Lcom/google/android/location/copresence/m/b;

    iput-object p2, p0, Lcom/google/android/location/copresence/m/d;->a:Landroid/os/IBinder;

    iput-object p3, p0, Lcom/google/android/location/copresence/m/d;->b:Ljava/lang/String;

    iput-object p4, p0, Lcom/google/android/location/copresence/m/d;->c:Ljava/lang/String;

    iput-object p5, p0, Lcom/google/android/location/copresence/m/d;->d:Ljava/lang/String;

    iput-object p6, p0, Lcom/google/android/location/copresence/m/d;->e:Lcom/google/android/gms/location/copresence/internal/BatchImpl;

    iput-object p7, p0, Lcom/google/android/location/copresence/m/d;->f:Lcom/google/android/gms/location/copresence/internal/i;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 9

    .prologue
    const/4 v8, 0x3

    .line 85
    iget-object v0, p0, Lcom/google/android/location/copresence/m/d;->g:Lcom/google/android/location/copresence/m/b;

    invoke-static {v0}, Lcom/google/android/location/copresence/m/b;->b(Lcom/google/android/location/copresence/m/b;)Lcom/google/android/location/copresence/r;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/location/copresence/m/d;->a:Landroid/os/IBinder;

    iget-object v1, p0, Lcom/google/android/location/copresence/m/d;->b:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/location/copresence/m/d;->c:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/location/copresence/m/d;->d:Ljava/lang/String;

    iget-object v5, p0, Lcom/google/android/location/copresence/m/d;->e:Lcom/google/android/gms/location/copresence/internal/BatchImpl;

    iget-object v6, p0, Lcom/google/android/location/copresence/m/d;->f:Lcom/google/android/gms/location/copresence/internal/i;

    invoke-static {v8}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v7

    if-eqz v7, :cond_0

    const-string v7, "CopresenceHelper: copresenceBatch called"

    invoke-static {v7}, Lcom/google/android/location/copresence/ag;->b(Ljava/lang/String;)I

    :cond_0
    if-eqz v6, :cond_2

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_1

    if-nez v5, :cond_3

    :cond_1
    const/16 v0, 0xa

    invoke-static {v6, v0}, Lcom/google/android/location/copresence/r;->a(Lcom/google/android/gms/location/copresence/internal/i;I)V

    .line 87
    :cond_2
    :goto_0
    return-void

    .line 85
    :cond_3
    invoke-static {}, Lcom/google/android/location/copresence/f/a;->b()Ljava/lang/Boolean;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v7

    if-nez v7, :cond_4

    invoke-static {v6, v8}, Lcom/google/android/location/copresence/r;->a(Lcom/google/android/gms/location/copresence/internal/i;I)V

    goto :goto_0

    :cond_4
    iget-object v7, v2, Lcom/google/android/location/copresence/r;->a:Lcom/google/android/location/copresence/ap;

    invoke-virtual {v7}, Lcom/google/android/location/copresence/ap;->c()V

    if-eqz v0, :cond_5

    :goto_1
    invoke-virtual {v2, v4}, Lcom/google/android/location/copresence/r;->c(Ljava/lang/String;)Lcom/google/android/location/copresence/a/a;

    move-result-object v1

    if-nez v1, :cond_6

    const/16 v0, 0x9c9

    invoke-static {v6, v0}, Lcom/google/android/location/copresence/r;->a(Lcom/google/android/gms/location/copresence/internal/i;I)V

    goto :goto_0

    :cond_5
    move-object v0, v1

    goto :goto_1

    :cond_6
    iget-object v4, v2, Lcom/google/android/location/copresence/r;->c:Lcom/google/android/location/copresence/e/a;

    invoke-virtual {v4, v0, v3}, Lcom/google/android/location/copresence/e/a;->a(Ljava/lang/String;Landroid/os/IBinder;)Lcom/google/android/location/copresence/e/c;

    move-result-object v3

    if-eqz v3, :cond_2

    iget-object v3, v2, Lcom/google/android/location/copresence/r;->a:Lcom/google/android/location/copresence/ap;

    invoke-virtual {v3}, Lcom/google/android/location/copresence/ap;->c()V

    iget-object v3, v5, Lcom/google/android/gms/location/copresence/internal/BatchImpl;->a:Ljava/util/ArrayList;

    invoke-virtual {v2, v0, v1, v3, v6}, Lcom/google/android/location/copresence/r;->a(Ljava/lang/String;Lcom/google/android/location/copresence/a/a;Ljava/util/ArrayList;Lcom/google/android/gms/location/copresence/internal/i;)V

    invoke-virtual {v2}, Lcom/google/android/location/copresence/r;->a()V

    goto :goto_0
.end method
