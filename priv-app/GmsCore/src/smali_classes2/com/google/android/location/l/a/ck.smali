.class public final Lcom/google/android/location/l/a/ck;
.super Lcom/google/protobuf/nano/d;
.source "SourceFile"


# static fields
.field private static volatile g:[Lcom/google/android/location/l/a/ck;


# instance fields
.field public a:[D

.field public b:[D

.field public c:[D

.field public d:[D

.field public e:[D

.field public f:Ljava/lang/Integer;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 41
    invoke-direct {p0}, Lcom/google/protobuf/nano/d;-><init>()V

    .line 42
    sget-object v0, Lcom/google/protobuf/nano/m;->d:[D

    iput-object v0, p0, Lcom/google/android/location/l/a/ck;->a:[D

    sget-object v0, Lcom/google/protobuf/nano/m;->d:[D

    iput-object v0, p0, Lcom/google/android/location/l/a/ck;->b:[D

    sget-object v0, Lcom/google/protobuf/nano/m;->d:[D

    iput-object v0, p0, Lcom/google/android/location/l/a/ck;->c:[D

    sget-object v0, Lcom/google/protobuf/nano/m;->d:[D

    iput-object v0, p0, Lcom/google/android/location/l/a/ck;->d:[D

    sget-object v0, Lcom/google/protobuf/nano/m;->d:[D

    iput-object v0, p0, Lcom/google/android/location/l/a/ck;->e:[D

    iput-object v1, p0, Lcom/google/android/location/l/a/ck;->f:Ljava/lang/Integer;

    iput-object v1, p0, Lcom/google/android/location/l/a/ck;->unknownFieldData:Lcom/google/protobuf/nano/f;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/location/l/a/ck;->cachedSize:I

    .line 43
    return-void
.end method

.method public static a()[Lcom/google/android/location/l/a/ck;
    .locals 2

    .prologue
    .line 12
    sget-object v0, Lcom/google/android/location/l/a/ck;->g:[Lcom/google/android/location/l/a/ck;

    if-nez v0, :cond_1

    .line 13
    sget-object v1, Lcom/google/protobuf/nano/h;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 15
    :try_start_0
    sget-object v0, Lcom/google/android/location/l/a/ck;->g:[Lcom/google/android/location/l/a/ck;

    if-nez v0, :cond_0

    .line 16
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/android/location/l/a/ck;

    sput-object v0, Lcom/google/android/location/l/a/ck;->g:[Lcom/google/android/location/l/a/ck;

    .line 18
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 20
    :cond_1
    sget-object v0, Lcom/google/android/location/l/a/ck;->g:[Lcom/google/android/location/l/a/ck;

    return-object v0

    .line 18
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 151
    invoke-super {p0}, Lcom/google/protobuf/nano/d;->computeSerializedSize()I

    move-result v0

    .line 152
    iget-object v1, p0, Lcom/google/android/location/l/a/ck;->a:[D

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/location/l/a/ck;->a:[D

    array-length v1, v1

    if-lez v1, :cond_0

    .line 153
    iget-object v1, p0, Lcom/google/android/location/l/a/ck;->a:[D

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x8

    .line 154
    add-int/2addr v0, v1

    .line 155
    iget-object v1, p0, Lcom/google/android/location/l/a/ck;->a:[D

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 157
    :cond_0
    iget-object v1, p0, Lcom/google/android/location/l/a/ck;->b:[D

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/android/location/l/a/ck;->b:[D

    array-length v1, v1

    if-lez v1, :cond_1

    .line 158
    iget-object v1, p0, Lcom/google/android/location/l/a/ck;->b:[D

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x8

    .line 159
    add-int/2addr v0, v1

    .line 160
    iget-object v1, p0, Lcom/google/android/location/l/a/ck;->b:[D

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 162
    :cond_1
    iget-object v1, p0, Lcom/google/android/location/l/a/ck;->c:[D

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/google/android/location/l/a/ck;->c:[D

    array-length v1, v1

    if-lez v1, :cond_2

    .line 163
    iget-object v1, p0, Lcom/google/android/location/l/a/ck;->c:[D

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x8

    .line 164
    add-int/2addr v0, v1

    .line 165
    iget-object v1, p0, Lcom/google/android/location/l/a/ck;->c:[D

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 167
    :cond_2
    iget-object v1, p0, Lcom/google/android/location/l/a/ck;->d:[D

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/google/android/location/l/a/ck;->d:[D

    array-length v1, v1

    if-lez v1, :cond_3

    .line 168
    iget-object v1, p0, Lcom/google/android/location/l/a/ck;->d:[D

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x8

    .line 169
    add-int/2addr v0, v1

    .line 170
    iget-object v1, p0, Lcom/google/android/location/l/a/ck;->d:[D

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 172
    :cond_3
    iget-object v1, p0, Lcom/google/android/location/l/a/ck;->e:[D

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/google/android/location/l/a/ck;->e:[D

    array-length v1, v1

    if-lez v1, :cond_4

    .line 173
    iget-object v1, p0, Lcom/google/android/location/l/a/ck;->e:[D

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x8

    .line 174
    add-int/2addr v0, v1

    .line 175
    iget-object v1, p0, Lcom/google/android/location/l/a/ck;->e:[D

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 177
    :cond_4
    iget-object v1, p0, Lcom/google/android/location/l/a/ck;->f:Ljava/lang/Integer;

    if-eqz v1, :cond_5

    .line 178
    const/4 v1, 0x6

    iget-object v2, p0, Lcom/google/android/location/l/a/ck;->f:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 181
    :cond_5
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 59
    if-ne p1, p0, :cond_1

    .line 60
    const/4 v0, 0x1

    .line 93
    :cond_0
    :goto_0
    return v0

    .line 62
    :cond_1
    instance-of v1, p1, Lcom/google/android/location/l/a/ck;

    if-eqz v1, :cond_0

    .line 65
    check-cast p1, Lcom/google/android/location/l/a/ck;

    .line 66
    iget-object v1, p0, Lcom/google/android/location/l/a/ck;->a:[D

    iget-object v2, p1, Lcom/google/android/location/l/a/ck;->a:[D

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/h;->a([D[D)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 70
    iget-object v1, p0, Lcom/google/android/location/l/a/ck;->b:[D

    iget-object v2, p1, Lcom/google/android/location/l/a/ck;->b:[D

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/h;->a([D[D)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 74
    iget-object v1, p0, Lcom/google/android/location/l/a/ck;->c:[D

    iget-object v2, p1, Lcom/google/android/location/l/a/ck;->c:[D

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/h;->a([D[D)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 78
    iget-object v1, p0, Lcom/google/android/location/l/a/ck;->d:[D

    iget-object v2, p1, Lcom/google/android/location/l/a/ck;->d:[D

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/h;->a([D[D)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 82
    iget-object v1, p0, Lcom/google/android/location/l/a/ck;->e:[D

    iget-object v2, p1, Lcom/google/android/location/l/a/ck;->e:[D

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/h;->a([D[D)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 86
    iget-object v1, p0, Lcom/google/android/location/l/a/ck;->f:Ljava/lang/Integer;

    if-nez v1, :cond_3

    .line 87
    iget-object v1, p1, Lcom/google/android/location/l/a/ck;->f:Ljava/lang/Integer;

    if-nez v1, :cond_0

    .line 93
    :cond_2
    invoke-virtual {p0, p1}, Lcom/google/android/location/l/a/ck;->unknownFieldDataEquals(Lcom/google/protobuf/nano/d;)Z

    move-result v0

    goto :goto_0

    .line 90
    :cond_3
    iget-object v1, p0, Lcom/google/android/location/l/a/ck;->f:Ljava/lang/Integer;

    iget-object v2, p1, Lcom/google/android/location/l/a/ck;->f:Ljava/lang/Integer;

    invoke-virtual {v1, v2}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 2

    .prologue
    .line 98
    iget-object v0, p0, Lcom/google/android/location/l/a/ck;->a:[D

    invoke-static {v0}, Lcom/google/protobuf/nano/h;->a([D)I

    move-result v0

    add-int/lit16 v0, v0, 0x20f

    .line 101
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/google/android/location/l/a/ck;->b:[D

    invoke-static {v1}, Lcom/google/protobuf/nano/h;->a([D)I

    move-result v1

    add-int/2addr v0, v1

    .line 103
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/google/android/location/l/a/ck;->c:[D

    invoke-static {v1}, Lcom/google/protobuf/nano/h;->a([D)I

    move-result v1

    add-int/2addr v0, v1

    .line 105
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/google/android/location/l/a/ck;->d:[D

    invoke-static {v1}, Lcom/google/protobuf/nano/h;->a([D)I

    move-result v1

    add-int/2addr v0, v1

    .line 107
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/google/android/location/l/a/ck;->e:[D

    invoke-static {v1}, Lcom/google/protobuf/nano/h;->a([D)I

    move-result v1

    add-int/2addr v0, v1

    .line 109
    mul-int/lit8 v1, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/location/l/a/ck;->f:Ljava/lang/Integer;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    add-int/2addr v0, v1

    .line 111
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/google/android/location/l/a/ck;->unknownFieldDataHashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 112
    return v0

    .line 109
    :cond_0
    iget-object v0, p0, Lcom/google/android/location/l/a/ck;->f:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->hashCode()I

    move-result v0

    goto :goto_0
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 5
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/android/location/l/a/ck;->storeUnknownField(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    const/16 v0, 0x9

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->b(Lcom/google/protobuf/nano/a;I)I

    move-result v2

    iget-object v0, p0, Lcom/google/android/location/l/a/ck;->a:[D

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [D

    if-eqz v0, :cond_1

    iget-object v3, p0, Lcom/google/android/location/l/a/ck;->a:[D

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_1
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_3

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->l()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Double;->longBitsToDouble(J)D

    move-result-wide v4

    aput-wide v4, v2, v0

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    iget-object v0, p0, Lcom/google/android/location/l/a/ck;->a:[D

    array-length v0, v0

    goto :goto_1

    :cond_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->l()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Double;->longBitsToDouble(J)D

    move-result-wide v4

    aput-wide v4, v2, v0

    iput-object v2, p0, Lcom/google/android/location/l/a/ck;->a:[D

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->c(I)I

    move-result v2

    div-int/lit8 v3, v0, 0x8

    iget-object v0, p0, Lcom/google/android/location/l/a/ck;->a:[D

    if-nez v0, :cond_5

    move v0, v1

    :goto_3
    add-int/2addr v3, v0

    new-array v3, v3, [D

    if-eqz v0, :cond_4

    iget-object v4, p0, Lcom/google/android/location/l/a/ck;->a:[D

    invoke-static {v4, v1, v3, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_4
    :goto_4
    array-length v4, v3

    if-ge v0, v4, :cond_6

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->l()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Double;->longBitsToDouble(J)D

    move-result-wide v4

    aput-wide v4, v3, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_5
    iget-object v0, p0, Lcom/google/android/location/l/a/ck;->a:[D

    array-length v0, v0

    goto :goto_3

    :cond_6
    iput-object v3, p0, Lcom/google/android/location/l/a/ck;->a:[D

    invoke-virtual {p1, v2}, Lcom/google/protobuf/nano/a;->d(I)V

    goto :goto_0

    :sswitch_3
    const/16 v0, 0x11

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->b(Lcom/google/protobuf/nano/a;I)I

    move-result v2

    iget-object v0, p0, Lcom/google/android/location/l/a/ck;->b:[D

    if-nez v0, :cond_8

    move v0, v1

    :goto_5
    add-int/2addr v2, v0

    new-array v2, v2, [D

    if-eqz v0, :cond_7

    iget-object v3, p0, Lcom/google/android/location/l/a/ck;->b:[D

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_7
    :goto_6
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_9

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->l()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Double;->longBitsToDouble(J)D

    move-result-wide v4

    aput-wide v4, v2, v0

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_6

    :cond_8
    iget-object v0, p0, Lcom/google/android/location/l/a/ck;->b:[D

    array-length v0, v0

    goto :goto_5

    :cond_9
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->l()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Double;->longBitsToDouble(J)D

    move-result-wide v4

    aput-wide v4, v2, v0

    iput-object v2, p0, Lcom/google/android/location/l/a/ck;->b:[D

    goto/16 :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->c(I)I

    move-result v2

    div-int/lit8 v3, v0, 0x8

    iget-object v0, p0, Lcom/google/android/location/l/a/ck;->b:[D

    if-nez v0, :cond_b

    move v0, v1

    :goto_7
    add-int/2addr v3, v0

    new-array v3, v3, [D

    if-eqz v0, :cond_a

    iget-object v4, p0, Lcom/google/android/location/l/a/ck;->b:[D

    invoke-static {v4, v1, v3, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_a
    :goto_8
    array-length v4, v3

    if-ge v0, v4, :cond_c

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->l()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Double;->longBitsToDouble(J)D

    move-result-wide v4

    aput-wide v4, v3, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_8

    :cond_b
    iget-object v0, p0, Lcom/google/android/location/l/a/ck;->b:[D

    array-length v0, v0

    goto :goto_7

    :cond_c
    iput-object v3, p0, Lcom/google/android/location/l/a/ck;->b:[D

    invoke-virtual {p1, v2}, Lcom/google/protobuf/nano/a;->d(I)V

    goto/16 :goto_0

    :sswitch_5
    const/16 v0, 0x19

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->b(Lcom/google/protobuf/nano/a;I)I

    move-result v2

    iget-object v0, p0, Lcom/google/android/location/l/a/ck;->c:[D

    if-nez v0, :cond_e

    move v0, v1

    :goto_9
    add-int/2addr v2, v0

    new-array v2, v2, [D

    if-eqz v0, :cond_d

    iget-object v3, p0, Lcom/google/android/location/l/a/ck;->c:[D

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_d
    :goto_a
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_f

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->l()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Double;->longBitsToDouble(J)D

    move-result-wide v4

    aput-wide v4, v2, v0

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_a

    :cond_e
    iget-object v0, p0, Lcom/google/android/location/l/a/ck;->c:[D

    array-length v0, v0

    goto :goto_9

    :cond_f
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->l()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Double;->longBitsToDouble(J)D

    move-result-wide v4

    aput-wide v4, v2, v0

    iput-object v2, p0, Lcom/google/android/location/l/a/ck;->c:[D

    goto/16 :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->c(I)I

    move-result v2

    div-int/lit8 v3, v0, 0x8

    iget-object v0, p0, Lcom/google/android/location/l/a/ck;->c:[D

    if-nez v0, :cond_11

    move v0, v1

    :goto_b
    add-int/2addr v3, v0

    new-array v3, v3, [D

    if-eqz v0, :cond_10

    iget-object v4, p0, Lcom/google/android/location/l/a/ck;->c:[D

    invoke-static {v4, v1, v3, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_10
    :goto_c
    array-length v4, v3

    if-ge v0, v4, :cond_12

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->l()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Double;->longBitsToDouble(J)D

    move-result-wide v4

    aput-wide v4, v3, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_c

    :cond_11
    iget-object v0, p0, Lcom/google/android/location/l/a/ck;->c:[D

    array-length v0, v0

    goto :goto_b

    :cond_12
    iput-object v3, p0, Lcom/google/android/location/l/a/ck;->c:[D

    invoke-virtual {p1, v2}, Lcom/google/protobuf/nano/a;->d(I)V

    goto/16 :goto_0

    :sswitch_7
    const/16 v0, 0x21

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->b(Lcom/google/protobuf/nano/a;I)I

    move-result v2

    iget-object v0, p0, Lcom/google/android/location/l/a/ck;->d:[D

    if-nez v0, :cond_14

    move v0, v1

    :goto_d
    add-int/2addr v2, v0

    new-array v2, v2, [D

    if-eqz v0, :cond_13

    iget-object v3, p0, Lcom/google/android/location/l/a/ck;->d:[D

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_13
    :goto_e
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_15

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->l()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Double;->longBitsToDouble(J)D

    move-result-wide v4

    aput-wide v4, v2, v0

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_e

    :cond_14
    iget-object v0, p0, Lcom/google/android/location/l/a/ck;->d:[D

    array-length v0, v0

    goto :goto_d

    :cond_15
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->l()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Double;->longBitsToDouble(J)D

    move-result-wide v4

    aput-wide v4, v2, v0

    iput-object v2, p0, Lcom/google/android/location/l/a/ck;->d:[D

    goto/16 :goto_0

    :sswitch_8
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->c(I)I

    move-result v2

    div-int/lit8 v3, v0, 0x8

    iget-object v0, p0, Lcom/google/android/location/l/a/ck;->d:[D

    if-nez v0, :cond_17

    move v0, v1

    :goto_f
    add-int/2addr v3, v0

    new-array v3, v3, [D

    if-eqz v0, :cond_16

    iget-object v4, p0, Lcom/google/android/location/l/a/ck;->d:[D

    invoke-static {v4, v1, v3, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_16
    :goto_10
    array-length v4, v3

    if-ge v0, v4, :cond_18

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->l()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Double;->longBitsToDouble(J)D

    move-result-wide v4

    aput-wide v4, v3, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_10

    :cond_17
    iget-object v0, p0, Lcom/google/android/location/l/a/ck;->d:[D

    array-length v0, v0

    goto :goto_f

    :cond_18
    iput-object v3, p0, Lcom/google/android/location/l/a/ck;->d:[D

    invoke-virtual {p1, v2}, Lcom/google/protobuf/nano/a;->d(I)V

    goto/16 :goto_0

    :sswitch_9
    const/16 v0, 0x29

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->b(Lcom/google/protobuf/nano/a;I)I

    move-result v2

    iget-object v0, p0, Lcom/google/android/location/l/a/ck;->e:[D

    if-nez v0, :cond_1a

    move v0, v1

    :goto_11
    add-int/2addr v2, v0

    new-array v2, v2, [D

    if-eqz v0, :cond_19

    iget-object v3, p0, Lcom/google/android/location/l/a/ck;->e:[D

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_19
    :goto_12
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_1b

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->l()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Double;->longBitsToDouble(J)D

    move-result-wide v4

    aput-wide v4, v2, v0

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_12

    :cond_1a
    iget-object v0, p0, Lcom/google/android/location/l/a/ck;->e:[D

    array-length v0, v0

    goto :goto_11

    :cond_1b
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->l()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Double;->longBitsToDouble(J)D

    move-result-wide v4

    aput-wide v4, v2, v0

    iput-object v2, p0, Lcom/google/android/location/l/a/ck;->e:[D

    goto/16 :goto_0

    :sswitch_a
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->c(I)I

    move-result v2

    div-int/lit8 v3, v0, 0x8

    iget-object v0, p0, Lcom/google/android/location/l/a/ck;->e:[D

    if-nez v0, :cond_1d

    move v0, v1

    :goto_13
    add-int/2addr v3, v0

    new-array v3, v3, [D

    if-eqz v0, :cond_1c

    iget-object v4, p0, Lcom/google/android/location/l/a/ck;->e:[D

    invoke-static {v4, v1, v3, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_1c
    :goto_14
    array-length v4, v3

    if-ge v0, v4, :cond_1e

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->l()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Double;->longBitsToDouble(J)D

    move-result-wide v4

    aput-wide v4, v3, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_14

    :cond_1d
    iget-object v0, p0, Lcom/google/android/location/l/a/ck;->e:[D

    array-length v0, v0

    goto :goto_13

    :cond_1e
    iput-object v3, p0, Lcom/google/android/location/l/a/ck;->e:[D

    invoke-virtual {p1, v2}, Lcom/google/protobuf/nano/a;->d(I)V

    goto/16 :goto_0

    :sswitch_b
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/l/a/ck;->f:Ljava/lang/Integer;

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x9 -> :sswitch_1
        0xa -> :sswitch_2
        0x11 -> :sswitch_3
        0x12 -> :sswitch_4
        0x19 -> :sswitch_5
        0x1a -> :sswitch_6
        0x21 -> :sswitch_7
        0x22 -> :sswitch_8
        0x29 -> :sswitch_9
        0x2a -> :sswitch_a
        0x30 -> :sswitch_b
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 118
    iget-object v0, p0, Lcom/google/android/location/l/a/ck;->a:[D

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/location/l/a/ck;->a:[D

    array-length v0, v0

    if-lez v0, :cond_0

    move v0, v1

    .line 119
    :goto_0
    iget-object v2, p0, Lcom/google/android/location/l/a/ck;->a:[D

    array-length v2, v2

    if-ge v0, v2, :cond_0

    .line 120
    const/4 v2, 0x1

    iget-object v3, p0, Lcom/google/android/location/l/a/ck;->a:[D

    aget-wide v4, v3, v0

    invoke-virtual {p1, v2, v4, v5}, Lcom/google/protobuf/nano/b;->a(ID)V

    .line 119
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 123
    :cond_0
    iget-object v0, p0, Lcom/google/android/location/l/a/ck;->b:[D

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/location/l/a/ck;->b:[D

    array-length v0, v0

    if-lez v0, :cond_1

    move v0, v1

    .line 124
    :goto_1
    iget-object v2, p0, Lcom/google/android/location/l/a/ck;->b:[D

    array-length v2, v2

    if-ge v0, v2, :cond_1

    .line 125
    const/4 v2, 0x2

    iget-object v3, p0, Lcom/google/android/location/l/a/ck;->b:[D

    aget-wide v4, v3, v0

    invoke-virtual {p1, v2, v4, v5}, Lcom/google/protobuf/nano/b;->a(ID)V

    .line 124
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 128
    :cond_1
    iget-object v0, p0, Lcom/google/android/location/l/a/ck;->c:[D

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/location/l/a/ck;->c:[D

    array-length v0, v0

    if-lez v0, :cond_2

    move v0, v1

    .line 129
    :goto_2
    iget-object v2, p0, Lcom/google/android/location/l/a/ck;->c:[D

    array-length v2, v2

    if-ge v0, v2, :cond_2

    .line 130
    const/4 v2, 0x3

    iget-object v3, p0, Lcom/google/android/location/l/a/ck;->c:[D

    aget-wide v4, v3, v0

    invoke-virtual {p1, v2, v4, v5}, Lcom/google/protobuf/nano/b;->a(ID)V

    .line 129
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 133
    :cond_2
    iget-object v0, p0, Lcom/google/android/location/l/a/ck;->d:[D

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/location/l/a/ck;->d:[D

    array-length v0, v0

    if-lez v0, :cond_3

    move v0, v1

    .line 134
    :goto_3
    iget-object v2, p0, Lcom/google/android/location/l/a/ck;->d:[D

    array-length v2, v2

    if-ge v0, v2, :cond_3

    .line 135
    const/4 v2, 0x4

    iget-object v3, p0, Lcom/google/android/location/l/a/ck;->d:[D

    aget-wide v4, v3, v0

    invoke-virtual {p1, v2, v4, v5}, Lcom/google/protobuf/nano/b;->a(ID)V

    .line 134
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 138
    :cond_3
    iget-object v0, p0, Lcom/google/android/location/l/a/ck;->e:[D

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/google/android/location/l/a/ck;->e:[D

    array-length v0, v0

    if-lez v0, :cond_4

    .line 139
    :goto_4
    iget-object v0, p0, Lcom/google/android/location/l/a/ck;->e:[D

    array-length v0, v0

    if-ge v1, v0, :cond_4

    .line 140
    const/4 v0, 0x5

    iget-object v2, p0, Lcom/google/android/location/l/a/ck;->e:[D

    aget-wide v2, v2, v1

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/b;->a(ID)V

    .line 139
    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    .line 143
    :cond_4
    iget-object v0, p0, Lcom/google/android/location/l/a/ck;->f:Ljava/lang/Integer;

    if-eqz v0, :cond_5

    .line 144
    const/4 v0, 0x6

    iget-object v1, p0, Lcom/google/android/location/l/a/ck;->f:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 146
    :cond_5
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/d;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 147
    return-void
.end method
