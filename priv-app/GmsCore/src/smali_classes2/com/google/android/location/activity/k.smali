.class public final Lcom/google/android/location/activity/k;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/location/activity/bo;
.implements Lcom/google/android/location/e/aw;


# static fields
.field private static volatile F:Z

.field private static volatile G:Z

.field private static final a:[I


# instance fields
.field private A:J

.field private B:J

.field private C:Lcom/google/android/location/o/n;

.field private D:Z

.field private E:Ljava/util/Set;

.field private H:Z

.field private I:Z

.field private J:I

.field private K:I

.field private L:I

.field private M:J

.field private N:Lcom/google/android/location/e/b;

.field private O:J

.field private P:J

.field private Q:J

.field private R:Ljava/util/Map;

.field private S:J

.field private final b:Lcom/google/android/location/os/bi;

.field private final c:Lcom/google/android/location/activity/g;

.field private final d:Lcom/google/android/location/activity/i;

.field private e:Lcom/google/android/location/activity/av;

.field private final f:Lcom/google/android/location/activity/a;

.field private g:Lcom/google/android/location/e/b;

.field private h:Lcom/google/android/gms/location/ActivityRecognitionResult;

.field private i:Lcom/google/android/gms/location/ActivityRecognitionResult;

.field private j:Lcom/google/android/location/activity/bd;

.field private k:J

.field private final l:Lcom/google/android/location/activity/m;

.field private final m:Lcom/google/android/location/activity/ah;

.field private final n:Lcom/google/android/location/activity/at;

.field private final o:Lcom/google/android/location/activity/bn;

.field private p:Ljava/util/List;

.field private final q:Ljava/util/Map;

.field private r:Lcom/google/android/location/activity/a/o;

.field private s:Lcom/google/android/location/activity/a/o;

.field private final t:Lcom/google/android/location/activity/a/o;

.field private final u:Lcom/google/android/location/activity/a/o;

.field private final v:Lcom/google/android/location/activity/ay;

.field private w:I

.field private x:Ljava/util/Queue;

.field private y:Ljava/util/Queue;

.field private z:I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 71
    const/4 v0, 0x1

    new-array v0, v0, [I

    const/16 v1, 0x9

    aput v1, v0, v2

    sput-object v0, Lcom/google/android/location/activity/k;->a:[I

    .line 140
    sput-boolean v2, Lcom/google/android/location/activity/k;->F:Z

    .line 145
    sput-boolean v2, Lcom/google/android/location/activity/k;->G:Z

    return-void
.end method

.method public constructor <init>(Lcom/google/android/location/os/bi;)V
    .locals 2

    .prologue
    .line 243
    invoke-interface {p1}, Lcom/google/android/location/os/bi;->w()Lcom/google/android/location/activity/at;

    move-result-object v0

    invoke-interface {p1}, Lcom/google/android/location/os/bi;->x()Lcom/google/android/location/activity/bn;

    move-result-object v1

    invoke-direct {p0, p1, v0, v1}, Lcom/google/android/location/activity/k;-><init>(Lcom/google/android/location/os/bi;Lcom/google/android/location/activity/at;Lcom/google/android/location/activity/bn;)V

    .line 244
    return-void
.end method

.method private constructor <init>(Lcom/google/android/location/os/bi;Lcom/google/android/location/activity/at;Lcom/google/android/location/activity/bn;)V
    .locals 10

    .prologue
    const-wide v8, 0x7fffffffffffffffL

    const/4 v6, 0x0

    const/4 v4, 0x1

    const-wide/16 v2, -0x1

    const/4 v5, 0x0

    .line 213
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 78
    iput-object v6, p0, Lcom/google/android/location/activity/k;->g:Lcom/google/android/location/e/b;

    .line 79
    iput-object v6, p0, Lcom/google/android/location/activity/k;->h:Lcom/google/android/gms/location/ActivityRecognitionResult;

    .line 80
    iput-object v6, p0, Lcom/google/android/location/activity/k;->i:Lcom/google/android/gms/location/ActivityRecognitionResult;

    .line 81
    sget-object v0, Lcom/google/android/location/activity/bb;->a:Lcom/google/android/location/activity/bd;

    iput-object v0, p0, Lcom/google/android/location/activity/k;->j:Lcom/google/android/location/activity/bd;

    .line 82
    iput-wide v2, p0, Lcom/google/android/location/activity/k;->k:J

    .line 84
    new-instance v0, Lcom/google/android/location/activity/ah;

    invoke-direct {v0}, Lcom/google/android/location/activity/ah;-><init>()V

    iput-object v0, p0, Lcom/google/android/location/activity/k;->m:Lcom/google/android/location/activity/ah;

    .line 87
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/location/activity/k;->p:Ljava/util/List;

    .line 88
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/location/activity/k;->q:Ljava/util/Map;

    .line 97
    new-instance v0, Lcom/google/android/location/activity/a/af;

    invoke-direct {v0}, Lcom/google/android/location/activity/a/af;-><init>()V

    iput-object v0, p0, Lcom/google/android/location/activity/k;->t:Lcom/google/android/location/activity/a/o;

    .line 100
    new-instance v0, Lcom/google/android/location/activity/a/ad;

    invoke-direct {v0}, Lcom/google/android/location/activity/a/ad;-><init>()V

    iput-object v0, p0, Lcom/google/android/location/activity/k;->u:Lcom/google/android/location/activity/a/o;

    .line 111
    iput v5, p0, Lcom/google/android/location/activity/k;->w:I

    .line 115
    new-instance v0, Ljava/util/PriorityQueue;

    invoke-direct {v0}, Ljava/util/PriorityQueue;-><init>()V

    iput-object v0, p0, Lcom/google/android/location/activity/k;->x:Ljava/util/Queue;

    .line 116
    new-instance v0, Ljava/util/PriorityQueue;

    invoke-direct {v0}, Ljava/util/PriorityQueue;-><init>()V

    iput-object v0, p0, Lcom/google/android/location/activity/k;->y:Ljava/util/Queue;

    .line 118
    const v0, 0x7fffffff

    iput v0, p0, Lcom/google/android/location/activity/k;->z:I

    .line 119
    iput-wide v8, p0, Lcom/google/android/location/activity/k;->A:J

    .line 120
    iput-wide v8, p0, Lcom/google/android/location/activity/k;->B:J

    .line 148
    iput-boolean v5, p0, Lcom/google/android/location/activity/k;->H:Z

    .line 149
    iput-boolean v5, p0, Lcom/google/android/location/activity/k;->I:Z

    .line 152
    iput v4, p0, Lcom/google/android/location/activity/k;->J:I

    .line 153
    iput v5, p0, Lcom/google/android/location/activity/k;->K:I

    .line 154
    iput v5, p0, Lcom/google/android/location/activity/k;->L:I

    .line 155
    const-wide v0, 0x1f3fffffc18L

    iput-wide v0, p0, Lcom/google/android/location/activity/k;->M:J

    .line 156
    iput-object v6, p0, Lcom/google/android/location/activity/k;->N:Lcom/google/android/location/e/b;

    .line 158
    iput-wide v2, p0, Lcom/google/android/location/activity/k;->O:J

    .line 159
    iput-wide v2, p0, Lcom/google/android/location/activity/k;->P:J

    .line 160
    iput-wide v2, p0, Lcom/google/android/location/activity/k;->Q:J

    .line 173
    iput-wide v2, p0, Lcom/google/android/location/activity/k;->S:J

    .line 214
    sget-boolean v0, Lcom/google/android/location/i/a;->b:Z

    if-eqz v0, :cond_0

    const-string v0, "ActivityScheduler"

    const-string v1, "ActivityDetectionScheduler started in state off"

    invoke-static {v0, v1}, Lcom/google/android/location/o/a/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 215
    :cond_0
    iput-object p1, p0, Lcom/google/android/location/activity/k;->b:Lcom/google/android/location/os/bi;

    .line 216
    sget-object v0, Lcom/google/android/location/d/a;->g:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    .line 218
    sget-boolean v1, Lcom/google/android/location/i/a;->b:Z

    if-eqz v1, :cond_1

    const-string v1, "ActivityScheduler"

    const-string v2, "offBodyDetectionAngularThreshold = %f degrees"

    new-array v3, v4, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/location/o/a/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 220
    :cond_1
    new-instance v1, Lcom/google/android/location/activity/a/aa;

    invoke-interface {p1}, Lcom/google/android/location/os/bi;->y()Z

    move-result v2

    invoke-direct {v1, v2, v0}, Lcom/google/android/location/activity/a/aa;-><init>(ZF)V

    iput-object v1, p0, Lcom/google/android/location/activity/k;->r:Lcom/google/android/location/activity/a/o;

    .line 222
    new-instance v0, Lcom/google/android/location/activity/a/z;

    invoke-interface {p1}, Lcom/google/android/location/os/bi;->y()Z

    move-result v1

    invoke-direct {v0, v1}, Lcom/google/android/location/activity/a/z;-><init>(Z)V

    iput-object v0, p0, Lcom/google/android/location/activity/k;->s:Lcom/google/android/location/activity/a/o;

    .line 223
    new-instance v0, Lcom/google/android/location/activity/an;

    invoke-direct {v0, p0, p1}, Lcom/google/android/location/activity/an;-><init>(Lcom/google/android/location/activity/k;Lcom/google/android/location/os/bi;)V

    iput-object v0, p0, Lcom/google/android/location/activity/k;->e:Lcom/google/android/location/activity/av;

    .line 224
    iput-object p2, p0, Lcom/google/android/location/activity/k;->n:Lcom/google/android/location/activity/at;

    .line 225
    iput-object p3, p0, Lcom/google/android/location/activity/k;->o:Lcom/google/android/location/activity/bn;

    .line 226
    new-instance v0, Lcom/google/android/location/activity/ay;

    invoke-direct {v0}, Lcom/google/android/location/activity/ay;-><init>()V

    iput-object v0, p0, Lcom/google/android/location/activity/k;->v:Lcom/google/android/location/activity/ay;

    .line 227
    new-instance v0, Lcom/google/android/location/activity/g;

    iget-object v1, p0, Lcom/google/android/location/activity/k;->v:Lcom/google/android/location/activity/ay;

    invoke-direct {v0, p1, v1}, Lcom/google/android/location/activity/g;-><init>(Lcom/google/android/location/os/bi;Lcom/google/android/location/activity/ay;)V

    iput-object v0, p0, Lcom/google/android/location/activity/k;->c:Lcom/google/android/location/activity/g;

    .line 229
    new-instance v0, Lcom/google/android/location/activity/i;

    iget-object v1, p0, Lcom/google/android/location/activity/k;->v:Lcom/google/android/location/activity/ay;

    invoke-direct {v0, p1, v1}, Lcom/google/android/location/activity/i;-><init>(Lcom/google/android/location/os/bi;Lcom/google/android/location/activity/ay;)V

    iput-object v0, p0, Lcom/google/android/location/activity/k;->d:Lcom/google/android/location/activity/i;

    .line 231
    new-instance v0, Lcom/google/android/location/activity/m;

    invoke-direct {v0}, Lcom/google/android/location/activity/m;-><init>()V

    iput-object v0, p0, Lcom/google/android/location/activity/k;->l:Lcom/google/android/location/activity/m;

    .line 232
    new-instance v0, Lcom/google/android/location/activity/a;

    invoke-direct {v0, p1}, Lcom/google/android/location/activity/a;-><init>(Lcom/google/android/location/os/bi;)V

    iput-object v0, p0, Lcom/google/android/location/activity/k;->f:Lcom/google/android/location/activity/a;

    .line 233
    invoke-direct {p0}, Lcom/google/android/location/activity/k;->N()Lcom/google/android/location/e/au;

    move-result-object v0

    :try_start_0
    invoke-virtual {v0}, Lcom/google/android/location/e/au;->a()Lcom/google/p/a/b/b/a;

    move-result-object v1

    sget-object v0, Lcom/google/android/location/d/a;->u:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_5

    const/4 v0, 0x1

    invoke-virtual {v1, v0}, Lcom/google/p/a/b/b/a;->b(I)Z

    move-result v0

    sput-boolean v0, Lcom/google/android/location/activity/k;->F:Z

    sput-boolean v0, Lcom/google/android/location/activity/k;->G:Z

    :goto_0
    const/4 v0, 0x2

    invoke-virtual {v1, v0}, Lcom/google/p/a/b/b/a;->c(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/location/activity/k;->J:I

    sget-boolean v0, Lcom/google/android/location/i/a;->b:Z

    if-eqz v0, :cond_2

    const-string v0, "ActivityScheduler"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "State loaded: lowPowerMode="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-boolean v2, Lcom/google/android/location/activity/k;->F:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " sensorDelay="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/google/android/location/activity/k;->J:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/location/o/a/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_2
    sget-boolean v0, Lcom/google/android/location/activity/k;->F:Z

    if-eqz v0, :cond_6

    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Lcom/google/android/location/activity/k;->a(ZZ)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 234
    :cond_3
    :goto_1
    invoke-interface {p1}, Lcom/google/android/location/os/bi;->s()Z

    move-result v0

    if-nez v0, :cond_4

    .line 235
    sget-boolean v0, Lcom/google/android/location/i/a;->b:Z

    if-eqz v0, :cond_4

    const-string v0, "ActivityScheduler"

    const-string v1, "No accelerometer detected. Activity detection will be disabled."

    invoke-static {v0, v1}, Lcom/google/android/location/o/a/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 240
    :cond_4
    return-void

    .line 233
    :cond_5
    const/4 v0, 0x0

    :try_start_1
    sput-boolean v0, Lcom/google/android/location/activity/k;->F:Z

    const/4 v0, 0x0

    sput-boolean v0, Lcom/google/android/location/activity/k;->G:Z
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    sget-boolean v0, Lcom/google/android/location/i/a;->b:Z

    if-eqz v0, :cond_3

    const-string v0, "ActivityScheduler"

    const-string v1, "Unable to load data from disk."

    invoke-static {v0, v1}, Lcom/google/android/location/o/a/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    :cond_6
    const/4 v0, 0x0

    :try_start_2
    invoke-virtual {p0, v0}, Lcom/google/android/location/activity/k;->a(Z)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_1
.end method

.method private M()V
    .locals 4

    .prologue
    .line 837
    invoke-direct {p0}, Lcom/google/android/location/activity/k;->N()Lcom/google/android/location/e/au;

    move-result-object v0

    .line 838
    new-instance v1, Lcom/google/p/a/b/b/a;

    sget-object v2, Lcom/google/android/location/m/a;->bD:Lcom/google/p/a/b/b/c;

    invoke-direct {v1, v2}, Lcom/google/p/a/b/b/a;-><init>(Lcom/google/p/a/b/b/c;)V

    .line 839
    const/4 v2, 0x1

    sget-boolean v3, Lcom/google/android/location/activity/k;->F:Z

    invoke-virtual {v1, v2, v3}, Lcom/google/p/a/b/b/a;->a(IZ)Lcom/google/p/a/b/b/a;

    .line 840
    const/4 v2, 0x2

    iget v3, p0, Lcom/google/android/location/activity/k;->J:I

    invoke-virtual {v1, v2, v3}, Lcom/google/p/a/b/b/a;->f(II)Lcom/google/p/a/b/b/a;

    .line 842
    :try_start_0
    invoke-virtual {v0, v1}, Lcom/google/android/location/e/au;->b(Lcom/google/p/a/b/b/a;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 846
    :cond_0
    :goto_0
    return-void

    .line 844
    :catch_0
    move-exception v0

    sget-boolean v0, Lcom/google/android/location/i/a;->b:Z

    if-eqz v0, :cond_0

    const-string v0, "ActivityScheduler"

    const-string v1, "Unable to save data to disk."

    invoke-static {v0, v1}, Lcom/google/android/location/o/a/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private N()Lcom/google/android/location/e/au;
    .locals 9

    .prologue
    .line 872
    new-instance v0, Lcom/google/android/location/e/au;

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/location/activity/k;->b:Lcom/google/android/location/os/bi;

    invoke-interface {v2}, Lcom/google/android/location/os/bi;->d()Lcom/google/android/location/j/d;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/location/j/d;->a()Ljavax/crypto/SecretKey;

    move-result-object v2

    const/4 v3, 0x2

    iget-object v4, p0, Lcom/google/android/location/activity/k;->b:Lcom/google/android/location/os/bi;

    invoke-interface {v4}, Lcom/google/android/location/os/bi;->d()Lcom/google/android/location/j/d;

    move-result-object v4

    invoke-interface {v4}, Lcom/google/android/location/j/d;->c()[B

    move-result-object v4

    sget-object v5, Lcom/google/android/location/m/a;->bD:Lcom/google/p/a/b/b/c;

    iget-object v6, p0, Lcom/google/android/location/activity/k;->b:Lcom/google/android/location/os/bi;

    invoke-interface {v6}, Lcom/google/android/location/os/bi;->p()Ljava/io/File;

    move-result-object v6

    iget-object v7, p0, Lcom/google/android/location/activity/k;->b:Lcom/google/android/location/os/bi;

    invoke-interface {v7}, Lcom/google/android/location/os/bi;->e()Lcom/google/android/location/j/e;

    move-result-object v8

    move-object v7, p0

    invoke-direct/range {v0 .. v8}, Lcom/google/android/location/e/au;-><init>(ILjavax/crypto/SecretKey;I[BLcom/google/p/a/b/b/c;Ljava/io/File;Lcom/google/android/location/e/aw;Lcom/google/android/location/j/e;)V

    .line 880
    return-object v0
.end method

.method private a(ZZ)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 492
    sget-boolean v0, Lcom/google/android/location/i/a;->b:Z

    if-eqz v0, :cond_0

    const-string v0, "ActivityScheduler"

    const-string v1, "Switching to low power mode"

    invoke-static {v0, v1}, Lcom/google/android/location/o/a/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 493
    :cond_0
    iget-object v0, p0, Lcom/google/android/location/activity/k;->b:Lcom/google/android/location/os/bi;

    invoke-interface {v0}, Lcom/google/android/location/os/bi;->v()Lcom/google/android/location/os/j;

    move-result-object v0

    sget-object v1, Lcom/google/android/location/os/au;->ab:Lcom/google/android/location/os/au;

    invoke-virtual {v0, v1}, Lcom/google/android/location/os/j;->a(Lcom/google/android/location/os/au;)V

    .line 494
    sput-boolean v2, Lcom/google/android/location/activity/k;->F:Z

    .line 497
    iget-object v0, p0, Lcom/google/android/location/activity/k;->b:Lcom/google/android/location/os/bi;

    const-string v1, "com.google.android.location.activity.LOW_POWER_MODE_ENABLED"

    invoke-interface {v0, v1}, Lcom/google/android/location/os/bi;->b(Ljava/lang/String;)V

    .line 498
    if-eqz p2, :cond_3

    .line 499
    iget-object v0, p0, Lcom/google/android/location/activity/k;->f:Lcom/google/android/location/activity/a;

    invoke-virtual {v0}, Lcom/google/android/location/activity/a;->a()V

    .line 500
    iget-object v0, p0, Lcom/google/android/location/activity/k;->y:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/location/activity/k;->o:Lcom/google/android/location/activity/bn;

    invoke-interface {v0}, Lcom/google/android/location/activity/bn;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 502
    iget-object v0, p0, Lcom/google/android/location/activity/k;->o:Lcom/google/android/location/activity/bn;

    invoke-interface {v0}, Lcom/google/android/location/activity/bn;->a()Z

    .line 504
    :cond_1
    sput-boolean v2, Lcom/google/android/location/activity/k;->G:Z

    .line 509
    :goto_0
    if-eqz p1, :cond_2

    .line 510
    invoke-direct {p0}, Lcom/google/android/location/activity/k;->M()V

    .line 512
    :cond_2
    return-void

    .line 506
    :cond_3
    sget-boolean v0, Lcom/google/android/location/i/a;->b:Z

    if-eqz v0, :cond_4

    const-string v0, "ActivityScheduler"

    const-string v1, "Activity recognition continues to run at high power mode"

    invoke-static {v0, v1}, Lcom/google/android/location/o/a/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 507
    :cond_4
    const/4 v0, 0x0

    sput-boolean v0, Lcom/google/android/location/activity/k;->G:Z

    goto :goto_0
.end method

.method public static a()Z
    .locals 1

    .prologue
    .line 183
    sget-boolean v0, Lcom/google/android/location/activity/k;->F:Z

    return v0
.end method

.method private b(ZZ)V
    .locals 11

    .prologue
    .line 790
    iget-wide v2, p0, Lcom/google/android/location/activity/k;->A:J

    .line 791
    iget-wide v4, p0, Lcom/google/android/location/activity/k;->B:J

    .line 792
    iget-object v0, p0, Lcom/google/android/location/activity/k;->x:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->size()I

    move-result v0

    if-nez v0, :cond_4

    iget v0, p0, Lcom/google/android/location/activity/k;->w:I

    if-nez v0, :cond_4

    .line 793
    const-wide v0, 0x7fffffffffffffffL

    iput-wide v0, p0, Lcom/google/android/location/activity/k;->A:J

    .line 801
    :goto_0
    const-wide/16 v0, 0x0

    iget-wide v6, p0, Lcom/google/android/location/activity/k;->A:J

    invoke-static {v0, v1, v6, v7}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/location/activity/k;->A:J

    .line 803
    iget-object v0, p0, Lcom/google/android/location/activity/k;->y:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->size()I

    move-result v0

    if-lez v0, :cond_6

    .line 804
    iget-object v0, p0, Lcom/google/android/location/activity/k;->y:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->peek()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    int-to-long v0, v0

    const-wide/16 v6, 0x3e8

    mul-long/2addr v0, v6

    iput-wide v0, p0, Lcom/google/android/location/activity/k;->B:J

    .line 809
    :goto_1
    sget-boolean v0, Lcom/google/android/location/i/a;->b:Z

    if-eqz v0, :cond_0

    const-string v0, "ActivityScheduler"

    const-string v1, "updateEnabledState: minPeriod=%d, minTiltPeriod=%d, newClientAdded=%s, forceDetectionNow=%s, internalClientPeriods=%d, lowPowerPeriods=%d, externalClientCount=%d"

    const/4 v6, 0x7

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    iget-wide v8, p0, Lcom/google/android/location/activity/k;->A:J

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x1

    iget-wide v8, p0, Lcom/google/android/location/activity/k;->B:J

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x2

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x3

    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x4

    iget-object v8, p0, Lcom/google/android/location/activity/k;->x:Ljava/util/Queue;

    invoke-interface {v8}, Ljava/util/Queue;->size()I

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x5

    iget-object v8, p0, Lcom/google/android/location/activity/k;->y:Ljava/util/Queue;

    invoke-interface {v8}, Ljava/util/Queue;->size()I

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x6

    iget v8, p0, Lcom/google/android/location/activity/k;->w:I

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-static {v1, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/location/o/a/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 816
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/location/activity/k;->K()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 817
    iget-object v0, p0, Lcom/google/android/location/activity/k;->e:Lcom/google/android/location/activity/av;

    invoke-virtual {v0}, Lcom/google/android/location/activity/av;->a()V

    .line 823
    :cond_1
    :goto_2
    iget-wide v0, p0, Lcom/google/android/location/activity/k;->A:J

    cmp-long v0, v2, v0

    if-nez v0, :cond_2

    iget-wide v0, p0, Lcom/google/android/location/activity/k;->B:J

    cmp-long v0, v4, v0

    if-nez v0, :cond_2

    if-eqz p2, :cond_3

    .line 825
    :cond_2
    iget-object v0, p0, Lcom/google/android/location/activity/k;->b:Lcom/google/android/location/os/bi;

    invoke-interface {v0}, Lcom/google/android/location/os/bi;->v()Lcom/google/android/location/os/j;

    move-result-object v2

    iget-wide v6, p0, Lcom/google/android/location/activity/k;->A:J

    iget-wide v8, p0, Lcom/google/android/location/activity/k;->B:J

    new-instance v1, Lcom/google/android/location/os/ak;

    sget-object v3, Lcom/google/android/location/os/au;->am:Lcom/google/android/location/os/au;

    iget-object v0, v2, Lcom/google/android/location/os/j;->a:Lcom/google/android/location/os/at;

    invoke-interface {v0}, Lcom/google/android/location/os/at;->a()J

    move-result-wide v4

    move v10, p2

    invoke-direct/range {v1 .. v10}, Lcom/google/android/location/os/ak;-><init>(Lcom/google/android/location/os/j;Lcom/google/android/location/os/au;JJJZ)V

    long-to-int v3, v6

    long-to-int v4, v8

    if-eqz p2, :cond_9

    const/4 v0, 0x1

    :goto_3
    invoke-virtual {v2, v1, v3, v4, v0}, Lcom/google/android/location/os/j;->a(Lcom/google/android/location/os/as;III)V

    .line 828
    :cond_3
    return-void

    .line 794
    :cond_4
    iget-object v0, p0, Lcom/google/android/location/activity/k;->x:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->size()I

    move-result v0

    if-nez v0, :cond_5

    .line 795
    iget v0, p0, Lcom/google/android/location/activity/k;->z:I

    int-to-long v0, v0

    const-wide/16 v6, 0x3e8

    mul-long/2addr v0, v6

    iput-wide v0, p0, Lcom/google/android/location/activity/k;->A:J

    goto/16 :goto_0

    .line 797
    :cond_5
    iget v1, p0, Lcom/google/android/location/activity/k;->z:I

    iget-object v0, p0, Lcom/google/android/location/activity/k;->x:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->peek()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v1, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    int-to-long v0, v0

    const-wide/16 v6, 0x3e8

    mul-long/2addr v0, v6

    iput-wide v0, p0, Lcom/google/android/location/activity/k;->A:J

    goto/16 :goto_0

    .line 807
    :cond_6
    const-wide v0, 0x7fffffffffffffffL

    iput-wide v0, p0, Lcom/google/android/location/activity/k;->B:J

    goto/16 :goto_1

    .line 818
    :cond_7
    if-eqz p1, :cond_1

    .line 819
    sget-boolean v0, Lcom/google/android/location/i/a;->b:Z

    if-eqz v0, :cond_8

    const-string v0, "ActivityScheduler"

    const-string v1, "state.newClientAdded(%s)"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-static {v1, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/location/o/a/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 820
    :cond_8
    iget-object v0, p0, Lcom/google/android/location/activity/k;->e:Lcom/google/android/location/activity/av;

    invoke-virtual {v0, p2}, Lcom/google/android/location/activity/av;->a(Z)V

    goto/16 :goto_2

    .line 825
    :cond_9
    const/4 v0, 0x0

    goto :goto_3
.end method

.method public static b()Z
    .locals 1

    .prologue
    .line 187
    sget-boolean v0, Lcom/google/android/location/activity/k;->G:Z

    return v0
.end method


# virtual methods
.method final A()Ljava/util/Map;
    .locals 1

    .prologue
    .line 392
    iget-object v0, p0, Lcom/google/android/location/activity/k;->R:Ljava/util/Map;

    return-object v0
.end method

.method public final B()Z
    .locals 1

    .prologue
    .line 462
    iget-boolean v0, p0, Lcom/google/android/location/activity/k;->I:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/location/activity/k;->H:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final C()Z
    .locals 1

    .prologue
    .line 466
    iget-boolean v0, p0, Lcom/google/android/location/activity/k;->I:Z

    return v0
.end method

.method protected final D()J
    .locals 2

    .prologue
    .line 470
    iget-wide v0, p0, Lcom/google/android/location/activity/k;->S:J

    return-wide v0
.end method

.method final E()V
    .locals 2

    .prologue
    .line 607
    iget-object v0, p0, Lcom/google/android/location/activity/k;->g:Lcom/google/android/location/e/b;

    if-eqz v0, :cond_1

    .line 608
    sget-boolean v0, Lcom/google/android/location/i/a;->b:Z

    if-eqz v0, :cond_0

    const-string v0, "ActivityScheduler"

    const-string v1, "Alarm canceled"

    invoke-static {v0, v1}, Lcom/google/android/location/o/a/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 609
    :cond_0
    iget-object v0, p0, Lcom/google/android/location/activity/k;->b:Lcom/google/android/location/os/bi;

    invoke-interface {v0}, Lcom/google/android/location/os/bi;->f()Lcom/google/android/location/j/j;

    move-result-object v0

    sget-object v1, Lcom/google/android/location/j/k;->i:Lcom/google/android/location/j/k;

    invoke-interface {v0, v1}, Lcom/google/android/location/j/j;->b(Lcom/google/android/location/j/k;)V

    .line 610
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/location/activity/k;->g:Lcom/google/android/location/e/b;

    .line 612
    :cond_1
    return-void
.end method

.method public final F()V
    .locals 1

    .prologue
    .line 668
    iget-object v0, p0, Lcom/google/android/location/activity/k;->e:Lcom/google/android/location/activity/av;

    invoke-virtual {v0}, Lcom/google/android/location/activity/av;->at_()V

    .line 669
    return-void
.end method

.method public final G()V
    .locals 1

    .prologue
    .line 681
    iget-object v0, p0, Lcom/google/android/location/activity/k;->e:Lcom/google/android/location/activity/av;

    invoke-virtual {v0}, Lcom/google/android/location/activity/av;->ar_()V

    .line 682
    return-void
.end method

.method public final H()V
    .locals 1

    .prologue
    .line 685
    iget-object v0, p0, Lcom/google/android/location/activity/k;->e:Lcom/google/android/location/activity/av;

    invoke-virtual {v0}, Lcom/google/android/location/activity/av;->as_()V

    .line 686
    return-void
.end method

.method public final I()V
    .locals 3

    .prologue
    .line 762
    sget-boolean v0, Lcom/google/android/location/i/a;->b:Z

    if-eqz v0, :cond_0

    const-string v0, "ActivityScheduler"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "addLowPowerTiltDetectionInternalClient 30"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/location/o/a/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 763
    :cond_0
    sget-boolean v0, Lcom/google/android/location/activity/k;->G:Z

    if-nez v0, :cond_1

    .line 764
    iget-object v0, p0, Lcom/google/android/location/activity/k;->y:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 765
    iget-object v0, p0, Lcom/google/android/location/activity/k;->o:Lcom/google/android/location/activity/bn;

    invoke-interface {v0}, Lcom/google/android/location/activity/bn;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 766
    iget-object v0, p0, Lcom/google/android/location/activity/k;->o:Lcom/google/android/location/activity/bn;

    invoke-interface {v0, p0}, Lcom/google/android/location/activity/bn;->a(Lcom/google/android/location/activity/bo;)Z

    .line 770
    :cond_1
    iget-object v0, p0, Lcom/google/android/location/activity/k;->y:Ljava/util/Queue;

    const/16 v1, 0x1e

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    .line 771
    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/google/android/location/activity/k;->b(ZZ)V

    .line 772
    return-void
.end method

.method public final J()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 780
    iget-object v0, p0, Lcom/google/android/location/activity/k;->y:Ljava/util/Queue;

    const/16 v1, 0x1e

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Queue;->remove(Ljava/lang/Object;)Z

    .line 781
    iget-object v0, p0, Lcom/google/android/location/activity/k;->y:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 782
    iget-object v0, p0, Lcom/google/android/location/activity/k;->o:Lcom/google/android/location/activity/bn;

    invoke-interface {v0}, Lcom/google/android/location/activity/bn;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 783
    iget-object v0, p0, Lcom/google/android/location/activity/k;->o:Lcom/google/android/location/activity/bn;

    invoke-interface {v0}, Lcom/google/android/location/activity/bn;->a()Z

    .line 786
    :cond_0
    invoke-direct {p0, v2, v2}, Lcom/google/android/location/activity/k;->b(ZZ)V

    .line 787
    return-void
.end method

.method final K()Z
    .locals 1

    .prologue
    .line 831
    iget v0, p0, Lcom/google/android/location/activity/k;->w:I

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/location/activity/k;->x:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->size()I

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/location/activity/k;->y:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->size()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final L()V
    .locals 6

    .prologue
    .line 927
    new-instance v0, Lcom/google/android/gms/location/ActivityRecognitionResult;

    new-instance v1, Lcom/google/android/gms/location/DetectedActivity;

    const/4 v2, 0x5

    const/16 v3, 0x64

    invoke-direct {v1, v2, v3}, Lcom/google/android/gms/location/DetectedActivity;-><init>(II)V

    iget-object v2, p0, Lcom/google/android/location/activity/k;->b:Lcom/google/android/location/os/bi;

    invoke-interface {v2}, Lcom/google/android/location/os/bi;->c()Lcom/google/android/location/j/b;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/location/j/b;->b()J

    move-result-wide v2

    iget-object v4, p0, Lcom/google/android/location/activity/k;->b:Lcom/google/android/location/os/bi;

    invoke-interface {v4}, Lcom/google/android/location/os/bi;->c()Lcom/google/android/location/j/b;

    move-result-object v4

    invoke-interface {v4}, Lcom/google/android/location/j/b;->c()J

    move-result-wide v4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/location/ActivityRecognitionResult;-><init>(Lcom/google/android/gms/location/DetectedActivity;JJ)V

    .line 930
    invoke-virtual {p0, v0}, Lcom/google/android/location/activity/k;->b(Lcom/google/android/gms/location/ActivityRecognitionResult;)V

    .line 931
    invoke-virtual {p0, v0}, Lcom/google/android/location/activity/k;->a(Lcom/google/android/gms/location/ActivityRecognitionResult;)V

    .line 932
    iget-object v0, p0, Lcom/google/android/location/activity/k;->e:Lcom/google/android/location/activity/av;

    invoke-virtual {v0}, Lcom/google/android/location/activity/av;->j()V

    .line 933
    return-void
.end method

.method public final a(D)V
    .locals 5

    .prologue
    const/4 v3, 0x0

    .line 430
    sget-boolean v0, Lcom/google/android/location/i/a;->b:Z

    if-eqz v0, :cond_0

    const-string v0, "ActivityScheduler"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Sample rate is: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " at sensorDelay: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/google/android/location/activity/k;->J:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/location/o/a/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 431
    :cond_0
    const-wide/high16 v0, 0x403e000000000000L    # 30.0

    cmpg-double v0, p1, v0

    if-gez v0, :cond_6

    .line 432
    iget v0, p0, Lcom/google/android/location/activity/k;->K:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/location/activity/k;->K:I

    .line 433
    iget v0, p0, Lcom/google/android/location/activity/k;->K:I

    const/4 v1, 0x2

    if-le v0, v1, :cond_5

    iget v0, p0, Lcom/google/android/location/activity/k;->J:I

    if-lez v0, :cond_5

    .line 434
    sget-boolean v0, Lcom/google/android/location/i/a;->d:Z

    if-eqz v0, :cond_1

    const-string v0, "ActivityScheduler"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Sample rate too slow. Changing sensor delay: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/google/android/location/activity/k;->J:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/location/o/a/a;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 436
    :cond_1
    iget v0, p0, Lcom/google/android/location/activity/k;->J:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/google/android/location/activity/k;->J:I

    .line 437
    invoke-direct {p0}, Lcom/google/android/location/activity/k;->M()V

    .line 445
    :cond_2
    :goto_0
    const-wide v0, 0x4062c00000000000L    # 150.0

    cmpl-double v0, p1, v0

    if-ltz v0, :cond_8

    .line 446
    iget v0, p0, Lcom/google/android/location/activity/k;->L:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/location/activity/k;->L:I

    .line 447
    iget v0, p0, Lcom/google/android/location/activity/k;->L:I

    const/16 v1, 0x32

    if-le v0, v1, :cond_7

    iget v0, p0, Lcom/google/android/location/activity/k;->J:I

    if-gtz v0, :cond_7

    .line 448
    sget-boolean v0, Lcom/google/android/location/i/a;->d:Z

    if-eqz v0, :cond_3

    const-string v0, "ActivityScheduler"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Sample rate too fast. Changing sensor delay: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/google/android/location/activity/k;->J:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/location/o/a/a;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 450
    :cond_3
    iget v0, p0, Lcom/google/android/location/activity/k;->J:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/location/activity/k;->J:I

    .line 451
    invoke-direct {p0}, Lcom/google/android/location/activity/k;->M()V

    .line 459
    :cond_4
    :goto_1
    return-void

    .line 439
    :cond_5
    sget-boolean v0, Lcom/google/android/location/i/a;->d:Z

    if-eqz v0, :cond_2

    const-string v0, "ActivityScheduler"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Sample rate too slow. consecutiveInsufficientSamplingRate="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/google/android/location/activity/k;->K:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/location/o/a/a;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 443
    :cond_6
    iput v3, p0, Lcom/google/android/location/activity/k;->K:I

    goto :goto_0

    .line 453
    :cond_7
    sget-boolean v0, Lcom/google/android/location/i/a;->d:Z

    if-eqz v0, :cond_4

    const-string v0, "ActivityScheduler"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Sample rate too fast. consecutiveHighSamplingRate="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/google/android/location/activity/k;->L:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/location/o/a/a;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 457
    :cond_8
    iput v3, p0, Lcom/google/android/location/activity/k;->L:I

    goto :goto_1
.end method

.method public final a(I)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 775
    iget-object v0, p0, Lcom/google/android/location/activity/k;->x:Ljava/util/Queue;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Queue;->remove(Ljava/lang/Object;)Z

    .line 776
    invoke-direct {p0, v2, v2}, Lcom/google/android/location/activity/k;->b(ZZ)V

    .line 777
    return-void
.end method

.method public final a(IIZLcom/google/android/location/o/n;Ljava/util/Set;)V
    .locals 7

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 710
    iget v0, p0, Lcom/google/android/location/activity/k;->w:I

    if-le p1, v0, :cond_5

    move v0, v1

    .line 711
    :goto_0
    iget v3, p0, Lcom/google/android/location/activity/k;->w:I

    .line 712
    iput p1, p0, Lcom/google/android/location/activity/k;->w:I

    .line 713
    sget-boolean v4, Lcom/google/android/location/i/a;->b:Z

    if-eqz v4, :cond_0

    const-string v4, "ActivityScheduler"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "setActivityDetectionExternalClientCount: count="

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", minPeriodSec="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", forceDetectionNow="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", externalClientCountPrevious="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", externalClientCount="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, p0, Lcom/google/android/location/activity/k;->w:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/android/location/o/a/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 717
    :cond_0
    iput p2, p0, Lcom/google/android/location/activity/k;->z:I

    .line 718
    iput-object p4, p0, Lcom/google/android/location/activity/k;->C:Lcom/google/android/location/o/n;

    .line 727
    iput-boolean v2, p0, Lcom/google/android/location/activity/k;->D:Z

    .line 728
    iput-object p5, p0, Lcom/google/android/location/activity/k;->E:Ljava/util/Set;

    .line 729
    iget v4, p0, Lcom/google/android/location/activity/k;->w:I

    if-eqz v4, :cond_1

    iget v4, p0, Lcom/google/android/location/activity/k;->w:I

    if-ge v4, v3, :cond_1

    .line 730
    iget-object v3, p0, Lcom/google/android/location/activity/k;->e:Lcom/google/android/location/activity/av;

    invoke-virtual {v3}, Lcom/google/android/location/activity/av;->i()V

    .line 732
    :cond_1
    iget-object v3, p0, Lcom/google/android/location/activity/k;->b:Lcom/google/android/location/os/bi;

    invoke-interface {v3}, Lcom/google/android/location/os/bi;->y()Z

    move-result v3

    if-nez v3, :cond_4

    if-eqz p5, :cond_4

    .line 733
    sget-object v4, Lcom/google/android/location/activity/k;->a:[I

    array-length v5, v4

    move v3, v2

    :goto_1
    if-ge v3, v5, :cond_2

    aget v6, v4, v3

    .line 734
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-interface {p5, v6}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_6

    .line 735
    iput-boolean v1, p0, Lcom/google/android/location/activity/k;->D:Z

    .line 739
    :cond_2
    iget-boolean v3, p0, Lcom/google/android/location/activity/k;->D:Z

    if-eqz v3, :cond_7

    .line 740
    sget-boolean v1, Lcom/google/android/location/i/a;->b:Z

    if-eqz v1, :cond_3

    const-string v1, "ActivityScheduler"

    const-string v3, "Bypass low power mode for activity detection scheduling."

    invoke-static {v1, v3}, Lcom/google/android/location/o/a/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 741
    :cond_3
    sget-boolean v1, Lcom/google/android/location/activity/k;->G:Z

    if-eqz v1, :cond_4

    .line 742
    sput-boolean v2, Lcom/google/android/location/activity/k;->G:Z

    .line 743
    iget-object v1, p0, Lcom/google/android/location/activity/k;->f:Lcom/google/android/location/activity/a;

    iget v2, p0, Lcom/google/android/location/activity/k;->J:I

    invoke-virtual {v1, v2}, Lcom/google/android/location/activity/a;->a(I)Z

    .line 752
    :cond_4
    :goto_2
    invoke-direct {p0, v0, p3}, Lcom/google/android/location/activity/k;->b(ZZ)V

    .line 753
    return-void

    :cond_5
    move v0, v2

    .line 710
    goto/16 :goto_0

    .line 733
    :cond_6
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 746
    :cond_7
    sget-boolean v2, Lcom/google/android/location/activity/k;->F:Z

    if-eqz v2, :cond_4

    sget-boolean v2, Lcom/google/android/location/activity/k;->G:Z

    if-nez v2, :cond_4

    .line 747
    sput-boolean v1, Lcom/google/android/location/activity/k;->G:Z

    .line 748
    iget-object v1, p0, Lcom/google/android/location/activity/k;->f:Lcom/google/android/location/activity/a;

    invoke-virtual {v1}, Lcom/google/android/location/activity/a;->a()V

    goto :goto_2
.end method

.method public final a(IZ)V
    .locals 2

    .prologue
    .line 757
    iget-object v0, p0, Lcom/google/android/location/activity/k;->x:Ljava/util/Queue;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    .line 758
    const/4 v0, 0x1

    invoke-direct {p0, v0, p2}, Lcom/google/android/location/activity/k;->b(ZZ)V

    .line 759
    return-void
.end method

.method final a(J)V
    .locals 1

    .prologue
    .line 365
    iput-wide p1, p0, Lcom/google/android/location/activity/k;->O:J

    .line 366
    return-void
.end method

.method final a(Lcom/google/android/gms/location/ActivityRecognitionResult;)V
    .locals 9

    .prologue
    const/4 v8, 0x3

    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 548
    iget-object v0, p0, Lcom/google/android/location/activity/k;->b:Lcom/google/android/location/os/bi;

    invoke-interface {v0}, Lcom/google/android/location/os/bi;->v()Lcom/google/android/location/os/j;

    move-result-object v0

    invoke-virtual {v0, p1, v1}, Lcom/google/android/location/os/j;->a(Lcom/google/android/gms/location/ActivityRecognitionResult;Z)V

    .line 549
    iput-object p1, p0, Lcom/google/android/location/activity/k;->h:Lcom/google/android/gms/location/ActivityRecognitionResult;

    .line 550
    invoke-virtual {p1}, Lcom/google/android/gms/location/ActivityRecognitionResult;->a()Lcom/google/android/gms/location/DetectedActivity;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/location/DetectedActivity;->a()I

    move-result v0

    const/4 v3, 0x5

    if-eq v0, v3, :cond_0

    .line 551
    iput-object p1, p0, Lcom/google/android/location/activity/k;->i:Lcom/google/android/gms/location/ActivityRecognitionResult;

    .line 559
    :cond_0
    sget-object v0, Lcom/google/android/location/d/a;->u:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_9

    iget-object v3, p0, Lcom/google/android/location/activity/k;->m:Lcom/google/android/location/activity/ah;

    invoke-virtual {p1}, Lcom/google/android/gms/location/ActivityRecognitionResult;->a()Lcom/google/android/gms/location/DetectedActivity;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/location/DetectedActivity;->a()I

    move-result v4

    if-ne v4, v8, :cond_4

    invoke-virtual {v0}, Lcom/google/android/gms/location/DetectedActivity;->b()I

    move-result v0

    const/16 v4, 0x3c

    if-lt v0, v4, :cond_4

    move v0, v1

    :goto_0
    if-eqz v0, :cond_5

    iget-object v0, v3, Lcom/google/android/location/activity/ah;->a:Lcom/google/android/gms/location/ActivityRecognitionResult;

    if-nez v0, :cond_1

    iput-object p1, v3, Lcom/google/android/location/activity/ah;->a:Lcom/google/android/gms/location/ActivityRecognitionResult;

    :cond_1
    iget v0, v3, Lcom/google/android/location/activity/ah;->b:I

    add-int/lit8 v0, v0, 0x1

    iput v0, v3, Lcom/google/android/location/activity/ah;->b:I

    sget-boolean v0, Lcom/google/android/location/i/a;->b:Z

    if-eqz v0, :cond_2

    const-string v0, "LowPowerMonitor"

    const-string v4, "shouldSwitchToLowPower: high confidence still."

    invoke-static {v0, v4}, Lcom/google/android/location/o/a/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_2
    iget-object v0, v3, Lcom/google/android/location/activity/ah;->a:Lcom/google/android/gms/location/ActivityRecognitionResult;

    invoke-virtual {p1}, Lcom/google/android/gms/location/ActivityRecognitionResult;->e()J

    move-result-wide v4

    invoke-virtual {v0}, Lcom/google/android/gms/location/ActivityRecognitionResult;->e()J

    move-result-wide v6

    sub-long/2addr v4, v6

    const-wide/32 v6, 0x75300

    cmp-long v0, v4, v6

    if-ltz v0, :cond_6

    iget v0, v3, Lcom/google/android/location/activity/ah;->b:I

    if-lt v0, v8, :cond_6

    invoke-virtual {v3}, Lcom/google/android/location/activity/ah;->a()V

    move v0, v1

    :goto_1
    if-eqz v0, :cond_9

    move v0, v1

    .line 562
    :goto_2
    if-eqz v0, :cond_3

    .line 563
    iget-boolean v0, p0, Lcom/google/android/location/activity/k;->D:Z

    if-nez v0, :cond_a

    move v0, v1

    :goto_3
    invoke-direct {p0, v1, v0}, Lcom/google/android/location/activity/k;->a(ZZ)V

    .line 565
    :cond_3
    iget-object v0, p0, Lcom/google/android/location/activity/k;->b:Lcom/google/android/location/os/bi;

    invoke-interface {v0, p1}, Lcom/google/android/location/os/bi;->a(Lcom/google/android/gms/location/ActivityRecognitionResult;)V

    .line 566
    iget-object v0, p0, Lcom/google/android/location/activity/k;->l:Lcom/google/android/location/activity/m;

    invoke-virtual {v0, p1}, Lcom/google/android/location/activity/m;->a(Lcom/google/android/gms/location/ActivityRecognitionResult;)V

    .line 567
    return-void

    :cond_4
    move v0, v2

    .line 559
    goto :goto_0

    :cond_5
    invoke-virtual {p1}, Lcom/google/android/gms/location/ActivityRecognitionResult;->a()Lcom/google/android/gms/location/DetectedActivity;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/location/DetectedActivity;->a()I

    move-result v0

    if-ne v0, v8, :cond_8

    move v0, v1

    :goto_4
    if-nez v0, :cond_6

    invoke-virtual {v3}, Lcom/google/android/location/activity/ah;->a()V

    :cond_6
    sget-boolean v0, Lcom/google/android/location/i/a;->b:Z

    if-eqz v0, :cond_7

    const-string v0, "LowPowerMonitor"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "shouldSwitchToLowPower: consecutiveStills="

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v3, v3, Lcom/google/android/location/activity/ah;->b:I

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Lcom/google/android/location/o/a/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_7
    move v0, v2

    goto :goto_1

    :cond_8
    move v0, v2

    goto :goto_4

    :cond_9
    move v0, v2

    goto :goto_2

    :cond_a
    move v0, v2

    .line 563
    goto :goto_3
.end method

.method final a(Lcom/google/android/location/activity/av;)V
    .locals 3

    .prologue
    .line 275
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/location/activity/k;->e:Lcom/google/android/location/activity/av;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 276
    invoke-virtual {p1}, Lcom/google/android/location/activity/av;->p()Ljava/lang/String;

    move-result-object v1

    .line 277
    iget-object v0, p0, Lcom/google/android/location/activity/k;->q:Ljava/util/Map;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 278
    if-nez v0, :cond_0

    .line 279
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 281
    :cond_0
    iget-object v2, p0, Lcom/google/android/location/activity/k;->q:Ljava/util/Map;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v2, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 283
    :cond_1
    iput-object p1, p0, Lcom/google/android/location/activity/k;->e:Lcom/google/android/location/activity/av;

    .line 284
    return-void
.end method

.method public final a(Lcom/google/android/location/activity/bd;)V
    .locals 2

    .prologue
    .line 699
    if-nez p1, :cond_0

    .line 705
    :goto_0
    return-void

    .line 702
    :cond_0
    iget-object v0, p0, Lcom/google/android/location/activity/k;->e:Lcom/google/android/location/activity/av;

    iget-object v1, p0, Lcom/google/android/location/activity/k;->j:Lcom/google/android/location/activity/bd;

    invoke-virtual {v0, v1, p1}, Lcom/google/android/location/activity/av;->a(Lcom/google/android/location/activity/bd;Lcom/google/android/location/activity/bd;)V

    .line 703
    iput-object p1, p0, Lcom/google/android/location/activity/k;->j:Lcom/google/android/location/activity/bd;

    .line 704
    iget-object v0, p0, Lcom/google/android/location/activity/k;->b:Lcom/google/android/location/os/bi;

    invoke-interface {v0}, Lcom/google/android/location/os/bi;->c()Lcom/google/android/location/j/b;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/location/j/b;->c()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/location/activity/k;->k:J

    goto :goto_0
.end method

.method public final a(Lcom/google/android/location/activity/l;)V
    .locals 2

    .prologue
    .line 905
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/google/android/location/activity/k;->p:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 907
    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 908
    iput-object v0, p0, Lcom/google/android/location/activity/k;->p:Ljava/util/List;

    .line 909
    return-void
.end method

.method final a(Lcom/google/android/location/e/b;)V
    .locals 8

    .prologue
    .line 594
    iget-object v0, p0, Lcom/google/android/location/activity/k;->g:Lcom/google/android/location/e/b;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/location/activity/k;->g:Lcom/google/android/location/e/b;

    invoke-virtual {v0, p1}, Lcom/google/android/location/e/b;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 595
    :cond_0
    iput-object p1, p0, Lcom/google/android/location/activity/k;->g:Lcom/google/android/location/e/b;

    .line 596
    sget-boolean v0, Lcom/google/android/location/i/a;->b:Z

    if-eqz v0, :cond_1

    const-string v0, "ActivityScheduler"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Alarm set to: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v2, p1, Lcom/google/android/location/e/b;->a:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    new-instance v2, Ljava/util/Date;

    iget-object v3, p0, Lcom/google/android/location/activity/k;->b:Lcom/google/android/location/os/bi;

    invoke-interface {v3}, Lcom/google/android/location/os/bi;->c()Lcom/google/android/location/j/b;

    move-result-object v3

    invoke-interface {v3}, Lcom/google/android/location/j/b;->d()J

    move-result-wide v4

    iget-wide v6, p1, Lcom/google/android/location/e/b;->a:J

    add-long/2addr v4, v6

    invoke-direct {v2, v4, v5}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " windowLength="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p1, Lcom/google/android/location/e/b;->b:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/location/o/a/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 599
    :cond_1
    iget-object v0, p0, Lcom/google/android/location/activity/k;->b:Lcom/google/android/location/os/bi;

    invoke-interface {v0}, Lcom/google/android/location/os/bi;->f()Lcom/google/android/location/j/j;

    move-result-object v0

    sget-object v1, Lcom/google/android/location/j/k;->i:Lcom/google/android/location/j/k;

    iget-wide v2, p1, Lcom/google/android/location/e/b;->a:J

    iget-wide v4, p1, Lcom/google/android/location/e/b;->b:J

    iget-object v6, p0, Lcom/google/android/location/activity/k;->C:Lcom/google/android/location/o/n;

    invoke-interface/range {v0 .. v6}, Lcom/google/android/location/j/j;->a(Lcom/google/android/location/j/k;JJLcom/google/android/location/o/n;)V

    .line 603
    :cond_2
    return-void
.end method

.method public final a(Lcom/google/android/location/j/k;)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 621
    sget-boolean v0, Lcom/google/android/location/i/a;->b:Z

    if-eqz v0, :cond_0

    const-string v0, "ActivityScheduler"

    const-string v1, "alarmRing, client=%s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p1, Lcom/google/android/location/j/k;->m:Ljava/lang/String;

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/location/o/a/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 622
    :cond_0
    iget-object v0, p0, Lcom/google/android/location/activity/k;->g:Lcom/google/android/location/e/b;

    if-nez v0, :cond_2

    .line 648
    :cond_1
    :goto_0
    return-void

    .line 626
    :cond_2
    iget-object v0, p0, Lcom/google/android/location/activity/k;->b:Lcom/google/android/location/os/bi;

    invoke-interface {v0}, Lcom/google/android/location/os/bi;->c()Lcom/google/android/location/j/b;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/location/j/b;->c()J

    move-result-wide v0

    .line 627
    iget-object v2, p0, Lcom/google/android/location/activity/k;->g:Lcom/google/android/location/e/b;

    iget-wide v2, v2, Lcom/google/android/location/e/b;->a:J

    cmp-long v2, v0, v2

    if-gez v2, :cond_3

    sget-object v2, Lcom/google/android/location/j/k;->i:Lcom/google/android/location/j/k;

    if-ne p1, v2, :cond_3

    .line 628
    sget-boolean v2, Lcom/google/android/location/i/a;->e:Z

    if-eqz v2, :cond_3

    const-string v2, "ActivityScheduler"

    const-string v3, "Alarm rings too early"

    invoke-static {v2, v3}, Lcom/google/android/location/o/a/a;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 630
    :cond_3
    const-wide/16 v2, 0xfa0

    add-long/2addr v0, v2

    iget-object v2, p0, Lcom/google/android/location/activity/k;->g:Lcom/google/android/location/e/b;

    iget-wide v2, v2, Lcom/google/android/location/e/b;->a:J

    cmp-long v0, v0, v2

    if-gez v0, :cond_4

    .line 633
    sget-object v0, Lcom/google/android/location/j/k;->i:Lcom/google/android/location/j/k;

    if-ne p1, v0, :cond_1

    .line 636
    iget-object v0, p0, Lcom/google/android/location/activity/k;->g:Lcom/google/android/location/e/b;

    .line 637
    iput-object v5, p0, Lcom/google/android/location/activity/k;->g:Lcom/google/android/location/e/b;

    .line 638
    invoke-virtual {p0, v0}, Lcom/google/android/location/activity/k;->a(Lcom/google/android/location/e/b;)V

    goto :goto_0

    .line 641
    :cond_4
    sget-object v0, Lcom/google/android/location/j/k;->i:Lcom/google/android/location/j/k;

    if-ne p1, v0, :cond_5

    .line 642
    iput-object v5, p0, Lcom/google/android/location/activity/k;->g:Lcom/google/android/location/e/b;

    .line 646
    :goto_1
    iget-object v0, p0, Lcom/google/android/location/activity/k;->e:Lcom/google/android/location/activity/av;

    invoke-virtual {v0}, Lcom/google/android/location/activity/av;->c()V

    goto :goto_0

    .line 644
    :cond_5
    invoke-virtual {p0}, Lcom/google/android/location/activity/k;->E()V

    goto :goto_1
.end method

.method public final a(Lcom/google/android/location/j/k;Lcom/google/android/location/e/b;)V
    .locals 1

    .prologue
    .line 661
    sget-object v0, Lcom/google/android/location/j/k;->a:Lcom/google/android/location/j/k;

    if-ne p1, v0, :cond_0

    .line 662
    iput-object p2, p0, Lcom/google/android/location/activity/k;->N:Lcom/google/android/location/e/b;

    .line 664
    :cond_0
    iget-object v0, p0, Lcom/google/android/location/activity/k;->e:Lcom/google/android/location/activity/av;

    invoke-virtual {v0, p1}, Lcom/google/android/location/activity/av;->a(Lcom/google/android/location/j/k;)V

    .line 665
    return-void
.end method

.method public final a(Ljava/io/PrintWriter;)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 955
    const-string v0, "####ActivityDetectionScheduler Start"

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 956
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "os.hasAccelerometer()="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/location/activity/k;->b:Lcom/google/android/location/os/bi;

    invoke-interface {v1}, Lcom/google/android/location/os/bi;->s()Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 957
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "significantMotionDetector.isSupported()="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/location/activity/k;->n:Lcom/google/android/location/activity/at;

    invoke-interface {v1}, Lcom/google/android/location/activity/at;->b()Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 959
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "wakeUpTiltDetector.isSupported()="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/location/activity/k;->o:Lcom/google/android/location/activity/bn;

    invoke-interface {v1}, Lcom/google/android/location/activity/bn;->b()Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 960
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "os.isSensorBatchingSupported(TYPE_ACCELEROMETER)="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/location/activity/k;->b:Lcom/google/android/location/os/bi;

    invoke-interface {v1, v5}, Lcom/google/android/location/os/bi;->a(I)Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 962
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Current state: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/location/activity/k;->e:Lcom/google/android/location/activity/av;

    invoke-virtual {v1}, Lcom/google/android/location/activity/av;->p()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 963
    iget-object v0, p0, Lcom/google/android/location/activity/k;->q:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 964
    const-string v2, "Entered %s %d times.\n"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    iget-object v4, p0, Lcom/google/android/location/activity/k;->q:Ljava/util/Map;

    invoke-interface {v4, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    aput-object v0, v3, v5

    invoke-virtual {p1, v2, v3}, Ljava/io/PrintWriter;->printf(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/PrintWriter;

    goto :goto_0

    .line 966
    :cond_0
    const-string v0, "####ActivityDetectionScheduler End"

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 967
    return-void
.end method

.method final a(Z)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 524
    sget-boolean v0, Lcom/google/android/location/i/a;->b:Z

    if-eqz v0, :cond_0

    const-string v0, "ActivityScheduler"

    const-string v1, "Disabling low power mode"

    invoke-static {v0, v1}, Lcom/google/android/location/o/a/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 525
    :cond_0
    iget-object v0, p0, Lcom/google/android/location/activity/k;->b:Lcom/google/android/location/os/bi;

    invoke-interface {v0}, Lcom/google/android/location/os/bi;->v()Lcom/google/android/location/os/j;

    move-result-object v0

    sget-object v1, Lcom/google/android/location/os/au;->aa:Lcom/google/android/location/os/au;

    invoke-virtual {v0, v1}, Lcom/google/android/location/os/j;->a(Lcom/google/android/location/os/au;)V

    .line 526
    iget-object v0, p0, Lcom/google/android/location/activity/k;->m:Lcom/google/android/location/activity/ah;

    invoke-virtual {v0}, Lcom/google/android/location/activity/ah;->a()V

    .line 527
    iget-object v0, p0, Lcom/google/android/location/activity/k;->f:Lcom/google/android/location/activity/a;

    iget v1, p0, Lcom/google/android/location/activity/k;->J:I

    invoke-virtual {v0, v1}, Lcom/google/android/location/activity/a;->a(I)Z

    .line 528
    iget-object v0, p0, Lcom/google/android/location/activity/k;->y:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/location/activity/k;->o:Lcom/google/android/location/activity/bn;

    invoke-interface {v0}, Lcom/google/android/location/activity/bn;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 530
    iget-object v0, p0, Lcom/google/android/location/activity/k;->o:Lcom/google/android/location/activity/bn;

    invoke-interface {v0, p0}, Lcom/google/android/location/activity/bn;->a(Lcom/google/android/location/activity/bo;)Z

    .line 532
    :cond_1
    sput-boolean v2, Lcom/google/android/location/activity/k;->F:Z

    .line 533
    sput-boolean v2, Lcom/google/android/location/activity/k;->G:Z

    .line 534
    if-eqz p1, :cond_2

    .line 535
    invoke-direct {p0}, Lcom/google/android/location/activity/k;->M()V

    .line 538
    :cond_2
    iget-object v0, p0, Lcom/google/android/location/activity/k;->b:Lcom/google/android/location/os/bi;

    const-string v1, "com.google.android.location.activity.LOW_POWER_MODE_DISABLED"

    invoke-interface {v0, v1}, Lcom/google/android/location/os/bi;->b(Ljava/lang/String;)V

    .line 539
    return-void
.end method

.method public final a(Lcom/google/p/a/b/b/a;)Z
    .locals 1

    .prologue
    .line 888
    if-eqz p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method final a(Ljava/lang/Integer;)Z
    .locals 8

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x1

    .line 413
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-eq v0, v2, :cond_0

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const/4 v3, 0x2

    if-ne v0, v3, :cond_1

    :cond_0
    move v0, v2

    .line 416
    :goto_0
    iget-object v3, p0, Lcom/google/android/location/activity/k;->j:Lcom/google/android/location/activity/bd;

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/google/android/location/activity/k;->b:Lcom/google/android/location/os/bi;

    invoke-interface {v3}, Lcom/google/android/location/os/bi;->c()Lcom/google/android/location/j/b;

    move-result-object v3

    invoke-interface {v3}, Lcom/google/android/location/j/b;->c()J

    move-result-wide v4

    iget-wide v6, p0, Lcom/google/android/location/activity/k;->k:J

    sub-long/2addr v4, v6

    const-wide/32 v6, 0x15f90

    cmp-long v3, v4, v6

    if-gez v3, :cond_2

    iget-object v3, p0, Lcom/google/android/location/activity/k;->j:Lcom/google/android/location/activity/bd;

    iget-wide v4, v3, Lcom/google/android/location/activity/bd;->b:D

    const-wide v6, 0x3fe6666666666666L    # 0.7

    cmpl-double v3, v4, v6

    if-ltz v3, :cond_2

    move v3, v2

    :goto_1
    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/google/android/location/activity/k;->j:Lcom/google/android/location/activity/bd;

    iget-object v3, v3, Lcom/google/android/location/activity/bd;->a:Lcom/google/android/location/e/ay;

    sget-object v4, Lcom/google/android/location/e/ay;->a:Lcom/google/android/location/e/ay;

    if-ne v3, v4, :cond_3

    move v3, v2

    .line 418
    :goto_2
    if-eqz v3, :cond_4

    if-eqz v0, :cond_4

    :goto_3
    return v2

    :cond_1
    move v0, v1

    .line 413
    goto :goto_0

    :cond_2
    move v3, v1

    .line 416
    goto :goto_1

    :cond_3
    move v3, v1

    goto :goto_2

    :cond_4
    move v2, v1

    .line 418
    goto :goto_3
.end method

.method public final b(I)V
    .locals 2

    .prologue
    .line 896
    mul-int/lit16 v0, p1, 0x3e8

    int-to-long v0, v0

    iput-wide v0, p0, Lcom/google/android/location/activity/k;->M:J

    .line 897
    return-void
.end method

.method final b(J)V
    .locals 1

    .prologue
    .line 373
    iput-wide p1, p0, Lcom/google/android/location/activity/k;->P:J

    .line 374
    return-void
.end method

.method final b(Lcom/google/android/gms/location/ActivityRecognitionResult;)V
    .locals 2

    .prologue
    .line 576
    iget-object v0, p0, Lcom/google/android/location/activity/k;->p:Ljava/util/List;

    .line 577
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/activity/l;

    .line 578
    invoke-interface {v0, p1}, Lcom/google/android/location/activity/l;->a_(Lcom/google/android/gms/location/ActivityRecognitionResult;)V

    goto :goto_0

    .line 580
    :cond_0
    return-void
.end method

.method public final b(Lcom/google/android/location/activity/l;)V
    .locals 2

    .prologue
    .line 916
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/google/android/location/activity/k;->p:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 918
    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 919
    iput-object v0, p0, Lcom/google/android/location/activity/k;->p:Ljava/util/List;

    .line 920
    return-void
.end method

.method public final b(Z)V
    .locals 3

    .prologue
    .line 675
    iput-boolean p1, p0, Lcom/google/android/location/activity/k;->I:Z

    .line 676
    sget-boolean v0, Lcom/google/android/location/i/a;->b:Z

    if-eqz v0, :cond_0

    const-string v0, "ActivityScheduler"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "isScreenOn="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v2, p0, Lcom/google/android/location/activity/k;->I:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/location/o/a/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 677
    :cond_0
    iget-object v0, p0, Lcom/google/android/location/activity/k;->e:Lcom/google/android/location/activity/av;

    invoke-virtual {v0}, Lcom/google/android/location/activity/av;->aq_()V

    .line 678
    return-void
.end method

.method final c(J)V
    .locals 1

    .prologue
    .line 381
    iput-wide p1, p0, Lcom/google/android/location/activity/k;->Q:J

    .line 382
    return-void
.end method

.method public final c(Z)V
    .locals 3

    .prologue
    .line 689
    iput-boolean p1, p0, Lcom/google/android/location/activity/k;->H:Z

    .line 690
    sget-boolean v0, Lcom/google/android/location/i/a;->b:Z

    if-eqz v0, :cond_0

    const-string v0, "ActivityScheduler"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "isPowerSaveMode="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v2, p0, Lcom/google/android/location/activity/k;->H:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/location/o/a/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 691
    :cond_0
    iget-object v0, p0, Lcom/google/android/location/activity/k;->e:Lcom/google/android/location/activity/av;

    invoke-virtual {v0}, Lcom/google/android/location/activity/av;->au_()V

    .line 692
    return-void
.end method

.method public final c()Z
    .locals 2

    .prologue
    .line 204
    iget-object v0, p0, Lcom/google/android/location/activity/k;->E:Ljava/util/Set;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/location/activity/k;->E:Ljava/util/Set;

    const/16 v1, 0x9

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 206
    const/4 v0, 0x1

    .line 208
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final d()Lcom/google/android/location/activity/m;
    .locals 1

    .prologue
    .line 250
    iget-object v0, p0, Lcom/google/android/location/activity/k;->l:Lcom/google/android/location/activity/m;

    return-object v0
.end method

.method protected final d(J)V
    .locals 3

    .prologue
    .line 474
    iget-object v0, p0, Lcom/google/android/location/activity/k;->e:Lcom/google/android/location/activity/av;

    instance-of v0, v0, Lcom/google/android/location/activity/ao;

    if-eqz v0, :cond_1

    .line 475
    iput-wide p1, p0, Lcom/google/android/location/activity/k;->S:J

    .line 480
    :cond_0
    :goto_0
    return-void

    .line 477
    :cond_1
    sget-boolean v0, Lcom/google/android/location/i/a;->e:Z

    if-eqz v0, :cond_0

    const-string v0, "ActivityScheduler"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/google/android/location/activity/k;->e:Lcom/google/android/location/activity/av;

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " attempt to setFirstScreenStateChangeTimeSinceLastTrigger, ignored."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/location/o/a/a;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method final e()Lcom/google/android/location/activity/g;
    .locals 1

    .prologue
    .line 263
    iget-object v0, p0, Lcom/google/android/location/activity/k;->c:Lcom/google/android/location/activity/g;

    return-object v0
.end method

.method public final e(J)V
    .locals 5

    .prologue
    .line 587
    new-instance v0, Lcom/google/android/location/e/b;

    const-wide/16 v2, 0x0

    invoke-direct {v0, p1, p2, v2, v3}, Lcom/google/android/location/e/b;-><init>(JJ)V

    invoke-virtual {p0, v0}, Lcom/google/android/location/activity/k;->a(Lcom/google/android/location/e/b;)V

    .line 588
    return-void
.end method

.method final f()Lcom/google/android/location/activity/i;
    .locals 1

    .prologue
    .line 267
    iget-object v0, p0, Lcom/google/android/location/activity/k;->d:Lcom/google/android/location/activity/i;

    return-object v0
.end method

.method final g()Lcom/google/android/location/activity/av;
    .locals 1

    .prologue
    .line 271
    iget-object v0, p0, Lcom/google/android/location/activity/k;->e:Lcom/google/android/location/activity/av;

    return-object v0
.end method

.method final h()Lcom/google/android/location/activity/a;
    .locals 1

    .prologue
    .line 287
    iget-object v0, p0, Lcom/google/android/location/activity/k;->f:Lcom/google/android/location/activity/a;

    return-object v0
.end method

.method final i()Lcom/google/android/gms/location/ActivityRecognitionResult;
    .locals 1

    .prologue
    .line 291
    iget-object v0, p0, Lcom/google/android/location/activity/k;->h:Lcom/google/android/gms/location/ActivityRecognitionResult;

    return-object v0
.end method

.method final j()Lcom/google/android/gms/location/ActivityRecognitionResult;
    .locals 1

    .prologue
    .line 295
    iget-object v0, p0, Lcom/google/android/location/activity/k;->i:Lcom/google/android/gms/location/ActivityRecognitionResult;

    return-object v0
.end method

.method final k()Lcom/google/android/location/activity/at;
    .locals 1

    .prologue
    .line 299
    iget-object v0, p0, Lcom/google/android/location/activity/k;->n:Lcom/google/android/location/activity/at;

    return-object v0
.end method

.method final l()Lcom/google/android/location/activity/bn;
    .locals 1

    .prologue
    .line 303
    iget-object v0, p0, Lcom/google/android/location/activity/k;->o:Lcom/google/android/location/activity/bn;

    return-object v0
.end method

.method final m()Lcom/google/android/location/activity/a/o;
    .locals 1

    .prologue
    .line 307
    iget-object v0, p0, Lcom/google/android/location/activity/k;->s:Lcom/google/android/location/activity/a/o;

    return-object v0
.end method

.method final n()Lcom/google/android/location/activity/a/o;
    .locals 1

    .prologue
    .line 311
    iget-object v0, p0, Lcom/google/android/location/activity/k;->r:Lcom/google/android/location/activity/a/o;

    return-object v0
.end method

.method final o()Lcom/google/android/location/activity/a/o;
    .locals 1

    .prologue
    .line 315
    iget-object v0, p0, Lcom/google/android/location/activity/k;->t:Lcom/google/android/location/activity/a/o;

    return-object v0
.end method

.method final p()Lcom/google/android/location/activity/a/o;
    .locals 1

    .prologue
    .line 319
    iget-object v0, p0, Lcom/google/android/location/activity/k;->u:Lcom/google/android/location/activity/a/o;

    return-object v0
.end method

.method final q()J
    .locals 4

    .prologue
    .line 323
    invoke-virtual {p0}, Lcom/google/android/location/activity/k;->B()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 324
    sget-object v0, Lcom/google/android/location/d/a;->v:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/google/android/location/activity/k;->A:J

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v0

    .line 327
    :goto_0
    return-wide v0

    :cond_0
    iget-wide v0, p0, Lcom/google/android/location/activity/k;->A:J

    goto :goto_0
.end method

.method final r()J
    .locals 4

    .prologue
    .line 332
    invoke-virtual {p0}, Lcom/google/android/location/activity/k;->B()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 333
    sget-object v0, Lcom/google/android/location/d/a;->v:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/google/android/location/activity/k;->B:J

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v0

    .line 336
    :goto_0
    return-wide v0

    :cond_0
    iget-wide v0, p0, Lcom/google/android/location/activity/k;->B:J

    goto :goto_0
.end method

.method final s()Lcom/google/android/location/o/n;
    .locals 1

    .prologue
    .line 341
    iget-object v0, p0, Lcom/google/android/location/activity/k;->C:Lcom/google/android/location/o/n;

    return-object v0
.end method

.method final t()I
    .locals 1

    .prologue
    .line 345
    iget v0, p0, Lcom/google/android/location/activity/k;->J:I

    return v0
.end method

.method final u()J
    .locals 2

    .prologue
    .line 349
    iget-wide v0, p0, Lcom/google/android/location/activity/k;->M:J

    return-wide v0
.end method

.method final v()V
    .locals 2

    .prologue
    .line 353
    const-wide v0, 0x1f3fffffc18L

    iput-wide v0, p0, Lcom/google/android/location/activity/k;->M:J

    .line 354
    return-void
.end method

.method final w()Lcom/google/android/location/e/b;
    .locals 1

    .prologue
    .line 357
    iget-object v0, p0, Lcom/google/android/location/activity/k;->N:Lcom/google/android/location/e/b;

    return-object v0
.end method

.method final x()J
    .locals 2

    .prologue
    .line 361
    iget-wide v0, p0, Lcom/google/android/location/activity/k;->O:J

    return-wide v0
.end method

.method final y()J
    .locals 2

    .prologue
    .line 369
    iget-wide v0, p0, Lcom/google/android/location/activity/k;->P:J

    return-wide v0
.end method

.method final z()J
    .locals 2

    .prologue
    .line 377
    iget-wide v0, p0, Lcom/google/android/location/activity/k;->Q:J

    return-wide v0
.end method
