.class public final Lcom/google/android/location/places/aa;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/android/location/j/b;

.field private final b:Lcom/google/android/location/places/af;


# direct methods
.method public constructor <init>(Lcom/google/android/location/j/b;Lcom/google/android/location/places/af;)V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    iput-object p1, p0, Lcom/google/android/location/places/aa;->a:Lcom/google/android/location/j/b;

    .line 22
    iput-object p2, p0, Lcom/google/android/location/places/aa;->b:Lcom/google/android/location/places/af;

    .line 23
    return-void
.end method

.method private a(Lcom/google/android/location/places/ae;)D
    .locals 8

    .prologue
    .line 81
    iget v0, p1, Lcom/google/android/location/places/ae;->b:I

    .line 82
    iget-wide v2, p1, Lcom/google/android/location/places/ae;->c:J

    iget-object v1, p0, Lcom/google/android/location/places/aa;->b:Lcom/google/android/location/places/af;

    invoke-virtual {v1}, Lcom/google/android/location/places/af;->a()Lcom/google/android/location/places/ad;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/location/places/ad;->a()J

    move-result-wide v4

    long-to-double v4, v4

    const-wide/high16 v6, 0x4000000000000000L    # 2.0

    invoke-static {v6, v7}, Ljava/lang/Math;->log(D)D

    move-result-wide v6

    div-double/2addr v4, v6

    iget-object v1, p0, Lcom/google/android/location/places/aa;->a:Lcom/google/android/location/j/b;

    invoke-interface {v1}, Lcom/google/android/location/j/b;->c()J

    move-result-wide v6

    sub-long v2, v6, v2

    long-to-double v2, v2

    div-double/2addr v2, v4

    neg-double v2, v2

    invoke-static {v2, v3}, Ljava/lang/Math;->exp(D)D

    move-result-wide v2

    .line 84
    int-to-double v0, v0

    mul-double/2addr v0, v2

    return-wide v0
.end method

.method private static a(Ljava/util/List;)Ljava/util/Map;
    .locals 4

    .prologue
    .line 102
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 103
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/places/w;

    .line 104
    iget-object v3, v0, Lcom/google/android/location/places/w;->a:Ljava/lang/String;

    iget v0, v0, Lcom/google/android/location/places/w;->b:F

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    invoke-interface {v1, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 106
    :cond_0
    return-object v1
.end method

.method private static a(D)Z
    .locals 2

    .prologue
    .line 98
    const-wide v0, 0x3f847ae140000000L    # 0.009999999776482582

    cmpg-double v0, p0, v0

    if-gez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(Lcom/google/android/location/places/ae;Lcom/google/android/location/places/ae;)Lcom/google/android/location/places/ae;
    .locals 16

    .prologue
    .line 31
    invoke-direct/range {p0 .. p1}, Lcom/google/android/location/places/aa;->a(Lcom/google/android/location/places/ae;)D

    move-result-wide v2

    .line 32
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-direct {v0, v1}, Lcom/google/android/location/places/aa;->a(Lcom/google/android/location/places/ae;)D

    move-result-wide v4

    .line 33
    add-double v6, v2, v4

    .line 34
    div-double v8, v2, v6

    .line 35
    div-double v6, v4, v6

    .line 36
    invoke-static {}, Lcom/google/android/location/places/p;->a()Lcom/google/android/location/o/a/c;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Fusion weights "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v8, v9}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " & "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v6, v7}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/android/location/o/a/c;->b(Ljava/lang/String;)V

    .line 38
    add-double v2, v8, v6

    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    sub-double/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Math;->abs(D)D

    move-result-wide v2

    const-wide v4, 0x3eb0c6f7a0b5ed8dL    # 1.0E-6

    cmpl-double v2, v2, v4

    if-lez v2, :cond_0

    .line 39
    invoke-static {}, Lcom/google/android/location/places/p;->a()Lcom/google/android/location/o/a/c;

    move-result-object v2

    const-string v3, "Fusion weights don\'t add up to 1!!"

    invoke-virtual {v2, v3}, Lcom/google/android/location/o/a/c;->d(Ljava/lang/String;)V

    .line 42
    :cond_0
    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/google/android/location/places/ae;->a:Ljava/util/List;

    invoke-static {v2}, Lcom/google/android/location/places/aa;->a(Ljava/util/List;)Ljava/util/Map;

    move-result-object v10

    .line 43
    move-object/from16 v0, p2

    iget-object v2, v0, Lcom/google/android/location/places/ae;->a:Ljava/util/List;

    invoke-static {v2}, Lcom/google/android/location/places/aa;->a(Ljava/util/List;)Ljava/util/Map;

    move-result-object v11

    .line 44
    new-instance v12, Ljava/util/ArrayList;

    invoke-direct {v12}, Ljava/util/ArrayList;-><init>()V

    .line 47
    invoke-interface {v10}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v13

    :cond_1
    :goto_0
    invoke-interface {v13}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v13}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 48
    invoke-interface {v10, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Float;

    invoke-virtual {v3}, Ljava/lang/Float;->floatValue()F

    move-result v3

    float-to-double v14, v3

    .line 49
    invoke-interface {v11, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v11, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Float;

    invoke-virtual {v3}, Ljava/lang/Float;->floatValue()F

    move-result v3

    float-to-double v4, v3

    .line 50
    :goto_1
    mul-double/2addr v14, v8

    mul-double/2addr v4, v6

    add-double/2addr v4, v14

    .line 52
    invoke-static {v4, v5}, Lcom/google/android/location/places/aa;->a(D)Z

    move-result v3

    if-nez v3, :cond_1

    .line 53
    new-instance v3, Lcom/google/android/location/places/w;

    double-to-float v4, v4

    invoke-direct {v3, v2, v4}, Lcom/google/android/location/places/w;-><init>(Ljava/lang/String;F)V

    invoke-interface {v12, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 49
    :cond_2
    const-wide/16 v4, 0x0

    goto :goto_1

    .line 58
    :cond_3
    invoke-interface {v11}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_4
    :goto_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_5

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 59
    invoke-interface {v10, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_4

    .line 61
    invoke-interface {v11, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Float;

    invoke-virtual {v3}, Ljava/lang/Float;->floatValue()F

    move-result v3

    float-to-double v14, v3

    .line 65
    mul-double/2addr v14, v6

    .line 66
    invoke-static {v14, v15}, Lcom/google/android/location/places/aa;->a(D)Z

    move-result v3

    if-nez v3, :cond_4

    .line 67
    new-instance v3, Lcom/google/android/location/places/w;

    double-to-float v5, v14

    invoke-direct {v3, v2, v5}, Lcom/google/android/location/places/w;-><init>(Ljava/lang/String;F)V

    invoke-interface {v12, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 70
    :cond_5
    invoke-static {}, Lcom/google/android/location/places/p;->a()Lcom/google/android/location/o/a/c;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Fused place inferences: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v12}, Ljava/util/List;->size()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/android/location/o/a/c;->b(Ljava/lang/String;)V

    .line 73
    move-object/from16 v0, p1

    iget v2, v0, Lcom/google/android/location/places/ae;->b:I

    int-to-double v2, v2

    mul-double/2addr v2, v8

    move-object/from16 v0, p2

    iget v4, v0, Lcom/google/android/location/places/ae;->b:I

    int-to-double v4, v4

    mul-double/2addr v4, v6

    add-double/2addr v2, v4

    double-to-int v2, v2

    .line 75
    invoke-static {}, Lcom/google/android/location/places/p;->a()Lcom/google/android/location/o/a/c;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Fusing trust levels "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p1

    iget v5, v0, Lcom/google/android/location/places/ae;->b:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " & "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p2

    iget v5, v0, Lcom/google/android/location/places/ae;->b:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ": new level "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/google/android/location/o/a/c;->b(Ljava/lang/String;)V

    .line 77
    new-instance v3, Lcom/google/android/location/places/ae;

    move-object/from16 v0, p2

    iget-wide v4, v0, Lcom/google/android/location/places/ae;->c:J

    invoke-direct {v3, v12, v2, v4, v5}, Lcom/google/android/location/places/ae;-><init>(Ljava/util/List;IJ)V

    return-object v3
.end method
