.class public final Lcom/google/android/location/collectionlib/cp;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private a:I

.field private b:I

.field private c:I

.field private d:I

.field private final e:Z


# direct methods
.method constructor <init>(Z)V
    .locals 0

    .prologue
    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 42
    iput-boolean p1, p0, Lcom/google/android/location/collectionlib/cp;->e:Z

    .line 43
    return-void
.end method

.method static a(Ljava/lang/String;)Lcom/google/android/location/collectionlib/cp;
    .locals 7

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 51
    new-instance v3, Ljava/io/BufferedReader;

    new-instance v2, Ljava/io/FileReader;

    invoke-static {p0}, Lcom/google/android/location/collectionlib/cp;->c(Ljava/lang/String;)Ljava/io/File;

    move-result-object v4

    invoke-direct {v2, v4}, Ljava/io/FileReader;-><init>(Ljava/io/File;)V

    invoke-direct {v3, v2}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    .line 53
    :try_start_0
    invoke-virtual {v3}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    .line 56
    invoke-virtual {v3}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v5

    .line 57
    invoke-virtual {v3}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v6

    .line 59
    const/4 v2, 0x2

    if-lt v4, v2, :cond_2

    .line 60
    invoke-virtual {v3}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    if-ne v2, v0, :cond_0

    :goto_0
    move v2, v0

    .line 64
    :goto_1
    const/4 v0, 0x3

    if-lt v4, v0, :cond_1

    .line 65
    invoke-virtual {v3}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    .line 66
    invoke-virtual {v3}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    .line 68
    :goto_2
    new-instance v4, Lcom/google/android/location/collectionlib/cp;

    invoke-direct {v4, v2}, Lcom/google/android/location/collectionlib/cp;-><init>(Z)V

    .line 69
    iput v5, v4, Lcom/google/android/location/collectionlib/cp;->a:I

    .line 70
    iput v6, v4, Lcom/google/android/location/collectionlib/cp;->b:I

    .line 71
    iput v0, v4, Lcom/google/android/location/collectionlib/cp;->c:I

    .line 72
    iput v1, v4, Lcom/google/android/location/collectionlib/cp;->d:I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 76
    invoke-virtual {v3}, Ljava/io/BufferedReader;->close()V

    .line 78
    return-object v4

    :cond_0
    move v0, v1

    .line 60
    goto :goto_0

    .line 73
    :catch_0
    move-exception v0

    .line 74
    :try_start_1
    new-instance v1, Ljava/io/IOException;

    invoke-virtual {v0}, Ljava/lang/NumberFormatException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 76
    :catchall_0
    move-exception v0

    invoke-virtual {v3}, Ljava/io/BufferedReader;->close()V

    throw v0

    :cond_1
    move v0, v1

    goto :goto_2

    :cond_2
    move v2, v1

    goto :goto_1
.end method

.method private static c(Ljava/lang/String;)Ljava/io/File;
    .locals 2

    .prologue
    .line 170
    new-instance v0, Ljava/io/File;

    const-string v1, "sessionSummary"

    invoke-direct {v0, p0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method


# virtual methods
.method public final declared-synchronized a()I
    .locals 1

    .prologue
    .line 150
    monitor-enter p0

    :try_start_0
    iget v0, p0, Lcom/google/android/location/collectionlib/cp;->a:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method final declared-synchronized a(Lcom/google/p/a/b/b/a;)V
    .locals 10

    .prologue
    const/4 v2, 0x0

    .line 87
    monitor-enter p0

    const/4 v0, 0x4

    :try_start_0
    invoke-virtual {p1, v0}, Lcom/google/p/a/b/b/a;->k(I)I

    move-result v4

    .line 88
    if-lez v4, :cond_5

    move v3, v2

    .line 89
    :goto_0
    if-ge v3, v4, :cond_5

    .line 90
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v3}, Lcom/google/p/a/b/b/a;->d(II)Lcom/google/p/a/b/b/a;

    move-result-object v0

    .line 91
    const/16 v1, 0x9

    invoke-virtual {v0, v1}, Lcom/google/p/a/b/b/a;->i(I)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 92
    const/16 v1, 0x9

    invoke-virtual {v0, v1}, Lcom/google/p/a/b/b/a;->f(I)Lcom/google/p/a/b/b/a;

    move-result-object v5

    .line 93
    iget v6, p0, Lcom/google/android/location/collectionlib/cp;->a:I

    const/16 v0, 0x8

    invoke-virtual {v5, v0}, Lcom/google/p/a/b/b/a;->k(I)I

    move-result v7

    move v1, v2

    move v0, v2

    :goto_1
    if-ge v1, v7, :cond_1

    const/16 v8, 0x8

    invoke-virtual {v5, v8, v1}, Lcom/google/p/a/b/b/a;->d(II)Lcom/google/p/a/b/b/a;

    move-result-object v8

    const/16 v9, 0x8

    invoke-virtual {v8, v9}, Lcom/google/p/a/b/b/a;->i(I)Z

    move-result v9

    if-eqz v9, :cond_0

    const/16 v9, 0x8

    invoke-virtual {v8, v9}, Lcom/google/p/a/b/b/a;->c(I)I

    move-result v8

    if-nez v8, :cond_0

    add-int/lit8 v0, v0, 0x1

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_1
    add-int/2addr v0, v6

    iput v0, p0, Lcom/google/android/location/collectionlib/cp;->a:I

    .line 94
    iget v0, p0, Lcom/google/android/location/collectionlib/cp;->b:I

    const/4 v1, 0x7

    invoke-virtual {v5, v1}, Lcom/google/p/a/b/b/a;->k(I)I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lcom/google/android/location/collectionlib/cp;->b:I

    .line 95
    iget v6, p0, Lcom/google/android/location/collectionlib/cp;->c:I

    const/16 v0, 0x13

    invoke-virtual {v5, v0}, Lcom/google/p/a/b/b/a;->k(I)I

    move-result v7

    move v1, v2

    move v0, v2

    :goto_2
    if-ge v1, v7, :cond_3

    const/16 v8, 0x13

    invoke-virtual {v5, v8, v1}, Lcom/google/p/a/b/b/a;->d(II)Lcom/google/p/a/b/b/a;

    move-result-object v8

    const/4 v9, 0x2

    invoke-virtual {v8, v9}, Lcom/google/p/a/b/b/a;->i(I)Z

    move-result v8

    if-eqz v8, :cond_2

    add-int/lit8 v0, v0, 0x1

    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    :cond_3
    add-int/2addr v0, v6

    iput v0, p0, Lcom/google/android/location/collectionlib/cp;->c:I

    .line 96
    iget v0, p0, Lcom/google/android/location/collectionlib/cp;->d:I

    add-int/lit8 v0, v0, 0x0

    iput v0, p0, Lcom/google/android/location/collectionlib/cp;->d:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 89
    :cond_4
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_0

    .line 100
    :cond_5
    monitor-exit p0

    return-void

    .line 87
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b()I
    .locals 1

    .prologue
    .line 154
    monitor-enter p0

    :try_start_0
    iget v0, p0, Lcom/google/android/location/collectionlib/cp;->b:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method final declared-synchronized b(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 139
    monitor-enter p0

    :try_start_0
    new-instance v1, Ljava/io/PrintWriter;

    invoke-static {p1}, Lcom/google/android/location/collectionlib/cp;->c(Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/io/PrintWriter;-><init>(Ljava/io/File;)V

    .line 140
    const/4 v0, 0x3

    invoke-virtual {v1, v0}, Ljava/io/PrintWriter;->println(I)V

    .line 141
    iget v0, p0, Lcom/google/android/location/collectionlib/cp;->a:I

    invoke-virtual {v1, v0}, Ljava/io/PrintWriter;->println(I)V

    .line 142
    iget v0, p0, Lcom/google/android/location/collectionlib/cp;->b:I

    invoke-virtual {v1, v0}, Ljava/io/PrintWriter;->println(I)V

    .line 143
    iget-boolean v0, p0, Lcom/google/android/location/collectionlib/cp;->e:Z

    if-eqz v0, :cond_0

    const-string v0, "1"

    :goto_0
    invoke-virtual {v1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 144
    iget v0, p0, Lcom/google/android/location/collectionlib/cp;->c:I

    invoke-virtual {v1, v0}, Ljava/io/PrintWriter;->println(I)V

    .line 145
    iget v0, p0, Lcom/google/android/location/collectionlib/cp;->d:I

    invoke-virtual {v1, v0}, Ljava/io/PrintWriter;->println(I)V

    .line 146
    invoke-virtual {v1}, Ljava/io/PrintWriter;->close()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 147
    monitor-exit p0

    return-void

    .line 143
    :cond_0
    :try_start_1
    const-string v0, "0"
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 139
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized c()I
    .locals 1

    .prologue
    .line 158
    monitor-enter p0

    :try_start_0
    iget v0, p0, Lcom/google/android/location/collectionlib/cp;->c:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized d()I
    .locals 1

    .prologue
    .line 162
    monitor-enter p0

    :try_start_0
    iget v0, p0, Lcom/google/android/location/collectionlib/cp;->d:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized e()Z
    .locals 1

    .prologue
    .line 166
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/location/collectionlib/cp;->e:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 175
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "SessionSummary [gpsCount="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v1, p0, Lcom/google/android/location/collectionlib/cp;->a:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", gpsMeasurementsCount="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/location/collectionlib/cp;->c:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", gpsNavigationMessagesCount="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/location/collectionlib/cp;->d:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", wifiScanCount="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/location/collectionlib/cp;->b:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", forceUpload="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/google/android/location/collectionlib/cp;->e:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
