.class public final Lcom/google/android/location/os/real/a;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/location/o/a/b;


# static fields
.field private static final a:[Ljava/lang/String;


# instance fields
.field private final b:Lcom/google/android/location/os/j;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 18
    const/16 v0, 0x8

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "0"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "1"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "V"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "D"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "I"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "W"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "E"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "A"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/location/os/real/a;->a:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/location/os/real/a;->b:Lcom/google/android/location/os/j;

    .line 28
    return-void
.end method

.method public constructor <init>(Lcom/google/android/location/os/j;)V
    .locals 0

    .prologue
    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    iput-object p1, p0, Lcom/google/android/location/os/real/a;->b:Lcom/google/android/location/os/j;

    .line 37
    return-void
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;I)V
    .locals 2

    .prologue
    .line 76
    invoke-static {p3, p1, p2}, Landroid/util/Log;->println(ILjava/lang/String;Ljava/lang/String;)I

    .line 77
    iget-object v0, p0, Lcom/google/android/location/os/real/a;->b:Lcom/google/android/location/os/j;

    if-eqz v0, :cond_0

    .line 78
    if-ltz p3, :cond_1

    sget-object v0, Lcom/google/android/location/os/real/a;->a:[Ljava/lang/String;

    array-length v0, v0

    if-ge p3, v0, :cond_1

    sget-object v0, Lcom/google/android/location/os/real/a;->a:[Ljava/lang/String;

    aget-object v0, v0, p3

    .line 81
    :goto_0
    iget-object v1, p0, Lcom/google/android/location/os/real/a;->b:Lcom/google/android/location/os/j;

    invoke-virtual {v1, p1, p2, v0}, Lcom/google/android/location/os/j;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 83
    :cond_0
    return-void

    .line 78
    :cond_1
    invoke-static {p3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 41
    const/4 v0, 0x3

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/location/os/real/a;->a(Ljava/lang/String;Ljava/lang/String;I)V

    .line 42
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 4

    .prologue
    .line 61
    sget-boolean v0, Lcom/google/android/location/i/a;->e:Z

    if-eqz v0, :cond_0

    invoke-static {p1, p2, p3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 62
    :cond_0
    iget-object v0, p0, Lcom/google/android/location/os/real/a;->b:Lcom/google/android/location/os/j;

    if-eqz v0, :cond_1

    .line 63
    iget-object v0, p0, Lcom/google/android/location/os/real/a;->b:Lcom/google/android/location/os/j;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const/16 v2, 0xa

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {p3}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/google/android/location/os/real/a;->a:[Ljava/lang/String;

    const/4 v3, 0x6

    aget-object v2, v2, v3

    invoke-virtual {v0, p1, v1, v2}, Lcom/google/android/location/os/j;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 65
    :cond_1
    return-void
.end method

.method public final a(Ljava/lang/String;I)Z
    .locals 1

    .prologue
    .line 87
    invoke-static {p1, p2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    return v0
.end method

.method public final b(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 56
    const/4 v0, 0x6

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/location/os/real/a;->a(Ljava/lang/String;Ljava/lang/String;I)V

    .line 57
    return-void
.end method

.method public final c(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 46
    const/4 v0, 0x4

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/location/os/real/a;->a(Ljava/lang/String;Ljava/lang/String;I)V

    .line 47
    return-void
.end method

.method public final d(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 51
    const/4 v0, 0x5

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/location/os/real/a;->a(Ljava/lang/String;Ljava/lang/String;I)V

    .line 52
    return-void
.end method

.method public final e(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 69
    invoke-static {p1, p2}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)I

    .line 70
    iget-object v0, p0, Lcom/google/android/location/os/real/a;->b:Lcom/google/android/location/os/j;

    if-eqz v0, :cond_0

    .line 71
    iget-object v0, p0, Lcom/google/android/location/os/real/a;->b:Lcom/google/android/location/os/j;

    sget-object v1, Lcom/google/android/location/os/real/a;->a:[Ljava/lang/String;

    const/4 v2, 0x7

    aget-object v1, v1, v2

    invoke-virtual {v0, p1, p2, v1}, Lcom/google/android/location/os/j;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 73
    :cond_0
    return-void
.end method
