.class public final Lcom/google/android/location/copresence/debug/CopresDatabaseHelper;
.super Landroid/database/sqlite/SQLiteOpenHelper;
.source "SourceFile"


# static fields
.field private static a:Lcom/google/android/location/copresence/debug/CopresDatabaseHelper;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 55
    const/4 v0, 0x0

    sput-object v0, Lcom/google/android/location/copresence/debug/CopresDatabaseHelper;->a:Lcom/google/android/location/copresence/debug/CopresDatabaseHelper;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 517
    const-string v0, "CopresDatabase"

    const/4 v1, 0x0

    const/16 v2, 0x32

    invoke-direct {p0, p1, v0, v1, v2}, Landroid/database/sqlite/SQLiteOpenHelper;-><init>(Landroid/content/Context;Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase$CursorFactory;I)V

    .line 518
    return-void
.end method

.method public static declared-synchronized a(Landroid/content/Context;)V
    .locals 6

    .prologue
    .line 618
    const-class v1, Lcom/google/android/location/copresence/debug/CopresDatabaseHelper;

    monitor-enter v1

    :try_start_0
    invoke-static {}, Lcom/google/android/location/copresence/f/a;->a()Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_0

    .line 631
    :goto_0
    monitor-exit v1

    return-void

    .line 621
    :cond_0
    :try_start_1
    invoke-static {p0}, Lcom/google/android/location/copresence/debug/CopresDatabaseHelper;->b(Landroid/content/Context;)Lcom/google/android/location/copresence/debug/CopresDatabaseHelper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/location/copresence/debug/CopresDatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 623
    new-instance v2, Lcom/google/android/location/copresence/debug/CopresDatabaseHelper$GcoreStartupRecord;

    invoke-direct {v2}, Lcom/google/android/location/copresence/debug/CopresDatabaseHelper$GcoreStartupRecord;-><init>()V

    .line 624
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    iput-object v3, v2, Lcom/google/android/location/copresence/debug/CopresDatabaseHelper$GcoreStartupRecord;->timestamp:Ljava/lang/Long;

    .line 625
    const/16 v3, 0xc

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    iput-object v3, v2, Lcom/google/android/location/copresence/debug/CopresDatabaseHelper$GcoreStartupRecord;->majorReleaseNumber:Ljava/lang/Integer;

    .line 626
    const v3, 0x6768a8

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    iput-object v3, v2, Lcom/google/android/location/copresence/debug/CopresDatabaseHelper$GcoreStartupRecord;->apkVersionCode:Ljava/lang/Integer;

    .line 627
    const-string v3, "6.7.77 (1747363-000)"

    iput-object v3, v2, Lcom/google/android/location/copresence/debug/CopresDatabaseHelper$GcoreStartupRecord;->apkVersionName:Ljava/lang/String;

    .line 628
    const v3, 0x6768a8

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    iput-object v3, v2, Lcom/google/android/location/copresence/debug/CopresDatabaseHelper$GcoreStartupRecord;->jarVersionCode:Ljava/lang/Integer;

    .line 629
    const-string v3, "6.7.77-000"

    iput-object v3, v2, Lcom/google/android/location/copresence/debug/CopresDatabaseHelper$GcoreStartupRecord;->jarVersionName:Ljava/lang/String;

    .line 630
    invoke-virtual {v2, v0}, Lcom/google/android/location/copresence/debug/CopresDatabaseHelper$GcoreStartupRecord;->save(Landroid/database/sqlite/SQLiteDatabase;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 618
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static a(Landroid/content/Context;Lcom/google/ac/b/c/ah;ZI)V
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 1034
    if-nez p0, :cond_0

    .line 1044
    :goto_0
    return-void

    .line 1040
    :cond_0
    new-instance v0, Lcom/google/android/location/copresence/debug/CopresDatabaseHelper$DirectiveRecord;

    move-object v1, p1

    move-object v3, v2

    move v4, p2

    move v5, p3

    invoke-direct/range {v0 .. v5}, Lcom/google/android/location/copresence/debug/CopresDatabaseHelper$DirectiveRecord;-><init>(Lcom/google/ac/b/c/ah;Lcom/google/android/location/copresence/debug/CopresDatabaseHelper$GcmPingRecord;Lcom/google/android/location/copresence/a/a;ZI)V

    .line 1043
    invoke-static {p0, v0}, Lcom/google/android/location/copresence/debug/CopresDatabaseHelper;->a(Landroid/content/Context;Lcom/google/android/location/copresence/debug/a;)V

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;Lcom/google/ac/b/c/co;Lcom/google/android/location/copresence/a/a;)V
    .locals 1

    .prologue
    .line 1058
    new-instance v0, Lcom/google/android/location/copresence/debug/CopresDatabaseHelper$GcmPingRecord;

    invoke-direct {v0, p2, p1}, Lcom/google/android/location/copresence/debug/CopresDatabaseHelper$GcmPingRecord;-><init>(Lcom/google/android/location/copresence/a/a;Lcom/google/ac/b/c/co;)V

    .line 1060
    invoke-static {p0, v0}, Lcom/google/android/location/copresence/debug/CopresDatabaseHelper;->a(Landroid/content/Context;Lcom/google/android/location/copresence/debug/a;)V

    .line 1061
    return-void
.end method

.method public static declared-synchronized a(Landroid/content/Context;Lcom/google/android/location/copresence/debug/CopresDatabaseHelper$TokenData;)V
    .locals 6

    .prologue
    .line 957
    const-class v1, Lcom/google/android/location/copresence/debug/CopresDatabaseHelper;

    monitor-enter v1

    :try_start_0
    invoke-static {}, Lcom/google/android/location/copresence/f/a;->a()Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_0

    .line 999
    :goto_0
    monitor-exit v1

    return-void

    .line 960
    :cond_0
    :try_start_1
    invoke-static {p0}, Lcom/google/android/location/copresence/debug/CopresDatabaseHelper;->b(Landroid/content/Context;)Lcom/google/android/location/copresence/debug/CopresDatabaseHelper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/location/copresence/debug/CopresDatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    .line 962
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 964
    :try_start_2
    iget-object v0, p1, Lcom/google/android/location/copresence/debug/CopresDatabaseHelper$TokenData;->tokenId:Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, p1, Lcom/google/android/location/copresence/debug/CopresDatabaseHelper$TokenData;->tokenId:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 965
    :cond_1
    const/4 v0, 0x6

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 966
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v3, "CopresDatabaseHelper: can\'t save TokenData to the database, no ID "

    invoke-direct {v0, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->e(Ljava/lang/String;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 998
    :cond_2
    :try_start_3
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 957
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 971
    :cond_3
    :try_start_4
    new-instance v0, Lcom/google/android/location/copresence/debug/CopresDatabaseHelper$TokenData;

    invoke-direct {v0}, Lcom/google/android/location/copresence/debug/CopresDatabaseHelper$TokenData;-><init>()V

    .line 972
    iget-object v3, p1, Lcom/google/android/location/copresence/debug/CopresDatabaseHelper$TokenData;->tokenId:Ljava/lang/String;

    iput-object v3, v0, Lcom/google/android/location/copresence/debug/CopresDatabaseHelper$TokenData;->tokenId:Ljava/lang/String;

    .line 973
    const/4 v3, 0x1

    const/4 v4, 0x0

    invoke-virtual {v0, v2, v3, v4}, Lcom/google/android/location/copresence/debug/CopresDatabaseHelper$TokenData;->selectMatching(Landroid/database/sqlite/SQLiteDatabase;II)Landroid/database/Cursor;

    move-result-object v0

    .line 974
    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v3

    if-nez v3, :cond_5

    .line 976
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    iput-object v3, p1, Lcom/google/android/location/copresence/debug/CopresDatabaseHelper$TokenData;->timestamp:Ljava/lang/Long;

    .line 978
    iget-object v3, p1, Lcom/google/android/location/copresence/debug/CopresDatabaseHelper$TokenData;->wasInitiated:Ljava/lang/Boolean;

    if-eqz v3, :cond_4

    iget-object v3, p1, Lcom/google/android/location/copresence/debug/CopresDatabaseHelper$TokenData;->wasInitiated:Ljava/lang/Boolean;

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    if-eqz v3, :cond_4

    .line 980
    const/4 v3, 0x1

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    iput-object v3, p1, Lcom/google/android/location/copresence/debug/CopresDatabaseHelper$TokenData;->isActiveToken:Ljava/lang/Boolean;

    .line 982
    new-instance v3, Lcom/google/android/location/copresence/debug/CopresDatabaseHelper$TokenData;

    invoke-direct {v3}, Lcom/google/android/location/copresence/debug/CopresDatabaseHelper$TokenData;-><init>()V

    .line 983
    const/4 v4, 0x1

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    iput-object v4, v3, Lcom/google/android/location/copresence/debug/CopresDatabaseHelper$TokenData;->isActiveToken:Ljava/lang/Boolean;

    .line 984
    new-instance v4, Lcom/google/android/location/copresence/debug/CopresDatabaseHelper$TokenData;

    invoke-direct {v4}, Lcom/google/android/location/copresence/debug/CopresDatabaseHelper$TokenData;-><init>()V

    .line 985
    const/4 v5, 0x0

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    iput-object v5, v4, Lcom/google/android/location/copresence/debug/CopresDatabaseHelper$TokenData;->isActiveToken:Ljava/lang/Boolean;

    .line 986
    invoke-virtual {v3, v2, v4}, Lcom/google/android/location/copresence/debug/CopresDatabaseHelper$TokenData;->updateMatching(Landroid/database/sqlite/SQLiteDatabase;Lcom/google/android/location/copresence/debug/a;)I

    .line 993
    :cond_4
    :goto_1
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 995
    invoke-virtual {p1, v2}, Lcom/google/android/location/copresence/debug/CopresDatabaseHelper$TokenData;->save(Landroid/database/sqlite/SQLiteDatabase;)I

    .line 996
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 998
    :try_start_5
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto/16 :goto_0

    .line 990
    :cond_5
    :try_start_6
    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    .line 991
    const/4 v3, 0x0

    invoke-virtual {p1, v0, v3}, Lcom/google/android/location/copresence/debug/CopresDatabaseHelper$TokenData;->updateWithCursor(Landroid/database/Cursor;Z)V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    goto :goto_1

    .line 998
    :catchall_1
    move-exception v0

    :try_start_7
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0
.end method

.method public static declared-synchronized a(Landroid/content/Context;Lcom/google/android/location/copresence/debug/a;)V
    .locals 2

    .prologue
    .line 609
    const-class v1, Lcom/google/android/location/copresence/debug/CopresDatabaseHelper;

    monitor-enter v1

    :try_start_0
    invoke-static {}, Lcom/google/android/location/copresence/f/a;->a()Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_0

    .line 615
    :goto_0
    monitor-exit v1

    return-void

    .line 612
    :cond_0
    :try_start_1
    invoke-static {p0}, Lcom/google/android/location/copresence/debug/CopresDatabaseHelper;->b(Landroid/content/Context;)Lcom/google/android/location/copresence/debug/CopresDatabaseHelper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/location/copresence/debug/CopresDatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 614
    invoke-virtual {p1, v0}, Lcom/google/android/location/copresence/debug/a;->save(Landroid/database/sqlite/SQLiteDatabase;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 609
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static a(Landroid/content/Context;ZZ)V
    .locals 1

    .prologue
    .line 1048
    if-nez p0, :cond_0

    .line 1054
    :goto_0
    return-void

    .line 1051
    :cond_0
    new-instance v0, Lcom/google/android/location/copresence/debug/CopresDatabaseHelper$NotificationPinRecord;

    invoke-direct {v0, p1, p2}, Lcom/google/android/location/copresence/debug/CopresDatabaseHelper$NotificationPinRecord;-><init>(ZZ)V

    .line 1053
    invoke-static {p0, v0}, Lcom/google/android/location/copresence/debug/CopresDatabaseHelper;->a(Landroid/content/Context;Lcom/google/android/location/copresence/debug/a;)V

    goto :goto_0
.end method

.method public static declared-synchronized a(Landroid/content/Context;[Lcom/google/ac/b/c/bf;[Ljava/lang/String;)V
    .locals 12

    .prologue
    const/4 v0, 0x0

    .line 697
    const-class v3, Lcom/google/android/location/copresence/debug/CopresDatabaseHelper;

    monitor-enter v3

    if-nez p0, :cond_1

    .line 757
    :cond_0
    monitor-exit v3

    return-void

    .line 700
    :cond_1
    if-eqz p1, :cond_0

    .line 703
    :try_start_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    .line 705
    array-length v6, p1

    move v2, v0

    :goto_0
    if-ge v2, v6, :cond_0

    aget-object v7, p1, v2

    .line 706
    iget-object v8, v7, Lcom/google/ac/b/c/bf;->c:Lcom/google/ac/b/c/at;

    .line 707
    if-eqz v8, :cond_3

    .line 708
    new-instance v9, Lcom/google/android/location/copresence/debug/CopresDatabaseHelper$PublishedMessageRecord;

    invoke-direct {v9}, Lcom/google/android/location/copresence/debug/CopresDatabaseHelper$PublishedMessageRecord;-><init>()V

    .line 711
    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, v9, Lcom/google/android/location/copresence/debug/CopresDatabaseHelper$PublishedMessageRecord;->mine:Ljava/lang/Boolean;

    .line 712
    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    iput-object v1, v9, Lcom/google/android/location/copresence/debug/CopresDatabaseHelper$PublishedMessageRecord;->sentTimestamp:Ljava/lang/Long;

    .line 713
    const-wide/16 v10, -0x1

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    .line 714
    iget-object v10, v7, Lcom/google/ac/b/c/bf;->b:Lcom/google/ac/b/c/t;

    if-eqz v10, :cond_2

    .line 715
    iget-object v1, v7, Lcom/google/ac/b/c/bf;->b:Lcom/google/ac/b/c/t;

    iget-object v1, v1, Lcom/google/ac/b/c/t;->a:Ljava/lang/Long;

    .line 717
    :cond_2
    iget-object v10, v8, Lcom/google/ac/b/c/at;->a:Lcom/google/ac/b/c/av;

    iget-object v11, v7, Lcom/google/ac/b/c/bf;->d:Ljava/lang/Integer;

    invoke-static {v9, v10, v11, v1}, Lcom/google/android/location/copresence/debug/CopresDatabaseHelper;->a(Lcom/google/android/location/copresence/debug/CopresDatabaseHelper$BaseMessageRecord;Lcom/google/ac/b/c/av;Ljava/lang/Integer;Ljava/lang/Long;)V

    .line 719
    if-eqz p2, :cond_5

    aget-object v1, p2, v0

    if-eqz v1, :cond_5

    .line 720
    aget-object v1, p2, v0

    const-string v10, ""

    invoke-virtual {v1, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 721
    const-string v1, "(empty published id)"

    iput-object v1, v9, Lcom/google/android/location/copresence/debug/CopresDatabaseHelper$PublishedMessageRecord;->publishedId:Ljava/lang/String;

    .line 728
    :goto_1
    iget-object v1, v8, Lcom/google/ac/b/c/at;->b:[B
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v1, :cond_6

    .line 730
    :try_start_1
    new-instance v1, Ljava/lang/String;

    iget-object v10, v8, Lcom/google/ac/b/c/at;->b:[B

    const-string v11, "UTF-8"

    invoke-direct {v1, v10, v11}, Ljava/lang/String;-><init>([BLjava/lang/String;)V

    iput-object v1, v9, Lcom/google/android/location/copresence/debug/CopresDatabaseHelper$PublishedMessageRecord;->payloadReadable:Ljava/lang/String;
    :try_end_1
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 734
    :goto_2
    :try_start_2
    iget-object v1, v8, Lcom/google/ac/b/c/at;->b:[B

    array-length v1, v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, v9, Lcom/google/android/location/copresence/debug/CopresDatabaseHelper$PublishedMessageRecord;->payloadSize:Ljava/lang/Integer;

    .line 739
    :goto_3
    iget-object v1, v7, Lcom/google/ac/b/c/bf;->b:Lcom/google/ac/b/c/t;

    .line 740
    if-eqz v1, :cond_8

    iget-object v7, v1, Lcom/google/ac/b/c/t;->c:Lcom/google/ac/b/c/r;

    if-eqz v7, :cond_8

    .line 741
    iget-object v7, v1, Lcom/google/ac/b/c/t;->b:Lcom/google/ac/b/c/u;

    iget-object v7, v7, Lcom/google/ac/b/c/u;->b:Ljava/lang/String;

    iput-object v7, v9, Lcom/google/android/location/copresence/debug/CopresDatabaseHelper$PublishedMessageRecord;->aclNamespace:Ljava/lang/String;

    .line 742
    iget-object v7, v1, Lcom/google/ac/b/c/t;->b:Lcom/google/ac/b/c/u;

    iget-object v7, v7, Lcom/google/ac/b/c/u;->a:Ljava/lang/Integer;

    iput-object v7, v9, Lcom/google/android/location/copresence/debug/CopresDatabaseHelper$PublishedMessageRecord;->aclType:Ljava/lang/Integer;

    .line 743
    iget-object v7, v1, Lcom/google/ac/b/c/t;->c:Lcom/google/ac/b/c/r;

    if-eqz v7, :cond_7

    iget-object v7, v1, Lcom/google/ac/b/c/t;->c:Lcom/google/ac/b/c/r;

    iget-object v7, v7, Lcom/google/ac/b/c/r;->a:Ljava/lang/String;

    if-eqz v7, :cond_7

    .line 744
    iget-object v1, v1, Lcom/google/ac/b/c/t;->c:Lcom/google/ac/b/c/r;

    iget-object v1, v1, Lcom/google/ac/b/c/r;->a:Ljava/lang/String;

    iput-object v1, v9, Lcom/google/android/location/copresence/debug/CopresDatabaseHelper$PublishedMessageRecord;->accessKey:Ljava/lang/String;

    .line 754
    :goto_4
    invoke-static {p0, v9}, Lcom/google/android/location/copresence/debug/CopresDatabaseHelper;->a(Landroid/content/Context;Lcom/google/android/location/copresence/debug/a;)V

    .line 755
    add-int/lit8 v0, v0, 0x1

    .line 705
    :cond_3
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto/16 :goto_0

    .line 723
    :cond_4
    aget-object v1, p2, v0

    iput-object v1, v9, Lcom/google/android/location/copresence/debug/CopresDatabaseHelper$PublishedMessageRecord;->publishedId:Ljava/lang/String;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    .line 697
    :catchall_0
    move-exception v0

    monitor-exit v3

    throw v0

    .line 726
    :cond_5
    :try_start_3
    const-string v1, "(null published id)"

    iput-object v1, v9, Lcom/google/android/location/copresence/debug/CopresDatabaseHelper$PublishedMessageRecord;->publishedId:Ljava/lang/String;

    goto :goto_1

    .line 732
    :catch_0
    move-exception v1

    const-string v1, "(not a UTF-8 string)"

    iput-object v1, v9, Lcom/google/android/location/copresence/debug/CopresDatabaseHelper$PublishedMessageRecord;->payloadReadable:Ljava/lang/String;

    goto :goto_2

    .line 736
    :cond_6
    const-string v1, "(null payload)"

    iput-object v1, v9, Lcom/google/android/location/copresence/debug/CopresDatabaseHelper$PublishedMessageRecord;->payloadReadable:Ljava/lang/String;

    .line 737
    const/4 v1, -0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, v9, Lcom/google/android/location/copresence/debug/CopresDatabaseHelper$PublishedMessageRecord;->payloadSize:Ljava/lang/Integer;

    goto :goto_3

    .line 746
    :cond_7
    const-string v1, "(null access key)"

    iput-object v1, v9, Lcom/google/android/location/copresence/debug/CopresDatabaseHelper$PublishedMessageRecord;->accessKey:Ljava/lang/String;

    goto :goto_4

    .line 749
    :cond_8
    const-string v1, "(null namespace)"

    iput-object v1, v9, Lcom/google/android/location/copresence/debug/CopresDatabaseHelper$PublishedMessageRecord;->aclNamespace:Ljava/lang/String;

    .line 750
    const/4 v1, -0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, v9, Lcom/google/android/location/copresence/debug/CopresDatabaseHelper$PublishedMessageRecord;->aclType:Ljava/lang/Integer;

    .line 751
    const-string v1, "(null access key)"

    iput-object v1, v9, Lcom/google/android/location/copresence/debug/CopresDatabaseHelper$PublishedMessageRecord;->accessKey:Ljava/lang/String;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_4
.end method

.method public static a(Landroid/content/Context;[Lcom/google/ac/b/c/bm;)V
    .locals 10

    .prologue
    const/4 v9, 0x0

    const/4 v1, 0x0

    .line 857
    invoke-static {}, Lcom/google/android/location/copresence/f/a;->a()Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_1

    .line 930
    :cond_0
    return-void

    .line 860
    :cond_1
    invoke-static {p0}, Lcom/google/android/location/copresence/debug/CopresDatabaseHelper;->b(Landroid/content/Context;)Lcom/google/android/location/copresence/debug/CopresDatabaseHelper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/location/copresence/debug/CopresDatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    .line 861
    new-instance v2, Lcom/google/android/location/copresence/debug/CopresDatabaseHelper$SubscribedMessageRecord;

    invoke-direct {v2}, Lcom/google/android/location/copresence/debug/CopresDatabaseHelper$SubscribedMessageRecord;-><init>()V

    .line 862
    array-length v3, p1

    move v0, v1

    :goto_0
    if-ge v0, v3, :cond_0

    aget-object v4, p1, v0

    .line 863
    if-eqz v4, :cond_2

    .line 864
    iget-object v5, v4, Lcom/google/ac/b/c/bm;->b:Lcom/google/ac/b/c/at;

    .line 867
    if-eqz v5, :cond_2

    .line 868
    iget-object v6, v4, Lcom/google/ac/b/c/bm;->c:Lcom/google/ac/b/c/bn;

    if-eqz v6, :cond_2

    .line 871
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    iput-object v6, v2, Lcom/google/android/location/copresence/debug/CopresDatabaseHelper$SubscribedMessageRecord;->mine:Ljava/lang/Boolean;

    .line 876
    iget-object v6, v4, Lcom/google/ac/b/c/bm;->c:Lcom/google/ac/b/c/bn;

    iget-object v6, v6, Lcom/google/ac/b/c/bn;->d:Ljava/lang/Long;

    iput-object v6, v2, Lcom/google/android/location/copresence/debug/CopresDatabaseHelper$SubscribedMessageRecord;->sentTimestamp:Ljava/lang/Long;

    .line 877
    iget-object v6, v5, Lcom/google/ac/b/c/at;->a:Lcom/google/ac/b/c/av;

    invoke-static {v2, v6, v9, v9}, Lcom/google/android/location/copresence/debug/CopresDatabaseHelper;->a(Lcom/google/android/location/copresence/debug/CopresDatabaseHelper$BaseMessageRecord;Lcom/google/ac/b/c/av;Ljava/lang/Integer;Ljava/lang/Long;)V

    .line 879
    iget-object v6, v5, Lcom/google/ac/b/c/at;->b:[B

    if-eqz v6, :cond_3

    .line 881
    :try_start_0
    new-instance v6, Ljava/lang/String;

    iget-object v7, v5, Lcom/google/ac/b/c/at;->b:[B

    const-string v8, "UTF-8"

    invoke-direct {v6, v7, v8}, Ljava/lang/String;-><init>([BLjava/lang/String;)V

    iput-object v6, v2, Lcom/google/android/location/copresence/debug/CopresDatabaseHelper$SubscribedMessageRecord;->payloadReadable:Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    .line 885
    :goto_1
    iget-object v5, v5, Lcom/google/ac/b/c/at;->b:[B

    array-length v5, v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    iput-object v5, v2, Lcom/google/android/location/copresence/debug/CopresDatabaseHelper$SubscribedMessageRecord;->payloadSize:Ljava/lang/Integer;

    .line 890
    :goto_2
    iget-object v5, v4, Lcom/google/ac/b/c/bm;->a:[Ljava/lang/String;

    if-eqz v5, :cond_4

    .line 891
    const-string v5, ","

    iget-object v6, v4, Lcom/google/ac/b/c/bm;->a:[Ljava/lang/String;

    invoke-static {v5, v6}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v2, Lcom/google/android/location/copresence/debug/CopresDatabaseHelper$SubscribedMessageRecord;->subscriptionIds:Ljava/lang/String;

    .line 895
    :goto_3
    iget-object v5, v4, Lcom/google/ac/b/c/bm;->c:Lcom/google/ac/b/c/bn;

    iget-object v5, v5, Lcom/google/ac/b/c/bn;->b:Ljava/lang/String;

    if-eqz v5, :cond_5

    .line 896
    iget-object v5, v4, Lcom/google/ac/b/c/bm;->c:Lcom/google/ac/b/c/bn;

    iget-object v5, v5, Lcom/google/ac/b/c/bn;->b:Ljava/lang/String;

    iput-object v5, v2, Lcom/google/android/location/copresence/debug/CopresDatabaseHelper$SubscribedMessageRecord;->senderGaiaId:Ljava/lang/String;

    .line 900
    :goto_4
    iget-object v5, v4, Lcom/google/ac/b/c/bm;->c:Lcom/google/ac/b/c/bn;

    iget-object v5, v5, Lcom/google/ac/b/c/bn;->c:Ljava/lang/String;

    if-eqz v5, :cond_6

    .line 901
    iget-object v5, v4, Lcom/google/ac/b/c/bm;->c:Lcom/google/ac/b/c/bn;

    iget-object v5, v5, Lcom/google/ac/b/c/bn;->c:Ljava/lang/String;

    iput-object v5, v2, Lcom/google/android/location/copresence/debug/CopresDatabaseHelper$SubscribedMessageRecord;->senderDeviceId:Ljava/lang/String;

    .line 905
    :goto_5
    iget-object v5, v4, Lcom/google/ac/b/c/bm;->c:Lcom/google/ac/b/c/bn;

    iget-object v5, v5, Lcom/google/ac/b/c/bn;->a:Ljava/lang/String;

    if-eqz v5, :cond_7

    .line 906
    iget-object v5, v4, Lcom/google/ac/b/c/bm;->c:Lcom/google/ac/b/c/bn;

    iget-object v5, v5, Lcom/google/ac/b/c/bn;->a:Ljava/lang/String;

    iput-object v5, v2, Lcom/google/android/location/copresence/debug/CopresDatabaseHelper$SubscribedMessageRecord;->senderMessageId:Ljava/lang/String;

    .line 910
    :goto_6
    iget-object v5, v4, Lcom/google/ac/b/c/bm;->d:Lcom/google/ac/b/c/aj;

    if-eqz v5, :cond_a

    .line 911
    iget-object v5, v4, Lcom/google/ac/b/c/bm;->d:Lcom/google/ac/b/c/aj;

    iget-object v5, v5, Lcom/google/ac/b/c/aj;->a:Ljava/lang/Double;

    if-eqz v5, :cond_8

    .line 912
    iget-object v5, v4, Lcom/google/ac/b/c/bm;->d:Lcom/google/ac/b/c/aj;

    iget-object v5, v5, Lcom/google/ac/b/c/aj;->a:Ljava/lang/Double;

    invoke-virtual {v5}, Ljava/lang/Double;->toString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, v2, Lcom/google/android/location/copresence/debug/CopresDatabaseHelper$SubscribedMessageRecord;->distanceDataDistanceM:Ljava/lang/String;

    .line 917
    :goto_7
    iget-object v5, v4, Lcom/google/ac/b/c/bm;->d:Lcom/google/ac/b/c/aj;

    iget-object v5, v5, Lcom/google/ac/b/c/aj;->b:Ljava/lang/Double;

    if-eqz v5, :cond_9

    .line 918
    iget-object v4, v4, Lcom/google/ac/b/c/bm;->d:Lcom/google/ac/b/c/aj;

    iget-object v4, v4, Lcom/google/ac/b/c/aj;->b:Ljava/lang/Double;

    invoke-virtual {v4}, Ljava/lang/Double;->toString()Ljava/lang/String;

    move-result-object v4

    iput-object v4, v2, Lcom/google/android/location/copresence/debug/CopresDatabaseHelper$SubscribedMessageRecord;->distanceDataAccuracyM:Ljava/lang/String;

    .line 928
    :goto_8
    invoke-static {p0, v2}, Lcom/google/android/location/copresence/debug/CopresDatabaseHelper;->a(Landroid/content/Context;Lcom/google/android/location/copresence/debug/a;)V

    .line 862
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto/16 :goto_0

    .line 883
    :catch_0
    move-exception v6

    const-string v6, "(not a UTF-8 string)"

    iput-object v6, v2, Lcom/google/android/location/copresence/debug/CopresDatabaseHelper$SubscribedMessageRecord;->payloadReadable:Ljava/lang/String;

    goto :goto_1

    .line 887
    :cond_3
    const-string v5, "(null payload)"

    iput-object v5, v2, Lcom/google/android/location/copresence/debug/CopresDatabaseHelper$SubscribedMessageRecord;->payloadReadable:Ljava/lang/String;

    .line 888
    const/4 v5, -0x1

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    iput-object v5, v2, Lcom/google/android/location/copresence/debug/CopresDatabaseHelper$SubscribedMessageRecord;->payloadSize:Ljava/lang/Integer;

    goto :goto_2

    .line 893
    :cond_4
    const-string v5, "(null subscriptions id array)"

    iput-object v5, v2, Lcom/google/android/location/copresence/debug/CopresDatabaseHelper$SubscribedMessageRecord;->subscriptionIds:Ljava/lang/String;

    goto :goto_3

    .line 898
    :cond_5
    const-string v5, "(null sender gaia id)"

    iput-object v5, v2, Lcom/google/android/location/copresence/debug/CopresDatabaseHelper$SubscribedMessageRecord;->senderGaiaId:Ljava/lang/String;

    goto :goto_4

    .line 903
    :cond_6
    const-string v5, "(null device id)"

    iput-object v5, v2, Lcom/google/android/location/copresence/debug/CopresDatabaseHelper$SubscribedMessageRecord;->senderDeviceId:Ljava/lang/String;

    goto :goto_5

    .line 908
    :cond_7
    const-string v5, "(null published id)"

    iput-object v5, v2, Lcom/google/android/location/copresence/debug/CopresDatabaseHelper$SubscribedMessageRecord;->senderMessageId:Ljava/lang/String;

    goto :goto_6

    .line 915
    :cond_8
    const-string v5, "(null distance)"

    iput-object v5, v2, Lcom/google/android/location/copresence/debug/CopresDatabaseHelper$SubscribedMessageRecord;->distanceDataDistanceM:Ljava/lang/String;

    goto :goto_7

    .line 921
    :cond_9
    const-string v4, "(null accuracy)"

    iput-object v4, v2, Lcom/google/android/location/copresence/debug/CopresDatabaseHelper$SubscribedMessageRecord;->distanceDataAccuracyM:Ljava/lang/String;

    goto :goto_8

    .line 924
    :cond_a
    const-string v4, "(null distance)"

    iput-object v4, v2, Lcom/google/android/location/copresence/debug/CopresDatabaseHelper$SubscribedMessageRecord;->distanceDataDistanceM:Ljava/lang/String;

    .line 925
    const-string v4, "(null accuracy)"

    iput-object v4, v2, Lcom/google/android/location/copresence/debug/CopresDatabaseHelper$SubscribedMessageRecord;->distanceDataAccuracyM:Ljava/lang/String;

    goto :goto_8
.end method

.method public static a(Landroid/content/Context;[Lcom/google/ac/b/c/bo;[Ljava/lang/String;)V
    .locals 8

    .prologue
    const/4 v0, 0x0

    .line 804
    new-instance v2, Lcom/google/android/location/copresence/debug/CopresDatabaseHelper$SubscriptionRecord;

    invoke-direct {v2}, Lcom/google/android/location/copresence/debug/CopresDatabaseHelper$SubscriptionRecord;-><init>()V

    .line 806
    if-nez p1, :cond_1

    .line 853
    :cond_0
    return-void

    .line 809
    :cond_1
    if-eqz p2, :cond_0

    .line 812
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    iput-object v1, v2, Lcom/google/android/location/copresence/debug/CopresDatabaseHelper$SubscriptionRecord;->sentTimestamp:Ljava/lang/Long;

    .line 813
    array-length v3, p1

    move v1, v0

    :goto_0
    if-ge v0, v3, :cond_0

    aget-object v4, p1, v0

    .line 814
    if-eqz p2, :cond_2

    array-length v5, p2

    if-ge v1, v5, :cond_2

    aget-object v5, p2, v1

    if-eqz v5, :cond_2

    .line 815
    aget-object v5, p2, v1

    iput-object v5, v2, Lcom/google/android/location/copresence/debug/CopresDatabaseHelper$SubscriptionRecord;->subscriptionId:Ljava/lang/String;

    .line 819
    :goto_1
    const/4 v5, 0x1

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    iput-object v5, v2, Lcom/google/android/location/copresence/debug/CopresDatabaseHelper$SubscriptionRecord;->isActive:Ljava/lang/Boolean;

    .line 820
    iget-object v5, v4, Lcom/google/ac/b/c/bo;->d:Lcom/google/ac/b/c/av;

    if-eqz v5, :cond_5

    .line 821
    iget-object v5, v4, Lcom/google/ac/b/c/bo;->d:Lcom/google/ac/b/c/av;

    iget-object v5, v5, Lcom/google/ac/b/c/av;->a:Ljava/lang/String;

    if-eqz v5, :cond_3

    .line 822
    iget-object v5, v4, Lcom/google/ac/b/c/bo;->d:Lcom/google/ac/b/c/av;

    iget-object v5, v5, Lcom/google/ac/b/c/av;->a:Ljava/lang/String;

    iput-object v5, v2, Lcom/google/android/location/copresence/debug/CopresDatabaseHelper$SubscriptionRecord;->namespace:Ljava/lang/String;

    .line 826
    :goto_2
    iget-object v5, v4, Lcom/google/ac/b/c/bo;->d:Lcom/google/ac/b/c/av;

    iget-object v5, v5, Lcom/google/ac/b/c/av;->b:Ljava/lang/String;

    if-eqz v5, :cond_4

    .line 827
    iget-object v5, v4, Lcom/google/ac/b/c/bo;->d:Lcom/google/ac/b/c/av;

    iget-object v5, v5, Lcom/google/ac/b/c/av;->b:Ljava/lang/String;

    iput-object v5, v2, Lcom/google/android/location/copresence/debug/CopresDatabaseHelper$SubscriptionRecord;->type:Ljava/lang/String;

    .line 835
    :goto_3
    iget-object v5, v4, Lcom/google/ac/b/c/bo;->b:Ljava/lang/Integer;

    if-eqz v5, :cond_6

    .line 836
    iget-object v5, v4, Lcom/google/ac/b/c/bo;->b:Ljava/lang/Integer;

    iput-object v5, v2, Lcom/google/android/location/copresence/debug/CopresDatabaseHelper$SubscriptionRecord;->strategy:Ljava/lang/Integer;

    .line 840
    :goto_4
    iget-object v5, v4, Lcom/google/ac/b/c/bo;->c:Ljava/lang/Long;

    if-eqz v5, :cond_7

    .line 841
    iget-object v5, v4, Lcom/google/ac/b/c/bo;->c:Ljava/lang/Long;

    iput-object v5, v2, Lcom/google/android/location/copresence/debug/CopresDatabaseHelper$SubscriptionRecord;->ttlMillis:Ljava/lang/Long;

    .line 845
    :goto_5
    iget-object v5, v4, Lcom/google/ac/b/c/bo;->e:Lcom/google/ac/b/c/r;

    if-eqz v5, :cond_8

    iget-object v5, v4, Lcom/google/ac/b/c/bo;->e:Lcom/google/ac/b/c/r;

    iget-object v5, v5, Lcom/google/ac/b/c/r;->a:Ljava/lang/String;

    if-eqz v5, :cond_8

    .line 846
    iget-object v4, v4, Lcom/google/ac/b/c/bo;->e:Lcom/google/ac/b/c/r;

    iget-object v4, v4, Lcom/google/ac/b/c/r;->a:Ljava/lang/String;

    iput-object v4, v2, Lcom/google/android/location/copresence/debug/CopresDatabaseHelper$SubscriptionRecord;->accessKey:Ljava/lang/String;

    .line 850
    :goto_6
    add-int/lit8 v1, v1, 0x1

    .line 851
    invoke-static {p0, v2}, Lcom/google/android/location/copresence/debug/CopresDatabaseHelper;->a(Landroid/content/Context;Lcom/google/android/location/copresence/debug/a;)V

    .line 813
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 817
    :cond_2
    const-string v5, "(null subscription id)"

    iput-object v5, v2, Lcom/google/android/location/copresence/debug/CopresDatabaseHelper$SubscriptionRecord;->subscriptionId:Ljava/lang/String;

    goto :goto_1

    .line 824
    :cond_3
    const-string v5, "(null namespace)"

    iput-object v5, v2, Lcom/google/android/location/copresence/debug/CopresDatabaseHelper$SubscriptionRecord;->namespace:Ljava/lang/String;

    goto :goto_2

    .line 829
    :cond_4
    const-string v5, "(null type)"

    iput-object v5, v2, Lcom/google/android/location/copresence/debug/CopresDatabaseHelper$SubscriptionRecord;->type:Ljava/lang/String;

    goto :goto_3

    .line 832
    :cond_5
    const-string v5, "(null namespace)"

    iput-object v5, v2, Lcom/google/android/location/copresence/debug/CopresDatabaseHelper$SubscriptionRecord;->namespace:Ljava/lang/String;

    .line 833
    const-string v5, "(null type)"

    iput-object v5, v2, Lcom/google/android/location/copresence/debug/CopresDatabaseHelper$SubscriptionRecord;->type:Ljava/lang/String;

    goto :goto_3

    .line 838
    :cond_6
    const/4 v5, -0x1

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    iput-object v5, v2, Lcom/google/android/location/copresence/debug/CopresDatabaseHelper$SubscriptionRecord;->strategy:Ljava/lang/Integer;

    goto :goto_4

    .line 843
    :cond_7
    const-wide/16 v6, -0x1

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    iput-object v5, v2, Lcom/google/android/location/copresence/debug/CopresDatabaseHelper$SubscriptionRecord;->ttlMillis:Ljava/lang/Long;

    goto :goto_5

    .line 848
    :cond_8
    const-string v4, "(null access key)"

    iput-object v4, v2, Lcom/google/android/location/copresence/debug/CopresDatabaseHelper$SubscriptionRecord;->accessKey:Ljava/lang/String;

    goto :goto_6
.end method

.method public static declared-synchronized a(Landroid/content/Context;[Ljava/lang/String;)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 933
    const-class v1, Lcom/google/android/location/copresence/debug/CopresDatabaseHelper;

    monitor-enter v1

    :try_start_0
    invoke-static {}, Lcom/google/android/location/copresence/f/a;->a()Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v2

    if-nez v2, :cond_1

    .line 954
    :cond_0
    monitor-exit v1

    return-void

    .line 936
    :cond_1
    :try_start_1
    invoke-static {p0}, Lcom/google/android/location/copresence/debug/CopresDatabaseHelper;->b(Landroid/content/Context;)Lcom/google/android/location/copresence/debug/CopresDatabaseHelper;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/location/copresence/debug/CopresDatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    .line 938
    new-instance v3, Lcom/google/android/location/copresence/debug/CopresDatabaseHelper$SubscriptionRecord;

    invoke-direct {v3}, Lcom/google/android/location/copresence/debug/CopresDatabaseHelper$SubscriptionRecord;-><init>()V

    .line 939
    const/4 v4, 0x0

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    iput-object v4, v3, Lcom/google/android/location/copresence/debug/CopresDatabaseHelper$SubscriptionRecord;->isActive:Ljava/lang/Boolean;

    .line 940
    new-instance v3, Lcom/google/android/location/copresence/debug/CopresDatabaseHelper$PublishedMessageRecord;

    invoke-direct {v3}, Lcom/google/android/location/copresence/debug/CopresDatabaseHelper$PublishedMessageRecord;-><init>()V

    .line 947
    const/4 v4, 0x0

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    iput-object v4, v3, Lcom/google/android/location/copresence/debug/CopresDatabaseHelper$PublishedMessageRecord;->isActive:Ljava/lang/Boolean;

    .line 948
    if-eqz p1, :cond_0

    .line 949
    array-length v4, p1

    :goto_0
    if-ge v0, v4, :cond_0

    aget-object v5, p1, v0

    .line 950
    iput-object v5, v3, Lcom/google/android/location/copresence/debug/CopresDatabaseHelper$PublishedMessageRecord;->publishedId:Ljava/lang/String;

    .line 951
    invoke-virtual {v3, v2, v3}, Lcom/google/android/location/copresence/debug/CopresDatabaseHelper$PublishedMessageRecord;->updateMatching(Landroid/database/sqlite/SQLiteDatabase;Lcom/google/android/location/copresence/debug/a;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 949
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 933
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private static declared-synchronized a(Lcom/google/android/location/copresence/debug/CopresDatabaseHelper$BaseMessageRecord;Lcom/google/ac/b/c/av;Ljava/lang/Integer;Ljava/lang/Long;)V
    .locals 4

    .prologue
    .line 674
    const-class v1, Lcom/google/android/location/copresence/debug/CopresDatabaseHelper;

    monitor-enter v1

    if-eqz p1, :cond_0

    .line 675
    :try_start_0
    iget-object v0, p1, Lcom/google/ac/b/c/av;->a:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/location/copresence/debug/CopresDatabaseHelper$BaseMessageRecord;->namespace:Ljava/lang/String;

    .line 676
    iget-object v0, p1, Lcom/google/ac/b/c/av;->b:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/location/copresence/debug/CopresDatabaseHelper$BaseMessageRecord;->type:Ljava/lang/String;

    .line 680
    :goto_0
    if-eqz p2, :cond_1

    .line 681
    iput-object p2, p0, Lcom/google/android/location/copresence/debug/CopresDatabaseHelper$BaseMessageRecord;->strategy:Ljava/lang/Integer;

    .line 685
    :goto_1
    if-eqz p3, :cond_2

    .line 686
    iput-object p3, p0, Lcom/google/android/location/copresence/debug/CopresDatabaseHelper$BaseMessageRecord;->ttlMillis:Ljava/lang/Long;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 690
    :goto_2
    monitor-exit v1

    return-void

    .line 678
    :cond_0
    :try_start_1
    const-string v0, "(null namespace)"

    iput-object v0, p0, Lcom/google/android/location/copresence/debug/CopresDatabaseHelper$BaseMessageRecord;->namespace:Ljava/lang/String;

    .line 679
    const-string v0, "(null type)"

    iput-object v0, p0, Lcom/google/android/location/copresence/debug/CopresDatabaseHelper$BaseMessageRecord;->type:Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 674
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 683
    :cond_1
    const/4 v0, -0x1

    :try_start_2
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/copresence/debug/CopresDatabaseHelper$BaseMessageRecord;->strategy:Ljava/lang/Integer;

    goto :goto_1

    .line 688
    :cond_2
    const-wide/16 v2, -0x1

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/copresence/debug/CopresDatabaseHelper$BaseMessageRecord;->ttlMillis:Ljava/lang/Long;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_2
.end method

.method private static declared-synchronized b(Landroid/content/Context;)Lcom/google/android/location/copresence/debug/CopresDatabaseHelper;
    .locals 3

    .prologue
    .line 587
    const-class v1, Lcom/google/android/location/copresence/debug/CopresDatabaseHelper;

    monitor-enter v1

    if-nez p0, :cond_0

    .line 588
    :try_start_0
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v2, "Context is null"

    invoke-direct {v0, v2}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 587
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 591
    :cond_0
    :try_start_1
    sget-object v0, Lcom/google/android/location/copresence/debug/CopresDatabaseHelper;->a:Lcom/google/android/location/copresence/debug/CopresDatabaseHelper;

    if-nez v0, :cond_1

    .line 592
    new-instance v0, Lcom/google/android/location/copresence/debug/CopresDatabaseHelper;

    invoke-direct {v0, p0}, Lcom/google/android/location/copresence/debug/CopresDatabaseHelper;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/google/android/location/copresence/debug/CopresDatabaseHelper;->a:Lcom/google/android/location/copresence/debug/CopresDatabaseHelper;

    .line 595
    :cond_1
    sget-object v0, Lcom/google/android/location/copresence/debug/CopresDatabaseHelper;->a:Lcom/google/android/location/copresence/debug/CopresDatabaseHelper;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit v1

    return-object v0
.end method


# virtual methods
.method public final onCreate(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1

    .prologue
    .line 522
    new-instance v0, Lcom/google/android/location/copresence/debug/CopresDatabaseHelper$GcoreStartupRecord;

    invoke-direct {v0}, Lcom/google/android/location/copresence/debug/CopresDatabaseHelper$GcoreStartupRecord;-><init>()V

    invoke-virtual {v0, p1}, Lcom/google/android/location/copresence/debug/CopresDatabaseHelper$GcoreStartupRecord;->createTable(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 523
    new-instance v0, Lcom/google/android/location/copresence/debug/CopresDatabaseHelper$HttpRecord;

    invoke-direct {v0}, Lcom/google/android/location/copresence/debug/CopresDatabaseHelper$HttpRecord;-><init>()V

    invoke-virtual {v0, p1}, Lcom/google/android/location/copresence/debug/CopresDatabaseHelper$HttpRecord;->createTable(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 524
    new-instance v0, Lcom/google/android/location/copresence/debug/CopresDatabaseHelper$TokenToPerson;

    invoke-direct {v0}, Lcom/google/android/location/copresence/debug/CopresDatabaseHelper$TokenToPerson;-><init>()V

    invoke-virtual {v0, p1}, Lcom/google/android/location/copresence/debug/CopresDatabaseHelper$TokenToPerson;->createTable(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 525
    new-instance v0, Lcom/google/android/location/copresence/debug/CopresDatabaseHelper$TokenData;

    invoke-direct {v0}, Lcom/google/android/location/copresence/debug/CopresDatabaseHelper$TokenData;-><init>()V

    invoke-virtual {v0, p1}, Lcom/google/android/location/copresence/debug/CopresDatabaseHelper$TokenData;->createTable(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 526
    const-string v0, "CREATE TABLE IF NOT EXISTS people_data (_id INTEGER PRIMARY KEY AUTOINCREMENT, person_gaia_id TEXT UNIQUE NOT NULL, display_name TEXT)"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 527
    new-instance v0, Lcom/google/android/location/copresence/debug/CopresDatabaseHelper$GcmPingRecord;

    invoke-direct {v0}, Lcom/google/android/location/copresence/debug/CopresDatabaseHelper$GcmPingRecord;-><init>()V

    invoke-virtual {v0, p1}, Lcom/google/android/location/copresence/debug/CopresDatabaseHelper$GcmPingRecord;->createTable(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 528
    new-instance v0, Lcom/google/android/location/copresence/debug/CopresDatabaseHelper$StateTransitionRecord;

    invoke-direct {v0}, Lcom/google/android/location/copresence/debug/CopresDatabaseHelper$StateTransitionRecord;-><init>()V

    invoke-virtual {v0, p1}, Lcom/google/android/location/copresence/debug/CopresDatabaseHelper$StateTransitionRecord;->createTable(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 529
    new-instance v0, Lcom/google/android/location/copresence/debug/CopresDatabaseHelper$BluetoothAdapterMethodCallRecord;

    invoke-direct {v0}, Lcom/google/android/location/copresence/debug/CopresDatabaseHelper$BluetoothAdapterMethodCallRecord;-><init>()V

    invoke-virtual {v0, p1}, Lcom/google/android/location/copresence/debug/CopresDatabaseHelper$BluetoothAdapterMethodCallRecord;->createTable(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 530
    new-instance v0, Lcom/google/android/location/copresence/debug/CopresDatabaseHelper$PublishedMessageRecord;

    invoke-direct {v0}, Lcom/google/android/location/copresence/debug/CopresDatabaseHelper$PublishedMessageRecord;-><init>()V

    invoke-virtual {v0, p1}, Lcom/google/android/location/copresence/debug/CopresDatabaseHelper$PublishedMessageRecord;->createTable(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 531
    new-instance v0, Lcom/google/android/location/copresence/debug/CopresDatabaseHelper$SubscribedMessageRecord;

    invoke-direct {v0}, Lcom/google/android/location/copresence/debug/CopresDatabaseHelper$SubscribedMessageRecord;-><init>()V

    invoke-virtual {v0, p1}, Lcom/google/android/location/copresence/debug/CopresDatabaseHelper$SubscribedMessageRecord;->createTable(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 532
    new-instance v0, Lcom/google/android/location/copresence/debug/CopresDatabaseHelper$SubscriptionRecord;

    invoke-direct {v0}, Lcom/google/android/location/copresence/debug/CopresDatabaseHelper$SubscriptionRecord;-><init>()V

    invoke-virtual {v0, p1}, Lcom/google/android/location/copresence/debug/CopresDatabaseHelper$SubscriptionRecord;->createTable(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 533
    new-instance v0, Lcom/google/android/location/copresence/debug/CopresDatabaseHelper$DirectiveRecord;

    invoke-direct {v0}, Lcom/google/android/location/copresence/debug/CopresDatabaseHelper$DirectiveRecord;-><init>()V

    invoke-virtual {v0, p1}, Lcom/google/android/location/copresence/debug/CopresDatabaseHelper$DirectiveRecord;->createTable(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 534
    new-instance v0, Lcom/google/android/location/copresence/debug/CopresDatabaseHelper$NotificationPinRecord;

    invoke-direct {v0}, Lcom/google/android/location/copresence/debug/CopresDatabaseHelper$NotificationPinRecord;-><init>()V

    invoke-virtual {v0, p1}, Lcom/google/android/location/copresence/debug/CopresDatabaseHelper$NotificationPinRecord;->createTable(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 537
    return-void
.end method

.method public final onDowngrade(Landroid/database/sqlite/SQLiteDatabase;II)V
    .locals 0

    .prologue
    .line 566
    invoke-virtual {p0, p1, p2, p3}, Lcom/google/android/location/copresence/debug/CopresDatabaseHelper;->onUpgrade(Landroid/database/sqlite/SQLiteDatabase;II)V

    .line 567
    return-void
.end method

.method public final onUpgrade(Landroid/database/sqlite/SQLiteDatabase;II)V
    .locals 4

    .prologue
    .line 544
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 545
    const-string v1, "SELECT * FROM sqlite_master WHERE type=\'table\';"

    const/4 v2, 0x0

    invoke-virtual {p1, v1, v2}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 546
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 547
    :goto_0
    invoke-interface {v1}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v2

    if-nez v2, :cond_1

    .line 548
    const/4 v2, 0x1

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 549
    const-string v3, "android_metadata"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    const-string v3, "sqlite_sequence"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 551
    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 552
    :cond_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    goto :goto_0

    .line 555
    :cond_1
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 557
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 558
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "DROP TABLE IF EXISTS "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    goto :goto_1

    .line 561
    :cond_2
    invoke-virtual {p0, p1}, Lcom/google/android/location/copresence/debug/CopresDatabaseHelper;->onCreate(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 562
    return-void
.end method
