.class public Lcom/google/android/location/copresence/debug/CopresDatabaseHelper$NotificationPinRecord;
.super Lcom/google/android/location/copresence/debug/a;
.source "SourceFile"


# instance fields
.field public isBootUp:Ljava/lang/Boolean;
    .annotation runtime Lcom/google/android/location/copresence/debug/b;
        a = false
    .end annotation
.end field

.field public isEnabled:Ljava/lang/Boolean;
    .annotation runtime Lcom/google/android/location/copresence/debug/b;
        a = false
    .end annotation
.end field

.field public timestamp:Ljava/lang/Long;
    .annotation runtime Lcom/google/android/location/copresence/debug/b;
        a = true
        d = 0x2
    .end annotation
.end field


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 502
    invoke-direct {p0}, Lcom/google/android/location/copresence/debug/a;-><init>()V

    .line 503
    return-void
.end method

.method public constructor <init>(ZZ)V
    .locals 2

    .prologue
    .line 506
    invoke-direct {p0}, Lcom/google/android/location/copresence/debug/CopresDatabaseHelper$NotificationPinRecord;-><init>()V

    .line 508
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/copresence/debug/CopresDatabaseHelper$NotificationPinRecord;->timestamp:Ljava/lang/Long;

    .line 509
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/copresence/debug/CopresDatabaseHelper$NotificationPinRecord;->isEnabled:Ljava/lang/Boolean;

    .line 512
    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/copresence/debug/CopresDatabaseHelper$NotificationPinRecord;->isBootUp:Ljava/lang/Boolean;

    .line 513
    return-void
.end method
