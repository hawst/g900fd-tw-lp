.class final Lcom/google/android/location/copresence/r/ab;
.super Lcom/google/android/location/copresence/r/ak;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/location/copresence/r/z;


# direct methods
.method constructor <init>(Lcom/google/android/location/copresence/r/z;)V
    .locals 0

    .prologue
    .line 225
    iput-object p1, p0, Lcom/google/android/location/copresence/r/ab;->a:Lcom/google/android/location/copresence/r/z;

    invoke-direct {p0}, Lcom/google/android/location/copresence/r/ak;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/net/wifi/p2p/WifiP2pDevice;)V
    .locals 2

    .prologue
    .line 228
    const/4 v0, 0x3

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 229
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "WifiDirectStates2: TransientGroupCreatedState wifi p2p device changed: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->b(Ljava/lang/String;)I

    .line 232
    :cond_0
    return-void
.end method

.method public final a(Landroid/net/wifi/p2p/WifiP2pInfo;)V
    .locals 2

    .prologue
    .line 236
    const/4 v0, 0x3

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 237
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "WifiDirectStates2: TransientGroupCreatedState wifi p2p info changed: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->b(Ljava/lang/String;)I

    .line 239
    :cond_0
    iget-object v0, p0, Lcom/google/android/location/copresence/r/ab;->a:Lcom/google/android/location/copresence/r/z;

    iget-object v0, v0, Lcom/google/android/location/copresence/r/z;->c:Lcom/google/android/location/copresence/r/p;

    invoke-static {v0}, Lcom/google/android/location/copresence/r/p;->b(Lcom/google/android/location/copresence/r/p;)Lcom/google/android/location/copresence/r/ah;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/location/copresence/r/ab;->a:Lcom/google/android/location/copresence/r/z;

    iget-object v1, v1, Lcom/google/android/location/copresence/r/z;->a:Landroid/net/wifi/p2p/WifiP2pManager$GroupInfoListener;

    invoke-virtual {v0, v1}, Lcom/google/android/location/copresence/r/ah;->a(Landroid/net/wifi/p2p/WifiP2pManager$GroupInfoListener;)V

    .line 240
    return-void
.end method
