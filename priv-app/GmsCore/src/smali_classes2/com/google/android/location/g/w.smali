.class public final Lcom/google/android/location/g/w;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/location/g/s;


# instance fields
.field public final a:Lcom/google/android/location/b/ad;

.field final b:Lcom/google/android/location/ao;

.field c:Z

.field d:Lcom/google/android/location/e/ae;

.field public e:Z

.field f:Z

.field private final g:Lcom/google/android/location/j/b;

.field private final h:Lcom/google/android/location/j/f;

.field private final i:Lcom/google/android/location/g/t;


# direct methods
.method public constructor <init>(Lcom/google/android/location/j/f;Lcom/google/android/location/b/ad;Lcom/google/android/location/j/b;Lcom/google/android/location/ao;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 80
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 59
    new-instance v0, Lcom/google/android/location/g/t;

    invoke-direct {v0}, Lcom/google/android/location/g/t;-><init>()V

    iput-object v0, p0, Lcom/google/android/location/g/w;->i:Lcom/google/android/location/g/t;

    .line 64
    iput-boolean v1, p0, Lcom/google/android/location/g/w;->c:Z

    .line 65
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/location/g/w;->d:Lcom/google/android/location/e/ae;

    .line 67
    iput-boolean v1, p0, Lcom/google/android/location/g/w;->e:Z

    .line 69
    iput-boolean v1, p0, Lcom/google/android/location/g/w;->f:Z

    .line 81
    iput-object p1, p0, Lcom/google/android/location/g/w;->h:Lcom/google/android/location/j/f;

    .line 82
    iput-object p2, p0, Lcom/google/android/location/g/w;->a:Lcom/google/android/location/b/ad;

    .line 83
    iput-object p3, p0, Lcom/google/android/location/g/w;->g:Lcom/google/android/location/j/b;

    .line 84
    iput-object p4, p0, Lcom/google/android/location/g/w;->b:Lcom/google/android/location/ao;

    .line 85
    return-void
.end method

.method private declared-synchronized a(Lcom/google/android/location/e/ae;)V
    .locals 3

    .prologue
    .line 112
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/location/g/w;->d:Lcom/google/android/location/e/ae;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/location/g/w;->d:Lcom/google/android/location/e/ae;

    invoke-virtual {v0, p1}, Lcom/google/android/location/e/ae;->equals(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v0, :cond_1

    .line 118
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 116
    :cond_1
    :try_start_1
    sget-boolean v0, Lcom/google/android/location/i/a;->c:Z

    if-eqz v0, :cond_2

    const-string v0, "ModelStateManager"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Queue "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/google/android/location/e/ae;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/location/o/a/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 117
    :cond_2
    iget-object v0, p0, Lcom/google/android/location/g/w;->i:Lcom/google/android/location/g/t;

    sget-object v1, Lcom/google/android/location/g/u;->a:[I

    iget-object v2, p1, Lcom/google/android/location/e/ae;->a:Lcom/google/android/location/e/af;

    invoke-virtual {v2}, Lcom/google/android/location/e/af;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    sget-boolean v0, Lcom/google/android/location/i/a;->d:Z

    if-eqz v0, :cond_0

    const-string v0, "ModelRequestQueue"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Programming error: unknown indoor request type: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p1, Lcom/google/android/location/e/ae;->a:Lcom/google/android/location/e/af;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/location/o/a/a;->c(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 112
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 117
    :pswitch_0
    :try_start_2
    iget-object v0, v0, Lcom/google/android/location/g/t;->a:Lcom/google/android/location/g/v;

    invoke-virtual {v0, p1}, Lcom/google/android/location/g/v;->a(Ljava/lang/Object;)V

    goto :goto_0

    :pswitch_1
    iget-object v0, v0, Lcom/google/android/location/g/t;->b:Lcom/google/android/location/g/v;

    invoke-virtual {v0, p1}, Lcom/google/android/location/g/v;->a(Ljava/lang/Object;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Lcom/google/android/location/e/t;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 184
    iget-object v0, p0, Lcom/google/android/location/g/w;->a:Lcom/google/android/location/b/ad;

    invoke-virtual {v0, p1}, Lcom/google/android/location/b/ad;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 185
    iget-object v0, p0, Lcom/google/android/location/g/w;->a:Lcom/google/android/location/b/ad;

    iget-object v2, p0, Lcom/google/android/location/g/w;->g:Lcom/google/android/location/j/b;

    invoke-interface {v2}, Lcom/google/android/location/j/b;->b()J

    move-result-wide v2

    invoke-virtual {v0, p1, v2, v3}, Lcom/google/android/location/b/ad;->a(Ljava/lang/String;J)Lcom/google/android/location/e/t;

    move-result-object v0

    .line 187
    if-eqz v0, :cond_0

    .line 199
    :goto_0
    return-object v0

    :cond_0
    move-object v0, v1

    .line 192
    goto :goto_0

    .line 194
    :cond_1
    iget-boolean v0, p0, Lcom/google/android/location/g/w;->e:Z

    if-eqz v0, :cond_2

    .line 196
    invoke-static {p1}, Lcom/google/android/location/e/ae;->a(Ljava/lang/String;)Lcom/google/android/location/e/ae;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/location/g/w;->a(Lcom/google/android/location/e/ae;)V

    move-object v0, v1

    .line 197
    goto :goto_0

    :cond_2
    move-object v0, v1

    .line 199
    goto :goto_0
.end method

.method public final a(Ljava/util/Set;)Ljava/util/List;
    .locals 8

    .prologue
    .line 168
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 169
    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    .line 170
    iget-object v3, p0, Lcom/google/android/location/g/w;->a:Lcom/google/android/location/b/ad;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    iget-object v0, p0, Lcom/google/android/location/g/w;->g:Lcom/google/android/location/j/b;

    invoke-interface {v0}, Lcom/google/android/location/j/b;->b()J

    move-result-wide v6

    invoke-virtual {v3, v4, v5, v6, v7}, Lcom/google/android/location/b/ad;->a(JJ)Ljava/lang/String;

    move-result-object v0

    .line 171
    if-eqz v0, :cond_0

    .line 172
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 175
    :cond_1
    return-object v1
.end method

.method public final a()V
    .locals 1

    .prologue
    .line 283
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/location/g/w;->f:Z

    .line 284
    return-void
.end method

.method public final declared-synchronized a(Lcom/google/android/location/o/n;)V
    .locals 9

    .prologue
    const/4 v1, 0x2

    const/4 v0, 0x0

    const/4 v2, 0x1

    .line 127
    monitor-enter p0

    :try_start_0
    iget-boolean v3, p0, Lcom/google/android/location/g/w;->c:Z

    if-nez v3, :cond_6

    .line 128
    iget-object v3, p0, Lcom/google/android/location/g/w;->i:Lcom/google/android/location/g/t;

    iget-object v4, v3, Lcom/google/android/location/g/t;->b:Lcom/google/android/location/g/v;

    invoke-virtual {v4}, Lcom/google/android/location/g/v;->b()Z

    move-result v4

    if-eqz v4, :cond_0

    iget-object v3, v3, Lcom/google/android/location/g/t;->a:Lcom/google/android/location/g/v;

    invoke-virtual {v3}, Lcom/google/android/location/g/v;->b()Z

    move-result v3

    if-nez v3, :cond_1

    :cond_0
    move v0, v2

    :cond_1
    if-eqz v0, :cond_6

    .line 129
    sget-boolean v0, Lcom/google/android/location/i/a;->c:Z

    if-eqz v0, :cond_2

    const-string v0, "ModelStateManager"

    const-string v2, "Sending GLS model request"

    invoke-static {v0, v2}, Lcom/google/android/location/o/a/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 130
    :cond_2
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/location/g/w;->c:Z

    .line 131
    iget-object v0, p0, Lcom/google/android/location/g/w;->i:Lcom/google/android/location/g/t;

    iget-object v2, v0, Lcom/google/android/location/g/t;->b:Lcom/google/android/location/g/v;

    invoke-virtual {v2}, Lcom/google/android/location/g/v;->b()Z

    move-result v2

    if-nez v2, :cond_3

    iget-object v0, v0, Lcom/google/android/location/g/t;->b:Lcom/google/android/location/g/v;

    invoke-virtual {v0}, Lcom/google/android/location/g/v;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/e/ae;

    :goto_0
    iput-object v0, p0, Lcom/google/android/location/g/w;->d:Lcom/google/android/location/e/ae;

    .line 133
    new-instance v2, Lcom/google/android/location/g/x;

    invoke-direct {v2, p0}, Lcom/google/android/location/g/x;-><init>(Lcom/google/android/location/g/w;)V

    .line 155
    iget-object v3, p0, Lcom/google/android/location/g/w;->h:Lcom/google/android/location/j/f;

    iget-object v4, p0, Lcom/google/android/location/g/w;->d:Lcom/google/android/location/e/ae;

    new-instance v5, Lcom/google/p/a/b/b/a;

    sget-object v0, Lcom/google/android/location/m/a;->I:Lcom/google/p/a/b/b/c;

    invoke-direct {v5, v0}, Lcom/google/p/a/b/b/a;-><init>(Lcom/google/p/a/b/b/c;)V

    const/4 v6, 0x1

    iget-object v0, v4, Lcom/google/android/location/e/ae;->a:Lcom/google/android/location/e/af;

    sget-object v7, Lcom/google/android/location/g/y;->a:[I

    invoke-virtual {v0}, Lcom/google/android/location/e/af;->ordinal()I

    move-result v8

    aget v7, v7, v8

    packed-switch v7, :pswitch_data_0

    new-instance v1, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Programming error: unsupported model type: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 127
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 131
    :cond_3
    :try_start_1
    iget-object v2, v0, Lcom/google/android/location/g/t;->a:Lcom/google/android/location/g/v;

    invoke-virtual {v2}, Lcom/google/android/location/g/v;->b()Z

    move-result v2

    if-nez v2, :cond_4

    iget-object v0, v0, Lcom/google/android/location/g/t;->a:Lcom/google/android/location/g/v;

    invoke-virtual {v0}, Lcom/google/android/location/g/v;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/e/ae;

    goto :goto_0

    :cond_4
    sget-boolean v0, Lcom/google/android/location/i/a;->d:Z

    if-eqz v0, :cond_5

    const-string v0, "ModelRequestQueue"

    const-string v2, "Programming error: trying to get next request when there\'s none"

    invoke-static {v0, v2}, Lcom/google/android/location/o/a/a;->c(Ljava/lang/String;Ljava/lang/String;)V

    :cond_5
    const/4 v0, 0x0

    goto :goto_0

    .line 155
    :pswitch_0
    const/4 v0, 0x3

    :goto_1
    invoke-virtual {v5, v6, v0}, Lcom/google/p/a/b/b/a;->f(II)Lcom/google/p/a/b/b/a;

    const/4 v0, 0x2

    iget-object v1, v4, Lcom/google/android/location/e/ae;->b:Ljava/lang/String;

    invoke-virtual {v5, v0, v1}, Lcom/google/p/a/b/b/a;->a(ILjava/lang/String;)Lcom/google/p/a/b/b/a;

    new-instance v0, Lcom/google/p/a/b/b/a;

    sget-object v1, Lcom/google/android/location/m/a;->F:Lcom/google/p/a/b/b/c;

    invoke-direct {v0, v1}, Lcom/google/p/a/b/b/a;-><init>(Lcom/google/p/a/b/b/c;)V

    const/16 v1, 0xc

    invoke-virtual {v0, v1, v5}, Lcom/google/p/a/b/b/a;->a(ILcom/google/p/a/b/b/a;)V

    const/16 v1, 0xa

    const/4 v4, 0x0

    invoke-virtual {v0, v1, v4}, Lcom/google/p/a/b/b/a;->f(II)Lcom/google/p/a/b/b/a;

    new-instance v1, Lcom/google/p/a/b/b/a;

    sget-object v4, Lcom/google/android/location/m/a;->Q:Lcom/google/p/a/b/b/c;

    invoke-direct {v1, v4}, Lcom/google/p/a/b/b/a;-><init>(Lcom/google/p/a/b/b/c;)V

    const/4 v4, 0x4

    invoke-virtual {v1, v4, v0}, Lcom/google/p/a/b/b/a;->a(ILcom/google/p/a/b/b/a;)V

    invoke-interface {v3, v1, p1, v2}, Lcom/google/android/location/j/f;->b(Lcom/google/p/a/b/b/a;Lcom/google/android/location/o/n;Lcom/google/android/location/j/g;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 158
    :cond_6
    monitor-exit p0

    return-void

    :pswitch_1
    move v0, v1

    .line 155
    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final b(Ljava/lang/String;)Lcom/google/android/location/e/q;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 209
    iget-object v0, p0, Lcom/google/android/location/g/w;->a:Lcom/google/android/location/b/ad;

    iget-object v0, v0, Lcom/google/android/location/b/ad;->c:Lcom/google/android/location/b/f;

    invoke-virtual {v0, p1}, Lcom/google/android/location/b/f;->a(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 210
    iget-object v0, p0, Lcom/google/android/location/g/w;->a:Lcom/google/android/location/b/ad;

    iget-object v2, p0, Lcom/google/android/location/g/w;->g:Lcom/google/android/location/j/b;

    invoke-interface {v2}, Lcom/google/android/location/j/b;->b()J

    move-result-wide v2

    iget-object v0, v0, Lcom/google/android/location/b/ad;->c:Lcom/google/android/location/b/f;

    invoke-virtual {v0, p1, v2, v3}, Lcom/google/android/location/b/f;->a(Ljava/lang/Object;J)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/b/ae;

    if-nez v0, :cond_0

    move-object v0, v1

    .line 211
    :goto_0
    if-eqz v0, :cond_1

    .line 223
    :goto_1
    return-object v0

    .line 210
    :cond_0
    iget-object v0, v0, Lcom/google/android/location/b/ae;->a:Lcom/google/android/location/e/q;

    goto :goto_0

    :cond_1
    move-object v0, v1

    .line 216
    goto :goto_1

    .line 218
    :cond_2
    iget-boolean v0, p0, Lcom/google/android/location/g/w;->e:Z

    if-eqz v0, :cond_3

    .line 220
    new-instance v0, Lcom/google/android/location/e/ae;

    sget-object v2, Lcom/google/android/location/e/af;->b:Lcom/google/android/location/e/af;

    invoke-direct {v0, v2, p1}, Lcom/google/android/location/e/ae;-><init>(Lcom/google/android/location/e/af;Ljava/lang/String;)V

    invoke-direct {p0, v0}, Lcom/google/android/location/g/w;->a(Lcom/google/android/location/e/ae;)V

    move-object v0, v1

    .line 221
    goto :goto_1

    :cond_3
    move-object v0, v1

    .line 223
    goto :goto_1
.end method
