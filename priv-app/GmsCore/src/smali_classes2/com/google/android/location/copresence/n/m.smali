.class public final Lcom/google/android/location/copresence/n/m;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/es;
.implements Lcom/google/android/gms/common/et;


# instance fields
.field public final a:Lcom/google/android/gms/location/reporting/i;

.field private final b:Lcom/google/android/location/copresence/n/o;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/location/copresence/n/o;)V
    .locals 1

    .prologue
    .line 68
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 69
    new-instance v0, Lcom/google/android/gms/location/reporting/i;

    invoke-direct {v0, p1, p0, p0}, Lcom/google/android/gms/location/reporting/i;-><init>(Landroid/content/Context;Lcom/google/android/gms/common/es;Lcom/google/android/gms/common/et;)V

    iput-object v0, p0, Lcom/google/android/location/copresence/n/m;->a:Lcom/google/android/gms/location/reporting/i;

    .line 70
    if-eqz p2, :cond_0

    .line 71
    iput-object p2, p0, Lcom/google/android/location/copresence/n/m;->b:Lcom/google/android/location/copresence/n/o;

    .line 79
    :goto_0
    return-void

    .line 73
    :cond_0
    new-instance v0, Lcom/google/android/location/copresence/n/n;

    invoke-direct {v0, p0}, Lcom/google/android/location/copresence/n/n;-><init>(Lcom/google/android/location/copresence/n/m;)V

    iput-object v0, p0, Lcom/google/android/location/copresence/n/m;->b:Lcom/google/android/location/copresence/n/o;

    goto :goto_0
.end method

.method private a()Z
    .locals 4

    .prologue
    .line 256
    const-string v0, "This function cannot be called in the main thread."

    invoke-static {v0}, Lcom/google/android/gms/common/internal/bx;->c(Ljava/lang/String;)V

    .line 257
    iget-object v1, p0, Lcom/google/android/location/copresence/n/m;->a:Lcom/google/android/gms/location/reporting/i;

    monitor-enter v1

    .line 258
    :try_start_0
    iget-object v0, p0, Lcom/google/android/location/copresence/n/m;->a:Lcom/google/android/gms/location/reporting/i;

    invoke-virtual {v0}, Lcom/google/android/gms/location/reporting/i;->c_()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 259
    const/4 v0, 0x1

    monitor-exit v1

    .line 271
    :goto_0
    return v0

    .line 261
    :cond_0
    iget-object v0, p0, Lcom/google/android/location/copresence/n/m;->a:Lcom/google/android/gms/location/reporting/i;

    invoke-virtual {v0}, Lcom/google/android/gms/location/reporting/i;->n_()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v0, :cond_1

    .line 263
    :try_start_1
    iget-object v0, p0, Lcom/google/android/location/copresence/n/m;->a:Lcom/google/android/gms/location/reporting/i;

    const-wide/32 v2, 0xea60

    invoke-virtual {v0, v2, v3}, Ljava/lang/Object;->wait(J)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 271
    :cond_1
    :try_start_2
    iget-object v0, p0, Lcom/google/android/location/copresence/n/m;->a:Lcom/google/android/gms/location/reporting/i;

    invoke-virtual {v0}, Lcom/google/android/gms/location/reporting/i;->c_()Z

    move-result v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 272
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 264
    :catch_0
    move-exception v0

    .line 265
    const/4 v2, 0x6

    :try_start_3
    invoke-static {v2}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 266
    const-string v2, "CopresenceUlrClient: Error waiting for initialization"

    invoke-static {v2, v0}, Lcom/google/android/location/copresence/ag;->a(Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 268
    :cond_2
    const/4 v0, 0x0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0
.end method


# virtual methods
.method public final T_()V
    .locals 2

    .prologue
    .line 91
    iget-object v1, p0, Lcom/google/android/location/copresence/n/m;->a:Lcom/google/android/gms/location/reporting/i;

    monitor-enter v1

    .line 92
    :try_start_0
    iget-object v0, p0, Lcom/google/android/location/copresence/n/m;->a:Lcom/google/android/gms/location/reporting/i;

    invoke-virtual {v0}, Ljava/lang/Object;->notifyAll()V

    .line 93
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final W_()V
    .locals 1

    .prologue
    .line 109
    const/4 v0, 0x5

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 110
    const-string v0, "CopresenceUlrClient: ReportingClient was disconnected, waiting for system to reconnect."

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->d(Ljava/lang/String;)I

    .line 112
    :cond_0
    return-void
.end method

.method public final a(Landroid/accounts/Account;)Lcom/google/android/gms/location/reporting/ReportingState;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 115
    invoke-direct {p0}, Lcom/google/android/location/copresence/n/m;->a()Z

    move-result v1

    if-nez v1, :cond_1

    .line 125
    :cond_0
    :goto_0
    return-object v0

    .line 119
    :cond_1
    :try_start_0
    iget-object v1, p0, Lcom/google/android/location/copresence/n/m;->a:Lcom/google/android/gms/location/reporting/i;

    invoke-virtual {v1, p1}, Lcom/google/android/gms/location/reporting/i;->a(Landroid/accounts/Account;)Lcom/google/android/gms/location/reporting/ReportingState;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_0

    .line 121
    :catch_0
    move-exception v1

    .line 122
    const/4 v2, 0x6

    invoke-static {v2}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 123
    const-string v2, "CopresenceUlrClient: Error getting reporting state."

    invoke-static {v2, v1}, Lcom/google/android/location/copresence/ag;->a(Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/common/c;)V
    .locals 4

    .prologue
    .line 98
    const-string v0, "Connection to ReportingClient failed: %d."

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-virtual {p1}, Lcom/google/android/gms/common/c;->c()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 101
    const/4 v1, 0x6

    invoke-static {v1}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 102
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "CopresenceUlrClient: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->e(Ljava/lang/String;)I

    .line 104
    :cond_0
    return-void
.end method

.method public final a(Landroid/accounts/Account;Ljava/lang/String;)Z
    .locals 7

    .prologue
    const/4 v1, 0x1

    const/4 v6, 0x2

    const/4 v0, 0x0

    .line 225
    invoke-virtual {p0, p1}, Lcom/google/android/location/copresence/n/m;->a(Landroid/accounts/Account;)Lcom/google/android/gms/location/reporting/ReportingState;

    move-result-object v2

    if-nez v2, :cond_1

    move v2, v0

    :goto_0
    if-nez v2, :cond_4

    .line 248
    :cond_0
    :goto_1
    return v0

    .line 225
    :cond_1
    invoke-static {v6}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v3

    if-eqz v3, :cond_2

    const-string v3, "Account \'%s\' opted into ULR: %b"

    new-array v4, v6, [Ljava/lang/Object;

    iget-object v5, p1, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-static {v5}, Lcom/google/android/location/copresence/ag;->f(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v0

    invoke-virtual {v2}, Lcom/google/android/gms/location/reporting/ReportingState;->f()Z

    move-result v5

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    aput-object v5, v4, v1

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "CopresenceUlrClient: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/location/copresence/ag;->a(Ljava/lang/String;)I

    :cond_2
    invoke-virtual {v2}, Lcom/google/android/gms/location/reporting/ReportingState;->f()Z

    move-result v3

    if-nez v3, :cond_3

    const/4 v3, 0x7

    invoke-static {v3}, Lcom/google/android/location/copresence/b/a;->c(I)V

    :cond_3
    invoke-virtual {v2}, Lcom/google/android/gms/location/reporting/ReportingState;->f()Z

    move-result v2

    goto :goto_0

    .line 228
    :cond_4
    const-wide/16 v2, 0x0

    invoke-static {p1, p2, v2, v3}, Lcom/google/android/gms/location/reporting/UploadRequest;->a(Landroid/accounts/Account;Ljava/lang/String;J)Lcom/google/android/gms/location/reporting/o;

    move-result-object v2

    new-instance v3, Lcom/google/android/gms/location/reporting/UploadRequest;

    invoke-direct {v3, v2, v0}, Lcom/google/android/gms/location/reporting/UploadRequest;-><init>(Lcom/google/android/gms/location/reporting/o;B)V

    .line 231
    :try_start_0
    iget-object v2, p0, Lcom/google/android/location/copresence/n/m;->a:Lcom/google/android/gms/location/reporting/i;

    iget-object v2, v2, Lcom/google/android/gms/location/reporting/i;->a:Lcom/google/android/gms/location/reporting/a/f;

    invoke-virtual {v2, v3}, Lcom/google/android/gms/location/reporting/a/f;->a(Lcom/google/android/gms/location/reporting/UploadRequest;)Lcom/google/android/gms/location/reporting/UploadRequestResult;

    move-result-object v2

    .line 232
    invoke-virtual {v2}, Lcom/google/android/gms/location/reporting/UploadRequestResult;->b()I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    .line 239
    if-eqz v2, :cond_5

    .line 240
    invoke-static {v6}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 241
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "CopresenceUlrClient: Error requesting ULR upload. ResultCode: "

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/location/copresence/ag;->a(Ljava/lang/String;)I

    goto :goto_1

    .line 233
    :catch_0
    move-exception v1

    .line 234
    const/4 v2, 0x5

    invoke-static {v2}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 235
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "CopresenceUlrClient: Error requesting ULR upload."

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/location/copresence/ag;->d(Ljava/lang/String;)I

    goto/16 :goto_1

    .line 245
    :cond_5
    invoke-static {v6}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 246
    const-string v0, "CopresenceUlrClient: Success requesting ULR upload"

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->a(Ljava/lang/String;)I

    :cond_6
    move v0, v1

    .line 248
    goto/16 :goto_1
.end method
