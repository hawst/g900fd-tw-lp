.class public final Lcom/google/android/location/copresence/h/d;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final a:Ljava/util/HashMap;

.field final synthetic b:Lcom/google/android/location/copresence/h/c;


# direct methods
.method private constructor <init>(Lcom/google/android/location/copresence/h/c;)V
    .locals 1

    .prologue
    .line 39
    iput-object p1, p0, Lcom/google/android/location/copresence/h/d;->b:Lcom/google/android/location/copresence/h/c;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/location/copresence/h/d;->a:Ljava/util/HashMap;

    .line 41
    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/location/copresence/h/c;B)V
    .locals 0

    .prologue
    .line 36
    invoke-direct {p0, p1}, Lcom/google/android/location/copresence/h/d;-><init>(Lcom/google/android/location/copresence/h/c;)V

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/location/copresence/SubscribedMessage;Ljava/util/List;)V
    .locals 5

    .prologue
    const/4 v4, 0x6

    .line 44
    if-nez p2, :cond_1

    .line 59
    :cond_0
    return-void

    .line 47
    :cond_1
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_2
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 49
    iget-object v2, p0, Lcom/google/android/location/copresence/h/d;->b:Lcom/google/android/location/copresence/h/c;

    iget-object v2, v2, Lcom/google/android/location/copresence/h/c;->a:Lcom/google/android/location/copresence/e/d;

    invoke-virtual {v2, v0}, Lcom/google/android/location/copresence/e/d;->a(Ljava/lang/String;)Lcom/google/android/location/copresence/e/f;

    move-result-object v2

    .line 50
    if-nez v2, :cond_3

    .line 52
    invoke-static {v4}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 53
    const-string v0, "MessageHandler: Unknown subscription id when trying to dispatch message."

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->e(Ljava/lang/String;)I

    goto :goto_0

    .line 57
    :cond_3
    iget-object v0, v2, Lcom/google/android/location/copresence/e/f;->b:Lcom/google/android/gms/location/copresence/MessageFilter;

    invoke-virtual {p1}, Lcom/google/android/gms/location/copresence/SubscribedMessage;->b()Lcom/google/android/gms/location/copresence/Message;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/android/gms/location/copresence/MessageFilter;->a(Lcom/google/android/gms/location/copresence/Message;)Z

    move-result v0

    if-nez v0, :cond_4

    invoke-static {v4}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, "MessageHandler: Received a message for an entry that should not have gotten it."

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->e(Ljava/lang/String;)I

    goto :goto_0

    :cond_4
    iget-object v0, p0, Lcom/google/android/location/copresence/h/d;->a:Ljava/util/HashMap;

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    if-nez v0, :cond_5

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iget-object v3, p0, Lcom/google/android/location/copresence/h/d;->a:Ljava/util/HashMap;

    invoke-virtual {v3, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_5
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method
