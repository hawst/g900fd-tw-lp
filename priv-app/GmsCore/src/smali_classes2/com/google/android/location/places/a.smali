.class final Lcom/google/android/location/places/a;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method public static a(Lcom/google/android/gms/location/places/AutocompleteFilter;)Lcom/google/android/location/l/a/av;
    .locals 6

    .prologue
    .line 17
    if-nez p0, :cond_0

    .line 18
    const/4 v0, 0x0

    .line 34
    :goto_0
    return-object v0

    .line 21
    :cond_0
    new-instance v3, Lcom/google/android/location/l/a/av;

    invoke-direct {v3}, Lcom/google/android/location/l/a/av;-><init>()V

    .line 22
    invoke-virtual {p0}, Lcom/google/android/gms/location/places/AutocompleteFilter;->c()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 23
    invoke-virtual {p0}, Lcom/google/android/gms/location/places/AutocompleteFilter;->c()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v0

    new-array v0, v0, [Ljava/lang/String;

    iput-object v0, v3, Lcom/google/android/location/l/a/av;->b:[Ljava/lang/String;

    .line 24
    const/4 v0, 0x0

    .line 25
    invoke-virtual {p0}, Lcom/google/android/gms/location/places/AutocompleteFilter;->c()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move v1, v0

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/location/places/PlaceType;

    .line 26
    iget-object v5, v3, Lcom/google/android/location/l/a/av;->b:[Ljava/lang/String;

    add-int/lit8 v2, v1, 0x1

    invoke-virtual {v0}, Lcom/google/android/gms/location/places/PlaceType;->a()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v5, v1

    move v1, v2

    .line 27
    goto :goto_1

    .line 30
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/gms/location/places/AutocompleteFilter;->b()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 31
    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, v3, Lcom/google/android/location/l/a/av;->a:Ljava/lang/Boolean;

    :cond_2
    move-object v0, v3

    .line 34
    goto :goto_0
.end method
