.class public final Lcom/google/android/location/collectionlib/co;
.super Lcom/google/android/location/collectionlib/g;
.source "SourceFile"


# instance fields
.field private final d:Lcom/google/android/location/collectionlib/SensorScannerConfig;

.field private e:J

.field private f:[J

.field private g:[D

.field private h:I


# direct methods
.method public constructor <init>(Lcom/google/android/location/collectionlib/ce;Lcom/google/android/location/collectionlib/ak;Lcom/google/android/location/collectionlib/SensorScannerConfig;)V
    .locals 3

    .prologue
    const/16 v2, 0x100

    .line 22
    invoke-direct {p0, p1, p2}, Lcom/google/android/location/collectionlib/g;-><init>(Lcom/google/android/location/collectionlib/ce;Lcom/google/android/location/collectionlib/ak;)V

    .line 16
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/location/collectionlib/co;->e:J

    .line 17
    new-array v0, v2, [J

    iput-object v0, p0, Lcom/google/android/location/collectionlib/co;->f:[J

    .line 18
    new-array v0, v2, [D

    iput-object v0, p0, Lcom/google/android/location/collectionlib/co;->g:[D

    .line 19
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/location/collectionlib/co;->h:I

    .line 23
    iput-object p3, p0, Lcom/google/android/location/collectionlib/co;->d:Lcom/google/android/location/collectionlib/SensorScannerConfig;

    .line 24
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 0

    .prologue
    .line 28
    return-void
.end method

.method public final a(Lcom/google/android/location/collectionlib/cg;JLandroid/hardware/SensorEvent;)V
    .locals 10

    .prologue
    const/4 v7, 0x2

    const-wide/16 v2, 0x0

    const/4 v6, 0x1

    const/4 v1, 0x0

    .line 33
    if-eqz p4, :cond_3

    iget-object v0, p4, Landroid/hardware/SensorEvent;->sensor:Landroid/hardware/Sensor;

    invoke-virtual {v0}, Landroid/hardware/Sensor;->getType()I

    move-result v0

    if-ne v0, v6, :cond_3

    .line 36
    iget-object v0, p4, Landroid/hardware/SensorEvent;->values:[F

    aget v4, v0, v1

    aget v5, v0, v1

    mul-float/2addr v4, v5

    aget v5, v0, v6

    aget v6, v0, v6

    mul-float/2addr v5, v6

    add-float/2addr v4, v5

    aget v5, v0, v7

    aget v0, v0, v7

    mul-float/2addr v0, v5

    add-float/2addr v0, v4

    float-to-double v4, v0

    invoke-static {v4, v5}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v4

    .line 39
    iget-object v0, p0, Lcom/google/android/location/collectionlib/co;->f:[J

    iget v6, p0, Lcom/google/android/location/collectionlib/co;->h:I

    aput-wide p2, v0, v6

    .line 40
    iget-object v0, p0, Lcom/google/android/location/collectionlib/co;->g:[D

    iget v6, p0, Lcom/google/android/location/collectionlib/co;->h:I

    add-int/lit8 v7, v6, 0x1

    iput v7, p0, Lcom/google/android/location/collectionlib/co;->h:I

    aput-wide v4, v0, v6

    .line 41
    iget v0, p0, Lcom/google/android/location/collectionlib/co;->h:I

    const/16 v4, 0x100

    if-ge v0, v4, :cond_0

    iget v0, p0, Lcom/google/android/location/collectionlib/co;->h:I

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/location/collectionlib/co;->f:[J

    aget-wide v4, v0, v1

    sub-long v4, p2, v4

    const-wide/16 v6, 0x12c

    cmp-long v0, v4, v6

    if-lez v0, :cond_3

    .line 43
    :cond_0
    iget v0, p0, Lcom/google/android/location/collectionlib/co;->h:I

    if-nez v0, :cond_4

    const-wide/high16 v2, 0x7ff8000000000000L    # NaN

    .line 44
    :goto_0
    const-wide v4, 0x3fc999999999999aL    # 0.2

    cmpl-double v0, v2, v4

    if-lez v0, :cond_1

    .line 45
    iput-wide p2, p0, Lcom/google/android/location/collectionlib/co;->e:J

    .line 47
    :cond_1
    iget-wide v2, p0, Lcom/google/android/location/collectionlib/co;->e:J

    const-wide/16 v4, 0x0

    cmp-long v0, v2, v4

    if-eqz v0, :cond_2

    iget-wide v2, p0, Lcom/google/android/location/collectionlib/co;->e:J

    sub-long v2, p2, v2

    iget-object v0, p0, Lcom/google/android/location/collectionlib/co;->d:Lcom/google/android/location/collectionlib/SensorScannerConfig;

    invoke-virtual {v0}, Lcom/google/android/location/collectionlib/SensorScannerConfig;->d()J

    move-result-wide v4

    cmp-long v0, v2, v4

    if-lez v0, :cond_2

    .line 50
    iget-object v0, p0, Lcom/google/android/location/collectionlib/g;->c:Lcom/google/android/location/collectionlib/ak;

    iget-object v2, p0, Lcom/google/android/location/collectionlib/co;->a:Ljava/lang/Runnable;

    invoke-virtual {v0, v2}, Lcom/google/android/location/collectionlib/ak;->postAtFrontOfQueue(Ljava/lang/Runnable;)Z

    .line 53
    :cond_2
    iput v1, p0, Lcom/google/android/location/collectionlib/co;->h:I

    .line 56
    :cond_3
    return-void

    :cond_4
    move v0, v1

    move-wide v4, v2

    .line 43
    :goto_1
    iget v6, p0, Lcom/google/android/location/collectionlib/co;->h:I

    if-ge v0, v6, :cond_5

    iget-object v6, p0, Lcom/google/android/location/collectionlib/co;->g:[D

    aget-wide v6, v6, v0

    add-double/2addr v4, v6

    iget-object v6, p0, Lcom/google/android/location/collectionlib/co;->g:[D

    aget-wide v6, v6, v0

    iget-object v8, p0, Lcom/google/android/location/collectionlib/co;->g:[D

    aget-wide v8, v8, v0

    mul-double/2addr v6, v8

    add-double/2addr v2, v6

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_5
    iget v0, p0, Lcom/google/android/location/collectionlib/co;->h:I

    int-to-double v6, v0

    div-double v6, v4, v6

    mul-double/2addr v4, v6

    sub-double/2addr v2, v4

    iget v0, p0, Lcom/google/android/location/collectionlib/co;->h:I

    int-to-double v4, v0

    div-double/2addr v2, v4

    goto :goto_0
.end method

.method public final b()V
    .locals 0

    .prologue
    .line 80
    return-void
.end method
