.class public Lcom/google/android/location/copresence/debug/CopresDatabaseHelper$SubscribedMessageRecord;
.super Lcom/google/android/location/copresence/debug/CopresDatabaseHelper$BaseMessageRecord;
.source "SourceFile"


# instance fields
.field public distanceDataAccuracyM:Ljava/lang/String;
    .annotation runtime Lcom/google/android/location/copresence/debug/b;
        a = true
    .end annotation
.end field

.field public distanceDataDistanceM:Ljava/lang/String;
    .annotation runtime Lcom/google/android/location/copresence/debug/b;
        a = true
    .end annotation
.end field

.field public senderDeviceId:Ljava/lang/String;
    .annotation runtime Lcom/google/android/location/copresence/debug/b;
        a = true
    .end annotation
.end field

.field public senderGaiaId:Ljava/lang/String;
    .annotation runtime Lcom/google/android/location/copresence/debug/b;
        a = true
    .end annotation
.end field

.field public senderMessageId:Ljava/lang/String;
    .annotation runtime Lcom/google/android/location/copresence/debug/b;
        a = true
    .end annotation
.end field

.field public subscriptionIds:Ljava/lang/String;
    .annotation runtime Lcom/google/android/location/copresence/debug/b;
        a = true
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 329
    invoke-direct {p0}, Lcom/google/android/location/copresence/debug/CopresDatabaseHelper$BaseMessageRecord;-><init>()V

    .line 330
    const-string v0, "SubscribedMessage"

    iput-object v0, p0, Lcom/google/android/location/copresence/debug/CopresDatabaseHelper$SubscribedMessageRecord;->messageType:Ljava/lang/String;

    .line 331
    return-void
.end method
