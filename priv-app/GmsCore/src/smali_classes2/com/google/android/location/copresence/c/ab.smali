.class final Lcom/google/android/location/copresence/c/ab;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lcom/google/android/location/copresence/c/aa;


# direct methods
.method constructor <init>(Lcom/google/android/location/copresence/c/aa;)V
    .locals 0

    .prologue
    .line 128
    iput-object p1, p0, Lcom/google/android/location/copresence/c/ab;->a:Lcom/google/android/location/copresence/c/aa;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 2

    .prologue
    .line 131
    iget-object v0, p0, Lcom/google/android/location/copresence/c/ab;->a:Lcom/google/android/location/copresence/c/aa;

    iget-object v0, v0, Lcom/google/android/location/copresence/c/aa;->a:Lcom/google/android/location/copresence/c/z;

    iget v0, v0, Lcom/google/android/location/copresence/c/z;->a:I

    const/4 v1, 0x2

    if-eq v0, v1, :cond_1

    .line 133
    const/4 v0, 0x3

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 134
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Audio Stack: JustListenManager: Callback occurred with listen request"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/location/copresence/c/ab;->a:Lcom/google/android/location/copresence/c/aa;

    iget-object v1, v1, Lcom/google/android/location/copresence/c/aa;->a:Lcom/google/android/location/copresence/c/z;

    iget v1, v1, Lcom/google/android/location/copresence/c/z;->a:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->b(Ljava/lang/String;)I

    .line 143
    :cond_0
    :goto_0
    return-void

    .line 139
    :cond_1
    iget-object v0, p0, Lcom/google/android/location/copresence/c/ab;->a:Lcom/google/android/location/copresence/c/aa;

    iget-object v0, v0, Lcom/google/android/location/copresence/c/aa;->a:Lcom/google/android/location/copresence/c/z;

    iget-object v0, v0, Lcom/google/android/location/copresence/c/z;->e:Lcom/google/android/location/copresence/c/b;

    iget-object v1, p0, Lcom/google/android/location/copresence/c/ab;->a:Lcom/google/android/location/copresence/c/aa;

    iget-object v1, v1, Lcom/google/android/location/copresence/c/aa;->a:Lcom/google/android/location/copresence/c/z;

    iget-object v1, v1, Lcom/google/android/location/copresence/c/z;->c:Lcom/google/android/gms/audiomodem/bm;

    invoke-virtual {v0, v1}, Lcom/google/android/location/copresence/c/b;->a(Lcom/google/android/gms/audiomodem/bm;)V

    .line 140
    iget-object v0, p0, Lcom/google/android/location/copresence/c/ab;->a:Lcom/google/android/location/copresence/c/aa;

    iget-object v0, v0, Lcom/google/android/location/copresence/c/aa;->a:Lcom/google/android/location/copresence/c/z;

    iget-object v0, v0, Lcom/google/android/location/copresence/c/z;->f:Lcom/google/android/location/copresence/c/l;

    if-eqz v0, :cond_0

    .line 141
    iget-object v0, p0, Lcom/google/android/location/copresence/c/ab;->a:Lcom/google/android/location/copresence/c/aa;

    iget-object v0, v0, Lcom/google/android/location/copresence/c/aa;->a:Lcom/google/android/location/copresence/c/z;

    iget-object v0, v0, Lcom/google/android/location/copresence/c/z;->f:Lcom/google/android/location/copresence/c/l;

    invoke-interface {v0}, Lcom/google/android/location/copresence/c/l;->a()V

    goto :goto_0
.end method
