.class final Lcom/google/android/location/collectionlib/bc;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/location/GpsNavigationMessageEvent$Listener;


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x16
.end annotation


# instance fields
.field final synthetic a:Lcom/google/android/location/collectionlib/ba;


# direct methods
.method private constructor <init>(Lcom/google/android/location/collectionlib/ba;)V
    .locals 0

    .prologue
    .line 204
    iput-object p1, p0, Lcom/google/android/location/collectionlib/bc;->a:Lcom/google/android/location/collectionlib/ba;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/location/collectionlib/ba;B)V
    .locals 0

    .prologue
    .line 204
    invoke-direct {p0, p1}, Lcom/google/android/location/collectionlib/bc;-><init>(Lcom/google/android/location/collectionlib/ba;)V

    return-void
.end method


# virtual methods
.method public final onGpsNavigationMessageReceived(Landroid/location/GpsNavigationMessageEvent;)V
    .locals 5

    .prologue
    .line 207
    iget-object v0, p0, Lcom/google/android/location/collectionlib/bc;->a:Lcom/google/android/location/collectionlib/ba;

    invoke-static {v0}, Lcom/google/android/location/collectionlib/ba;->b(Lcom/google/android/location/collectionlib/ba;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/location/collectionlib/bc;->a:Lcom/google/android/location/collectionlib/ba;

    invoke-virtual {v0}, Lcom/google/android/location/collectionlib/ba;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 213
    :cond_0
    :goto_0
    return-void

    .line 210
    :cond_1
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    .line 211
    iget-object v2, p0, Lcom/google/android/location/collectionlib/bc;->a:Lcom/google/android/location/collectionlib/ba;

    iget-object v2, v2, Lcom/google/android/location/collectionlib/ce;->d:Lcom/google/android/location/collectionlib/ak;

    invoke-virtual {v2, p1, v0, v1}, Lcom/google/android/location/collectionlib/ak;->a(Landroid/location/GpsNavigationMessageEvent;J)V

    .line 212
    iget-object v2, p0, Lcom/google/android/location/collectionlib/bc;->a:Lcom/google/android/location/collectionlib/ba;

    sget-object v3, Lcom/google/android/location/collectionlib/cg;->q:Lcom/google/android/location/collectionlib/cg;

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v0, v1, v4}, Lcom/google/android/location/collectionlib/ba;->b(Lcom/google/android/location/collectionlib/cg;JLandroid/hardware/SensorEvent;)V

    goto :goto_0
.end method

.method public final onStatusChanged(I)V
    .locals 0

    .prologue
    .line 216
    return-void
.end method
