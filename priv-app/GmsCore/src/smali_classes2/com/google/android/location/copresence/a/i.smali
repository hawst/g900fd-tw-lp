.class final Lcom/google/android/location/copresence/a/i;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static d:Lcom/google/android/location/copresence/a/i;


# instance fields
.field final a:Ljava/util/HashMap;

.field final b:Landroid/content/Context;

.field c:Lcom/google/android/location/os/bl;


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 58
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 43
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/location/copresence/a/i;->a:Ljava/util/HashMap;

    .line 59
    iput-object p1, p0, Lcom/google/android/location/copresence/a/i;->b:Landroid/content/Context;

    .line 60
    return-void
.end method

.method static declared-synchronized a(Landroid/content/Context;)Lcom/google/android/location/copresence/a/i;
    .locals 2

    .prologue
    .line 52
    const-class v1, Lcom/google/android/location/copresence/a/i;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/google/android/location/copresence/a/i;->d:Lcom/google/android/location/copresence/a/i;

    if-nez v0, :cond_0

    .line 53
    new-instance v0, Lcom/google/android/location/copresence/a/i;

    invoke-direct {v0, p0}, Lcom/google/android/location/copresence/a/i;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/google/android/location/copresence/a/i;->d:Lcom/google/android/location/copresence/a/i;

    .line 55
    :cond_0
    sget-object v0, Lcom/google/android/location/copresence/a/i;->d:Lcom/google/android/location/copresence/a/i;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 52
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method final a(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 115
    iget-object v1, p0, Lcom/google/android/location/copresence/a/i;->a:Ljava/util/HashMap;

    monitor-enter v1

    .line 116
    :try_start_0
    iget-object v0, p0, Lcom/google/android/location/copresence/a/i;->a:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v0

    .line 117
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method final a()V
    .locals 8

    .prologue
    .line 121
    new-instance v3, Lcom/google/android/location/copresence/j/d;

    invoke-direct {v3}, Lcom/google/android/location/copresence/j/d;-><init>()V

    .line 124
    iget-object v4, p0, Lcom/google/android/location/copresence/a/i;->a:Ljava/util/HashMap;

    monitor-enter v4

    .line 125
    :try_start_0
    iget-object v0, p0, Lcom/google/android/location/copresence/a/i;->a:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->size()I

    move-result v0

    new-array v5, v0, [Lcom/google/android/location/copresence/j/c;

    .line 128
    const/4 v0, 0x0

    .line 129
    iget-object v1, p0, Lcom/google/android/location/copresence/a/i;->a:Ljava/util/HashMap;

    invoke-virtual {v1}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v6

    move v2, v0

    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 130
    new-instance v7, Lcom/google/android/location/copresence/j/c;

    invoke-direct {v7}, Lcom/google/android/location/copresence/j/c;-><init>()V

    .line 132
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    iput-object v1, v7, Lcom/google/android/location/copresence/j/c;->a:Ljava/lang/String;

    .line 133
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, v7, Lcom/google/android/location/copresence/j/c;->b:Ljava/lang/String;

    .line 135
    add-int/lit8 v0, v2, 0x1

    aput-object v7, v5, v2

    move v2, v0

    .line 136
    goto :goto_0

    .line 137
    :cond_0
    iput-object v5, v3, Lcom/google/android/location/copresence/j/d;->a:[Lcom/google/android/location/copresence/j/c;

    .line 138
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 141
    :try_start_1
    iget-object v0, p0, Lcom/google/android/location/copresence/a/i;->c:Lcom/google/android/location/os/bl;

    invoke-virtual {v0, v3}, Lcom/google/android/location/os/bl;->b(Lcom/google/protobuf/nano/j;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    .line 147
    :cond_1
    :goto_1
    return-void

    .line 138
    :catchall_0
    move-exception v0

    monitor-exit v4

    throw v0

    .line 142
    :catch_0
    move-exception v0

    .line 143
    const/4 v1, 0x6

    invoke-static {v1}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 144
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "ObfuscatedGaiaIdLookup: Unable to write GaiaId cache: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->e(Ljava/lang/String;)I

    goto :goto_1
.end method

.method final b()Ljava/util/HashMap;
    .locals 6

    .prologue
    .line 151
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 152
    new-instance v1, Lcom/google/android/location/copresence/j/d;

    invoke-direct {v1}, Lcom/google/android/location/copresence/j/d;-><init>()V

    .line 156
    :try_start_0
    iget-object v2, p0, Lcom/google/android/location/copresence/a/i;->c:Lcom/google/android/location/os/bl;

    invoke-virtual {v2, v1}, Lcom/google/android/location/os/bl;->a(Lcom/google/protobuf/nano/j;)Lcom/google/protobuf/nano/j;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 164
    iget-object v2, v1, Lcom/google/android/location/copresence/j/d;->a:[Lcom/google/android/location/copresence/j/c;

    array-length v3, v2

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v3, :cond_0

    aget-object v4, v2, v1

    .line 165
    iget-object v5, v4, Lcom/google/android/location/copresence/j/c;->a:Ljava/lang/String;

    iget-object v4, v4, Lcom/google/android/location/copresence/j/c;->b:Ljava/lang/String;

    invoke-virtual {v0, v5, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 164
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 157
    :catch_0
    move-exception v1

    .line 158
    const/4 v2, 0x6

    invoke-static {v2}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 159
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "ObfuscatedGaiaIdLookup: Could not load ObfuscatedGaiaIds: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/location/copresence/ag;->e(Ljava/lang/String;)I

    .line 167
    :cond_0
    return-object v0
.end method
