.class public final Lcom/google/android/location/places/bv;
.super Lcom/google/android/location/places/ab;
.source "SourceFile"


# static fields
.field private static final a:J


# instance fields
.field private final b:Lcom/google/android/location/places/af;

.field private final c:Lcom/google/android/location/places/be;

.field private final d:Lcom/google/android/location/places/f/c;

.field private final e:Lcom/google/android/location/j/b;

.field private f:Lcom/google/android/location/e/bi;

.field private g:J

.field private h:Lcom/google/android/location/places/ac;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 39
    sget-object v0, Ljava/util/concurrent/TimeUnit;->MINUTES:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0x1

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    sput-wide v0, Lcom/google/android/location/places/bv;->a:J

    return-void
.end method

.method public constructor <init>(Lcom/google/android/location/places/af;Lcom/google/android/location/places/be;Lcom/google/android/location/j/b;Lcom/google/android/location/places/f/c;)V
    .locals 1

    .prologue
    .line 60
    invoke-direct {p0}, Lcom/google/android/location/places/ab;-><init>()V

    .line 61
    invoke-static {p1}, Lcom/google/k/a/ah;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/places/af;

    iput-object v0, p0, Lcom/google/android/location/places/bv;->b:Lcom/google/android/location/places/af;

    .line 62
    invoke-static {p2}, Lcom/google/k/a/ah;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/places/be;

    iput-object v0, p0, Lcom/google/android/location/places/bv;->c:Lcom/google/android/location/places/be;

    .line 63
    invoke-static {p3}, Lcom/google/k/a/ah;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/j/b;

    iput-object v0, p0, Lcom/google/android/location/places/bv;->e:Lcom/google/android/location/j/b;

    .line 64
    iput-object p4, p0, Lcom/google/android/location/places/bv;->d:Lcom/google/android/location/places/f/c;

    .line 65
    return-void
.end method

.method static synthetic a(Lcom/google/android/location/places/bv;)Lcom/google/android/location/places/f/c;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lcom/google/android/location/places/bv;->d:Lcom/google/android/location/places/f/c;

    return-object v0
.end method

.method static synthetic b(Lcom/google/android/location/places/bv;)Lcom/google/android/location/j/b;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lcom/google/android/location/places/bv;->e:Lcom/google/android/location/j/b;

    return-object v0
.end method

.method static synthetic c(Lcom/google/android/location/places/bv;)Lcom/google/android/location/places/ac;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lcom/google/android/location/places/bv;->h:Lcom/google/android/location/places/ac;

    return-object v0
.end method


# virtual methods
.method protected final a()I
    .locals 1

    .prologue
    .line 101
    iget-object v0, p0, Lcom/google/android/location/places/bv;->b:Lcom/google/android/location/places/af;

    invoke-virtual {v0}, Lcom/google/android/location/places/af;->a()Lcom/google/android/location/places/ad;

    const/16 v0, 0x32

    return v0
.end method

.method public final a(Lcom/google/android/location/e/al;)V
    .locals 6

    .prologue
    .line 74
    const/4 v0, 0x0

    .line 75
    iget-object v1, p0, Lcom/google/android/location/places/bv;->d:Lcom/google/android/location/places/f/c;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/location/places/bv;->f:Lcom/google/android/location/e/bi;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/android/location/places/bv;->e:Lcom/google/android/location/j/b;

    invoke-interface {v1}, Lcom/google/android/location/j/b;->c()J

    move-result-wide v2

    iget-wide v4, p0, Lcom/google/android/location/places/bv;->g:J

    sub-long/2addr v2, v4

    sget-wide v4, Lcom/google/android/location/places/bv;->a:J

    cmp-long v1, v2, v4

    if-gez v1, :cond_1

    const/4 v1, 0x1

    :goto_0
    if-eqz v1, :cond_0

    .line 76
    iget-object v0, p0, Lcom/google/android/location/places/bv;->d:Lcom/google/android/location/places/f/c;

    iget-object v1, p0, Lcom/google/android/location/places/bv;->f:Lcom/google/android/location/e/bi;

    invoke-interface {v0, v1}, Lcom/google/android/location/places/f/c;->a(Lcom/google/android/location/e/bi;)Ljava/util/List;

    move-result-object v0

    .line 78
    :cond_0
    if-eqz v0, :cond_2

    .line 79
    invoke-static {}, Lcom/google/android/location/places/p;->a()Lcom/google/android/location/o/a/c;

    move-result-object v1

    const-string v2, "Found cached results"

    invoke-virtual {v1, v2}, Lcom/google/android/location/o/a/c;->b(Ljava/lang/String;)V

    .line 81
    iget-object v1, p0, Lcom/google/android/location/places/bv;->h:Lcom/google/android/location/places/ac;

    new-instance v2, Lcom/google/android/location/places/ae;

    invoke-virtual {p0}, Lcom/google/android/location/places/bv;->a()I

    move-result v3

    iget-object v4, p0, Lcom/google/android/location/places/bv;->e:Lcom/google/android/location/j/b;

    invoke-interface {v4}, Lcom/google/android/location/j/b;->c()J

    move-result-wide v4

    invoke-direct {v2, v0, v3, v4, v5}, Lcom/google/android/location/places/ae;-><init>(Ljava/util/List;IJ)V

    invoke-interface {v1, v2}, Lcom/google/android/location/places/ac;->a(Lcom/google/android/location/places/ae;)V

    .line 89
    :goto_1
    return-void

    .line 75
    :cond_1
    const/4 v1, 0x0

    goto :goto_0

    .line 84
    :cond_2
    invoke-static {}, Lcom/google/android/location/places/p;->a()Lcom/google/android/location/o/a/c;

    move-result-object v0

    const-string v1, "Cache miss, calling server"

    invoke-virtual {v0, v1}, Lcom/google/android/location/o/a/c;->b(Ljava/lang/String;)V

    .line 87
    iget-object v0, p0, Lcom/google/android/location/places/bv;->c:Lcom/google/android/location/places/be;

    new-instance v1, Lcom/google/android/location/places/bw;

    iget-object v2, p0, Lcom/google/android/location/places/bv;->f:Lcom/google/android/location/e/bi;

    invoke-direct {v1, p0, v2}, Lcom/google/android/location/places/bw;-><init>(Lcom/google/android/location/places/bv;Lcom/google/android/location/e/bi;)V

    invoke-interface {v0, p1, v1}, Lcom/google/android/location/places/be;->a(Lcom/google/android/location/e/al;Lcom/google/android/location/places/x;)V

    goto :goto_1
.end method

.method public final a(Lcom/google/android/location/e/bi;)V
    .locals 2

    .prologue
    .line 93
    if-eqz p1, :cond_0

    .line 94
    iput-object p1, p0, Lcom/google/android/location/places/bv;->f:Lcom/google/android/location/e/bi;

    .line 95
    iget-object v0, p0, Lcom/google/android/location/places/bv;->e:Lcom/google/android/location/j/b;

    invoke-interface {v0}, Lcom/google/android/location/j/b;->c()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/location/places/bv;->g:J

    .line 97
    :cond_0
    return-void
.end method

.method public final a(Lcom/google/android/location/places/ac;)V
    .locals 0

    .prologue
    .line 69
    iput-object p1, p0, Lcom/google/android/location/places/bv;->h:Lcom/google/android/location/places/ac;

    .line 70
    return-void
.end method
