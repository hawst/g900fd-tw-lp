.class public Lcom/google/android/location/os/real/SdkSpecific21;
.super Lcom/google/android/location/os/real/SdkSpecific19;
.source "SourceFile"


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x15
.end annotation


# instance fields
.field a:Landroid/net/wifi/WifiScanner$ScanListener;

.field private b:Ljava/lang/Boolean;

.field private c:Lcom/google/android/location/activity/z;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 28
    invoke-direct {p0}, Lcom/google/android/location/os/real/SdkSpecific19;-><init>()V

    .line 41
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/location/os/real/SdkSpecific21;->c:Lcom/google/android/location/activity/z;

    return-void
.end method

.method private static a(ZI)Landroid/net/wifi/WifiScanner$ScanSettings;
    .locals 2

    .prologue
    .line 153
    new-instance v0, Landroid/net/wifi/WifiScanner$ScanSettings;

    invoke-direct {v0}, Landroid/net/wifi/WifiScanner$ScanSettings;-><init>()V

    .line 155
    if-eqz p0, :cond_0

    .line 156
    const/4 v1, 0x2

    iput v1, v0, Landroid/net/wifi/WifiScanner$ScanSettings;->reportEvents:I

    .line 163
    :goto_0
    const/4 v1, 0x3

    iput v1, v0, Landroid/net/wifi/WifiScanner$ScanSettings;->band:I

    .line 164
    iput p1, v0, Landroid/net/wifi/WifiScanner$ScanSettings;->periodInMs:I

    .line 165
    return-object v0

    .line 158
    :cond_0
    const/4 v1, 0x0

    iput v1, v0, Landroid/net/wifi/WifiScanner$ScanSettings;->reportEvents:I

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/content/Context;Lcom/google/android/location/os/bi;)Lcom/google/android/location/os/bm;
    .locals 3

    .prologue
    .line 171
    sget-object v0, Lcom/google/android/location/d/a;->w:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 173
    :try_start_0
    new-instance v0, Lcom/google/android/location/os/real/ba;

    invoke-direct {v0, p1, p2}, Lcom/google/android/location/os/real/ba;-><init>(Landroid/content/Context;Lcom/google/android/location/os/bi;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 178
    :goto_0
    return-object v0

    .line 174
    :catch_0
    move-exception v0

    .line 175
    sget-boolean v1, Lcom/google/android/location/i/a;->e:Z

    if-eqz v1, :cond_0

    const-string v1, "SdkSpecific21"

    invoke-virtual {v0}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lcom/google/android/location/o/a/a;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 178
    :cond_0
    new-instance v0, Lcom/google/android/location/os/bh;

    invoke-direct {v0}, Lcom/google/android/location/os/bh;-><init>()V

    goto :goto_0
.end method

.method public final a(Landroid/content/Context;Lcom/google/android/location/os/real/be;Lcom/google/android/location/o/n;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 120
    const-string v0, "wifi"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/wifi/WifiManager;

    .line 122
    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/google/android/location/os/real/SdkSpecific21;->a(Landroid/net/wifi/WifiManager;I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 123
    sget-boolean v0, Lcom/google/android/location/i/a;->b:Z

    if-eqz v0, :cond_0

    const-string v0, "SdkSpecific21"

    const-string v1, "Starting wifi scan via WifiScanner"

    invoke-static {v0, v1}, Lcom/google/android/location/o/a/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 125
    :cond_0
    const-string v0, "wifiscanner"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/wifi/WifiScanner;

    .line 132
    const/16 v1, 0x2710

    invoke-static {v3, v1}, Lcom/google/android/location/os/real/SdkSpecific21;->a(ZI)Landroid/net/wifi/WifiScanner$ScanSettings;

    move-result-object v1

    .line 133
    new-instance v2, Lcom/google/android/location/os/real/bh;

    invoke-direct {v2, v0, v3, p2}, Lcom/google/android/location/os/real/bh;-><init>(Landroid/net/wifi/WifiScanner;ZLcom/google/android/location/os/real/be;)V

    invoke-virtual {v0, v1, v2}, Landroid/net/wifi/WifiScanner;->startBackgroundScan(Landroid/net/wifi/WifiScanner$ScanSettings;Landroid/net/wifi/WifiScanner$ScanListener;)V

    .line 139
    :goto_0
    return-void

    .line 136
    :cond_1
    sget-boolean v0, Lcom/google/android/location/i/a;->b:Z

    if-eqz v0, :cond_2

    const-string v0, "SdkSpecific21"

    const-string v1, "Starting wifi scan via legacy WifiManager"

    invoke-static {v0, v1}, Lcom/google/android/location/o/a/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 137
    :cond_2
    invoke-super {p0, p1, p2, p3}, Lcom/google/android/location/os/real/SdkSpecific19;->a(Landroid/content/Context;Lcom/google/android/location/os/real/be;Lcom/google/android/location/o/n;)V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/location/os/bi;J)V
    .locals 6

    .prologue
    .line 193
    iget-object v0, p0, Lcom/google/android/location/os/real/SdkSpecific21;->c:Lcom/google/android/location/activity/z;

    if-eqz v0, :cond_1

    .line 194
    sget-boolean v0, Lcom/google/android/location/i/a;->d:Z

    if-eqz v0, :cond_0

    const-string v0, "HWAR"

    const-string v1, "startHardwareActivityRecognitionLog has been called."

    invoke-static {v0, v1}, Lcom/google/android/location/o/a/a;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 202
    :cond_0
    :goto_0
    return-void

    .line 197
    :cond_1
    sget-object v0, Lcom/google/android/location/d/a;->p:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    .line 198
    new-instance v1, Lcom/google/android/location/activity/z;

    invoke-direct {v1, p1, p2, p3, v0}, Lcom/google/android/location/activity/z;-><init>(Lcom/google/android/location/os/bi;JZ)V

    iput-object v1, p0, Lcom/google/android/location/os/real/SdkSpecific21;->c:Lcom/google/android/location/activity/z;

    .line 201
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iget-object v1, p0, Lcom/google/android/location/os/real/SdkSpecific21;->c:Lcom/google/android/location/activity/z;

    sget-object v2, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v4, 0x14

    invoke-virtual {v2, v4, v5}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0
.end method

.method public final a(Landroid/content/Context;ZJLcom/google/android/location/os/real/be;)Z
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 85
    const-string v0, "wifi"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/wifi/WifiManager;

    .line 87
    invoke-virtual {p0, v0, v1}, Lcom/google/android/location/os/real/SdkSpecific21;->a(Landroid/net/wifi/WifiManager;I)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 88
    const-string v0, "wifiscanner"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/wifi/WifiScanner;

    .line 91
    if-eqz p2, :cond_3

    .line 92
    iget-object v3, p0, Lcom/google/android/location/os/real/SdkSpecific21;->a:Landroid/net/wifi/WifiScanner$ScanListener;

    if-eqz v3, :cond_1

    .line 94
    iget-object v2, p0, Lcom/google/android/location/os/real/SdkSpecific21;->a:Landroid/net/wifi/WifiScanner$ScanListener;

    invoke-virtual {v0, v2}, Landroid/net/wifi/WifiScanner;->stopBackgroundScan(Landroid/net/wifi/WifiScanner$ScanListener;)V

    :cond_0
    :goto_0
    move v0, v1

    .line 111
    :goto_1
    return v0

    .line 96
    :cond_1
    sget-boolean v3, Lcom/google/android/location/i/a;->b:Z

    if-eqz v3, :cond_2

    const-string v3, "SdkSpecific21"

    const-string v4, "Starting background scan ..."

    invoke-static {v3, v4}, Lcom/google/android/location/o/a/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 97
    :cond_2
    long-to-int v3, p3

    invoke-static {v2, v3}, Lcom/google/android/location/os/real/SdkSpecific21;->a(ZI)Landroid/net/wifi/WifiScanner$ScanSettings;

    move-result-object v3

    .line 98
    new-instance v4, Lcom/google/android/location/os/real/bh;

    invoke-direct {v4, v0, v2, p5}, Lcom/google/android/location/os/real/bh;-><init>(Landroid/net/wifi/WifiScanner;ZLcom/google/android/location/os/real/be;)V

    iput-object v4, p0, Lcom/google/android/location/os/real/SdkSpecific21;->a:Landroid/net/wifi/WifiScanner$ScanListener;

    .line 99
    iget-object v2, p0, Lcom/google/android/location/os/real/SdkSpecific21;->a:Landroid/net/wifi/WifiScanner$ScanListener;

    invoke-virtual {v0, v3, v2}, Landroid/net/wifi/WifiScanner;->startBackgroundScan(Landroid/net/wifi/WifiScanner$ScanSettings;Landroid/net/wifi/WifiScanner$ScanListener;)V

    goto :goto_0

    .line 103
    :cond_3
    iget-object v2, p0, Lcom/google/android/location/os/real/SdkSpecific21;->a:Landroid/net/wifi/WifiScanner$ScanListener;

    if-eqz v2, :cond_0

    .line 104
    iget-object v2, p0, Lcom/google/android/location/os/real/SdkSpecific21;->a:Landroid/net/wifi/WifiScanner$ScanListener;

    invoke-virtual {v0, v2}, Landroid/net/wifi/WifiScanner;->stopBackgroundScan(Landroid/net/wifi/WifiScanner$ScanListener;)V

    .line 105
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/location/os/real/SdkSpecific21;->a:Landroid/net/wifi/WifiScanner$ScanListener;

    goto :goto_0

    :cond_4
    move v0, v2

    .line 111
    goto :goto_1
.end method

.method public final declared-synchronized a(Landroid/net/wifi/WifiManager;I)Z
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 59
    monitor-enter p0

    :try_start_0
    sget-object v0, Lcom/google/android/location/d/a;->j:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    .line 60
    and-int/2addr v0, p2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    .line 65
    :goto_0
    if-nez v0, :cond_1

    move v0, v1

    .line 76
    :goto_1
    monitor-exit p0

    return v0

    :cond_0
    move v0, v1

    .line 60
    goto :goto_0

    .line 72
    :cond_1
    :try_start_1
    iget-object v0, p0, Lcom/google/android/location/os/real/SdkSpecific21;->b:Ljava/lang/Boolean;

    if-nez v0, :cond_2

    .line 73
    invoke-virtual {p1}, Landroid/net/wifi/WifiManager;->isWifiScannerSupported()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/os/real/SdkSpecific21;->b:Ljava/lang/Boolean;

    .line 76
    :cond_2
    iget-object v0, p0, Lcom/google/android/location/os/real/SdkSpecific21;->b:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v0

    goto :goto_1

    .line 59
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final a(Landroid/os/PowerManager;)Z
    .locals 1

    .prologue
    .line 188
    invoke-virtual {p1}, Landroid/os/PowerManager;->isPowerSaveMode()Z

    move-result v0

    return v0
.end method

.method public final b(Landroid/hardware/SensorManager;Lcom/google/android/location/os/bi;)Lcom/google/android/location/activity/bn;
    .locals 1

    .prologue
    .line 183
    new-instance v0, Lcom/google/android/location/activity/ad;

    invoke-direct {v0, p1, p2}, Lcom/google/android/location/activity/ad;-><init>(Landroid/hardware/SensorManager;Lcom/google/android/location/os/bi;)V

    return-object v0
.end method
