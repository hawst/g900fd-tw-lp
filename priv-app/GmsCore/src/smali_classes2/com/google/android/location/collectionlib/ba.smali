.class final Lcom/google/android/location/collectionlib/ba;
.super Lcom/google/android/location/collectionlib/ce;
.source "SourceFile"

# interfaces
.implements Landroid/location/GpsStatus$Listener;
.implements Landroid/location/LocationListener;


# instance fields
.field private final a:Lcom/google/android/location/d/b;

.field private final b:Ljava/lang/String;

.field private final g:Lcom/google/android/location/collectionlib/bb;

.field private final h:Lcom/google/android/location/collectionlib/bc;

.field private i:Z

.field private final j:Z

.field private final k:Z

.field private final l:Z

.field private final m:Z

.field private n:Landroid/location/GpsStatus;


# direct methods
.method public constructor <init>(Landroid/content/Context;ZZZZLcom/google/android/location/d/b;Lcom/google/android/location/collectionlib/ak;Lcom/google/android/location/collectionlib/ar;Lcom/google/android/location/o/a/c;Lcom/google/android/location/o/n;)V
    .locals 6

    .prologue
    .line 71
    move-object v0, p0

    move-object v1, p1

    move-object v2, p7

    move-object v3, p8

    move-object v4, p9

    move-object/from16 v5, p10

    invoke-direct/range {v0 .. v5}, Lcom/google/android/location/collectionlib/ce;-><init>(Landroid/content/Context;Lcom/google/android/location/collectionlib/ak;Lcom/google/android/location/collectionlib/ar;Lcom/google/android/location/o/a/c;Lcom/google/android/location/o/n;)V

    .line 44
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/location/collectionlib/ba;->i:Z

    .line 59
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/location/collectionlib/ba;->n:Landroid/location/GpsStatus;

    .line 72
    iput-boolean p2, p0, Lcom/google/android/location/collectionlib/ba;->j:Z

    .line 73
    iput-boolean p3, p0, Lcom/google/android/location/collectionlib/ba;->k:Z

    .line 74
    iput-boolean p4, p0, Lcom/google/android/location/collectionlib/ba;->l:Z

    .line 75
    iput-boolean p5, p0, Lcom/google/android/location/collectionlib/ba;->m:Z

    .line 76
    if-nez p6, :cond_0

    .line 77
    new-instance v0, Lcom/google/android/location/d/b;

    const/4 v1, 0x0

    invoke-direct {v0, p1, v1}, Lcom/google/android/location/d/b;-><init>(Landroid/content/Context;Z)V

    iput-object v0, p0, Lcom/google/android/location/collectionlib/ba;->a:Lcom/google/android/location/d/b;

    .line 81
    :goto_0
    iget-object v0, p0, Lcom/google/android/location/collectionlib/ba;->c:Lcom/google/android/location/o/a/c;

    iget-object v0, v0, Lcom/google/android/location/o/a/c;->a:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/location/collectionlib/ba;->b:Ljava/lang/String;

    .line 82
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x16

    if-lt v0, v1, :cond_1

    new-instance v0, Lcom/google/android/location/collectionlib/bb;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/android/location/collectionlib/bb;-><init>(Lcom/google/android/location/collectionlib/ba;B)V

    :goto_1
    iput-object v0, p0, Lcom/google/android/location/collectionlib/ba;->g:Lcom/google/android/location/collectionlib/bb;

    .line 85
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x16

    if-lt v0, v1, :cond_2

    new-instance v0, Lcom/google/android/location/collectionlib/bc;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/android/location/collectionlib/bc;-><init>(Lcom/google/android/location/collectionlib/ba;B)V

    :goto_2
    iput-object v0, p0, Lcom/google/android/location/collectionlib/ba;->h:Lcom/google/android/location/collectionlib/bc;

    .line 88
    return-void

    .line 79
    :cond_0
    iput-object p6, p0, Lcom/google/android/location/collectionlib/ba;->a:Lcom/google/android/location/d/b;

    goto :goto_0

    .line 82
    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    .line 85
    :cond_2
    const/4 v0, 0x0

    goto :goto_2
.end method

.method static synthetic a(Lcom/google/android/location/collectionlib/ba;)Z
    .locals 1

    .prologue
    .line 29
    iget-boolean v0, p0, Lcom/google/android/location/collectionlib/ba;->l:Z

    return v0
.end method

.method static synthetic b(Lcom/google/android/location/collectionlib/ba;)Z
    .locals 1

    .prologue
    .line 29
    iget-boolean v0, p0, Lcom/google/android/location/collectionlib/ba;->m:Z

    return v0
.end method


# virtual methods
.method protected final a()V
    .locals 5

    .prologue
    const/16 v4, 0x16

    .line 92
    iget-boolean v0, p0, Lcom/google/android/location/collectionlib/ba;->k:Z

    if-eqz v0, :cond_0

    .line 93
    iget-object v0, p0, Lcom/google/android/location/collectionlib/ba;->a:Lcom/google/android/location/d/b;

    iget-object v1, p0, Lcom/google/android/location/collectionlib/ba;->b:Ljava/lang/String;

    sget-object v2, Lcom/google/android/location/d/c;->c:Lcom/google/android/location/d/c;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/location/d/b;->a(Ljava/lang/String;Lcom/google/android/location/d/c;)V

    iget-object v0, v0, Lcom/google/android/location/d/b;->b:Landroid/location/LocationManager;

    invoke-virtual {v0, p0}, Landroid/location/LocationManager;->addGpsStatusListener(Landroid/location/GpsStatus$Listener;)Z

    .line 95
    :cond_0
    iget-object v0, p0, Lcom/google/android/location/collectionlib/ba;->a:Lcom/google/android/location/d/b;

    if-eqz v0, :cond_1

    .line 96
    iget-object v0, p0, Lcom/google/android/location/collectionlib/ba;->a:Lcom/google/android/location/d/b;

    iget-object v1, p0, Lcom/google/android/location/collectionlib/ba;->b:Ljava/lang/String;

    const-string v2, "gps"

    iget-object v3, p0, Lcom/google/android/location/collectionlib/ce;->d:Lcom/google/android/location/collectionlib/ak;

    invoke-virtual {v3}, Lcom/google/android/location/collectionlib/ak;->getLooper()Landroid/os/Looper;

    move-result-object v3

    invoke-virtual {v0, v1, v2, p0, v3}, Lcom/google/android/location/d/b;->a(Ljava/lang/String;Ljava/lang/String;Landroid/location/LocationListener;Landroid/os/Looper;)V

    .line 99
    :cond_1
    iget-object v0, p0, Lcom/google/android/location/collectionlib/ba;->e:Lcom/google/android/location/collectionlib/ar;

    if-eqz v0, :cond_2

    .line 100
    iget-object v0, p0, Lcom/google/android/location/collectionlib/ba;->e:Lcom/google/android/location/collectionlib/ar;

    invoke-interface {v0}, Lcom/google/android/location/collectionlib/ar;->h()V

    .line 102
    :cond_2
    iget-boolean v0, p0, Lcom/google/android/location/collectionlib/ba;->l:Z

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/location/collectionlib/ba;->g:Lcom/google/android/location/collectionlib/bb;

    if-eqz v0, :cond_3

    .line 103
    iget-object v0, p0, Lcom/google/android/location/collectionlib/ba;->a:Lcom/google/android/location/d/b;

    iget-object v1, p0, Lcom/google/android/location/collectionlib/ba;->b:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/location/collectionlib/ba;->g:Lcom/google/android/location/collectionlib/bb;

    sget-object v3, Lcom/google/android/location/d/c;->e:Lcom/google/android/location/d/c;

    invoke-virtual {v0, v1, v3}, Lcom/google/android/location/d/b;->a(Ljava/lang/String;Lcom/google/android/location/d/c;)V

    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v1, v4, :cond_3

    iget-object v0, v0, Lcom/google/android/location/d/b;->b:Landroid/location/LocationManager;

    invoke-virtual {v0, v2}, Landroid/location/LocationManager;->addGpsMeasurementListener(Landroid/location/GpsMeasurementsEvent$Listener;)Z

    .line 105
    :cond_3
    iget-boolean v0, p0, Lcom/google/android/location/collectionlib/ba;->m:Z

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/google/android/location/collectionlib/ba;->h:Lcom/google/android/location/collectionlib/bc;

    if-eqz v0, :cond_4

    .line 106
    iget-object v0, p0, Lcom/google/android/location/collectionlib/ba;->a:Lcom/google/android/location/d/b;

    iget-object v1, p0, Lcom/google/android/location/collectionlib/ba;->b:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/location/collectionlib/ba;->h:Lcom/google/android/location/collectionlib/bc;

    sget-object v3, Lcom/google/android/location/d/c;->g:Lcom/google/android/location/d/c;

    invoke-virtual {v0, v1, v3}, Lcom/google/android/location/d/b;->a(Ljava/lang/String;Lcom/google/android/location/d/c;)V

    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v1, v4, :cond_4

    iget-object v0, v0, Lcom/google/android/location/d/b;->b:Landroid/location/LocationManager;

    invoke-virtual {v0, v2}, Landroid/location/LocationManager;->addGpsNavigationMessageListener(Landroid/location/GpsNavigationMessageEvent$Listener;)Z

    .line 108
    :cond_4
    return-void
.end method

.method protected final b()V
    .locals 5

    .prologue
    const/16 v4, 0x16

    .line 112
    iget-boolean v0, p0, Lcom/google/android/location/collectionlib/ba;->k:Z

    if-eqz v0, :cond_0

    .line 113
    iget-object v0, p0, Lcom/google/android/location/collectionlib/ba;->a:Lcom/google/android/location/d/b;

    iget-object v1, p0, Lcom/google/android/location/collectionlib/ba;->b:Ljava/lang/String;

    sget-object v2, Lcom/google/android/location/d/c;->d:Lcom/google/android/location/d/c;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/location/d/b;->a(Ljava/lang/String;Lcom/google/android/location/d/c;)V

    iget-object v0, v0, Lcom/google/android/location/d/b;->b:Landroid/location/LocationManager;

    invoke-virtual {v0, p0}, Landroid/location/LocationManager;->removeGpsStatusListener(Landroid/location/GpsStatus$Listener;)V

    .line 115
    :cond_0
    iget-object v0, p0, Lcom/google/android/location/collectionlib/ba;->a:Lcom/google/android/location/d/b;

    if-eqz v0, :cond_1

    .line 116
    iget-object v0, p0, Lcom/google/android/location/collectionlib/ba;->a:Lcom/google/android/location/d/b;

    iget-object v1, p0, Lcom/google/android/location/collectionlib/ba;->b:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2, p0}, Lcom/google/android/location/d/b;->a(Ljava/lang/String;ZLandroid/location/LocationListener;)V

    .line 118
    :cond_1
    iget-object v0, p0, Lcom/google/android/location/collectionlib/ba;->e:Lcom/google/android/location/collectionlib/ar;

    if-eqz v0, :cond_2

    .line 119
    iget-object v0, p0, Lcom/google/android/location/collectionlib/ba;->e:Lcom/google/android/location/collectionlib/ar;

    invoke-interface {v0}, Lcom/google/android/location/collectionlib/ar;->i()V

    .line 121
    :cond_2
    iget-boolean v0, p0, Lcom/google/android/location/collectionlib/ba;->l:Z

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/location/collectionlib/ba;->g:Lcom/google/android/location/collectionlib/bb;

    if-eqz v0, :cond_3

    .line 122
    iget-object v0, p0, Lcom/google/android/location/collectionlib/ba;->a:Lcom/google/android/location/d/b;

    iget-object v1, p0, Lcom/google/android/location/collectionlib/ba;->b:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/location/collectionlib/ba;->g:Lcom/google/android/location/collectionlib/bb;

    sget-object v3, Lcom/google/android/location/d/c;->f:Lcom/google/android/location/d/c;

    invoke-virtual {v0, v1, v3}, Lcom/google/android/location/d/b;->a(Ljava/lang/String;Lcom/google/android/location/d/c;)V

    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v1, v4, :cond_3

    iget-object v0, v0, Lcom/google/android/location/d/b;->b:Landroid/location/LocationManager;

    invoke-virtual {v0, v2}, Landroid/location/LocationManager;->removeGpsMeasurementListener(Landroid/location/GpsMeasurementsEvent$Listener;)V

    .line 124
    :cond_3
    iget-boolean v0, p0, Lcom/google/android/location/collectionlib/ba;->m:Z

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/google/android/location/collectionlib/ba;->h:Lcom/google/android/location/collectionlib/bc;

    if-eqz v0, :cond_4

    .line 125
    iget-object v0, p0, Lcom/google/android/location/collectionlib/ba;->a:Lcom/google/android/location/d/b;

    iget-object v1, p0, Lcom/google/android/location/collectionlib/ba;->b:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/location/collectionlib/ba;->h:Lcom/google/android/location/collectionlib/bc;

    sget-object v3, Lcom/google/android/location/d/c;->h:Lcom/google/android/location/d/c;

    invoke-virtual {v0, v1, v3}, Lcom/google/android/location/d/b;->a(Ljava/lang/String;Lcom/google/android/location/d/c;)V

    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v1, v4, :cond_4

    iget-object v0, v0, Lcom/google/android/location/d/b;->b:Landroid/location/LocationManager;

    invoke-virtual {v0, v2}, Landroid/location/LocationManager;->removeGpsNavigationMessageListener(Landroid/location/GpsNavigationMessageEvent$Listener;)V

    .line 127
    :cond_4
    return-void
.end method

.method public final onGpsStatusChanged(I)V
    .locals 4

    .prologue
    .line 169
    iget-boolean v0, p0, Lcom/google/android/location/collectionlib/ba;->k:Z

    if-nez v0, :cond_1

    .line 185
    :cond_0
    :goto_0
    return-void

    .line 172
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/location/collectionlib/ba;->f()V

    .line 173
    invoke-virtual {p0}, Lcom/google/android/location/collectionlib/ba;->e()Z

    move-result v0

    if-nez v0, :cond_0

    .line 176
    const/4 v0, 0x4

    if-ne p1, v0, :cond_0

    .line 179
    iget-object v0, p0, Lcom/google/android/location/collectionlib/ba;->a:Lcom/google/android/location/d/b;

    iget-object v1, p0, Lcom/google/android/location/collectionlib/ba;->n:Landroid/location/GpsStatus;

    iget-object v0, v0, Lcom/google/android/location/d/b;->b:Landroid/location/LocationManager;

    invoke-virtual {v0, v1}, Landroid/location/LocationManager;->getGpsStatus(Landroid/location/GpsStatus;)Landroid/location/GpsStatus;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/collectionlib/ba;->n:Landroid/location/GpsStatus;

    .line 181
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    .line 182
    iget-object v2, p0, Lcom/google/android/location/collectionlib/ce;->d:Lcom/google/android/location/collectionlib/ak;

    iget-object v3, p0, Lcom/google/android/location/collectionlib/ba;->n:Landroid/location/GpsStatus;

    invoke-virtual {v2, v3, v0, v1}, Lcom/google/android/location/collectionlib/ak;->a(Landroid/location/GpsStatus;J)V

    .line 183
    sget-object v2, Lcom/google/android/location/collectionlib/cg;->h:Lcom/google/android/location/collectionlib/cg;

    const/4 v3, 0x0

    invoke-virtual {p0, v2, v0, v1, v3}, Lcom/google/android/location/collectionlib/ba;->b(Lcom/google/android/location/collectionlib/cg;JLandroid/hardware/SensorEvent;)V

    goto :goto_0
.end method

.method public final onLocationChanged(Landroid/location/Location;)V
    .locals 6

    .prologue
    .line 131
    iget-boolean v0, p0, Lcom/google/android/location/collectionlib/ba;->j:Z

    if-nez v0, :cond_1

    .line 151
    :cond_0
    :goto_0
    return-void

    .line 134
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/location/collectionlib/ba;->f()V

    .line 135
    invoke-virtual {p0}, Lcom/google/android/location/collectionlib/ba;->e()Z

    move-result v0

    if-nez v0, :cond_0

    .line 138
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    .line 139
    iget-object v2, p0, Lcom/google/android/location/collectionlib/ce;->d:Lcom/google/android/location/collectionlib/ak;

    invoke-virtual {v2, p1, v0, v1}, Lcom/google/android/location/collectionlib/ak;->a(Landroid/location/Location;J)V

    .line 140
    sget-object v2, Lcom/google/android/location/collectionlib/cg;->g:Lcom/google/android/location/collectionlib/cg;

    const/4 v3, 0x0

    invoke-virtual {p0, v2, v0, v1, v3}, Lcom/google/android/location/collectionlib/ba;->b(Lcom/google/android/location/collectionlib/cg;JLandroid/hardware/SensorEvent;)V

    .line 143
    iget-boolean v0, p0, Lcom/google/android/location/collectionlib/ba;->i:Z

    if-nez v0, :cond_0

    .line 144
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/location/collectionlib/ba;->i:Z

    .line 145
    new-instance v0, Landroid/hardware/GeomagneticField;

    invoke-virtual {p1}, Landroid/location/Location;->getLatitude()D

    move-result-wide v2

    double-to-float v1, v2

    invoke-virtual {p1}, Landroid/location/Location;->getLongitude()D

    move-result-wide v2

    double-to-float v2, v2

    invoke-virtual {p1}, Landroid/location/Location;->getAltitude()D

    move-result-wide v4

    double-to-float v3, v4

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-direct/range {v0 .. v5}, Landroid/hardware/GeomagneticField;-><init>(FFFJ)V

    .line 148
    iget-object v1, p0, Lcom/google/android/location/collectionlib/ce;->d:Lcom/google/android/location/collectionlib/ak;

    invoke-virtual {v0}, Landroid/hardware/GeomagneticField;->getX()F

    move-result v2

    invoke-virtual {v0}, Landroid/hardware/GeomagneticField;->getY()F

    move-result v3

    invoke-virtual {v0}, Landroid/hardware/GeomagneticField;->getZ()F

    move-result v4

    invoke-virtual {v0}, Landroid/hardware/GeomagneticField;->getDeclination()F

    move-result v0

    invoke-virtual {v1, v2, v3, v4, v0}, Lcom/google/android/location/collectionlib/ak;->a(FFFF)V

    goto :goto_0
.end method

.method public final onProviderDisabled(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 156
    return-void
.end method

.method public final onProviderEnabled(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 161
    return-void
.end method

.method public final onStatusChanged(Ljava/lang/String;ILandroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 165
    return-void
.end method
