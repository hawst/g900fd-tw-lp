.class final Lcom/google/android/location/reporting/service/g;
.super Lcom/google/android/location/reporting/service/h;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/location/reporting/service/DispatchingService;

.field private final c:Lcom/google/android/location/reporting/k;


# direct methods
.method public constructor <init>(Lcom/google/android/location/reporting/service/DispatchingService;Landroid/content/Context;Landroid/os/Looper;)V
    .locals 8

    .prologue
    .line 578
    iput-object p1, p0, Lcom/google/android/location/reporting/service/g;->a:Lcom/google/android/location/reporting/service/DispatchingService;

    .line 579
    const/4 v0, 0x0

    invoke-direct {p0, p1, p3, v0}, Lcom/google/android/location/reporting/service/h;-><init>(Lcom/google/android/location/reporting/service/DispatchingService;Landroid/os/Looper;B)V

    .line 581
    new-instance v0, Lcom/google/android/location/reporting/k;

    invoke-static {p1}, Lcom/google/android/location/reporting/service/DispatchingService;->i(Lcom/google/android/location/reporting/service/DispatchingService;)Lcom/google/android/location/reporting/config/h;

    move-result-object v2

    invoke-static {p1}, Lcom/google/android/location/reporting/service/DispatchingService;->d(Lcom/google/android/location/reporting/service/DispatchingService;)Lcom/google/android/location/reporting/s;

    move-result-object v3

    invoke-static {p1}, Lcom/google/android/location/reporting/service/DispatchingService;->g(Lcom/google/android/location/reporting/service/DispatchingService;)Lcom/google/android/location/reporting/LocationRecordStore;

    move-result-object v4

    invoke-static {p1}, Lcom/google/android/location/reporting/service/DispatchingService;->h(Lcom/google/android/location/reporting/service/DispatchingService;)Lcom/google/android/location/reporting/DetectedActivityStore;

    move-result-object v5

    invoke-static {p1}, Lcom/google/android/location/reporting/service/DispatchingService;->e(Lcom/google/android/location/reporting/service/DispatchingService;)Lcom/google/android/location/reporting/ApiMetadataStore;

    move-result-object v6

    invoke-static {p1}, Lcom/google/android/location/reporting/service/DispatchingService;->k(Lcom/google/android/location/reporting/service/DispatchingService;)Lcom/google/android/location/fused/g;

    move-result-object v7

    move-object v1, p2

    invoke-direct/range {v0 .. v7}, Lcom/google/android/location/reporting/k;-><init>(Landroid/content/Context;Lcom/google/android/location/reporting/config/ConfigGetter;Lcom/google/android/location/reporting/s;Lcom/google/android/location/reporting/LocationRecordStore;Lcom/google/android/location/reporting/DetectedActivityStore;Lcom/google/android/location/reporting/ApiMetadataStore;Lcom/google/android/location/fused/g;)V

    iput-object v0, p0, Lcom/google/android/location/reporting/service/g;->c:Lcom/google/android/location/reporting/k;

    .line 583
    return-void
.end method


# virtual methods
.method protected final a(Landroid/content/Intent;)V
    .locals 4

    .prologue
    .line 586
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 587
    const-string v1, "GCoreUlr"

    const/4 v2, 0x3

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 588
    const-string v1, "GCoreUlr"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "DispatchingService.Slow dispatching "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 591
    :cond_0
    const-string v1, "com.google.android.location.reporting.UPLOAD"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 592
    iget-object v0, p0, Lcom/google/android/location/reporting/service/g;->c:Lcom/google/android/location/reporting/k;

    invoke-virtual {v0}, Lcom/google/android/location/reporting/k;->a()V

    .line 596
    :goto_0
    return-void

    .line 594
    :cond_1
    const-string v0, "GCoreUlr"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unsupported Slow action in "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/location/reporting/b/d;->f(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
