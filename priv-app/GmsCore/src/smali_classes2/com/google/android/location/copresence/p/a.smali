.class public final Lcom/google/android/location/copresence/p/a;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final a:Ljava/util/concurrent/ConcurrentMap;


# instance fields
.field private final b:Ljava/util/Map;

.field private c:J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 38
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    sput-object v0, Lcom/google/android/location/copresence/p/a;->a:Ljava/util/concurrent/ConcurrentMap;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    .line 47
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/location/copresence/p/a;->b:Ljava/util/Map;

    .line 44
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/location/copresence/p/a;->c:J

    .line 47
    return-void
.end method

.method public static declared-synchronized a(Lcom/google/android/location/copresence/a/a;)Lcom/google/android/location/copresence/p/a;
    .locals 3

    .prologue
    .line 50
    const-class v1, Lcom/google/android/location/copresence/p/a;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/google/android/location/copresence/p/a;->a:Ljava/util/concurrent/ConcurrentMap;

    invoke-interface {v0, p0}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/copresence/p/a;

    .line 51
    if-nez v0, :cond_0

    .line 52
    new-instance v0, Lcom/google/android/location/copresence/p/a;

    invoke-direct {v0}, Lcom/google/android/location/copresence/p/a;-><init>()V

    .line 53
    sget-object v2, Lcom/google/android/location/copresence/p/a;->a:Ljava/util/concurrent/ConcurrentMap;

    invoke-interface {v2, p0, v0}, Ljava/util/concurrent/ConcurrentMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 55
    :cond_0
    monitor-exit v1

    return-object v0

    .line 50
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private declared-synchronized a(Lcom/google/android/gms/location/copresence/x;ZLjava/util/Set;)V
    .locals 2

    .prologue
    .line 142
    monitor-enter p0

    :try_start_0
    new-instance v0, Lcom/google/android/location/copresence/p/b;

    invoke-direct {v0, p1, p2, p3}, Lcom/google/android/location/copresence/p/b;-><init>(Lcom/google/android/gms/location/copresence/x;ZLjava/util/Set;)V

    .line 143
    iget-object v1, p0, Lcom/google/android/location/copresence/p/a;->b:Ljava/util/Map;

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 144
    monitor-exit p0

    return-void

    .line 142
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized c()V
    .locals 8

    .prologue
    .line 319
    monitor-enter p0

    :try_start_0
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    .line 320
    invoke-static {}, Lcom/google/android/location/copresence/f/b;->b()Lcom/google/ac/b/c/g;

    move-result-object v0

    iget-object v0, v0, Lcom/google/ac/b/c/g;->k:Lcom/google/ac/b/c/n;

    iget-object v0, v0, Lcom/google/ac/b/c/n;->a:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    .line 321
    iget-wide v4, p0, Lcom/google/android/location/copresence/p/a;->c:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    sub-long v4, v2, v4

    cmp-long v0, v4, v0

    if-gez v0, :cond_1

    .line 331
    :cond_0
    monitor-exit p0

    return-void

    .line 324
    :cond_1
    :try_start_1
    iput-wide v2, p0, Lcom/google/android/location/copresence/p/a;->c:J

    .line 325
    iget-object v0, p0, Lcom/google/android/location/copresence/p/a;->b:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_2
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 326
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/location/copresence/p/b;

    iget-wide v6, v1, Lcom/google/android/location/copresence/p/b;->a:J

    cmp-long v1, v2, v6

    if-lez v1, :cond_3

    const/4 v1, 0x1

    :goto_1
    if-eqz v1, :cond_2

    .line 327
    iget-object v1, p0, Lcom/google/android/location/copresence/p/a;->b:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 319
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 326
    :cond_3
    const/4 v1, 0x0

    goto :goto_1
.end method

.method private static d(Lcom/google/android/gms/location/copresence/x;)Z
    .locals 1

    .prologue
    .line 336
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/gms/location/copresence/x;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final declared-synchronized a()Ljava/util/Set;
    .locals 5

    .prologue
    .line 249
    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, Lcom/google/android/location/copresence/p/a;->c()V

    .line 250
    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    .line 251
    iget-object v0, p0, Lcom/google/android/location/copresence/p/a;->b:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 252
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/location/copresence/p/b;

    iget v1, v1, Lcom/google/android/location/copresence/p/b;->i:I

    const/4 v4, 0x1

    if-ne v1, v4, :cond_0

    .line 254
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 249
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 257
    :cond_1
    monitor-exit p0

    return-object v2
.end method

.method public final declared-synchronized a(Landroid/content/Context;Lcom/google/android/gms/location/copresence/x;I)Z
    .locals 5

    .prologue
    .line 204
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/location/copresence/p/a;->b:Ljava/util/Map;

    invoke-interface {v0, p2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 205
    iget-object v0, p0, Lcom/google/android/location/copresence/p/a;->b:Ljava/util/Map;

    invoke-interface {v0, p2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/copresence/p/b;

    .line 206
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    iget-object v1, v0, Lcom/google/android/location/copresence/p/b;->h:Ljava/util/List;

    invoke-virtual {v0, v1}, Lcom/google/android/location/copresence/p/b;->a(Ljava/util/List;)V

    iget-object v1, v0, Lcom/google/android/location/copresence/p/b;->h:Ljava/util/List;

    new-instance v4, Lcom/google/android/location/copresence/p/d;

    invoke-direct {v4, v0, v2, v3, p3}, Lcom/google/android/location/copresence/p/d;-><init>(Lcom/google/android/location/copresence/p/b;JI)V

    invoke-interface {v1, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iput p3, v0, Lcom/google/android/location/copresence/p/b;->i:I

    .line 210
    if-eqz p1, :cond_0

    .line 211
    new-instance v0, Lcom/google/android/location/copresence/debug/CopresDatabaseHelper$TokenData;

    invoke-direct {v0}, Lcom/google/android/location/copresence/debug/CopresDatabaseHelper$TokenData;-><init>()V

    .line 212
    invoke-virtual {p2}, Lcom/google/android/gms/location/copresence/x;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/google/android/location/copresence/debug/CopresDatabaseHelper$TokenData;->tokenId:Ljava/lang/String;

    .line 213
    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, v0, Lcom/google/android/location/copresence/debug/CopresDatabaseHelper$TokenData;->networkState:Ljava/lang/Integer;

    .line 215
    invoke-static {p1, v0}, Lcom/google/android/location/copresence/debug/CopresDatabaseHelper;->a(Landroid/content/Context;Lcom/google/android/location/copresence/debug/CopresDatabaseHelper$TokenData;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 218
    :cond_0
    const/4 v0, 0x1

    .line 221
    :goto_0
    monitor-exit p0

    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 204
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Landroid/content/Context;Lcom/google/android/gms/location/copresence/x;Lcom/google/ac/b/c/bu;)Z
    .locals 8

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 156
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/location/copresence/p/a;->b:Ljava/util/Map;

    invoke-interface {v0, p2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 157
    iget-object v0, p0, Lcom/google/android/location/copresence/p/a;->b:Ljava/util/Map;

    invoke-interface {v0, p2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/copresence/p/b;

    .line 158
    if-eqz p3, :cond_0

    iget-object v3, p3, Lcom/google/ac/b/c/bu;->b:[Lcom/google/ac/b/c/bv;

    if-eqz v3, :cond_0

    iget-object v3, p3, Lcom/google/ac/b/c/bu;->b:[Lcom/google/ac/b/c/bv;

    array-length v4, v3

    :goto_0
    if-ge v2, v4, :cond_0

    aget-object v5, v3, v2

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    iput-object v6, v5, Lcom/google/ac/b/c/bv;->c:Ljava/lang/Long;

    iget-object v6, v5, Lcom/google/ac/b/c/bv;->a:Ljava/lang/Integer;

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v6

    packed-switch v6, :pswitch_data_0

    :goto_1
    :pswitch_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :pswitch_1
    iget-object v6, v0, Lcom/google/android/location/copresence/p/b;->d:Ljava/util/List;

    invoke-virtual {v0, v6}, Lcom/google/android/location/copresence/p/b;->a(Ljava/util/List;)V

    iget-object v6, v0, Lcom/google/android/location/copresence/p/b;->d:Ljava/util/List;

    invoke-interface {v6, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    .line 156
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 158
    :pswitch_2
    :try_start_1
    iget-object v6, v0, Lcom/google/android/location/copresence/p/b;->e:Ljava/util/List;

    invoke-virtual {v0, v6}, Lcom/google/android/location/copresence/p/b;->a(Ljava/util/List;)V

    iget-object v6, v0, Lcom/google/android/location/copresence/p/b;->e:Ljava/util/List;

    invoke-interface {v6, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :pswitch_3
    iget-object v6, v0, Lcom/google/android/location/copresence/p/b;->f:Ljava/util/List;

    invoke-virtual {v0, v6}, Lcom/google/android/location/copresence/p/b;->a(Ljava/util/List;)V

    iget-object v6, v0, Lcom/google/android/location/copresence/p/b;->f:Ljava/util/List;

    invoke-interface {v6, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :pswitch_4
    iget-object v6, v0, Lcom/google/android/location/copresence/p/b;->g:Ljava/util/List;

    invoke-virtual {v0, v6}, Lcom/google/android/location/copresence/p/b;->a(Ljava/util/List;)V

    iget-object v6, v0, Lcom/google/android/location/copresence/p/b;->g:Ljava/util/List;

    invoke-interface {v6, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 162
    :cond_0
    if-eqz p1, :cond_1

    iget-object v0, p3, Lcom/google/ac/b/c/bu;->b:[Lcom/google/ac/b/c/bv;

    if-eqz v0, :cond_1

    iget-object v0, p3, Lcom/google/ac/b/c/bu;->b:[Lcom/google/ac/b/c/bv;

    array-length v0, v0

    if-ne v0, v1, :cond_1

    .line 163
    new-instance v0, Lcom/google/android/location/copresence/debug/CopresDatabaseHelper$TokenData;

    invoke-direct {v0}, Lcom/google/android/location/copresence/debug/CopresDatabaseHelper$TokenData;-><init>()V

    .line 164
    invoke-virtual {p2}, Lcom/google/android/gms/location/copresence/x;->a()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v0, Lcom/google/android/location/copresence/debug/CopresDatabaseHelper$TokenData;->tokenId:Ljava/lang/String;

    .line 165
    iget-object v2, p3, Lcom/google/ac/b/c/bu;->b:[Lcom/google/ac/b/c/bv;

    const/4 v3, 0x0

    aget-object v2, v2, v3

    iget-object v2, v2, Lcom/google/ac/b/c/bv;->a:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    packed-switch v2, :pswitch_data_1

    .line 179
    :goto_2
    :pswitch_5
    invoke-static {p1, v0}, Lcom/google/android/location/copresence/debug/CopresDatabaseHelper;->a(Landroid/content/Context;Lcom/google/android/location/copresence/debug/CopresDatabaseHelper$TokenData;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_1
    move v0, v1

    .line 189
    :goto_3
    monitor-exit p0

    return v0

    .line 168
    :pswitch_6
    const/4 v2, 0x1

    :try_start_2
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    iput-object v2, v0, Lcom/google/android/location/copresence/debug/CopresDatabaseHelper$TokenData;->fromAudio:Ljava/lang/Boolean;

    goto :goto_2

    .line 171
    :pswitch_7
    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    iput-object v2, v0, Lcom/google/android/location/copresence/debug/CopresDatabaseHelper$TokenData;->fromBt:Ljava/lang/Boolean;

    goto :goto_2

    .line 174
    :pswitch_8
    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    iput-object v2, v0, Lcom/google/android/location/copresence/debug/CopresDatabaseHelper$TokenData;->fromBle:Ljava/lang/Boolean;

    goto :goto_2

    .line 178
    :pswitch_9
    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    iput-object v2, v0, Lcom/google/android/location/copresence/debug/CopresDatabaseHelper$TokenData;->fromWifi:Ljava/lang/Boolean;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_2

    :cond_2
    move v0, v2

    .line 189
    goto :goto_3

    .line 158
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_3
        :pswitch_2
        :pswitch_4
        :pswitch_0
        :pswitch_2
        :pswitch_4
        :pswitch_1
    .end packed-switch

    .line 165
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_5
        :pswitch_5
        :pswitch_9
        :pswitch_6
    .end packed-switch
.end method

.method public final declared-synchronized a(Landroid/content/Context;Lcom/google/android/gms/location/copresence/x;Ljava/util/Set;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 93
    monitor-enter p0

    :try_start_0
    invoke-static {p2}, Lcom/google/android/location/copresence/p/a;->d(Lcom/google/android/gms/location/copresence/x;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v2

    if-nez v2, :cond_1

    .line 121
    :cond_0
    :goto_0
    monitor-exit p0

    return v0

    .line 96
    :cond_1
    :try_start_1
    iget-object v2, p0, Lcom/google/android/location/copresence/p/a;->b:Ljava/util/Map;

    invoke-interface {v2, p2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    if-nez v2, :cond_0

    .line 102
    if-eqz p1, :cond_5

    .line 103
    new-instance v3, Lcom/google/android/location/copresence/debug/CopresDatabaseHelper$TokenData;

    invoke-direct {v3}, Lcom/google/android/location/copresence/debug/CopresDatabaseHelper$TokenData;-><init>()V

    .line 104
    invoke-virtual {p2}, Lcom/google/android/gms/location/copresence/x;->a()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v3, Lcom/google/android/location/copresence/debug/CopresDatabaseHelper$TokenData;->tokenId:Ljava/lang/String;

    .line 105
    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    iput-object v2, v3, Lcom/google/android/location/copresence/debug/CopresDatabaseHelper$TokenData;->isExpired:Ljava/lang/Boolean;

    .line 106
    const/16 v2, 0x8

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {p3, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {p3, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_6

    :cond_2
    move v2, v1

    :goto_1
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    iput-object v2, v3, Lcom/google/android/location/copresence/debug/CopresDatabaseHelper$TokenData;->broadcastAudio:Ljava/lang/Boolean;

    .line 108
    const/4 v2, 0x2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {p3, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    iput-object v2, v3, Lcom/google/android/location/copresence/debug/CopresDatabaseHelper$TokenData;->broadcastBt:Ljava/lang/Boolean;

    .line 109
    const/4 v2, 0x3

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {p3, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    iput-object v2, v3, Lcom/google/android/location/copresence/debug/CopresDatabaseHelper$TokenData;->broadcastBle:Ljava/lang/Boolean;

    .line 110
    const/4 v2, 0x4

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {p3, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    const/4 v2, 0x7

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {p3, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    :cond_3
    move v0, v1

    :cond_4
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, v3, Lcom/google/android/location/copresence/debug/CopresDatabaseHelper$TokenData;->broadcastWifi:Ljava/lang/Boolean;

    .line 112
    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, v3, Lcom/google/android/location/copresence/debug/CopresDatabaseHelper$TokenData;->wasInitiated:Ljava/lang/Boolean;

    .line 113
    const/4 v0, 0x3

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, v3, Lcom/google/android/location/copresence/debug/CopresDatabaseHelper$TokenData;->networkState:Ljava/lang/Integer;

    .line 115
    invoke-static {p1, v3}, Lcom/google/android/location/copresence/debug/CopresDatabaseHelper;->a(Landroid/content/Context;Lcom/google/android/location/copresence/debug/CopresDatabaseHelper$TokenData;)V

    .line 118
    :cond_5
    const/4 v0, 0x1

    invoke-direct {p0, p2, v0, p3}, Lcom/google/android/location/copresence/p/a;->a(Lcom/google/android/gms/location/copresence/x;ZLjava/util/Set;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move v0, v1

    .line 121
    goto/16 :goto_0

    :cond_6
    move v2, v0

    .line 106
    goto :goto_1

    .line 93
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Lcom/google/android/gms/location/copresence/x;)Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 68
    monitor-enter p0

    :try_start_0
    invoke-static {p1}, Lcom/google/android/location/copresence/p/a;->d(Lcom/google/android/gms/location/copresence/x;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    if-nez v1, :cond_1

    .line 77
    :cond_0
    :goto_0
    monitor-exit p0

    return v0

    .line 71
    :cond_1
    :try_start_1
    iget-object v1, p0, Lcom/google/android/location/copresence/p/a;->b:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    if-nez v1, :cond_0

    .line 74
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/google/android/location/copresence/p/a;->a(Lcom/google/android/gms/location/copresence/x;ZLjava/util/Set;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 77
    const/4 v0, 0x1

    goto :goto_0

    .line 68
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Lcom/google/android/gms/location/copresence/x;I)Z
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 284
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/location/copresence/p/a;->b:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/google/android/location/copresence/p/a;->b:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/copresence/p/b;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    packed-switch p2, :pswitch_data_0

    :pswitch_0
    move v0, v2

    :goto_0
    if-eqz v0, :cond_4

    move v0, v1

    :goto_1
    monitor-exit p0

    return v0

    :pswitch_1
    :try_start_1
    iget-object v0, v0, Lcom/google/android/location/copresence/p/b;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_0

    move v0, v1

    goto :goto_0

    :cond_0
    move v0, v2

    goto :goto_0

    :pswitch_2
    iget-object v0, v0, Lcom/google/android/location/copresence/p/b;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_1

    move v0, v1

    goto :goto_0

    :cond_1
    move v0, v2

    goto :goto_0

    :pswitch_3
    iget-object v0, v0, Lcom/google/android/location/copresence/p/b;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    move v0, v2

    goto :goto_0

    :pswitch_4
    iget-object v0, v0, Lcom/google/android/location/copresence/p/b;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v0

    if-lez v0, :cond_3

    move v0, v1

    goto :goto_0

    :cond_3
    move v0, v2

    goto :goto_0

    :cond_4
    move v0, v2

    goto :goto_1

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_3
        :pswitch_2
        :pswitch_4
        :pswitch_0
        :pswitch_2
        :pswitch_4
        :pswitch_1
    .end packed-switch
.end method

.method public final declared-synchronized b(Lcom/google/android/gms/location/copresence/x;)I
    .locals 1

    .prologue
    .line 132
    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, Lcom/google/android/location/copresence/p/a;->c()V

    .line 133
    iget-object v0, p0, Lcom/google/android/location/copresence/p/a;->b:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_0

    .line 134
    const/4 v0, 0x0

    .line 136
    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/google/android/location/copresence/p/a;->b:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/copresence/p/b;

    iget v0, v0, Lcom/google/android/location/copresence/p/b;->i:I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 132
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b()[Lcom/google/ac/b/c/bu;
    .locals 6

    .prologue
    .line 266
    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, Lcom/google/android/location/copresence/p/a;->c()V

    .line 267
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 268
    iget-object v0, p0, Lcom/google/android/location/copresence/p/a;->b:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 269
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/location/copresence/p/b;

    iget v1, v1, Lcom/google/android/location/copresence/p/b;->i:I

    .line 270
    const/4 v4, 0x1

    if-eq v1, v4, :cond_1

    const/4 v4, 0x3

    if-ne v1, v4, :cond_0

    .line 272
    :cond_1
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/copresence/p/b;

    new-instance v1, Lcom/google/ac/b/c/bu;

    invoke-direct {v1}, Lcom/google/ac/b/c/bu;-><init>()V

    iget-object v4, v0, Lcom/google/android/location/copresence/p/b;->b:Lcom/google/android/gms/location/copresence/x;

    invoke-virtual {v4}, Lcom/google/android/gms/location/copresence/x;->a()Ljava/lang/String;

    move-result-object v4

    iput-object v4, v1, Lcom/google/ac/b/c/bu;->a:Ljava/lang/String;

    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    iget-object v5, v0, Lcom/google/android/location/copresence/p/b;->d:Ljava/util/List;

    invoke-interface {v4, v5}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    iget-object v5, v0, Lcom/google/android/location/copresence/p/b;->e:Ljava/util/List;

    invoke-interface {v4, v5}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    iget-object v5, v0, Lcom/google/android/location/copresence/p/b;->f:Ljava/util/List;

    invoke-interface {v4, v5}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    iget-object v0, v0, Lcom/google/android/location/copresence/p/b;->g:Ljava/util/List;

    invoke-interface {v4, v0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v0

    new-array v0, v0, [Lcom/google/ac/b/c/bv;

    invoke-interface {v4, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/ac/b/c/bv;

    iput-object v0, v1, Lcom/google/ac/b/c/bu;->b:[Lcom/google/ac/b/c/bv;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 266
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 275
    :cond_2
    :try_start_1
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v0

    new-array v0, v0, [Lcom/google/ac/b/c/bu;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/ac/b/c/bu;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit p0

    return-object v0
.end method

.method public final declared-synchronized c(Lcom/google/android/gms/location/copresence/x;)Z
    .locals 1

    .prologue
    .line 288
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/location/copresence/p/a;->b:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/location/copresence/p/a;->b:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/copresence/p/b;

    iget-object v0, v0, Lcom/google/android/location/copresence/p/b;->c:Lcom/google/android/location/copresence/p/c;

    iget-boolean v0, v0, Lcom/google/android/location/copresence/p/c;->a:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
