.class final Lcom/google/android/location/collectionlib/ay;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/location/collectionlib/ca;


# instance fields
.field private A:F

.field private B:F

.field private C:F

.field private D:J

.field private E:J

.field private F:J

.field private G:J

.field private H:J

.field private I:J

.field private J:J

.field private K:J

.field private L:J

.field private M:J

.field private N:J

.field private O:J

.field private P:J

.field private Q:J

.field private R:J

.field private S:J

.field private T:J

.field private U:J

.field private V:J

.field private W:Lcom/google/android/location/o/a/c;

.field private X:Lcom/google/android/location/collectionlib/az;

.field private final Y:Lcom/google/p/a/b/b/a;

.field a:Lcom/google/p/a/b/b/a;

.field b:Lcom/google/p/a/b/b/a;

.field c:Lcom/google/p/a/b/b/a;

.field private final d:Ljava/lang/String;

.field private final e:Ljava/lang/String;

.field private final f:Ljava/lang/Integer;

.field private g:Z

.field private h:J

.field private i:I

.field private j:Z

.field private final k:J

.field private l:J

.field private m:J

.field private n:J

.field private o:J

.field private final p:Lcom/google/android/location/collectionlib/cc;

.field private q:I

.field private r:I

.field private s:I

.field private t:I

.field private u:I

.field private v:I

.field private w:I

.field private x:I

.field private y:I

.field private z:I


# direct methods
.method public constructor <init>(Lcom/google/android/location/collectionlib/cc;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Lcom/google/p/a/b/b/a;Lcom/google/android/location/o/a/c;)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    const v5, 0x7f7fffff    # Float.MAX_VALUE

    const/4 v4, -0x1

    const-wide v2, 0x7fffffffffffffffL

    .line 199
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 96
    iput-boolean v6, p0, Lcom/google/android/location/collectionlib/ay;->g:Z

    .line 102
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/location/collectionlib/ay;->h:J

    .line 104
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/location/collectionlib/ay;->i:I

    .line 110
    iput-boolean v6, p0, Lcom/google/android/location/collectionlib/ay;->j:Z

    .line 121
    iput-wide v2, p0, Lcom/google/android/location/collectionlib/ay;->l:J

    .line 126
    iput-wide v2, p0, Lcom/google/android/location/collectionlib/ay;->m:J

    .line 131
    iput-wide v2, p0, Lcom/google/android/location/collectionlib/ay;->n:J

    .line 136
    iput-wide v2, p0, Lcom/google/android/location/collectionlib/ay;->o:J

    .line 143
    iput v4, p0, Lcom/google/android/location/collectionlib/ay;->q:I

    .line 144
    iput v4, p0, Lcom/google/android/location/collectionlib/ay;->r:I

    .line 145
    iput v4, p0, Lcom/google/android/location/collectionlib/ay;->s:I

    .line 146
    iput v4, p0, Lcom/google/android/location/collectionlib/ay;->t:I

    .line 147
    iput v4, p0, Lcom/google/android/location/collectionlib/ay;->u:I

    .line 148
    iput v4, p0, Lcom/google/android/location/collectionlib/ay;->v:I

    .line 149
    iput v4, p0, Lcom/google/android/location/collectionlib/ay;->w:I

    .line 150
    iput v4, p0, Lcom/google/android/location/collectionlib/ay;->x:I

    .line 151
    iput v4, p0, Lcom/google/android/location/collectionlib/ay;->y:I

    .line 152
    iput v4, p0, Lcom/google/android/location/collectionlib/ay;->z:I

    .line 154
    iput v5, p0, Lcom/google/android/location/collectionlib/ay;->A:F

    .line 155
    iput v5, p0, Lcom/google/android/location/collectionlib/ay;->B:F

    .line 156
    iput v5, p0, Lcom/google/android/location/collectionlib/ay;->C:F

    .line 158
    iput-wide v2, p0, Lcom/google/android/location/collectionlib/ay;->D:J

    .line 159
    iput-wide v2, p0, Lcom/google/android/location/collectionlib/ay;->E:J

    .line 160
    iput-wide v2, p0, Lcom/google/android/location/collectionlib/ay;->F:J

    .line 161
    iput-wide v2, p0, Lcom/google/android/location/collectionlib/ay;->G:J

    .line 162
    iput-wide v2, p0, Lcom/google/android/location/collectionlib/ay;->H:J

    .line 163
    iput-wide v2, p0, Lcom/google/android/location/collectionlib/ay;->I:J

    .line 164
    iput-wide v2, p0, Lcom/google/android/location/collectionlib/ay;->J:J

    .line 165
    iput-wide v2, p0, Lcom/google/android/location/collectionlib/ay;->K:J

    .line 166
    iput-wide v2, p0, Lcom/google/android/location/collectionlib/ay;->L:J

    .line 167
    iput-wide v2, p0, Lcom/google/android/location/collectionlib/ay;->M:J

    .line 169
    iput-wide v2, p0, Lcom/google/android/location/collectionlib/ay;->N:J

    .line 170
    iput-wide v2, p0, Lcom/google/android/location/collectionlib/ay;->O:J

    .line 171
    iput-wide v2, p0, Lcom/google/android/location/collectionlib/ay;->P:J

    .line 172
    iput-wide v2, p0, Lcom/google/android/location/collectionlib/ay;->Q:J

    .line 173
    iput-wide v2, p0, Lcom/google/android/location/collectionlib/ay;->R:J

    .line 174
    iput-wide v2, p0, Lcom/google/android/location/collectionlib/ay;->S:J

    .line 175
    iput-wide v2, p0, Lcom/google/android/location/collectionlib/ay;->T:J

    .line 176
    iput-wide v2, p0, Lcom/google/android/location/collectionlib/ay;->U:J

    .line 177
    iput-wide v2, p0, Lcom/google/android/location/collectionlib/ay;->V:J

    .line 180
    new-instance v0, Lcom/google/android/location/collectionlib/az;

    invoke-direct {v0, p0}, Lcom/google/android/location/collectionlib/az;-><init>(Lcom/google/android/location/collectionlib/ay;)V

    iput-object v0, p0, Lcom/google/android/location/collectionlib/ay;->X:Lcom/google/android/location/collectionlib/az;

    .line 200
    iput-object p1, p0, Lcom/google/android/location/collectionlib/ay;->p:Lcom/google/android/location/collectionlib/cc;

    .line 201
    invoke-static {p6}, Lcom/google/android/location/collectionlib/df;->a(Lcom/google/android/location/o/a/c;)Lcom/google/android/location/o/a/c;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/collectionlib/ay;->W:Lcom/google/android/location/o/a/c;

    .line 202
    invoke-static {p2}, Lcom/google/android/location/collectionlib/df;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/collectionlib/ay;->d:Ljava/lang/String;

    .line 203
    iput-object p3, p0, Lcom/google/android/location/collectionlib/ay;->e:Ljava/lang/String;

    .line 204
    iput-object p4, p0, Lcom/google/android/location/collectionlib/ay;->f:Ljava/lang/Integer;

    .line 205
    if-eqz p5, :cond_0

    .line 206
    sget-object v0, Lcom/google/android/location/m/a;->v:Lcom/google/p/a/b/b/c;

    invoke-virtual {p5}, Lcom/google/p/a/b/b/a;->c()Lcom/google/p/a/b/b/c;

    move-result-object v1

    if-eq v1, v0, :cond_0

    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Wrong protocol buffer type. Expected "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " but was "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p5}, Lcom/google/p/a/b/b/a;->c()Lcom/google/p/a/b/b/c;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 208
    :cond_0
    iput-object p5, p0, Lcom/google/android/location/collectionlib/ay;->Y:Lcom/google/p/a/b/b/a;

    .line 209
    invoke-direct {p0}, Lcom/google/android/location/collectionlib/ay;->d()V

    .line 210
    invoke-direct {p0}, Lcom/google/android/location/collectionlib/ay;->e()V

    .line 212
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    sub-long/2addr v0, v2

    iput-wide v0, p0, Lcom/google/android/location/collectionlib/ay;->k:J

    .line 213
    return-void
.end method

.method private a(FFFJJJ)Lcom/google/p/a/b/b/a;
    .locals 10

    .prologue
    .line 1099
    move-wide/from16 v0, p6

    invoke-direct {p0, p4, p5, v0, v1}, Lcom/google/android/location/collectionlib/ay;->a(JJ)V

    .line 1100
    new-instance v4, Lcom/google/p/a/b/b/a;

    sget-object v2, Lcom/google/android/location/m/a;->aX:Lcom/google/p/a/b/b/c;

    const/16 v3, 0x9

    invoke-direct {v4, v2, v3}, Lcom/google/p/a/b/b/a;-><init>(Lcom/google/p/a/b/b/c;I)V

    .line 1101
    const/4 v2, 0x1

    invoke-virtual {v4, v2, p1}, Lcom/google/p/a/b/b/a;->a(IF)Lcom/google/p/a/b/b/a;

    .line 1102
    const/4 v2, 0x2

    invoke-virtual {v4, v2, p2}, Lcom/google/p/a/b/b/a;->a(IF)Lcom/google/p/a/b/b/a;

    .line 1103
    const/4 v2, 0x3

    invoke-virtual {v4, v2, p3}, Lcom/google/p/a/b/b/a;->a(IF)Lcom/google/p/a/b/b/a;

    .line 1104
    iget-wide v2, p0, Lcom/google/android/location/collectionlib/ay;->P:J

    const-wide v6, 0x7fffffffffffffffL

    cmp-long v2, v2, v6

    if-nez v2, :cond_0

    .line 1105
    iget-wide v2, p0, Lcom/google/android/location/collectionlib/ay;->o:J

    sub-long/2addr v2, p4

    iget-wide v6, p0, Lcom/google/android/location/collectionlib/ay;->n:J

    sub-long v6, p6, v6

    const-wide/32 v8, 0xf4240

    mul-long/2addr v6, v8

    add-long/2addr v2, v6

    iput-wide v2, p0, Lcom/google/android/location/collectionlib/ay;->P:J

    .line 1108
    :cond_0
    iget-object v2, p0, Lcom/google/android/location/collectionlib/ay;->c:Lcom/google/p/a/b/b/a;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lcom/google/p/a/b/b/a;->i(I)Z

    move-result v2

    if-nez v2, :cond_1

    .line 1110
    iget-object v2, p0, Lcom/google/android/location/collectionlib/ay;->c:Lcom/google/p/a/b/b/a;

    const/4 v3, 0x1

    iget-wide v6, p0, Lcom/google/android/location/collectionlib/ay;->P:J

    invoke-direct {p0, p4, p5, v6, v7}, Lcom/google/android/location/collectionlib/ay;->b(JJ)J

    move-result-wide v6

    invoke-virtual {v2, v3, v6, v7}, Lcom/google/p/a/b/b/a;->b(IJ)Lcom/google/p/a/b/b/a;

    .line 1113
    :cond_1
    iget-object v2, p0, Lcom/google/android/location/collectionlib/ay;->c:Lcom/google/p/a/b/b/a;

    const/4 v3, 0x5

    invoke-virtual {v2, v3}, Lcom/google/p/a/b/b/a;->i(I)Z

    move-result v2

    if-nez v2, :cond_2

    .line 1115
    const/16 v2, 0xa

    invoke-virtual {v4, v2, p4, p5}, Lcom/google/p/a/b/b/a;->b(IJ)Lcom/google/p/a/b/b/a;

    .line 1116
    const/16 v2, 0xb

    const-wide/32 v6, 0xf4240

    mul-long v6, v6, p6

    invoke-virtual {v4, v2, v6, v7}, Lcom/google/p/a/b/b/a;->b(IJ)Lcom/google/p/a/b/b/a;

    .line 1120
    :cond_2
    const-wide v2, 0x7fffffffffffffffL

    cmp-long v2, p8, v2

    if-nez v2, :cond_5

    .line 1121
    iget-object v2, p0, Lcom/google/android/location/collectionlib/ay;->c:Lcom/google/p/a/b/b/a;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lcom/google/p/a/b/b/a;->d(I)J

    move-result-wide v6

    .line 1122
    iget-wide v2, p0, Lcom/google/android/location/collectionlib/ay;->P:J

    invoke-direct {p0, p4, p5, v2, v3}, Lcom/google/android/location/collectionlib/ay;->b(JJ)J

    move-result-wide v8

    .line 1124
    invoke-direct {p0, v6, v7, v8, v9}, Lcom/google/android/location/collectionlib/ay;->c(JJ)I

    move-result v3

    .line 1125
    invoke-direct {p0, v6, v7, v8, v9}, Lcom/google/android/location/collectionlib/ay;->d(JJ)I

    move-result v2

    .line 1130
    :goto_0
    if-eqz v3, :cond_3

    .line 1131
    const/16 v5, 0x8

    invoke-virtual {v4, v5, v3}, Lcom/google/p/a/b/b/a;->f(II)Lcom/google/p/a/b/b/a;

    .line 1133
    :cond_3
    if-eqz v2, :cond_4

    .line 1134
    const/16 v3, 0x9

    invoke-virtual {v4, v3, v2}, Lcom/google/p/a/b/b/a;->f(II)Lcom/google/p/a/b/b/a;

    .line 1136
    :cond_4
    return-object v4

    .line 1127
    :cond_5
    move-wide/from16 v0, p8

    invoke-direct {p0, v0, v1, p4, p5}, Lcom/google/android/location/collectionlib/ay;->c(JJ)I

    move-result v3

    .line 1128
    move-wide/from16 v0, p8

    invoke-direct {p0, v0, v1, p4, p5}, Lcom/google/android/location/collectionlib/ay;->d(JJ)I

    move-result v2

    goto :goto_0
.end method

.method private a(J)V
    .locals 5

    .prologue
    .line 489
    iget-wide v0, p0, Lcom/google/android/location/collectionlib/ay;->l:J

    const-wide v2, 0x7fffffffffffffffL

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 490
    iput-wide p1, p0, Lcom/google/android/location/collectionlib/ay;->l:J

    .line 492
    :cond_0
    iput-wide p1, p0, Lcom/google/android/location/collectionlib/ay;->m:J

    .line 493
    return-void
.end method

.method private a(JJ)V
    .locals 5

    .prologue
    .line 823
    iget-wide v0, p0, Lcom/google/android/location/collectionlib/ay;->o:J

    const-wide v2, 0x7fffffffffffffffL

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 824
    iput-wide p1, p0, Lcom/google/android/location/collectionlib/ay;->o:J

    .line 825
    iput-wide p3, p0, Lcom/google/android/location/collectionlib/ay;->n:J

    .line 827
    :cond_0
    return-void
.end method

.method private declared-synchronized a(Lcom/google/p/a/b/b/a;Z)V
    .locals 6

    .prologue
    .line 448
    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, Lcom/google/android/location/collectionlib/ay;->f()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/location/collectionlib/ay;->h:J

    .line 449
    new-instance v0, Lcom/google/p/a/b/b/a;

    sget-object v1, Lcom/google/android/location/m/a;->P:Lcom/google/p/a/b/b/c;

    invoke-direct {v0, v1}, Lcom/google/p/a/b/b/a;-><init>(Lcom/google/p/a/b/b/c;)V

    .line 450
    const/4 v1, 0x1

    iget-wide v2, p0, Lcom/google/android/location/collectionlib/ay;->h:J

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/p/a/b/b/a;->b(IJ)Lcom/google/p/a/b/b/a;

    .line 451
    iget-wide v2, p0, Lcom/google/android/location/collectionlib/ay;->l:J

    const-wide v4, 0x7fffffffffffffffL

    cmp-long v1, v2, v4

    if-nez v1, :cond_0

    .line 452
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/android/location/collectionlib/ay;->l:J

    .line 454
    :cond_0
    const/4 v1, 0x4

    iget-wide v2, p0, Lcom/google/android/location/collectionlib/ay;->l:J

    iget-wide v4, p0, Lcom/google/android/location/collectionlib/ay;->k:J

    add-long/2addr v2, v4

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/p/a/b/b/a;->b(IJ)Lcom/google/p/a/b/b/a;

    .line 456
    if-eqz p2, :cond_2

    .line 457
    const/4 v1, 0x3

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/google/p/a/b/b/a;->f(II)Lcom/google/p/a/b/b/a;

    .line 462
    :goto_0
    iget-object v1, p0, Lcom/google/android/location/collectionlib/ay;->p:Lcom/google/android/location/collectionlib/cc;

    if-eqz v1, :cond_1

    .line 463
    iget-object v1, p0, Lcom/google/android/location/collectionlib/ay;->p:Lcom/google/android/location/collectionlib/cc;

    invoke-virtual {v1, p1, v0}, Lcom/google/android/location/collectionlib/cc;->b(Lcom/google/p/a/b/b/a;Lcom/google/p/a/b/b/a;)Z

    .line 464
    if-eqz p2, :cond_1

    .line 465
    iget-object v0, p0, Lcom/google/android/location/collectionlib/ay;->p:Lcom/google/android/location/collectionlib/cc;

    invoke-virtual {v0}, Lcom/google/android/location/collectionlib/cc;->d()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 468
    :cond_1
    monitor-exit p0

    return-void

    .line 459
    :cond_2
    const/4 v1, 0x3

    :try_start_1
    iget v2, p0, Lcom/google/android/location/collectionlib/ay;->i:I

    invoke-virtual {v0, v1, v2}, Lcom/google/p/a/b/b/a;->f(II)Lcom/google/p/a/b/b/a;

    .line 460
    iget v1, p0, Lcom/google/android/location/collectionlib/ay;->i:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/google/android/location/collectionlib/ay;->i:I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 448
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized a(Lcom/google/p/a/b/b/a;J)Z
    .locals 4

    .prologue
    .line 503
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/location/collectionlib/ay;->j:Z

    if-eqz v0, :cond_1

    .line 504
    sget-boolean v0, Lcom/google/android/location/i/a;->b:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/location/collectionlib/ay;->W:Lcom/google/android/location/o/a/c;

    const-string v1, "Could not add location after the composer is closed."

    invoke-virtual {v0, v1}, Lcom/google/android/location/o/a/c;->a(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 505
    :cond_0
    const/4 v0, 0x0

    .line 511
    :goto_0
    monitor-exit p0

    return v0

    .line 507
    :cond_1
    :try_start_1
    invoke-direct {p0, p2, p3}, Lcom/google/android/location/collectionlib/ay;->a(J)V

    .line 508
    const/4 v0, 0x6

    iget-wide v2, p0, Lcom/google/android/location/collectionlib/ay;->l:J

    sub-long v2, p2, v2

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/p/a/b/b/a;->b(IJ)Lcom/google/p/a/b/b/a;

    .line 509
    iget-object v0, p0, Lcom/google/android/location/collectionlib/ay;->c:Lcom/google/p/a/b/b/a;

    const/16 v1, 0x8

    invoke-virtual {v0, v1, p1}, Lcom/google/p/a/b/b/a;->a(ILcom/google/p/a/b/b/a;)V

    .line 510
    invoke-direct {p0}, Lcom/google/android/location/collectionlib/ay;->c()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 511
    const/4 v0, 0x1

    goto :goto_0

    .line 503
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private b(JJ)J
    .locals 7

    .prologue
    .line 834
    iget-wide v0, p0, Lcom/google/android/location/collectionlib/ay;->o:J

    sub-long v0, p1, v0

    iget-wide v2, p0, Lcom/google/android/location/collectionlib/ay;->n:J

    iget-wide v4, p0, Lcom/google/android/location/collectionlib/ay;->l:J

    sub-long/2addr v2, v4

    const-wide/32 v4, 0xf4240

    mul-long/2addr v2, v4

    add-long/2addr v0, v2

    add-long/2addr v0, p3

    return-wide v0
.end method

.method private declared-synchronized b()V
    .locals 3

    .prologue
    .line 339
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/location/collectionlib/ay;->c:Lcom/google/p/a/b/b/a;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/google/p/a/b/b/a;->k(I)I

    move-result v0

    if-gtz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/location/collectionlib/ay;->c:Lcom/google/p/a/b/b/a;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Lcom/google/p/a/b/b/a;->k(I)I

    move-result v0

    if-gtz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/location/collectionlib/ay;->c:Lcom/google/p/a/b/b/a;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/google/p/a/b/b/a;->k(I)I

    move-result v0

    if-gtz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/location/collectionlib/ay;->c:Lcom/google/p/a/b/b/a;

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Lcom/google/p/a/b/b/a;->k(I)I

    move-result v0

    if-gtz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/location/collectionlib/ay;->c:Lcom/google/p/a/b/b/a;

    const/4 v1, 0x6

    invoke-virtual {v0, v1}, Lcom/google/p/a/b/b/a;->k(I)I

    move-result v0

    if-gtz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/location/collectionlib/ay;->c:Lcom/google/p/a/b/b/a;

    const/4 v1, 0x7

    invoke-virtual {v0, v1}, Lcom/google/p/a/b/b/a;->k(I)I

    move-result v0

    if-gtz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/location/collectionlib/ay;->c:Lcom/google/p/a/b/b/a;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/google/p/a/b/b/a;->k(I)I

    move-result v0

    if-gtz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/location/collectionlib/ay;->c:Lcom/google/p/a/b/b/a;

    const/16 v1, 0xa

    invoke-virtual {v0, v1}, Lcom/google/p/a/b/b/a;->k(I)I

    move-result v0

    if-gtz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/location/collectionlib/ay;->c:Lcom/google/p/a/b/b/a;

    const/16 v1, 0x10

    invoke-virtual {v0, v1}, Lcom/google/p/a/b/b/a;->k(I)I

    move-result v0

    if-gtz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/location/collectionlib/ay;->c:Lcom/google/p/a/b/b/a;

    const/16 v1, 0xb

    invoke-virtual {v0, v1}, Lcom/google/p/a/b/b/a;->k(I)I

    move-result v0

    if-gtz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/location/collectionlib/ay;->c:Lcom/google/p/a/b/b/a;

    const/16 v1, 0xd

    invoke-virtual {v0, v1}, Lcom/google/p/a/b/b/a;->k(I)I

    move-result v0

    if-gtz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/location/collectionlib/ay;->c:Lcom/google/p/a/b/b/a;

    const/16 v1, 0xe

    invoke-virtual {v0, v1}, Lcom/google/p/a/b/b/a;->k(I)I

    move-result v0

    if-gtz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/location/collectionlib/ay;->c:Lcom/google/p/a/b/b/a;

    const/16 v1, 0xf

    invoke-virtual {v0, v1}, Lcom/google/p/a/b/b/a;->k(I)I

    move-result v0

    if-gtz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/location/collectionlib/ay;->c:Lcom/google/p/a/b/b/a;

    const/16 v1, 0x11

    invoke-virtual {v0, v1}, Lcom/google/p/a/b/b/a;->k(I)I

    move-result v0

    if-gtz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/location/collectionlib/ay;->c:Lcom/google/p/a/b/b/a;

    const/16 v1, 0xc

    invoke-virtual {v0, v1}, Lcom/google/p/a/b/b/a;->k(I)I

    move-result v0

    if-gtz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/location/collectionlib/ay;->c:Lcom/google/p/a/b/b/a;

    const/16 v1, 0x13

    invoke-virtual {v0, v1}, Lcom/google/p/a/b/b/a;->k(I)I

    move-result v0

    if-lez v0, :cond_1

    .line 355
    :cond_0
    iget-object v0, p0, Lcom/google/android/location/collectionlib/ay;->b:Lcom/google/p/a/b/b/a;

    const/16 v1, 0x9

    iget-object v2, p0, Lcom/google/android/location/collectionlib/ay;->c:Lcom/google/p/a/b/b/a;

    invoke-virtual {v0, v1, v2}, Lcom/google/p/a/b/b/a;->b(ILcom/google/p/a/b/b/a;)Lcom/google/p/a/b/b/a;

    .line 358
    :cond_1
    iget-object v0, p0, Lcom/google/android/location/collectionlib/ay;->b:Lcom/google/p/a/b/b/a;

    invoke-virtual {v0}, Lcom/google/p/a/b/b/a;->e()I

    move-result v0

    if-eqz v0, :cond_2

    .line 359
    iget-object v0, p0, Lcom/google/android/location/collectionlib/ay;->a:Lcom/google/p/a/b/b/a;

    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/android/location/collectionlib/ay;->b:Lcom/google/p/a/b/b/a;

    invoke-virtual {v0, v1, v2}, Lcom/google/p/a/b/b/a;->a(ILcom/google/p/a/b/b/a;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 361
    :cond_2
    monitor-exit p0

    return-void

    .line 339
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private c(JJ)I
    .locals 5

    .prologue
    .line 848
    sub-long v0, p3, p1

    .line 849
    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-gez v2, :cond_0

    .line 850
    sget-boolean v2, Lcom/google/android/location/i/a;->b:Z

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/location/collectionlib/ay;->W:Lcom/google/android/location/o/a/c;

    const-string v3, "Later event arrive earlier"

    invoke-virtual {v2, v3}, Lcom/google/android/location/o/a/c;->a(Ljava/lang/String;)V

    .line 852
    :cond_0
    const-wide/16 v2, 0x3e8

    div-long/2addr v0, v2

    long-to-int v0, v0

    return v0
.end method

.method private declared-synchronized c()V
    .locals 2

    .prologue
    .line 369
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/location/collectionlib/ay;->X:Lcom/google/android/location/collectionlib/az;

    invoke-virtual {v0}, Lcom/google/android/location/collectionlib/az;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 370
    invoke-direct {p0}, Lcom/google/android/location/collectionlib/ay;->b()V

    .line 371
    invoke-direct {p0}, Lcom/google/android/location/collectionlib/ay;->e()V

    .line 372
    iget-object v0, p0, Lcom/google/android/location/collectionlib/ay;->a:Lcom/google/p/a/b/b/a;

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/google/android/location/collectionlib/ay;->a(Lcom/google/p/a/b/b/a;Z)V

    .line 373
    invoke-direct {p0}, Lcom/google/android/location/collectionlib/ay;->d()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 375
    :cond_0
    monitor-exit p0

    return-void

    .line 369
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private d(JJ)I
    .locals 5

    .prologue
    .line 864
    sub-long v0, p3, p1

    .line 865
    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-gez v2, :cond_0

    .line 866
    sget-boolean v2, Lcom/google/android/location/i/a;->b:Z

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/location/collectionlib/ay;->W:Lcom/google/android/location/o/a/c;

    const-string v3, "Later event arrive earlier"

    invoke-virtual {v2, v3}, Lcom/google/android/location/o/a/c;->a(Ljava/lang/String;)V

    .line 868
    :cond_0
    const-wide/16 v2, 0x3e8

    rem-long/2addr v0, v2

    long-to-int v0, v0

    return v0
.end method

.method private declared-synchronized d()V
    .locals 4

    .prologue
    .line 381
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/location/collectionlib/ay;->X:Lcom/google/android/location/collectionlib/az;

    invoke-virtual {v0}, Lcom/google/android/location/collectionlib/az;->b()V

    .line 382
    new-instance v0, Lcom/google/p/a/b/b/a;

    sget-object v1, Lcom/google/android/location/m/a;->Q:Lcom/google/p/a/b/b/c;

    invoke-direct {v0, v1}, Lcom/google/p/a/b/b/a;-><init>(Lcom/google/p/a/b/b/c;)V

    iput-object v0, p0, Lcom/google/android/location/collectionlib/ay;->a:Lcom/google/p/a/b/b/a;

    .line 383
    new-instance v0, Lcom/google/p/a/b/b/a;

    sget-object v1, Lcom/google/android/location/m/a;->y:Lcom/google/p/a/b/b/c;

    invoke-direct {v0, v1}, Lcom/google/p/a/b/b/a;-><init>(Lcom/google/p/a/b/b/c;)V

    .line 384
    const/4 v1, 0x2

    sget-object v2, Lcom/google/android/location/collectionlib/bn;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/google/p/a/b/b/a;->a(ILjava/lang/String;)Lcom/google/p/a/b/b/a;

    .line 385
    const/4 v1, 0x5

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/p/a/b/b/a;->a(ILjava/lang/String;)Lcom/google/p/a/b/b/a;

    .line 386
    const/4 v1, 0x1

    const-string v2, "1.0"

    invoke-virtual {v0, v1, v2}, Lcom/google/p/a/b/b/a;->a(ILjava/lang/String;)Lcom/google/p/a/b/b/a;

    .line 387
    iget-object v1, p0, Lcom/google/android/location/collectionlib/ay;->Y:Lcom/google/p/a/b/b/a;

    if-eqz v1, :cond_0

    .line 388
    const/16 v1, 0xa

    iget-object v2, p0, Lcom/google/android/location/collectionlib/ay;->Y:Lcom/google/p/a/b/b/a;

    invoke-virtual {v0, v1, v2}, Lcom/google/p/a/b/b/a;->b(ILcom/google/p/a/b/b/a;)Lcom/google/p/a/b/b/a;

    .line 390
    :cond_0
    iget-object v1, p0, Lcom/google/android/location/collectionlib/ay;->a:Lcom/google/p/a/b/b/a;

    const/4 v2, 0x1

    invoke-virtual {v1, v2, v0}, Lcom/google/p/a/b/b/a;->b(ILcom/google/p/a/b/b/a;)Lcom/google/p/a/b/b/a;

    .line 391
    new-instance v0, Lcom/google/p/a/b/b/a;

    sget-object v1, Lcom/google/android/location/m/a;->z:Lcom/google/p/a/b/b/c;

    invoke-direct {v0, v1}, Lcom/google/p/a/b/b/a;-><init>(Lcom/google/p/a/b/b/c;)V

    .line 392
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/location/collectionlib/ay;->d:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/google/p/a/b/b/a;->a(ILjava/lang/String;)Lcom/google/p/a/b/b/a;

    .line 393
    iget-object v1, p0, Lcom/google/android/location/collectionlib/ay;->e:Ljava/lang/String;

    invoke-static {v1}, Lcom/google/android/location/collectionlib/df;->a(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 394
    new-instance v1, Lcom/google/p/a/b/b/a;

    sget-object v2, Lcom/google/android/location/m/a;->A:Lcom/google/p/a/b/b/c;

    invoke-direct {v1, v2}, Lcom/google/p/a/b/b/a;-><init>(Lcom/google/p/a/b/b/c;)V

    .line 395
    const/4 v2, 0x1

    iget-object v3, p0, Lcom/google/android/location/collectionlib/ay;->e:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Lcom/google/p/a/b/b/a;->a(ILjava/lang/String;)Lcom/google/p/a/b/b/a;

    .line 396
    iget-object v2, p0, Lcom/google/android/location/collectionlib/ay;->a:Lcom/google/p/a/b/b/a;

    const/4 v3, 0x3

    invoke-virtual {v2, v3, v1}, Lcom/google/p/a/b/b/a;->b(ILcom/google/p/a/b/b/a;)Lcom/google/p/a/b/b/a;

    .line 398
    :cond_1
    iget-object v1, p0, Lcom/google/android/location/collectionlib/ay;->a:Lcom/google/p/a/b/b/a;

    const/4 v2, 0x2

    invoke-virtual {v1, v2, v0}, Lcom/google/p/a/b/b/a;->a(ILcom/google/p/a/b/b/a;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 399
    monitor-exit p0

    return-void

    .line 381
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized e()V
    .locals 3

    .prologue
    .line 405
    monitor-enter p0

    :try_start_0
    new-instance v0, Lcom/google/p/a/b/b/a;

    sget-object v1, Lcom/google/android/location/m/a;->F:Lcom/google/p/a/b/b/c;

    invoke-direct {v0, v1}, Lcom/google/p/a/b/b/a;-><init>(Lcom/google/p/a/b/b/c;)V

    iput-object v0, p0, Lcom/google/android/location/collectionlib/ay;->b:Lcom/google/p/a/b/b/a;

    .line 406
    new-instance v0, Lcom/google/p/a/b/b/a;

    sget-object v1, Lcom/google/android/location/m/a;->aT:Lcom/google/p/a/b/b/c;

    invoke-direct {v0, v1}, Lcom/google/p/a/b/b/a;-><init>(Lcom/google/p/a/b/b/c;)V

    iput-object v0, p0, Lcom/google/android/location/collectionlib/ay;->c:Lcom/google/p/a/b/b/a;

    .line 407
    const-wide v0, 0x7fffffffffffffffL

    iput-wide v0, p0, Lcom/google/android/location/collectionlib/ay;->D:J

    .line 408
    const-wide v0, 0x7fffffffffffffffL

    iput-wide v0, p0, Lcom/google/android/location/collectionlib/ay;->E:J

    .line 409
    const-wide v0, 0x7fffffffffffffffL

    iput-wide v0, p0, Lcom/google/android/location/collectionlib/ay;->F:J

    .line 410
    const-wide v0, 0x7fffffffffffffffL

    iput-wide v0, p0, Lcom/google/android/location/collectionlib/ay;->G:J

    .line 411
    const-wide v0, 0x7fffffffffffffffL

    iput-wide v0, p0, Lcom/google/android/location/collectionlib/ay;->I:J

    .line 412
    const-wide v0, 0x7fffffffffffffffL

    iput-wide v0, p0, Lcom/google/android/location/collectionlib/ay;->J:J

    .line 413
    const-wide v0, 0x7fffffffffffffffL

    iput-wide v0, p0, Lcom/google/android/location/collectionlib/ay;->K:J

    .line 414
    const-wide v0, 0x7fffffffffffffffL

    iput-wide v0, p0, Lcom/google/android/location/collectionlib/ay;->H:J

    .line 415
    const-wide v0, 0x7fffffffffffffffL

    iput-wide v0, p0, Lcom/google/android/location/collectionlib/ay;->L:J

    .line 416
    const-wide v0, 0x7fffffffffffffffL

    iput-wide v0, p0, Lcom/google/android/location/collectionlib/ay;->M:J

    .line 417
    iget-object v0, p0, Lcom/google/android/location/collectionlib/ay;->f:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/location/collectionlib/ay;->g:Z

    if-nez v0, :cond_0

    .line 418
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/location/collectionlib/ay;->g:Z

    .line 419
    new-instance v0, Lcom/google/p/a/b/b/a;

    sget-object v1, Lcom/google/android/location/m/a;->o:Lcom/google/p/a/b/b/c;

    invoke-direct {v0, v1}, Lcom/google/p/a/b/b/a;-><init>(Lcom/google/p/a/b/b/c;)V

    .line 420
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/location/collectionlib/ay;->f:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/google/p/a/b/b/a;->f(II)Lcom/google/p/a/b/b/a;

    .line 421
    iget-object v1, p0, Lcom/google/android/location/collectionlib/ay;->b:Lcom/google/p/a/b/b/a;

    const/16 v2, 0x63

    invoke-virtual {v1, v2, v0}, Lcom/google/p/a/b/b/a;->b(ILcom/google/p/a/b/b/a;)Lcom/google/p/a/b/b/a;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 423
    :cond_0
    monitor-exit p0

    return-void

    .line 405
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized f()J
    .locals 10

    .prologue
    const-wide/16 v8, 0x1

    .line 429
    monitor-enter p0

    :try_start_0
    iget-wide v0, p0, Lcom/google/android/location/collectionlib/ay;->m:J

    const-wide v2, 0x7fffffffffffffffL

    cmp-long v0, v0, v2

    if-nez v0, :cond_2

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    .line 431
    :goto_0
    iget-wide v2, p0, Lcom/google/android/location/collectionlib/ay;->k:J

    add-long/2addr v0, v2

    .line 432
    iget-wide v2, p0, Lcom/google/android/location/collectionlib/ay;->h:J

    cmp-long v2, v0, v2

    if-gtz v2, :cond_1

    .line 433
    sget-boolean v2, Lcom/google/android/location/i/a;->b:Z

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/location/collectionlib/ay;->W:Lcom/google/android/location/o/a/c;

    const-string v3, "Timestaime changed from %d to %d."

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x1

    add-long/2addr v0, v8

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    aput-object v0, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/google/android/location/o/a/c;->a(Ljava/lang/String;)V

    .line 435
    :cond_0
    iget-wide v0, p0, Lcom/google/android/location/collectionlib/ay;->h:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    add-long/2addr v0, v8

    .line 437
    :cond_1
    monitor-exit p0

    return-wide v0

    .line 429
    :cond_2
    :try_start_1
    iget-wide v0, p0, Lcom/google/android/location/collectionlib/ay;->m:J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public final declared-synchronized a()V
    .locals 2

    .prologue
    .line 322
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/location/collectionlib/ay;->j:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    .line 332
    :goto_0
    monitor-exit p0

    return-void

    .line 325
    :cond_0
    const/4 v0, 0x1

    :try_start_1
    iput-boolean v0, p0, Lcom/google/android/location/collectionlib/ay;->j:Z

    .line 327
    invoke-direct {p0}, Lcom/google/android/location/collectionlib/ay;->b()V

    .line 328
    iget-object v0, p0, Lcom/google/android/location/collectionlib/ay;->a:Lcom/google/p/a/b/b/a;

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Lcom/google/android/location/collectionlib/ay;->a(Lcom/google/p/a/b/b/a;Z)V

    .line 329
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/location/collectionlib/ay;->a:Lcom/google/p/a/b/b/a;

    .line 330
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/location/collectionlib/ay;->b:Lcom/google/p/a/b/b/a;

    .line 331
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/location/collectionlib/ay;->c:Lcom/google/p/a/b/b/a;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 322
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(FFFF)V
    .locals 3

    .prologue
    .line 1334
    monitor-enter p0

    :try_start_0
    new-instance v0, Lcom/google/p/a/b/b/a;

    sget-object v1, Lcom/google/android/location/m/a;->aX:Lcom/google/p/a/b/b/c;

    invoke-direct {v0, v1}, Lcom/google/p/a/b/b/a;-><init>(Lcom/google/p/a/b/b/c;)V

    .line 1335
    const/4 v1, 0x1

    invoke-virtual {v0, v1, p1}, Lcom/google/p/a/b/b/a;->a(IF)Lcom/google/p/a/b/b/a;

    .line 1336
    const/4 v1, 0x2

    invoke-virtual {v0, v1, p2}, Lcom/google/p/a/b/b/a;->a(IF)Lcom/google/p/a/b/b/a;

    .line 1337
    const/4 v1, 0x3

    invoke-virtual {v0, v1, p3}, Lcom/google/p/a/b/b/a;->a(IF)Lcom/google/p/a/b/b/a;

    .line 1338
    const/4 v1, 0x7

    invoke-virtual {v0, v1, p4}, Lcom/google/p/a/b/b/a;->a(IF)Lcom/google/p/a/b/b/a;

    .line 1339
    iget-object v1, p0, Lcom/google/android/location/collectionlib/ay;->c:Lcom/google/p/a/b/b/a;

    const/16 v2, 0x9

    invoke-virtual {v1, v2, v0}, Lcom/google/p/a/b/b/a;->b(ILcom/google/p/a/b/b/a;)Lcom/google/p/a/b/b/a;

    .line 1340
    invoke-direct {p0}, Lcom/google/android/location/collectionlib/ay;->c()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1341
    monitor-exit p0

    return-void

    .line 1334
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(FFFFFFIJJ)V
    .locals 12

    .prologue
    .line 1049
    monitor-enter p0

    :try_start_0
    iget-boolean v2, p0, Lcom/google/android/location/collectionlib/ay;->j:Z

    if-eqz v2, :cond_1

    .line 1050
    sget-boolean v2, Lcom/google/android/location/i/a;->b:Z

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/location/collectionlib/ay;->W:Lcom/google/android/location/o/a/c;

    const-string v3, "Could not add uncalibrated magnetic field snapshot after the composer is closed."

    invoke-virtual {v2, v3}, Lcom/google/android/location/o/a/c;->a(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1085
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 1053
    :cond_1
    :try_start_1
    move-wide/from16 v0, p10

    invoke-direct {p0, v0, v1}, Lcom/google/android/location/collectionlib/ay;->a(J)V

    .line 1054
    iget-wide v10, p0, Lcom/google/android/location/collectionlib/ay;->M:J

    move-object v2, p0

    move v3, p1

    move v4, p2

    move v5, p3

    move-wide/from16 v6, p8

    move-wide/from16 v8, p10

    invoke-direct/range {v2 .. v11}, Lcom/google/android/location/collectionlib/ay;->a(FFFJJJ)Lcom/google/p/a/b/b/a;

    move-result-object v2

    .line 1060
    move-wide/from16 v0, p8

    iput-wide v0, p0, Lcom/google/android/location/collectionlib/ay;->M:J

    .line 1063
    iget-object v3, p0, Lcom/google/android/location/collectionlib/ay;->c:Lcom/google/p/a/b/b/a;

    const/16 v4, 0xc

    invoke-virtual {v3, v4}, Lcom/google/p/a/b/b/a;->k(I)I

    move-result v3

    if-eqz v3, :cond_2

    iget v3, p0, Lcom/google/android/location/collectionlib/ay;->z:I

    move/from16 v0, p7

    if-eq v3, v0, :cond_3

    .line 1065
    :cond_2
    const/4 v3, 0x4

    move/from16 v0, p7

    invoke-virtual {v2, v3, v0}, Lcom/google/p/a/b/b/a;->f(II)Lcom/google/p/a/b/b/a;

    .line 1066
    move/from16 v0, p7

    iput v0, p0, Lcom/google/android/location/collectionlib/ay;->z:I

    .line 1069
    :cond_3
    new-instance v3, Lcom/google/p/a/b/b/a;

    sget-object v4, Lcom/google/android/location/m/a;->aY:Lcom/google/p/a/b/b/c;

    const/4 v5, 0x4

    invoke-direct {v3, v4, v5}, Lcom/google/p/a/b/b/a;-><init>(Lcom/google/p/a/b/b/c;I)V

    .line 1070
    const/4 v4, 0x1

    invoke-virtual {v3, v4, v2}, Lcom/google/p/a/b/b/a;->b(ILcom/google/p/a/b/b/a;)Lcom/google/p/a/b/b/a;

    .line 1073
    iget-object v2, p0, Lcom/google/android/location/collectionlib/ay;->c:Lcom/google/p/a/b/b/a;

    const/16 v4, 0xc

    invoke-virtual {v2, v4}, Lcom/google/p/a/b/b/a;->k(I)I

    move-result v2

    if-eqz v2, :cond_5

    iget v2, p0, Lcom/google/android/location/collectionlib/ay;->A:F

    sub-float v2, p4, v2

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v2

    const v4, 0x358637bd    # 1.0E-6f

    cmpl-float v2, v2, v4

    if-gtz v2, :cond_4

    iget v2, p0, Lcom/google/android/location/collectionlib/ay;->B:F

    sub-float v2, p5, v2

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v2

    const v4, 0x358637bd    # 1.0E-6f

    cmpl-float v2, v2, v4

    if-gtz v2, :cond_4

    iget v2, p0, Lcom/google/android/location/collectionlib/ay;->C:F

    sub-float v2, p6, v2

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v2

    const v4, 0x358637bd    # 1.0E-6f

    cmpl-float v2, v2, v4

    if-lez v2, :cond_7

    :cond_4
    const/4 v2, 0x1

    :goto_1
    if-eqz v2, :cond_6

    .line 1075
    :cond_5
    const/4 v2, 0x2

    move/from16 v0, p4

    invoke-virtual {v3, v2, v0}, Lcom/google/p/a/b/b/a;->a(IF)Lcom/google/p/a/b/b/a;

    .line 1076
    const/4 v2, 0x3

    move/from16 v0, p5

    invoke-virtual {v3, v2, v0}, Lcom/google/p/a/b/b/a;->a(IF)Lcom/google/p/a/b/b/a;

    .line 1077
    const/4 v2, 0x4

    move/from16 v0, p6

    invoke-virtual {v3, v2, v0}, Lcom/google/p/a/b/b/a;->a(IF)Lcom/google/p/a/b/b/a;

    .line 1078
    move/from16 v0, p4

    iput v0, p0, Lcom/google/android/location/collectionlib/ay;->A:F

    .line 1079
    move/from16 v0, p5

    iput v0, p0, Lcom/google/android/location/collectionlib/ay;->B:F

    .line 1080
    move/from16 v0, p6

    iput v0, p0, Lcom/google/android/location/collectionlib/ay;->C:F

    .line 1083
    :cond_6
    iget-object v2, p0, Lcom/google/android/location/collectionlib/ay;->c:Lcom/google/p/a/b/b/a;

    const/16 v4, 0xc

    invoke-virtual {v2, v4, v3}, Lcom/google/p/a/b/b/a;->a(ILcom/google/p/a/b/b/a;)V

    .line 1084
    invoke-direct {p0}, Lcom/google/android/location/collectionlib/ay;->c()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/16 :goto_0

    .line 1049
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2

    .line 1073
    :cond_7
    const/4 v2, 0x0

    goto :goto_1
.end method

.method public final declared-synchronized a(FFFIJJ)V
    .locals 13

    .prologue
    .line 881
    monitor-enter p0

    :try_start_0
    iget-boolean v4, p0, Lcom/google/android/location/collectionlib/ay;->j:Z

    if-eqz v4, :cond_1

    .line 882
    sget-boolean v4, Lcom/google/android/location/i/a;->b:Z

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/google/android/location/collectionlib/ay;->W:Lcom/google/android/location/o/a/c;

    const-string v5, "Could not add orientation snapshot after the composer is closed."

    invoke-virtual {v4, v5}, Lcom/google/android/location/o/a/c;->a(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 933
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 885
    :cond_1
    :try_start_1
    move-wide/from16 v0, p7

    invoke-direct {p0, v0, v1}, Lcom/google/android/location/collectionlib/ay;->a(J)V

    .line 886
    move-wide/from16 v0, p5

    move-wide/from16 v2, p7

    invoke-direct {p0, v0, v1, v2, v3}, Lcom/google/android/location/collectionlib/ay;->a(JJ)V

    .line 887
    new-instance v6, Lcom/google/p/a/b/b/a;

    sget-object v4, Lcom/google/android/location/m/a;->aU:Lcom/google/p/a/b/b/c;

    const/16 v5, 0x8

    invoke-direct {v6, v4, v5}, Lcom/google/p/a/b/b/a;-><init>(Lcom/google/p/a/b/b/c;I)V

    .line 888
    const/4 v4, 0x1

    invoke-virtual {v6, v4, p1}, Lcom/google/p/a/b/b/a;->a(IF)Lcom/google/p/a/b/b/a;

    .line 889
    const/4 v4, 0x3

    invoke-virtual {v6, v4, p2}, Lcom/google/p/a/b/b/a;->a(IF)Lcom/google/p/a/b/b/a;

    .line 890
    const/4 v4, 0x2

    move/from16 v0, p3

    invoke-virtual {v6, v4, v0}, Lcom/google/p/a/b/b/a;->a(IF)Lcom/google/p/a/b/b/a;

    .line 891
    iget-wide v4, p0, Lcom/google/android/location/collectionlib/ay;->O:J

    const-wide v8, 0x7fffffffffffffffL

    cmp-long v4, v4, v8

    if-nez v4, :cond_2

    .line 892
    iget-wide v4, p0, Lcom/google/android/location/collectionlib/ay;->o:J

    sub-long v4, v4, p5

    iget-wide v8, p0, Lcom/google/android/location/collectionlib/ay;->n:J

    sub-long v8, p7, v8

    const-wide/32 v10, 0xf4240

    mul-long/2addr v8, v10

    add-long/2addr v4, v8

    iput-wide v4, p0, Lcom/google/android/location/collectionlib/ay;->O:J

    .line 895
    :cond_2
    iget-object v4, p0, Lcom/google/android/location/collectionlib/ay;->c:Lcom/google/p/a/b/b/a;

    const/4 v5, 0x1

    invoke-virtual {v4, v5}, Lcom/google/p/a/b/b/a;->i(I)Z

    move-result v4

    if-nez v4, :cond_3

    .line 897
    iget-object v4, p0, Lcom/google/android/location/collectionlib/ay;->c:Lcom/google/p/a/b/b/a;

    const/4 v5, 0x1

    iget-wide v8, p0, Lcom/google/android/location/collectionlib/ay;->O:J

    move-wide/from16 v0, p5

    invoke-direct {p0, v0, v1, v8, v9}, Lcom/google/android/location/collectionlib/ay;->b(JJ)J

    move-result-wide v8

    invoke-virtual {v4, v5, v8, v9}, Lcom/google/p/a/b/b/a;->b(IJ)Lcom/google/p/a/b/b/a;

    .line 900
    :cond_3
    iget-object v4, p0, Lcom/google/android/location/collectionlib/ay;->c:Lcom/google/p/a/b/b/a;

    const/4 v5, 0x2

    invoke-virtual {v4, v5}, Lcom/google/p/a/b/b/a;->i(I)Z

    move-result v4

    if-nez v4, :cond_4

    .line 902
    const/16 v4, 0x9

    move-wide/from16 v0, p5

    invoke-virtual {v6, v4, v0, v1}, Lcom/google/p/a/b/b/a;->b(IJ)Lcom/google/p/a/b/b/a;

    .line 903
    const/16 v4, 0xa

    const-wide/32 v8, 0xf4240

    mul-long v8, v8, p7

    invoke-virtual {v6, v4, v8, v9}, Lcom/google/p/a/b/b/a;->b(IJ)Lcom/google/p/a/b/b/a;

    .line 907
    :cond_4
    iget-wide v4, p0, Lcom/google/android/location/collectionlib/ay;->E:J

    const-wide v8, 0x7fffffffffffffffL

    cmp-long v4, v4, v8

    if-nez v4, :cond_9

    .line 908
    iget-object v4, p0, Lcom/google/android/location/collectionlib/ay;->c:Lcom/google/p/a/b/b/a;

    const/4 v5, 0x1

    invoke-virtual {v4, v5}, Lcom/google/p/a/b/b/a;->d(I)J

    move-result-wide v8

    .line 909
    iget-wide v4, p0, Lcom/google/android/location/collectionlib/ay;->O:J

    move-wide/from16 v0, p5

    invoke-direct {p0, v0, v1, v4, v5}, Lcom/google/android/location/collectionlib/ay;->b(JJ)J

    move-result-wide v10

    .line 911
    invoke-direct {p0, v8, v9, v10, v11}, Lcom/google/android/location/collectionlib/ay;->c(JJ)I

    move-result v5

    .line 912
    invoke-direct {p0, v8, v9, v10, v11}, Lcom/google/android/location/collectionlib/ay;->d(JJ)I

    move-result v4

    .line 917
    :goto_1
    if-eqz v5, :cond_5

    .line 918
    const/4 v7, 0x7

    invoke-virtual {v6, v7, v5}, Lcom/google/p/a/b/b/a;->f(II)Lcom/google/p/a/b/b/a;

    .line 920
    :cond_5
    if-eqz v4, :cond_6

    .line 921
    const/16 v5, 0x8

    invoke-virtual {v6, v5, v4}, Lcom/google/p/a/b/b/a;->f(II)Lcom/google/p/a/b/b/a;

    .line 923
    :cond_6
    move-wide/from16 v0, p5

    iput-wide v0, p0, Lcom/google/android/location/collectionlib/ay;->E:J

    .line 926
    iget-object v4, p0, Lcom/google/android/location/collectionlib/ay;->c:Lcom/google/p/a/b/b/a;

    const/4 v5, 0x2

    invoke-virtual {v4, v5}, Lcom/google/p/a/b/b/a;->k(I)I

    move-result v4

    if-eqz v4, :cond_7

    iget v4, p0, Lcom/google/android/location/collectionlib/ay;->r:I

    move/from16 v0, p4

    if-eq v4, v0, :cond_8

    .line 928
    :cond_7
    const/4 v4, 0x4

    move/from16 v0, p4

    invoke-virtual {v6, v4, v0}, Lcom/google/p/a/b/b/a;->f(II)Lcom/google/p/a/b/b/a;

    .line 929
    move/from16 v0, p4

    iput v0, p0, Lcom/google/android/location/collectionlib/ay;->r:I

    .line 931
    :cond_8
    iget-object v4, p0, Lcom/google/android/location/collectionlib/ay;->c:Lcom/google/p/a/b/b/a;

    const/4 v5, 0x2

    invoke-virtual {v4, v5, v6}, Lcom/google/p/a/b/b/a;->a(ILcom/google/p/a/b/b/a;)V

    .line 932
    invoke-direct {p0}, Lcom/google/android/location/collectionlib/ay;->c()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/16 :goto_0

    .line 881
    :catchall_0
    move-exception v4

    monitor-exit p0

    throw v4

    .line 914
    :cond_9
    :try_start_2
    iget-wide v4, p0, Lcom/google/android/location/collectionlib/ay;->E:J

    move-wide/from16 v0, p5

    invoke-direct {p0, v4, v5, v0, v1}, Lcom/google/android/location/collectionlib/ay;->c(JJ)I

    move-result v5

    .line 915
    iget-wide v8, p0, Lcom/google/android/location/collectionlib/ay;->E:J

    move-wide/from16 v0, p5

    invoke-direct {p0, v8, v9, v0, v1}, Lcom/google/android/location/collectionlib/ay;->d(JJ)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result v4

    goto :goto_1
.end method

.method public final declared-synchronized a(FIJJ)V
    .locals 9

    .prologue
    .line 1217
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/location/collectionlib/ay;->j:Z

    if-eqz v0, :cond_1

    .line 1218
    sget-boolean v0, Lcom/google/android/location/i/a;->b:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/location/collectionlib/ay;->W:Lcom/google/android/location/o/a/c;

    const-string v1, "Could not add barometer snapshot after the composer is closed."

    invoke-virtual {v0, v1}, Lcom/google/android/location/o/a/c;->a(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1267
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 1221
    :cond_1
    :try_start_1
    invoke-direct {p0, p5, p6}, Lcom/google/android/location/collectionlib/ay;->a(J)V

    .line 1222
    invoke-direct {p0, p3, p4, p5, p6}, Lcom/google/android/location/collectionlib/ay;->a(JJ)V

    .line 1223
    new-instance v2, Lcom/google/p/a/b/b/a;

    sget-object v0, Lcom/google/android/location/m/a;->bb:Lcom/google/p/a/b/b/c;

    const/4 v1, 0x4

    invoke-direct {v2, v0, v1}, Lcom/google/p/a/b/b/a;-><init>(Lcom/google/p/a/b/b/c;I)V

    .line 1224
    const/4 v0, 0x1

    invoke-virtual {v2, v0, p1}, Lcom/google/p/a/b/b/a;->a(IF)Lcom/google/p/a/b/b/a;

    .line 1225
    iget-wide v0, p0, Lcom/google/android/location/collectionlib/ay;->S:J

    const-wide v4, 0x7fffffffffffffffL

    cmp-long v0, v0, v4

    if-nez v0, :cond_2

    .line 1226
    iget-wide v0, p0, Lcom/google/android/location/collectionlib/ay;->o:J

    sub-long/2addr v0, p3

    iget-wide v4, p0, Lcom/google/android/location/collectionlib/ay;->n:J

    sub-long v4, p5, v4

    const-wide/32 v6, 0xf4240

    mul-long/2addr v4, v6

    add-long/2addr v0, v4

    iput-wide v0, p0, Lcom/google/android/location/collectionlib/ay;->S:J

    .line 1229
    :cond_2
    iget-object v0, p0, Lcom/google/android/location/collectionlib/ay;->c:Lcom/google/p/a/b/b/a;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/p/a/b/b/a;->i(I)Z

    move-result v0

    if-nez v0, :cond_3

    .line 1231
    iget-object v0, p0, Lcom/google/android/location/collectionlib/ay;->c:Lcom/google/p/a/b/b/a;

    const/4 v1, 0x1

    iget-wide v4, p0, Lcom/google/android/location/collectionlib/ay;->S:J

    invoke-direct {p0, p3, p4, v4, v5}, Lcom/google/android/location/collectionlib/ay;->b(JJ)J

    move-result-wide v4

    invoke-virtual {v0, v1, v4, v5}, Lcom/google/p/a/b/b/a;->b(IJ)Lcom/google/p/a/b/b/a;

    .line 1234
    :cond_3
    iget-object v0, p0, Lcom/google/android/location/collectionlib/ay;->c:Lcom/google/p/a/b/b/a;

    const/16 v1, 0xb

    invoke-virtual {v0, v1}, Lcom/google/p/a/b/b/a;->i(I)Z

    move-result v0

    if-nez v0, :cond_4

    .line 1236
    const/4 v0, 0x5

    invoke-virtual {v2, v0, p3, p4}, Lcom/google/p/a/b/b/a;->b(IJ)Lcom/google/p/a/b/b/a;

    .line 1237
    const/4 v0, 0x6

    const-wide/32 v4, 0xf4240

    mul-long/2addr v4, p5

    invoke-virtual {v2, v0, v4, v5}, Lcom/google/p/a/b/b/a;->b(IJ)Lcom/google/p/a/b/b/a;

    .line 1241
    :cond_4
    iget-wide v0, p0, Lcom/google/android/location/collectionlib/ay;->I:J

    const-wide v4, 0x7fffffffffffffffL

    cmp-long v0, v0, v4

    if-nez v0, :cond_9

    .line 1242
    iget-object v0, p0, Lcom/google/android/location/collectionlib/ay;->c:Lcom/google/p/a/b/b/a;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/p/a/b/b/a;->d(I)J

    move-result-wide v4

    .line 1243
    iget-wide v0, p0, Lcom/google/android/location/collectionlib/ay;->S:J

    invoke-direct {p0, p3, p4, v0, v1}, Lcom/google/android/location/collectionlib/ay;->b(JJ)J

    move-result-wide v6

    .line 1245
    invoke-direct {p0, v4, v5, v6, v7}, Lcom/google/android/location/collectionlib/ay;->c(JJ)I

    move-result v1

    .line 1246
    invoke-direct {p0, v4, v5, v6, v7}, Lcom/google/android/location/collectionlib/ay;->d(JJ)I

    move-result v0

    .line 1251
    :goto_1
    if-eqz v1, :cond_5

    .line 1252
    const/4 v3, 0x3

    invoke-virtual {v2, v3, v1}, Lcom/google/p/a/b/b/a;->f(II)Lcom/google/p/a/b/b/a;

    .line 1254
    :cond_5
    if-eqz v0, :cond_6

    .line 1255
    const/4 v1, 0x4

    invoke-virtual {v2, v1, v0}, Lcom/google/p/a/b/b/a;->f(II)Lcom/google/p/a/b/b/a;

    .line 1257
    :cond_6
    iput-wide p3, p0, Lcom/google/android/location/collectionlib/ay;->I:J

    .line 1260
    iget-object v0, p0, Lcom/google/android/location/collectionlib/ay;->c:Lcom/google/p/a/b/b/a;

    const/16 v1, 0xb

    invoke-virtual {v0, v1}, Lcom/google/p/a/b/b/a;->k(I)I

    move-result v0

    if-eqz v0, :cond_7

    iget v0, p0, Lcom/google/android/location/collectionlib/ay;->v:I

    if-eq v0, p2, :cond_8

    .line 1262
    :cond_7
    const/4 v0, 0x2

    invoke-virtual {v2, v0, p2}, Lcom/google/p/a/b/b/a;->f(II)Lcom/google/p/a/b/b/a;

    .line 1263
    iput p2, p0, Lcom/google/android/location/collectionlib/ay;->v:I

    .line 1265
    :cond_8
    iget-object v0, p0, Lcom/google/android/location/collectionlib/ay;->c:Lcom/google/p/a/b/b/a;

    const/16 v1, 0xb

    invoke-virtual {v0, v1, v2}, Lcom/google/p/a/b/b/a;->a(ILcom/google/p/a/b/b/a;)V

    .line 1266
    invoke-direct {p0}, Lcom/google/android/location/collectionlib/ay;->c()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/16 :goto_0

    .line 1217
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 1248
    :cond_9
    :try_start_2
    iget-wide v0, p0, Lcom/google/android/location/collectionlib/ay;->I:J

    invoke-direct {p0, v0, v1, p3, p4}, Lcom/google/android/location/collectionlib/ay;->c(JJ)I

    move-result v1

    .line 1249
    iget-wide v4, p0, Lcom/google/android/location/collectionlib/ay;->I:J

    invoke-direct {p0, v4, v5, p3, p4}, Lcom/google/android/location/collectionlib/ay;->d(JJ)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result v0

    goto :goto_1
.end method

.method public final declared-synchronized a(ILjava/lang/String;Landroid/telephony/CellLocation;ILjava/util/List;J)V
    .locals 10

    .prologue
    .line 737
    monitor-enter p0

    :try_start_0
    iget-boolean v2, p0, Lcom/google/android/location/collectionlib/ay;->j:Z

    if-eqz v2, :cond_1

    .line 738
    sget-boolean v2, Lcom/google/android/location/i/a;->b:Z

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/location/collectionlib/ay;->W:Lcom/google/android/location/o/a/c;

    const-string v3, "Could not add cell profile after the composer is closed."

    invoke-virtual {v2, v3}, Lcom/google/android/location/o/a/c;->a(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 787
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 741
    :cond_1
    :try_start_1
    move-wide/from16 v0, p6

    invoke-direct {p0, v0, v1}, Lcom/google/android/location/collectionlib/ay;->a(J)V

    .line 742
    const/4 v5, -0x1

    .line 743
    const/4 v4, -0x1

    .line 744
    const/4 v3, -0x1

    .line 745
    const/4 v2, -0x1

    .line 746
    if-eqz p2, :cond_2

    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v6

    const/4 v7, 0x3

    if-le v6, v7, :cond_2

    .line 747
    const/4 v4, 0x0

    const/4 v5, 0x3

    invoke-virtual {p2, v4, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v5

    .line 748
    const/4 v4, 0x3

    invoke-virtual {p2, v4}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    .line 750
    :cond_2
    if-eqz p3, :cond_3

    .line 752
    instance-of v6, p3, Landroid/telephony/gsm/GsmCellLocation;

    if-eqz v6, :cond_5

    .line 753
    move-object v0, p3

    check-cast v0, Landroid/telephony/gsm/GsmCellLocation;

    move-object v2, v0

    invoke-virtual {v2}, Landroid/telephony/gsm/GsmCellLocation;->getLac()I

    move-result v3

    .line 754
    check-cast p3, Landroid/telephony/gsm/GsmCellLocation;

    invoke-virtual {p3}, Landroid/telephony/gsm/GsmCellLocation;->getCid()I

    move-result v2

    .line 760
    :cond_3
    :goto_1
    new-instance v6, Lcom/google/p/a/b/b/a;

    sget-object v7, Lcom/google/android/location/m/a;->k:Lcom/google/p/a/b/b/c;

    invoke-direct {v6, v7}, Lcom/google/p/a/b/b/a;-><init>(Lcom/google/p/a/b/b/c;)V

    .line 761
    new-instance v7, Lcom/google/p/a/b/b/a;

    sget-object v8, Lcom/google/android/location/m/a;->j:Lcom/google/p/a/b/b/c;

    invoke-direct {v7, v8}, Lcom/google/p/a/b/b/a;-><init>(Lcom/google/p/a/b/b/c;)V

    .line 762
    const/4 v8, 0x1

    invoke-virtual {v7, v8, v3}, Lcom/google/p/a/b/b/a;->f(II)Lcom/google/p/a/b/b/a;

    .line 763
    const/4 v3, 0x2

    invoke-virtual {v7, v3, v2}, Lcom/google/p/a/b/b/a;->f(II)Lcom/google/p/a/b/b/a;

    .line 764
    const/4 v2, 0x3

    invoke-virtual {v7, v2, v4}, Lcom/google/p/a/b/b/a;->f(II)Lcom/google/p/a/b/b/a;

    .line 765
    const/4 v2, 0x4

    invoke-virtual {v7, v2, v5}, Lcom/google/p/a/b/b/a;->f(II)Lcom/google/p/a/b/b/a;

    .line 766
    const/4 v2, 0x5

    invoke-virtual {v7, v2, p4}, Lcom/google/p/a/b/b/a;->f(II)Lcom/google/p/a/b/b/a;

    .line 767
    packed-switch p1, :pswitch_data_0

    :pswitch_0
    const/4 v2, -0x1

    .line 768
    :goto_2
    if-ltz v2, :cond_4

    .line 769
    const/16 v3, 0xa

    invoke-virtual {v7, v3, v2}, Lcom/google/p/a/b/b/a;->f(II)Lcom/google/p/a/b/b/a;

    .line 771
    :cond_4
    const/4 v2, 0x1

    invoke-virtual {v6, v2, v7}, Lcom/google/p/a/b/b/a;->b(ILcom/google/p/a/b/b/a;)Lcom/google/p/a/b/b/a;

    .line 772
    const/4 v2, 0x2

    iget-wide v4, p0, Lcom/google/android/location/collectionlib/ay;->l:J

    sub-long v4, p6, v4

    invoke-virtual {v6, v2, v4, v5}, Lcom/google/p/a/b/b/a;->b(IJ)Lcom/google/p/a/b/b/a;

    .line 774
    if-eqz p5, :cond_6

    .line 775
    invoke-interface {p5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_3
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_6

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/telephony/NeighboringCellInfo;

    .line 776
    new-instance v4, Lcom/google/p/a/b/b/a;

    sget-object v5, Lcom/google/android/location/m/a;->j:Lcom/google/p/a/b/b/c;

    invoke-direct {v4, v5}, Lcom/google/p/a/b/b/a;-><init>(Lcom/google/p/a/b/b/c;)V

    .line 777
    const/4 v5, 0x1

    invoke-virtual {v2}, Landroid/telephony/NeighboringCellInfo;->getLac()I

    move-result v7

    invoke-virtual {v4, v5, v7}, Lcom/google/p/a/b/b/a;->f(II)Lcom/google/p/a/b/b/a;

    .line 778
    const/4 v5, 0x2

    invoke-virtual {v2}, Landroid/telephony/NeighboringCellInfo;->getCid()I

    move-result v7

    invoke-virtual {v4, v5, v7}, Lcom/google/p/a/b/b/a;->f(II)Lcom/google/p/a/b/b/a;

    .line 779
    const/16 v5, 0x8

    invoke-virtual {v2}, Landroid/telephony/NeighboringCellInfo;->getPsc()I

    move-result v7

    invoke-virtual {v4, v5, v7}, Lcom/google/p/a/b/b/a;->f(II)Lcom/google/p/a/b/b/a;

    .line 780
    const/4 v5, 0x5

    invoke-virtual {v2}, Landroid/telephony/NeighboringCellInfo;->getRssi()I

    move-result v2

    invoke-virtual {v4, v5, v2}, Lcom/google/p/a/b/b/a;->f(II)Lcom/google/p/a/b/b/a;

    .line 781
    const/4 v2, 0x3

    invoke-virtual {v6, v2, v4}, Lcom/google/p/a/b/b/a;->a(ILcom/google/p/a/b/b/a;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_3

    .line 737
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2

    .line 755
    :cond_5
    :try_start_2
    instance-of v6, p3, Landroid/telephony/cdma/CdmaCellLocation;

    if-eqz v6, :cond_3

    .line 756
    move-object v0, p3

    check-cast v0, Landroid/telephony/cdma/CdmaCellLocation;

    move-object v2, v0

    invoke-virtual {v2}, Landroid/telephony/cdma/CdmaCellLocation;->getSystemId()I

    move-result v4

    .line 757
    move-object v0, p3

    check-cast v0, Landroid/telephony/cdma/CdmaCellLocation;

    move-object v2, v0

    invoke-virtual {v2}, Landroid/telephony/cdma/CdmaCellLocation;->getNetworkId()I

    move-result v3

    .line 758
    check-cast p3, Landroid/telephony/cdma/CdmaCellLocation;

    invoke-virtual {p3}, Landroid/telephony/cdma/CdmaCellLocation;->getBaseStationId()I

    move-result v2

    goto/16 :goto_1

    .line 767
    :pswitch_1
    const/4 v2, 0x3

    goto :goto_2

    :pswitch_2
    const/4 v2, 0x5

    goto :goto_2

    :pswitch_3
    const/4 v2, 0x4

    goto :goto_2

    .line 784
    :cond_6
    iget-object v2, p0, Lcom/google/android/location/collectionlib/ay;->c:Lcom/google/p/a/b/b/a;

    const/4 v3, 0x6

    invoke-virtual {v2, v3, v6}, Lcom/google/p/a/b/b/a;->a(ILcom/google/p/a/b/b/a;)V

    .line 786
    invoke-direct {p0}, Lcom/google/android/location/collectionlib/ay;->c()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_0

    .line 767
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_0
        :pswitch_3
        :pswitch_0
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public final declared-synchronized a(Landroid/location/GpsMeasurementsEvent;J)V
    .locals 8

    .prologue
    .line 581
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/location/collectionlib/ay;->j:Z

    if-eqz v0, :cond_1

    .line 582
    sget-boolean v0, Lcom/google/android/location/i/a;->b:Z

    if-eqz v0, :cond_0

    .line 583
    iget-object v0, p0, Lcom/google/android/location/collectionlib/ay;->W:Lcom/google/android/location/o/a/c;

    const-string v1, "Could not add GPS Measurements after the composer is closed."

    invoke-virtual {v0, v1}, Lcom/google/android/location/o/a/c;->a(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 664
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 587
    :cond_1
    if-eqz p1, :cond_0

    .line 590
    :try_start_1
    invoke-direct {p0, p2, p3}, Lcom/google/android/location/collectionlib/ay;->a(J)V

    .line 592
    new-instance v1, Lcom/google/p/a/b/b/a;

    sget-object v0, Lcom/google/android/location/m/a;->bk:Lcom/google/p/a/b/b/c;

    invoke-direct {v1, v0}, Lcom/google/p/a/b/b/a;-><init>(Lcom/google/p/a/b/b/c;)V

    .line 593
    invoke-virtual {p1}, Landroid/location/GpsMeasurementsEvent;->getClock()Landroid/location/GpsClock;

    move-result-object v0

    .line 594
    new-instance v2, Lcom/google/p/a/b/b/a;

    sget-object v3, Lcom/google/android/location/m/a;->bi:Lcom/google/p/a/b/b/c;

    invoke-direct {v2, v3}, Lcom/google/p/a/b/b/a;-><init>(Lcom/google/p/a/b/b/c;)V

    .line 595
    invoke-virtual {v0}, Landroid/location/GpsClock;->hasLeapSecond()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 596
    const/4 v3, 0x1

    invoke-virtual {v0}, Landroid/location/GpsClock;->getLeapSecond()S

    move-result v4

    invoke-virtual {v2, v3, v4}, Lcom/google/p/a/b/b/a;->f(II)Lcom/google/p/a/b/b/a;

    .line 599
    :cond_2
    const/4 v3, 0x2

    invoke-virtual {v0}, Landroid/location/GpsClock;->getType()B

    move-result v4

    invoke-virtual {v2, v3, v4}, Lcom/google/p/a/b/b/a;->f(II)Lcom/google/p/a/b/b/a;

    .line 600
    const/4 v3, 0x3

    invoke-virtual {v0}, Landroid/location/GpsClock;->getTimeInNs()J

    move-result-wide v4

    invoke-virtual {v2, v3, v4, v5}, Lcom/google/p/a/b/b/a;->b(IJ)Lcom/google/p/a/b/b/a;

    .line 601
    invoke-virtual {v0}, Landroid/location/GpsClock;->hasTimeUncertaintyInNs()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 602
    const/4 v3, 0x4

    invoke-virtual {v0}, Landroid/location/GpsClock;->getTimeUncertaintyInNs()D

    move-result-wide v4

    invoke-virtual {v2, v3, v4, v5}, Lcom/google/p/a/b/b/a;->b(ID)Lcom/google/p/a/b/b/a;

    .line 604
    :cond_3
    invoke-virtual {v0}, Landroid/location/GpsClock;->hasFullBiasInNs()Z

    move-result v3

    if-eqz v3, :cond_4

    .line 605
    const/4 v3, 0x5

    invoke-virtual {v0}, Landroid/location/GpsClock;->getFullBiasInNs()J

    move-result-wide v4

    invoke-virtual {v2, v3, v4, v5}, Lcom/google/p/a/b/b/a;->b(IJ)Lcom/google/p/a/b/b/a;

    .line 607
    :cond_4
    invoke-virtual {v0}, Landroid/location/GpsClock;->hasBiasInNs()Z

    move-result v3

    if-eqz v3, :cond_5

    .line 608
    const/4 v3, 0x6

    invoke-virtual {v0}, Landroid/location/GpsClock;->getBiasInNs()D

    move-result-wide v4

    invoke-virtual {v2, v3, v4, v5}, Lcom/google/p/a/b/b/a;->b(ID)Lcom/google/p/a/b/b/a;

    .line 610
    :cond_5
    invoke-virtual {v0}, Landroid/location/GpsClock;->hasBiasUncertaintyInNs()Z

    move-result v3

    if-eqz v3, :cond_6

    .line 611
    const/4 v3, 0x7

    invoke-virtual {v0}, Landroid/location/GpsClock;->getBiasUncertaintyInNs()D

    move-result-wide v4

    invoke-virtual {v2, v3, v4, v5}, Lcom/google/p/a/b/b/a;->b(ID)Lcom/google/p/a/b/b/a;

    .line 613
    :cond_6
    invoke-virtual {v0}, Landroid/location/GpsClock;->hasDriftInNsPerSec()Z

    move-result v3

    if-eqz v3, :cond_7

    .line 614
    const/16 v3, 0x8

    invoke-virtual {v0}, Landroid/location/GpsClock;->getDriftInNsPerSec()D

    move-result-wide v4

    invoke-virtual {v2, v3, v4, v5}, Lcom/google/p/a/b/b/a;->b(ID)Lcom/google/p/a/b/b/a;

    .line 616
    :cond_7
    invoke-virtual {v0}, Landroid/location/GpsClock;->hasDriftUncertaintyInNsPerSec()Z

    move-result v3

    if-eqz v3, :cond_8

    .line 617
    const/16 v3, 0x9

    invoke-virtual {v0}, Landroid/location/GpsClock;->getDriftUncertaintyInNsPerSec()D

    move-result-wide v4

    invoke-virtual {v2, v3, v4, v5}, Lcom/google/p/a/b/b/a;->b(ID)Lcom/google/p/a/b/b/a;

    .line 621
    :cond_8
    const/4 v0, 0x2

    invoke-virtual {v1, v0, v2}, Lcom/google/p/a/b/b/a;->b(ILcom/google/p/a/b/b/a;)Lcom/google/p/a/b/b/a;

    .line 623
    invoke-virtual {p1}, Landroid/location/GpsMeasurementsEvent;->getMeasurements()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_9

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/location/GpsMeasurement;

    .line 624
    new-instance v3, Lcom/google/p/a/b/b/a;

    sget-object v4, Lcom/google/android/location/m/a;->bj:Lcom/google/p/a/b/b/c;

    invoke-direct {v3, v4}, Lcom/google/p/a/b/b/a;-><init>(Lcom/google/p/a/b/b/c;)V

    .line 625
    const/4 v4, 0x1

    invoke-virtual {v0}, Landroid/location/GpsMeasurement;->getPrn()B

    move-result v5

    invoke-virtual {v3, v4, v5}, Lcom/google/p/a/b/b/a;->f(II)Lcom/google/p/a/b/b/a;

    .line 626
    const/4 v4, 0x2

    invoke-virtual {v0}, Landroid/location/GpsMeasurement;->getTimeOffsetInNs()D

    move-result-wide v6

    invoke-virtual {v3, v4, v6, v7}, Lcom/google/p/a/b/b/a;->b(ID)Lcom/google/p/a/b/b/a;

    .line 628
    const/4 v4, 0x3

    invoke-virtual {v0}, Landroid/location/GpsMeasurement;->getState()S

    move-result v5

    int-to-double v6, v5

    invoke-virtual {v3, v4, v6, v7}, Lcom/google/p/a/b/b/a;->b(ID)Lcom/google/p/a/b/b/a;

    .line 629
    const/4 v4, 0x4

    invoke-virtual {v0}, Landroid/location/GpsMeasurement;->getReceivedGpsTowInNs()J

    move-result-wide v6

    invoke-virtual {v3, v4, v6, v7}, Lcom/google/p/a/b/b/a;->b(IJ)Lcom/google/p/a/b/b/a;

    .line 631
    const/4 v4, 0x5

    invoke-virtual {v0}, Landroid/location/GpsMeasurement;->getReceivedGpsTowUncertaintyInNs()J

    move-result-wide v6

    invoke-virtual {v3, v4, v6, v7}, Lcom/google/p/a/b/b/a;->b(IJ)Lcom/google/p/a/b/b/a;

    .line 634
    const/4 v4, 0x6

    invoke-virtual {v0}, Landroid/location/GpsMeasurement;->getCn0InDbHz()D

    move-result-wide v6

    invoke-virtual {v3, v4, v6, v7}, Lcom/google/p/a/b/b/a;->b(ID)Lcom/google/p/a/b/b/a;

    .line 635
    const/4 v4, 0x7

    invoke-virtual {v0}, Landroid/location/GpsMeasurement;->getPseudorangeRateInMetersPerSec()D

    move-result-wide v6

    invoke-virtual {v3, v4, v6, v7}, Lcom/google/p/a/b/b/a;->b(ID)Lcom/google/p/a/b/b/a;

    .line 638
    const/16 v4, 0x8

    invoke-virtual {v0}, Landroid/location/GpsMeasurement;->getPseudorangeRateUncertaintyInMetersPerSec()D

    move-result-wide v6

    invoke-virtual {v3, v4, v6, v7}, Lcom/google/p/a/b/b/a;->b(ID)Lcom/google/p/a/b/b/a;

    .line 642
    const/16 v4, 0x9

    invoke-virtual {v0}, Landroid/location/GpsMeasurement;->getAccumulatedDeltaRangeState()S

    move-result v5

    invoke-virtual {v3, v4, v5}, Lcom/google/p/a/b/b/a;->f(II)Lcom/google/p/a/b/b/a;

    .line 645
    const/16 v4, 0xa

    invoke-virtual {v0}, Landroid/location/GpsMeasurement;->getAccumulatedDeltaRangeInMeters()D

    move-result-wide v6

    invoke-virtual {v3, v4, v6, v7}, Lcom/google/p/a/b/b/a;->b(ID)Lcom/google/p/a/b/b/a;

    .line 648
    const/16 v4, 0xb

    invoke-virtual {v0}, Landroid/location/GpsMeasurement;->getAccumulatedDeltaRangeUncertaintyInMeters()D

    move-result-wide v6

    invoke-virtual {v3, v4, v6, v7}, Lcom/google/p/a/b/b/a;->b(ID)Lcom/google/p/a/b/b/a;

    .line 653
    const/4 v0, 0x1

    invoke-virtual {v1, v0, v3}, Lcom/google/p/a/b/b/a;->a(ILcom/google/p/a/b/b/a;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 581
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 656
    :cond_9
    :try_start_2
    new-instance v0, Lcom/google/p/a/b/b/a;

    sget-object v2, Lcom/google/android/location/m/a;->bl:Lcom/google/p/a/b/b/c;

    invoke-direct {v0, v2}, Lcom/google/p/a/b/b/a;-><init>(Lcom/google/p/a/b/b/c;)V

    .line 657
    const/4 v2, 0x1

    iget-wide v4, p0, Lcom/google/android/location/collectionlib/ay;->l:J

    sub-long v4, p2, v4

    long-to-int v3, v4

    invoke-virtual {v0, v2, v3}, Lcom/google/p/a/b/b/a;->f(II)Lcom/google/p/a/b/b/a;

    .line 660
    const/4 v2, 0x2

    invoke-virtual {v0, v2, v1}, Lcom/google/p/a/b/b/a;->b(ILcom/google/p/a/b/b/a;)Lcom/google/p/a/b/b/a;

    .line 662
    iget-object v1, p0, Lcom/google/android/location/collectionlib/ay;->c:Lcom/google/p/a/b/b/a;

    const/16 v2, 0x13

    invoke-virtual {v1, v2, v0}, Lcom/google/p/a/b/b/a;->a(ILcom/google/p/a/b/b/a;)V

    .line 663
    invoke-direct {p0}, Lcom/google/android/location/collectionlib/ay;->c()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_0
.end method

.method public final declared-synchronized a(Landroid/location/GpsNavigationMessageEvent;J)V
    .locals 2

    .prologue
    .line 675
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/location/collectionlib/ay;->j:Z

    if-eqz v0, :cond_1

    .line 676
    sget-boolean v0, Lcom/google/android/location/i/a;->b:Z

    if-eqz v0, :cond_0

    .line 677
    iget-object v0, p0, Lcom/google/android/location/collectionlib/ay;->W:Lcom/google/android/location/o/a/c;

    const-string v1, "Could not add GPS Navigation Message after the composer is closed."

    invoke-virtual {v0, v1}, Lcom/google/android/location/o/a/c;->a(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 688
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 681
    :cond_1
    if-eqz p1, :cond_0

    .line 684
    :try_start_1
    invoke-direct {p0, p2, p3}, Lcom/google/android/location/collectionlib/ay;->a(J)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 675
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Landroid/location/GpsStatus;J)V
    .locals 6

    .prologue
    .line 548
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/location/collectionlib/ay;->j:Z

    if-eqz v0, :cond_1

    .line 549
    sget-boolean v0, Lcom/google/android/location/i/a;->b:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/location/collectionlib/ay;->W:Lcom/google/android/location/o/a/c;

    const-string v1, "Could not add GPS status after the composer is closed."

    invoke-virtual {v0, v1}, Lcom/google/android/location/o/a/c;->a(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 572
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 552
    :cond_1
    if-eqz p1, :cond_0

    .line 555
    :try_start_1
    invoke-direct {p0, p2, p3}, Lcom/google/android/location/collectionlib/ay;->a(J)V

    .line 558
    invoke-virtual {p1}, Landroid/location/GpsStatus;->getSatellites()Ljava/lang/Iterable;

    move-result-object v0

    .line 559
    new-instance v1, Lcom/google/p/a/b/b/a;

    sget-object v2, Lcom/google/android/location/m/a;->ba:Lcom/google/p/a/b/b/c;

    invoke-direct {v1, v2}, Lcom/google/p/a/b/b/a;-><init>(Lcom/google/p/a/b/b/c;)V

    .line 560
    const/4 v2, 0x1

    iget-wide v4, p0, Lcom/google/android/location/collectionlib/ay;->l:J

    sub-long v4, p2, v4

    long-to-int v3, v4

    invoke-virtual {v1, v2, v3}, Lcom/google/p/a/b/b/a;->f(II)Lcom/google/p/a/b/b/a;

    .line 562
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/location/GpsSatellite;

    .line 563
    new-instance v3, Lcom/google/p/a/b/b/a;

    sget-object v4, Lcom/google/android/location/m/a;->aZ:Lcom/google/p/a/b/b/c;

    invoke-direct {v3, v4}, Lcom/google/p/a/b/b/a;-><init>(Lcom/google/p/a/b/b/c;)V

    .line 564
    const/4 v4, 0x4

    invoke-virtual {v0}, Landroid/location/GpsSatellite;->getAzimuth()F

    move-result v5

    invoke-virtual {v3, v4, v5}, Lcom/google/p/a/b/b/a;->a(IF)Lcom/google/p/a/b/b/a;

    .line 565
    const/4 v4, 0x3

    invoke-virtual {v0}, Landroid/location/GpsSatellite;->getElevation()F

    move-result v5

    invoke-virtual {v3, v4, v5}, Lcom/google/p/a/b/b/a;->a(IF)Lcom/google/p/a/b/b/a;

    .line 566
    const/4 v4, 0x1

    invoke-virtual {v0}, Landroid/location/GpsSatellite;->getPrn()I

    move-result v5

    invoke-virtual {v3, v4, v5}, Lcom/google/p/a/b/b/a;->f(II)Lcom/google/p/a/b/b/a;

    .line 567
    const/4 v4, 0x2

    invoke-virtual {v0}, Landroid/location/GpsSatellite;->getSnr()F

    move-result v0

    invoke-virtual {v3, v4, v0}, Lcom/google/p/a/b/b/a;->a(IF)Lcom/google/p/a/b/b/a;

    .line 568
    const/4 v0, 0x2

    invoke-virtual {v1, v0, v3}, Lcom/google/p/a/b/b/a;->a(ILcom/google/p/a/b/b/a;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 548
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 570
    :cond_2
    :try_start_2
    iget-object v0, p0, Lcom/google/android/location/collectionlib/ay;->c:Lcom/google/p/a/b/b/a;

    const/16 v2, 0xa

    invoke-virtual {v0, v2, v1}, Lcom/google/p/a/b/b/a;->a(ILcom/google/p/a/b/b/a;)V

    .line 571
    invoke-direct {p0}, Lcom/google/android/location/collectionlib/ay;->c()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method

.method public final declared-synchronized a(Ljava/util/List;J)V
    .locals 8

    .prologue
    .line 698
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/location/collectionlib/ay;->j:Z

    if-eqz v0, :cond_1

    .line 699
    sget-boolean v0, Lcom/google/android/location/i/a;->b:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/location/collectionlib/ay;->W:Lcom/google/android/location/o/a/c;

    const-string v1, "Could not add wifi profile after the composer is closed."

    invoke-virtual {v0, v1}, Lcom/google/android/location/o/a/c;->a(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 723
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 702
    :cond_1
    if-eqz p1, :cond_0

    :try_start_1
    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 706
    invoke-direct {p0, p2, p3}, Lcom/google/android/location/collectionlib/ay;->a(J)V

    .line 707
    new-instance v1, Lcom/google/p/a/b/b/a;

    sget-object v0, Lcom/google/android/location/m/a;->h:Lcom/google/p/a/b/b/c;

    invoke-direct {v1, v0}, Lcom/google/p/a/b/b/a;-><init>(Lcom/google/p/a/b/b/c;)V

    .line 708
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_2
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/wifi/ScanResult;

    .line 709
    new-instance v3, Lcom/google/p/a/b/b/a;

    sget-object v4, Lcom/google/android/location/m/a;->f:Lcom/google/p/a/b/b/c;

    invoke-direct {v3, v4}, Lcom/google/p/a/b/b/a;-><init>(Lcom/google/p/a/b/b/c;)V

    .line 710
    iget-object v4, v0, Landroid/net/wifi/ScanResult;->BSSID:Ljava/lang/String;

    invoke-static {v4}, Lcom/google/android/location/collectionlib/df;->c(Ljava/lang/String;)J

    move-result-wide v4

    .line 711
    const-wide/16 v6, -0x1

    cmp-long v6, v4, v6

    if-eqz v6, :cond_2

    .line 712
    const/4 v6, 0x1

    const-string v7, ""

    invoke-virtual {v3, v6, v7}, Lcom/google/p/a/b/b/a;->a(ILjava/lang/String;)Lcom/google/p/a/b/b/a;

    .line 715
    const/16 v6, 0x8

    invoke-virtual {v3, v6, v4, v5}, Lcom/google/p/a/b/b/a;->b(IJ)Lcom/google/p/a/b/b/a;

    .line 716
    const/16 v4, 0x9

    iget v5, v0, Landroid/net/wifi/ScanResult;->level:I

    invoke-virtual {v3, v4, v5}, Lcom/google/p/a/b/b/a;->f(II)Lcom/google/p/a/b/b/a;

    .line 717
    iget v0, v0, Landroid/net/wifi/ScanResult;->frequency:I

    invoke-static {v3, v0}, Lcom/google/android/location/e/bg;->a(Lcom/google/p/a/b/b/a;I)V

    .line 718
    const/4 v0, 0x2

    invoke-virtual {v1, v0, v3}, Lcom/google/p/a/b/b/a;->a(ILcom/google/p/a/b/b/a;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 698
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 720
    :cond_3
    const/4 v0, 0x1

    :try_start_2
    iget-wide v2, p0, Lcom/google/android/location/collectionlib/ay;->l:J

    sub-long v2, p2, v2

    invoke-virtual {v1, v0, v2, v3}, Lcom/google/p/a/b/b/a;->b(IJ)Lcom/google/p/a/b/b/a;

    .line 721
    iget-object v0, p0, Lcom/google/android/location/collectionlib/ay;->c:Lcom/google/p/a/b/b/a;

    const/4 v2, 0x7

    invoke-virtual {v0, v2, v1}, Lcom/google/p/a/b/b/a;->a(ILcom/google/p/a/b/b/a;)V

    .line 722
    invoke-direct {p0}, Lcom/google/android/location/collectionlib/ay;->c()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method

.method public final declared-synchronized a([D[D[D[D[DJ)V
    .locals 8

    .prologue
    const/4 v0, 0x0

    .line 1543
    monitor-enter p0

    :try_start_0
    iget-boolean v1, p0, Lcom/google/android/location/collectionlib/ay;->j:Z

    if-eqz v1, :cond_1

    .line 1544
    sget-boolean v0, Lcom/google/android/location/i/a;->b:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/location/collectionlib/ay;->W:Lcom/google/android/location/o/a/c;

    const-string v1, "Could not add sound snapshot after the composer is closed."

    invoke-virtual {v0, v1}, Lcom/google/android/location/o/a/c;->a(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1569
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 1547
    :cond_1
    :try_start_1
    invoke-direct {p0, p6, p7}, Lcom/google/android/location/collectionlib/ay;->a(J)V

    .line 1548
    new-instance v2, Lcom/google/p/a/b/b/a;

    sget-object v1, Lcom/google/android/location/m/a;->bg:Lcom/google/p/a/b/b/c;

    const/4 v3, 0x6

    invoke-direct {v2, v1, v3}, Lcom/google/p/a/b/b/a;-><init>(Lcom/google/p/a/b/b/c;I)V

    .line 1549
    array-length v3, p1

    move v1, v0

    :goto_1
    if-ge v1, v3, :cond_2

    aget-wide v4, p1, v1

    .line 1550
    const/4 v6, 0x1

    invoke-virtual {v2, v6, v4, v5}, Lcom/google/p/a/b/b/a;->a(ID)V

    .line 1549
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 1552
    :cond_2
    array-length v3, p2

    move v1, v0

    :goto_2
    if-ge v1, v3, :cond_3

    aget-wide v4, p2, v1

    .line 1553
    const/4 v6, 0x2

    invoke-virtual {v2, v6, v4, v5}, Lcom/google/p/a/b/b/a;->a(ID)V

    .line 1552
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 1555
    :cond_3
    array-length v3, p3

    move v1, v0

    :goto_3
    if-ge v1, v3, :cond_4

    aget-wide v4, p3, v1

    .line 1556
    const/4 v6, 0x3

    invoke-virtual {v2, v6, v4, v5}, Lcom/google/p/a/b/b/a;->a(ID)V

    .line 1555
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 1558
    :cond_4
    array-length v3, p4

    move v1, v0

    :goto_4
    if-ge v1, v3, :cond_5

    aget-wide v4, p4, v1

    .line 1559
    const/4 v6, 0x4

    invoke-virtual {v2, v6, v4, v5}, Lcom/google/p/a/b/b/a;->a(ID)V

    .line 1558
    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    .line 1561
    :cond_5
    array-length v1, p5

    :goto_5
    if-ge v0, v1, :cond_6

    aget-wide v4, p5, v0

    .line 1562
    const/4 v3, 0x5

    invoke-virtual {v2, v3, v4, v5}, Lcom/google/p/a/b/b/a;->a(ID)V

    .line 1561
    add-int/lit8 v0, v0, 0x1

    goto :goto_5

    .line 1564
    :cond_6
    const/4 v0, 0x6

    iget-wide v4, p0, Lcom/google/android/location/collectionlib/ay;->l:J

    sub-long v4, p6, v4

    long-to-int v1, v4

    invoke-virtual {v2, v0, v1}, Lcom/google/p/a/b/b/a;->f(II)Lcom/google/p/a/b/b/a;

    .line 1567
    iget-object v0, p0, Lcom/google/android/location/collectionlib/ay;->c:Lcom/google/p/a/b/b/a;

    const/16 v1, 0xf

    invoke-virtual {v0, v1, v2}, Lcom/google/p/a/b/b/a;->a(ILcom/google/p/a/b/b/a;)V

    .line 1568
    invoke-direct {p0}, Lcom/google/android/location/collectionlib/ay;->c()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 1543
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Landroid/location/Location;J)Z
    .locals 8

    .prologue
    const/4 v0, 0x0

    const-wide v6, 0x416312d000000000L    # 1.0E7

    .line 516
    monitor-enter p0

    if-nez p1, :cond_0

    .line 537
    :goto_0
    monitor-exit p0

    return v0

    .line 519
    :cond_0
    :try_start_0
    new-instance v0, Lcom/google/p/a/b/b/a;

    sget-object v1, Lcom/google/android/location/m/a;->r:Lcom/google/p/a/b/b/c;

    invoke-direct {v0, v1}, Lcom/google/p/a/b/b/a;-><init>(Lcom/google/p/a/b/b/c;)V

    .line 520
    new-instance v1, Lcom/google/p/a/b/b/a;

    sget-object v2, Lcom/google/android/location/m/a;->i:Lcom/google/p/a/b/b/c;

    invoke-direct {v1, v2}, Lcom/google/p/a/b/b/a;-><init>(Lcom/google/p/a/b/b/c;)V

    .line 521
    const/4 v2, 0x1

    invoke-virtual {p1}, Landroid/location/Location;->getLatitude()D

    move-result-wide v4

    mul-double/2addr v4, v6

    double-to-int v3, v4

    invoke-virtual {v1, v2, v3}, Lcom/google/p/a/b/b/a;->f(II)Lcom/google/p/a/b/b/a;

    .line 522
    const/4 v2, 0x2

    invoke-virtual {p1}, Landroid/location/Location;->getLongitude()D

    move-result-wide v4

    mul-double/2addr v4, v6

    double-to-int v3, v4

    invoke-virtual {v1, v2, v3}, Lcom/google/p/a/b/b/a;->f(II)Lcom/google/p/a/b/b/a;

    .line 523
    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/google/p/a/b/b/a;->b(ILcom/google/p/a/b/b/a;)Lcom/google/p/a/b/b/a;

    .line 524
    invoke-virtual {p1}, Landroid/location/Location;->hasAccuracy()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 525
    const/4 v1, 0x3

    invoke-virtual {p1}, Landroid/location/Location;->getAccuracy()F

    move-result v2

    float-to-int v2, v2

    invoke-virtual {v0, v1, v2}, Lcom/google/p/a/b/b/a;->f(II)Lcom/google/p/a/b/b/a;

    .line 527
    :cond_1
    invoke-virtual {p1}, Landroid/location/Location;->hasAltitude()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 528
    const/16 v1, 0xa

    invoke-virtual {p1}, Landroid/location/Location;->getAltitude()D

    move-result-wide v2

    double-to-int v2, v2

    invoke-virtual {v0, v1, v2}, Lcom/google/p/a/b/b/a;->f(II)Lcom/google/p/a/b/b/a;

    .line 530
    :cond_2
    invoke-virtual {p1}, Landroid/location/Location;->hasBearing()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 531
    const/16 v1, 0xd

    invoke-virtual {p1}, Landroid/location/Location;->getBearing()F

    move-result v2

    float-to-int v2, v2

    invoke-virtual {v0, v1, v2}, Lcom/google/p/a/b/b/a;->f(II)Lcom/google/p/a/b/b/a;

    .line 533
    :cond_3
    invoke-virtual {p1}, Landroid/location/Location;->hasSpeed()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 534
    const/16 v1, 0x10

    invoke-virtual {p1}, Landroid/location/Location;->getSpeed()F

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/google/p/a/b/b/a;->a(IF)Lcom/google/p/a/b/b/a;

    .line 536
    :cond_4
    const/16 v1, 0x8

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/google/p/a/b/b/a;->f(II)Lcom/google/p/a/b/b/a;

    .line 537
    invoke-direct {p0, v0, p2, p3}, Lcom/google/android/location/collectionlib/ay;->a(Lcom/google/p/a/b/b/a;J)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    goto :goto_0

    .line 516
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Lcom/google/p/a/b/b/a;)Z
    .locals 2

    .prologue
    .line 479
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/location/collectionlib/ay;->j:Z

    if-eqz v0, :cond_1

    .line 480
    sget-boolean v0, Lcom/google/android/location/i/a;->b:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/location/collectionlib/ay;->W:Lcom/google/android/location/o/a/c;

    const-string v1, "Could not add customized data after the composer is closed."

    invoke-virtual {v0, v1}, Lcom/google/android/location/o/a/c;->a(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 481
    :cond_0
    const/4 v0, 0x0

    .line 485
    :goto_0
    monitor-exit p0

    return v0

    .line 483
    :cond_1
    :try_start_1
    iget-object v0, p0, Lcom/google/android/location/collectionlib/ay;->a:Lcom/google/p/a/b/b/a;

    const/4 v1, 0x4

    invoke-virtual {v0, v1, p1}, Lcom/google/p/a/b/b/a;->a(ILcom/google/p/a/b/b/a;)V

    .line 484
    invoke-direct {p0}, Lcom/google/android/location/collectionlib/ay;->c()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 485
    const/4 v0, 0x1

    goto :goto_0

    .line 479
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b(FFFIJJ)V
    .locals 13

    .prologue
    .line 945
    monitor-enter p0

    :try_start_0
    iget-boolean v4, p0, Lcom/google/android/location/collectionlib/ay;->j:Z

    if-eqz v4, :cond_1

    .line 946
    sget-boolean v4, Lcom/google/android/location/i/a;->b:Z

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/google/android/location/collectionlib/ay;->W:Lcom/google/android/location/o/a/c;

    const-string v5, "Could not add accelerometer snapshot after the composer is closed."

    invoke-virtual {v4, v5}, Lcom/google/android/location/o/a/c;->a(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 997
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 949
    :cond_1
    :try_start_1
    move-wide/from16 v0, p7

    invoke-direct {p0, v0, v1}, Lcom/google/android/location/collectionlib/ay;->a(J)V

    .line 950
    move-wide/from16 v0, p5

    move-wide/from16 v2, p7

    invoke-direct {p0, v0, v1, v2, v3}, Lcom/google/android/location/collectionlib/ay;->a(JJ)V

    .line 951
    new-instance v6, Lcom/google/p/a/b/b/a;

    sget-object v4, Lcom/google/android/location/m/a;->aV:Lcom/google/p/a/b/b/c;

    const/16 v5, 0x8

    invoke-direct {v6, v4, v5}, Lcom/google/p/a/b/b/a;-><init>(Lcom/google/p/a/b/b/c;I)V

    .line 952
    const/4 v4, 0x1

    invoke-virtual {v6, v4, p1}, Lcom/google/p/a/b/b/a;->a(IF)Lcom/google/p/a/b/b/a;

    .line 953
    const/4 v4, 0x2

    invoke-virtual {v6, v4, p2}, Lcom/google/p/a/b/b/a;->a(IF)Lcom/google/p/a/b/b/a;

    .line 954
    const/4 v4, 0x3

    move/from16 v0, p3

    invoke-virtual {v6, v4, v0}, Lcom/google/p/a/b/b/a;->a(IF)Lcom/google/p/a/b/b/a;

    .line 955
    iget-wide v4, p0, Lcom/google/android/location/collectionlib/ay;->N:J

    const-wide v8, 0x7fffffffffffffffL

    cmp-long v4, v4, v8

    if-nez v4, :cond_2

    .line 956
    iget-wide v4, p0, Lcom/google/android/location/collectionlib/ay;->o:J

    sub-long v4, v4, p5

    iget-wide v8, p0, Lcom/google/android/location/collectionlib/ay;->n:J

    sub-long v8, p7, v8

    const-wide/32 v10, 0xf4240

    mul-long/2addr v8, v10

    add-long/2addr v4, v8

    iput-wide v4, p0, Lcom/google/android/location/collectionlib/ay;->N:J

    .line 959
    :cond_2
    iget-object v4, p0, Lcom/google/android/location/collectionlib/ay;->c:Lcom/google/p/a/b/b/a;

    const/4 v5, 0x1

    invoke-virtual {v4, v5}, Lcom/google/p/a/b/b/a;->i(I)Z

    move-result v4

    if-nez v4, :cond_3

    .line 961
    iget-object v4, p0, Lcom/google/android/location/collectionlib/ay;->c:Lcom/google/p/a/b/b/a;

    const/4 v5, 0x1

    iget-wide v8, p0, Lcom/google/android/location/collectionlib/ay;->N:J

    move-wide/from16 v0, p5

    invoke-direct {p0, v0, v1, v8, v9}, Lcom/google/android/location/collectionlib/ay;->b(JJ)J

    move-result-wide v8

    invoke-virtual {v4, v5, v8, v9}, Lcom/google/p/a/b/b/a;->b(IJ)Lcom/google/p/a/b/b/a;

    .line 964
    :cond_3
    iget-object v4, p0, Lcom/google/android/location/collectionlib/ay;->c:Lcom/google/p/a/b/b/a;

    const/4 v5, 0x3

    invoke-virtual {v4, v5}, Lcom/google/p/a/b/b/a;->i(I)Z

    move-result v4

    if-nez v4, :cond_4

    .line 966
    const/16 v4, 0x9

    move-wide/from16 v0, p5

    invoke-virtual {v6, v4, v0, v1}, Lcom/google/p/a/b/b/a;->b(IJ)Lcom/google/p/a/b/b/a;

    .line 967
    const/16 v4, 0xa

    const-wide/32 v8, 0xf4240

    mul-long v8, v8, p7

    invoke-virtual {v6, v4, v8, v9}, Lcom/google/p/a/b/b/a;->b(IJ)Lcom/google/p/a/b/b/a;

    .line 971
    :cond_4
    iget-wide v4, p0, Lcom/google/android/location/collectionlib/ay;->D:J

    const-wide v8, 0x7fffffffffffffffL

    cmp-long v4, v4, v8

    if-nez v4, :cond_9

    .line 972
    iget-object v4, p0, Lcom/google/android/location/collectionlib/ay;->c:Lcom/google/p/a/b/b/a;

    const/4 v5, 0x1

    invoke-virtual {v4, v5}, Lcom/google/p/a/b/b/a;->d(I)J

    move-result-wide v8

    .line 973
    iget-wide v4, p0, Lcom/google/android/location/collectionlib/ay;->N:J

    move-wide/from16 v0, p5

    invoke-direct {p0, v0, v1, v4, v5}, Lcom/google/android/location/collectionlib/ay;->b(JJ)J

    move-result-wide v10

    .line 975
    invoke-direct {p0, v8, v9, v10, v11}, Lcom/google/android/location/collectionlib/ay;->c(JJ)I

    move-result v5

    .line 976
    invoke-direct {p0, v8, v9, v10, v11}, Lcom/google/android/location/collectionlib/ay;->d(JJ)I

    move-result v4

    .line 981
    :goto_1
    if-eqz v5, :cond_5

    .line 982
    const/4 v7, 0x7

    invoke-virtual {v6, v7, v5}, Lcom/google/p/a/b/b/a;->f(II)Lcom/google/p/a/b/b/a;

    .line 984
    :cond_5
    if-eqz v4, :cond_6

    .line 985
    const/16 v5, 0x8

    invoke-virtual {v6, v5, v4}, Lcom/google/p/a/b/b/a;->f(II)Lcom/google/p/a/b/b/a;

    .line 987
    :cond_6
    move-wide/from16 v0, p5

    iput-wide v0, p0, Lcom/google/android/location/collectionlib/ay;->D:J

    .line 990
    iget-object v4, p0, Lcom/google/android/location/collectionlib/ay;->c:Lcom/google/p/a/b/b/a;

    const/4 v5, 0x3

    invoke-virtual {v4, v5}, Lcom/google/p/a/b/b/a;->k(I)I

    move-result v4

    if-eqz v4, :cond_7

    iget v4, p0, Lcom/google/android/location/collectionlib/ay;->q:I

    move/from16 v0, p4

    if-eq v4, v0, :cond_8

    .line 992
    :cond_7
    const/4 v4, 0x4

    move/from16 v0, p4

    invoke-virtual {v6, v4, v0}, Lcom/google/p/a/b/b/a;->f(II)Lcom/google/p/a/b/b/a;

    .line 993
    move/from16 v0, p4

    iput v0, p0, Lcom/google/android/location/collectionlib/ay;->q:I

    .line 995
    :cond_8
    iget-object v4, p0, Lcom/google/android/location/collectionlib/ay;->c:Lcom/google/p/a/b/b/a;

    const/4 v5, 0x3

    invoke-virtual {v4, v5, v6}, Lcom/google/p/a/b/b/a;->a(ILcom/google/p/a/b/b/a;)V

    .line 996
    invoke-direct {p0}, Lcom/google/android/location/collectionlib/ay;->c()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/16 :goto_0

    .line 945
    :catchall_0
    move-exception v4

    monitor-exit p0

    throw v4

    .line 978
    :cond_9
    :try_start_2
    iget-wide v4, p0, Lcom/google/android/location/collectionlib/ay;->D:J

    move-wide/from16 v0, p5

    invoke-direct {p0, v4, v5, v0, v1}, Lcom/google/android/location/collectionlib/ay;->c(JJ)I

    move-result v5

    .line 979
    iget-wide v8, p0, Lcom/google/android/location/collectionlib/ay;->D:J

    move-wide/from16 v0, p5

    invoke-direct {p0, v8, v9, v0, v1}, Lcom/google/android/location/collectionlib/ay;->d(JJ)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result v4

    goto :goto_1
.end method

.method public final declared-synchronized b(FIJJ)V
    .locals 9

    .prologue
    .line 1281
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/location/collectionlib/ay;->j:Z

    if-eqz v0, :cond_1

    .line 1282
    sget-boolean v0, Lcom/google/android/location/i/a;->b:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/location/collectionlib/ay;->W:Lcom/google/android/location/o/a/c;

    const-string v1, "Could not add light snapshot after the composer is closed."

    invoke-virtual {v0, v1}, Lcom/google/android/location/o/a/c;->a(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1330
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 1285
    :cond_1
    :try_start_1
    invoke-direct {p0, p5, p6}, Lcom/google/android/location/collectionlib/ay;->a(J)V

    .line 1286
    invoke-direct {p0, p3, p4, p5, p6}, Lcom/google/android/location/collectionlib/ay;->a(JJ)V

    .line 1287
    new-instance v2, Lcom/google/p/a/b/b/a;

    sget-object v0, Lcom/google/android/location/m/a;->bc:Lcom/google/p/a/b/b/c;

    const/4 v1, 0x4

    invoke-direct {v2, v0, v1}, Lcom/google/p/a/b/b/a;-><init>(Lcom/google/p/a/b/b/c;I)V

    .line 1288
    const/4 v0, 0x1

    invoke-virtual {v2, v0, p1}, Lcom/google/p/a/b/b/a;->a(IF)Lcom/google/p/a/b/b/a;

    .line 1289
    iget-wide v0, p0, Lcom/google/android/location/collectionlib/ay;->T:J

    const-wide v4, 0x7fffffffffffffffL

    cmp-long v0, v0, v4

    if-nez v0, :cond_2

    .line 1290
    iget-wide v0, p0, Lcom/google/android/location/collectionlib/ay;->o:J

    sub-long/2addr v0, p3

    iget-wide v4, p0, Lcom/google/android/location/collectionlib/ay;->n:J

    sub-long v4, p5, v4

    const-wide/32 v6, 0xf4240

    mul-long/2addr v4, v6

    add-long/2addr v0, v4

    iput-wide v0, p0, Lcom/google/android/location/collectionlib/ay;->T:J

    .line 1293
    :cond_2
    iget-object v0, p0, Lcom/google/android/location/collectionlib/ay;->c:Lcom/google/p/a/b/b/a;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/p/a/b/b/a;->i(I)Z

    move-result v0

    if-nez v0, :cond_3

    .line 1295
    iget-object v0, p0, Lcom/google/android/location/collectionlib/ay;->c:Lcom/google/p/a/b/b/a;

    const/4 v1, 0x1

    iget-wide v4, p0, Lcom/google/android/location/collectionlib/ay;->T:J

    invoke-direct {p0, p3, p4, v4, v5}, Lcom/google/android/location/collectionlib/ay;->b(JJ)J

    move-result-wide v4

    invoke-virtual {v0, v1, v4, v5}, Lcom/google/p/a/b/b/a;->b(IJ)Lcom/google/p/a/b/b/a;

    .line 1298
    :cond_3
    iget-object v0, p0, Lcom/google/android/location/collectionlib/ay;->c:Lcom/google/p/a/b/b/a;

    const/16 v1, 0xd

    invoke-virtual {v0, v1}, Lcom/google/p/a/b/b/a;->i(I)Z

    move-result v0

    if-nez v0, :cond_4

    .line 1300
    const/4 v0, 0x5

    invoke-virtual {v2, v0, p3, p4}, Lcom/google/p/a/b/b/a;->b(IJ)Lcom/google/p/a/b/b/a;

    .line 1301
    const/4 v0, 0x6

    const-wide/32 v4, 0xf4240

    mul-long/2addr v4, p5

    invoke-virtual {v2, v0, v4, v5}, Lcom/google/p/a/b/b/a;->b(IJ)Lcom/google/p/a/b/b/a;

    .line 1305
    :cond_4
    iget-wide v0, p0, Lcom/google/android/location/collectionlib/ay;->J:J

    const-wide v4, 0x7fffffffffffffffL

    cmp-long v0, v0, v4

    if-nez v0, :cond_9

    .line 1306
    iget-object v0, p0, Lcom/google/android/location/collectionlib/ay;->c:Lcom/google/p/a/b/b/a;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/p/a/b/b/a;->d(I)J

    move-result-wide v4

    .line 1307
    iget-wide v0, p0, Lcom/google/android/location/collectionlib/ay;->T:J

    invoke-direct {p0, p3, p4, v0, v1}, Lcom/google/android/location/collectionlib/ay;->b(JJ)J

    move-result-wide v6

    .line 1309
    invoke-direct {p0, v4, v5, v6, v7}, Lcom/google/android/location/collectionlib/ay;->c(JJ)I

    move-result v1

    .line 1310
    invoke-direct {p0, v4, v5, v6, v7}, Lcom/google/android/location/collectionlib/ay;->d(JJ)I

    move-result v0

    .line 1315
    :goto_1
    if-eqz v1, :cond_5

    .line 1316
    const/4 v3, 0x3

    invoke-virtual {v2, v3, v1}, Lcom/google/p/a/b/b/a;->f(II)Lcom/google/p/a/b/b/a;

    .line 1318
    :cond_5
    if-eqz v0, :cond_6

    .line 1319
    const/4 v1, 0x4

    invoke-virtual {v2, v1, v0}, Lcom/google/p/a/b/b/a;->f(II)Lcom/google/p/a/b/b/a;

    .line 1321
    :cond_6
    iput-wide p3, p0, Lcom/google/android/location/collectionlib/ay;->J:J

    .line 1323
    iget-object v0, p0, Lcom/google/android/location/collectionlib/ay;->c:Lcom/google/p/a/b/b/a;

    const/16 v1, 0xd

    invoke-virtual {v0, v1}, Lcom/google/p/a/b/b/a;->k(I)I

    move-result v0

    if-eqz v0, :cond_7

    iget v0, p0, Lcom/google/android/location/collectionlib/ay;->w:I

    if-eq v0, p2, :cond_8

    .line 1325
    :cond_7
    const/4 v0, 0x2

    invoke-virtual {v2, v0, p2}, Lcom/google/p/a/b/b/a;->f(II)Lcom/google/p/a/b/b/a;

    .line 1326
    iput p2, p0, Lcom/google/android/location/collectionlib/ay;->w:I

    .line 1328
    :cond_8
    iget-object v0, p0, Lcom/google/android/location/collectionlib/ay;->c:Lcom/google/p/a/b/b/a;

    const/16 v1, 0xd

    invoke-virtual {v0, v1, v2}, Lcom/google/p/a/b/b/a;->a(ILcom/google/p/a/b/b/a;)V

    .line 1329
    invoke-direct {p0}, Lcom/google/android/location/collectionlib/ay;->c()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/16 :goto_0

    .line 1281
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 1312
    :cond_9
    :try_start_2
    iget-wide v0, p0, Lcom/google/android/location/collectionlib/ay;->J:J

    invoke-direct {p0, v0, v1, p3, p4}, Lcom/google/android/location/collectionlib/ay;->c(JJ)I

    move-result v1

    .line 1313
    iget-wide v4, p0, Lcom/google/android/location/collectionlib/ay;->J:J

    invoke-direct {p0, v4, v5, p3, p4}, Lcom/google/android/location/collectionlib/ay;->d(JJ)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result v0

    goto :goto_1
.end method

.method public final declared-synchronized c(FFFIJJ)V
    .locals 13

    .prologue
    .line 1008
    monitor-enter p0

    :try_start_0
    iget-boolean v2, p0, Lcom/google/android/location/collectionlib/ay;->j:Z

    if-eqz v2, :cond_1

    .line 1009
    sget-boolean v2, Lcom/google/android/location/i/a;->b:Z

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/location/collectionlib/ay;->W:Lcom/google/android/location/o/a/c;

    const-string v3, "Could not add magnetic field snapshot after the composer is closed."

    invoke-virtual {v2, v3}, Lcom/google/android/location/o/a/c;->a(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1029
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 1012
    :cond_1
    :try_start_1
    move-wide/from16 v0, p7

    invoke-direct {p0, v0, v1}, Lcom/google/android/location/collectionlib/ay;->a(J)V

    .line 1013
    iget-wide v10, p0, Lcom/google/android/location/collectionlib/ay;->F:J

    move-object v2, p0

    move v3, p1

    move v4, p2

    move/from16 v5, p3

    move-wide/from16 v6, p5

    move-wide/from16 v8, p7

    invoke-direct/range {v2 .. v11}, Lcom/google/android/location/collectionlib/ay;->a(FFFJJJ)Lcom/google/p/a/b/b/a;

    move-result-object v2

    .line 1019
    move-wide/from16 v0, p5

    iput-wide v0, p0, Lcom/google/android/location/collectionlib/ay;->F:J

    .line 1022
    iget-object v3, p0, Lcom/google/android/location/collectionlib/ay;->c:Lcom/google/p/a/b/b/a;

    const/4 v4, 0x5

    invoke-virtual {v3, v4}, Lcom/google/p/a/b/b/a;->k(I)I

    move-result v3

    if-eqz v3, :cond_2

    iget v3, p0, Lcom/google/android/location/collectionlib/ay;->s:I

    move/from16 v0, p4

    if-eq v3, v0, :cond_3

    .line 1024
    :cond_2
    const/4 v3, 0x4

    move/from16 v0, p4

    invoke-virtual {v2, v3, v0}, Lcom/google/p/a/b/b/a;->f(II)Lcom/google/p/a/b/b/a;

    .line 1025
    move/from16 v0, p4

    iput v0, p0, Lcom/google/android/location/collectionlib/ay;->s:I

    .line 1027
    :cond_3
    iget-object v3, p0, Lcom/google/android/location/collectionlib/ay;->c:Lcom/google/p/a/b/b/a;

    const/4 v4, 0x5

    invoke-virtual {v3, v4, v2}, Lcom/google/p/a/b/b/a;->a(ILcom/google/p/a/b/b/a;)V

    .line 1028
    invoke-direct {p0}, Lcom/google/android/location/collectionlib/ay;->c()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 1008
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2
.end method

.method public final declared-synchronized c(FIJJ)V
    .locals 9

    .prologue
    .line 1355
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/location/collectionlib/ay;->j:Z

    if-eqz v0, :cond_1

    .line 1356
    sget-boolean v0, Lcom/google/android/location/i/a;->b:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/location/collectionlib/ay;->W:Lcom/google/android/location/o/a/c;

    const-string v1, "Could not add proximity snapshot after the composer is closed."

    invoke-virtual {v0, v1}, Lcom/google/android/location/o/a/c;->a(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1404
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 1359
    :cond_1
    :try_start_1
    invoke-direct {p0, p5, p6}, Lcom/google/android/location/collectionlib/ay;->a(J)V

    .line 1360
    invoke-direct {p0, p3, p4, p5, p6}, Lcom/google/android/location/collectionlib/ay;->a(JJ)V

    .line 1361
    new-instance v2, Lcom/google/p/a/b/b/a;

    sget-object v0, Lcom/google/android/location/m/a;->bd:Lcom/google/p/a/b/b/c;

    const/4 v1, 0x4

    invoke-direct {v2, v0, v1}, Lcom/google/p/a/b/b/a;-><init>(Lcom/google/p/a/b/b/c;I)V

    .line 1362
    const/4 v0, 0x1

    invoke-virtual {v2, v0, p1}, Lcom/google/p/a/b/b/a;->a(IF)Lcom/google/p/a/b/b/a;

    .line 1363
    iget-wide v0, p0, Lcom/google/android/location/collectionlib/ay;->V:J

    const-wide v4, 0x7fffffffffffffffL

    cmp-long v0, v0, v4

    if-nez v0, :cond_2

    .line 1364
    iget-wide v0, p0, Lcom/google/android/location/collectionlib/ay;->o:J

    sub-long/2addr v0, p3

    iget-wide v4, p0, Lcom/google/android/location/collectionlib/ay;->n:J

    sub-long v4, p5, v4

    const-wide/32 v6, 0xf4240

    mul-long/2addr v4, v6

    add-long/2addr v0, v4

    iput-wide v0, p0, Lcom/google/android/location/collectionlib/ay;->V:J

    .line 1367
    :cond_2
    iget-object v0, p0, Lcom/google/android/location/collectionlib/ay;->c:Lcom/google/p/a/b/b/a;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/p/a/b/b/a;->i(I)Z

    move-result v0

    if-nez v0, :cond_3

    .line 1369
    iget-object v0, p0, Lcom/google/android/location/collectionlib/ay;->c:Lcom/google/p/a/b/b/a;

    const/4 v1, 0x1

    iget-wide v4, p0, Lcom/google/android/location/collectionlib/ay;->V:J

    invoke-direct {p0, p3, p4, v4, v5}, Lcom/google/android/location/collectionlib/ay;->b(JJ)J

    move-result-wide v4

    invoke-virtual {v0, v1, v4, v5}, Lcom/google/p/a/b/b/a;->b(IJ)Lcom/google/p/a/b/b/a;

    .line 1372
    :cond_3
    iget-object v0, p0, Lcom/google/android/location/collectionlib/ay;->c:Lcom/google/p/a/b/b/a;

    const/16 v1, 0xe

    invoke-virtual {v0, v1}, Lcom/google/p/a/b/b/a;->i(I)Z

    move-result v0

    if-nez v0, :cond_4

    .line 1374
    const/4 v0, 0x5

    invoke-virtual {v2, v0, p3, p4}, Lcom/google/p/a/b/b/a;->b(IJ)Lcom/google/p/a/b/b/a;

    .line 1375
    const/4 v0, 0x6

    const-wide/32 v4, 0xf4240

    mul-long/2addr v4, p5

    invoke-virtual {v2, v0, v4, v5}, Lcom/google/p/a/b/b/a;->b(IJ)Lcom/google/p/a/b/b/a;

    .line 1379
    :cond_4
    iget-wide v0, p0, Lcom/google/android/location/collectionlib/ay;->L:J

    const-wide v4, 0x7fffffffffffffffL

    cmp-long v0, v0, v4

    if-nez v0, :cond_9

    .line 1380
    iget-object v0, p0, Lcom/google/android/location/collectionlib/ay;->c:Lcom/google/p/a/b/b/a;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/p/a/b/b/a;->d(I)J

    move-result-wide v4

    .line 1381
    iget-wide v0, p0, Lcom/google/android/location/collectionlib/ay;->V:J

    invoke-direct {p0, p3, p4, v0, v1}, Lcom/google/android/location/collectionlib/ay;->b(JJ)J

    move-result-wide v6

    .line 1383
    invoke-direct {p0, v4, v5, v6, v7}, Lcom/google/android/location/collectionlib/ay;->c(JJ)I

    move-result v1

    .line 1384
    invoke-direct {p0, v4, v5, v6, v7}, Lcom/google/android/location/collectionlib/ay;->d(JJ)I

    move-result v0

    .line 1389
    :goto_1
    if-eqz v1, :cond_5

    .line 1390
    const/4 v3, 0x3

    invoke-virtual {v2, v3, v1}, Lcom/google/p/a/b/b/a;->f(II)Lcom/google/p/a/b/b/a;

    .line 1392
    :cond_5
    if-eqz v0, :cond_6

    .line 1393
    const/4 v1, 0x4

    invoke-virtual {v2, v1, v0}, Lcom/google/p/a/b/b/a;->f(II)Lcom/google/p/a/b/b/a;

    .line 1395
    :cond_6
    iput-wide p3, p0, Lcom/google/android/location/collectionlib/ay;->L:J

    .line 1397
    iget-object v0, p0, Lcom/google/android/location/collectionlib/ay;->c:Lcom/google/p/a/b/b/a;

    const/16 v1, 0xe

    invoke-virtual {v0, v1}, Lcom/google/p/a/b/b/a;->k(I)I

    move-result v0

    if-eqz v0, :cond_7

    iget v0, p0, Lcom/google/android/location/collectionlib/ay;->x:I

    if-eq v0, p2, :cond_8

    .line 1399
    :cond_7
    const/4 v0, 0x2

    invoke-virtual {v2, v0, p2}, Lcom/google/p/a/b/b/a;->f(II)Lcom/google/p/a/b/b/a;

    .line 1400
    iput p2, p0, Lcom/google/android/location/collectionlib/ay;->x:I

    .line 1402
    :cond_8
    iget-object v0, p0, Lcom/google/android/location/collectionlib/ay;->c:Lcom/google/p/a/b/b/a;

    const/16 v1, 0xe

    invoke-virtual {v0, v1, v2}, Lcom/google/p/a/b/b/a;->a(ILcom/google/p/a/b/b/a;)V

    .line 1403
    invoke-direct {p0}, Lcom/google/android/location/collectionlib/ay;->c()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/16 :goto_0

    .line 1355
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 1386
    :cond_9
    :try_start_2
    iget-wide v0, p0, Lcom/google/android/location/collectionlib/ay;->L:J

    invoke-direct {p0, v0, v1, p3, p4}, Lcom/google/android/location/collectionlib/ay;->c(JJ)I

    move-result v1

    .line 1387
    iget-wide v4, p0, Lcom/google/android/location/collectionlib/ay;->L:J

    invoke-direct {p0, v4, v5, p3, p4}, Lcom/google/android/location/collectionlib/ay;->d(JJ)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result v0

    goto :goto_1
.end method

.method public final declared-synchronized d(FFFIJJ)V
    .locals 13

    .prologue
    .line 1150
    monitor-enter p0

    :try_start_0
    iget-boolean v4, p0, Lcom/google/android/location/collectionlib/ay;->j:Z

    if-eqz v4, :cond_1

    .line 1151
    sget-boolean v4, Lcom/google/android/location/i/a;->b:Z

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/google/android/location/collectionlib/ay;->W:Lcom/google/android/location/o/a/c;

    const-string v5, "Could not add gyroscope snapshot after the composer is closed."

    invoke-virtual {v4, v5}, Lcom/google/android/location/o/a/c;->a(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1202
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 1154
    :cond_1
    :try_start_1
    move-wide/from16 v0, p7

    invoke-direct {p0, v0, v1}, Lcom/google/android/location/collectionlib/ay;->a(J)V

    .line 1155
    move-wide/from16 v0, p5

    move-wide/from16 v2, p7

    invoke-direct {p0, v0, v1, v2, v3}, Lcom/google/android/location/collectionlib/ay;->a(JJ)V

    .line 1156
    new-instance v6, Lcom/google/p/a/b/b/a;

    sget-object v4, Lcom/google/android/location/m/a;->aW:Lcom/google/p/a/b/b/c;

    const/16 v5, 0x8

    invoke-direct {v6, v4, v5}, Lcom/google/p/a/b/b/a;-><init>(Lcom/google/p/a/b/b/c;I)V

    .line 1157
    const/4 v4, 0x1

    invoke-virtual {v6, v4, p1}, Lcom/google/p/a/b/b/a;->a(IF)Lcom/google/p/a/b/b/a;

    .line 1158
    const/4 v4, 0x2

    invoke-virtual {v6, v4, p2}, Lcom/google/p/a/b/b/a;->a(IF)Lcom/google/p/a/b/b/a;

    .line 1159
    const/4 v4, 0x3

    move/from16 v0, p3

    invoke-virtual {v6, v4, v0}, Lcom/google/p/a/b/b/a;->a(IF)Lcom/google/p/a/b/b/a;

    .line 1160
    iget-wide v4, p0, Lcom/google/android/location/collectionlib/ay;->Q:J

    const-wide v8, 0x7fffffffffffffffL

    cmp-long v4, v4, v8

    if-nez v4, :cond_2

    .line 1161
    iget-wide v4, p0, Lcom/google/android/location/collectionlib/ay;->o:J

    sub-long v4, v4, p5

    iget-wide v8, p0, Lcom/google/android/location/collectionlib/ay;->n:J

    sub-long v8, p7, v8

    const-wide/32 v10, 0xf4240

    mul-long/2addr v8, v10

    add-long/2addr v4, v8

    iput-wide v4, p0, Lcom/google/android/location/collectionlib/ay;->Q:J

    .line 1164
    :cond_2
    iget-object v4, p0, Lcom/google/android/location/collectionlib/ay;->c:Lcom/google/p/a/b/b/a;

    const/4 v5, 0x1

    invoke-virtual {v4, v5}, Lcom/google/p/a/b/b/a;->i(I)Z

    move-result v4

    if-nez v4, :cond_3

    .line 1166
    iget-object v4, p0, Lcom/google/android/location/collectionlib/ay;->c:Lcom/google/p/a/b/b/a;

    const/4 v5, 0x1

    iget-wide v8, p0, Lcom/google/android/location/collectionlib/ay;->Q:J

    move-wide/from16 v0, p5

    invoke-direct {p0, v0, v1, v8, v9}, Lcom/google/android/location/collectionlib/ay;->b(JJ)J

    move-result-wide v8

    invoke-virtual {v4, v5, v8, v9}, Lcom/google/p/a/b/b/a;->b(IJ)Lcom/google/p/a/b/b/a;

    .line 1169
    :cond_3
    iget-object v4, p0, Lcom/google/android/location/collectionlib/ay;->c:Lcom/google/p/a/b/b/a;

    const/4 v5, 0x4

    invoke-virtual {v4, v5}, Lcom/google/p/a/b/b/a;->i(I)Z

    move-result v4

    if-nez v4, :cond_4

    .line 1171
    const/16 v4, 0x9

    move-wide/from16 v0, p5

    invoke-virtual {v6, v4, v0, v1}, Lcom/google/p/a/b/b/a;->b(IJ)Lcom/google/p/a/b/b/a;

    .line 1172
    const/16 v4, 0xa

    const-wide/32 v8, 0xf4240

    mul-long v8, v8, p7

    invoke-virtual {v6, v4, v8, v9}, Lcom/google/p/a/b/b/a;->b(IJ)Lcom/google/p/a/b/b/a;

    .line 1176
    :cond_4
    iget-wide v4, p0, Lcom/google/android/location/collectionlib/ay;->G:J

    const-wide v8, 0x7fffffffffffffffL

    cmp-long v4, v4, v8

    if-nez v4, :cond_9

    .line 1177
    iget-object v4, p0, Lcom/google/android/location/collectionlib/ay;->c:Lcom/google/p/a/b/b/a;

    const/4 v5, 0x1

    invoke-virtual {v4, v5}, Lcom/google/p/a/b/b/a;->d(I)J

    move-result-wide v8

    .line 1178
    iget-wide v4, p0, Lcom/google/android/location/collectionlib/ay;->Q:J

    move-wide/from16 v0, p5

    invoke-direct {p0, v0, v1, v4, v5}, Lcom/google/android/location/collectionlib/ay;->b(JJ)J

    move-result-wide v10

    .line 1180
    invoke-direct {p0, v8, v9, v10, v11}, Lcom/google/android/location/collectionlib/ay;->c(JJ)I

    move-result v5

    .line 1181
    invoke-direct {p0, v8, v9, v10, v11}, Lcom/google/android/location/collectionlib/ay;->d(JJ)I

    move-result v4

    .line 1186
    :goto_1
    if-eqz v5, :cond_5

    .line 1187
    const/4 v7, 0x7

    invoke-virtual {v6, v7, v5}, Lcom/google/p/a/b/b/a;->f(II)Lcom/google/p/a/b/b/a;

    .line 1189
    :cond_5
    if-eqz v4, :cond_6

    .line 1190
    const/16 v5, 0x8

    invoke-virtual {v6, v5, v4}, Lcom/google/p/a/b/b/a;->f(II)Lcom/google/p/a/b/b/a;

    .line 1192
    :cond_6
    move-wide/from16 v0, p5

    iput-wide v0, p0, Lcom/google/android/location/collectionlib/ay;->G:J

    .line 1195
    iget-object v4, p0, Lcom/google/android/location/collectionlib/ay;->c:Lcom/google/p/a/b/b/a;

    const/4 v5, 0x4

    invoke-virtual {v4, v5}, Lcom/google/p/a/b/b/a;->k(I)I

    move-result v4

    if-eqz v4, :cond_7

    iget v4, p0, Lcom/google/android/location/collectionlib/ay;->t:I

    move/from16 v0, p4

    if-eq v4, v0, :cond_8

    .line 1197
    :cond_7
    const/4 v4, 0x4

    move/from16 v0, p4

    invoke-virtual {v6, v4, v0}, Lcom/google/p/a/b/b/a;->f(II)Lcom/google/p/a/b/b/a;

    .line 1198
    move/from16 v0, p4

    iput v0, p0, Lcom/google/android/location/collectionlib/ay;->t:I

    .line 1200
    :cond_8
    iget-object v4, p0, Lcom/google/android/location/collectionlib/ay;->c:Lcom/google/p/a/b/b/a;

    const/4 v5, 0x4

    invoke-virtual {v4, v5, v6}, Lcom/google/p/a/b/b/a;->a(ILcom/google/p/a/b/b/a;)V

    .line 1201
    invoke-direct {p0}, Lcom/google/android/location/collectionlib/ay;->c()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/16 :goto_0

    .line 1150
    :catchall_0
    move-exception v4

    monitor-exit p0

    throw v4

    .line 1183
    :cond_9
    :try_start_2
    iget-wide v4, p0, Lcom/google/android/location/collectionlib/ay;->G:J

    move-wide/from16 v0, p5

    invoke-direct {p0, v4, v5, v0, v1}, Lcom/google/android/location/collectionlib/ay;->c(JJ)I

    move-result v5

    .line 1184
    iget-wide v8, p0, Lcom/google/android/location/collectionlib/ay;->G:J

    move-wide/from16 v0, p5

    invoke-direct {p0, v8, v9, v0, v1}, Lcom/google/android/location/collectionlib/ay;->d(JJ)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result v4

    goto :goto_1
.end method

.method public final declared-synchronized d(FIJJ)V
    .locals 9

    .prologue
    .line 1417
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/location/collectionlib/ay;->j:Z

    if-eqz v0, :cond_1

    .line 1418
    sget-boolean v0, Lcom/google/android/location/i/a;->b:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/location/collectionlib/ay;->W:Lcom/google/android/location/o/a/c;

    const-string v1, "Could not add heart rate snapshot after the composer is closed."

    invoke-virtual {v0, v1}, Lcom/google/android/location/o/a/c;->a(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1466
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 1421
    :cond_1
    :try_start_1
    invoke-direct {p0, p5, p6}, Lcom/google/android/location/collectionlib/ay;->a(J)V

    .line 1422
    invoke-direct {p0, p3, p4, p5, p6}, Lcom/google/android/location/collectionlib/ay;->a(JJ)V

    .line 1423
    new-instance v2, Lcom/google/p/a/b/b/a;

    sget-object v0, Lcom/google/android/location/m/a;->be:Lcom/google/p/a/b/b/c;

    const/4 v1, 0x4

    invoke-direct {v2, v0, v1}, Lcom/google/p/a/b/b/a;-><init>(Lcom/google/p/a/b/b/c;I)V

    .line 1424
    const/4 v0, 0x1

    invoke-virtual {v2, v0, p1}, Lcom/google/p/a/b/b/a;->a(IF)Lcom/google/p/a/b/b/a;

    .line 1425
    iget-wide v0, p0, Lcom/google/android/location/collectionlib/ay;->R:J

    const-wide v4, 0x7fffffffffffffffL

    cmp-long v0, v0, v4

    if-nez v0, :cond_2

    .line 1426
    iget-wide v0, p0, Lcom/google/android/location/collectionlib/ay;->o:J

    sub-long/2addr v0, p3

    iget-wide v4, p0, Lcom/google/android/location/collectionlib/ay;->n:J

    sub-long v4, p5, v4

    const-wide/32 v6, 0xf4240

    mul-long/2addr v4, v6

    add-long/2addr v0, v4

    iput-wide v0, p0, Lcom/google/android/location/collectionlib/ay;->R:J

    .line 1429
    :cond_2
    iget-object v0, p0, Lcom/google/android/location/collectionlib/ay;->c:Lcom/google/p/a/b/b/a;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/p/a/b/b/a;->i(I)Z

    move-result v0

    if-nez v0, :cond_3

    .line 1431
    iget-object v0, p0, Lcom/google/android/location/collectionlib/ay;->c:Lcom/google/p/a/b/b/a;

    const/4 v1, 0x1

    iget-wide v4, p0, Lcom/google/android/location/collectionlib/ay;->R:J

    invoke-direct {p0, p3, p4, v4, v5}, Lcom/google/android/location/collectionlib/ay;->b(JJ)J

    move-result-wide v4

    invoke-virtual {v0, v1, v4, v5}, Lcom/google/p/a/b/b/a;->b(IJ)Lcom/google/p/a/b/b/a;

    .line 1434
    :cond_3
    iget-object v0, p0, Lcom/google/android/location/collectionlib/ay;->c:Lcom/google/p/a/b/b/a;

    const/16 v1, 0x10

    invoke-virtual {v0, v1}, Lcom/google/p/a/b/b/a;->i(I)Z

    move-result v0

    if-nez v0, :cond_4

    .line 1436
    const/4 v0, 0x5

    invoke-virtual {v2, v0, p3, p4}, Lcom/google/p/a/b/b/a;->b(IJ)Lcom/google/p/a/b/b/a;

    .line 1437
    const/4 v0, 0x6

    const-wide/32 v4, 0xf4240

    mul-long/2addr v4, p5

    invoke-virtual {v2, v0, v4, v5}, Lcom/google/p/a/b/b/a;->b(IJ)Lcom/google/p/a/b/b/a;

    .line 1441
    :cond_4
    iget-wide v0, p0, Lcom/google/android/location/collectionlib/ay;->H:J

    const-wide v4, 0x7fffffffffffffffL

    cmp-long v0, v0, v4

    if-nez v0, :cond_9

    .line 1442
    iget-object v0, p0, Lcom/google/android/location/collectionlib/ay;->c:Lcom/google/p/a/b/b/a;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/p/a/b/b/a;->d(I)J

    move-result-wide v4

    .line 1443
    iget-wide v0, p0, Lcom/google/android/location/collectionlib/ay;->R:J

    invoke-direct {p0, p3, p4, v0, v1}, Lcom/google/android/location/collectionlib/ay;->b(JJ)J

    move-result-wide v6

    .line 1445
    invoke-direct {p0, v4, v5, v6, v7}, Lcom/google/android/location/collectionlib/ay;->c(JJ)I

    move-result v1

    .line 1446
    invoke-direct {p0, v4, v5, v6, v7}, Lcom/google/android/location/collectionlib/ay;->d(JJ)I

    move-result v0

    .line 1451
    :goto_1
    if-eqz v1, :cond_5

    .line 1452
    const/4 v3, 0x3

    invoke-virtual {v2, v3, v1}, Lcom/google/p/a/b/b/a;->f(II)Lcom/google/p/a/b/b/a;

    .line 1454
    :cond_5
    if-eqz v0, :cond_6

    .line 1455
    const/4 v1, 0x4

    invoke-virtual {v2, v1, v0}, Lcom/google/p/a/b/b/a;->f(II)Lcom/google/p/a/b/b/a;

    .line 1457
    :cond_6
    iput-wide p3, p0, Lcom/google/android/location/collectionlib/ay;->H:J

    .line 1459
    iget-object v0, p0, Lcom/google/android/location/collectionlib/ay;->c:Lcom/google/p/a/b/b/a;

    const/16 v1, 0x10

    invoke-virtual {v0, v1}, Lcom/google/p/a/b/b/a;->k(I)I

    move-result v0

    if-eqz v0, :cond_7

    iget v0, p0, Lcom/google/android/location/collectionlib/ay;->u:I

    if-eq v0, p2, :cond_8

    .line 1461
    :cond_7
    const/4 v0, 0x2

    invoke-virtual {v2, v0, p2}, Lcom/google/p/a/b/b/a;->f(II)Lcom/google/p/a/b/b/a;

    .line 1462
    iput p2, p0, Lcom/google/android/location/collectionlib/ay;->u:I

    .line 1464
    :cond_8
    iget-object v0, p0, Lcom/google/android/location/collectionlib/ay;->c:Lcom/google/p/a/b/b/a;

    const/16 v1, 0x10

    invoke-virtual {v0, v1, v2}, Lcom/google/p/a/b/b/a;->a(ILcom/google/p/a/b/b/a;)V

    .line 1465
    invoke-direct {p0}, Lcom/google/android/location/collectionlib/ay;->c()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/16 :goto_0

    .line 1417
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 1448
    :cond_9
    :try_start_2
    iget-wide v0, p0, Lcom/google/android/location/collectionlib/ay;->H:J

    invoke-direct {p0, v0, v1, p3, p4}, Lcom/google/android/location/collectionlib/ay;->c(JJ)I

    move-result v1

    .line 1449
    iget-wide v4, p0, Lcom/google/android/location/collectionlib/ay;->H:J

    invoke-direct {p0, v4, v5, p3, p4}, Lcom/google/android/location/collectionlib/ay;->d(JJ)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result v0

    goto :goto_1
.end method

.method public final declared-synchronized e(FIJJ)V
    .locals 9

    .prologue
    .line 1479
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/location/collectionlib/ay;->j:Z

    if-eqz v0, :cond_1

    .line 1480
    sget-boolean v0, Lcom/google/android/location/i/a;->b:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/location/collectionlib/ay;->W:Lcom/google/android/location/o/a/c;

    const-string v1, "Could not add step counter snapshot after the composer is closed."

    invoke-virtual {v0, v1}, Lcom/google/android/location/o/a/c;->a(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1528
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 1483
    :cond_1
    :try_start_1
    invoke-direct {p0, p5, p6}, Lcom/google/android/location/collectionlib/ay;->a(J)V

    .line 1484
    invoke-direct {p0, p3, p4, p5, p6}, Lcom/google/android/location/collectionlib/ay;->a(JJ)V

    .line 1485
    new-instance v2, Lcom/google/p/a/b/b/a;

    sget-object v0, Lcom/google/android/location/m/a;->bf:Lcom/google/p/a/b/b/c;

    const/4 v1, 0x4

    invoke-direct {v2, v0, v1}, Lcom/google/p/a/b/b/a;-><init>(Lcom/google/p/a/b/b/c;I)V

    .line 1486
    const/4 v0, 0x1

    invoke-virtual {v2, v0, p1}, Lcom/google/p/a/b/b/a;->a(IF)Lcom/google/p/a/b/b/a;

    .line 1487
    iget-wide v0, p0, Lcom/google/android/location/collectionlib/ay;->U:J

    const-wide v4, 0x7fffffffffffffffL

    cmp-long v0, v0, v4

    if-nez v0, :cond_2

    .line 1488
    iget-wide v0, p0, Lcom/google/android/location/collectionlib/ay;->o:J

    sub-long/2addr v0, p3

    iget-wide v4, p0, Lcom/google/android/location/collectionlib/ay;->n:J

    sub-long v4, p5, v4

    const-wide/32 v6, 0xf4240

    mul-long/2addr v4, v6

    add-long/2addr v0, v4

    iput-wide v0, p0, Lcom/google/android/location/collectionlib/ay;->U:J

    .line 1491
    :cond_2
    iget-object v0, p0, Lcom/google/android/location/collectionlib/ay;->c:Lcom/google/p/a/b/b/a;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/p/a/b/b/a;->i(I)Z

    move-result v0

    if-nez v0, :cond_3

    .line 1493
    iget-object v0, p0, Lcom/google/android/location/collectionlib/ay;->c:Lcom/google/p/a/b/b/a;

    const/4 v1, 0x1

    iget-wide v4, p0, Lcom/google/android/location/collectionlib/ay;->U:J

    invoke-direct {p0, p3, p4, v4, v5}, Lcom/google/android/location/collectionlib/ay;->b(JJ)J

    move-result-wide v4

    invoke-virtual {v0, v1, v4, v5}, Lcom/google/p/a/b/b/a;->b(IJ)Lcom/google/p/a/b/b/a;

    .line 1496
    :cond_3
    iget-object v0, p0, Lcom/google/android/location/collectionlib/ay;->c:Lcom/google/p/a/b/b/a;

    const/16 v1, 0x11

    invoke-virtual {v0, v1}, Lcom/google/p/a/b/b/a;->i(I)Z

    move-result v0

    if-nez v0, :cond_4

    .line 1498
    const/4 v0, 0x5

    invoke-virtual {v2, v0, p3, p4}, Lcom/google/p/a/b/b/a;->b(IJ)Lcom/google/p/a/b/b/a;

    .line 1499
    const/4 v0, 0x6

    const-wide/32 v4, 0xf4240

    mul-long/2addr v4, p5

    invoke-virtual {v2, v0, v4, v5}, Lcom/google/p/a/b/b/a;->b(IJ)Lcom/google/p/a/b/b/a;

    .line 1503
    :cond_4
    iget-wide v0, p0, Lcom/google/android/location/collectionlib/ay;->K:J

    const-wide v4, 0x7fffffffffffffffL

    cmp-long v0, v0, v4

    if-nez v0, :cond_9

    .line 1504
    iget-object v0, p0, Lcom/google/android/location/collectionlib/ay;->c:Lcom/google/p/a/b/b/a;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/p/a/b/b/a;->d(I)J

    move-result-wide v4

    .line 1505
    iget-wide v0, p0, Lcom/google/android/location/collectionlib/ay;->U:J

    invoke-direct {p0, p3, p4, v0, v1}, Lcom/google/android/location/collectionlib/ay;->b(JJ)J

    move-result-wide v6

    .line 1507
    invoke-direct {p0, v4, v5, v6, v7}, Lcom/google/android/location/collectionlib/ay;->c(JJ)I

    move-result v1

    .line 1508
    invoke-direct {p0, v4, v5, v6, v7}, Lcom/google/android/location/collectionlib/ay;->d(JJ)I

    move-result v0

    .line 1513
    :goto_1
    if-eqz v1, :cond_5

    .line 1514
    const/4 v3, 0x3

    invoke-virtual {v2, v3, v1}, Lcom/google/p/a/b/b/a;->f(II)Lcom/google/p/a/b/b/a;

    .line 1516
    :cond_5
    if-eqz v0, :cond_6

    .line 1517
    const/4 v1, 0x4

    invoke-virtual {v2, v1, v0}, Lcom/google/p/a/b/b/a;->f(II)Lcom/google/p/a/b/b/a;

    .line 1519
    :cond_6
    iput-wide p3, p0, Lcom/google/android/location/collectionlib/ay;->K:J

    .line 1521
    iget-object v0, p0, Lcom/google/android/location/collectionlib/ay;->c:Lcom/google/p/a/b/b/a;

    const/16 v1, 0x11

    invoke-virtual {v0, v1}, Lcom/google/p/a/b/b/a;->k(I)I

    move-result v0

    if-eqz v0, :cond_7

    iget v0, p0, Lcom/google/android/location/collectionlib/ay;->y:I

    if-eq v0, p2, :cond_8

    .line 1523
    :cond_7
    const/4 v0, 0x2

    invoke-virtual {v2, v0, p2}, Lcom/google/p/a/b/b/a;->f(II)Lcom/google/p/a/b/b/a;

    .line 1524
    iput p2, p0, Lcom/google/android/location/collectionlib/ay;->y:I

    .line 1526
    :cond_8
    iget-object v0, p0, Lcom/google/android/location/collectionlib/ay;->c:Lcom/google/p/a/b/b/a;

    const/16 v1, 0x11

    invoke-virtual {v0, v1, v2}, Lcom/google/p/a/b/b/a;->a(ILcom/google/p/a/b/b/a;)V

    .line 1527
    invoke-direct {p0}, Lcom/google/android/location/collectionlib/ay;->c()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/16 :goto_0

    .line 1479
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 1510
    :cond_9
    :try_start_2
    iget-wide v0, p0, Lcom/google/android/location/collectionlib/ay;->K:J

    invoke-direct {p0, v0, v1, p3, p4}, Lcom/google/android/location/collectionlib/ay;->c(JJ)I

    move-result v1

    .line 1511
    iget-wide v4, p0, Lcom/google/android/location/collectionlib/ay;->K:J

    invoke-direct {p0, v4, v5, p3, p4}, Lcom/google/android/location/collectionlib/ay;->d(JJ)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result v0

    goto :goto_1
.end method
