.class public final Lcom/google/android/location/copresence/o/n;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:I

.field public final c:J

.field public final d:Ljava/lang/Long;

.field final synthetic e:Lcom/google/android/location/copresence/o/m;


# direct methods
.method private constructor <init>(Lcom/google/android/location/copresence/o/m;Ljava/lang/String;JILjava/lang/Long;)V
    .locals 1

    .prologue
    .line 496
    iput-object p1, p0, Lcom/google/android/location/copresence/o/n;->e:Lcom/google/android/location/copresence/o/m;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 497
    invoke-static {p2}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/location/copresence/o/n;->a:Ljava/lang/String;

    .line 498
    iput-wide p3, p0, Lcom/google/android/location/copresence/o/n;->c:J

    .line 499
    iput p5, p0, Lcom/google/android/location/copresence/o/n;->b:I

    .line 500
    iput-object p6, p0, Lcom/google/android/location/copresence/o/n;->d:Ljava/lang/Long;

    .line 501
    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/location/copresence/o/m;Ljava/lang/String;JILjava/lang/Long;B)V
    .locals 1

    .prologue
    .line 489
    invoke-direct/range {p0 .. p6}, Lcom/google/android/location/copresence/o/n;-><init>(Lcom/google/android/location/copresence/o/m;Ljava/lang/String;JILjava/lang/Long;)V

    return-void
.end method


# virtual methods
.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 510
    if-ne p0, p1, :cond_1

    .line 511
    const/4 v0, 0x1

    .line 520
    :cond_0
    :goto_0
    return v0

    .line 513
    :cond_1
    if-eqz p1, :cond_0

    .line 516
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    if-ne v1, v2, :cond_0

    .line 519
    check-cast p1, Lcom/google/android/location/copresence/o/n;

    .line 520
    iget-object v0, p0, Lcom/google/android/location/copresence/o/n;->a:Ljava/lang/String;

    iget-object v1, p1, Lcom/google/android/location/copresence/o/n;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 505
    iget-object v0, p0, Lcom/google/android/location/copresence/o/n;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    return v0
.end method
