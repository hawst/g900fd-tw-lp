.class public final Lcom/google/android/location/places/bx;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public a:Z

.field private final b:Lcom/google/android/location/b/at;

.field private final c:I

.field private final d:Lcom/google/android/location/b/ax;


# direct methods
.method public constructor <init>(Landroid/content/Context;I)V
    .locals 4

    .prologue
    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    new-instance v0, Lcom/google/android/location/places/by;

    invoke-direct {v0, p0}, Lcom/google/android/location/places/by;-><init>(Lcom/google/android/location/places/bx;)V

    iput-object v0, p0, Lcom/google/android/location/places/bx;->d:Lcom/google/android/location/b/ax;

    .line 32
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/location/places/bx;->a:Z

    .line 35
    iput p2, p0, Lcom/google/android/location/places/bx;->c:I

    .line 36
    new-instance v0, Lcom/google/android/location/b/at;

    const-class v1, Lcom/google/android/location/internal/GoogleLocationManagerService;

    iget-object v2, p0, Lcom/google/android/location/places/bx;->d:Lcom/google/android/location/b/ax;

    iget v3, p0, Lcom/google/android/location/places/bx;->c:I

    invoke-direct {v0, p1, v1, v2, v3}, Lcom/google/android/location/b/at;-><init>(Landroid/content/Context;Ljava/lang/Class;Lcom/google/android/location/b/ax;I)V

    iput-object v0, p0, Lcom/google/android/location/places/bx;->b:Lcom/google/android/location/b/at;

    .line 41
    return-void
.end method


# virtual methods
.method public final a()Ljava/util/List;
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lcom/google/android/location/places/bx;->b:Lcom/google/android/location/b/at;

    invoke-virtual {v0}, Lcom/google/android/location/b/at;->c()Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;)Ljava/util/List;
    .locals 4

    .prologue
    .line 110
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 111
    iget-object v0, p0, Lcom/google/android/location/places/bx;->b:Lcom/google/android/location/b/at;

    invoke-virtual {v0}, Lcom/google/android/location/b/at;->c()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/places/Subscription;

    .line 112
    invoke-virtual {v0}, Lcom/google/android/location/places/Subscription;->a()Lcom/google/android/gms/location/places/internal/PlacesParams;

    move-result-object v3

    iget-object v3, v3, Lcom/google/android/gms/location/places/internal/PlacesParams;->c:Ljava/lang/String;

    invoke-virtual {v3, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 113
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 116
    :cond_1
    return-object v1
.end method

.method public final a(Landroid/content/Intent;)V
    .locals 1

    .prologue
    .line 85
    iget-object v0, p0, Lcom/google/android/location/places/bx;->b:Lcom/google/android/location/b/at;

    invoke-virtual {v0, p1}, Lcom/google/android/location/b/at;->c(Landroid/content/Intent;)V

    .line 86
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/location/places/bx;->a:Z

    .line 87
    return-void
.end method

.method public final a(Lcom/google/android/location/places/Subscription;)V
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, Lcom/google/android/location/places/bx;->b:Lcom/google/android/location/b/at;

    invoke-virtual {v0, p1}, Lcom/google/android/location/b/at;->a(Landroid/os/Parcelable;)V

    .line 57
    return-void
.end method

.method public final b(Lcom/google/android/location/places/Subscription;)Lcom/google/android/location/places/Subscription;
    .locals 4

    .prologue
    .line 66
    const/4 v1, 0x0

    .line 67
    invoke-virtual {p0}, Lcom/google/android/location/places/bx;->a()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/places/Subscription;

    .line 68
    iget-object v3, p0, Lcom/google/android/location/places/bx;->d:Lcom/google/android/location/b/ax;

    invoke-interface {v3, v0, p1}, Lcom/google/android/location/b/ax;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 73
    :goto_0
    iget-object v1, p0, Lcom/google/android/location/places/bx;->b:Lcom/google/android/location/b/at;

    invoke-virtual {v1, p1}, Lcom/google/android/location/b/at;->b(Landroid/os/Parcelable;)V

    .line 74
    return-object v0

    :cond_1
    move-object v0, v1

    goto :goto_0
.end method

.method public final b(Landroid/content/Intent;)Z
    .locals 2

    .prologue
    .line 94
    invoke-static {p1}, Lcom/google/android/location/b/at;->a(Landroid/content/Intent;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p1}, Lcom/google/android/location/b/at;->b(Landroid/content/Intent;)I

    move-result v0

    iget v1, p0, Lcom/google/android/location/places/bx;->c:I

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
