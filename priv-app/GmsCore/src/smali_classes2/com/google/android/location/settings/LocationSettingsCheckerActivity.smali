.class public Lcom/google/android/location/settings/LocationSettingsCheckerActivity;
.super Landroid/app/Activity;
.source "SourceFile"

# interfaces
.implements Landroid/content/DialogInterface$OnCancelListener;
.implements Landroid/content/DialogInterface$OnClickListener;
.implements Lcom/google/android/location/settings/p;


# instance fields
.field private a:Ljava/lang/String;

.field private b:Lcom/google/android/location/n/w;

.field private c:Lcom/google/android/location/settings/n;

.field private d:Z

.field private e:Ljava/lang/String;

.field private f:J


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 45
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 56
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/location/settings/LocationSettingsCheckerActivity;->d:Z

    .line 211
    return-void
.end method

.method private static a(Landroid/text/Spannable;)Landroid/text/Spannable;
    .locals 8

    .prologue
    const/4 v2, 0x0

    .line 223
    invoke-interface {p0}, Landroid/text/Spannable;->length()I

    move-result v0

    const-class v1, Landroid/text/style/URLSpan;

    invoke-interface {p0, v2, v0, v1}, Landroid/text/Spannable;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Landroid/text/style/URLSpan;

    .line 224
    array-length v3, v0

    move v1, v2

    :goto_0
    if-ge v1, v3, :cond_0

    aget-object v4, v0, v1

    .line 225
    invoke-interface {p0, v4}, Landroid/text/Spannable;->getSpanStart(Ljava/lang/Object;)I

    move-result v5

    .line 226
    invoke-interface {p0, v4}, Landroid/text/Spannable;->getSpanEnd(Ljava/lang/Object;)I

    move-result v6

    .line 227
    invoke-interface {p0, v4}, Landroid/text/Spannable;->removeSpan(Ljava/lang/Object;)V

    .line 228
    new-instance v7, Lcom/google/android/location/settings/LocationSettingsCheckerActivity$URLSpanNoUnderline;

    invoke-virtual {v4}, Landroid/text/style/URLSpan;->getURL()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v7, v4}, Lcom/google/android/location/settings/LocationSettingsCheckerActivity$URLSpanNoUnderline;-><init>(Ljava/lang/String;)V

    .line 229
    invoke-interface {p0, v7, v5, v6, v2}, Landroid/text/Spannable;->setSpan(Ljava/lang/Object;III)V

    .line 224
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 231
    :cond_0
    return-object p0
.end method

.method private static a(Ljava/util/List;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 110
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 111
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 112
    packed-switch v0, :pswitch_data_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    :pswitch_0
    const-string v0, "gps"

    :goto_1
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 113
    const/16 v0, 0x2c

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 112
    :pswitch_1
    const-string v0, "nlp"

    goto :goto_1

    :pswitch_2
    const-string v0, "gpsnlp"

    goto :goto_1

    :pswitch_3
    const-string v0, "wifi"

    goto :goto_1

    :pswitch_4
    const-string v0, "wifiscanning"

    goto :goto_1

    :pswitch_5
    const-string v0, "glsconsent"

    goto :goto_1

    :pswitch_6
    const-string v0, "lgaayl"

    goto :goto_1

    .line 116
    :cond_0
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 117
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 112
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method

.method private a(ZLjava/util/List;)V
    .locals 8

    .prologue
    .line 235
    new-instance v3, Landroid/app/AlertDialog$Builder;

    invoke-direct {v3, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 236
    if-eqz p1, :cond_0

    sget v0, Lcom/google/android/gms/p;->qi:I

    :goto_0
    invoke-virtual {v3, v0}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 239
    invoke-virtual {p0}, Lcom/google/android/location/settings/LocationSettingsCheckerActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 240
    invoke-virtual {v3, p0}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    .line 242
    invoke-virtual {p0}, Lcom/google/android/location/settings/LocationSettingsCheckerActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    sget v1, Lcom/google/android/gms/l;->cE:I

    const/4 v4, 0x0

    invoke-virtual {v0, v1, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v4

    .line 244
    sget v0, Lcom/google/android/gms/j;->mb:I

    invoke-virtual {v4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 246
    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 247
    if-eqz p1, :cond_1

    sget v1, Lcom/google/android/gms/p;->qa:I

    :goto_1
    invoke-virtual {v2, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v1

    check-cast v1, Landroid/text/Spannable;

    invoke-static {v1}, Lcom/google/android/location/settings/LocationSettingsCheckerActivity;->a(Landroid/text/Spannable;)Landroid/text/Spannable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 250
    sget v0, Lcom/google/android/gms/j;->md:I

    invoke-virtual {v4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 251
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_2
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v6

    .line 252
    invoke-virtual {p0}, Lcom/google/android/location/settings/LocationSettingsCheckerActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v1

    sget v2, Lcom/google/android/gms/l;->cD:I

    const/4 v7, 0x0

    invoke-virtual {v1, v2, v0, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v7

    sget v1, Lcom/google/android/gms/j;->ka:I

    invoke-virtual {v7, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    sget v2, Lcom/google/android/gms/j;->sy:I

    invoke-virtual {v7, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    packed-switch v6, :pswitch_data_0

    const-string v1, "LocationSettingsChecker"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v7, "Invalid message: "

    invoke-direct {v2, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 236
    :cond_0
    sget v0, Lcom/google/android/gms/p;->qh:I

    goto/16 :goto_0

    .line 247
    :cond_1
    sget v1, Lcom/google/android/gms/p;->pZ:I

    goto :goto_1

    .line 252
    :pswitch_0
    sget v6, Lcom/google/android/gms/h;->bP:I

    invoke-virtual {v1, v6}, Landroid/widget/ImageView;->setImageResource(I)V

    sget v1, Lcom/google/android/gms/p;->pW:I

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(I)V

    :goto_3
    invoke-virtual {v0, v7}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    goto :goto_2

    :pswitch_1
    sget v6, Lcom/google/android/gms/h;->bP:I

    invoke-virtual {v1, v6}, Landroid/widget/ImageView;->setImageResource(I)V

    sget v1, Lcom/google/android/gms/p;->pY:I

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(I)V

    goto :goto_3

    :pswitch_2
    sget v6, Lcom/google/android/gms/h;->bP:I

    invoke-virtual {v1, v6}, Landroid/widget/ImageView;->setImageResource(I)V

    sget v1, Lcom/google/android/gms/p;->pX:I

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(I)V

    goto :goto_3

    :pswitch_3
    sget v6, Lcom/google/android/gms/h;->bQ:I

    invoke-virtual {v1, v6}, Landroid/widget/ImageView;->setImageResource(I)V

    sget v1, Lcom/google/android/gms/p;->qb:I

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(I)V

    goto :goto_3

    :pswitch_4
    sget v6, Lcom/google/android/gms/h;->bQ:I

    invoke-virtual {v1, v6}, Landroid/widget/ImageView;->setImageResource(I)V

    sget v1, Lcom/google/android/gms/p;->qc:I

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(I)V

    goto :goto_3

    :pswitch_5
    sget v6, Lcom/google/android/gms/h;->bO:I

    invoke-virtual {v1, v6}, Landroid/widget/ImageView;->setImageResource(I)V

    sget v1, Lcom/google/android/gms/p;->pU:I

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(I)V

    goto :goto_3

    :pswitch_6
    sget v6, Lcom/google/android/gms/h;->bN:I

    invoke-virtual {v1, v6}, Landroid/widget/ImageView;->setImageResource(I)V

    sget v1, Lcom/google/android/gms/p;->pV:I

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(I)V

    goto :goto_3

    .line 254
    :cond_2
    invoke-virtual {v3, v4}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 255
    if-eqz p1, :cond_3

    sget v0, Lcom/google/android/gms/p;->qg:I

    :goto_4
    invoke-virtual {v3, v0, p0}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 258
    sget v0, Lcom/google/android/gms/p;->qd:I

    invoke-virtual {v3, v0, p0}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 260
    invoke-virtual {v3}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    .line 261
    sget v0, Lcom/google/android/gms/j;->cE:I

    invoke-virtual {v4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    .line 262
    new-instance v2, Lcom/google/android/location/settings/m;

    invoke-direct {v2, p0, v1}, Lcom/google/android/location/settings/m;-><init>(Lcom/google/android/location/settings/LocationSettingsCheckerActivity;Landroid/app/AlertDialog;)V

    invoke-virtual {v0, v2}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 277
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/android/location/settings/LocationSettingsCheckerActivity;->f:J

    .line 278
    invoke-virtual {v1}, Landroid/app/AlertDialog;->show()V

    .line 279
    return-void

    .line 255
    :cond_3
    sget v0, Lcom/google/android/gms/p;->qf:I

    goto :goto_4

    .line 252
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method

.method static synthetic a(Lcom/google/android/location/settings/LocationSettingsCheckerActivity;Z)Z
    .locals 0

    .prologue
    .line 45
    iput-boolean p1, p0, Lcom/google/android/location/settings/LocationSettingsCheckerActivity;->d:Z

    return p1
.end method

.method private b()V
    .locals 2

    .prologue
    .line 80
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    iget-wide v0, p0, Lcom/google/android/location/settings/LocationSettingsCheckerActivity;->f:J

    .line 83
    return-void
.end method

.method private c()V
    .locals 1

    .prologue
    .line 362
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lcom/google/android/location/settings/LocationSettingsCheckerActivity;->setResult(I)V

    .line 363
    invoke-virtual {p0}, Lcom/google/android/location/settings/LocationSettingsCheckerActivity;->finish()V

    .line 364
    return-void
.end method

.method private d()V
    .locals 1

    .prologue
    .line 367
    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Lcom/google/android/location/settings/LocationSettingsCheckerActivity;->setResult(I)V

    .line 368
    invoke-virtual {p0}, Lcom/google/android/location/settings/LocationSettingsCheckerActivity;->finish()V

    .line 369
    return-void
.end method

.method private e()V
    .locals 1

    .prologue
    .line 372
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/location/settings/LocationSettingsCheckerActivity;->setResult(I)V

    .line 373
    invoke-virtual {p0}, Lcom/google/android/location/settings/LocationSettingsCheckerActivity;->finish()V

    .line 374
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 325
    iget-object v0, p0, Lcom/google/android/location/settings/LocationSettingsCheckerActivity;->c:Lcom/google/android/location/settings/n;

    invoke-virtual {v0}, Lcom/google/android/location/settings/n;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 326
    invoke-direct {p0}, Lcom/google/android/location/settings/LocationSettingsCheckerActivity;->d()V

    .line 330
    :goto_0
    return-void

    .line 328
    :cond_0
    invoke-direct {p0}, Lcom/google/android/location/settings/LocationSettingsCheckerActivity;->c()V

    goto :goto_0
.end method

.method public onCancel(Landroid/content/DialogInterface;)V
    .locals 0

    .prologue
    .line 356
    invoke-direct {p0}, Lcom/google/android/location/settings/LocationSettingsCheckerActivity;->b()V

    .line 358
    invoke-direct {p0}, Lcom/google/android/location/settings/LocationSettingsCheckerActivity;->e()V

    .line 359
    return-void
.end method

.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 334
    packed-switch p2, :pswitch_data_0

    .line 352
    :cond_0
    :goto_0
    return-void

    .line 336
    :pswitch_0
    invoke-direct {p0}, Lcom/google/android/location/settings/LocationSettingsCheckerActivity;->b()V

    .line 338
    iget-object v1, p0, Lcom/google/android/location/settings/LocationSettingsCheckerActivity;->c:Lcom/google/android/location/settings/n;

    iget-boolean v0, v1, Lcom/google/android/location/settings/n;->f:Z

    if-eqz v0, :cond_1

    iget-object v0, v1, Lcom/google/android/location/settings/n;->a:Lcom/google/android/location/settings/j;

    iget-object v0, v0, Lcom/google/android/location/settings/j;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/gsf/p;->a(Landroid/content/Context;)Z

    :cond_1
    iget-boolean v0, v1, Lcom/google/android/location/settings/n;->d:Z

    if-eqz v0, :cond_2

    iget-object v0, v1, Lcom/google/android/location/settings/n;->a:Lcom/google/android/location/settings/j;

    iget-object v0, v0, Lcom/google/android/location/settings/j;->c:Landroid/net/wifi/WifiManager;

    invoke-virtual {v0, v3}, Landroid/net/wifi/WifiManager;->setWifiEnabled(Z)Z

    :cond_2
    iget-boolean v0, v1, Lcom/google/android/location/settings/n;->e:Z

    if-eqz v0, :cond_3

    iget-object v0, v1, Lcom/google/android/location/settings/n;->a:Lcom/google/android/location/settings/j;

    iget-object v0, v0, Lcom/google/android/location/settings/j;->e:Landroid/content/ContentResolver;

    const-string v2, "wifi_scan_always_enabled"

    invoke-static {v0, v2, v3}, Landroid/provider/Settings$Global;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    :cond_3
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iget-boolean v0, v1, Lcom/google/android/location/settings/n;->b:Z

    if-eqz v0, :cond_4

    const-string v0, "gps"

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_4
    iget-boolean v0, v1, Lcom/google/android/location/settings/n;->c:Z

    if-eqz v0, :cond_5

    const-string v0, "network"

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_5
    invoke-virtual {v2}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_6

    if-eqz p0, :cond_0

    invoke-interface {p0}, Lcom/google/android/location/settings/p;->a()V

    goto :goto_0

    :cond_6
    if-eqz p0, :cond_7

    new-instance v0, Lcom/google/android/location/settings/o;

    invoke-direct {v0, v1, p0}, Lcom/google/android/location/settings/o;-><init>(Lcom/google/android/location/settings/n;Lcom/google/android/location/settings/p;)V

    :goto_1
    iget-object v1, v1, Lcom/google/android/location/settings/n;->a:Lcom/google/android/location/settings/j;

    invoke-virtual {v1, v2, v0}, Lcom/google/android/location/settings/j;->a(Ljava/util/ArrayList;Lcom/google/android/location/settings/l;)V

    goto :goto_0

    :cond_7
    const/4 v0, 0x0

    goto :goto_1

    .line 341
    :pswitch_1
    iget-boolean v0, p0, Lcom/google/android/location/settings/LocationSettingsCheckerActivity;->d:Z

    if-eqz v0, :cond_8

    .line 342
    invoke-direct {p0}, Lcom/google/android/location/settings/LocationSettingsCheckerActivity;->b()V

    .line 344
    iget-object v0, p0, Lcom/google/android/location/settings/LocationSettingsCheckerActivity;->b:Lcom/google/android/location/n/w;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/location/settings/LocationSettingsCheckerActivity;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/location/n/w;->a(ZLjava/lang/String;)V

    .line 349
    :goto_2
    invoke-direct {p0}, Lcom/google/android/location/settings/LocationSettingsCheckerActivity;->e()V

    goto :goto_0

    .line 346
    :cond_8
    invoke-direct {p0}, Lcom/google/android/location/settings/LocationSettingsCheckerActivity;->b()V

    goto :goto_2

    .line 334
    :pswitch_data_0
    .packed-switch -0x2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 9

    .prologue
    const/4 v5, 0x0

    const/4 v6, 0x1

    .line 122
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 125
    invoke-static {p0}, Lcom/google/android/gms/common/util/e;->a(Landroid/app/Activity;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/settings/LocationSettingsCheckerActivity;->a:Ljava/lang/String;

    .line 126
    iget-object v0, p0, Lcom/google/android/location/settings/LocationSettingsCheckerActivity;->a:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 127
    const-string v0, "LocationSettingsChecker"

    const-string v1, "Cannot find caller. Did you forget to use startActivityForResult?"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 128
    invoke-direct {p0}, Lcom/google/android/location/settings/LocationSettingsCheckerActivity;->c()V

    .line 206
    :goto_0
    return-void

    .line 132
    :cond_0
    new-instance v0, Lcom/google/android/location/n/w;

    invoke-direct {v0, p0}, Lcom/google/android/location/n/w;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/location/settings/LocationSettingsCheckerActivity;->b:Lcom/google/android/location/n/w;

    .line 134
    invoke-virtual {p0}, Lcom/google/android/location/settings/LocationSettingsCheckerActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 135
    new-instance v7, Lcom/google/android/location/settings/j;

    invoke-direct {v7, p0}, Lcom/google/android/location/settings/j;-><init>(Landroid/content/Context;)V

    .line 137
    const-string v1, "locationRequests"

    sget-object v2, Lcom/google/android/gms/location/LocationRequest;->CREATOR:Lcom/google/android/gms/location/o;

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/common/internal/safeparcel/d;->b(Landroid/content/Intent;Ljava/lang/String;Landroid/os/Parcelable$Creator;)Ljava/util/ArrayList;

    move-result-object v1

    .line 141
    const-string v2, "showDialog"

    invoke-virtual {v0, v2, v6}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v8

    .line 145
    if-nez v1, :cond_1

    .line 146
    invoke-direct {p0}, Lcom/google/android/location/settings/LocationSettingsCheckerActivity;->c()V

    goto :goto_0

    .line 150
    :cond_1
    new-instance v4, Lcom/google/android/location/settings/q;

    iget-object v0, p0, Lcom/google/android/location/settings/LocationSettingsCheckerActivity;->a:Ljava/lang/String;

    invoke-direct {v4, v7, v0}, Lcom/google/android/location/settings/q;-><init>(Lcom/google/android/location/settings/j;Ljava/lang/String;)V

    .line 153
    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/location/LocationRequest;

    .line 155
    :try_start_0
    invoke-virtual {v0}, Lcom/google/android/gms/location/LocationRequest;->b()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :pswitch_0
    goto :goto_1

    :pswitch_1
    const/4 v0, 0x1

    iput-boolean v0, v4, Lcom/google/android/location/settings/q;->d:Z

    const/4 v0, 0x1

    iput-boolean v0, v4, Lcom/google/android/location/settings/q;->c:Z
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 157
    :catch_0
    move-exception v0

    invoke-direct {p0}, Lcom/google/android/location/settings/LocationSettingsCheckerActivity;->c()V

    goto :goto_0

    .line 155
    :pswitch_2
    const/4 v0, 0x1

    :try_start_1
    iput-boolean v0, v4, Lcom/google/android/location/settings/q;->c:Z

    goto :goto_1

    :pswitch_3
    const/4 v0, 0x1

    iput-boolean v0, v4, Lcom/google/android/location/settings/q;->c:Z
    :try_end_1
    .catch Ljava/lang/ClassCastException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1

    .line 160
    :cond_2
    new-instance v0, Lcom/google/android/location/settings/n;

    iget-object v1, v4, Lcom/google/android/location/settings/q;->a:Lcom/google/android/location/settings/j;

    iget-boolean v2, v4, Lcom/google/android/location/settings/q;->b:Z

    iget-boolean v3, v4, Lcom/google/android/location/settings/q;->d:Z

    iget-boolean v4, v4, Lcom/google/android/location/settings/q;->c:Z

    invoke-direct/range {v0 .. v5}, Lcom/google/android/location/settings/n;-><init>(Lcom/google/android/location/settings/j;ZZZB)V

    iput-object v0, p0, Lcom/google/android/location/settings/LocationSettingsCheckerActivity;->c:Lcom/google/android/location/settings/n;

    .line 164
    iget-object v0, p0, Lcom/google/android/location/settings/LocationSettingsCheckerActivity;->c:Lcom/google/android/location/settings/n;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iget-boolean v2, v0, Lcom/google/android/location/settings/n;->b:Z

    if-eqz v2, :cond_7

    iget-boolean v2, v0, Lcom/google/android/location/settings/n;->c:Z

    if-eqz v2, :cond_7

    const/4 v2, 0x2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_3
    :goto_2
    iget-boolean v2, v0, Lcom/google/android/location/settings/n;->d:Z

    if-eqz v2, :cond_9

    const/4 v2, 0x3

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_4
    :goto_3
    iget-boolean v2, v0, Lcom/google/android/location/settings/n;->c:Z

    if-eqz v2, :cond_5

    iget-object v2, v0, Lcom/google/android/location/settings/n;->a:Lcom/google/android/location/settings/j;

    iget-object v2, v2, Lcom/google/android/location/settings/j;->b:Lcom/google/android/location/n/w;

    invoke-virtual {v2}, Lcom/google/android/location/n/w;->a()Z

    move-result v2

    if-eqz v2, :cond_5

    const/4 v2, 0x5

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_5
    iget-boolean v0, v0, Lcom/google/android/location/settings/n;->f:Z

    if-eqz v0, :cond_6

    const/4 v0, 0x6

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 173
    :cond_6
    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_a

    .line 174
    invoke-direct {p0}, Lcom/google/android/location/settings/LocationSettingsCheckerActivity;->d()V

    goto/16 :goto_0

    .line 164
    :cond_7
    iget-boolean v2, v0, Lcom/google/android/location/settings/n;->b:Z

    if-eqz v2, :cond_8

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    :cond_8
    iget-boolean v2, v0, Lcom/google/android/location/settings/n;->c:Z

    if-eqz v2, :cond_3

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    :cond_9
    iget-boolean v2, v0, Lcom/google/android/location/settings/n;->e:Z

    if-eqz v2, :cond_4

    const/4 v2, 0x4

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 181
    :cond_a
    iget-object v0, p0, Lcom/google/android/location/settings/LocationSettingsCheckerActivity;->c:Lcom/google/android/location/settings/n;

    iget-boolean v2, v0, Lcom/google/android/location/settings/n;->b:Z

    iget-boolean v0, v0, Lcom/google/android/location/settings/n;->c:Z

    or-int/2addr v0, v2

    if-eqz v0, :cond_c

    iget-object v0, v7, Lcom/google/android/location/settings/j;->f:Landroid/os/UserManager;

    if-nez v0, :cond_b

    move v0, v5

    :goto_4
    if-eqz v0, :cond_c

    .line 182
    invoke-direct {p0}, Lcom/google/android/location/settings/LocationSettingsCheckerActivity;->c()V

    goto/16 :goto_0

    .line 181
    :cond_b
    iget-object v0, v7, Lcom/google/android/location/settings/j;->f:Landroid/os/UserManager;

    const-string v2, "no_share_location"

    invoke-virtual {v0, v2}, Landroid/os/UserManager;->hasUserRestriction(Ljava/lang/String;)Z

    move-result v0

    goto :goto_4

    .line 188
    :cond_c
    if-nez v8, :cond_d

    .line 189
    invoke-direct {p0}, Lcom/google/android/location/settings/LocationSettingsCheckerActivity;->e()V

    goto/16 :goto_0

    .line 194
    :cond_d
    iget-object v0, p0, Lcom/google/android/location/settings/LocationSettingsCheckerActivity;->b:Lcom/google/android/location/n/w;

    iget-object v2, p0, Lcom/google/android/location/settings/LocationSettingsCheckerActivity;->a:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/google/android/location/n/w;->b()Ljava/util/HashSet;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_e

    move v0, v6

    :goto_5
    if-nez v0, :cond_f

    .line 195
    invoke-direct {p0}, Lcom/google/android/location/settings/LocationSettingsCheckerActivity;->e()V

    goto/16 :goto_0

    :cond_e
    move v0, v5

    .line 194
    goto :goto_5

    .line 200
    :cond_f
    invoke-virtual {v7}, Lcom/google/android/location/settings/j;->a()Z

    move-result v0

    if-nez v0, :cond_10

    invoke-virtual {v7}, Lcom/google/android/location/settings/j;->b()Z

    move-result v0

    if-eqz v0, :cond_11

    :cond_10
    move v5, v6

    .line 202
    :cond_11
    invoke-static {v1}, Lcom/google/android/location/settings/LocationSettingsCheckerActivity;->a(Ljava/util/List;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/settings/LocationSettingsCheckerActivity;->e:Ljava/lang/String;

    .line 205
    invoke-direct {p0, v5, v1}, Lcom/google/android/location/settings/LocationSettingsCheckerActivity;->a(ZLjava/util/List;)V

    goto/16 :goto_0

    .line 155
    :pswitch_data_0
    .packed-switch 0x64
        :pswitch_1
        :pswitch_0
        :pswitch_3
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method
