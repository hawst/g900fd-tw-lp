.class final Lcom/google/android/location/copresence/k/b;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lcom/google/android/location/copresence/k/a;


# direct methods
.method constructor <init>(Lcom/google/android/location/copresence/k/a;)V
    .locals 0

    .prologue
    .line 39
    iput-object p1, p0, Lcom/google/android/location/copresence/k/b;->a:Lcom/google/android/location/copresence/k/a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 7

    .prologue
    const/4 v6, 0x3

    .line 43
    iget-object v0, p0, Lcom/google/android/location/copresence/k/b;->a:Lcom/google/android/location/copresence/k/a;

    iget-object v0, v0, Lcom/google/android/location/copresence/k/a;->d:Landroid/os/Handler;

    invoke-virtual {v0, p0}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 44
    iget-object v0, p0, Lcom/google/android/location/copresence/k/b;->a:Lcom/google/android/location/copresence/k/a;

    iget-object v0, v0, Lcom/google/android/location/copresence/k/a;->f:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->peek()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/copresence/k/f;

    .line 45
    if-eqz v0, :cond_b

    .line 46
    invoke-virtual {v0}, Lcom/google/android/location/copresence/k/f;->b()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 48
    iget-object v1, p0, Lcom/google/android/location/copresence/k/b;->a:Lcom/google/android/location/copresence/k/a;

    iget-object v1, v1, Lcom/google/android/location/copresence/k/a;->f:Ljava/util/LinkedList;

    invoke-virtual {v1}, Ljava/util/LinkedList;->removeFirst()Ljava/lang/Object;

    .line 49
    iget-object v1, p0, Lcom/google/android/location/copresence/k/b;->a:Lcom/google/android/location/copresence/k/a;

    iget-object v1, v1, Lcom/google/android/location/copresence/k/a;->e:Lcom/google/android/location/copresence/k/c;

    invoke-interface {v1, v0}, Lcom/google/android/location/copresence/k/c;->b(Lcom/google/android/location/copresence/k/f;)V

    .line 50
    iget-object v0, p0, Lcom/google/android/location/copresence/k/b;->a:Lcom/google/android/location/copresence/k/a;

    invoke-virtual {v0}, Lcom/google/android/location/copresence/k/a;->a()V

    .line 75
    :goto_0
    return-void

    .line 51
    :cond_0
    iget-object v1, p0, Lcom/google/android/location/copresence/k/b;->a:Lcom/google/android/location/copresence/k/a;

    iget v1, v1, Lcom/google/android/location/copresence/k/a;->g:I

    iget-object v2, p0, Lcom/google/android/location/copresence/k/b;->a:Lcom/google/android/location/copresence/k/a;

    iget v2, v2, Lcom/google/android/location/copresence/k/a;->a:I

    if-ge v1, v2, :cond_a

    .line 53
    iget-object v1, p0, Lcom/google/android/location/copresence/k/b;->a:Lcom/google/android/location/copresence/k/a;

    iget v1, v1, Lcom/google/android/location/copresence/k/a;->g:I

    if-nez v1, :cond_2

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    iget-object v1, p0, Lcom/google/android/location/copresence/k/b;->a:Lcom/google/android/location/copresence/k/a;

    iget-wide v4, v1, Lcom/google/android/location/copresence/k/a;->i:J

    sub-long/2addr v2, v4

    iget-object v1, p0, Lcom/google/android/location/copresence/k/b;->a:Lcom/google/android/location/copresence/k/a;

    iget-wide v4, v1, Lcom/google/android/location/copresence/k/a;->c:J

    sub-long v2, v4, v2

    .line 54
    :goto_1
    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-gtz v1, :cond_8

    .line 56
    invoke-virtual {v0}, Lcom/google/android/location/copresence/k/f;->c()Lcom/google/android/location/copresence/k/f;

    move-result-object v1

    if-nez v1, :cond_6

    iget-object v1, p0, Lcom/google/android/location/copresence/k/b;->a:Lcom/google/android/location/copresence/k/a;

    iget-object v1, v1, Lcom/google/android/location/copresence/k/a;->e:Lcom/google/android/location/copresence/k/c;

    invoke-interface {v1, v0}, Lcom/google/android/location/copresence/k/c;->c(Lcom/google/android/location/copresence/k/f;)V

    iget-object v1, p0, Lcom/google/android/location/copresence/k/b;->a:Lcom/google/android/location/copresence/k/a;

    invoke-virtual {v0}, Lcom/google/android/location/copresence/k/f;->a()Z

    move-result v2

    iput-boolean v2, v1, Lcom/google/android/location/copresence/k/a;->j:Z

    iget-object v1, p0, Lcom/google/android/location/copresence/k/b;->a:Lcom/google/android/location/copresence/k/a;

    iget v2, v1, Lcom/google/android/location/copresence/k/a;->g:I

    add-int/lit8 v2, v2, 0x1

    iput v2, v1, Lcom/google/android/location/copresence/k/a;->g:I

    iget-object v1, p0, Lcom/google/android/location/copresence/k/b;->a:Lcom/google/android/location/copresence/k/a;

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    iput-wide v2, v1, Lcom/google/android/location/copresence/k/a;->h:J

    iget-object v1, p0, Lcom/google/android/location/copresence/k/b;->a:Lcom/google/android/location/copresence/k/a;

    iget-boolean v1, v1, Lcom/google/android/location/copresence/k/a;->j:Z

    if-eqz v1, :cond_4

    invoke-static {v6}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v1

    if-eqz v1, :cond_1

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "SerializedStateTransitioner: Success starting transition: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v0, v0, Lcom/google/android/location/copresence/k/f;->e:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->b(Ljava/lang/String;)I

    :cond_1
    iget-object v0, p0, Lcom/google/android/location/copresence/k/b;->a:Lcom/google/android/location/copresence/k/a;

    iget-object v0, v0, Lcom/google/android/location/copresence/k/a;->d:Landroid/os/Handler;

    invoke-virtual {v0, p0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0

    .line 53
    :cond_2
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    iget-object v1, p0, Lcom/google/android/location/copresence/k/b;->a:Lcom/google/android/location/copresence/k/a;

    iget-wide v4, v1, Lcom/google/android/location/copresence/k/a;->h:J

    sub-long/2addr v2, v4

    iget-object v1, p0, Lcom/google/android/location/copresence/k/b;->a:Lcom/google/android/location/copresence/k/a;

    iget-boolean v1, v1, Lcom/google/android/location/copresence/k/a;->j:Z

    if-eqz v1, :cond_3

    invoke-virtual {v0}, Lcom/google/android/location/copresence/k/f;->d()J

    move-result-wide v4

    sub-long v2, v4, v2

    goto :goto_1

    :cond_3
    iget-object v1, p0, Lcom/google/android/location/copresence/k/b;->a:Lcom/google/android/location/copresence/k/a;

    iget-wide v4, v1, Lcom/google/android/location/copresence/k/a;->b:J

    sub-long v2, v4, v2

    goto :goto_1

    .line 56
    :cond_4
    invoke-static {v6}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v1

    if-eqz v1, :cond_5

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "SerializedStateTransitioner: Failed state transition "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v0, v0, Lcom/google/android/location/copresence/k/f;->e:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ". Retry "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/location/copresence/k/b;->a:Lcom/google/android/location/copresence/k/a;

    iget v1, v1, Lcom/google/android/location/copresence/k/a;->g:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " in "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/location/copresence/k/b;->a:Lcom/google/android/location/copresence/k/a;

    iget-wide v2, v1, Lcom/google/android/location/copresence/k/a;->b:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "ms."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->b(Ljava/lang/String;)I

    :cond_5
    iget-object v0, p0, Lcom/google/android/location/copresence/k/b;->a:Lcom/google/android/location/copresence/k/a;

    iget-object v0, v0, Lcom/google/android/location/copresence/k/a;->d:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/location/copresence/k/b;->a:Lcom/google/android/location/copresence/k/a;

    iget-wide v2, v1, Lcom/google/android/location/copresence/k/a;->b:J

    invoke-virtual {v0, p0, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto/16 :goto_0

    :cond_6
    invoke-static {v6}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v2

    if-eqz v2, :cond_7

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "SerializedStateTransitioner: Adding precondition for "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v0, v0, Lcom/google/android/location/copresence/k/f;->e:Ljava/lang/String;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ": "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, v1, Lcom/google/android/location/copresence/k/f;->e:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->b(Ljava/lang/String;)I

    :cond_7
    iget-object v0, p0, Lcom/google/android/location/copresence/k/b;->a:Lcom/google/android/location/copresence/k/a;

    iget-object v0, v0, Lcom/google/android/location/copresence/k/a;->f:Ljava/util/LinkedList;

    invoke-virtual {v0, v1}, Ljava/util/LinkedList;->addFirst(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/location/copresence/k/b;->a:Lcom/google/android/location/copresence/k/a;

    invoke-virtual {v0}, Lcom/google/android/location/copresence/k/a;->a()V

    goto/16 :goto_0

    .line 59
    :cond_8
    invoke-static {v6}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v1

    if-eqz v1, :cond_9

    .line 60
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v4, "SerializedStateTransitioner: Scheduling check in "

    invoke-direct {v1, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, "ms: "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v0, v0, Lcom/google/android/location/copresence/k/f;->e:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->b(Ljava/lang/String;)I

    .line 63
    :cond_9
    iget-object v0, p0, Lcom/google/android/location/copresence/k/b;->a:Lcom/google/android/location/copresence/k/a;

    iget-object v0, v0, Lcom/google/android/location/copresence/k/a;->d:Landroid/os/Handler;

    invoke-virtual {v0, p0, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto/16 :goto_0

    .line 67
    :cond_a
    iget-object v1, p0, Lcom/google/android/location/copresence/k/b;->a:Lcom/google/android/location/copresence/k/a;

    iget-object v1, v1, Lcom/google/android/location/copresence/k/a;->e:Lcom/google/android/location/copresence/k/c;

    invoke-interface {v1, v0}, Lcom/google/android/location/copresence/k/c;->a(Lcom/google/android/location/copresence/k/f;)V

    goto/16 :goto_0

    .line 70
    :cond_b
    invoke-static {v6}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 71
    const-string v0, "SerializedStateTransitioner: Finished queue"

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->b(Ljava/lang/String;)I

    .line 73
    :cond_c
    iget-object v0, p0, Lcom/google/android/location/copresence/k/b;->a:Lcom/google/android/location/copresence/k/a;

    iget-object v0, v0, Lcom/google/android/location/copresence/k/a;->e:Lcom/google/android/location/copresence/k/c;

    invoke-interface {v0}, Lcom/google/android/location/copresence/k/c;->a()V

    goto/16 :goto_0
.end method
