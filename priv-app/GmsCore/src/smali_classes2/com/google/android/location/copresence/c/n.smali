.class public final Lcom/google/android/location/copresence/c/n;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/location/copresence/z;


# instance fields
.field private final b:Lcom/google/android/location/copresence/c/h;

.field private final c:I

.field private d:Lcom/google/android/location/copresence/ab;

.field private final e:Landroid/os/PowerManager;

.field private final f:Landroid/content/Context;

.field private final g:Landroid/os/Handler;

.field private h:Z

.field private i:Z

.field private j:I

.field private k:Lcom/google/ac/b/c/o;

.field private final l:Landroid/content/BroadcastReceiver;

.field private final m:Lcom/google/android/location/copresence/c/l;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/location/copresence/c/h;I)V
    .locals 1

    .prologue
    .line 134
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 59
    new-instance v0, Lcom/google/android/location/copresence/c/o;

    invoke-direct {v0, p0}, Lcom/google/android/location/copresence/c/o;-><init>(Lcom/google/android/location/copresence/c/n;)V

    iput-object v0, p0, Lcom/google/android/location/copresence/c/n;->l:Landroid/content/BroadcastReceiver;

    .line 70
    new-instance v0, Lcom/google/android/location/copresence/c/p;

    invoke-direct {v0, p0}, Lcom/google/android/location/copresence/c/p;-><init>(Lcom/google/android/location/copresence/c/n;)V

    iput-object v0, p0, Lcom/google/android/location/copresence/c/n;->m:Lcom/google/android/location/copresence/c/l;

    .line 135
    iput-object p2, p0, Lcom/google/android/location/copresence/c/n;->b:Lcom/google/android/location/copresence/c/h;

    .line 136
    iput p3, p0, Lcom/google/android/location/copresence/c/n;->c:I

    .line 137
    const-string v0, "power"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    iput-object v0, p0, Lcom/google/android/location/copresence/c/n;->e:Landroid/os/PowerManager;

    .line 138
    iput-object p1, p0, Lcom/google/android/location/copresence/c/n;->f:Landroid/content/Context;

    .line 139
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/google/android/location/copresence/c/n;->g:Landroid/os/Handler;

    .line 140
    return-void
.end method

.method static synthetic a(Lcom/google/android/location/copresence/c/n;)Landroid/os/PowerManager;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lcom/google/android/location/copresence/c/n;->e:Landroid/os/PowerManager;

    return-object v0
.end method

.method static synthetic b(Lcom/google/android/location/copresence/c/n;)Lcom/google/android/location/copresence/c/h;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lcom/google/android/location/copresence/c/n;->b:Lcom/google/android/location/copresence/c/h;

    return-object v0
.end method

.method private static b(Lcom/google/android/location/copresence/q/al;)V
    .locals 0

    .prologue
    .line 274
    if-eqz p0, :cond_0

    .line 275
    invoke-interface {p0}, Lcom/google/android/location/copresence/q/al;->a()V

    .line 277
    :cond_0
    return-void
.end method

.method static synthetic c(Lcom/google/android/location/copresence/c/n;)V
    .locals 1

    .prologue
    .line 38
    iget-boolean v0, p0, Lcom/google/android/location/copresence/c/n;->i:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/location/copresence/c/n;->b:Lcom/google/android/location/copresence/c/h;

    invoke-virtual {v0}, Lcom/google/android/location/copresence/c/h;->a()V

    :cond_0
    return-void
.end method

.method private static c(Lcom/google/android/location/copresence/q/al;)V
    .locals 0

    .prologue
    .line 280
    if-eqz p0, :cond_0

    .line 281
    invoke-interface {p0}, Lcom/google/android/location/copresence/q/al;->b()V

    .line 283
    :cond_0
    return-void
.end method

.method static synthetic d(Lcom/google/android/location/copresence/c/n;)I
    .locals 1

    .prologue
    .line 38
    iget v0, p0, Lcom/google/android/location/copresence/c/n;->c:I

    return v0
.end method

.method private d()V
    .locals 2

    .prologue
    .line 228
    :try_start_0
    iget-object v0, p0, Lcom/google/android/location/copresence/c/n;->f:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/location/copresence/c/n;->l:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 229
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/location/copresence/c/n;->h:Z
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 233
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method static synthetic e(Lcom/google/android/location/copresence/c/n;)Lcom/google/android/location/copresence/ab;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lcom/google/android/location/copresence/c/n;->d:Lcom/google/android/location/copresence/ab;

    return-object v0
.end method

.method private e()Z
    .locals 2

    .prologue
    .line 242
    iget-object v0, p0, Lcom/google/android/location/copresence/c/n;->f:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    const-string v1, "android.hardware.microphone"

    invoke-virtual {v0, v1}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method static synthetic f(Lcom/google/android/location/copresence/c/n;)V
    .locals 0

    .prologue
    .line 38
    invoke-direct {p0}, Lcom/google/android/location/copresence/c/n;->d()V

    return-void
.end method

.method private f()Z
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 248
    iget-object v0, p0, Lcom/google/android/location/copresence/c/n;->f:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    const-string v3, "android.permission.CAPTURE_AUDIO_HOTWORD"

    iget-object v4, p0, Lcom/google/android/location/copresence/c/n;->f:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/content/pm/PackageManager;->checkPermission(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    .line 251
    :goto_0
    const-string v3, "HOTWORD"

    sget-object v4, Lcom/google/android/gms/audiomodem/m;->n:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v4}, Lcom/google/android/gms/common/a/d;->c()Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    .line 253
    invoke-direct {p0}, Lcom/google/android/location/copresence/c/n;->e()Z

    move-result v4

    if-eqz v4, :cond_1

    if-eqz v0, :cond_1

    if-eqz v3, :cond_1

    :goto_1
    return v1

    :cond_0
    move v0, v2

    .line 248
    goto :goto_0

    :cond_1
    move v1, v2

    .line 253
    goto :goto_1
.end method

.method static synthetic g(Lcom/google/android/location/copresence/c/n;)Z
    .locals 1

    .prologue
    .line 38
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/location/copresence/c/n;->i:Z

    return v0
.end method


# virtual methods
.method public final a(Lcom/google/android/location/copresence/ab;Lcom/google/android/location/copresence/as;Lcom/google/android/location/copresence/q/al;)V
    .locals 8

    .prologue
    const/4 v3, 0x0

    const/4 v7, 0x3

    const/4 v1, 0x1

    .line 160
    invoke-static {v7}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 161
    const-string v0, "E2E Listen: AudioTokenListener: Requested to start listening"

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->b(Ljava/lang/String;)I

    .line 163
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/location/copresence/c/n;->b()Z

    move-result v0

    if-nez v0, :cond_1

    .line 164
    new-instance v0, Lcom/google/android/location/copresence/ac;

    invoke-direct {v0}, Lcom/google/android/location/copresence/ac;-><init>()V

    throw v0

    .line 167
    :cond_1
    iget-boolean v0, p0, Lcom/google/android/location/copresence/c/n;->i:Z

    if-eqz v0, :cond_3

    .line 169
    invoke-static {v7}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 170
    const-string v0, "E2E Listen: AudioTokenListener: Requested listen after listening already requested"

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->b(Ljava/lang/String;)I

    .line 197
    :cond_2
    :goto_0
    return-void

    .line 176
    :cond_3
    iget v0, p2, Lcom/google/android/location/copresence/as;->a:I

    iget-object v2, p0, Lcom/google/android/location/copresence/c/n;->e:Landroid/os/PowerManager;

    invoke-virtual {v2}, Landroid/os/PowerManager;->isScreenOn()Z

    move-result v2

    if-eqz v2, :cond_4

    if-ne v0, v1, :cond_4

    invoke-direct {p0}, Lcom/google/android/location/copresence/c/n;->f()Z

    move-result v0

    if-eqz v0, :cond_5

    :cond_4
    move v0, v1

    :goto_1
    if-nez v0, :cond_6

    .line 177
    iget v0, p0, Lcom/google/android/location/copresence/c/n;->c:I

    invoke-interface {p1, v0}, Lcom/google/android/location/copresence/ab;->a(I)V

    .line 178
    invoke-static {p3}, Lcom/google/android/location/copresence/c/n;->c(Lcom/google/android/location/copresence/q/al;)V

    goto :goto_0

    :cond_5
    move v0, v3

    .line 176
    goto :goto_1

    .line 181
    :cond_6
    iput-object p1, p0, Lcom/google/android/location/copresence/c/n;->d:Lcom/google/android/location/copresence/ab;

    .line 182
    invoke-direct {p0}, Lcom/google/android/location/copresence/c/n;->f()Z

    move-result v0

    if-nez v0, :cond_7

    .line 183
    iget-boolean v0, p0, Lcom/google/android/location/copresence/c/n;->h:Z

    if-nez v0, :cond_7

    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    const-string v2, "android.intent.action.SCREEN_OFF"

    invoke-virtual {v0, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v2, "android.intent.action.SCREEN_ON"

    invoke-virtual {v0, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/location/copresence/c/n;->f:Landroid/content/Context;

    iget-object v4, p0, Lcom/google/android/location/copresence/c/n;->l:Landroid/content/BroadcastReceiver;

    const/4 v5, 0x0

    iget-object v6, p0, Lcom/google/android/location/copresence/c/n;->g:Landroid/os/Handler;

    invoke-virtual {v2, v4, v0, v5, v6}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;Ljava/lang/String;Landroid/os/Handler;)Landroid/content/Intent;

    iput-boolean v1, p0, Lcom/google/android/location/copresence/c/n;->h:Z

    .line 186
    :cond_7
    iput-boolean v1, p0, Lcom/google/android/location/copresence/c/n;->i:Z

    .line 187
    iget v0, p2, Lcom/google/android/location/copresence/as;->a:I

    iput v0, p0, Lcom/google/android/location/copresence/c/n;->j:I

    .line 188
    iget-object v0, p2, Lcom/google/android/location/copresence/as;->b:Lcom/google/ac/b/c/o;

    iput-object v0, p0, Lcom/google/android/location/copresence/c/n;->k:Lcom/google/ac/b/c/o;

    .line 190
    iget-object v4, p0, Lcom/google/android/location/copresence/c/n;->b:Lcom/google/android/location/copresence/c/h;

    iget v0, p0, Lcom/google/android/location/copresence/c/n;->j:I

    if-ne v0, v1, :cond_a

    const/4 v0, 0x2

    :goto_2
    iget v2, p0, Lcom/google/android/location/copresence/c/n;->c:I

    iget-object v5, p0, Lcom/google/android/location/copresence/c/n;->k:Lcom/google/ac/b/c/o;

    sparse-switch v2, :sswitch_data_0

    const-string v2, "Audio Stack: AudioTokenListener: Invalid medium"

    new-instance v3, Ljava/lang/IllegalStateException;

    const-string v6, "getTokenReceiverParams called with invalid medium"

    invoke-direct {v3, v6}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    invoke-static {v2, v3}, Lcom/google/android/location/copresence/ag;->b(Ljava/lang/String;Ljava/lang/Throwable;)I

    iget-object v2, v5, Lcom/google/ac/b/c/o;->a:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v2}, Lcom/google/android/gms/audiomodem/TokenReceiver$Params;->a(I)Lcom/google/android/gms/audiomodem/TokenReceiver$Params;

    move-result-object v2

    :goto_3
    iget-object v3, p0, Lcom/google/android/location/copresence/c/n;->m:Lcom/google/android/location/copresence/c/l;

    invoke-static {v7}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v5

    if-eqz v5, :cond_8

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Audio Stack: AudioStateMachine: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v6, v4, Lcom/google/android/location/copresence/c/h;->a:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "Requesting to listen for tokens with request"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/google/android/location/copresence/ag;->b(Ljava/lang/String;)I

    :cond_8
    iput v0, v4, Lcom/google/android/location/copresence/c/h;->b:I

    iput-object v2, v4, Lcom/google/android/location/copresence/c/h;->g:Lcom/google/android/gms/audiomodem/TokenReceiver$Params;

    iput-object v3, v4, Lcom/google/android/location/copresence/c/h;->f:Lcom/google/android/location/copresence/c/l;

    iget v2, v4, Lcom/google/android/location/copresence/c/h;->c:I

    packed-switch v2, :pswitch_data_0

    :cond_9
    :goto_4
    iget v0, v4, Lcom/google/android/location/copresence/c/h;->c:I

    invoke-virtual {v4, v0}, Lcom/google/android/location/copresence/c/h;->a(I)V

    .line 196
    invoke-static {p3}, Lcom/google/android/location/copresence/c/n;->b(Lcom/google/android/location/copresence/q/al;)V

    goto/16 :goto_0

    :cond_a
    move v0, v1

    .line 190
    goto :goto_2

    :sswitch_0
    iget-object v2, v5, Lcom/google/ac/b/c/o;->a:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v2}, Lcom/google/android/location/copresence/c/b;->b(I)Lcom/google/android/gms/audiomodem/Encoding;

    move-result-object v5

    new-instance v2, Lcom/google/android/gms/audiomodem/TokenReceiver$Params;

    new-array v6, v1, [Lcom/google/android/gms/audiomodem/Encoding;

    aput-object v5, v6, v3

    invoke-direct {v2, v6}, Lcom/google/android/gms/audiomodem/TokenReceiver$Params;-><init>([Lcom/google/android/gms/audiomodem/Encoding;)V

    goto :goto_3

    :sswitch_1
    iget-object v2, v5, Lcom/google/ac/b/c/o;->a:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v2}, Lcom/google/android/location/copresence/c/b;->a(I)Lcom/google/android/gms/audiomodem/Encoding;

    move-result-object v5

    new-instance v2, Lcom/google/android/gms/audiomodem/TokenReceiver$Params;

    new-array v6, v1, [Lcom/google/android/gms/audiomodem/Encoding;

    aput-object v5, v6, v3

    invoke-direct {v2, v6}, Lcom/google/android/gms/audiomodem/TokenReceiver$Params;-><init>([Lcom/google/android/gms/audiomodem/Encoding;)V

    goto :goto_3

    :pswitch_0
    iput v7, v4, Lcom/google/android/location/copresence/c/h;->c:I

    goto :goto_4

    :pswitch_1
    iget-boolean v2, v4, Lcom/google/android/location/copresence/c/h;->d:Z

    if-eqz v2, :cond_9

    if-ne v0, v1, :cond_9

    iput v1, v4, Lcom/google/android/location/copresence/c/h;->c:I

    goto :goto_4

    :pswitch_2
    invoke-static {v7}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v0

    if-eqz v0, :cond_9

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Audio Stack: AudioStateMachine: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, v4, Lcom/google/android/location/copresence/c/h;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "Remaining in state "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, v4, Lcom/google/android/location/copresence/c/h;->c:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->b(Ljava/lang/String;)I

    goto :goto_4

    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_1
        0x8 -> :sswitch_0
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final a(Lcom/google/android/location/copresence/q/al;)V
    .locals 1

    .prologue
    .line 144
    const/4 v0, 0x3

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 145
    const-string v0, "E2E Listen: AudioTokenListener: Requested to stop listening"

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->b(Ljava/lang/String;)I

    .line 147
    :cond_0
    iget-boolean v0, p0, Lcom/google/android/location/copresence/c/n;->i:Z

    if-nez v0, :cond_1

    .line 148
    invoke-static {p1}, Lcom/google/android/location/copresence/c/n;->c(Lcom/google/android/location/copresence/q/al;)V

    .line 155
    :goto_0
    return-void

    .line 151
    :cond_1
    iget-object v0, p0, Lcom/google/android/location/copresence/c/n;->b:Lcom/google/android/location/copresence/c/h;

    invoke-virtual {v0}, Lcom/google/android/location/copresence/c/h;->a()V

    .line 152
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/location/copresence/c/n;->i:Z

    .line 153
    invoke-direct {p0}, Lcom/google/android/location/copresence/c/n;->d()V

    .line 154
    invoke-static {p1}, Lcom/google/android/location/copresence/c/n;->b(Lcom/google/android/location/copresence/q/al;)V

    goto :goto_0
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 112
    const/4 v0, 0x1

    return v0
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 117
    invoke-direct {p0}, Lcom/google/android/location/copresence/c/n;->e()Z

    move-result v1

    if-nez v1, :cond_0

    .line 129
    :goto_0
    return v0

    .line 120
    :cond_0
    iget v1, p0, Lcom/google/android/location/copresence/c/n;->c:I

    sparse-switch v1, :sswitch_data_0

    .line 126
    const-string v1, "Audio Stack: AudioTokenListener: Invalid medium"

    new-instance v2, Ljava/lang/IllegalStateException;

    const-string v3, "canListen called with invalid medium"

    invoke-direct {v2, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    invoke-static {v1, v2}, Lcom/google/android/location/copresence/ag;->b(Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 122
    :sswitch_0
    invoke-static {}, Lcom/google/android/location/copresence/f/b;->b()Lcom/google/ac/b/c/g;

    move-result-object v0

    iget-object v0, v0, Lcom/google/ac/b/c/g;->e:Lcom/google/ac/b/c/m;

    iget-object v0, v0, Lcom/google/ac/b/c/m;->b:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    goto :goto_0

    .line 124
    :sswitch_1
    invoke-static {}, Lcom/google/android/location/copresence/f/b;->b()Lcom/google/ac/b/c/g;

    move-result-object v0

    iget-object v0, v0, Lcom/google/ac/b/c/g;->e:Lcom/google/ac/b/c/m;

    iget-object v0, v0, Lcom/google/ac/b/c/m;->A:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    goto :goto_0

    .line 120
    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x8 -> :sswitch_1
    .end sparse-switch
.end method

.method public final c()V
    .locals 2

    .prologue
    .line 210
    iget-object v0, p0, Lcom/google/android/location/copresence/c/n;->b:Lcom/google/android/location/copresence/c/h;

    iget v0, v0, Lcom/google/android/location/copresence/c/h;->b:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 212
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/location/copresence/c/n;->a(Lcom/google/android/location/copresence/q/al;)V

    .line 214
    :cond_0
    return-void
.end method
