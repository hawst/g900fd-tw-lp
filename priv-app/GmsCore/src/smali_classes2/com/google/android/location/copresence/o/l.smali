.class final Lcom/google/android/location/copresence/o/l;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lcom/google/android/location/copresence/o/k;


# direct methods
.method constructor <init>(Lcom/google/android/location/copresence/o/k;)V
    .locals 0

    .prologue
    .line 56
    iput-object p1, p0, Lcom/google/android/location/copresence/o/l;->a:Lcom/google/android/location/copresence/o/k;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 7

    .prologue
    .line 59
    iget-object v0, p0, Lcom/google/android/location/copresence/o/l;->a:Lcom/google/android/location/copresence/o/k;

    invoke-static {}, Lcom/google/android/location/copresence/f/a;->d()Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    iget-object v1, v0, Lcom/google/android/location/copresence/o/k;->b:Lcom/google/android/location/copresence/ap;

    invoke-virtual {v1}, Lcom/google/android/location/copresence/ap;->c()V

    iget-object v0, v0, Lcom/google/android/location/copresence/o/k;->a:Lcom/google/android/location/copresence/o/d;

    iget-object v1, v0, Lcom/google/android/location/copresence/o/d;->a:Lcom/google/android/gms/common/util/p;

    invoke-interface {v1}, Lcom/google/android/gms/common/util/p;->a()J

    move-result-wide v4

    sub-long v2, v4, v2

    iget-object v0, v0, Lcom/google/android/location/copresence/o/d;->b:Lcom/google/android/location/copresence/o/g;

    invoke-virtual {v0}, Lcom/google/android/location/copresence/o/g;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    const-string v1, "operations"

    invoke-static {}, Lcom/google/android/location/copresence/q/af;->a()Lcom/google/android/location/copresence/q/ak;

    move-result-object v4

    const-string v5, "deletable"

    const/4 v6, 0x1

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Lcom/google/android/location/copresence/q/ak;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/android/location/copresence/q/ak;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/location/copresence/q/ak;->a()Lcom/google/android/location/copresence/q/ak;

    move-result-object v4

    const-string v5, "deletable"

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    iget-object v3, v4, Lcom/google/android/location/copresence/q/ak;->a:Ljava/lang/StringBuilder;

    const/16 v6, 0x20

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, " < "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    iget-object v2, v4, Lcom/google/android/location/copresence/q/ak;->a:Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 60
    return-void
.end method
