.class public final Lcom/google/android/location/k;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/location/os/a;


# instance fields
.field public final a:Lcom/google/android/location/activity/k;

.field public final b:Lcom/google/android/location/activity/be;


# direct methods
.method public constructor <init>(Lcom/google/android/location/os/bi;)V
    .locals 4

    .prologue
    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    sget-boolean v0, Lcom/google/android/location/i/a;->c:Z

    if-eqz v0, :cond_0

    const-string v0, "ActivityProvider"

    const-string v1, "ActivityDetectionScheduler instantiated."

    invoke-static {v0, v1}, Lcom/google/android/location/o/a/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 42
    :cond_0
    new-instance v0, Lcom/google/android/location/activity/k;

    invoke-direct {v0, p1}, Lcom/google/android/location/activity/k;-><init>(Lcom/google/android/location/os/bi;)V

    iput-object v0, p0, Lcom/google/android/location/k;->a:Lcom/google/android/location/activity/k;

    .line 43
    invoke-interface {p1}, Lcom/google/android/location/os/bi;->y()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 44
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/location/k;->b:Lcom/google/android/location/activity/be;

    .line 49
    :goto_0
    sget-object v0, Lcom/google/android/location/d/a;->o:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    .line 51
    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-ltz v2, :cond_1

    .line 52
    sget-object v2, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v2, v0, v1}, Ljava/util/concurrent/TimeUnit;->toNanos(J)J

    move-result-wide v0

    invoke-interface {p1, v0, v1}, Lcom/google/android/location/os/bi;->b(J)V

    .line 54
    :cond_1
    return-void

    .line 46
    :cond_2
    new-instance v0, Lcom/google/android/location/activity/be;

    iget-object v1, p0, Lcom/google/android/location/k;->a:Lcom/google/android/location/activity/k;

    invoke-direct {v0, p1, v1}, Lcom/google/android/location/activity/be;-><init>(Lcom/google/android/location/os/bi;Lcom/google/android/location/activity/k;)V

    iput-object v0, p0, Lcom/google/android/location/k;->b:Lcom/google/android/location/activity/be;

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 147
    iget-object v0, p0, Lcom/google/android/location/k;->a:Lcom/google/android/location/activity/k;

    invoke-virtual {v0}, Lcom/google/android/location/activity/k;->G()V

    .line 148
    return-void
.end method

.method public final a(IIIZLcom/google/android/location/o/n;)V
    .locals 1

    .prologue
    .line 76
    iget-object v0, p0, Lcom/google/android/location/k;->a:Lcom/google/android/location/activity/k;

    invoke-virtual {v0, p1}, Lcom/google/android/location/activity/k;->b(I)V

    .line 77
    return-void
.end method

.method public final a(IIZ)V
    .locals 0

    .prologue
    .line 214
    return-void
.end method

.method public final a(IIZZLcom/google/android/location/o/n;Ljava/util/Set;)V
    .locals 6

    .prologue
    .line 134
    sget-boolean v0, Lcom/google/android/location/i/a;->b:Z

    if-eqz v0, :cond_0

    const-string v0, "ActivityProvider"

    const-string v1, "setActivityDetectionExternalClientCount, count=%d, minPeriodSec=%d, forceDetectionNow=%s, hasFirstPartyClient=%s, "

    const/4 v2, 0x4

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x2

    invoke-static {p3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x3

    invoke-static {p4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/location/o/a/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 137
    :cond_0
    iget-object v0, p0, Lcom/google/android/location/k;->a:Lcom/google/android/location/activity/k;

    move v1, p1

    move v2, p2

    move v3, p3

    move-object v4, p5

    move-object v5, p6

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/location/activity/k;->a(IIZLcom/google/android/location/o/n;Ljava/util/Set;)V

    .line 140
    iget-object v0, p0, Lcom/google/android/location/k;->b:Lcom/google/android/location/activity/be;

    if-eqz v0, :cond_1

    .line 141
    iget-object v0, p0, Lcom/google/android/location/k;->b:Lcom/google/android/location/activity/be;

    invoke-virtual {v0, p4}, Lcom/google/android/location/activity/be;->a(Z)V

    .line 143
    :cond_1
    return-void
.end method

.method public final a(Lcom/google/android/gms/location/ActivityRecognitionResult;)V
    .locals 3

    .prologue
    .line 121
    iget-object v0, p0, Lcom/google/android/location/k;->b:Lcom/google/android/location/activity/be;

    if-eqz v0, :cond_0

    .line 122
    iget-object v0, p0, Lcom/google/android/location/k;->b:Lcom/google/android/location/activity/be;

    invoke-virtual {p1}, Lcom/google/android/gms/location/ActivityRecognitionResult;->a()Lcom/google/android/gms/location/DetectedActivity;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/location/DetectedActivity;->a()I

    move-result v1

    const/4 v2, 0x6

    if-eq v1, v2, :cond_0

    iget-object v0, v0, Lcom/google/android/location/activity/be;->d:Lcom/google/android/location/activity/bj;

    invoke-virtual {v0, p1}, Lcom/google/android/location/activity/bj;->a(Lcom/google/android/gms/location/ActivityRecognitionResult;)V

    .line 124
    :cond_0
    return-void
.end method

.method public final a(Lcom/google/android/location/activity/bd;)V
    .locals 1

    .prologue
    .line 113
    iget-object v0, p0, Lcom/google/android/location/k;->a:Lcom/google/android/location/activity/k;

    invoke-virtual {v0, p1}, Lcom/google/android/location/activity/k;->a(Lcom/google/android/location/activity/bd;)V

    .line 114
    return-void
.end method

.method public final a(Lcom/google/android/location/e/ah;)V
    .locals 0

    .prologue
    .line 230
    return-void
.end method

.method public final a(Lcom/google/android/location/e/bh;)V
    .locals 0

    .prologue
    .line 242
    return-void
.end method

.method public final a(Lcom/google/android/location/e/h;)V
    .locals 0

    .prologue
    .line 190
    return-void
.end method

.method public final a(Lcom/google/android/location/j/k;)V
    .locals 5

    .prologue
    .line 87
    sget-boolean v0, Lcom/google/android/location/i/a;->b:Z

    if-eqz v0, :cond_0

    const-string v0, "ActivityProvider"

    const-string v1, "alarmRing. client=%s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-virtual {p1}, Lcom/google/android/location/j/k;->name()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/location/o/a/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 88
    :cond_0
    sget-object v0, Lcom/google/android/location/l;->a:[I

    invoke-virtual {p1}, Lcom/google/android/location/j/k;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 99
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/google/android/location/k;->a:Lcom/google/android/location/activity/k;

    invoke-virtual {v0, p1}, Lcom/google/android/location/activity/k;->a(Lcom/google/android/location/j/k;)V

    .line 100
    return-void

    .line 90
    :pswitch_0
    iget-object v0, p0, Lcom/google/android/location/k;->b:Lcom/google/android/location/activity/be;

    if-eqz v0, :cond_1

    .line 91
    iget-object v0, p0, Lcom/google/android/location/k;->b:Lcom/google/android/location/activity/be;

    invoke-virtual {v0}, Lcom/google/android/location/activity/be;->a()V

    goto :goto_0

    .line 88
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public final a(Lcom/google/android/location/j/k;Lcom/google/android/location/e/b;)V
    .locals 1

    .prologue
    .line 160
    iget-object v0, p0, Lcom/google/android/location/k;->a:Lcom/google/android/location/activity/k;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/location/activity/k;->a(Lcom/google/android/location/j/k;Lcom/google/android/location/e/b;)V

    .line 161
    return-void
.end method

.method public final a(Lcom/google/android/location/os/aw;)V
    .locals 0

    .prologue
    .line 202
    return-void
.end method

.method public final a(Lcom/google/android/location/os/bk;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 238
    return-void
.end method

.method public final a(Lcom/google/p/a/b/b/a;)V
    .locals 0

    .prologue
    .line 194
    return-void
.end method

.method public final a(Z)V
    .locals 1

    .prologue
    .line 108
    iget-object v0, p0, Lcom/google/android/location/k;->a:Lcom/google/android/location/activity/k;

    invoke-virtual {v0, p1}, Lcom/google/android/location/activity/k;->b(Z)V

    .line 109
    return-void
.end method

.method public final a(ZLjava/lang/String;)V
    .locals 0

    .prologue
    .line 234
    return-void
.end method

.method public final a(ZZ)V
    .locals 0

    .prologue
    .line 210
    return-void
.end method

.method public final a(ZZI)V
    .locals 0

    .prologue
    .line 226
    return-void
.end method

.method public final a([Lcom/google/android/location/e/bi;Z)V
    .locals 0

    .prologue
    .line 206
    return-void
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 152
    iget-object v0, p0, Lcom/google/android/location/k;->a:Lcom/google/android/location/activity/k;

    invoke-virtual {v0}, Lcom/google/android/location/activity/k;->H()V

    .line 153
    return-void
.end method

.method public final b(Lcom/google/p/a/b/b/a;)V
    .locals 0

    .prologue
    .line 198
    return-void
.end method

.method public final b(Z)V
    .locals 1

    .prologue
    .line 170
    iget-object v0, p0, Lcom/google/android/location/k;->a:Lcom/google/android/location/activity/k;

    invoke-virtual {v0, p1}, Lcom/google/android/location/activity/k;->c(Z)V

    .line 171
    return-void
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 165
    iget-object v0, p0, Lcom/google/android/location/k;->a:Lcom/google/android/location/activity/k;

    invoke-virtual {v0}, Lcom/google/android/location/activity/k;->F()V

    .line 166
    return-void
.end method

.method public final c(Z)V
    .locals 0

    .prologue
    .line 186
    return-void
.end method

.method public final d()V
    .locals 0

    .prologue
    .line 182
    return-void
.end method

.method public final d(Z)V
    .locals 0

    .prologue
    .line 218
    return-void
.end method

.method public final e()V
    .locals 0

    .prologue
    .line 246
    return-void
.end method

.method public final e(Z)V
    .locals 0

    .prologue
    .line 222
    return-void
.end method
