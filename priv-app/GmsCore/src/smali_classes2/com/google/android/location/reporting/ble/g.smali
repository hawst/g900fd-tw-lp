.class final Lcom/google/android/location/reporting/ble/g;
.super Lcom/google/android/gms/blescanner/compat/v;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/location/reporting/ble/e;


# direct methods
.method constructor <init>(Lcom/google/android/location/reporting/ble/e;)V
    .locals 0

    .prologue
    .line 226
    iput-object p1, p0, Lcom/google/android/location/reporting/ble/g;->a:Lcom/google/android/location/reporting/ble/e;

    invoke-direct {p0}, Lcom/google/android/gms/blescanner/compat/v;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(I)V
    .locals 3

    .prologue
    .line 246
    const/4 v0, 0x5

    if-ne p1, v0, :cond_1

    .line 247
    const-string v0, "GCoreUlr"

    const/4 v1, 0x4

    invoke-static {v0, v1}, Lcom/google/android/location/reporting/b/d;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 248
    const-string v0, "GCoreUlr"

    const-string v1, "Bluetooth disabled; disabling ULR BLE scanner"

    invoke-static {v0, v1}, Lcom/google/android/location/reporting/b/d;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 250
    :cond_0
    iget-object v0, p0, Lcom/google/android/location/reporting/ble/g;->a:Lcom/google/android/location/reporting/ble/e;

    invoke-virtual {v0}, Lcom/google/android/location/reporting/ble/e;->b()V

    .line 254
    :goto_0
    return-void

    .line 252
    :cond_1
    const-string v0, "GCoreUlr"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unable to scan for BLE beacons - errorCode "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/location/reporting/b/d;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final a(ILcom/google/android/gms/blescanner/compat/ScanResult;)V
    .locals 2

    .prologue
    .line 230
    const/4 v0, 0x4

    if-eq p1, v0, :cond_0

    .line 231
    iget-object v0, p0, Lcom/google/android/location/reporting/ble/g;->a:Lcom/google/android/location/reporting/ble/e;

    iget-object v0, v0, Lcom/google/android/location/reporting/ble/e;->d:Lcom/google/android/gms/blescanner/a/c;

    invoke-static {v0, p2}, Lcom/google/android/location/reporting/ble/ParcelableBleScan;->a(Lcom/google/android/gms/blescanner/a/c;Lcom/google/android/gms/blescanner/compat/ScanResult;)Lcom/google/android/location/reporting/ble/ParcelableBleScan;

    move-result-object v0

    .line 232
    if-eqz v0, :cond_0

    .line 233
    iget-object v1, p0, Lcom/google/android/location/reporting/ble/g;->a:Lcom/google/android/location/reporting/ble/e;

    iget-object v1, v1, Lcom/google/android/location/reporting/ble/e;->e:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 236
    :cond_0
    return-void
.end method

.method public final a(Ljava/util/List;)V
    .locals 2

    .prologue
    .line 240
    iget-object v0, p0, Lcom/google/android/location/reporting/ble/g;->a:Lcom/google/android/location/reporting/ble/e;

    iget-object v0, v0, Lcom/google/android/location/reporting/ble/e;->e:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/google/android/location/reporting/ble/g;->a:Lcom/google/android/location/reporting/ble/e;

    iget-object v1, v1, Lcom/google/android/location/reporting/ble/e;->d:Lcom/google/android/gms/blescanner/a/c;

    invoke-static {v1, p1}, Lcom/google/android/location/reporting/ble/ParcelableBleScan;->a(Lcom/google/android/gms/blescanner/a/c;Ljava/util/List;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 241
    iget-object v0, p0, Lcom/google/android/location/reporting/ble/g;->a:Lcom/google/android/location/reporting/ble/e;

    invoke-static {v0}, Lcom/google/android/location/reporting/ble/e;->a(Lcom/google/android/location/reporting/ble/e;)V

    .line 242
    return-void
.end method
