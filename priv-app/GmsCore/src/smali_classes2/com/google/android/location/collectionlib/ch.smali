.class public final Lcom/google/android/location/collectionlib/ch;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/location/collectionlib/ca;


# instance fields
.field private a:Ljava/util/HashMap;

.field private final b:Ljava/util/HashMap;

.field private c:Z

.field private final d:Lcom/google/android/location/collectionlib/ar;


# direct methods
.method public constructor <init>(Lcom/google/android/location/collectionlib/ar;)V
    .locals 1

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/location/collectionlib/ch;->a:Ljava/util/HashMap;

    .line 26
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/location/collectionlib/ch;->b:Ljava/util/HashMap;

    .line 27
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/location/collectionlib/ch;->c:Z

    .line 31
    iput-object p1, p0, Lcom/google/android/location/collectionlib/ch;->d:Lcom/google/android/location/collectionlib/ar;

    .line 32
    return-void
.end method

.method private a(JLcom/google/android/location/collectionlib/cg;[F)V
    .locals 5

    .prologue
    .line 127
    iget-boolean v0, p0, Lcom/google/android/location/collectionlib/ch;->c:Z

    if-eqz v0, :cond_0

    .line 139
    :goto_0
    return-void

    .line 130
    :cond_0
    iget-object v0, p0, Lcom/google/android/location/collectionlib/ch;->b:Ljava/util/HashMap;

    invoke-virtual {p3}, Lcom/google/android/location/collectionlib/cg;->a()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    .line 131
    if-nez v0, :cond_1

    .line 132
    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    .line 133
    iget-object v1, p0, Lcom/google/android/location/collectionlib/ch;->b:Ljava/util/HashMap;

    invoke-virtual {p3}, Lcom/google/android/location/collectionlib/cg;->a()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 135
    :cond_1
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    sub-long v2, p1, v0

    .line 136
    iget-object v0, p0, Lcom/google/android/location/collectionlib/ch;->a:Ljava/util/HashMap;

    invoke-virtual {v0, p3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    if-nez v0, :cond_2

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iget-object v1, p0, Lcom/google/android/location/collectionlib/ch;->a:Ljava/util/HashMap;

    invoke-virtual {v1, p3, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 137
    :cond_2
    new-instance v1, Lcom/google/android/location/d/i;

    invoke-direct {v1, v2, v3, p4}, Lcom/google/android/location/d/i;-><init>(J[F)V

    .line 138
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 36
    iget-boolean v0, p0, Lcom/google/android/location/collectionlib/ch;->c:Z

    if-eqz v0, :cond_0

    .line 41
    :goto_0
    return-void

    .line 39
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/location/collectionlib/ch;->c:Z

    .line 40
    iget-object v0, p0, Lcom/google/android/location/collectionlib/ch;->d:Lcom/google/android/location/collectionlib/ar;

    iget-object v1, p0, Lcom/google/android/location/collectionlib/ch;->a:Ljava/util/HashMap;

    invoke-interface {v0, v1}, Lcom/google/android/location/collectionlib/ar;->a(Ljava/util/Map;)V

    goto :goto_0
.end method

.method public final a(FFFF)V
    .locals 0

    .prologue
    .line 145
    return-void
.end method

.method public final a(FFFFFFIJJ)V
    .locals 4

    .prologue
    .line 93
    sget-object v0, Lcom/google/android/location/collectionlib/cg;->j:Lcom/google/android/location/collectionlib/cg;

    const/4 v1, 0x6

    new-array v1, v1, [F

    const/4 v2, 0x0

    aput p1, v1, v2

    const/4 v2, 0x1

    aput p2, v1, v2

    const/4 v2, 0x2

    aput p3, v1, v2

    const/4 v2, 0x3

    aput p4, v1, v2

    const/4 v2, 0x4

    aput p5, v1, v2

    const/4 v2, 0x5

    aput p6, v1, v2

    invoke-direct {p0, p8, p9, v0, v1}, Lcom/google/android/location/collectionlib/ch;->a(JLcom/google/android/location/collectionlib/cg;[F)V

    .line 95
    return-void
.end method

.method public final a(FFFIJJ)V
    .locals 3

    .prologue
    .line 59
    sget-object v0, Lcom/google/android/location/collectionlib/cg;->c:Lcom/google/android/location/collectionlib/cg;

    const/4 v1, 0x3

    new-array v1, v1, [F

    const/4 v2, 0x0

    aput p1, v1, v2

    const/4 v2, 0x1

    aput p2, v1, v2

    const/4 v2, 0x2

    aput p3, v1, v2

    invoke-direct {p0, p5, p6, v0, v1}, Lcom/google/android/location/collectionlib/ch;->a(JLcom/google/android/location/collectionlib/cg;[F)V

    .line 60
    return-void
.end method

.method public final a(FIJJ)V
    .locals 3

    .prologue
    .line 111
    sget-object v0, Lcom/google/android/location/collectionlib/cg;->i:Lcom/google/android/location/collectionlib/cg;

    const/4 v1, 0x1

    new-array v1, v1, [F

    const/4 v2, 0x0

    aput p1, v1, v2

    invoke-direct {p0, p3, p4, v0, v1}, Lcom/google/android/location/collectionlib/ch;->a(JLcom/google/android/location/collectionlib/cg;[F)V

    .line 112
    return-void
.end method

.method public final a(ILjava/lang/String;Landroid/telephony/CellLocation;ILjava/util/List;J)V
    .locals 0

    .prologue
    .line 206
    return-void
.end method

.method public final a(Landroid/location/GpsMeasurementsEvent;J)V
    .locals 0

    .prologue
    .line 167
    return-void
.end method

.method public final a(Landroid/location/GpsNavigationMessageEvent;J)V
    .locals 0

    .prologue
    .line 172
    return-void
.end method

.method public final a(Landroid/location/GpsStatus;J)V
    .locals 0

    .prologue
    .line 162
    return-void
.end method

.method public final a(Ljava/util/List;J)V
    .locals 0

    .prologue
    .line 196
    return-void
.end method

.method public final a([D[D[D[D[DJ)V
    .locals 0

    .prologue
    .line 191
    return-void
.end method

.method public final a(Landroid/location/Location;J)Z
    .locals 1

    .prologue
    .line 156
    const/4 v0, 0x0

    return v0
.end method

.method public final a(Lcom/google/p/a/b/b/a;)Z
    .locals 1

    .prologue
    .line 219
    const/4 v0, 0x0

    return v0
.end method

.method public final b(FFFIJJ)V
    .locals 3

    .prologue
    .line 69
    sget-object v0, Lcom/google/android/location/collectionlib/cg;->d:Lcom/google/android/location/collectionlib/cg;

    const/4 v1, 0x3

    new-array v1, v1, [F

    const/4 v2, 0x0

    aput p1, v1, v2

    const/4 v2, 0x1

    aput p2, v1, v2

    const/4 v2, 0x2

    aput p3, v1, v2

    invoke-direct {p0, p5, p6, v0, v1}, Lcom/google/android/location/collectionlib/ch;->a(JLcom/google/android/location/collectionlib/cg;[F)V

    .line 70
    return-void
.end method

.method public final b(FIJJ)V
    .locals 3

    .prologue
    .line 117
    sget-object v0, Lcom/google/android/location/collectionlib/cg;->k:Lcom/google/android/location/collectionlib/cg;

    const/4 v1, 0x1

    new-array v1, v1, [F

    const/4 v2, 0x0

    aput p1, v1, v2

    invoke-direct {p0, p3, p4, v0, v1}, Lcom/google/android/location/collectionlib/ch;->a(JLcom/google/android/location/collectionlib/cg;[F)V

    .line 118
    return-void
.end method

.method public final c(FFFIJJ)V
    .locals 3

    .prologue
    .line 79
    sget-object v0, Lcom/google/android/location/collectionlib/cg;->f:Lcom/google/android/location/collectionlib/cg;

    const/4 v1, 0x3

    new-array v1, v1, [F

    const/4 v2, 0x0

    aput p1, v1, v2

    const/4 v2, 0x1

    aput p2, v1, v2

    const/4 v2, 0x2

    aput p3, v1, v2

    invoke-direct {p0, p5, p6, v0, v1}, Lcom/google/android/location/collectionlib/ch;->a(JLcom/google/android/location/collectionlib/cg;[F)V

    .line 81
    return-void
.end method

.method public final c(FIJJ)V
    .locals 3

    .prologue
    .line 123
    sget-object v0, Lcom/google/android/location/collectionlib/cg;->l:Lcom/google/android/location/collectionlib/cg;

    const/4 v1, 0x1

    new-array v1, v1, [F

    const/4 v2, 0x0

    aput p1, v1, v2

    invoke-direct {p0, p3, p4, v0, v1}, Lcom/google/android/location/collectionlib/ch;->a(JLcom/google/android/location/collectionlib/cg;[F)V

    .line 124
    return-void
.end method

.method public final d(FFFIJJ)V
    .locals 3

    .prologue
    .line 104
    sget-object v0, Lcom/google/android/location/collectionlib/cg;->e:Lcom/google/android/location/collectionlib/cg;

    const/4 v1, 0x3

    new-array v1, v1, [F

    const/4 v2, 0x0

    aput p1, v1, v2

    const/4 v2, 0x1

    aput p2, v1, v2

    const/4 v2, 0x2

    aput p3, v1, v2

    invoke-direct {p0, p5, p6, v0, v1}, Lcom/google/android/location/collectionlib/ch;->a(JLcom/google/android/location/collectionlib/cg;[F)V

    .line 106
    return-void
.end method

.method public final d(FIJJ)V
    .locals 0

    .prologue
    .line 184
    return-void
.end method

.method public final e(FIJJ)V
    .locals 0

    .prologue
    .line 178
    return-void
.end method
