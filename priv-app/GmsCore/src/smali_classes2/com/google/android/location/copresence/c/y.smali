.class public final Lcom/google/android/location/copresence/c/y;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final a:Lcom/google/android/gms/audiomodem/bf;

.field b:Lcom/google/android/gms/audiomodem/TokenBroadcaster$Params;

.field c:Z

.field final d:Lcom/google/android/location/copresence/c/b;


# direct methods
.method public constructor <init>(Lcom/google/android/location/copresence/c/b;Lcom/google/android/gms/audiomodem/bf;)V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    iput-object p2, p0, Lcom/google/android/location/copresence/c/y;->a:Lcom/google/android/gms/audiomodem/bf;

    .line 27
    iput-object p1, p0, Lcom/google/android/location/copresence/c/y;->d:Lcom/google/android/location/copresence/c/b;

    .line 28
    return-void
.end method


# virtual methods
.method final a()V
    .locals 2

    .prologue
    const/4 v1, 0x3

    .line 40
    iget-boolean v0, p0, Lcom/google/android/location/copresence/c/y;->c:Z

    if-nez v0, :cond_1

    .line 41
    invoke-static {v1}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 42
    const-string v0, "Audio Stack: JustBroadcastManager: already stopped"

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->b(Ljava/lang/String;)I

    .line 51
    :cond_0
    :goto_0
    return-void

    .line 46
    :cond_1
    invoke-static {v1}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 47
    const-string v0, "Audio Stack: JustBroadcastManager: stopping broadcasting"

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->b(Ljava/lang/String;)I

    .line 49
    :cond_2
    iget-object v0, p0, Lcom/google/android/location/copresence/c/y;->d:Lcom/google/android/location/copresence/c/b;

    iget-object v1, p0, Lcom/google/android/location/copresence/c/y;->a:Lcom/google/android/gms/audiomodem/bf;

    invoke-virtual {v0, v1}, Lcom/google/android/location/copresence/c/b;->a(Lcom/google/android/gms/audiomodem/bf;)V

    .line 50
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/location/copresence/c/y;->c:Z

    goto :goto_0
.end method
