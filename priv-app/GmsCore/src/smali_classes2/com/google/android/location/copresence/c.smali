.class public final Lcom/google/android/location/copresence/c;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static i:Lcom/google/android/location/copresence/c;


# instance fields
.field final a:Ljava/util/Map;

.field public final b:Ljava/util/Map;

.field final c:J

.field public final d:Lcom/google/android/location/copresence/ap;

.field private final e:Landroid/app/AlarmManager;

.field private final f:Landroid/content/Context;

.field private final g:Ljava/lang/String;

.field private h:I

.field private final j:Landroid/content/BroadcastReceiver;


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 83
    invoke-static {p1}, Lcom/google/android/location/copresence/ap;->a(Landroid/content/Context;)Lcom/google/android/location/copresence/ap;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lcom/google/android/location/copresence/c;-><init>(Landroid/content/Context;Lcom/google/android/location/copresence/ap;)V

    .line 84
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Lcom/google/android/location/copresence/ap;)V
    .locals 4

    .prologue
    .line 63
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/location/copresence/c;->a:Ljava/util/Map;

    .line 41
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/location/copresence/c;->b:Ljava/util/Map;

    .line 47
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/location/copresence/c;->h:I

    .line 159
    new-instance v0, Lcom/google/android/location/copresence/d;

    invoke-direct {v0, p0}, Lcom/google/android/location/copresence/d;-><init>(Lcom/google/android/location/copresence/c;)V

    iput-object v0, p0, Lcom/google/android/location/copresence/c;->j:Landroid/content/BroadcastReceiver;

    .line 64
    const-string v0, "alarm"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    iput-object v0, p0, Lcom/google/android/location/copresence/c;->e:Landroid/app/AlarmManager;

    .line 65
    iput-object p1, p0, Lcom/google/android/location/copresence/c;->f:Landroid/content/Context;

    .line 66
    iput-object p2, p0, Lcom/google/android/location/copresence/c;->d:Lcom/google/android/location/copresence/ap;

    .line 71
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/location/copresence/c;->c:J

    .line 73
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "com.google.android.location.copresence.ALARM_WAKEUP_COPRESENCE"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v2, p0, Lcom/google/android/location/copresence/c;->c:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/copresence/c;->g:Ljava/lang/String;

    .line 75
    new-instance v0, Landroid/content/IntentFilter;

    iget-object v1, p0, Lcom/google/android/location/copresence/c;->g:Ljava/lang/String;

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 79
    iget-object v1, p0, Lcom/google/android/location/copresence/c;->j:Landroid/content/BroadcastReceiver;

    invoke-virtual {p1, v1, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 80
    return-void
.end method

.method public static declared-synchronized a(Landroid/content/Context;)Lcom/google/android/location/copresence/c;
    .locals 2

    .prologue
    .line 52
    const-class v1, Lcom/google/android/location/copresence/c;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/google/android/location/copresence/c;->i:Lcom/google/android/location/copresence/c;

    if-nez v0, :cond_0

    .line 53
    new-instance v0, Lcom/google/android/location/copresence/c;

    invoke-direct {v0, p0}, Lcom/google/android/location/copresence/c;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/google/android/location/copresence/c;->i:Lcom/google/android/location/copresence/c;

    .line 55
    :cond_0
    sget-object v0, Lcom/google/android/location/copresence/c;->i:Lcom/google/android/location/copresence/c;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 52
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final a(Ljava/lang/Runnable;)V
    .locals 3

    .prologue
    .line 131
    iget-object v0, p0, Lcom/google/android/location/copresence/c;->d:Lcom/google/android/location/copresence/ap;

    invoke-virtual {v0}, Lcom/google/android/location/copresence/ap;->c()V

    .line 133
    iget-object v0, p0, Lcom/google/android/location/copresence/c;->b:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/Pair;

    .line 134
    if-eqz v0, :cond_0

    .line 135
    iget-object v1, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 136
    iget-object v0, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Landroid/app/PendingIntent;

    .line 137
    iget-object v2, p0, Lcom/google/android/location/copresence/c;->e:Landroid/app/AlarmManager;

    invoke-virtual {v2, v0}, Landroid/app/AlarmManager;->cancel(Landroid/app/PendingIntent;)V

    .line 138
    iget-object v0, p0, Lcom/google/android/location/copresence/c;->b:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 139
    iget-object v0, p0, Lcom/google/android/location/copresence/c;->a:Ljava/util/Map;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 140
    const/4 v0, 0x3

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 141
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "Canceled alarm: id="

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " runnable="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->b(Ljava/lang/String;)I

    .line 144
    :cond_0
    return-void
.end method

.method public final a(Ljava/lang/Runnable;J)V
    .locals 8

    .prologue
    .line 99
    iget-object v0, p0, Lcom/google/android/location/copresence/c;->d:Lcom/google/android/location/copresence/ap;

    invoke-virtual {v0}, Lcom/google/android/location/copresence/ap;->c()V

    .line 105
    invoke-virtual {p0, p1}, Lcom/google/android/location/copresence/c;->a(Ljava/lang/Runnable;)V

    .line 106
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    add-long/2addr v0, p2

    .line 107
    iget v2, p0, Lcom/google/android/location/copresence/c;->h:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/google/android/location/copresence/c;->h:I

    iget v2, p0, Lcom/google/android/location/copresence/c;->h:I

    .line 108
    new-instance v3, Landroid/content/Intent;

    iget-object v4, p0, Lcom/google/android/location/copresence/c;->g:Ljava/lang/String;

    invoke-direct {v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 109
    const-string v4, "ALARM_ID_EXTRA"

    invoke-virtual {v3, v4, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 110
    const-string v4, "SESSION_ID_EXTRA"

    iget-wide v6, p0, Lcom/google/android/location/copresence/c;->c:J

    invoke-virtual {v3, v4, v6, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 111
    iget-object v4, p0, Lcom/google/android/location/copresence/c;->f:Landroid/content/Context;

    const/4 v5, 0x0

    invoke-static {v4, v2, v3, v5}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v3

    .line 115
    iget-object v4, p0, Lcom/google/android/location/copresence/c;->a:Ljava/util/Map;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v4, v5, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 116
    iget-object v4, p0, Lcom/google/android/location/copresence/c;->b:Ljava/util/Map;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-static {v5, v3}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v5

    invoke-interface {v4, p1, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 117
    const/4 v4, 0x3

    invoke-static {v4}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 118
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Setting alarm: id="

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, " delay="

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, " runnable="

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/location/copresence/ag;->b(Ljava/lang/String;)I

    .line 121
    :cond_0
    iget-object v2, p0, Lcom/google/android/location/copresence/c;->e:Landroid/app/AlarmManager;

    const/4 v4, 0x2

    invoke-virtual {v2, v4, v0, v1, v3}, Landroid/app/AlarmManager;->set(IJLandroid/app/PendingIntent;)V

    .line 122
    return-void
.end method
