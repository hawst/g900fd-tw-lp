.class public final enum Lcom/google/android/location/d/c;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field public static final enum a:Lcom/google/android/location/d/c;

.field public static final enum b:Lcom/google/android/location/d/c;

.field public static final enum c:Lcom/google/android/location/d/c;

.field public static final enum d:Lcom/google/android/location/d/c;

.field public static final enum e:Lcom/google/android/location/d/c;

.field public static final enum f:Lcom/google/android/location/d/c;

.field public static final enum g:Lcom/google/android/location/d/c;

.field public static final enum h:Lcom/google/android/location/d/c;

.field private static final synthetic i:[Lcom/google/android/location/d/c;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 205
    new-instance v0, Lcom/google/android/location/d/c;

    const-string v1, "LOCATION_ACTIVE_ON"

    invoke-direct {v0, v1, v3}, Lcom/google/android/location/d/c;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/location/d/c;->a:Lcom/google/android/location/d/c;

    .line 206
    new-instance v0, Lcom/google/android/location/d/c;

    const-string v1, "LOCATION_ACTIVE_OFF"

    invoke-direct {v0, v1, v4}, Lcom/google/android/location/d/c;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/location/d/c;->b:Lcom/google/android/location/d/c;

    .line 207
    new-instance v0, Lcom/google/android/location/d/c;

    const-string v1, "SATELLITE_STATUS_ON"

    invoke-direct {v0, v1, v5}, Lcom/google/android/location/d/c;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/location/d/c;->c:Lcom/google/android/location/d/c;

    .line 208
    new-instance v0, Lcom/google/android/location/d/c;

    const-string v1, "SATELLITE_STATUS_OFF"

    invoke-direct {v0, v1, v6}, Lcom/google/android/location/d/c;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/location/d/c;->d:Lcom/google/android/location/d/c;

    .line 209
    new-instance v0, Lcom/google/android/location/d/c;

    const-string v1, "MEASUREMENTS_STATUS_ON"

    invoke-direct {v0, v1, v7}, Lcom/google/android/location/d/c;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/location/d/c;->e:Lcom/google/android/location/d/c;

    .line 210
    new-instance v0, Lcom/google/android/location/d/c;

    const-string v1, "MEASUREMENTS_STATUS_OFF"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/google/android/location/d/c;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/location/d/c;->f:Lcom/google/android/location/d/c;

    .line 211
    new-instance v0, Lcom/google/android/location/d/c;

    const-string v1, "NAV_MESSAGES_STATUS_ON"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/google/android/location/d/c;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/location/d/c;->g:Lcom/google/android/location/d/c;

    .line 212
    new-instance v0, Lcom/google/android/location/d/c;

    const-string v1, "NAV_MESSAGES_STATUS_OFF"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/google/android/location/d/c;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/location/d/c;->h:Lcom/google/android/location/d/c;

    .line 204
    const/16 v0, 0x8

    new-array v0, v0, [Lcom/google/android/location/d/c;

    sget-object v1, Lcom/google/android/location/d/c;->a:Lcom/google/android/location/d/c;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/android/location/d/c;->b:Lcom/google/android/location/d/c;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/android/location/d/c;->c:Lcom/google/android/location/d/c;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/android/location/d/c;->d:Lcom/google/android/location/d/c;

    aput-object v1, v0, v6

    sget-object v1, Lcom/google/android/location/d/c;->e:Lcom/google/android/location/d/c;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/google/android/location/d/c;->f:Lcom/google/android/location/d/c;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/google/android/location/d/c;->g:Lcom/google/android/location/d/c;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/google/android/location/d/c;->h:Lcom/google/android/location/d/c;

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/location/d/c;->i:[Lcom/google/android/location/d/c;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 204
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/location/d/c;
    .locals 1

    .prologue
    .line 204
    const-class v0, Lcom/google/android/location/d/c;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/d/c;

    return-object v0
.end method

.method public static values()[Lcom/google/android/location/d/c;
    .locals 1

    .prologue
    .line 204
    sget-object v0, Lcom/google/android/location/d/c;->i:[Lcom/google/android/location/d/c;

    invoke-virtual {v0}, [Lcom/google/android/location/d/c;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/location/d/c;

    return-object v0
.end method
