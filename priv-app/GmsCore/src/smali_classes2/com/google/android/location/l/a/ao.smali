.class public final Lcom/google/android/location/l/a/ao;
.super Lcom/google/protobuf/nano/d;
.source "SourceFile"


# static fields
.field private static volatile l:[Lcom/google/android/location/l/a/ao;


# instance fields
.field public a:Ljava/lang/Float;

.field public b:Ljava/lang/Float;

.field public c:Ljava/lang/Float;

.field public d:Ljava/lang/Integer;

.field public e:Ljava/lang/Integer;

.field public f:Ljava/lang/Integer;

.field public g:Ljava/lang/Float;

.field public h:Ljava/lang/Integer;

.field public i:Ljava/lang/Integer;

.field public j:Ljava/lang/Long;

.field public k:Ljava/lang/Long;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 56
    invoke-direct {p0}, Lcom/google/protobuf/nano/d;-><init>()V

    .line 57
    iput-object v0, p0, Lcom/google/android/location/l/a/ao;->a:Ljava/lang/Float;

    iput-object v0, p0, Lcom/google/android/location/l/a/ao;->b:Ljava/lang/Float;

    iput-object v0, p0, Lcom/google/android/location/l/a/ao;->c:Ljava/lang/Float;

    iput-object v0, p0, Lcom/google/android/location/l/a/ao;->d:Ljava/lang/Integer;

    iput-object v0, p0, Lcom/google/android/location/l/a/ao;->e:Ljava/lang/Integer;

    iput-object v0, p0, Lcom/google/android/location/l/a/ao;->f:Ljava/lang/Integer;

    iput-object v0, p0, Lcom/google/android/location/l/a/ao;->g:Ljava/lang/Float;

    iput-object v0, p0, Lcom/google/android/location/l/a/ao;->h:Ljava/lang/Integer;

    iput-object v0, p0, Lcom/google/android/location/l/a/ao;->i:Ljava/lang/Integer;

    iput-object v0, p0, Lcom/google/android/location/l/a/ao;->j:Ljava/lang/Long;

    iput-object v0, p0, Lcom/google/android/location/l/a/ao;->k:Ljava/lang/Long;

    iput-object v0, p0, Lcom/google/android/location/l/a/ao;->unknownFieldData:Lcom/google/protobuf/nano/f;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/location/l/a/ao;->cachedSize:I

    .line 58
    return-void
.end method

.method public static a()[Lcom/google/android/location/l/a/ao;
    .locals 2

    .prologue
    .line 12
    sget-object v0, Lcom/google/android/location/l/a/ao;->l:[Lcom/google/android/location/l/a/ao;

    if-nez v0, :cond_1

    .line 13
    sget-object v1, Lcom/google/protobuf/nano/h;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 15
    :try_start_0
    sget-object v0, Lcom/google/android/location/l/a/ao;->l:[Lcom/google/android/location/l/a/ao;

    if-nez v0, :cond_0

    .line 16
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/android/location/l/a/ao;

    sput-object v0, Lcom/google/android/location/l/a/ao;->l:[Lcom/google/android/location/l/a/ao;

    .line 18
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 20
    :cond_1
    sget-object v0, Lcom/google/android/location/l/a/ao;->l:[Lcom/google/android/location/l/a/ao;

    return-object v0

    .line 18
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 4

    .prologue
    .line 236
    invoke-super {p0}, Lcom/google/protobuf/nano/d;->computeSerializedSize()I

    move-result v0

    .line 237
    iget-object v1, p0, Lcom/google/android/location/l/a/ao;->a:Ljava/lang/Float;

    if-eqz v1, :cond_0

    .line 238
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/location/l/a/ao;->a:Ljava/lang/Float;

    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    .line 241
    :cond_0
    iget-object v1, p0, Lcom/google/android/location/l/a/ao;->b:Ljava/lang/Float;

    if-eqz v1, :cond_1

    .line 242
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/location/l/a/ao;->b:Ljava/lang/Float;

    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    .line 245
    :cond_1
    iget-object v1, p0, Lcom/google/android/location/l/a/ao;->c:Ljava/lang/Float;

    if-eqz v1, :cond_2

    .line 246
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/android/location/l/a/ao;->c:Ljava/lang/Float;

    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    .line 249
    :cond_2
    iget-object v1, p0, Lcom/google/android/location/l/a/ao;->d:Ljava/lang/Integer;

    if-eqz v1, :cond_3

    .line 250
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/android/location/l/a/ao;->d:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 253
    :cond_3
    iget-object v1, p0, Lcom/google/android/location/l/a/ao;->e:Ljava/lang/Integer;

    if-eqz v1, :cond_4

    .line 254
    const/4 v1, 0x5

    iget-object v2, p0, Lcom/google/android/location/l/a/ao;->e:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 257
    :cond_4
    iget-object v1, p0, Lcom/google/android/location/l/a/ao;->f:Ljava/lang/Integer;

    if-eqz v1, :cond_5

    .line 258
    const/4 v1, 0x6

    iget-object v2, p0, Lcom/google/android/location/l/a/ao;->f:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 261
    :cond_5
    iget-object v1, p0, Lcom/google/android/location/l/a/ao;->g:Ljava/lang/Float;

    if-eqz v1, :cond_6

    .line 262
    const/4 v1, 0x7

    iget-object v2, p0, Lcom/google/android/location/l/a/ao;->g:Ljava/lang/Float;

    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    .line 265
    :cond_6
    iget-object v1, p0, Lcom/google/android/location/l/a/ao;->h:Ljava/lang/Integer;

    if-eqz v1, :cond_7

    .line 266
    const/16 v1, 0x8

    iget-object v2, p0, Lcom/google/android/location/l/a/ao;->h:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 269
    :cond_7
    iget-object v1, p0, Lcom/google/android/location/l/a/ao;->i:Ljava/lang/Integer;

    if-eqz v1, :cond_8

    .line 270
    const/16 v1, 0x9

    iget-object v2, p0, Lcom/google/android/location/l/a/ao;->i:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 273
    :cond_8
    iget-object v1, p0, Lcom/google/android/location/l/a/ao;->j:Ljava/lang/Long;

    if-eqz v1, :cond_9

    .line 274
    const/16 v1, 0xa

    iget-object v2, p0, Lcom/google/android/location/l/a/ao;->j:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/b;->f(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 277
    :cond_9
    iget-object v1, p0, Lcom/google/android/location/l/a/ao;->k:Ljava/lang/Long;

    if-eqz v1, :cond_a

    .line 278
    const/16 v1, 0xb

    iget-object v2, p0, Lcom/google/android/location/l/a/ao;->k:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/b;->f(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 281
    :cond_a
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 79
    if-ne p1, p0, :cond_1

    .line 80
    const/4 v0, 0x1

    .line 163
    :cond_0
    :goto_0
    return v0

    .line 82
    :cond_1
    instance-of v1, p1, Lcom/google/android/location/l/a/ao;

    if-eqz v1, :cond_0

    .line 85
    check-cast p1, Lcom/google/android/location/l/a/ao;

    .line 86
    iget-object v1, p0, Lcom/google/android/location/l/a/ao;->a:Ljava/lang/Float;

    if-nez v1, :cond_d

    .line 87
    iget-object v1, p1, Lcom/google/android/location/l/a/ao;->a:Ljava/lang/Float;

    if-nez v1, :cond_0

    .line 93
    :cond_2
    iget-object v1, p0, Lcom/google/android/location/l/a/ao;->b:Ljava/lang/Float;

    if-nez v1, :cond_e

    .line 94
    iget-object v1, p1, Lcom/google/android/location/l/a/ao;->b:Ljava/lang/Float;

    if-nez v1, :cond_0

    .line 100
    :cond_3
    iget-object v1, p0, Lcom/google/android/location/l/a/ao;->c:Ljava/lang/Float;

    if-nez v1, :cond_f

    .line 101
    iget-object v1, p1, Lcom/google/android/location/l/a/ao;->c:Ljava/lang/Float;

    if-nez v1, :cond_0

    .line 107
    :cond_4
    iget-object v1, p0, Lcom/google/android/location/l/a/ao;->d:Ljava/lang/Integer;

    if-nez v1, :cond_10

    .line 108
    iget-object v1, p1, Lcom/google/android/location/l/a/ao;->d:Ljava/lang/Integer;

    if-nez v1, :cond_0

    .line 114
    :cond_5
    iget-object v1, p0, Lcom/google/android/location/l/a/ao;->e:Ljava/lang/Integer;

    if-nez v1, :cond_11

    .line 115
    iget-object v1, p1, Lcom/google/android/location/l/a/ao;->e:Ljava/lang/Integer;

    if-nez v1, :cond_0

    .line 121
    :cond_6
    iget-object v1, p0, Lcom/google/android/location/l/a/ao;->f:Ljava/lang/Integer;

    if-nez v1, :cond_12

    .line 122
    iget-object v1, p1, Lcom/google/android/location/l/a/ao;->f:Ljava/lang/Integer;

    if-nez v1, :cond_0

    .line 128
    :cond_7
    iget-object v1, p0, Lcom/google/android/location/l/a/ao;->g:Ljava/lang/Float;

    if-nez v1, :cond_13

    .line 129
    iget-object v1, p1, Lcom/google/android/location/l/a/ao;->g:Ljava/lang/Float;

    if-nez v1, :cond_0

    .line 135
    :cond_8
    iget-object v1, p0, Lcom/google/android/location/l/a/ao;->h:Ljava/lang/Integer;

    if-nez v1, :cond_14

    .line 136
    iget-object v1, p1, Lcom/google/android/location/l/a/ao;->h:Ljava/lang/Integer;

    if-nez v1, :cond_0

    .line 142
    :cond_9
    iget-object v1, p0, Lcom/google/android/location/l/a/ao;->i:Ljava/lang/Integer;

    if-nez v1, :cond_15

    .line 143
    iget-object v1, p1, Lcom/google/android/location/l/a/ao;->i:Ljava/lang/Integer;

    if-nez v1, :cond_0

    .line 149
    :cond_a
    iget-object v1, p0, Lcom/google/android/location/l/a/ao;->j:Ljava/lang/Long;

    if-nez v1, :cond_16

    .line 150
    iget-object v1, p1, Lcom/google/android/location/l/a/ao;->j:Ljava/lang/Long;

    if-nez v1, :cond_0

    .line 156
    :cond_b
    iget-object v1, p0, Lcom/google/android/location/l/a/ao;->k:Ljava/lang/Long;

    if-nez v1, :cond_17

    .line 157
    iget-object v1, p1, Lcom/google/android/location/l/a/ao;->k:Ljava/lang/Long;

    if-nez v1, :cond_0

    .line 163
    :cond_c
    invoke-virtual {p0, p1}, Lcom/google/android/location/l/a/ao;->unknownFieldDataEquals(Lcom/google/protobuf/nano/d;)Z

    move-result v0

    goto :goto_0

    .line 90
    :cond_d
    iget-object v1, p0, Lcom/google/android/location/l/a/ao;->a:Ljava/lang/Float;

    iget-object v2, p1, Lcom/google/android/location/l/a/ao;->a:Ljava/lang/Float;

    invoke-virtual {v1, v2}, Ljava/lang/Float;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    goto :goto_0

    .line 97
    :cond_e
    iget-object v1, p0, Lcom/google/android/location/l/a/ao;->b:Ljava/lang/Float;

    iget-object v2, p1, Lcom/google/android/location/l/a/ao;->b:Ljava/lang/Float;

    invoke-virtual {v1, v2}, Ljava/lang/Float;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    goto :goto_0

    .line 104
    :cond_f
    iget-object v1, p0, Lcom/google/android/location/l/a/ao;->c:Ljava/lang/Float;

    iget-object v2, p1, Lcom/google/android/location/l/a/ao;->c:Ljava/lang/Float;

    invoke-virtual {v1, v2}, Ljava/lang/Float;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    goto/16 :goto_0

    .line 111
    :cond_10
    iget-object v1, p0, Lcom/google/android/location/l/a/ao;->d:Ljava/lang/Integer;

    iget-object v2, p1, Lcom/google/android/location/l/a/ao;->d:Ljava/lang/Integer;

    invoke-virtual {v1, v2}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_5

    goto/16 :goto_0

    .line 118
    :cond_11
    iget-object v1, p0, Lcom/google/android/location/l/a/ao;->e:Ljava/lang/Integer;

    iget-object v2, p1, Lcom/google/android/location/l/a/ao;->e:Ljava/lang/Integer;

    invoke-virtual {v1, v2}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_6

    goto/16 :goto_0

    .line 125
    :cond_12
    iget-object v1, p0, Lcom/google/android/location/l/a/ao;->f:Ljava/lang/Integer;

    iget-object v2, p1, Lcom/google/android/location/l/a/ao;->f:Ljava/lang/Integer;

    invoke-virtual {v1, v2}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_7

    goto/16 :goto_0

    .line 132
    :cond_13
    iget-object v1, p0, Lcom/google/android/location/l/a/ao;->g:Ljava/lang/Float;

    iget-object v2, p1, Lcom/google/android/location/l/a/ao;->g:Ljava/lang/Float;

    invoke-virtual {v1, v2}, Ljava/lang/Float;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_8

    goto/16 :goto_0

    .line 139
    :cond_14
    iget-object v1, p0, Lcom/google/android/location/l/a/ao;->h:Ljava/lang/Integer;

    iget-object v2, p1, Lcom/google/android/location/l/a/ao;->h:Ljava/lang/Integer;

    invoke-virtual {v1, v2}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_9

    goto/16 :goto_0

    .line 146
    :cond_15
    iget-object v1, p0, Lcom/google/android/location/l/a/ao;->i:Ljava/lang/Integer;

    iget-object v2, p1, Lcom/google/android/location/l/a/ao;->i:Ljava/lang/Integer;

    invoke-virtual {v1, v2}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_a

    goto/16 :goto_0

    .line 153
    :cond_16
    iget-object v1, p0, Lcom/google/android/location/l/a/ao;->j:Ljava/lang/Long;

    iget-object v2, p1, Lcom/google/android/location/l/a/ao;->j:Ljava/lang/Long;

    invoke-virtual {v1, v2}, Ljava/lang/Long;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_b

    goto/16 :goto_0

    .line 160
    :cond_17
    iget-object v1, p0, Lcom/google/android/location/l/a/ao;->k:Ljava/lang/Long;

    iget-object v2, p1, Lcom/google/android/location/l/a/ao;->k:Ljava/lang/Long;

    invoke-virtual {v1, v2}, Ljava/lang/Long;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_c

    goto/16 :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 168
    iget-object v0, p0, Lcom/google/android/location/l/a/ao;->a:Ljava/lang/Float;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    .line 171
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/location/l/a/ao;->b:Ljava/lang/Float;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    .line 173
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/location/l/a/ao;->c:Ljava/lang/Float;

    if-nez v0, :cond_2

    move v0, v1

    :goto_2
    add-int/2addr v0, v2

    .line 175
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/location/l/a/ao;->d:Ljava/lang/Integer;

    if-nez v0, :cond_3

    move v0, v1

    :goto_3
    add-int/2addr v0, v2

    .line 177
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/location/l/a/ao;->e:Ljava/lang/Integer;

    if-nez v0, :cond_4

    move v0, v1

    :goto_4
    add-int/2addr v0, v2

    .line 179
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/location/l/a/ao;->f:Ljava/lang/Integer;

    if-nez v0, :cond_5

    move v0, v1

    :goto_5
    add-int/2addr v0, v2

    .line 181
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/location/l/a/ao;->g:Ljava/lang/Float;

    if-nez v0, :cond_6

    move v0, v1

    :goto_6
    add-int/2addr v0, v2

    .line 183
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/location/l/a/ao;->h:Ljava/lang/Integer;

    if-nez v0, :cond_7

    move v0, v1

    :goto_7
    add-int/2addr v0, v2

    .line 185
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/location/l/a/ao;->i:Ljava/lang/Integer;

    if-nez v0, :cond_8

    move v0, v1

    :goto_8
    add-int/2addr v0, v2

    .line 187
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/location/l/a/ao;->j:Ljava/lang/Long;

    if-nez v0, :cond_9

    move v0, v1

    :goto_9
    add-int/2addr v0, v2

    .line 189
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/android/location/l/a/ao;->k:Ljava/lang/Long;

    if-nez v2, :cond_a

    :goto_a
    add-int/2addr v0, v1

    .line 191
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/google/android/location/l/a/ao;->unknownFieldDataHashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 192
    return v0

    .line 168
    :cond_0
    iget-object v0, p0, Lcom/google/android/location/l/a/ao;->a:Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->hashCode()I

    move-result v0

    goto :goto_0

    .line 171
    :cond_1
    iget-object v0, p0, Lcom/google/android/location/l/a/ao;->b:Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->hashCode()I

    move-result v0

    goto :goto_1

    .line 173
    :cond_2
    iget-object v0, p0, Lcom/google/android/location/l/a/ao;->c:Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->hashCode()I

    move-result v0

    goto :goto_2

    .line 175
    :cond_3
    iget-object v0, p0, Lcom/google/android/location/l/a/ao;->d:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->hashCode()I

    move-result v0

    goto :goto_3

    .line 177
    :cond_4
    iget-object v0, p0, Lcom/google/android/location/l/a/ao;->e:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->hashCode()I

    move-result v0

    goto :goto_4

    .line 179
    :cond_5
    iget-object v0, p0, Lcom/google/android/location/l/a/ao;->f:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->hashCode()I

    move-result v0

    goto :goto_5

    .line 181
    :cond_6
    iget-object v0, p0, Lcom/google/android/location/l/a/ao;->g:Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->hashCode()I

    move-result v0

    goto :goto_6

    .line 183
    :cond_7
    iget-object v0, p0, Lcom/google/android/location/l/a/ao;->h:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->hashCode()I

    move-result v0

    goto :goto_7

    .line 185
    :cond_8
    iget-object v0, p0, Lcom/google/android/location/l/a/ao;->i:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->hashCode()I

    move-result v0

    goto :goto_8

    .line 187
    :cond_9
    iget-object v0, p0, Lcom/google/android/location/l/a/ao;->j:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->hashCode()I

    move-result v0

    goto :goto_9

    .line 189
    :cond_a
    iget-object v1, p0, Lcom/google/android/location/l/a/ao;->k:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->hashCode()I

    move-result v1

    goto :goto_a
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 2

    .prologue
    .line 5
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/android/location/l/a/ao;->storeUnknownField(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->k()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->intBitsToFloat(I)F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/l/a/ao;->a:Ljava/lang/Float;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->k()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->intBitsToFloat(I)F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/l/a/ao;->b:Ljava/lang/Float;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->k()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->intBitsToFloat(I)F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/l/a/ao;->c:Ljava/lang/Float;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/l/a/ao;->d:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/l/a/ao;->e:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/l/a/ao;->f:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_7
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->k()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->intBitsToFloat(I)F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/l/a/ao;->g:Ljava/lang/Float;

    goto :goto_0

    :sswitch_8
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/l/a/ao;->h:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_9
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/l/a/ao;->i:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_a
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->j()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/l/a/ao;->j:Ljava/lang/Long;

    goto/16 :goto_0

    :sswitch_b
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->j()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/l/a/ao;->k:Ljava/lang/Long;

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xd -> :sswitch_1
        0x15 -> :sswitch_2
        0x1d -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
        0x30 -> :sswitch_6
        0x3d -> :sswitch_7
        0x40 -> :sswitch_8
        0x48 -> :sswitch_9
        0x50 -> :sswitch_a
        0x58 -> :sswitch_b
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 4

    .prologue
    .line 198
    iget-object v0, p0, Lcom/google/android/location/l/a/ao;->a:Ljava/lang/Float;

    if-eqz v0, :cond_0

    .line 199
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/location/l/a/ao;->a:Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IF)V

    .line 201
    :cond_0
    iget-object v0, p0, Lcom/google/android/location/l/a/ao;->b:Ljava/lang/Float;

    if-eqz v0, :cond_1

    .line 202
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/android/location/l/a/ao;->b:Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IF)V

    .line 204
    :cond_1
    iget-object v0, p0, Lcom/google/android/location/l/a/ao;->c:Ljava/lang/Float;

    if-eqz v0, :cond_2

    .line 205
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/android/location/l/a/ao;->c:Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IF)V

    .line 207
    :cond_2
    iget-object v0, p0, Lcom/google/android/location/l/a/ao;->d:Ljava/lang/Integer;

    if-eqz v0, :cond_3

    .line 208
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/android/location/l/a/ao;->d:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 210
    :cond_3
    iget-object v0, p0, Lcom/google/android/location/l/a/ao;->e:Ljava/lang/Integer;

    if-eqz v0, :cond_4

    .line 211
    const/4 v0, 0x5

    iget-object v1, p0, Lcom/google/android/location/l/a/ao;->e:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 213
    :cond_4
    iget-object v0, p0, Lcom/google/android/location/l/a/ao;->f:Ljava/lang/Integer;

    if-eqz v0, :cond_5

    .line 214
    const/4 v0, 0x6

    iget-object v1, p0, Lcom/google/android/location/l/a/ao;->f:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 216
    :cond_5
    iget-object v0, p0, Lcom/google/android/location/l/a/ao;->g:Ljava/lang/Float;

    if-eqz v0, :cond_6

    .line 217
    const/4 v0, 0x7

    iget-object v1, p0, Lcom/google/android/location/l/a/ao;->g:Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IF)V

    .line 219
    :cond_6
    iget-object v0, p0, Lcom/google/android/location/l/a/ao;->h:Ljava/lang/Integer;

    if-eqz v0, :cond_7

    .line 220
    const/16 v0, 0x8

    iget-object v1, p0, Lcom/google/android/location/l/a/ao;->h:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 222
    :cond_7
    iget-object v0, p0, Lcom/google/android/location/l/a/ao;->i:Ljava/lang/Integer;

    if-eqz v0, :cond_8

    .line 223
    const/16 v0, 0x9

    iget-object v1, p0, Lcom/google/android/location/l/a/ao;->i:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 225
    :cond_8
    iget-object v0, p0, Lcom/google/android/location/l/a/ao;->j:Ljava/lang/Long;

    if-eqz v0, :cond_9

    .line 226
    const/16 v0, 0xa

    iget-object v1, p0, Lcom/google/android/location/l/a/ao;->j:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/b;->b(IJ)V

    .line 228
    :cond_9
    iget-object v0, p0, Lcom/google/android/location/l/a/ao;->k:Ljava/lang/Long;

    if-eqz v0, :cond_a

    .line 229
    const/16 v0, 0xb

    iget-object v1, p0, Lcom/google/android/location/l/a/ao;->k:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/b;->b(IJ)V

    .line 231
    :cond_a
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/d;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 232
    return-void
.end method
