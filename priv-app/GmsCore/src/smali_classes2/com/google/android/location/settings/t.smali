.class public final Lcom/google/android/location/settings/t;
.super Landroid/support/v4/app/Fragment;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation


# static fields
.field private static final a:Ljava/util/ArrayList;


# instance fields
.field private b:Lcom/google/android/location/settings/v;

.field private c:I

.field private d:Lcom/google/android/gms/common/acl/ScopeData;

.field private e:Ljava/util/ArrayList;

.field private f:Landroid/view/View;

.field private g:Lcom/google/android/gms/common/people/views/AudienceView;

.field private h:Landroid/view/View;

.field private i:Lcom/google/android/gms/common/people/views/AudienceView;

.field private j:Landroid/widget/RadioButton;

.field private k:Landroid/widget/RadioButton;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 65
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/google/android/location/settings/t;->a:Ljava/util/ArrayList;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 55
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    .line 76
    return-void
.end method

.method public static a(Lcom/google/android/gms/common/acl/ScopeData;)Lcom/google/android/location/settings/t;
    .locals 3

    .prologue
    .line 120
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 121
    const-string v1, "index"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 122
    const-string v1, "scope_data"

    invoke-virtual {v0, v1, p0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 123
    new-instance v1, Lcom/google/android/location/settings/t;

    invoke-direct {v1}, Lcom/google/android/location/settings/t;-><init>()V

    .line 124
    invoke-virtual {v1, v0}, Lcom/google/android/location/settings/t;->setArguments(Landroid/os/Bundle;)V

    .line 125
    return-object v1
.end method

.method static synthetic a(Lcom/google/android/location/settings/t;)Lcom/google/android/location/settings/v;
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Lcom/google/android/location/settings/t;->b:Lcom/google/android/location/settings/v;

    return-object v0
.end method

.method private a()Ljava/util/ArrayList;
    .locals 5

    .prologue
    .line 272
    iget-object v0, p0, Lcom/google/android/location/settings/t;->d:Lcom/google/android/gms/common/acl/ScopeData;

    invoke-virtual {v0}, Lcom/google/android/gms/common/acl/ScopeData;->e()Ljava/lang/String;

    move-result-object v1

    .line 273
    if-nez v1, :cond_0

    .line 274
    const/4 v0, 0x0

    .line 289
    :goto_0
    return-object v0

    .line 277
    :cond_0
    :try_start_0
    invoke-static {v1}, Lcom/google/android/gms/common/util/m;->c(Ljava/lang/String;)[B

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/common/people/data/c;->a([B)Ljava/util/List;

    move-result-object v2

    .line 280
    if-eqz v2, :cond_1

    .line 281
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 283
    :catch_0
    move-exception v0

    .line 284
    const-string v2, "GLSActivity"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Failed to parse audience from roster: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 286
    :cond_1
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 287
    const-string v1, "myCircles"

    invoke-virtual {p0}, Lcom/google/android/location/settings/t;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/google/android/gms/p;->en:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/gms/common/people/data/AudienceMember;->c(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/people/data/AudienceMember;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method private a(Ljava/lang/String;Ljava/util/ArrayList;)Ljava/util/ArrayList;
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 343
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    move-object p2, v0

    .line 363
    :goto_0
    return-object p2

    .line 347
    :cond_0
    :try_start_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 348
    const-string v1, " "

    invoke-virtual {p1, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 349
    const/4 v1, 0x0

    :goto_1
    array-length v3, v2

    if-ge v1, v3, :cond_1

    .line 350
    aget-object v3, v2, v1

    invoke-static {v3}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->toHexString(J)Ljava/lang/String;

    move-result-object v3

    .line 351
    const/4 v4, 0x0

    invoke-static {v3, v4}, Lcom/google/android/gms/common/people/data/AudienceMember;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/people/data/AudienceMember;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 349
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 353
    :cond_1
    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    if-nez v1, :cond_2

    move-object p2, v0

    .line 354
    goto :goto_0

    .line 356
    :catch_0
    move-exception v0

    .line 357
    const-string v1, "GLSActivity"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Failed to parse audience from circle ID list: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 361
    :cond_2
    iget-object v0, p0, Lcom/google/android/location/settings/t;->d:Lcom/google/android/gms/common/acl/ScopeData;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/acl/ScopeData;->a(Z)V

    goto :goto_0
.end method

.method private a(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 129
    const-string v0, "index"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/location/settings/t;->c:I

    .line 130
    const-string v0, "scope_data"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/acl/ScopeData;

    iput-object v0, p0, Lcom/google/android/location/settings/t;->d:Lcom/google/android/gms/common/acl/ScopeData;

    .line 131
    const-string v0, "facl_audience"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/settings/t;->e:Ljava/util/ArrayList;

    .line 132
    return-void
.end method

.method private a(Z)V
    .locals 2

    .prologue
    .line 519
    iget-object v0, p0, Lcom/google/android/location/settings/t;->j:Landroid/widget/RadioButton;

    invoke-virtual {v0, p1}, Landroid/widget/RadioButton;->setChecked(Z)V

    .line 520
    iget-object v1, p0, Lcom/google/android/location/settings/t;->k:Landroid/widget/RadioButton;

    if-nez p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/RadioButton;->setChecked(Z)V

    .line 521
    return-void

    .line 520
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final onActivityCreated(Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 144
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 145
    if-eqz p1, :cond_0

    .line 146
    invoke-direct {p0, p1}, Lcom/google/android/location/settings/t;->a(Landroid/os/Bundle;)V

    .line 148
    :cond_0
    return-void
.end method

.method public final onClick(Landroid/view/View;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 505
    iget-object v0, p0, Lcom/google/android/location/settings/t;->g:Lcom/google/android/gms/common/people/views/AudienceView;

    if-eq p1, v0, :cond_0

    iget-object v0, p0, Lcom/google/android/location/settings/t;->j:Landroid/widget/RadioButton;

    if-eq p1, v0, :cond_0

    iget-object v0, p0, Lcom/google/android/location/settings/t;->h:Landroid/view/View;

    if-ne p1, v0, :cond_3

    .line 506
    :cond_0
    iget-object v0, p0, Lcom/google/android/location/settings/t;->g:Lcom/google/android/gms/common/people/views/AudienceView;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/location/settings/t;->j:Landroid/widget/RadioButton;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/location/settings/t;->h:Landroid/view/View;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/location/settings/t;->g:Lcom/google/android/gms/common/people/views/AudienceView;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/people/views/AudienceView;->setEnabled(Z)V

    iget-object v0, p0, Lcom/google/android/location/settings/t;->j:Landroid/widget/RadioButton;

    invoke-virtual {v0, v1}, Landroid/widget/RadioButton;->setEnabled(Z)V

    iget-object v0, p0, Lcom/google/android/location/settings/t;->h:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setEnabled(Z)V

    .line 507
    :cond_1
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/location/settings/t;->a(Z)V

    .line 508
    iget-object v0, p0, Lcom/google/android/location/settings/t;->b:Lcom/google/android/location/settings/v;

    iget v0, p0, Lcom/google/android/location/settings/t;->c:I

    iget-object v0, p0, Lcom/google/android/location/settings/t;->g:Lcom/google/android/gms/common/people/views/AudienceView;

    invoke-virtual {v0}, Lcom/google/android/gms/common/people/views/AudienceView;->a()Ljava/util/ArrayList;

    .line 516
    :cond_2
    :goto_0
    return-void

    .line 509
    :cond_3
    iget-object v0, p0, Lcom/google/android/location/settings/t;->i:Lcom/google/android/gms/common/people/views/AudienceView;

    if-eq p1, v0, :cond_4

    iget-object v0, p0, Lcom/google/android/location/settings/t;->k:Landroid/widget/RadioButton;

    if-ne p1, v0, :cond_5

    .line 510
    :cond_4
    invoke-direct {p0, v1}, Lcom/google/android/location/settings/t;->a(Z)V

    goto :goto_0

    .line 511
    :cond_5
    iget-object v0, p0, Lcom/google/android/location/settings/t;->f:Landroid/view/View;

    if-ne p1, v0, :cond_2

    .line 512
    iget-object v0, p0, Lcom/google/android/location/settings/t;->f:Landroid/view/View;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/google/android/location/settings/t;->f:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setEnabled(Z)V

    .line 513
    :cond_6
    iget-object v0, p0, Lcom/google/android/location/settings/t;->b:Lcom/google/android/location/settings/v;

    iget v0, p0, Lcom/google/android/location/settings/t;->c:I

    iget-object v0, p0, Lcom/google/android/location/settings/t;->e:Ljava/util/ArrayList;

    iget-object v0, p0, Lcom/google/android/location/settings/t;->d:Lcom/google/android/gms/common/acl/ScopeData;

    invoke-virtual {v0}, Lcom/google/android/gms/common/acl/ScopeData;->h()Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/location/settings/t;->d:Lcom/google/android/gms/common/acl/ScopeData;

    invoke-virtual {v0}, Lcom/google/android/gms/common/acl/ScopeData;->p()Z

    goto :goto_0
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 8

    .prologue
    const/4 v1, 0x0

    const/16 v5, 0x8

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 152
    invoke-virtual {p0}, Lcom/google/android/location/settings/t;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    .line 153
    instance-of v4, v0, Lcom/google/android/location/settings/v;

    if-eqz v4, :cond_4

    check-cast v0, Lcom/google/android/location/settings/v;

    :goto_0
    iput-object v0, p0, Lcom/google/android/location/settings/t;->b:Lcom/google/android/location/settings/v;

    .line 154
    if-nez p3, :cond_5

    .line 155
    invoke-virtual {p0}, Lcom/google/android/location/settings/t;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v4, "index"

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/location/settings/t;->c:I

    .line 156
    invoke-virtual {p0}, Lcom/google/android/location/settings/t;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v4, "scope_data"

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/acl/ScopeData;

    iput-object v0, p0, Lcom/google/android/location/settings/t;->d:Lcom/google/android/gms/common/acl/ScopeData;

    .line 157
    iget-object v0, p0, Lcom/google/android/location/settings/t;->d:Lcom/google/android/gms/common/acl/ScopeData;

    invoke-virtual {v0}, Lcom/google/android/gms/common/acl/ScopeData;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 158
    iget-object v0, p0, Lcom/google/android/location/settings/t;->d:Lcom/google/android/gms/common/acl/ScopeData;

    invoke-virtual {v0}, Lcom/google/android/gms/common/acl/ScopeData;->g()Ljava/lang/String;

    move-result-object v0

    sget-object v4, Lcom/google/android/location/settings/t;->a:Ljava/util/ArrayList;

    invoke-direct {p0, v0, v4}, Lcom/google/android/location/settings/t;->a(Ljava/lang/String;Ljava/util/ArrayList;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/settings/t;->e:Ljava/util/ArrayList;

    .line 165
    :cond_0
    :goto_1
    invoke-virtual {p0}, Lcom/google/android/location/settings/t;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/q;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    sget v4, Lcom/google/android/gms/l;->w:I

    invoke-virtual {v0, v4, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 167
    iget-object v0, p0, Lcom/google/android/location/settings/t;->d:Lcom/google/android/gms/common/acl/ScopeData;

    invoke-virtual {v0}, Lcom/google/android/gms/common/acl/ScopeData;->d()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/location/settings/t;->d:Lcom/google/android/gms/common/acl/ScopeData;

    invoke-virtual {v0}, Lcom/google/android/gms/common/acl/ScopeData;->f()Z

    move-result v0

    if-eqz v0, :cond_b

    .line 168
    :cond_1
    sget v0, Lcom/google/android/gms/j;->qF:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iget-object v4, p0, Lcom/google/android/location/settings/t;->d:Lcom/google/android/gms/common/acl/ScopeData;

    invoke-virtual {v4}, Lcom/google/android/gms/common/acl/ScopeData;->a()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    sget v0, Lcom/google/android/gms/j;->ei:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    iget-object v0, p0, Lcom/google/android/location/settings/t;->d:Lcom/google/android/gms/common/acl/ScopeData;

    invoke-virtual {v0}, Lcom/google/android/gms/common/acl/ScopeData;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-virtual {v4, v5}, Landroid/view/View;->setVisibility(I)V

    :goto_2
    iget-object v0, p0, Lcom/google/android/location/settings/t;->d:Lcom/google/android/gms/common/acl/ScopeData;

    invoke-virtual {v0}, Lcom/google/android/gms/common/acl/ScopeData;->f()Z

    move-result v0

    if-eqz v0, :cond_9

    sget v0, Lcom/google/android/gms/j;->fV:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    sget v0, Lcom/google/android/gms/j;->fW:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/settings/t;->f:Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/location/settings/t;->f:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setClickable(Z)V

    iget-object v0, p0, Lcom/google/android/location/settings/t;->f:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    sget v0, Lcom/google/android/gms/j;->qE:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/google/android/location/settings/t;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    sget v5, Lcom/google/android/gms/h;->k:I

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    iget-object v0, p0, Lcom/google/android/location/settings/t;->d:Lcom/google/android/gms/common/acl/ScopeData;

    invoke-virtual {v0}, Lcom/google/android/gms/common/acl/ScopeData;->r()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/location/settings/t;->d:Lcom/google/android/gms/common/acl/ScopeData;

    invoke-virtual {v0}, Lcom/google/android/gms/common/acl/ScopeData;->i()Z

    move-result v0

    if-eqz v0, :cond_2

    sget v0, Lcom/google/android/gms/j;->uc:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    sget v0, Lcom/google/android/gms/j;->ub:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iget-object v4, p0, Lcom/google/android/location/settings/t;->d:Lcom/google/android/gms/common/acl/ScopeData;

    invoke-virtual {v4}, Lcom/google/android/gms/common/acl/ScopeData;->s()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_2
    :goto_3
    iget-object v0, p0, Lcom/google/android/location/settings/t;->d:Lcom/google/android/gms/common/acl/ScopeData;

    invoke-virtual {v0}, Lcom/google/android/gms/common/acl/ScopeData;->d()Z

    move-result v0

    if-eqz v0, :cond_3

    sget v0, Lcom/google/android/gms/j;->nl:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    sget v0, Lcom/google/android/gms/j;->pI:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    invoke-direct {p0}, Lcom/google/android/location/settings/t;->a()Ljava/util/ArrayList;

    move-result-object v4

    sget v0, Lcom/google/android/gms/j;->bj:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/people/views/AudienceView;

    iput-object v0, p0, Lcom/google/android/location/settings/t;->g:Lcom/google/android/gms/common/people/views/AudienceView;

    iget-object v0, p0, Lcom/google/android/location/settings/t;->g:Lcom/google/android/gms/common/people/views/AudienceView;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/common/people/views/AudienceView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/google/android/location/settings/t;->g:Lcom/google/android/gms/common/people/views/AudienceView;

    invoke-virtual {v0, v3}, Lcom/google/android/gms/common/people/views/AudienceView;->a(Z)V

    iget-object v0, p0, Lcom/google/android/location/settings/t;->g:Lcom/google/android/gms/common/people/views/AudienceView;

    invoke-virtual {v0}, Lcom/google/android/gms/common/people/views/AudienceView;->b()V

    iget-object v0, p0, Lcom/google/android/location/settings/t;->g:Lcom/google/android/gms/common/people/views/AudienceView;

    invoke-virtual {v0, v4}, Lcom/google/android/gms/common/people/views/AudienceView;->a(Ljava/util/Collection;)V

    sget v0, Lcom/google/android/gms/j;->nj:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/settings/t;->h:Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/location/settings/t;->h:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setClickable(Z)V

    iget-object v0, p0, Lcom/google/android/location/settings/t;->h:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    sget v0, Lcom/google/android/gms/j;->mO:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/people/views/AudienceView;

    iput-object v0, p0, Lcom/google/android/location/settings/t;->i:Lcom/google/android/gms/common/people/views/AudienceView;

    iget-object v0, p0, Lcom/google/android/location/settings/t;->i:Lcom/google/android/gms/common/people/views/AudienceView;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/common/people/views/AudienceView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/google/android/location/settings/t;->i:Lcom/google/android/gms/common/people/views/AudienceView;

    invoke-virtual {v0, v3}, Lcom/google/android/gms/common/people/views/AudienceView;->a(Z)V

    iget-object v0, p0, Lcom/google/android/location/settings/t;->i:Lcom/google/android/gms/common/people/views/AudienceView;

    invoke-virtual {v0}, Lcom/google/android/gms/common/people/views/AudienceView;->c()V

    sget v0, Lcom/google/android/gms/j;->s:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioButton;

    iput-object v0, p0, Lcom/google/android/location/settings/t;->j:Landroid/widget/RadioButton;

    iget-object v5, p0, Lcom/google/android/location/settings/t;->j:Landroid/widget/RadioButton;

    invoke-virtual {v4}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_a

    move v0, v2

    :goto_4
    invoke-virtual {v5, v0}, Landroid/widget/RadioButton;->setChecked(Z)V

    sget v0, Lcom/google/android/gms/j;->pK:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioButton;

    iput-object v0, p0, Lcom/google/android/location/settings/t;->k:Landroid/widget/RadioButton;

    iget-object v0, p0, Lcom/google/android/location/settings/t;->k:Landroid/widget/RadioButton;

    invoke-virtual {v4}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v2

    invoke-virtual {v0, v2}, Landroid/widget/RadioButton;->setChecked(Z)V

    iget-object v0, p0, Lcom/google/android/location/settings/t;->j:Landroid/widget/RadioButton;

    invoke-virtual {v0, p0}, Landroid/widget/RadioButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/google/android/location/settings/t;->k:Landroid/widget/RadioButton;

    invoke-virtual {v0, p0}, Landroid/widget/RadioButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    sget v0, Lcom/google/android/gms/j;->qE:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/google/android/location/settings/t;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/google/android/gms/h;->l:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 172
    :cond_3
    :goto_5
    return-object v1

    :cond_4
    move-object v0, v1

    .line 153
    goto/16 :goto_0

    .line 162
    :cond_5
    invoke-direct {p0, p3}, Lcom/google/android/location/settings/t;->a(Landroid/os/Bundle;)V

    goto/16 :goto_1

    .line 168
    :cond_6
    iget-object v0, p0, Lcom/google/android/location/settings/t;->d:Lcom/google/android/gms/common/acl/ScopeData;

    invoke-virtual {v0}, Lcom/google/android/gms/common/acl/ScopeData;->f()Z

    move-result v0

    if-eqz v0, :cond_7

    new-instance v0, Lcom/google/android/location/settings/u;

    iget-object v5, p0, Lcom/google/android/location/settings/t;->d:Lcom/google/android/gms/common/acl/ScopeData;

    invoke-virtual {v5}, Lcom/google/android/gms/common/acl/ScopeData;->b()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p0}, Lcom/google/android/location/settings/t;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    sget v7, Lcom/google/android/gms/p;->bp:I

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    sget-object v7, Lcom/google/android/gms/common/analytics/c;->c:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    invoke-direct {v0, p0, v5, v6, v7}, Lcom/google/android/location/settings/u;-><init>(Lcom/google/android/location/settings/t;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)V

    :goto_6
    invoke-virtual {v4, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto/16 :goto_2

    :cond_7
    iget-object v0, p0, Lcom/google/android/location/settings/t;->d:Lcom/google/android/gms/common/acl/ScopeData;

    invoke-virtual {v0}, Lcom/google/android/gms/common/acl/ScopeData;->d()Z

    move-result v0

    if-eqz v0, :cond_8

    new-instance v0, Lcom/google/android/location/settings/u;

    iget-object v5, p0, Lcom/google/android/location/settings/t;->d:Lcom/google/android/gms/common/acl/ScopeData;

    invoke-virtual {v5}, Lcom/google/android/gms/common/acl/ScopeData;->b()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p0}, Lcom/google/android/location/settings/t;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    sget v7, Lcom/google/android/gms/p;->bR:I

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    sget-object v7, Lcom/google/android/gms/common/analytics/c;->e:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    invoke-direct {v0, p0, v5, v6, v7}, Lcom/google/android/location/settings/u;-><init>(Lcom/google/android/location/settings/t;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)V

    goto :goto_6

    :cond_8
    new-instance v0, Lcom/google/android/location/settings/u;

    iget-object v5, p0, Lcom/google/android/location/settings/t;->d:Lcom/google/android/gms/common/acl/ScopeData;

    invoke-virtual {v5}, Lcom/google/android/gms/common/acl/ScopeData;->b()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v0, p0, v5}, Lcom/google/android/location/settings/u;-><init>(Lcom/google/android/location/settings/t;Ljava/lang/String;)V

    goto :goto_6

    :cond_9
    iget-object v0, p0, Lcom/google/android/location/settings/t;->d:Lcom/google/android/gms/common/acl/ScopeData;

    invoke-virtual {v0}, Lcom/google/android/gms/common/acl/ScopeData;->r()Z

    move-result v0

    if-eqz v0, :cond_2

    sget v0, Lcom/google/android/gms/j;->uc:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    sget v0, Lcom/google/android/gms/j;->ub:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iget-object v4, p0, Lcom/google/android/location/settings/t;->d:Lcom/google/android/gms/common/acl/ScopeData;

    invoke-virtual {v4}, Lcom/google/android/gms/common/acl/ScopeData;->s()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_3

    :cond_a
    move v0, v3

    goto/16 :goto_4

    .line 170
    :cond_b
    sget v0, Lcom/google/android/gms/j;->qF:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iget-object v2, p0, Lcom/google/android/location/settings/t;->d:Lcom/google/android/gms/common/acl/ScopeData;

    invoke-virtual {v2}, Lcom/google/android/gms/common/acl/ScopeData;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    sget v0, Lcom/google/android/gms/j;->ei:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/location/settings/t;->d:Lcom/google/android/gms/common/acl/ScopeData;

    invoke-virtual {v2}, Lcom/google/android/gms/common/acl/ScopeData;->b()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_c

    invoke-virtual {v0, v5}, Landroid/view/View;->setVisibility(I)V

    :goto_7
    iget-object v0, p0, Lcom/google/android/location/settings/t;->d:Lcom/google/android/gms/common/acl/ScopeData;

    invoke-virtual {v0}, Lcom/google/android/gms/common/acl/ScopeData;->r()Z

    move-result v0

    if-eqz v0, :cond_3

    sget v0, Lcom/google/android/gms/j;->uc:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    sget v0, Lcom/google/android/gms/j;->ub:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iget-object v2, p0, Lcom/google/android/location/settings/t;->d:Lcom/google/android/gms/common/acl/ScopeData;

    invoke-virtual {v2}, Lcom/google/android/gms/common/acl/ScopeData;->s()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_5

    :cond_c
    new-instance v2, Lcom/google/android/location/settings/u;

    iget-object v4, p0, Lcom/google/android/location/settings/t;->d:Lcom/google/android/gms/common/acl/ScopeData;

    invoke-virtual {v4}, Lcom/google/android/gms/common/acl/ScopeData;->b()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, p0, v4}, Lcom/google/android/location/settings/u;-><init>(Lcom/google/android/location/settings/t;Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_7
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 136
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 137
    const-string v0, "index"

    iget v1, p0, Lcom/google/android/location/settings/t;->c:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 138
    const-string v0, "scope_data"

    iget-object v1, p0, Lcom/google/android/location/settings/t;->d:Lcom/google/android/gms/common/acl/ScopeData;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 139
    const-string v0, "facl_audience"

    iget-object v1, p0, Lcom/google/android/location/settings/t;->e:Ljava/util/ArrayList;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 140
    return-void
.end method
