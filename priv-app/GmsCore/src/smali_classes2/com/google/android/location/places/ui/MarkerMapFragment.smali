.class public Lcom/google/android/location/places/ui/MarkerMapFragment;
.super Lcom/google/android/gms/maps/q;
.source "SourceFile"

# interfaces
.implements Landroid/view/GestureDetector$OnDoubleTapListener;
.implements Landroid/view/GestureDetector$OnGestureListener;
.implements Landroid/view/View$OnClickListener;
.implements Landroid/view/View$OnTouchListener;
.implements Lcom/google/android/gms/maps/h;
.implements Lcom/google/android/gms/maps/i;
.implements Lcom/google/android/gms/maps/j;


# instance fields
.field a:Landroid/graphics/Point;

.field private final b:Landroid/support/v4/g/s;

.field private c:Lcom/google/android/gms/maps/model/a;

.field private d:Lcom/google/android/location/places/ui/r;

.field private e:Landroid/widget/FrameLayout;

.field private f:Landroid/view/View;

.field private g:Landroid/view/View;

.field private h:Landroid/view/View;

.field private i:Landroid/widget/ImageView;

.field private j:Landroid/view/View;

.field private k:Landroid/view/View;

.field private l:Landroid/view/View;

.field private m:Lcom/google/android/gms/maps/model/j;

.field private n:Lcom/google/android/gms/maps/model/f;

.field private o:Landroid/support/v4/view/q;

.field private p:I

.field private q:I

.field private r:I

.field private s:I

.field private t:Lcom/google/android/gms/maps/model/a;

.field private u:I

.field private v:I

.field private w:Lcom/google/android/location/places/ui/ab;

.field private x:Z

.field private y:Z

.field private z:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 122
    invoke-direct {p0}, Lcom/google/android/gms/maps/q;-><init>()V

    .line 123
    new-instance v0, Landroid/support/v4/g/s;

    invoke-direct {v0}, Landroid/support/v4/g/s;-><init>()V

    iput-object v0, p0, Lcom/google/android/location/places/ui/MarkerMapFragment;->b:Landroid/support/v4/g/s;

    .line 124
    return-void
.end method

.method static synthetic a(Lcom/google/android/location/places/ui/MarkerMapFragment;)Landroid/view/View;
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lcom/google/android/location/places/ui/MarkerMapFragment;->k:Landroid/view/View;

    return-object v0
.end method

.method static synthetic b(Lcom/google/android/location/places/ui/MarkerMapFragment;)Lcom/google/android/location/places/ui/ab;
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lcom/google/android/location/places/ui/MarkerMapFragment;->w:Lcom/google/android/location/places/ui/ab;

    return-object v0
.end method

.method private c(Z)V
    .locals 2

    .prologue
    .line 504
    invoke-virtual {p0}, Lcom/google/android/location/places/ui/MarkerMapFragment;->c()Lcom/google/android/gms/maps/c;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/maps/c;->d()Lcom/google/android/gms/maps/u;

    move-result-object v0

    .line 505
    :try_start_0
    iget-object v1, v0, Lcom/google/android/gms/maps/u;->a:Lcom/google/android/gms/maps/internal/co;

    invoke-interface {v1, p1}, Lcom/google/android/gms/maps/internal/co;->h(Z)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 506
    :try_start_1
    iget-object v0, v0, Lcom/google/android/gms/maps/u;->a:Lcom/google/android/gms/maps/internal/co;

    invoke-interface {v0, p1}, Lcom/google/android/gms/maps/internal/co;->i(Z)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_1

    return-void

    .line 505
    :catch_0
    move-exception v0

    new-instance v1, Lcom/google/android/gms/maps/model/n;

    invoke-direct {v1, v0}, Lcom/google/android/gms/maps/model/n;-><init>(Landroid/os/RemoteException;)V

    throw v1

    .line 506
    :catch_1
    move-exception v0

    new-instance v1, Lcom/google/android/gms/maps/model/n;

    invoke-direct {v1, v0}, Lcom/google/android/gms/maps/model/n;-><init>(Landroid/os/RemoteException;)V

    throw v1
.end method

.method private m()V
    .locals 14

    .prologue
    const-wide v8, 0x41584db080000000L    # 6371010.0

    const-wide/high16 v12, 0x4000000000000000L    # 2.0

    .line 605
    iget-object v0, p0, Lcom/google/android/location/places/ui/MarkerMapFragment;->t:Lcom/google/android/gms/maps/model/a;

    if-nez v0, :cond_0

    .line 623
    :goto_0
    return-void

    .line 609
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/location/places/ui/MarkerMapFragment;->f()Lcom/google/android/gms/maps/model/LatLng;

    move-result-object v0

    .line 610
    iget v1, p0, Lcom/google/android/location/places/ui/MarkerMapFragment;->v:I

    int-to-double v2, v1

    div-double/2addr v2, v8

    invoke-static {v2, v3}, Ljava/lang/Math;->toDegrees(D)D

    move-result-wide v2

    .line 612
    iget v1, p0, Lcom/google/android/location/places/ui/MarkerMapFragment;->u:I

    int-to-double v4, v1

    iget-wide v6, v0, Lcom/google/android/gms/maps/model/LatLng;->a:D

    invoke-static {v6, v7}, Ljava/lang/Math;->toRadians(D)D

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Math;->cos(D)D

    move-result-wide v6

    mul-double/2addr v6, v8

    div-double/2addr v4, v6

    invoke-static {v4, v5}, Ljava/lang/Math;->toDegrees(D)D

    move-result-wide v4

    .line 615
    new-instance v1, Lcom/google/android/gms/maps/model/LatLng;

    iget-wide v6, v0, Lcom/google/android/gms/maps/model/LatLng;->a:D

    div-double v8, v2, v12

    add-double/2addr v6, v8

    iget-wide v8, v0, Lcom/google/android/gms/maps/model/LatLng;->b:D

    div-double v10, v4, v12

    sub-double/2addr v8, v10

    invoke-direct {v1, v6, v7, v8, v9}, Lcom/google/android/gms/maps/model/LatLng;-><init>(DD)V

    .line 616
    new-instance v6, Lcom/google/android/gms/maps/model/LatLng;

    iget-wide v8, v0, Lcom/google/android/gms/maps/model/LatLng;->a:D

    div-double/2addr v2, v12

    sub-double v2, v8, v2

    iget-wide v8, v0, Lcom/google/android/gms/maps/model/LatLng;->b:D

    div-double/2addr v4, v12

    add-double/2addr v4, v8

    invoke-direct {v6, v2, v3, v4, v5}, Lcom/google/android/gms/maps/model/LatLng;-><init>(DD)V

    .line 617
    invoke-virtual {p0}, Lcom/google/android/location/places/ui/MarkerMapFragment;->c()Lcom/google/android/gms/maps/c;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/maps/c;->e()Lcom/google/android/gms/maps/o;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/google/android/gms/maps/o;->a(Lcom/google/android/gms/maps/model/LatLng;)Landroid/graphics/Point;

    move-result-object v1

    .line 618
    invoke-virtual {p0}, Lcom/google/android/location/places/ui/MarkerMapFragment;->c()Lcom/google/android/gms/maps/c;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/maps/c;->e()Lcom/google/android/gms/maps/o;

    move-result-object v0

    invoke-virtual {v0, v6}, Lcom/google/android/gms/maps/o;->a(Lcom/google/android/gms/maps/model/LatLng;)Landroid/graphics/Point;

    move-result-object v2

    .line 620
    iget-object v0, p0, Lcom/google/android/location/places/ui/MarkerMapFragment;->i:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout$LayoutParams;

    iget v3, v2, Landroid/graphics/Point;->x:I

    iget v4, v1, Landroid/graphics/Point;->x:I

    sub-int/2addr v3, v4

    iput v3, v0, Landroid/widget/FrameLayout$LayoutParams;->width:I

    .line 621
    iget-object v0, p0, Lcom/google/android/location/places/ui/MarkerMapFragment;->i:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout$LayoutParams;

    iget v2, v2, Landroid/graphics/Point;->y:I

    iget v1, v1, Landroid/graphics/Point;->y:I

    sub-int v1, v2, v1

    iput v1, v0, Landroid/widget/FrameLayout$LayoutParams;->height:I

    .line 622
    iget-object v0, p0, Lcom/google/android/location/places/ui/MarkerMapFragment;->i:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->requestLayout()V

    goto :goto_0
.end method


# virtual methods
.method public final a(IIII)Lcom/google/android/location/places/ui/MarkerMapFragment;
    .locals 7

    .prologue
    const/high16 v6, 0x41a00000    # 20.0f

    const/high16 v5, 0x41000000    # 8.0f

    const/4 v1, 0x0

    .line 127
    iput p1, p0, Lcom/google/android/location/places/ui/MarkerMapFragment;->p:I

    .line 128
    iput p2, p0, Lcom/google/android/location/places/ui/MarkerMapFragment;->q:I

    .line 129
    iput p3, p0, Lcom/google/android/location/places/ui/MarkerMapFragment;->s:I

    .line 130
    iput p4, p0, Lcom/google/android/location/places/ui/MarkerMapFragment;->r:I

    .line 131
    invoke-virtual {p0}, Lcom/google/android/location/places/ui/MarkerMapFragment;->c()Lcom/google/android/gms/maps/c;

    move-result-object v0

    iget v2, p0, Lcom/google/android/location/places/ui/MarkerMapFragment;->s:I

    iget v3, p0, Lcom/google/android/location/places/ui/MarkerMapFragment;->r:I

    invoke-virtual {v0, v2, v3}, Lcom/google/android/gms/maps/c;->a(II)V

    .line 134
    iget-object v0, p0, Lcom/google/android/location/places/ui/MarkerMapFragment;->g:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout$LayoutParams;

    iget v2, p0, Lcom/google/android/location/places/ui/MarkerMapFragment;->r:I

    iget v3, p0, Lcom/google/android/location/places/ui/MarkerMapFragment;->s:I

    sub-int/2addr v2, v3

    div-int/lit8 v2, v2, 0x2

    const/high16 v3, 0x41900000    # 18.0f

    invoke-virtual {p0}, Lcom/google/android/location/places/ui/MarkerMapFragment;->getActivity()Landroid/support/v4/app/q;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/android/location/places/ui/bm;->a(FLandroid/content/Context;)I

    move-result v3

    add-int/2addr v2, v3

    invoke-virtual {v0, v1, v1, v1, v2}, Landroid/widget/FrameLayout$LayoutParams;->setMargins(IIII)V

    .line 136
    iget-object v0, p0, Lcom/google/android/location/places/ui/MarkerMapFragment;->h:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout$LayoutParams;

    iget v2, p0, Lcom/google/android/location/places/ui/MarkerMapFragment;->r:I

    iget v3, p0, Lcom/google/android/location/places/ui/MarkerMapFragment;->s:I

    sub-int/2addr v2, v3

    div-int/lit8 v2, v2, 0x2

    invoke-virtual {v0, v1, v1, v1, v2}, Landroid/widget/FrameLayout$LayoutParams;->setMargins(IIII)V

    .line 138
    iget-object v0, p0, Lcom/google/android/location/places/ui/MarkerMapFragment;->i:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout$LayoutParams;

    iget v2, p0, Lcom/google/android/location/places/ui/MarkerMapFragment;->r:I

    iget v3, p0, Lcom/google/android/location/places/ui/MarkerMapFragment;->s:I

    sub-int/2addr v2, v3

    div-int/lit8 v2, v2, 0x2

    invoke-virtual {v0, v1, v1, v1, v2}, Landroid/widget/FrameLayout$LayoutParams;->setMargins(IIII)V

    .line 141
    iget-object v0, p0, Lcom/google/android/location/places/ui/MarkerMapFragment;->k:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout$LayoutParams;

    invoke-virtual {p0}, Lcom/google/android/location/places/ui/MarkerMapFragment;->getActivity()Landroid/support/v4/app/q;

    move-result-object v2

    invoke-static {v5, v2}, Lcom/google/android/location/places/ui/bm;->a(FLandroid/content/Context;)I

    move-result v2

    iget v3, p0, Lcom/google/android/location/places/ui/MarkerMapFragment;->r:I

    invoke-virtual {p0}, Lcom/google/android/location/places/ui/MarkerMapFragment;->getActivity()Landroid/support/v4/app/q;

    move-result-object v4

    invoke-static {v5, v4}, Lcom/google/android/location/places/ui/bm;->a(FLandroid/content/Context;)I

    move-result v4

    add-int/2addr v3, v4

    invoke-virtual {v0, v1, v1, v2, v3}, Landroid/widget/FrameLayout$LayoutParams;->setMargins(IIII)V

    .line 146
    iget v0, p0, Lcom/google/android/location/places/ui/MarkerMapFragment;->q:I

    iget v2, p0, Lcom/google/android/location/places/ui/MarkerMapFragment;->s:I

    add-int/2addr v0, v2

    iget v2, p0, Lcom/google/android/location/places/ui/MarkerMapFragment;->r:I

    sub-int/2addr v0, v2

    div-int/lit8 v0, v0, 0x2

    invoke-virtual {p0}, Lcom/google/android/location/places/ui/MarkerMapFragment;->getActivity()Landroid/support/v4/app/q;

    move-result-object v2

    invoke-static {v6, v2}, Lcom/google/android/location/places/ui/bm;->a(FLandroid/content/Context;)I

    move-result v2

    sub-int v2, v0, v2

    .line 148
    iget v0, p0, Lcom/google/android/location/places/ui/MarkerMapFragment;->q:I

    iget v3, p0, Lcom/google/android/location/places/ui/MarkerMapFragment;->s:I

    sub-int/2addr v0, v3

    iget v3, p0, Lcom/google/android/location/places/ui/MarkerMapFragment;->r:I

    add-int/2addr v0, v3

    div-int/lit8 v0, v0, 0x2

    invoke-virtual {p0}, Lcom/google/android/location/places/ui/MarkerMapFragment;->getActivity()Landroid/support/v4/app/q;

    move-result-object v3

    invoke-static {v6, v3}, Lcom/google/android/location/places/ui/bm;->a(FLandroid/content/Context;)I

    move-result v3

    sub-int/2addr v0, v3

    .line 150
    if-gez v2, :cond_0

    move v2, v1

    .line 153
    :cond_0
    if-gez v0, :cond_1

    move v0, v1

    .line 157
    :cond_1
    iget-object v3, p0, Lcom/google/android/location/places/ui/MarkerMapFragment;->j:Landroid/view/View;

    invoke-virtual {v3, v1, v2, v1, v0}, Landroid/view/View;->setPadding(IIII)V

    .line 158
    iget v0, p0, Lcom/google/android/location/places/ui/MarkerMapFragment;->p:I

    div-int/lit8 v0, v0, 0x2

    .line 159
    iget v1, p0, Lcom/google/android/location/places/ui/MarkerMapFragment;->q:I

    div-int/lit8 v1, v1, 0x2

    iget v2, p0, Lcom/google/android/location/places/ui/MarkerMapFragment;->r:I

    iget v3, p0, Lcom/google/android/location/places/ui/MarkerMapFragment;->s:I

    sub-int/2addr v2, v3

    div-int/lit8 v2, v2, 0x2

    sub-int/2addr v1, v2

    .line 160
    new-instance v2, Landroid/graphics/Point;

    invoke-direct {v2, v0, v1}, Landroid/graphics/Point;-><init>(II)V

    iput-object v2, p0, Lcom/google/android/location/places/ui/MarkerMapFragment;->a:Landroid/graphics/Point;

    .line 162
    return-object p0
.end method

.method public final a(Lcom/google/android/location/places/ui/ab;)Lcom/google/android/location/places/ui/MarkerMapFragment;
    .locals 0

    .prologue
    .line 176
    iput-object p1, p0, Lcom/google/android/location/places/ui/MarkerMapFragment;->w:Lcom/google/android/location/places/ui/ab;

    .line 177
    return-object p0
.end method

.method public final a(Lcom/google/android/location/places/ui/r;)Lcom/google/android/location/places/ui/MarkerMapFragment;
    .locals 0

    .prologue
    .line 166
    iput-object p1, p0, Lcom/google/android/location/places/ui/MarkerMapFragment;->d:Lcom/google/android/location/places/ui/r;

    .line 167
    return-object p0
.end method

.method public final a(Z)Lcom/google/android/location/places/ui/MarkerMapFragment;
    .locals 0

    .prologue
    .line 171
    iput-boolean p1, p0, Lcom/google/android/location/places/ui/MarkerMapFragment;->z:Z

    .line 172
    return-object p0
.end method

.method public final a()V
    .locals 1

    .prologue
    .line 628
    iget-object v0, p0, Lcom/google/android/location/places/ui/MarkerMapFragment;->w:Lcom/google/android/location/places/ui/ab;

    if-eqz v0, :cond_0

    .line 629
    iget-object v0, p0, Lcom/google/android/location/places/ui/MarkerMapFragment;->w:Lcom/google/android/location/places/ui/ab;

    invoke-interface {v0}, Lcom/google/android/location/places/ui/ab;->d()V

    .line 631
    :cond_0
    return-void
.end method

.method public final a(I)V
    .locals 2

    .prologue
    .line 373
    invoke-virtual {p0}, Lcom/google/android/location/places/ui/MarkerMapFragment;->c()Lcom/google/android/gms/maps/c;

    move-result-object v0

    iget v1, p0, Lcom/google/android/location/places/ui/MarkerMapFragment;->s:I

    invoke-virtual {v0, v1, p1}, Lcom/google/android/gms/maps/c;->a(II)V

    .line 374
    return-void
.end method

.method public final a(Lcom/google/android/gms/location/places/f;)V
    .locals 2

    .prologue
    .line 458
    const-string v0, "animateToFitPlace must be called on the UI thread."

    invoke-static {v0}, Lcom/google/android/gms/common/internal/bx;->b(Ljava/lang/String;)V

    .line 459
    if-nez p1, :cond_0

    .line 470
    :goto_0
    return-void

    .line 463
    :cond_0
    invoke-virtual {p1}, Lcom/google/android/gms/location/places/f;->f()Lcom/google/android/gms/maps/model/LatLngBounds;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 464
    invoke-virtual {p0}, Lcom/google/android/location/places/ui/MarkerMapFragment;->c()Lcom/google/android/gms/maps/c;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/android/gms/location/places/f;->f()Lcom/google/android/gms/maps/model/LatLngBounds;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/gms/maps/b;->a(Lcom/google/android/gms/maps/model/LatLngBounds;)Lcom/google/android/gms/maps/a;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/maps/c;->b(Lcom/google/android/gms/maps/a;)V

    goto :goto_0

    .line 467
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/location/places/ui/MarkerMapFragment;->c()Lcom/google/android/gms/maps/c;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/android/gms/location/places/f;->e()Lcom/google/android/gms/maps/model/LatLng;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/gms/maps/b;->a(Lcom/google/android/gms/maps/model/LatLng;)Lcom/google/android/gms/maps/a;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/maps/c;->b(Lcom/google/android/gms/maps/a;)V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/maps/model/LatLng;)V
    .locals 2

    .prologue
    .line 416
    const-string v0, "moveMapViewport must be called on the UI thread."

    invoke-static {v0}, Lcom/google/android/gms/common/internal/bx;->b(Ljava/lang/String;)V

    .line 417
    invoke-virtual {p0}, Lcom/google/android/location/places/ui/MarkerMapFragment;->c()Lcom/google/android/gms/maps/c;

    move-result-object v0

    invoke-static {p1}, Lcom/google/android/gms/maps/b;->a(Lcom/google/android/gms/maps/model/LatLng;)Lcom/google/android/gms/maps/a;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/maps/c;->a(Lcom/google/android/gms/maps/a;)V

    .line 418
    return-void
.end method

.method public final a(Lcom/google/android/gms/maps/model/LatLngBounds;)V
    .locals 3

    .prologue
    .line 410
    const-string v0, "moveMapViewport must be called on the UI thread."

    invoke-static {v0}, Lcom/google/android/gms/common/internal/bx;->b(Ljava/lang/String;)V

    .line 411
    invoke-virtual {p0}, Lcom/google/android/location/places/ui/MarkerMapFragment;->c()Lcom/google/android/gms/maps/c;

    move-result-object v0

    iget v1, p0, Lcom/google/android/location/places/ui/MarkerMapFragment;->p:I

    iget v2, p0, Lcom/google/android/location/places/ui/MarkerMapFragment;->q:I

    invoke-static {p1, v1, v2}, Lcom/google/android/gms/maps/b;->a(Lcom/google/android/gms/maps/model/LatLngBounds;II)Lcom/google/android/gms/maps/a;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/maps/c;->a(Lcom/google/android/gms/maps/a;)V

    .line 412
    return-void
.end method

.method public final a(Ljava/lang/String;III)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/16 v3, 0x8

    const/4 v2, 0x5

    .line 344
    if-eqz p1, :cond_1

    if-lez p2, :cond_1

    if-lez p3, :cond_1

    if-lez p4, :cond_1

    .line 345
    invoke-virtual {p0}, Lcom/google/android/location/places/ui/MarkerMapFragment;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/q;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    .line 348
    :try_start_0
    invoke-virtual {v0, p1}, Landroid/content/pm/PackageManager;->getResourcesForApplication(Ljava/lang/String;)Landroid/content/res/Resources;

    move-result-object v0

    .line 349
    invoke-static {v0, p2}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/gms/maps/model/b;->a(Landroid/graphics/Bitmap;)Lcom/google/android/gms/maps/model/a;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/location/places/ui/MarkerMapFragment;->t:Lcom/google/android/gms/maps/model/a;

    .line 351
    iget-object v1, p0, Lcom/google/android/location/places/ui/MarkerMapFragment;->i:Landroid/widget/ImageView;

    invoke-virtual {v0, p2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 352
    iput p3, p0, Lcom/google/android/location/places/ui/MarkerMapFragment;->u:I

    .line 353
    iput p4, p0, Lcom/google/android/location/places/ui/MarkerMapFragment;->v:I
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 370
    :cond_0
    :goto_0
    return-void

    .line 355
    :catch_0
    move-exception v0

    iget-object v0, p0, Lcom/google/android/location/places/ui/MarkerMapFragment;->i:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 356
    iput-object v4, p0, Lcom/google/android/location/places/ui/MarkerMapFragment;->t:Lcom/google/android/gms/maps/model/a;

    .line 357
    const-string v0, "Places"

    invoke-static {v0, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 358
    const-string v0, "Places"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Could not find reference marker overlay resource for package: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", and resourceId: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/location/n/aa;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 363
    :cond_1
    const-string v0, "Places"

    invoke-static {v0, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_3

    if-eqz p1, :cond_3

    if-lez p2, :cond_3

    if-lez p3, :cond_2

    if-gtz p4, :cond_3

    .line 365
    :cond_2
    const-string v0, "Places"

    const-string v1, "Invalid width or height for reference marker overlay"

    invoke-static {v0, v1}, Lcom/google/android/location/n/aa;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 367
    :cond_3
    iget-object v0, p0, Lcom/google/android/location/places/ui/MarkerMapFragment;->i:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 368
    iput-object v4, p0, Lcom/google/android/location/places/ui/MarkerMapFragment;->t:Lcom/google/android/gms/maps/model/a;

    goto :goto_0
.end method

.method public final a([Lcom/google/android/gms/location/places/f;)V
    .locals 8

    .prologue
    .line 440
    const-string v0, "setPlacesOnMap must be called on the UI thread."

    invoke-static {v0}, Lcom/google/android/gms/common/internal/bx;->b(Ljava/lang/String;)V

    .line 441
    if-nez p1, :cond_1

    .line 454
    :cond_0
    return-void

    .line 446
    :cond_1
    array-length v0, p1

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_0
    if-ltz v1, :cond_0

    .line 447
    aget-object v2, p1, v1

    .line 448
    invoke-virtual {p0}, Lcom/google/android/location/places/ui/MarkerMapFragment;->c()Lcom/google/android/gms/maps/c;

    move-result-object v3

    new-instance v0, Lcom/google/android/gms/maps/model/MarkerOptions;

    invoke-direct {v0}, Lcom/google/android/gms/maps/model/MarkerOptions;-><init>()V

    invoke-virtual {v2}, Lcom/google/android/gms/location/places/f;->e()Lcom/google/android/gms/maps/model/LatLng;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/google/android/gms/maps/model/MarkerOptions;->a(Lcom/google/android/gms/maps/model/LatLng;)Lcom/google/android/gms/maps/model/MarkerOptions;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/location/places/ui/MarkerMapFragment;->d:Lcom/google/android/location/places/ui/r;

    invoke-virtual {v2}, Lcom/google/android/gms/location/places/f;->b()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_2
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/location/places/PlaceType;

    iget-object v7, v5, Lcom/google/android/location/places/ui/r;->b:Landroid/support/v4/g/s;

    invoke-virtual {v0}, Lcom/google/android/gms/location/places/PlaceType;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v7, v0}, Landroid/support/v4/g/s;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    if-eqz v0, :cond_2

    :goto_1
    invoke-static {v0}, Lcom/google/android/gms/maps/model/b;->a(Landroid/graphics/Bitmap;)Lcom/google/android/gms/maps/model/a;

    move-result-object v0

    invoke-virtual {v4, v0}, Lcom/google/android/gms/maps/model/MarkerOptions;->a(Lcom/google/android/gms/maps/model/a;)Lcom/google/android/gms/maps/model/MarkerOptions;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/maps/model/MarkerOptions;->c()Lcom/google/android/gms/maps/model/MarkerOptions;

    move-result-object v0

    invoke-virtual {v3, v0}, Lcom/google/android/gms/maps/c;->a(Lcom/google/android/gms/maps/model/MarkerOptions;)Lcom/google/android/gms/maps/model/j;

    move-result-object v0

    .line 452
    iget-object v3, p0, Lcom/google/android/location/places/ui/MarkerMapFragment;->b:Landroid/support/v4/g/s;

    invoke-virtual {v0}, Lcom/google/android/gms/maps/model/j;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0, v2}, Landroid/support/v4/g/s;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 446
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_0

    .line 448
    :cond_3
    iget-object v0, v5, Lcom/google/android/location/places/ui/r;->b:Landroid/support/v4/g/s;

    sget-object v5, Lcom/google/android/gms/location/places/PlaceType;->H:Lcom/google/android/gms/location/places/PlaceType;

    invoke-virtual {v5}, Lcom/google/android/gms/location/places/PlaceType;->a()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Landroid/support/v4/g/s;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    goto :goto_1
.end method

.method public final a(Lcom/google/android/gms/maps/model/j;)Z
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 638
    iget-object v0, p0, Lcom/google/android/location/places/ui/MarkerMapFragment;->b:Landroid/support/v4/g/s;

    invoke-virtual {p1}, Lcom/google/android/gms/maps/model/j;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v4/g/s;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/location/places/f;

    .line 639
    if-nez v0, :cond_1

    .line 647
    :cond_0
    :goto_0
    return v2

    .line 642
    :cond_1
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/google/android/location/places/ui/MarkerMapFragment;->b(Z)V

    .line 643
    invoke-virtual {p0, v0}, Lcom/google/android/location/places/ui/MarkerMapFragment;->b(Lcom/google/android/gms/location/places/f;)V

    .line 644
    iget-object v1, p0, Lcom/google/android/location/places/ui/MarkerMapFragment;->w:Lcom/google/android/location/places/ui/ab;

    if-eqz v1, :cond_0

    .line 645
    iget-object v1, p0, Lcom/google/android/location/places/ui/MarkerMapFragment;->w:Lcom/google/android/location/places/ui/ab;

    invoke-interface {v1, v0}, Lcom/google/android/location/places/ui/ab;->a(Lcom/google/android/gms/location/places/f;)V

    goto :goto_0
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 653
    iget-boolean v0, p0, Lcom/google/android/location/places/ui/MarkerMapFragment;->y:Z

    if-eqz v0, :cond_1

    .line 660
    :cond_0
    :goto_0
    return-void

    .line 657
    :cond_1
    iget-object v0, p0, Lcom/google/android/location/places/ui/MarkerMapFragment;->w:Lcom/google/android/location/places/ui/ab;

    if-eqz v0, :cond_0

    .line 658
    iget-object v0, p0, Lcom/google/android/location/places/ui/MarkerMapFragment;->w:Lcom/google/android/location/places/ui/ab;

    invoke-interface {v0}, Lcom/google/android/location/places/ui/ab;->e()V

    goto :goto_0
.end method

.method public final b(Lcom/google/android/gms/location/places/f;)V
    .locals 5

    .prologue
    .line 511
    const-string v0, "markPlace must be called on the UI thread."

    invoke-static {v0}, Lcom/google/android/gms/common/internal/bx;->b(Ljava/lang/String;)V

    .line 512
    invoke-virtual {p0}, Lcom/google/android/location/places/ui/MarkerMapFragment;->k()V

    .line 513
    if-nez p1, :cond_1

    .line 526
    :cond_0
    :goto_0
    return-void

    .line 516
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/location/places/ui/MarkerMapFragment;->c()Lcom/google/android/gms/maps/c;

    move-result-object v0

    new-instance v1, Lcom/google/android/gms/maps/model/MarkerOptions;

    invoke-direct {v1}, Lcom/google/android/gms/maps/model/MarkerOptions;-><init>()V

    invoke-virtual {p1}, Lcom/google/android/gms/location/places/f;->e()Lcom/google/android/gms/maps/model/LatLng;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/gms/maps/model/MarkerOptions;->a(Lcom/google/android/gms/maps/model/LatLng;)Lcom/google/android/gms/maps/model/MarkerOptions;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/location/places/ui/MarkerMapFragment;->c:Lcom/google/android/gms/maps/model/a;

    invoke-virtual {v1, v2}, Lcom/google/android/gms/maps/model/MarkerOptions;->a(Lcom/google/android/gms/maps/model/a;)Lcom/google/android/gms/maps/model/MarkerOptions;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/maps/c;->a(Lcom/google/android/gms/maps/model/MarkerOptions;)Lcom/google/android/gms/maps/model/j;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/places/ui/MarkerMapFragment;->m:Lcom/google/android/gms/maps/model/j;

    .line 520
    iget-object v0, p0, Lcom/google/android/location/places/ui/MarkerMapFragment;->t:Lcom/google/android/gms/maps/model/a;

    if-eqz v0, :cond_0

    .line 521
    invoke-virtual {p0}, Lcom/google/android/location/places/ui/MarkerMapFragment;->c()Lcom/google/android/gms/maps/c;

    move-result-object v0

    new-instance v1, Lcom/google/android/gms/maps/model/GroundOverlayOptions;

    invoke-direct {v1}, Lcom/google/android/gms/maps/model/GroundOverlayOptions;-><init>()V

    invoke-virtual {p1}, Lcom/google/android/gms/location/places/f;->e()Lcom/google/android/gms/maps/model/LatLng;

    move-result-object v2

    iget v3, p0, Lcom/google/android/location/places/ui/MarkerMapFragment;->u:I

    int-to-float v3, v3

    iget v4, p0, Lcom/google/android/location/places/ui/MarkerMapFragment;->v:I

    int-to-float v4, v4

    invoke-virtual {v1, v2, v3, v4}, Lcom/google/android/gms/maps/model/GroundOverlayOptions;->a(Lcom/google/android/gms/maps/model/LatLng;FF)Lcom/google/android/gms/maps/model/GroundOverlayOptions;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/maps/model/GroundOverlayOptions;->b()Lcom/google/android/gms/maps/model/GroundOverlayOptions;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/location/places/ui/MarkerMapFragment;->t:Lcom/google/android/gms/maps/model/a;

    invoke-virtual {v1, v2}, Lcom/google/android/gms/maps/model/GroundOverlayOptions;->a(Lcom/google/android/gms/maps/model/a;)Lcom/google/android/gms/maps/model/GroundOverlayOptions;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/maps/c;->a(Lcom/google/android/gms/maps/model/GroundOverlayOptions;)Lcom/google/android/gms/maps/model/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/places/ui/MarkerMapFragment;->n:Lcom/google/android/gms/maps/model/f;

    goto :goto_0
.end method

.method public final b(Lcom/google/android/gms/maps/model/LatLng;)V
    .locals 2

    .prologue
    .line 422
    const-string v0, "moveMapViewport must be called on the UI thread."

    invoke-static {v0}, Lcom/google/android/gms/common/internal/bx;->b(Ljava/lang/String;)V

    .line 423
    invoke-virtual {p0}, Lcom/google/android/location/places/ui/MarkerMapFragment;->c()Lcom/google/android/gms/maps/c;

    move-result-object v0

    invoke-static {p1}, Lcom/google/android/gms/maps/b;->a(Lcom/google/android/gms/maps/model/LatLng;)Lcom/google/android/gms/maps/a;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/maps/c;->b(Lcom/google/android/gms/maps/a;)V

    .line 424
    return-void
.end method

.method public final b(Z)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/16 v1, 0x8

    .line 474
    const-string v0, "setCenterMarkerEnabled must be called on the UI thread."

    invoke-static {v0}, Lcom/google/android/gms/common/internal/bx;->b(Ljava/lang/String;)V

    .line 475
    if-eqz p1, :cond_1

    .line 476
    invoke-virtual {p0}, Lcom/google/android/location/places/ui/MarkerMapFragment;->k()V

    .line 477
    iget-object v0, p0, Lcom/google/android/location/places/ui/MarkerMapFragment;->g:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 478
    invoke-direct {p0}, Lcom/google/android/location/places/ui/MarkerMapFragment;->m()V

    .line 479
    iget-boolean v0, p0, Lcom/google/android/location/places/ui/MarkerMapFragment;->x:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/location/places/ui/MarkerMapFragment;->t:Lcom/google/android/gms/maps/model/a;

    if-eqz v0, :cond_0

    .line 480
    iget-object v0, p0, Lcom/google/android/location/places/ui/MarkerMapFragment;->i:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 488
    :cond_0
    :goto_0
    return-void

    .line 484
    :cond_1
    iget-object v0, p0, Lcom/google/android/location/places/ui/MarkerMapFragment;->g:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 485
    iget-object v0, p0, Lcom/google/android/location/places/ui/MarkerMapFragment;->h:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 486
    iget-object v0, p0, Lcom/google/android/location/places/ui/MarkerMapFragment;->i:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0
.end method

.method public final d()V
    .locals 4
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    .prologue
    const/16 v3, 0x8

    const/4 v2, 0x1

    .line 297
    iget-boolean v0, p0, Lcom/google/android/location/places/ui/MarkerMapFragment;->x:Z

    if-nez v0, :cond_2

    .line 298
    iput-boolean v2, p0, Lcom/google/android/location/places/ui/MarkerMapFragment;->x:Z

    .line 299
    invoke-virtual {p0}, Lcom/google/android/location/places/ui/MarkerMapFragment;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    sget v1, Lcom/google/android/gms/b;->x:I

    invoke-static {v0, v1}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    .line 300
    invoke-virtual {v0, v2}, Landroid/view/animation/Animation;->setFillAfter(Z)V

    .line 301
    iget-object v1, p0, Lcom/google/android/location/places/ui/MarkerMapFragment;->k:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 302
    iget-object v0, p0, Lcom/google/android/location/places/ui/MarkerMapFragment;->k:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 303
    iget-boolean v0, p0, Lcom/google/android/location/places/ui/MarkerMapFragment;->z:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/location/places/ui/MarkerMapFragment;->l:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 304
    iget-object v0, p0, Lcom/google/android/location/places/ui/MarkerMapFragment;->l:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 306
    :cond_0
    iget-object v0, p0, Lcom/google/android/location/places/ui/MarkerMapFragment;->g:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_2

    .line 307
    invoke-virtual {p0}, Lcom/google/android/location/places/ui/MarkerMapFragment;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    sget v1, Lcom/google/android/gms/b;->y:I

    invoke-static {v0, v1}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    .line 308
    invoke-virtual {v0, v2}, Landroid/view/animation/Animation;->setFillAfter(Z)V

    .line 309
    iget-object v1, p0, Lcom/google/android/location/places/ui/MarkerMapFragment;->g:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 310
    iget-object v0, p0, Lcom/google/android/location/places/ui/MarkerMapFragment;->h:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 311
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_1

    .line 312
    iget-object v0, p0, Lcom/google/android/location/places/ui/MarkerMapFragment;->g:Landroid/view/View;

    const v1, 0x3f19999a    # 0.6f

    invoke-virtual {v0, v1}, Landroid/view/View;->setAlpha(F)V

    .line 314
    :cond_1
    iget-object v0, p0, Lcom/google/android/location/places/ui/MarkerMapFragment;->i:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 317
    :cond_2
    return-void
.end method

.method public final e()V
    .locals 3
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 321
    iget-boolean v0, p0, Lcom/google/android/location/places/ui/MarkerMapFragment;->x:Z

    if-eqz v0, :cond_3

    .line 322
    invoke-virtual {p0}, Lcom/google/android/location/places/ui/MarkerMapFragment;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    sget v1, Lcom/google/android/gms/b;->u:I

    invoke-static {v0, v1}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    .line 323
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setFillAfter(Z)V

    .line 324
    iget-object v1, p0, Lcom/google/android/location/places/ui/MarkerMapFragment;->k:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 325
    iget-object v0, p0, Lcom/google/android/location/places/ui/MarkerMapFragment;->k:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 326
    iget-boolean v0, p0, Lcom/google/android/location/places/ui/MarkerMapFragment;->z:Z

    if-nez v0, :cond_0

    .line 327
    iget-object v0, p0, Lcom/google/android/location/places/ui/MarkerMapFragment;->l:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 329
    :cond_0
    iget-object v0, p0, Lcom/google/android/location/places/ui/MarkerMapFragment;->g:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_2

    .line 330
    invoke-virtual {p0}, Lcom/google/android/location/places/ui/MarkerMapFragment;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    sget v1, Lcom/google/android/gms/b;->s:I

    invoke-static {v0, v1}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    .line 331
    iget-object v1, p0, Lcom/google/android/location/places/ui/MarkerMapFragment;->g:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 332
    iget-object v0, p0, Lcom/google/android/location/places/ui/MarkerMapFragment;->h:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 333
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_1

    .line 334
    iget-object v0, p0, Lcom/google/android/location/places/ui/MarkerMapFragment;->g:Landroid/view/View;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Landroid/view/View;->setAlpha(F)V

    .line 336
    :cond_1
    invoke-direct {p0}, Lcom/google/android/location/places/ui/MarkerMapFragment;->m()V

    .line 337
    iget-object v0, p0, Lcom/google/android/location/places/ui/MarkerMapFragment;->i:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 339
    :cond_2
    iput-boolean v2, p0, Lcom/google/android/location/places/ui/MarkerMapFragment;->x:Z

    .line 341
    :cond_3
    return-void
.end method

.method public final f()Lcom/google/android/gms/maps/model/LatLng;
    .locals 2

    .prologue
    .line 378
    iget-object v0, p0, Lcom/google/android/location/places/ui/MarkerMapFragment;->a:Landroid/graphics/Point;

    invoke-static {v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 379
    invoke-virtual {p0}, Lcom/google/android/location/places/ui/MarkerMapFragment;->c()Lcom/google/android/gms/maps/c;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/maps/c;->e()Lcom/google/android/gms/maps/o;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/location/places/ui/MarkerMapFragment;->a:Landroid/graphics/Point;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/maps/o;->a(Landroid/graphics/Point;)Lcom/google/android/gms/maps/model/LatLng;

    move-result-object v0

    return-object v0
.end method

.method public final g()Lcom/google/android/gms/maps/model/LatLngBounds;
    .locals 1

    .prologue
    .line 384
    invoke-virtual {p0}, Lcom/google/android/location/places/ui/MarkerMapFragment;->c()Lcom/google/android/gms/maps/c;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/maps/c;->e()Lcom/google/android/gms/maps/o;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/maps/o;->a()Lcom/google/android/gms/maps/model/VisibleRegion;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/gms/maps/model/VisibleRegion;->e:Lcom/google/android/gms/maps/model/LatLngBounds;

    return-object v0
.end method

.method public final h()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 389
    iget-boolean v0, p0, Lcom/google/android/location/places/ui/MarkerMapFragment;->y:Z

    if-nez v0, :cond_0

    .line 390
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/location/places/ui/MarkerMapFragment;->y:Z

    .line 391
    invoke-virtual {p0, v1}, Lcom/google/android/location/places/ui/MarkerMapFragment;->b(Z)V

    .line 392
    invoke-direct {p0, v1}, Lcom/google/android/location/places/ui/MarkerMapFragment;->c(Z)V

    .line 393
    iget-object v0, p0, Lcom/google/android/location/places/ui/MarkerMapFragment;->k:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setEnabled(Z)V

    .line 394
    iget-object v0, p0, Lcom/google/android/location/places/ui/MarkerMapFragment;->j:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 396
    :cond_0
    return-void
.end method

.method public final i()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 400
    iget-boolean v0, p0, Lcom/google/android/location/places/ui/MarkerMapFragment;->y:Z

    if-eqz v0, :cond_0

    .line 401
    invoke-direct {p0, v1}, Lcom/google/android/location/places/ui/MarkerMapFragment;->c(Z)V

    .line 402
    iget-object v0, p0, Lcom/google/android/location/places/ui/MarkerMapFragment;->k:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setEnabled(Z)V

    .line 403
    iget-object v0, p0, Lcom/google/android/location/places/ui/MarkerMapFragment;->j:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 404
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/location/places/ui/MarkerMapFragment;->y:Z

    .line 406
    :cond_0
    return-void
.end method

.method public final j()V
    .locals 2

    .prologue
    .line 428
    const-string v0, "clearPlacesOnMap must be called on the UI thread."

    invoke-static {v0}, Lcom/google/android/gms/common/internal/bx;->b(Ljava/lang/String;)V

    .line 429
    invoke-virtual {p0}, Lcom/google/android/location/places/ui/MarkerMapFragment;->k()V

    .line 430
    invoke-virtual {p0}, Lcom/google/android/location/places/ui/MarkerMapFragment;->c()Lcom/google/android/gms/maps/c;

    move-result-object v0

    :try_start_0
    iget-object v0, v0, Lcom/google/android/gms/maps/c;->a:Lcom/google/android/gms/maps/internal/k;

    invoke-interface {v0}, Lcom/google/android/gms/maps/internal/k;->e()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 431
    iget-object v0, p0, Lcom/google/android/location/places/ui/MarkerMapFragment;->b:Landroid/support/v4/g/s;

    invoke-virtual {v0}, Landroid/support/v4/g/s;->clear()V

    .line 432
    return-void

    .line 430
    :catch_0
    move-exception v0

    new-instance v1, Lcom/google/android/gms/maps/model/n;

    invoke-direct {v1, v0}, Lcom/google/android/gms/maps/model/n;-><init>(Landroid/os/RemoteException;)V

    throw v1
.end method

.method public final k()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 491
    iget-object v0, p0, Lcom/google/android/location/places/ui/MarkerMapFragment;->m:Lcom/google/android/gms/maps/model/j;

    if-eqz v0, :cond_0

    .line 492
    iget-object v0, p0, Lcom/google/android/location/places/ui/MarkerMapFragment;->m:Lcom/google/android/gms/maps/model/j;

    :try_start_0
    iget-object v0, v0, Lcom/google/android/gms/maps/model/j;->a:Lcom/google/android/gms/maps/model/internal/s;

    invoke-interface {v0}, Lcom/google/android/gms/maps/model/internal/s;->a()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 493
    iput-object v1, p0, Lcom/google/android/location/places/ui/MarkerMapFragment;->m:Lcom/google/android/gms/maps/model/j;

    .line 496
    :cond_0
    iget-object v0, p0, Lcom/google/android/location/places/ui/MarkerMapFragment;->n:Lcom/google/android/gms/maps/model/f;

    if-eqz v0, :cond_1

    .line 497
    iget-object v0, p0, Lcom/google/android/location/places/ui/MarkerMapFragment;->n:Lcom/google/android/gms/maps/model/f;

    :try_start_1
    iget-object v0, v0, Lcom/google/android/gms/maps/model/f;->a:Lcom/google/android/gms/maps/model/internal/j;

    invoke-interface {v0}, Lcom/google/android/gms/maps/model/internal/j;->a()V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_1

    .line 498
    iput-object v1, p0, Lcom/google/android/location/places/ui/MarkerMapFragment;->n:Lcom/google/android/gms/maps/model/f;

    .line 500
    :cond_1
    return-void

    .line 492
    :catch_0
    move-exception v0

    new-instance v1, Lcom/google/android/gms/maps/model/n;

    invoke-direct {v1, v0}, Lcom/google/android/gms/maps/model/n;-><init>(Landroid/os/RemoteException;)V

    throw v1

    .line 497
    :catch_1
    move-exception v0

    new-instance v1, Lcom/google/android/gms/maps/model/n;

    invoke-direct {v1, v0}, Lcom/google/android/gms/maps/model/n;-><init>(Landroid/os/RemoteException;)V

    throw v1
.end method

.method public final l()V
    .locals 8

    .prologue
    .line 533
    invoke-virtual {p0}, Lcom/google/android/location/places/ui/MarkerMapFragment;->c()Lcom/google/android/gms/maps/c;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/maps/c;->c()Landroid/location/Location;

    move-result-object v0

    .line 534
    if-eqz v0, :cond_1

    .line 536
    iget-object v1, p0, Lcom/google/android/location/places/ui/MarkerMapFragment;->w:Lcom/google/android/location/places/ui/ab;

    if-eqz v1, :cond_0

    .line 537
    iget-object v1, p0, Lcom/google/android/location/places/ui/MarkerMapFragment;->w:Lcom/google/android/location/places/ui/ab;

    new-instance v2, Lcom/google/android/gms/maps/model/LatLng;

    invoke-virtual {v0}, Landroid/location/Location;->getLatitude()D

    move-result-wide v4

    invoke-virtual {v0}, Landroid/location/Location;->getLongitude()D

    move-result-wide v6

    invoke-direct {v2, v4, v5, v6, v7}, Lcom/google/android/gms/maps/model/LatLng;-><init>(DD)V

    invoke-interface {v1, v2}, Lcom/google/android/location/places/ui/ab;->a(Lcom/google/android/gms/maps/model/LatLng;)V

    .line 602
    :cond_0
    :goto_0
    return-void

    .line 543
    :cond_1
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    .line 545
    invoke-virtual {p0}, Lcom/google/android/location/places/ui/MarkerMapFragment;->c()Lcom/google/android/gms/maps/c;

    move-result-object v1

    new-instance v2, Lcom/google/android/location/places/ui/z;

    invoke-direct {v2, p0, v0}, Lcom/google/android/location/places/ui/z;-><init>(Lcom/google/android/location/places/ui/MarkerMapFragment;Ljava/util/concurrent/atomic/AtomicBoolean;)V

    invoke-virtual {v1, v2}, Lcom/google/android/gms/maps/c;->a(Lcom/google/android/gms/maps/k;)V

    .line 562
    new-instance v1, Lcom/google/android/location/places/ui/aa;

    invoke-direct {v1, p0, v0}, Lcom/google/android/location/places/ui/aa;-><init>(Lcom/google/android/location/places/ui/MarkerMapFragment;Ljava/util/concurrent/atomic/AtomicBoolean;)V

    .line 600
    new-instance v2, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-direct {v2, v0}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    sget-object v0, Lcom/google/android/location/x;->R:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-virtual {v2, v1, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 6

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 198
    invoke-super {p0, p1}, Lcom/google/android/gms/maps/q;->onActivityCreated(Landroid/os/Bundle;)V

    .line 200
    new-instance v0, Landroid/support/v4/view/q;

    invoke-virtual {p0}, Lcom/google/android/location/places/ui/MarkerMapFragment;->getActivity()Landroid/support/v4/app/q;

    move-result-object v3

    invoke-direct {v0, v3, p0}, Landroid/support/v4/view/q;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;)V

    iput-object v0, p0, Lcom/google/android/location/places/ui/MarkerMapFragment;->o:Landroid/support/v4/view/q;

    .line 201
    iget-object v0, p0, Lcom/google/android/location/places/ui/MarkerMapFragment;->o:Landroid/support/v4/view/q;

    iget-object v0, v0, Landroid/support/v4/view/q;->a:Landroid/support/v4/view/r;

    invoke-interface {v0, p0}, Landroid/support/v4/view/r;->a(Landroid/view/GestureDetector$OnDoubleTapListener;)V

    .line 203
    sget v0, Lcom/google/android/gms/h;->de:I

    invoke-static {v0}, Lcom/google/android/gms/maps/model/b;->a(I)Lcom/google/android/gms/maps/model/a;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/places/ui/MarkerMapFragment;->c:Lcom/google/android/gms/maps/model/a;

    .line 204
    invoke-virtual {p0}, Lcom/google/android/location/places/ui/MarkerMapFragment;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    sget v3, Lcom/google/android/gms/j;->ot:I

    invoke-virtual {v0, v3}, Landroid/support/v4/app/q;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/places/ui/MarkerMapFragment;->k:Landroid/view/View;

    .line 205
    invoke-virtual {p0}, Lcom/google/android/location/places/ui/MarkerMapFragment;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    sget v3, Lcom/google/android/gms/j;->oq:I

    invoke-virtual {v0, v3}, Landroid/support/v4/app/q;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/places/ui/MarkerMapFragment;->g:Landroid/view/View;

    .line 206
    invoke-virtual {p0}, Lcom/google/android/location/places/ui/MarkerMapFragment;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    sget v3, Lcom/google/android/gms/j;->op:I

    invoke-virtual {v0, v3}, Landroid/support/v4/app/q;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/places/ui/MarkerMapFragment;->h:Landroid/view/View;

    .line 207
    invoke-virtual {p0}, Lcom/google/android/location/places/ui/MarkerMapFragment;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    sget v3, Lcom/google/android/gms/j;->or:I

    invoke-virtual {v0, v3}, Landroid/support/v4/app/q;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/location/places/ui/MarkerMapFragment;->i:Landroid/widget/ImageView;

    .line 208
    invoke-virtual {p0}, Lcom/google/android/location/places/ui/MarkerMapFragment;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    sget v3, Lcom/google/android/gms/j;->ou:I

    invoke-virtual {v0, v3}, Landroid/support/v4/app/q;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/places/ui/MarkerMapFragment;->j:Landroid/view/View;

    .line 210
    iget-object v0, p0, Lcom/google/android/location/places/ui/MarkerMapFragment;->k:Landroid/view/View;

    new-instance v3, Lcom/google/android/location/places/ui/x;

    invoke-direct {v3, p0}, Lcom/google/android/location/places/ui/x;-><init>(Lcom/google/android/location/places/ui/MarkerMapFragment;)V

    invoke-virtual {v0, v3}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 226
    iget-object v0, p0, Lcom/google/android/location/places/ui/MarkerMapFragment;->e:Landroid/widget/FrameLayout;

    invoke-virtual {v0, p0}, Landroid/widget/FrameLayout;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 228
    iget-object v0, p0, Lcom/google/android/location/places/ui/MarkerMapFragment;->k:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 231
    invoke-virtual {p0}, Lcom/google/android/location/places/ui/MarkerMapFragment;->c()Lcom/google/android/gms/maps/c;

    move-result-object v0

    :try_start_0
    iget-object v0, v0, Lcom/google/android/gms/maps/c;->a:Lcom/google/android/gms/maps/internal/k;

    const/4 v3, 0x1

    invoke-interface {v0, v3}, Lcom/google/android/gms/maps/internal/k;->d(Z)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 232
    invoke-virtual {p0}, Lcom/google/android/location/places/ui/MarkerMapFragment;->c()Lcom/google/android/gms/maps/c;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/maps/c;->b()Z

    .line 234
    invoke-virtual {p0}, Lcom/google/android/location/places/ui/MarkerMapFragment;->c()Lcom/google/android/gms/maps/c;

    move-result-object v0

    :try_start_1
    iget-object v0, v0, Lcom/google/android/gms/maps/c;->a:Lcom/google/android/gms/maps/internal/k;

    const/4 v3, 0x1

    invoke-interface {v0, v3}, Lcom/google/android/gms/maps/internal/k;->c(Z)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_1

    .line 236
    invoke-virtual {p0}, Lcom/google/android/location/places/ui/MarkerMapFragment;->c()Lcom/google/android/gms/maps/c;

    move-result-object v0

    if-nez p0, :cond_3

    :try_start_2
    iget-object v0, v0, Lcom/google/android/gms/maps/c;->a:Lcom/google/android/gms/maps/internal/k;

    const/4 v3, 0x0

    invoke-interface {v0, v3}, Lcom/google/android/gms/maps/internal/k;->a(Lcom/google/android/gms/maps/internal/ad;)V
    :try_end_2
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2} :catch_2

    .line 237
    :goto_0
    invoke-virtual {p0}, Lcom/google/android/location/places/ui/MarkerMapFragment;->c()Lcom/google/android/gms/maps/c;

    move-result-object v0

    if-nez p0, :cond_4

    :try_start_3
    iget-object v0, v0, Lcom/google/android/gms/maps/c;->a:Lcom/google/android/gms/maps/internal/k;

    const/4 v3, 0x0

    invoke-interface {v0, v3}, Lcom/google/android/gms/maps/internal/k;->a(Lcom/google/android/gms/maps/internal/bb;)V
    :try_end_3
    .catch Landroid/os/RemoteException; {:try_start_3 .. :try_end_3} :catch_3

    .line 238
    :goto_1
    invoke-virtual {p0}, Lcom/google/android/location/places/ui/MarkerMapFragment;->c()Lcom/google/android/gms/maps/c;

    move-result-object v0

    if-nez p0, :cond_5

    :try_start_4
    iget-object v0, v0, Lcom/google/android/gms/maps/c;->a:Lcom/google/android/gms/maps/internal/k;

    const/4 v3, 0x0

    invoke-interface {v0, v3}, Lcom/google/android/gms/maps/internal/k;->a(Lcom/google/android/gms/maps/internal/ap;)V
    :try_end_4
    .catch Landroid/os/RemoteException; {:try_start_4 .. :try_end_4} :catch_4

    .line 239
    :goto_2
    invoke-virtual {p0}, Lcom/google/android/location/places/ui/MarkerMapFragment;->c()Lcom/google/android/gms/maps/c;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/maps/c;->d()Lcom/google/android/gms/maps/u;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/maps/u;->a()V

    .line 240
    invoke-virtual {p0}, Lcom/google/android/location/places/ui/MarkerMapFragment;->c()Lcom/google/android/gms/maps/c;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/maps/c;->d()Lcom/google/android/gms/maps/u;

    move-result-object v0

    :try_start_5
    iget-object v0, v0, Lcom/google/android/gms/maps/u;->a:Lcom/google/android/gms/maps/internal/co;

    const/4 v3, 0x0

    invoke-interface {v0, v3}, Lcom/google/android/gms/maps/internal/co;->e(Z)V
    :try_end_5
    .catch Landroid/os/RemoteException; {:try_start_5 .. :try_end_5} :catch_5

    .line 241
    invoke-virtual {p0}, Lcom/google/android/location/places/ui/MarkerMapFragment;->c()Lcom/google/android/gms/maps/c;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/maps/c;->d()Lcom/google/android/gms/maps/u;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/maps/u;->a()V

    .line 242
    invoke-virtual {p0}, Lcom/google/android/location/places/ui/MarkerMapFragment;->c()Lcom/google/android/gms/maps/c;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/maps/c;->d()Lcom/google/android/gms/maps/u;

    move-result-object v3

    iget-boolean v0, p0, Lcom/google/android/location/places/ui/MarkerMapFragment;->z:Z

    if-nez v0, :cond_6

    move v0, v1

    :goto_3
    :try_start_6
    iget-object v1, v3, Lcom/google/android/gms/maps/u;->a:Lcom/google/android/gms/maps/internal/co;

    invoke-interface {v1, v0}, Lcom/google/android/gms/maps/internal/co;->m(Z)V
    :try_end_6
    .catch Landroid/os/RemoteException; {:try_start_6 .. :try_end_6} :catch_6

    .line 247
    iget-object v0, p0, Lcom/google/android/location/places/ui/MarkerMapFragment;->f:Landroid/view/View;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 249
    if-eqz v0, :cond_0

    .line 250
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 255
    :cond_0
    iget-boolean v0, p0, Lcom/google/android/location/places/ui/MarkerMapFragment;->z:Z

    if-nez v0, :cond_1

    .line 257
    iget-object v0, p0, Lcom/google/android/location/places/ui/MarkerMapFragment;->f:Landroid/view/View;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/places/ui/MarkerMapFragment;->l:Landroid/view/View;

    .line 258
    iget-object v0, p0, Lcom/google/android/location/places/ui/MarkerMapFragment;->l:Landroid/view/View;

    if-eqz v0, :cond_1

    .line 259
    iget-object v0, p0, Lcom/google/android/location/places/ui/MarkerMapFragment;->l:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout$LayoutParams;

    .line 262
    iget v1, v0, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    iget v2, v0, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    iget v3, v0, Landroid/widget/RelativeLayout$LayoutParams;->rightMargin:I

    const/high16 v4, 0x42400000    # 48.0f

    invoke-virtual {p0}, Lcom/google/android/location/places/ui/MarkerMapFragment;->getActivity()Landroid/support/v4/app/q;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/android/location/places/ui/bm;->a(FLandroid/content/Context;)I

    move-result v4

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    .line 272
    :cond_1
    if-eqz p1, :cond_2

    .line 273
    const-string v0, "last_camera_position"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/maps/model/CameraPosition;

    .line 275
    if-eqz v0, :cond_2

    .line 276
    invoke-virtual {p0}, Lcom/google/android/location/places/ui/MarkerMapFragment;->getView()Landroid/view/View;

    move-result-object v1

    new-instance v2, Lcom/google/android/location/places/ui/y;

    invoke-direct {v2, p0, v0}, Lcom/google/android/location/places/ui/y;-><init>(Lcom/google/android/location/places/ui/MarkerMapFragment;Lcom/google/android/gms/maps/model/CameraPosition;)V

    invoke-virtual {v1}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    new-instance v3, Lcom/google/android/location/places/ui/bn;

    invoke-direct {v3, v1, v2}, Lcom/google/android/location/places/ui/bn;-><init>(Landroid/view/View;Ljava/lang/Runnable;)V

    invoke-virtual {v0, v3}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 284
    :cond_2
    return-void

    .line 231
    :catch_0
    move-exception v0

    new-instance v1, Lcom/google/android/gms/maps/model/n;

    invoke-direct {v1, v0}, Lcom/google/android/gms/maps/model/n;-><init>(Landroid/os/RemoteException;)V

    throw v1

    .line 234
    :catch_1
    move-exception v0

    new-instance v1, Lcom/google/android/gms/maps/model/n;

    invoke-direct {v1, v0}, Lcom/google/android/gms/maps/model/n;-><init>(Landroid/os/RemoteException;)V

    throw v1

    .line 236
    :cond_3
    :try_start_7
    iget-object v3, v0, Lcom/google/android/gms/maps/c;->a:Lcom/google/android/gms/maps/internal/k;

    new-instance v4, Lcom/google/android/gms/maps/e;

    invoke-direct {v4, v0, p0}, Lcom/google/android/gms/maps/e;-><init>(Lcom/google/android/gms/maps/c;Lcom/google/android/gms/maps/h;)V

    invoke-interface {v3, v4}, Lcom/google/android/gms/maps/internal/k;->a(Lcom/google/android/gms/maps/internal/ad;)V
    :try_end_7
    .catch Landroid/os/RemoteException; {:try_start_7 .. :try_end_7} :catch_2

    goto/16 :goto_0

    :catch_2
    move-exception v0

    new-instance v1, Lcom/google/android/gms/maps/model/n;

    invoke-direct {v1, v0}, Lcom/google/android/gms/maps/model/n;-><init>(Landroid/os/RemoteException;)V

    throw v1

    .line 237
    :cond_4
    :try_start_8
    iget-object v3, v0, Lcom/google/android/gms/maps/c;->a:Lcom/google/android/gms/maps/internal/k;

    new-instance v4, Lcom/google/android/gms/maps/g;

    invoke-direct {v4, v0, p0}, Lcom/google/android/gms/maps/g;-><init>(Lcom/google/android/gms/maps/c;Lcom/google/android/gms/maps/j;)V

    invoke-interface {v3, v4}, Lcom/google/android/gms/maps/internal/k;->a(Lcom/google/android/gms/maps/internal/bb;)V
    :try_end_8
    .catch Landroid/os/RemoteException; {:try_start_8 .. :try_end_8} :catch_3

    goto/16 :goto_1

    :catch_3
    move-exception v0

    new-instance v1, Lcom/google/android/gms/maps/model/n;

    invoke-direct {v1, v0}, Lcom/google/android/gms/maps/model/n;-><init>(Landroid/os/RemoteException;)V

    throw v1

    .line 238
    :cond_5
    :try_start_9
    iget-object v3, v0, Lcom/google/android/gms/maps/c;->a:Lcom/google/android/gms/maps/internal/k;

    new-instance v4, Lcom/google/android/gms/maps/f;

    invoke-direct {v4, v0, p0}, Lcom/google/android/gms/maps/f;-><init>(Lcom/google/android/gms/maps/c;Lcom/google/android/gms/maps/i;)V

    invoke-interface {v3, v4}, Lcom/google/android/gms/maps/internal/k;->a(Lcom/google/android/gms/maps/internal/ap;)V
    :try_end_9
    .catch Landroid/os/RemoteException; {:try_start_9 .. :try_end_9} :catch_4

    goto/16 :goto_2

    :catch_4
    move-exception v0

    new-instance v1, Lcom/google/android/gms/maps/model/n;

    invoke-direct {v1, v0}, Lcom/google/android/gms/maps/model/n;-><init>(Landroid/os/RemoteException;)V

    throw v1

    .line 240
    :catch_5
    move-exception v0

    new-instance v1, Lcom/google/android/gms/maps/model/n;

    invoke-direct {v1, v0}, Lcom/google/android/gms/maps/model/n;-><init>(Landroid/os/RemoteException;)V

    throw v1

    :cond_6
    move v0, v2

    .line 242
    goto/16 :goto_3

    :catch_6
    move-exception v0

    new-instance v1, Lcom/google/android/gms/maps/model/n;

    invoke-direct {v1, v0}, Lcom/google/android/gms/maps/model/n;-><init>(Landroid/os/RemoteException;)V

    throw v1
.end method

.method public onAttach(Landroid/app/Activity;)V
    .locals 0

    .prologue
    .line 182
    invoke-super {p0, p1}, Lcom/google/android/gms/maps/q;->onAttach(Landroid/app/Activity;)V

    .line 184
    invoke-static {p1}, Lcom/google/android/gms/maps/m;->a(Landroid/content/Context;)I

    .line 185
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 665
    iget-boolean v0, p0, Lcom/google/android/location/places/ui/MarkerMapFragment;->y:Z

    if-eqz v0, :cond_0

    .line 671
    :goto_0
    return-void

    .line 669
    :cond_0
    iget-object v0, p0, Lcom/google/android/location/places/ui/MarkerMapFragment;->w:Lcom/google/android/location/places/ui/ab;

    invoke-interface {v0}, Lcom/google/android/location/places/ui/ab;->g()V

    .line 670
    invoke-virtual {p0}, Lcom/google/android/location/places/ui/MarkerMapFragment;->l()V

    goto :goto_0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2

    .prologue
    .line 190
    invoke-super {p0, p1, p2, p3}, Lcom/google/android/gms/maps/q;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/places/ui/MarkerMapFragment;->f:Landroid/view/View;

    .line 191
    new-instance v0, Lcom/google/android/location/places/ui/bl;

    invoke-virtual {p0}, Lcom/google/android/location/places/ui/MarkerMapFragment;->getActivity()Landroid/support/v4/app/q;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/location/places/ui/bl;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/location/places/ui/MarkerMapFragment;->e:Landroid/widget/FrameLayout;

    .line 192
    iget-object v0, p0, Lcom/google/android/location/places/ui/MarkerMapFragment;->e:Landroid/widget/FrameLayout;

    iget-object v1, p0, Lcom/google/android/location/places/ui/MarkerMapFragment;->f:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    .line 193
    iget-object v0, p0, Lcom/google/android/location/places/ui/MarkerMapFragment;->e:Landroid/widget/FrameLayout;

    return-object v0
.end method

.method public onDoubleTap(Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 716
    iget-object v0, p0, Lcom/google/android/location/places/ui/MarkerMapFragment;->w:Lcom/google/android/location/places/ui/ab;

    if-eqz v0, :cond_0

    .line 717
    iget-object v0, p0, Lcom/google/android/location/places/ui/MarkerMapFragment;->w:Lcom/google/android/location/places/ui/ab;

    invoke-interface {v0}, Lcom/google/android/location/places/ui/ab;->b()V

    .line 719
    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public onDoubleTapEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 730
    const/4 v0, 0x0

    return v0
.end method

.method public onDown(Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 736
    const/4 v0, 0x0

    return v0
.end method

.method public onFling(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 1

    .prologue
    .line 708
    iget-object v0, p0, Lcom/google/android/location/places/ui/MarkerMapFragment;->w:Lcom/google/android/location/places/ui/ab;

    if-eqz v0, :cond_0

    .line 709
    iget-object v0, p0, Lcom/google/android/location/places/ui/MarkerMapFragment;->w:Lcom/google/android/location/places/ui/ab;

    invoke-interface {v0}, Lcom/google/android/location/places/ui/ab;->b()V

    .line 711
    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public onLongPress(Landroid/view/MotionEvent;)V
    .locals 1

    .prologue
    .line 701
    iget-object v0, p0, Lcom/google/android/location/places/ui/MarkerMapFragment;->w:Lcom/google/android/location/places/ui/ab;

    if-eqz v0, :cond_0

    .line 702
    iget-object v0, p0, Lcom/google/android/location/places/ui/MarkerMapFragment;->w:Lcom/google/android/location/places/ui/ab;

    invoke-interface {v0}, Lcom/google/android/location/places/ui/ab;->b()V

    .line 704
    :cond_0
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 288
    invoke-super {p0, p1}, Lcom/google/android/gms/maps/q;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 289
    invoke-virtual {p0}, Lcom/google/android/location/places/ui/MarkerMapFragment;->c()Lcom/google/android/gms/maps/c;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/maps/c;->a()Lcom/google/android/gms/maps/model/CameraPosition;

    move-result-object v0

    .line 290
    if-eqz v0, :cond_0

    .line 291
    const-string v1, "last_camera_position"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 293
    :cond_0
    return-void
.end method

.method public onScroll(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 1

    .prologue
    .line 693
    iget-object v0, p0, Lcom/google/android/location/places/ui/MarkerMapFragment;->w:Lcom/google/android/location/places/ui/ab;

    if-eqz v0, :cond_0

    .line 694
    iget-object v0, p0, Lcom/google/android/location/places/ui/MarkerMapFragment;->w:Lcom/google/android/location/places/ui/ab;

    invoke-interface {v0}, Lcom/google/android/location/places/ui/ab;->b()V

    .line 696
    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public onShowPress(Landroid/view/MotionEvent;)V
    .locals 0

    .prologue
    .line 742
    return-void
.end method

.method public onSingleTapConfirmed(Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 724
    const/4 v0, 0x0

    return v0
.end method

.method public onSingleTapUp(Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 747
    const/4 v0, 0x0

    return v0
.end method

.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 676
    iget-boolean v1, p0, Lcom/google/android/location/places/ui/MarkerMapFragment;->y:Z

    if-eqz v1, :cond_0

    .line 687
    :goto_0
    return v0

    .line 680
    :cond_0
    iget-object v1, p0, Lcom/google/android/location/places/ui/MarkerMapFragment;->o:Landroid/support/v4/view/q;

    invoke-virtual {v1, p2}, Landroid/support/v4/view/q;->a(Landroid/view/MotionEvent;)Z

    .line 681
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    if-ne v1, v0, :cond_1

    .line 682
    iget-object v0, p0, Lcom/google/android/location/places/ui/MarkerMapFragment;->w:Lcom/google/android/location/places/ui/ab;

    if-eqz v0, :cond_1

    .line 683
    iget-object v0, p0, Lcom/google/android/location/places/ui/MarkerMapFragment;->w:Lcom/google/android/location/places/ui/ab;

    invoke-interface {v0}, Lcom/google/android/location/places/ui/ab;->c()V

    .line 687
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
