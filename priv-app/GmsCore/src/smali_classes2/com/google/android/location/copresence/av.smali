.class final Lcom/google/android/location/copresence/av;
.super Landroid/os/AsyncTask;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/location/copresence/at;

.field private final b:Landroid/accounts/Account;

.field private final c:Lcom/google/android/location/copresence/n/m;


# direct methods
.method public constructor <init>(Lcom/google/android/location/copresence/at;Landroid/accounts/Account;Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 91
    iput-object p1, p0, Lcom/google/android/location/copresence/av;->a:Lcom/google/android/location/copresence/at;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 92
    iput-object p2, p0, Lcom/google/android/location/copresence/av;->b:Landroid/accounts/Account;

    .line 93
    new-instance v0, Lcom/google/android/location/copresence/n/m;

    const/4 v1, 0x0

    invoke-direct {v0, p3, v1}, Lcom/google/android/location/copresence/n/m;-><init>(Landroid/content/Context;Lcom/google/android/location/copresence/n/o;)V

    iput-object v0, p0, Lcom/google/android/location/copresence/av;->c:Lcom/google/android/location/copresence/n/m;

    .line 95
    iget-object v0, p0, Lcom/google/android/location/copresence/av;->c:Lcom/google/android/location/copresence/n/m;

    iget-object v0, v0, Lcom/google/android/location/copresence/n/m;->a:Lcom/google/android/gms/location/reporting/i;

    invoke-virtual {v0}, Lcom/google/android/gms/location/reporting/i;->a()V

    .line 96
    return-void
.end method


# virtual methods
.method protected final synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 86
    iget-object v0, p0, Lcom/google/android/location/copresence/av;->c:Lcom/google/android/location/copresence/n/m;

    iget-object v1, p0, Lcom/google/android/location/copresence/av;->b:Landroid/accounts/Account;

    const-string v2, "copresence_seetings_change"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/location/copresence/n/m;->a(Landroid/accounts/Account;Ljava/lang/String;)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method protected final synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 3

    .prologue
    const/4 v1, 0x2

    .line 86
    check-cast p1, Ljava/lang/Boolean;

    invoke-static {v1}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "LocationReportUploadRequester: Finished with the client. Disconnecting."

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->a(Ljava/lang/String;)I

    :cond_0
    iget-object v0, p0, Lcom/google/android/location/copresence/av;->c:Lcom/google/android/location/copresence/n/m;

    iget-object v0, v0, Lcom/google/android/location/copresence/n/m;->a:Lcom/google/android/gms/location/reporting/i;

    invoke-virtual {v0}, Lcom/google/android/gms/location/reporting/i;->b()V

    invoke-static {v1}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "LocationReportUploadRequester: Result of requesting ULR upload for account "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/location/copresence/av;->b:Landroid/accounts/Account;

    iget-object v1, v1, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " success: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->a(Ljava/lang/String;)I

    :cond_1
    iget-object v0, p0, Lcom/google/android/location/copresence/av;->a:Lcom/google/android/location/copresence/at;

    invoke-static {v0}, Lcom/google/android/location/copresence/at;->b(Lcom/google/android/location/copresence/at;)Ljava/util/HashSet;

    move-result-object v1

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/google/android/location/copresence/av;->a:Lcom/google/android/location/copresence/at;

    invoke-static {v0}, Lcom/google/android/location/copresence/at;->b(Lcom/google/android/location/copresence/at;)Ljava/util/HashSet;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/location/copresence/av;->b:Landroid/accounts/Account;

    invoke-virtual {v0, v2}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method
