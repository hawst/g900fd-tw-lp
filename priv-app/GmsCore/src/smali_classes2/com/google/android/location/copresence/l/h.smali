.class public final Lcom/google/android/location/copresence/l/h;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static g:Lcom/google/android/location/copresence/l/h;


# instance fields
.field public final a:Landroid/content/Context;

.field public final b:Lcom/google/android/location/copresence/a/b;

.field public final c:Lcom/google/android/location/copresence/n/f;

.field public final d:Lcom/google/android/location/copresence/h/c;

.field public final e:Lcom/google/android/location/copresence/h/a;

.field public final f:Lcom/google/android/location/copresence/l/e;


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 63
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 64
    iput-object p1, p0, Lcom/google/android/location/copresence/l/h;->a:Landroid/content/Context;

    .line 65
    invoke-static {p1}, Lcom/google/android/location/copresence/a/b;->a(Landroid/content/Context;)Lcom/google/android/location/copresence/a/b;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/copresence/l/h;->b:Lcom/google/android/location/copresence/a/b;

    .line 66
    invoke-static {p1}, Lcom/google/android/location/copresence/n/f;->a(Landroid/content/Context;)Lcom/google/android/location/copresence/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/copresence/l/h;->c:Lcom/google/android/location/copresence/n/f;

    .line 67
    invoke-static {p1}, Lcom/google/android/location/copresence/h/c;->a(Landroid/content/Context;)Lcom/google/android/location/copresence/h/c;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/copresence/l/h;->d:Lcom/google/android/location/copresence/h/c;

    .line 68
    sget-object v0, Lcom/google/android/location/copresence/h/a;->b:Lcom/google/android/location/copresence/h/a;

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/android/location/copresence/h/a;

    invoke-direct {v0, p1}, Lcom/google/android/location/copresence/h/a;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/google/android/location/copresence/h/a;->b:Lcom/google/android/location/copresence/h/a;

    :cond_0
    sget-object v0, Lcom/google/android/location/copresence/h/a;->b:Lcom/google/android/location/copresence/h/a;

    iput-object v0, p0, Lcom/google/android/location/copresence/l/h;->e:Lcom/google/android/location/copresence/h/a;

    .line 69
    invoke-static {p1}, Lcom/google/android/location/copresence/l/e;->a(Landroid/content/Context;)Lcom/google/android/location/copresence/l/e;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/copresence/l/h;->f:Lcom/google/android/location/copresence/l/e;

    .line 70
    return-void
.end method

.method public static a(Landroid/os/Bundle;)Lcom/google/ac/b/c/co;
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 222
    const-string v1, "PUSH_MESSAGE"

    invoke-virtual {p0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 223
    if-nez v2, :cond_1

    .line 235
    :cond_0
    :goto_0
    return-object v0

    .line 226
    :cond_1
    new-instance v1, Lcom/google/ac/b/c/co;

    invoke-direct {v1}, Lcom/google/ac/b/c/co;-><init>()V

    .line 228
    :try_start_0
    invoke-static {v2}, Lcom/google/android/location/copresence/l/h;->a(Ljava/lang/String;)[B

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/j;->mergeFrom(Lcom/google/protobuf/nano/j;[B)Lcom/google/protobuf/nano/j;
    :try_end_0
    .catch Lcom/google/protobuf/nano/i; {:try_start_0 .. :try_end_0} :catch_0

    move-object v0, v1

    .line 235
    goto :goto_0

    .line 229
    :catch_0
    move-exception v1

    .line 230
    const/4 v2, 0x6

    invoke-static {v2}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 231
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "GcmHandler: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/location/copresence/ag;->e(Ljava/lang/String;)I

    goto :goto_0
.end method

.method public static declared-synchronized a(Landroid/content/Context;)Lcom/google/android/location/copresence/l/h;
    .locals 2

    .prologue
    .line 57
    const-class v1, Lcom/google/android/location/copresence/l/h;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/google/android/location/copresence/l/h;->g:Lcom/google/android/location/copresence/l/h;

    if-nez v0, :cond_0

    .line 58
    new-instance v0, Lcom/google/android/location/copresence/l/h;

    invoke-direct {v0, p0}, Lcom/google/android/location/copresence/l/h;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/google/android/location/copresence/l/h;->g:Lcom/google/android/location/copresence/l/h;

    .line 60
    :cond_0
    sget-object v0, Lcom/google/android/location/copresence/l/h;->g:Lcom/google/android/location/copresence/l/h;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 57
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private static a(Ljava/lang/String;)[B
    .locals 4

    .prologue
    .line 280
    const/16 v0, 0xb

    :try_start_0
    invoke-static {p0, v0}, Landroid/util/Base64;->decode(Ljava/lang/String;I)[B
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    return-object v0

    .line 281
    :catch_0
    move-exception v0

    .line 282
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Bad base64 token received from server: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method
