.class final Lcom/google/android/location/places/ui/av;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final k:Lcom/google/android/gms/location/places/PlaceFilter;


# instance fields
.field private final a:Lcom/google/android/gms/common/api/v;

.field private final b:Lcom/google/android/gms/location/places/PlaceFilter;

.field private c:Lcom/google/android/location/places/ui/ay;

.field private final d:Ljava/util/concurrent/Executor;

.field private final e:Ljava/lang/Object;

.field private volatile f:Lcom/google/android/location/places/ui/az;

.field private volatile g:Lcom/google/android/location/places/ui/az;

.field private volatile h:Lcom/google/android/location/places/ui/az;

.field private volatile i:Lcom/google/android/location/places/ui/az;

.field private volatile j:Lcom/google/android/location/places/ui/az;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 75
    invoke-static {}, Lcom/google/android/gms/location/places/PlaceFilter;->a()Lcom/google/android/gms/location/places/g;

    move-result-object v0

    const/4 v1, 0x1

    new-array v1, v1, [Lcom/google/android/gms/location/places/PlaceType;

    const/4 v2, 0x0

    sget-object v3, Lcom/google/android/gms/location/places/PlaceType;->aY:Lcom/google/android/gms/location/places/PlaceType;

    aput-object v3, v1, v2

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/google/android/gms/location/places/g;->a:Ljava/util/Collection;

    invoke-virtual {v0}, Lcom/google/android/gms/location/places/g;->a()Lcom/google/android/gms/location/places/PlaceFilter;

    move-result-object v0

    sput-object v0, Lcom/google/android/location/places/ui/av;->k:Lcom/google/android/gms/location/places/PlaceFilter;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/android/location/places/ui/s;)V
    .locals 2

    .prologue
    .line 78
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 65
    invoke-static {}, Ljava/util/concurrent/Executors;->newSingleThreadExecutor()Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/places/ui/av;->d:Ljava/util/concurrent/Executor;

    .line 67
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/location/places/ui/av;->e:Ljava/lang/Object;

    .line 79
    new-instance v0, Lcom/google/android/gms/common/api/w;

    invoke-direct {v0, p1}, Lcom/google/android/gms/common/api/w;-><init>(Landroid/content/Context;)V

    .line 80
    iget-object v1, p2, Lcom/google/android/location/places/ui/s;->l:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 81
    iget-object v1, p2, Lcom/google/android/location/places/ui/s;->l:Ljava/lang/String;

    iput-object v1, v0, Lcom/google/android/gms/common/api/w;->a:Ljava/lang/String;

    .line 83
    :cond_0
    iget-object v1, p2, Lcom/google/android/location/places/ui/s;->a:Ljava/lang/String;

    invoke-static {v1}, Lcom/google/android/gms/location/places/q;->a(Ljava/lang/String;)Lcom/google/android/gms/common/api/c;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/api/w;->a(Lcom/google/android/gms/common/api/c;)Lcom/google/android/gms/common/api/w;

    .line 84
    invoke-virtual {v0}, Lcom/google/android/gms/common/api/w;->a()Lcom/google/android/gms/common/api/v;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/places/ui/av;->a:Lcom/google/android/gms/common/api/v;

    .line 85
    iget-object v0, p2, Lcom/google/android/location/places/ui/s;->d:Lcom/google/android/gms/location/places/PlaceFilter;

    iput-object v0, p0, Lcom/google/android/location/places/ui/av;->b:Lcom/google/android/gms/location/places/PlaceFilter;

    .line 86
    return-void
.end method

.method static synthetic a(Lcom/google/android/location/places/ui/av;)Lcom/google/android/location/places/ui/ay;
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lcom/google/android/location/places/ui/av;->c:Lcom/google/android/location/places/ui/ay;

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/gms/location/places/i;)[Lcom/google/android/gms/location/places/f;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 40
    if-nez p0, :cond_0

    new-array v0, v0, [Lcom/google/android/gms/location/places/f;

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/location/places/i;->c()I

    move-result v1

    new-array v1, v1, [Lcom/google/android/gms/location/places/f;

    :goto_1
    invoke-virtual {p0}, Lcom/google/android/gms/location/places/i;->c()I

    move-result v2

    if-ge v0, v2, :cond_1

    invoke-virtual {p0, v0}, Lcom/google/android/gms/location/places/i;->c(I)Lcom/google/android/gms/location/places/PlaceLikelihood;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/gms/location/places/PlaceLikelihood;->b()Lcom/google/android/gms/location/places/f;

    move-result-object v2

    aput-object v2, v1, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_1
    move-object v0, v1

    goto :goto_0
.end method

.method static synthetic b(Lcom/google/android/location/places/ui/av;)Lcom/google/android/gms/common/api/v;
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lcom/google/android/location/places/ui/av;->a:Lcom/google/android/gms/common/api/v;

    return-object v0
.end method

.method static synthetic b(Lcom/google/android/gms/location/places/i;)Lcom/google/android/gms/location/places/f;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 40
    if-nez p0, :cond_1

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/gms/location/places/i;->iterator()Ljava/util/Iterator;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/location/places/PlaceLikelihood;

    invoke-virtual {v0}, Lcom/google/android/gms/location/places/PlaceLikelihood;->b()Lcom/google/android/gms/location/places/f;

    move-result-object v0

    goto :goto_0
.end method

.method static synthetic c(Lcom/google/android/location/places/ui/av;)Lcom/google/android/gms/location/places/PlaceFilter;
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lcom/google/android/location/places/ui/av;->b:Lcom/google/android/gms/location/places/PlaceFilter;

    return-object v0
.end method

.method static synthetic f()Lcom/google/android/gms/location/places/PlaceFilter;
    .locals 1

    .prologue
    .line 40
    sget-object v0, Lcom/google/android/location/places/ui/av;->k:Lcom/google/android/gms/location/places/PlaceFilter;

    return-object v0
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 98
    iget-object v1, p0, Lcom/google/android/location/places/ui/av;->e:Ljava/lang/Object;

    monitor-enter v1

    .line 99
    :try_start_0
    iget-object v0, p0, Lcom/google/android/location/places/ui/av;->f:Lcom/google/android/location/places/ui/az;

    if-eqz v0, :cond_0

    .line 100
    iget-object v0, p0, Lcom/google/android/location/places/ui/av;->f:Lcom/google/android/location/places/ui/az;

    invoke-virtual {v0}, Lcom/google/android/location/places/ui/az;->a()V

    .line 103
    :cond_0
    iget-object v0, p0, Lcom/google/android/location/places/ui/av;->g:Lcom/google/android/location/places/ui/az;

    if-eqz v0, :cond_1

    .line 104
    iget-object v0, p0, Lcom/google/android/location/places/ui/av;->g:Lcom/google/android/location/places/ui/az;

    invoke-virtual {v0}, Lcom/google/android/location/places/ui/az;->a()V

    .line 107
    :cond_1
    iget-object v0, p0, Lcom/google/android/location/places/ui/av;->h:Lcom/google/android/location/places/ui/az;

    if-eqz v0, :cond_2

    .line 108
    iget-object v0, p0, Lcom/google/android/location/places/ui/av;->h:Lcom/google/android/location/places/ui/az;

    invoke-virtual {v0}, Lcom/google/android/location/places/ui/az;->a()V

    .line 111
    :cond_2
    iget-object v0, p0, Lcom/google/android/location/places/ui/av;->i:Lcom/google/android/location/places/ui/az;

    if-eqz v0, :cond_3

    .line 112
    iget-object v0, p0, Lcom/google/android/location/places/ui/av;->i:Lcom/google/android/location/places/ui/az;

    invoke-virtual {v0}, Lcom/google/android/location/places/ui/az;->a()V

    .line 115
    :cond_3
    iget-object v0, p0, Lcom/google/android/location/places/ui/av;->j:Lcom/google/android/location/places/ui/az;

    if-eqz v0, :cond_4

    .line 116
    iget-object v0, p0, Lcom/google/android/location/places/ui/av;->j:Lcom/google/android/location/places/ui/az;

    invoke-virtual {v0}, Lcom/google/android/location/places/ui/az;->a()V

    .line 118
    :cond_4
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final a(Lcom/google/android/gms/location/places/UserAddedPlace;)V
    .locals 2

    .prologue
    .line 202
    iget-object v0, p0, Lcom/google/android/location/places/ui/av;->d:Ljava/util/concurrent/Executor;

    new-instance v1, Lcom/google/android/location/places/ui/aw;

    invoke-direct {v1, p0, p1}, Lcom/google/android/location/places/ui/aw;-><init>(Lcom/google/android/location/places/ui/av;Lcom/google/android/gms/location/places/UserAddedPlace;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 203
    return-void
.end method

.method public final a(Lcom/google/android/gms/maps/model/LatLng;)V
    .locals 3

    .prologue
    .line 158
    new-instance v0, Lcom/google/android/location/places/ui/ba;

    invoke-direct {v0, p0, p1}, Lcom/google/android/location/places/ui/ba;-><init>(Lcom/google/android/location/places/ui/av;Lcom/google/android/gms/maps/model/LatLng;)V

    .line 159
    iget-object v1, p0, Lcom/google/android/location/places/ui/av;->e:Ljava/lang/Object;

    monitor-enter v1

    .line 161
    :try_start_0
    iget-object v2, p0, Lcom/google/android/location/places/ui/av;->h:Lcom/google/android/location/places/ui/az;

    if-eqz v2, :cond_0

    .line 162
    iget-object v2, p0, Lcom/google/android/location/places/ui/av;->h:Lcom/google/android/location/places/ui/az;

    invoke-virtual {v2}, Lcom/google/android/location/places/ui/az;->a()V

    .line 164
    :cond_0
    iput-object v0, p0, Lcom/google/android/location/places/ui/av;->h:Lcom/google/android/location/places/ui/az;

    .line 165
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 166
    iget-object v1, p0, Lcom/google/android/location/places/ui/av;->d:Ljava/util/concurrent/Executor;

    invoke-interface {v1, v0}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 167
    return-void

    .line 165
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final declared-synchronized a(Lcom/google/android/location/places/ui/ay;)V
    .locals 1

    .prologue
    .line 93
    monitor-enter p0

    :try_start_0
    iput-object p1, p0, Lcom/google/android/location/places/ui/av;->c:Lcom/google/android/location/places/ui/ay;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 94
    monitor-exit p0

    return-void

    .line 93
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final a(Ljava/lang/String;Lcom/google/android/gms/maps/model/LatLngBounds;)V
    .locals 3

    .prologue
    .line 134
    new-instance v0, Lcom/google/android/location/places/ui/bb;

    invoke-direct {v0, p0, p1, p2}, Lcom/google/android/location/places/ui/bb;-><init>(Lcom/google/android/location/places/ui/av;Ljava/lang/String;Lcom/google/android/gms/maps/model/LatLngBounds;)V

    .line 135
    iget-object v1, p0, Lcom/google/android/location/places/ui/av;->e:Ljava/lang/Object;

    monitor-enter v1

    .line 137
    :try_start_0
    iget-object v2, p0, Lcom/google/android/location/places/ui/av;->f:Lcom/google/android/location/places/ui/az;

    if-eqz v2, :cond_0

    .line 138
    iget-object v2, p0, Lcom/google/android/location/places/ui/av;->f:Lcom/google/android/location/places/ui/az;

    invoke-virtual {v2}, Lcom/google/android/location/places/ui/az;->a()V

    .line 140
    :cond_0
    iput-object v0, p0, Lcom/google/android/location/places/ui/av;->f:Lcom/google/android/location/places/ui/az;

    .line 141
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 142
    iget-object v1, p0, Lcom/google/android/location/places/ui/av;->d:Ljava/util/concurrent/Executor;

    invoke-interface {v1, v0}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 143
    return-void

    .line 141
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/location/places/PlaceType;Lcom/google/android/gms/maps/model/LatLng;)V
    .locals 9

    .prologue
    const/4 v1, 0x0

    .line 183
    new-instance v8, Lcom/google/android/location/places/ui/aw;

    invoke-static {}, Lcom/google/android/gms/location/places/UserAddedPlace;->a()Lcom/google/android/gms/location/places/ao;

    move-result-object v7

    iput-object p1, v7, Lcom/google/android/gms/location/places/ao;->a:Ljava/lang/String;

    iput-object p2, v7, Lcom/google/android/gms/location/places/ao;->c:Ljava/lang/String;

    iput-object p3, v7, Lcom/google/android/gms/location/places/ao;->e:Ljava/lang/String;

    iput-object p4, v7, Lcom/google/android/gms/location/places/ao;->f:Ljava/lang/String;

    const/4 v0, 0x1

    new-array v0, v0, [Lcom/google/android/gms/location/places/PlaceType;

    aput-object p5, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    iput-object v0, v7, Lcom/google/android/gms/location/places/ao;->d:Ljava/util/List;

    iput-object p6, v7, Lcom/google/android/gms/location/places/ao;->b:Lcom/google/android/gms/maps/model/LatLng;

    new-instance v0, Lcom/google/android/gms/location/places/UserAddedPlace;

    iget-object v2, v7, Lcom/google/android/gms/location/places/ao;->a:Ljava/lang/String;

    iget-object v3, v7, Lcom/google/android/gms/location/places/ao;->b:Lcom/google/android/gms/maps/model/LatLng;

    iget-object v4, v7, Lcom/google/android/gms/location/places/ao;->c:Ljava/lang/String;

    iget-object v5, v7, Lcom/google/android/gms/location/places/ao;->d:Ljava/util/List;

    iget-object v6, v7, Lcom/google/android/gms/location/places/ao;->e:Ljava/lang/String;

    iget-object v7, v7, Lcom/google/android/gms/location/places/ao;->f:Ljava/lang/String;

    invoke-direct/range {v0 .. v7}, Lcom/google/android/gms/location/places/UserAddedPlace;-><init>(ILjava/lang/String;Lcom/google/android/gms/maps/model/LatLng;Ljava/lang/String;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {v8, p0, v0}, Lcom/google/android/location/places/ui/aw;-><init>(Lcom/google/android/location/places/ui/av;Lcom/google/android/gms/location/places/UserAddedPlace;)V

    .line 191
    iget-object v1, p0, Lcom/google/android/location/places/ui/av;->e:Ljava/lang/Object;

    monitor-enter v1

    .line 193
    :try_start_0
    iget-object v0, p0, Lcom/google/android/location/places/ui/av;->j:Lcom/google/android/location/places/ui/az;

    if-eqz v0, :cond_0

    .line 194
    iget-object v0, p0, Lcom/google/android/location/places/ui/av;->j:Lcom/google/android/location/places/ui/az;

    invoke-virtual {v0}, Lcom/google/android/location/places/ui/az;->a()V

    .line 196
    :cond_0
    iput-object v8, p0, Lcom/google/android/location/places/ui/av;->j:Lcom/google/android/location/places/ui/az;

    .line 197
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 198
    iget-object v0, p0, Lcom/google/android/location/places/ui/av;->d:Ljava/util/concurrent/Executor;

    invoke-interface {v0, v8}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 199
    return-void

    .line 197
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final b()Ljava/lang/String;
    .locals 2

    .prologue
    .line 122
    sget-object v0, Lcom/google/android/gms/location/places/q;->c:Lcom/google/android/gms/location/places/u;

    iget-object v1, p0, Lcom/google/android/location/places/ui/av;->a:Lcom/google/android/gms/common/api/v;

    invoke-interface {v0}, Lcom/google/android/gms/location/places/u;->a()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final b(Ljava/lang/String;Lcom/google/android/gms/maps/model/LatLngBounds;)V
    .locals 3

    .prologue
    .line 146
    new-instance v0, Lcom/google/android/location/places/ui/bc;

    invoke-direct {v0, p0, p1, p2}, Lcom/google/android/location/places/ui/bc;-><init>(Lcom/google/android/location/places/ui/av;Ljava/lang/String;Lcom/google/android/gms/maps/model/LatLngBounds;)V

    .line 147
    iget-object v1, p0, Lcom/google/android/location/places/ui/av;->e:Ljava/lang/Object;

    monitor-enter v1

    .line 149
    :try_start_0
    iget-object v2, p0, Lcom/google/android/location/places/ui/av;->g:Lcom/google/android/location/places/ui/az;

    if-eqz v2, :cond_0

    .line 150
    iget-object v2, p0, Lcom/google/android/location/places/ui/av;->g:Lcom/google/android/location/places/ui/az;

    invoke-virtual {v2}, Lcom/google/android/location/places/ui/az;->a()V

    .line 152
    :cond_0
    iput-object v0, p0, Lcom/google/android/location/places/ui/av;->g:Lcom/google/android/location/places/ui/az;

    .line 153
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 154
    iget-object v1, p0, Lcom/google/android/location/places/ui/av;->d:Ljava/util/concurrent/Executor;

    invoke-interface {v1, v0}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 155
    return-void

    .line 153
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 126
    iget-object v0, p0, Lcom/google/android/location/places/ui/av;->a:Lcom/google/android/gms/common/api/v;

    invoke-interface {v0}, Lcom/google/android/gms/common/api/v;->b()V

    .line 127
    return-void
.end method

.method public final d()V
    .locals 1

    .prologue
    .line 130
    iget-object v0, p0, Lcom/google/android/location/places/ui/av;->a:Lcom/google/android/gms/common/api/v;

    invoke-interface {v0}, Lcom/google/android/gms/common/api/v;->d()V

    .line 131
    return-void
.end method

.method public final e()V
    .locals 3

    .prologue
    .line 170
    new-instance v0, Lcom/google/android/location/places/ui/ax;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/android/location/places/ui/ax;-><init>(Lcom/google/android/location/places/ui/av;B)V

    .line 171
    iget-object v1, p0, Lcom/google/android/location/places/ui/av;->e:Ljava/lang/Object;

    monitor-enter v1

    .line 173
    :try_start_0
    iget-object v2, p0, Lcom/google/android/location/places/ui/av;->i:Lcom/google/android/location/places/ui/az;

    if-eqz v2, :cond_0

    .line 174
    iget-object v2, p0, Lcom/google/android/location/places/ui/av;->i:Lcom/google/android/location/places/ui/az;

    invoke-virtual {v2}, Lcom/google/android/location/places/ui/az;->a()V

    .line 176
    :cond_0
    iput-object v0, p0, Lcom/google/android/location/places/ui/av;->i:Lcom/google/android/location/places/ui/az;

    .line 177
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 178
    iget-object v1, p0, Lcom/google/android/location/places/ui/av;->d:Ljava/util/concurrent/Executor;

    invoke-interface {v1, v0}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 179
    return-void

    .line 177
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method
