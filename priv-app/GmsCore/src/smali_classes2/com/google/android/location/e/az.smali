.class public final Lcom/google/android/location/e/az;
.super Lcom/google/android/location/e/al;
.source "SourceFile"


# static fields
.field public static final c:Lcom/google/android/location/e/ak;


# instance fields
.field public final a:I

.field public final b:Lcom/google/android/location/e/bb;

.field private i:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 173
    new-instance v0, Lcom/google/android/location/e/ba;

    invoke-direct {v0}, Lcom/google/android/location/e/ba;-><init>()V

    sput-object v0, Lcom/google/android/location/e/az;->c:Lcom/google/android/location/e/ak;

    return-void
.end method

.method public constructor <init>(IIIII)V
    .locals 1

    .prologue
    .line 48
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/android/location/e/al;-><init>(IIII)V

    .line 49
    iput p5, p0, Lcom/google/android/location/e/az;->a:I

    .line 50
    const/16 v0, 0x50

    if-lt p4, v0, :cond_0

    const/16 v0, 0x54

    if-gt p4, v0, :cond_0

    sget-object v0, Lcom/google/android/location/e/bb;->b:Lcom/google/android/location/e/bb;

    :goto_0
    iput-object v0, p0, Lcom/google/android/location/e/az;->b:Lcom/google/android/location/e/bb;

    .line 51
    return-void

    .line 50
    :cond_0
    const/16 v0, 0x55

    if-lt p4, v0, :cond_1

    const/16 v0, 0x59

    if-gt p4, v0, :cond_1

    sget-object v0, Lcom/google/android/location/e/bb;->c:Lcom/google/android/location/e/bb;

    goto :goto_0

    :cond_1
    const/16 v0, 0x5a

    if-lt p4, v0, :cond_2

    const/16 v0, 0x5e

    if-gt p4, v0, :cond_2

    sget-object v0, Lcom/google/android/location/e/bb;->d:Lcom/google/android/location/e/bb;

    goto :goto_0

    :cond_2
    sget-object v0, Lcom/google/android/location/e/bb;->a:Lcom/google/android/location/e/bb;

    goto :goto_0
.end method

.method static synthetic a(Lcom/google/android/location/e/az;)I
    .locals 1

    .prologue
    .line 15
    iget v0, p0, Lcom/google/android/location/e/az;->i:I

    return v0
.end method

.method static synthetic a(Lcom/google/android/location/e/az;I)I
    .locals 0

    .prologue
    .line 15
    iput p1, p0, Lcom/google/android/location/e/az;->i:I

    return p1
.end method

.method public static a(Ljava/lang/StringBuilder;Lcom/google/android/location/e/az;)V
    .locals 1

    .prologue
    .line 146
    if-nez p1, :cond_0

    .line 147
    const-string v0, "null"

    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 157
    :goto_0
    return-void

    .line 150
    :cond_0
    const-string v0, "["

    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 151
    invoke-static {p0, p1}, Lcom/google/android/location/e/al;->a(Ljava/lang/StringBuilder;Lcom/google/android/location/e/al;)V

    .line 152
    const-string v0, ", Uncert="

    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 153
    iget v0, p1, Lcom/google/android/location/e/az;->a:I

    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 154
    const-string v0, "mm, "

    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 155
    iget-object v0, p1, Lcom/google/android/location/e/az;->b:Lcom/google/android/location/e/bb;

    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 156
    const-string v0, "]"

    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0
.end method


# virtual methods
.method public final c()V
    .locals 1

    .prologue
    .line 81
    iget v0, p0, Lcom/google/android/location/e/az;->i:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/location/e/az;->i:I

    .line 82
    return-void
.end method

.method public final d()I
    .locals 1

    .prologue
    .line 89
    iget v0, p0, Lcom/google/android/location/e/az;->i:I

    return v0
.end method

.method public final e()Z
    .locals 1

    .prologue
    .line 99
    iget v0, p0, Lcom/google/android/location/e/az;->i:I

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 141
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "WifiApPosition ["

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-super {p0}, Lcom/google/android/location/e/al;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", horizontalUncertaintyMm="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/location/e/az;->a:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", positionType="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/location/e/az;->b:Lcom/google/android/location/e/bb;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
