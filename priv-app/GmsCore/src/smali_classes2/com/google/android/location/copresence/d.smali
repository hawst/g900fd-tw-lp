.class final Lcom/google/android/location/copresence/d;
.super Landroid/content/BroadcastReceiver;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/location/copresence/c;


# direct methods
.method constructor <init>(Lcom/google/android/location/copresence/c;)V
    .locals 0

    .prologue
    .line 159
    iput-object p1, p0, Lcom/google/android/location/copresence/d;->a:Lcom/google/android/location/copresence/c;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public final onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 3

    .prologue
    .line 167
    const-string v0, "ALARM_ID_EXTRA"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "SESSION_ID_EXTRA"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 168
    :cond_0
    const/4 v0, 0x5

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 169
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "No alarm id or session id found for intent: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->d(Ljava/lang/String;)I

    .line 206
    :cond_1
    :goto_0
    return-void

    .line 173
    :cond_2
    const/4 v0, 0x0

    .line 174
    const-string v1, "WORK_SOURCE_EXTRA"

    invoke-virtual {p2, v1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 175
    const-string v0, "WORK_SOURCE_EXTRA"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/os/WorkSource;

    .line 177
    :cond_3
    iget-object v1, p0, Lcom/google/android/location/copresence/d;->a:Lcom/google/android/location/copresence/c;

    iget-object v1, v1, Lcom/google/android/location/copresence/c;->d:Lcom/google/android/location/copresence/ap;

    new-instance v2, Lcom/google/android/location/copresence/e;

    invoke-direct {v2, p0, p2}, Lcom/google/android/location/copresence/e;-><init>(Lcom/google/android/location/copresence/d;Landroid/content/Intent;)V

    invoke-virtual {v1, v2, v0}, Lcom/google/android/location/copresence/ap;->a(Ljava/lang/Runnable;Landroid/os/WorkSource;)V

    goto :goto_0
.end method
