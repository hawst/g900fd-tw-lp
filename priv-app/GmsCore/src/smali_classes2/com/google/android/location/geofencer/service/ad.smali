.class final Lcom/google/android/location/geofencer/service/ad;
.super Landroid/content/BroadcastReceiver;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/location/geofencer/service/ac;


# direct methods
.method constructor <init>(Lcom/google/android/location/geofencer/service/ac;)V
    .locals 0

    .prologue
    .line 365
    iput-object p1, p0, Lcom/google/android/location/geofencer/service/ad;->a:Lcom/google/android/location/geofencer/service/ac;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public final onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 5

    .prologue
    .line 368
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 369
    const-string v1, "com.google.android.location.intent.action.END_LOCATION_BURST"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 372
    iget-object v0, p0, Lcom/google/android/location/geofencer/service/ad;->a:Lcom/google/android/location/geofencer/service/ac;

    iget-object v1, v0, Lcom/google/android/location/geofencer/service/ac;->c:Ljava/lang/Object;

    monitor-enter v1

    .line 373
    :try_start_0
    iget-object v0, p0, Lcom/google/android/location/geofencer/service/ad;->a:Lcom/google/android/location/geofencer/service/ac;

    iget-boolean v0, v0, Lcom/google/android/location/geofencer/service/ac;->i:Z

    if-eqz v0, :cond_0

    .line 374
    iget-object v0, p0, Lcom/google/android/location/geofencer/service/ad;->a:Lcom/google/android/location/geofencer/service/ac;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Lcom/google/android/location/geofencer/service/ac;->a(Z)V

    .line 375
    iget-object v0, p0, Lcom/google/android/location/geofencer/service/ad;->a:Lcom/google/android/location/geofencer/service/ac;

    const/16 v2, 0x3c

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/android/location/geofencer/service/ad;->a:Lcom/google/android/location/geofencer/service/ac;

    iget-object v4, v4, Lcom/google/android/location/geofencer/service/ac;->g:Ljava/util/Collection;

    invoke-virtual {v0, v2, v3, v4}, Lcom/google/android/location/geofencer/service/ac;->a(IZLjava/util/Collection;)V

    .line 378
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 387
    :cond_1
    :goto_0
    return-void

    .line 378
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 379
    :cond_2
    const-string v1, "com.google.android.location.internal.action.GEOFENCER_LOCATION_RESULT"

    invoke-static {v1}, Lcom/google/android/location/internal/PendingIntentCallbackService;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 381
    const-string v0, "com.google.android.location.LOCATION"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/location/Location;

    .line 383
    if-eqz v0, :cond_1

    .line 384
    iget-object v1, p0, Lcom/google/android/location/geofencer/service/ad;->a:Lcom/google/android/location/geofencer/service/ac;

    iget-boolean v2, v1, Lcom/google/android/location/geofencer/service/ac;->a:Z

    if-eqz v2, :cond_3

    const-string v2, "mock"

    invoke-virtual {v0}, Landroid/location/Location;->getProvider()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    const-string v0, "LocationDetector"

    const-string v1, "Non-mock locations are ignored in testing."

    invoke-static {v0, v1}, Lcom/google/android/location/geofencer/a/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_3
    iget-object v1, v1, Lcom/google/android/location/geofencer/service/ac;->b:Lcom/google/android/location/geofencer/service/k;

    invoke-virtual {v1, v0}, Lcom/google/android/location/geofencer/service/k;->a(Landroid/location/Location;)V

    goto :goto_0
.end method
