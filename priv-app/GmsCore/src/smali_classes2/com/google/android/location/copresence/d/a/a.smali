.class public final Lcom/google/android/location/copresence/d/a/a;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/location/copresence/d/a/m;


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x15
.end annotation


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Landroid/bluetooth/BluetoothManager;

.field private final c:Lcom/google/android/location/copresence/d/e;

.field private final d:Lcom/google/android/location/copresence/d/a/o;

.field private e:Landroid/bluetooth/BluetoothGattServer;

.field private f:Z

.field private g:Lcom/google/android/location/copresence/d/a/c;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/location/copresence/d/e;Lcom/google/android/location/copresence/d/a/o;)V
    .locals 1

    .prologue
    .line 57
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 58
    iput-object p1, p0, Lcom/google/android/location/copresence/d/a/a;->a:Landroid/content/Context;

    .line 59
    const-string v0, "bluetooth"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/bluetooth/BluetoothManager;

    iput-object v0, p0, Lcom/google/android/location/copresence/d/a/a;->b:Landroid/bluetooth/BluetoothManager;

    .line 60
    iput-object p2, p0, Lcom/google/android/location/copresence/d/a/a;->c:Lcom/google/android/location/copresence/d/e;

    .line 61
    iput-object p3, p0, Lcom/google/android/location/copresence/d/a/a;->d:Lcom/google/android/location/copresence/d/a/o;

    .line 62
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/location/copresence/d/a/a;->f:Z

    .line 63
    return-void
.end method

.method private c()V
    .locals 1

    .prologue
    .line 125
    iget-object v0, p0, Lcom/google/android/location/copresence/d/a/a;->e:Landroid/bluetooth/BluetoothGattServer;

    if-eqz v0, :cond_0

    .line 126
    iget-object v0, p0, Lcom/google/android/location/copresence/d/a/a;->e:Landroid/bluetooth/BluetoothGattServer;

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothGattServer;->clearServices()V

    .line 127
    iget-object v0, p0, Lcom/google/android/location/copresence/d/a/a;->e:Landroid/bluetooth/BluetoothGattServer;

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothGattServer;->close()V

    .line 128
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/location/copresence/d/a/a;->e:Landroid/bluetooth/BluetoothGattServer;

    .line 130
    :cond_0
    return-void
.end method

.method private d()Landroid/bluetooth/le/BluetoothLeAdvertiser;
    .locals 3

    .prologue
    .line 203
    iget-object v0, p0, Lcom/google/android/location/copresence/d/a/a;->c:Lcom/google/android/location/copresence/d/e;

    iget-object v0, v0, Lcom/google/android/location/copresence/d/e;->a:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothAdapter;->getBluetoothLeAdvertiser()Landroid/bluetooth/le/BluetoothLeAdvertiser;

    move-result-object v0

    .line 205
    if-nez v0, :cond_0

    .line 206
    const/4 v1, 0x5

    invoke-static {v1}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 207
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "BleAdvertiseImplL: Failed to get advertiser. Bluetooth must be enabled. Enabled state: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/location/copresence/d/a/a;->c:Lcom/google/android/location/copresence/d/e;

    iget-object v2, v2, Lcom/google/android/location/copresence/d/e;->a:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v2}, Landroid/bluetooth/BluetoothAdapter;->isEnabled()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/location/copresence/ag;->d(Ljava/lang/String;)I

    .line 211
    :cond_0
    return-object v0
.end method


# virtual methods
.method public final a()Z
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 134
    const/4 v2, 0x2

    invoke-static {v2}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 135
    const-string v2, "BleAdvertiseImplL: stopAdvertise"

    invoke-static {v2}, Lcom/google/android/location/copresence/ag;->a(Ljava/lang/String;)I

    .line 137
    :cond_0
    invoke-direct {p0}, Lcom/google/android/location/copresence/d/a/a;->d()Landroid/bluetooth/le/BluetoothLeAdvertiser;

    move-result-object v2

    .line 138
    if-eqz v2, :cond_1

    .line 139
    iget-object v3, p0, Lcom/google/android/location/copresence/d/a/a;->g:Lcom/google/android/location/copresence/d/a/c;

    if-eqz v3, :cond_1

    .line 140
    iget-object v3, p0, Lcom/google/android/location/copresence/d/a/a;->c:Lcom/google/android/location/copresence/d/e;

    const-string v4, "stopAdvertising"

    invoke-virtual {v3, v4, v5}, Lcom/google/android/location/copresence/d/e;->a(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 142
    iget-object v3, p0, Lcom/google/android/location/copresence/d/a/a;->g:Lcom/google/android/location/copresence/d/a/c;

    invoke-virtual {v2, v3}, Landroid/bluetooth/le/BluetoothLeAdvertiser;->stopAdvertising(Landroid/bluetooth/le/AdvertiseCallback;)V

    .line 144
    iget-object v2, p0, Lcom/google/android/location/copresence/d/a/a;->c:Lcom/google/android/location/copresence/d/e;

    const-string v3, "stopAdvertising"

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lcom/google/android/location/copresence/d/e;->a(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 145
    iput-object v5, p0, Lcom/google/android/location/copresence/d/a/a;->g:Lcom/google/android/location/copresence/d/a/c;

    .line 148
    :cond_1
    invoke-direct {p0}, Lcom/google/android/location/copresence/d/a/a;->c()V

    .line 149
    iget-object v2, p0, Lcom/google/android/location/copresence/d/a/a;->d:Lcom/google/android/location/copresence/d/a/o;

    if-eqz v2, :cond_2

    .line 150
    iget-object v2, p0, Lcom/google/android/location/copresence/d/a/a;->d:Lcom/google/android/location/copresence/d/a/o;

    .line 152
    :cond_2
    iput-boolean v1, p0, Lcom/google/android/location/copresence/d/a/a;->f:Z

    .line 153
    iget-boolean v2, p0, Lcom/google/android/location/copresence/d/a/a;->f:Z

    if-nez v2, :cond_3

    :goto_0
    return v0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public final a(ILjava/util/UUID;[B)Z
    .locals 7

    .prologue
    const/4 v6, 0x5

    const/4 v5, 0x2

    const/4 v0, 0x0

    .line 67
    invoke-static {v5}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 68
    const-string v1, "BleAdvertiseImplL: startAdvertise"

    invoke-static {v1}, Lcom/google/android/location/copresence/ag;->a(Ljava/lang/String;)I

    .line 71
    :cond_0
    iget-object v1, p0, Lcom/google/android/location/copresence/d/a/a;->e:Landroid/bluetooth/BluetoothGattServer;

    if-nez v1, :cond_1

    .line 72
    iget-object v1, p0, Lcom/google/android/location/copresence/d/a/a;->b:Landroid/bluetooth/BluetoothManager;

    iget-object v2, p0, Lcom/google/android/location/copresence/d/a/a;->a:Landroid/content/Context;

    new-instance v3, Lcom/google/android/location/copresence/d/a/b;

    invoke-direct {v3, p0}, Lcom/google/android/location/copresence/d/a/b;-><init>(Lcom/google/android/location/copresence/d/a/a;)V

    invoke-virtual {v1, v2, v3}, Landroid/bluetooth/BluetoothManager;->openGattServer(Landroid/content/Context;Landroid/bluetooth/BluetoothGattServerCallback;)Landroid/bluetooth/BluetoothGattServer;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/location/copresence/d/a/a;->e:Landroid/bluetooth/BluetoothGattServer;

    .line 74
    :cond_1
    iget-object v1, p0, Lcom/google/android/location/copresence/d/a/a;->e:Landroid/bluetooth/BluetoothGattServer;

    if-nez v1, :cond_3

    .line 75
    invoke-static {v6}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 76
    const-string v1, "BleAdvertiseImplL: unable to create gatt server."

    invoke-static {v1}, Lcom/google/android/location/copresence/ag;->d(Ljava/lang/String;)I

    .line 121
    :cond_2
    :goto_0
    return v0

    .line 82
    :cond_3
    iget-object v1, p0, Lcom/google/android/location/copresence/d/a/a;->e:Landroid/bluetooth/BluetoothGattServer;

    invoke-virtual {v1}, Landroid/bluetooth/BluetoothGattServer;->clearServices()V

    .line 83
    new-instance v1, Landroid/bluetooth/BluetoothGattService;

    invoke-direct {v1, p2, v0}, Landroid/bluetooth/BluetoothGattService;-><init>(Ljava/util/UUID;I)V

    .line 85
    iget-object v2, p0, Lcom/google/android/location/copresence/d/a/a;->e:Landroid/bluetooth/BluetoothGattServer;

    invoke-virtual {v2, v1}, Landroid/bluetooth/BluetoothGattServer;->addService(Landroid/bluetooth/BluetoothGattService;)Z

    move-result v1

    .line 86
    iget-object v2, p0, Lcom/google/android/location/copresence/d/a/a;->c:Lcom/google/android/location/copresence/d/e;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "addService "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lcom/google/android/location/copresence/d/e;->a(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 87
    if-nez v1, :cond_5

    .line 88
    invoke-static {v6}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 89
    const-string v1, "BleAdvertiseImplL: could not add gatt service."

    invoke-static {v1}, Lcom/google/android/location/copresence/ag;->d(Ljava/lang/String;)I

    .line 91
    :cond_4
    invoke-direct {p0}, Lcom/google/android/location/copresence/d/a/a;->c()V

    goto :goto_0

    .line 95
    :cond_5
    invoke-direct {p0}, Lcom/google/android/location/copresence/d/a/a;->d()Landroid/bluetooth/le/BluetoothLeAdvertiser;

    move-result-object v1

    .line 96
    if-eqz v1, :cond_7

    .line 97
    new-instance v2, Landroid/bluetooth/le/AdvertiseSettings$Builder;

    invoke-direct {v2}, Landroid/bluetooth/le/AdvertiseSettings$Builder;-><init>()V

    invoke-virtual {v2, v5}, Landroid/bluetooth/le/AdvertiseSettings$Builder;->setAdvertiseMode(I)Landroid/bluetooth/le/AdvertiseSettings$Builder;

    move-result-object v2

    const/4 v3, 0x3

    invoke-virtual {v2, v3}, Landroid/bluetooth/le/AdvertiseSettings$Builder;->setTxPowerLevel(I)Landroid/bluetooth/le/AdvertiseSettings$Builder;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/bluetooth/le/AdvertiseSettings$Builder;->setConnectable(Z)Landroid/bluetooth/le/AdvertiseSettings$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/bluetooth/le/AdvertiseSettings$Builder;->build()Landroid/bluetooth/le/AdvertiseSettings;

    move-result-object v2

    .line 102
    new-instance v3, Landroid/os/ParcelUuid;

    invoke-direct {v3, p2}, Landroid/os/ParcelUuid;-><init>(Ljava/util/UUID;)V

    .line 103
    new-instance v4, Landroid/bluetooth/le/AdvertiseData$Builder;

    invoke-direct {v4}, Landroid/bluetooth/le/AdvertiseData$Builder;-><init>()V

    invoke-virtual {v4, v3}, Landroid/bluetooth/le/AdvertiseData$Builder;->addServiceUuid(Landroid/os/ParcelUuid;)Landroid/bluetooth/le/AdvertiseData$Builder;

    move-result-object v4

    invoke-virtual {v4, v3, p3}, Landroid/bluetooth/le/AdvertiseData$Builder;->addServiceData(Landroid/os/ParcelUuid;[B)Landroid/bluetooth/le/AdvertiseData$Builder;

    move-result-object v3

    invoke-virtual {v3}, Landroid/bluetooth/le/AdvertiseData$Builder;->build()Landroid/bluetooth/le/AdvertiseData;

    move-result-object v3

    .line 107
    new-instance v4, Lcom/google/android/location/copresence/d/a/c;

    invoke-direct {v4, p0}, Lcom/google/android/location/copresence/d/a/c;-><init>(Lcom/google/android/location/copresence/d/a/a;)V

    iput-object v4, p0, Lcom/google/android/location/copresence/d/a/a;->g:Lcom/google/android/location/copresence/d/a/c;

    .line 108
    iget-object v4, p0, Lcom/google/android/location/copresence/d/a/a;->c:Lcom/google/android/location/copresence/d/e;

    const-string v5, "startAdvertising"

    const/4 v6, 0x0

    invoke-virtual {v4, v5, v6}, Lcom/google/android/location/copresence/d/e;->a(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 109
    iget-object v4, p0, Lcom/google/android/location/copresence/d/a/a;->g:Lcom/google/android/location/copresence/d/a/c;

    invoke-virtual {v1, v2, v3, v4}, Landroid/bluetooth/le/BluetoothLeAdvertiser;->startAdvertising(Landroid/bluetooth/le/AdvertiseSettings;Landroid/bluetooth/le/AdvertiseData;Landroid/bluetooth/le/AdvertiseCallback;)V

    .line 110
    iget-object v1, p0, Lcom/google/android/location/copresence/d/a/a;->g:Lcom/google/android/location/copresence/d/a/c;

    invoke-static {}, Lcom/google/android/location/copresence/f/b;->b()Lcom/google/ac/b/c/g;

    move-result-object v2

    iget-object v2, v2, Lcom/google/ac/b/c/g;->i:Lcom/google/ac/b/c/d;

    iget-object v2, v2, Lcom/google/ac/b/c/d;->c:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lcom/google/android/location/copresence/d/a/c;->a(J)I

    move-result v1

    .line 111
    if-nez v1, :cond_6

    const/4 v0, 0x1

    :cond_6
    iput-boolean v0, p0, Lcom/google/android/location/copresence/d/a/a;->f:Z

    .line 112
    iget-object v0, p0, Lcom/google/android/location/copresence/d/a/a;->c:Lcom/google/android/location/copresence/d/e;

    const-string v1, "startAdvertising"

    iget-boolean v2, p0, Lcom/google/android/location/copresence/d/a/a;->f:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/location/copresence/d/e;->a(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 115
    :cond_7
    iget-boolean v0, p0, Lcom/google/android/location/copresence/d/a/a;->f:Z

    if-eqz v0, :cond_8

    .line 116
    iget-object v0, p0, Lcom/google/android/location/copresence/d/a/a;->d:Lcom/google/android/location/copresence/d/a/o;

    if-eqz v0, :cond_8

    .line 118
    iget-object v0, p0, Lcom/google/android/location/copresence/d/a/a;->d:Lcom/google/android/location/copresence/d/a/o;

    .line 121
    :cond_8
    iget-boolean v0, p0, Lcom/google/android/location/copresence/d/a/a;->f:Z

    goto/16 :goto_0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 159
    iget-boolean v0, p0, Lcom/google/android/location/copresence/d/a/a;->f:Z

    return v0
.end method
