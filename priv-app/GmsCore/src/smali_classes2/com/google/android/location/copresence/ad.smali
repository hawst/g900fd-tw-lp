.class public final Lcom/google/android/location/copresence/ad;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/location/copresence/q/n;


# instance fields
.field final a:I

.field final b:Lcom/google/android/location/copresence/z;

.field final c:Lcom/google/android/location/copresence/as;

.field private final d:Lcom/google/android/location/copresence/ab;

.field private e:Z


# direct methods
.method public constructor <init>(ILcom/google/android/location/copresence/z;Lcom/google/android/location/copresence/ab;Lcom/google/android/location/copresence/as;)V
    .locals 1

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    iput p1, p0, Lcom/google/android/location/copresence/ad;->a:I

    .line 25
    iput-object p2, p0, Lcom/google/android/location/copresence/ad;->b:Lcom/google/android/location/copresence/z;

    .line 26
    iput-object p3, p0, Lcom/google/android/location/copresence/ad;->d:Lcom/google/android/location/copresence/ab;

    .line 27
    iput-object p4, p0, Lcom/google/android/location/copresence/ad;->c:Lcom/google/android/location/copresence/as;

    .line 28
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/location/copresence/ad;->e:Z

    .line 29
    return-void
.end method

.method private b(Lcom/google/android/location/copresence/q/l;)V
    .locals 7

    .prologue
    .line 81
    iget-object v0, p0, Lcom/google/android/location/copresence/ad;->b:Lcom/google/android/location/copresence/z;

    invoke-interface {v0}, Lcom/google/android/location/copresence/z;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 83
    :try_start_0
    iget-object v1, p0, Lcom/google/android/location/copresence/ad;->b:Lcom/google/android/location/copresence/z;

    iget-object v2, p0, Lcom/google/android/location/copresence/ad;->d:Lcom/google/android/location/copresence/ab;

    iget-object v3, p0, Lcom/google/android/location/copresence/ad;->c:Lcom/google/android/location/copresence/as;

    if-nez p1, :cond_1

    const/4 v0, 0x0

    :goto_0
    invoke-interface {v1, v2, v3, v0}, Lcom/google/android/location/copresence/z;->a(Lcom/google/android/location/copresence/ab;Lcom/google/android/location/copresence/as;Lcom/google/android/location/copresence/q/al;)V

    .line 108
    :cond_0
    :goto_1
    return-void

    .line 83
    :cond_1
    new-instance v0, Lcom/google/android/location/copresence/q/ar;

    invoke-static {}, Lcom/google/android/location/copresence/f/a;->e()Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    new-instance v6, Lcom/google/android/location/copresence/ae;

    invoke-direct {v6, p0, p1}, Lcom/google/android/location/copresence/ae;-><init>(Lcom/google/android/location/copresence/ad;Lcom/google/android/location/copresence/q/l;)V

    invoke-direct {v0, v4, v5, v6}, Lcom/google/android/location/copresence/q/ar;-><init>(JLcom/google/android/location/copresence/q/at;)V
    :try_end_0
    .catch Lcom/google/android/location/copresence/ac; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 103
    :catch_0
    move-exception v0

    const/4 v0, 0x6

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 104
    const-string v0, "CopresenceTokenObserverTask error! Cannot start available observer."

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->e(Ljava/lang/String;)I

    goto :goto_1
.end method

.method private b(Lcom/google/android/location/copresence/q/m;)V
    .locals 5

    .prologue
    .line 111
    iget-object v1, p0, Lcom/google/android/location/copresence/ad;->b:Lcom/google/android/location/copresence/z;

    if-nez p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-interface {v1, v0}, Lcom/google/android/location/copresence/z;->a(Lcom/google/android/location/copresence/q/al;)V

    .line 129
    return-void

    .line 111
    :cond_0
    new-instance v0, Lcom/google/android/location/copresence/q/ar;

    invoke-static {}, Lcom/google/android/location/copresence/f/a;->e()Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    new-instance v4, Lcom/google/android/location/copresence/af;

    invoke-direct {v4, p0, p1}, Lcom/google/android/location/copresence/af;-><init>(Lcom/google/android/location/copresence/ad;Lcom/google/android/location/copresence/q/m;)V

    invoke-direct {v0, v2, v3, v4}, Lcom/google/android/location/copresence/q/ar;-><init>(JLcom/google/android/location/copresence/q/at;)V

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 52
    iget-boolean v0, p0, Lcom/google/android/location/copresence/ad;->e:Z

    if-nez v0, :cond_0

    .line 53
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/location/copresence/ad;->e:Z

    .line 54
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/location/copresence/ad;->b(Lcom/google/android/location/copresence/q/l;)V

    .line 56
    :cond_0
    return-void
.end method

.method public final a(Lcom/google/android/location/copresence/q/l;)V
    .locals 1

    .prologue
    .line 67
    iget-boolean v0, p0, Lcom/google/android/location/copresence/ad;->e:Z

    if-nez v0, :cond_0

    .line 68
    invoke-direct {p0, p1}, Lcom/google/android/location/copresence/ad;->b(Lcom/google/android/location/copresence/q/l;)V

    .line 70
    :cond_0
    return-void
.end method

.method public final a(Lcom/google/android/location/copresence/q/m;)V
    .locals 1

    .prologue
    .line 74
    iget-boolean v0, p0, Lcom/google/android/location/copresence/ad;->e:Z

    if-nez v0, :cond_0

    .line 75
    invoke-direct {p0, p1}, Lcom/google/android/location/copresence/ad;->b(Lcom/google/android/location/copresence/q/m;)V

    .line 77
    :cond_0
    return-void
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 59
    iget-boolean v0, p0, Lcom/google/android/location/copresence/ad;->e:Z

    if-eqz v0, :cond_0

    .line 60
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/location/copresence/ad;->b(Lcom/google/android/location/copresence/q/m;)V

    .line 61
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/location/copresence/ad;->e:Z

    .line 63
    :cond_0
    return-void
.end method
