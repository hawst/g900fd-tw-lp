.class final Lcom/google/android/location/copresence/d/ac;
.super Lcom/google/android/location/copresence/k/f;
.source "SourceFile"


# instance fields
.field final a:Lcom/google/android/location/copresence/d/f;

.field final b:Lcom/google/android/location/copresence/k/f;

.field c:Z

.field final synthetic d:Lcom/google/android/location/copresence/d/n;

.field private f:J


# direct methods
.method constructor <init>(Lcom/google/android/location/copresence/d/n;Lcom/google/android/location/copresence/d/f;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 246
    iput-object p1, p0, Lcom/google/android/location/copresence/d/ac;->d:Lcom/google/android/location/copresence/d/n;

    .line 247
    const-string v0, "BleScanning"

    const/4 v1, 0x3

    new-array v1, v1, [Lcom/google/android/location/copresence/k/f;

    iget-object v2, p1, Lcom/google/android/location/copresence/d/n;->d:Lcom/google/android/location/copresence/k/f;

    aput-object v2, v1, v4

    const/4 v2, 0x1

    iget-object v3, p1, Lcom/google/android/location/copresence/d/n;->f:Lcom/google/android/location/copresence/k/f;

    aput-object v3, v1, v2

    const/4 v2, 0x2

    iget-object v3, p1, Lcom/google/android/location/copresence/d/n;->i:Lcom/google/android/location/copresence/k/f;

    aput-object v3, v1, v2

    invoke-direct {p0, v0, v1}, Lcom/google/android/location/copresence/k/f;-><init>(Ljava/lang/String;[Lcom/google/android/location/copresence/k/f;)V

    .line 228
    new-instance v0, Lcom/google/android/location/copresence/d/ad;

    const-string v1, "BleScanStopped"

    new-array v2, v4, [Lcom/google/android/location/copresence/k/f;

    invoke-direct {v0, p0, v1, v2}, Lcom/google/android/location/copresence/d/ad;-><init>(Lcom/google/android/location/copresence/d/ac;Ljava/lang/String;[Lcom/google/android/location/copresence/k/f;)V

    iput-object v0, p0, Lcom/google/android/location/copresence/d/ac;->b:Lcom/google/android/location/copresence/k/f;

    .line 249
    iput-object p2, p0, Lcom/google/android/location/copresence/d/ac;->a:Lcom/google/android/location/copresence/d/f;

    .line 250
    iput-boolean v4, p0, Lcom/google/android/location/copresence/d/ac;->c:Z

    .line 251
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/location/copresence/d/ac;->f:J

    .line 252
    return-void
.end method


# virtual methods
.method public final a()Z
    .locals 4

    .prologue
    .line 262
    iget-object v0, p0, Lcom/google/android/location/copresence/d/ac;->d:Lcom/google/android/location/copresence/d/n;

    iget-object v0, v0, Lcom/google/android/location/copresence/d/n;->b:Lcom/google/android/location/copresence/d/e;

    iget-object v1, p0, Lcom/google/android/location/copresence/d/ac;->a:Lcom/google/android/location/copresence/d/f;

    invoke-virtual {v0, v1}, Lcom/google/android/location/copresence/d/e;->a(Lcom/google/android/location/copresence/d/f;)V

    .line 263
    iget-object v0, p0, Lcom/google/android/location/copresence/d/ac;->d:Lcom/google/android/location/copresence/d/n;

    iget-object v1, v0, Lcom/google/android/location/copresence/d/n;->b:Lcom/google/android/location/copresence/d/e;

    iget-object v0, p0, Lcom/google/android/location/copresence/d/ac;->a:Lcom/google/android/location/copresence/d/f;

    const/4 v2, 0x2

    invoke-static {v2}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v2

    if-eqz v2, :cond_0

    const-string v2, "BluetoothAdapterWrapper: startBleScan"

    invoke-static {v2}, Lcom/google/android/location/copresence/ag;->a(Ljava/lang/String;)I

    :cond_0
    const-string v2, "startBleScan"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Lcom/google/android/location/copresence/d/e;->a(Ljava/lang/String;Ljava/lang/Boolean;)V

    iget-object v2, v1, Lcom/google/android/location/copresence/d/e;->c:Ljava/util/HashSet;

    invoke-virtual {v2}, Ljava/util/HashSet;->isEmpty()Z

    move-result v2

    iget-object v3, v1, Lcom/google/android/location/copresence/d/e;->c:Ljava/util/HashSet;

    invoke-virtual {v3, v0}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    if-eqz v2, :cond_4

    iget-object v0, v1, Lcom/google/android/location/copresence/d/e;->b:Lcom/google/android/location/copresence/d/a/l;

    iget-object v2, v0, Lcom/google/android/location/copresence/d/a/l;->b:Lcom/google/android/location/copresence/d/a/n;

    if-nez v2, :cond_3

    const/4 v0, 0x0

    :goto_0
    iput-boolean v0, v1, Lcom/google/android/location/copresence/d/e;->d:Z

    :goto_1
    iget-boolean v0, v1, Lcom/google/android/location/copresence/d/e;->d:Z

    if-nez v0, :cond_1

    iget-object v0, v1, Lcom/google/android/location/copresence/d/e;->c:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->clear()V

    :cond_1
    iget-boolean v0, v1, Lcom/google/android/location/copresence/d/e;->d:Z

    const-string v0, "startBleScan"

    iget-boolean v2, v1, Lcom/google/android/location/copresence/d/e;->d:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Lcom/google/android/location/copresence/d/e;->a(Ljava/lang/String;Ljava/lang/Boolean;)V

    iget-boolean v0, v1, Lcom/google/android/location/copresence/d/e;->d:Z

    iput-boolean v0, p0, Lcom/google/android/location/copresence/d/ac;->c:Z

    .line 264
    iget-boolean v0, p0, Lcom/google/android/location/copresence/d/ac;->c:Z

    if-eqz v0, :cond_2

    .line 265
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/location/copresence/d/ac;->f:J

    .line 267
    :cond_2
    iget-boolean v0, p0, Lcom/google/android/location/copresence/d/ac;->c:Z

    return v0

    .line 263
    :cond_3
    iget-object v0, v0, Lcom/google/android/location/copresence/d/a/l;->b:Lcom/google/android/location/copresence/d/a/n;

    invoke-interface {v0}, Lcom/google/android/location/copresence/d/a/n;->a()Z

    move-result v0

    goto :goto_0

    :cond_4
    const/4 v0, 0x1

    iput-boolean v0, v1, Lcom/google/android/location/copresence/d/e;->d:Z

    goto :goto_1
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 256
    iget-boolean v0, p0, Lcom/google/android/location/copresence/d/ac;->c:Z

    return v0
.end method
