.class public final Lcom/google/android/location/copresence/h;
.super Landroid/content/BroadcastReceiver;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/android/location/copresence/j;

.field private final b:Ljava/util/HashSet;


# direct methods
.method public constructor <init>(Lcom/google/android/location/copresence/j;)V
    .locals 1

    .prologue
    .line 42
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    .line 43
    iput-object p1, p0, Lcom/google/android/location/copresence/h;->a:Lcom/google/android/location/copresence/j;

    .line 44
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/location/copresence/h;->b:Ljava/util/HashSet;

    .line 45
    return-void
.end method

.method static a()Landroid/content/IntentFilter;
    .locals 2

    .prologue
    .line 52
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 53
    const-string v1, "com.google.android.location.copresence.sync_settings_action"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 54
    const-string v1, "com.google.gservices.intent.action.GSERVICES_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 55
    const-string v1, "android.net.conn.CONNECTIVITY_CHANGE"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 56
    return-object v0
.end method


# virtual methods
.method public final a(Lcom/google/android/location/copresence/i;)V
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lcom/google/android/location/copresence/h;->b:Ljava/util/HashSet;

    invoke-virtual {v0, p1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 49
    return-void
.end method

.method public final onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 8

    .prologue
    const/4 v7, 0x6

    const/4 v6, 0x1

    const/4 v5, 0x0

    const/4 v4, 0x2

    .line 61
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 63
    const-string v1, "com.google.gservices.intent.action.GSERVICES_CHANGED"

    invoke-virtual {v1, v0}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 64
    invoke-static {v4}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 65
    const-string v0, "CopresenceBroadcastReceiver: Gservices settings have changed."

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->a(Ljava/lang/String;)I

    .line 67
    :cond_0
    iget-object v0, p0, Lcom/google/android/location/copresence/h;->a:Lcom/google/android/location/copresence/j;

    invoke-static {}, Lcom/google/android/location/copresence/f/b;->a()Lcom/google/android/location/copresence/f/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/location/copresence/f/b;->d()V

    .line 129
    :cond_1
    :goto_0
    return-void

    .line 71
    :cond_2
    const-string v1, "android.net.conn.CONNECTIVITY_CHANGE"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 72
    const-string v0, "connectivity"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    .line 74
    invoke-virtual {v0}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v0

    .line 76
    if-eqz v0, :cond_3

    invoke-virtual {v0}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v0

    if-nez v0, :cond_5

    .line 77
    :cond_3
    invoke-static {v4}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 78
    const-string v0, "CopresenceBroadcastReceiver: Network connectivity lost."

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->a(Ljava/lang/String;)I

    .line 81
    :cond_4
    iget-object v0, p0, Lcom/google/android/location/copresence/h;->b:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/copresence/i;

    .line 82
    invoke-interface {v0}, Lcom/google/android/location/copresence/i;->b()V

    goto :goto_1

    .line 85
    :cond_5
    invoke-static {v4}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 86
    const-string v0, "CopresenceBroadcastReceiver: Network connectivity established."

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->a(Ljava/lang/String;)I

    .line 89
    :cond_6
    iget-object v0, p0, Lcom/google/android/location/copresence/h;->b:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/copresence/i;

    .line 90
    invoke-interface {v0}, Lcom/google/android/location/copresence/i;->a()V

    goto :goto_2

    .line 97
    :cond_7
    const-string v1, "SelectionAccountName"

    invoke-virtual {p2, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 98
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_8

    .line 99
    invoke-static {v7}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 100
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "CopresenceBroadcastReceiver: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, "Intent \'%s\' does not contain account name"

    new-array v3, v6, [Ljava/lang/Object;

    aput-object v0, v3, v5

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->e(Ljava/lang/String;)I

    goto/16 :goto_0

    .line 108
    :cond_8
    invoke-static {v1}, Lcom/google/android/location/copresence/ag;->f(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 109
    iget-object v3, p0, Lcom/google/android/location/copresence/h;->a:Lcom/google/android/location/copresence/j;

    iget-object v3, v3, Lcom/google/android/location/copresence/j;->e:Lcom/google/android/location/copresence/a/b;

    .line 110
    invoke-virtual {v3, v1}, Lcom/google/android/location/copresence/a/b;->a(Ljava/lang/String;)Lcom/google/android/location/copresence/a/a;

    move-result-object v1

    .line 111
    if-nez v1, :cond_9

    .line 112
    invoke-static {v7}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 113
    const-string v1, "Intent \'%s\' sent for invalid copresence account \'%s\'"

    new-array v3, v4, [Ljava/lang/Object;

    aput-object v0, v3, v5

    aput-object v2, v3, v6

    invoke-static {v1, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 117
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "CopresenceBroadcastReceiver: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->e(Ljava/lang/String;)I

    goto/16 :goto_0

    .line 122
    :cond_9
    const-string v1, "com.google.android.location.copresence.sync_settings_action"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 123
    invoke-static {v4}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 124
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Sync settings requested for account: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->a(Ljava/lang/String;)I

    .line 126
    :cond_a
    iget-object v0, p0, Lcom/google/android/location/copresence/h;->a:Lcom/google/android/location/copresence/j;

    iget-object v0, v0, Lcom/google/android/location/copresence/j;->f:Lcom/google/android/location/copresence/n/f;

    invoke-static {}, Lcom/google/android/location/copresence/n/f;->a()V

    goto/16 :goto_0
.end method
