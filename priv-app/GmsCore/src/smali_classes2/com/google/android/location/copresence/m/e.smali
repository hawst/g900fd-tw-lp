.class final Lcom/google/android/location/copresence/m/e;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Ljava/lang/String;

.field final synthetic b:Lcom/google/android/gms/location/copresence/CopresenceSettings;

.field final synthetic c:Lcom/google/android/gms/location/copresence/internal/i;

.field final synthetic d:Lcom/google/android/location/copresence/m/b;


# direct methods
.method constructor <init>(Lcom/google/android/location/copresence/m/b;Ljava/lang/String;Lcom/google/android/gms/location/copresence/CopresenceSettings;Lcom/google/android/gms/location/copresence/internal/i;)V
    .locals 0

    .prologue
    .line 102
    iput-object p1, p0, Lcom/google/android/location/copresence/m/e;->d:Lcom/google/android/location/copresence/m/b;

    iput-object p2, p0, Lcom/google/android/location/copresence/m/e;->a:Ljava/lang/String;

    iput-object p3, p0, Lcom/google/android/location/copresence/m/e;->b:Lcom/google/android/gms/location/copresence/CopresenceSettings;

    iput-object p4, p0, Lcom/google/android/location/copresence/m/e;->c:Lcom/google/android/gms/location/copresence/internal/i;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 105
    iget-object v0, p0, Lcom/google/android/location/copresence/m/e;->d:Lcom/google/android/location/copresence/m/b;

    invoke-static {v0}, Lcom/google/android/location/copresence/m/b;->b(Lcom/google/android/location/copresence/m/b;)Lcom/google/android/location/copresence/r;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/location/copresence/m/e;->a:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/location/copresence/m/e;->b:Lcom/google/android/gms/location/copresence/CopresenceSettings;

    iget-object v3, p0, Lcom/google/android/location/copresence/m/e;->c:Lcom/google/android/gms/location/copresence/internal/i;

    if-eqz v3, :cond_0

    invoke-static {}, Lcom/google/android/location/copresence/f/a;->b()Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    if-nez v4, :cond_1

    const/4 v0, 0x3

    invoke-static {v3, v0, v5}, Lcom/google/android/location/copresence/r;->a(Lcom/google/android/gms/location/copresence/internal/i;ILcom/google/android/gms/location/copresence/CopresenceSettings;)V

    .line 106
    :cond_0
    :goto_0
    return-void

    .line 105
    :cond_1
    iget-object v4, v0, Lcom/google/android/location/copresence/r;->a:Lcom/google/android/location/copresence/ap;

    invoke-virtual {v4}, Lcom/google/android/location/copresence/ap;->c()V

    invoke-virtual {v0, v1}, Lcom/google/android/location/copresence/r;->c(Ljava/lang/String;)Lcom/google/android/location/copresence/a/a;

    move-result-object v1

    if-eqz v1, :cond_2

    iget-object v0, v0, Lcom/google/android/location/copresence/r;->b:Lcom/google/android/location/copresence/j;

    iget-object v0, v0, Lcom/google/android/location/copresence/j;->f:Lcom/google/android/location/copresence/n/f;

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/location/copresence/n/f;->a(Lcom/google/android/location/copresence/a/a;Lcom/google/android/gms/location/copresence/CopresenceSettings;Lcom/google/android/gms/location/copresence/internal/i;)V

    goto :goto_0

    :cond_2
    const/16 v0, 0x9c9

    invoke-static {v3, v0, v5}, Lcom/google/android/location/copresence/r;->a(Lcom/google/android/gms/location/copresence/internal/i;ILcom/google/android/gms/location/copresence/CopresenceSettings;)V

    goto :goto_0
.end method
