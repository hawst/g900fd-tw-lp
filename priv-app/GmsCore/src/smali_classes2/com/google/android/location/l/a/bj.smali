.class public final Lcom/google/android/location/l/a/bj;
.super Lcom/google/protobuf/nano/d;
.source "SourceFile"


# static fields
.field private static volatile f:[Lcom/google/android/location/l/a/bj;


# instance fields
.field public a:Ljava/lang/Integer;

.field public b:Lcom/google/android/location/l/a/bc;

.field public c:Lcom/google/android/location/l/a/bf;

.field public d:[Lcom/google/android/location/l/a/br;

.field public e:[Lcom/google/android/location/l/a/aw;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 38
    invoke-direct {p0}, Lcom/google/protobuf/nano/d;-><init>()V

    .line 39
    iput-object v1, p0, Lcom/google/android/location/l/a/bj;->a:Ljava/lang/Integer;

    iput-object v1, p0, Lcom/google/android/location/l/a/bj;->b:Lcom/google/android/location/l/a/bc;

    iput-object v1, p0, Lcom/google/android/location/l/a/bj;->c:Lcom/google/android/location/l/a/bf;

    invoke-static {}, Lcom/google/android/location/l/a/br;->a()[Lcom/google/android/location/l/a/br;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/l/a/bj;->d:[Lcom/google/android/location/l/a/br;

    invoke-static {}, Lcom/google/android/location/l/a/aw;->a()[Lcom/google/android/location/l/a/aw;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/l/a/bj;->e:[Lcom/google/android/location/l/a/aw;

    iput-object v1, p0, Lcom/google/android/location/l/a/bj;->unknownFieldData:Lcom/google/protobuf/nano/f;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/location/l/a/bj;->cachedSize:I

    .line 40
    return-void
.end method

.method public static a()[Lcom/google/android/location/l/a/bj;
    .locals 2

    .prologue
    .line 12
    sget-object v0, Lcom/google/android/location/l/a/bj;->f:[Lcom/google/android/location/l/a/bj;

    if-nez v0, :cond_1

    .line 13
    sget-object v1, Lcom/google/protobuf/nano/h;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 15
    :try_start_0
    sget-object v0, Lcom/google/android/location/l/a/bj;->f:[Lcom/google/android/location/l/a/bj;

    if-nez v0, :cond_0

    .line 16
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/android/location/l/a/bj;

    sput-object v0, Lcom/google/android/location/l/a/bj;->f:[Lcom/google/android/location/l/a/bj;

    .line 18
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 20
    :cond_1
    sget-object v0, Lcom/google/android/location/l/a/bj;->f:[Lcom/google/android/location/l/a/bj;

    return-object v0

    .line 18
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 148
    invoke-super {p0}, Lcom/google/protobuf/nano/d;->computeSerializedSize()I

    move-result v0

    .line 149
    iget-object v2, p0, Lcom/google/android/location/l/a/bj;->a:Ljava/lang/Integer;

    if-eqz v2, :cond_0

    .line 150
    const/4 v2, 0x1

    iget-object v3, p0, Lcom/google/android/location/l/a/bj;->a:Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 153
    :cond_0
    iget-object v2, p0, Lcom/google/android/location/l/a/bj;->b:Lcom/google/android/location/l/a/bc;

    if-eqz v2, :cond_1

    .line 154
    const/4 v2, 0x2

    iget-object v3, p0, Lcom/google/android/location/l/a/bj;->b:Lcom/google/android/location/l/a/bc;

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v2

    add-int/2addr v0, v2

    .line 157
    :cond_1
    iget-object v2, p0, Lcom/google/android/location/l/a/bj;->c:Lcom/google/android/location/l/a/bf;

    if-eqz v2, :cond_2

    .line 158
    const/4 v2, 0x3

    iget-object v3, p0, Lcom/google/android/location/l/a/bj;->c:Lcom/google/android/location/l/a/bf;

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v2

    add-int/2addr v0, v2

    .line 161
    :cond_2
    iget-object v2, p0, Lcom/google/android/location/l/a/bj;->d:[Lcom/google/android/location/l/a/br;

    if-eqz v2, :cond_5

    iget-object v2, p0, Lcom/google/android/location/l/a/bj;->d:[Lcom/google/android/location/l/a/br;

    array-length v2, v2

    if-lez v2, :cond_5

    move v2, v0

    move v0, v1

    .line 162
    :goto_0
    iget-object v3, p0, Lcom/google/android/location/l/a/bj;->d:[Lcom/google/android/location/l/a/br;

    array-length v3, v3

    if-ge v0, v3, :cond_4

    .line 163
    iget-object v3, p0, Lcom/google/android/location/l/a/bj;->d:[Lcom/google/android/location/l/a/br;

    aget-object v3, v3, v0

    .line 164
    if-eqz v3, :cond_3

    .line 165
    const/4 v4, 0x4

    invoke-static {v4, v3}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v3

    add-int/2addr v2, v3

    .line 162
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_4
    move v0, v2

    .line 170
    :cond_5
    iget-object v2, p0, Lcom/google/android/location/l/a/bj;->e:[Lcom/google/android/location/l/a/aw;

    if-eqz v2, :cond_7

    iget-object v2, p0, Lcom/google/android/location/l/a/bj;->e:[Lcom/google/android/location/l/a/aw;

    array-length v2, v2

    if-lez v2, :cond_7

    .line 171
    :goto_1
    iget-object v2, p0, Lcom/google/android/location/l/a/bj;->e:[Lcom/google/android/location/l/a/aw;

    array-length v2, v2

    if-ge v1, v2, :cond_7

    .line 172
    iget-object v2, p0, Lcom/google/android/location/l/a/bj;->e:[Lcom/google/android/location/l/a/aw;

    aget-object v2, v2, v1

    .line 173
    if-eqz v2, :cond_6

    .line 174
    const/4 v3, 0x5

    invoke-static {v3, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v2

    add-int/2addr v0, v2

    .line 171
    :cond_6
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 179
    :cond_7
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 55
    if-ne p1, p0, :cond_1

    .line 56
    const/4 v0, 0x1

    .line 95
    :cond_0
    :goto_0
    return v0

    .line 58
    :cond_1
    instance-of v1, p1, Lcom/google/android/location/l/a/bj;

    if-eqz v1, :cond_0

    .line 61
    check-cast p1, Lcom/google/android/location/l/a/bj;

    .line 62
    iget-object v1, p0, Lcom/google/android/location/l/a/bj;->a:Ljava/lang/Integer;

    if-nez v1, :cond_5

    .line 63
    iget-object v1, p1, Lcom/google/android/location/l/a/bj;->a:Ljava/lang/Integer;

    if-nez v1, :cond_0

    .line 69
    :cond_2
    iget-object v1, p0, Lcom/google/android/location/l/a/bj;->b:Lcom/google/android/location/l/a/bc;

    if-nez v1, :cond_6

    .line 70
    iget-object v1, p1, Lcom/google/android/location/l/a/bj;->b:Lcom/google/android/location/l/a/bc;

    if-nez v1, :cond_0

    .line 78
    :cond_3
    iget-object v1, p0, Lcom/google/android/location/l/a/bj;->c:Lcom/google/android/location/l/a/bf;

    if-nez v1, :cond_7

    .line 79
    iget-object v1, p1, Lcom/google/android/location/l/a/bj;->c:Lcom/google/android/location/l/a/bf;

    if-nez v1, :cond_0

    .line 87
    :cond_4
    iget-object v1, p0, Lcom/google/android/location/l/a/bj;->d:[Lcom/google/android/location/l/a/br;

    iget-object v2, p1, Lcom/google/android/location/l/a/bj;->d:[Lcom/google/android/location/l/a/br;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 91
    iget-object v1, p0, Lcom/google/android/location/l/a/bj;->e:[Lcom/google/android/location/l/a/aw;

    iget-object v2, p1, Lcom/google/android/location/l/a/bj;->e:[Lcom/google/android/location/l/a/aw;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 95
    invoke-virtual {p0, p1}, Lcom/google/android/location/l/a/bj;->unknownFieldDataEquals(Lcom/google/protobuf/nano/d;)Z

    move-result v0

    goto :goto_0

    .line 66
    :cond_5
    iget-object v1, p0, Lcom/google/android/location/l/a/bj;->a:Ljava/lang/Integer;

    iget-object v2, p1, Lcom/google/android/location/l/a/bj;->a:Ljava/lang/Integer;

    invoke-virtual {v1, v2}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    goto :goto_0

    .line 74
    :cond_6
    iget-object v1, p0, Lcom/google/android/location/l/a/bj;->b:Lcom/google/android/location/l/a/bc;

    iget-object v2, p1, Lcom/google/android/location/l/a/bj;->b:Lcom/google/android/location/l/a/bc;

    invoke-virtual {v1, v2}, Lcom/google/android/location/l/a/bc;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    goto :goto_0

    .line 83
    :cond_7
    iget-object v1, p0, Lcom/google/android/location/l/a/bj;->c:Lcom/google/android/location/l/a/bf;

    iget-object v2, p1, Lcom/google/android/location/l/a/bj;->c:Lcom/google/android/location/l/a/bf;

    invoke-virtual {v1, v2}, Lcom/google/android/location/l/a/bf;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 100
    iget-object v0, p0, Lcom/google/android/location/l/a/bj;->a:Ljava/lang/Integer;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    .line 103
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/location/l/a/bj;->b:Lcom/google/android/location/l/a/bc;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    .line 105
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/android/location/l/a/bj;->c:Lcom/google/android/location/l/a/bf;

    if-nez v2, :cond_2

    :goto_2
    add-int/2addr v0, v1

    .line 107
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/google/android/location/l/a/bj;->d:[Lcom/google/android/location/l/a/br;

    invoke-static {v1}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 109
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/google/android/location/l/a/bj;->e:[Lcom/google/android/location/l/a/aw;

    invoke-static {v1}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 111
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/google/android/location/l/a/bj;->unknownFieldDataHashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 112
    return v0

    .line 100
    :cond_0
    iget-object v0, p0, Lcom/google/android/location/l/a/bj;->a:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->hashCode()I

    move-result v0

    goto :goto_0

    .line 103
    :cond_1
    iget-object v0, p0, Lcom/google/android/location/l/a/bj;->b:Lcom/google/android/location/l/a/bc;

    invoke-virtual {v0}, Lcom/google/android/location/l/a/bc;->hashCode()I

    move-result v0

    goto :goto_1

    .line 105
    :cond_2
    iget-object v1, p0, Lcom/google/android/location/l/a/bj;->c:Lcom/google/android/location/l/a/bf;

    invoke-virtual {v1}, Lcom/google/android/location/l/a/bf;->hashCode()I

    move-result v1

    goto :goto_2
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 5
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/android/location/l/a/bj;->storeUnknownField(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/l/a/bj;->a:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Lcom/google/android/location/l/a/bj;->b:Lcom/google/android/location/l/a/bc;

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/android/location/l/a/bc;

    invoke-direct {v0}, Lcom/google/android/location/l/a/bc;-><init>()V

    iput-object v0, p0, Lcom/google/android/location/l/a/bj;->b:Lcom/google/android/location/l/a/bc;

    :cond_1
    iget-object v0, p0, Lcom/google/android/location/l/a/bj;->b:Lcom/google/android/location/l/a/bc;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Lcom/google/android/location/l/a/bj;->c:Lcom/google/android/location/l/a/bf;

    if-nez v0, :cond_2

    new-instance v0, Lcom/google/android/location/l/a/bf;

    invoke-direct {v0}, Lcom/google/android/location/l/a/bf;-><init>()V

    iput-object v0, p0, Lcom/google/android/location/l/a/bj;->c:Lcom/google/android/location/l/a/bf;

    :cond_2
    iget-object v0, p0, Lcom/google/android/location/l/a/bj;->c:Lcom/google/android/location/l/a/bf;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_4
    const/16 v0, 0x22

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->b(Lcom/google/protobuf/nano/a;I)I

    move-result v2

    iget-object v0, p0, Lcom/google/android/location/l/a/bj;->d:[Lcom/google/android/location/l/a/br;

    if-nez v0, :cond_4

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lcom/google/android/location/l/a/br;

    if-eqz v0, :cond_3

    iget-object v3, p0, Lcom/google/android/location/l/a/bj;->d:[Lcom/google/android/location/l/a/br;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_3
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_5

    new-instance v3, Lcom/google/android/location/l/a/br;

    invoke-direct {v3}, Lcom/google/android/location/l/a/br;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_4
    iget-object v0, p0, Lcom/google/android/location/l/a/bj;->d:[Lcom/google/android/location/l/a/br;

    array-length v0, v0

    goto :goto_1

    :cond_5
    new-instance v3, Lcom/google/android/location/l/a/br;

    invoke-direct {v3}, Lcom/google/android/location/l/a/br;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    iput-object v2, p0, Lcom/google/android/location/l/a/bj;->d:[Lcom/google/android/location/l/a/br;

    goto :goto_0

    :sswitch_5
    const/16 v0, 0x2a

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->b(Lcom/google/protobuf/nano/a;I)I

    move-result v2

    iget-object v0, p0, Lcom/google/android/location/l/a/bj;->e:[Lcom/google/android/location/l/a/aw;

    if-nez v0, :cond_7

    move v0, v1

    :goto_3
    add-int/2addr v2, v0

    new-array v2, v2, [Lcom/google/android/location/l/a/aw;

    if-eqz v0, :cond_6

    iget-object v3, p0, Lcom/google/android/location/l/a/bj;->e:[Lcom/google/android/location/l/a/aw;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_6
    :goto_4
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_8

    new-instance v3, Lcom/google/android/location/l/a/aw;

    invoke-direct {v3}, Lcom/google/android/location/l/a/aw;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_7
    iget-object v0, p0, Lcom/google/android/location/l/a/bj;->e:[Lcom/google/android/location/l/a/aw;

    array-length v0, v0

    goto :goto_3

    :cond_8
    new-instance v3, Lcom/google/android/location/l/a/aw;

    invoke-direct {v3}, Lcom/google/android/location/l/a/aw;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    iput-object v2, p0, Lcom/google/android/location/l/a/bj;->e:[Lcom/google/android/location/l/a/aw;

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 118
    iget-object v0, p0, Lcom/google/android/location/l/a/bj;->a:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 119
    const/4 v0, 0x1

    iget-object v2, p0, Lcom/google/android/location/l/a/bj;->a:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {p1, v0, v2}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 121
    :cond_0
    iget-object v0, p0, Lcom/google/android/location/l/a/bj;->b:Lcom/google/android/location/l/a/bc;

    if-eqz v0, :cond_1

    .line 122
    const/4 v0, 0x2

    iget-object v2, p0, Lcom/google/android/location/l/a/bj;->b:Lcom/google/android/location/l/a/bc;

    invoke-virtual {p1, v0, v2}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 124
    :cond_1
    iget-object v0, p0, Lcom/google/android/location/l/a/bj;->c:Lcom/google/android/location/l/a/bf;

    if-eqz v0, :cond_2

    .line 125
    const/4 v0, 0x3

    iget-object v2, p0, Lcom/google/android/location/l/a/bj;->c:Lcom/google/android/location/l/a/bf;

    invoke-virtual {p1, v0, v2}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 127
    :cond_2
    iget-object v0, p0, Lcom/google/android/location/l/a/bj;->d:[Lcom/google/android/location/l/a/br;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/google/android/location/l/a/bj;->d:[Lcom/google/android/location/l/a/br;

    array-length v0, v0

    if-lez v0, :cond_4

    move v0, v1

    .line 128
    :goto_0
    iget-object v2, p0, Lcom/google/android/location/l/a/bj;->d:[Lcom/google/android/location/l/a/br;

    array-length v2, v2

    if-ge v0, v2, :cond_4

    .line 129
    iget-object v2, p0, Lcom/google/android/location/l/a/bj;->d:[Lcom/google/android/location/l/a/br;

    aget-object v2, v2, v0

    .line 130
    if-eqz v2, :cond_3

    .line 131
    const/4 v3, 0x4

    invoke-virtual {p1, v3, v2}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 128
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 135
    :cond_4
    iget-object v0, p0, Lcom/google/android/location/l/a/bj;->e:[Lcom/google/android/location/l/a/aw;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/google/android/location/l/a/bj;->e:[Lcom/google/android/location/l/a/aw;

    array-length v0, v0

    if-lez v0, :cond_6

    .line 136
    :goto_1
    iget-object v0, p0, Lcom/google/android/location/l/a/bj;->e:[Lcom/google/android/location/l/a/aw;

    array-length v0, v0

    if-ge v1, v0, :cond_6

    .line 137
    iget-object v0, p0, Lcom/google/android/location/l/a/bj;->e:[Lcom/google/android/location/l/a/aw;

    aget-object v0, v0, v1

    .line 138
    if-eqz v0, :cond_5

    .line 139
    const/4 v2, 0x5

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 136
    :cond_5
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 143
    :cond_6
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/d;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 144
    return-void
.end method
