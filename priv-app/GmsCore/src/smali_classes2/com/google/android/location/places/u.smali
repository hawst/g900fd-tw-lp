.class final Lcom/google/android/location/places/u;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field a:Lcom/google/android/location/b/aa;

.field final synthetic b:Landroid/os/Handler;

.field final synthetic c:Lcom/google/android/location/n/ai;

.field final synthetic d:Lcom/google/android/location/places/t;


# direct methods
.method constructor <init>(Lcom/google/android/location/places/t;Landroid/os/Handler;Lcom/google/android/location/n/ai;)V
    .locals 0

    .prologue
    .line 73
    iput-object p1, p0, Lcom/google/android/location/places/u;->d:Lcom/google/android/location/places/t;

    iput-object p2, p0, Lcom/google/android/location/places/u;->b:Landroid/os/Handler;

    iput-object p3, p0, Lcom/google/android/location/places/u;->c:Lcom/google/android/location/n/ai;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 26

    .prologue
    .line 78
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/location/places/u;->b:Landroid/os/Handler;

    sget-object v2, Lcom/google/android/location/x;->Z:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v2}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    move-object/from16 v0, p0

    invoke-virtual {v3, v0, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 79
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/location/places/u;->d:Lcom/google/android/location/places/t;

    invoke-static {v2}, Lcom/google/android/location/places/t;->a(Lcom/google/android/location/places/t;)Lcom/google/android/location/b/p;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/location/places/u;->c:Lcom/google/android/location/n/ai;

    invoke-virtual {v2}, Lcom/google/android/location/n/ai;->a()J

    move-result-wide v4

    iget-object v6, v3, Lcom/google/android/location/b/p;->a:Lcom/google/android/location/b/bd;

    monitor-enter v6

    :try_start_0
    iget-object v2, v3, Lcom/google/android/location/b/p;->a:Lcom/google/android/location/b/bd;

    const-wide/16 v8, 0x0

    invoke-virtual {v2, v4, v5, v8, v9}, Lcom/google/android/location/b/bd;->a(JJ)V

    monitor-exit v6
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iput-wide v4, v3, Lcom/google/android/location/b/p;->e:J

    iget-boolean v2, v3, Lcom/google/android/location/b/p;->y:Z

    if-nez v2, :cond_0

    const/4 v2, 0x0

    :goto_0
    iget v4, v3, Lcom/google/android/location/b/p;->t:I

    if-ge v2, v4, :cond_0

    iget-object v4, v3, Lcom/google/android/location/b/p;->f:Ljava/util/concurrent/atomic/AtomicIntegerArray;

    const/4 v5, 0x1

    invoke-virtual {v4, v2, v5}, Ljava/util/concurrent/atomic/AtomicIntegerArray;->set(II)V

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :catchall_0
    move-exception v2

    monitor-exit v6

    throw v2

    .line 80
    :cond_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/location/places/u;->d:Lcom/google/android/location/places/t;

    invoke-static {v2}, Lcom/google/android/location/places/t;->a(Lcom/google/android/location/places/t;)Lcom/google/android/location/b/p;

    move-result-object v2

    new-instance v3, Lcom/google/android/location/b/aa;

    iget-object v4, v2, Lcom/google/android/location/b/p;->h:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v4}, Ljava/util/concurrent/atomic/AtomicLong;->get()J

    move-result-wide v4

    iget-object v6, v2, Lcom/google/android/location/b/p;->i:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v6}, Ljava/util/concurrent/atomic/AtomicLong;->get()J

    move-result-wide v6

    iget-object v8, v2, Lcom/google/android/location/b/p;->j:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v8}, Ljava/util/concurrent/atomic/AtomicLong;->get()J

    move-result-wide v8

    iget-object v10, v2, Lcom/google/android/location/b/p;->k:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v10}, Ljava/util/concurrent/atomic/AtomicLong;->get()J

    move-result-wide v10

    iget-object v12, v2, Lcom/google/android/location/b/p;->l:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v12}, Ljava/util/concurrent/atomic/AtomicLong;->get()J

    move-result-wide v12

    iget-object v14, v2, Lcom/google/android/location/b/p;->m:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v14}, Ljava/util/concurrent/atomic/AtomicLong;->get()J

    move-result-wide v14

    iget-object v0, v2, Lcom/google/android/location/b/p;->n:Ljava/util/concurrent/atomic/AtomicLong;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, Ljava/util/concurrent/atomic/AtomicLong;->get()J

    move-result-wide v16

    iget-object v0, v2, Lcom/google/android/location/b/p;->o:Ljava/util/concurrent/atomic/AtomicLong;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Ljava/util/concurrent/atomic/AtomicLong;->get()J

    move-result-wide v18

    iget-object v0, v2, Lcom/google/android/location/b/p;->p:Ljava/util/concurrent/atomic/AtomicLong;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Ljava/util/concurrent/atomic/AtomicLong;->get()J

    move-result-wide v20

    iget-object v0, v2, Lcom/google/android/location/b/p;->q:Ljava/util/concurrent/atomic/AtomicLong;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Ljava/util/concurrent/atomic/AtomicLong;->get()J

    move-result-wide v22

    iget-object v2, v2, Lcom/google/android/location/b/p;->r:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicLong;->get()J

    move-result-wide v24

    invoke-direct/range {v3 .. v25}, Lcom/google/android/location/b/aa;-><init>(JJJJJJJJJJJ)V

    .line 82
    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/location/places/u;->a:Lcom/google/android/location/b/aa;

    .line 83
    return-void
.end method
