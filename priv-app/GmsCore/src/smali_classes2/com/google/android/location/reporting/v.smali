.class public final Lcom/google/android/location/reporting/v;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/location/reporting/x;


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Lcom/google/android/location/reporting/b/f;

.field private final c:Lcom/google/android/location/reporting/config/h;

.field private final d:I


# direct methods
.method private constructor <init>(Landroid/content/Context;Lcom/google/android/location/reporting/b/f;Lcom/google/android/location/reporting/config/h;I)V
    .locals 0

    .prologue
    .line 96
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 97
    iput-object p1, p0, Lcom/google/android/location/reporting/v;->a:Landroid/content/Context;

    .line 98
    iput-object p2, p0, Lcom/google/android/location/reporting/v;->b:Lcom/google/android/location/reporting/b/f;

    .line 99
    iput-object p3, p0, Lcom/google/android/location/reporting/v;->c:Lcom/google/android/location/reporting/config/h;

    .line 100
    iput p4, p0, Lcom/google/android/location/reporting/v;->d:I

    .line 101
    return-void
.end method

.method private static a(Landroid/content/Context;Landroid/accounts/Account;)Lcom/google/android/gms/common/server/ClientContext;
    .locals 5

    .prologue
    .line 829
    new-instance v0, Lcom/google/android/gms/common/server/ClientContext;

    invoke-static {}, Landroid/os/Process;->myUid()I

    move-result v1

    iget-object v2, p1, Landroid/accounts/Account;->name:Ljava/lang/String;

    iget-object v3, p1, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/gms/common/server/ClientContext;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 831
    const-string v1, "https://www.googleapis.com/auth/userlocation.reporting"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/server/ClientContext;->a(Ljava/lang/String;)V

    .line 832
    return-object v0
.end method

.method public static a(Landroid/content/Context;)Lcom/google/android/location/reporting/v;
    .locals 4

    .prologue
    .line 104
    invoke-static {p0}, Lcom/google/android/location/reporting/b/n;->a(Landroid/content/Context;)V

    .line 105
    invoke-static {p0}, Lcom/google/android/location/reporting/config/h;->a(Landroid/content/Context;)Lcom/google/android/location/reporting/config/h;

    move-result-object v0

    .line 106
    new-instance v1, Lcom/google/android/location/reporting/b/g;

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/google/android/location/reporting/b/g;-><init>(Landroid/content/ContentResolver;)V

    .line 110
    sget-object v2, Lcom/google/android/location/d/g;->e:Lcom/google/android/location/d/g;

    invoke-static {v2, p0}, Lcom/google/android/location/d/f;->a(Lcom/google/android/location/d/g;Landroid/content/Context;)Lcom/google/android/location/d/f;

    move-result-object v2

    .line 112
    new-instance v3, Lcom/google/android/location/reporting/v;

    iget v2, v2, Lcom/google/android/location/d/f;->d:I

    invoke-direct {v3, p0, v1, v0, v2}, Lcom/google/android/location/reporting/v;-><init>(Landroid/content/Context;Lcom/google/android/location/reporting/b/f;Lcom/google/android/location/reporting/config/h;I)V

    return-object v3
.end method

.method private a()Lcom/google/android/ulr/ApiClientInfo;
    .locals 2

    .prologue
    .line 383
    iget-object v0, p0, Lcom/google/android/location/reporting/v;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/common/ew;->a(Landroid/content/res/Resources;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "tablet"

    .line 385
    :goto_0
    new-instance v1, Lcom/google/android/ulr/ApiClientInfo;

    invoke-direct {v1, v0}, Lcom/google/android/ulr/ApiClientInfo;-><init>(Ljava/lang/String;)V

    return-object v1

    .line 383
    :cond_0
    const-string v0, "phone"

    goto :goto_0
.end method

.method private static a(Ljava/lang/String;)Lcom/google/android/ulr/ApiExperiment;
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x4

    const/4 v0, 0x0

    .line 567
    const-string v1, "GCoreUlr"

    invoke-static {v1, v6}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 568
    const-string v1, "GCoreUlr"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "experiment spec: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 571
    :cond_0
    const-string v1, ":"

    invoke-virtual {p0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 572
    array-length v2, v1

    if-ge v2, v5, :cond_1

    .line 573
    const-string v1, "GCoreUlr"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Incomplete experiment specification: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/location/reporting/b/d;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 602
    :goto_0
    return-object v0

    .line 579
    :cond_1
    const/4 v2, 0x2

    :try_start_0
    aget-object v2, v1, v2

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    .line 585
    const/4 v3, 0x3

    aget-object v3, v1, v3

    .line 586
    const-string v4, "experiment"

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_2

    const-string v4, "control"

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_2

    .line 587
    const-string v1, "GCoreUlr"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v4, "Bad group "

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " in specification: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/location/reporting/b/d;->f(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 581
    :catch_0
    move-exception v2

    const-string v2, "GCoreUlr"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Bad experiment ID "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    aget-object v1, v1, v6

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " in specification: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Lcom/google/android/location/reporting/b/d;->f(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 592
    :cond_2
    array-length v4, v1

    if-le v4, v5, :cond_3

    .line 594
    const/4 v4, 0x4

    :try_start_1
    aget-object v4, v1, v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;
    :try_end_1
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v0

    .line 602
    :cond_3
    new-instance v1, Lcom/google/android/ulr/ApiExperiment;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-direct {v1, v3, v2, v0}, Lcom/google/android/ulr/ApiExperiment;-><init>(Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Integer;)V

    move-object v0, v1

    goto :goto_0

    .line 596
    :catch_1
    move-exception v2

    const-string v2, "GCoreUlr"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Bad subgroup "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    aget-object v1, v1, v5

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " in specification: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Lcom/google/android/location/reporting/b/d;->f(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method private a(Ljava/util/List;)Ljava/util/ArrayList;
    .locals 16

    .prologue
    .line 394
    const/4 v0, 0x0

    .line 397
    invoke-interface/range {p1 .. p1}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_14

    .line 398
    new-instance v11, Ljava/util/ArrayList;

    invoke-interface/range {p1 .. p1}, Ljava/util/List;->size()I

    move-result v0

    invoke-direct {v11, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 400
    invoke-interface/range {p1 .. p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v12

    :goto_0
    invoke-interface {v12}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_13

    invoke-interface {v12}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v10, v0

    check-cast v10, Lcom/google/android/location/reporting/a/f;

    .line 401
    iget-boolean v0, v10, Lcom/google/android/location/reporting/a/f;->k:Z

    if-eqz v0, :cond_6

    iget-wide v0, v10, Lcom/google/android/location/reporting/a/f;->l:D

    double-to-int v0, v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    :goto_1
    iget-boolean v0, v10, Lcom/google/android/location/reporting/a/f;->p:Z

    if-eqz v0, :cond_7

    iget-boolean v0, v10, Lcom/google/android/location/reporting/a/f;->q:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    :goto_2
    iget-boolean v0, v10, Lcom/google/android/location/reporting/a/f;->i:Z

    if-eqz v0, :cond_8

    iget v0, v10, Lcom/google/android/location/reporting/a/f;->j:F

    float-to-int v0, v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    :goto_3
    iget-boolean v0, v10, Lcom/google/android/location/reporting/a/f;->m:Z

    if-eqz v0, :cond_9

    iget v0, v10, Lcom/google/android/location/reporting/a/f;->n:F

    float-to-int v0, v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    :goto_4
    iget-boolean v0, v10, Lcom/google/android/location/reporting/a/f;->t:Z

    if-eqz v0, :cond_a

    iget v0, v10, Lcom/google/android/location/reporting/a/f;->u:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    :goto_5
    iget-boolean v0, v10, Lcom/google/android/location/reporting/a/f;->r:Z

    if-eqz v0, :cond_b

    iget-object v6, v10, Lcom/google/android/location/reporting/a/f;->s:Ljava/lang/String;

    :goto_6
    const/4 v7, 0x0

    const/4 v8, 0x0

    iget-boolean v0, v10, Lcom/google/android/location/reporting/a/f;->a:Z

    if-eqz v0, :cond_e

    iget-object v7, v10, Lcom/google/android/location/reporting/a/f;->b:Lcom/google/android/location/reporting/a/i;

    iget-boolean v0, v7, Lcom/google/android/location/reporting/a/i;->a:Z

    if-eqz v0, :cond_c

    iget v0, v7, Lcom/google/android/location/reporting/a/i;->b:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    :goto_7
    iget-boolean v8, v7, Lcom/google/android/location/reporting/a/i;->c:Z

    if-eqz v8, :cond_d

    iget v7, v7, Lcom/google/android/location/reporting/a/i;->d:I

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    :goto_8
    move-object v8, v7

    move-object v7, v0

    :goto_9
    iget-boolean v0, v10, Lcom/google/android/location/reporting/a/f;->g:Z

    if-eqz v0, :cond_f

    iget v0, v10, Lcom/google/android/location/reporting/a/f;->h:F

    float-to-int v0, v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    :goto_a
    new-instance v0, Lcom/google/android/ulr/ApiLocation;

    invoke-direct/range {v0 .. v9}, Lcom/google/android/ulr/ApiLocation;-><init>(Ljava/lang/Integer;Ljava/lang/Boolean;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;)V

    const/4 v1, 0x0

    iget-boolean v2, v10, Lcom/google/android/location/reporting/a/f;->w:Z

    if-eqz v2, :cond_1

    iget-object v3, v10, Lcom/google/android/location/reporting/a/f;->x:Lcom/google/android/location/reporting/a/b;

    const/4 v1, 0x0

    iget-boolean v2, v3, Lcom/google/android/location/reporting/a/b;->a:Z

    if-eqz v2, :cond_0

    iget v2, v3, Lcom/google/android/location/reporting/a/b;->b:I

    packed-switch v2, :pswitch_data_0

    const-string v2, "GCoreUlr"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Unknown charging method: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v5, v3, Lcom/google/android/location/reporting/a/b;->b:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Lcom/google/android/location/reporting/b/d;->f(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    :goto_b
    :pswitch_0
    new-instance v2, Lcom/google/android/ulr/ApiBatteryCondition;

    iget v4, v3, Lcom/google/android/location/reporting/a/b;->c:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    iget v5, v3, Lcom/google/android/location/reporting/a/b;->d:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    iget v3, v3, Lcom/google/android/location/reporting/a/b;->e:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-direct {v2, v1, v4, v5, v3}, Lcom/google/android/ulr/ApiBatteryCondition;-><init>(Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;)V

    move-object v1, v2

    :cond_1
    const/4 v2, 0x0

    iget-boolean v3, v10, Lcom/google/android/location/reporting/a/f;->c:Z

    if-eqz v3, :cond_2

    iget v2, v10, Lcom/google/android/location/reporting/a/f;->d:I

    packed-switch v2, :pswitch_data_1

    const-string v2, "unknown"

    :cond_2
    :goto_c
    invoke-direct/range {p0 .. p0}, Lcom/google/android/location/reporting/v;->b()Ljava/util/ArrayList;

    move-result-object v5

    invoke-virtual {v10}, Lcom/google/android/location/reporting/a/f;->c()I

    move-result v3

    if-lez v3, :cond_10

    iget-object v3, v10, Lcom/google/android/location/reporting/a/f;->v:Ljava/util/List;

    invoke-static {v3}, Lcom/google/android/location/reporting/v;->b(Ljava/util/List;)Ljava/util/ArrayList;

    move-result-object v3

    :goto_d
    const-string v4, "GCoreUlr"

    const/4 v6, 0x2

    invoke-static {v4, v6}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_3

    if-eqz v3, :cond_11

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v4

    :goto_e
    const-string v6, "GCoreUlr"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "Found wifi scans: "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v6, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_3
    new-instance v4, Lcom/google/android/ulr/ApiReadingInfo;

    invoke-direct {v4, v1, v2, v3}, Lcom/google/android/ulr/ApiReadingInfo;-><init>(Lcom/google/android/ulr/ApiBatteryCondition;Ljava/lang/String;Ljava/util/ArrayList;)V

    const-string v1, "GCoreUlr"

    const/4 v2, 0x2

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_4

    const-string v1, "GCoreUlr"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "apiReadingInfo: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_4
    iget-boolean v1, v10, Lcom/google/android/location/reporting/a/f;->e:Z

    if-eqz v1, :cond_12

    iget-wide v2, v10, Lcom/google/android/location/reporting/a/f;->f:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    :goto_f
    if-nez v1, :cond_5

    const-string v2, "GCoreUlr"

    new-instance v3, Ljava/lang/IllegalStateException;

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "Location missing timestamp; source="

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v7, v10, Lcom/google/android/location/reporting/a/f;->d:I

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v3, v6}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    invoke-static {v2, v3}, Lcom/google/android/location/reporting/b/d;->b(Ljava/lang/String;Ljava/lang/Throwable;)V

    :cond_5
    new-instance v2, Lcom/google/android/ulr/ApiLocationReading;

    invoke-direct {v2, v5, v0, v4, v1}, Lcom/google/android/ulr/ApiLocationReading;-><init>(Ljava/util/ArrayList;Lcom/google/android/ulr/ApiLocation;Lcom/google/android/ulr/ApiReadingInfo;Ljava/lang/Long;)V

    .line 402
    invoke-virtual {v11, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 401
    :cond_6
    const/4 v1, 0x0

    goto/16 :goto_1

    :cond_7
    const/4 v2, 0x0

    goto/16 :goto_2

    :cond_8
    const/4 v3, 0x0

    goto/16 :goto_3

    :cond_9
    const/4 v4, 0x0

    goto/16 :goto_4

    :cond_a
    const/4 v5, 0x0

    goto/16 :goto_5

    :cond_b
    const/4 v6, 0x0

    goto/16 :goto_6

    :cond_c
    const/4 v0, 0x0

    goto/16 :goto_7

    :cond_d
    const/4 v7, 0x0

    goto/16 :goto_8

    :cond_e
    const-string v0, "GCoreUlr"

    new-instance v9, Ljava/lang/IllegalStateException;

    new-instance v13, Ljava/lang/StringBuilder;

    const-string v14, "Location missing position; timestamp="

    invoke-direct {v13, v14}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v14, v10, Lcom/google/android/location/reporting/a/f;->f:J

    invoke-virtual {v13, v14, v15}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, "; source="

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    iget v14, v10, Lcom/google/android/location/reporting/a/f;->d:I

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-direct {v9, v13}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    invoke-static {v0, v9}, Lcom/google/android/location/reporting/b/d;->b(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_9

    :cond_f
    const/4 v9, 0x0

    goto/16 :goto_a

    :pswitch_1
    const-string v1, "ac"

    goto/16 :goto_b

    :pswitch_2
    const-string v1, "usb"

    goto/16 :goto_b

    :pswitch_3
    const-string v1, "wireless"

    goto/16 :goto_b

    :pswitch_4
    const-string v1, "notCharging"

    goto/16 :goto_b

    :pswitch_5
    const-string v2, "wifi"

    goto/16 :goto_c

    :pswitch_6
    const-string v2, "cell"

    goto/16 :goto_c

    :pswitch_7
    const-string v2, "gps"

    goto/16 :goto_c

    :pswitch_8
    const-string v2, "unknown"

    goto/16 :goto_c

    :pswitch_9
    const-string v2, "manual"

    goto/16 :goto_c

    :cond_10
    const/4 v3, 0x0

    goto/16 :goto_d

    :cond_11
    const/4 v4, 0x0

    goto/16 :goto_e

    :cond_12
    const/4 v1, 0x0

    goto/16 :goto_f

    :cond_13
    move-object v0, v11

    .line 406
    :cond_14
    return-object v0

    .line 401
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_4
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
    .end packed-switch
.end method

.method private a(Landroid/accounts/Account;Z)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 377
    invoke-static {p1}, Lcom/google/android/location/reporting/config/i;->a(Landroid/accounts/Account;)Lcom/google/android/location/reporting/config/j;

    move-result-object v0

    iput-boolean v1, v0, Lcom/google/android/location/reporting/config/j;->g:Z

    iput-boolean v1, v0, Lcom/google/android/location/reporting/config/j;->d:Z

    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, v0, Lcom/google/android/location/reporting/config/j;->m:Ljava/lang/Boolean;

    invoke-virtual {v0}, Lcom/google/android/location/reporting/config/j;->a()Lcom/google/android/location/reporting/config/i;

    move-result-object v0

    .line 379
    iget-object v1, p0, Lcom/google/android/location/reporting/v;->c:Lcom/google/android/location/reporting/config/h;

    const-string v2, "RealReportingServer(auth)"

    const-string v3, "auth_update"

    invoke-virtual {v1, v2, v0, v3}, Lcom/google/android/location/reporting/config/h;->a(Ljava/lang/String;Lcom/google/android/location/reporting/config/i;Ljava/lang/String;)Z

    .line 380
    return-void
.end method

.method private static a(Lcom/android/volley/ac;)V
    .locals 6

    .prologue
    const/4 v4, 0x6

    .line 761
    const-string v0, "GCoreUlr"

    invoke-static {v0, v4}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-nez v0, :cond_1

    .line 821
    :cond_0
    :goto_0
    return-void

    .line 765
    :cond_1
    iget-object v3, p0, Lcom/android/volley/ac;->a:Lcom/android/volley/m;

    .line 766
    const-string v0, "GCoreUlr"

    invoke-static {v0, v4}, Lcom/google/android/location/reporting/b/d;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 767
    const-string v1, "GCoreUlr"

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "VolleyError: "

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", response is "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    if-nez v3, :cond_5

    const-string v0, "null"

    :goto_1
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/google/android/location/reporting/b/d;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 771
    :cond_2
    if-eqz v3, :cond_0

    .line 776
    const-string v0, "GCoreUlr"

    invoke-static {v0, v4}, Lcom/google/android/location/reporting/b/d;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 777
    const-string v0, "GCoreUlr"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, " networkResponse: status code is :"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, v3, Lcom/android/volley/m;->a:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/location/reporting/b/d;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 780
    :cond_3
    const/4 v2, 0x0

    .line 782
    :try_start_0
    iget-object v0, v3, Lcom/android/volley/m;->b:[B

    if-eqz v0, :cond_9

    .line 783
    iget-object v0, v3, Lcom/android/volley/m;->b:[B

    invoke-static {v0}, Lcom/google/android/gms/common/util/ab;->a([B)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 784
    new-instance v1, Ljava/util/zip/GZIPInputStream;

    new-instance v0, Ljava/io/ByteArrayInputStream;

    iget-object v4, v3, Lcom/android/volley/m;->b:[B

    invoke-direct {v0, v4}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    invoke-direct {v1, v0}, Ljava/util/zip/GZIPInputStream;-><init>(Ljava/io/InputStream;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 790
    :try_start_1
    new-instance v0, Ljava/lang/StringBuilder;

    const/16 v2, 0x20

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 792
    :goto_2
    invoke-virtual {v1}, Ljava/util/zip/GZIPInputStream;->read()I

    move-result v2

    const/4 v4, -0x1

    if-eq v2, v4, :cond_7

    .line 793
    int-to-char v2, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    goto :goto_2

    .line 808
    :catch_0
    move-exception v0

    .line 809
    :goto_3
    :try_start_2
    const-string v2, "GCoreUlr"

    const/4 v3, 0x6

    invoke-static {v2, v3}, Lcom/google/android/location/reporting/b/d;->a(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 810
    const-string v2, "GCoreUlr"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, " io exception "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/google/android/location/reporting/b/d;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 813
    :cond_4
    if-eqz v1, :cond_0

    .line 815
    :try_start_3
    invoke-virtual {v1}, Ljava/util/zip/GZIPInputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1

    goto/16 :goto_0

    .line 816
    :catch_1
    move-exception v0

    .line 817
    const-string v1, "GCoreUlr"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, " io exception: cannot close input stream "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/google/android/location/reporting/b/d;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 767
    :cond_5
    const-string v0, "non-null"

    goto/16 :goto_1

    .line 786
    :cond_6
    :try_start_4
    new-instance v0, Ljava/io/IOException;

    const-string v1, "Error in GZIP header, bad magic code"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 808
    :catch_2
    move-exception v0

    move-object v1, v2

    goto :goto_3

    .line 795
    :cond_7
    :try_start_5
    const-string v2, "GCoreUlr"

    const/4 v4, 0x6

    invoke-static {v2, v4}, Lcom/google/android/location/reporting/b/d;->a(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_8

    .line 796
    const-string v2, "GCoreUlr"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, " networkResponse data is:"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/google/android/location/reporting/b/d;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_0
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    :cond_8
    move-object v2, v1

    .line 799
    :cond_9
    :try_start_6
    iget-object v0, v3, Lcom/android/volley/m;->c:Ljava/util/Map;

    if-eqz v0, :cond_b

    .line 800
    iget-object v0, v3, Lcom/android/volley/m;->c:Ljava/util/Map;

    .line 801
    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_a
    :goto_4
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_b

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 802
    const-string v1, "GCoreUlr"

    const/4 v4, 0x6

    invoke-static {v1, v4}, Lcom/google/android/location/reporting/b/d;->a(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_a

    .line 803
    const-string v4, "GCoreUlr"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v1, " networkResponse header: "

    invoke-direct {v5, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v5, ":"

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v4, v0}, Lcom/google/android/location/reporting/b/d;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_3
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    goto :goto_4

    .line 808
    :catch_3
    move-exception v0

    move-object v1, v2

    goto/16 :goto_3

    .line 813
    :cond_b
    if-eqz v2, :cond_0

    .line 815
    :try_start_7
    invoke-virtual {v2}, Ljava/util/zip/GZIPInputStream;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_4

    goto/16 :goto_0

    .line 816
    :catch_4
    move-exception v0

    .line 817
    const-string v1, "GCoreUlr"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, " io exception: cannot close input stream "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/google/android/location/reporting/b/d;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 813
    :catchall_0
    move-exception v0

    :goto_5
    if-eqz v2, :cond_c

    .line 815
    :try_start_8
    invoke-virtual {v2}, Ljava/util/zip/GZIPInputStream;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_5

    .line 818
    :cond_c
    :goto_6
    throw v0

    .line 816
    :catch_5
    move-exception v1

    .line 817
    const-string v2, "GCoreUlr"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, " io exception: cannot close input stream "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Lcom/google/android/location/reporting/b/d;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_6

    .line 813
    :catchall_1
    move-exception v0

    move-object v2, v1

    goto :goto_5
.end method

.method private b()Ljava/util/ArrayList;
    .locals 3

    .prologue
    .line 538
    const-string v0, "user_location_reporting:experiment:"

    .line 539
    iget-object v1, p0, Lcom/google/android/location/reporting/v;->b:Lcom/google/android/location/reporting/b/f;

    invoke-interface {v1, v0}, Lcom/google/android/location/reporting/b/f;->a(Ljava/lang/String;)Ljava/util/Map;

    move-result-object v0

    .line 540
    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 541
    const/4 v0, 0x0

    .line 553
    :goto_0
    return-object v0

    .line 544
    :cond_0
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 545
    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 546
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 547
    invoke-static {v0}, Lcom/google/android/location/reporting/v;->a(Ljava/lang/String;)Lcom/google/android/ulr/ApiExperiment;

    move-result-object v0

    .line 548
    if-eqz v0, :cond_1

    .line 549
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_2
    move-object v0, v1

    .line 553
    goto :goto_0
.end method

.method private static b(Ljava/util/List;)Ljava/util/ArrayList;
    .locals 10

    .prologue
    const/4 v2, 0x0

    .line 614
    if-eqz p0, :cond_0

    :try_start_0
    invoke-interface {p0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    move-object v0, v2

    .line 667
    :goto_0
    return-object v0

    .line 618
    :cond_1
    new-instance v1, Ljava/util/ArrayList;

    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v0

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 620
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_2
    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/reporting/a/l;

    .line 621
    iget-boolean v3, v0, Lcom/google/android/location/reporting/a/l;->a:Z

    if-eqz v3, :cond_2

    iget-boolean v3, v0, Lcom/google/android/location/reporting/a/l;->c:Z

    if-eqz v3, :cond_2

    .line 625
    iget-boolean v3, v0, Lcom/google/android/location/reporting/a/l;->e:Z

    if-eqz v3, :cond_3

    .line 626
    iget v3, v0, Lcom/google/android/location/reporting/a/l;->f:I

    packed-switch v3, :pswitch_data_0

    .line 647
    invoke-static {v0}, Lcom/google/android/location/reporting/b/l;->a(Lcom/google/android/location/reporting/a/l;)V

    .line 648
    const-string v3, "GCoreUlr"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v6, "Unknown value for wifi auth type: "

    invoke-direct {v4, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v6, v0, Lcom/google/android/location/reporting/a/l;->f:I

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/android/location/reporting/b/d;->f(Ljava/lang/String;Ljava/lang/String;)V

    :cond_3
    move-object v4, v2

    .line 654
    :goto_2
    iget-boolean v3, v0, Lcom/google/android/location/reporting/a/l;->g:Z

    if-eqz v3, :cond_4

    iget-boolean v3, v0, Lcom/google/android/location/reporting/a/l;->h:Z

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    .line 657
    :goto_3
    new-instance v6, Lcom/google/android/ulr/WifiStrengthProto;

    iget-wide v8, v0, Lcom/google/android/location/reporting/a/l;->b:J

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    iget v0, v0, Lcom/google/android/location/reporting/a/l;->d:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-direct {v6, v3, v7, v0, v4}, Lcom/google/android/ulr/WifiStrengthProto;-><init>(Ljava/lang/Boolean;Ljava/lang/Long;Ljava/lang/Integer;Ljava/lang/String;)V

    .line 659
    invoke-virtual {v1, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 664
    :catch_0
    move-exception v0

    .line 665
    invoke-static {v0}, Lcom/google/android/location/reporting/b/l;->b(Ljava/lang/Exception;)V

    .line 666
    const-string v1, "GCoreUlr"

    const-string v3, "Best-effort Wifi scan conversion failed"

    invoke-static {v1, v3, v0}, Lcom/google/android/location/reporting/b/d;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    move-object v0, v2

    .line 667
    goto :goto_0

    .line 628
    :pswitch_0
    :try_start_1
    const-string v3, "unknown"

    .line 629
    const-string v4, "GCoreUlr"

    const/4 v6, 0x3

    invoke-static {v4, v6}, Lcom/google/android/location/reporting/b/d;->a(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_6

    .line 630
    const-string v4, "GCoreUlr"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "Unknown wifi auth type: "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v7, v0, Lcom/google/android/location/reporting/a/l;->f:I

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v6}, Lcom/google/android/location/reporting/b/d;->b(Ljava/lang/String;Ljava/lang/String;)V

    move-object v4, v3

    goto :goto_2

    .line 635
    :pswitch_1
    const-string v3, "none"

    move-object v4, v3

    .line 636
    goto :goto_2

    .line 638
    :pswitch_2
    const-string v3, "wpaPsk"

    move-object v4, v3

    .line 639
    goto :goto_2

    .line 641
    :pswitch_3
    const-string v3, "wpaEap"

    move-object v4, v3

    .line 642
    goto :goto_2

    .line 644
    :pswitch_4
    const-string v3, "securedOther"
    :try_end_1
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_0

    move-object v4, v3

    .line 645
    goto :goto_2

    :cond_4
    move-object v3, v2

    .line 654
    goto :goto_3

    :cond_5
    move-object v0, v1

    .line 663
    goto/16 :goto_0

    :cond_6
    move-object v4, v3

    goto :goto_2

    .line 626
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method private c(Landroid/accounts/Account;)I
    .locals 1

    .prologue
    .line 126
    iget-object v0, p0, Lcom/google/android/location/reporting/v;->c:Lcom/google/android/location/reporting/config/h;

    invoke-virtual {v0, p1}, Lcom/google/android/location/reporting/config/h;->b(Landroid/accounts/Account;)I

    move-result v0

    return v0
.end method


# virtual methods
.method public final a(Landroid/accounts/Account;)Lcom/google/android/location/reporting/a;
    .locals 9

    .prologue
    .line 132
    invoke-direct {p0, p1}, Lcom/google/android/location/reporting/v;->c(Landroid/accounts/Account;)I

    move-result v0

    .line 133
    sget-object v2, Lcom/google/android/location/collectionlib/bn;->a:Ljava/lang/String;

    .line 138
    new-instance v1, Lcom/google/android/location/reporting/w;

    iget-object v3, p0, Lcom/google/android/location/reporting/v;->a:Landroid/content/Context;

    invoke-direct {v1, v3}, Lcom/google/android/location/reporting/w;-><init>(Landroid/content/Context;)V

    .line 144
    :try_start_0
    new-instance v4, Lcom/google/android/ulr/b;

    invoke-direct {v4, v1}, Lcom/google/android/ulr/b;-><init>(Lcom/google/android/gms/common/server/n;)V

    .line 145
    iget-object v1, p0, Lcom/google/android/location/reporting/v;->a:Landroid/content/Context;

    invoke-static {v1, p1}, Lcom/google/android/location/reporting/v;->a(Landroid/content/Context;Landroid/accounts/Account;)Lcom/google/android/gms/common/server/ClientContext;

    move-result-object v1

    .line 147
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iget v3, p0, Lcom/google/android/location/reporting/v;->d:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    sget v3, Landroid/os/Build$VERSION;->SDK_INT:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    const-string v3, "settings/%1$s"

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v7, v8

    invoke-static {v3, v7}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    if-eqz v5, :cond_0

    const-string v0, "nlpVersion"

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v0, v5}, Lcom/google/android/ulr/b;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    :cond_0
    if-eqz v6, :cond_1

    const-string v0, "osLevel"

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v0, v5}, Lcom/google/android/ulr/b;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    :cond_1
    if-eqz v2, :cond_2

    const-string v0, "platform"

    invoke-static {v2}, Lcom/google/android/ulr/b;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v0, v2}, Lcom/google/android/ulr/b;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    :cond_2
    iget-object v0, v4, Lcom/google/android/ulr/b;->a:Lcom/google/android/gms/common/server/n;

    const/4 v2, 0x0

    const/4 v4, 0x0

    const-class v5, Lcom/google/android/ulr/ApiSettings;

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/common/server/n;->a(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;Ljava/lang/Object;Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse;

    move-result-object v0

    check-cast v0, Lcom/google/android/ulr/ApiSettings;

    .line 152
    if-nez v0, :cond_3

    .line 153
    new-instance v0, Ljava/io/IOException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Received null settings from server for account "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {p1}, Lcom/google/android/gms/location/reporting/a/d;->a(Landroid/accounts/Account;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catch Lcom/google/android/gms/auth/q; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/android/volley/ac; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 161
    :catch_0
    move-exception v0

    .line 163
    const/4 v1, 0x0

    :try_start_1
    invoke-direct {p0, p1, v1}, Lcom/google/android/location/reporting/v;->a(Landroid/accounts/Account;Z)V

    .line 164
    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 171
    :catchall_0
    move-exception v0

    throw v0

    .line 156
    :cond_3
    const/4 v1, 0x0

    :try_start_2
    invoke-static {p1, v0, v1}, Lcom/google/android/location/reporting/a;->a(Landroid/accounts/Account;Lcom/google/android/ulr/ApiSettings;Z)Lcom/google/android/location/reporting/a;

    move-result-object v0

    .line 159
    const/4 v1, 0x1

    invoke-direct {p0, p1, v1}, Lcom/google/android/location/reporting/v;->a(Landroid/accounts/Account;Z)V
    :try_end_2
    .catch Lcom/google/android/gms/auth/q; {:try_start_2 .. :try_end_2} :catch_0
    .catch Lcom/android/volley/ac; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 171
    return-object v0

    .line 165
    :catch_1
    move-exception v0

    .line 166
    :try_start_3
    invoke-static {v0}, Lcom/google/android/location/reporting/v;->a(Lcom/android/volley/ac;)V

    .line 167
    new-instance v1, Ljava/io/IOException;

    invoke-direct {v1}, Ljava/io/IOException;-><init>()V

    .line 168
    invoke-virtual {v1, v0}, Ljava/io/IOException;->initCause(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    .line 169
    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0
.end method

.method public final a(Landroid/accounts/Account;Lcom/google/android/location/reporting/a;)Lcom/google/android/location/reporting/a;
    .locals 13

    .prologue
    const/4 v7, 0x1

    const/4 v8, 0x0

    .line 180
    invoke-direct {p0, p1}, Lcom/google/android/location/reporting/v;->c(Landroid/accounts/Account;)I

    move-result v2

    .line 181
    sget-object v5, Lcom/google/android/location/collectionlib/bn;->a:Ljava/lang/String;

    .line 192
    iget-object v1, p0, Lcom/google/android/location/reporting/v;->c:Lcom/google/android/location/reporting/config/h;

    invoke-virtual {v1}, Lcom/google/android/location/reporting/config/h;->d()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, p1}, Lcom/google/android/location/reporting/config/h;->a(Landroid/accounts/Account;)Lcom/google/android/location/reporting/config/AccountConfig;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/location/reporting/config/AccountConfig;->q()Z

    move-result v1

    if-eqz v0, :cond_0

    if-eqz v1, :cond_4

    :cond_0
    const/4 v0, 0x0

    move-object v6, v0

    .line 195
    :goto_0
    new-instance v4, Lcom/google/android/ulr/ApiSettings;

    iget-object v0, p2, Lcom/google/android/location/reporting/a;->c:Ljava/lang/Boolean;

    iget-object v1, p2, Lcom/google/android/location/reporting/a;->a:Ljava/lang/Long;

    iget-object v3, p2, Lcom/google/android/location/reporting/a;->b:Ljava/lang/Boolean;

    invoke-direct {v4, v6, v0, v1, v3}, Lcom/google/android/ulr/ApiSettings;-><init>(Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Long;Ljava/lang/Boolean;)V

    .line 198
    new-instance v0, Lcom/google/android/location/reporting/w;

    iget-object v1, p0, Lcom/google/android/location/reporting/v;->a:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/google/android/location/reporting/w;-><init>(Landroid/content/Context;)V

    .line 201
    :try_start_0
    new-instance v9, Lcom/google/android/ulr/b;

    invoke-direct {v9, v0}, Lcom/google/android/ulr/b;-><init>(Lcom/google/android/gms/common/server/n;)V

    .line 202
    iget-object v0, p0, Lcom/google/android/location/reporting/v;->a:Landroid/content/Context;

    invoke-static {v0, p1}, Lcom/google/android/location/reporting/v;->a(Landroid/content/Context;Landroid/accounts/Account;)Lcom/google/android/gms/common/server/ClientContext;

    move-result-object v1

    .line 204
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iget v2, p0, Lcom/google/android/location/reporting/v;->d:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    sget v3, Landroid/os/Build$VERSION;->SDK_INT:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    const-string v3, "settings/%1$s"

    const/4 v11, 0x1

    new-array v11, v11, [Ljava/lang/Object;

    const/4 v12, 0x0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v11, v12

    invoke-static {v3, v11}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    if-eqz v2, :cond_1

    const-string v0, "nlpVersion"

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v0, v2}, Lcom/google/android/ulr/b;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    :cond_1
    if-eqz v10, :cond_2

    const-string v0, "osLevel"

    invoke-static {v10}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v0, v2}, Lcom/google/android/ulr/b;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    :cond_2
    if-eqz v5, :cond_3

    const-string v0, "platform"

    invoke-static {v5}, Lcom/google/android/ulr/b;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v0, v2}, Lcom/google/android/ulr/b;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    :cond_3
    iget-object v0, v9, Lcom/google/android/ulr/b;->a:Lcom/google/android/gms/common/server/n;

    const/4 v2, 0x2

    const-class v5, Lcom/google/android/ulr/ApiSettings;

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/common/server/n;->a(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;Ljava/lang/Object;Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse;

    move-result-object v0

    check-cast v0, Lcom/google/android/ulr/ApiSettings;

    .line 207
    if-eqz v6, :cond_6

    move v1, v7

    :goto_1
    invoke-static {p1, v0, v1}, Lcom/google/android/location/reporting/a;->a(Landroid/accounts/Account;Lcom/google/android/ulr/ApiSettings;Z)Lcom/google/android/location/reporting/a;

    move-result-object v0

    .line 209
    const/4 v1, 0x1

    invoke-direct {p0, p1, v1}, Lcom/google/android/location/reporting/v;->a(Landroid/accounts/Account;Z)V
    :try_end_0
    .catch Lcom/google/android/gms/auth/q; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/android/volley/ac; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 220
    return-object v0

    .line 192
    :cond_4
    const-string v1, "GCoreUlr"

    const/4 v3, 0x4

    invoke-static {v1, v3}, Lcom/google/android/location/reporting/b/d;->a(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_5

    const-string v1, "GCoreUlr"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Sending GCM registration ID to server for "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {p1}, Lcom/google/android/gms/location/reporting/a/d;->a(Landroid/accounts/Account;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Lcom/google/android/location/reporting/b/d;->c(Ljava/lang/String;Ljava/lang/String;)V

    :cond_5
    move-object v6, v0

    goto/16 :goto_0

    :cond_6
    move v1, v8

    .line 207
    goto :goto_1

    .line 211
    :catch_0
    move-exception v0

    .line 212
    const/4 v1, 0x0

    :try_start_1
    invoke-direct {p0, p1, v1}, Lcom/google/android/location/reporting/v;->a(Landroid/accounts/Account;Z)V

    .line 213
    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 220
    :catchall_0
    move-exception v0

    throw v0

    .line 214
    :catch_1
    move-exception v0

    .line 215
    :try_start_2
    invoke-static {v0}, Lcom/google/android/location/reporting/v;->a(Lcom/android/volley/ac;)V

    .line 216
    new-instance v1, Ljava/io/IOException;

    invoke-direct {v1}, Ljava/io/IOException;-><init>()V

    .line 217
    invoke-virtual {v1, v0}, Ljava/io/IOException;->initCause(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    .line 218
    throw v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0
.end method

.method public final a(Landroid/accounts/Account;Ljava/util/List;Ljava/util/List;Ljava/util/ArrayList;J)Lcom/google/android/location/reporting/a;
    .locals 15

    .prologue
    .line 245
    invoke-direct/range {p0 .. p1}, Lcom/google/android/location/reporting/v;->c(Landroid/accounts/Account;)I

    move-result v7

    .line 246
    invoke-direct {p0}, Lcom/google/android/location/reporting/v;->a()Lcom/google/android/ulr/ApiClientInfo;

    move-result-object v9

    .line 248
    if-eqz p2, :cond_1

    invoke-interface/range {p2 .. p2}, Ljava/util/List;->size()I

    move-result v2

    if-lez v2, :cond_1

    const/4 v2, 0x1

    :goto_0
    const-string v3, "Must have at least 1 location to upload"

    invoke-static {v2, v3}, Lcom/google/android/gms/common/internal/bx;->b(ZLjava/lang/Object;)V

    .line 258
    const/4 v2, 0x0

    invoke-interface/range {p3 .. p3}, Ljava/util/List;->size()I

    move-result v3

    if-lez v3, :cond_c

    new-instance v6, Lcom/google/android/ulr/ApiReadingInfo;

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-direct {v6, v2, v3, v4}, Lcom/google/android/ulr/ApiReadingInfo;-><init>(Lcom/google/android/ulr/ApiBatteryCondition;Ljava/lang/String;Ljava/util/ArrayList;)V

    new-instance v5, Ljava/util/ArrayList;

    invoke-interface/range {p3 .. p3}, Ljava/util/List;->size()I

    move-result v2

    invoke-direct {v5, v2}, Ljava/util/ArrayList;-><init>(I)V

    invoke-interface/range {p3 .. p3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :cond_0
    :goto_1
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/gms/location/ActivityRecognitionResult;

    invoke-virtual {v2}, Lcom/google/android/gms/location/ActivityRecognitionResult;->c()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v4

    if-lez v4, :cond_0

    new-instance v10, Ljava/util/ArrayList;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v4

    invoke-direct {v10, v4}, Ljava/util/ArrayList;-><init>(I)V

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v11

    :goto_2
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/gms/location/DetectedActivity;

    invoke-virtual {v3}, Lcom/google/android/gms/location/DetectedActivity;->a()I

    move-result v4

    packed-switch v4, :pswitch_data_0

    :pswitch_0
    const-string v4, "unknown"

    :goto_3
    new-instance v12, Lcom/google/android/ulr/ApiActivity;

    invoke-virtual {v3}, Lcom/google/android/gms/location/DetectedActivity;->b()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-direct {v12, v3, v4}, Lcom/google/android/ulr/ApiActivity;-><init>(Ljava/lang/Integer;Ljava/lang/String;)V

    invoke-virtual {v10, v12}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 248
    :cond_1
    const/4 v2, 0x0

    goto :goto_0

    .line 258
    :pswitch_1
    const-string v4, "inVehicle"

    goto :goto_3

    :pswitch_2
    const-string v4, "onBicycle"

    goto :goto_3

    :pswitch_3
    const-string v4, "onFoot"

    goto :goto_3

    :pswitch_4
    const-string v4, "still"

    goto :goto_3

    :pswitch_5
    const-string v4, "tilting"

    goto :goto_3

    :pswitch_6
    const-string v4, "exitingVehicle"

    goto :goto_3

    :pswitch_7
    const-string v4, "walking"

    goto :goto_3

    :pswitch_8
    const-string v4, "running"

    goto :goto_3

    :cond_2
    new-instance v3, Lcom/google/android/ulr/ApiActivityReading;

    invoke-virtual {v2}, Lcom/google/android/gms/location/ActivityRecognitionResult;->d()J

    move-result-wide v12

    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-direct {v3, v10, v6, v2}, Lcom/google/android/ulr/ApiActivityReading;-><init>(Ljava/util/ArrayList;Lcom/google/android/ulr/ApiReadingInfo;Ljava/lang/Long;)V

    invoke-virtual {v5, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_3
    move-object v8, v5

    .line 259
    :goto_4
    move-object/from16 v0, p2

    invoke-direct {p0, v0}, Lcom/google/android/location/reporting/v;->a(Ljava/util/List;)Ljava/util/ArrayList;

    move-result-object v10

    .line 260
    new-instance v2, Lcom/google/android/ulr/ApiBatch;

    move-object/from16 v0, p4

    invoke-direct {v2, v8, v0, v10}, Lcom/google/android/ulr/ApiBatch;-><init>(Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/ArrayList;)V

    .line 263
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    .line 264
    new-instance v6, Lcom/google/android/ulr/ReportApiBatchRequest;

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-static/range {p5 .. p6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-direct {v6, v2, v9, v3, v4}, Lcom/google/android/ulr/ReportApiBatchRequest;-><init>(Lcom/google/android/ulr/ApiBatch;Lcom/google/android/ulr/ApiClientInfo;Ljava/lang/Long;Ljava/lang/Long;)V

    .line 266
    new-instance v2, Lcom/google/android/location/reporting/w;

    iget-object v3, p0, Lcom/google/android/location/reporting/v;->a:Landroid/content/Context;

    invoke-direct {v2, v3}, Lcom/google/android/location/reporting/w;-><init>(Landroid/content/Context;)V

    .line 269
    :try_start_0
    new-instance v4, Lcom/google/android/ulr/a;

    invoke-direct {v4, v2}, Lcom/google/android/ulr/a;-><init>(Lcom/google/android/gms/common/server/n;)V

    .line 270
    iget-object v2, p0, Lcom/google/android/location/reporting/v;->a:Landroid/content/Context;

    move-object/from16 v0, p1

    invoke-static {v2, v0}, Lcom/google/android/location/reporting/v;->a(Landroid/content/Context;Landroid/accounts/Account;)Lcom/google/android/gms/common/server/ClientContext;

    move-result-object v3

    .line 272
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iget v5, p0, Lcom/google/android/location/reporting/v;->d:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    sget v5, Landroid/os/Build$VERSION;->SDK_INT:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    sget-object v11, Lcom/google/android/location/collectionlib/bn;->a:Ljava/lang/String;

    const-string v5, "reports/%1$s"

    const/4 v12, 0x1

    new-array v12, v12, [Ljava/lang/Object;

    const/4 v13, 0x0

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v12, v13

    invoke-static {v5, v12}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    if-eqz v7, :cond_4

    const-string v2, "nlpVersion"

    invoke-static {v7}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v2, v7}, Lcom/google/android/ulr/a;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    :cond_4
    if-eqz v9, :cond_5

    const-string v2, "osLevel"

    invoke-static {v9}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v2, v7}, Lcom/google/android/ulr/a;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    :cond_5
    if-eqz v11, :cond_6

    const-string v2, "platform"

    invoke-static {v11}, Lcom/google/android/ulr/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v2, v7}, Lcom/google/android/ulr/a;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    :cond_6
    iget-object v2, v4, Lcom/google/android/ulr/a;->a:Lcom/google/android/gms/common/server/n;

    const/4 v4, 0x1

    const-class v7, Lcom/google/android/ulr/ReportApiBatchReply;

    invoke-virtual/range {v2 .. v7}, Lcom/google/android/gms/common/server/n;->a(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;Ljava/lang/Object;Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse;

    move-result-object v2

    check-cast v2, Lcom/google/android/ulr/ReportApiBatchReply;

    .line 277
    invoke-virtual {v10}, Ljava/util/ArrayList;->size()I

    move-result v3

    invoke-virtual {v2}, Lcom/google/android/ulr/ReportApiBatchReply;->b()Ljava/lang/Integer;

    move-result-object v4

    if-eqz v4, :cond_a

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    sub-int/2addr v3, v4

    if-lez v3, :cond_9

    invoke-static {v3}, Lcom/google/android/location/reporting/b/l;->b(I)V

    .line 279
    :cond_7
    :goto_5
    const/4 v3, 0x1

    move-object/from16 v0, p1

    invoke-direct {p0, v0, v3}, Lcom/google/android/location/reporting/v;->a(Landroid/accounts/Account;Z)V

    .line 281
    invoke-virtual {v2}, Lcom/google/android/ulr/ReportApiBatchReply;->getSettings()Lcom/google/android/ulr/ApiSettings;

    move-result-object v2

    .line 284
    if-nez v2, :cond_8

    .line 286
    move-object/from16 v0, p4

    invoke-static {v10, v8, v0}, Lcom/google/android/location/reporting/b/l;->b(Ljava/util/List;Ljava/util/List;Ljava/util/List;)V

    .line 288
    :cond_8
    if-eqz v2, :cond_b

    const/4 v3, 0x0

    move-object/from16 v0, p1

    invoke-static {v0, v2, v3}, Lcom/google/android/location/reporting/a;->a(Landroid/accounts/Account;Lcom/google/android/ulr/ApiSettings;Z)Lcom/google/android/location/reporting/a;

    move-result-object v2

    .line 298
    :goto_6
    return-object v2

    .line 277
    :cond_9
    if-gez v3, :cond_7

    rsub-int/lit8 v3, v3, 0x0

    invoke-static {v3}, Lcom/google/android/location/reporting/b/l;->c(I)V
    :try_end_0
    .catch Lcom/google/android/gms/auth/q; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/android/volley/ac; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_5

    .line 289
    :catch_0
    move-exception v2

    .line 290
    const/4 v3, 0x0

    :try_start_1
    move-object/from16 v0, p1

    invoke-direct {p0, v0, v3}, Lcom/google/android/location/reporting/v;->a(Landroid/accounts/Account;Z)V

    .line 291
    throw v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 298
    :catchall_0
    move-exception v2

    throw v2

    .line 277
    :cond_a
    :try_start_2
    invoke-static {v3}, Lcom/google/android/location/reporting/b/l;->d(I)V
    :try_end_2
    .catch Lcom/google/android/gms/auth/q; {:try_start_2 .. :try_end_2} :catch_0
    .catch Lcom/android/volley/ac; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_5

    .line 292
    :catch_1
    move-exception v2

    .line 293
    :try_start_3
    invoke-static {v2}, Lcom/google/android/location/reporting/v;->a(Lcom/android/volley/ac;)V

    .line 294
    new-instance v3, Ljava/io/IOException;

    invoke-direct {v3}, Ljava/io/IOException;-><init>()V

    .line 295
    invoke-virtual {v3, v2}, Ljava/io/IOException;->initCause(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    .line 296
    throw v3
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 288
    :cond_b
    const/4 v2, 0x0

    goto :goto_6

    :cond_c
    move-object v8, v2

    goto/16 :goto_4

    .line 258
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_0
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
    .end packed-switch
.end method

.method public final b(Landroid/accounts/Account;)Lcom/google/android/location/reporting/a;
    .locals 10

    .prologue
    const/4 v1, 0x0

    .line 328
    invoke-direct {p0}, Lcom/google/android/location/reporting/v;->a()Lcom/google/android/ulr/ApiClientInfo;

    move-result-object v0

    .line 330
    invoke-direct {p0, p1}, Lcom/google/android/location/reporting/v;->c(Landroid/accounts/Account;)I

    move-result v2

    .line 331
    sget-object v5, Lcom/google/android/location/collectionlib/bn;->a:Ljava/lang/String;

    .line 334
    new-instance v4, Lcom/google/android/ulr/DeleteApiLocationsRequest;

    invoke-direct {v4, v0, v1, v1, v1}, Lcom/google/android/ulr/DeleteApiLocationsRequest;-><init>(Lcom/google/android/ulr/ApiClientInfo;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Long;)V

    .line 341
    new-instance v0, Lcom/google/android/location/reporting/w;

    iget-object v1, p0, Lcom/google/android/location/reporting/v;->a:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/google/android/location/reporting/w;-><init>(Landroid/content/Context;)V

    .line 347
    :try_start_0
    new-instance v6, Lcom/google/android/ulr/b;

    invoke-direct {v6, v0}, Lcom/google/android/ulr/b;-><init>(Lcom/google/android/gms/common/server/n;)V

    .line 348
    iget-object v0, p0, Lcom/google/android/location/reporting/v;->a:Landroid/content/Context;

    invoke-static {v0, p1}, Lcom/google/android/location/reporting/v;->a(Landroid/content/Context;Landroid/accounts/Account;)Lcom/google/android/gms/common/server/ClientContext;

    move-result-object v1

    .line 350
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iget v2, p0, Lcom/google/android/location/reporting/v;->d:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    sget v3, Landroid/os/Build$VERSION;->SDK_INT:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    const-string v3, "deletes/%1$s"

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v8, v9

    invoke-static {v3, v8}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    if-eqz v2, :cond_0

    const-string v0, "nlpVersion"

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v0, v2}, Lcom/google/android/ulr/b;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    :cond_0
    if-eqz v7, :cond_1

    const-string v0, "osLevel"

    invoke-static {v7}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v0, v2}, Lcom/google/android/ulr/b;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    :cond_1
    if-eqz v5, :cond_2

    const-string v0, "platform"

    invoke-static {v5}, Lcom/google/android/ulr/b;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v0, v2}, Lcom/google/android/ulr/b;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    :cond_2
    iget-object v0, v6, Lcom/google/android/ulr/b;->a:Lcom/google/android/gms/common/server/n;

    const/4 v2, 0x1

    const-class v5, Lcom/google/android/ulr/DeleteApiLocationsReply;

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/common/server/n;->a(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;Ljava/lang/Object;Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse;

    move-result-object v0

    check-cast v0, Lcom/google/android/ulr/DeleteApiLocationsReply;

    invoke-virtual {v0}, Lcom/google/android/ulr/DeleteApiLocationsReply;->getSettings()Lcom/google/android/ulr/ApiSettings;

    move-result-object v0

    .line 355
    const/4 v1, 0x1

    invoke-direct {p0, p1, v1}, Lcom/google/android/location/reporting/v;->a(Landroid/accounts/Account;Z)V

    .line 356
    const/4 v1, 0x0

    invoke-static {p1, v0, v1}, Lcom/google/android/location/reporting/a;->a(Landroid/accounts/Account;Lcom/google/android/ulr/ApiSettings;Z)Lcom/google/android/location/reporting/a;
    :try_end_0
    .catch Lcom/google/android/gms/auth/q; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/android/volley/ac; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 366
    return-object v0

    .line 357
    :catch_0
    move-exception v0

    .line 358
    const/4 v1, 0x0

    :try_start_1
    invoke-direct {p0, p1, v1}, Lcom/google/android/location/reporting/v;->a(Landroid/accounts/Account;Z)V

    .line 359
    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 366
    :catchall_0
    move-exception v0

    throw v0

    .line 360
    :catch_1
    move-exception v0

    .line 361
    :try_start_2
    invoke-static {v0}, Lcom/google/android/location/reporting/v;->a(Lcom/android/volley/ac;)V

    .line 362
    new-instance v1, Ljava/io/IOException;

    invoke-direct {v1}, Ljava/io/IOException;-><init>()V

    .line 363
    invoke-virtual {v1, v0}, Ljava/io/IOException;->initCause(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    .line 364
    throw v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0
.end method
