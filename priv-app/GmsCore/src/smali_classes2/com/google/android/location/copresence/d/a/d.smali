.class public final Lcom/google/android/location/copresence/d/a/d;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final a:Ljava/util/List;

.field final b:Ljava/util/List;

.field final c:[B

.field final d:[B

.field private final e:I

.field private final f:Ljava/util/List;

.field private final g:[B


# direct methods
.method private constructor <init>(ILjava/util/List;Ljava/util/List;[BLjava/util/List;[B[B)V
    .locals 0

    .prologue
    .line 58
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 59
    iput p1, p0, Lcom/google/android/location/copresence/d/a/d;->e:I

    .line 60
    iput-object p4, p0, Lcom/google/android/location/copresence/d/a/d;->g:[B

    .line 61
    iput-object p2, p0, Lcom/google/android/location/copresence/d/a/d;->f:Ljava/util/List;

    .line 62
    iput-object p3, p0, Lcom/google/android/location/copresence/d/a/d;->a:Ljava/util/List;

    .line 63
    iput-object p5, p0, Lcom/google/android/location/copresence/d/a/d;->b:Ljava/util/List;

    .line 64
    iput-object p6, p0, Lcom/google/android/location/copresence/d/a/d;->c:[B

    .line 65
    iput-object p7, p0, Lcom/google/android/location/copresence/d/a/d;->d:[B

    .line 66
    return-void
.end method

.method public static a([B)Lcom/google/android/location/copresence/d/a/d;
    .locals 18

    .prologue
    .line 168
    const/4 v2, 0x0

    .line 169
    const/4 v3, 0x0

    .line 171
    const/4 v4, 0x0

    .line 172
    const/4 v5, 0x0

    .line 173
    const/4 v7, 0x0

    .line 175
    const/4 v6, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    .line 178
    :goto_0
    :try_start_0
    move-object/from16 v0, p0

    array-length v10, v0

    if-ge v2, v10, :cond_7

    .line 180
    aget-byte v10, p0, v2

    and-int/lit16 v10, v10, 0xff

    .line 181
    if-eqz v10, :cond_7

    .line 182
    add-int/lit8 v11, v10, -0x1

    .line 185
    add-int/lit8 v2, v2, 0x1

    .line 186
    aget-byte v10, p0, v2

    and-int/lit16 v10, v10, 0xff

    .line 187
    add-int/lit8 v12, v2, 0x1

    .line 188
    sparse-switch v10, :sswitch_data_0

    .line 238
    :cond_0
    :goto_1
    add-int v2, v12, v11

    .line 244
    goto :goto_0

    .line 190
    :sswitch_0
    aget-byte v2, p0, v12

    and-int/lit16 v3, v2, 0xff

    .line 191
    goto :goto_1

    .line 193
    :sswitch_1
    move-object/from16 v0, p0

    invoke-static {v0, v12, v11}, Lcom/google/android/location/copresence/d/a/p;->a([BII)[B

    move-result-object v6

    goto :goto_1

    .line 196
    :sswitch_2
    move-object/from16 v0, p0

    invoke-static {v0, v12, v11}, Lcom/google/android/location/copresence/d/a/p;->a([BII)[B

    move-result-object v2

    .line 198
    array-length v10, v2

    const/4 v13, 0x2

    if-ge v10, v13, :cond_3

    const/4 v2, 0x0

    .line 199
    :goto_2
    if-eqz v2, :cond_0

    .line 200
    if-nez v7, :cond_1

    .line 201
    new-instance v7, Ljava/util/ArrayList;

    const/4 v10, 0x1

    invoke-direct {v7, v10}, Ljava/util/ArrayList;-><init>(I)V

    .line 203
    :cond_1
    invoke-interface {v7, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 249
    :catch_0
    move-exception v2

    const/4 v2, 0x5

    invoke-static {v2}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 250
    const-string v2, "AdvertisedData: Invalid scan data length."

    invoke-static {v2}, Lcom/google/android/location/copresence/ag;->d(Ljava/lang/String;)I

    .line 252
    :cond_2
    const/4 v2, 0x0

    :goto_3
    return-object v2

    .line 198
    :cond_3
    const/4 v10, 0x0

    const/4 v13, 0x2

    :try_start_1
    invoke-static {v2, v10, v13}, Lcom/google/android/location/copresence/d/a/p;->a([BII)[B

    move-result-object v13

    array-length v10, v2

    add-int/lit8 v10, v10, -0x2

    if-nez v10, :cond_4

    const/4 v2, 0x0

    new-array v2, v2, [B

    move-object v10, v2

    :goto_4
    new-instance v2, Lcom/google/android/location/copresence/d/a/p;

    invoke-direct {v2, v13, v10}, Lcom/google/android/location/copresence/d/a/p;-><init>([B[B)V

    goto :goto_2

    :cond_4
    const/4 v14, 0x2

    invoke-static {v2, v14, v10}, Lcom/google/android/location/copresence/d/a/p;->a([BII)[B

    move-result-object v2

    move-object v10, v2

    goto :goto_4

    .line 207
    :sswitch_3
    move-object/from16 v0, p0

    invoke-static {v0, v12, v11}, Lcom/google/android/location/copresence/d/a/p;->a([BII)[B

    move-result-object v8

    goto :goto_1

    .line 210
    :sswitch_4
    move-object/from16 v0, p0

    invoke-static {v0, v12, v11}, Lcom/google/android/location/copresence/d/a/p;->a([BII)[B

    move-result-object v9

    goto :goto_1

    .line 216
    :sswitch_5
    move-object/from16 v0, p0

    invoke-static {v0, v12, v11}, Lcom/google/android/location/copresence/d/a/p;->a([BII)[B

    move-result-object v2

    .line 218
    if-nez v5, :cond_5

    .line 219
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 223
    :cond_5
    invoke-static {v2}, Lcom/google/android/location/copresence/d/a/p;->a([B)[B

    move-result-object v2

    invoke-interface {v5, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 228
    :sswitch_6
    move-object/from16 v0, p0

    invoke-static {v0, v12, v11}, Lcom/google/android/location/copresence/d/a/p;->a([BII)[B

    move-result-object v2

    .line 230
    invoke-static {v2}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v2

    sget-object v10, Ljava/nio/ByteOrder;->LITTLE_ENDIAN:Ljava/nio/ByteOrder;

    invoke-virtual {v2, v10}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    move-result-object v2

    .line 232
    const/16 v10, 0x8

    invoke-virtual {v2, v10}, Ljava/nio/ByteBuffer;->getLong(I)J

    move-result-wide v14

    .line 233
    const/4 v10, 0x0

    invoke-virtual {v2, v10}, Ljava/nio/ByteBuffer;->getLong(I)J

    move-result-wide v16

    .line 234
    if-nez v4, :cond_6

    .line 235
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 237
    :cond_6
    new-instance v2, Ljava/util/UUID;

    move-wide/from16 v0, v16

    invoke-direct {v2, v14, v15, v0, v1}, Ljava/util/UUID;-><init>(JJ)V

    invoke-interface {v4, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_1

    .line 246
    :cond_7
    new-instance v2, Lcom/google/android/location/copresence/d/a/d;

    invoke-direct/range {v2 .. v9}, Lcom/google/android/location/copresence/d/a/d;-><init>(ILjava/util/List;Ljava/util/List;[BLjava/util/List;[B[B)V
    :try_end_1
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_3

    .line 188
    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x2 -> :sswitch_5
        0x3 -> :sswitch_5
        0x6 -> :sswitch_6
        0x7 -> :sswitch_6
        0x8 -> :sswitch_3
        0x9 -> :sswitch_4
        0x16 -> :sswitch_2
        0xff -> :sswitch_1
    .end sparse-switch
.end method


# virtual methods
.method public final toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 258
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 259
    const-string v1, "AdvertisedData [mAdvertiseType="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/google/android/location/copresence/d/a/d;->e:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 261
    iget-object v1, p0, Lcom/google/android/location/copresence/d/a/d;->f:Ljava/util/List;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/location/copresence/d/a/d;->f:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 262
    const-string v1, ", m128BitServiceUuids="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/location/copresence/d/a/d;->f:Ljava/util/List;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 265
    :cond_0
    iget-object v1, p0, Lcom/google/android/location/copresence/d/a/d;->a:Ljava/util/List;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/android/location/copresence/d/a/d;->a:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    .line 266
    const-string v1, ", m16BitServiceUuids="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/location/copresence/d/a/d;->a:Ljava/util/List;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 269
    :cond_1
    iget-object v1, p0, Lcom/google/android/location/copresence/d/a/d;->g:[B

    if-eqz v1, :cond_2

    .line 270
    const-string v1, ", mManufacturerData="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/location/copresence/d/a/d;->g:[B

    invoke-static {v2}, Ljava/util/Arrays;->toString([B)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 273
    :cond_2
    iget-object v1, p0, Lcom/google/android/location/copresence/d/a/d;->b:Ljava/util/List;

    if-eqz v1, :cond_3

    .line 274
    const-string v1, ", mServiceData="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/location/copresence/d/a/d;->b:Ljava/util/List;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 277
    :cond_3
    iget-object v1, p0, Lcom/google/android/location/copresence/d/a/d;->c:[B

    if-eqz v1, :cond_4

    .line 278
    const-string v1, ", mShortName="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    new-instance v2, Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/location/copresence/d/a/d;->c:[B

    invoke-direct {v2, v3}, Ljava/lang/String;-><init>([B)V

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 281
    :cond_4
    iget-object v1, p0, Lcom/google/android/location/copresence/d/a/d;->d:[B

    if-eqz v1, :cond_5

    .line 282
    const-string v1, ", mCompleteName="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    new-instance v2, Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/location/copresence/d/a/d;->d:[B

    invoke-direct {v2, v3}, Ljava/lang/String;-><init>([B)V

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 285
    :cond_5
    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 286
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
