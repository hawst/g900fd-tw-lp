.class final Lcom/google/android/location/copresence/l/v;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/location/copresence/l/ab;


# instance fields
.field final synthetic a:Lcom/google/android/location/copresence/o/k;

.field final synthetic b:Ljava/util/HashSet;

.field final synthetic c:Lcom/google/android/location/copresence/l/w;

.field final synthetic d:Lcom/google/android/location/copresence/a/a;

.field final synthetic e:Lcom/google/android/location/copresence/l/y;

.field final synthetic f:Lcom/google/android/location/copresence/l/x;

.field final synthetic g:Lcom/google/android/location/copresence/l/p;

.field final synthetic h:Ljava/util/Set;

.field final synthetic i:Lcom/google/android/location/copresence/l/t;


# direct methods
.method constructor <init>(Lcom/google/android/location/copresence/l/t;Lcom/google/android/location/copresence/o/k;Ljava/util/HashSet;Lcom/google/android/location/copresence/l/w;Lcom/google/android/location/copresence/a/a;Lcom/google/android/location/copresence/l/y;Lcom/google/android/location/copresence/l/x;Lcom/google/android/location/copresence/l/p;Ljava/util/Set;)V
    .locals 0

    .prologue
    .line 362
    iput-object p1, p0, Lcom/google/android/location/copresence/l/v;->i:Lcom/google/android/location/copresence/l/t;

    iput-object p2, p0, Lcom/google/android/location/copresence/l/v;->a:Lcom/google/android/location/copresence/o/k;

    iput-object p3, p0, Lcom/google/android/location/copresence/l/v;->b:Ljava/util/HashSet;

    iput-object p4, p0, Lcom/google/android/location/copresence/l/v;->c:Lcom/google/android/location/copresence/l/w;

    iput-object p5, p0, Lcom/google/android/location/copresence/l/v;->d:Lcom/google/android/location/copresence/a/a;

    iput-object p6, p0, Lcom/google/android/location/copresence/l/v;->e:Lcom/google/android/location/copresence/l/y;

    iput-object p7, p0, Lcom/google/android/location/copresence/l/v;->f:Lcom/google/android/location/copresence/l/x;

    iput-object p8, p0, Lcom/google/android/location/copresence/l/v;->g:Lcom/google/android/location/copresence/l/p;

    iput-object p9, p0, Lcom/google/android/location/copresence/l/v;->h:Ljava/util/Set;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/location/copresence/l/aa;I)V
    .locals 5

    .prologue
    const/4 v3, 0x3

    .line 418
    invoke-static {v3}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 419
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "ReportRpcManager: Network error on report call.  Common status code was "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->b(Ljava/lang/String;)I

    .line 423
    :cond_0
    const/4 v0, 0x7

    if-ne p2, v0, :cond_2

    .line 424
    iget-object v0, p0, Lcom/google/android/location/copresence/l/v;->a:Lcom/google/android/location/copresence/o/k;

    if-eqz v0, :cond_1

    .line 425
    iget-object v0, p0, Lcom/google/android/location/copresence/l/v;->a:Lcom/google/android/location/copresence/o/k;

    new-instance v1, Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/google/android/location/copresence/l/v;->b:Ljava/util/HashSet;

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    const/4 v2, 0x2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/location/copresence/o/k;->a(Ljava/util/List;I)V

    .line 437
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/google/android/location/copresence/l/v;->h:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/location/copresence/x;

    .line 438
    iget-object v2, p0, Lcom/google/android/location/copresence/l/v;->d:Lcom/google/android/location/copresence/a/a;

    invoke-static {v2}, Lcom/google/android/location/copresence/p/a;->a(Lcom/google/android/location/copresence/a/a;)Lcom/google/android/location/copresence/p/a;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/location/copresence/l/v;->i:Lcom/google/android/location/copresence/l/t;

    iget-object v3, v3, Lcom/google/android/location/copresence/l/t;->a:Landroid/content/Context;

    const/4 v4, 0x1

    invoke-virtual {v2, v3, v0, v4}, Lcom/google/android/location/copresence/p/a;->a(Landroid/content/Context;Lcom/google/android/gms/location/copresence/x;I)Z

    goto :goto_1

    .line 429
    :cond_2
    iget-object v0, p0, Lcom/google/android/location/copresence/l/v;->a:Lcom/google/android/location/copresence/o/k;

    if-eqz v0, :cond_1

    .line 430
    iget-object v0, p0, Lcom/google/android/location/copresence/l/v;->a:Lcom/google/android/location/copresence/o/k;

    new-instance v1, Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/google/android/location/copresence/l/v;->b:Ljava/util/HashSet;

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-virtual {v0, v1, v3}, Lcom/google/android/location/copresence/o/k;->a(Ljava/util/List;I)V

    goto :goto_0

    .line 442
    :cond_3
    iget-object v0, p0, Lcom/google/android/location/copresence/l/v;->i:Lcom/google/android/location/copresence/l/t;

    iget-object v0, p0, Lcom/google/android/location/copresence/l/v;->f:Lcom/google/android/location/copresence/l/x;

    iget-object v1, p0, Lcom/google/android/location/copresence/l/v;->e:Lcom/google/android/location/copresence/l/y;

    invoke-static {v0, v1}, Lcom/google/android/location/copresence/l/t;->a(Lcom/google/android/location/copresence/l/x;Lcom/google/android/location/copresence/l/y;)V

    .line 443
    iget-object v0, p0, Lcom/google/android/location/copresence/l/v;->g:Lcom/google/android/location/copresence/l/p;

    invoke-virtual {v0}, Lcom/google/android/location/copresence/l/p;->a()V

    .line 444
    return-void
.end method

.method public final synthetic a(Lcom/google/android/location/copresence/l/aa;Ljava/lang/Object;)V
    .locals 10

    .prologue
    const/4 v1, 0x4

    const/4 v9, 0x1

    const/4 v3, 0x0

    .line 362
    check-cast p2, Lcom/google/ac/b/c/da;

    iget-object v0, p0, Lcom/google/android/location/copresence/l/v;->a:Lcom/google/android/location/copresence/o/k;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/location/copresence/l/v;->a:Lcom/google/android/location/copresence/o/k;

    new-instance v2, Ljava/util/ArrayList;

    iget-object v4, p0, Lcom/google/android/location/copresence/l/v;->b:Ljava/util/HashSet;

    invoke-direct {v2, v4}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-virtual {v0, v2, v1}, Lcom/google/android/location/copresence/o/k;->a(Ljava/util/List;I)V

    :cond_0
    iget-object v0, p2, Lcom/google/ac/b/c/da;->d:Lcom/google/ac/b/c/dg;

    if-eqz v0, :cond_7

    iget-object v0, p2, Lcom/google/ac/b/c/da;->d:Lcom/google/ac/b/c/dg;

    iget-object v4, v0, Lcom/google/ac/b/c/dg;->b:[Lcom/google/ac/b/c/bq;

    array-length v5, v4

    move v2, v3

    :goto_0
    if-ge v2, v5, :cond_2

    aget-object v6, v4, v2

    iget-object v0, v6, Lcom/google/ac/b/c/bq;->c:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-ne v0, v9, :cond_8

    iget-object v0, p0, Lcom/google/android/location/copresence/l/v;->c:Lcom/google/android/location/copresence/l/w;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/location/copresence/l/v;->c:Lcom/google/android/location/copresence/l/w;

    iget-object v7, p0, Lcom/google/android/location/copresence/l/v;->d:Lcom/google/android/location/copresence/a/a;

    invoke-interface {v0, v6, v7}, Lcom/google/android/location/copresence/l/w;->a(Lcom/google/ac/b/c/bq;Lcom/google/android/location/copresence/a/a;)V

    :cond_1
    const/4 v0, 0x3

    :goto_1
    iget-object v6, v6, Lcom/google/ac/b/c/bq;->a:Ljava/lang/String;

    invoke-static {v6}, Lcom/google/android/gms/location/copresence/x;->a(Ljava/lang/String;)Lcom/google/android/gms/location/copresence/x;

    move-result-object v6

    iget-object v7, p0, Lcom/google/android/location/copresence/l/v;->d:Lcom/google/android/location/copresence/a/a;

    invoke-static {v7}, Lcom/google/android/location/copresence/p/a;->a(Lcom/google/android/location/copresence/a/a;)Lcom/google/android/location/copresence/p/a;

    move-result-object v7

    iget-object v8, p0, Lcom/google/android/location/copresence/l/v;->i:Lcom/google/android/location/copresence/l/t;

    iget-object v8, v8, Lcom/google/android/location/copresence/l/t;->a:Landroid/content/Context;

    invoke-virtual {v7, v8, v6, v0}, Lcom/google/android/location/copresence/p/a;->a(Landroid/content/Context;Lcom/google/android/gms/location/copresence/x;I)Z

    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/google/android/location/copresence/l/v;->i:Lcom/google/android/location/copresence/l/t;

    iget-object v0, v0, Lcom/google/android/location/copresence/l/t;->d:Lcom/google/android/location/copresence/o/v;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/location/copresence/l/v;->i:Lcom/google/android/location/copresence/l/t;

    iget-object v0, v0, Lcom/google/android/location/copresence/l/t;->d:Lcom/google/android/location/copresence/o/v;

    iget-object v1, p0, Lcom/google/android/location/copresence/l/v;->d:Lcom/google/android/location/copresence/a/a;

    iget-object v2, p2, Lcom/google/ac/b/c/da;->d:Lcom/google/ac/b/c/dg;

    iget-object v2, v2, Lcom/google/ac/b/c/dg;->e:Ljava/lang/String;

    iget-object v0, v0, Lcom/google/android/location/copresence/o/v;->a:Ljava/util/Map;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_3
    iget-object v0, p0, Lcom/google/android/location/copresence/l/v;->e:Lcom/google/android/location/copresence/l/y;

    if-eqz v0, :cond_4

    iget-object v0, p2, Lcom/google/ac/b/c/da;->d:Lcom/google/ac/b/c/dg;

    iget-object v0, v0, Lcom/google/ac/b/c/dg;->c:[Lcom/google/ac/b/c/bm;

    iget-object v1, p0, Lcom/google/android/location/copresence/l/v;->e:Lcom/google/android/location/copresence/l/y;

    sget-object v2, Lcom/google/android/gms/common/api/Status;->a:Lcom/google/android/gms/common/api/Status;

    invoke-interface {v1, v0}, Lcom/google/android/location/copresence/l/y;->a([Lcom/google/ac/b/c/bm;)V

    :cond_4
    iget-object v0, p0, Lcom/google/android/location/copresence/l/v;->f:Lcom/google/android/location/copresence/l/x;

    if-eqz v0, :cond_5

    iget-object v0, p2, Lcom/google/ac/b/c/da;->d:Lcom/google/ac/b/c/dg;

    iget-object v0, v0, Lcom/google/ac/b/c/dg;->d:[Lcom/google/ac/b/c/ah;

    iget-object v1, p0, Lcom/google/android/location/copresence/l/v;->f:Lcom/google/android/location/copresence/l/x;

    sget-object v2, Lcom/google/android/gms/common/api/Status;->a:Lcom/google/android/gms/common/api/Status;

    iget-object v4, p0, Lcom/google/android/location/copresence/l/v;->b:Ljava/util/HashSet;

    invoke-interface {v1, v2, v0, v3, v4}, Lcom/google/android/location/copresence/l/x;->a(Lcom/google/android/gms/common/api/Status;[Lcom/google/ac/b/c/ah;ZLjava/util/HashSet;)V

    :cond_5
    iget-object v0, p2, Lcom/google/ac/b/c/da;->e:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_7

    const/4 v0, 0x2

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v0

    if-eqz v0, :cond_6

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "ReportRpcManager: Registered a user with "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p2, Lcom/google/ac/b/c/da;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->a(Ljava/lang/String;)I

    :cond_6
    iget-object v0, p0, Lcom/google/android/location/copresence/l/v;->i:Lcom/google/android/location/copresence/l/t;

    iget-object v0, v0, Lcom/google/android/location/copresence/l/t;->a:Landroid/content/Context;

    iget-object v1, p2, Lcom/google/ac/b/c/da;->e:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/location/copresence/l/v;->d:Lcom/google/android/location/copresence/a/a;

    iget-object v2, v2, Lcom/google/android/location/copresence/a/a;->a:Landroid/accounts/Account;

    iget-object v2, v2, Landroid/accounts/Account;->name:Ljava/lang/String;

    const-string v4, "copresence_gcm_pref"

    invoke-virtual {v0, v4, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v3, "copresence_uuid"

    invoke-interface {v0, v3, v1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "REGISTERED_"

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, v9}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    :cond_7
    iget-object v0, p0, Lcom/google/android/location/copresence/l/v;->g:Lcom/google/android/location/copresence/l/p;

    invoke-virtual {v0}, Lcom/google/android/location/copresence/l/p;->a()V

    return-void

    :cond_8
    move v0, v1

    goto/16 :goto_1
.end method
