.class public final Lcom/google/android/location/copresence/d/a/e;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/location/copresence/d/a/n;


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x12
.end annotation


# instance fields
.field final a:Lcom/google/android/location/copresence/d/a/o;

.field private final b:Lcom/google/android/location/copresence/d/e;

.field private final c:Landroid/bluetooth/BluetoothAdapter$LeScanCallback;


# direct methods
.method public constructor <init>(Lcom/google/android/location/copresence/d/e;Lcom/google/android/location/copresence/d/a/o;)V
    .locals 1

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    iput-object p1, p0, Lcom/google/android/location/copresence/d/a/e;->b:Lcom/google/android/location/copresence/d/e;

    .line 33
    new-instance v0, Lcom/google/android/location/copresence/d/a/f;

    invoke-direct {v0, p0}, Lcom/google/android/location/copresence/d/a/f;-><init>(Lcom/google/android/location/copresence/d/a/e;)V

    iput-object v0, p0, Lcom/google/android/location/copresence/d/a/e;->c:Landroid/bluetooth/BluetoothAdapter$LeScanCallback;

    .line 98
    iput-object p2, p0, Lcom/google/android/location/copresence/d/a/e;->a:Lcom/google/android/location/copresence/d/a/o;

    .line 99
    return-void
.end method


# virtual methods
.method public final a()Z
    .locals 2

    .prologue
    .line 103
    const/4 v0, 0x2

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 104
    const-string v0, "BleScanImplJBMR2startScan"

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->a(Ljava/lang/String;)I

    .line 106
    :cond_0
    iget-object v0, p0, Lcom/google/android/location/copresence/d/a/e;->b:Lcom/google/android/location/copresence/d/e;

    iget-object v0, v0, Lcom/google/android/location/copresence/d/e;->a:Landroid/bluetooth/BluetoothAdapter;

    iget-object v1, p0, Lcom/google/android/location/copresence/d/a/e;->c:Landroid/bluetooth/BluetoothAdapter$LeScanCallback;

    invoke-virtual {v0, v1}, Landroid/bluetooth/BluetoothAdapter;->startLeScan(Landroid/bluetooth/BluetoothAdapter$LeScanCallback;)Z

    move-result v0

    return v0
.end method

.method public final b()Z
    .locals 2

    .prologue
    .line 111
    const/4 v0, 0x2

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 112
    const-string v0, "BleScanImplJBMR2stopScan"

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->a(Ljava/lang/String;)I

    .line 114
    :cond_0
    iget-object v0, p0, Lcom/google/android/location/copresence/d/a/e;->b:Lcom/google/android/location/copresence/d/e;

    iget-object v0, v0, Lcom/google/android/location/copresence/d/e;->a:Landroid/bluetooth/BluetoothAdapter;

    iget-object v1, p0, Lcom/google/android/location/copresence/d/a/e;->c:Landroid/bluetooth/BluetoothAdapter$LeScanCallback;

    invoke-virtual {v0, v1}, Landroid/bluetooth/BluetoothAdapter;->stopLeScan(Landroid/bluetooth/BluetoothAdapter$LeScanCallback;)V

    .line 115
    const/4 v0, 0x1

    return v0
.end method
