.class public final Lcom/google/android/location/places/d/b;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final a:Ljava/util/concurrent/atomic/AtomicInteger;

.field final b:Landroid/content/Context;

.field final c:Lcom/google/android/gms/common/util/p;

.field final d:Landroid/app/AlarmManager;

.field final e:Lcom/google/android/location/geofencer/service/ah;

.field final f:Lcom/google/android/location/places/d/d;

.field final g:Ljava/util/Map;

.field final h:Ljava/util/Map;

.field private final i:Landroid/content/BroadcastReceiver;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/gms/common/util/p;Landroid/app/AlarmManager;Lcom/google/android/location/geofencer/service/ah;Landroid/support/v4/a/m;Lcom/google/android/location/places/d/d;)V
    .locals 4

    .prologue
    .line 65
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 56
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/location/places/d/b;->g:Ljava/util/Map;

    .line 57
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/location/places/d/b;->h:Ljava/util/Map;

    .line 172
    new-instance v0, Lcom/google/android/location/places/d/c;

    invoke-direct {v0, p0}, Lcom/google/android/location/places/d/c;-><init>(Lcom/google/android/location/places/d/b;)V

    iput-object v0, p0, Lcom/google/android/location/places/d/b;->i:Landroid/content/BroadcastReceiver;

    .line 66
    iput-object p1, p0, Lcom/google/android/location/places/d/b;->b:Landroid/content/Context;

    .line 67
    iput-object p2, p0, Lcom/google/android/location/places/d/b;->c:Lcom/google/android/gms/common/util/p;

    .line 68
    iput-object p3, p0, Lcom/google/android/location/places/d/b;->d:Landroid/app/AlarmManager;

    .line 69
    iput-object p4, p0, Lcom/google/android/location/places/d/b;->e:Lcom/google/android/location/geofencer/service/ah;

    .line 70
    iput-object p6, p0, Lcom/google/android/location/places/d/b;->f:Lcom/google/android/location/places/d/d;

    .line 71
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    iget-object v1, p0, Lcom/google/android/location/places/d/b;->c:Lcom/google/android/gms/common/util/p;

    invoke-interface {v1}, Lcom/google/android/gms/common/util/p;->b()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Long;->intValue()I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/location/places/d/b;->a:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 75
    iget-object v0, p0, Lcom/google/android/location/places/d/b;->i:Landroid/content/BroadcastReceiver;

    new-instance v1, Landroid/content/IntentFilter;

    const-string v2, "com.google.android.location.internal.action.PLACES_REFRESH_USER_DATA_SUBSCRIPTION"

    invoke-static {v2}, Lcom/google/android/location/internal/PendingIntentCallbackService;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {p5, v0, v1}, Landroid/support/v4/a/m;->a(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)V

    .line 77
    return-void
.end method
