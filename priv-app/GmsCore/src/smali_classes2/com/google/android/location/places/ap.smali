.class final Lcom/google/android/location/places/ap;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/location/places/x;


# instance fields
.field final synthetic a:Lcom/google/android/location/places/an;


# direct methods
.method constructor <init>(Lcom/google/android/location/places/an;)V
    .locals 0

    .prologue
    .line 423
    iput-object p1, p0, Lcom/google/android/location/places/ap;->a:Lcom/google/android/location/places/an;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/util/List;)V
    .locals 5

    .prologue
    .line 426
    const-string v0, "Places"

    const/4 v1, 0x2

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 427
    const-string v0, "Places"

    const-string v1, "Received new place estimation"

    invoke-static {v0, v1}, Lcom/google/android/location/n/aa;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 429
    :cond_0
    new-instance v1, Ljava/util/ArrayList;

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 430
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    .line 431
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/places/w;

    .line 432
    iget-object v4, v0, Lcom/google/android/location/places/w;->a:Ljava/lang/String;

    invoke-interface {v1, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 433
    iget-object v4, v0, Lcom/google/android/location/places/w;->a:Ljava/lang/String;

    iget v0, v0, Lcom/google/android/location/places/w;->b:F

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    invoke-virtual {v2, v4, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 435
    :cond_1
    new-instance v0, Lcom/google/android/location/places/aq;

    invoke-direct {v0, p0, v2}, Lcom/google/android/location/places/aq;-><init>(Lcom/google/android/location/places/ap;Ljava/util/HashMap;)V

    .line 452
    iget-object v2, p0, Lcom/google/android/location/places/ap;->a:Lcom/google/android/location/places/an;

    iget-object v2, v2, Lcom/google/android/location/places/an;->b:Lcom/google/android/location/places/f;

    sget-object v3, Lcom/google/android/gms/location/places/internal/PlacesParams;->a:Lcom/google/android/gms/location/places/internal/PlacesParams;

    invoke-virtual {v2, v1, v3, v0}, Lcom/google/android/location/places/f;->a(Ljava/util/Collection;Lcom/google/android/gms/location/places/internal/PlacesParams;Lcom/google/android/location/places/aj;)V

    .line 453
    return-void
.end method
