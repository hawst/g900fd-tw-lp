.class public final Lcom/google/android/location/reporting/ble/a;
.super Lcom/google/android/location/reporting/ble/e;
.source "SourceFile"


# static fields
.field static final a:Ljava/lang/String;

.field private static final q:Lcom/google/android/gms/blescanner/compat/ScanSettings;


# instance fields
.field private final h:Landroid/app/AlarmManager;

.field private final i:Landroid/os/PowerManager;

.field private final j:Landroid/support/v4/a/m;

.field private final k:Landroid/app/PendingIntent;

.field private final l:Ljava/util/concurrent/Executor;

.field private m:J

.field private n:Z

.field private o:Z

.field private final p:Landroid/support/v4/a/z;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 47
    const-string v0, "com.google.android.location.internal.action.ULR_BLE_SCAN_ALARM"

    invoke-static {v0}, Lcom/google/android/location/internal/PendingIntentCallbackService;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/location/reporting/ble/a;->a:Ljava/lang/String;

    .line 227
    new-instance v0, Lcom/google/android/gms/blescanner/compat/ab;

    invoke-direct {v0}, Lcom/google/android/gms/blescanner/compat/ab;-><init>()V

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/google/android/gms/blescanner/compat/ab;->a(I)Lcom/google/android/gms/blescanner/compat/ab;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/blescanner/compat/ab;->a()Lcom/google/android/gms/blescanner/compat/ScanSettings;

    move-result-object v0

    sput-object v0, Lcom/google/android/location/reporting/ble/a;->q:Lcom/google/android/gms/blescanner/compat/ScanSettings;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/android/gms/common/util/p;)V
    .locals 7

    .prologue
    .line 61
    invoke-static {p1}, Lcom/google/android/location/reporting/ble/a;->a(Landroid/content/Context;)Lcom/google/android/gms/blescanner/compat/a;

    move-result-object v3

    const-string v0, "alarm"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/app/AlarmManager;

    new-instance v5, Lcom/google/android/location/geofencer/service/ah;

    invoke-direct {v5}, Lcom/google/android/location/geofencer/service/ah;-><init>()V

    invoke-static {p1}, Landroid/support/v4/a/m;->a(Landroid/content/Context;)Landroid/support/v4/a/m;

    move-result-object v6

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    invoke-direct/range {v0 .. v6}, Lcom/google/android/location/reporting/ble/a;-><init>(Landroid/content/Context;Lcom/google/android/gms/common/util/p;Lcom/google/android/gms/blescanner/compat/a;Landroid/app/AlarmManager;Lcom/google/android/location/geofencer/service/ah;Landroid/support/v4/a/m;)V

    .line 67
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Lcom/google/android/gms/common/util/p;Lcom/google/android/gms/blescanner/compat/a;Landroid/app/AlarmManager;Lcom/google/android/location/geofencer/service/ah;Landroid/support/v4/a/m;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 77
    sget-object v0, Lcom/google/android/location/reporting/ble/a;->q:Lcom/google/android/gms/blescanner/compat/ScanSettings;

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/google/android/location/reporting/ble/e;-><init>(Landroid/content/Context;Lcom/google/android/gms/common/util/p;Lcom/google/android/gms/blescanner/compat/a;Lcom/google/android/gms/blescanner/compat/ScanSettings;)V

    .line 214
    new-instance v0, Lcom/google/android/location/reporting/ble/c;

    invoke-direct {v0, p0}, Lcom/google/android/location/reporting/ble/c;-><init>(Lcom/google/android/location/reporting/ble/a;)V

    iput-object v0, p0, Lcom/google/android/location/reporting/ble/a;->p:Landroid/support/v4/a/z;

    .line 78
    iput-boolean v4, p0, Lcom/google/android/location/reporting/ble/a;->o:Z

    .line 79
    iput-object p4, p0, Lcom/google/android/location/reporting/ble/a;->h:Landroid/app/AlarmManager;

    .line 80
    const-string v0, "power"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    iput-object v0, p0, Lcom/google/android/location/reporting/ble/a;->i:Landroid/os/PowerManager;

    .line 81
    iput-object p6, p0, Lcom/google/android/location/reporting/ble/a;->j:Landroid/support/v4/a/m;

    .line 82
    invoke-static {}, Ljava/util/concurrent/Executors;->newSingleThreadExecutor()Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/reporting/ble/a;->l:Ljava/util/concurrent/Executor;

    .line 83
    const-string v0, "com.google.android.location.internal.action.ULR_BLE_SCAN_ALARM"

    new-instance v1, Landroid/content/Intent;

    iget-object v2, p0, Lcom/google/android/location/reporting/ble/a;->b:Landroid/content/Context;

    const-class v3, Lcom/google/android/location/internal/PendingIntentCallbackService;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    iget-object v2, p0, Lcom/google/android/location/reporting/ble/a;->b:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    const/high16 v0, 0x8000000

    invoke-static {p1, v4, v1, v0}, Landroid/app/PendingIntent;->getService(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/reporting/ble/a;->k:Landroid/app/PendingIntent;

    .line 85
    return-void
.end method

.method static synthetic a(Lcom/google/android/location/reporting/ble/a;)J
    .locals 2

    .prologue
    .line 40
    iget-wide v0, p0, Lcom/google/android/location/reporting/ble/a;->m:J

    return-wide v0
.end method

.method static synthetic a(Lcom/google/android/location/reporting/ble/a;J)J
    .locals 1

    .prologue
    .line 40
    iput-wide p1, p0, Lcom/google/android/location/reporting/ble/a;->m:J

    return-wide p1
.end method

.method private a(J)V
    .locals 5

    .prologue
    .line 141
    iget-object v0, p0, Lcom/google/android/location/reporting/ble/a;->c:Lcom/google/android/gms/common/util/p;

    invoke-interface {v0}, Lcom/google/android/gms/common/util/p;->b()J

    move-result-wide v0

    add-long/2addr v0, p1

    .line 142
    iget-object v2, p0, Lcom/google/android/location/reporting/ble/a;->h:Landroid/app/AlarmManager;

    const/4 v3, 0x2

    iget-object v4, p0, Lcom/google/android/location/reporting/ble/a;->k:Landroid/app/PendingIntent;

    invoke-virtual {v2, v3, v0, v1, v4}, Landroid/app/AlarmManager;->set(IJLandroid/app/PendingIntent;)V

    .line 143
    return-void
.end method

.method static synthetic a(Lcom/google/android/location/reporting/ble/a;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 40
    iget-object v0, p0, Lcom/google/android/location/reporting/ble/a;->i:Landroid/os/PowerManager;

    const/4 v1, 0x1

    const-string v2, "BleScanReporter_WakeLock"

    invoke-virtual {v0, v1, v2}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v0

    const-string v1, "."

    invoke-virtual {p1, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "."

    invoke-virtual {p1, v1}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    invoke-virtual {p1, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object p1

    :cond_0
    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->acquire()V

    iget-object v1, p0, Lcom/google/android/location/reporting/ble/a;->l:Ljava/util/concurrent/Executor;

    new-instance v2, Lcom/google/android/location/reporting/ble/b;

    invoke-direct {v2, p0, p1, v0}, Lcom/google/android/location/reporting/ble/b;-><init>(Lcom/google/android/location/reporting/ble/a;Ljava/lang/String;Landroid/os/PowerManager$WakeLock;)V

    invoke-interface {v1, v2}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method static synthetic b(Lcom/google/android/location/reporting/ble/a;J)V
    .locals 1

    .prologue
    .line 40
    invoke-direct {p0, p1, p2}, Lcom/google/android/location/reporting/ble/a;->a(J)V

    return-void
.end method

.method static synthetic b(Lcom/google/android/location/reporting/ble/a;)Z
    .locals 1

    .prologue
    .line 40
    iget-boolean v0, p0, Lcom/google/android/location/reporting/ble/a;->o:Z

    return v0
.end method

.method static synthetic c(Lcom/google/android/location/reporting/ble/a;)V
    .locals 3

    .prologue
    .line 40
    :try_start_0
    sget-object v0, Lcom/google/android/location/reporting/service/ab;->v:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    int-to-long v0, v0

    invoke-virtual {p0, v0, v1}, Ljava/lang/Object;->wait(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v1, "GCoreUlr"

    const-string v2, "BLE: Exception in BLE wakelock thread waitPatiently()"

    invoke-static {v1, v2, v0}, Lcom/google/android/location/reporting/b/d;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method static synthetic e()J
    .locals 2

    .prologue
    .line 40
    invoke-static {}, Lcom/google/android/location/reporting/ble/a;->h()J

    move-result-wide v0

    return-wide v0
.end method

.method private static h()J
    .locals 2

    .prologue
    .line 149
    sget-object v0, Lcom/google/android/location/reporting/service/ab;->s:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    return-wide v0
.end method


# virtual methods
.method protected final declared-synchronized a()V
    .locals 3

    .prologue
    .line 102
    monitor-enter p0

    :try_start_0
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 103
    const-string v1, "com.google.android.gms.nlp.ALARM_WAKEUP_LOCATOR"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 104
    iget-object v1, p0, Lcom/google/android/location/reporting/ble/a;->b:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/location/reporting/ble/a;->p:Landroid/support/v4/a/z;

    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 106
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 107
    sget-object v1, Lcom/google/android/location/reporting/ble/a;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 108
    iget-object v1, p0, Lcom/google/android/location/reporting/ble/a;->j:Landroid/support/v4/a/m;

    iget-object v2, p0, Lcom/google/android/location/reporting/ble/a;->p:Landroid/support/v4/a/z;

    invoke-virtual {v1, v2, v0}, Landroid/support/v4/a/m;->a(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)V

    .line 110
    invoke-static {}, Lcom/google/android/location/reporting/ble/a;->h()J

    move-result-wide v0

    invoke-direct {p0, v0, v1}, Lcom/google/android/location/reporting/ble/a;->a(J)V

    .line 111
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/location/reporting/ble/a;->n:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 112
    monitor-exit p0

    return-void

    .line 102
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected final declared-synchronized b()V
    .locals 2

    .prologue
    .line 116
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/location/reporting/ble/a;->d()V

    .line 117
    iget-object v0, p0, Lcom/google/android/location/reporting/ble/a;->h:Landroid/app/AlarmManager;

    iget-object v1, p0, Lcom/google/android/location/reporting/ble/a;->k:Landroid/app/PendingIntent;

    invoke-virtual {v0, v1}, Landroid/app/AlarmManager;->cancel(Landroid/app/PendingIntent;)V

    .line 118
    iget-boolean v0, p0, Lcom/google/android/location/reporting/ble/a;->n:Z

    if-eqz v0, :cond_0

    .line 119
    iget-object v0, p0, Lcom/google/android/location/reporting/ble/a;->b:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/location/reporting/ble/a;->p:Landroid/support/v4/a/z;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 120
    iget-object v0, p0, Lcom/google/android/location/reporting/ble/a;->j:Landroid/support/v4/a/m;

    iget-object v1, p0, Lcom/google/android/location/reporting/ble/a;->p:Landroid/support/v4/a/z;

    invoke-virtual {v0, v1}, Landroid/support/v4/a/m;->a(Landroid/content/BroadcastReceiver;)V

    .line 121
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/location/reporting/ble/a;->n:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 123
    :cond_0
    monitor-exit p0

    return-void

    .line 116
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected final c()Z
    .locals 1

    .prologue
    .line 127
    invoke-super {p0}, Lcom/google/android/location/reporting/ble/e;->c()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/location/reporting/ble/a;->o:Z

    .line 128
    iget-boolean v0, p0, Lcom/google/android/location/reporting/ble/a;->o:Z

    return v0
.end method

.method protected final d()V
    .locals 1

    .prologue
    .line 133
    invoke-super {p0}, Lcom/google/android/location/reporting/ble/e;->d()V

    .line 134
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/location/reporting/ble/a;->o:Z

    .line 135
    return-void
.end method
