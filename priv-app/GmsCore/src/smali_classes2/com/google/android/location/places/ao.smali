.class final Lcom/google/android/location/places/ao;
.super Lcom/google/android/gms/location/k;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/location/places/an;


# direct methods
.method constructor <init>(Lcom/google/android/location/places/an;)V
    .locals 0

    .prologue
    .line 405
    iput-object p1, p0, Lcom/google/android/location/places/ao;->a:Lcom/google/android/location/places/an;

    invoke-direct {p0}, Lcom/google/android/gms/location/k;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/location/Location;)V
    .locals 3

    .prologue
    const/4 v2, 0x2

    .line 410
    const-string v0, "Places"

    invoke-static {v0, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 411
    const-string v0, "Places"

    const-string v1, "Received new location update"

    invoke-static {v0, v1}, Lcom/google/android/location/n/aa;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 413
    :cond_0
    iget-object v0, p0, Lcom/google/android/location/places/ao;->a:Lcom/google/android/location/places/an;

    iget-object v0, v0, Lcom/google/android/location/places/an;->a:Landroid/os/Handler;

    invoke-virtual {v0, v2, p1}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 416
    return-void
.end method
