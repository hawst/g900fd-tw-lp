.class public final Lcom/google/android/location/copresence/u;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Ljava/io/PrintWriter;

.field final synthetic b:Ljava/util/concurrent/CountDownLatch;

.field final synthetic c:Lcom/google/android/location/copresence/r;


# direct methods
.method public constructor <init>(Lcom/google/android/location/copresence/r;Ljava/io/PrintWriter;Ljava/util/concurrent/CountDownLatch;)V
    .locals 0

    .prologue
    .line 549
    iput-object p1, p0, Lcom/google/android/location/copresence/u;->c:Lcom/google/android/location/copresence/r;

    iput-object p2, p0, Lcom/google/android/location/copresence/u;->a:Ljava/io/PrintWriter;

    iput-object p3, p0, Lcom/google/android/location/copresence/u;->b:Ljava/util/concurrent/CountDownLatch;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 5

    .prologue
    .line 552
    iget-object v0, p0, Lcom/google/android/location/copresence/u;->c:Lcom/google/android/location/copresence/r;

    iget-object v0, v0, Lcom/google/android/location/copresence/r;->b:Lcom/google/android/location/copresence/j;

    iget-object v1, p0, Lcom/google/android/location/copresence/u;->a:Ljava/io/PrintWriter;

    iget-object v2, v0, Lcom/google/android/location/copresence/j;->m:Lcom/google/android/location/copresence/bg;

    const/4 v3, 0x3

    invoke-static {v3}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v3

    if-eqz v3, :cond_0

    const-string v3, "TokenAdvertiser:"

    invoke-virtual {v1, v3}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "  Started medium: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, v2, Lcom/google/android/location/copresence/bg;->b:Lcom/google/android/location/copresence/bf;

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    :cond_0
    iget-object v0, v0, Lcom/google/android/location/copresence/j;->c:Lcom/google/android/location/copresence/a;

    iget-object v2, v0, Lcom/google/android/location/copresence/a;->b:Lcom/google/android/location/copresence/bi;

    if-eqz v2, :cond_1

    iget-object v0, v0, Lcom/google/android/location/copresence/a;->b:Lcom/google/android/location/copresence/bi;

    invoke-virtual {v0, v1}, Lcom/google/android/location/copresence/bi;->a(Ljava/io/PrintWriter;)V

    .line 553
    :cond_1
    iget-object v0, p0, Lcom/google/android/location/copresence/u;->c:Lcom/google/android/location/copresence/r;

    iget-object v0, v0, Lcom/google/android/location/copresence/r;->d:Lcom/google/android/location/copresence/e/d;

    iget-object v1, p0, Lcom/google/android/location/copresence/u;->a:Ljava/io/PrintWriter;

    invoke-virtual {v0, v1}, Lcom/google/android/location/copresence/e/d;->a(Ljava/io/PrintWriter;)V

    .line 554
    iget-object v0, p0, Lcom/google/android/location/copresence/u;->b:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    .line 555
    return-void
.end method
