.class final Lcom/google/android/location/copresence/p/b;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field a:J

.field final b:Lcom/google/android/gms/location/copresence/x;

.field final c:Lcom/google/android/location/copresence/p/c;

.field final d:Ljava/util/List;

.field final e:Ljava/util/List;

.field final f:Ljava/util/List;

.field final g:Ljava/util/List;

.field final h:Ljava/util/List;

.field i:I


# direct methods
.method constructor <init>(Lcom/google/android/gms/location/copresence/x;ZLjava/util/Set;)V
    .locals 4

    .prologue
    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 42
    iput-object p1, p0, Lcom/google/android/location/copresence/p/b;->b:Lcom/google/android/gms/location/copresence/x;

    .line 43
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    invoke-static {}, Lcom/google/android/location/copresence/f/b;->b()Lcom/google/ac/b/c/g;

    move-result-object v2

    iget-object v2, v2, Lcom/google/ac/b/c/g;->k:Lcom/google/ac/b/c/n;

    iget-object v2, v2, Lcom/google/ac/b/c/n;->c:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    add-long/2addr v0, v2

    iput-wide v0, p0, Lcom/google/android/location/copresence/p/b;->a:J

    .line 45
    new-instance v0, Lcom/google/android/location/copresence/p/c;

    invoke-direct {v0, p0, p2, p3}, Lcom/google/android/location/copresence/p/c;-><init>(Lcom/google/android/location/copresence/p/b;ZLjava/util/Set;)V

    iput-object v0, p0, Lcom/google/android/location/copresence/p/b;->c:Lcom/google/android/location/copresence/p/c;

    .line 46
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/location/copresence/p/b;->d:Ljava/util/List;

    .line 47
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/location/copresence/p/b;->e:Ljava/util/List;

    .line 48
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/location/copresence/p/b;->f:Ljava/util/List;

    .line 49
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/location/copresence/p/b;->g:Ljava/util/List;

    .line 50
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/location/copresence/p/b;->h:Ljava/util/List;

    .line 51
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/location/copresence/p/b;->i:I

    .line 52
    return-void
.end method


# virtual methods
.method final declared-synchronized a(Ljava/util/List;)V
    .locals 4

    .prologue
    .line 150
    monitor-enter p0

    :goto_0
    :try_start_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    int-to-long v0, v0

    invoke-static {}, Lcom/google/android/location/copresence/f/b;->b()Lcom/google/ac/b/c/g;

    move-result-object v2

    iget-object v2, v2, Lcom/google/ac/b/c/g;->k:Lcom/google/ac/b/c/n;

    iget-object v2, v2, Lcom/google/ac/b/c/n;->b:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    .line 151
    const/4 v0, 0x0

    invoke-interface {p1, v0}, Ljava/util/List;->remove(I)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 150
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 153
    :cond_0
    monitor-exit p0

    return-void
.end method
