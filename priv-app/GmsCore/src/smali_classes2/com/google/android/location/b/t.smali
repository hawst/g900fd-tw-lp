.class final Lcom/google/android/location/b/t;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:I

.field final synthetic b:Lcom/google/android/location/b/p;


# direct methods
.method constructor <init>(Lcom/google/android/location/b/p;I)V
    .locals 0

    .prologue
    .line 733
    iput-object p1, p0, Lcom/google/android/location/b/t;->b:Lcom/google/android/location/b/p;

    iput p2, p0, Lcom/google/android/location/b/t;->a:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 12

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 736
    iget-object v4, p0, Lcom/google/android/location/b/t;->b:Lcom/google/android/location/b/p;

    iget v5, p0, Lcom/google/android/location/b/t;->a:I

    new-instance v6, Lcom/google/android/location/b/v;

    iget v0, v4, Lcom/google/android/location/b/p;->u:I

    invoke-direct {v6, v4, v0}, Lcom/google/android/location/b/v;-><init>(Lcom/google/android/location/b/p;I)V

    iget-object v0, v4, Lcom/google/android/location/b/p;->b:Ljava/util/List;

    invoke-interface {v0, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/io/File;

    iget-object v1, v4, Lcom/google/android/location/b/p;->o:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicLong;->incrementAndGet()J

    invoke-virtual {v4}, Lcom/google/android/location/b/p;->b()Z

    move-result v1

    if-eqz v1, :cond_1

    new-instance v1, Lcom/google/android/location/b/ar;

    iget-short v7, v4, Lcom/google/android/location/b/p;->s:S

    iget-object v8, v4, Lcom/google/android/location/b/p;->w:Lcom/google/android/location/e/a;

    invoke-direct {v1, v7, v0, v8}, Lcom/google/android/location/b/ar;-><init>(ILjava/io/File;Lcom/google/android/location/e/a;)V

    const/4 v0, 0x0

    :try_start_0
    invoke-virtual {v1}, Lcom/google/android/location/b/ar;->a()[B
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    :goto_0
    if-eqz v0, :cond_1

    new-instance v1, Ljava/io/DataInputStream;

    new-instance v7, Ljava/io/ByteArrayInputStream;

    invoke-direct {v7, v0}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    invoke-direct {v1, v7}, Ljava/io/DataInputStream;-><init>(Ljava/io/InputStream;)V

    :try_start_1
    invoke-virtual {v1}, Ljava/io/DataInputStream;->readInt()I

    move-result v7

    move v0, v3

    :goto_1
    if-ge v0, v7, :cond_0

    iget-object v8, v4, Lcom/google/android/location/b/p;->x:Lcom/google/android/location/b/y;

    invoke-interface {v8}, Lcom/google/android/location/b/y;->a()Ljava/lang/Object;

    move-result-object v8

    iget-object v9, v4, Lcom/google/android/location/b/p;->x:Lcom/google/android/location/b/y;

    invoke-interface {v9}, Lcom/google/android/location/b/y;->b()Ljava/lang/Object;

    move-result-object v9

    invoke-virtual {v1}, Ljava/io/DataInputStream;->readLong()J

    move-result-wide v10

    invoke-static {v9, v10, v11, v10, v11}, Lcom/google/android/location/e/ax;->a(Ljava/lang/Object;JJ)Lcom/google/android/location/e/ax;

    move-result-object v9

    invoke-virtual {v6, v8, v9}, Lcom/google/android/location/b/bd;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :catch_0
    move-exception v1

    iget-object v1, v4, Lcom/google/android/location/b/p;->p:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicLong;->incrementAndGet()J

    goto :goto_0

    :cond_0
    new-array v0, v2, [Ljava/io/InputStream;

    aput-object v1, v0, v3

    invoke-static {v0}, Lcom/google/android/location/b/p;->a([Ljava/io/InputStream;)V

    :cond_1
    :goto_2
    iget-object v0, v4, Lcom/google/android/location/b/p;->d:Ljava/util/List;

    invoke-interface {v0, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/ConcurrentHashMap;

    iget-object v1, v4, Lcom/google/android/location/b/p;->c:Ljava/util/List;

    invoke-interface {v1, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/concurrent/ConcurrentLinkedQueue;

    iget-object v7, v4, Lcom/google/android/location/b/p;->f:Ljava/util/concurrent/atomic/AtomicIntegerArray;

    invoke-virtual {v7, v5}, Ljava/util/concurrent/atomic/AtomicIntegerArray;->get(I)I

    move-result v7

    if-eqz v7, :cond_2

    :goto_3
    invoke-virtual {v4, v6, v0, v1, v2}, Lcom/google/android/location/b/p;->a(Lcom/google/android/location/b/bd;Ljava/util/concurrent/ConcurrentHashMap;Ljava/util/concurrent/ConcurrentLinkedQueue;Z)V

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentHashMap;->keySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_4
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    invoke-virtual {v0, v7}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Lcom/google/android/location/b/bd;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {v0, v7}, Ljava/util/concurrent/ConcurrentHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_4

    :catch_1
    move-exception v0

    :try_start_2
    iget-object v0, v4, Lcom/google/android/location/b/p;->p:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicLong;->incrementAndGet()J
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    new-array v0, v2, [Ljava/io/InputStream;

    aput-object v1, v0, v3

    invoke-static {v0}, Lcom/google/android/location/b/p;->a([Ljava/io/InputStream;)V

    goto :goto_2

    :catchall_0
    move-exception v0

    new-array v2, v2, [Ljava/io/InputStream;

    aput-object v1, v2, v3

    invoke-static {v2}, Lcom/google/android/location/b/p;->a([Ljava/io/InputStream;)V

    throw v0

    :cond_2
    move v2, v3

    goto :goto_3

    :cond_3
    invoke-virtual {v6}, Lcom/google/android/location/b/bd;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_4

    if-eqz v2, :cond_6

    :cond_4
    if-eqz v2, :cond_5

    iget-wide v0, v4, Lcom/google/android/location/b/p;->e:J

    const-wide/16 v8, 0x0

    invoke-virtual {v6, v0, v1, v8, v9}, Lcom/google/android/location/b/bd;->a(JJ)V

    iget-object v0, v4, Lcom/google/android/location/b/p;->f:Ljava/util/concurrent/atomic/AtomicIntegerArray;

    invoke-virtual {v0, v5, v3}, Ljava/util/concurrent/atomic/AtomicIntegerArray;->set(II)V

    :cond_5
    iget-object v0, v4, Lcom/google/android/location/b/p;->b:Ljava/util/List;

    invoke-interface {v0, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/io/File;

    invoke-virtual {v4, v0, v6}, Lcom/google/android/location/b/p;->a(Ljava/io/File;Lcom/google/android/location/b/bd;)V

    .line 737
    :cond_6
    iget-object v0, p0, Lcom/google/android/location/b/t;->b:Lcom/google/android/location/b/p;

    iput-boolean v3, v0, Lcom/google/android/location/b/p;->g:Z

    .line 740
    iget-object v0, p0, Lcom/google/android/location/b/t;->b:Lcom/google/android/location/b/p;

    invoke-virtual {v0}, Lcom/google/android/location/b/p;->a()V

    .line 741
    return-void
.end method
