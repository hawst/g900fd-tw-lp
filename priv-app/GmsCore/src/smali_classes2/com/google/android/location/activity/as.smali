.class final Lcom/google/android/location/activity/as;
.super Lcom/google/android/location/activity/av;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/location/activity/au;


# instance fields
.field private a:Z


# direct methods
.method public constructor <init>(Lcom/google/android/location/activity/k;Lcom/google/android/location/os/bi;)V
    .locals 1

    .prologue
    .line 23
    invoke-direct {p0, p1, p2}, Lcom/google/android/location/activity/av;-><init>(Lcom/google/android/location/activity/k;Lcom/google/android/location/os/bi;)V

    .line 20
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/location/activity/as;->a:Z

    .line 24
    return-void
.end method


# virtual methods
.method protected final a(Lcom/google/android/location/activity/bd;Lcom/google/android/location/activity/bd;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 60
    iget-object v0, p2, Lcom/google/android/location/activity/bd;->a:Lcom/google/android/location/e/ay;

    sget-object v1, Lcom/google/android/location/e/ay;->b:Lcom/google/android/location/e/ay;

    if-ne v0, v1, :cond_1

    iget-wide v0, p2, Lcom/google/android/location/activity/bd;->b:D

    const-wide v2, 0x3fe6666666666666L    # 0.7

    cmpl-double v0, v0, v2

    if-ltz v0, :cond_1

    .line 63
    sget-boolean v0, Lcom/google/android/location/i/a;->b:Z

    if-eqz v0, :cond_0

    const-string v0, "ActivityScheduler"

    const-string v1, "Location Changed. Leaving low power mode."

    invoke-static {v0, v1}, Lcom/google/android/location/o/a/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 64
    :cond_0
    iget-object v0, p0, Lcom/google/android/location/activity/as;->d:Lcom/google/android/location/activity/k;

    invoke-virtual {v0, v4}, Lcom/google/android/location/activity/k;->a(Z)V

    .line 65
    invoke-virtual {p0, v4}, Lcom/google/android/location/activity/as;->a(I)V

    .line 67
    :cond_1
    return-void
.end method

.method protected final a(Z)V
    .locals 7

    .prologue
    const/4 v6, 0x3

    .line 73
    new-instance v0, Lcom/google/android/gms/location/ActivityRecognitionResult;

    new-instance v1, Lcom/google/android/gms/location/DetectedActivity;

    const/16 v2, 0x64

    invoke-direct {v1, v6, v2}, Lcom/google/android/gms/location/DetectedActivity;-><init>(II)V

    iget-object v2, p0, Lcom/google/android/location/activity/as;->e:Lcom/google/android/location/os/bi;

    invoke-interface {v2}, Lcom/google/android/location/os/bi;->c()Lcom/google/android/location/j/b;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/location/j/b;->b()J

    move-result-wide v2

    iget-object v4, p0, Lcom/google/android/location/activity/as;->e:Lcom/google/android/location/os/bi;

    invoke-interface {v4}, Lcom/google/android/location/os/bi;->c()Lcom/google/android/location/j/b;

    move-result-object v4

    invoke-interface {v4}, Lcom/google/android/location/j/b;->c()J

    move-result-wide v4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/location/ActivityRecognitionResult;-><init>(Lcom/google/android/gms/location/DetectedActivity;JJ)V

    .line 76
    iget-object v1, p0, Lcom/google/android/location/activity/as;->d:Lcom/google/android/location/activity/k;

    invoke-virtual {v1, v0}, Lcom/google/android/location/activity/k;->a(Lcom/google/android/gms/location/ActivityRecognitionResult;)V

    .line 81
    iget-object v0, p0, Lcom/google/android/location/activity/as;->d:Lcom/google/android/location/activity/k;

    invoke-virtual {v0}, Lcom/google/android/location/activity/k;->c()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/location/activity/as;->e:Lcom/google/android/location/os/bi;

    invoke-interface {v0}, Lcom/google/android/location/os/bi;->y()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 82
    invoke-virtual {p0, v6}, Lcom/google/android/location/activity/as;->a(I)V

    .line 86
    :cond_0
    :goto_0
    return-void

    .line 83
    :cond_1
    iget-object v0, p0, Lcom/google/android/location/activity/as;->d:Lcom/google/android/location/activity/k;

    invoke-static {}, Lcom/google/android/location/activity/k;->b()Z

    move-result v0

    if-nez v0, :cond_0

    .line 84
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/location/activity/as;->a(I)V

    goto :goto_0
.end method

.method protected final g()V
    .locals 2

    .prologue
    .line 28
    sget-boolean v0, Lcom/google/android/location/i/a;->b:Z

    if-eqz v0, :cond_0

    const-string v0, "ActivityScheduler"

    const-string v1, "SignificantMotionDetectionState: stateEntered."

    invoke-static {v0, v1}, Lcom/google/android/location/o/a/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 29
    :cond_0
    invoke-super {p0}, Lcom/google/android/location/activity/av;->g()V

    .line 30
    iget-object v0, p0, Lcom/google/android/location/activity/as;->d:Lcom/google/android/location/activity/k;

    invoke-virtual {v0}, Lcom/google/android/location/activity/k;->k()Lcom/google/android/location/activity/at;

    move-result-object v0

    invoke-interface {v0, p0}, Lcom/google/android/location/activity/at;->a(Lcom/google/android/location/activity/au;)Z

    .line 31
    iget-object v0, p0, Lcom/google/android/location/activity/as;->e:Lcom/google/android/location/os/bi;

    invoke-interface {v0}, Lcom/google/android/location/os/bi;->v()Lcom/google/android/location/os/j;

    move-result-object v0

    sget-object v1, Lcom/google/android/location/os/au;->ak:Lcom/google/android/location/os/au;

    invoke-virtual {v0, v1}, Lcom/google/android/location/os/j;->a(Lcom/google/android/location/os/au;)V

    .line 32
    return-void
.end method

.method protected final h()V
    .locals 2

    .prologue
    .line 36
    invoke-super {p0}, Lcom/google/android/location/activity/av;->h()V

    .line 37
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/location/activity/as;->a:Z

    .line 38
    iget-object v0, p0, Lcom/google/android/location/activity/as;->d:Lcom/google/android/location/activity/k;

    invoke-virtual {v0}, Lcom/google/android/location/activity/k;->k()Lcom/google/android/location/activity/at;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/location/activity/at;->a()Z

    .line 39
    iget-object v0, p0, Lcom/google/android/location/activity/as;->e:Lcom/google/android/location/os/bi;

    invoke-interface {v0}, Lcom/google/android/location/os/bi;->v()Lcom/google/android/location/os/j;

    move-result-object v0

    sget-object v1, Lcom/google/android/location/os/au;->al:Lcom/google/android/location/os/au;

    invoke-virtual {v0, v1}, Lcom/google/android/location/os/j;->a(Lcom/google/android/location/os/au;)V

    .line 40
    return-void
.end method

.method public final l()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 44
    iget-boolean v0, p0, Lcom/google/android/location/activity/as;->a:Z

    if-eqz v0, :cond_0

    .line 52
    :goto_0
    return-void

    .line 48
    :cond_0
    sget-boolean v0, Lcom/google/android/location/i/a;->b:Z

    if-eqz v0, :cond_1

    const-string v0, "ActivityScheduler"

    const-string v1, "Significant motion detected"

    invoke-static {v0, v1}, Lcom/google/android/location/o/a/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 49
    :cond_1
    iget-object v0, p0, Lcom/google/android/location/activity/as;->e:Lcom/google/android/location/os/bi;

    invoke-interface {v0}, Lcom/google/android/location/os/bi;->v()Lcom/google/android/location/os/j;

    move-result-object v0

    sget-object v1, Lcom/google/android/location/os/au;->Y:Lcom/google/android/location/os/au;

    invoke-virtual {v0, v1}, Lcom/google/android/location/os/j;->a(Lcom/google/android/location/os/au;)V

    .line 50
    iget-object v0, p0, Lcom/google/android/location/activity/as;->d:Lcom/google/android/location/activity/k;

    invoke-virtual {v0, v2}, Lcom/google/android/location/activity/k;->a(Z)V

    .line 51
    invoke-virtual {p0, v2}, Lcom/google/android/location/activity/as;->a(I)V

    goto :goto_0
.end method

.method public final p()Ljava/lang/String;
    .locals 1

    .prologue
    .line 90
    const-string v0, "SignificantMotionDetectionState"

    return-object v0
.end method
