.class public final Lcom/google/android/location/copresence/r/c;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/location/copresence/w;


# instance fields
.field private final b:Lcom/google/android/location/copresence/r/ae;

.field private final c:Lcom/google/android/location/copresence/k/a;

.field private final d:Lcom/google/android/location/copresence/r/f;

.field private final e:Landroid/content/BroadcastReceiver;

.field private final f:Lcom/google/android/location/copresence/k/c;

.field private g:Lcom/google/android/gms/location/copresence/x;

.field private h:Z


# direct methods
.method constructor <init>(Landroid/content/Context;Lcom/google/android/location/copresence/r/ae;)V
    .locals 10

    .prologue
    const/4 v9, 0x0

    const/4 v8, 0x0

    .line 103
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 44
    new-instance v0, Lcom/google/android/location/copresence/r/d;

    invoke-direct {v0, p0}, Lcom/google/android/location/copresence/r/d;-><init>(Lcom/google/android/location/copresence/r/c;)V

    iput-object v0, p0, Lcom/google/android/location/copresence/r/c;->e:Landroid/content/BroadcastReceiver;

    .line 63
    new-instance v0, Lcom/google/android/location/copresence/r/e;

    invoke-direct {v0, p0}, Lcom/google/android/location/copresence/r/e;-><init>(Lcom/google/android/location/copresence/r/c;)V

    iput-object v0, p0, Lcom/google/android/location/copresence/r/c;->f:Lcom/google/android/location/copresence/k/c;

    .line 104
    iput-object p2, p0, Lcom/google/android/location/copresence/r/c;->b:Lcom/google/android/location/copresence/r/ae;

    .line 105
    new-instance v2, Landroid/os/Handler;

    invoke-direct {v2}, Landroid/os/Handler;-><init>()V

    .line 106
    new-instance v1, Lcom/google/android/location/copresence/k/a;

    new-instance v3, Lcom/google/android/location/copresence/k/e;

    const-string v0, "WifiApBeacon2"

    iget-object v4, p0, Lcom/google/android/location/copresence/r/c;->f:Lcom/google/android/location/copresence/k/c;

    invoke-direct {v3, p1, v0, v4}, Lcom/google/android/location/copresence/k/e;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/location/copresence/k/c;)V

    invoke-static {}, Lcom/google/android/location/copresence/f/b;->b()Lcom/google/ac/b/c/g;

    move-result-object v0

    iget-object v0, v0, Lcom/google/ac/b/c/g;->j:Lcom/google/ac/b/c/p;

    iget-object v0, v0, Lcom/google/ac/b/c/p;->b:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-static {}, Lcom/google/android/location/copresence/f/b;->b()Lcom/google/ac/b/c/g;

    move-result-object v0

    iget-object v0, v0, Lcom/google/ac/b/c/g;->j:Lcom/google/ac/b/c/p;

    iget-object v0, v0, Lcom/google/ac/b/c/p;->c:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    invoke-direct/range {v1 .. v7}, Lcom/google/android/location/copresence/k/a;-><init>(Landroid/os/Handler;Lcom/google/android/location/copresence/k/c;JJ)V

    iput-object v1, p0, Lcom/google/android/location/copresence/r/c;->c:Lcom/google/android/location/copresence/k/a;

    .line 111
    new-instance v0, Lcom/google/android/location/copresence/r/f;

    const-string v1, "copresence_wifi_ap_beacon_state"

    invoke-virtual {p1, v1, v8}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    invoke-direct {v0, p1, p2, v1}, Lcom/google/android/location/copresence/r/f;-><init>(Landroid/content/Context;Lcom/google/android/location/copresence/r/ae;Landroid/content/SharedPreferences;)V

    iput-object v0, p0, Lcom/google/android/location/copresence/r/c;->d:Lcom/google/android/location/copresence/r/f;

    .line 113
    iput-object v9, p0, Lcom/google/android/location/copresence/r/c;->g:Lcom/google/android/gms/location/copresence/x;

    .line 114
    iput-boolean v8, p0, Lcom/google/android/location/copresence/r/c;->h:Z

    .line 115
    iget-object v0, p0, Lcom/google/android/location/copresence/r/c;->c:Lcom/google/android/location/copresence/k/a;

    const/4 v1, 0x1

    new-array v1, v1, [Lcom/google/android/location/copresence/k/f;

    iget-object v3, p0, Lcom/google/android/location/copresence/r/c;->d:Lcom/google/android/location/copresence/r/f;

    iget-object v3, v3, Lcom/google/android/location/copresence/r/f;->b:Lcom/google/android/location/copresence/k/f;

    aput-object v3, v1, v8

    invoke-virtual {v0, v1}, Lcom/google/android/location/copresence/k/a;->b([Lcom/google/android/location/copresence/k/f;)V

    .line 116
    iget-object v0, p0, Lcom/google/android/location/copresence/r/c;->e:Landroid/content/BroadcastReceiver;

    new-instance v1, Landroid/content/IntentFilter;

    const-string v3, "android.net.wifi.WIFI_STATE_CHANGED"

    invoke-direct {v1, v3}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1, v0, v1, v9, v2}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;Ljava/lang/String;Landroid/os/Handler;)Landroid/content/Intent;

    .line 118
    return-void
.end method

.method static synthetic a(Lcom/google/android/location/copresence/r/c;)Lcom/google/android/location/copresence/r/f;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lcom/google/android/location/copresence/r/c;->d:Lcom/google/android/location/copresence/r/f;

    return-object v0
.end method

.method static synthetic b(Lcom/google/android/location/copresence/r/c;)Z
    .locals 1

    .prologue
    .line 35
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/location/copresence/r/c;->h:Z

    return v0
.end method

.method static synthetic c(Lcom/google/android/location/copresence/r/c;)Lcom/google/android/location/copresence/k/a;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lcom/google/android/location/copresence/r/c;->c:Lcom/google/android/location/copresence/k/a;

    return-object v0
.end method

.method private d()Z
    .locals 1

    .prologue
    .line 135
    iget-object v0, p0, Lcom/google/android/location/copresence/r/c;->g:Lcom/google/android/gms/location/copresence/x;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic d(Lcom/google/android/location/copresence/r/c;)Z
    .locals 1

    .prologue
    .line 35
    iget-boolean v0, p0, Lcom/google/android/location/copresence/r/c;->h:Z

    return v0
.end method


# virtual methods
.method public final a(Lcom/google/android/location/copresence/b;)V
    .locals 6

    .prologue
    .line 141
    invoke-static {p1}, Lcom/google/k/a/ah;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 142
    invoke-virtual {p0}, Lcom/google/android/location/copresence/r/c;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 143
    new-instance v0, Lcom/google/android/location/copresence/y;

    invoke-direct {v0}, Lcom/google/android/location/copresence/y;-><init>()V

    throw v0

    .line 145
    :cond_0
    iget-object v0, p1, Lcom/google/android/location/copresence/b;->a:Lcom/google/android/gms/location/copresence/x;

    iget-object v1, p0, Lcom/google/android/location/copresence/r/c;->g:Lcom/google/android/gms/location/copresence/x;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/location/copresence/x;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 146
    iget-object v0, p1, Lcom/google/android/location/copresence/b;->a:Lcom/google/android/gms/location/copresence/x;

    iput-object v0, p0, Lcom/google/android/location/copresence/r/c;->g:Lcom/google/android/gms/location/copresence/x;

    .line 147
    iget-object v0, p0, Lcom/google/android/location/copresence/r/c;->c:Lcom/google/android/location/copresence/k/a;

    invoke-virtual {v0}, Lcom/google/android/location/copresence/k/a;->c()V

    .line 148
    iget-object v0, p0, Lcom/google/android/location/copresence/r/c;->c:Lcom/google/android/location/copresence/k/a;

    const/4 v1, 0x2

    new-array v1, v1, [Lcom/google/android/location/copresence/k/f;

    const/4 v2, 0x0

    new-instance v3, Lcom/google/android/location/copresence/r/m;

    iget-object v4, p0, Lcom/google/android/location/copresence/r/c;->d:Lcom/google/android/location/copresence/r/f;

    invoke-virtual {v4}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    invoke-virtual {p1}, Lcom/google/android/location/copresence/b;->a()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v3, v4, v5}, Lcom/google/android/location/copresence/r/m;-><init>(Lcom/google/android/location/copresence/r/f;Ljava/lang/String;)V

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/google/android/location/copresence/r/c;->d:Lcom/google/android/location/copresence/r/f;

    iget-object v3, v3, Lcom/google/android/location/copresence/r/f;->a:Lcom/google/android/location/copresence/k/f;

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Lcom/google/android/location/copresence/k/a;->b([Lcom/google/android/location/copresence/k/f;)V

    .line 152
    :cond_1
    return-void
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 128
    invoke-static {}, Lcom/google/android/location/copresence/f/a;->c()Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/location/copresence/r/c;->b:Lcom/google/android/location/copresence/r/ae;

    invoke-virtual {v0}, Lcom/google/android/location/copresence/r/ae;->a()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/location/copresence/r/c;->b:Lcom/google/android/location/copresence/r/ae;

    invoke-virtual {v0}, Lcom/google/android/location/copresence/r/ae;->d()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-direct {p0}, Lcom/google/android/location/copresence/r/c;->d()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/location/copresence/r/c;->b:Lcom/google/android/location/copresence/r/ae;

    invoke-virtual {v0}, Lcom/google/android/location/copresence/r/ae;->c()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()V
    .locals 4

    .prologue
    .line 156
    invoke-direct {p0}, Lcom/google/android/location/copresence/r/c;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 157
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/location/copresence/r/c;->g:Lcom/google/android/gms/location/copresence/x;

    .line 158
    iget-object v0, p0, Lcom/google/android/location/copresence/r/c;->c:Lcom/google/android/location/copresence/k/a;

    invoke-virtual {v0}, Lcom/google/android/location/copresence/k/a;->c()V

    .line 159
    iget-object v0, p0, Lcom/google/android/location/copresence/r/c;->c:Lcom/google/android/location/copresence/k/a;

    const/4 v1, 0x1

    new-array v1, v1, [Lcom/google/android/location/copresence/k/f;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/android/location/copresence/r/c;->d:Lcom/google/android/location/copresence/r/f;

    iget-object v3, v3, Lcom/google/android/location/copresence/r/f;->b:Lcom/google/android/location/copresence/k/f;

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Lcom/google/android/location/copresence/k/a;->b([Lcom/google/android/location/copresence/k/f;)V

    .line 161
    :cond_0
    return-void
.end method

.method public final c()V
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 164
    iput-boolean v1, p0, Lcom/google/android/location/copresence/r/c;->h:Z

    .line 165
    iget-object v0, p0, Lcom/google/android/location/copresence/r/c;->c:Lcom/google/android/location/copresence/k/a;

    invoke-virtual {v0}, Lcom/google/android/location/copresence/k/a;->b()V

    .line 166
    iget-object v0, p0, Lcom/google/android/location/copresence/r/c;->c:Lcom/google/android/location/copresence/k/a;

    new-array v1, v1, [Lcom/google/android/location/copresence/k/f;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/android/location/copresence/r/c;->d:Lcom/google/android/location/copresence/r/f;

    iget-object v3, v3, Lcom/google/android/location/copresence/r/f;->b:Lcom/google/android/location/copresence/k/f;

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Lcom/google/android/location/copresence/k/a;->b([Lcom/google/android/location/copresence/k/f;)V

    .line 167
    return-void
.end method
