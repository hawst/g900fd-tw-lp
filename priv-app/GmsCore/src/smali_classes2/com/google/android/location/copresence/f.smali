.class final Lcom/google/android/location/copresence/f;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# static fields
.field private static a:Lcom/google/android/location/copresence/f;


# instance fields
.field private final b:Landroid/content/Context;

.field private final c:Lcom/google/android/location/copresence/ap;

.field private final d:Lcom/google/android/location/copresence/d/j;

.field private e:Z


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 44
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 45
    iput-object p1, p0, Lcom/google/android/location/copresence/f;->b:Landroid/content/Context;

    .line 46
    invoke-static {p1}, Lcom/google/android/location/copresence/ap;->a(Landroid/content/Context;)Lcom/google/android/location/copresence/ap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/copresence/f;->c:Lcom/google/android/location/copresence/ap;

    .line 47
    invoke-static {p1}, Lcom/google/android/location/copresence/d/j;->a(Landroid/content/Context;)Lcom/google/android/location/copresence/d/j;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/copresence/f;->d:Lcom/google/android/location/copresence/d/j;

    .line 48
    return-void
.end method

.method static declared-synchronized a(Landroid/content/Context;)Lcom/google/android/location/copresence/f;
    .locals 2

    .prologue
    .line 38
    const-class v1, Lcom/google/android/location/copresence/f;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/google/android/location/copresence/f;->a:Lcom/google/android/location/copresence/f;

    if-nez v0, :cond_0

    .line 39
    new-instance v0, Lcom/google/android/location/copresence/f;

    invoke-direct {v0, p0}, Lcom/google/android/location/copresence/f;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/google/android/location/copresence/f;->a:Lcom/google/android/location/copresence/f;

    .line 41
    :cond_0
    sget-object v0, Lcom/google/android/location/copresence/f;->a:Lcom/google/android/location/copresence/f;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 38
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static b(Landroid/content/Context;)Z
    .locals 2

    .prologue
    .line 135
    invoke-static {p0}, Lcom/google/android/location/copresence/an;->a(Landroid/content/Context;)Lcom/google/android/location/copresence/an;

    move-result-object v0

    .line 136
    const/4 v1, 0x3

    new-array v1, v1, [I

    fill-array-data v1, :array_0

    invoke-virtual {v0, v1}, Lcom/google/android/location/copresence/an;->a([I)Z

    move-result v0

    return v0

    nop

    :array_0
    .array-data 4
        0x2
        0x3
        0x6
    .end array-data
.end method


# virtual methods
.method public final a()V
    .locals 4

    .prologue
    .line 126
    iget-boolean v0, p0, Lcom/google/android/location/copresence/f;->e:Z

    if-nez v0, :cond_0

    .line 127
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/location/copresence/f;->e:Z

    .line 128
    iget-object v0, p0, Lcom/google/android/location/copresence/f;->c:Lcom/google/android/location/copresence/ap;

    invoke-virtual {v0}, Lcom/google/android/location/copresence/ap;->b()Lcom/google/android/location/copresence/c;

    move-result-object v0

    invoke-static {}, Lcom/google/android/location/copresence/f/b;->b()Lcom/google/ac/b/c/g;

    move-result-object v1

    iget-object v1, v1, Lcom/google/ac/b/c/g;->h:Lcom/google/ac/b/c/e;

    iget-object v1, v1, Lcom/google/ac/b/c/e;->i:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {v0, p0, v2, v3}, Lcom/google/android/location/copresence/c;->a(Ljava/lang/Runnable;J)V

    .line 132
    :cond_0
    return-void
.end method

.method public final run()V
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 118
    iput-boolean v0, p0, Lcom/google/android/location/copresence/f;->e:Z

    .line 119
    invoke-static {}, Lcom/google/android/location/copresence/f/b;->b()Lcom/google/ac/b/c/g;

    move-result-object v1

    iget-object v1, v1, Lcom/google/ac/b/c/g;->h:Lcom/google/ac/b/c/e;

    iget-object v1, v1, Lcom/google/ac/b/c/e;->h:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-nez v1, :cond_2

    .line 120
    :cond_0
    :goto_0
    if-eqz v0, :cond_1

    .line 121
    invoke-virtual {p0}, Lcom/google/android/location/copresence/f;->a()V

    .line 123
    :cond_1
    return-void

    .line 119
    :cond_2
    iget-object v1, p0, Lcom/google/android/location/copresence/f;->b:Landroid/content/Context;

    invoke-static {v1}, Lcom/google/android/location/copresence/f;->b(Landroid/content/Context;)Z

    move-result v1

    const/4 v2, 0x3

    invoke-static {v2}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v2

    if-eqz v2, :cond_3

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "BluetoothCleanup: hasBluetoothDirective: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/location/copresence/ag;->b(Ljava/lang/String;)I

    :cond_3
    if-nez v1, :cond_4

    new-instance v1, Lcom/google/android/location/copresence/q/ar;

    invoke-static {}, Lcom/google/android/location/copresence/f/b;->b()Lcom/google/ac/b/c/g;

    move-result-object v2

    iget-object v2, v2, Lcom/google/ac/b/c/g;->h:Lcom/google/ac/b/c/e;

    iget-object v2, v2, Lcom/google/ac/b/c/e;->j:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    new-instance v4, Lcom/google/android/location/copresence/g;

    invoke-direct {v4, p0}, Lcom/google/android/location/copresence/g;-><init>(Lcom/google/android/location/copresence/f;)V

    invoke-direct {v1, v2, v3, v4}, Lcom/google/android/location/copresence/q/ar;-><init>(JLcom/google/android/location/copresence/q/at;)V

    iget-object v2, p0, Lcom/google/android/location/copresence/f;->d:Lcom/google/android/location/copresence/d/j;

    invoke-virtual {v2, v1}, Lcom/google/android/location/copresence/d/j;->c(Lcom/google/android/location/copresence/q/al;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x5

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v0

    if-eqz v0, :cond_4

    const-string v0, "BluetoothCleanup: Bluetooth revert attempt began."

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->d(Ljava/lang/String;)I

    :cond_4
    const/4 v0, 0x1

    goto :goto_0
.end method
