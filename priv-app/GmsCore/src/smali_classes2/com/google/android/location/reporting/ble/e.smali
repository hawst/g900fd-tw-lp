.class public abstract Lcom/google/android/location/reporting/ble/e;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/location/reporting/ble/d;


# instance fields
.field private final a:Lcom/google/android/gms/blescanner/compat/a;

.field protected final b:Landroid/content/Context;

.field protected final c:Lcom/google/android/gms/common/util/p;

.field final d:Lcom/google/android/gms/blescanner/a/c;

.field final e:Ljava/util/ArrayList;

.field final f:Lcom/google/android/gms/blescanner/b/b;

.field g:Z

.field private final h:Lcom/google/android/gms/blescanner/compat/ScanSettings;

.field private final i:Lcom/google/android/gms/blescanner/compat/v;


# direct methods
.method protected constructor <init>(Landroid/content/Context;Lcom/google/android/gms/common/util/p;Lcom/google/android/gms/blescanner/compat/a;Lcom/google/android/gms/blescanner/compat/ScanSettings;)V
    .locals 1

    .prologue
    .line 77
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 226
    new-instance v0, Lcom/google/android/location/reporting/ble/g;

    invoke-direct {v0, p0}, Lcom/google/android/location/reporting/ble/g;-><init>(Lcom/google/android/location/reporting/ble/e;)V

    iput-object v0, p0, Lcom/google/android/location/reporting/ble/e;->i:Lcom/google/android/gms/blescanner/compat/v;

    .line 78
    iput-object p1, p0, Lcom/google/android/location/reporting/ble/e;->b:Landroid/content/Context;

    .line 79
    iput-object p2, p0, Lcom/google/android/location/reporting/ble/e;->c:Lcom/google/android/gms/common/util/p;

    .line 80
    iput-object p3, p0, Lcom/google/android/location/reporting/ble/e;->a:Lcom/google/android/gms/blescanner/compat/a;

    .line 81
    iput-object p4, p0, Lcom/google/android/location/reporting/ble/e;->h:Lcom/google/android/gms/blescanner/compat/ScanSettings;

    .line 82
    new-instance v0, Lcom/google/android/gms/blescanner/a/c;

    invoke-direct {v0}, Lcom/google/android/gms/blescanner/a/c;-><init>()V

    iput-object v0, p0, Lcom/google/android/location/reporting/ble/e;->d:Lcom/google/android/gms/blescanner/a/c;

    .line 83
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/location/reporting/ble/e;->e:Ljava/util/ArrayList;

    .line 84
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/location/reporting/ble/e;->g:Z

    .line 85
    new-instance v0, Lcom/google/android/location/reporting/ble/f;

    invoke-direct {v0, p0, p1}, Lcom/google/android/location/reporting/ble/f;-><init>(Lcom/google/android/location/reporting/ble/e;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/location/reporting/ble/e;->f:Lcom/google/android/gms/blescanner/b/b;

    .line 105
    return-void
.end method

.method protected static a(Landroid/content/Context;)Lcom/google/android/gms/blescanner/compat/a;
    .locals 2
    .annotation build Landroid/annotation/TargetApi;
        value = 0x12
    .end annotation

    .prologue
    .line 150
    sget-object v0, Lcom/google/android/location/reporting/service/ab;->q:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    .line 151
    sget-object v0, Lcom/google/android/location/reporting/service/ab;->r:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    .line 152
    invoke-static {p0, v1, v0}, Lcom/google/android/gms/blescanner/compat/j;->a(Landroid/content/Context;ZZ)Lcom/google/android/gms/blescanner/compat/a;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/location/reporting/ble/e;)V
    .locals 0

    .prologue
    .line 60
    invoke-direct {p0}, Lcom/google/android/location/reporting/ble/e;->e()V

    return-void
.end method

.method private e()V
    .locals 3

    .prologue
    .line 216
    iget-object v0, p0, Lcom/google/android/location/reporting/ble/e;->e:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 217
    iget-object v0, p0, Lcom/google/android/location/reporting/ble/e;->b:Landroid/content/Context;

    new-instance v1, Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/google/android/location/reporting/ble/e;->e:Ljava/util/ArrayList;

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-static {v0, v1}, Lcom/google/android/location/reporting/service/DispatchingService;->a(Landroid/content/Context;Ljava/util/ArrayList;)V

    .line 219
    const-string v0, "GCoreUlr"

    const/4 v1, 0x4

    invoke-static {v0, v1}, Lcom/google/android/location/reporting/b/d;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 220
    const-string v0, "GCoreUlr"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "BLE: collected "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/location/reporting/ble/e;->e:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " results"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/location/reporting/b/d;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 222
    :cond_0
    iget-object v0, p0, Lcom/google/android/location/reporting/ble/e;->e:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 224
    :cond_1
    return-void
.end method


# virtual methods
.method protected abstract a()V
.end method

.method protected abstract b()V
.end method

.method protected c()Z
    .locals 8

    .prologue
    const/16 v7, 0x4c

    const/4 v3, 0x1

    .line 160
    iget-object v0, p0, Lcom/google/android/location/reporting/ble/e;->a:Lcom/google/android/gms/blescanner/compat/a;

    if-eqz v0, :cond_7

    .line 162
    sget-object v0, Lcom/google/android/location/reporting/service/ab;->u:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 163
    sget-object v0, Lcom/google/android/location/reporting/service/ab;->w:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 164
    sub-int v2, v0, v1

    .line 165
    sget-object v0, Lcom/google/android/location/reporting/service/ab;->x:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    .line 168
    iget-object v0, p0, Lcom/google/android/location/reporting/ble/e;->a:Lcom/google/android/gms/blescanner/compat/a;

    invoke-virtual {v0, v1, v2, v4, v5}, Lcom/google/android/gms/blescanner/compat/a;->a(IIJ)V

    .line 170
    iget-object v1, p0, Lcom/google/android/location/reporting/ble/e;->a:Lcom/google/android/gms/blescanner/compat/a;

    new-instance v2, Lcom/google/android/gms/blescanner/a/d;

    invoke-direct {v2}, Lcom/google/android/gms/blescanner/a/d;-><init>()V

    sget-object v0, Lcom/google/android/location/reporting/service/ab;->y:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_2

    sget-object v0, Lcom/google/android/location/reporting/service/ab;->z:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    iput-boolean v3, v2, Lcom/google/android/gms/blescanner/a/d;->c:Z

    :cond_0
    sget-object v0, Lcom/google/android/location/reporting/service/ab;->A:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1

    iput-boolean v3, v2, Lcom/google/android/gms/blescanner/a/d;->d:Z

    :cond_1
    sget-object v0, Lcom/google/android/location/reporting/service/ab;->B:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_2

    iput-boolean v3, v2, Lcom/google/android/gms/blescanner/a/d;->b:Z

    :cond_2
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    iget-boolean v0, v2, Lcom/google/android/gms/blescanner/a/d;->c:Z

    if-eqz v0, :cond_3

    invoke-static {}, Lcom/google/android/gms/blescanner/a/e;->b()[B

    move-result-object v0

    invoke-static {}, Lcom/google/android/gms/blescanner/a/e;->c()[B

    move-result-object v4

    new-instance v5, Lcom/google/android/gms/blescanner/compat/x;

    invoke-direct {v5}, Lcom/google/android/gms/blescanner/compat/x;-><init>()V

    const/16 v6, 0xe0

    invoke-virtual {v5, v6, v0, v4}, Lcom/google/android/gms/blescanner/compat/x;->a(I[B[B)Lcom/google/android/gms/blescanner/compat/x;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/blescanner/compat/x;->a()Lcom/google/android/gms/blescanner/compat/ScanFilter;

    move-result-object v0

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-static {}, Lcom/google/android/gms/blescanner/a/e;->d()[B

    move-result-object v0

    invoke-static {}, Lcom/google/android/gms/blescanner/a/e;->c()[B

    move-result-object v4

    new-instance v5, Lcom/google/android/gms/blescanner/compat/x;

    invoke-direct {v5}, Lcom/google/android/gms/blescanner/compat/x;-><init>()V

    const/16 v6, 0x550

    invoke-virtual {v5, v6, v0, v4}, Lcom/google/android/gms/blescanner/compat/x;->a(I[B[B)Lcom/google/android/gms/blescanner/compat/x;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/blescanner/compat/x;->a()Lcom/google/android/gms/blescanner/compat/ScanFilter;

    move-result-object v0

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_3
    iget-boolean v0, v2, Lcom/google/android/gms/blescanner/a/d;->d:Z

    if-eqz v0, :cond_4

    invoke-static {}, Lcom/google/android/gms/blescanner/a/f;->b()[B

    move-result-object v0

    invoke-static {}, Lcom/google/android/gms/blescanner/a/f;->c()[B

    move-result-object v4

    new-instance v5, Lcom/google/android/gms/blescanner/compat/x;

    invoke-direct {v5}, Lcom/google/android/gms/blescanner/compat/x;-><init>()V

    const-string v6, "0000FEF4-0000-1000-8000-00805F9B34FB"

    invoke-static {v6}, Landroid/os/ParcelUuid;->fromString(Ljava/lang/String;)Landroid/os/ParcelUuid;

    move-result-object v6

    invoke-virtual {v5, v6, v0, v4}, Lcom/google/android/gms/blescanner/compat/x;->a(Landroid/os/ParcelUuid;[B[B)Lcom/google/android/gms/blescanner/compat/x;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/blescanner/compat/x;->a()Lcom/google/android/gms/blescanner/compat/ScanFilter;

    move-result-object v0

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_4
    iget-boolean v0, v2, Lcom/google/android/gms/blescanner/a/d;->b:Z

    if-eqz v0, :cond_6

    invoke-static {}, Lcom/google/android/gms/blescanner/a/a;->b()[B

    move-result-object v0

    invoke-static {}, Lcom/google/android/gms/blescanner/a/a;->c()[B

    move-result-object v2

    new-instance v4, Lcom/google/android/gms/blescanner/compat/x;

    invoke-direct {v4}, Lcom/google/android/gms/blescanner/compat/x;-><init>()V

    invoke-virtual {v4, v7, v0, v2}, Lcom/google/android/gms/blescanner/compat/x;->a(I[B[B)Lcom/google/android/gms/blescanner/compat/x;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/blescanner/compat/x;->a()Lcom/google/android/gms/blescanner/compat/ScanFilter;

    move-result-object v0

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_5
    iget-object v0, p0, Lcom/google/android/location/reporting/ble/e;->h:Lcom/google/android/gms/blescanner/compat/ScanSettings;

    iget-object v2, p0, Lcom/google/android/location/reporting/ble/e;->i:Lcom/google/android/gms/blescanner/compat/v;

    invoke-virtual {v1, v3, v0, v2}, Lcom/google/android/gms/blescanner/compat/a;->a(Ljava/util/List;Lcom/google/android/gms/blescanner/compat/ScanSettings;Lcom/google/android/gms/blescanner/compat/v;)Z

    move-result v0

    .line 172
    :goto_0
    return v0

    .line 170
    :cond_6
    iget-object v0, v2, Lcom/google/android/gms/blescanner/a/d;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/ParcelUuid;

    invoke-static {v0}, Lcom/google/android/gms/blescanner/a/a;->a(Landroid/os/ParcelUuid;)[B

    move-result-object v4

    invoke-static {v0}, Lcom/google/android/gms/blescanner/a/a;->b(Landroid/os/ParcelUuid;)[B

    move-result-object v0

    new-instance v5, Lcom/google/android/gms/blescanner/compat/x;

    invoke-direct {v5}, Lcom/google/android/gms/blescanner/compat/x;-><init>()V

    invoke-virtual {v5, v7, v4, v0}, Lcom/google/android/gms/blescanner/compat/x;->a(I[B[B)Lcom/google/android/gms/blescanner/compat/x;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/blescanner/compat/x;->a()Lcom/google/android/gms/blescanner/compat/ScanFilter;

    move-result-object v0

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 172
    :cond_7
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected d()V
    .locals 2

    .prologue
    .line 179
    iget-object v0, p0, Lcom/google/android/location/reporting/ble/e;->a:Lcom/google/android/gms/blescanner/compat/a;

    if-eqz v0, :cond_0

    .line 180
    iget-object v0, p0, Lcom/google/android/location/reporting/ble/e;->a:Lcom/google/android/gms/blescanner/compat/a;

    iget-object v1, p0, Lcom/google/android/location/reporting/ble/e;->i:Lcom/google/android/gms/blescanner/compat/v;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/blescanner/compat/a;->a(Lcom/google/android/gms/blescanner/compat/v;)V

    .line 181
    invoke-direct {p0}, Lcom/google/android/location/reporting/ble/e;->e()V

    .line 183
    :cond_0
    return-void
.end method

.method public final f()V
    .locals 1

    .prologue
    .line 114
    iget-boolean v0, p0, Lcom/google/android/location/reporting/ble/e;->g:Z

    if-nez v0, :cond_0

    .line 115
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/location/reporting/ble/e;->g:Z

    .line 116
    iget-object v0, p0, Lcom/google/android/location/reporting/ble/e;->f:Lcom/google/android/gms/blescanner/b/b;

    invoke-virtual {v0}, Lcom/google/android/gms/blescanner/b/b;->c()V

    .line 117
    invoke-virtual {p0}, Lcom/google/android/location/reporting/ble/e;->a()V

    .line 119
    :cond_0
    return-void
.end method

.method public final g()V
    .locals 1

    .prologue
    .line 125
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/location/reporting/ble/e;->g:Z

    .line 126
    iget-object v0, p0, Lcom/google/android/location/reporting/ble/e;->f:Lcom/google/android/gms/blescanner/b/b;

    invoke-virtual {v0}, Lcom/google/android/gms/blescanner/b/b;->d()V

    .line 127
    invoke-virtual {p0}, Lcom/google/android/location/reporting/ble/e;->b()V

    .line 128
    return-void
.end method
