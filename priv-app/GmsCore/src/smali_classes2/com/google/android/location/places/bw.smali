.class final Lcom/google/android/location/places/bw;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/location/places/x;


# instance fields
.field final a:Lcom/google/android/location/e/bi;

.field final synthetic b:Lcom/google/android/location/places/bv;


# direct methods
.method constructor <init>(Lcom/google/android/location/places/bv;Lcom/google/android/location/e/bi;)V
    .locals 0

    .prologue
    .line 112
    iput-object p1, p0, Lcom/google/android/location/places/bw;->b:Lcom/google/android/location/places/bv;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 113
    iput-object p2, p0, Lcom/google/android/location/places/bw;->a:Lcom/google/android/location/e/bi;

    .line 114
    return-void
.end method


# virtual methods
.method public final a(Ljava/util/List;)V
    .locals 6

    .prologue
    .line 118
    invoke-static {}, Lcom/google/android/location/places/p;->a()Lcom/google/android/location/o/a/c;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Results from server: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " places"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/location/o/a/c;->a(Ljava/lang/String;)V

    .line 119
    iget-object v0, p0, Lcom/google/android/location/places/bw;->b:Lcom/google/android/location/places/bv;

    invoke-static {v0}, Lcom/google/android/location/places/bv;->a(Lcom/google/android/location/places/bv;)Lcom/google/android/location/places/f/c;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/location/places/bw;->a:Lcom/google/android/location/e/bi;

    if-eqz v0, :cond_0

    .line 120
    iget-object v0, p0, Lcom/google/android/location/places/bw;->b:Lcom/google/android/location/places/bv;

    invoke-static {v0}, Lcom/google/android/location/places/bv;->a(Lcom/google/android/location/places/bv;)Lcom/google/android/location/places/f/c;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/location/places/bw;->a:Lcom/google/android/location/e/bi;

    invoke-interface {v0, v1, p1}, Lcom/google/android/location/places/f/c;->a(Lcom/google/android/location/e/bi;Ljava/util/List;)V

    .line 122
    :cond_0
    iget-object v0, p0, Lcom/google/android/location/places/bw;->b:Lcom/google/android/location/places/bv;

    invoke-static {v0}, Lcom/google/android/location/places/bv;->c(Lcom/google/android/location/places/bv;)Lcom/google/android/location/places/ac;

    move-result-object v0

    new-instance v1, Lcom/google/android/location/places/ae;

    iget-object v2, p0, Lcom/google/android/location/places/bw;->b:Lcom/google/android/location/places/bv;

    invoke-virtual {v2}, Lcom/google/android/location/places/bv;->a()I

    move-result v2

    iget-object v3, p0, Lcom/google/android/location/places/bw;->b:Lcom/google/android/location/places/bv;

    invoke-static {v3}, Lcom/google/android/location/places/bv;->b(Lcom/google/android/location/places/bv;)Lcom/google/android/location/j/b;

    move-result-object v3

    invoke-interface {v3}, Lcom/google/android/location/j/b;->c()J

    move-result-wide v4

    invoke-direct {v1, p1, v2, v4, v5}, Lcom/google/android/location/places/ae;-><init>(Ljava/util/List;IJ)V

    invoke-interface {v0, v1}, Lcom/google/android/location/places/ac;->a(Lcom/google/android/location/places/ae;)V

    .line 124
    return-void
.end method
