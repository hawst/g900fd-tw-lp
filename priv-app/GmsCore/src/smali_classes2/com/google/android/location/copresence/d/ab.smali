.class final Lcom/google/android/location/copresence/d/ab;
.super Lcom/google/android/location/copresence/k/f;
.source "SourceFile"


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x12
.end annotation


# instance fields
.field final synthetic a:Lcom/google/android/location/copresence/d/n;

.field private final b:Lcom/google/android/gms/location/copresence/x;

.field private volatile c:Z


# direct methods
.method public constructor <init>(Lcom/google/android/location/copresence/d/n;Lcom/google/android/gms/location/copresence/x;)V
    .locals 5
    .annotation build Landroid/annotation/TargetApi;
        value = 0x12
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 398
    iput-object p1, p0, Lcom/google/android/location/copresence/d/ab;->a:Lcom/google/android/location/copresence/d/n;

    .line 399
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "BleAdvertiseUuidAndTokenState token="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p2}, Lcom/google/android/gms/location/copresence/x;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x2

    new-array v1, v1, [Lcom/google/android/location/copresence/k/f;

    iget-object v2, p1, Lcom/google/android/location/copresence/d/n;->g:Lcom/google/android/location/copresence/k/f;

    aput-object v2, v1, v4

    const/4 v2, 0x1

    iget-object v3, p1, Lcom/google/android/location/copresence/d/n;->f:Lcom/google/android/location/copresence/k/f;

    aput-object v3, v1, v2

    invoke-direct {p0, v0, v1}, Lcom/google/android/location/copresence/k/f;-><init>(Ljava/lang/String;[Lcom/google/android/location/copresence/k/f;)V

    .line 395
    iput-boolean v4, p0, Lcom/google/android/location/copresence/d/ab;->c:Z

    .line 401
    iput-object p2, p0, Lcom/google/android/location/copresence/d/ab;->b:Lcom/google/android/gms/location/copresence/x;

    .line 402
    return-void
.end method


# virtual methods
.method public final a()Z
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 411
    iget-object v1, p0, Lcom/google/android/location/copresence/d/ab;->a:Lcom/google/android/location/copresence/d/n;

    iget-object v1, v1, Lcom/google/android/location/copresence/d/n;->g:Lcom/google/android/location/copresence/k/f;

    invoke-virtual {v1}, Lcom/google/android/location/copresence/k/f;->b()Z

    move-result v1

    if-nez v1, :cond_1

    .line 412
    const/4 v1, 0x6

    invoke-static {v1}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 413
    const-string v1, "BluetoothStates: Cannot set name if user settings not saved"

    invoke-static {v1}, Lcom/google/android/location/copresence/ag;->e(Ljava/lang/String;)I

    .line 419
    :cond_0
    :goto_0
    return v0

    .line 417
    :cond_1
    iget-object v1, p0, Lcom/google/android/location/copresence/d/ab;->a:Lcom/google/android/location/copresence/d/n;

    iget-object v1, v1, Lcom/google/android/location/copresence/d/n;->b:Lcom/google/android/location/copresence/d/e;

    sget-object v2, Lcom/google/android/location/copresence/d/j;->a:Ljava/util/UUID;

    iget-object v3, p0, Lcom/google/android/location/copresence/d/ab;->b:Lcom/google/android/gms/location/copresence/x;

    iget-object v3, v3, Lcom/google/android/gms/location/copresence/x;->a:[B

    iget-object v1, v1, Lcom/google/android/location/copresence/d/e;->b:Lcom/google/android/location/copresence/d/a/l;

    iget-object v4, v1, Lcom/google/android/location/copresence/d/a/l;->a:Lcom/google/android/location/copresence/d/a/m;

    if-nez v4, :cond_2

    :goto_1
    iput-boolean v0, p0, Lcom/google/android/location/copresence/d/ab;->c:Z

    .line 419
    iget-boolean v0, p0, Lcom/google/android/location/copresence/d/ab;->c:Z

    goto :goto_0

    .line 417
    :cond_2
    iget-object v1, v1, Lcom/google/android/location/copresence/d/a/l;->a:Lcom/google/android/location/copresence/d/a/m;

    invoke-interface {v1, v0, v2, v3}, Lcom/google/android/location/copresence/d/a/m;->a(ILjava/util/UUID;[B)Z

    move-result v0

    goto :goto_1
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 406
    iget-boolean v0, p0, Lcom/google/android/location/copresence/d/ab;->c:Z

    return v0
.end method
