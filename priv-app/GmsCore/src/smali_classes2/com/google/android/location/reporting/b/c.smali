.class public final Lcom/google/android/location/reporting/b/c;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final a:Landroid/content/IntentFilter;


# instance fields
.field private final b:Landroid/content/Context;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 31
    new-instance v0, Landroid/content/IntentFilter;

    const-string v1, "android.intent.action.BATTERY_CHANGED"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/location/reporting/b/c;->a:Landroid/content/IntentFilter;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    iput-object p1, p0, Lcom/google/android/location/reporting/b/c;->b:Landroid/content/Context;

    .line 39
    return-void
.end method

.method private static a(Landroid/content/Context;)Lcom/google/android/location/reporting/a/b;
    .locals 4

    .prologue
    const/4 v0, 0x0

    const/4 v3, 0x0

    .line 57
    .line 60
    const/4 v1, 0x0

    :try_start_0
    sget-object v2, Lcom/google/android/location/reporting/b/c;->a:Landroid/content/IntentFilter;

    invoke-virtual {p0, v1, v2}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;
    :try_end_0
    .catch Landroid/content/ReceiverCallNotAllowedException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 66
    :goto_0
    new-instance v1, Lcom/google/android/location/reporting/a/b;

    invoke-direct {v1}, Lcom/google/android/location/reporting/a/b;-><init>()V

    .line 67
    if-eqz v0, :cond_0

    .line 68
    const-string v2, "plugged"

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/google/android/location/reporting/a/b;->a(I)Lcom/google/android/location/reporting/a/b;

    .line 69
    const-string v2, "level"

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/google/android/location/reporting/a/b;->b(I)Lcom/google/android/location/reporting/a/b;

    .line 70
    const-string v2, "scale"

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/google/android/location/reporting/a/b;->c(I)Lcom/google/android/location/reporting/a/b;

    .line 71
    const-string v2, "voltage"

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    invoke-virtual {v1, v0}, Lcom/google/android/location/reporting/a/b;->d(I)Lcom/google/android/location/reporting/a/b;

    .line 73
    :cond_0
    return-object v1

    :catch_0
    move-exception v1

    goto :goto_0
.end method


# virtual methods
.method public final a()Z
    .locals 2

    .prologue
    .line 47
    iget-object v0, p0, Lcom/google/android/location/reporting/b/c;->b:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/location/reporting/b/c;->a(Landroid/content/Context;)Lcom/google/android/location/reporting/a/b;

    move-result-object v0

    .line 48
    iget-boolean v1, v0, Lcom/google/android/location/reporting/a/b;->a:Z

    if-eqz v1, :cond_0

    iget v0, v0, Lcom/google/android/location/reporting/a/b;->b:I

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()Lcom/google/android/location/reporting/a/b;
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lcom/google/android/location/reporting/b/c;->b:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/location/reporting/b/c;->a(Landroid/content/Context;)Lcom/google/android/location/reporting/a/b;

    move-result-object v0

    return-object v0
.end method
