.class public final Lcom/google/android/location/e/ah;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field static final a:Lcom/google/p/a/b/b/a;

.field static final b:Lcom/google/p/a/b/b/a;


# instance fields
.field final c:Lcom/google/android/location/j/b;

.field final d:Lcom/google/android/location/j/e;

.field volatile e:Lcom/google/p/a/b/b/a;

.field volatile f:J

.field private final g:Lcom/google/android/location/o/e;

.field private final h:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 48
    new-instance v0, Lcom/google/p/a/b/b/a;

    sget-object v1, Lcom/google/android/location/m/a;->aE:Lcom/google/p/a/b/b/c;

    invoke-direct {v0, v1}, Lcom/google/p/a/b/b/a;-><init>(Lcom/google/p/a/b/b/c;)V

    sput-object v0, Lcom/google/android/location/e/ah;->a:Lcom/google/p/a/b/b/a;

    .line 59
    new-instance v0, Lcom/google/p/a/b/b/a;

    sget-object v1, Lcom/google/android/location/m/a;->aF:Lcom/google/p/a/b/b/c;

    invoke-direct {v0, v1}, Lcom/google/p/a/b/b/a;-><init>(Lcom/google/p/a/b/b/c;)V

    sput-object v0, Lcom/google/android/location/e/ah;->b:Lcom/google/p/a/b/b/a;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/location/j/b;Lcom/google/android/location/j/e;Lcom/google/android/location/o/e;)V
    .locals 2

    .prologue
    .line 84
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 68
    sget-object v0, Lcom/google/android/location/e/ah;->a:Lcom/google/p/a/b/b/a;

    iput-object v0, p0, Lcom/google/android/location/e/ah;->e:Lcom/google/p/a/b/b/a;

    .line 74
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/google/android/location/e/ah;->f:J

    .line 81
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/location/e/ah;->h:Ljava/lang/Object;

    .line 85
    iput-object p1, p0, Lcom/google/android/location/e/ah;->c:Lcom/google/android/location/j/b;

    .line 86
    iput-object p2, p0, Lcom/google/android/location/e/ah;->d:Lcom/google/android/location/j/e;

    .line 87
    iput-object p3, p0, Lcom/google/android/location/e/ah;->g:Lcom/google/android/location/o/e;

    .line 88
    return-void
.end method

.method private static a(Lcom/google/android/location/j/e;)Ljava/io/File;
    .locals 3

    .prologue
    .line 155
    new-instance v0, Ljava/io/File;

    invoke-interface {p0}, Lcom/google/android/location/j/e;->c()Ljava/io/File;

    move-result-object v1

    const-string v2, "nlp_params"

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 156
    return-object v0
.end method

.method private static a(Ljava/io/Closeable;)V
    .locals 1

    .prologue
    .line 418
    if-eqz p0, :cond_0

    .line 420
    :try_start_0
    invoke-interface {p0}, Ljava/io/Closeable;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 425
    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method private a(I)Z
    .locals 2

    .prologue
    .line 325
    invoke-direct {p0}, Lcom/google/android/location/e/ah;->x()Lcom/google/p/a/b/b/a;

    move-result-object v0

    .line 326
    invoke-virtual {v0, p1}, Lcom/google/p/a/b/b/a;->i(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 327
    invoke-virtual {v0, p1}, Lcom/google/p/a/b/b/a;->b(I)Z

    move-result v0

    .line 329
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(Lcom/google/p/a/b/b/a;J)Z
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 164
    sget-object v1, Lcom/google/android/location/e/ah;->a:Lcom/google/p/a/b/b/a;

    if-ne p1, v1, :cond_1

    .line 168
    :cond_0
    :goto_0
    return v0

    .line 167
    :cond_1
    const/4 v1, 0x3

    invoke-virtual {p1, v1}, Lcom/google/p/a/b/b/a;->d(I)J

    move-result-wide v2

    const-wide/16 v4, 0x3e8

    mul-long/2addr v2, v4

    .line 168
    add-long/2addr v2, p2

    iget-object v1, p0, Lcom/google/android/location/e/ah;->c:Lcom/google/android/location/j/b;

    invoke-interface {v1}, Lcom/google/android/location/j/b;->c()J

    move-result-wide v4

    cmp-long v1, v2, v4

    if-gez v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method private x()Lcom/google/p/a/b/b/a;
    .locals 4

    .prologue
    .line 405
    iget-object v1, p0, Lcom/google/android/location/e/ah;->h:Ljava/lang/Object;

    monitor-enter v1

    .line 406
    :try_start_0
    iget-object v0, p0, Lcom/google/android/location/e/ah;->e:Lcom/google/p/a/b/b/a;

    sget-object v2, Lcom/google/android/location/e/ah;->a:Lcom/google/p/a/b/b/a;

    if-eq v0, v2, :cond_0

    iget-object v0, p0, Lcom/google/android/location/e/ah;->e:Lcom/google/p/a/b/b/a;

    iget-wide v2, p0, Lcom/google/android/location/e/ah;->f:J

    invoke-direct {p0, v0, v2, v3}, Lcom/google/android/location/e/ah;->a(Lcom/google/p/a/b/b/a;J)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 408
    invoke-direct {p0}, Lcom/google/android/location/e/ah;->y()V

    .line 410
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 411
    iget-object v0, p0, Lcom/google/android/location/e/ah;->e:Lcom/google/p/a/b/b/a;

    return-object v0

    .line 410
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private declared-synchronized y()V
    .locals 6

    .prologue
    .line 450
    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lcom/google/android/location/e/ah;->h:Ljava/lang/Object;

    monitor-enter v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 451
    :try_start_1
    iget-object v0, p0, Lcom/google/android/location/e/ah;->e:Lcom/google/p/a/b/b/a;

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Lcom/google/p/a/b/b/a;->c(I)I

    move-result v0

    .line 452
    sget-object v2, Lcom/google/android/location/e/ah;->a:Lcom/google/p/a/b/b/a;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lcom/google/p/a/b/b/a;->c(I)I

    move-result v2

    .line 453
    sget-object v3, Lcom/google/android/location/e/ah;->a:Lcom/google/p/a/b/b/a;

    iput-object v3, p0, Lcom/google/android/location/e/ah;->e:Lcom/google/p/a/b/b/a;

    .line 454
    const-wide/16 v4, -0x1

    iput-wide v4, p0, Lcom/google/android/location/e/ah;->f:J

    .line 455
    if-eq v0, v2, :cond_0

    iget-object v0, p0, Lcom/google/android/location/e/ah;->g:Lcom/google/android/location/o/e;

    if-eqz v0, :cond_0

    .line 456
    iget-object v0, p0, Lcom/google/android/location/e/ah;->g:Lcom/google/android/location/o/e;

    invoke-interface {v0, p0}, Lcom/google/android/location/o/e;->a(Ljava/lang/Object;)V

    .line 458
    :cond_0
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit v1

    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 450
    :catchall_1
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public final a()V
    .locals 9

    .prologue
    const/4 v8, 0x1

    .line 91
    iget-object v0, p0, Lcom/google/android/location/e/ah;->d:Lcom/google/android/location/j/e;

    invoke-static {v0}, Lcom/google/android/location/e/ah;->a(Lcom/google/android/location/j/e;)Ljava/io/File;

    move-result-object v0

    .line 92
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 94
    :try_start_0
    new-instance v4, Ljava/io/DataInputStream;

    new-instance v1, Ljava/io/FileInputStream;

    invoke-direct {v1, v0}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    invoke-direct {v4, v1}, Ljava/io/DataInputStream;-><init>(Ljava/io/InputStream;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 95
    :try_start_1
    invoke-virtual {v4}, Ljava/io/DataInputStream;->readUnsignedShort()I

    move-result v0

    const/4 v1, 0x2

    if-eq v0, v1, :cond_2

    invoke-direct {p0}, Lcom/google/android/location/e/ah;->y()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    :try_start_2
    invoke-static {v4}, Lcom/google/android/location/e/ah;->a(Ljava/io/Closeable;)V
    :try_end_2
    .catch Ljava/io/FileNotFoundException; {:try_start_2 .. :try_end_2} :catch_0

    .line 100
    :cond_0
    :goto_0
    sget-boolean v0, Lcom/google/android/location/i/a;->b:Z

    if-eqz v0, :cond_1

    const-string v0, "NlpParametersState"

    const-string v1, "Using NlpParameterId: %d."

    new-array v2, v8, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/android/location/e/ah;->e:Lcom/google/p/a/b/b/a;

    invoke-virtual {v4, v8}, Lcom/google/p/a/b/b/a;->c(I)I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/location/o/a/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 101
    :cond_1
    return-void

    .line 95
    :cond_2
    :try_start_3
    invoke-virtual {v4}, Ljava/io/DataInputStream;->readLong()J

    move-result-wide v2

    iget-object v0, p0, Lcom/google/android/location/e/ah;->c:Lcom/google/android/location/j/b;

    invoke-interface {v0}, Lcom/google/android/location/j/b;->b()J

    move-result-wide v0

    iget-object v5, p0, Lcom/google/android/location/e/ah;->c:Lcom/google/android/location/j/b;

    invoke-interface {v5}, Lcom/google/android/location/j/b;->c()J

    move-result-wide v6

    sub-long v6, v0, v6

    cmp-long v5, v2, v0

    if-lez v5, :cond_4

    :goto_1
    sub-long/2addr v0, v6

    sget-object v2, Lcom/google/android/location/m/a;->aE:Lcom/google/p/a/b/b/c;

    invoke-static {v4, v2}, Lcom/google/android/location/o/j;->a(Ljava/io/InputStream;Lcom/google/p/a/b/b/c;)Lcom/google/p/a/b/b/a;

    move-result-object v2

    invoke-direct {p0, v2, v0, v1}, Lcom/google/android/location/e/ah;->a(Lcom/google/p/a/b/b/a;J)Z

    move-result v3

    if-nez v3, :cond_5

    iget-object v3, p0, Lcom/google/android/location/e/ah;->h:Ljava/lang/Object;

    monitor-enter v3
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    :try_start_4
    iput-object v2, p0, Lcom/google/android/location/e/ah;->e:Lcom/google/p/a/b/b/a;

    iput-wide v0, p0, Lcom/google/android/location/e/ah;->f:J

    iget-object v0, p0, Lcom/google/android/location/e/ah;->e:Lcom/google/p/a/b/b/a;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/p/a/b/b/a;->c(I)I

    move-result v0

    sget-object v1, Lcom/google/android/location/e/ah;->a:Lcom/google/p/a/b/b/a;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/google/p/a/b/b/a;->c(I)I

    move-result v1

    if-eq v0, v1, :cond_3

    iget-object v0, p0, Lcom/google/android/location/e/ah;->g:Lcom/google/android/location/o/e;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/location/e/ah;->g:Lcom/google/android/location/o/e;

    invoke-interface {v0, p0}, Lcom/google/android/location/o/e;->a(Ljava/lang/Object;)V

    :cond_3
    monitor-exit v3
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    :goto_2
    :try_start_5
    invoke-static {v4}, Lcom/google/android/location/e/ah;->a(Ljava/io/Closeable;)V
    :try_end_5
    .catch Ljava/io/FileNotFoundException; {:try_start_5 .. :try_end_5} :catch_0

    goto :goto_0

    .line 97
    :catch_0
    move-exception v0

    invoke-direct {p0}, Lcom/google/android/location/e/ah;->y()V

    goto :goto_0

    :cond_4
    move-wide v0, v2

    .line 95
    goto :goto_1

    :catchall_0
    move-exception v0

    :try_start_6
    monitor-exit v3

    throw v0
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_1
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    :catch_1
    move-exception v0

    :try_start_7
    invoke-direct {p0}, Lcom/google/android/location/e/ah;->y()V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    :try_start_8
    invoke-static {v4}, Lcom/google/android/location/e/ah;->a(Ljava/io/Closeable;)V
    :try_end_8
    .catch Ljava/io/FileNotFoundException; {:try_start_8 .. :try_end_8} :catch_0

    goto :goto_0

    :cond_5
    :try_start_9
    invoke-direct {p0}, Lcom/google/android/location/e/ah;->y()V
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_1
    .catchall {:try_start_9 .. :try_end_9} :catchall_1

    goto :goto_2

    :catchall_1
    move-exception v0

    :try_start_a
    invoke-static {v4}, Lcom/google/android/location/e/ah;->a(Ljava/io/Closeable;)V

    throw v0
    :try_end_a
    .catch Ljava/io/FileNotFoundException; {:try_start_a .. :try_end_a} :catch_0
.end method

.method public final a(Lcom/google/p/a/b/b/a;)V
    .locals 9

    .prologue
    .line 428
    iget-object v2, p0, Lcom/google/android/location/e/ah;->h:Ljava/lang/Object;

    monitor-enter v2

    .line 429
    if-eqz p1, :cond_2

    .line 432
    :try_start_0
    iget-object v0, p0, Lcom/google/android/location/e/ah;->e:Lcom/google/p/a/b/b/a;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/p/a/b/b/a;->c(I)I

    move-result v0

    int-to-long v0, v0

    .line 433
    const/4 v3, 0x1

    invoke-virtual {p1, v3}, Lcom/google/p/a/b/b/a;->c(I)I

    move-result v3

    int-to-long v4, v3

    .line 434
    iput-object p1, p0, Lcom/google/android/location/e/ah;->e:Lcom/google/p/a/b/b/a;

    .line 435
    iget-object v3, p0, Lcom/google/android/location/e/ah;->c:Lcom/google/android/location/j/b;

    invoke-interface {v3}, Lcom/google/android/location/j/b;->c()J

    move-result-wide v6

    iput-wide v6, p0, Lcom/google/android/location/e/ah;->f:J

    .line 436
    cmp-long v0, v4, v0

    if-eqz v0, :cond_2

    .line 439
    sget-boolean v0, Lcom/google/android/location/i/a;->b:Z

    if-eqz v0, :cond_0

    const-string v0, "NlpParametersState"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "Updating NlpParameters. New version: "

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/location/o/a/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 440
    :cond_0
    iget-object v0, p0, Lcom/google/android/location/e/ah;->d:Lcom/google/android/location/j/e;

    invoke-static {v0}, Lcom/google/android/location/e/ah;->a(Lcom/google/android/location/j/e;)Ljava/io/File;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v3

    const/4 v0, 0x0

    :try_start_1
    iget-object v1, p0, Lcom/google/android/location/e/ah;->e:Lcom/google/p/a/b/b/a;

    sget-object v4, Lcom/google/android/location/e/ah;->a:Lcom/google/p/a/b/b/a;

    if-ne v1, v4, :cond_3

    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {v3}, Ljava/io/File;->delete()Z
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    :cond_1
    :goto_0
    :try_start_2
    invoke-static {v0}, Lcom/google/android/location/e/ah;->a(Ljava/io/Closeable;)V

    .line 441
    :goto_1
    iget-object v0, p0, Lcom/google/android/location/e/ah;->g:Lcom/google/android/location/o/e;

    if-eqz v0, :cond_2

    .line 442
    iget-object v0, p0, Lcom/google/android/location/e/ah;->g:Lcom/google/android/location/o/e;

    invoke-interface {v0, p0}, Lcom/google/android/location/o/e;->a(Ljava/lang/Object;)V

    .line 446
    :cond_2
    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    return-void

    .line 440
    :cond_3
    :try_start_3
    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v1

    if-nez v1, :cond_4

    invoke-virtual {v3}, Ljava/io/File;->createNewFile()Z

    :cond_4
    iget-object v1, p0, Lcom/google/android/location/e/ah;->d:Lcom/google/android/location/j/e;

    invoke-interface {v1, v3}, Lcom/google/android/location/j/e;->a(Ljava/io/File;)V

    new-instance v1, Ljava/io/DataOutputStream;

    new-instance v4, Ljava/io/FileOutputStream;

    invoke-direct {v4, v3}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    invoke-direct {v1, v4}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    const/4 v0, 0x2

    :try_start_4
    invoke-virtual {v1, v0}, Ljava/io/DataOutputStream;->writeShort(I)V

    iget-object v3, p0, Lcom/google/android/location/e/ah;->h:Ljava/lang/Object;

    monitor-enter v3
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_3

    :try_start_5
    iget-wide v4, p0, Lcom/google/android/location/e/ah;->f:J

    iget-object v0, p0, Lcom/google/android/location/e/ah;->c:Lcom/google/android/location/j/b;

    invoke-interface {v0}, Lcom/google/android/location/j/b;->d()J

    move-result-wide v6

    add-long/2addr v4, v6

    invoke-virtual {v1, v4, v5}, Ljava/io/DataOutputStream;->writeLong(J)V

    iget-object v0, p0, Lcom/google/android/location/e/ah;->e:Lcom/google/p/a/b/b/a;

    invoke-virtual {v0}, Lcom/google/p/a/b/b/a;->f()[B

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/io/DataOutputStream;->write([B)V

    monitor-exit v3
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    move-object v0, v1

    goto :goto_0

    :catchall_0
    move-exception v0

    :try_start_6
    monitor-exit v3

    throw v0
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_0
    .catchall {:try_start_6 .. :try_end_6} :catchall_3

    :catch_0
    move-exception v0

    move-object v0, v1

    :goto_2
    :try_start_7
    sget-boolean v1, Lcom/google/android/location/i/a;->e:Z

    if-eqz v1, :cond_5

    const-string v1, "NlpParametersState"

    const-string v3, "Failed to write parameter state."

    invoke-static {v1, v3}, Lcom/google/android/location/o/a/a;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_4

    :cond_5
    :try_start_8
    invoke-static {v0}, Lcom/google/android/location/e/ah;->a(Ljava/io/Closeable;)V
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    goto :goto_1

    .line 446
    :catchall_1
    move-exception v0

    monitor-exit v2

    throw v0

    .line 440
    :catchall_2
    move-exception v1

    move-object v8, v1

    move-object v1, v0

    move-object v0, v8

    :goto_3
    :try_start_9
    invoke-static {v1}, Lcom/google/android/location/e/ah;->a(Ljava/io/Closeable;)V

    throw v0
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_1

    :catchall_3
    move-exception v0

    goto :goto_3

    :catchall_4
    move-exception v1

    move-object v8, v1

    move-object v1, v0

    move-object v0, v8

    goto :goto_3

    :catch_1
    move-exception v1

    goto :goto_2
.end method

.method public final b()Z
    .locals 2

    .prologue
    .line 204
    invoke-direct {p0}, Lcom/google/android/location/e/ah;->x()Lcom/google/p/a/b/b/a;

    move-result-object v0

    sget-object v1, Lcom/google/android/location/e/ah;->a:Lcom/google/p/a/b/b/a;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c()I
    .locals 2

    .prologue
    .line 211
    invoke-direct {p0}, Lcom/google/android/location/e/ah;->x()Lcom/google/p/a/b/b/a;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/p/a/b/b/a;->c(I)I

    move-result v0

    return v0
.end method

.method public final d()I
    .locals 2

    .prologue
    .line 220
    invoke-direct {p0}, Lcom/google/android/location/e/ah;->x()Lcom/google/p/a/b/b/a;

    move-result-object v0

    const/16 v1, 0xb

    invoke-virtual {v0, v1}, Lcom/google/p/a/b/b/a;->c(I)I

    move-result v0

    return v0
.end method

.method public final e()I
    .locals 2

    .prologue
    .line 229
    invoke-direct {p0}, Lcom/google/android/location/e/ah;->x()Lcom/google/p/a/b/b/a;

    move-result-object v0

    const/16 v1, 0xc

    invoke-virtual {v0, v1}, Lcom/google/p/a/b/b/a;->c(I)I

    move-result v0

    return v0
.end method

.method public final f()Z
    .locals 1

    .prologue
    .line 236
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/google/android/location/e/ah;->a(I)Z

    move-result v0

    return v0
.end method

.method public final g()Z
    .locals 1

    .prologue
    .line 243
    const/4 v0, 0x5

    invoke-direct {p0, v0}, Lcom/google/android/location/e/ah;->a(I)Z

    move-result v0

    return v0
.end method

.method public final h()Z
    .locals 1

    .prologue
    .line 250
    const/4 v0, 0x6

    invoke-direct {p0, v0}, Lcom/google/android/location/e/ah;->a(I)Z

    move-result v0

    return v0
.end method

.method public final i()Z
    .locals 1

    .prologue
    .line 257
    const/4 v0, 0x7

    invoke-direct {p0, v0}, Lcom/google/android/location/e/ah;->a(I)Z

    move-result v0

    return v0
.end method

.method public final j()Z
    .locals 1

    .prologue
    .line 264
    const/16 v0, 0x8

    invoke-direct {p0, v0}, Lcom/google/android/location/e/ah;->a(I)Z

    move-result v0

    return v0
.end method

.method public final k()I
    .locals 2

    .prologue
    .line 271
    invoke-direct {p0}, Lcom/google/android/location/e/ah;->x()Lcom/google/p/a/b/b/a;

    move-result-object v0

    const/16 v1, 0x11

    invoke-virtual {v0, v1}, Lcom/google/p/a/b/b/a;->c(I)I

    move-result v0

    return v0
.end method

.method public final l()I
    .locals 4

    .prologue
    .line 280
    invoke-direct {p0}, Lcom/google/android/location/e/ah;->x()Lcom/google/p/a/b/b/a;

    move-result-object v0

    const/16 v1, 0x12

    invoke-virtual {v0, v1}, Lcom/google/p/a/b/b/a;->c(I)I

    move-result v0

    int-to-long v0, v0

    const-wide/16 v2, 0x3e8

    mul-long/2addr v0, v2

    long-to-int v0, v0

    return v0
.end method

.method public final m()I
    .locals 2

    .prologue
    .line 288
    invoke-direct {p0}, Lcom/google/android/location/e/ah;->x()Lcom/google/p/a/b/b/a;

    move-result-object v0

    const/16 v1, 0xc8

    invoke-virtual {v0, v1}, Lcom/google/p/a/b/b/a;->c(I)I

    move-result v0

    return v0
.end method

.method public final n()I
    .locals 2

    .prologue
    .line 295
    invoke-direct {p0}, Lcom/google/android/location/e/ah;->x()Lcom/google/p/a/b/b/a;

    move-result-object v0

    const/16 v1, 0xc9

    invoke-virtual {v0, v1}, Lcom/google/p/a/b/b/a;->c(I)I

    move-result v0

    return v0
.end method

.method public final o()I
    .locals 2

    .prologue
    .line 302
    invoke-direct {p0}, Lcom/google/android/location/e/ah;->x()Lcom/google/p/a/b/b/a;

    move-result-object v0

    const/16 v1, 0xca

    invoke-virtual {v0, v1}, Lcom/google/p/a/b/b/a;->c(I)I

    move-result v0

    return v0
.end method

.method public final p()I
    .locals 3

    .prologue
    const/16 v2, 0x32

    const/4 v1, 0x1

    .line 309
    invoke-direct {p0}, Lcom/google/android/location/e/ah;->x()Lcom/google/p/a/b/b/a;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/google/p/a/b/b/a;->i(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 310
    invoke-direct {p0}, Lcom/google/android/location/e/ah;->x()Lcom/google/p/a/b/b/a;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/google/p/a/b/b/a;->f(I)Lcom/google/p/a/b/b/a;

    move-result-object v0

    .line 312
    invoke-virtual {v0, v1}, Lcom/google/p/a/b/b/a;->c(I)I

    move-result v0

    .line 315
    :goto_0
    return v0

    :cond_0
    sget-object v0, Lcom/google/android/location/e/ah;->b:Lcom/google/p/a/b/b/a;

    invoke-virtual {v0, v1}, Lcom/google/p/a/b/b/a;->c(I)I

    move-result v0

    goto :goto_0
.end method

.method public final q()I
    .locals 2

    .prologue
    .line 338
    invoke-direct {p0}, Lcom/google/android/location/e/ah;->x()Lcom/google/p/a/b/b/a;

    move-result-object v0

    const/16 v1, 0xd

    invoke-virtual {v0, v1}, Lcom/google/p/a/b/b/a;->c(I)I

    move-result v0

    return v0
.end method

.method public final r()Lcom/google/android/location/e/ai;
    .locals 10

    .prologue
    .line 345
    invoke-direct {p0}, Lcom/google/android/location/e/ah;->x()Lcom/google/p/a/b/b/a;

    move-result-object v0

    .line 346
    new-instance v1, Lcom/google/android/location/e/ai;

    const/16 v2, 0x16

    invoke-virtual {v0, v2}, Lcom/google/p/a/b/b/a;->c(I)I

    move-result v2

    int-to-long v2, v2

    const/16 v4, 0x22

    invoke-virtual {v0, v4}, Lcom/google/p/a/b/b/a;->c(I)I

    move-result v4

    int-to-long v4, v4

    const/16 v6, 0x23

    invoke-virtual {v0, v6}, Lcom/google/p/a/b/b/a;->c(I)I

    move-result v0

    int-to-long v6, v0

    const-wide/16 v8, 0x3e8

    mul-long/2addr v6, v8

    invoke-direct/range {v1 .. v7}, Lcom/google/android/location/e/ai;-><init>(JJJ)V

    return-object v1
.end method

.method public final s()Lcom/google/android/location/e/ai;
    .locals 10

    .prologue
    const-wide/16 v8, 0x3e8

    .line 357
    invoke-direct {p0}, Lcom/google/android/location/e/ah;->x()Lcom/google/p/a/b/b/a;

    move-result-object v0

    .line 358
    new-instance v1, Lcom/google/android/location/e/ai;

    const/16 v2, 0x15

    invoke-virtual {v0, v2}, Lcom/google/p/a/b/b/a;->c(I)I

    move-result v2

    int-to-long v2, v2

    mul-long/2addr v2, v8

    const/16 v4, 0x1f

    invoke-virtual {v0, v4}, Lcom/google/p/a/b/b/a;->c(I)I

    move-result v4

    int-to-long v4, v4

    mul-long/2addr v4, v8

    const/16 v6, 0x20

    invoke-virtual {v0, v6}, Lcom/google/p/a/b/b/a;->c(I)I

    move-result v0

    int-to-long v6, v0

    mul-long/2addr v6, v8

    invoke-direct/range {v1 .. v7}, Lcom/google/android/location/e/ai;-><init>(JJJ)V

    return-object v1
.end method

.method public final t()Lcom/google/android/location/e/ai;
    .locals 8

    .prologue
    .line 368
    invoke-direct {p0}, Lcom/google/android/location/e/ah;->x()Lcom/google/p/a/b/b/a;

    move-result-object v0

    .line 369
    new-instance v1, Lcom/google/android/location/e/ai;

    const/16 v2, 0x17

    invoke-virtual {v0, v2}, Lcom/google/p/a/b/b/a;->c(I)I

    move-result v2

    mul-int/lit16 v2, v2, 0x3e8

    int-to-long v2, v2

    const/16 v4, 0x24

    invoke-virtual {v0, v4}, Lcom/google/p/a/b/b/a;->c(I)I

    move-result v4

    mul-int/lit16 v4, v4, 0x3e8

    int-to-long v4, v4

    const/16 v6, 0x25

    invoke-virtual {v0, v6}, Lcom/google/p/a/b/b/a;->c(I)I

    move-result v0

    mul-int/lit16 v0, v0, 0x3e8

    int-to-long v6, v0

    invoke-direct/range {v1 .. v7}, Lcom/google/android/location/e/ai;-><init>(JJJ)V

    return-object v1
.end method

.method public final u()Lcom/google/android/location/e/ai;
    .locals 8

    .prologue
    .line 379
    invoke-direct {p0}, Lcom/google/android/location/e/ah;->x()Lcom/google/p/a/b/b/a;

    move-result-object v0

    .line 380
    new-instance v1, Lcom/google/android/location/e/ai;

    const/16 v2, 0x18

    invoke-virtual {v0, v2}, Lcom/google/p/a/b/b/a;->c(I)I

    move-result v2

    mul-int/lit16 v2, v2, 0x3e8

    int-to-long v2, v2

    const/16 v4, 0x26

    invoke-virtual {v0, v4}, Lcom/google/p/a/b/b/a;->c(I)I

    move-result v4

    mul-int/lit16 v4, v4, 0x3e8

    int-to-long v4, v4

    const/16 v6, 0x27

    invoke-virtual {v0, v6}, Lcom/google/p/a/b/b/a;->c(I)I

    move-result v0

    mul-int/lit16 v0, v0, 0x3e8

    int-to-long v6, v0

    invoke-direct/range {v1 .. v7}, Lcom/google/android/location/e/ai;-><init>(JJJ)V

    return-object v1
.end method

.method public final v()J
    .locals 2

    .prologue
    .line 390
    invoke-direct {p0}, Lcom/google/android/location/e/ah;->x()Lcom/google/p/a/b/b/a;

    move-result-object v0

    const/16 v1, 0xf

    invoke-virtual {v0, v1}, Lcom/google/p/a/b/b/a;->c(I)I

    move-result v0

    mul-int/lit16 v0, v0, 0x3e8

    int-to-long v0, v0

    return-wide v0
.end method

.method public final w()J
    .locals 2

    .prologue
    .line 397
    invoke-direct {p0}, Lcom/google/android/location/e/ah;->x()Lcom/google/p/a/b/b/a;

    move-result-object v0

    const/16 v1, 0x10

    invoke-virtual {v0, v1}, Lcom/google/p/a/b/b/a;->c(I)I

    move-result v0

    mul-int/lit16 v0, v0, 0x3e8

    int-to-long v0, v0

    return-wide v0
.end method
