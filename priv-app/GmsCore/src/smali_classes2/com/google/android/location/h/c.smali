.class public final Lcom/google/android/location/h/c;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field a:Z

.field public b:J

.field private final c:Lcom/google/android/gms/common/util/p;

.field private d:J

.field private e:Z

.field private f:J


# direct methods
.method public constructor <init>(ZLcom/google/android/gms/common/util/p;)V
    .locals 4

    .prologue
    const-wide/16 v2, -0x1

    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/location/h/c;->a:Z

    .line 21
    iput-wide v2, p0, Lcom/google/android/location/h/c;->d:J

    .line 24
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/location/h/c;->e:Z

    .line 25
    iput-wide v2, p0, Lcom/google/android/location/h/c;->f:J

    .line 28
    iput-wide v2, p0, Lcom/google/android/location/h/c;->b:J

    .line 31
    iput-object p2, p0, Lcom/google/android/location/h/c;->c:Lcom/google/android/gms/common/util/p;

    .line 32
    iget-object v0, p0, Lcom/google/android/location/h/c;->c:Lcom/google/android/gms/common/util/p;

    invoke-interface {v0}, Lcom/google/android/gms/common/util/p;->b()J

    move-result-wide v0

    .line 33
    iput-boolean p1, p0, Lcom/google/android/location/h/c;->a:Z

    .line 34
    iput-wide v0, p0, Lcom/google/android/location/h/c;->d:J

    .line 35
    iput-wide v0, p0, Lcom/google/android/location/h/c;->f:J

    .line 36
    invoke-direct {p0}, Lcom/google/android/location/h/c;->a()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/location/h/c;->b:J

    .line 37
    return-void
.end method

.method private a()J
    .locals 4

    .prologue
    .line 96
    iget-wide v0, p0, Lcom/google/android/location/h/c;->d:J

    iget-wide v2, p0, Lcom/google/android/location/h/c;->f:J

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v0

    return-wide v0
.end method


# virtual methods
.method public final a(Z)V
    .locals 2

    .prologue
    .line 84
    iput-boolean p1, p0, Lcom/google/android/location/h/c;->a:Z

    .line 85
    iget-object v0, p0, Lcom/google/android/location/h/c;->c:Lcom/google/android/gms/common/util/p;

    invoke-interface {v0}, Lcom/google/android/gms/common/util/p;->b()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/location/h/c;->d:J

    .line 86
    invoke-direct {p0}, Lcom/google/android/location/h/c;->a()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/location/h/c;->b:J

    .line 87
    return-void
.end method

.method public final b(Z)V
    .locals 2

    .prologue
    .line 90
    iput-boolean p1, p0, Lcom/google/android/location/h/c;->e:Z

    .line 91
    iget-object v0, p0, Lcom/google/android/location/h/c;->c:Lcom/google/android/gms/common/util/p;

    invoke-interface {v0}, Lcom/google/android/gms/common/util/p;->b()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/location/h/c;->f:J

    .line 92
    invoke-direct {p0}, Lcom/google/android/location/h/c;->a()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/location/h/c;->b:J

    .line 93
    return-void
.end method
