.class abstract enum Lcom/google/android/location/reporting/n;
.super Ljava/lang/Enum;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/location/reporting/i;


# static fields
.field public static final enum a:Lcom/google/android/location/reporting/n;

.field public static final enum b:Lcom/google/android/location/reporting/n;

.field public static final enum c:Lcom/google/android/location/reporting/n;

.field public static final enum d:Lcom/google/android/location/reporting/n;

.field private static final synthetic e:[Lcom/google/android/location/reporting/n;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 226
    new-instance v0, Lcom/google/android/location/reporting/o;

    const-string v1, "First"

    invoke-direct {v0, v1}, Lcom/google/android/location/reporting/o;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/location/reporting/n;->a:Lcom/google/android/location/reporting/n;

    .line 243
    new-instance v0, Lcom/google/android/location/reporting/p;

    const-string v1, "ChargingInVehicle"

    invoke-direct {v0, v1}, Lcom/google/android/location/reporting/p;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/location/reporting/n;->b:Lcom/google/android/location/reporting/n;

    .line 263
    new-instance v0, Lcom/google/android/location/reporting/q;

    const-string v1, "Moving"

    invoke-direct {v0, v1}, Lcom/google/android/location/reporting/q;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/location/reporting/n;->c:Lcom/google/android/location/reporting/n;

    .line 273
    new-instance v0, Lcom/google/android/location/reporting/r;

    const-string v1, "Stationary"

    invoke-direct {v0, v1}, Lcom/google/android/location/reporting/r;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/location/reporting/n;->d:Lcom/google/android/location/reporting/n;

    .line 220
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/google/android/location/reporting/n;

    const/4 v1, 0x0

    sget-object v2, Lcom/google/android/location/reporting/n;->a:Lcom/google/android/location/reporting/n;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    sget-object v2, Lcom/google/android/location/reporting/n;->b:Lcom/google/android/location/reporting/n;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    sget-object v2, Lcom/google/android/location/reporting/n;->c:Lcom/google/android/location/reporting/n;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    sget-object v2, Lcom/google/android/location/reporting/n;->d:Lcom/google/android/location/reporting/n;

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/location/reporting/n;->e:[Lcom/google/android/location/reporting/n;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 220
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;IB)V
    .locals 0

    .prologue
    .line 220
    invoke-direct {p0, p1, p2}, Lcom/google/android/location/reporting/n;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/location/reporting/n;
    .locals 1

    .prologue
    .line 220
    const-class v0, Lcom/google/android/location/reporting/n;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/reporting/n;

    return-object v0
.end method

.method public static values()[Lcom/google/android/location/reporting/n;
    .locals 1

    .prologue
    .line 220
    sget-object v0, Lcom/google/android/location/reporting/n;->e:[Lcom/google/android/location/reporting/n;

    invoke-virtual {v0}, [Lcom/google/android/location/reporting/n;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/location/reporting/n;

    return-object v0
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 297
    const-string v0, "Normal"

    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 302
    const-string v0, "internal"

    return-object v0
.end method

.method public b(Z)Ljava/lang/String;
    .locals 1

    .prologue
    .line 292
    const-string v0, "default"

    return-object v0
.end method
