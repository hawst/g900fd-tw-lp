.class final Lcom/google/android/location/reporting/service/e;
.super Lcom/google/android/location/reporting/service/h;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/location/reporting/service/DispatchingService;

.field private final c:Lcom/google/android/location/reporting/m;

.field private final d:Lcom/google/android/location/reporting/j;

.field private final e:Lcom/google/android/location/reporting/b;

.field private final f:Lcom/google/android/location/reporting/service/v;

.field private final g:Lcom/google/android/location/reporting/service/i;

.field private final h:Lcom/google/android/location/reporting/service/ac;


# direct methods
.method public constructor <init>(Lcom/google/android/location/reporting/service/DispatchingService;Landroid/content/Context;Landroid/os/Looper;)V
    .locals 10

    .prologue
    .line 440
    iput-object p1, p0, Lcom/google/android/location/reporting/service/e;->a:Lcom/google/android/location/reporting/service/DispatchingService;

    .line 441
    const/4 v0, 0x0

    invoke-direct {p0, p1, p3, v0}, Lcom/google/android/location/reporting/service/h;-><init>(Lcom/google/android/location/reporting/service/DispatchingService;Landroid/os/Looper;B)V

    .line 443
    new-instance v0, Lcom/google/android/location/reporting/m;

    invoke-static {p1}, Lcom/google/android/location/reporting/service/DispatchingService;->c(Lcom/google/android/location/reporting/service/DispatchingService;)Lcom/google/android/location/reporting/service/q;

    move-result-object v1

    invoke-static {p1}, Lcom/google/android/location/reporting/service/DispatchingService;->d(Lcom/google/android/location/reporting/service/DispatchingService;)Lcom/google/android/location/reporting/s;

    move-result-object v2

    invoke-static {p1}, Lcom/google/android/location/reporting/service/DispatchingService;->e(Lcom/google/android/location/reporting/service/DispatchingService;)Lcom/google/android/location/reporting/ApiMetadataStore;

    move-result-object v3

    invoke-direct {v0, p2, v1, v2, v3}, Lcom/google/android/location/reporting/m;-><init>(Landroid/content/Context;Lcom/google/android/location/reporting/service/q;Lcom/google/android/location/reporting/s;Lcom/google/android/location/reporting/e;)V

    iput-object v0, p0, Lcom/google/android/location/reporting/service/e;->c:Lcom/google/android/location/reporting/m;

    .line 445
    invoke-static {p1}, Lcom/google/android/location/reporting/service/DispatchingService;->c(Lcom/google/android/location/reporting/service/DispatchingService;)Lcom/google/android/location/reporting/service/q;

    move-result-object v1

    invoke-static {p1}, Lcom/google/android/location/reporting/service/DispatchingService;->f(Lcom/google/android/location/reporting/service/DispatchingService;)Lcom/google/android/location/reporting/LocationReportingController;

    move-result-object v5

    iget-object v4, p0, Lcom/google/android/location/reporting/service/e;->c:Lcom/google/android/location/reporting/m;

    invoke-static {p1}, Lcom/google/android/location/reporting/service/DispatchingService;->g(Lcom/google/android/location/reporting/service/DispatchingService;)Lcom/google/android/location/reporting/LocationRecordStore;

    move-result-object v2

    invoke-static {p2}, Lcom/google/android/location/reporting/b/n;->a(Landroid/content/Context;)V

    new-instance v3, Lcom/google/android/location/reporting/b/c;

    invoke-direct {v3, p2}, Lcom/google/android/location/reporting/b/c;-><init>(Landroid/content/Context;)V

    sget-object v0, Lcom/google/android/location/d/g;->e:Lcom/google/android/location/d/g;

    invoke-static {v0, p2}, Lcom/google/android/location/d/f;->a(Lcom/google/android/location/d/g;Landroid/content/Context;)Lcom/google/android/location/d/f;

    move-result-object v0

    iget v6, v0, Lcom/google/android/location/d/f;->d:I

    const-string v0, "wifi"

    invoke-virtual {p2, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/net/wifi/WifiManager;

    new-instance v0, Lcom/google/android/location/reporting/j;

    new-instance v7, Lcom/google/android/gms/common/util/r;

    invoke-direct {v7}, Lcom/google/android/gms/common/util/r;-><init>()V

    invoke-direct/range {v0 .. v8}, Lcom/google/android/location/reporting/j;-><init>(Lcom/google/android/location/reporting/service/q;Lcom/google/android/location/reporting/e;Lcom/google/android/location/reporting/b/c;Lcom/google/android/location/reporting/m;Lcom/google/android/location/reporting/LocationReportingController;ILcom/google/android/gms/common/util/p;Landroid/net/wifi/WifiManager;)V

    iput-object v0, p0, Lcom/google/android/location/reporting/service/e;->d:Lcom/google/android/location/reporting/j;

    .line 447
    invoke-static {p1}, Lcom/google/android/location/reporting/service/DispatchingService;->c(Lcom/google/android/location/reporting/service/DispatchingService;)Lcom/google/android/location/reporting/service/q;

    move-result-object v0

    invoke-static {p1}, Lcom/google/android/location/reporting/service/DispatchingService;->f(Lcom/google/android/location/reporting/service/DispatchingService;)Lcom/google/android/location/reporting/LocationReportingController;

    move-result-object v1

    invoke-static {p1}, Lcom/google/android/location/reporting/service/DispatchingService;->h(Lcom/google/android/location/reporting/service/DispatchingService;)Lcom/google/android/location/reporting/DetectedActivityStore;

    move-result-object v2

    invoke-static {p2}, Lcom/google/android/location/reporting/b/n;->a(Landroid/content/Context;)V

    new-instance v3, Lcom/google/android/location/reporting/b;

    new-instance v4, Lcom/google/android/gms/common/util/r;

    invoke-direct {v4}, Lcom/google/android/gms/common/util/r;-><init>()V

    invoke-direct {v3, v0, v2, v1, v4}, Lcom/google/android/location/reporting/b;-><init>(Lcom/google/android/location/reporting/service/q;Lcom/google/android/location/reporting/e;Lcom/google/android/location/reporting/LocationReportingController;Lcom/google/android/gms/common/util/p;)V

    iput-object v3, p0, Lcom/google/android/location/reporting/service/e;->e:Lcom/google/android/location/reporting/b;

    .line 450
    new-instance v0, Lcom/google/android/location/reporting/service/v;

    invoke-direct {v0, p2}, Lcom/google/android/location/reporting/service/v;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/location/reporting/service/e;->f:Lcom/google/android/location/reporting/service/v;

    .line 451
    new-instance v0, Lcom/google/android/location/reporting/service/i;

    invoke-static {p1}, Lcom/google/android/location/reporting/service/DispatchingService;->i(Lcom/google/android/location/reporting/service/DispatchingService;)Lcom/google/android/location/reporting/config/h;

    move-result-object v1

    invoke-direct {v0, p2, v1}, Lcom/google/android/location/reporting/service/i;-><init>(Landroid/content/Context;Lcom/google/android/location/reporting/config/h;)V

    iput-object v0, p0, Lcom/google/android/location/reporting/service/e;->g:Lcom/google/android/location/reporting/service/i;

    .line 453
    new-instance v0, Lcom/google/android/location/reporting/service/ac;

    invoke-static {p1}, Lcom/google/android/location/reporting/service/DispatchingService;->c(Lcom/google/android/location/reporting/service/DispatchingService;)Lcom/google/android/location/reporting/service/q;

    move-result-object v2

    invoke-static {p1}, Lcom/google/android/location/reporting/service/DispatchingService;->i(Lcom/google/android/location/reporting/service/DispatchingService;)Lcom/google/android/location/reporting/config/h;

    move-result-object v3

    invoke-static {p1}, Lcom/google/android/location/reporting/service/DispatchingService;->d(Lcom/google/android/location/reporting/service/DispatchingService;)Lcom/google/android/location/reporting/s;

    move-result-object v4

    invoke-static {p1}, Lcom/google/android/location/reporting/service/DispatchingService;->f(Lcom/google/android/location/reporting/service/DispatchingService;)Lcom/google/android/location/reporting/LocationReportingController;

    move-result-object v5

    invoke-static {p1}, Lcom/google/android/location/reporting/service/DispatchingService;->g(Lcom/google/android/location/reporting/service/DispatchingService;)Lcom/google/android/location/reporting/LocationRecordStore;

    move-result-object v6

    invoke-static {p1}, Lcom/google/android/location/reporting/service/DispatchingService;->h(Lcom/google/android/location/reporting/service/DispatchingService;)Lcom/google/android/location/reporting/DetectedActivityStore;

    move-result-object v7

    invoke-static {p1}, Lcom/google/android/location/reporting/service/DispatchingService;->e(Lcom/google/android/location/reporting/service/DispatchingService;)Lcom/google/android/location/reporting/ApiMetadataStore;

    move-result-object v8

    iget-object v9, p0, Lcom/google/android/location/reporting/service/e;->g:Lcom/google/android/location/reporting/service/i;

    move-object v1, p2

    invoke-direct/range {v0 .. v9}, Lcom/google/android/location/reporting/service/ac;-><init>(Landroid/content/Context;Lcom/google/android/location/reporting/service/q;Lcom/google/android/location/reporting/config/h;Lcom/google/android/location/reporting/s;Lcom/google/android/location/reporting/LocationReportingController;Lcom/google/android/location/reporting/LocationRecordStore;Lcom/google/android/location/reporting/e;Lcom/google/android/location/reporting/e;Lcom/google/android/location/reporting/service/i;)V

    iput-object v0, p0, Lcom/google/android/location/reporting/service/e;->h:Lcom/google/android/location/reporting/service/ac;

    .line 456
    return-void
.end method

.method private a(Lcom/google/android/ulr/ApiMetadata;Ljava/lang/String;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 543
    iget-object v0, p0, Lcom/google/android/location/reporting/service/e;->a:Lcom/google/android/location/reporting/service/DispatchingService;

    invoke-static {v0}, Lcom/google/android/location/reporting/service/DispatchingService;->i(Lcom/google/android/location/reporting/service/DispatchingService;)Lcom/google/android/location/reporting/config/h;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/location/reporting/config/h;->a()Lcom/google/android/location/reporting/config/ReportingConfig;

    move-result-object v0

    .line 544
    const-string v1, "GCoreUlr"

    const/4 v2, 0x3

    invoke-static {v1, v2}, Lcom/google/android/location/reporting/b/d;->a(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 545
    const-string v1, "GCoreUlr"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Received "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " for accounts "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Lcom/google/android/location/reporting/config/ReportingConfig;->getActiveAccounts()Ljava/util/List;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/gms/location/reporting/a/d;->a(Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/location/reporting/b/d;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 548
    :cond_0
    iget-object v1, p0, Lcom/google/android/location/reporting/service/e;->a:Lcom/google/android/location/reporting/service/DispatchingService;

    invoke-static {v1}, Lcom/google/android/location/reporting/service/DispatchingService;->e(Lcom/google/android/location/reporting/service/DispatchingService;)Lcom/google/android/location/reporting/ApiMetadataStore;

    move-result-object v1

    invoke-static {v1, v0, p1, p3}, Lcom/google/android/location/reporting/c;->a(Lcom/google/android/location/reporting/e;Lcom/google/android/location/reporting/config/ReportingConfig;Lcom/google/android/ulr/ApiMetadata;Ljava/lang/String;)V

    .line 549
    return-void
.end method

.method private b(Landroid/content/Intent;)V
    .locals 4

    .prologue
    .line 526
    :try_start_0
    const-string v0, "ble_scan"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getParcelableArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    .line 527
    if-nez v0, :cond_1

    .line 528
    const-string v0, "GCoreUlr"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "bleScans not set in "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/location/reporting/b/d;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 539
    :cond_0
    :goto_0
    return-void

    .line 531
    :cond_1
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/reporting/ble/ParcelableBleScan;

    .line 532
    invoke-static {v0}, Lcom/google/android/location/reporting/ble/ParcelableBleScan;->a(Lcom/google/android/location/reporting/ble/ParcelableBleScan;)Lcom/google/android/ulr/ApiMetadata;

    move-result-object v2

    .line 533
    invoke-virtual {v0}, Lcom/google/android/location/reporting/ble/ParcelableBleScan;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v3, "ble scan"

    invoke-direct {p0, v2, v0, v3}, Lcom/google/android/location/reporting/service/e;->a(Lcom/google/android/ulr/ApiMetadata;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 536
    :catch_0
    move-exception v0

    .line 537
    iget-object v1, p0, Lcom/google/android/location/reporting/service/e;->a:Lcom/google/android/location/reporting/service/DispatchingService;

    invoke-static {v0}, Lcom/google/android/location/reporting/service/DispatchingService;->a(Ljava/lang/RuntimeException;)V

    goto :goto_0
.end method


# virtual methods
.method protected final a(Landroid/content/Intent;)V
    .locals 9

    .prologue
    const/4 v8, 0x3

    .line 460
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 461
    if-eqz v0, :cond_2

    .line 462
    const-string v1, "GCoreUlr"

    invoke-static {v1, v8}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 463
    const-string v1, "GCoreUlr"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "DispatchingService dispatching "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", source: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/location/reporting/service/e;->a:Lcom/google/android/location/reporting/service/DispatchingService;

    invoke-static {p1}, Lcom/google/android/location/reporting/service/DispatchingService;->a(Landroid/content/Intent;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 481
    :cond_0
    const-string v1, "com.google.android.location.reporting.ACTION_LOCATION"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 482
    iget-object v0, p0, Lcom/google/android/location/reporting/service/e;->a:Lcom/google/android/location/reporting/service/DispatchingService;

    invoke-static {v0}, Lcom/google/android/location/reporting/service/DispatchingService;->i(Lcom/google/android/location/reporting/service/DispatchingService;)Lcom/google/android/location/reporting/config/h;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/location/reporting/config/h;->a()Lcom/google/android/location/reporting/config/ReportingConfig;

    move-result-object v0

    .line 483
    iget-object v1, p0, Lcom/google/android/location/reporting/service/e;->d:Lcom/google/android/location/reporting/j;

    invoke-virtual {v1, p1, v0}, Lcom/google/android/location/reporting/j;->a(Landroid/content/Intent;Lcom/google/android/location/reporting/config/ReportingConfig;)V

    .line 504
    :cond_1
    :goto_0
    return-void

    .line 470
    :cond_2
    const-string v0, "GCoreUlr"

    const/4 v1, 0x5

    invoke-static {v0, v1}, Lcom/google/android/location/reporting/b/d;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 471
    const-string v0, "GCoreUlr"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Legacy intent "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", updating active state to cancel it"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/location/reporting/b/d;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 473
    :cond_3
    iget-object v0, p0, Lcom/google/android/location/reporting/service/e;->a:Lcom/google/android/location/reporting/service/DispatchingService;

    invoke-static {v0}, Lcom/google/android/location/reporting/service/DispatchingService;->j(Lcom/google/android/location/reporting/service/DispatchingService;)Landroid/content/Intent;

    move-result-object v0

    .line 474
    iget-object v1, p0, Lcom/google/android/location/reporting/service/e;->a:Lcom/google/android/location/reporting/service/DispatchingService;

    invoke-static {v1, v0}, Lcom/google/android/location/reporting/LocationReportingController;->a(Landroid/content/Context;Landroid/content/Intent;)Landroid/app/PendingIntent;

    move-result-object v0

    .line 476
    iget-object v1, p0, Lcom/google/android/location/reporting/service/e;->a:Lcom/google/android/location/reporting/service/DispatchingService;

    invoke-static {v1}, Lcom/google/android/location/reporting/service/DispatchingService;->k(Lcom/google/android/location/reporting/service/DispatchingService;)Lcom/google/android/location/fused/g;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/android/location/fused/g;->b(Landroid/app/PendingIntent;)V

    .line 477
    invoke-virtual {v0}, Landroid/app/PendingIntent;->cancel()V

    goto :goto_0

    .line 484
    :cond_4
    const-string v1, "com.google.android.location.reporting.ACTION_ACTIVITY"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_c

    .line 485
    iget-object v0, p0, Lcom/google/android/location/reporting/service/e;->a:Lcom/google/android/location/reporting/service/DispatchingService;

    invoke-static {v0}, Lcom/google/android/location/reporting/service/DispatchingService;->i(Lcom/google/android/location/reporting/service/DispatchingService;)Lcom/google/android/location/reporting/config/h;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/location/reporting/config/h;->a()Lcom/google/android/location/reporting/config/ReportingConfig;

    move-result-object v1

    .line 486
    iget-object v2, p0, Lcom/google/android/location/reporting/service/e;->e:Lcom/google/android/location/reporting/b;

    const-string v0, "GCoreUlr"

    invoke-static {v0, v8}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_5

    const-string v0, "GCoreUlr"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "ActivityReceiver received "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "; lowPowerMode: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {}, Lcom/google/android/location/activity/k;->a()Z

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "; mState: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, v2, Lcom/google/android/location/reporting/b;->a:Lcom/google/android/location/reporting/service/q;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_5
    invoke-virtual {v1}, Lcom/google/android/location/reporting/config/ReportingConfig;->isReportingActive()Z

    move-result v0

    if-nez v0, :cond_6

    iget-object v0, v2, Lcom/google/android/location/reporting/b;->b:Lcom/google/android/location/reporting/LocationReportingController;

    iget-object v2, v2, Lcom/google/android/location/reporting/b;->c:Lcom/google/android/gms/common/util/p;

    invoke-static {v1, v0}, Lcom/google/android/location/reporting/j;->a(Lcom/google/android/location/reporting/config/ReportingConfig;Lcom/google/android/location/reporting/LocationReportingController;)V

    goto/16 :goto_0

    :cond_6
    invoke-static {p1}, Lcom/google/android/location/reporting/j;->a(Landroid/content/Intent;)Z

    move-result v0

    if-eqz v0, :cond_7

    const-string v0, "GCoreUlr"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Administrative event in ActivityReceiver: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    :cond_7
    invoke-static {p1}, Lcom/google/android/gms/location/ActivityRecognitionResult;->b(Landroid/content/Intent;)Lcom/google/android/gms/location/ActivityRecognitionResult;

    move-result-object v3

    if-nez v3, :cond_8

    const-string v0, "GCoreUlr"

    new-instance v1, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " has no activity"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    invoke-static {v0, v1}, Lcom/google/android/location/reporting/b/d;->b(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_0

    :cond_8
    iget-object v0, v2, Lcom/google/android/location/reporting/b;->a:Lcom/google/android/location/reporting/service/q;

    iput-object v3, v0, Lcom/google/android/location/reporting/service/q;->b:Lcom/google/android/gms/location/ActivityRecognitionResult;

    invoke-virtual {v3}, Lcom/google/android/gms/location/ActivityRecognitionResult;->e()J

    move-result-wide v4

    const-wide/16 v6, 0x0

    cmp-long v0, v4, v6

    if-gtz v0, :cond_9

    const-string v0, "GCoreUlr"

    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v4, "No realtime in activity result: "

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    invoke-static {v0, v1}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/Throwable;)I

    goto/16 :goto_0

    :cond_9
    iget-object v0, v2, Lcom/google/android/location/reporting/b;->a:Lcom/google/android/location/reporting/service/q;

    invoke-virtual {v0}, Lcom/google/android/location/reporting/service/q;->e()Lcom/google/android/gms/location/ActivityRecognitionResult;

    move-result-object v0

    if-eqz v0, :cond_b

    invoke-virtual {v0}, Lcom/google/android/gms/location/ActivityRecognitionResult;->a()Lcom/google/android/gms/location/DetectedActivity;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/android/gms/location/DetectedActivity;->a()I

    move-result v6

    invoke-virtual {v3}, Lcom/google/android/gms/location/ActivityRecognitionResult;->a()Lcom/google/android/gms/location/DetectedActivity;

    move-result-object v7

    invoke-virtual {v7}, Lcom/google/android/gms/location/DetectedActivity;->a()I

    move-result v7

    if-ne v6, v7, :cond_b

    invoke-virtual {v0}, Lcom/google/android/gms/location/ActivityRecognitionResult;->e()J

    move-result-wide v6

    sub-long/2addr v4, v6

    sget-object v0, Lcom/google/android/location/reporting/service/ab;->j:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    cmp-long v0, v4, v6

    if-gtz v0, :cond_b

    const-string v0, "GCoreUlr"

    invoke-static {v0, v8}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_a

    const-string v0, "GCoreUlr"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "Throttling activity because it\'s only been "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " millis"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_a
    const/4 v0, 0x1

    :goto_1
    if-nez v0, :cond_1

    iget-object v0, v2, Lcom/google/android/location/reporting/b;->a:Lcom/google/android/location/reporting/service/q;

    invoke-virtual {v0, v3}, Lcom/google/android/location/reporting/service/q;->a(Lcom/google/android/gms/location/ActivityRecognitionResult;)V

    invoke-virtual {v2, v3, v1}, Lcom/google/android/location/reporting/b;->a(Lcom/google/android/gms/location/ActivityRecognitionResult;Lcom/google/android/location/reporting/config/ReportingConfig;)V

    goto/16 :goto_0

    :cond_b
    const/4 v0, 0x0

    goto :goto_1

    .line 487
    :cond_c
    const-string v1, "com.google.android.location.reporting.ACTION_LOCATION_STATUS"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_e

    .line 488
    const-string v0, "status"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/location/LocationStatus;

    if-nez v0, :cond_d

    const-string v0, "GCoreUlr"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "locationStatus not set in "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/location/reporting/b/d;->f(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_d
    iget-object v1, p0, Lcom/google/android/location/reporting/service/e;->a:Lcom/google/android/location/reporting/service/DispatchingService;

    invoke-static {v1}, Lcom/google/android/location/reporting/service/DispatchingService;->c(Lcom/google/android/location/reporting/service/DispatchingService;)Lcom/google/android/location/reporting/service/q;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/android/location/reporting/service/q;->a(Lcom/google/android/gms/location/LocationStatus;)V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-static {v0, v2, v3}, Lcom/google/android/location/reporting/c;->a(Lcom/google/android/gms/location/LocationStatus;J)Lcom/google/android/ulr/ApiMetadata;

    move-result-object v1

    invoke-virtual {v0}, Lcom/google/android/gms/location/LocationStatus;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v2, "location status"

    invoke-direct {p0, v1, v0, v2}, Lcom/google/android/location/reporting/service/e;->a(Lcom/google/android/ulr/ApiMetadata;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 489
    :cond_e
    const-string v1, "com.google.android.location.reporting.ACTION_BLE_SCAN"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_f

    .line 490
    invoke-direct {p0, p1}, Lcom/google/android/location/reporting/service/e;->b(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 491
    :cond_f
    const-string v1, "com.google.android.location.reporting.ACTION_UPDATE_WORLD"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_10

    .line 492
    iget-object v0, p0, Lcom/google/android/location/reporting/service/e;->h:Lcom/google/android/location/reporting/service/ac;

    invoke-virtual {v0, p1}, Lcom/google/android/location/reporting/service/ac;->a(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 493
    :cond_10
    const-string v1, "com.google.android.location.reporting.ACTION_UPDATE_ACTIVE_STATE"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_11

    .line 494
    iget-object v0, p0, Lcom/google/android/location/reporting/service/e;->a:Lcom/google/android/location/reporting/service/DispatchingService;

    invoke-static {v0}, Lcom/google/android/location/reporting/service/DispatchingService;->i(Lcom/google/android/location/reporting/service/DispatchingService;)Lcom/google/android/location/reporting/config/h;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/location/reporting/config/h;->a()Lcom/google/android/location/reporting/config/ReportingConfig;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/location/reporting/service/e;->a:Lcom/google/android/location/reporting/service/DispatchingService;

    invoke-static {v1}, Lcom/google/android/location/reporting/service/DispatchingService;->f(Lcom/google/android/location/reporting/service/DispatchingService;)Lcom/google/android/location/reporting/LocationReportingController;

    move-result-object v1

    const-string v2, "DispatchingService.updateActiveState"

    invoke-virtual {v1, v0, v2}, Lcom/google/android/location/reporting/LocationReportingController;->a(Lcom/google/android/location/reporting/config/ReportingConfig;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 495
    :cond_11
    const-string v1, "com.google.android.location.reporting.ACTION_APPLY_UPLOAD_REQUESTS"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_12

    .line 496
    iget-object v0, p0, Lcom/google/android/location/reporting/service/e;->a:Lcom/google/android/location/reporting/service/DispatchingService;

    invoke-static {v0}, Lcom/google/android/location/reporting/service/DispatchingService;->i(Lcom/google/android/location/reporting/service/DispatchingService;)Lcom/google/android/location/reporting/config/h;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/location/reporting/config/h;->a()Lcom/google/android/location/reporting/config/ReportingConfig;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/location/reporting/service/e;->c:Lcom/google/android/location/reporting/m;

    iget-object v2, p0, Lcom/google/android/location/reporting/service/e;->a:Lcom/google/android/location/reporting/service/DispatchingService;

    invoke-static {v2}, Lcom/google/android/location/reporting/service/DispatchingService;->c(Lcom/google/android/location/reporting/service/DispatchingService;)Lcom/google/android/location/reporting/service/q;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/location/reporting/service/e;->a:Lcom/google/android/location/reporting/service/DispatchingService;

    invoke-static {v3}, Lcom/google/android/location/reporting/service/DispatchingService;->c(Lcom/google/android/location/reporting/service/DispatchingService;)Lcom/google/android/location/reporting/service/q;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/location/reporting/service/q;->d()Landroid/location/Location;

    move-result-object v3

    invoke-virtual {v1, v2, v0, v3}, Lcom/google/android/location/reporting/m;->a(Lcom/google/android/location/reporting/service/q;Lcom/google/android/location/reporting/config/ReportingConfig;Landroid/location/Location;)Z

    goto/16 :goto_0

    .line 497
    :cond_12
    const-string v1, "com.google.android.location.reporting.ACTION_INSISTENT_SYNC"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_13

    .line 498
    iget-object v0, p0, Lcom/google/android/location/reporting/service/e;->f:Lcom/google/android/location/reporting/service/v;

    invoke-virtual {v0, p1}, Lcom/google/android/location/reporting/service/v;->a(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 499
    :cond_13
    const-string v1, "com.google.android.location.reporting.ACTION_RESET_WIFI_ATTACHMENT"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_14

    .line 500
    iget-object v0, p0, Lcom/google/android/location/reporting/service/e;->a:Lcom/google/android/location/reporting/service/DispatchingService;

    invoke-static {v0}, Lcom/google/android/location/reporting/service/DispatchingService;->c(Lcom/google/android/location/reporting/service/DispatchingService;)Lcom/google/android/location/reporting/service/q;

    move-result-object v0

    const-wide/16 v2, -0x1

    invoke-virtual {v0, v2, v3}, Lcom/google/android/location/reporting/service/q;->a(J)V

    goto/16 :goto_0

    .line 502
    :cond_14
    const-string v0, "GCoreUlr"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unsupported action in "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/location/reporting/b/d;->f(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0
.end method
