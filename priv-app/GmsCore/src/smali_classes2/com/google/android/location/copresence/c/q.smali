.class public final Lcom/google/android/location/copresence/c/q;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field a:I

.field final b:Landroid/os/Handler;

.field final c:Lcom/google/android/gms/audiomodem/bm;

.field final d:Lcom/google/android/gms/audiomodem/bf;

.field e:Lcom/google/android/gms/audiomodem/TokenBroadcaster$Params;

.field f:Lcom/google/android/gms/audiomodem/TokenReceiver$Params;

.field final g:Lcom/google/android/location/copresence/c/b;

.field final h:Ljava/lang/Runnable;

.field final i:Ljava/lang/Runnable;

.field final j:Lcom/google/android/gms/audiomodem/bc;

.field private final k:Ljava/util/Random;

.field private final l:Lcom/google/android/gms/audiomodem/bc;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/location/copresence/c/b;Lcom/google/android/gms/audiomodem/bm;Lcom/google/android/gms/audiomodem/bf;)V
    .locals 1

    .prologue
    .line 65
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 107
    new-instance v0, Lcom/google/android/location/copresence/c/r;

    invoke-direct {v0, p0}, Lcom/google/android/location/copresence/c/r;-><init>(Lcom/google/android/location/copresence/c/q;)V

    iput-object v0, p0, Lcom/google/android/location/copresence/c/q;->h:Ljava/lang/Runnable;

    .line 126
    new-instance v0, Lcom/google/android/location/copresence/c/s;

    invoke-direct {v0, p0}, Lcom/google/android/location/copresence/c/s;-><init>(Lcom/google/android/location/copresence/c/q;)V

    iput-object v0, p0, Lcom/google/android/location/copresence/c/q;->i:Ljava/lang/Runnable;

    .line 181
    new-instance v0, Lcom/google/android/location/copresence/c/t;

    invoke-direct {v0, p0}, Lcom/google/android/location/copresence/c/t;-><init>(Lcom/google/android/location/copresence/c/q;)V

    iput-object v0, p0, Lcom/google/android/location/copresence/c/q;->l:Lcom/google/android/gms/audiomodem/bc;

    .line 209
    new-instance v0, Lcom/google/android/location/copresence/c/v;

    invoke-direct {v0, p0}, Lcom/google/android/location/copresence/c/v;-><init>(Lcom/google/android/location/copresence/c/q;)V

    iput-object v0, p0, Lcom/google/android/location/copresence/c/q;->j:Lcom/google/android/gms/audiomodem/bc;

    .line 66
    invoke-static {p1}, Lcom/google/android/location/copresence/ap;->a(Landroid/content/Context;)Lcom/google/android/location/copresence/ap;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/location/copresence/ap;->d()Landroid/os/Handler;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/copresence/c/q;->b:Landroid/os/Handler;

    .line 67
    iput-object p3, p0, Lcom/google/android/location/copresence/c/q;->c:Lcom/google/android/gms/audiomodem/bm;

    .line 68
    iput-object p4, p0, Lcom/google/android/location/copresence/c/q;->d:Lcom/google/android/gms/audiomodem/bf;

    .line 69
    iput-object p2, p0, Lcom/google/android/location/copresence/c/q;->g:Lcom/google/android/location/copresence/c/b;

    .line 70
    new-instance v0, Ljava/util/Random;

    invoke-direct {v0}, Ljava/util/Random;-><init>()V

    iput-object v0, p0, Lcom/google/android/location/copresence/c/q;->k:Ljava/util/Random;

    .line 71
    return-void
.end method

.method static synthetic a(Lcom/google/android/location/copresence/c/q;)V
    .locals 5

    .prologue
    .line 30
    const/4 v0, 0x3

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "Audio Stack: CarrierSenseManager: Entering broadcast"

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->b(Ljava/lang/String;)I

    :cond_0
    invoke-static {}, Lcom/google/android/location/copresence/f/b;->b()Lcom/google/ac/b/c/g;

    move-result-object v0

    iget-object v0, v0, Lcom/google/ac/b/c/g;->f:Lcom/google/ac/b/c/f;

    iget-object v0, v0, Lcom/google/ac/b/c/f;->c:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-static {}, Lcom/google/android/location/copresence/f/b;->b()Lcom/google/ac/b/c/g;

    move-result-object v2

    iget-object v2, v2, Lcom/google/ac/b/c/g;->f:Lcom/google/ac/b/c/f;

    iget-object v2, v2, Lcom/google/ac/b/c/f;->a:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/google/android/location/copresence/c/q;->a(JJ)J

    move-result-wide v0

    iget-object v2, p0, Lcom/google/android/location/copresence/c/q;->g:Lcom/google/android/location/copresence/c/b;

    iget-object v3, p0, Lcom/google/android/location/copresence/c/q;->d:Lcom/google/android/gms/audiomodem/bf;

    iget-object v4, p0, Lcom/google/android/location/copresence/c/q;->e:Lcom/google/android/gms/audiomodem/TokenBroadcaster$Params;

    invoke-virtual {v2, v3, v4}, Lcom/google/android/location/copresence/c/b;->a(Lcom/google/android/gms/audiomodem/bf;Lcom/google/android/gms/audiomodem/TokenBroadcaster$Params;)V

    iget-object v2, p0, Lcom/google/android/location/copresence/c/q;->b:Landroid/os/Handler;

    iget-object v3, p0, Lcom/google/android/location/copresence/c/q;->h:Ljava/lang/Runnable;

    invoke-virtual {v2, v3, v0, v1}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    const/4 v0, 0x2

    iput v0, p0, Lcom/google/android/location/copresence/c/q;->a:I

    return-void
.end method


# virtual methods
.method final a(JJ)J
    .locals 7

    .prologue
    .line 267
    invoke-static {}, Lcom/google/android/location/copresence/f/b;->b()Lcom/google/ac/b/c/g;

    move-result-object v0

    iget-object v0, v0, Lcom/google/ac/b/c/g;->f:Lcom/google/ac/b/c/f;

    iget-object v0, v0, Lcom/google/ac/b/c/f;->h:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    .line 268
    const-wide/16 v0, 0x0

    cmp-long v0, v2, v0

    if-nez v0, :cond_0

    .line 270
    long-to-double v0, p1

    long-to-double v2, p3

    invoke-static {}, Ljava/lang/Math;->random()D

    move-result-wide v4

    mul-double/2addr v2, v4

    add-double/2addr v0, v2

    double-to-long v0, v0

    .line 274
    :goto_0
    return-wide v0

    .line 272
    :cond_0
    div-long v0, p3, v2

    long-to-int v0, v0

    add-int/lit8 v0, v0, 0x1

    .line 273
    if-lez v0, :cond_1

    iget-object v1, p0, Lcom/google/android/location/copresence/c/q;->k:Ljava/util/Random;

    invoke-virtual {v1, v0}, Ljava/util/Random;->nextInt(I)I

    move-result v0

    .line 274
    :goto_1
    int-to-long v0, v0

    mul-long/2addr v0, v2

    add-long/2addr v0, p1

    goto :goto_0

    .line 273
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method final a()V
    .locals 7

    .prologue
    .line 165
    const/4 v0, 0x3

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 166
    const-string v0, "Audio Stack: CarrierSenseManager: Entering listenWhileActiveBroadcaster"

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->b(Ljava/lang/String;)I

    .line 169
    :cond_0
    invoke-static {}, Lcom/google/android/location/copresence/f/b;->b()Lcom/google/ac/b/c/g;

    move-result-object v0

    iget-object v0, v0, Lcom/google/ac/b/c/g;->f:Lcom/google/ac/b/c/f;

    iget-object v0, v0, Lcom/google/ac/b/c/f;->e:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-static {}, Lcom/google/android/location/copresence/f/b;->b()Lcom/google/ac/b/c/g;

    move-result-object v2

    iget-object v2, v2, Lcom/google/ac/b/c/g;->f:Lcom/google/ac/b/c/f;

    iget-object v2, v2, Lcom/google/ac/b/c/f;->f:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/google/android/location/copresence/c/q;->a(JJ)J

    move-result-wide v4

    .line 173
    iget-object v0, p0, Lcom/google/android/location/copresence/c/q;->g:Lcom/google/android/location/copresence/c/b;

    iget-object v1, p0, Lcom/google/android/location/copresence/c/q;->c:Lcom/google/android/gms/audiomodem/bm;

    iget-object v2, p0, Lcom/google/android/location/copresence/c/q;->f:Lcom/google/android/gms/audiomodem/TokenReceiver$Params;

    const/4 v3, 0x0

    iget-object v6, p0, Lcom/google/android/location/copresence/c/q;->l:Lcom/google/android/gms/audiomodem/bc;

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/location/copresence/c/b;->a(Lcom/google/android/gms/audiomodem/bm;Lcom/google/android/gms/audiomodem/TokenReceiver$Params;ZJLcom/google/android/gms/audiomodem/bc;)V

    .line 178
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/location/copresence/c/q;->a:I

    .line 179
    return-void
.end method
