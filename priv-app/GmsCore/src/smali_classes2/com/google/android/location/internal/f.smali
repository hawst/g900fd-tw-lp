.class public final Lcom/google/android/location/internal/f;
.super Lcom/google/android/gms/location/internal/r;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/android/location/internal/e;

.field private final b:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/google/android/location/internal/e;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 47
    invoke-direct {p0}, Lcom/google/android/gms/location/internal/r;-><init>()V

    .line 48
    iput-object p1, p0, Lcom/google/android/location/internal/f;->a:Lcom/google/android/location/internal/e;

    .line 49
    iput-object p2, p0, Lcom/google/android/location/internal/f;->b:Ljava/lang/String;

    .line 50
    return-void
.end method


# virtual methods
.method public final a()Landroid/location/Location;
    .locals 2

    .prologue
    .line 269
    iget-object v0, p0, Lcom/google/android/location/internal/f;->a:Lcom/google/android/location/internal/e;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/location/internal/e;->a(Ljava/lang/String;)Landroid/location/Location;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;)Landroid/location/Location;
    .locals 1

    .prologue
    .line 93
    iget-object v0, p0, Lcom/google/android/location/internal/f;->a:Lcom/google/android/location/internal/e;

    invoke-virtual {v0, p1}, Lcom/google/android/location/internal/e;->a(Ljava/lang/String;)Landroid/location/Location;

    move-result-object v0

    return-object v0
.end method

.method public final a(JZLandroid/app/PendingIntent;)V
    .locals 9

    .prologue
    .line 258
    iget-object v0, p0, Lcom/google/android/location/internal/f;->a:Lcom/google/android/location/internal/e;

    iget-object v1, v0, Lcom/google/android/location/internal/e;->a:Landroid/content/Context;

    invoke-static {v1}, Lcom/google/android/location/internal/e;->a(Landroid/content/Context;)V

    invoke-virtual {v0}, Lcom/google/android/location/internal/e;->e()Z

    move-result v7

    const-string v1, "GLMSImpl"

    const/4 v2, 0x3

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "GLMSImpl"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "requestActivityUpdates: isFirstParty="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " detectionIntervalMillis="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " force="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " callback="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    invoke-static {}, Lcom/google/android/gms/common/util/ay;->e()I

    move-result v1

    const/16 v2, 0xa

    if-ne v1, v2, :cond_1

    if-nez v7, :cond_1

    const-string v0, "GLMSImpl"

    const-string v1, "Activity recognition is not implemented on Android Wear yet."

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 260
    :goto_0
    return-void

    .line 258
    :cond_1
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v1

    invoke-virtual {p4}, Landroid/app/PendingIntent;->getTargetPackage()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/gms/common/util/be;->a(ILjava/lang/String;)Landroid/os/WorkSource;

    move-result-object v8

    new-instance v1, Lcom/google/android/location/internal/j;

    invoke-direct {v1}, Lcom/google/android/location/internal/j;-><init>()V

    const-string v6, "GLMSImplProxy"

    move-wide v2, p1

    move v4, p3

    move-object v5, p4

    invoke-virtual/range {v1 .. v6}, Lcom/google/android/location/internal/j;->a(JZLandroid/app/PendingIntent;Ljava/lang/String;)Lcom/google/android/location/internal/j;

    move-result-object v1

    invoke-virtual {v1, v7}, Lcom/google/android/location/internal/j;->c(Z)Lcom/google/android/location/internal/j;

    move-result-object v1

    invoke-virtual {v1, v8}, Lcom/google/android/location/internal/j;->a(Landroid/os/WorkSource;)Lcom/google/android/location/internal/j;

    move-result-object v1

    iget-object v0, v0, Lcom/google/android/location/internal/e;->a:Landroid/content/Context;

    invoke-virtual {v1, v0}, Lcom/google/android/location/internal/j;->a(Landroid/content/Context;)Landroid/content/ComponentName;

    goto :goto_0
.end method

.method public final a(Landroid/app/PendingIntent;)V
    .locals 3

    .prologue
    .line 264
    iget-object v0, p0, Lcom/google/android/location/internal/f;->a:Lcom/google/android/location/internal/e;

    iget-object v1, v0, Lcom/google/android/location/internal/e;->a:Landroid/content/Context;

    invoke-static {v1}, Lcom/google/android/location/internal/e;->a(Landroid/content/Context;)V

    invoke-static {}, Lcom/google/android/gms/common/util/ay;->e()I

    move-result v1

    const/16 v2, 0xa

    if-ne v1, v2, :cond_0

    invoke-virtual {v0}, Lcom/google/android/location/internal/e;->e()Z

    move-result v1

    if-nez v1, :cond_0

    const-string v0, "GLMSImpl"

    const-string v1, "Activity recognition is not implemented on Android Wear yet."

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 265
    :goto_0
    return-void

    .line 264
    :cond_0
    new-instance v1, Lcom/google/android/location/internal/j;

    invoke-direct {v1}, Lcom/google/android/location/internal/j;-><init>()V

    invoke-virtual {v1, p1}, Lcom/google/android/location/internal/j;->a(Landroid/app/PendingIntent;)Lcom/google/android/location/internal/j;

    move-result-object v1

    iget-object v0, v0, Lcom/google/android/location/internal/e;->a:Landroid/content/Context;

    invoke-virtual {v1, v0}, Lcom/google/android/location/internal/j;->a(Landroid/content/Context;)Landroid/content/ComponentName;

    goto :goto_0
.end method

.method public final a(Landroid/app/PendingIntent;Lcom/google/android/gms/location/internal/n;Ljava/lang/String;)V
    .locals 7

    .prologue
    .line 72
    iget-object v0, p0, Lcom/google/android/location/internal/f;->a:Lcom/google/android/location/internal/e;

    :try_start_0
    invoke-static {p1, p3}, Lcom/google/android/location/internal/e;->a(Landroid/app/PendingIntent;Ljava/lang/String;)V

    invoke-virtual {v0, p3}, Lcom/google/android/location/internal/e;->b(Ljava/lang/String;)V

    iget-object v6, v0, Lcom/google/android/location/internal/e;->c:Lcom/google/android/location/geofencer/service/g;

    new-instance v5, Lcom/google/android/location/geofencer/service/am;

    invoke-direct {v5, p2}, Lcom/google/android/location/geofencer/service/am;-><init>(Lcom/google/android/gms/location/internal/n;)V

    const-string v0, "PendingIntent not specified."

    invoke-static {p1, v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "Package name not specified."

    invoke-static {p3, v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v0, Lcom/google/android/location/geofencer/service/an;

    const/4 v1, 0x3

    const/4 v3, 0x0

    const/4 v4, 0x0

    move-object v2, p1

    invoke-direct/range {v0 .. v5}, Lcom/google/android/location/geofencer/service/an;-><init>(ILandroid/app/PendingIntent;[Ljava/lang/String;Ljava/lang/String;Lcom/google/android/location/geofencer/service/f;)V

    iget-object v1, v6, Lcom/google/android/location/geofencer/service/g;->a:Lcom/google/android/location/geofencer/service/k;

    invoke-virtual {v1, v0}, Lcom/google/android/location/geofencer/service/k;->a(Lcom/google/android/location/geofencer/service/an;)V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception v0

    const-string v1, "GLMSImpl"

    const-string v2, "original removeGeofencesByPendingIntent() exception (before parcelling)"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    throw v0
.end method

.method public final a(Landroid/location/Location;)V
    .locals 1

    .prologue
    .line 152
    iget-object v0, p0, Lcom/google/android/location/internal/f;->a:Lcom/google/android/location/internal/e;

    invoke-virtual {v0}, Lcom/google/android/location/internal/e;->d()V

    iget-object v0, v0, Lcom/google/android/location/internal/e;->b:Lcom/google/android/location/fused/g;

    invoke-virtual {v0, p1}, Lcom/google/android/location/fused/g;->a(Landroid/location/Location;)V

    .line 153
    return-void
.end method

.method public final a(Landroid/location/Location;I)V
    .locals 2

    .prologue
    .line 157
    iget-object v0, p0, Lcom/google/android/location/internal/f;->a:Lcom/google/android/location/internal/e;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/google/android/location/internal/e;->a(I)V

    invoke-virtual {v0}, Lcom/google/android/location/internal/e;->e()Z

    move-result v1

    if-nez v1, :cond_0

    new-instance v0, Ljava/lang/SecurityException;

    const-string v1, "Client must be signed by Google to use injection API"

    invoke-direct {v0, v1}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget-object v0, v0, Lcom/google/android/location/internal/e;->b:Lcom/google/android/location/fused/g;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/location/fused/g;->a(Landroid/location/Location;I)V

    .line 158
    return-void
.end method

.method public final a(Lcom/google/android/gms/location/GeofencingRequest;Landroid/app/PendingIntent;Lcom/google/android/gms/location/internal/n;)V
    .locals 5

    .prologue
    .line 66
    iget-object v0, p0, Lcom/google/android/location/internal/f;->a:Lcom/google/android/location/internal/e;

    iget-object v1, p0, Lcom/google/android/location/internal/f;->b:Ljava/lang/String;

    :try_start_0
    invoke-static {p2, v1}, Lcom/google/android/location/internal/e;->a(Landroid/app/PendingIntent;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/google/android/location/internal/e;->b(Ljava/lang/String;)V

    const-string v2, "GLMSImpl"

    const/4 v3, 0x3

    invoke-static {v2, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_0

    const-string v2, "GLMSImpl"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "addingGeofences from "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v0, v0, Lcom/google/android/location/internal/e;->c:Lcom/google/android/location/geofencer/service/g;

    new-instance v2, Lcom/google/android/location/geofencer/service/am;

    invoke-direct {v2, p3}, Lcom/google/android/location/geofencer/service/am;-><init>(Lcom/google/android/gms/location/internal/n;)V

    invoke-virtual {v0, p1, p2, v2, v1}, Lcom/google/android/location/geofencer/service/g;->a(Lcom/google/android/gms/location/GeofencingRequest;Landroid/app/PendingIntent;Lcom/google/android/location/geofencer/service/f;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception v0

    const-string v1, "GLMSImpl"

    const-string v2, "original addGeofence() exception (before parcelling)"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    throw v0
.end method

.method public final a(Lcom/google/android/gms/location/LocationRequest;Landroid/app/PendingIntent;)V
    .locals 2

    .prologue
    .line 126
    iget-object v0, p0, Lcom/google/android/location/internal/f;->a:Lcom/google/android/location/internal/e;

    invoke-static {p1}, Lcom/google/android/gms/location/internal/LocationRequestInternal;->a(Lcom/google/android/gms/location/LocationRequest;)Lcom/google/android/gms/location/internal/LocationRequestInternal;

    move-result-object v1

    invoke-virtual {v0, v1, p2}, Lcom/google/android/location/internal/e;->a(Lcom/google/android/gms/location/internal/LocationRequestInternal;Landroid/app/PendingIntent;)V

    .line 127
    return-void
.end method

.method public final a(Lcom/google/android/gms/location/LocationRequest;Lcom/google/android/gms/location/j;)V
    .locals 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 103
    iget-object v0, p0, Lcom/google/android/location/internal/f;->a:Lcom/google/android/location/internal/e;

    iget-object v1, p0, Lcom/google/android/location/internal/f;->b:Ljava/lang/String;

    invoke-virtual {v0, p1, p2, v1}, Lcom/google/android/location/internal/e;->a(Lcom/google/android/gms/location/LocationRequest;Lcom/google/android/gms/location/j;Ljava/lang/String;)V

    .line 104
    return-void
.end method

.method public final a(Lcom/google/android/gms/location/LocationRequest;Lcom/google/android/gms/location/j;Ljava/lang/String;)V
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 114
    iget-object v0, p0, Lcom/google/android/location/internal/f;->a:Lcom/google/android/location/internal/e;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/location/internal/e;->a(Lcom/google/android/gms/location/LocationRequest;Lcom/google/android/gms/location/j;Ljava/lang/String;)V

    .line 115
    return-void
.end method

.method public final a(Lcom/google/android/gms/location/internal/LocationRequestInternal;Landroid/app/PendingIntent;)V
    .locals 1

    .prologue
    .line 132
    iget-object v0, p0, Lcom/google/android/location/internal/f;->a:Lcom/google/android/location/internal/e;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/location/internal/e;->a(Lcom/google/android/gms/location/internal/LocationRequestInternal;Landroid/app/PendingIntent;)V

    .line 133
    return-void
.end method

.method public final a(Lcom/google/android/gms/location/internal/LocationRequestInternal;Lcom/google/android/gms/location/j;)V
    .locals 2

    .prologue
    .line 120
    iget-object v0, p0, Lcom/google/android/location/internal/f;->a:Lcom/google/android/location/internal/e;

    iget-object v1, p0, Lcom/google/android/location/internal/f;->b:Ljava/lang/String;

    invoke-virtual {v0, p1, p2, v1}, Lcom/google/android/location/internal/e;->a(Lcom/google/android/gms/location/internal/LocationRequestInternal;Lcom/google/android/gms/location/j;Ljava/lang/String;)V

    .line 121
    return-void
.end method

.method public final a(Lcom/google/android/gms/location/internal/n;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 83
    iget-object v0, p0, Lcom/google/android/location/internal/f;->a:Lcom/google/android/location/internal/e;

    :try_start_0
    invoke-virtual {v0, p2}, Lcom/google/android/location/internal/e;->b(Ljava/lang/String;)V

    iget-object v0, v0, Lcom/google/android/location/internal/e;->c:Lcom/google/android/location/geofencer/service/g;

    new-instance v1, Lcom/google/android/location/geofencer/service/am;

    invoke-direct {v1, p1}, Lcom/google/android/location/geofencer/service/am;-><init>(Lcom/google/android/gms/location/internal/n;)V

    invoke-virtual {v0, v1, p2}, Lcom/google/android/location/geofencer/service/g;->a(Lcom/google/android/location/geofencer/service/f;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception v0

    const-string v1, "GLMSImpl"

    const-string v2, "original removeAllGeofences() exception (before parcelling)"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    throw v0
.end method

.method public final a(Lcom/google/android/gms/location/j;)V
    .locals 2

    .prologue
    .line 137
    iget-object v0, p0, Lcom/google/android/location/internal/f;->a:Lcom/google/android/location/internal/e;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/location/internal/e;->a(I)V

    iget-object v0, v0, Lcom/google/android/location/internal/e;->b:Lcom/google/android/location/fused/g;

    invoke-virtual {v0, p1}, Lcom/google/android/location/fused/g;->a(Lcom/google/android/gms/location/j;)V

    .line 138
    return-void
.end method

.method public final a(Lcom/google/android/gms/location/places/NearbyAlertRequest;Lcom/google/android/gms/location/places/internal/PlacesParams;Landroid/app/PendingIntent;)V
    .locals 1

    .prologue
    .line 221
    iget-object v0, p0, Lcom/google/android/location/internal/f;->a:Lcom/google/android/location/internal/e;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/location/internal/e;->a(Lcom/google/android/gms/location/places/NearbyAlertRequest;Lcom/google/android/gms/location/places/internal/PlacesParams;Landroid/app/PendingIntent;)V

    .line 222
    return-void
.end method

.method public final a(Lcom/google/android/gms/location/places/PlaceFilter;Lcom/google/android/gms/location/places/internal/PlacesParams;Lcom/google/android/gms/location/places/internal/d;)V
    .locals 1

    .prologue
    .line 215
    iget-object v0, p0, Lcom/google/android/location/internal/f;->a:Lcom/google/android/location/internal/e;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/location/internal/e;->a(Lcom/google/android/gms/location/places/PlaceFilter;Lcom/google/android/gms/location/places/internal/PlacesParams;Lcom/google/android/gms/location/places/internal/d;)V

    .line 216
    return-void
.end method

.method public final a(Lcom/google/android/gms/location/places/PlaceReport;Lcom/google/android/gms/location/places/internal/PlacesParams;)V
    .locals 1

    .prologue
    .line 252
    iget-object v0, p0, Lcom/google/android/location/internal/f;->a:Lcom/google/android/location/internal/e;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/location/internal/e;->a(Lcom/google/android/gms/location/places/PlaceReport;Lcom/google/android/gms/location/places/internal/PlacesParams;)V

    .line 253
    return-void
.end method

.method public final a(Lcom/google/android/gms/location/places/PlaceRequest;Lcom/google/android/gms/location/places/internal/PlacesParams;Landroid/app/PendingIntent;)V
    .locals 1

    .prologue
    .line 232
    iget-object v0, p0, Lcom/google/android/location/internal/f;->a:Lcom/google/android/location/internal/e;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/location/internal/e;->a(Lcom/google/android/gms/location/places/PlaceRequest;Lcom/google/android/gms/location/places/internal/PlacesParams;Landroid/app/PendingIntent;)V

    .line 233
    return-void
.end method

.method public final a(Lcom/google/android/gms/location/places/UserAddedPlace;Lcom/google/android/gms/location/places/internal/PlacesParams;Lcom/google/android/gms/location/places/internal/d;)V
    .locals 1

    .prologue
    .line 180
    iget-object v0, p0, Lcom/google/android/location/internal/f;->a:Lcom/google/android/location/internal/e;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/location/internal/e;->a(Lcom/google/android/gms/location/places/UserAddedPlace;Lcom/google/android/gms/location/places/internal/PlacesParams;Lcom/google/android/gms/location/places/internal/d;)V

    .line 181
    return-void
.end method

.method public final a(Lcom/google/android/gms/location/places/UserDataType;Lcom/google/android/gms/maps/model/LatLngBounds;Ljava/util/List;Lcom/google/android/gms/location/places/internal/PlacesParams;Lcom/google/android/gms/location/places/internal/d;)V
    .locals 6

    .prologue
    .line 174
    iget-object v0, p0, Lcom/google/android/location/internal/f;->a:Lcom/google/android/location/internal/e;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/location/internal/e;->a(Lcom/google/android/gms/location/places/UserDataType;Lcom/google/android/gms/maps/model/LatLngBounds;Ljava/util/List;Lcom/google/android/gms/location/places/internal/PlacesParams;Lcom/google/android/gms/location/places/internal/d;)V

    .line 175
    return-void
.end method

.method public final a(Lcom/google/android/gms/location/places/internal/PlacesParams;Landroid/app/PendingIntent;)V
    .locals 1

    .prologue
    .line 237
    iget-object v0, p0, Lcom/google/android/location/internal/f;->a:Lcom/google/android/location/internal/e;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/location/internal/e;->b(Lcom/google/android/gms/location/places/internal/PlacesParams;Landroid/app/PendingIntent;)V

    .line 238
    return-void
.end method

.method public final a(Lcom/google/android/gms/maps/model/LatLng;Lcom/google/android/gms/location/places/PlaceFilter;Lcom/google/android/gms/location/places/internal/PlacesParams;Lcom/google/android/gms/location/places/internal/d;)V
    .locals 1

    .prologue
    .line 186
    iget-object v0, p0, Lcom/google/android/location/internal/f;->a:Lcom/google/android/location/internal/e;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/google/android/location/internal/e;->a(Lcom/google/android/gms/maps/model/LatLng;Lcom/google/android/gms/location/places/PlaceFilter;Lcom/google/android/gms/location/places/internal/PlacesParams;Lcom/google/android/gms/location/places/internal/d;)V

    .line 187
    return-void
.end method

.method public final a(Lcom/google/android/gms/maps/model/LatLngBounds;ILcom/google/android/gms/location/places/PlaceFilter;Lcom/google/android/gms/location/places/internal/PlacesParams;Lcom/google/android/gms/location/places/internal/d;)V
    .locals 7

    .prologue
    .line 198
    iget-object v0, p0, Lcom/google/android/location/internal/f;->a:Lcom/google/android/location/internal/e;

    const-string v3, ""

    move-object v1, p1

    move v2, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/location/internal/e;->a(Lcom/google/android/gms/maps/model/LatLngBounds;ILjava/lang/String;Lcom/google/android/gms/location/places/PlaceFilter;Lcom/google/android/gms/location/places/internal/PlacesParams;Lcom/google/android/gms/location/places/internal/d;)V

    .line 199
    return-void
.end method

.method public final a(Lcom/google/android/gms/maps/model/LatLngBounds;ILjava/lang/String;Lcom/google/android/gms/location/places/PlaceFilter;Lcom/google/android/gms/location/places/internal/PlacesParams;Lcom/google/android/gms/location/places/internal/d;)V
    .locals 7

    .prologue
    .line 204
    iget-object v0, p0, Lcom/google/android/location/internal/f;->a:Lcom/google/android/location/internal/e;

    move-object v1, p1

    move v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object v6, p6

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/location/internal/e;->a(Lcom/google/android/gms/maps/model/LatLngBounds;ILjava/lang/String;Lcom/google/android/gms/location/places/PlaceFilter;Lcom/google/android/gms/location/places/internal/PlacesParams;Lcom/google/android/gms/location/places/internal/d;)V

    .line 205
    return-void
.end method

.method public final a(Ljava/lang/String;Lcom/google/android/gms/location/places/internal/PlacesParams;Lcom/google/android/gms/location/places/internal/d;)V
    .locals 1

    .prologue
    .line 210
    iget-object v0, p0, Lcom/google/android/location/internal/f;->a:Lcom/google/android/location/internal/e;

    invoke-virtual {v0, p2}, Lcom/google/android/location/internal/e;->a(Lcom/google/android/gms/location/places/internal/PlacesParams;)V

    .line 211
    return-void
.end method

.method public final a(Ljava/lang/String;Lcom/google/android/gms/maps/model/LatLngBounds;Lcom/google/android/gms/location/places/AutocompleteFilter;Lcom/google/android/gms/location/places/internal/PlacesParams;Lcom/google/android/gms/location/places/internal/d;)V
    .locals 6

    .prologue
    .line 192
    iget-object v0, p0, Lcom/google/android/location/internal/f;->a:Lcom/google/android/location/internal/e;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/location/internal/e;->a(Ljava/lang/String;Lcom/google/android/gms/maps/model/LatLngBounds;Lcom/google/android/gms/location/places/AutocompleteFilter;Lcom/google/android/gms/location/places/internal/PlacesParams;Lcom/google/android/gms/location/places/internal/d;)V

    .line 193
    return-void
.end method

.method public final a(Ljava/util/List;Landroid/app/PendingIntent;Lcom/google/android/gms/location/internal/n;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 55
    new-instance v0, Lcom/google/android/gms/location/h;

    invoke-direct {v0}, Lcom/google/android/gms/location/h;-><init>()V

    .line 56
    invoke-virtual {v0, p1}, Lcom/google/android/gms/location/h;->a(Ljava/util/List;)Lcom/google/android/gms/location/h;

    .line 59
    const/4 v1, 0x5

    iput v1, v0, Lcom/google/android/gms/location/h;->a:I

    .line 60
    invoke-virtual {v0}, Lcom/google/android/gms/location/h;->a()Lcom/google/android/gms/location/GeofencingRequest;

    move-result-object v0

    invoke-virtual {p0, v0, p2, p3}, Lcom/google/android/location/internal/f;->a(Lcom/google/android/gms/location/GeofencingRequest;Landroid/app/PendingIntent;Lcom/google/android/gms/location/internal/n;)V

    .line 61
    return-void
.end method

.method public final a(Ljava/util/List;Lcom/google/android/gms/location/places/internal/PlacesParams;Lcom/google/android/gms/location/places/internal/d;)V
    .locals 1

    .prologue
    .line 168
    iget-object v0, p0, Lcom/google/android/location/internal/f;->a:Lcom/google/android/location/internal/e;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/location/internal/e;->a(Ljava/util/List;Lcom/google/android/gms/location/places/internal/PlacesParams;Lcom/google/android/gms/location/places/internal/d;)V

    .line 169
    return-void
.end method

.method public final a(Z)V
    .locals 1

    .prologue
    .line 147
    iget-object v0, p0, Lcom/google/android/location/internal/f;->a:Lcom/google/android/location/internal/e;

    invoke-virtual {v0}, Lcom/google/android/location/internal/e;->d()V

    iget-object v0, v0, Lcom/google/android/location/internal/e;->b:Lcom/google/android/location/fused/g;

    invoke-virtual {v0, p1}, Lcom/google/android/location/fused/g;->a(Z)V

    .line 148
    return-void
.end method

.method public final a([Ljava/lang/String;Lcom/google/android/gms/location/internal/n;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 78
    iget-object v0, p0, Lcom/google/android/location/internal/f;->a:Lcom/google/android/location/internal/e;

    :try_start_0
    invoke-virtual {v0, p3}, Lcom/google/android/location/internal/e;->b(Ljava/lang/String;)V

    iget-object v0, v0, Lcom/google/android/location/internal/e;->c:Lcom/google/android/location/geofencer/service/g;

    new-instance v1, Lcom/google/android/location/geofencer/service/am;

    invoke-direct {v1, p2}, Lcom/google/android/location/geofencer/service/am;-><init>(Lcom/google/android/gms/location/internal/n;)V

    invoke-virtual {v0, p1, v1, p3}, Lcom/google/android/location/geofencer/service/g;->a([Ljava/lang/String;Lcom/google/android/location/geofencer/service/f;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception v0

    const-string v1, "GLMSImpl"

    const-string v2, "original removeGeofencesByRequestIds() exception (before parcelling)"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    throw v0
.end method

.method public final b()Landroid/os/IBinder;
    .locals 2

    .prologue
    .line 242
    iget-object v0, p0, Lcom/google/android/location/internal/f;->a:Lcom/google/android/location/internal/e;

    invoke-virtual {v0}, Lcom/google/android/location/internal/e;->e()Z

    move-result v1

    if-nez v1, :cond_0

    new-instance v0, Ljava/lang/SecurityException;

    invoke-direct {v0}, Ljava/lang/SecurityException;-><init>()V

    throw v0

    :cond_0
    invoke-virtual {v0}, Lcom/google/android/location/internal/e;->b()Lcom/google/android/location/copresence/m/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/location/copresence/m/b;->asBinder()Landroid/os/IBinder;

    move-result-object v0

    return-object v0
.end method

.method public final b(Ljava/lang/String;)Lcom/google/android/gms/location/LocationStatus;
    .locals 2

    .prologue
    .line 88
    iget-object v0, p0, Lcom/google/android/location/internal/f;->a:Lcom/google/android/location/internal/e;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/location/internal/e;->a(I)V

    invoke-virtual {v0}, Lcom/google/android/location/internal/e;->e()Z

    move-result v1

    if-nez v1, :cond_0

    new-instance v0, Ljava/lang/SecurityException;

    const-string v1, "Client must be signed by Google to use status API"

    invoke-direct {v0, v1}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget-object v0, v0, Lcom/google/android/location/internal/e;->b:Lcom/google/android/location/fused/g;

    invoke-virtual {v0}, Lcom/google/android/location/fused/g;->d()Lcom/google/android/gms/location/LocationStatus;

    move-result-object v0

    return-object v0
.end method

.method public final b(Landroid/app/PendingIntent;)V
    .locals 2

    .prologue
    .line 142
    iget-object v0, p0, Lcom/google/android/location/internal/f;->a:Lcom/google/android/location/internal/e;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/location/internal/e;->a(I)V

    iget-object v0, v0, Lcom/google/android/location/internal/e;->b:Lcom/google/android/location/fused/g;

    invoke-virtual {v0, p1}, Lcom/google/android/location/fused/g;->b(Landroid/app/PendingIntent;)V

    .line 143
    return-void
.end method

.method public final b(Lcom/google/android/gms/location/places/internal/PlacesParams;Landroid/app/PendingIntent;)V
    .locals 1

    .prologue
    .line 226
    iget-object v0, p0, Lcom/google/android/location/internal/f;->a:Lcom/google/android/location/internal/e;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/location/internal/e;->a(Lcom/google/android/gms/location/places/internal/PlacesParams;Landroid/app/PendingIntent;)V

    .line 227
    return-void
.end method

.method public final b(Ljava/lang/String;Lcom/google/android/gms/location/places/internal/PlacesParams;Lcom/google/android/gms/location/places/internal/d;)V
    .locals 1

    .prologue
    .line 162
    iget-object v0, p0, Lcom/google/android/location/internal/f;->a:Lcom/google/android/location/internal/e;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/location/internal/e;->a(Ljava/lang/String;Lcom/google/android/gms/location/places/internal/PlacesParams;Lcom/google/android/gms/location/places/internal/d;)V

    .line 163
    return-void
.end method

.method public final c()Landroid/os/IBinder;
    .locals 2

    .prologue
    .line 247
    iget-object v0, p0, Lcom/google/android/location/internal/f;->a:Lcom/google/android/location/internal/e;

    invoke-virtual {v0}, Lcom/google/android/location/internal/e;->c()Lcom/google/android/gms/audiomodem/b/a;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/google/android/location/internal/e;->e()Z

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    new-instance v0, Ljava/lang/SecurityException;

    invoke-direct {v0}, Ljava/lang/SecurityException;-><init>()V

    throw v0

    :cond_1
    invoke-virtual {v0}, Lcom/google/android/location/internal/e;->c()Lcom/google/android/gms/audiomodem/b/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/audiomodem/b/a;->asBinder()Landroid/os/IBinder;

    move-result-object v0

    return-object v0
.end method
