.class public Lcom/google/android/location/places/ui/PlacePickerActivity;
.super Landroid/support/v4/app/q;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/location/places/ui/am;
.implements Lcom/google/android/location/places/ui/j;


# instance fields
.field private a:Lcom/google/android/location/places/ui/s;

.field private b:Lcom/google/android/location/places/ui/r;

.field private c:Lcom/google/android/location/places/ui/av;

.field private d:Lcom/google/android/location/places/ui/MarkerMapFragment;

.field private e:Lcom/google/android/location/places/ui/ac;

.field private f:Lcom/google/android/location/places/ui/a;

.field private g:Landroid/view/View;

.field private h:Landroid/view/ViewGroup;

.field private i:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0}, Landroid/support/v4/app/q;-><init>()V

    return-void
.end method

.method static synthetic a(Lcom/google/android/location/places/ui/PlacePickerActivity;)V
    .locals 0

    .prologue
    .line 29
    invoke-super {p0}, Landroid/app/Activity;->finish()V

    return-void
.end method

.method private f()V
    .locals 1

    .prologue
    .line 265
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lcom/google/android/location/places/ui/PlacePickerActivity;->setResult(I)V

    .line 266
    invoke-virtual {p0}, Lcom/google/android/location/places/ui/PlacePickerActivity;->finish()V

    .line 267
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 256
    iget-object v0, p0, Lcom/google/android/location/places/ui/PlacePickerActivity;->a:Lcom/google/android/location/places/ui/s;

    iget v0, v0, Lcom/google/android/location/places/ui/s;->e:I

    if-nez v0, :cond_0

    .line 257
    invoke-virtual {p0}, Lcom/google/android/location/places/ui/PlacePickerActivity;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/v;->c()V

    .line 258
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/location/places/ui/PlacePickerActivity;->i:Z

    .line 262
    :goto_0
    return-void

    .line 260
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/location/places/ui/PlacePickerActivity;->finish()V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/location/places/f;)V
    .locals 1

    .prologue
    .line 246
    new-instance v0, Lcom/google/android/location/places/ui/au;

    invoke-direct {v0, p0, p1}, Lcom/google/android/location/places/ui/au;-><init>(Lcom/google/android/location/places/ui/PlacePickerActivity;Lcom/google/android/gms/location/places/f;)V

    invoke-virtual {p0, v0}, Lcom/google/android/location/places/ui/PlacePickerActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 252
    return-void
.end method

.method public final a(Lcom/google/android/gms/location/places/f;IILjava/lang/String;)V
    .locals 8

    .prologue
    const/4 v7, 0x0

    .line 223
    iget-object v4, p0, Lcom/google/android/location/places/ui/PlacePickerActivity;->a:Lcom/google/android/location/places/ui/s;

    iget-object v0, p0, Lcom/google/android/location/places/ui/PlacePickerActivity;->d:Lcom/google/android/location/places/ui/MarkerMapFragment;

    invoke-virtual {v0}, Lcom/google/android/location/places/ui/MarkerMapFragment;->g()Lcom/google/android/gms/maps/model/LatLngBounds;

    move-result-object v5

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move v3, p3

    move-object v6, p4

    invoke-static/range {v0 .. v6}, Lcom/google/android/location/places/ui/bd;->a(Landroid/app/Activity;Lcom/google/android/gms/location/places/f;IILcom/google/android/location/places/ui/s;Lcom/google/android/gms/maps/model/LatLngBounds;Ljava/lang/String;)V

    .line 225
    const/4 v1, -0x1

    iget-object v2, p0, Lcom/google/android/location/places/ui/PlacePickerActivity;->a:Lcom/google/android/location/places/ui/s;

    iget-object v0, p0, Lcom/google/android/location/places/ui/PlacePickerActivity;->c:Lcom/google/android/location/places/ui/av;

    invoke-virtual {v0}, Lcom/google/android/location/places/ui/av;->b()Ljava/lang/String;

    move-result-object v0

    iget-object v3, p0, Lcom/google/android/location/places/ui/PlacePickerActivity;->d:Lcom/google/android/location/places/ui/MarkerMapFragment;

    invoke-virtual {v3}, Lcom/google/android/location/places/ui/MarkerMapFragment;->g()Lcom/google/android/gms/maps/model/LatLngBounds;

    move-result-object v3

    check-cast p1, Lcom/google/android/gms/location/places/internal/PlaceImpl;

    iget-object v4, v2, Lcom/google/android/location/places/ui/s;->p:Landroid/content/Intent;

    const-string v5, "selected_place"

    invoke-static {p1, v4, v5}, Lcom/google/android/gms/common/internal/safeparcel/d;->a(Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;Landroid/content/Intent;Ljava/lang/String;)V

    iget-object v4, v2, Lcom/google/android/location/places/ui/s;->p:Landroid/content/Intent;

    const-string v5, "third_party_attributions"

    invoke-virtual {v4, v5, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    if-eqz v3, :cond_0

    iget-object v0, v2, Lcom/google/android/location/places/ui/s;->p:Landroid/content/Intent;

    const-string v4, "final_latlng_bounds"

    invoke-static {v3, v0, v4}, Lcom/google/android/gms/common/internal/safeparcel/d;->a(Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;Landroid/content/Intent;Ljava/lang/String;)V

    :cond_0
    iget-object v0, v2, Lcom/google/android/location/places/ui/s;->q:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v3

    invoke-virtual {v0, v3}, Landroid/content/pm/PackageManager;->getPackagesForUid(I)[Ljava/lang/String;

    move-result-object v0

    array-length v3, v0

    if-lez v3, :cond_2

    iget-object v3, v2, Lcom/google/android/location/places/ui/s;->q:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    aget-object v0, v0, v7

    invoke-static {v3, v0}, Lcom/google/android/gms/common/ew;->b(Landroid/content/pm/PackageManager;Ljava/lang/String;)Z

    move-result v0

    :goto_0
    if-eqz v0, :cond_1

    iget-boolean v0, v2, Lcom/google/android/location/places/ui/s;->k:Z

    if-eqz v0, :cond_1

    iget-object v0, v2, Lcom/google/android/location/places/ui/s;->p:Landroid/content/Intent;

    const-string v3, "selection_type"

    invoke-virtual {v0, v3, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    iget-object v0, v2, Lcom/google/android/location/places/ui/s;->p:Landroid/content/Intent;

    const-string v3, "selection_index"

    invoke-virtual {v0, v3, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    :cond_1
    iget-object v0, v2, Lcom/google/android/location/places/ui/s;->p:Landroid/content/Intent;

    invoke-virtual {p0, v1, v0}, Lcom/google/android/location/places/ui/PlacePickerActivity;->setResult(ILandroid/content/Intent;)V

    .line 227
    invoke-virtual {p0}, Lcom/google/android/location/places/ui/PlacePickerActivity;->finish()V

    .line 228
    return-void

    :cond_2
    move v0, v7

    .line 225
    goto :goto_0
.end method

.method public final b()V
    .locals 4

    .prologue
    .line 232
    iget-object v0, p0, Lcom/google/android/location/places/ui/PlacePickerActivity;->f:Lcom/google/android/location/places/ui/a;

    if-nez v0, :cond_0

    .line 233
    invoke-static {}, Lcom/google/android/location/places/ui/a;->a()Lcom/google/android/location/places/ui/a;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/places/ui/PlacePickerActivity;->f:Lcom/google/android/location/places/ui/a;

    .line 236
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/location/places/ui/PlacePickerActivity;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/v;->a()Landroid/support/v4/app/aj;

    move-result-object v0

    sget v1, Lcom/google/android/gms/j;->oi:I

    iget-object v2, p0, Lcom/google/android/location/places/ui/PlacePickerActivity;->f:Lcom/google/android/location/places/ui/a;

    const-string v3, "add_a_place_fragment"

    invoke-virtual {v0, v1, v2, v3}, Landroid/support/v4/app/aj;->b(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/aj;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v4/app/aj;->a(Ljava/lang/String;)Landroid/support/v4/app/aj;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/aj;->a()I

    .line 241
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/location/places/ui/PlacePickerActivity;->i:Z

    .line 242
    return-void
.end method

.method public final c()Lcom/google/android/location/places/ui/s;
    .locals 1

    .prologue
    .line 203
    iget-object v0, p0, Lcom/google/android/location/places/ui/PlacePickerActivity;->a:Lcom/google/android/location/places/ui/s;

    return-object v0
.end method

.method public final d()Lcom/google/android/location/places/ui/r;
    .locals 1

    .prologue
    .line 207
    iget-object v0, p0, Lcom/google/android/location/places/ui/PlacePickerActivity;->b:Lcom/google/android/location/places/ui/r;

    return-object v0
.end method

.method public final e()Lcom/google/android/location/places/ui/av;
    .locals 1

    .prologue
    .line 211
    iget-object v0, p0, Lcom/google/android/location/places/ui/PlacePickerActivity;->c:Lcom/google/android/location/places/ui/av;

    return-object v0
.end method

.method public finish()V
    .locals 2

    .prologue
    .line 174
    iget-object v0, p0, Lcom/google/android/location/places/ui/PlacePickerActivity;->g:Landroid/view/View;

    if-nez v0, :cond_0

    .line 175
    invoke-super {p0}, Landroid/support/v4/app/q;->finish()V

    .line 200
    :goto_0
    return-void

    .line 180
    :cond_0
    iget-object v0, p0, Lcom/google/android/location/places/ui/PlacePickerActivity;->g:Landroid/view/View;

    sget v1, Lcom/google/android/gms/b;->d:I

    invoke-static {p0, v1}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 182
    sget v0, Lcom/google/android/gms/b;->o:I

    invoke-static {p0, v0}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    .line 184
    new-instance v1, Lcom/google/android/location/places/ui/at;

    invoke-direct {v1, p0}, Lcom/google/android/location/places/ui/at;-><init>(Lcom/google/android/location/places/ui/PlacePickerActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 199
    iget-object v1, p0, Lcom/google/android/location/places/ui/PlacePickerActivity;->h:Landroid/view/ViewGroup;

    invoke-virtual {v1, v0}, Landroid/view/ViewGroup;->startAnimation(Landroid/view/animation/Animation;)V

    goto :goto_0
.end method

.method public onBackPressed()V
    .locals 1

    .prologue
    .line 63
    iget-boolean v0, p0, Lcom/google/android/location/places/ui/PlacePickerActivity;->i:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/location/places/ui/PlacePickerActivity;->f:Lcom/google/android/location/places/ui/a;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/location/places/ui/PlacePickerActivity;->f:Lcom/google/android/location/places/ui/a;

    invoke-virtual {v0}, Lcom/google/android/location/places/ui/a;->isAdded()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 64
    iget-object v0, p0, Lcom/google/android/location/places/ui/PlacePickerActivity;->f:Lcom/google/android/location/places/ui/a;

    invoke-virtual {v0}, Lcom/google/android/location/places/ui/a;->m()V

    .line 68
    :goto_0
    return-void

    .line 66
    :cond_0
    invoke-super {p0}, Landroid/support/v4/app/q;->onBackPressed()V

    goto :goto_0
.end method

.method public onClickOutsideDialog(Landroid/view/View;)V
    .locals 7

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x0

    .line 215
    iget-object v4, p0, Lcom/google/android/location/places/ui/PlacePickerActivity;->a:Lcom/google/android/location/places/ui/s;

    iget-object v0, p0, Lcom/google/android/location/places/ui/PlacePickerActivity;->d:Lcom/google/android/location/places/ui/MarkerMapFragment;

    invoke-virtual {v0}, Lcom/google/android/location/places/ui/MarkerMapFragment;->g()Lcom/google/android/gms/maps/model/LatLngBounds;

    move-result-object v5

    move-object v0, p0

    move v3, v2

    move-object v6, v1

    invoke-static/range {v0 .. v6}, Lcom/google/android/location/places/ui/bd;->a(Landroid/app/Activity;Lcom/google/android/gms/location/places/f;IILcom/google/android/location/places/ui/s;Lcom/google/android/gms/maps/model/LatLngBounds;Ljava/lang/String;)V

    .line 217
    invoke-virtual {p0}, Lcom/google/android/location/places/ui/PlacePickerActivity;->finish()V

    .line 218
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 72
    invoke-super {p0, p1}, Landroid/support/v4/app/q;->onCreate(Landroid/os/Bundle;)V

    .line 75
    invoke-virtual {p0}, Lcom/google/android/location/places/ui/PlacePickerActivity;->getCallingActivity()Landroid/content/ComponentName;

    move-result-object v0

    .line 76
    if-nez v0, :cond_0

    .line 77
    const-string v0, "PlacePicker"

    const-string v1, "Cannot find caller. Did you forget to use startActivityForResult?"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 78
    invoke-direct {p0}, Lcom/google/android/location/places/ui/PlacePickerActivity;->f()V

    .line 162
    :goto_0
    return-void

    .line 80
    :cond_0
    invoke-virtual {v0}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_1

    .line 81
    const-string v0, "PlacePicker"

    const-string v1, "Cannot find caller\'s package name."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 82
    invoke-direct {p0}, Lcom/google/android/location/places/ui/PlacePickerActivity;->f()V

    goto :goto_0

    .line 87
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/location/places/ui/PlacePickerActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    const-string v2, "android.permission.ACCESS_FINE_LOCATION"

    invoke-virtual {v0}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/pm/PackageManager;->checkPermission(Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    const/4 v2, -0x1

    if-ne v1, v2, :cond_2

    .line 90
    const-string v0, "PlacePicker"

    const-string v1, "Missing required permission: android.permission.ACCESS_FINE_LOCATION"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 92
    invoke-direct {p0}, Lcom/google/android/location/places/ui/PlacePickerActivity;->f()V

    goto :goto_0

    .line 97
    :cond_2
    new-instance v1, Lcom/google/android/location/places/ui/s;

    invoke-virtual {p0}, Lcom/google/android/location/places/ui/PlacePickerActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    invoke-direct {v1, p0, v2, v0}, Lcom/google/android/location/places/ui/s;-><init>(Landroid/content/Context;Landroid/content/Intent;Landroid/content/ComponentName;)V

    iput-object v1, p0, Lcom/google/android/location/places/ui/PlacePickerActivity;->a:Lcom/google/android/location/places/ui/s;

    .line 99
    sget v0, Lcom/google/android/gms/l;->da:I

    invoke-virtual {p0, v0}, Lcom/google/android/location/places/ui/PlacePickerActivity;->setContentView(I)V

    .line 100
    const v0, 0x1020002

    invoke-virtual {p0, v0}, Lcom/google/android/location/places/ui/PlacePickerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/places/ui/PlacePickerActivity;->g:Landroid/view/View;

    .line 101
    iget-object v0, p0, Lcom/google/android/location/places/ui/PlacePickerActivity;->g:Landroid/view/View;

    sget v1, Lcom/google/android/gms/j;->om:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/google/android/location/places/ui/PlacePickerActivity;->h:Landroid/view/ViewGroup;

    .line 104
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    .line 105
    invoke-virtual {p0}, Lcom/google/android/location/places/ui/PlacePickerActivity;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/view/View;->getWindowVisibleDisplayFrame(Landroid/graphics/Rect;)V

    .line 106
    iget-object v1, p0, Lcom/google/android/location/places/ui/PlacePickerActivity;->h:Landroid/view/ViewGroup;

    invoke-virtual {v1}, Landroid/view/ViewGroup;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    .line 107
    invoke-virtual {p0}, Lcom/google/android/location/places/ui/PlacePickerActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/google/android/gms/i;->a:I

    invoke-virtual {v2, v3, v4, v4}, Landroid/content/res/Resources;->getFraction(III)F

    move-result v2

    .line 108
    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v3

    int-to-float v3, v3

    mul-float/2addr v3, v2

    float-to-int v3, v3

    iput v3, v1, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 109
    invoke-virtual {v0}, Landroid/graphics/Rect;->height()I

    move-result v0

    int-to-float v0, v0

    mul-float/2addr v0, v2

    float-to-int v0, v0

    iput v0, v1, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 112
    new-instance v0, Lcom/google/android/location/places/ui/r;

    invoke-direct {v0, p0}, Lcom/google/android/location/places/ui/r;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/location/places/ui/PlacePickerActivity;->b:Lcom/google/android/location/places/ui/r;

    .line 113
    new-instance v0, Lcom/google/android/location/places/ui/av;

    iget-object v1, p0, Lcom/google/android/location/places/ui/PlacePickerActivity;->a:Lcom/google/android/location/places/ui/s;

    invoke-direct {v0, p0, v1}, Lcom/google/android/location/places/ui/av;-><init>(Landroid/content/Context;Lcom/google/android/location/places/ui/s;)V

    iput-object v0, p0, Lcom/google/android/location/places/ui/PlacePickerActivity;->c:Lcom/google/android/location/places/ui/av;

    .line 114
    iget-object v0, p0, Lcom/google/android/location/places/ui/PlacePickerActivity;->c:Lcom/google/android/location/places/ui/av;

    invoke-virtual {v0}, Lcom/google/android/location/places/ui/av;->c()V

    .line 116
    invoke-virtual {p0}, Lcom/google/android/location/places/ui/PlacePickerActivity;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v0

    sget v1, Lcom/google/android/gms/j;->os:I

    invoke-virtual {v0, v1}, Landroid/support/v4/app/v;->a(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/places/ui/MarkerMapFragment;

    iput-object v0, p0, Lcom/google/android/location/places/ui/PlacePickerActivity;->d:Lcom/google/android/location/places/ui/MarkerMapFragment;

    .line 118
    iget-object v0, p0, Lcom/google/android/location/places/ui/PlacePickerActivity;->d:Lcom/google/android/location/places/ui/MarkerMapFragment;

    iget-object v1, p0, Lcom/google/android/location/places/ui/PlacePickerActivity;->b:Lcom/google/android/location/places/ui/r;

    invoke-virtual {v0, v1}, Lcom/google/android/location/places/ui/MarkerMapFragment;->a(Lcom/google/android/location/places/ui/r;)Lcom/google/android/location/places/ui/MarkerMapFragment;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/location/places/ui/PlacePickerActivity;->a:Lcom/google/android/location/places/ui/s;

    iget-boolean v1, v1, Lcom/google/android/location/places/ui/s;->i:Z

    invoke-virtual {v0, v1}, Lcom/google/android/location/places/ui/MarkerMapFragment;->a(Z)Lcom/google/android/location/places/ui/MarkerMapFragment;

    .line 121
    if-eqz p1, :cond_3

    .line 122
    const-string v0, "ADDING_PLACE"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/location/places/ui/PlacePickerActivity;->i:Z

    .line 123
    invoke-virtual {p0}, Lcom/google/android/location/places/ui/PlacePickerActivity;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v0

    const-string v1, "add_a_place_fragment"

    invoke-virtual {v0, v1}, Landroid/support/v4/app/v;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/places/ui/a;

    iput-object v0, p0, Lcom/google/android/location/places/ui/PlacePickerActivity;->f:Lcom/google/android/location/places/ui/a;

    .line 125
    invoke-virtual {p0}, Lcom/google/android/location/places/ui/PlacePickerActivity;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v0

    const-string v1, "pick_a_place_fragment"

    invoke-virtual {v0, v1}, Landroid/support/v4/app/v;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/places/ui/ac;

    iput-object v0, p0, Lcom/google/android/location/places/ui/PlacePickerActivity;->e:Lcom/google/android/location/places/ui/ac;

    .line 129
    :cond_3
    iget-object v0, p0, Lcom/google/android/location/places/ui/PlacePickerActivity;->a:Lcom/google/android/location/places/ui/s;

    iget v0, v0, Lcom/google/android/location/places/ui/s;->e:I

    if-nez v0, :cond_6

    .line 130
    iget-boolean v0, p0, Lcom/google/android/location/places/ui/PlacePickerActivity;->i:Z

    if-eqz v0, :cond_5

    .line 131
    iget-object v0, p0, Lcom/google/android/location/places/ui/PlacePickerActivity;->f:Lcom/google/android/location/places/ui/a;

    if-nez v0, :cond_4

    .line 132
    invoke-static {}, Lcom/google/android/location/places/ui/a;->a()Lcom/google/android/location/places/ui/a;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/places/ui/PlacePickerActivity;->f:Lcom/google/android/location/places/ui/a;

    .line 133
    invoke-virtual {p0}, Lcom/google/android/location/places/ui/PlacePickerActivity;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/v;->a()Landroid/support/v4/app/aj;

    move-result-object v0

    sget v1, Lcom/google/android/gms/j;->oi:I

    iget-object v2, p0, Lcom/google/android/location/places/ui/PlacePickerActivity;->f:Lcom/google/android/location/places/ui/a;

    const-string v3, "add_a_place_fragment"

    invoke-virtual {v0, v1, v2, v3}, Landroid/support/v4/app/aj;->a(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/aj;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/aj;->a()I

    .line 157
    :cond_4
    :goto_1
    new-instance v0, Lcom/google/android/gms/location/a/a;

    invoke-direct {v0}, Lcom/google/android/gms/location/a/a;-><init>()V

    invoke-static {}, Lcom/google/android/gms/location/LocationRequest;->a()Lcom/google/android/gms/location/LocationRequest;

    move-result-object v1

    const/16 v2, 0x64

    invoke-virtual {v1, v2}, Lcom/google/android/gms/location/LocationRequest;->a(I)Lcom/google/android/gms/location/LocationRequest;

    move-result-object v1

    iget-object v2, v0, Lcom/google/android/gms/location/a/a;->a:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 160
    new-instance v1, Landroid/content/Intent;

    const-string v2, "com.google.android.gms.location.settings.CHECK_SETTINGS"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    iget-object v2, v0, Lcom/google/android/gms/location/a/a;->a:Ljava/util/ArrayList;

    const-string v3, "locationRequests"

    invoke-static {v2, v1, v3}, Lcom/google/android/gms/common/internal/safeparcel/d;->a(Ljava/lang/Iterable;Landroid/content/Intent;Ljava/lang/String;)V

    const-string v2, "showDialog"

    iget-boolean v0, v0, Lcom/google/android/gms/location/a/a;->b:Z

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const-string v0, "com.google.android.gms"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 161
    const/4 v0, 0x0

    invoke-virtual {p0, v1, v0}, Lcom/google/android/location/places/ui/PlacePickerActivity;->startActivityForResult(Landroid/content/Intent;I)V

    goto/16 :goto_0

    .line 139
    :cond_5
    iget-object v0, p0, Lcom/google/android/location/places/ui/PlacePickerActivity;->e:Lcom/google/android/location/places/ui/ac;

    if-nez v0, :cond_4

    .line 140
    invoke-static {}, Lcom/google/android/location/places/ui/ac;->j()Lcom/google/android/location/places/ui/ac;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/places/ui/PlacePickerActivity;->e:Lcom/google/android/location/places/ui/ac;

    .line 141
    invoke-virtual {p0}, Lcom/google/android/location/places/ui/PlacePickerActivity;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/v;->a()Landroid/support/v4/app/aj;

    move-result-object v0

    sget v1, Lcom/google/android/gms/j;->oi:I

    iget-object v2, p0, Lcom/google/android/location/places/ui/PlacePickerActivity;->e:Lcom/google/android/location/places/ui/ac;

    const-string v3, "pick_a_place_fragment"

    invoke-virtual {v0, v1, v2, v3}, Landroid/support/v4/app/aj;->a(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/aj;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/aj;->a()I

    goto :goto_1

    .line 148
    :cond_6
    iget-object v0, p0, Lcom/google/android/location/places/ui/PlacePickerActivity;->f:Lcom/google/android/location/places/ui/a;

    if-nez v0, :cond_4

    .line 149
    invoke-static {}, Lcom/google/android/location/places/ui/a;->a()Lcom/google/android/location/places/ui/a;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/places/ui/PlacePickerActivity;->f:Lcom/google/android/location/places/ui/a;

    .line 150
    invoke-virtual {p0}, Lcom/google/android/location/places/ui/PlacePickerActivity;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/v;->a()Landroid/support/v4/app/aj;

    move-result-object v0

    sget v1, Lcom/google/android/gms/j;->oi:I

    iget-object v2, p0, Lcom/google/android/location/places/ui/PlacePickerActivity;->f:Lcom/google/android/location/places/ui/a;

    const-string v3, "add_a_place_fragment"

    invoke-virtual {v0, v1, v2, v3}, Landroid/support/v4/app/aj;->a(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/aj;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/aj;->a()I

    goto :goto_1
.end method

.method protected onDestroy()V
    .locals 1

    .prologue
    .line 166
    iget-object v0, p0, Lcom/google/android/location/places/ui/PlacePickerActivity;->c:Lcom/google/android/location/places/ui/av;

    if-eqz v0, :cond_0

    .line 167
    iget-object v0, p0, Lcom/google/android/location/places/ui/PlacePickerActivity;->c:Lcom/google/android/location/places/ui/av;

    invoke-virtual {v0}, Lcom/google/android/location/places/ui/av;->d()V

    .line 169
    :cond_0
    invoke-super {p0}, Landroid/support/v4/app/q;->onDestroy()V

    .line 170
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 57
    invoke-super {p0, p1}, Landroid/support/v4/app/q;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 58
    const-string v0, "ADDING_PLACE"

    iget-boolean v1, p0, Lcom/google/android/location/places/ui/PlacePickerActivity;->i:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 59
    return-void
.end method
