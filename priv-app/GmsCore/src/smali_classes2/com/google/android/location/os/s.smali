.class public final Lcom/google/android/location/os/s;
.super Lcom/google/android/location/os/as;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/location/e/ag;

.field final synthetic b:Lcom/google/android/location/os/j;


# direct methods
.method public constructor <init>(Lcom/google/android/location/os/j;Lcom/google/android/location/os/au;JLcom/google/android/location/e/ag;)V
    .locals 1

    .prologue
    .line 438
    iput-object p1, p0, Lcom/google/android/location/os/s;->b:Lcom/google/android/location/os/j;

    iput-object p5, p0, Lcom/google/android/location/os/s;->a:Lcom/google/android/location/e/ag;

    invoke-direct {p0, p2, p3, p4}, Lcom/google/android/location/os/as;-><init>(Lcom/google/android/location/os/au;J)V

    return-void
.end method


# virtual methods
.method protected final a(Ljava/io/PrintWriter;)V
    .locals 3

    .prologue
    .line 441
    iget-object v0, p0, Lcom/google/android/location/os/s;->a:Lcom/google/android/location/e/ag;

    const-string v1, "NetworkLocation [\n bestResult="

    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-object v1, v0, Lcom/google/android/location/e/ag;->a:Lcom/google/android/location/e/z;

    if-nez v1, :cond_1

    const-string v1, "null"

    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    :cond_0
    :goto_0
    const-string v1, "\n wifiResult="

    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-object v1, v0, Lcom/google/android/location/e/ag;->b:Lcom/google/android/location/e/bf;

    invoke-static {p1, v1}, Lcom/google/android/location/e/bf;->a(Ljava/io/PrintWriter;Lcom/google/android/location/e/bf;)V

    const-string v1, "\n cellResult="

    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-object v1, v0, Lcom/google/android/location/e/ag;->c:Lcom/google/android/location/e/f;

    invoke-static {p1, v1}, Lcom/google/android/location/e/f;->a(Ljava/io/PrintWriter;Lcom/google/android/location/e/f;)V

    const-string v1, "\n isLowPower="

    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-boolean v0, v0, Lcom/google/android/location/e/ag;->d:Z

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(Z)V

    const-string v0, "\n]"

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 442
    return-void

    .line 441
    :cond_1
    iget-object v1, v0, Lcom/google/android/location/e/ag;->a:Lcom/google/android/location/e/z;

    iget-object v2, v0, Lcom/google/android/location/e/ag;->b:Lcom/google/android/location/e/bf;

    if-ne v1, v2, :cond_2

    const-string v1, "WIFI"

    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    iget-object v1, v0, Lcom/google/android/location/e/ag;->a:Lcom/google/android/location/e/z;

    iget-object v2, v0, Lcom/google/android/location/e/ag;->c:Lcom/google/android/location/e/f;

    if-ne v1, v2, :cond_0

    const-string v1, "CELL"

    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    goto :goto_0
.end method
