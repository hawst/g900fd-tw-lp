.class public final Lcom/google/android/location/geofencer/data/n;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public final a:I

.field public final b:Ljava/util/LinkedList;

.field public c:Landroid/util/Pair;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 85
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 72
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/google/android/location/geofencer/data/n;->b:Ljava/util/LinkedList;

    .line 80
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/location/geofencer/data/n;->c:Landroid/util/Pair;

    .line 86
    const/16 v0, 0x64

    iput v0, p0, Lcom/google/android/location/geofencer/data/n;->a:I

    .line 87
    return-void
.end method

.method private static a(Ljava/util/List;)F
    .locals 18

    .prologue
    .line 334
    const-wide/high16 v0, 0x7ff0000000000000L    # Double.POSITIVE_INFINITY

    .line 335
    const-wide/high16 v4, -0x10000000000000L    # Double.NEGATIVE_INFINITY

    .line 336
    const-wide/high16 v2, 0x7ff8000000000000L    # NaN

    .line 337
    const-wide/high16 v6, 0x7ff8000000000000L    # NaN

    .line 338
    invoke-interface/range {p0 .. p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :goto_0
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_7

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/util/Pair;

    .line 339
    iget-object v8, v8, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v8, Landroid/location/Location;

    .line 340
    invoke-virtual {v8}, Landroid/location/Location;->getLatitude()D

    move-result-wide v10

    invoke-static {v0, v1, v10, v11}, Ljava/lang/Math;->min(DD)D

    move-result-wide v10

    .line 341
    invoke-virtual {v8}, Landroid/location/Location;->getLatitude()D

    move-result-wide v0

    invoke-static {v4, v5, v0, v1}, Ljava/lang/Math;->max(DD)D

    move-result-wide v4

    .line 343
    invoke-virtual {v8}, Landroid/location/Location;->getLongitude()D

    move-result-wide v0

    .line 344
    invoke-static {v2, v3}, Ljava/lang/Double;->isNaN(D)Z

    move-result v8

    if-eqz v8, :cond_2

    move-wide v2, v0

    :cond_0
    move-wide v6, v0

    :cond_1
    move-wide v0, v10

    .line 355
    goto :goto_0

    .line 347
    :cond_2
    cmpg-double v8, v2, v6

    if-gtz v8, :cond_4

    cmpg-double v8, v2, v0

    if-gtz v8, :cond_3

    cmpg-double v8, v0, v6

    if-gtz v8, :cond_3

    const/4 v8, 0x1

    :goto_1
    if-nez v8, :cond_1

    .line 348
    sub-double v12, v2, v0

    const-wide v14, 0x4076800000000000L    # 360.0

    add-double/2addr v12, v14

    const-wide v14, 0x4076800000000000L    # 360.0

    rem-double/2addr v12, v14

    sub-double v14, v0, v6

    const-wide v16, 0x4076800000000000L    # 360.0

    add-double v14, v14, v16

    const-wide v16, 0x4076800000000000L    # 360.0

    rem-double v14, v14, v16

    cmpg-double v8, v12, v14

    if-gez v8, :cond_0

    move-wide v2, v0

    move-wide v0, v10

    .line 350
    goto :goto_0

    .line 347
    :cond_3
    const/4 v8, 0x0

    goto :goto_1

    :cond_4
    cmpg-double v8, v2, v0

    if-lez v8, :cond_5

    cmpg-double v8, v0, v6

    if-gtz v8, :cond_6

    :cond_5
    const/4 v8, 0x1

    goto :goto_1

    :cond_6
    const/4 v8, 0x0

    goto :goto_1

    .line 356
    :cond_7
    const/4 v8, 0x1

    new-array v8, v8, [F

    .line 357
    invoke-static/range {v0 .. v8}, Landroid/location/Location;->distanceBetween(DDDD[F)V

    .line 358
    sget-boolean v9, Lcom/google/android/location/geofencer/a/a;->a:Z

    if-eqz v9, :cond_8

    .line 359
    const-string v9, "LocationHistory"

    const-string v10, "SW=%.6f,%.6f, NE=%.6f,%.6f d=%.6f"

    const/4 v11, 0x5

    new-array v11, v11, [Ljava/lang/Object;

    const/4 v12, 0x0

    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    aput-object v0, v11, v12

    const/4 v0, 0x1

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v1

    aput-object v1, v11, v0

    const/4 v0, 0x2

    invoke-static {v4, v5}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v1

    aput-object v1, v11, v0

    const/4 v0, 0x3

    invoke-static {v6, v7}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v1

    aput-object v1, v11, v0

    const/4 v0, 0x4

    const/4 v1, 0x0

    aget v1, v8, v1

    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    aput-object v1, v11, v0

    invoke-static {v10, v11}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v9, v0}, Lcom/google/android/location/geofencer/a/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 362
    :cond_8
    const/4 v0, 0x0

    aget v0, v8, v0

    return v0
.end method

.method private b(JJJI)Ljava/util/LinkedList;
    .locals 11

    .prologue
    const/4 v4, 0x0

    .line 209
    iget-object v2, p0, Lcom/google/android/location/geofencer/data/n;->b:Ljava/util/LinkedList;

    invoke-virtual {v2}, Ljava/util/LinkedList;->size()I

    move-result v2

    if-nez v2, :cond_0

    move-object v2, v4

    .line 249
    :goto_0
    return-object v2

    .line 212
    :cond_0
    iget-object v2, p0, Lcom/google/android/location/geofencer/data/n;->b:Ljava/util/LinkedList;

    invoke-virtual {v2}, Ljava/util/LinkedList;->getLast()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/util/Pair;

    .line 216
    iget-object v2, v2, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v2, Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    sub-long v2, p1, v2

    const-wide/32 v6, 0x2bf20

    cmp-long v2, v2, v6

    if-ltz v2, :cond_1

    move-object v2, v4

    .line 217
    goto :goto_0

    .line 221
    :cond_1
    new-instance v5, Ljava/util/LinkedList;

    invoke-direct {v5}, Ljava/util/LinkedList;-><init>()V

    .line 222
    iget-object v2, p0, Lcom/google/android/location/geofencer/data/n;->b:Ljava/util/LinkedList;

    invoke-virtual {v2}, Ljava/util/LinkedList;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    move v6, v2

    :goto_1
    if-ltz v6, :cond_3

    .line 223
    iget-object v2, p0, Lcom/google/android/location/geofencer/data/n;->b:Ljava/util/LinkedList;

    invoke-virtual {v2, v6}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/util/Pair;

    .line 224
    iget-object v3, v2, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v3, Ljava/lang/Long;

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    cmp-long v3, v8, p1

    if-gtz v3, :cond_3

    iget-object v3, v2, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v3, Ljava/lang/Long;

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    sub-long v8, p1, v8

    cmp-long v3, v8, p3

    if-gtz v3, :cond_3

    .line 228
    const-string v7, "geofencing"

    iget-object v3, v2, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v3, Landroid/location/Location;

    invoke-virtual {v3}, Landroid/location/Location;->getProvider()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v7, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_2

    iget-object v3, v2, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v3, Landroid/location/Location;

    invoke-virtual {v3}, Landroid/location/Location;->getAccuracy()F

    move-result v3

    const/high16 v7, 0x42a00000    # 80.0f

    cmpg-float v3, v3, v7

    if-gtz v3, :cond_2

    .line 230
    const/4 v3, 0x0

    invoke-virtual {v5, v3, v2}, Ljava/util/LinkedList;->add(ILjava/lang/Object;)V

    .line 222
    :cond_2
    add-int/lit8 v2, v6, -0x1

    move v6, v2

    goto :goto_1

    .line 239
    :cond_3
    invoke-virtual {v5}, Ljava/util/LinkedList;->size()I

    move-result v3

    .line 240
    move/from16 v0, p7

    if-lt v3, v0, :cond_4

    invoke-virtual {v5}, Ljava/util/LinkedList;->getLast()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/util/Pair;

    iget-object v2, v2, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v2, Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    invoke-virtual {v5}, Ljava/util/LinkedList;->getFirst()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/util/Pair;

    iget-object v2, v2, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v2, Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    sub-long/2addr v6, v8

    cmp-long v2, v6, p5

    if-gez v2, :cond_6

    .line 242
    :cond_4
    sget-boolean v2, Lcom/google/android/location/geofencer/a/a;->a:Z

    if-eqz v2, :cond_5

    .line 243
    const-string v2, "LocationHistory"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "History too short to determine if user is stationary: #locations="

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/location/geofencer/a/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_5
    move-object v2, v4

    .line 246
    goto/16 :goto_0

    :cond_6
    move-object v2, v5

    .line 249
    goto/16 :goto_0
.end method


# virtual methods
.method public final a(JJ)Landroid/location/Location;
    .locals 15

    .prologue
    .line 301
    const-wide/32 v6, 0x75300

    const-wide/32 v8, 0x1d4c0

    const/4 v10, 0x3

    move-object v3, p0

    move-wide/from16 v4, p1

    invoke-direct/range {v3 .. v10}, Lcom/google/android/location/geofencer/data/n;->b(JJJI)Ljava/util/LinkedList;

    move-result-object v10

    .line 303
    if-nez v10, :cond_0

    .line 304
    const/4 v2, 0x0

    .line 323
    :goto_0
    return-object v2

    .line 306
    :cond_0
    invoke-static {v10}, Lcom/google/android/location/geofencer/data/n;->a(Ljava/util/List;)F

    move-result v2

    const/high16 v3, 0x428c0000    # 70.0f

    cmpg-float v2, v2, v3

    if-gez v2, :cond_1

    const/4 v2, 0x1

    .line 307
    :goto_1
    if-nez v2, :cond_2

    .line 308
    const/4 v2, 0x0

    goto :goto_0

    .line 306
    :cond_1
    const/4 v2, 0x0

    goto :goto_1

    .line 310
    :cond_2
    const-wide/16 v6, 0x0

    const-wide/16 v4, 0x0

    const-wide/16 v2, 0x0

    .line 311
    invoke-interface {v10}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v11

    move-wide v8, v6

    move-wide v6, v4

    move-wide v4, v2

    :goto_2
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/util/Pair;

    .line 312
    iget-object v2, v2, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v2, Landroid/location/Location;

    .line 313
    invoke-virtual {v2}, Landroid/location/Location;->getLatitude()D

    move-result-wide v12

    add-double/2addr v8, v12

    .line 314
    invoke-virtual {v2}, Landroid/location/Location;->getLongitude()D

    move-result-wide v12

    add-double/2addr v6, v12

    .line 315
    invoke-virtual {v2}, Landroid/location/Location;->getAccuracy()F

    move-result v2

    float-to-double v2, v2

    add-double/2addr v2, v4

    move-wide v4, v2

    .line 316
    goto :goto_2

    .line 317
    :cond_3
    invoke-interface {v10}, Ljava/util/List;->size()I

    move-result v3

    .line 318
    new-instance v2, Landroid/location/Location;

    const-string v10, "mean-location"

    invoke-direct {v2, v10}, Landroid/location/Location;-><init>(Ljava/lang/String;)V

    .line 319
    int-to-double v10, v3

    div-double/2addr v8, v10

    invoke-virtual {v2, v8, v9}, Landroid/location/Location;->setLatitude(D)V

    .line 320
    int-to-double v8, v3

    div-double/2addr v6, v8

    invoke-virtual {v2, v6, v7}, Landroid/location/Location;->setLongitude(D)V

    .line 321
    int-to-double v6, v3

    div-double/2addr v4, v6

    double-to-float v3, v4

    invoke-virtual {v2, v3}, Landroid/location/Location;->setAccuracy(F)V

    .line 322
    move-wide/from16 v0, p3

    invoke-virtual {v2, v0, v1}, Landroid/location/Location;->setTime(J)V

    goto :goto_0
.end method

.method public final a(JJJI)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 272
    invoke-direct/range {p0 .. p7}, Lcom/google/android/location/geofencer/data/n;->b(JJJI)Ljava/util/LinkedList;

    move-result-object v1

    .line 274
    if-nez v1, :cond_1

    .line 280
    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-static {v1}, Lcom/google/android/location/geofencer/data/n;->a(Ljava/util/List;)F

    move-result v1

    const/high16 v2, 0x41f00000    # 30.0f

    cmpg-float v1, v1, v2

    if-gez v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method
