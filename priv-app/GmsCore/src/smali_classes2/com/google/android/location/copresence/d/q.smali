.class final Lcom/google/android/location/copresence/d/q;
.super Lcom/google/android/location/copresence/k/f;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/location/copresence/d/n;


# direct methods
.method varargs constructor <init>(Lcom/google/android/location/copresence/d/n;Ljava/lang/String;[Lcom/google/android/location/copresence/k/f;)V
    .locals 0

    .prologue
    .line 483
    iput-object p1, p0, Lcom/google/android/location/copresence/d/q;->a:Lcom/google/android/location/copresence/d/n;

    invoke-direct {p0, p2, p3}, Lcom/google/android/location/copresence/k/f;-><init>(Ljava/lang/String;[Lcom/google/android/location/copresence/k/f;)V

    return-void
.end method

.method private e()Z
    .locals 3

    .prologue
    .line 514
    iget-object v0, p0, Lcom/google/android/location/copresence/d/q;->a:Lcom/google/android/location/copresence/d/n;

    iget-object v0, v0, Lcom/google/android/location/copresence/d/n;->c:Landroid/content/SharedPreferences;

    const-string v1, "bluetoothEnabled"

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method


# virtual methods
.method public final a()Z
    .locals 2

    .prologue
    .line 497
    iget-object v0, p0, Lcom/google/android/location/copresence/d/q;->a:Lcom/google/android/location/copresence/d/n;

    iget-object v0, v0, Lcom/google/android/location/copresence/d/n;->c:Landroid/content/SharedPreferences;

    const-string v1, "bluetoothEnabled"

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 500
    const/4 v0, 0x3

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 501
    const-string v0, "BluetoothStates: User bluetooth enabled state not stored. Assuming already reverted."

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->b(Ljava/lang/String;)I

    .line 504
    :cond_0
    const/4 v0, 0x1

    .line 509
    :goto_0
    return v0

    .line 506
    :cond_1
    invoke-direct {p0}, Lcom/google/android/location/copresence/d/q;->e()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 507
    iget-object v0, p0, Lcom/google/android/location/copresence/d/q;->a:Lcom/google/android/location/copresence/d/n;

    iget-object v0, v0, Lcom/google/android/location/copresence/d/n;->b:Lcom/google/android/location/copresence/d/e;

    invoke-virtual {v0}, Lcom/google/android/location/copresence/d/e;->a()Z

    move-result v0

    goto :goto_0

    .line 509
    :cond_2
    iget-object v0, p0, Lcom/google/android/location/copresence/d/q;->a:Lcom/google/android/location/copresence/d/n;

    iget-object v0, v0, Lcom/google/android/location/copresence/d/n;->b:Lcom/google/android/location/copresence/d/e;

    invoke-virtual {v0}, Lcom/google/android/location/copresence/d/e;->b()Z

    move-result v0

    goto :goto_0
.end method

.method public final b()Z
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 487
    iget-object v0, p0, Lcom/google/android/location/copresence/d/q;->a:Lcom/google/android/location/copresence/d/n;

    iget-object v0, v0, Lcom/google/android/location/copresence/d/n;->d:Lcom/google/android/location/copresence/k/f;

    invoke-virtual {v0}, Lcom/google/android/location/copresence/k/f;->b()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 488
    invoke-direct {p0}, Lcom/google/android/location/copresence/d/q;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0xc

    .line 490
    :goto_0
    iget-object v2, p0, Lcom/google/android/location/copresence/d/q;->a:Lcom/google/android/location/copresence/d/n;

    iget-object v2, v2, Lcom/google/android/location/copresence/d/n;->b:Lcom/google/android/location/copresence/d/e;

    iget-object v2, v2, Lcom/google/android/location/copresence/d/e;->a:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v2}, Landroid/bluetooth/BluetoothAdapter;->getState()I

    move-result v2

    if-ne v2, v0, :cond_1

    move v0, v1

    .line 492
    :goto_1
    return v0

    .line 488
    :cond_0
    const/16 v0, 0xa

    goto :goto_0

    .line 490
    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    :cond_2
    move v0, v1

    .line 492
    goto :goto_1
.end method

.method public final d()J
    .locals 2

    .prologue
    .line 519
    invoke-direct {p0}, Lcom/google/android/location/copresence/d/q;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 520
    invoke-static {}, Lcom/google/android/location/copresence/f/a;->h()Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    .line 522
    :goto_0
    return-wide v0

    :cond_0
    invoke-static {}, Lcom/google/android/location/copresence/f/a;->g()Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    goto :goto_0
.end method
