.class public final Lcom/google/android/location/copresence/d/g;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/location/copresence/z;


# instance fields
.field private final b:Lcom/google/android/location/copresence/d/j;

.field private final c:Landroid/content/BroadcastReceiver;

.field private d:Lcom/google/android/location/copresence/ab;

.field private e:Lcom/google/ac/b/c/o;


# direct methods
.method constructor <init>(Lcom/google/android/location/copresence/d/j;)V
    .locals 1

    .prologue
    .line 55
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    new-instance v0, Lcom/google/android/location/copresence/d/h;

    invoke-direct {v0, p0}, Lcom/google/android/location/copresence/d/h;-><init>(Lcom/google/android/location/copresence/d/g;)V

    iput-object v0, p0, Lcom/google/android/location/copresence/d/g;->c:Landroid/content/BroadcastReceiver;

    .line 56
    iput-object p1, p0, Lcom/google/android/location/copresence/d/g;->b:Lcom/google/android/location/copresence/d/j;

    .line 57
    return-void
.end method

.method static synthetic a(Lcom/google/android/location/copresence/d/g;)Lcom/google/android/location/copresence/ab;
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Lcom/google/android/location/copresence/d/g;->d:Lcom/google/android/location/copresence/ab;

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/location/copresence/d/g;Ljava/lang/String;ILcom/google/ac/b/c/o;)V
    .locals 5

    .prologue
    .line 27
    invoke-static {p1, p3}, Lcom/google/android/location/copresence/d/m;->a(Ljava/lang/String;Lcom/google/ac/b/c/o;)Z

    move-result v0

    if-eqz v0, :cond_0

    :try_start_0
    invoke-static {p1, p3}, Lcom/google/android/location/copresence/d/m;->b(Ljava/lang/String;Lcom/google/ac/b/c/o;)Lcom/google/android/gms/location/copresence/x;

    move-result-object v0

    new-instance v1, Lcom/google/ac/b/c/bu;

    invoke-direct {v1}, Lcom/google/ac/b/c/bu;-><init>()V

    invoke-virtual {v0}, Lcom/google/android/gms/location/copresence/x;->a()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/google/ac/b/c/bu;->a:Ljava/lang/String;

    const/4 v2, 0x1

    new-array v2, v2, [Lcom/google/ac/b/c/bv;

    iput-object v2, v1, Lcom/google/ac/b/c/bu;->b:[Lcom/google/ac/b/c/bv;

    new-instance v2, Lcom/google/ac/b/c/bv;

    invoke-direct {v2}, Lcom/google/ac/b/c/bv;-><init>()V

    const/4 v3, 0x2

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    iput-object v3, v2, Lcom/google/ac/b/c/bv;->a:Ljava/lang/Integer;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    iput-object v3, v2, Lcom/google/ac/b/c/bv;->b:Ljava/lang/Integer;

    iget-object v3, v1, Lcom/google/ac/b/c/bu;->b:[Lcom/google/ac/b/c/bv;

    const/4 v4, 0x0

    aput-object v2, v3, v4

    iget-object v2, p0, Lcom/google/android/location/copresence/d/g;->d:Lcom/google/android/location/copresence/ab;

    invoke-interface {v2, v0, v1}, Lcom/google/android/location/copresence/ab;->a(Lcom/google/android/gms/location/copresence/x;Lcom/google/ac/b/c/bu;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    const/4 v0, 0x5

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "BluetoothDiscoveryTokenListener: device name appears to be copresence, but is not valid token."

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->d(Ljava/lang/String;)I

    goto :goto_0
.end method

.method static synthetic b(Lcom/google/android/location/copresence/d/g;)Lcom/google/ac/b/c/o;
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Lcom/google/android/location/copresence/d/g;->e:Lcom/google/ac/b/c/o;

    return-object v0
.end method


# virtual methods
.method public final a(Lcom/google/android/location/copresence/ab;Lcom/google/android/location/copresence/as;Lcom/google/android/location/copresence/q/al;)V
    .locals 5

    .prologue
    .line 72
    invoke-virtual {p0}, Lcom/google/android/location/copresence/d/g;->b()Z

    move-result v0

    if-nez v0, :cond_0

    .line 73
    new-instance v0, Lcom/google/android/location/copresence/ac;

    invoke-direct {v0}, Lcom/google/android/location/copresence/ac;-><init>()V

    throw v0

    .line 75
    :cond_0
    iput-object p1, p0, Lcom/google/android/location/copresence/d/g;->d:Lcom/google/android/location/copresence/ab;

    .line 76
    iget-object v0, p2, Lcom/google/android/location/copresence/as;->b:Lcom/google/ac/b/c/o;

    iput-object v0, p0, Lcom/google/android/location/copresence/d/g;->e:Lcom/google/ac/b/c/o;

    .line 77
    new-instance v0, Landroid/content/IntentFilter;

    const-string v1, "android.bluetooth.device.action.FOUND"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 78
    iget-object v1, p0, Lcom/google/android/location/copresence/d/g;->b:Lcom/google/android/location/copresence/d/j;

    invoke-virtual {v1}, Lcom/google/android/location/copresence/d/j;->e()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/location/copresence/d/g;->c:Landroid/content/BroadcastReceiver;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/android/location/copresence/d/g;->b:Lcom/google/android/location/copresence/d/j;

    invoke-virtual {v4}, Lcom/google/android/location/copresence/d/j;->f()Landroid/os/Handler;

    move-result-object v4

    invoke-virtual {v1, v2, v0, v3, v4}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;Ljava/lang/String;Landroid/os/Handler;)Landroid/content/Intent;

    .line 79
    iget-object v0, p0, Lcom/google/android/location/copresence/d/g;->b:Lcom/google/android/location/copresence/d/j;

    invoke-virtual {v0, p3}, Lcom/google/android/location/copresence/d/j;->a(Lcom/google/android/location/copresence/q/al;)V

    .line 80
    const/4 v0, 0x3

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 81
    const-string v0, "E2E Listen: step 2b) Listen on Bluetooth Classic Discovery started."

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->b(Ljava/lang/String;)I

    .line 83
    :cond_1
    return-void
.end method

.method public final a(Lcom/google/android/location/copresence/q/al;)V
    .locals 2

    .prologue
    .line 87
    iget-object v0, p0, Lcom/google/android/location/copresence/d/g;->d:Lcom/google/android/location/copresence/ab;

    if-eqz v0, :cond_1

    .line 88
    iget-object v0, p0, Lcom/google/android/location/copresence/d/g;->b:Lcom/google/android/location/copresence/d/j;

    invoke-virtual {v0}, Lcom/google/android/location/copresence/d/j;->e()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/location/copresence/d/g;->c:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 89
    iget-object v0, p0, Lcom/google/android/location/copresence/d/g;->b:Lcom/google/android/location/copresence/d/j;

    invoke-virtual {v0, p1}, Lcom/google/android/location/copresence/d/j;->b(Lcom/google/android/location/copresence/q/al;)V

    .line 90
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/location/copresence/d/g;->d:Lcom/google/android/location/copresence/ab;

    .line 96
    :cond_0
    :goto_0
    return-void

    .line 92
    :cond_1
    if-eqz p1, :cond_0

    .line 93
    invoke-interface {p1}, Lcom/google/android/location/copresence/q/al;->b()V

    goto :goto_0
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 61
    const/4 v0, 0x1

    return v0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 66
    invoke-static {}, Lcom/google/android/location/copresence/f/b;->b()Lcom/google/ac/b/c/g;

    move-result-object v0

    iget-object v0, v0, Lcom/google/ac/b/c/g;->e:Lcom/google/ac/b/c/m;

    iget-object v0, v0, Lcom/google/ac/b/c/m;->d:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method

.method public final c()V
    .locals 0

    .prologue
    .line 101
    return-void
.end method
