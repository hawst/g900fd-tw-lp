.class public final Lcom/google/android/location/copresence/i/a;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/location/copresence/i/d;


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x13
.end annotation


# instance fields
.field private final a:Lcom/google/android/location/n/g;

.field private final b:I

.field private final c:Ljava/lang/String;

.field private final d:Landroid/content/Context;

.field private final e:Landroid/content/Intent;

.field private f:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    iput-object p1, p0, Lcom/google/android/location/copresence/i/a;->d:Landroid/content/Context;

    .line 30
    invoke-static {p1}, Lcom/google/android/location/n/g;->a(Landroid/content/Context;)Lcom/google/android/location/n/g;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/copresence/i/a;->a:Lcom/google/android/location/n/g;

    .line 31
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/location/copresence/i/a;->f:Z

    .line 32
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object v0

    iget v0, v0, Landroid/content/pm/ApplicationInfo;->uid:I

    iput v0, p0, Lcom/google/android/location/copresence/i/a;->b:I

    .line 33
    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/copresence/i/a;->c:Ljava/lang/String;

    .line 37
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.location.HIGH_POWER_REQUEST_CHANGE"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/location/copresence/i/a;->e:Landroid/content/Intent;

    .line 38
    return-void
.end method

.method private a()V
    .locals 2

    .prologue
    .line 74
    iget-object v0, p0, Lcom/google/android/location/copresence/i/a;->d:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/location/copresence/i/a;->e:Landroid/content/Intent;

    invoke-virtual {v0, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 75
    return-void
.end method


# virtual methods
.method public final a(Z)V
    .locals 4

    .prologue
    .line 42
    iget-boolean v0, p0, Lcom/google/android/location/copresence/i/a;->f:Z

    if-eqz v0, :cond_1

    .line 43
    if-eqz p1, :cond_0

    .line 44
    invoke-direct {p0}, Lcom/google/android/location/copresence/i/a;->a()V

    .line 52
    :cond_0
    :goto_0
    return-void

    .line 48
    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/location/copresence/i/a;->f:Z

    .line 49
    iget-object v0, p0, Lcom/google/android/location/copresence/i/a;->a:Lcom/google/android/location/n/g;

    const-string v1, "android:monitor_location_high_power"

    iget v2, p0, Lcom/google/android/location/copresence/i/a;->b:I

    iget-object v3, p0, Lcom/google/android/location/copresence/i/a;->c:Ljava/lang/String;

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/location/n/g;->a(Ljava/lang/String;ILjava/lang/String;)I

    .line 51
    invoke-direct {p0}, Lcom/google/android/location/copresence/i/a;->a()V

    goto :goto_0
.end method

.method public final b(Z)V
    .locals 4

    .prologue
    .line 56
    iget-boolean v0, p0, Lcom/google/android/location/copresence/i/a;->f:Z

    if-nez v0, :cond_1

    .line 57
    if-eqz p1, :cond_0

    .line 58
    invoke-direct {p0}, Lcom/google/android/location/copresence/i/a;->a()V

    .line 67
    :cond_0
    :goto_0
    return-void

    .line 64
    :cond_1
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/location/copresence/i/a;->f:Z

    .line 65
    iget-object v0, p0, Lcom/google/android/location/copresence/i/a;->a:Lcom/google/android/location/n/g;

    const-string v1, "android:monitor_location_high_power"

    iget v2, p0, Lcom/google/android/location/copresence/i/a;->b:I

    iget-object v3, p0, Lcom/google/android/location/copresence/i/a;->c:Ljava/lang/String;

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/location/n/g;->b(Ljava/lang/String;ILjava/lang/String;)V

    .line 66
    invoke-direct {p0}, Lcom/google/android/location/copresence/i/a;->a()V

    goto :goto_0
.end method
