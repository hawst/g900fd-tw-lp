.class final Lcom/google/android/location/copresence/r/n;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/location/copresence/w;


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x10
.end annotation


# static fields
.field private static final b:I


# instance fields
.field private final c:Lcom/google/android/location/copresence/r/ae;

.field private final d:Lcom/google/android/location/copresence/r/ah;

.field private final e:Landroid/content/SharedPreferences;

.field private final f:Lcom/google/android/location/copresence/k/a;

.field private final g:Lcom/google/android/location/copresence/r/p;

.field private final h:Lcom/google/android/location/copresence/k/c;

.field private i:Lcom/google/android/gms/location/copresence/x;

.field private j:Ljava/lang/String;

.field private k:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 42
    const/16 v0, 0xa

    sput v0, Lcom/google/android/location/copresence/r/n;->b:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/android/location/copresence/r/ae;Lcom/google/android/location/copresence/r/ah;)V
    .locals 9

    .prologue
    const/4 v8, 0x0

    .line 92
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 51
    new-instance v0, Lcom/google/android/location/copresence/r/o;

    invoke-direct {v0, p0}, Lcom/google/android/location/copresence/r/o;-><init>(Lcom/google/android/location/copresence/r/n;)V

    iput-object v0, p0, Lcom/google/android/location/copresence/r/n;->h:Lcom/google/android/location/copresence/k/c;

    .line 93
    iput-object p2, p0, Lcom/google/android/location/copresence/r/n;->c:Lcom/google/android/location/copresence/r/ae;

    .line 94
    iput-object p3, p0, Lcom/google/android/location/copresence/r/n;->d:Lcom/google/android/location/copresence/r/ah;

    .line 95
    const-string v0, "copresence_wifi_direct_beacon_state"

    invoke-virtual {p1, v0, v8}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/copresence/r/n;->e:Landroid/content/SharedPreferences;

    .line 96
    new-instance v1, Lcom/google/android/location/copresence/k/a;

    new-instance v2, Landroid/os/Handler;

    invoke-direct {v2}, Landroid/os/Handler;-><init>()V

    new-instance v3, Lcom/google/android/location/copresence/k/e;

    const-string v0, "WifiDirectBeacon2"

    iget-object v4, p0, Lcom/google/android/location/copresence/r/n;->h:Lcom/google/android/location/copresence/k/c;

    invoke-direct {v3, p1, v0, v4}, Lcom/google/android/location/copresence/k/e;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/location/copresence/k/c;)V

    invoke-static {}, Lcom/google/android/location/copresence/f/b;->b()Lcom/google/ac/b/c/g;

    move-result-object v0

    iget-object v0, v0, Lcom/google/ac/b/c/g;->j:Lcom/google/ac/b/c/p;

    iget-object v0, v0, Lcom/google/ac/b/c/p;->e:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-static {}, Lcom/google/android/location/copresence/f/b;->b()Lcom/google/ac/b/c/g;

    move-result-object v0

    iget-object v0, v0, Lcom/google/ac/b/c/g;->j:Lcom/google/ac/b/c/p;

    iget-object v0, v0, Lcom/google/ac/b/c/p;->d:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    invoke-direct/range {v1 .. v7}, Lcom/google/android/location/copresence/k/a;-><init>(Landroid/os/Handler;Lcom/google/android/location/copresence/k/c;JJ)V

    iput-object v1, p0, Lcom/google/android/location/copresence/r/n;->f:Lcom/google/android/location/copresence/k/a;

    .line 101
    new-instance v0, Lcom/google/android/location/copresence/r/p;

    iget-object v1, p0, Lcom/google/android/location/copresence/r/n;->f:Lcom/google/android/location/copresence/k/a;

    iget-object v2, p0, Lcom/google/android/location/copresence/r/n;->e:Landroid/content/SharedPreferences;

    invoke-direct {v0, p1, p3, v1, v2}, Lcom/google/android/location/copresence/r/p;-><init>(Landroid/content/Context;Lcom/google/android/location/copresence/r/ah;Lcom/google/android/location/copresence/k/a;Landroid/content/SharedPreferences;)V

    iput-object v0, p0, Lcom/google/android/location/copresence/r/n;->g:Lcom/google/android/location/copresence/r/p;

    .line 103
    invoke-direct {p0}, Lcom/google/android/location/copresence/r/n;->d()V

    .line 104
    iput-boolean v8, p0, Lcom/google/android/location/copresence/r/n;->k:Z

    .line 105
    return-void
.end method

.method static synthetic a(Lcom/google/android/location/copresence/r/n;)Lcom/google/android/location/copresence/r/p;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lcom/google/android/location/copresence/r/n;->g:Lcom/google/android/location/copresence/r/p;

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/location/copresence/r/n;Z)Z
    .locals 0

    .prologue
    .line 38
    iput-boolean p1, p0, Lcom/google/android/location/copresence/r/n;->k:Z

    return p1
.end method

.method static synthetic b(Lcom/google/android/location/copresence/r/n;)Lcom/google/android/location/copresence/k/a;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lcom/google/android/location/copresence/r/n;->f:Lcom/google/android/location/copresence/k/a;

    return-object v0
.end method

.method private c()Z
    .locals 8

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x1

    const/4 v7, 0x3

    .line 120
    iget-object v0, p0, Lcom/google/android/location/copresence/r/n;->d:Lcom/google/android/location/copresence/r/ah;

    iget-object v3, v0, Lcom/google/android/location/copresence/r/ah;->d:Landroid/net/wifi/p2p/WifiP2pGroup;

    .line 121
    iget-object v0, p0, Lcom/google/android/location/copresence/r/n;->d:Lcom/google/android/location/copresence/r/ah;

    iget-object v0, v0, Lcom/google/android/location/copresence/r/ah;->c:Landroid/net/wifi/p2p/WifiP2pInfo;

    .line 122
    invoke-static {v7}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 123
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "WifiDirectBeacon2: Wifi direct information to check if already connected: CurrentGroup: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " CurrentInfo: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " CurrentDeviceName: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/location/copresence/r/n;->d:Lcom/google/android/location/copresence/r/ah;

    invoke-virtual {v5}, Lcom/google/android/location/copresence/r/ah;->c()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/google/android/location/copresence/ag;->b(Ljava/lang/String;)I

    .line 128
    :cond_0
    iget-boolean v0, v0, Landroid/net/wifi/p2p/WifiP2pInfo;->groupFormed:Z

    if-eqz v0, :cond_8

    if-eqz v3, :cond_8

    .line 134
    invoke-virtual {v3}, Landroid/net/wifi/p2p/WifiP2pGroup;->isGroupOwner()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 135
    invoke-virtual {v3}, Landroid/net/wifi/p2p/WifiP2pGroup;->getNetworkName()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_4

    move-object v0, v1

    .line 137
    :goto_0
    iget-object v4, p0, Lcom/google/android/location/copresence/r/n;->i:Lcom/google/android/gms/location/copresence/x;

    if-nez v4, :cond_5

    .line 139
    :goto_1
    iget-object v4, p0, Lcom/google/android/location/copresence/r/n;->d:Lcom/google/android/location/copresence/r/ah;

    invoke-virtual {v4}, Lcom/google/android/location/copresence/r/ah;->c()Ljava/lang/String;

    move-result-object v4

    .line 140
    invoke-static {v7}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 141
    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "WifiDirectBeacon2: Wifi direct information to check if already connected : CurrentGroupName: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " CombinedToken: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " CurrentDeviceName: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/google/android/location/copresence/ag;->b(Ljava/lang/String;)I

    .line 146
    :cond_1
    if-eqz v0, :cond_2

    invoke-static {v0, v4}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-static {v4, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_8

    .line 148
    :cond_2
    invoke-static {v7}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 149
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "WifiDirectBeacon2: Wifi direct is already being used, locally created group: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->b(Ljava/lang/String;)I

    :cond_3
    move v0, v2

    .line 162
    :goto_2
    return v0

    .line 135
    :cond_4
    invoke-virtual {v3}, Landroid/net/wifi/p2p/WifiP2pGroup;->getNetworkName()Ljava/lang/String;

    move-result-object v0

    sget v4, Lcom/google/android/location/copresence/r/n;->b:I

    invoke-virtual {v0, v4}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 137
    :cond_5
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lcom/google/android/location/copresence/r/n;->j:Ljava/lang/String;

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v4, p0, Lcom/google/android/location/copresence/r/n;->i:Lcom/google/android/gms/location/copresence/x;

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_1

    .line 155
    :cond_6
    invoke-static {v7}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 156
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "WifiDirectBeacon2: Wifi direct is already being used, connected to group: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3}, Landroid/net/wifi/p2p/WifiP2pGroup;->getOwner()Landroid/net/wifi/p2p/WifiP2pDevice;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->b(Ljava/lang/String;)I

    :cond_7
    move v0, v2

    .line 159
    goto :goto_2

    .line 162
    :cond_8
    const/4 v0, 0x0

    goto :goto_2
.end method

.method static synthetic c(Lcom/google/android/location/copresence/r/n;)Z
    .locals 1

    .prologue
    .line 38
    iget-boolean v0, p0, Lcom/google/android/location/copresence/r/n;->k:Z

    return v0
.end method

.method private d()V
    .locals 4

    .prologue
    .line 198
    invoke-direct {p0}, Lcom/google/android/location/copresence/r/n;->c()Z

    move-result v0

    if-nez v0, :cond_0

    .line 199
    iget-object v0, p0, Lcom/google/android/location/copresence/r/n;->f:Lcom/google/android/location/copresence/k/a;

    const/4 v1, 0x1

    new-array v1, v1, [Lcom/google/android/location/copresence/k/f;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/android/location/copresence/r/n;->g:Lcom/google/android/location/copresence/r/p;

    iget-object v3, v3, Lcom/google/android/location/copresence/r/p;->c:Lcom/google/android/location/copresence/r/ad;

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Lcom/google/android/location/copresence/k/a;->b([Lcom/google/android/location/copresence/k/f;)V

    .line 201
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/location/copresence/b;)V
    .locals 6

    .prologue
    .line 168
    invoke-static {p1}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 169
    iget-object v0, p1, Lcom/google/android/location/copresence/b;->a:Lcom/google/android/gms/location/copresence/x;

    iget-object v1, p0, Lcom/google/android/location/copresence/r/n;->i:Lcom/google/android/gms/location/copresence/x;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/location/copresence/x;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 170
    invoke-virtual {p0}, Lcom/google/android/location/copresence/r/n;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 171
    new-instance v0, Lcom/google/android/location/copresence/y;

    invoke-direct {v0}, Lcom/google/android/location/copresence/y;-><init>()V

    throw v0

    .line 173
    :cond_0
    const/4 v0, 0x3

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 174
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "WifiDirectBeacon2: Starting advertising token "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p1, Lcom/google/android/location/copresence/b;->a:Lcom/google/android/gms/location/copresence/x;

    invoke-virtual {v1}, Lcom/google/android/gms/location/copresence/x;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->b(Ljava/lang/String;)I

    .line 177
    :cond_1
    iget-object v0, p1, Lcom/google/android/location/copresence/b;->a:Lcom/google/android/gms/location/copresence/x;

    iput-object v0, p0, Lcom/google/android/location/copresence/r/n;->i:Lcom/google/android/gms/location/copresence/x;

    .line 178
    iget-object v0, p1, Lcom/google/android/location/copresence/b;->b:Lcom/google/ac/b/c/o;

    iget-object v0, v0, Lcom/google/ac/b/c/o;->b:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/location/copresence/r/n;->j:Ljava/lang/String;

    .line 179
    iget-object v0, p0, Lcom/google/android/location/copresence/r/n;->f:Lcom/google/android/location/copresence/k/a;

    const/4 v1, 0x2

    new-array v1, v1, [Lcom/google/android/location/copresence/k/f;

    const/4 v2, 0x0

    new-instance v3, Lcom/google/android/location/copresence/r/z;

    iget-object v4, p0, Lcom/google/android/location/copresence/r/n;->g:Lcom/google/android/location/copresence/r/p;

    invoke-virtual {v4}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    invoke-virtual {p1}, Lcom/google/android/location/copresence/b;->a()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v3, v4, v5}, Lcom/google/android/location/copresence/r/z;-><init>(Lcom/google/android/location/copresence/r/p;Ljava/lang/String;)V

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/google/android/location/copresence/r/n;->g:Lcom/google/android/location/copresence/r/p;

    iget-object v3, v3, Lcom/google/android/location/copresence/r/p;->b:Lcom/google/android/location/copresence/r/ad;

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Lcom/google/android/location/copresence/k/a;->b([Lcom/google/android/location/copresence/k/f;)V

    .line 183
    :cond_2
    return-void
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 114
    invoke-static {}, Lcom/google/android/location/copresence/f/b;->b()Lcom/google/ac/b/c/g;

    move-result-object v0

    iget-object v0, v0, Lcom/google/ac/b/c/g;->e:Lcom/google/ac/b/c/m;

    iget-object v0, v0, Lcom/google/ac/b/c/m;->l:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/location/copresence/r/n;->c:Lcom/google/android/location/copresence/r/ae;

    invoke-virtual {v0}, Lcom/google/android/location/copresence/r/ae;->a()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/location/copresence/r/n;->c()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 187
    iget-object v0, p0, Lcom/google/android/location/copresence/r/n;->i:Lcom/google/android/gms/location/copresence/x;

    if-eqz v0, :cond_1

    .line 188
    const/4 v0, 0x3

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 189
    const-string v0, "WifiDirectBeacon2: Stopping advertising token"

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->b(Ljava/lang/String;)I

    .line 191
    :cond_0
    invoke-direct {p0}, Lcom/google/android/location/copresence/r/n;->d()V

    .line 192
    iput-object v1, p0, Lcom/google/android/location/copresence/r/n;->i:Lcom/google/android/gms/location/copresence/x;

    .line 193
    iput-object v1, p0, Lcom/google/android/location/copresence/r/n;->j:Ljava/lang/String;

    .line 195
    :cond_1
    return-void
.end method
