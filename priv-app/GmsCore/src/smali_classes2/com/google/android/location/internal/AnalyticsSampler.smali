.class public final Lcom/google/android/location/internal/AnalyticsSampler;
.super Landroid/content/BroadcastReceiver;
.source "SourceFile"


# static fields
.field private static a:Landroid/app/PendingIntent;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method public static declared-synchronized a(Landroid/content/Context;)V
    .locals 10

    .prologue
    const-wide/32 v8, 0x5265c00

    const/4 v1, 0x0

    .line 46
    const-class v7, Lcom/google/android/location/internal/AnalyticsSampler;

    monitor-enter v7

    :try_start_0
    sget-object v0, Lcom/google/android/location/internal/AnalyticsSampler;->a:Landroid/app/PendingIntent;

    if-nez v0, :cond_1

    .line 47
    const-string v0, "alarm"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    .line 49
    new-instance v2, Landroid/content/Intent;

    const-class v3, Lcom/google/android/location/internal/AnalyticsSampler;

    invoke-direct {v2, p0, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 51
    const/4 v3, 0x0

    const/high16 v4, 0x20000000

    invoke-static {p0, v3, v2, v4}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v3

    if-nez v3, :cond_0

    const/4 v1, 0x1

    .line 54
    :cond_0
    if-eqz v1, :cond_1

    .line 55
    const/4 v1, 0x0

    const/4 v3, 0x0

    invoke-static {p0, v1, v2, v3}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    sput-object v1, Lcom/google/android/location/internal/AnalyticsSampler;->a:Landroid/app/PendingIntent;

    .line 56
    const/4 v1, 0x2

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    add-long/2addr v2, v8

    const-wide/32 v4, 0x5265c00

    sget-object v6, Lcom/google/android/location/internal/AnalyticsSampler;->a:Landroid/app/PendingIntent;

    invoke-virtual/range {v0 .. v6}, Landroid/app/AlarmManager;->setInexactRepeating(IJJLandroid/app/PendingIntent;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 68
    :cond_1
    monitor-exit v7

    return-void

    .line 46
    :catchall_0
    move-exception v0

    monitor-exit v7

    throw v0
.end method


# virtual methods
.method public final onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 2

    .prologue
    .line 76
    if-nez p2, :cond_1

    .line 88
    :cond_0
    :goto_0
    return-void

    .line 82
    :cond_1
    sget-object v0, Lcom/google/android/location/d/a;->l:Lcom/google/android/gms/common/a/d;

    invoke-static {v0}, Lcom/google/android/location/d/a;->a(Lcom/google/android/gms/common/a/d;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 85
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/location/internal/AnalyticsSamplerService;

    invoke-direct {v0, p1, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 86
    invoke-virtual {p1, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    goto :goto_0
.end method
