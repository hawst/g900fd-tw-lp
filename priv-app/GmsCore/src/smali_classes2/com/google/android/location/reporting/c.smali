.class public final Lcom/google/android/location/reporting/c;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method static a(Lcom/google/android/gms/location/LocationStatus;)Lcom/google/android/ulr/ApiLocationStatus;
    .locals 3

    .prologue
    .line 88
    invoke-virtual {p0}, Lcom/google/android/gms/location/LocationStatus;->a()I

    move-result v0

    invoke-static {v0}, Lcom/google/android/location/reporting/c;->a(I)Ljava/lang/String;

    move-result-object v0

    .line 89
    invoke-virtual {p0}, Lcom/google/android/gms/location/LocationStatus;->b()I

    move-result v1

    invoke-static {v1}, Lcom/google/android/location/reporting/c;->a(I)Ljava/lang/String;

    move-result-object v1

    .line 91
    new-instance v2, Lcom/google/android/ulr/ApiLocationStatus;

    invoke-direct {v2, v0, v1}, Lcom/google/android/ulr/ApiLocationStatus;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    return-object v2
.end method

.method public static a(Lcom/google/android/gms/location/LocationStatus;J)Lcom/google/android/ulr/ApiMetadata;
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 77
    invoke-static {p0}, Lcom/google/android/location/reporting/c;->a(Lcom/google/android/gms/location/LocationStatus;)Lcom/google/android/ulr/ApiLocationStatus;

    move-result-object v3

    .line 83
    new-instance v0, Lcom/google/android/ulr/ApiMetadata;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    move-object v2, v1

    move-object v4, v1

    move-object v5, v1

    invoke-direct/range {v0 .. v6}, Lcom/google/android/ulr/ApiMetadata;-><init>(Lcom/google/android/ulr/ApiActivationChange;Lcom/google/android/ulr/ApiBleScanReport;Lcom/google/android/ulr/ApiLocationStatus;Lcom/google/android/ulr/ApiPlaceReport;Lcom/google/android/ulr/ApiRate;Ljava/lang/Long;)V

    return-object v0
.end method

.method public static a(Lcom/google/android/ulr/ApiRate;)Lcom/google/android/ulr/ApiMetadata;
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 44
    new-instance v0, Lcom/google/android/ulr/ApiMetadata;

    invoke-virtual {p0}, Lcom/google/android/ulr/ApiRate;->c()Ljava/lang/Long;

    move-result-object v6

    move-object v2, v1

    move-object v3, v1

    move-object v4, v1

    move-object v5, p0

    invoke-direct/range {v0 .. v6}, Lcom/google/android/ulr/ApiMetadata;-><init>(Lcom/google/android/ulr/ApiActivationChange;Lcom/google/android/ulr/ApiBleScanReport;Lcom/google/android/ulr/ApiLocationStatus;Lcom/google/android/ulr/ApiPlaceReport;Lcom/google/android/ulr/ApiRate;Ljava/lang/Long;)V

    return-object v0
.end method

.method private static a(I)Ljava/lang/String;
    .locals 1

    .prologue
    .line 142
    packed-switch p0, :pswitch_data_0

    .line 162
    const-string v0, "indeterminate"

    :goto_0
    return-object v0

    .line 144
    :pswitch_0
    const-string v0, "successful"

    goto :goto_0

    .line 146
    :pswitch_1
    const-string v0, "unknown"

    goto :goto_0

    .line 148
    :pswitch_2
    const-string v0, "timedOutOnScan"

    goto :goto_0

    .line 150
    :pswitch_3
    const-string v0, "noInfoInDatabase"

    goto :goto_0

    .line 152
    :pswitch_4
    const-string v0, "invalidScan"

    goto :goto_0

    .line 154
    :pswitch_5
    const-string v0, "unableToQueryDatabase"

    goto :goto_0

    .line 156
    :pswitch_6
    const-string v0, "scansDisabledInSettings"

    goto :goto_0

    .line 158
    :pswitch_7
    const-string v0, "locationDisabledInSettings"

    goto :goto_0

    .line 160
    :pswitch_8
    const-string v0, "inProgress"

    goto :goto_0

    .line 142
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
    .end packed-switch
.end method

.method public static a(Lcom/google/android/location/reporting/e;Lcom/google/android/location/reporting/config/ReportingConfig;Lcom/google/android/ulr/ApiMetadata;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 31
    invoke-virtual {p1}, Lcom/google/android/location/reporting/config/ReportingConfig;->getActiveAccountConfigs()Ljava/util/List;

    move-result-object v0

    .line 32
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    invoke-static {v1}, Lcom/google/android/location/reporting/b/l;->a(I)V

    .line 35
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/reporting/config/AccountConfig;

    .line 36
    invoke-virtual {p0, v0, p2, p3}, Lcom/google/android/location/reporting/e;->a(Lcom/google/android/location/reporting/config/AccountConfig;Ljava/lang/Object;Ljava/lang/String;)Z

    goto :goto_0

    .line 38
    :cond_0
    return-void
.end method
