.class public final Lcom/google/android/location/os/real/bd;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/location/os/bn;


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Lcom/google/android/location/os/j;

.field private final c:Lcom/google/android/location/os/real/be;

.field private final d:Lcom/google/android/location/os/real/be;

.field private final e:Landroid/net/wifi/WifiManager;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/location/os/j;Lcom/google/android/location/os/real/be;Lcom/google/android/location/os/real/be;Landroid/net/wifi/WifiManager;)V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    iput-object p1, p0, Lcom/google/android/location/os/real/bd;->a:Landroid/content/Context;

    .line 24
    iput-object p2, p0, Lcom/google/android/location/os/real/bd;->b:Lcom/google/android/location/os/j;

    .line 25
    iput-object p3, p0, Lcom/google/android/location/os/real/bd;->c:Lcom/google/android/location/os/real/be;

    .line 26
    iput-object p4, p0, Lcom/google/android/location/os/real/bd;->d:Lcom/google/android/location/os/real/be;

    .line 27
    iput-object p5, p0, Lcom/google/android/location/os/real/bd;->e:Landroid/net/wifi/WifiManager;

    .line 28
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/location/j/k;Lcom/google/android/location/o/n;)V
    .locals 7

    .prologue
    .line 32
    iget-object v2, p0, Lcom/google/android/location/os/real/bd;->b:Lcom/google/android/location/os/j;

    invoke-virtual {p1}, Lcom/google/android/location/j/k;->ordinal()I

    move-result v6

    new-instance v1, Lcom/google/android/location/os/k;

    sget-object v3, Lcom/google/android/location/os/au;->T:Lcom/google/android/location/os/au;

    iget-object v0, v2, Lcom/google/android/location/os/j;->a:Lcom/google/android/location/os/at;

    invoke-interface {v0}, Lcom/google/android/location/os/at;->a()J

    move-result-wide v4

    invoke-direct/range {v1 .. v6}, Lcom/google/android/location/os/k;-><init>(Lcom/google/android/location/os/j;Lcom/google/android/location/os/au;JI)V

    invoke-virtual {v2, v1, v6}, Lcom/google/android/location/os/j;->a(Lcom/google/android/location/os/as;I)V

    .line 33
    invoke-static {}, Lcom/google/android/location/os/real/bf;->a()Lcom/google/android/location/os/real/bf;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/location/os/real/bd;->a:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/location/os/real/bd;->d:Lcom/google/android/location/os/real/be;

    invoke-virtual {v0, v1, v2, p2}, Lcom/google/android/location/os/real/bf;->a(Landroid/content/Context;Lcom/google/android/location/os/real/be;Lcom/google/android/location/o/n;)V

    .line 34
    return-void
.end method

.method public final a()Z
    .locals 3

    .prologue
    .line 38
    invoke-static {}, Lcom/google/android/location/os/real/bf;->a()Lcom/google/android/location/os/real/bf;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/location/os/real/bd;->e:Landroid/net/wifi/WifiManager;

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/google/android/location/os/real/bf;->a(Landroid/net/wifi/WifiManager;I)Z

    move-result v0

    .line 41
    return v0
.end method

.method public final a(ZJ)Z
    .locals 8

    .prologue
    .line 46
    invoke-static {}, Lcom/google/android/location/os/real/bf;->a()Lcom/google/android/location/os/real/bf;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/location/os/real/bd;->a:Landroid/content/Context;

    iget-object v6, p0, Lcom/google/android/location/os/real/bd;->c:Lcom/google/android/location/os/real/be;

    move v3, p1

    move-wide v4, p2

    invoke-virtual/range {v1 .. v6}, Lcom/google/android/location/os/real/bf;->a(Landroid/content/Context;ZJLcom/google/android/location/os/real/be;)Z

    move-result v0

    return v0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Lcom/google/android/location/os/real/bd;->e:Landroid/net/wifi/WifiManager;

    invoke-virtual {v0}, Landroid/net/wifi/WifiManager;->reconnect()Z

    move-result v0

    return v0
.end method

.method public final c()Z
    .locals 3

    .prologue
    .line 52
    invoke-static {}, Lcom/google/android/location/os/real/bf;->a()Lcom/google/android/location/os/real/bf;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/location/os/real/bd;->e:Landroid/net/wifi/WifiManager;

    iget-object v2, p0, Lcom/google/android/location/os/real/bd;->a:Landroid/content/Context;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/location/os/real/bf;->a(Landroid/net/wifi/WifiManager;Landroid/content/Context;)Z

    move-result v0

    return v0
.end method
