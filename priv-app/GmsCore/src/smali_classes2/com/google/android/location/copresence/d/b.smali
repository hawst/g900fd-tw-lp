.class public final Lcom/google/android/location/copresence/d/b;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/location/copresence/z;


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x12
.end annotation


# instance fields
.field private final b:Lcom/google/android/location/copresence/d/j;

.field private final c:Lcom/google/android/gms/common/internal/w;

.field private final d:Lcom/google/android/location/copresence/d/f;

.field private e:Lcom/google/android/location/copresence/ab;


# direct methods
.method constructor <init>(Lcom/google/android/location/copresence/d/j;)V
    .locals 4

    .prologue
    .line 66
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    new-instance v0, Lcom/google/android/location/copresence/d/c;

    invoke-direct {v0, p0}, Lcom/google/android/location/copresence/d/c;-><init>(Lcom/google/android/location/copresence/d/b;)V

    iput-object v0, p0, Lcom/google/android/location/copresence/d/b;->d:Lcom/google/android/location/copresence/d/f;

    .line 67
    iput-object p1, p0, Lcom/google/android/location/copresence/d/b;->b:Lcom/google/android/location/copresence/d/j;

    .line 68
    new-instance v0, Lcom/google/android/gms/common/internal/w;

    sget v1, Lcom/google/android/gms/common/internal/w;->a:I

    int-to-long v2, v1

    sget-object v1, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-direct {v0, v2, v3, v1}, Lcom/google/android/gms/common/internal/w;-><init>(JLjava/util/concurrent/TimeUnit;)V

    iput-object v0, p0, Lcom/google/android/location/copresence/d/b;->c:Lcom/google/android/gms/common/internal/w;

    .line 73
    return-void
.end method

.method static synthetic a(Lcom/google/android/location/copresence/d/b;)Lcom/google/android/gms/common/internal/w;
    .locals 1

    .prologue
    .line 29
    iget-object v0, p0, Lcom/google/android/location/copresence/d/b;->c:Lcom/google/android/gms/common/internal/w;

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/location/copresence/d/b;Lcom/google/android/gms/location/copresence/x;I)V
    .locals 2

    .prologue
    .line 29
    iget-object v0, p0, Lcom/google/android/location/copresence/d/b;->e:Lcom/google/android/location/copresence/ab;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/location/copresence/d/b;->b:Lcom/google/android/location/copresence/d/j;

    invoke-virtual {v0}, Lcom/google/android/location/copresence/d/j;->f()Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Lcom/google/android/location/copresence/d/d;

    invoke-direct {v1, p0, p1, p2}, Lcom/google/android/location/copresence/d/d;-><init>(Lcom/google/android/location/copresence/d/b;Lcom/google/android/gms/location/copresence/x;I)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    :cond_0
    return-void
.end method

.method static synthetic b(Lcom/google/android/location/copresence/d/b;)Lcom/google/android/location/copresence/ab;
    .locals 1

    .prologue
    .line 29
    iget-object v0, p0, Lcom/google/android/location/copresence/d/b;->e:Lcom/google/android/location/copresence/ab;

    return-object v0
.end method


# virtual methods
.method public final a(Lcom/google/android/location/copresence/ab;Lcom/google/android/location/copresence/as;Lcom/google/android/location/copresence/q/al;)V
    .locals 2

    .prologue
    .line 88
    invoke-virtual {p0}, Lcom/google/android/location/copresence/d/b;->b()Z

    move-result v0

    if-nez v0, :cond_0

    .line 89
    new-instance v0, Lcom/google/android/location/copresence/ac;

    invoke-direct {v0}, Lcom/google/android/location/copresence/ac;-><init>()V

    throw v0

    .line 91
    :cond_0
    iput-object p1, p0, Lcom/google/android/location/copresence/d/b;->e:Lcom/google/android/location/copresence/ab;

    .line 94
    iget-object v0, p0, Lcom/google/android/location/copresence/d/b;->b:Lcom/google/android/location/copresence/d/j;

    iget-object v1, p0, Lcom/google/android/location/copresence/d/b;->d:Lcom/google/android/location/copresence/d/f;

    invoke-virtual {v0, v1, p3}, Lcom/google/android/location/copresence/d/j;->a(Lcom/google/android/location/copresence/d/f;Lcom/google/android/location/copresence/q/al;)V

    .line 95
    const/4 v0, 0x3

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 96
    const-string v0, "E2E Listen: step 2c) Listen on BLE Advertisement scan started."

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->b(Ljava/lang/String;)I

    .line 98
    :cond_1
    return-void
.end method

.method public final a(Lcom/google/android/location/copresence/q/al;)V
    .locals 2

    .prologue
    .line 102
    iget-object v0, p0, Lcom/google/android/location/copresence/d/b;->b:Lcom/google/android/location/copresence/d/j;

    iget-object v1, p0, Lcom/google/android/location/copresence/d/b;->d:Lcom/google/android/location/copresence/d/f;

    invoke-virtual {v0, v1, p1}, Lcom/google/android/location/copresence/d/j;->b(Lcom/google/android/location/copresence/d/f;Lcom/google/android/location/copresence/q/al;)V

    .line 103
    return-void
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 77
    const/4 v0, 0x1

    return v0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 82
    invoke-static {}, Lcom/google/android/location/copresence/f/b;->b()Lcom/google/ac/b/c/g;

    move-result-object v0

    iget-object v0, v0, Lcom/google/ac/b/c/g;->e:Lcom/google/ac/b/c/m;

    iget-object v0, v0, Lcom/google/ac/b/c/m;->f:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method

.method public final c()V
    .locals 0

    .prologue
    .line 108
    return-void
.end method
