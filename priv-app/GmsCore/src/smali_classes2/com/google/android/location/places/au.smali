.class public final Lcom/google/android/location/places/au;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/location/places/b;


# instance fields
.field private final a:Lcom/google/android/gms/location/places/internal/d;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/location/places/internal/d;)V
    .locals 0

    .prologue
    .line 631
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 632
    iput-object p1, p0, Lcom/google/android/location/places/au;->a:Lcom/google/android/gms/location/places/internal/d;

    .line 633
    return-void
.end method


# virtual methods
.method public final a(ILjava/util/List;)V
    .locals 4

    .prologue
    .line 637
    invoke-static {}, Lcom/google/android/gms/common/data/j;->g()Lcom/google/android/gms/common/data/m;

    move-result-object v1

    .line 638
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/location/places/AutocompletePrediction;

    .line 639
    invoke-static {v1, v0}, Lcom/google/android/gms/common/data/j;->a(Lcom/google/android/gms/common/data/m;Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;)V

    goto :goto_0

    .line 641
    :cond_0
    invoke-virtual {v1, p1}, Lcom/google/android/gms/common/data/m;->a(I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v1

    .line 643
    :try_start_0
    iget-object v0, p0, Lcom/google/android/location/places/au;->a:Lcom/google/android/gms/location/places/internal/d;

    invoke-interface {v0, v1}, Lcom/google/android/gms/location/places/internal/d;->b(Lcom/google/android/gms/common/data/DataHolder;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 650
    invoke-virtual {v1}, Lcom/google/android/gms/common/data/DataHolder;->j()V

    .line 651
    :goto_1
    return-void

    .line 644
    :catch_0
    move-exception v0

    .line 646
    :try_start_1
    const-string v2, "Places"

    const/4 v3, 0x4

    invoke-static {v2, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 647
    const-string v2, "Places"

    const-string v3, "query suggestion callback failed"

    invoke-static {v2, v3, v0}, Lcom/google/android/location/n/aa;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 650
    :cond_1
    invoke-virtual {v1}, Lcom/google/android/gms/common/data/DataHolder;->j()V

    goto :goto_1

    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Lcom/google/android/gms/common/data/DataHolder;->j()V

    throw v0
.end method
