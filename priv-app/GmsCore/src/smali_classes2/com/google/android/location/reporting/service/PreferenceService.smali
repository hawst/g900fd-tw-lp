.class public Lcom/google/android/location/reporting/service/PreferenceService;
.super Landroid/app/Service;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/android/location/reporting/service/t;

.field private b:Lcom/google/android/location/reporting/config/h;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 42
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 46
    new-instance v0, Lcom/google/android/location/reporting/service/t;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/android/location/reporting/service/t;-><init>(Lcom/google/android/location/reporting/service/PreferenceService;B)V

    iput-object v0, p0, Lcom/google/android/location/reporting/service/PreferenceService;->a:Lcom/google/android/location/reporting/service/t;

    .line 97
    return-void
.end method

.method static synthetic a(Lcom/google/android/location/reporting/service/PreferenceService;)Lcom/google/android/location/reporting/config/h;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/google/android/location/reporting/service/PreferenceService;->b:Lcom/google/android/location/reporting/config/h;

    return-object v0
.end method

.method public static a(Landroid/content/Context;Landroid/content/ServiceConnection;)V
    .locals 4

    .prologue
    .line 51
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/location/reporting/service/PreferenceService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 52
    invoke-static {}, Lcom/google/android/gms/common/stats/b;->a()Lcom/google/android/gms/common/stats/b;

    move-result-object v1

    const/16 v2, 0x81

    invoke-virtual {v1, p0, v0, p1, v2}, Lcom/google/android/gms/common/stats/b;->a(Landroid/content/Context;Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    move-result v0

    .line 55
    if-nez v0, :cond_0

    .line 56
    const-string v1, "GCoreUlr"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "PreferenceService.bindTo() bound: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/google/android/location/reporting/b/d;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 58
    :cond_0
    return-void
.end method


# virtual methods
.method protected dump(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 81
    invoke-super {p0, p1, p2, p3}, Landroid/app/Service;->dump(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V

    .line 82
    const-string v0, "GCoreUlr"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 83
    const-string v0, "GCoreUlr"

    const-string v1, "PreferenceService dumping...."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 85
    :cond_0
    const-string v0, "PreferenceService ULR dump...."

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 86
    invoke-static {}, Lcom/google/android/location/reporting/service/DispatchingService;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 87
    const-string v0, "PreferenceService deferring to DispatchingService for dump"

    .line 88
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 89
    const-string v1, "GCoreUlr"

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 95
    :goto_0
    return-void

    .line 91
    :cond_1
    invoke-static {p0}, Lcom/google/android/location/reporting/s;->a(Landroid/content/Context;)Lcom/google/android/location/reporting/s;

    move-result-object v0

    .line 93
    iget-object v1, p0, Lcom/google/android/location/reporting/service/PreferenceService;->b:Lcom/google/android/location/reporting/config/h;

    invoke-static {p2, p0, v1, v0}, Lcom/google/android/location/reporting/b/n;->a(Ljava/io/PrintWriter;Landroid/content/Context;Lcom/google/android/location/reporting/config/h;Lcom/google/android/location/reporting/s;)V

    goto :goto_0
.end method

.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 2

    .prologue
    .line 69
    const-string v0, "GCoreUlr"

    const/4 v1, 0x2

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 70
    const-string v0, "GCoreUlr"

    const-string v1, "PreferenceService.onBind"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 72
    :cond_0
    iget-object v0, p0, Lcom/google/android/location/reporting/service/PreferenceService;->a:Lcom/google/android/location/reporting/service/t;

    return-object v0
.end method

.method public onCreate()V
    .locals 1

    .prologue
    .line 62
    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    .line 63
    invoke-static {p0}, Lcom/google/android/location/reporting/b/n;->a(Landroid/content/Context;)V

    .line 64
    invoke-static {p0}, Lcom/google/android/location/reporting/config/h;->a(Landroid/content/Context;)Lcom/google/android/location/reporting/config/h;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/reporting/service/PreferenceService;->b:Lcom/google/android/location/reporting/config/h;

    .line 65
    return-void
.end method
