.class public final Lcom/google/android/location/reporting/b/l;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final a:Ljava/util/Random;

.field private static b:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 306
    new-instance v0, Ljava/util/Random;

    invoke-direct {v0}, Ljava/util/Random;-><init>()V

    sput-object v0, Lcom/google/android/location/reporting/b/l;->a:Ljava/util/Random;

    return-void
.end method

.method private static a(Landroid/location/Location;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 770
    if-nez p0, :cond_0

    .line 771
    const-string v0, "null"

    .line 775
    :goto_0
    return-object v0

    .line 772
    :cond_0
    invoke-virtual {p0}, Landroid/location/Location;->getAccuracy()F

    move-result v0

    const/4 v1, 0x0

    cmpg-float v0, v0, v1

    if-gtz v0, :cond_1

    .line 773
    const-string v0, "no_accuracy"

    goto :goto_0

    .line 775
    :cond_1
    const-string v0, "has_accuracy"

    goto :goto_0
.end method

.method public static a(Lcom/google/android/location/reporting/config/Conditions;)Ljava/lang/String;
    .locals 4

    .prologue
    .line 1087
    invoke-virtual {p0}, Lcom/google/android/location/reporting/config/Conditions;->i()Ljava/util/List;

    move-result-object v0

    .line 1088
    invoke-static {v0}, Lcom/google/android/location/reporting/b/l;->a(Ljava/util/Collection;)Ljava/lang/String;

    move-result-object v1

    .line 1089
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "clear:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1091
    invoke-static {v0}, Lcom/google/android/gms/location/reporting/InactiveReason;->a(Ljava/lang/Iterable;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1093
    invoke-static {}, Lcom/google/android/location/reporting/b/n;->a()Ljava/lang/String;

    move-result-object v0

    .line 1094
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "_"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    if-eqz v0, :cond_0

    :goto_0
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1096
    :goto_1
    return-object v0

    .line 1094
    :cond_0
    const-string v0, "unknown-country"

    goto :goto_0

    :cond_1
    move-object v0, v1

    goto :goto_1
.end method

.method private static a(Ljava/lang/String;Ljava/lang/Throwable;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 363
    move-object v0, p1

    :goto_0
    invoke-virtual {v0}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object v1

    if-eqz v1, :cond_2

    instance-of v1, v0, Lcom/google/android/location/reporting/f;

    if-nez v1, :cond_0

    instance-of v1, v0, Ljava/util/concurrent/ExecutionException;

    if-nez v1, :cond_0

    instance-of v1, v0, Ljava/io/IOException;

    if-eqz v1, :cond_1

    :cond_0
    const/4 v1, 0x1

    :goto_1
    if-eqz v1, :cond_2

    .line 364
    invoke-virtual {v0}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object v0

    goto :goto_0

    .line 363
    :cond_1
    const/4 v1, 0x0

    goto :goto_1

    .line 366
    :cond_2
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    .line 369
    instance-of v2, v0, Lcom/android/volley/ac;

    if-eqz v2, :cond_4

    .line 371
    check-cast v0, Lcom/android/volley/ac;

    .line 372
    iget-object v1, v0, Lcom/android/volley/ac;->a:Lcom/android/volley/m;

    if-eqz v1, :cond_3

    .line 373
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "VolleyError:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v0, v0, Lcom/android/volley/ac;->a:Lcom/android/volley/m;

    iget v0, v0, Lcom/android/volley/m;->a:I

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 382
    :goto_2
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 375
    :cond_3
    const-string v0, "VolleyError:null-response"

    goto :goto_2

    .line 377
    :cond_4
    instance-of v2, v0, Lcom/google/android/gms/auth/ae;

    if-eqz v2, :cond_5

    .line 378
    const-string v0, "UserRecoverableAuthException"

    goto :goto_2

    .line 379
    :cond_5
    instance-of v0, v0, Lcom/google/android/gms/auth/q;

    if-eqz v0, :cond_6

    .line 380
    const-string v0, "GoogleAuthException"

    goto :goto_2

    :cond_6
    move-object v0, v1

    goto :goto_2
.end method

.method private static a(Ljava/util/Collection;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 1066
    new-instance v1, Ljava/util/TreeSet;

    invoke-direct {v1}, Ljava/util/TreeSet;-><init>()V

    .line 1067
    invoke-interface {p0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/location/reporting/InactiveReason;

    .line 1068
    invoke-virtual {v0}, Lcom/google/android/gms/location/reporting/InactiveReason;->b()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1070
    :cond_0
    const-string v0, "_"

    invoke-static {v0, v1}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static a()V
    .locals 5

    .prologue
    .line 497
    const-string v0, "gmm_read"

    const-string v1, "success"

    const-wide/16 v2, 0x1

    const/4 v4, 0x0

    invoke-static {v0, v1, v2, v3, v4}, Lcom/google/android/location/reporting/b/l;->a(Ljava/lang/String;Ljava/lang/String;JZ)V

    .line 498
    return-void
.end method

.method public static a(I)V
    .locals 4

    .prologue
    .line 624
    const-string v0, "metadata_created"

    int-to-long v2, p0

    const/4 v1, 0x1

    invoke-static {v0, v2, v3, v1}, Lcom/google/android/location/reporting/b/l;->a(Ljava/lang/String;JZ)V

    .line 625
    return-void
.end method

.method private static a(Landroid/accounts/Account;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 1056
    const-string v0, "GCoreUlrLong"

    const/4 v1, 0x4

    invoke-static {v0, v1}, Lcom/google/android/location/reporting/b/d;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1059
    const-string v0, "GCoreUlrLong"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {p0}, Lcom/google/android/gms/location/reporting/a/d;->a(Landroid/accounts/Account;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ": reporting "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/location/reporting/b/d;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 1061
    :cond_0
    const-string v0, "controller"

    const-wide/16 v2, 0x1

    const/4 v1, 0x0

    invoke-static {v0, p1, v2, v3, v1}, Lcom/google/android/location/reporting/b/l;->a(Ljava/lang/String;Ljava/lang/String;JZ)V

    .line 1062
    return-void
.end method

.method public static a(Landroid/content/ComponentName;)V
    .locals 3

    .prologue
    .line 459
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Landroid/content/ComponentName;->getShortClassName()Ljava/lang/String;

    move-result-object v0

    .line 460
    :goto_0
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "start_svc:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 461
    invoke-static {v0}, Lcom/google/android/location/reporting/b/l;->d(Ljava/lang/String;)V

    .line 462
    return-void

    .line 459
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 318
    invoke-static {p0}, Lcom/google/android/gms/common/a/d;->a(Landroid/content/Context;)V

    .line 319
    sget-object v0, Lcom/google/android/location/reporting/service/ab;->S:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    sput v0, Lcom/google/android/location/reporting/b/l;->b:I

    .line 320
    return-void
.end method

.method public static a(Lcom/google/android/location/reporting/a/l;)V
    .locals 2

    .prologue
    .line 482
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "wifi_unknown_auth_type:"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v1, p0, Lcom/google/android/location/reporting/a/l;->f:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 483
    invoke-static {v0}, Lcom/google/android/location/reporting/b/l;->d(Ljava/lang/String;)V

    .line 484
    return-void
.end method

.method public static a(Lcom/google/android/location/reporting/a;)V
    .locals 5

    .prologue
    .line 539
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "new_account:"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/android/location/reporting/a;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 540
    const-string v1, "sync"

    const-wide/16 v2, 0x1

    const/4 v4, 0x0

    invoke-static {v1, v0, v2, v3, v4}, Lcom/google/android/location/reporting/b/l;->a(Ljava/lang/String;Ljava/lang/String;JZ)V

    .line 541
    return-void
.end method

.method public static a(Lcom/google/android/location/reporting/a;Ljava/util/List;Ljava/util/List;Ljava/util/ArrayList;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 838
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "settings_invalid:"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/android/location/reporting/a;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 839
    const-string v1, "upload"

    const-wide/16 v2, 0x1

    invoke-static {v1, v0, v2, v3, v4}, Lcom/google/android/location/reporting/b/l;->a(Ljava/lang/String;Ljava/lang/String;JZ)V

    .line 840
    const-string v0, "location_upload_invalid_settings"

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v1

    int-to-long v2, v1

    invoke-static {v0, v2, v3, v4}, Lcom/google/android/location/reporting/b/l;->a(Ljava/lang/String;JZ)V

    .line 841
    const-string v0, "activity_upload_invalid_settings"

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v1

    int-to-long v2, v1

    invoke-static {v0, v2, v3, v4}, Lcom/google/android/location/reporting/b/l;->a(Ljava/lang/String;JZ)V

    .line 842
    const-string v0, "metadata_upload_invalid_settings"

    invoke-virtual {p3}, Ljava/util/ArrayList;->size()I

    move-result v1

    int-to-long v2, v1

    invoke-static {v0, v2, v3, v4}, Lcom/google/android/location/reporting/b/l;->a(Ljava/lang/String;JZ)V

    .line 843
    return-void
.end method

.method public static a(Lcom/google/android/location/reporting/config/ReportingConfig;)V
    .locals 4

    .prologue
    .line 594
    const-string v0, "location_received"

    invoke-virtual {p0}, Lcom/google/android/location/reporting/config/ReportingConfig;->getActiveAccounts()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    int-to-long v2, v1

    const/4 v1, 0x1

    invoke-static {v0, v2, v3, v1}, Lcom/google/android/location/reporting/b/l;->a(Ljava/lang/String;JZ)V

    .line 595
    return-void
.end method

.method public static a(Lcom/google/android/location/reporting/config/ReportingConfig;Lcom/google/android/location/reporting/a/f;)V
    .locals 4

    .prologue
    .line 614
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/google/android/location/reporting/a/f;->c()I

    move-result v0

    if-lez v0, :cond_0

    .line 615
    const-string v0, "wifi_attached"

    invoke-virtual {p0}, Lcom/google/android/location/reporting/config/ReportingConfig;->getActiveAccounts()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    int-to-long v2, v1

    const/4 v1, 0x1

    invoke-static {v0, v2, v3, v1}, Lcom/google/android/location/reporting/b/l;->a(Ljava/lang/String;JZ)V

    .line 617
    :cond_0
    return-void
.end method

.method public static a(Lcom/google/android/location/reporting/config/ReportingConfig;Lcom/google/android/location/reporting/config/ReportingConfig;)V
    .locals 5

    .prologue
    .line 1021
    if-nez p0, :cond_1

    .line 1046
    :cond_0
    return-void

    .line 1027
    :cond_1
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    .line 1028
    invoke-virtual {p0}, Lcom/google/android/location/reporting/config/ReportingConfig;->getActiveAccounts()Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 1029
    invoke-virtual {p1}, Lcom/google/android/location/reporting/config/ReportingConfig;->getActiveAccounts()Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 1031
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_2
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/accounts/Account;

    .line 1032
    invoke-virtual {p0, v0}, Lcom/google/android/location/reporting/config/ReportingConfig;->a(Landroid/accounts/Account;)Lcom/google/android/location/reporting/config/AccountConfig;

    move-result-object v2

    .line 1033
    invoke-virtual {p1, v0}, Lcom/google/android/location/reporting/config/ReportingConfig;->a(Landroid/accounts/Account;)Lcom/google/android/location/reporting/config/AccountConfig;

    move-result-object v3

    .line 1034
    if-nez v2, :cond_3

    .line 1035
    const-string v2, "activated:accountAdded"

    invoke-static {v0, v2}, Lcom/google/android/location/reporting/b/l;->a(Landroid/accounts/Account;Ljava/lang/String;)V

    goto :goto_0

    .line 1036
    :cond_3
    if-nez v3, :cond_4

    .line 1037
    const-string v2, "inactivated:accountRemoved"

    invoke-static {v0, v2}, Lcom/google/android/location/reporting/b/l;->a(Landroid/accounts/Account;Ljava/lang/String;)V

    goto :goto_0

    .line 1038
    :cond_4
    invoke-virtual {v2}, Lcom/google/android/location/reporting/config/AccountConfig;->z()Z

    move-result v4

    if-nez v4, :cond_5

    invoke-virtual {v3}, Lcom/google/android/location/reporting/config/AccountConfig;->z()Z

    move-result v4

    if-eqz v4, :cond_5

    .line 1039
    invoke-virtual {v2}, Lcom/google/android/location/reporting/config/AccountConfig;->w()Ljava/util/List;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/location/reporting/b/l;->a(Ljava/util/Collection;)Ljava/lang/String;

    move-result-object v2

    .line 1040
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "activated:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/google/android/location/reporting/b/l;->a(Landroid/accounts/Account;Ljava/lang/String;)V

    goto :goto_0

    .line 1041
    :cond_5
    invoke-virtual {v2}, Lcom/google/android/location/reporting/config/AccountConfig;->z()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-virtual {v3}, Lcom/google/android/location/reporting/config/AccountConfig;->z()Z

    move-result v2

    if-nez v2, :cond_2

    .line 1042
    invoke-virtual {v3}, Lcom/google/android/location/reporting/config/AccountConfig;->w()Ljava/util/List;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/location/reporting/b/l;->a(Ljava/util/Collection;)Ljava/lang/String;

    move-result-object v2

    .line 1043
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "inactivated:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/google/android/location/reporting/b/l;->a(Landroid/accounts/Account;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static a(Lcom/google/android/location/reporting/config/ReportingConfig;Lcom/google/android/location/reporting/config/ReportingConfig;Ljava/lang/String;)V
    .locals 6

    .prologue
    .line 1112
    invoke-virtual {p0}, Lcom/google/android/location/reporting/config/ReportingConfig;->h()Ljava/util/Set;

    move-result-object v0

    .line 1113
    invoke-virtual {p1}, Lcom/google/android/location/reporting/config/ReportingConfig;->h()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 1115
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/accounts/Account;

    .line 1116
    invoke-virtual {p0, v0}, Lcom/google/android/location/reporting/config/ReportingConfig;->a(Landroid/accounts/Account;)Lcom/google/android/location/reporting/config/AccountConfig;

    move-result-object v2

    .line 1117
    invoke-virtual {p1, v0}, Lcom/google/android/location/reporting/config/ReportingConfig;->a(Landroid/accounts/Account;)Lcom/google/android/location/reporting/config/AccountConfig;

    move-result-object v0

    .line 1118
    if-eqz v2, :cond_0

    if-eqz v0, :cond_0

    .line 1119
    invoke-virtual {v2}, Lcom/google/android/location/reporting/config/AccountConfig;->r()I

    move-result v3

    .line 1120
    invoke-virtual {v0}, Lcom/google/android/location/reporting/config/AccountConfig;->r()I

    move-result v4

    .line 1121
    const-string v5, "reporting:"

    invoke-static {v5, v3, v4, p2}, Lcom/google/android/location/reporting/b/l;->a(Ljava/lang/String;IILjava/lang/String;)V

    .line 1123
    invoke-virtual {v2}, Lcom/google/android/location/reporting/config/AccountConfig;->s()I

    move-result v2

    .line 1124
    invoke-virtual {v0}, Lcom/google/android/location/reporting/config/AccountConfig;->s()I

    move-result v0

    .line 1125
    const-string v3, "history:"

    invoke-static {v3, v2, v0, p2}, Lcom/google/android/location/reporting/b/l;->a(Ljava/lang/String;IILjava/lang/String;)V

    goto :goto_0

    .line 1128
    :cond_0
    const-string v3, "GCoreUlr"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Accounts changed during setting update: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, " -> "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, Lcom/google/android/location/reporting/b/d;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1130
    const-string v0, "account_change_during_update"

    const-string v2, "error"

    const-wide/16 v4, 0x1

    const/4 v3, 0x0

    invoke-static {v2, v0, v4, v5, v3}, Lcom/google/android/location/reporting/b/l;->a(Ljava/lang/String;Ljava/lang/String;JZ)V

    goto :goto_0

    .line 1133
    :cond_1
    return-void
.end method

.method public static a(Lcom/google/android/location/reporting/config/ReportingConfig;Ljava/util/List;)V
    .locals 4

    .prologue
    .line 603
    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 604
    const-string v0, "wifi_received"

    invoke-virtual {p0}, Lcom/google/android/location/reporting/config/ReportingConfig;->getActiveAccounts()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    int-to-long v2, v1

    const/4 v1, 0x1

    invoke-static {v0, v2, v3, v1}, Lcom/google/android/location/reporting/b/l;->a(Ljava/lang/String;JZ)V

    .line 606
    :cond_0
    return-void
.end method

.method public static a(Lcom/google/android/location/reporting/e;)V
    .locals 4

    .prologue
    .line 632
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ":save_attempt"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 633
    const-wide/16 v2, 0x1

    const/4 v1, 0x1

    invoke-static {v0, v2, v3, v1}, Lcom/google/android/location/reporting/b/l;->a(Ljava/lang/String;JZ)V

    .line 634
    return-void
.end method

.method public static a(Lcom/google/android/location/reporting/e;I)V
    .locals 4

    .prologue
    .line 658
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ":evicted"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 659
    int-to-long v2, p1

    const/4 v1, 0x1

    invoke-static {v0, v2, v3, v1}, Lcom/google/android/location/reporting/b/l;->a(Ljava/lang/String;JZ)V

    .line 660
    return-void
.end method

.method public static a(Lcom/google/android/location/reporting/e;Ljava/lang/Exception;)V
    .locals 1

    .prologue
    .line 650
    const-string v0, ":save:"

    invoke-static {v0, p0, p1}, Lcom/google/android/location/reporting/b/l;->a(Ljava/lang/String;Lcom/google/android/location/reporting/e;Ljava/lang/Exception;)V

    .line 651
    return-void
.end method

.method public static a(Lcom/google/android/location/reporting/service/q;Lcom/google/android/location/reporting/config/ReportingConfig;Landroid/location/Location;Lcom/google/android/location/reporting/u;Lcom/google/android/location/reporting/i;)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 751
    invoke-virtual {p1}, Lcom/google/android/location/reporting/config/ReportingConfig;->getActiveAccounts()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    int-to-long v2, v0

    .line 753
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "prev_location:"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/android/location/reporting/service/q;->h()Landroid/location/Location;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/location/reporting/b/l;->a(Landroid/location/Location;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 755
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v0, v1, v5}, Lcom/google/android/location/reporting/b/l;->a(Ljava/lang/String;Ljava/lang/Long;Z)V

    .line 756
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "location:"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {p2}, Lcom/google/android/location/reporting/b/l;->a(Landroid/location/Location;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 757
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v0, v1, v5}, Lcom/google/android/location/reporting/b/l;->a(Ljava/lang/String;Ljava/lang/Long;Z)V

    .line 758
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "request:"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p4}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v0, p3, Lcom/google/android/location/reporting/u;->a:Z

    if-eqz v0, :cond_0

    const-string v0, "moved"

    :goto_0
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, ":"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v0, p3, Lcom/google/android/location/reporting/u;->b:I

    packed-switch v0, :pswitch_data_0

    const-string v0, "unknown_reason"

    :goto_1
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 760
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v0, v1, v5}, Lcom/google/android/location/reporting/b/l;->a(Ljava/lang/String;Ljava/lang/Long;Z)V

    .line 761
    return-void

    .line 758
    :cond_0
    const-string v0, "stationary"

    goto :goto_0

    :pswitch_0
    const-string v0, "no_accuracy"

    goto :goto_1

    :pswitch_1
    const-string v0, "location_changed"

    goto :goto_1

    :pswitch_2
    const-string v0, "insufficient_accuracy"

    goto :goto_1

    :pswitch_3
    const-string v0, "sufficient_accuracy"

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public static a(Ljava/lang/Exception;)V
    .locals 2

    .prologue
    .line 468
    const-string v0, "wtf_wifi"

    const-string v1, "wifi_attachment:"

    invoke-static {v0, v1, p0}, Lcom/google/android/location/reporting/b/l;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    .line 469
    return-void
.end method

.method public static a(Ljava/lang/RuntimeException;)V
    .locals 1

    .prologue
    .line 863
    const-string v0, "exception:"

    invoke-static {v0, p0}, Lcom/google/android/location/reporting/b/l;->a(Ljava/lang/String;Ljava/lang/Exception;)V

    .line 865
    return-void
.end method

.method public static a(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 995
    const-string v0, "flp"

    const-wide/16 v2, 0x1

    const/4 v1, 0x0

    invoke-static {v0, p0, v2, v3, v1}, Lcom/google/android/location/reporting/b/l;->a(Ljava/lang/String;Ljava/lang/String;JZ)V

    .line 996
    return-void
.end method

.method private static a(Ljava/lang/String;I)V
    .locals 4

    .prologue
    .line 1165
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "server_location:"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    int-to-long v2, p1

    const/4 v1, 0x0

    invoke-static {v0, v2, v3, v1}, Lcom/google/android/location/reporting/b/l;->a(Ljava/lang/String;JZ)V

    .line 1166
    return-void
.end method

.method private static a(Ljava/lang/String;IILjava/lang/String;)V
    .locals 6

    .prologue
    const-wide/16 v4, 0x1

    const/4 v2, 0x0

    .line 1141
    if-eq p1, p2, :cond_0

    .line 1142
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {p1}, Lcom/google/android/gms/location/reporting/m;->b(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "_to_"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {p2}, Lcom/google/android/gms/location/reporting/m;->b(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1148
    invoke-static {v0, p3, v4, v5, v2}, Lcom/google/android/location/reporting/b/l;->a(Ljava/lang/String;Ljava/lang/String;JZ)V

    .line 1149
    invoke-static {p3, v0, v4, v5, v2}, Lcom/google/android/location/reporting/b/l;->a(Ljava/lang/String;Ljava/lang/String;JZ)V

    .line 1151
    :cond_0
    return-void
.end method

.method public static a(Ljava/lang/String;J)V
    .locals 3

    .prologue
    .line 979
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "recreate_rows_lost:"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 980
    const/4 v1, 0x0

    invoke-static {v0, p1, p2, v1}, Lcom/google/android/location/reporting/b/l;->a(Ljava/lang/String;JZ)V

    .line 981
    return-void
.end method

.method private static a(Ljava/lang/String;JZ)V
    .locals 3

    .prologue
    .line 570
    const-wide/16 v0, 0x0

    cmp-long v0, p1, v0

    if-nez v0, :cond_0

    .line 575
    :goto_0
    return-void

    .line 574
    :cond_0
    const-string v0, "entity"

    invoke-static {v0, p0, p1, p2, p3}, Lcom/google/android/location/reporting/b/l;->a(Ljava/lang/String;Ljava/lang/String;JZ)V

    goto :goto_0
.end method

.method private static a(Ljava/lang/String;Lcom/google/android/location/reporting/e;Ljava/lang/Exception;)V
    .locals 4

    .prologue
    .line 584
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 585
    invoke-static {v0, p2}, Lcom/google/android/location/reporting/b/l;->a(Ljava/lang/String;Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v0

    .line 586
    const-wide/16 v2, 0x1

    const/4 v1, 0x0

    invoke-static {v0, v2, v3, v1}, Lcom/google/android/location/reporting/b/l;->a(Ljava/lang/String;JZ)V

    .line 588
    return-void
.end method

.method private static a(Ljava/lang/String;Ljava/lang/Exception;)V
    .locals 4

    .prologue
    .line 846
    invoke-static {p0, p1}, Lcom/google/android/location/reporting/b/l;->a(Ljava/lang/String;Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v0

    .line 847
    const-string v1, "exception:VolleyError:null-response"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    .line 848
    const-wide/16 v2, 0x1

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-static {v0, v2, v1}, Lcom/google/android/location/reporting/b/l;->a(Ljava/lang/String;Ljava/lang/Long;Z)V

    .line 849
    return-void
.end method

.method public static a(Ljava/lang/String;Ljava/lang/Long;)V
    .locals 1

    .prologue
    .line 731
    const-string v0, "attempt"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "success"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "no_connection"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    .line 733
    :goto_0
    invoke-static {p0, p1, v0}, Lcom/google/android/location/reporting/b/l;->a(Ljava/lang/String;Ljava/lang/Long;Z)V

    .line 734
    return-void

    .line 731
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static a(Ljava/lang/String;Ljava/lang/Long;Z)V
    .locals 4

    .prologue
    .line 740
    const-string v0, "upload"

    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v0, p0, v2, v3, p2}, Lcom/google/android/location/reporting/b/l;->a(Ljava/lang/String;Ljava/lang/String;JZ)V

    .line 741
    return-void
.end method

.method private static a(Ljava/lang/String;Ljava/lang/String;JZ)V
    .locals 4

    .prologue
    const/4 v2, 0x3

    .line 339
    sget v0, Lcom/google/android/location/reporting/b/l;->b:I

    if-gtz v0, :cond_1

    .line 340
    const-string v0, "GCoreUlr"

    invoke-static {v0, v2}, Lcom/google/android/location/reporting/b/d;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 341
    const-string v0, "GCoreUlr"

    const-string v1, "UlrAnalytics.init() must be called."

    invoke-static {v0, v1}, Lcom/google/android/location/reporting/b/d;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 355
    :cond_0
    :goto_0
    return-void

    .line 345
    :cond_1
    if-eqz p4, :cond_0

    .line 346
    sget-object v0, Lcom/google/android/location/reporting/b/l;->a:Ljava/util/Random;

    sget v1, Lcom/google/android/location/reporting/b/l;->b:I

    invoke-virtual {v0, v1}, Ljava/util/Random;->nextInt(I)I

    move-result v0

    if-lez v0, :cond_2

    const/4 v0, 0x1

    :goto_1
    if-eqz v0, :cond_3

    .line 347
    const-string v0, "GCoreUlr"

    invoke-static {v0, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 348
    const-string v0, "GCoreUlr"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Throttling analytics event: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 346
    :cond_2
    const/4 v0, 0x0

    goto :goto_1

    .line 352
    :cond_3
    sget v0, Lcom/google/android/location/reporting/b/l;->b:I

    goto :goto_0
.end method

.method private static a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V
    .locals 4

    .prologue
    .line 402
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ""

    invoke-static {v1, p2}, Lcom/google/android/location/reporting/b/l;->a(Ljava/lang/String;Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 403
    const-wide/16 v2, 0x1

    const/4 v1, 0x0

    invoke-static {p0, v0, v2, v3, v1}, Lcom/google/android/location/reporting/b/l;->a(Ljava/lang/String;Ljava/lang/String;JZ)V

    .line 405
    return-void
.end method

.method public static a(Ljava/lang/Throwable;)V
    .locals 6

    .prologue
    .line 443
    invoke-virtual {p0}, Ljava/lang/Throwable;->getStackTrace()[Ljava/lang/StackTraceElement;

    move-result-object v2

    .line 444
    if-eqz v2, :cond_1

    const-class v0, Lcom/google/android/location/reporting/b/d;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v3

    array-length v4, v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v4, :cond_1

    aget-object v0, v2, v1

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/StackTraceElement;->getClassName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 448
    :goto_1
    if-eqz v0, :cond_2

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0}, Ljava/lang/StackTraceElement;->getClassName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Ljava/lang/StackTraceElement;->getMethodName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 450
    :goto_2
    invoke-static {v0, p0}, Lcom/google/android/location/reporting/b/l;->a(Ljava/lang/String;Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v0

    .line 451
    invoke-static {v0}, Lcom/google/android/location/reporting/b/l;->d(Ljava/lang/String;)V

    .line 453
    return-void

    .line 444
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    .line 448
    :cond_2
    const-string v0, "unknown:"

    goto :goto_2
.end method

.method public static a(Ljava/util/List;Ljava/util/List;Ljava/util/List;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 785
    const-string v0, "success"

    const-wide/16 v2, 0x1

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/location/reporting/b/l;->a(Ljava/lang/String;Ljava/lang/Long;)V

    .line 786
    const-string v0, "location_uploaded"

    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v1

    int-to-long v2, v1

    invoke-static {v0, v2, v3, v4}, Lcom/google/android/location/reporting/b/l;->a(Ljava/lang/String;JZ)V

    .line 787
    const-string v0, "activity_uploaded"

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v1

    int-to-long v2, v1

    invoke-static {v0, v2, v3, v4}, Lcom/google/android/location/reporting/b/l;->a(Ljava/lang/String;JZ)V

    .line 788
    const-string v0, "metadata_uploaded"

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v1

    int-to-long v2, v1

    invoke-static {v0, v2, v3, v4}, Lcom/google/android/location/reporting/b/l;->a(Ljava/lang/String;JZ)V

    .line 789
    return-void
.end method

.method public static a(Z)V
    .locals 5

    .prologue
    .line 521
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "sync_dirty_retry:"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 522
    const-string v1, "sync"

    const-wide/16 v2, 0x1

    const/4 v4, 0x0

    invoke-static {v1, v0, v2, v3, v4}, Lcom/google/android/location/reporting/b/l;->a(Ljava/lang/String;Ljava/lang/String;JZ)V

    .line 523
    return-void
.end method

.method public static b()V
    .locals 5

    .prologue
    .line 505
    const-string v0, "gmm_read"

    const-string v1, "skipped"

    const-wide/16 v2, 0x1

    const/4 v4, 0x0

    invoke-static {v0, v1, v2, v3, v4}, Lcom/google/android/location/reporting/b/l;->a(Ljava/lang/String;Ljava/lang/String;JZ)V

    .line 506
    return-void
.end method

.method public static b(I)V
    .locals 1

    .prologue
    .line 1169
    const-string v0, "missing"

    invoke-static {v0, p0}, Lcom/google/android/location/reporting/b/l;->a(Ljava/lang/String;I)V

    .line 1170
    return-void
.end method

.method public static b(Lcom/google/android/location/reporting/a;)V
    .locals 6

    .prologue
    .line 548
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "not_dirty:"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/android/location/reporting/a;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 549
    const-string v1, "success"

    invoke-virtual {p0}, Lcom/google/android/location/reporting/a;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    .line 550
    const-string v2, "sync"

    const-wide/16 v4, 0x1

    invoke-static {v2, v0, v4, v5, v1}, Lcom/google/android/location/reporting/b/l;->a(Ljava/lang/String;Ljava/lang/String;JZ)V

    .line 551
    return-void
.end method

.method public static b(Lcom/google/android/location/reporting/config/ReportingConfig;)V
    .locals 4

    .prologue
    .line 620
    const-string v0, "activity_received"

    invoke-virtual {p0}, Lcom/google/android/location/reporting/config/ReportingConfig;->getActiveAccounts()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    int-to-long v2, v1

    const/4 v1, 0x1

    invoke-static {v0, v2, v3, v1}, Lcom/google/android/location/reporting/b/l;->a(Ljava/lang/String;JZ)V

    .line 621
    return-void
.end method

.method public static b(Lcom/google/android/location/reporting/e;)V
    .locals 4

    .prologue
    .line 640
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ":saved"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 641
    const-wide/16 v2, 0x1

    const/4 v1, 0x1

    invoke-static {v0, v2, v3, v1}, Lcom/google/android/location/reporting/b/l;->a(Ljava/lang/String;JZ)V

    .line 642
    return-void
.end method

.method public static b(Lcom/google/android/location/reporting/e;I)V
    .locals 4

    .prologue
    .line 667
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ":expired"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 668
    int-to-long v2, p1

    const/4 v1, 0x0

    invoke-static {v0, v2, v3, v1}, Lcom/google/android/location/reporting/b/l;->a(Ljava/lang/String;JZ)V

    .line 669
    return-void
.end method

.method public static b(Lcom/google/android/location/reporting/e;Ljava/lang/Exception;)V
    .locals 1

    .prologue
    .line 715
    const-string v0, ":deserialize:"

    invoke-static {v0, p0, p1}, Lcom/google/android/location/reporting/b/l;->a(Ljava/lang/String;Lcom/google/android/location/reporting/e;Ljava/lang/Exception;)V

    .line 716
    return-void
.end method

.method public static b(Ljava/lang/Exception;)V
    .locals 2

    .prologue
    .line 475
    const-string v0, "wtf_wifi"

    const-string v1, "wifi_scan_conversion:"

    invoke-static {v0, v1, p0}, Lcom/google/android/location/reporting/b/l;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    .line 476
    return-void
.end method

.method public static b(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 1052
    const-string v0, "ble"

    const-wide/16 v2, 0x1

    const/4 v1, 0x0

    invoke-static {v0, p0, v2, v3, v1}, Lcom/google/android/location/reporting/b/l;->a(Ljava/lang/String;Ljava/lang/String;JZ)V

    .line 1053
    return-void
.end method

.method private static b(Ljava/lang/String;Lcom/google/android/location/reporting/e;Ljava/lang/Exception;)V
    .locals 5

    .prologue
    .line 908
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 909
    invoke-static {v0, p2}, Lcom/google/android/location/reporting/b/l;->a(Ljava/lang/String;Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v0

    .line 910
    const-string v1, "db"

    const-wide/16 v2, 0x1

    const/4 v4, 0x0

    invoke-static {v1, v0, v2, v3, v4}, Lcom/google/android/location/reporting/b/l;->a(Ljava/lang/String;Ljava/lang/String;JZ)V

    .line 912
    return-void
.end method

.method public static b(Ljava/util/List;Ljava/util/List;Ljava/util/List;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 799
    if-eqz p0, :cond_1

    .line 800
    const-string v0, "location_sent"

    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v1

    int-to-long v2, v1

    invoke-static {v0, v2, v3, v4}, Lcom/google/android/location/reporting/b/l;->a(Ljava/lang/String;JZ)V

    .line 801
    const/4 v0, 0x0

    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/ulr/ApiLocationReading;

    invoke-virtual {v0}, Lcom/google/android/ulr/ApiLocationReading;->getReadingInfo()Lcom/google/android/ulr/ApiReadingInfo;

    move-result-object v0

    if-eqz v0, :cond_5

    invoke-virtual {v0}, Lcom/google/android/ulr/ApiReadingInfo;->getWifiScans()Ljava/util/ArrayList;

    move-result-object v0

    if-eqz v0, :cond_5

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_5

    add-int/lit8 v1, v1, 0x1

    move v0, v1

    :goto_1
    move v1, v0

    goto :goto_0

    :cond_0
    if-lez v1, :cond_4

    const-string v0, "wifi_sent"

    int-to-long v2, v1

    invoke-static {v0, v2, v3, v4}, Lcom/google/android/location/reporting/b/l;->a(Ljava/lang/String;JZ)V

    .line 803
    :cond_1
    :goto_2
    if-eqz p1, :cond_2

    .line 804
    const-string v0, "activity_sent"

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v1

    int-to-long v2, v1

    invoke-static {v0, v2, v3, v4}, Lcom/google/android/location/reporting/b/l;->a(Ljava/lang/String;JZ)V

    .line 806
    :cond_2
    if-eqz p2, :cond_3

    .line 807
    const-string v0, "metadata_sent"

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v1

    int-to-long v2, v1

    invoke-static {v0, v2, v3, v4}, Lcom/google/android/location/reporting/b/l;->a(Ljava/lang/String;JZ)V

    .line 809
    :cond_3
    return-void

    .line 801
    :cond_4
    const-string v0, "GCoreUlr"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "GCoreUlr"

    const-string v1, "Analytics: no wifis sent"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    :cond_5
    move v0, v1

    goto :goto_1
.end method

.method public static c(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 1079
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "opt_in:"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static c()V
    .locals 5

    .prologue
    .line 890
    const-string v0, "should_undefine_action"

    const-string v1, "sleep"

    const-wide/16 v2, 0x1

    const/4 v4, 0x0

    invoke-static {v0, v1, v2, v3, v4}, Lcom/google/android/location/reporting/b/l;->a(Ljava/lang/String;Ljava/lang/String;JZ)V

    .line 891
    return-void
.end method

.method public static c(I)V
    .locals 1

    .prologue
    .line 1173
    const-string v0, "extra"

    invoke-static {v0, p0}, Lcom/google/android/location/reporting/b/l;->a(Ljava/lang/String;I)V

    .line 1174
    return-void
.end method

.method public static c(Lcom/google/android/location/reporting/a;)V
    .locals 5

    .prologue
    .line 557
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "dirty:"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/android/location/reporting/a;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 558
    const-string v1, "sync"

    const-wide/16 v2, 0x1

    const/4 v4, 0x0

    invoke-static {v1, v0, v2, v3, v4}, Lcom/google/android/location/reporting/b/l;->a(Ljava/lang/String;Ljava/lang/String;JZ)V

    .line 559
    return-void
.end method

.method public static c(Lcom/google/android/location/reporting/e;)V
    .locals 4

    .prologue
    .line 675
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ":overflow"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 676
    const-wide/16 v2, 0x1

    const/4 v1, 0x0

    invoke-static {v0, v2, v3, v1}, Lcom/google/android/location/reporting/b/l;->a(Ljava/lang/String;JZ)V

    .line 677
    return-void
.end method

.method public static c(Lcom/google/android/location/reporting/e;I)V
    .locals 4

    .prologue
    .line 685
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ":obsolete"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 686
    int-to-long v2, p1

    const/4 v1, 0x0

    invoke-static {v0, v2, v3, v1}, Lcom/google/android/location/reporting/b/l;->a(Ljava/lang/String;JZ)V

    .line 687
    return-void
.end method

.method public static c(Lcom/google/android/location/reporting/e;Ljava/lang/Exception;)V
    .locals 1

    .prologue
    .line 927
    const-string v0, "retrieve_entity:"

    invoke-static {v0, p0, p1}, Lcom/google/android/location/reporting/b/l;->b(Ljava/lang/String;Lcom/google/android/location/reporting/e;Ljava/lang/Exception;)V

    .line 928
    return-void
.end method

.method public static c(Ljava/lang/Exception;)V
    .locals 2

    .prologue
    .line 490
    const-string v0, "gmm_read"

    const-string v1, ""

    invoke-static {v0, v1, p0}, Lcom/google/android/location/reporting/b/l;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    .line 491
    return-void
.end method

.method public static d()V
    .locals 5

    .prologue
    .line 898
    const-string v0, "should_undefine_action"

    const-string v1, "eligible_after_sleep"

    const-wide/16 v2, 0x1

    const/4 v4, 0x0

    invoke-static {v0, v1, v2, v3, v4}, Lcom/google/android/location/reporting/b/l;->a(Ljava/lang/String;Ljava/lang/String;JZ)V

    .line 899
    return-void
.end method

.method public static d(I)V
    .locals 1

    .prologue
    .line 1177
    const-string v0, "errors"

    invoke-static {v0, p0}, Lcom/google/android/location/reporting/b/l;->a(Ljava/lang/String;I)V

    .line 1178
    return-void
.end method

.method public static d(Lcom/google/android/location/reporting/e;I)V
    .locals 4

    .prologue
    .line 695
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ":deserialize_attempt"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 696
    int-to-long v2, p1

    const/4 v1, 0x1

    invoke-static {v0, v2, v3, v1}, Lcom/google/android/location/reporting/b/l;->a(Ljava/lang/String;JZ)V

    .line 697
    return-void
.end method

.method public static d(Lcom/google/android/location/reporting/e;Ljava/lang/Exception;)V
    .locals 1

    .prologue
    .line 942
    const-string v0, "clear_store:"

    invoke-static {v0, p0, p1}, Lcom/google/android/location/reporting/b/l;->b(Ljava/lang/String;Lcom/google/android/location/reporting/e;Ljava/lang/Exception;)V

    .line 943
    return-void
.end method

.method public static d(Ljava/lang/Exception;)V
    .locals 5

    .prologue
    .line 529
    const-string v0, "exception:"

    invoke-static {v0, p0}, Lcom/google/android/location/reporting/b/l;->a(Ljava/lang/String;Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v0

    .line 530
    const-string v1, "sync"

    const-wide/16 v2, 0x1

    const/4 v4, 0x0

    invoke-static {v1, v0, v2, v3, v4}, Lcom/google/android/location/reporting/b/l;->a(Ljava/lang/String;Ljava/lang/String;JZ)V

    .line 532
    return-void
.end method

.method private static d(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 394
    const-string v0, "wtf"

    const-wide/16 v2, 0x1

    const/4 v1, 0x0

    invoke-static {v0, p0, v2, v3, v1}, Lcom/google/android/location/reporting/b/l;->a(Ljava/lang/String;Ljava/lang/String;JZ)V

    .line 395
    return-void
.end method

.method public static e()V
    .locals 1

    .prologue
    .line 918
    const-string v0, "retrieve_entity:"

    invoke-static {v0}, Lcom/google/android/location/reporting/b/l;->e(Ljava/lang/String;)V

    .line 919
    return-void
.end method

.method public static e(Lcom/google/android/location/reporting/e;I)V
    .locals 4

    .prologue
    .line 705
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ":deserialized"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 706
    int-to-long v2, p1

    const/4 v1, 0x1

    invoke-static {v0, v2, v3, v1}, Lcom/google/android/location/reporting/b/l;->a(Ljava/lang/String;JZ)V

    .line 707
    return-void
.end method

.method public static e(Lcom/google/android/location/reporting/e;Ljava/lang/Exception;)V
    .locals 1

    .prologue
    .line 957
    const-string v0, "clear_old:"

    invoke-static {v0, p0, p1}, Lcom/google/android/location/reporting/b/l;->b(Ljava/lang/String;Lcom/google/android/location/reporting/e;Ljava/lang/Exception;)V

    .line 958
    return-void
.end method

.method public static e(Ljava/lang/Exception;)V
    .locals 1

    .prologue
    .line 856
    const-string v0, "exception:"

    invoke-static {v0, p0}, Lcom/google/android/location/reporting/b/l;->a(Ljava/lang/String;Ljava/lang/Exception;)V

    .line 857
    return-void
.end method

.method private static e(Ljava/lang/String;)V
    .locals 5

    .prologue
    .line 902
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "attempt"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 903
    const-string v1, "db"

    const-wide/16 v2, 0x1

    const/4 v4, 0x1

    invoke-static {v1, v0, v2, v3, v4}, Lcom/google/android/location/reporting/b/l;->a(Ljava/lang/String;Ljava/lang/String;JZ)V

    .line 904
    return-void
.end method

.method public static f()V
    .locals 1

    .prologue
    .line 934
    const-string v0, "clear_store:"

    invoke-static {v0}, Lcom/google/android/location/reporting/b/l;->e(Ljava/lang/String;)V

    .line 935
    return-void
.end method

.method public static f(Lcom/google/android/location/reporting/e;I)V
    .locals 4

    .prologue
    .line 723
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ":deleted_after_use"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 724
    int-to-long v2, p1

    const/4 v1, 0x1

    invoke-static {v0, v2, v3, v1}, Lcom/google/android/location/reporting/b/l;->a(Ljava/lang/String;JZ)V

    .line 725
    return-void
.end method

.method public static f(Lcom/google/android/location/reporting/e;Ljava/lang/Exception;)V
    .locals 1

    .prologue
    .line 972
    const-string v0, "restrict_to:"

    invoke-static {v0, p0, p1}, Lcom/google/android/location/reporting/b/l;->b(Ljava/lang/String;Lcom/google/android/location/reporting/e;Ljava/lang/Exception;)V

    .line 973
    return-void
.end method

.method public static f(Ljava/lang/Exception;)V
    .locals 1

    .prologue
    .line 872
    const-string v0, "clear_db_exception:"

    invoke-static {v0, p0}, Lcom/google/android/location/reporting/b/l;->a(Ljava/lang/String;Ljava/lang/Exception;)V

    .line 874
    return-void
.end method

.method public static g()V
    .locals 1

    .prologue
    .line 949
    const-string v0, "clear_old:"

    invoke-static {v0}, Lcom/google/android/location/reporting/b/l;->e(Ljava/lang/String;)V

    .line 950
    return-void
.end method

.method public static g(Ljava/lang/Exception;)V
    .locals 1

    .prologue
    .line 881
    const-string v0, "read_db_exception:"

    invoke-static {v0, p0}, Lcom/google/android/location/reporting/b/l;->a(Ljava/lang/String;Ljava/lang/Exception;)V

    .line 883
    return-void
.end method

.method public static h()V
    .locals 1

    .prologue
    .line 964
    const-string v0, "restrict_to:"

    invoke-static {v0}, Lcom/google/android/location/reporting/b/l;->e(Ljava/lang/String;)V

    .line 965
    return-void
.end method

.method public static i()V
    .locals 5

    .prologue
    .line 1003
    const-string v0, "ambiguous_setting_notification"

    const-string v1, "shown"

    const-wide/16 v2, 0x1

    const/4 v4, 0x0

    invoke-static {v0, v1, v2, v3, v4}, Lcom/google/android/location/reporting/b/l;->a(Ljava/lang/String;Ljava/lang/String;JZ)V

    .line 1004
    return-void
.end method

.method public static j()V
    .locals 5

    .prologue
    .line 1011
    const-string v0, "ambiguous_setting_notification"

    const-string v1, "click"

    const-wide/16 v2, 0x1

    const/4 v4, 0x0

    invoke-static {v0, v1, v2, v3, v4}, Lcom/google/android/location/reporting/b/l;->a(Ljava/lang/String;Ljava/lang/String;JZ)V

    .line 1012
    return-void
.end method

.method public static k()V
    .locals 5

    .prologue
    .line 1158
    const-string v0, "obsolete"

    const-string v1, "device_tag"

    const-wide/16 v2, 0x1

    const/4 v4, 0x0

    invoke-static {v0, v1, v2, v3, v4}, Lcom/google/android/location/reporting/b/l;->a(Ljava/lang/String;Ljava/lang/String;JZ)V

    .line 1159
    return-void
.end method

.method public static l()V
    .locals 5

    .prologue
    .line 1181
    const-string v0, "sync"

    const-string v1, "gcm_notification_received"

    const-wide/16 v2, 0x1

    const/4 v4, 0x0

    invoke-static {v0, v1, v2, v3, v4}, Lcom/google/android/location/reporting/b/l;->a(Ljava/lang/String;Ljava/lang/String;JZ)V

    .line 1182
    return-void
.end method

.method public static m()V
    .locals 5

    .prologue
    .line 1185
    const-string v0, "sync"

    const-string v1, "gcm_notification_old_account"

    const-wide/16 v2, 0x1

    const/4 v4, 0x0

    invoke-static {v0, v1, v2, v3, v4}, Lcom/google/android/location/reporting/b/l;->a(Ljava/lang/String;Ljava/lang/String;JZ)V

    .line 1186
    return-void
.end method
