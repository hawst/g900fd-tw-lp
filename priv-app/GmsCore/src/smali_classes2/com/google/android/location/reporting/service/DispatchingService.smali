.class public Lcom/google/android/location/reporting/service/DispatchingService;
.super Landroid/app/Service;
.source "SourceFile"


# static fields
.field private static a:Z


# instance fields
.field private b:Lcom/google/android/location/reporting/config/h;

.field private c:Lcom/google/android/location/reporting/s;

.field private d:Lcom/google/android/location/reporting/service/q;

.field private e:Lcom/google/android/location/n/ae;

.field private f:Lcom/google/android/location/fused/g;

.field private g:Lcom/google/android/location/reporting/service/e;

.field private h:Lcom/google/android/location/reporting/service/g;

.field private i:Landroid/content/BroadcastReceiver;

.field private j:Lcom/google/android/location/reporting/LocationRecordStore;

.field private k:Lcom/google/android/location/reporting/DetectedActivityStore;

.field private l:Lcom/google/android/location/reporting/ApiMetadataStore;

.field private m:Lcom/google/android/location/reporting/LocationReportingController;

.field private n:Landroid/os/PowerManager$WakeLock;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 64
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 606
    return-void
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 196
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/location/reporting/service/DispatchingService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 197
    invoke-virtual {v0, p1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 198
    return-object v0
.end method

.method static synthetic a(Lcom/google/android/location/reporting/service/DispatchingService;)Lcom/google/android/location/n/ae;
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, Lcom/google/android/location/reporting/service/DispatchingService;->e:Lcom/google/android/location/n/ae;

    return-object v0
.end method

.method static synthetic a(Landroid/content/Intent;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 64
    const-string v0, "source"

    invoke-virtual {p0, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "source"

    invoke-virtual {p0, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 235
    const-string v0, "com.google.android.location.reporting.ACTION_APPLY_UPLOAD_REQUESTS"

    invoke-static {p0, v0}, Lcom/google/android/location/reporting/service/DispatchingService;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 236
    invoke-static {p0, v0}, Lcom/google/android/location/reporting/b/n;->a(Landroid/content/Context;Landroid/content/Intent;)Z

    .line 237
    return-void
.end method

.method public static a(Landroid/content/Context;Lcom/google/android/gms/location/LocationStatus;)V
    .locals 2

    .prologue
    .line 243
    const-string v0, "com.google.android.location.reporting.ACTION_LOCATION_STATUS"

    invoke-static {p0, v0}, Lcom/google/android/location/reporting/service/DispatchingService;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 244
    const-string v1, "status"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 245
    invoke-static {p0, v0}, Lcom/google/android/location/reporting/b/n;->a(Landroid/content/Context;Landroid/content/Intent;)Z

    .line 246
    return-void
.end method

.method public static a(Landroid/content/Context;Ljava/util/ArrayList;)V
    .locals 2

    .prologue
    .line 252
    const-string v0, "com.google.android.location.reporting.ACTION_BLE_SCAN"

    invoke-static {p0, v0}, Lcom/google/android/location/reporting/service/DispatchingService;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 253
    const-string v1, "ble_scan"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putParcelableArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    .line 254
    invoke-static {p0, v0}, Lcom/google/android/location/reporting/b/n;->a(Landroid/content/Context;Landroid/content/Intent;)Z

    .line 255
    return-void
.end method

.method static synthetic a(Ljava/lang/RuntimeException;)V
    .locals 2

    .prologue
    .line 64
    const-string v0, "GCoreUlr"

    const-string v1, "Unexpected BLE exception in DispatchingService"

    invoke-static {v0, v1, p0}, Lcom/google/android/location/reporting/b/d;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    return-void
.end method

.method public static a()Z
    .locals 1

    .prologue
    .line 185
    sget-boolean v0, Lcom/google/android/location/reporting/service/DispatchingService;->a:Z

    return v0
.end method

.method private static b()Landroid/os/Looper;
    .locals 3

    .prologue
    .line 313
    new-instance v0, Landroid/os/HandlerThread;

    const-string v1, "UlrDispatchingService"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;I)V

    .line 315
    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    .line 316
    invoke-virtual {v0}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v0

    return-object v0
.end method

.method static synthetic b(Lcom/google/android/location/reporting/service/DispatchingService;)Landroid/os/PowerManager$WakeLock;
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, Lcom/google/android/location/reporting/service/DispatchingService;->n:Landroid/os/PowerManager$WakeLock;

    return-object v0
.end method

.method public static b(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 261
    const-string v0, "com.google.android.location.reporting.UPLOAD"

    invoke-static {p0, v0}, Lcom/google/android/location/reporting/service/DispatchingService;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 262
    invoke-static {p0, v0}, Lcom/google/android/location/reporting/b/n;->a(Landroid/content/Context;Landroid/content/Intent;)Z

    .line 263
    return-void
.end method

.method public static b(Landroid/content/Context;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 227
    const-string v0, "com.google.android.location.reporting.ACTION_UPDATE_ACTIVE_STATE"

    invoke-static {p0, v0}, Lcom/google/android/location/reporting/service/DispatchingService;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "source"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 228
    invoke-static {p0, v0}, Lcom/google/android/location/reporting/b/n;->a(Landroid/content/Context;Landroid/content/Intent;)Z

    .line 229
    return-void
.end method

.method static synthetic c(Lcom/google/android/location/reporting/service/DispatchingService;)Lcom/google/android/location/reporting/service/q;
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, Lcom/google/android/location/reporting/service/DispatchingService;->d:Lcom/google/android/location/reporting/service/q;

    return-object v0
.end method

.method static synthetic d(Lcom/google/android/location/reporting/service/DispatchingService;)Lcom/google/android/location/reporting/s;
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, Lcom/google/android/location/reporting/service/DispatchingService;->c:Lcom/google/android/location/reporting/s;

    return-object v0
.end method

.method static synthetic e(Lcom/google/android/location/reporting/service/DispatchingService;)Lcom/google/android/location/reporting/ApiMetadataStore;
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, Lcom/google/android/location/reporting/service/DispatchingService;->l:Lcom/google/android/location/reporting/ApiMetadataStore;

    return-object v0
.end method

.method static synthetic f(Lcom/google/android/location/reporting/service/DispatchingService;)Lcom/google/android/location/reporting/LocationReportingController;
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, Lcom/google/android/location/reporting/service/DispatchingService;->m:Lcom/google/android/location/reporting/LocationReportingController;

    return-object v0
.end method

.method static synthetic g(Lcom/google/android/location/reporting/service/DispatchingService;)Lcom/google/android/location/reporting/LocationRecordStore;
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, Lcom/google/android/location/reporting/service/DispatchingService;->j:Lcom/google/android/location/reporting/LocationRecordStore;

    return-object v0
.end method

.method static synthetic h(Lcom/google/android/location/reporting/service/DispatchingService;)Lcom/google/android/location/reporting/DetectedActivityStore;
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, Lcom/google/android/location/reporting/service/DispatchingService;->k:Lcom/google/android/location/reporting/DetectedActivityStore;

    return-object v0
.end method

.method static synthetic i(Lcom/google/android/location/reporting/service/DispatchingService;)Lcom/google/android/location/reporting/config/h;
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, Lcom/google/android/location/reporting/service/DispatchingService;->b:Lcom/google/android/location/reporting/config/h;

    return-object v0
.end method

.method static synthetic j(Lcom/google/android/location/reporting/service/DispatchingService;)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 64
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/location/reporting/service/DispatchingService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    return-object v0
.end method

.method static synthetic k(Lcom/google/android/location/reporting/service/DispatchingService;)Lcom/google/android/location/fused/g;
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, Lcom/google/android/location/reporting/service/DispatchingService;->f:Lcom/google/android/location/fused/g;

    return-object v0
.end method


# virtual methods
.method protected dump(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 363
    invoke-super {p0, p1, p2, p3}, Landroid/app/Service;->dump(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V

    .line 364
    const-string v0, "GCoreUlr"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 365
    const-string v0, "GCoreUlr"

    const-string v1, "DispatchingService dumping...."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 367
    :cond_0
    const-string v0, "DispatchingService ULR dump...."

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 368
    iget-object v0, p0, Lcom/google/android/location/reporting/service/DispatchingService;->d:Lcom/google/android/location/reporting/service/q;

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    .line 369
    iget-object v0, p0, Lcom/google/android/location/reporting/service/DispatchingService;->b:Lcom/google/android/location/reporting/config/h;

    iget-object v1, p0, Lcom/google/android/location/reporting/service/DispatchingService;->c:Lcom/google/android/location/reporting/s;

    invoke-static {p2, p0, v0, v1}, Lcom/google/android/location/reporting/b/n;->a(Ljava/io/PrintWriter;Landroid/content/Context;Lcom/google/android/location/reporting/config/h;Lcom/google/android/location/reporting/s;)V

    .line 370
    return-void
.end method

.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1

    .prologue
    .line 321
    const/4 v0, 0x0

    return-object v0
.end method

.method public onCreate()V
    .locals 11

    .prologue
    const/4 v10, 0x1

    .line 267
    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    .line 270
    invoke-static {p0}, Lcom/google/android/location/reporting/b/n;->a(Landroid/content/Context;)V

    .line 272
    const-string v0, "GCoreUlr"

    const/4 v1, 0x4

    invoke-static {v0, v1}, Lcom/google/android/location/reporting/b/d;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 273
    const-string v0, "GCoreUlr"

    const-string v1, "DispatchingService.onCreate()"

    invoke-static {v0, v1}, Lcom/google/android/location/reporting/b/d;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 276
    :cond_0
    sput-boolean v10, Lcom/google/android/location/reporting/service/DispatchingService;->a:Z

    .line 277
    invoke-static {p0}, Lcom/google/android/location/reporting/config/h;->a(Landroid/content/Context;)Lcom/google/android/location/reporting/config/h;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/reporting/service/DispatchingService;->b:Lcom/google/android/location/reporting/config/h;

    .line 278
    invoke-static {p0}, Lcom/google/android/location/reporting/s;->a(Landroid/content/Context;)Lcom/google/android/location/reporting/s;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/reporting/service/DispatchingService;->c:Lcom/google/android/location/reporting/s;

    .line 279
    new-instance v0, Lcom/google/android/location/reporting/service/q;

    invoke-direct {v0}, Lcom/google/android/location/reporting/service/q;-><init>()V

    iput-object v0, p0, Lcom/google/android/location/reporting/service/DispatchingService;->d:Lcom/google/android/location/reporting/service/q;

    .line 281
    new-instance v0, Lcom/google/android/location/reporting/h;

    iget-object v1, p0, Lcom/google/android/location/reporting/service/DispatchingService;->c:Lcom/google/android/location/reporting/s;

    invoke-direct {v0, v1}, Lcom/google/android/location/reporting/h;-><init>(Lcom/google/android/location/reporting/s;)V

    .line 282
    new-instance v1, Lcom/google/android/location/reporting/LocationRecordStore;

    invoke-direct {v1, p0, v0}, Lcom/google/android/location/reporting/LocationRecordStore;-><init>(Landroid/content/Context;Lcom/google/android/location/reporting/h;)V

    iput-object v1, p0, Lcom/google/android/location/reporting/service/DispatchingService;->j:Lcom/google/android/location/reporting/LocationRecordStore;

    .line 283
    new-instance v1, Lcom/google/android/location/reporting/DetectedActivityStore;

    invoke-direct {v1, p0, v0}, Lcom/google/android/location/reporting/DetectedActivityStore;-><init>(Landroid/content/Context;Lcom/google/android/location/reporting/h;)V

    iput-object v1, p0, Lcom/google/android/location/reporting/service/DispatchingService;->k:Lcom/google/android/location/reporting/DetectedActivityStore;

    .line 284
    new-instance v1, Lcom/google/android/location/reporting/ApiMetadataStore;

    invoke-direct {v1, p0, v0}, Lcom/google/android/location/reporting/ApiMetadataStore;-><init>(Landroid/content/Context;Lcom/google/android/location/reporting/h;)V

    iput-object v1, p0, Lcom/google/android/location/reporting/service/DispatchingService;->l:Lcom/google/android/location/reporting/ApiMetadataStore;

    .line 286
    invoke-static {p0}, Lcom/google/android/location/n/ae;->a(Landroid/content/Context;)Lcom/google/android/location/n/ae;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/reporting/service/DispatchingService;->e:Lcom/google/android/location/n/ae;

    .line 287
    invoke-static {p0}, Lcom/google/android/location/fused/g;->a(Landroid/content/Context;)Lcom/google/android/location/fused/g;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/reporting/service/DispatchingService;->f:Lcom/google/android/location/fused/g;

    .line 289
    new-instance v2, Lcom/google/android/gms/common/util/r;

    invoke-direct {v2}, Lcom/google/android/gms/common/util/r;-><init>()V

    .line 291
    new-instance v0, Lcom/google/android/location/reporting/LocationReportingController;

    iget-object v3, p0, Lcom/google/android/location/reporting/service/DispatchingService;->j:Lcom/google/android/location/reporting/LocationRecordStore;

    iget-object v4, p0, Lcom/google/android/location/reporting/service/DispatchingService;->k:Lcom/google/android/location/reporting/DetectedActivityStore;

    iget-object v5, p0, Lcom/google/android/location/reporting/service/DispatchingService;->l:Lcom/google/android/location/reporting/ApiMetadataStore;

    iget-object v6, p0, Lcom/google/android/location/reporting/service/DispatchingService;->c:Lcom/google/android/location/reporting/s;

    iget-object v7, p0, Lcom/google/android/location/reporting/service/DispatchingService;->b:Lcom/google/android/location/reporting/config/h;

    iget-object v8, p0, Lcom/google/android/location/reporting/service/DispatchingService;->d:Lcom/google/android/location/reporting/service/q;

    iget-object v9, p0, Lcom/google/android/location/reporting/service/DispatchingService;->f:Lcom/google/android/location/fused/g;

    move-object v1, p0

    invoke-direct/range {v0 .. v9}, Lcom/google/android/location/reporting/LocationReportingController;-><init>(Landroid/content/Context;Lcom/google/android/gms/common/util/p;Lcom/google/android/location/reporting/e;Lcom/google/android/location/reporting/e;Lcom/google/android/location/reporting/e;Lcom/google/android/location/reporting/s;Lcom/google/android/location/reporting/config/h;Lcom/google/android/location/reporting/service/q;Lcom/google/android/location/fused/g;)V

    iput-object v0, p0, Lcom/google/android/location/reporting/service/DispatchingService;->m:Lcom/google/android/location/reporting/LocationReportingController;

    .line 295
    new-instance v0, Lcom/google/android/location/reporting/service/e;

    invoke-static {}, Lcom/google/android/location/reporting/service/DispatchingService;->b()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, p0, p0, v1}, Lcom/google/android/location/reporting/service/e;-><init>(Lcom/google/android/location/reporting/service/DispatchingService;Landroid/content/Context;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/google/android/location/reporting/service/DispatchingService;->g:Lcom/google/android/location/reporting/service/e;

    .line 296
    new-instance v0, Lcom/google/android/location/reporting/service/g;

    invoke-static {}, Lcom/google/android/location/reporting/service/DispatchingService;->b()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, p0, p0, v1}, Lcom/google/android/location/reporting/service/g;-><init>(Lcom/google/android/location/reporting/service/DispatchingService;Landroid/content/Context;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/google/android/location/reporting/service/DispatchingService;->h:Lcom/google/android/location/reporting/service/g;

    .line 298
    const-string v0, "power"

    invoke-virtual {p0, v0}, Lcom/google/android/location/reporting/service/DispatchingService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    .line 299
    const-string v1, "UlrDispatchingService"

    invoke-virtual {v0, v10, v1}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/reporting/service/DispatchingService;->n:Landroid/os/PowerManager$WakeLock;

    .line 302
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 303
    const-string v1, "com.google.android.location.activity.LOW_POWER_MODE_ENABLED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 304
    const-string v1, "com.google.android.location.activity.LOW_POWER_MODE_DISABLED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 305
    new-instance v1, Lcom/google/android/location/reporting/service/f;

    invoke-direct {v1}, Lcom/google/android/location/reporting/service/f;-><init>()V

    iput-object v1, p0, Lcom/google/android/location/reporting/service/DispatchingService;->i:Landroid/content/BroadcastReceiver;

    .line 306
    invoke-static {p0}, Landroid/support/v4/a/m;->a(Landroid/content/Context;)Landroid/support/v4/a/m;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/location/reporting/service/DispatchingService;->i:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/support/v4/a/m;->a(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)V

    .line 307
    return-void
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 351
    const/4 v0, 0x0

    sput-boolean v0, Lcom/google/android/location/reporting/service/DispatchingService;->a:Z

    .line 352
    const-string v0, "GCoreUlr"

    const/4 v1, 0x4

    invoke-static {v0, v1}, Lcom/google/android/location/reporting/b/d;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 353
    const-string v0, "GCoreUlr"

    const-string v1, "DispatchingService.onDestroy()"

    invoke-static {v0, v1}, Lcom/google/android/location/reporting/b/d;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 355
    :cond_0
    invoke-static {p0}, Landroid/support/v4/a/m;->a(Landroid/content/Context;)Landroid/support/v4/a/m;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/location/reporting/service/DispatchingService;->i:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/support/v4/a/m;->a(Landroid/content/BroadcastReceiver;)V

    .line 356
    iget-object v0, p0, Lcom/google/android/location/reporting/service/DispatchingService;->g:Lcom/google/android/location/reporting/service/e;

    invoke-virtual {v0}, Lcom/google/android/location/reporting/service/e;->a()V

    .line 357
    iget-object v0, p0, Lcom/google/android/location/reporting/service/DispatchingService;->h:Lcom/google/android/location/reporting/service/g;

    invoke-virtual {v0}, Lcom/google/android/location/reporting/service/g;->a()V

    .line 358
    iget-object v0, p0, Lcom/google/android/location/reporting/service/DispatchingService;->m:Lcom/google/android/location/reporting/LocationReportingController;

    invoke-virtual {v0}, Lcom/google/android/location/reporting/LocationReportingController;->a()V

    .line 359
    return-void
.end method

.method public onStart(Landroid/content/Intent;I)V
    .locals 2

    .prologue
    .line 326
    const-string v0, "GCoreUlr"

    const-string v1, "We don\'t support Froyo, this shouldn\'t be called"

    invoke-static {v0, v1}, Lcom/google/android/location/reporting/b/d;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 327
    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 3

    .prologue
    const/4 v2, 0x2

    .line 331
    if-nez p1, :cond_0

    .line 346
    :goto_0
    return v2

    .line 340
    :cond_0
    const-string v0, "com.google.android.location.reporting.UPLOAD"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/location/reporting/service/DispatchingService;->h:Lcom/google/android/location/reporting/service/g;

    .line 341
    :goto_1
    invoke-virtual {v0}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v1

    .line 342
    iput p3, v1, Landroid/os/Message;->arg1:I

    .line 343
    iput-object p1, v1, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 344
    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0

    .line 340
    :cond_1
    iget-object v0, p0, Lcom/google/android/location/reporting/service/DispatchingService;->g:Lcom/google/android/location/reporting/service/e;

    goto :goto_1
.end method
