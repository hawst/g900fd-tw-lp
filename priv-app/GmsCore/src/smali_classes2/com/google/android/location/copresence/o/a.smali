.class public final Lcom/google/android/location/copresence/o/a;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/location/copresence/o/o;


# instance fields
.field final a:Ljava/util/HashMap;

.field final b:Ljava/util/HashMap;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 175
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 176
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/location/copresence/o/a;->a:Ljava/util/HashMap;

    .line 177
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/location/copresence/o/a;->b:Ljava/util/HashMap;

    .line 178
    return-void
.end method

.method private b(JII)V
    .locals 3

    .prologue
    .line 245
    iget-object v0, p0, Lcom/google/android/location/copresence/o/a;->b:Ljava/util/HashMap;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/copresence/o/b;

    .line 246
    if-nez v0, :cond_1

    .line 247
    const/4 v0, 0x3

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 248
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "CopresenceCallbackCache: Untracked operationId="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", networkStatus="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->b(Ljava/lang/String;)I

    .line 254
    :cond_0
    :goto_0
    return-void

    .line 253
    :cond_1
    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/google/android/location/copresence/o/b;->a(JII)V

    goto :goto_0
.end method


# virtual methods
.method public final a(JII)V
    .locals 1

    .prologue
    .line 290
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/android/location/copresence/o/a;->b(JII)V

    .line 291
    return-void
.end method

.method public final a(JILjava/lang/String;I)V
    .locals 1

    .prologue
    .line 281
    invoke-direct {p0, p1, p2, p3, p5}, Lcom/google/android/location/copresence/o/a;->b(JII)V

    .line 282
    return-void
.end method

.method public final a(Lcom/google/android/location/copresence/o/b;)V
    .locals 4

    .prologue
    .line 233
    if-nez p1, :cond_0

    .line 241
    :goto_0
    return-void

    .line 236
    :cond_0
    iget-object v0, p1, Lcom/google/android/location/copresence/o/b;->b:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    .line 237
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    .line 238
    iget-object v0, p0, Lcom/google/android/location/copresence/o/a;->b:Ljava/util/HashMap;

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 240
    :cond_1
    iget-object v0, p0, Lcom/google/android/location/copresence/o/a;->a:Ljava/util/HashMap;

    iget-object v1, p1, Lcom/google/android/location/copresence/o/b;->a:Lcom/google/android/gms/location/copresence/internal/i;

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method
