.class final Lcom/google/android/location/places/b/f;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final a:Lcom/google/android/location/places/NearbyAlertSubscription;

.field final b:Lcom/google/android/gms/maps/model/LatLngBounds;

.field final c:Lcom/google/android/location/places/b/j;

.field final d:J

.field e:Z

.field final synthetic f:Lcom/google/android/location/places/b/d;

.field private final g:Lcom/google/android/location/places/aj;


# direct methods
.method constructor <init>(Lcom/google/android/location/places/b/d;Lcom/google/android/location/places/NearbyAlertSubscription;Lcom/google/android/gms/maps/model/LatLngBounds;J)V
    .locals 4

    .prologue
    .line 168
    iput-object p1, p0, Lcom/google/android/location/places/b/f;->f:Lcom/google/android/location/places/b/d;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 268
    new-instance v0, Lcom/google/android/location/places/b/h;

    invoke-direct {v0, p0}, Lcom/google/android/location/places/b/h;-><init>(Lcom/google/android/location/places/b/f;)V

    iput-object v0, p0, Lcom/google/android/location/places/b/f;->g:Lcom/google/android/location/places/aj;

    .line 169
    iput-object p2, p0, Lcom/google/android/location/places/b/f;->a:Lcom/google/android/location/places/NearbyAlertSubscription;

    .line 170
    iput-object p3, p0, Lcom/google/android/location/places/b/f;->b:Lcom/google/android/gms/maps/model/LatLngBounds;

    .line 171
    iput-wide p4, p0, Lcom/google/android/location/places/b/f;->d:J

    .line 172
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/location/places/b/f;->e:Z

    .line 173
    new-instance v1, Lcom/google/android/location/places/b/j;

    invoke-virtual {p3}, Lcom/google/android/gms/maps/model/LatLngBounds;->b()Lcom/google/android/gms/maps/model/LatLng;

    move-result-object v2

    sget-object v0, Lcom/google/android/location/x;->u:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    invoke-direct {v1, v2, v0}, Lcom/google/android/location/places/b/j;-><init>(Lcom/google/android/gms/maps/model/LatLng;F)V

    iput-object v1, p0, Lcom/google/android/location/places/b/f;->c:Lcom/google/android/location/places/b/j;

    .line 175
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 7

    .prologue
    .line 183
    iget-object v0, p0, Lcom/google/android/location/places/b/f;->a:Lcom/google/android/location/places/NearbyAlertSubscription;

    invoke-virtual {v0}, Lcom/google/android/location/places/NearbyAlertSubscription;->b()Lcom/google/android/gms/location/places/PlaceFilter;

    move-result-object v0

    .line 185
    :try_start_0
    invoke-virtual {v0}, Lcom/google/android/gms/location/places/PlaceFilter;->b()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    .line 186
    iget-object v1, p0, Lcom/google/android/location/places/b/f;->f:Lcom/google/android/location/places/b/d;

    invoke-static {v1}, Lcom/google/android/location/places/b/d;->a(Lcom/google/android/location/places/b/d;)Lcom/google/android/location/places/f;

    move-result-object v1

    invoke-virtual {v0}, Lcom/google/android/gms/location/places/PlaceFilter;->b()Ljava/util/Set;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/location/places/b/f;->a:Lcom/google/android/location/places/NearbyAlertSubscription;

    invoke-virtual {v2}, Lcom/google/android/location/places/NearbyAlertSubscription;->a()Lcom/google/android/gms/location/places/internal/PlacesParams;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/location/places/b/f;->g:Lcom/google/android/location/places/aj;

    invoke-virtual {v1, v0, v2, v3}, Lcom/google/android/location/places/f;->a(Ljava/util/Collection;Lcom/google/android/gms/location/places/internal/PlacesParams;Lcom/google/android/location/places/aj;)V

    .line 205
    :cond_0
    :goto_0
    return-void

    .line 191
    :cond_1
    iget-object v0, p0, Lcom/google/android/location/places/b/f;->f:Lcom/google/android/location/places/b/d;

    invoke-static {v0}, Lcom/google/android/location/places/b/d;->a(Lcom/google/android/location/places/b/d;)Lcom/google/android/location/places/f;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/location/places/b/f;->b:Lcom/google/android/gms/maps/model/LatLngBounds;

    const/16 v2, 0x64

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/android/location/places/b/f;->a:Lcom/google/android/location/places/NearbyAlertSubscription;

    invoke-virtual {v4}, Lcom/google/android/location/places/NearbyAlertSubscription;->b()Lcom/google/android/gms/location/places/PlaceFilter;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/location/places/b/f;->a:Lcom/google/android/location/places/NearbyAlertSubscription;

    invoke-virtual {v5}, Lcom/google/android/location/places/NearbyAlertSubscription;->a()Lcom/google/android/gms/location/places/internal/PlacesParams;

    move-result-object v5

    iget-object v6, p0, Lcom/google/android/location/places/b/f;->g:Lcom/google/android/location/places/aj;

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/location/places/f;->a(Lcom/google/android/gms/maps/model/LatLngBounds;ILjava/lang/String;Lcom/google/android/gms/location/places/PlaceFilter;Lcom/google/android/gms/location/places/internal/PlacesParams;Lcom/google/android/location/places/aj;)V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 199
    :catch_0
    move-exception v0

    .line 201
    const-string v1, "Places"

    const/4 v2, 0x3

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 202
    const-string v1, "Places"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Caught IllegalStateException - "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/google/android/location/n/aa;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
