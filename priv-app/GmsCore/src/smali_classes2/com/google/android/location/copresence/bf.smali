.class public final Lcom/google/android/location/copresence/bf;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final a:Ljava/util/HashSet;

.field private b:Z

.field private final c:Lcom/google/android/location/copresence/i/c;


# direct methods
.method public constructor <init>(ZLcom/google/android/location/copresence/i/c;)V
    .locals 1

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/location/copresence/bf;->a:Ljava/util/HashSet;

    .line 32
    iput-boolean p1, p0, Lcom/google/android/location/copresence/bf;->b:Z

    .line 33
    iput-object p2, p0, Lcom/google/android/location/copresence/bf;->c:Lcom/google/android/location/copresence/i/c;

    .line 34
    return-void
.end method

.method private a(II)V
    .locals 3

    .prologue
    .line 93
    if-nez p2, :cond_2

    if-lez p1, :cond_2

    .line 95
    iget-boolean v0, p0, Lcom/google/android/location/copresence/bf;->b:Z

    if-eqz v0, :cond_1

    .line 96
    iget-object v0, p0, Lcom/google/android/location/copresence/bf;->c:Lcom/google/android/location/copresence/i/c;

    iget-object v1, v0, Lcom/google/android/location/copresence/i/c;->a:Ljava/lang/Object;

    monitor-enter v1

    const/4 v2, 0x0

    :try_start_0
    iput-boolean v2, v0, Lcom/google/android/location/copresence/i/c;->c:Z

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Lcom/google/android/location/copresence/i/c;->a(Z)V

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 108
    :cond_0
    :goto_0
    return-void

    .line 96
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 98
    :cond_1
    iget-object v0, p0, Lcom/google/android/location/copresence/bf;->c:Lcom/google/android/location/copresence/i/c;

    iget-object v1, v0, Lcom/google/android/location/copresence/i/c;->a:Ljava/lang/Object;

    monitor-enter v1

    const/4 v2, 0x0

    :try_start_1
    iput-boolean v2, v0, Lcom/google/android/location/copresence/i/c;->b:Z

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Lcom/google/android/location/copresence/i/c;->a(Z)V

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    goto :goto_0

    :catchall_1
    move-exception v0

    monitor-exit v1

    throw v0

    .line 100
    :cond_2
    if-lez p2, :cond_0

    if-nez p1, :cond_0

    .line 102
    iget-boolean v0, p0, Lcom/google/android/location/copresence/bf;->b:Z

    if-eqz v0, :cond_3

    .line 103
    iget-object v0, p0, Lcom/google/android/location/copresence/bf;->c:Lcom/google/android/location/copresence/i/c;

    iget-object v1, v0, Lcom/google/android/location/copresence/i/c;->a:Ljava/lang/Object;

    monitor-enter v1

    const/4 v2, 0x1

    :try_start_2
    iput-boolean v2, v0, Lcom/google/android/location/copresence/i/c;->c:Z

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Lcom/google/android/location/copresence/i/c;->a(Z)V

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    goto :goto_0

    :catchall_2
    move-exception v0

    monitor-exit v1

    throw v0

    .line 105
    :cond_3
    iget-object v0, p0, Lcom/google/android/location/copresence/bf;->c:Lcom/google/android/location/copresence/i/c;

    iget-object v1, v0, Lcom/google/android/location/copresence/i/c;->a:Ljava/lang/Object;

    monitor-enter v1

    const/4 v2, 0x1

    :try_start_3
    iput-boolean v2, v0, Lcom/google/android/location/copresence/i/c;->b:Z

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Lcom/google/android/location/copresence/i/c;->a(Z)V

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_3

    goto :goto_0

    :catchall_3
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    .line 40
    iget-object v0, p0, Lcom/google/android/location/copresence/bf;->a:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->size()I

    move-result v0

    .line 41
    iget-object v1, p0, Lcom/google/android/location/copresence/bf;->a:Ljava/util/HashSet;

    invoke-virtual {v1}, Ljava/util/HashSet;->clear()V

    .line 42
    iget-object v1, p0, Lcom/google/android/location/copresence/bf;->a:Ljava/util/HashSet;

    invoke-virtual {v1}, Ljava/util/HashSet;->size()I

    move-result v1

    invoke-direct {p0, v0, v1}, Lcom/google/android/location/copresence/bf;->a(II)V

    .line 43
    const/4 v1, 0x2

    invoke-static {v1}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 44
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "StartedMediumTracker: Called clear() with previousSize = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->a(Ljava/lang/String;)I

    .line 46
    :cond_0
    return-void
.end method

.method public final a(I)V
    .locals 3

    .prologue
    .line 53
    iget-object v0, p0, Lcom/google/android/location/copresence/bf;->a:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->size()I

    move-result v0

    .line 54
    iget-object v1, p0, Lcom/google/android/location/copresence/bf;->a:Ljava/util/HashSet;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    .line 55
    iget-object v1, p0, Lcom/google/android/location/copresence/bf;->a:Ljava/util/HashSet;

    invoke-virtual {v1}, Ljava/util/HashSet;->size()I

    move-result v1

    invoke-direct {p0, v0, v1}, Lcom/google/android/location/copresence/bf;->a(II)V

    .line 56
    const/4 v1, 0x2

    invoke-static {v1}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 57
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "StartedMediumTracker: Called remove() with previousSize = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " on tokenMedium "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->a(Ljava/lang/String;)I

    .line 60
    :cond_0
    return-void
.end method

.method public final b(I)V
    .locals 3

    .prologue
    .line 67
    iget-object v0, p0, Lcom/google/android/location/copresence/bf;->a:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->size()I

    move-result v0

    .line 68
    iget-object v1, p0, Lcom/google/android/location/copresence/bf;->a:Ljava/util/HashSet;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 69
    iget-object v1, p0, Lcom/google/android/location/copresence/bf;->a:Ljava/util/HashSet;

    invoke-virtual {v1}, Ljava/util/HashSet;->size()I

    move-result v1

    invoke-direct {p0, v0, v1}, Lcom/google/android/location/copresence/bf;->a(II)V

    .line 70
    const/4 v1, 0x2

    invoke-static {v1}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 71
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "StartedMediumTracker: Called add() with previousSize = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " on tokenMedium "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->a(Ljava/lang/String;)I

    .line 74
    :cond_0
    return-void
.end method

.method public final c(I)Z
    .locals 2

    .prologue
    .line 89
    iget-object v0, p0, Lcom/google/android/location/copresence/bf;->a:Ljava/util/HashSet;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 112
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "StartedMediumTracker [mMediumsStarted="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/location/copresence/bf;->a:Ljava/util/HashSet;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mIsAdvertisingMedium="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/google/android/location/copresence/bf;->b:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
