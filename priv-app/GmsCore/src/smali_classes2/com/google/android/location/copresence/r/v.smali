.class final Lcom/google/android/location/copresence/r/v;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/net/wifi/p2p/WifiP2pManager$ActionListener;


# instance fields
.field final synthetic a:Lcom/google/android/location/copresence/r/u;


# direct methods
.method constructor <init>(Lcom/google/android/location/copresence/r/u;)V
    .locals 0

    .prologue
    .line 312
    iput-object p1, p0, Lcom/google/android/location/copresence/r/v;->a:Lcom/google/android/location/copresence/r/u;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(I)V
    .locals 2

    .prologue
    .line 321
    const/4 v0, 0x6

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 322
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "WifiDirectStates2: Failed to remove current group: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->e(Ljava/lang/String;)I

    .line 324
    :cond_0
    return-void
.end method

.method public final onSuccess()V
    .locals 2

    .prologue
    .line 315
    iget-object v0, p0, Lcom/google/android/location/copresence/r/v;->a:Lcom/google/android/location/copresence/r/u;

    iget-object v0, v0, Lcom/google/android/location/copresence/r/u;->a:Lcom/google/android/location/copresence/r/t;

    iget-object v0, v0, Lcom/google/android/location/copresence/r/t;->b:Lcom/google/android/location/copresence/r/p;

    invoke-static {v0}, Lcom/google/android/location/copresence/r/p;->b(Lcom/google/android/location/copresence/r/p;)Lcom/google/android/location/copresence/r/ah;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/location/copresence/r/v;->a:Lcom/google/android/location/copresence/r/u;

    iget-object v1, v1, Lcom/google/android/location/copresence/r/u;->a:Lcom/google/android/location/copresence/r/t;

    iget-object v1, v1, Lcom/google/android/location/copresence/r/t;->a:Landroid/net/wifi/p2p/WifiP2pManager$GroupInfoListener;

    invoke-virtual {v0, v1}, Lcom/google/android/location/copresence/r/ah;->a(Landroid/net/wifi/p2p/WifiP2pManager$GroupInfoListener;)V

    .line 316
    return-void
.end method
