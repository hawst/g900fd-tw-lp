.class final Lcom/google/android/location/copresence/am;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Ljava/lang/String;

.field final synthetic b:Lcom/google/android/location/copresence/al;


# direct methods
.method constructor <init>(Lcom/google/android/location/copresence/al;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 284
    iput-object p1, p0, Lcom/google/android/location/copresence/am;->b:Lcom/google/android/location/copresence/al;

    iput-object p2, p0, Lcom/google/android/location/copresence/am;->a:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 5

    .prologue
    .line 288
    iget-object v0, p0, Lcom/google/android/location/copresence/am;->b:Lcom/google/android/location/copresence/al;

    iget-object v0, v0, Lcom/google/android/location/copresence/al;->b:Lcom/google/android/location/copresence/ah;

    iget-object v1, p0, Lcom/google/android/location/copresence/am;->a:Ljava/lang/String;

    iput-object v1, v0, Lcom/google/android/location/copresence/ah;->h:Ljava/lang/String;

    .line 289
    iget-object v0, p0, Lcom/google/android/location/copresence/am;->b:Lcom/google/android/location/copresence/al;

    iget-object v0, v0, Lcom/google/android/location/copresence/al;->b:Lcom/google/android/location/copresence/ah;

    iget-object v1, p0, Lcom/google/android/location/copresence/am;->b:Lcom/google/android/location/copresence/al;

    iget-object v1, v1, Lcom/google/android/location/copresence/al;->b:Lcom/google/android/location/copresence/ah;

    iget-object v1, v1, Lcom/google/android/location/copresence/ah;->b:Landroid/content/Context;

    const-string v2, "copresence_gcm_pref"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v2

    invoke-static {v1}, Lcom/google/android/location/copresence/ah;->b(Landroid/content/Context;)I

    move-result v1

    const/4 v3, 0x3

    invoke-static {v3}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v3

    if-eqz v3, :cond_0

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "DeviceRegistrationHelper: Saving regId on app version "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/location/copresence/ag;->b(Ljava/lang/String;)I

    :cond_0
    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    const-string v3, "registration_id"

    iget-object v0, v0, Lcom/google/android/location/copresence/ah;->h:Ljava/lang/String;

    invoke-interface {v2, v3, v0}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    const-string v0, "appVersion"

    invoke-interface {v2, v0, v1}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 292
    iget-object v0, p0, Lcom/google/android/location/copresence/am;->b:Lcom/google/android/location/copresence/al;

    iget-object v0, v0, Lcom/google/android/location/copresence/al;->a:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    .line 293
    return-void
.end method
