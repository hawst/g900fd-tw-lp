.class final Lcom/google/android/location/collectionlib/e;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final a:Ljava/lang/String;

.field final b:Z

.field final c:Z

.field final d:Ljava/util/Set;

.field final e:Z


# direct methods
.method public constructor <init>(Ljava/lang/String;ZZLjava/util/Set;Z)V
    .locals 0

    .prologue
    .line 225
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 226
    iput-object p1, p0, Lcom/google/android/location/collectionlib/e;->a:Ljava/lang/String;

    .line 227
    iput-boolean p2, p0, Lcom/google/android/location/collectionlib/e;->b:Z

    .line 228
    iput-boolean p3, p0, Lcom/google/android/location/collectionlib/e;->c:Z

    .line 229
    iput-object p4, p0, Lcom/google/android/location/collectionlib/e;->d:Ljava/util/Set;

    .line 230
    iput-boolean p5, p0, Lcom/google/android/location/collectionlib/e;->e:Z

    .line 231
    return-void
.end method

.method static a(Ljava/lang/String;)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 267
    :try_start_0
    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v1

    .line 268
    new-instance v2, Ljava/io/File;

    invoke-direct {v2, p0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 269
    invoke-virtual {v2}, Ljava/io/File;->getCanonicalPath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1}, Ljava/io/File;->getCanonicalPath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    if-eqz v1, :cond_0

    .line 277
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 275
    :catch_0
    move-exception v1

    goto :goto_0
.end method
