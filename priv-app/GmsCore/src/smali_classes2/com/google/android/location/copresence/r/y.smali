.class final Lcom/google/android/location/copresence/r/y;
.super Lcom/google/android/location/copresence/r/ad;
.source "SourceFile"


# instance fields
.field public final a:Ljava/lang/String;

.field final synthetic b:Lcom/google/android/location/copresence/r/p;


# direct methods
.method public constructor <init>(Lcom/google/android/location/copresence/r/p;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 89
    iput-object p1, p0, Lcom/google/android/location/copresence/r/y;->b:Lcom/google/android/location/copresence/r/p;

    .line 90
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "DeviceNameSet="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    new-array v1, v1, [Lcom/google/android/location/copresence/k/f;

    const/4 v2, 0x0

    invoke-static {p1}, Lcom/google/android/location/copresence/r/p;->a(Lcom/google/android/location/copresence/r/p;)Lcom/google/android/location/copresence/r/ad;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-direct {p0, p1, v0, v1}, Lcom/google/android/location/copresence/r/ad;-><init>(Lcom/google/android/location/copresence/r/p;Ljava/lang/String;[Lcom/google/android/location/copresence/k/f;)V

    .line 91
    invoke-static {p2}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 92
    iput-object p2, p0, Lcom/google/android/location/copresence/r/y;->a:Ljava/lang/String;

    .line 93
    return-void
.end method


# virtual methods
.method public final a()Z
    .locals 9

    .prologue
    .line 102
    iget-object v0, p0, Lcom/google/android/location/copresence/r/y;->b:Lcom/google/android/location/copresence/r/p;

    invoke-static {v0}, Lcom/google/android/location/copresence/r/p;->b(Lcom/google/android/location/copresence/r/p;)Lcom/google/android/location/copresence/r/ah;

    move-result-object v4

    iget-object v6, p0, Lcom/google/android/location/copresence/r/y;->a:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/location/copresence/r/y;->b:Lcom/google/android/location/copresence/r/p;

    invoke-static {v0}, Lcom/google/android/location/copresence/r/p;->c(Lcom/google/android/location/copresence/r/p;)Landroid/net/wifi/p2p/WifiP2pManager$ActionListener;

    move-result-object v8

    invoke-virtual {v4}, Lcom/google/android/location/copresence/r/ah;->a()V

    const/4 v0, 0x2

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "WifiP2pManager: Setting device name: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->a(Ljava/lang/String;)I

    :cond_0
    :try_start_0
    iget-object v0, v4, Lcom/google/android/location/copresence/r/ah;->a:Landroid/net/wifi/p2p/WifiP2pManager;

    const-class v1, Ljava/lang/Void;

    const-string v2, "setDeviceName"

    const-class v3, Landroid/net/wifi/p2p/WifiP2pManager$Channel;

    iget-object v4, v4, Lcom/google/android/location/copresence/r/ah;->e:Landroid/net/wifi/p2p/WifiP2pManager$Channel;

    const-class v5, Ljava/lang/String;

    const-class v7, Landroid/net/wifi/p2p/WifiP2pManager$ActionListener;

    invoke-static/range {v0 .. v8}, Lcom/google/android/location/copresence/bd;->a(Ljava/lang/Object;Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Class;Ljava/lang/Object;Ljava/lang/Class;Ljava/lang/Object;Ljava/lang/Class;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Lcom/google/android/location/copresence/be; {:try_start_0 .. :try_end_0} :catch_0

    .line 103
    :goto_0
    const/4 v0, 0x1

    return v0

    .line 102
    :catch_0
    move-exception v0

    const/4 v0, 0x0

    invoke-interface {v8, v0}, Landroid/net/wifi/p2p/WifiP2pManager$ActionListener;->onFailure(I)V

    goto :goto_0
.end method

.method public final b()Z
    .locals 2

    .prologue
    .line 97
    iget-object v0, p0, Lcom/google/android/location/copresence/r/y;->a:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/location/copresence/r/y;->b:Lcom/google/android/location/copresence/r/p;

    invoke-static {v1}, Lcom/google/android/location/copresence/r/p;->b(Lcom/google/android/location/copresence/r/p;)Lcom/google/android/location/copresence/r/ah;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/location/copresence/r/ah;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method
