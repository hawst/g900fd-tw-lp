.class public final Lcom/google/android/location/reporting/b/i;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/location/reporting/b/h;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 65
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/accounts/Account;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 84
    invoke-static {p1, p2}, Lcom/google/android/location/reporting/service/ReportingSyncService;->a(Landroid/accounts/Account;Ljava/lang/String;)V

    .line 85
    return-void
.end method

.method public final a(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 69
    invoke-static {p1}, Lcom/google/android/location/reporting/service/DispatchingService;->b(Landroid/content/Context;)V

    .line 70
    return-void
.end method

.method public final a(Landroid/content/Context;Landroid/accounts/Account;)V
    .locals 3

    .prologue
    .line 96
    const-string v0, "GCoreUlr"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "GCoreUlr"

    const-string v1, "showing ambiguous notification"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    invoke-static {p2}, Lcom/google/android/gms/location/a/b;->a(Landroid/accounts/Account;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "ambiguous_setting_notification"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const/4 v1, 0x0

    const/high16 v2, 0x8000000

    invoke-static {p1, v1, v0, v2}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    new-instance v1, Landroid/support/v4/app/bk;

    invoke-direct {v1, p1}, Landroid/support/v4/app/bk;-><init>(Landroid/content/Context;)V

    sget v2, Lcom/google/android/gms/p;->qr:I

    invoke-virtual {p1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/support/v4/app/bk;->e(Ljava/lang/CharSequence;)Landroid/support/v4/app/bk;

    move-result-object v1

    sget v2, Lcom/google/android/gms/p;->qr:I

    invoke-virtual {p1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/support/v4/app/bk;->a(Ljava/lang/CharSequence;)Landroid/support/v4/app/bk;

    move-result-object v1

    sget v2, Lcom/google/android/gms/p;->qS:I

    invoke-virtual {p1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/support/v4/app/bk;->b(Ljava/lang/CharSequence;)Landroid/support/v4/app/bk;

    move-result-object v1

    const v2, 0x108008a

    invoke-virtual {v1, v2}, Landroid/support/v4/app/bk;->a(I)Landroid/support/v4/app/bk;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/bk;->a()Landroid/support/v4/app/bk;

    move-result-object v1

    iput-object v0, v1, Landroid/support/v4/app/bk;->d:Landroid/app/PendingIntent;

    invoke-virtual {v1}, Landroid/support/v4/app/bk;->b()Landroid/app/Notification;

    move-result-object v1

    invoke-static {}, Lcom/google/android/location/reporting/b/l;->i()V

    const-string v0, "notification"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    const v2, 0x1b2c42a

    invoke-virtual {v0, v2, v1}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    .line 97
    return-void
.end method

.method public final a(Landroid/content/Context;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 74
    invoke-static {p1, p2}, Lcom/google/android/location/reporting/service/DispatchingService;->b(Landroid/content/Context;Ljava/lang/String;)V

    .line 75
    return-void
.end method

.method public final a(Landroid/content/Context;Ljava/lang/String;Landroid/accounts/Account;Ljava/lang/Boolean;Ljava/lang/Boolean;)V
    .locals 0

    .prologue
    .line 90
    invoke-static {p1, p2, p3, p4, p5}, Lcom/google/android/location/reporting/service/v;->a(Landroid/content/Context;Ljava/lang/String;Landroid/accounts/Account;Ljava/lang/Boolean;Ljava/lang/Boolean;)Z

    .line 92
    return-void
.end method

.method public final b(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 79
    invoke-static {p1}, Lcom/google/android/location/reporting/service/DispatchingService;->a(Landroid/content/Context;)V

    .line 80
    return-void
.end method

.method public final c(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 101
    const-string v0, "notification"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    const v1, 0x1b2c42a

    invoke-virtual {v0, v1}, Landroid/app/NotificationManager;->cancel(I)V

    .line 102
    return-void
.end method
