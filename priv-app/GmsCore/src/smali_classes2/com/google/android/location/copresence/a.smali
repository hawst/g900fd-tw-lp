.class public final Lcom/google/android/location/copresence/a;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static i:Lcom/google/android/location/copresence/a;


# instance fields
.field final a:Landroid/content/Context;

.field b:Lcom/google/android/location/copresence/bi;

.field final c:Lcom/google/android/location/copresence/c/a;

.field final d:Lcom/google/android/location/copresence/d/j;

.field final e:Lcom/google/android/location/copresence/r/ag;

.field f:I

.field private final g:Lcom/google/android/location/copresence/l/d;

.field private final h:Lcom/google/android/location/copresence/n/f;


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 61
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/location/copresence/a;->b:Lcom/google/android/location/copresence/bi;

    .line 62
    iput-object p1, p0, Lcom/google/android/location/copresence/a;->a:Landroid/content/Context;

    .line 63
    invoke-static {p1}, Lcom/google/android/location/copresence/l/d;->a(Landroid/content/Context;)Lcom/google/android/location/copresence/l/d;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/copresence/a;->g:Lcom/google/android/location/copresence/l/d;

    .line 64
    invoke-static {p1}, Lcom/google/android/location/copresence/n/f;->a(Landroid/content/Context;)Lcom/google/android/location/copresence/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/copresence/a;->h:Lcom/google/android/location/copresence/n/f;

    .line 65
    invoke-static {p1}, Lcom/google/android/location/copresence/c/a;->a(Landroid/content/Context;)Lcom/google/android/location/copresence/c/a;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/copresence/a;->c:Lcom/google/android/location/copresence/c/a;

    .line 66
    invoke-static {p1}, Lcom/google/android/location/copresence/d/j;->a(Landroid/content/Context;)Lcom/google/android/location/copresence/d/j;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/copresence/a;->d:Lcom/google/android/location/copresence/d/j;

    .line 67
    invoke-static {p1}, Lcom/google/android/location/copresence/r/ag;->a(Landroid/content/Context;)Lcom/google/android/location/copresence/r/ag;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/copresence/a;->e:Lcom/google/android/location/copresence/r/ag;

    .line 68
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/location/copresence/a;->f:I

    .line 69
    return-void
.end method

.method public static declared-synchronized a(Landroid/content/Context;)Lcom/google/android/location/copresence/a;
    .locals 2

    .prologue
    .line 55
    const-class v1, Lcom/google/android/location/copresence/a;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/google/android/location/copresence/a;->i:Lcom/google/android/location/copresence/a;

    if-nez v0, :cond_0

    .line 56
    new-instance v0, Lcom/google/android/location/copresence/a;

    invoke-direct {v0, p0}, Lcom/google/android/location/copresence/a;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/google/android/location/copresence/a;->i:Lcom/google/android/location/copresence/a;

    .line 58
    :cond_0
    sget-object v0, Lcom/google/android/location/copresence/a;->i:Lcom/google/android/location/copresence/a;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 55
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method static a(I)Ljava/lang/String;
    .locals 1

    .prologue
    .line 122
    packed-switch p0, :pswitch_data_0

    .line 130
    const-string v0, "UNKNOWN_STATE"

    :goto_0
    return-object v0

    .line 124
    :pswitch_0
    const-string v0, "STATE_NOT_READY"

    goto :goto_0

    .line 126
    :pswitch_1
    const-string v0, "STATE_READY"

    goto :goto_0

    .line 128
    :pswitch_2
    const-string v0, "STATE_INITIATOR"

    goto :goto_0

    .line 122
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
