.class final Lcom/google/android/location/places/bm;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/location/e/ak;


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 60
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final synthetic a(Ljava/io/DataInput;)Ljava/lang/Object;
    .locals 10

    .prologue
    .line 60
    invoke-interface {p1}, Ljava/io/DataInput;->readDouble()D

    move-result-wide v0

    invoke-interface {p1}, Ljava/io/DataInput;->readDouble()D

    move-result-wide v2

    invoke-interface {p1}, Ljava/io/DataInput;->readDouble()D

    move-result-wide v4

    invoke-interface {p1}, Ljava/io/DataInput;->readDouble()D

    move-result-wide v6

    new-instance v8, Lcom/google/android/gms/maps/model/LatLngBounds;

    new-instance v9, Lcom/google/android/gms/maps/model/LatLng;

    invoke-direct {v9, v4, v5, v6, v7}, Lcom/google/android/gms/maps/model/LatLng;-><init>(DD)V

    new-instance v4, Lcom/google/android/gms/maps/model/LatLng;

    invoke-direct {v4, v0, v1, v2, v3}, Lcom/google/android/gms/maps/model/LatLng;-><init>(DD)V

    invoke-direct {v8, v9, v4}, Lcom/google/android/gms/maps/model/LatLngBounds;-><init>(Lcom/google/android/gms/maps/model/LatLng;Lcom/google/android/gms/maps/model/LatLng;)V

    invoke-interface {p1}, Ljava/io/DataInput;->readUTF()Ljava/lang/String;

    move-result-object v2

    invoke-interface {p1}, Ljava/io/DataInput;->readUTF()Ljava/lang/String;

    move-result-object v3

    invoke-interface {p1}, Ljava/io/DataInput;->readInt()I

    move-result v0

    const/4 v1, -0x1

    if-eq v0, v1, :cond_4

    new-array v0, v0, [B

    invoke-interface {p1, v0}, Ljava/io/DataInput;->readFully([B)V

    invoke-static {v0}, Lcom/google/android/location/l/a/av;->a([B)Lcom/google/android/location/l/a/av;

    move-result-object v4

    invoke-static {}, Lcom/google/android/gms/location/places/AutocompleteFilter;->a()Lcom/google/android/gms/location/places/a;

    move-result-object v5

    iget-object v0, v4, Lcom/google/android/location/l/a/av;->b:[Ljava/lang/String;

    array-length v6, v0

    if-eqz v6, :cond_2

    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7, v6}, Ljava/util/ArrayList;-><init>(I)V

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v6, :cond_1

    iget-object v0, v4, Lcom/google/android/location/l/a/av;->b:[Ljava/lang/String;

    aget-object v9, v0, v1

    invoke-static {v9}, Lcom/google/android/gms/location/places/p;->a(Ljava/lang/String;)Lcom/google/android/gms/location/places/PlaceType;

    move-result-object v0

    if-nez v0, :cond_0

    invoke-static {v9}, Lcom/google/android/gms/location/places/PlaceType;->a(Ljava/lang/String;)Lcom/google/android/gms/location/places/PlaceType;

    move-result-object v0

    :cond_0
    invoke-interface {v7, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_1
    iput-object v7, v5, Lcom/google/android/gms/location/places/a;->b:Ljava/util/Collection;

    :cond_2
    iget-object v0, v4, Lcom/google/android/location/l/a/av;->a:Ljava/lang/Boolean;

    invoke-static {v0}, Lcom/google/android/location/places/bn;->a(Ljava/lang/Boolean;)Z

    move-result v0

    if-eqz v0, :cond_3

    const/4 v0, 0x1

    iput-boolean v0, v5, Lcom/google/android/gms/location/places/a;->a:Z

    :cond_3
    invoke-virtual {v5}, Lcom/google/android/gms/location/places/a;->a()Lcom/google/android/gms/location/places/AutocompleteFilter;

    move-result-object v0

    :goto_1
    new-instance v1, Lcom/google/android/location/places/bl;

    invoke-direct {v1, v8, v2, v3, v0}, Lcom/google/android/location/places/bl;-><init>(Lcom/google/android/gms/maps/model/LatLngBounds;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/location/places/AutocompleteFilter;)V

    return-object v1

    :cond_4
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public final synthetic a(Ljava/lang/Object;Ljava/io/DataOutput;)V
    .locals 2

    .prologue
    .line 60
    check-cast p1, Lcom/google/android/location/places/bl;

    iget-object v0, p1, Lcom/google/android/location/places/bl;->a:Lcom/google/android/gms/maps/model/LatLngBounds;

    iget-object v0, v0, Lcom/google/android/gms/maps/model/LatLngBounds;->b:Lcom/google/android/gms/maps/model/LatLng;

    iget-wide v0, v0, Lcom/google/android/gms/maps/model/LatLng;->a:D

    invoke-interface {p2, v0, v1}, Ljava/io/DataOutput;->writeDouble(D)V

    iget-object v0, p1, Lcom/google/android/location/places/bl;->a:Lcom/google/android/gms/maps/model/LatLngBounds;

    iget-object v0, v0, Lcom/google/android/gms/maps/model/LatLngBounds;->b:Lcom/google/android/gms/maps/model/LatLng;

    iget-wide v0, v0, Lcom/google/android/gms/maps/model/LatLng;->b:D

    invoke-interface {p2, v0, v1}, Ljava/io/DataOutput;->writeDouble(D)V

    iget-object v0, p1, Lcom/google/android/location/places/bl;->a:Lcom/google/android/gms/maps/model/LatLngBounds;

    iget-object v0, v0, Lcom/google/android/gms/maps/model/LatLngBounds;->a:Lcom/google/android/gms/maps/model/LatLng;

    iget-wide v0, v0, Lcom/google/android/gms/maps/model/LatLng;->a:D

    invoke-interface {p2, v0, v1}, Ljava/io/DataOutput;->writeDouble(D)V

    iget-object v0, p1, Lcom/google/android/location/places/bl;->a:Lcom/google/android/gms/maps/model/LatLngBounds;

    iget-object v0, v0, Lcom/google/android/gms/maps/model/LatLngBounds;->a:Lcom/google/android/gms/maps/model/LatLng;

    iget-wide v0, v0, Lcom/google/android/gms/maps/model/LatLng;->b:D

    invoke-interface {p2, v0, v1}, Ljava/io/DataOutput;->writeDouble(D)V

    iget-object v0, p1, Lcom/google/android/location/places/bl;->b:Ljava/lang/String;

    invoke-interface {p2, v0}, Ljava/io/DataOutput;->writeUTF(Ljava/lang/String;)V

    iget-object v0, p1, Lcom/google/android/location/places/bl;->c:Ljava/lang/String;

    invoke-interface {p2, v0}, Ljava/io/DataOutput;->writeUTF(Ljava/lang/String;)V

    iget-object v0, p1, Lcom/google/android/location/places/bl;->d:Lcom/google/android/gms/location/places/AutocompleteFilter;

    invoke-static {v0}, Lcom/google/android/location/places/a;->a(Lcom/google/android/gms/location/places/AutocompleteFilter;)Lcom/google/android/location/l/a/av;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, -0x1

    invoke-interface {p2, v0}, Ljava/io/DataOutput;->writeInt(I)V

    :goto_0
    return-void

    :cond_0
    invoke-static {v0}, Lcom/google/protobuf/nano/j;->toByteArray(Lcom/google/protobuf/nano/j;)[B

    move-result-object v0

    array-length v1, v0

    invoke-interface {p2, v1}, Ljava/io/DataOutput;->writeInt(I)V

    invoke-interface {p2, v0}, Ljava/io/DataOutput;->write([B)V

    goto :goto_0
.end method
