.class public final Lcom/google/android/location/copresence/q/r;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final a:Lcom/google/android/location/copresence/ap;

.field b:J

.field private final c:Ljava/util/HashMap;

.field private final d:Ljava/util/HashSet;


# direct methods
.method public constructor <init>(Lcom/google/android/location/copresence/ap;J)V
    .locals 2

    .prologue
    .line 142
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 143
    iput-object p1, p0, Lcom/google/android/location/copresence/q/r;->a:Lcom/google/android/location/copresence/ap;

    .line 144
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/location/copresence/q/r;->c:Ljava/util/HashMap;

    .line 145
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/location/copresence/q/r;->d:Ljava/util/HashSet;

    .line 146
    iput-wide p2, p0, Lcom/google/android/location/copresence/q/r;->b:J

    .line 147
    return-void
.end method


# virtual methods
.method public final declared-synchronized a(Lcom/google/android/location/copresence/q/v;)V
    .locals 1

    .prologue
    .line 154
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/location/copresence/q/r;->d:Ljava/util/HashSet;

    invoke-virtual {v0, p1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 155
    monitor-exit p0

    return-void

    .line 154
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Ljava/lang/Object;)V
    .locals 6

    .prologue
    .line 179
    monitor-enter p0

    :try_start_0
    invoke-static {p1}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 181
    iget-object v0, p0, Lcom/google/android/location/copresence/q/r;->c:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/copresence/q/s;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 182
    if-nez v0, :cond_1

    .line 187
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 186
    :cond_1
    :try_start_1
    iget-boolean v1, v0, Lcom/google/android/location/copresence/q/s;->d:Z

    if-eqz v1, :cond_0

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/google/android/location/copresence/q/s;->d:Z

    iget-object v1, v0, Lcom/google/android/location/copresence/q/s;->e:Lcom/google/android/location/copresence/q/r;

    iget-object v1, v1, Lcom/google/android/location/copresence/q/r;->a:Lcom/google/android/location/copresence/ap;

    invoke-virtual {v1}, Lcom/google/android/location/copresence/ap;->b()Lcom/google/android/location/copresence/c;

    move-result-object v1

    iget-object v2, v0, Lcom/google/android/location/copresence/q/s;->b:Ljava/lang/Runnable;

    invoke-virtual {v1, v2}, Lcom/google/android/location/copresence/c;->a(Ljava/lang/Runnable;)V

    iget-wide v2, v0, Lcom/google/android/location/copresence/q/s;->c:J

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-ltz v1, :cond_0

    invoke-virtual {v0}, Lcom/google/android/location/copresence/q/s;->a()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 179
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Ljava/lang/Object;J)V
    .locals 2

    .prologue
    .line 167
    monitor-enter p0

    :try_start_0
    invoke-static {p1}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 168
    const-wide/16 v0, 0x0

    cmp-long v0, p2, v0

    if-ltz v0, :cond_2

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/android/gms/common/internal/bx;->b(Z)V

    .line 170
    iget-object v0, p0, Lcom/google/android/location/copresence/q/r;->c:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/copresence/q/s;

    .line 171
    if-nez v0, :cond_0

    .line 172
    new-instance v0, Lcom/google/android/location/copresence/q/s;

    invoke-direct {v0, p0, p1}, Lcom/google/android/location/copresence/q/s;-><init>(Lcom/google/android/location/copresence/q/r;Ljava/lang/Object;)V

    .line 173
    iget-object v1, p0, Lcom/google/android/location/copresence/q/r;->c:Ljava/util/HashMap;

    invoke-virtual {v1, p1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 175
    :cond_0
    iget-boolean v1, v0, Lcom/google/android/location/copresence/q/s;->d:Z

    if-eqz v1, :cond_3

    invoke-virtual {v0, p2, p3}, Lcom/google/android/location/copresence/q/s;->a(J)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 176
    :cond_1
    :goto_1
    monitor-exit p0

    return-void

    .line 168
    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    .line 175
    :cond_3
    :try_start_1
    invoke-virtual {v0, p2, p3}, Lcom/google/android/location/copresence/q/s;->a(J)Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {v0}, Lcom/google/android/location/copresence/q/s;->a()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 167
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b(Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 201
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/location/copresence/q/r;->d:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/copresence/q/v;

    .line 202
    invoke-interface {v0, p1}, Lcom/google/android/location/copresence/q/v;->a(Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 201
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 204
    :cond_0
    monitor-exit p0

    return-void
.end method
