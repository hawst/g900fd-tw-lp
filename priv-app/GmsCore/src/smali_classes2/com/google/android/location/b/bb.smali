.class public final Lcom/google/android/location/b/bb;
.super Ljava/util/LinkedHashMap;
.source "SourceFile"


# instance fields
.field final a:I

.field final b:Lcom/google/android/location/b/as;

.field final c:Lcom/google/android/location/b/bc;


# direct methods
.method constructor <init>(ILcom/google/android/location/b/as;Lcom/google/android/location/b/bc;)V
    .locals 2

    .prologue
    .line 218
    const/high16 v0, 0x3f400000    # 0.75f

    const/4 v1, 0x1

    invoke-direct {p0, p1, v0, v1}, Ljava/util/LinkedHashMap;-><init>(IFZ)V

    .line 219
    iput p1, p0, Lcom/google/android/location/b/bb;->a:I

    .line 220
    iput-object p2, p0, Lcom/google/android/location/b/bb;->b:Lcom/google/android/location/b/as;

    .line 221
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/location/b/bb;->c:Lcom/google/android/location/b/bc;

    .line 222
    return-void
.end method


# virtual methods
.method protected final removeEldestEntry(Ljava/util/Map$Entry;)Z
    .locals 2

    .prologue
    .line 228
    invoke-virtual {p0}, Lcom/google/android/location/b/bb;->size()I

    move-result v0

    iget v1, p0, Lcom/google/android/location/b/bb;->a:I

    if-le v0, v1, :cond_0

    .line 229
    iget-object v0, p0, Lcom/google/android/location/b/bb;->b:Lcom/google/android/location/b/as;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Lcom/google/android/location/b/as;->a(I)V

    .line 230
    invoke-interface {p1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/location/b/bb;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 231
    iget-object v0, p0, Lcom/google/android/location/b/bb;->c:Lcom/google/android/location/b/bc;

    if-eqz v0, :cond_0

    .line 232
    iget-object v0, p0, Lcom/google/android/location/b/bb;->c:Lcom/google/android/location/b/bc;

    invoke-interface {p1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    invoke-interface {p1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    .line 236
    :cond_0
    const/4 v0, 0x0

    return v0
.end method
