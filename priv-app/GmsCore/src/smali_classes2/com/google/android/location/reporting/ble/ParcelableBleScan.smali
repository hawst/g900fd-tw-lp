.class public Lcom/google/android/location/reporting/ble/ParcelableBleScan;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:Lcom/google/android/location/reporting/ble/i;


# instance fields
.field final a:I

.field final b:Ljava/lang/String;

.field final c:Ljava/lang/Long;

.field final d:Ljava/lang/Integer;

.field final e:Ljava/lang/Integer;

.field final f:Ljava/lang/String;

.field final g:Ljava/lang/Long;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 40
    new-instance v0, Lcom/google/android/location/reporting/ble/i;

    invoke-direct {v0}, Lcom/google/android/location/reporting/ble/i;-><init>()V

    sput-object v0, Lcom/google/android/location/reporting/ble/ParcelableBleScan;->CREATOR:Lcom/google/android/location/reporting/ble/i;

    return-void
.end method

.method constructor <init>(ILjava/lang/String;Ljava/lang/Long;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/Long;)V
    .locals 0

    .prologue
    .line 73
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 74
    iput p1, p0, Lcom/google/android/location/reporting/ble/ParcelableBleScan;->a:I

    .line 75
    iput-object p2, p0, Lcom/google/android/location/reporting/ble/ParcelableBleScan;->b:Ljava/lang/String;

    .line 76
    iput-object p3, p0, Lcom/google/android/location/reporting/ble/ParcelableBleScan;->c:Ljava/lang/Long;

    .line 77
    iput-object p4, p0, Lcom/google/android/location/reporting/ble/ParcelableBleScan;->d:Ljava/lang/Integer;

    .line 78
    iput-object p5, p0, Lcom/google/android/location/reporting/ble/ParcelableBleScan;->e:Ljava/lang/Integer;

    .line 79
    iput-object p6, p0, Lcom/google/android/location/reporting/ble/ParcelableBleScan;->f:Ljava/lang/String;

    .line 80
    iput-object p7, p0, Lcom/google/android/location/reporting/ble/ParcelableBleScan;->g:Ljava/lang/Long;

    .line 81
    return-void
.end method

.method static a(Lcom/google/android/gms/blescanner/a/c;Lcom/google/android/gms/blescanner/compat/ScanResult;)Lcom/google/android/location/reporting/ble/ParcelableBleScan;
    .locals 14
    .annotation build Landroid/annotation/TargetApi;
        value = 0x12
    .end annotation

    .prologue
    const/16 v6, 0xe0

    const/4 v1, 0x1

    const/4 v5, -0x1

    const/4 v8, 0x0

    const/4 v0, 0x0

    .line 182
    invoke-virtual {p1}, Lcom/google/android/gms/blescanner/compat/ScanResult;->b()Lcom/google/android/gms/blescanner/compat/y;

    move-result-object v2

    if-eqz v2, :cond_b

    .line 183
    invoke-virtual {p1}, Lcom/google/android/gms/blescanner/compat/ScanResult;->b()Lcom/google/android/gms/blescanner/compat/y;

    move-result-object v2

    iget-object v4, v2, Lcom/google/android/gms/blescanner/compat/y;->e:[B

    .line 185
    const/4 v2, -0x1

    :try_start_0
    invoke-static {v4, v2}, Lcom/google/android/gms/blescanner/b/a;->a([BB)I

    move-result v2

    if-eq v2, v5, :cond_1

    invoke-static {v4, v2}, Lcom/google/android/gms/blescanner/b/a;->a([BI)S

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    move-object v3, v2

    :goto_0
    const/16 v2, 0x16

    invoke-static {v4, v2}, Lcom/google/android/gms/blescanner/b/a;->a([BB)I

    move-result v2

    if-eq v2, v5, :cond_2

    invoke-static {v4, v2}, Lcom/google/android/gms/blescanner/b/a;->a([BI)S

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    :goto_1
    if-eqz v3, :cond_0

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v5

    if-ne v5, v6, :cond_3

    :cond_0
    if-eqz v2, :cond_3

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    const/16 v5, -0x10c

    if-ne v2, v5, :cond_3

    invoke-static {v4}, Lcom/google/android/gms/blescanner/a/f;->a([B)Z

    move-result v2

    if-eqz v2, :cond_3

    move v2, v1

    :goto_2
    if-eqz v2, :cond_4

    iget-object v1, p0, Lcom/google/android/gms/blescanner/a/c;->c:Lcom/google/android/gms/blescanner/a/f;

    .line 187
    :goto_3
    if-eqz v1, :cond_c

    .line 188
    invoke-virtual {v1}, Lcom/google/android/gms/blescanner/a/b;->a()I

    move-result v3

    .line 189
    packed-switch v3, :pswitch_data_0

    :goto_4
    if-eqz v0, :cond_a

    .line 190
    invoke-virtual {v1, v4}, Lcom/google/android/gms/blescanner/a/b;->b([B)[B

    move-result-object v0

    const/16 v2, 0xa

    invoke-static {v0, v2}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;

    move-result-object v2

    .line 193
    invoke-virtual {p1}, Lcom/google/android/gms/blescanner/compat/ScanResult;->a()Landroid/bluetooth/BluetoothDevice;

    move-result-object v0

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object v0

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "0x"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v6, "\\:|-"

    const-string v7, ""

    invoke-virtual {v0, v6, v7}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Long;->decode(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v10

    .line 194
    invoke-virtual {p1}, Lcom/google/android/gms/blescanner/compat/ScanResult;->c()I

    move-result v5

    .line 195
    invoke-virtual {v1, v4}, Lcom/google/android/gms/blescanner/a/b;->c([B)I

    move-result v7

    .line 197
    sget-object v0, Ljava/util/concurrent/TimeUnit;->NANOSECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {p1}, Lcom/google/android/gms/blescanner/compat/ScanResult;->d()J

    move-result-wide v12

    invoke-virtual {v0, v12, v13}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v12

    .line 199
    packed-switch v3, :pswitch_data_1

    const-string v6, "unknown"

    :goto_5
    new-instance v0, Lcom/google/android/location/reporting/ble/ParcelableBleScan;

    const/4 v1, 0x0

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-direct/range {v0 .. v7}, Lcom/google/android/location/reporting/ble/ParcelableBleScan;-><init>(ILjava/lang/String;Ljava/lang/Long;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/Long;)V

    .line 219
    :goto_6
    return-object v0

    :cond_1
    move-object v3, v8

    .line 185
    goto/16 :goto_0

    :cond_2
    move-object v2, v8

    goto/16 :goto_1

    :cond_3
    move v2, v0

    goto :goto_2

    :cond_4
    if-eqz v3, :cond_6

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v2

    if-eq v2, v6, :cond_5

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v2

    const/16 v5, 0x550

    if-ne v2, v5, :cond_6

    :cond_5
    invoke-static {v4}, Lcom/google/android/gms/blescanner/a/e;->a([B)Z

    move-result v2

    if-eqz v2, :cond_6

    move v2, v1

    :goto_7
    if-eqz v2, :cond_7

    iget-object v1, p0, Lcom/google/android/gms/blescanner/a/c;->b:Lcom/google/android/gms/blescanner/a/e;

    goto/16 :goto_3

    :cond_6
    move v2, v0

    goto :goto_7

    :cond_7
    if-eqz v3, :cond_8

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v2

    const/16 v3, 0x4c

    if-ne v2, v3, :cond_8

    invoke-static {v4}, Lcom/google/android/gms/blescanner/a/a;->a([B)Z

    move-result v2

    if-eqz v2, :cond_8

    :goto_8
    if-eqz v1, :cond_9

    iget-object v1, p0, Lcom/google/android/gms/blescanner/a/c;->a:Lcom/google/android/gms/blescanner/a/a;

    goto/16 :goto_3

    :cond_8
    move v1, v0

    goto :goto_8

    :cond_9
    move-object v1, v8

    goto/16 :goto_3

    .line 189
    :pswitch_0
    sget-object v0, Lcom/google/android/location/reporting/service/ab;->z:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    goto/16 :goto_4

    :pswitch_1
    sget-object v0, Lcom/google/android/location/reporting/service/ab;->A:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    goto/16 :goto_4

    :pswitch_2
    sget-object v0, Lcom/google/android/location/reporting/service/ab;->B:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    goto/16 :goto_4

    .line 199
    :pswitch_3
    const-string v6, "ibeacon"

    goto/16 :goto_5

    :pswitch_4
    const-string v6, "gbeaconV1"

    goto/16 :goto_5

    :pswitch_5
    const-string v6, "gbeaconV3"

    goto/16 :goto_5

    :pswitch_6
    const-string v6, "gbeaconV3ephemeral"

    goto/16 :goto_5

    .line 207
    :cond_a
    const-string v0, "GCoreUlr"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unexpected beacon type not allowed in ULR: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/location/reporting/b/d;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 208
    const-string v0, "ble_unexpected_type"

    invoke-static {v0}, Lcom/google/android/location/reporting/b/l;->b(Ljava/lang/String;)V

    :cond_b
    :goto_9
    move-object v0, v8

    .line 219
    goto/16 :goto_6

    .line 211
    :cond_c
    const-string v0, "GCoreUlr"

    const-string v1, "Unable to decode BLE beacon"

    invoke-static {v0, v1}, Lcom/google/android/location/reporting/b/d;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 212
    const-string v0, "ble_decoding_error"

    invoke-static {v0}, Lcom/google/android/location/reporting/b/l;->b(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_9

    .line 214
    :catch_0
    move-exception v0

    .line 216
    const-string v1, "GCoreUlr"

    const-string v2, "Unexpected exception thrown by BleScannerLib"

    invoke-static {v1, v2, v0}, Lcom/google/android/location/reporting/b/d;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_9

    .line 189
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch

    .line 199
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method

.method public static a(Lcom/google/android/location/reporting/ble/ParcelableBleScan;)Lcom/google/android/ulr/ApiMetadata;
    .locals 8

    .prologue
    const/4 v7, 0x0

    .line 110
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    new-instance v0, Lcom/google/android/ulr/BleStrengthProto;

    iget-object v1, p0, Lcom/google/android/location/reporting/ble/ParcelableBleScan;->b:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/location/reporting/ble/ParcelableBleScan;->c:Ljava/lang/Long;

    iget-object v3, p0, Lcom/google/android/location/reporting/ble/ParcelableBleScan;->d:Ljava/lang/Integer;

    iget-object v4, p0, Lcom/google/android/location/reporting/ble/ParcelableBleScan;->e:Ljava/lang/Integer;

    iget-object v5, p0, Lcom/google/android/location/reporting/ble/ParcelableBleScan;->f:Ljava/lang/String;

    invoke-direct/range {v0 .. v5}, Lcom/google/android/ulr/BleStrengthProto;-><init>(Ljava/lang/String;Ljava/lang/Long;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/String;)V

    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v2, Lcom/google/android/ulr/ApiBleScanReport;

    invoke-direct {v2, v6}, Lcom/google/android/ulr/ApiBleScanReport;-><init>(Ljava/util/ArrayList;)V

    .line 113
    new-instance v0, Lcom/google/android/ulr/ApiMetadata;

    iget-object v6, p0, Lcom/google/android/location/reporting/ble/ParcelableBleScan;->g:Ljava/lang/Long;

    move-object v1, v7

    move-object v3, v7

    move-object v4, v7

    move-object v5, v7

    invoke-direct/range {v0 .. v6}, Lcom/google/android/ulr/ApiMetadata;-><init>(Lcom/google/android/ulr/ApiActivationChange;Lcom/google/android/ulr/ApiBleScanReport;Lcom/google/android/ulr/ApiLocationStatus;Lcom/google/android/ulr/ApiPlaceReport;Lcom/google/android/ulr/ApiRate;Ljava/lang/Long;)V

    return-object v0
.end method

.method static a(Lcom/google/android/gms/blescanner/a/c;Ljava/util/List;)Ljava/util/ArrayList;
    .locals 3

    .prologue
    .line 165
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 166
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/blescanner/compat/ScanResult;

    .line 167
    invoke-static {p0, v0}, Lcom/google/android/location/reporting/ble/ParcelableBleScan;->a(Lcom/google/android/gms/blescanner/a/c;Lcom/google/android/gms/blescanner/compat/ScanResult;)Lcom/google/android/location/reporting/ble/ParcelableBleScan;

    move-result-object v0

    .line 168
    if-eqz v0, :cond_0

    .line 169
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 172
    :cond_1
    return-object v1
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 150
    sget-object v0, Lcom/google/android/location/reporting/ble/ParcelableBleScan;->CREATOR:Lcom/google/android/location/reporting/ble/i;

    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 139
    instance-of v0, p1, Lcom/google/android/location/reporting/ble/ParcelableBleScan;

    if-eqz v0, :cond_0

    move-object v0, p1

    check-cast v0, Lcom/google/android/location/reporting/ble/ParcelableBleScan;

    iget-object v0, v0, Lcom/google/android/location/reporting/ble/ParcelableBleScan;->b:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/location/reporting/ble/ParcelableBleScan;->b:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/bu;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    move-object v0, p1

    check-cast v0, Lcom/google/android/location/reporting/ble/ParcelableBleScan;

    iget-object v0, v0, Lcom/google/android/location/reporting/ble/ParcelableBleScan;->c:Ljava/lang/Long;

    iget-object v1, p0, Lcom/google/android/location/reporting/ble/ParcelableBleScan;->c:Ljava/lang/Long;

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/bu;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    move-object v0, p1

    check-cast v0, Lcom/google/android/location/reporting/ble/ParcelableBleScan;

    iget-object v0, v0, Lcom/google/android/location/reporting/ble/ParcelableBleScan;->d:Ljava/lang/Integer;

    iget-object v1, p0, Lcom/google/android/location/reporting/ble/ParcelableBleScan;->d:Ljava/lang/Integer;

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/bu;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    move-object v0, p1

    check-cast v0, Lcom/google/android/location/reporting/ble/ParcelableBleScan;

    iget-object v0, v0, Lcom/google/android/location/reporting/ble/ParcelableBleScan;->e:Ljava/lang/Integer;

    iget-object v1, p0, Lcom/google/android/location/reporting/ble/ParcelableBleScan;->e:Ljava/lang/Integer;

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/bu;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    move-object v0, p1

    check-cast v0, Lcom/google/android/location/reporting/ble/ParcelableBleScan;

    iget-object v0, v0, Lcom/google/android/location/reporting/ble/ParcelableBleScan;->f:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/location/reporting/ble/ParcelableBleScan;->f:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/bu;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    check-cast p1, Lcom/google/android/location/reporting/ble/ParcelableBleScan;

    iget-object v0, p1, Lcom/google/android/location/reporting/ble/ParcelableBleScan;->g:Ljava/lang/Long;

    iget-object v1, p0, Lcom/google/android/location/reporting/ble/ParcelableBleScan;->g:Ljava/lang/Long;

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/bu;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 123
    const/4 v0, 0x6

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/location/reporting/ble/ParcelableBleScan;->b:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/location/reporting/ble/ParcelableBleScan;->c:Ljava/lang/Long;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/location/reporting/ble/ParcelableBleScan;->d:Ljava/lang/Integer;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/android/location/reporting/ble/ParcelableBleScan;->e:Ljava/lang/Integer;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/android/location/reporting/ble/ParcelableBleScan;->f:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    iget-object v2, p0, Lcom/google/android/location/reporting/ble/ParcelableBleScan;->g:Ljava/lang/Long;

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 128
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "["

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/location/reporting/ble/ParcelableBleScan;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/location/reporting/ble/ParcelableBleScan;->c:Ljava/lang/Long;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/location/reporting/ble/ParcelableBleScan;->d:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/location/reporting/ble/ParcelableBleScan;->e:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/location/reporting/ble/ParcelableBleScan;->f:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/location/reporting/ble/ParcelableBleScan;->g:Ljava/lang/Long;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 155
    sget-object v0, Lcom/google/android/location/reporting/ble/ParcelableBleScan;->CREATOR:Lcom/google/android/location/reporting/ble/i;

    invoke-static {p0, p1}, Lcom/google/android/location/reporting/ble/i;->a(Lcom/google/android/location/reporting/ble/ParcelableBleScan;Landroid/os/Parcel;)V

    .line 156
    return-void
.end method
