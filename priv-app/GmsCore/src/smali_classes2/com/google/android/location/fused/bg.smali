.class public abstract Lcom/google/android/location/fused/bg;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final a:J


# instance fields
.field b:Lcom/google/android/location/fused/bl;

.field c:I

.field private final d:Lcom/google/android/location/fused/a/s;

.field private final e:Lcom/google/android/location/fused/a/r;

.field private final f:Lcom/google/android/location/fused/c;

.field private final g:Landroid/os/Looper;

.field private h:Lcom/google/android/location/fused/av;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 31
    sget-object v0, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0x14

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toNanos(J)J

    move-result-wide v0

    sput-wide v0, Lcom/google/android/location/fused/bg;->a:J

    return-void
.end method

.method public constructor <init>(Lcom/google/android/location/fused/a/s;Lcom/google/android/location/fused/a/r;Lcom/google/android/location/fused/c;Landroid/os/Looper;)V
    .locals 2

    .prologue
    .line 67
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 50
    sget-object v0, Lcom/google/android/location/fused/av;->a:Lcom/google/android/location/fused/av;

    iput-object v0, p0, Lcom/google/android/location/fused/bg;->h:Lcom/google/android/location/fused/av;

    .line 51
    new-instance v0, Lcom/google/android/location/fused/bi;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/android/location/fused/bi;-><init>(Lcom/google/android/location/fused/bg;B)V

    iput-object v0, p0, Lcom/google/android/location/fused/bg;->b:Lcom/google/android/location/fused/bl;

    .line 57
    const/16 v0, 0x1f

    iput v0, p0, Lcom/google/android/location/fused/bg;->c:I

    .line 68
    iput-object p1, p0, Lcom/google/android/location/fused/bg;->d:Lcom/google/android/location/fused/a/s;

    .line 69
    iput-object p2, p0, Lcom/google/android/location/fused/bg;->e:Lcom/google/android/location/fused/a/r;

    .line 70
    iput-object p3, p0, Lcom/google/android/location/fused/bg;->f:Lcom/google/android/location/fused/c;

    .line 71
    iput-object p4, p0, Lcom/google/android/location/fused/bg;->g:Landroid/os/Looper;

    .line 72
    return-void
.end method

.method static synthetic a(Lcom/google/android/location/fused/bg;)Lcom/google/android/location/fused/av;
    .locals 1

    .prologue
    .line 22
    iget-object v0, p0, Lcom/google/android/location/fused/bg;->h:Lcom/google/android/location/fused/av;

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/location/fused/bg;Lcom/google/android/location/fused/bl;)Lcom/google/android/location/fused/bl;
    .locals 0

    .prologue
    .line 22
    iput-object p1, p0, Lcom/google/android/location/fused/bg;->b:Lcom/google/android/location/fused/bl;

    return-object p1
.end method

.method static synthetic b(Lcom/google/android/location/fused/bg;)Lcom/google/android/location/fused/bl;
    .locals 1

    .prologue
    .line 22
    iget-object v0, p0, Lcom/google/android/location/fused/bg;->b:Lcom/google/android/location/fused/bl;

    return-object v0
.end method

.method static synthetic c(Lcom/google/android/location/fused/bg;)Lcom/google/android/location/fused/c;
    .locals 1

    .prologue
    .line 22
    iget-object v0, p0, Lcom/google/android/location/fused/bg;->f:Lcom/google/android/location/fused/c;

    return-object v0
.end method

.method static synthetic d(Lcom/google/android/location/fused/bg;)Z
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 22
    iget-object v0, p0, Lcom/google/android/location/fused/bg;->d:Lcom/google/android/location/fused/a/s;

    iget-boolean v0, v0, Lcom/google/android/location/fused/a/q;->i:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/location/fused/bg;->h:Lcom/google/android/location/fused/av;

    iget v0, v0, Lcom/google/android/location/fused/av;->e:F

    const/4 v2, 0x0

    cmpl-float v0, v0, v2

    if-lez v0, :cond_1

    sget-object v0, Lcom/google/android/location/x;->G:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    iget-object v0, p0, Lcom/google/android/location/fused/bg;->h:Lcom/google/android/location/fused/av;

    iget-wide v4, v0, Lcom/google/android/location/fused/av;->b:J

    cmp-long v0, v4, v2

    if-gez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/location/fused/bg;->h:Lcom/google/android/location/fused/av;

    iget-wide v4, v0, Lcom/google/android/location/fused/av;->b:J

    mul-long/2addr v4, v2

    iget-object v0, p0, Lcom/google/android/location/fused/bg;->h:Lcom/google/android/location/fused/av;

    iget-wide v6, v0, Lcom/google/android/location/fused/av;->b:J

    sub-long/2addr v2, v6

    div-long v2, v4, v2

    iget-object v0, p0, Lcom/google/android/location/fused/bg;->h:Lcom/google/android/location/fused/av;

    iget-wide v4, v0, Lcom/google/android/location/fused/av;->c:J

    cmp-long v0, v4, v2

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method static synthetic e(Lcom/google/android/location/fused/bg;)Lcom/google/android/location/fused/a/s;
    .locals 1

    .prologue
    .line 22
    iget-object v0, p0, Lcom/google/android/location/fused/bg;->d:Lcom/google/android/location/fused/a/s;

    return-object v0
.end method

.method static synthetic f()J
    .locals 2

    .prologue
    .line 22
    sget-wide v0, Lcom/google/android/location/fused/bg;->a:J

    return-wide v0
.end method

.method static synthetic f(Lcom/google/android/location/fused/bg;)Landroid/os/Looper;
    .locals 1

    .prologue
    .line 22
    iget-object v0, p0, Lcom/google/android/location/fused/bg;->g:Landroid/os/Looper;

    return-object v0
.end method

.method static synthetic g(Lcom/google/android/location/fused/bg;)Lcom/google/android/location/fused/a/r;
    .locals 1

    .prologue
    .line 22
    iget-object v0, p0, Lcom/google/android/location/fused/bg;->e:Lcom/google/android/location/fused/a/r;

    return-object v0
.end method


# virtual methods
.method public abstract a()V
.end method

.method public final a(I)V
    .locals 2

    .prologue
    .line 98
    iget v0, p0, Lcom/google/android/location/fused/bg;->c:I

    if-eq v0, p1, :cond_0

    .line 99
    iput p1, p0, Lcom/google/android/location/fused/bg;->c:I

    .line 100
    iget-object v0, p0, Lcom/google/android/location/fused/bg;->b:Lcom/google/android/location/fused/bl;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/location/fused/bl;->a(Z)V

    .line 103
    :cond_0
    return-void
.end method

.method public final a(Landroid/location/Location;)V
    .locals 1

    .prologue
    .line 89
    iget-object v0, p0, Lcom/google/android/location/fused/bg;->b:Lcom/google/android/location/fused/bl;

    invoke-virtual {v0, p1}, Lcom/google/android/location/fused/bl;->a(Landroid/location/Location;)V

    .line 90
    return-void
.end method

.method public final a(Lcom/google/android/location/fused/av;Z)V
    .locals 1

    .prologue
    .line 81
    iput-object p1, p0, Lcom/google/android/location/fused/bg;->h:Lcom/google/android/location/fused/av;

    .line 82
    iget-object v0, p0, Lcom/google/android/location/fused/bg;->b:Lcom/google/android/location/fused/bl;

    invoke-virtual {v0, p2}, Lcom/google/android/location/fused/bl;->a(Z)V

    .line 83
    return-void
.end method

.method public abstract a(Z)V
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 109
    iget-object v0, p0, Lcom/google/android/location/fused/bg;->b:Lcom/google/android/location/fused/bl;

    invoke-virtual {v0}, Lcom/google/android/location/fused/bl;->a()V

    .line 110
    return-void
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 116
    iget-object v0, p0, Lcom/google/android/location/fused/bg;->b:Lcom/google/android/location/fused/bl;

    invoke-virtual {v0}, Lcom/google/android/location/fused/bl;->g()V

    .line 117
    return-void
.end method

.method public final d()Z
    .locals 1

    .prologue
    .line 159
    iget v0, p0, Lcom/google/android/location/fused/bg;->c:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final e()Z
    .locals 1

    .prologue
    .line 173
    iget v0, p0, Lcom/google/android/location/fused/bg;->c:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
