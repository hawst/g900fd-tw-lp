.class public final Lcom/google/android/location/u;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/location/os/a;


# instance fields
.field public final a:Lcom/google/android/location/k;

.field private final b:Ljava/util/List;

.field private final c:Ljava/lang/Thread;

.field private d:Lcom/google/android/location/ap;

.field private e:Lcom/google/android/location/os/i;


# direct methods
.method public constructor <init>(Lcom/google/android/location/k;)V
    .locals 1

    .prologue
    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/location/u;->b:Ljava/util/List;

    .line 42
    iput-object p1, p0, Lcom/google/android/location/u;->a:Lcom/google/android/location/k;

    .line 43
    iget-object v0, p0, Lcom/google/android/location/u;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 44
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/u;->c:Ljava/lang/Thread;

    .line 45
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/location/os/bi;Lcom/google/android/location/e/ah;Z)Lcom/google/android/location/ap;
    .locals 3

    .prologue
    .line 52
    invoke-virtual {p0}, Lcom/google/android/location/u;->g()V

    .line 53
    iget-object v0, p0, Lcom/google/android/location/u;->d:Lcom/google/android/location/ap;

    if-eqz v0, :cond_1

    .line 54
    sget-boolean v0, Lcom/google/android/location/i/a;->b:Z

    if-eqz v0, :cond_0

    const-string v0, "CallbacksDispatcher"

    const-string v1, "Network provider already created."

    invoke-static {v0, v1}, Lcom/google/android/location/o/a/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 55
    :cond_0
    const/4 v0, 0x0

    .line 63
    :goto_0
    return-object v0

    .line 57
    :cond_1
    sget-boolean v0, Lcom/google/android/location/i/a;->b:Z

    if-eqz v0, :cond_2

    const-string v0, "CallbacksDispatcher"

    const-string v1, "Creating NetworkProvider."

    invoke-static {v0, v1}, Lcom/google/android/location/o/a/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 58
    :cond_2
    new-instance v0, Lcom/google/android/location/os/i;

    invoke-direct {v0, p1}, Lcom/google/android/location/os/i;-><init>(Lcom/google/android/location/os/bi;)V

    iput-object v0, p0, Lcom/google/android/location/u;->e:Lcom/google/android/location/os/i;

    .line 59
    new-instance v0, Lcom/google/android/location/ap;

    iget-object v1, p0, Lcom/google/android/location/u;->e:Lcom/google/android/location/os/i;

    iget-object v2, p0, Lcom/google/android/location/u;->a:Lcom/google/android/location/k;

    iget-object v2, v2, Lcom/google/android/location/k;->a:Lcom/google/android/location/activity/k;

    invoke-direct {v0, v1, p2, v2, p3}, Lcom/google/android/location/ap;-><init>(Lcom/google/android/location/os/bi;Lcom/google/android/location/e/ah;Lcom/google/android/location/activity/k;Z)V

    iput-object v0, p0, Lcom/google/android/location/u;->d:Lcom/google/android/location/ap;

    .line 61
    iget-object v0, p0, Lcom/google/android/location/u;->d:Lcom/google/android/location/ap;

    invoke-virtual {v0}, Lcom/google/android/location/ap;->d()V

    .line 62
    iget-object v0, p0, Lcom/google/android/location/u;->b:Ljava/util/List;

    iget-object v1, p0, Lcom/google/android/location/u;->d:Lcom/google/android/location/ap;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 63
    iget-object v0, p0, Lcom/google/android/location/u;->d:Lcom/google/android/location/ap;

    goto :goto_0
.end method

.method public final a()V
    .locals 2

    .prologue
    .line 249
    invoke-virtual {p0}, Lcom/google/android/location/u;->g()V

    .line 250
    iget-object v0, p0, Lcom/google/android/location/u;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/os/a;

    .line 251
    invoke-interface {v0}, Lcom/google/android/location/os/a;->a()V

    goto :goto_0

    .line 253
    :cond_0
    return-void
.end method

.method public final a(IIIZLcom/google/android/location/o/n;)V
    .locals 7

    .prologue
    .line 94
    invoke-virtual {p0}, Lcom/google/android/location/u;->g()V

    .line 95
    iget-object v0, p0, Lcom/google/android/location/u;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/os/a;

    move v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move-object v5, p5

    .line 96
    invoke-interface/range {v0 .. v5}, Lcom/google/android/location/os/a;->a(IIIZLcom/google/android/location/o/n;)V

    goto :goto_0

    .line 98
    :cond_0
    return-void
.end method

.method public final a(IIZ)V
    .locals 2

    .prologue
    .line 158
    invoke-virtual {p0}, Lcom/google/android/location/u;->g()V

    .line 159
    iget-object v0, p0, Lcom/google/android/location/u;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/os/a;

    .line 160
    invoke-interface {v0, p1, p2, p3}, Lcom/google/android/location/os/a;->a(IIZ)V

    goto :goto_0

    .line 162
    :cond_0
    return-void
.end method

.method public final a(IIZZLcom/google/android/location/o/n;Ljava/util/Set;)V
    .locals 8

    .prologue
    .line 224
    invoke-virtual {p0}, Lcom/google/android/location/u;->g()V

    .line 225
    iget-object v0, p0, Lcom/google/android/location/u;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/os/a;

    move v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move-object v5, p5

    move-object v6, p6

    .line 226
    invoke-interface/range {v0 .. v6}, Lcom/google/android/location/os/a;->a(IIZZLcom/google/android/location/o/n;Ljava/util/Set;)V

    goto :goto_0

    .line 229
    :cond_0
    return-void
.end method

.method public final a(Lcom/google/android/gms/location/ActivityRecognitionResult;)V
    .locals 2

    .prologue
    .line 214
    invoke-virtual {p0}, Lcom/google/android/location/u;->g()V

    .line 215
    iget-object v0, p0, Lcom/google/android/location/u;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/os/a;

    .line 216
    invoke-interface {v0, p1}, Lcom/google/android/location/os/a;->a(Lcom/google/android/gms/location/ActivityRecognitionResult;)V

    goto :goto_0

    .line 218
    :cond_0
    return-void
.end method

.method public final a(Lcom/google/android/location/activity/bd;)V
    .locals 2

    .prologue
    .line 206
    invoke-virtual {p0}, Lcom/google/android/location/u;->g()V

    .line 207
    iget-object v0, p0, Lcom/google/android/location/u;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/os/a;

    .line 208
    invoke-interface {v0, p1}, Lcom/google/android/location/os/a;->a(Lcom/google/android/location/activity/bd;)V

    goto :goto_0

    .line 210
    :cond_0
    return-void
.end method

.method public final a(Lcom/google/android/location/e/ah;)V
    .locals 2

    .prologue
    .line 198
    invoke-virtual {p0}, Lcom/google/android/location/u;->g()V

    .line 199
    iget-object v0, p0, Lcom/google/android/location/u;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/os/a;

    .line 200
    invoke-interface {v0, p1}, Lcom/google/android/location/os/a;->a(Lcom/google/android/location/e/ah;)V

    goto :goto_0

    .line 202
    :cond_0
    return-void
.end method

.method public final a(Lcom/google/android/location/e/bh;)V
    .locals 2

    .prologue
    .line 328
    invoke-virtual {p0}, Lcom/google/android/location/u;->g()V

    .line 329
    iget-object v0, p0, Lcom/google/android/location/u;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/os/a;

    .line 330
    invoke-interface {v0, p1}, Lcom/google/android/location/os/a;->a(Lcom/google/android/location/e/bh;)V

    goto :goto_0

    .line 332
    :cond_0
    return-void
.end method

.method public final a(Lcom/google/android/location/e/h;)V
    .locals 2

    .prologue
    .line 110
    invoke-virtual {p0}, Lcom/google/android/location/u;->g()V

    .line 111
    iget-object v0, p0, Lcom/google/android/location/u;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/os/a;

    .line 112
    invoke-interface {v0, p1}, Lcom/google/android/location/os/a;->a(Lcom/google/android/location/e/h;)V

    goto :goto_0

    .line 114
    :cond_0
    return-void
.end method

.method public final a(Lcom/google/android/location/j/k;)V
    .locals 2

    .prologue
    .line 102
    invoke-virtual {p0}, Lcom/google/android/location/u;->g()V

    .line 103
    iget-object v0, p0, Lcom/google/android/location/u;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/os/a;

    .line 104
    invoke-interface {v0, p1}, Lcom/google/android/location/os/a;->a(Lcom/google/android/location/j/k;)V

    goto :goto_0

    .line 106
    :cond_0
    return-void
.end method

.method public final a(Lcom/google/android/location/j/k;Lcom/google/android/location/e/b;)V
    .locals 2

    .prologue
    .line 265
    invoke-virtual {p0}, Lcom/google/android/location/u;->g()V

    .line 266
    iget-object v0, p0, Lcom/google/android/location/u;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/os/a;

    .line 267
    invoke-interface {v0, p1, p2}, Lcom/google/android/location/os/a;->a(Lcom/google/android/location/j/k;Lcom/google/android/location/e/b;)V

    goto :goto_0

    .line 269
    :cond_0
    return-void
.end method

.method public final a(Lcom/google/android/location/os/aw;)V
    .locals 2

    .prologue
    .line 134
    invoke-virtual {p0}, Lcom/google/android/location/u;->g()V

    .line 135
    iget-object v0, p0, Lcom/google/android/location/u;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/os/a;

    .line 136
    invoke-interface {v0, p1}, Lcom/google/android/location/os/a;->a(Lcom/google/android/location/os/aw;)V

    goto :goto_0

    .line 138
    :cond_0
    return-void
.end method

.method public final a(Lcom/google/android/location/os/bk;Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 241
    invoke-virtual {p0}, Lcom/google/android/location/u;->g()V

    .line 242
    iget-object v0, p0, Lcom/google/android/location/u;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/os/a;

    .line 243
    invoke-interface {v0, p1, p2}, Lcom/google/android/location/os/a;->a(Lcom/google/android/location/os/bk;Ljava/lang/Object;)V

    goto :goto_0

    .line 245
    :cond_0
    return-void
.end method

.method public final a(Lcom/google/p/a/b/b/a;)V
    .locals 2

    .prologue
    .line 118
    invoke-virtual {p0}, Lcom/google/android/location/u;->g()V

    .line 119
    iget-object v0, p0, Lcom/google/android/location/u;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/os/a;

    .line 120
    invoke-interface {v0, p1}, Lcom/google/android/location/os/a;->a(Lcom/google/p/a/b/b/a;)V

    goto :goto_0

    .line 122
    :cond_0
    return-void
.end method

.method public final a(Z)V
    .locals 2

    .prologue
    .line 166
    invoke-virtual {p0}, Lcom/google/android/location/u;->g()V

    .line 167
    iget-object v0, p0, Lcom/google/android/location/u;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/os/a;

    .line 168
    invoke-interface {v0, p1}, Lcom/google/android/location/os/a;->a(Z)V

    goto :goto_0

    .line 170
    :cond_0
    return-void
.end method

.method public final a(ZLjava/lang/String;)V
    .locals 2

    .prologue
    .line 233
    invoke-virtual {p0}, Lcom/google/android/location/u;->g()V

    .line 234
    iget-object v0, p0, Lcom/google/android/location/u;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/os/a;

    .line 235
    invoke-interface {v0, p1, p2}, Lcom/google/android/location/os/a;->a(ZLjava/lang/String;)V

    goto :goto_0

    .line 237
    :cond_0
    return-void
.end method

.method public final a(ZZ)V
    .locals 2

    .prologue
    .line 150
    invoke-virtual {p0}, Lcom/google/android/location/u;->g()V

    .line 151
    iget-object v0, p0, Lcom/google/android/location/u;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/os/a;

    .line 152
    invoke-interface {v0, p1, p2}, Lcom/google/android/location/os/a;->a(ZZ)V

    goto :goto_0

    .line 154
    :cond_0
    return-void
.end method

.method public final a(ZZI)V
    .locals 2

    .prologue
    .line 190
    invoke-virtual {p0}, Lcom/google/android/location/u;->g()V

    .line 191
    iget-object v0, p0, Lcom/google/android/location/u;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/os/a;

    .line 192
    invoke-interface {v0, p1, p2, p3}, Lcom/google/android/location/os/a;->a(ZZI)V

    goto :goto_0

    .line 194
    :cond_0
    return-void
.end method

.method public final a([Lcom/google/android/location/e/bi;Z)V
    .locals 2

    .prologue
    .line 142
    invoke-virtual {p0}, Lcom/google/android/location/u;->g()V

    .line 143
    iget-object v0, p0, Lcom/google/android/location/u;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/os/a;

    .line 144
    invoke-interface {v0, p1, p2}, Lcom/google/android/location/os/a;->a([Lcom/google/android/location/e/bi;Z)V

    goto :goto_0

    .line 146
    :cond_0
    return-void
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 257
    invoke-virtual {p0}, Lcom/google/android/location/u;->g()V

    .line 258
    iget-object v0, p0, Lcom/google/android/location/u;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/os/a;

    .line 259
    invoke-interface {v0}, Lcom/google/android/location/os/a;->b()V

    goto :goto_0

    .line 261
    :cond_0
    return-void
.end method

.method public final b(Lcom/google/p/a/b/b/a;)V
    .locals 2

    .prologue
    .line 126
    invoke-virtual {p0}, Lcom/google/android/location/u;->g()V

    .line 127
    iget-object v0, p0, Lcom/google/android/location/u;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/os/a;

    .line 128
    invoke-interface {v0, p1}, Lcom/google/android/location/os/a;->b(Lcom/google/p/a/b/b/a;)V

    goto :goto_0

    .line 130
    :cond_0
    return-void
.end method

.method public final b(Z)V
    .locals 2

    .prologue
    .line 344
    invoke-virtual {p0}, Lcom/google/android/location/u;->g()V

    .line 345
    iget-object v0, p0, Lcom/google/android/location/u;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/os/a;

    .line 346
    invoke-interface {v0, p1}, Lcom/google/android/location/os/a;->b(Z)V

    goto :goto_0

    .line 348
    :cond_0
    return-void
.end method

.method public final c()V
    .locals 2

    .prologue
    .line 273
    invoke-virtual {p0}, Lcom/google/android/location/u;->g()V

    .line 274
    iget-object v0, p0, Lcom/google/android/location/u;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/os/a;

    .line 275
    invoke-interface {v0}, Lcom/google/android/location/os/a;->c()V

    goto :goto_0

    .line 277
    :cond_0
    return-void
.end method

.method public final c(Z)V
    .locals 2

    .prologue
    .line 85
    invoke-virtual {p0}, Lcom/google/android/location/u;->g()V

    .line 86
    iget-object v0, p0, Lcom/google/android/location/u;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/os/a;

    .line 87
    invoke-interface {v0, p1}, Lcom/google/android/location/os/a;->c(Z)V

    goto :goto_0

    .line 89
    :cond_0
    return-void
.end method

.method public final d()V
    .locals 2

    .prologue
    .line 77
    invoke-virtual {p0}, Lcom/google/android/location/u;->g()V

    .line 78
    iget-object v0, p0, Lcom/google/android/location/u;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/os/a;

    .line 79
    invoke-interface {v0}, Lcom/google/android/location/os/a;->d()V

    goto :goto_0

    .line 81
    :cond_0
    return-void
.end method

.method public final d(Z)V
    .locals 2

    .prologue
    .line 174
    invoke-virtual {p0}, Lcom/google/android/location/u;->g()V

    .line 175
    iget-object v0, p0, Lcom/google/android/location/u;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/os/a;

    .line 176
    invoke-interface {v0, p1}, Lcom/google/android/location/os/a;->d(Z)V

    goto :goto_0

    .line 178
    :cond_0
    return-void
.end method

.method public final e()V
    .locals 2

    .prologue
    .line 336
    invoke-virtual {p0}, Lcom/google/android/location/u;->g()V

    .line 337
    iget-object v0, p0, Lcom/google/android/location/u;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/os/a;

    .line 338
    invoke-interface {v0}, Lcom/google/android/location/os/a;->e()V

    goto :goto_0

    .line 340
    :cond_0
    return-void
.end method

.method public final e(Z)V
    .locals 2

    .prologue
    .line 182
    invoke-virtual {p0}, Lcom/google/android/location/u;->g()V

    .line 183
    iget-object v0, p0, Lcom/google/android/location/u;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/os/a;

    .line 184
    invoke-interface {v0, p1}, Lcom/google/android/location/os/a;->e(Z)V

    goto :goto_0

    .line 186
    :cond_0
    return-void
.end method

.method public final f(Z)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 289
    invoke-virtual {p0}, Lcom/google/android/location/u;->g()V

    .line 290
    iget-object v0, p0, Lcom/google/android/location/u;->d:Lcom/google/android/location/ap;

    if-nez v0, :cond_1

    .line 291
    sget-boolean v0, Lcom/google/android/location/i/a;->b:Z

    if-eqz v0, :cond_0

    const-string v0, "CallbacksDispatcher"

    const-string v1, "Network provider already destroyed."

    invoke-static {v0, v1}, Lcom/google/android/location/o/a/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 300
    :cond_0
    :goto_0
    return-void

    .line 294
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/location/u;->c()V

    .line 295
    iget-object v0, p0, Lcom/google/android/location/u;->b:Ljava/util/List;

    iget-object v1, p0, Lcom/google/android/location/u;->d:Lcom/google/android/location/ap;

    invoke-interface {v0, v1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 296
    iget-object v0, p0, Lcom/google/android/location/u;->d:Lcom/google/android/location/ap;

    invoke-virtual {v0, p1}, Lcom/google/android/location/ap;->c(Z)V

    .line 297
    iput-object v2, p0, Lcom/google/android/location/u;->d:Lcom/google/android/location/ap;

    .line 298
    iget-object v0, p0, Lcom/google/android/location/u;->e:Lcom/google/android/location/os/i;

    invoke-virtual {v0}, Lcom/google/android/location/os/i;->a()V

    .line 299
    iput-object v2, p0, Lcom/google/android/location/u;->e:Lcom/google/android/location/os/i;

    goto :goto_0
.end method

.method public final f()Z
    .locals 1

    .prologue
    .line 306
    invoke-virtual {p0}, Lcom/google/android/location/u;->g()V

    .line 307
    iget-object v0, p0, Lcom/google/android/location/u;->d:Lcom/google/android/location/ap;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final g()V
    .locals 4

    .prologue
    .line 320
    sget-boolean v0, Lcom/google/android/location/i/a;->a:Z

    if-eqz v0, :cond_0

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/location/u;->c:Ljava/lang/Thread;

    if-eq v0, v1, :cond_0

    .line 321
    new-instance v0, Ljava/lang/IllegalAccessError;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "This method should only be called on thread id "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/location/u;->c:Ljava/lang/Thread;

    invoke-virtual {v2}, Ljava/lang/Thread;->getId()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalAccessError;-><init>(Ljava/lang/String;)V

    throw v0

    .line 324
    :cond_0
    return-void
.end method
