.class final Lcom/google/android/location/places/ui/p;
.super Landroid/widget/ArrayAdapter;
.source "SourceFile"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# instance fields
.field final synthetic a:Lcom/google/android/location/places/ui/k;


# direct methods
.method public constructor <init>(Lcom/google/android/location/places/ui/k;Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 379
    iput-object p1, p0, Lcom/google/android/location/places/ui/p;->a:Lcom/google/android/location/places/ui/k;

    .line 380
    sget v0, Lcom/google/android/gms/l;->cZ:I

    invoke-direct {p0, p2, v0}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I)V

    .line 381
    return-void
.end method


# virtual methods
.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 386
    invoke-virtual {p0, p1}, Lcom/google/android/location/places/ui/p;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/location/places/f;

    .line 388
    if-nez p2, :cond_1

    .line 389
    iget-object v1, p0, Lcom/google/android/location/places/ui/p;->a:Lcom/google/android/location/places/ui/k;

    invoke-virtual {v1}, Lcom/google/android/location/places/ui/k;->getActivity()Landroid/support/v4/app/q;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/q;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v1

    sget v2, Lcom/google/android/gms/l;->cZ:I

    iget-object v3, p0, Lcom/google/android/location/places/ui/p;->a:Lcom/google/android/location/places/ui/k;

    invoke-virtual {v3}, Lcom/google/android/location/places/ui/k;->a()Landroid/widget/ListView;

    move-result-object v3

    invoke-virtual {v1, v2, v3, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 390
    new-instance v2, Lcom/google/android/location/places/ui/q;

    invoke-direct {v2, p0, v6}, Lcom/google/android/location/places/ui/q;-><init>(Lcom/google/android/location/places/ui/p;B)V

    .line 391
    sget v1, Lcom/google/android/gms/j;->ov:I

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, v2, Lcom/google/android/location/places/ui/q;->b:Landroid/widget/TextView;

    .line 392
    sget v1, Lcom/google/android/gms/j;->oh:I

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, v2, Lcom/google/android/location/places/ui/q;->c:Landroid/widget/TextView;

    .line 393
    sget v1, Lcom/google/android/gms/j;->ol:I

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, v2, Lcom/google/android/location/places/ui/q;->d:Landroid/widget/ImageView;

    .line 394
    sget v1, Lcom/google/android/gms/j;->on:I

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, v2, Lcom/google/android/location/places/ui/q;->e:Landroid/view/View;

    .line 395
    invoke-virtual {p2, v2}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    move-object v1, v2

    .line 399
    :goto_0
    iput-object v0, v1, Lcom/google/android/location/places/ui/q;->a:Lcom/google/android/gms/location/places/f;

    .line 400
    iget-object v2, v1, Lcom/google/android/location/places/ui/q;->b:Landroid/widget/TextView;

    invoke-virtual {v0}, Lcom/google/android/gms/location/places/f;->d()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 401
    iget-object v2, v1, Lcom/google/android/location/places/ui/q;->c:Landroid/widget/TextView;

    invoke-virtual {v0}, Lcom/google/android/gms/location/places/f;->c()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 402
    iget-object v2, v1, Lcom/google/android/location/places/ui/q;->d:Landroid/widget/ImageView;

    iget-object v3, p0, Lcom/google/android/location/places/ui/p;->a:Lcom/google/android/location/places/ui/k;

    invoke-static {v3}, Lcom/google/android/location/places/ui/k;->d(Lcom/google/android/location/places/ui/k;)Lcom/google/android/location/places/ui/r;

    move-result-object v3

    invoke-virtual {v0}, Lcom/google/android/gms/location/places/f;->b()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/location/places/PlaceType;

    iget-object v5, v3, Lcom/google/android/location/places/ui/r;->a:Landroid/support/v4/g/s;

    invoke-virtual {v0}, Lcom/google/android/gms/location/places/PlaceType;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Landroid/support/v4/g/s;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    :goto_1
    invoke-virtual {v2, v0}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 404
    invoke-virtual {p0}, Lcom/google/android/location/places/ui/p;->getCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    if-ne p1, v0, :cond_3

    .line 405
    iget-object v0, v1, Lcom/google/android/location/places/ui/q;->e:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 409
    :goto_2
    return-object p2

    .line 397
    :cond_1
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/location/places/ui/q;

    goto :goto_0

    .line 402
    :cond_2
    iget-object v0, v3, Lcom/google/android/location/places/ui/r;->a:Landroid/support/v4/g/s;

    sget-object v3, Lcom/google/android/gms/location/places/PlaceType;->H:Lcom/google/android/gms/location/places/PlaceType;

    invoke-virtual {v3}, Lcom/google/android/gms/location/places/PlaceType;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/support/v4/g/s;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    goto :goto_1

    .line 407
    :cond_3
    iget-object v0, v1, Lcom/google/android/location/places/ui/q;->e:Landroid/view/View;

    invoke-virtual {v0, v6}, Landroid/view/View;->setVisibility(I)V

    goto :goto_2
.end method

.method public final onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 4

    .prologue
    .line 415
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/places/ui/q;

    iget-object v0, v0, Lcom/google/android/location/places/ui/q;->a:Lcom/google/android/gms/location/places/f;

    .line 416
    iget-object v1, p0, Lcom/google/android/location/places/ui/p;->a:Lcom/google/android/location/places/ui/k;

    invoke-static {v1}, Lcom/google/android/location/places/ui/k;->c(Lcom/google/android/location/places/ui/k;)Lcom/google/android/location/places/ui/o;

    move-result-object v1

    const/4 v2, 0x2

    long-to-int v3, p4

    invoke-interface {v1, v0, v2, v3}, Lcom/google/android/location/places/ui/o;->a(Lcom/google/android/gms/location/places/f;II)V

    .line 418
    return-void
.end method
