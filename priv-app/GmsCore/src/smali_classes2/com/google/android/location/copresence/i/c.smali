.class public final Lcom/google/android/location/copresence/i/c;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static e:Lcom/google/android/location/copresence/i/c;


# instance fields
.field public final a:Ljava/lang/Object;

.field public b:Z

.field public c:Z

.field public d:Z

.field private final f:Lcom/google/android/location/copresence/i/d;

.field private final g:Landroid/content/Context;


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 44
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/location/copresence/i/c;-><init>(Landroid/content/Context;B)V

    .line 45
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;B)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 48
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/location/copresence/i/c;->a:Ljava/lang/Object;

    .line 29
    iput-boolean v1, p0, Lcom/google/android/location/copresence/i/c;->b:Z

    .line 30
    iput-boolean v1, p0, Lcom/google/android/location/copresence/i/c;->c:Z

    .line 31
    iput-boolean v1, p0, Lcom/google/android/location/copresence/i/c;->d:Z

    .line 49
    iput-object p1, p0, Lcom/google/android/location/copresence/i/c;->g:Landroid/content/Context;

    .line 50
    const/16 v0, 0x13

    invoke-static {v0}, Lcom/google/android/gms/common/util/ao;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 52
    new-instance v0, Lcom/google/android/location/copresence/i/a;

    invoke-direct {v0, p1}, Lcom/google/android/location/copresence/i/a;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/location/copresence/i/c;->f:Lcom/google/android/location/copresence/i/d;

    .line 57
    :goto_0
    return-void

    .line 54
    :cond_0
    new-instance v0, Lcom/google/android/location/copresence/i/b;

    invoke-direct {v0, p1}, Lcom/google/android/location/copresence/i/b;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/location/copresence/i/c;->f:Lcom/google/android/location/copresence/i/d;

    goto :goto_0
.end method

.method public static declared-synchronized a(Landroid/content/Context;)Lcom/google/android/location/copresence/i/c;
    .locals 3

    .prologue
    .line 34
    const-class v1, Lcom/google/android/location/copresence/i/c;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/google/android/location/copresence/i/c;->e:Lcom/google/android/location/copresence/i/c;

    if-nez v0, :cond_0

    .line 35
    new-instance v0, Lcom/google/android/location/copresence/i/c;

    invoke-direct {v0, p0}, Lcom/google/android/location/copresence/i/c;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/google/android/location/copresence/i/c;->e:Lcom/google/android/location/copresence/i/c;

    .line 37
    const/4 v0, 0x0

    const/4 v2, 0x1

    invoke-static {p0, v0, v2}, Lcom/google/android/location/copresence/debug/CopresDatabaseHelper;->a(Landroid/content/Context;ZZ)V

    .line 40
    :cond_0
    sget-object v0, Lcom/google/android/location/copresence/i/c;->e:Lcom/google/android/location/copresence/i/c;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 34
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final a(Z)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v1, 0x0

    .line 118
    invoke-static {v3}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 119
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "SensorsOnNotificationManager: Calling UpdateNotificationLocked with force = "

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", mIsAdvertising = "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v2, p0, Lcom/google/android/location/copresence/i/c;->c:Z

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", mIsListening = "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v2, p0, Lcom/google/android/location/copresence/i/c;->b:Z

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", and mIsHidden = "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v2, p0, Lcom/google/android/location/copresence/i/c;->d:Z

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->a(Ljava/lang/String;)I

    .line 123
    :cond_0
    iget-boolean v0, p0, Lcom/google/android/location/copresence/i/c;->c:Z

    if-nez v0, :cond_1

    iget-boolean v0, p0, Lcom/google/android/location/copresence/i/c;->b:Z

    if-nez v0, :cond_1

    const/4 v0, 0x1

    .line 124
    :goto_0
    if-nez p1, :cond_2

    iget-boolean v2, p0, Lcom/google/android/location/copresence/i/c;->d:Z

    if-ne v0, v2, :cond_2

    .line 137
    :goto_1
    return-void

    :cond_1
    move v0, v1

    .line 123
    goto :goto_0

    .line 127
    :cond_2
    if-eqz v0, :cond_4

    .line 128
    iget-object v2, p0, Lcom/google/android/location/copresence/i/c;->f:Lcom/google/android/location/copresence/i/d;

    invoke-interface {v2, p1}, Lcom/google/android/location/copresence/i/d;->b(Z)V

    .line 132
    :goto_2
    iput-boolean v0, p0, Lcom/google/android/location/copresence/i/c;->d:Z

    .line 133
    invoke-static {v3}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 134
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "SensorsOnNotificationManager: Setting mIsHidden to "

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v2, p0, Lcom/google/android/location/copresence/i/c;->d:Z

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->a(Ljava/lang/String;)I

    .line 136
    :cond_3
    iget-object v0, p0, Lcom/google/android/location/copresence/i/c;->g:Landroid/content/Context;

    iget-boolean v2, p0, Lcom/google/android/location/copresence/i/c;->d:Z

    invoke-static {v0, v2, v1}, Lcom/google/android/location/copresence/debug/CopresDatabaseHelper;->a(Landroid/content/Context;ZZ)V

    goto :goto_1

    .line 130
    :cond_4
    iget-object v2, p0, Lcom/google/android/location/copresence/i/c;->f:Lcom/google/android/location/copresence/i/d;

    invoke-interface {v2, p1}, Lcom/google/android/location/copresence/i/d;->a(Z)V

    goto :goto_2
.end method
