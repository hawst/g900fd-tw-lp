.class final Lcom/google/android/location/collectionlib/bi;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:I

.field final synthetic b:Lcom/google/p/a/b/b/a;

.field final synthetic c:Lcom/google/android/location/collectionlib/bg;


# direct methods
.method constructor <init>(Lcom/google/android/location/collectionlib/bg;ILcom/google/p/a/b/b/a;)V
    .locals 0

    .prologue
    .line 181
    iput-object p1, p0, Lcom/google/android/location/collectionlib/bi;->c:Lcom/google/android/location/collectionlib/bg;

    iput p2, p0, Lcom/google/android/location/collectionlib/bi;->a:I

    iput-object p3, p0, Lcom/google/android/location/collectionlib/bi;->b:Lcom/google/p/a/b/b/a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 6

    .prologue
    .line 184
    new-instance v0, Lcom/google/android/location/o/h;

    iget-object v1, p0, Lcom/google/android/location/collectionlib/bi;->c:Lcom/google/android/location/collectionlib/bg;

    invoke-static {v1}, Lcom/google/android/location/collectionlib/bg;->c(Lcom/google/android/location/collectionlib/bg;)Landroid/os/PowerManager;

    move-result-object v1

    const/4 v2, 0x1

    const/4 v3, 0x0

    const-class v4, Lcom/google/android/location/collectionlib/bg;

    invoke-virtual {v4}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v4

    sget-object v5, Lcom/google/android/location/o/h;->b:[S

    invoke-direct/range {v0 .. v5}, Lcom/google/android/location/o/h;-><init>(Landroid/os/PowerManager;IZLjava/lang/String;[S)V

    .line 187
    iget-object v1, p0, Lcom/google/android/location/collectionlib/bi;->c:Lcom/google/android/location/collectionlib/bg;

    invoke-static {v1}, Lcom/google/android/location/collectionlib/bg;->d(Lcom/google/android/location/collectionlib/bg;)Lcom/google/android/location/o/n;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/location/o/h;->a(Lcom/google/android/location/o/n;)V

    .line 188
    invoke-virtual {v0}, Lcom/google/android/location/o/h;->a()V

    .line 190
    :try_start_0
    iget-object v1, p0, Lcom/google/android/location/collectionlib/bi;->c:Lcom/google/android/location/collectionlib/bg;

    invoke-static {v1}, Lcom/google/android/location/collectionlib/bg;->e(Lcom/google/android/location/collectionlib/bg;)Z

    move-result v1

    .line 191
    if-nez v1, :cond_1

    .line 192
    iget-object v1, p0, Lcom/google/android/location/collectionlib/bi;->c:Lcom/google/android/location/collectionlib/bg;

    iget-object v1, v1, Lcom/google/android/location/collectionlib/bg;->a:Lcom/google/android/location/collectionlib/ar;

    if-eqz v1, :cond_0

    .line 193
    iget-object v1, p0, Lcom/google/android/location/collectionlib/bi;->c:Lcom/google/android/location/collectionlib/bg;

    iget-object v1, v1, Lcom/google/android/location/collectionlib/bg;->a:Lcom/google/android/location/collectionlib/ar;

    iget v2, p0, Lcom/google/android/location/collectionlib/bi;->a:I

    const/4 v3, 0x0

    const-string v4, "Failed to create lock file."

    invoke-interface {v1, v2, v3, v4}, Lcom/google/android/location/collectionlib/ar;->a(ILjava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 207
    :cond_0
    invoke-virtual {v0}, Lcom/google/android/location/o/h;->b()V

    .line 208
    :goto_0
    return-void

    .line 197
    :cond_1
    :try_start_1
    iget-object v1, p0, Lcom/google/android/location/collectionlib/bi;->c:Lcom/google/android/location/collectionlib/bg;

    iget-object v2, p0, Lcom/google/android/location/collectionlib/bi;->b:Lcom/google/p/a/b/b/a;

    invoke-static {v1, v2}, Lcom/google/android/location/collectionlib/bg;->a(Lcom/google/android/location/collectionlib/bg;Lcom/google/p/a/b/b/a;)Lcom/google/android/location/collectionlib/cd;

    move-result-object v1

    .line 198
    iget-object v2, p0, Lcom/google/android/location/collectionlib/bi;->c:Lcom/google/android/location/collectionlib/bg;

    iget-object v2, v2, Lcom/google/android/location/collectionlib/bg;->a:Lcom/google/android/location/collectionlib/ar;

    if-eqz v2, :cond_2

    .line 199
    iget-boolean v2, v1, Lcom/google/android/location/collectionlib/cd;->a:Z

    if-eqz v2, :cond_3

    .line 200
    iget-object v2, p0, Lcom/google/android/location/collectionlib/bi;->c:Lcom/google/android/location/collectionlib/bg;

    iget-object v2, v2, Lcom/google/android/location/collectionlib/bg;->a:Lcom/google/android/location/collectionlib/ar;

    iget v3, p0, Lcom/google/android/location/collectionlib/bi;->a:I

    iget-object v1, v1, Lcom/google/android/location/collectionlib/cd;->c:Ljava/lang/String;

    invoke-interface {v2, v3, v1}, Lcom/google/android/location/collectionlib/ar;->a(ILjava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 207
    :cond_2
    :goto_1
    invoke-virtual {v0}, Lcom/google/android/location/o/h;->b()V

    goto :goto_0

    .line 202
    :cond_3
    :try_start_2
    iget-object v2, p0, Lcom/google/android/location/collectionlib/bi;->c:Lcom/google/android/location/collectionlib/bg;

    iget-object v2, v2, Lcom/google/android/location/collectionlib/bg;->a:Lcom/google/android/location/collectionlib/ar;

    iget v3, p0, Lcom/google/android/location/collectionlib/bi;->a:I

    iget-object v4, v1, Lcom/google/android/location/collectionlib/cd;->c:Ljava/lang/String;

    iget-object v1, v1, Lcom/google/android/location/collectionlib/cd;->d:Ljava/lang/String;

    invoke-interface {v2, v3, v4, v1}, Lcom/google/android/location/collectionlib/ar;->a(ILjava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    .line 207
    :catchall_0
    move-exception v1

    invoke-virtual {v0}, Lcom/google/android/location/o/h;->b()V

    throw v1
.end method
