.class final Lcom/google/android/location/places/b/i;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:I

.field final synthetic b:Ljava/util/List;

.field final synthetic c:Lcom/google/android/location/places/b/h;


# direct methods
.method constructor <init>(Lcom/google/android/location/places/b/h;ILjava/util/List;)V
    .locals 0

    .prologue
    .line 273
    iput-object p1, p0, Lcom/google/android/location/places/b/i;->c:Lcom/google/android/location/places/b/h;

    iput p2, p0, Lcom/google/android/location/places/b/i;->a:I

    iput-object p3, p0, Lcom/google/android/location/places/b/i;->b:Ljava/util/List;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 7

    .prologue
    const/4 v6, 0x3

    .line 276
    iget-object v0, p0, Lcom/google/android/location/places/b/i;->c:Lcom/google/android/location/places/b/h;

    iget-object v1, v0, Lcom/google/android/location/places/b/h;->a:Lcom/google/android/location/places/b/f;

    iget v0, p0, Lcom/google/android/location/places/b/i;->a:I

    iget-object v2, p0, Lcom/google/android/location/places/b/i;->b:Ljava/util/List;

    iget-boolean v3, v1, Lcom/google/android/location/places/b/f;->e:Z

    if-nez v3, :cond_0

    iget-object v3, v1, Lcom/google/android/location/places/b/f;->f:Lcom/google/android/location/places/b/d;

    invoke-static {v3}, Lcom/google/android/location/places/b/d;->b(Lcom/google/android/location/places/b/d;)Ljava/util/Set;

    move-result-object v3

    iget-object v4, v1, Lcom/google/android/location/places/b/f;->a:Lcom/google/android/location/places/NearbyAlertSubscription;

    invoke-interface {v3, v4}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    const-string v0, "Places"

    invoke-static {v0, v6}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "Places"

    const-string v1, "Nearby alert subscription was canceled before places were retrieved"

    invoke-static {v0, v1}, Lcom/google/android/location/n/aa;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 277
    :cond_0
    :goto_0
    return-void

    .line 276
    :cond_1
    if-eqz v0, :cond_5

    const-string v2, "Places"

    invoke-static {v2, v6}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_2

    const-string v2, "Places"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Place estimation failed, status = "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/google/android/location/n/aa;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_2
    iget-wide v2, v1, Lcom/google/android/location/places/b/f;->d:J

    sget-wide v4, Lcom/google/android/location/places/b/d;->c:J

    cmp-long v0, v2, v4

    if-lez v0, :cond_3

    const/4 v0, 0x1

    iput-boolean v0, v1, Lcom/google/android/location/places/b/f;->e:Z

    iget-object v0, v1, Lcom/google/android/location/places/b/f;->f:Lcom/google/android/location/places/b/d;

    invoke-static {v0}, Lcom/google/android/location/places/b/d;->b(Lcom/google/android/location/places/b/d;)Ljava/util/Set;

    move-result-object v0

    iget-object v1, v1, Lcom/google/android/location/places/b/f;->a:Lcom/google/android/location/places/NearbyAlertSubscription;

    invoke-interface {v0, v1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_3
    const-string v0, "Places"

    invoke-static {v0, v6}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_4

    const-string v0, "Places"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Retrying GooglePlacesServer.search() in "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v4, v1, Lcom/google/android/location/places/b/f;->d:J

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " millis"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/google/android/location/n/aa;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_4
    iget-object v0, v1, Lcom/google/android/location/places/b/f;->f:Lcom/google/android/location/places/b/d;

    invoke-static {v0}, Lcom/google/android/location/places/b/d;->c(Lcom/google/android/location/places/b/d;)Lcom/google/android/location/places/bz;

    move-result-object v0

    new-instance v2, Lcom/google/android/location/places/b/g;

    invoke-direct {v2, v1}, Lcom/google/android/location/places/b/g;-><init>(Lcom/google/android/location/places/b/f;)V

    iget-wide v4, v1, Lcom/google/android/location/places/b/f;->d:J

    invoke-virtual {v0, v2, v4, v5}, Lcom/google/android/location/places/bz;->a(Ljava/lang/Runnable;J)Z

    goto :goto_0

    :cond_5
    const-string v0, "Places"

    const/4 v3, 0x2

    invoke-static {v0, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_6

    const-string v0, "Places"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Received "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " places"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Lcom/google/android/location/n/aa;->d(Ljava/lang/String;Ljava/lang/String;)V

    :cond_6
    new-instance v3, Ljava/util/ArrayList;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v0

    invoke-direct {v3, v0}, Ljava/util/ArrayList;-><init>(I)V

    iget-object v0, v1, Lcom/google/android/location/places/b/f;->a:Lcom/google/android/location/places/NearbyAlertSubscription;

    invoke-virtual {v0}, Lcom/google/android/location/places/NearbyAlertSubscription;->b()Lcom/google/android/gms/location/places/PlaceFilter;

    move-result-object v4

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_7
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/location/places/PlaceLikelihood;

    invoke-virtual {v0}, Lcom/google/android/gms/location/places/PlaceLikelihood;->b()Lcom/google/android/gms/location/places/f;

    move-result-object v0

    invoke-virtual {v4, v0}, Lcom/google/android/gms/location/places/PlaceFilter;->a(Lcom/google/android/gms/location/places/f;)Z

    move-result v5

    if-eqz v5, :cond_7

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_8
    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, v1, Lcom/google/android/location/places/b/f;->f:Lcom/google/android/location/places/b/d;

    iget-object v2, v1, Lcom/google/android/location/places/b/f;->a:Lcom/google/android/location/places/NearbyAlertSubscription;

    iget-object v1, v1, Lcom/google/android/location/places/b/f;->c:Lcom/google/android/location/places/b/j;

    invoke-static {v0, v2, v3, v1}, Lcom/google/android/location/places/b/d;->a(Lcom/google/android/location/places/b/d;Lcom/google/android/location/places/NearbyAlertSubscription;Ljava/util/List;Lcom/google/android/location/places/b/j;)V

    goto/16 :goto_0
.end method
