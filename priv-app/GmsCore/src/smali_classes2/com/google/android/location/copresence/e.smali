.class final Lcom/google/android/location/copresence/e;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Landroid/content/Intent;

.field final synthetic b:Lcom/google/android/location/copresence/d;


# direct methods
.method constructor <init>(Lcom/google/android/location/copresence/d;Landroid/content/Intent;)V
    .locals 0

    .prologue
    .line 177
    iput-object p1, p0, Lcom/google/android/location/copresence/e;->b:Lcom/google/android/location/copresence/d;

    iput-object p2, p0, Lcom/google/android/location/copresence/e;->a:Landroid/content/Intent;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 4

    .prologue
    .line 180
    iget-object v0, p0, Lcom/google/android/location/copresence/e;->a:Landroid/content/Intent;

    const-string v1, "SESSION_ID_EXTRA"

    const-wide/16 v2, -0x1

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v0

    .line 181
    iget-object v2, p0, Lcom/google/android/location/copresence/e;->b:Lcom/google/android/location/copresence/d;

    iget-object v2, v2, Lcom/google/android/location/copresence/d;->a:Lcom/google/android/location/copresence/c;

    iget-wide v2, v2, Lcom/google/android/location/copresence/c;->c:J

    cmp-long v2, v0, v2

    if-eqz v2, :cond_1

    .line 182
    const/4 v2, 0x4

    invoke-static {v2}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 183
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Skipping alarm since it\'s from a previous session: intentSessionId="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " currentSessionId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/location/copresence/e;->b:Lcom/google/android/location/copresence/d;

    iget-object v1, v1, Lcom/google/android/location/copresence/d;->a:Lcom/google/android/location/copresence/c;

    iget-wide v2, v1, Lcom/google/android/location/copresence/c;->c:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " intent="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/location/copresence/e;->a:Landroid/content/Intent;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->c(Ljava/lang/String;)I

    .line 204
    :cond_0
    :goto_0
    return-void

    .line 190
    :cond_1
    iget-object v0, p0, Lcom/google/android/location/copresence/e;->a:Landroid/content/Intent;

    const-string v1, "ALARM_ID_EXTRA"

    const/high16 v2, -0x80000000

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    .line 191
    iget-object v0, p0, Lcom/google/android/location/copresence/e;->b:Lcom/google/android/location/copresence/d;

    iget-object v0, v0, Lcom/google/android/location/copresence/d;->a:Lcom/google/android/location/copresence/c;

    iget-object v0, v0, Lcom/google/android/location/copresence/c;->a:Ljava/util/Map;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Runnable;

    .line 192
    const/4 v2, 0x3

    invoke-static {v2}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 193
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Alarm ring: id="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " runnable="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/location/copresence/ag;->b(Ljava/lang/String;)I

    .line 195
    :cond_2
    if-eqz v0, :cond_3

    .line 196
    iget-object v2, p0, Lcom/google/android/location/copresence/e;->b:Lcom/google/android/location/copresence/d;

    iget-object v2, v2, Lcom/google/android/location/copresence/d;->a:Lcom/google/android/location/copresence/c;

    iget-object v2, v2, Lcom/google/android/location/copresence/c;->b:Ljava/util/Map;

    invoke-interface {v2, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 197
    iget-object v2, p0, Lcom/google/android/location/copresence/e;->b:Lcom/google/android/location/copresence/d;

    iget-object v2, v2, Lcom/google/android/location/copresence/d;->a:Lcom/google/android/location/copresence/c;

    iget-object v2, v2, Lcom/google/android/location/copresence/c;->a:Ljava/util/Map;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v2, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 198
    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    goto :goto_0

    .line 200
    :cond_3
    const/4 v0, 0x5

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 201
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "Didn\'t find runnable for alarmId="

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/location/copresence/e;->a:Landroid/content/Intent;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->d(Ljava/lang/String;)I

    goto :goto_0
.end method
