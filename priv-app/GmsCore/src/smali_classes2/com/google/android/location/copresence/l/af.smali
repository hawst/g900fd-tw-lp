.class final Lcom/google/android/location/copresence/l/af;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/android/volley/w;
.implements Lcom/android/volley/x;


# instance fields
.field final synthetic a:Lcom/google/android/location/copresence/l/ac;


# direct methods
.method private constructor <init>(Lcom/google/android/location/copresence/l/ac;)V
    .locals 0

    .prologue
    .line 388
    iput-object p1, p0, Lcom/google/android/location/copresence/l/af;->a:Lcom/google/android/location/copresence/l/ac;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/location/copresence/l/ac;B)V
    .locals 0

    .prologue
    .line 388
    invoke-direct {p0, p1}, Lcom/google/android/location/copresence/l/af;-><init>(Lcom/google/android/location/copresence/l/ac;)V

    return-void
.end method

.method private a(Lcom/google/protobuf/nano/j;ILjava/util/Map;)V
    .locals 7

    .prologue
    .line 472
    iget-object v0, p0, Lcom/google/android/location/copresence/l/af;->a:Lcom/google/android/location/copresence/l/ac;

    iget-object v1, v0, Lcom/google/android/location/copresence/l/ac;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 473
    :try_start_0
    iget-object v0, p0, Lcom/google/android/location/copresence/l/af;->a:Lcom/google/android/location/copresence/l/ac;

    iget-object v6, v0, Lcom/google/android/location/copresence/l/ac;->m:Lcom/google/protobuf/nano/j;

    .line 474
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 476
    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/google/android/location/copresence/l/af;->a:Lcom/google/android/location/copresence/l/ac;

    invoke-virtual {v0, p1}, Lcom/google/android/location/copresence/l/ac;->a(Lcom/google/protobuf/nano/j;)Lcom/google/ac/b/c/dc;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, v0, Lcom/google/ac/b/c/dc;->d:Lcom/google/ac/b/c/g;

    if-nez v1, :cond_3

    .line 478
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/google/android/location/copresence/l/af;->a:Lcom/google/android/location/copresence/l/ac;

    iget-object v0, v0, Lcom/google/android/location/copresence/l/ac;->j:Lcom/google/android/location/copresence/l/ai;

    iget-object v1, p0, Lcom/google/android/location/copresence/l/af;->a:Lcom/google/android/location/copresence/l/ac;

    iget-object v1, v1, Lcom/google/android/location/copresence/l/ac;->f:Landroid/net/ConnectivityManager;

    iget-object v1, p0, Lcom/google/android/location/copresence/l/af;->a:Lcom/google/android/location/copresence/l/ac;

    invoke-virtual {v1, p1}, Lcom/google/android/location/copresence/l/ac;->a(Lcom/google/protobuf/nano/j;)Lcom/google/ac/b/c/dc;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/location/copresence/l/af;->a:Lcom/google/android/location/copresence/l/ac;

    invoke-virtual {v2, p1}, Lcom/google/android/location/copresence/l/ac;->b(Lcom/google/protobuf/nano/j;)I

    move-result v5

    move-object v2, p1

    move v3, p2

    move-object v4, p3

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/location/copresence/l/ai;->a(Lcom/google/ac/b/c/dc;Lcom/google/protobuf/nano/j;ILjava/util/Map;I)V

    .line 481
    const/4 v0, 0x2

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 482
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "ServerTask: Task http operation completed "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->a(Ljava/lang/String;)I

    .line 486
    :cond_1
    :try_start_1
    iget-object v0, p0, Lcom/google/android/location/copresence/l/af;->a:Lcom/google/android/location/copresence/l/ac;

    invoke-virtual {v0, v6, p1}, Lcom/google/android/location/copresence/l/ac;->a(Lcom/google/protobuf/nano/j;Lcom/google/protobuf/nano/j;)Ljava/lang/Object;

    move-result-object v0

    .line 487
    iget-object v1, p0, Lcom/google/android/location/copresence/l/af;->a:Lcom/google/android/location/copresence/l/ac;

    invoke-virtual {v1, p1}, Lcom/google/android/location/copresence/l/ac;->b(Lcom/google/protobuf/nano/j;)I

    move-result v1

    .line 488
    sparse-switch v1, :sswitch_data_0

    .line 509
    const/4 v0, 0x5

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 510
    const-string v0, "ServerTask:  response header not set by server."

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->d(Ljava/lang/String;)I

    .line 512
    :cond_2
    iget-object v0, p0, Lcom/google/android/location/copresence/l/af;->a:Lcom/google/android/location/copresence/l/ac;

    const/4 v1, 0x0

    const/16 v2, 0xd

    invoke-virtual {v0, v1, v2}, Lcom/google/android/location/copresence/l/ac;->a(Ljava/lang/Exception;I)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    .line 520
    :goto_1
    return-void

    .line 474
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 476
    :cond_3
    iget-object v1, p0, Lcom/google/android/location/copresence/l/af;->a:Lcom/google/android/location/copresence/l/ac;

    iget-object v1, v1, Lcom/google/android/location/copresence/l/ac;->i:Lcom/google/android/location/copresence/f/b;

    iget-object v0, v0, Lcom/google/ac/b/c/dc;->d:Lcom/google/ac/b/c/g;

    invoke-virtual {v1, v0}, Lcom/google/android/location/copresence/f/b;->a(Lcom/google/ac/b/c/g;)V

    goto :goto_0

    .line 490
    :sswitch_0
    :try_start_2
    iget-object v1, p0, Lcom/google/android/location/copresence/l/af;->a:Lcom/google/android/location/copresence/l/ac;

    iget-object v1, v1, Lcom/google/android/location/copresence/l/ac;->d:Lcom/google/android/location/copresence/ap;

    new-instance v2, Lcom/google/android/location/copresence/l/ae;

    iget-object v3, p0, Lcom/google/android/location/copresence/l/af;->a:Lcom/google/android/location/copresence/l/ac;

    invoke-direct {v2, v3, v0}, Lcom/google/android/location/copresence/l/ae;-><init>(Lcom/google/android/location/copresence/l/ac;Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/location/copresence/l/af;->a:Lcom/google/android/location/copresence/l/ac;

    iget-object v0, v0, Lcom/google/android/location/copresence/l/ac;->h:Landroid/os/WorkSource;

    invoke-virtual {v1, v2, v0}, Lcom/google/android/location/copresence/ap;->a(Ljava/lang/Runnable;Landroid/os/WorkSource;)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_1

    .line 515
    :catch_0
    move-exception v0

    .line 518
    iget-object v1, p0, Lcom/google/android/location/copresence/l/af;->a:Lcom/google/android/location/copresence/l/ac;

    iget-object v2, p0, Lcom/google/android/location/copresence/l/af;->a:Lcom/google/android/location/copresence/l/ac;

    const/16 v2, 0x190

    invoke-static {v2}, Lcom/google/android/location/copresence/l/ac;->a(I)I

    move-result v2

    invoke-virtual {v1, v0, v2}, Lcom/google/android/location/copresence/l/ac;->a(Ljava/lang/Exception;I)V

    goto :goto_1

    .line 493
    :sswitch_1
    :try_start_3
    iget-object v0, p0, Lcom/google/android/location/copresence/l/af;->a:Lcom/google/android/location/copresence/l/ac;

    iget-object v0, v0, Lcom/google/android/location/copresence/l/ac;->g:Lcom/google/android/location/copresence/a/a;

    iget-object v0, v0, Lcom/google/android/location/copresence/a/a;->a:Landroid/accounts/Account;

    iget-object v0, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/google/android/location/copresence/a/h;->a(Ljava/lang/String;Z)V

    .line 495
    const/4 v0, 0x3

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/google/android/location/copresence/l/af;->a:Lcom/google/android/location/copresence/l/ac;

    iget-object v0, v0, Lcom/google/android/location/copresence/l/ac;->g:Lcom/google/android/location/copresence/a/a;

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->a(Lcom/google/android/location/copresence/a/a;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "ServerTask: CopresenceDisabled: Requesting SyncSettings for: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->b(Ljava/lang/String;)I

    :cond_4
    iget-object v0, p0, Lcom/google/android/location/copresence/l/af;->a:Lcom/google/android/location/copresence/l/ac;

    iget-object v0, v0, Lcom/google/android/location/copresence/l/ac;->b:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/location/copresence/l/af;->a:Lcom/google/android/location/copresence/l/ac;

    iget-object v1, v1, Lcom/google/android/location/copresence/l/ac;->g:Lcom/google/android/location/copresence/a/a;

    iget-object v1, v1, Lcom/google/android/location/copresence/a/a;->a:Landroid/accounts/Account;

    invoke-static {v0, v1}, Lcom/google/android/location/copresence/m/a;->a(Landroid/content/Context;Landroid/accounts/Account;)V

    iget-object v0, p0, Lcom/google/android/location/copresence/l/af;->a:Lcom/google/android/location/copresence/l/ac;

    const/4 v1, 0x0

    const/16 v2, 0x9cf

    invoke-virtual {v0, v1, v2}, Lcom/google/android/location/copresence/l/ac;->a(Ljava/lang/Exception;I)V

    goto :goto_1

    .line 498
    :sswitch_2
    iget-object v0, p0, Lcom/google/android/location/copresence/l/af;->a:Lcom/google/android/location/copresence/l/ac;

    const/4 v1, 0x0

    const/16 v2, 0x9cb

    invoke-virtual {v0, v1, v2}, Lcom/google/android/location/copresence/l/ac;->a(Ljava/lang/Exception;I)V

    goto :goto_1

    .line 501
    :sswitch_3
    iget-object v0, p0, Lcom/google/android/location/copresence/l/af;->a:Lcom/google/android/location/copresence/l/ac;

    const/4 v1, 0x0

    const/16 v2, 0x9c9

    invoke-virtual {v0, v1, v2}, Lcom/google/android/location/copresence/l/ac;->a(Ljava/lang/Exception;I)V

    goto/16 :goto_1

    .line 504
    :sswitch_4
    iget-object v0, p0, Lcom/google/android/location/copresence/l/af;->a:Lcom/google/android/location/copresence/l/ac;

    iget-object v0, v0, Lcom/google/android/location/copresence/l/ac;->d:Lcom/google/android/location/copresence/ap;

    new-instance v1, Lcom/google/android/location/copresence/l/ah;

    invoke-direct {v1, p0}, Lcom/google/android/location/copresence/l/ah;-><init>(Lcom/google/android/location/copresence/l/af;)V

    iget-object v2, p0, Lcom/google/android/location/copresence/l/af;->a:Lcom/google/android/location/copresence/l/ac;

    iget-object v2, v2, Lcom/google/android/location/copresence/l/ac;->h:Landroid/os/WorkSource;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/location/copresence/ap;->a(Ljava/lang/Runnable;Landroid/os/WorkSource;)V

    .line 505
    iget-object v0, p0, Lcom/google/android/location/copresence/l/af;->a:Lcom/google/android/location/copresence/l/ac;

    const/4 v1, 0x0

    const/16 v2, 0xd

    invoke-virtual {v0, v1, v2}, Lcom/google/android/location/copresence/l/ac;->a(Ljava/lang/Exception;I)V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0

    goto/16 :goto_1

    .line 488
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xc9 -> :sswitch_1
        0xca -> :sswitch_2
        0xcb -> :sswitch_3
        0xcc -> :sswitch_4
    .end sparse-switch
.end method


# virtual methods
.method public final a(Lcom/android/volley/ac;)V
    .locals 10

    .prologue
    const/4 v9, 0x6

    const/4 v5, -0x1

    const/16 v8, 0x190

    const/4 v1, 0x0

    .line 408
    iget-object v0, p0, Lcom/google/android/location/copresence/l/af;->a:Lcom/google/android/location/copresence/l/ac;

    iget-object v2, v0, Lcom/google/android/location/copresence/l/ac;->a:Ljava/lang/Object;

    monitor-enter v2

    .line 409
    :try_start_0
    iget-object v0, p0, Lcom/google/android/location/copresence/l/af;->a:Lcom/google/android/location/copresence/l/ac;

    iget-object v6, v0, Lcom/google/android/location/copresence/l/ac;->m:Lcom/google/protobuf/nano/j;

    .line 410
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 412
    iget-object v7, p1, Lcom/android/volley/ac;->a:Lcom/android/volley/m;

    .line 413
    if-eqz v7, :cond_1

    iget v0, v7, Lcom/android/volley/m;->a:I

    const/16 v2, 0x191

    if-ne v0, v2, :cond_1

    iget-object v0, p0, Lcom/google/android/location/copresence/l/af;->a:Lcom/google/android/location/copresence/l/ac;

    iget v0, v0, Lcom/google/android/location/copresence/l/ac;->k:I

    if-lez v0, :cond_1

    .line 416
    iget-object v0, p0, Lcom/google/android/location/copresence/l/af;->a:Lcom/google/android/location/copresence/l/ac;

    iget-object v0, v0, Lcom/google/android/location/copresence/l/ac;->j:Lcom/google/android/location/copresence/l/ai;

    iget-object v2, p0, Lcom/google/android/location/copresence/l/af;->a:Lcom/google/android/location/copresence/l/ac;

    iget-object v2, v2, Lcom/google/android/location/copresence/l/ac;->f:Landroid/net/ConnectivityManager;

    iget v3, v7, Lcom/android/volley/m;->a:I

    iget-object v4, v7, Lcom/android/volley/m;->c:Ljava/util/Map;

    move-object v2, v1

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/location/copresence/l/ai;->a(Lcom/google/ac/b/c/dc;Lcom/google/protobuf/nano/j;ILjava/util/Map;I)V

    .line 418
    invoke-static {v9}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 419
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "ServerTask: Failed call with invalid auth, retrying "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/location/copresence/l/af;->a:Lcom/google/android/location/copresence/l/ac;

    iget v1, v1, Lcom/google/android/location/copresence/l/ac;->k:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " more times."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->e(Ljava/lang/String;)I

    .line 422
    :cond_0
    iget-object v0, p0, Lcom/google/android/location/copresence/l/af;->a:Lcom/google/android/location/copresence/l/ac;

    iget v1, v0, Lcom/google/android/location/copresence/l/ac;->k:I

    add-int/lit8 v1, v1, -0x1

    iput v1, v0, Lcom/google/android/location/copresence/l/ac;->k:I

    .line 423
    iget-object v0, p0, Lcom/google/android/location/copresence/l/af;->a:Lcom/google/android/location/copresence/l/ac;

    iget-object v0, v0, Lcom/google/android/location/copresence/l/ac;->d:Lcom/google/android/location/copresence/ap;

    new-instance v1, Lcom/google/android/location/copresence/l/ag;

    invoke-direct {v1, p0}, Lcom/google/android/location/copresence/l/ag;-><init>(Lcom/google/android/location/copresence/l/af;)V

    iget-object v2, p0, Lcom/google/android/location/copresence/l/af;->a:Lcom/google/android/location/copresence/l/ac;

    iget-object v2, v2, Lcom/google/android/location/copresence/l/ac;->h:Landroid/os/WorkSource;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/location/copresence/ap;->a(Ljava/lang/Runnable;Landroid/os/WorkSource;)V

    .line 468
    :goto_0
    return-void

    .line 410
    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0

    .line 432
    :cond_1
    if-eqz v7, :cond_3

    .line 433
    iget-object v0, p0, Lcom/google/android/location/copresence/l/af;->a:Lcom/google/android/location/copresence/l/ac;

    iget-object v0, v0, Lcom/google/android/location/copresence/l/ac;->j:Lcom/google/android/location/copresence/l/ai;

    iget-object v2, p0, Lcom/google/android/location/copresence/l/af;->a:Lcom/google/android/location/copresence/l/ac;

    iget-object v2, v2, Lcom/google/android/location/copresence/l/ac;->f:Landroid/net/ConnectivityManager;

    iget v3, v7, Lcom/android/volley/m;->a:I

    iget-object v4, v7, Lcom/android/volley/m;->c:Ljava/util/Map;

    move-object v2, v1

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/location/copresence/l/ai;->a(Lcom/google/ac/b/c/dc;Lcom/google/protobuf/nano/j;ILjava/util/Map;I)V

    .line 446
    iget v0, v7, Lcom/android/volley/m;->a:I

    .line 447
    const/16 v1, 0x12c

    if-ne v0, v1, :cond_4

    .line 449
    iget-object v1, v7, Lcom/android/volley/m;->b:[B

    iget-object v2, v7, Lcom/android/volley/m;->c:Ljava/util/Map;

    :try_start_1
    iget-object v3, p0, Lcom/google/android/location/copresence/l/af;->a:Lcom/google/android/location/copresence/l/ac;

    iget-object v3, v3, Lcom/google/android/location/copresence/l/ac;->c:Lcom/google/protobuf/nano/j;

    invoke-static {v3, v1}, Lcom/google/protobuf/nano/j;->mergeFrom(Lcom/google/protobuf/nano/j;[B)Lcom/google/protobuf/nano/j;

    iget-object v1, p0, Lcom/google/android/location/copresence/l/af;->a:Lcom/google/android/location/copresence/l/ac;

    iget-object v1, v1, Lcom/google/android/location/copresence/l/ac;->c:Lcom/google/protobuf/nano/j;

    invoke-direct {p0, v1, v0, v2}, Lcom/google/android/location/copresence/l/af;->a(Lcom/google/protobuf/nano/j;ILjava/util/Map;)V
    :try_end_1
    .catch Lcom/google/protobuf/nano/i; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-static {v9}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v1

    if-eqz v1, :cond_2

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "ServerTask: NetworkResponse error: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/google/protobuf/nano/i;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/location/copresence/ag;->e(Ljava/lang/String;)I

    :cond_2
    iget-object v1, p0, Lcom/google/android/location/copresence/l/af;->a:Lcom/google/android/location/copresence/l/ac;

    new-instance v2, Lcom/android/volley/ac;

    invoke-direct {v2, v0}, Lcom/android/volley/ac;-><init>(Ljava/lang/Throwable;)V

    iget-object v0, p0, Lcom/google/android/location/copresence/l/af;->a:Lcom/google/android/location/copresence/l/ac;

    invoke-static {v8}, Lcom/google/android/location/copresence/l/ac;->a(I)I

    move-result v0

    invoke-virtual {v1, v2, v0}, Lcom/google/android/location/copresence/l/ac;->a(Ljava/lang/Exception;I)V

    goto :goto_0

    .line 436
    :cond_3
    iget-object v0, p0, Lcom/google/android/location/copresence/l/af;->a:Lcom/google/android/location/copresence/l/ac;

    iget-object v0, v0, Lcom/google/android/location/copresence/l/ac;->j:Lcom/google/android/location/copresence/l/ai;

    iget-object v2, p0, Lcom/google/android/location/copresence/l/af;->a:Lcom/google/android/location/copresence/l/ac;

    iget-object v2, v2, Lcom/google/android/location/copresence/l/ac;->f:Landroid/net/ConnectivityManager;

    move-object v2, v1

    move v3, v8

    move-object v4, v1

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/location/copresence/l/ai;->a(Lcom/google/ac/b/c/dc;Lcom/google/protobuf/nano/j;ILjava/util/Map;I)V

    .line 438
    iget-object v0, p0, Lcom/google/android/location/copresence/l/af;->a:Lcom/google/android/location/copresence/l/ac;

    iget-object v0, v0, Lcom/google/android/location/copresence/l/ac;->g:Lcom/google/android/location/copresence/a/a;

    iget-object v0, v0, Lcom/google/android/location/copresence/a/a;->a:Landroid/accounts/Account;

    iget-object v0, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/location/copresence/l/af;->a:Lcom/google/android/location/copresence/l/ac;

    iget-object v1, v1, Lcom/google/android/location/copresence/l/ac;->b:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v6, v8}, Lcom/google/android/location/copresence/b/a;->a(Ljava/lang/String;Ljava/lang/String;Lcom/google/protobuf/nano/j;I)V

    .line 442
    iget-object v0, p0, Lcom/google/android/location/copresence/l/af;->a:Lcom/google/android/location/copresence/l/ac;

    iget-object v1, p0, Lcom/google/android/location/copresence/l/af;->a:Lcom/google/android/location/copresence/l/ac;

    invoke-static {v8}, Lcom/google/android/location/copresence/l/ac;->a(I)I

    move-result v1

    invoke-virtual {v0, p1, v1}, Lcom/google/android/location/copresence/l/ac;->a(Ljava/lang/Exception;I)V

    goto/16 :goto_0

    .line 452
    :cond_4
    const/16 v1, 0xcc

    if-ne v0, v1, :cond_5

    .line 455
    iget-object v1, p0, Lcom/google/android/location/copresence/l/af;->a:Lcom/google/android/location/copresence/l/ac;

    iget-object v1, v1, Lcom/google/android/location/copresence/l/ac;->c:Lcom/google/protobuf/nano/j;

    iget-object v2, v7, Lcom/android/volley/m;->c:Ljava/util/Map;

    invoke-direct {p0, v1, v0, v2}, Lcom/google/android/location/copresence/l/af;->a(Lcom/google/protobuf/nano/j;ILjava/util/Map;)V

    goto/16 :goto_0

    .line 458
    :cond_5
    iget-object v1, p0, Lcom/google/android/location/copresence/l/af;->a:Lcom/google/android/location/copresence/l/ac;

    iget-object v1, v1, Lcom/google/android/location/copresence/l/ac;->g:Lcom/google/android/location/copresence/a/a;

    iget-object v1, v1, Lcom/google/android/location/copresence/a/a;->a:Landroid/accounts/Account;

    iget-object v1, v1, Landroid/accounts/Account;->name:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/location/copresence/l/af;->a:Lcom/google/android/location/copresence/l/ac;

    iget-object v2, v2, Lcom/google/android/location/copresence/l/ac;->b:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v6, v0}, Lcom/google/android/location/copresence/b/a;->a(Ljava/lang/String;Ljava/lang/String;Lcom/google/protobuf/nano/j;I)V

    .line 463
    invoke-static {v9}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 464
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "ServerTask: Bad status response: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/location/copresence/l/af;->a:Lcom/google/android/location/copresence/l/ac;

    iget-object v1, v1, Lcom/google/android/location/copresence/l/ac;->j:Lcom/google/android/location/copresence/l/ai;

    invoke-static {p1}, Lcom/google/android/location/copresence/l/ai;->a(Lcom/android/volley/ac;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->e(Ljava/lang/String;)I

    .line 467
    :cond_6
    iget-object v0, p0, Lcom/google/android/location/copresence/l/af;->a:Lcom/google/android/location/copresence/l/ac;

    iget-object v1, p0, Lcom/google/android/location/copresence/l/af;->a:Lcom/google/android/location/copresence/l/ac;

    iget-object v1, p1, Lcom/android/volley/ac;->a:Lcom/android/volley/m;

    iget v1, v1, Lcom/android/volley/m;->a:I

    invoke-static {v1}, Lcom/google/android/location/copresence/l/ac;->a(I)I

    move-result v1

    invoke-virtual {v0, p1, v1}, Lcom/google/android/location/copresence/l/ac;->a(Ljava/lang/Exception;I)V

    goto/16 :goto_0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 388
    check-cast p1, Lcom/google/protobuf/nano/j;

    const/16 v0, 0xc8

    sget-object v1, Lcom/google/android/location/copresence/l/ai;->a:Ljava/util/Map;

    invoke-direct {p0, p1, v0, v1}, Lcom/google/android/location/copresence/l/af;->a(Lcom/google/protobuf/nano/j;ILjava/util/Map;)V

    return-void
.end method
