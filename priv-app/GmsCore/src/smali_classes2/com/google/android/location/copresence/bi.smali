.class public final Lcom/google/android/location/copresence/bi;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/location/copresence/ab;
.implements Lcom/google/android/location/copresence/e/h;


# static fields
.field private static q:Lcom/google/android/location/copresence/bi;

.field private static final r:[Lcom/google/android/location/copresence/q/n;


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Lcom/google/android/location/copresence/n/f;

.field private final c:Lcom/google/android/location/copresence/z;

.field private final d:Lcom/google/android/location/copresence/z;

.field private final e:Lcom/google/android/location/copresence/z;

.field private final f:Lcom/google/android/location/copresence/an;

.field private g:I

.field private final h:Ljava/util/HashSet;

.field private final i:Lcom/google/android/location/copresence/bf;

.field private final j:Lcom/google/android/location/copresence/bj;

.field private final k:Lcom/google/android/location/copresence/l/m;

.field private final l:Lcom/google/android/location/copresence/q/g;

.field private final m:Ljava/util/HashMap;

.field private n:Lcom/google/android/location/copresence/ab;

.field private o:I

.field private final p:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 458
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/android/location/copresence/q/n;

    sput-object v0, Lcom/google/android/location/copresence/bi;->r:[Lcom/google/android/location/copresence/q/n;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 87
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 75
    iput v2, p0, Lcom/google/android/location/copresence/bi;->o:I

    .line 76
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/location/copresence/bi;->p:Ljava/lang/Object;

    .line 88
    iput-object p1, p0, Lcom/google/android/location/copresence/bi;->a:Landroid/content/Context;

    .line 89
    invoke-static {p1}, Lcom/google/android/location/copresence/n/f;->a(Landroid/content/Context;)Lcom/google/android/location/copresence/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/copresence/bi;->b:Lcom/google/android/location/copresence/n/f;

    .line 90
    iget-object v0, p0, Lcom/google/android/location/copresence/bi;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/location/copresence/c/a;->a(Landroid/content/Context;)Lcom/google/android/location/copresence/c/a;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/location/copresence/c/a;->b:Lcom/google/android/location/copresence/z;

    iput-object v0, p0, Lcom/google/android/location/copresence/bi;->d:Lcom/google/android/location/copresence/z;

    .line 91
    iget-object v0, p0, Lcom/google/android/location/copresence/bi;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/location/copresence/c/a;->a(Landroid/content/Context;)Lcom/google/android/location/copresence/c/a;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/location/copresence/c/a;->d:Lcom/google/android/location/copresence/z;

    iput-object v0, p0, Lcom/google/android/location/copresence/bi;->e:Lcom/google/android/location/copresence/z;

    .line 93
    iget-object v0, p0, Lcom/google/android/location/copresence/bi;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/location/copresence/r/ag;->a(Landroid/content/Context;)Lcom/google/android/location/copresence/r/ag;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/location/copresence/r/ag;->d:Lcom/google/android/location/copresence/z;

    iput-object v0, p0, Lcom/google/android/location/copresence/bi;->c:Lcom/google/android/location/copresence/z;

    .line 94
    new-instance v0, Lcom/google/android/location/copresence/bf;

    invoke-static {p1}, Lcom/google/android/location/copresence/i/c;->a(Landroid/content/Context;)Lcom/google/android/location/copresence/i/c;

    move-result-object v1

    invoke-direct {v0, v2, v1}, Lcom/google/android/location/copresence/bf;-><init>(ZLcom/google/android/location/copresence/i/c;)V

    iput-object v0, p0, Lcom/google/android/location/copresence/bi;->i:Lcom/google/android/location/copresence/bf;

    .line 96
    invoke-static {p1}, Lcom/google/android/location/copresence/an;->a(Landroid/content/Context;)Lcom/google/android/location/copresence/an;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/copresence/bi;->f:Lcom/google/android/location/copresence/an;

    .line 97
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/location/copresence/bi;->h:Ljava/util/HashSet;

    .line 98
    new-instance v0, Lcom/google/android/location/copresence/bj;

    invoke-direct {v0, p0, v2}, Lcom/google/android/location/copresence/bj;-><init>(Lcom/google/android/location/copresence/bi;B)V

    iput-object v0, p0, Lcom/google/android/location/copresence/bi;->j:Lcom/google/android/location/copresence/bj;

    .line 99
    invoke-static {p1}, Lcom/google/android/location/copresence/l/m;->a(Landroid/content/Context;)Lcom/google/android/location/copresence/l/m;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/copresence/bi;->k:Lcom/google/android/location/copresence/l/m;

    .line 100
    invoke-static {p1}, Lcom/google/android/location/copresence/ax;->a(Landroid/content/Context;)Lcom/google/android/location/copresence/q/g;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/copresence/bi;->l:Lcom/google/android/location/copresence/q/g;

    .line 101
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/location/copresence/bi;->m:Ljava/util/HashMap;

    .line 102
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/location/copresence/bi;->n:Lcom/google/android/location/copresence/ab;

    .line 103
    invoke-static {p1}, Lcom/google/android/location/copresence/e/d;->a(Landroid/content/Context;)Lcom/google/android/location/copresence/e/d;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/android/location/copresence/e/d;->a(Lcom/google/android/location/copresence/e/h;)V

    .line 104
    return-void
.end method

.method public static declared-synchronized a(Landroid/content/Context;)Lcom/google/android/location/copresence/bi;
    .locals 2

    .prologue
    .line 81
    const-class v1, Lcom/google/android/location/copresence/bi;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/google/android/location/copresence/bi;->q:Lcom/google/android/location/copresence/bi;

    if-nez v0, :cond_0

    .line 82
    new-instance v0, Lcom/google/android/location/copresence/bi;

    invoke-direct {v0, p0}, Lcom/google/android/location/copresence/bi;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/google/android/location/copresence/bi;->q:Lcom/google/android/location/copresence/bi;

    .line 84
    :cond_0
    sget-object v0, Lcom/google/android/location/copresence/bi;->q:Lcom/google/android/location/copresence/bi;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 81
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method static synthetic a(Lcom/google/android/location/copresence/bi;)Lcom/google/android/location/copresence/z;
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lcom/google/android/location/copresence/bi;->d:Lcom/google/android/location/copresence/z;

    return-object v0
.end method

.method private a(Lcom/google/android/location/copresence/ad;)[Lcom/google/android/location/copresence/q/n;
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    const/4 v3, 0x3

    const/4 v2, 0x2

    .line 460
    iget v0, p1, Lcom/google/android/location/copresence/ad;->a:I

    if-ne v0, v2, :cond_0

    iget-object v0, p0, Lcom/google/android/location/copresence/bi;->m:Ljava/util/HashMap;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 462
    new-array v1, v5, [Lcom/google/android/location/copresence/q/n;

    iget-object v0, p0, Lcom/google/android/location/copresence/bi;->m:Ljava/util/HashMap;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/copresence/q/n;

    aput-object v0, v1, v4

    move-object v0, v1

    .line 468
    :goto_0
    return-object v0

    .line 464
    :cond_0
    iget v0, p1, Lcom/google/android/location/copresence/ad;->a:I

    if-ne v0, v3, :cond_1

    iget-object v0, p0, Lcom/google/android/location/copresence/bi;->m:Ljava/util/HashMap;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 466
    new-array v1, v5, [Lcom/google/android/location/copresence/q/n;

    iget-object v0, p0, Lcom/google/android/location/copresence/bi;->m:Ljava/util/HashMap;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/copresence/q/n;

    aput-object v0, v1, v4

    move-object v0, v1

    goto :goto_0

    .line 468
    :cond_1
    sget-object v0, Lcom/google/android/location/copresence/bi;->r:[Lcom/google/android/location/copresence/q/n;

    goto :goto_0
.end method

.method static synthetic b(Lcom/google/android/location/copresence/bi;)Lcom/google/android/location/copresence/z;
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lcom/google/android/location/copresence/bi;->e:Lcom/google/android/location/copresence/z;

    return-object v0
.end method

.method private b(I)V
    .locals 3

    .prologue
    const/4 v2, 0x3

    .line 417
    invoke-static {v2}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 418
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "TokenListener: ActionReceiver stopForMedium "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->b(Ljava/lang/String;)I

    .line 420
    :cond_0
    iget-object v0, p0, Lcom/google/android/location/copresence/bi;->i:Lcom/google/android/location/copresence/bf;

    invoke-virtual {v0, p1}, Lcom/google/android/location/copresence/bf;->a(I)V

    .line 421
    invoke-static {v2}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 422
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "TokenListener: active mediums: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/location/copresence/bi;->i:Lcom/google/android/location/copresence/bf;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->b(Ljava/lang/String;)I

    .line 424
    :cond_1
    iget-object v0, p0, Lcom/google/android/location/copresence/bi;->m:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/copresence/ad;

    .line 425
    if-eqz v0, :cond_2

    .line 426
    packed-switch p1, :pswitch_data_0

    .line 448
    :cond_2
    :goto_0
    :pswitch_0
    return-void

    .line 429
    :pswitch_1
    invoke-virtual {v0}, Lcom/google/android/location/copresence/ad;->b()V

    .line 430
    iget-object v0, p0, Lcom/google/android/location/copresence/bi;->m:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 434
    :pswitch_2
    iget-object v1, p0, Lcom/google/android/location/copresence/bi;->l:Lcom/google/android/location/copresence/q/g;

    invoke-virtual {v1, v0}, Lcom/google/android/location/copresence/q/g;->a(Lcom/google/android/location/copresence/q/n;)V

    .line 435
    iget-object v0, p0, Lcom/google/android/location/copresence/bi;->m:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 440
    :pswitch_3
    iget-object v1, p0, Lcom/google/android/location/copresence/bi;->i:Lcom/google/android/location/copresence/bf;

    const/4 v2, 0x7

    invoke-virtual {v1, v2}, Lcom/google/android/location/copresence/bf;->c(I)Z

    move-result v1

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/google/android/location/copresence/bi;->i:Lcom/google/android/location/copresence/bf;

    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Lcom/google/android/location/copresence/bf;->c(I)Z

    move-result v1

    if-nez v1, :cond_2

    .line 442
    invoke-virtual {v0}, Lcom/google/android/location/copresence/ad;->b()V

    .line 443
    iget-object v0, p0, Lcom/google/android/location/copresence/bi;->m:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 426
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_0
        :pswitch_3
        :pswitch_1
    .end packed-switch
.end method

.method private c()Z
    .locals 1

    .prologue
    .line 248
    iget-object v0, p0, Lcom/google/android/location/copresence/bi;->i:Lcom/google/android/location/copresence/bf;

    iget-object v0, v0, Lcom/google/android/location/copresence/bf;->a:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 4

    .prologue
    .line 133
    sget-object v0, Lcom/google/android/location/copresence/ax;->a:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 134
    iget-object v2, p0, Lcom/google/android/location/copresence/bi;->f:Lcom/google/android/location/copresence/an;

    const/4 v3, 0x2

    invoke-virtual {v2, v3, v0}, Lcom/google/android/location/copresence/an;->a(II)Lcom/google/android/location/copresence/ao;

    move-result-object v2

    .line 136
    if-nez v2, :cond_1

    .line 137
    invoke-direct {p0, v0}, Lcom/google/android/location/copresence/bi;->b(I)V

    goto :goto_0

    .line 138
    :cond_1
    const/4 v2, 0x3

    invoke-static {v2}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 139
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "TokenListener: stopIfNoDirectives: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " still has active directive"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->b(Ljava/lang/String;)I

    goto :goto_0

    .line 142
    :cond_2
    return-void
.end method

.method public final a(I)V
    .locals 3

    .prologue
    const/4 v2, 0x3

    .line 238
    invoke-static {v2}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 239
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "TokenListener: onListeningFinished: tokenMedium="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->b(Ljava/lang/String;)I

    .line 241
    :cond_0
    iget-object v0, p0, Lcom/google/android/location/copresence/bi;->i:Lcom/google/android/location/copresence/bf;

    invoke-virtual {v0, p1}, Lcom/google/android/location/copresence/bf;->a(I)V

    .line 242
    invoke-static {v2}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 243
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "TokenListener: active mediums: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/location/copresence/bi;->i:Lcom/google/android/location/copresence/bf;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->b(Ljava/lang/String;)I

    .line 245
    :cond_1
    return-void
.end method

.method public final a(IILcom/google/ac/b/c/o;)V
    .locals 6

    .prologue
    const/4 v5, 0x6

    const/4 v0, 0x1

    const/4 v1, 0x0

    const/4 v4, 0x3

    .line 306
    iget-object v2, p0, Lcom/google/android/location/copresence/bi;->i:Lcom/google/android/location/copresence/bf;

    invoke-virtual {v2, p1}, Lcom/google/android/location/copresence/bf;->c(I)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 307
    invoke-static {v4}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 308
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "TokenListener:  medium "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " has been started, ignoring."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->b(Ljava/lang/String;)I

    .line 402
    :cond_0
    :goto_0
    return-void

    .line 313
    :cond_1
    invoke-static {v4}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 314
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "TokenListener:  starting listening on medium: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/location/copresence/ag;->b(Ljava/lang/String;)I

    .line 318
    :cond_2
    invoke-direct {p0}, Lcom/google/android/location/copresence/bi;->c()Z

    move-result v2

    if-nez v2, :cond_6

    .line 319
    invoke-static {v4}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 320
    const-string v2, "E2E Listen: TokenListener:  starting"

    invoke-static {v2}, Lcom/google/android/location/copresence/ag;->b(Ljava/lang/String;)I

    .line 322
    :cond_3
    iput v1, p0, Lcom/google/android/location/copresence/bi;->g:I

    .line 323
    iget-object v2, p0, Lcom/google/android/location/copresence/bi;->h:Ljava/util/HashSet;

    invoke-virtual {v2}, Ljava/util/HashSet;->clear()V

    .line 324
    const/4 v2, 0x5

    invoke-static {v2}, Lcom/google/android/location/copresence/b/a;->c(I)V

    .line 329
    :goto_1
    iget-object v2, p0, Lcom/google/android/location/copresence/bi;->i:Lcom/google/android/location/copresence/bf;

    invoke-virtual {v2, p1}, Lcom/google/android/location/copresence/bf;->b(I)V

    .line 330
    invoke-static {v4}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 331
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "TokenListener: ActionReceiver listenForMedium "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/location/copresence/ag;->b(Ljava/lang/String;)I

    .line 333
    :cond_4
    if-ne p2, v0, :cond_7

    :goto_2
    new-instance v1, Lcom/google/android/location/copresence/as;

    invoke-direct {v1, v0, p3}, Lcom/google/android/location/copresence/as;-><init>(ILcom/google/ac/b/c/o;)V

    .line 334
    iget-object v0, p0, Lcom/google/android/location/copresence/bi;->m:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/copresence/ad;

    .line 335
    if-eqz v0, :cond_5

    iget-object v2, v0, Lcom/google/android/location/copresence/ad;->c:Lcom/google/android/location/copresence/as;

    invoke-virtual {v1, v2}, Lcom/google/android/location/copresence/as;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_5

    .line 336
    iget-object v2, p0, Lcom/google/android/location/copresence/bi;->m:Ljava/util/HashMap;

    invoke-virtual {v2, v0}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 337
    iget-object v2, p0, Lcom/google/android/location/copresence/bi;->l:Lcom/google/android/location/copresence/q/g;

    invoke-virtual {v2, v0}, Lcom/google/android/location/copresence/q/g;->a(Lcom/google/android/location/copresence/q/n;)V

    .line 338
    const/4 v0, 0x0

    .line 340
    :cond_5
    if-nez v0, :cond_0

    .line 343
    new-instance v0, Lcom/google/android/location/copresence/ad;

    iget-object v2, p0, Lcom/google/android/location/copresence/bi;->a:Landroid/content/Context;

    invoke-static {v2, p1}, Lcom/google/android/location/copresence/ax;->b(Landroid/content/Context;I)Lcom/google/android/location/copresence/z;

    move-result-object v2

    invoke-direct {v0, p1, v2, p0, v1}, Lcom/google/android/location/copresence/ad;-><init>(ILcom/google/android/location/copresence/z;Lcom/google/android/location/copresence/ab;Lcom/google/android/location/copresence/as;)V

    .line 345
    iget-object v1, p0, Lcom/google/android/location/copresence/bi;->m:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 346
    packed-switch p1, :pswitch_data_0

    .line 397
    :pswitch_0
    invoke-static {v5}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 398
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "TokenListener: Unknown medium type: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->e(Ljava/lang/String;)I

    goto/16 :goto_0

    .line 326
    :cond_6
    invoke-static {v5}, Lcom/google/android/location/copresence/b/a;->c(I)V

    goto/16 :goto_1

    :cond_7
    move v0, v1

    .line 333
    goto :goto_2

    .line 348
    :pswitch_1
    iget-object v1, p0, Lcom/google/android/location/copresence/bi;->d:Lcom/google/android/location/copresence/z;

    invoke-interface {v1}, Lcom/google/android/location/copresence/z;->b()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 349
    invoke-static {v4}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v1

    if-eqz v1, :cond_8

    .line 350
    const-string v1, "E2E Listen: Audio can listen (audible)"

    invoke-static {v1}, Lcom/google/android/location/copresence/ag;->b(Ljava/lang/String;)I

    .line 352
    :cond_8
    invoke-virtual {v0}, Lcom/google/android/location/copresence/ad;->a()V

    goto/16 :goto_0

    .line 356
    :pswitch_2
    iget-object v1, p0, Lcom/google/android/location/copresence/bi;->e:Lcom/google/android/location/copresence/z;

    invoke-interface {v1}, Lcom/google/android/location/copresence/z;->b()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 357
    invoke-static {v4}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v1

    if-eqz v1, :cond_9

    .line 358
    const-string v1, "E2E Listen: Audio can listen (inaudible)"

    invoke-static {v1}, Lcom/google/android/location/copresence/ag;->b(Ljava/lang/String;)I

    .line 360
    :cond_9
    invoke-virtual {v0}, Lcom/google/android/location/copresence/ad;->a()V

    goto/16 :goto_0

    .line 364
    :pswitch_3
    iget-object v1, v0, Lcom/google/android/location/copresence/ad;->b:Lcom/google/android/location/copresence/z;

    invoke-interface {v1}, Lcom/google/android/location/copresence/z;->b()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 365
    invoke-static {v4}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v1

    if-eqz v1, :cond_a

    .line 366
    const-string v1, "E2E Listen: Bluetooth Classic can listen"

    invoke-static {v1}, Lcom/google/android/location/copresence/ag;->b(Ljava/lang/String;)I

    .line 368
    :cond_a
    new-instance v1, Lcom/google/android/location/copresence/q/j;

    invoke-static {}, Lcom/google/android/location/copresence/f/b;->b()Lcom/google/ac/b/c/g;

    move-result-object v2

    iget-object v2, v2, Lcom/google/ac/b/c/g;->h:Lcom/google/ac/b/c/e;

    iget-object v2, v2, Lcom/google/ac/b/c/e;->e:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {}, Lcom/google/android/location/copresence/f/b;->b()Lcom/google/ac/b/c/g;

    move-result-object v4

    iget-object v4, v4, Lcom/google/ac/b/c/g;->h:Lcom/google/ac/b/c/e;

    iget-object v4, v4, Lcom/google/ac/b/c/e;->f:Ljava/lang/Long;

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/google/android/location/copresence/q/j;-><init>(JJ)V

    .line 371
    iget-object v2, p0, Lcom/google/android/location/copresence/bi;->l:Lcom/google/android/location/copresence/q/g;

    invoke-direct {p0, v0}, Lcom/google/android/location/copresence/bi;->a(Lcom/google/android/location/copresence/ad;)[Lcom/google/android/location/copresence/q/n;

    move-result-object v3

    invoke-virtual {v2, v0, v1, v3}, Lcom/google/android/location/copresence/q/g;->a(Lcom/google/android/location/copresence/q/n;Lcom/google/android/location/copresence/q/j;[Lcom/google/android/location/copresence/q/n;)V

    goto/16 :goto_0

    .line 377
    :pswitch_4
    iget-object v1, v0, Lcom/google/android/location/copresence/ad;->b:Lcom/google/android/location/copresence/z;

    invoke-interface {v1}, Lcom/google/android/location/copresence/z;->b()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 378
    invoke-static {v4}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v1

    if-eqz v1, :cond_b

    .line 379
    const-string v1, "E2E Listen: Bluetooth LE can listen"

    invoke-static {v1}, Lcom/google/android/location/copresence/ag;->b(Ljava/lang/String;)I

    .line 381
    :cond_b
    new-instance v1, Lcom/google/android/location/copresence/q/j;

    invoke-static {}, Lcom/google/android/location/copresence/f/b;->b()Lcom/google/ac/b/c/g;

    move-result-object v2

    iget-object v2, v2, Lcom/google/ac/b/c/g;->i:Lcom/google/ac/b/c/d;

    iget-object v2, v2, Lcom/google/ac/b/c/d;->a:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {}, Lcom/google/android/location/copresence/f/b;->b()Lcom/google/ac/b/c/g;

    move-result-object v4

    iget-object v4, v4, Lcom/google/ac/b/c/g;->i:Lcom/google/ac/b/c/d;

    iget-object v4, v4, Lcom/google/ac/b/c/d;->b:Ljava/lang/Long;

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/google/android/location/copresence/q/j;-><init>(JJ)V

    .line 384
    iget-object v2, p0, Lcom/google/android/location/copresence/bi;->l:Lcom/google/android/location/copresence/q/g;

    invoke-direct {p0, v0}, Lcom/google/android/location/copresence/bi;->a(Lcom/google/android/location/copresence/ad;)[Lcom/google/android/location/copresence/q/n;

    move-result-object v3

    invoke-virtual {v2, v0, v1, v3}, Lcom/google/android/location/copresence/q/g;->a(Lcom/google/android/location/copresence/q/n;Lcom/google/android/location/copresence/q/j;[Lcom/google/android/location/copresence/q/n;)V

    goto/16 :goto_0

    .line 389
    :pswitch_5
    iget-object v1, v0, Lcom/google/android/location/copresence/ad;->b:Lcom/google/android/location/copresence/z;

    invoke-interface {v1}, Lcom/google/android/location/copresence/z;->b()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 390
    invoke-static {v4}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v1

    if-eqz v1, :cond_c

    .line 391
    const-string v1, "E2E Listen: Wifi Direct/AP can listen"

    invoke-static {v1}, Lcom/google/android/location/copresence/ag;->b(Ljava/lang/String;)I

    .line 393
    :cond_c
    invoke-virtual {v0}, Lcom/google/android/location/copresence/ad;->a()V

    goto/16 :goto_0

    .line 346
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_0
        :pswitch_0
        :pswitch_5
        :pswitch_1
    .end packed-switch
.end method

.method public final declared-synchronized a(Lcom/google/android/gms/location/copresence/x;Lcom/google/ac/b/c/bu;)V
    .locals 7

    .prologue
    const/4 v0, 0x0

    .line 173
    monitor-enter p0

    :try_start_0
    iget-object v2, p2, Lcom/google/ac/b/c/bu;->b:[Lcom/google/ac/b/c/bv;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    .line 174
    const/4 v5, 0x3

    invoke-static {v5}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 175
    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "E2E Listen: step 3"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v6, v4, Lcom/google/ac/b/c/bv;->a:Ljava/lang/Integer;

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v6

    invoke-static {v6}, Lcom/google/android/location/copresence/ag;->b(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ") TokenListener:  detected token "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {p1}, Lcom/google/android/gms/location/copresence/x;->a()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " on medium "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, v4, Lcom/google/ac/b/c/bv;->a:Ljava/lang/Integer;

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v6

    invoke-static {v6}, Lcom/google/android/location/copresence/ag;->c(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/google/android/location/copresence/ag;->b(Ljava/lang/String;)I

    .line 180
    :cond_0
    invoke-virtual {p1}, Lcom/google/android/gms/location/copresence/x;->a()Ljava/lang/String;

    iget-object v4, v4, Lcom/google/ac/b/c/bv;->a:Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    invoke-static {}, Lcom/google/android/location/copresence/debug/d;->b()V

    .line 173
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 182
    :cond_1
    iget-object v1, p0, Lcom/google/android/location/copresence/bi;->n:Lcom/google/android/location/copresence/ab;

    if-eqz v1, :cond_2

    .line 183
    iget-object v1, p0, Lcom/google/android/location/copresence/bi;->n:Lcom/google/android/location/copresence/ab;

    invoke-interface {v1, p1, p2}, Lcom/google/android/location/copresence/ab;->a(Lcom/google/android/gms/location/copresence/x;Lcom/google/ac/b/c/bu;)V

    .line 186
    :cond_2
    iget-object v1, p0, Lcom/google/android/location/copresence/bi;->b:Lcom/google/android/location/copresence/n/f;

    invoke-virtual {v1}, Lcom/google/android/location/copresence/n/f;->b()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/copresence/a/a;

    .line 187
    invoke-static {v0}, Lcom/google/android/location/copresence/p/a;->a(Lcom/google/android/location/copresence/a/a;)Lcom/google/android/location/copresence/p/a;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/location/copresence/bi;->a:Landroid/content/Context;

    invoke-virtual {v3, p1}, Lcom/google/android/location/copresence/p/a;->a(Lcom/google/android/gms/location/copresence/x;)Z

    move-result v3

    or-int/2addr v1, v3

    .line 190
    invoke-static {v0}, Lcom/google/android/location/copresence/p/a;->a(Lcom/google/android/location/copresence/a/a;)Lcom/google/android/location/copresence/p/a;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/location/copresence/bi;->a:Landroid/content/Context;

    invoke-virtual {v3, v4, p1, p2}, Lcom/google/android/location/copresence/p/a;->a(Landroid/content/Context;Lcom/google/android/gms/location/copresence/x;Lcom/google/ac/b/c/bu;)Z

    .line 193
    invoke-static {v0}, Lcom/google/android/location/copresence/p/a;->a(Lcom/google/android/location/copresence/a/a;)Lcom/google/android/location/copresence/p/a;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/location/copresence/p/a;->b(Lcom/google/android/gms/location/copresence/x;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    .line 173
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 197
    :cond_3
    :try_start_1
    iget v0, p0, Lcom/google/android/location/copresence/bi;->g:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/location/copresence/bi;->g:I

    .line 198
    iget-object v0, p0, Lcom/google/android/location/copresence/bi;->h:Ljava/util/HashSet;

    invoke-virtual {v0, p1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 200
    if-eqz v1, :cond_4

    .line 201
    iget-object v0, p0, Lcom/google/android/location/copresence/bi;->k:Lcom/google/android/location/copresence/l/m;

    invoke-static {}, Lcom/google/android/location/copresence/f/b;->b()Lcom/google/ac/b/c/g;

    move-result-object v1

    iget-object v1, v1, Lcom/google/ac/b/c/g;->e:Lcom/google/ac/b/c/m;

    iget-object v1, v1, Lcom/google/ac/b/c/m;->x:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    iget-object v1, p0, Lcom/google/android/location/copresence/bi;->j:Lcom/google/android/location/copresence/bj;

    invoke-virtual {v0, v2, v3, v1}, Lcom/google/android/location/copresence/l/m;->a(JLcom/google/android/location/copresence/l/w;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 204
    :cond_4
    monitor-exit p0

    return-void
.end method

.method public final a(Lcom/google/android/location/copresence/e/f;)V
    .locals 5

    .prologue
    const/4 v2, 0x1

    .line 259
    iget-object v1, p0, Lcom/google/android/location/copresence/bi;->p:Ljava/lang/Object;

    monitor-enter v1

    .line 260
    :try_start_0
    iget v0, p0, Lcom/google/android/location/copresence/bi;->o:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/location/copresence/bi;->o:I

    .line 261
    iget v0, p0, Lcom/google/android/location/copresence/bi;->o:I

    if-ne v0, v2, :cond_0

    .line 262
    iget-object v0, p0, Lcom/google/android/location/copresence/bi;->c:Lcom/google/android/location/copresence/z;

    invoke-interface {v0}, Lcom/google/android/location/copresence/z;->b()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v0, :cond_0

    :try_start_1
    iget-object v0, p0, Lcom/google/android/location/copresence/bi;->c:Lcom/google/android/location/copresence/z;

    new-instance v2, Lcom/google/android/location/copresence/as;

    const/4 v3, 0x0

    invoke-static {}, Lcom/google/android/location/copresence/f/b;->b()Lcom/google/ac/b/c/g;

    move-result-object v4

    iget-object v4, v4, Lcom/google/ac/b/c/g;->b:Lcom/google/ac/b/c/o;

    invoke-direct {v2, v3, v4}, Lcom/google/android/location/copresence/as;-><init>(ILcom/google/ac/b/c/o;)V

    const/4 v3, 0x0

    invoke-interface {v0, p0, v2, v3}, Lcom/google/android/location/copresence/z;->a(Lcom/google/android/location/copresence/ab;Lcom/google/android/location/copresence/as;Lcom/google/android/location/copresence/q/al;)V
    :try_end_1
    .catch Lcom/google/android/location/copresence/ac; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 264
    :cond_0
    :goto_0
    :try_start_2
    monitor-exit v1

    return-void

    .line 262
    :catch_0
    move-exception v0

    const/4 v2, 0x6

    invoke-static {v2}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v2

    if-eqz v2, :cond_1

    const-string v2, "TokenListener: Listen called on passive Wifi listener but not available."

    invoke-static {v2, v0}, Lcom/google/android/location/copresence/ag;->a(Ljava/lang/String;Ljava/lang/Throwable;)I

    :cond_1
    const/4 v0, 0x1

    invoke-static {v0}, Lcom/google/android/location/copresence/b/a;->b(I)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 264
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final a(Ljava/io/PrintWriter;)V
    .locals 2

    .prologue
    .line 451
    const/4 v0, 0x3

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 456
    :goto_0
    return-void

    .line 454
    :cond_0
    const-string v0, "TokenListener:"

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 455
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "  Started medium: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/location/copresence/bi;->i:Lcom/google/android/location/copresence/bf;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final declared-synchronized a(Ljava/util/List;Ljava/util/List;)V
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 209
    monitor-enter p0

    :try_start_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v3

    .line 210
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    move v2, v1

    .line 211
    :goto_0
    if-ge v2, v3, :cond_2

    .line 213
    iget-object v0, p0, Lcom/google/android/location/copresence/bi;->b:Lcom/google/android/location/copresence/n/f;

    invoke-virtual {v0}, Lcom/google/android/location/copresence/n/f;->b()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/copresence/a/a;

    .line 214
    invoke-static {v0}, Lcom/google/android/location/copresence/p/a;->a(Lcom/google/android/location/copresence/a/a;)Lcom/google/android/location/copresence/p/a;

    move-result-object v6

    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/location/copresence/x;

    invoke-virtual {v6, v0}, Lcom/google/android/location/copresence/p/a;->b(Lcom/google/android/gms/location/copresence/x;)I

    move-result v0

    const/4 v6, 0x3

    if-ne v0, v6, :cond_0

    .line 216
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 211
    :cond_1
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 221
    :cond_2
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v5

    .line 222
    if-nez v5, :cond_3

    move v2, v1

    .line 224
    :goto_1
    if-ge v2, v3, :cond_4

    .line 225
    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/location/copresence/x;

    invoke-interface {p2, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/ac/b/c/bu;

    invoke-virtual {p0, v0, v1}, Lcom/google/android/location/copresence/bi;->a(Lcom/google/android/gms/location/copresence/x;Lcom/google/ac/b/c/bu;)V

    .line 224
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_1

    :cond_3
    move v2, v1

    .line 229
    :goto_2
    if-ge v2, v5, :cond_4

    .line 230
    invoke-interface {v4, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 231
    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/location/copresence/x;

    invoke-interface {p2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/ac/b/c/bu;

    invoke-virtual {p0, v0, v1}, Lcom/google/android/location/copresence/bi;->a(Lcom/google/android/gms/location/copresence/x;Lcom/google/ac/b/c/bu;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 229
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_2

    .line 234
    :cond_4
    monitor-exit p0

    return-void

    .line 209
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final b()V
    .locals 2

    .prologue
    const/4 v1, 0x3

    .line 146
    invoke-static {v1}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 147
    const-string v0, "TokenListener: ActionReceiver finish"

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->b(Ljava/lang/String;)I

    .line 149
    :cond_0
    invoke-direct {p0}, Lcom/google/android/location/copresence/bi;->c()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 153
    sget-object v0, Lcom/google/android/location/copresence/ax;->a:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 154
    invoke-direct {p0, v0}, Lcom/google/android/location/copresence/bi;->b(I)V

    goto :goto_0

    .line 157
    :cond_1
    iget-object v0, p0, Lcom/google/android/location/copresence/bi;->i:Lcom/google/android/location/copresence/bf;

    invoke-virtual {v0}, Lcom/google/android/location/copresence/bf;->a()V

    .line 158
    const/4 v0, 0x4

    invoke-static {v0}, Lcom/google/android/location/copresence/b/a;->c(I)V

    .line 162
    :cond_2
    :goto_1
    return-void

    .line 159
    :cond_3
    invoke-static {v1}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 160
    const-string v0, "TokenListener: finish called with empty active medium, ignoring"

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->b(Ljava/lang/String;)I

    goto :goto_1
.end method

.method public final b(Lcom/google/android/location/copresence/e/f;)V
    .locals 3

    .prologue
    .line 274
    iget-object v1, p0, Lcom/google/android/location/copresence/bi;->p:Ljava/lang/Object;

    monitor-enter v1

    .line 275
    :try_start_0
    iget v0, p0, Lcom/google/android/location/copresence/bi;->o:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/google/android/location/copresence/bi;->o:I

    .line 276
    iget v0, p0, Lcom/google/android/location/copresence/bi;->o:I

    if-nez v0, :cond_0

    .line 277
    iget-object v0, p0, Lcom/google/android/location/copresence/bi;->c:Lcom/google/android/location/copresence/z;

    const/4 v2, 0x0

    invoke-interface {v0, v2}, Lcom/google/android/location/copresence/z;->a(Lcom/google/android/location/copresence/q/al;)V

    .line 279
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method
