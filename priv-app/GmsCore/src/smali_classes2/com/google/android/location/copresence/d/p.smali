.class final Lcom/google/android/location/copresence/d/p;
.super Lcom/google/android/location/copresence/k/f;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/location/copresence/d/n;


# direct methods
.method varargs constructor <init>(Lcom/google/android/location/copresence/d/n;Ljava/lang/String;[Lcom/google/android/location/copresence/k/f;)V
    .locals 0

    .prologue
    .line 446
    iput-object p1, p0, Lcom/google/android/location/copresence/d/p;->a:Lcom/google/android/location/copresence/d/n;

    invoke-direct {p0, p2, p3}, Lcom/google/android/location/copresence/k/f;-><init>(Ljava/lang/String;[Lcom/google/android/location/copresence/k/f;)V

    return-void
.end method

.method private e()Ljava/lang/String;
    .locals 3

    .prologue
    .line 475
    iget-object v0, p0, Lcom/google/android/location/copresence/d/p;->a:Lcom/google/android/location/copresence/d/n;

    iget-object v0, v0, Lcom/google/android/location/copresence/d/n;->c:Landroid/content/SharedPreferences;

    const-string v1, "deviceName"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 476
    if-nez v0, :cond_0

    const/4 v1, 0x6

    invoke-static {v1}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 477
    const-string v1, "BluetoothStates: User bluetooth state not stored."

    invoke-static {v1}, Lcom/google/android/location/copresence/ag;->e(Ljava/lang/String;)I

    .line 479
    :cond_0
    return-object v0
.end method


# virtual methods
.method public final a()Z
    .locals 2

    .prologue
    .line 467
    iget-object v0, p0, Lcom/google/android/location/copresence/d/p;->a:Lcom/google/android/location/copresence/d/n;

    iget-object v0, v0, Lcom/google/android/location/copresence/d/n;->g:Lcom/google/android/location/copresence/k/f;

    invoke-virtual {v0}, Lcom/google/android/location/copresence/k/f;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 468
    iget-object v0, p0, Lcom/google/android/location/copresence/d/p;->a:Lcom/google/android/location/copresence/d/n;

    iget-object v0, v0, Lcom/google/android/location/copresence/d/n;->b:Lcom/google/android/location/copresence/d/e;

    invoke-direct {p0}, Lcom/google/android/location/copresence/d/p;->e()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/location/copresence/d/e;->a(Ljava/lang/String;)Z

    move-result v0

    .line 471
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final b()Z
    .locals 2

    .prologue
    .line 458
    iget-object v0, p0, Lcom/google/android/location/copresence/d/p;->a:Lcom/google/android/location/copresence/d/n;

    iget-object v0, v0, Lcom/google/android/location/copresence/d/n;->g:Lcom/google/android/location/copresence/k/f;

    invoke-virtual {v0}, Lcom/google/android/location/copresence/k/f;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 459
    invoke-direct {p0}, Lcom/google/android/location/copresence/d/p;->e()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/location/copresence/d/p;->a:Lcom/google/android/location/copresence/d/n;

    iget-object v1, v1, Lcom/google/android/location/copresence/d/n;->b:Lcom/google/android/location/copresence/d/e;

    iget-object v1, v1, Lcom/google/android/location/copresence/d/e;->a:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v1}, Landroid/bluetooth/BluetoothAdapter;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    .line 462
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final c()Lcom/google/android/location/copresence/k/f;
    .locals 1

    .prologue
    .line 450
    iget-object v0, p0, Lcom/google/android/location/copresence/d/p;->a:Lcom/google/android/location/copresence/d/n;

    iget-object v0, v0, Lcom/google/android/location/copresence/d/n;->g:Lcom/google/android/location/copresence/k/f;

    invoke-virtual {v0}, Lcom/google/android/location/copresence/k/f;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/location/copresence/d/p;->a:Lcom/google/android/location/copresence/d/n;

    iget-object v0, v0, Lcom/google/android/location/copresence/d/n;->f:Lcom/google/android/location/copresence/k/f;

    invoke-virtual {v0}, Lcom/google/android/location/copresence/k/f;->b()Z

    move-result v0

    if-nez v0, :cond_0

    .line 451
    iget-object v0, p0, Lcom/google/android/location/copresence/d/p;->a:Lcom/google/android/location/copresence/d/n;

    iget-object v0, v0, Lcom/google/android/location/copresence/d/n;->f:Lcom/google/android/location/copresence/k/f;

    .line 453
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
