.class public final Lcom/google/android/location/settings/j;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final a:Landroid/content/Context;

.field final b:Lcom/google/android/location/n/w;

.field final c:Landroid/net/wifi/WifiManager;

.field final d:Landroid/content/pm/PackageManager;

.field final e:Landroid/content/ContentResolver;

.field final f:Landroid/os/UserManager;

.field private final g:Landroid/location/LocationManager;

.field private final h:Landroid/os/UserHandle;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 4
    .annotation build Landroid/annotation/TargetApi;
        value = 0x15
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 59
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 60
    iput-object p1, p0, Lcom/google/android/location/settings/j;->a:Landroid/content/Context;

    .line 62
    new-instance v0, Lcom/google/android/location/n/w;

    iget-object v2, p0, Lcom/google/android/location/settings/j;->a:Landroid/content/Context;

    invoke-direct {v0, v2}, Lcom/google/android/location/n/w;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/location/settings/j;->b:Lcom/google/android/location/n/w;

    .line 63
    iget-object v0, p0, Lcom/google/android/location/settings/j;->a:Landroid/content/Context;

    const-string v2, "wifi"

    invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/wifi/WifiManager;

    iput-object v0, p0, Lcom/google/android/location/settings/j;->c:Landroid/net/wifi/WifiManager;

    .line 64
    iget-object v0, p0, Lcom/google/android/location/settings/j;->a:Landroid/content/Context;

    const-string v2, "location"

    invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/location/LocationManager;

    iput-object v0, p0, Lcom/google/android/location/settings/j;->g:Landroid/location/LocationManager;

    .line 65
    iget-object v0, p0, Lcom/google/android/location/settings/j;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/settings/j;->d:Landroid/content/pm/PackageManager;

    .line 66
    iget-object v0, p0, Lcom/google/android/location/settings/j;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/settings/j;->e:Landroid/content/ContentResolver;

    .line 69
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x12

    if-ge v0, v2, :cond_1

    .line 70
    iput-object v1, p0, Lcom/google/android/location/settings/j;->f:Landroid/os/UserManager;

    .line 77
    :goto_0
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x15

    if-lt v0, v2, :cond_3

    .line 78
    iget-object v0, p0, Lcom/google/android/location/settings/j;->f:Landroid/os/UserManager;

    invoke-virtual {v0}, Landroid/os/UserManager;->getUserProfiles()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/UserHandle;

    .line 79
    invoke-virtual {v0}, Landroid/os/UserHandle;->isOwner()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 85
    :goto_1
    if-eqz v0, :cond_2

    invoke-static {}, Landroid/os/Process;->myUserHandle()Landroid/os/UserHandle;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/os/UserHandle;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 86
    iput-object v0, p0, Lcom/google/android/location/settings/j;->h:Landroid/os/UserHandle;

    .line 90
    :goto_2
    return-void

    .line 72
    :cond_1
    iget-object v0, p0, Lcom/google/android/location/settings/j;->a:Landroid/content/Context;

    const-string v2, "user"

    invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/UserManager;

    iput-object v0, p0, Lcom/google/android/location/settings/j;->f:Landroid/os/UserManager;

    goto :goto_0

    .line 88
    :cond_2
    iput-object v1, p0, Lcom/google/android/location/settings/j;->h:Landroid/os/UserHandle;

    goto :goto_2

    :cond_3
    move-object v0, v1

    goto :goto_1
.end method

.method public static c()Z
    .locals 2

    .prologue
    .line 129
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x12

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(Ljava/util/ArrayList;Lcom/google/android/location/settings/l;)V
    .locals 9
    .annotation build Landroid/annotation/TargetApi;
        value = 0x11
    .end annotation

    .prologue
    const/4 v3, 0x1

    const/4 v5, 0x0

    .line 162
    iget-object v0, p0, Lcom/google/android/location/settings/j;->h:Landroid/os/UserHandle;

    if-eqz v0, :cond_1

    .line 163
    iget-object v0, p0, Lcom/google/android/location/settings/j;->a:Landroid/content/Context;

    invoke-static {v0, p1}, Lcom/google/android/location/settings/LocationProviderEnabler;->a(Landroid/content/Context;Ljava/util/ArrayList;)Landroid/content/Intent;

    move-result-object v1

    .line 164
    iget-object v0, p0, Lcom/google/android/location/settings/j;->a:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/location/settings/j;->h:Landroid/os/UserHandle;

    const-string v3, "android.permission.WRITE_SECURE_SETTINGS"

    new-instance v4, Lcom/google/android/location/settings/k;

    invoke-direct {v4, p0, p2}, Lcom/google/android/location/settings/k;-><init>(Lcom/google/android/location/settings/j;Lcom/google/android/location/settings/l;)V

    const/4 v6, 0x0

    move-object v7, v5

    move-object v8, v5

    invoke-virtual/range {v0 .. v8}, Landroid/content/Context;->sendOrderedBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;Ljava/lang/String;Landroid/content/BroadcastReceiver;Landroid/os/Handler;ILjava/lang/String;Landroid/os/Bundle;)V

    .line 190
    :cond_0
    :goto_0
    return-void

    .line 180
    :cond_1
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 182
    const-string v2, "network"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 183
    iget-object v2, p0, Lcom/google/android/location/settings/j;->a:Landroid/content/Context;

    invoke-static {v2, v3}, Lcom/google/android/location/network/NetworkLocationService;->a(Landroid/content/Context;Z)V

    .line 185
    :cond_2
    iget-object v2, p0, Lcom/google/android/location/settings/j;->e:Landroid/content/ContentResolver;

    invoke-static {v2, v0, v3}, Landroid/provider/Settings$Secure;->setLocationProviderEnabled(Landroid/content/ContentResolver;Ljava/lang/String;Z)V

    goto :goto_1

    .line 187
    :cond_3
    if-eqz p2, :cond_0

    .line 188
    invoke-interface {p2}, Lcom/google/android/location/settings/l;->a()V

    goto :goto_0
.end method

.method public final a()Z
    .locals 2

    .prologue
    .line 94
    iget-object v0, p0, Lcom/google/android/location/settings/j;->g:Landroid/location/LocationManager;

    const-string v1, "gps"

    invoke-virtual {v0, v1}, Landroid/location/LocationManager;->isProviderEnabled(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public final b()Z
    .locals 2

    .prologue
    .line 104
    iget-object v0, p0, Lcom/google/android/location/settings/j;->g:Landroid/location/LocationManager;

    const-string v1, "network"

    invoke-virtual {v0, v1}, Landroid/location/LocationManager;->isProviderEnabled(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method
