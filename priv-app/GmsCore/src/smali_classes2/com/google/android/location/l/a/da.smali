.class public final Lcom/google/android/location/l/a/da;
.super Lcom/google/protobuf/nano/d;
.source "SourceFile"


# instance fields
.field public a:[Lcom/google/android/location/l/a/cx;

.field public b:[Lcom/google/android/location/l/a/cx;

.field public c:[Lcom/google/android/location/l/a/cx;

.field public d:[Lcom/google/android/location/l/a/cx;

.field public e:Ljava/lang/String;

.field public f:Lcom/google/android/location/l/a/db;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 41
    invoke-direct {p0}, Lcom/google/protobuf/nano/d;-><init>()V

    .line 42
    invoke-static {}, Lcom/google/android/location/l/a/cx;->a()[Lcom/google/android/location/l/a/cx;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/l/a/da;->a:[Lcom/google/android/location/l/a/cx;

    invoke-static {}, Lcom/google/android/location/l/a/cx;->a()[Lcom/google/android/location/l/a/cx;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/l/a/da;->b:[Lcom/google/android/location/l/a/cx;

    invoke-static {}, Lcom/google/android/location/l/a/cx;->a()[Lcom/google/android/location/l/a/cx;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/l/a/da;->c:[Lcom/google/android/location/l/a/cx;

    invoke-static {}, Lcom/google/android/location/l/a/cx;->a()[Lcom/google/android/location/l/a/cx;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/l/a/da;->d:[Lcom/google/android/location/l/a/cx;

    iput-object v1, p0, Lcom/google/android/location/l/a/da;->e:Ljava/lang/String;

    iput-object v1, p0, Lcom/google/android/location/l/a/da;->f:Lcom/google/android/location/l/a/db;

    iput-object v1, p0, Lcom/google/android/location/l/a/da;->unknownFieldData:Lcom/google/protobuf/nano/f;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/location/l/a/da;->cachedSize:I

    .line 43
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 166
    invoke-super {p0}, Lcom/google/protobuf/nano/d;->computeSerializedSize()I

    move-result v0

    .line 167
    iget-object v2, p0, Lcom/google/android/location/l/a/da;->a:[Lcom/google/android/location/l/a/cx;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/google/android/location/l/a/da;->a:[Lcom/google/android/location/l/a/cx;

    array-length v2, v2

    if-lez v2, :cond_2

    move v2, v0

    move v0, v1

    .line 168
    :goto_0
    iget-object v3, p0, Lcom/google/android/location/l/a/da;->a:[Lcom/google/android/location/l/a/cx;

    array-length v3, v3

    if-ge v0, v3, :cond_1

    .line 169
    iget-object v3, p0, Lcom/google/android/location/l/a/da;->a:[Lcom/google/android/location/l/a/cx;

    aget-object v3, v3, v0

    .line 170
    if-eqz v3, :cond_0

    .line 171
    const/4 v4, 0x1

    invoke-static {v4, v3}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v3

    add-int/2addr v2, v3

    .line 168
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    move v0, v2

    .line 176
    :cond_2
    iget-object v2, p0, Lcom/google/android/location/l/a/da;->b:[Lcom/google/android/location/l/a/cx;

    if-eqz v2, :cond_5

    iget-object v2, p0, Lcom/google/android/location/l/a/da;->b:[Lcom/google/android/location/l/a/cx;

    array-length v2, v2

    if-lez v2, :cond_5

    move v2, v0

    move v0, v1

    .line 177
    :goto_1
    iget-object v3, p0, Lcom/google/android/location/l/a/da;->b:[Lcom/google/android/location/l/a/cx;

    array-length v3, v3

    if-ge v0, v3, :cond_4

    .line 178
    iget-object v3, p0, Lcom/google/android/location/l/a/da;->b:[Lcom/google/android/location/l/a/cx;

    aget-object v3, v3, v0

    .line 179
    if-eqz v3, :cond_3

    .line 180
    const/4 v4, 0x2

    invoke-static {v4, v3}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v3

    add-int/2addr v2, v3

    .line 177
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_4
    move v0, v2

    .line 185
    :cond_5
    iget-object v2, p0, Lcom/google/android/location/l/a/da;->c:[Lcom/google/android/location/l/a/cx;

    if-eqz v2, :cond_8

    iget-object v2, p0, Lcom/google/android/location/l/a/da;->c:[Lcom/google/android/location/l/a/cx;

    array-length v2, v2

    if-lez v2, :cond_8

    move v2, v0

    move v0, v1

    .line 186
    :goto_2
    iget-object v3, p0, Lcom/google/android/location/l/a/da;->c:[Lcom/google/android/location/l/a/cx;

    array-length v3, v3

    if-ge v0, v3, :cond_7

    .line 187
    iget-object v3, p0, Lcom/google/android/location/l/a/da;->c:[Lcom/google/android/location/l/a/cx;

    aget-object v3, v3, v0

    .line 188
    if-eqz v3, :cond_6

    .line 189
    const/4 v4, 0x3

    invoke-static {v4, v3}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v3

    add-int/2addr v2, v3

    .line 186
    :cond_6
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_7
    move v0, v2

    .line 194
    :cond_8
    iget-object v2, p0, Lcom/google/android/location/l/a/da;->d:[Lcom/google/android/location/l/a/cx;

    if-eqz v2, :cond_a

    iget-object v2, p0, Lcom/google/android/location/l/a/da;->d:[Lcom/google/android/location/l/a/cx;

    array-length v2, v2

    if-lez v2, :cond_a

    .line 195
    :goto_3
    iget-object v2, p0, Lcom/google/android/location/l/a/da;->d:[Lcom/google/android/location/l/a/cx;

    array-length v2, v2

    if-ge v1, v2, :cond_a

    .line 196
    iget-object v2, p0, Lcom/google/android/location/l/a/da;->d:[Lcom/google/android/location/l/a/cx;

    aget-object v2, v2, v1

    .line 197
    if-eqz v2, :cond_9

    .line 198
    const/4 v3, 0x4

    invoke-static {v3, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v2

    add-int/2addr v0, v2

    .line 195
    :cond_9
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 203
    :cond_a
    iget-object v1, p0, Lcom/google/android/location/l/a/da;->e:Ljava/lang/String;

    if-eqz v1, :cond_b

    .line 204
    const/4 v1, 0x5

    iget-object v2, p0, Lcom/google/android/location/l/a/da;->e:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 207
    :cond_b
    iget-object v1, p0, Lcom/google/android/location/l/a/da;->f:Lcom/google/android/location/l/a/db;

    if-eqz v1, :cond_c

    .line 208
    const/4 v1, 0x6

    iget-object v2, p0, Lcom/google/android/location/l/a/da;->f:Lcom/google/android/location/l/a/db;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 211
    :cond_c
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 59
    if-ne p1, p0, :cond_1

    .line 60
    const/4 v0, 0x1

    .line 98
    :cond_0
    :goto_0
    return v0

    .line 62
    :cond_1
    instance-of v1, p1, Lcom/google/android/location/l/a/da;

    if-eqz v1, :cond_0

    .line 65
    check-cast p1, Lcom/google/android/location/l/a/da;

    .line 66
    iget-object v1, p0, Lcom/google/android/location/l/a/da;->a:[Lcom/google/android/location/l/a/cx;

    iget-object v2, p1, Lcom/google/android/location/l/a/da;->a:[Lcom/google/android/location/l/a/cx;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 70
    iget-object v1, p0, Lcom/google/android/location/l/a/da;->b:[Lcom/google/android/location/l/a/cx;

    iget-object v2, p1, Lcom/google/android/location/l/a/da;->b:[Lcom/google/android/location/l/a/cx;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 74
    iget-object v1, p0, Lcom/google/android/location/l/a/da;->c:[Lcom/google/android/location/l/a/cx;

    iget-object v2, p1, Lcom/google/android/location/l/a/da;->c:[Lcom/google/android/location/l/a/cx;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 78
    iget-object v1, p0, Lcom/google/android/location/l/a/da;->d:[Lcom/google/android/location/l/a/cx;

    iget-object v2, p1, Lcom/google/android/location/l/a/da;->d:[Lcom/google/android/location/l/a/cx;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 82
    iget-object v1, p0, Lcom/google/android/location/l/a/da;->e:Ljava/lang/String;

    if-nez v1, :cond_4

    .line 83
    iget-object v1, p1, Lcom/google/android/location/l/a/da;->e:Ljava/lang/String;

    if-nez v1, :cond_0

    .line 89
    :cond_2
    iget-object v1, p0, Lcom/google/android/location/l/a/da;->f:Lcom/google/android/location/l/a/db;

    if-nez v1, :cond_5

    .line 90
    iget-object v1, p1, Lcom/google/android/location/l/a/da;->f:Lcom/google/android/location/l/a/db;

    if-nez v1, :cond_0

    .line 98
    :cond_3
    invoke-virtual {p0, p1}, Lcom/google/android/location/l/a/da;->unknownFieldDataEquals(Lcom/google/protobuf/nano/d;)Z

    move-result v0

    goto :goto_0

    .line 86
    :cond_4
    iget-object v1, p0, Lcom/google/android/location/l/a/da;->e:Ljava/lang/String;

    iget-object v2, p1, Lcom/google/android/location/l/a/da;->e:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    goto :goto_0

    .line 94
    :cond_5
    iget-object v1, p0, Lcom/google/android/location/l/a/da;->f:Lcom/google/android/location/l/a/db;

    iget-object v2, p1, Lcom/google/android/location/l/a/da;->f:Lcom/google/android/location/l/a/db;

    invoke-virtual {v1, v2}, Lcom/google/android/location/l/a/db;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 103
    iget-object v0, p0, Lcom/google/android/location/l/a/da;->a:[Lcom/google/android/location/l/a/cx;

    invoke-static {v0}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;)I

    move-result v0

    add-int/lit16 v0, v0, 0x20f

    .line 106
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/android/location/l/a/da;->b:[Lcom/google/android/location/l/a/cx;

    invoke-static {v2}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;)I

    move-result v2

    add-int/2addr v0, v2

    .line 108
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/android/location/l/a/da;->c:[Lcom/google/android/location/l/a/cx;

    invoke-static {v2}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;)I

    move-result v2

    add-int/2addr v0, v2

    .line 110
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/android/location/l/a/da;->d:[Lcom/google/android/location/l/a/cx;

    invoke-static {v2}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;)I

    move-result v2

    add-int/2addr v0, v2

    .line 112
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/location/l/a/da;->e:Ljava/lang/String;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/2addr v0, v2

    .line 114
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/android/location/l/a/da;->f:Lcom/google/android/location/l/a/db;

    if-nez v2, :cond_1

    :goto_1
    add-int/2addr v0, v1

    .line 116
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/google/android/location/l/a/da;->unknownFieldDataHashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 117
    return v0

    .line 112
    :cond_0
    iget-object v0, p0, Lcom/google/android/location/l/a/da;->e:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_0

    .line 114
    :cond_1
    iget-object v1, p0, Lcom/google/android/location/l/a/da;->f:Lcom/google/android/location/l/a/db;

    invoke-virtual {v1}, Lcom/google/android/location/l/a/db;->hashCode()I

    move-result v1

    goto :goto_1
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 5
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/android/location/l/a/da;->storeUnknownField(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    const/16 v0, 0xa

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->b(Lcom/google/protobuf/nano/a;I)I

    move-result v2

    iget-object v0, p0, Lcom/google/android/location/l/a/da;->a:[Lcom/google/android/location/l/a/cx;

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lcom/google/android/location/l/a/cx;

    if-eqz v0, :cond_1

    iget-object v3, p0, Lcom/google/android/location/l/a/da;->a:[Lcom/google/android/location/l/a/cx;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_1
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_3

    new-instance v3, Lcom/google/android/location/l/a/cx;

    invoke-direct {v3}, Lcom/google/android/location/l/a/cx;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    iget-object v0, p0, Lcom/google/android/location/l/a/da;->a:[Lcom/google/android/location/l/a/cx;

    array-length v0, v0

    goto :goto_1

    :cond_3
    new-instance v3, Lcom/google/android/location/l/a/cx;

    invoke-direct {v3}, Lcom/google/android/location/l/a/cx;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    iput-object v2, p0, Lcom/google/android/location/l/a/da;->a:[Lcom/google/android/location/l/a/cx;

    goto :goto_0

    :sswitch_2
    const/16 v0, 0x12

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->b(Lcom/google/protobuf/nano/a;I)I

    move-result v2

    iget-object v0, p0, Lcom/google/android/location/l/a/da;->b:[Lcom/google/android/location/l/a/cx;

    if-nez v0, :cond_5

    move v0, v1

    :goto_3
    add-int/2addr v2, v0

    new-array v2, v2, [Lcom/google/android/location/l/a/cx;

    if-eqz v0, :cond_4

    iget-object v3, p0, Lcom/google/android/location/l/a/da;->b:[Lcom/google/android/location/l/a/cx;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_4
    :goto_4
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_6

    new-instance v3, Lcom/google/android/location/l/a/cx;

    invoke-direct {v3}, Lcom/google/android/location/l/a/cx;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_5
    iget-object v0, p0, Lcom/google/android/location/l/a/da;->b:[Lcom/google/android/location/l/a/cx;

    array-length v0, v0

    goto :goto_3

    :cond_6
    new-instance v3, Lcom/google/android/location/l/a/cx;

    invoke-direct {v3}, Lcom/google/android/location/l/a/cx;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    iput-object v2, p0, Lcom/google/android/location/l/a/da;->b:[Lcom/google/android/location/l/a/cx;

    goto/16 :goto_0

    :sswitch_3
    const/16 v0, 0x1a

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->b(Lcom/google/protobuf/nano/a;I)I

    move-result v2

    iget-object v0, p0, Lcom/google/android/location/l/a/da;->c:[Lcom/google/android/location/l/a/cx;

    if-nez v0, :cond_8

    move v0, v1

    :goto_5
    add-int/2addr v2, v0

    new-array v2, v2, [Lcom/google/android/location/l/a/cx;

    if-eqz v0, :cond_7

    iget-object v3, p0, Lcom/google/android/location/l/a/da;->c:[Lcom/google/android/location/l/a/cx;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_7
    :goto_6
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_9

    new-instance v3, Lcom/google/android/location/l/a/cx;

    invoke-direct {v3}, Lcom/google/android/location/l/a/cx;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_6

    :cond_8
    iget-object v0, p0, Lcom/google/android/location/l/a/da;->c:[Lcom/google/android/location/l/a/cx;

    array-length v0, v0

    goto :goto_5

    :cond_9
    new-instance v3, Lcom/google/android/location/l/a/cx;

    invoke-direct {v3}, Lcom/google/android/location/l/a/cx;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    iput-object v2, p0, Lcom/google/android/location/l/a/da;->c:[Lcom/google/android/location/l/a/cx;

    goto/16 :goto_0

    :sswitch_4
    const/16 v0, 0x22

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->b(Lcom/google/protobuf/nano/a;I)I

    move-result v2

    iget-object v0, p0, Lcom/google/android/location/l/a/da;->d:[Lcom/google/android/location/l/a/cx;

    if-nez v0, :cond_b

    move v0, v1

    :goto_7
    add-int/2addr v2, v0

    new-array v2, v2, [Lcom/google/android/location/l/a/cx;

    if-eqz v0, :cond_a

    iget-object v3, p0, Lcom/google/android/location/l/a/da;->d:[Lcom/google/android/location/l/a/cx;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_a
    :goto_8
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_c

    new-instance v3, Lcom/google/android/location/l/a/cx;

    invoke-direct {v3}, Lcom/google/android/location/l/a/cx;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_8

    :cond_b
    iget-object v0, p0, Lcom/google/android/location/l/a/da;->d:[Lcom/google/android/location/l/a/cx;

    array-length v0, v0

    goto :goto_7

    :cond_c
    new-instance v3, Lcom/google/android/location/l/a/cx;

    invoke-direct {v3}, Lcom/google/android/location/l/a/cx;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    iput-object v2, p0, Lcom/google/android/location/l/a/da;->d:[Lcom/google/android/location/l/a/cx;

    goto/16 :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/l/a/da;->e:Ljava/lang/String;

    goto/16 :goto_0

    :sswitch_6
    iget-object v0, p0, Lcom/google/android/location/l/a/da;->f:Lcom/google/android/location/l/a/db;

    if-nez v0, :cond_d

    new-instance v0, Lcom/google/android/location/l/a/db;

    invoke-direct {v0}, Lcom/google/android/location/l/a/db;-><init>()V

    iput-object v0, p0, Lcom/google/android/location/l/a/da;->f:Lcom/google/android/location/l/a/db;

    :cond_d
    iget-object v0, p0, Lcom/google/android/location/l/a/da;->f:Lcom/google/android/location/l/a/db;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 123
    iget-object v0, p0, Lcom/google/android/location/l/a/da;->a:[Lcom/google/android/location/l/a/cx;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/location/l/a/da;->a:[Lcom/google/android/location/l/a/cx;

    array-length v0, v0

    if-lez v0, :cond_1

    move v0, v1

    .line 124
    :goto_0
    iget-object v2, p0, Lcom/google/android/location/l/a/da;->a:[Lcom/google/android/location/l/a/cx;

    array-length v2, v2

    if-ge v0, v2, :cond_1

    .line 125
    iget-object v2, p0, Lcom/google/android/location/l/a/da;->a:[Lcom/google/android/location/l/a/cx;

    aget-object v2, v2, v0

    .line 126
    if-eqz v2, :cond_0

    .line 127
    const/4 v3, 0x1

    invoke-virtual {p1, v3, v2}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 124
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 131
    :cond_1
    iget-object v0, p0, Lcom/google/android/location/l/a/da;->b:[Lcom/google/android/location/l/a/cx;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/location/l/a/da;->b:[Lcom/google/android/location/l/a/cx;

    array-length v0, v0

    if-lez v0, :cond_3

    move v0, v1

    .line 132
    :goto_1
    iget-object v2, p0, Lcom/google/android/location/l/a/da;->b:[Lcom/google/android/location/l/a/cx;

    array-length v2, v2

    if-ge v0, v2, :cond_3

    .line 133
    iget-object v2, p0, Lcom/google/android/location/l/a/da;->b:[Lcom/google/android/location/l/a/cx;

    aget-object v2, v2, v0

    .line 134
    if-eqz v2, :cond_2

    .line 135
    const/4 v3, 0x2

    invoke-virtual {p1, v3, v2}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 132
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 139
    :cond_3
    iget-object v0, p0, Lcom/google/android/location/l/a/da;->c:[Lcom/google/android/location/l/a/cx;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/google/android/location/l/a/da;->c:[Lcom/google/android/location/l/a/cx;

    array-length v0, v0

    if-lez v0, :cond_5

    move v0, v1

    .line 140
    :goto_2
    iget-object v2, p0, Lcom/google/android/location/l/a/da;->c:[Lcom/google/android/location/l/a/cx;

    array-length v2, v2

    if-ge v0, v2, :cond_5

    .line 141
    iget-object v2, p0, Lcom/google/android/location/l/a/da;->c:[Lcom/google/android/location/l/a/cx;

    aget-object v2, v2, v0

    .line 142
    if-eqz v2, :cond_4

    .line 143
    const/4 v3, 0x3

    invoke-virtual {p1, v3, v2}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 140
    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 147
    :cond_5
    iget-object v0, p0, Lcom/google/android/location/l/a/da;->d:[Lcom/google/android/location/l/a/cx;

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/google/android/location/l/a/da;->d:[Lcom/google/android/location/l/a/cx;

    array-length v0, v0

    if-lez v0, :cond_7

    .line 148
    :goto_3
    iget-object v0, p0, Lcom/google/android/location/l/a/da;->d:[Lcom/google/android/location/l/a/cx;

    array-length v0, v0

    if-ge v1, v0, :cond_7

    .line 149
    iget-object v0, p0, Lcom/google/android/location/l/a/da;->d:[Lcom/google/android/location/l/a/cx;

    aget-object v0, v0, v1

    .line 150
    if-eqz v0, :cond_6

    .line 151
    const/4 v2, 0x4

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 148
    :cond_6
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 155
    :cond_7
    iget-object v0, p0, Lcom/google/android/location/l/a/da;->e:Ljava/lang/String;

    if-eqz v0, :cond_8

    .line 156
    const/4 v0, 0x5

    iget-object v1, p0, Lcom/google/android/location/l/a/da;->e:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 158
    :cond_8
    iget-object v0, p0, Lcom/google/android/location/l/a/da;->f:Lcom/google/android/location/l/a/db;

    if-eqz v0, :cond_9

    .line 159
    const/4 v0, 0x6

    iget-object v1, p0, Lcom/google/android/location/l/a/da;->f:Lcom/google/android/location/l/a/db;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 161
    :cond_9
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/d;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 162
    return-void
.end method
