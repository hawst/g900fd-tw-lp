.class final Lcom/google/android/location/activity/ae;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/hardware/SensorEventListener;


# instance fields
.field final synthetic a:Lcom/google/android/location/activity/ad;

.field private final b:J


# direct methods
.method public constructor <init>(Lcom/google/android/location/activity/ad;J)V
    .locals 0

    .prologue
    .line 106
    iput-object p1, p0, Lcom/google/android/location/activity/ae;->a:Lcom/google/android/location/activity/ad;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 107
    iput-wide p2, p0, Lcom/google/android/location/activity/ae;->b:J

    .line 108
    return-void
.end method


# virtual methods
.method public final onAccuracyChanged(Landroid/hardware/Sensor;I)V
    .locals 0

    .prologue
    .line 148
    return-void
.end method

.method public final onSensorChanged(Landroid/hardware/SensorEvent;)V
    .locals 4

    .prologue
    .line 112
    sget-boolean v0, Lcom/google/android/location/i/a;->b:Z

    if-eqz v0, :cond_0

    const-string v0, "HwTilt"

    const-string v1, "tilt detected"

    invoke-static {v0, v1}, Lcom/google/android/location/o/a/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 122
    :cond_0
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/google/android/location/activity/ae;->b:J

    sub-long/2addr v0, v2

    const-wide/16 v2, 0x3e8

    cmp-long v0, v0, v2

    if-gez v0, :cond_2

    .line 145
    :cond_1
    :goto_0
    return-void

    .line 126
    :cond_2
    iget-object v0, p0, Lcom/google/android/location/activity/ae;->a:Lcom/google/android/location/activity/ad;

    iget-object v0, v0, Lcom/google/android/location/activity/ad;->a:Lcom/google/android/location/os/bi;

    if-nez v0, :cond_3

    .line 127
    iget-object v0, p0, Lcom/google/android/location/activity/ae;->a:Lcom/google/android/location/activity/ad;

    iget-object v0, v0, Lcom/google/android/location/activity/ad;->b:Lcom/google/android/location/activity/bo;

    if-eqz v0, :cond_1

    .line 128
    iget-object v0, p0, Lcom/google/android/location/activity/ae;->a:Lcom/google/android/location/activity/ad;

    iget-object v0, v0, Lcom/google/android/location/activity/ad;->b:Lcom/google/android/location/activity/bo;

    invoke-interface {v0}, Lcom/google/android/location/activity/bo;->L()V

    goto :goto_0

    .line 131
    :cond_3
    iget-object v0, p0, Lcom/google/android/location/activity/ae;->a:Lcom/google/android/location/activity/ad;

    iget-object v0, v0, Lcom/google/android/location/activity/ad;->a:Lcom/google/android/location/os/bi;

    invoke-interface {v0}, Lcom/google/android/location/os/bi;->v()Lcom/google/android/location/os/j;

    move-result-object v0

    sget-object v1, Lcom/google/android/location/os/au;->Z:Lcom/google/android/location/os/au;

    invoke-virtual {v0, v1}, Lcom/google/android/location/os/j;->a(Lcom/google/android/location/os/au;)V

    .line 133
    iget-object v0, p0, Lcom/google/android/location/activity/ae;->a:Lcom/google/android/location/activity/ad;

    iget-object v0, v0, Lcom/google/android/location/activity/ad;->a:Lcom/google/android/location/os/bi;

    invoke-interface {v0}, Lcom/google/android/location/os/bi;->f()Lcom/google/android/location/j/j;

    move-result-object v0

    new-instance v1, Lcom/google/android/location/activity/af;

    invoke-direct {v1, p0}, Lcom/google/android/location/activity/af;-><init>(Lcom/google/android/location/activity/ae;)V

    invoke-interface {v0, v1}, Lcom/google/android/location/j/j;->a(Ljava/lang/Runnable;)Z

    move-result v0

    .line 141
    if-nez v0, :cond_1

    .line 142
    sget-boolean v0, Lcom/google/android/location/i/a;->d:Z

    if-eqz v0, :cond_1

    const-string v0, "HwTilt"

    const-string v1, "onTilt not called because os is quitting or quit."

    invoke-static {v0, v1}, Lcom/google/android/location/o/a/a;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
