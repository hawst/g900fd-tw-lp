.class public final Lcom/google/android/location/copresence/d/a/l;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public final a:Lcom/google/android/location/copresence/d/a/m;

.field public final b:Lcom/google/android/location/copresence/d/a/n;

.field public final c:I

.field public final d:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/location/copresence/d/e;Lcom/google/android/location/copresence/d/a/o;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/16 v3, 0x15

    const/4 v1, 0x2

    const/4 v2, 0x0

    .line 104
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 105
    if-nez p3, :cond_0

    .line 106
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "listener cannot be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 109
    :cond_0
    invoke-static {v3}, Lcom/google/android/gms/common/util/ao;->a(I)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-static {p1}, Lcom/google/android/location/copresence/d/a/l;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    move v0, v1

    :goto_0
    iput v0, p0, Lcom/google/android/location/copresence/d/a/l;->c:I

    .line 110
    iget v0, p0, Lcom/google/android/location/copresence/d/a/l;->c:I

    packed-switch v0, :pswitch_data_0

    .line 116
    iput-object v4, p0, Lcom/google/android/location/copresence/d/a/l;->a:Lcom/google/android/location/copresence/d/a/m;

    .line 120
    :goto_1
    invoke-static {v3}, Lcom/google/android/gms/common/util/ao;->a(I)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-static {p1}, Lcom/google/android/location/copresence/d/a/l;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_3

    :goto_2
    iput v1, p0, Lcom/google/android/location/copresence/d/a/l;->d:I

    .line 121
    iget v0, p0, Lcom/google/android/location/copresence/d/a/l;->d:I

    packed-switch v0, :pswitch_data_1

    .line 130
    iput-object v4, p0, Lcom/google/android/location/copresence/d/a/l;->b:Lcom/google/android/location/copresence/d/a/n;

    .line 133
    :goto_3
    return-void

    :cond_1
    move v0, v2

    .line 109
    goto :goto_0

    .line 112
    :pswitch_0
    new-instance v0, Lcom/google/android/location/copresence/d/a/a;

    invoke-direct {v0, p1, p2, p3}, Lcom/google/android/location/copresence/d/a/a;-><init>(Landroid/content/Context;Lcom/google/android/location/copresence/d/e;Lcom/google/android/location/copresence/d/a/o;)V

    iput-object v0, p0, Lcom/google/android/location/copresence/d/a/l;->a:Lcom/google/android/location/copresence/d/a/m;

    goto :goto_1

    .line 120
    :cond_2
    const/16 v0, 0x12

    invoke-static {v0}, Lcom/google/android/gms/common/util/ao;->a(I)Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-static {p1}, Lcom/google/android/location/copresence/d/a/l;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_3

    const/4 v1, 0x1

    goto :goto_2

    :cond_3
    move v1, v2

    goto :goto_2

    .line 123
    :pswitch_1
    new-instance v0, Lcom/google/android/location/copresence/d/a/h;

    invoke-direct {v0, p2, p3}, Lcom/google/android/location/copresence/d/a/h;-><init>(Lcom/google/android/location/copresence/d/e;Lcom/google/android/location/copresence/d/a/o;)V

    iput-object v0, p0, Lcom/google/android/location/copresence/d/a/l;->b:Lcom/google/android/location/copresence/d/a/n;

    goto :goto_3

    .line 126
    :pswitch_2
    new-instance v0, Lcom/google/android/location/copresence/d/a/e;

    invoke-direct {v0, p2, p3}, Lcom/google/android/location/copresence/d/a/e;-><init>(Lcom/google/android/location/copresence/d/e;Lcom/google/android/location/copresence/d/a/o;)V

    iput-object v0, p0, Lcom/google/android/location/copresence/d/a/l;->b:Lcom/google/android/location/copresence/d/a/n;

    goto :goto_3

    .line 110
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
    .end packed-switch

    .line 121
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method private static a([B)Lcom/google/android/gms/location/copresence/x;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 204
    if-nez p0, :cond_0

    .line 212
    :goto_0
    return-object v0

    .line 208
    :cond_0
    :try_start_0
    new-instance v1, Lcom/google/android/gms/location/copresence/x;

    invoke-static {p0}, Lcom/google/android/gms/location/copresence/x;->a([B)[B

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/google/android/gms/location/copresence/x;-><init>([B)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-object v0, v1

    goto :goto_0

    .line 212
    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method public static a([B[B)Lcom/google/android/gms/location/copresence/x;
    .locals 1

    .prologue
    .line 187
    if-nez p0, :cond_1

    const/4 v0, 0x0

    .line 188
    :goto_0
    if-nez v0, :cond_0

    .line 189
    invoke-static {p1}, Lcom/google/android/location/copresence/d/a/l;->a([B)Lcom/google/android/gms/location/copresence/x;

    move-result-object v0

    .line 191
    :cond_0
    return-object v0

    .line 187
    :cond_1
    new-instance v0, Lcom/google/android/gms/location/copresence/x;

    invoke-direct {v0, p0}, Lcom/google/android/gms/location/copresence/x;-><init>([B)V

    goto :goto_0
.end method

.method private static a(Landroid/content/Context;)Z
    .locals 2

    .prologue
    .line 62
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    const-string v1, "android.hardware.bluetooth_le"

    invoke-virtual {v0, v1}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method
