.class final Lcom/google/android/location/copresence/r/i;
.super Lcom/google/android/location/copresence/k/f;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/location/copresence/r/f;


# direct methods
.method varargs constructor <init>(Lcom/google/android/location/copresence/r/f;Ljava/lang/String;[Lcom/google/android/location/copresence/k/f;)V
    .locals 0

    .prologue
    .line 120
    iput-object p1, p0, Lcom/google/android/location/copresence/r/i;->a:Lcom/google/android/location/copresence/r/f;

    invoke-direct {p0, p2, p3}, Lcom/google/android/location/copresence/k/f;-><init>(Ljava/lang/String;[Lcom/google/android/location/copresence/k/f;)V

    return-void
.end method


# virtual methods
.method public final a()Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v5, -0x1

    const/4 v1, 0x0

    .line 129
    .line 131
    :try_start_0
    iget-object v2, p0, Lcom/google/android/location/copresence/r/i;->a:Lcom/google/android/location/copresence/r/f;

    invoke-static {v2}, Lcom/google/android/location/copresence/r/f;->a(Lcom/google/android/location/copresence/r/f;)Landroid/content/SharedPreferences;

    move-result-object v2

    const-string v3, "wifiTethering"

    const/4 v4, -0x1

    invoke-interface {v2, v3, v4}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v3

    .line 133
    if-eq v3, v5, :cond_1

    move v2, v0

    :goto_0
    invoke-static {v2}, Lcom/google/k/a/ah;->a(Z)V

    .line 134
    iget-object v2, p0, Lcom/google/android/location/copresence/r/i;->a:Lcom/google/android/location/copresence/r/f;

    const-string v4, "tether_supported"

    invoke-static {v2, v4}, Lcom/google/android/location/copresence/r/f;->a(Lcom/google/android/location/copresence/r/f;Ljava/lang/String;)I

    move-result v2

    .line 135
    if-eq v3, v2, :cond_2

    .line 136
    iget-object v2, p0, Lcom/google/android/location/copresence/r/i;->a:Lcom/google/android/location/copresence/r/f;

    const-string v4, "tether_supported"

    invoke-static {v2, v4, v3}, Lcom/google/android/location/copresence/r/f;->b(Lcom/google/android/location/copresence/r/f;Ljava/lang/String;I)Z
    :try_end_0
    .catch Landroid/provider/Settings$SettingNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 142
    :goto_1
    if-eqz v0, :cond_0

    .line 143
    iget-object v0, p0, Lcom/google/android/location/copresence/r/i;->a:Lcom/google/android/location/copresence/r/f;

    iget-object v0, p0, Lcom/google/android/location/copresence/r/i;->a:Lcom/google/android/location/copresence/r/f;

    invoke-static {v0}, Lcom/google/android/location/copresence/r/f;->a(Lcom/google/android/location/copresence/r/f;)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "wifiTethering"

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "revert tethering setting"

    invoke-static {v0, v1}, Lcom/google/android/location/copresence/r/f;->a(Landroid/content/SharedPreferences$Editor;Ljava/lang/String;)Z

    move-result v1

    .line 146
    :cond_0
    return v1

    :cond_1
    move v2, v1

    .line 133
    goto :goto_0

    .line 140
    :catch_0
    move-exception v2

    goto :goto_1

    :cond_2
    move v0, v1

    goto :goto_1
.end method

.method public final b()Z
    .locals 2

    .prologue
    .line 124
    iget-object v0, p0, Lcom/google/android/location/copresence/r/i;->a:Lcom/google/android/location/copresence/r/f;

    invoke-static {v0}, Lcom/google/android/location/copresence/r/f;->a(Lcom/google/android/location/copresence/r/f;)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "wifiTethering"

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
