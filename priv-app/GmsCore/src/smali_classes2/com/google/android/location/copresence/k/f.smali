.class public abstract Lcom/google/android/location/copresence/k/f;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:[Lcom/google/android/location/copresence/k/f;

.field public final e:Ljava/lang/String;


# direct methods
.method public varargs constructor <init>(Ljava/lang/String;[Lcom/google/android/location/copresence/k/f;)V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    iput-object p1, p0, Lcom/google/android/location/copresence/k/f;->e:Ljava/lang/String;

    .line 20
    iput-object p2, p0, Lcom/google/android/location/copresence/k/f;->a:[Lcom/google/android/location/copresence/k/f;

    .line 21
    return-void
.end method


# virtual methods
.method public abstract a()Z
.end method

.method public abstract b()Z
.end method

.method public c()Lcom/google/android/location/copresence/k/f;
    .locals 5

    .prologue
    .line 37
    iget-object v2, p0, Lcom/google/android/location/copresence/k/f;->a:[Lcom/google/android/location/copresence/k/f;

    .line 38
    if-eqz v2, :cond_1

    .line 39
    array-length v3, v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v0, v2, v1

    .line 40
    invoke-virtual {v0}, Lcom/google/android/location/copresence/k/f;->b()Z

    move-result v4

    if-nez v4, :cond_0

    .line 45
    :goto_1
    return-object v0

    .line 39
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 45
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public d()J
    .locals 2

    .prologue
    .line 72
    invoke-static {}, Lcom/google/android/location/copresence/f/b;->b()Lcom/google/ac/b/c/g;

    move-result-object v0

    iget-object v0, v0, Lcom/google/ac/b/c/g;->e:Lcom/google/ac/b/c/m;

    iget-object v0, v0, Lcom/google/ac/b/c/m;->s:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    return-wide v0
.end method
