.class final Lcom/google/android/location/reporting/ble/f;
.super Lcom/google/android/gms/blescanner/b/b;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/location/reporting/ble/e;


# direct methods
.method constructor <init>(Lcom/google/android/location/reporting/ble/e;Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 85
    iput-object p1, p0, Lcom/google/android/location/reporting/ble/f;->a:Lcom/google/android/location/reporting/ble/e;

    invoke-direct {p0, p2}, Lcom/google/android/gms/blescanner/b/b;-><init>(Landroid/content/Context;)V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 97
    iget-object v0, p0, Lcom/google/android/location/reporting/ble/f;->a:Lcom/google/android/location/reporting/ble/e;

    iget-boolean v0, v0, Lcom/google/android/location/reporting/ble/e;->g:Z

    if-eqz v0, :cond_1

    .line 98
    const-string v0, "GCoreUlr"

    const/4 v1, 0x4

    invoke-static {v0, v1}, Lcom/google/android/location/reporting/b/d;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 99
    const-string v0, "GCoreUlr"

    const-string v1, "Caught bluetooth disabled event; disabling ULR BLE scanner"

    invoke-static {v0, v1}, Lcom/google/android/location/reporting/b/d;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 101
    :cond_0
    iget-object v0, p0, Lcom/google/android/location/reporting/ble/f;->a:Lcom/google/android/location/reporting/ble/e;

    invoke-virtual {v0}, Lcom/google/android/location/reporting/ble/e;->b()V

    .line 103
    :cond_1
    return-void
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 88
    iget-object v0, p0, Lcom/google/android/location/reporting/ble/f;->a:Lcom/google/android/location/reporting/ble/e;

    iget-boolean v0, v0, Lcom/google/android/location/reporting/ble/e;->g:Z

    if-eqz v0, :cond_1

    .line 89
    const-string v0, "GCoreUlr"

    const/4 v1, 0x4

    invoke-static {v0, v1}, Lcom/google/android/location/reporting/b/d;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 90
    const-string v0, "GCoreUlr"

    const-string v1, "Caught bluetooth enabled event; re-enabling ULR BLE scanner"

    invoke-static {v0, v1}, Lcom/google/android/location/reporting/b/d;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 92
    :cond_0
    iget-object v0, p0, Lcom/google/android/location/reporting/ble/f;->a:Lcom/google/android/location/reporting/ble/e;

    invoke-virtual {v0}, Lcom/google/android/location/reporting/ble/e;->a()V

    .line 94
    :cond_1
    return-void
.end method
