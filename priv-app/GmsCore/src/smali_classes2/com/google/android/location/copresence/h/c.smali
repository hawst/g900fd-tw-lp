.class public final Lcom/google/android/location/copresence/h/c;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/location/copresence/l/y;


# static fields
.field private static b:Lcom/google/android/location/copresence/h/c;


# instance fields
.field final a:Lcom/google/android/location/copresence/e/d;


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 99
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 100
    invoke-static {p1}, Lcom/google/android/location/copresence/e/d;->a(Landroid/content/Context;)Lcom/google/android/location/copresence/e/d;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/copresence/h/c;->a:Lcom/google/android/location/copresence/e/d;

    .line 102
    invoke-static {p1}, Lcom/google/android/location/copresence/l/m;->a(Landroid/content/Context;)Lcom/google/android/location/copresence/l/m;

    move-result-object v0

    iput-object p0, v0, Lcom/google/android/location/copresence/l/m;->b:Lcom/google/android/location/copresence/l/y;

    .line 103
    return-void
.end method

.method public static declared-synchronized a(Landroid/content/Context;)Lcom/google/android/location/copresence/h/c;
    .locals 2

    .prologue
    .line 91
    const-class v1, Lcom/google/android/location/copresence/h/c;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/google/android/location/copresence/h/c;->b:Lcom/google/android/location/copresence/h/c;

    if-nez v0, :cond_0

    .line 92
    new-instance v0, Lcom/google/android/location/copresence/h/c;

    invoke-direct {v0, p0}, Lcom/google/android/location/copresence/h/c;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/google/android/location/copresence/h/c;->b:Lcom/google/android/location/copresence/h/c;

    .line 94
    :cond_0
    sget-object v0, Lcom/google/android/location/copresence/h/c;->b:Lcom/google/android/location/copresence/h/c;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 91
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private static a(Lcom/google/android/location/copresence/h/d;)V
    .locals 4

    .prologue
    .line 115
    iget-object v0, p0, Lcom/google/android/location/copresence/h/d;->a:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/copresence/e/f;

    iget-object v3, v0, Lcom/google/android/location/copresence/e/f;->a:Lcom/google/android/location/copresence/e/g;

    iget-object v1, p0, Lcom/google/android/location/copresence/h/d;->a:Ljava/util/HashMap;

    invoke-virtual {v1, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/ArrayList;

    invoke-virtual {v3, v1}, Lcom/google/android/location/copresence/e/g;->a(Ljava/util/ArrayList;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/location/copresence/h/d;->b:Lcom/google/android/location/copresence/h/c;

    iget-object v1, v1, Lcom/google/android/location/copresence/h/c;->a:Lcom/google/android/location/copresence/e/d;

    invoke-virtual {v1, v0}, Lcom/google/android/location/copresence/e/d;->a(Lcom/google/android/location/copresence/e/f;)V

    goto :goto_0

    .line 116
    :cond_1
    return-void
.end method


# virtual methods
.method public final a([Lcom/google/ac/b/c/bm;)V
    .locals 8

    .prologue
    const/4 v0, 0x0

    .line 120
    if-nez p1, :cond_0

    .line 135
    :goto_0
    return-void

    .line 124
    :cond_0
    new-instance v2, Lcom/google/android/location/copresence/h/d;

    invoke-direct {v2, p0, v0}, Lcom/google/android/location/copresence/h/d;-><init>(Lcom/google/android/location/copresence/h/c;B)V

    .line 125
    array-length v3, p1

    move v1, v0

    :goto_1
    if-ge v1, v3, :cond_2

    aget-object v4, p1, v1

    .line 126
    const/4 v0, 0x0

    .line 127
    iget-object v5, v4, Lcom/google/ac/b/c/bm;->a:[Ljava/lang/String;

    if-eqz v5, :cond_1

    .line 128
    new-instance v0, Ljava/util/ArrayList;

    iget-object v5, v4, Lcom/google/ac/b/c/bm;->a:[Ljava/lang/String;

    invoke-static {v5}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v5

    invoke-direct {v0, v5}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 130
    :cond_1
    iget-object v4, v4, Lcom/google/ac/b/c/bm;->b:Lcom/google/ac/b/c/at;

    new-instance v5, Lcom/google/android/gms/location/copresence/Message;

    iget-object v6, v4, Lcom/google/ac/b/c/at;->a:Lcom/google/ac/b/c/av;

    iget-object v6, v6, Lcom/google/ac/b/c/av;->a:Ljava/lang/String;

    iget-object v7, v4, Lcom/google/ac/b/c/at;->a:Lcom/google/ac/b/c/av;

    iget-object v7, v7, Lcom/google/ac/b/c/av;->b:Ljava/lang/String;

    iget-object v4, v4, Lcom/google/ac/b/c/at;->b:[B

    invoke-direct {v5, v6, v7, v4}, Lcom/google/android/gms/location/copresence/Message;-><init>(Ljava/lang/String;Ljava/lang/String;[B)V

    new-instance v4, Lcom/google/android/gms/location/copresence/SubscribedMessage;

    invoke-direct {v4, v5}, Lcom/google/android/gms/location/copresence/SubscribedMessage;-><init>(Lcom/google/android/gms/location/copresence/Message;)V

    invoke-virtual {v2, v4, v0}, Lcom/google/android/location/copresence/h/d;->a(Lcom/google/android/gms/location/copresence/SubscribedMessage;Ljava/util/List;)V

    .line 125
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 134
    :cond_2
    invoke-static {v2}, Lcom/google/android/location/copresence/h/c;->a(Lcom/google/android/location/copresence/h/d;)V

    goto :goto_0
.end method
