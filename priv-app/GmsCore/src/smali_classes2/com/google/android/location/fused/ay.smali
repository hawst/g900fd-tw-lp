.class public final Lcom/google/android/location/fused/ay;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/location/fused/ap;
.implements Lcom/google/android/location/n/ad;


# instance fields
.field private final a:Lcom/google/android/location/fused/ap;

.field private final b:Landroid/content/Context;

.field private final c:Lcom/google/android/location/n/ab;

.field private final d:Ljava/util/ArrayList;

.field private e:Ljava/util/Collection;

.field private f:Z

.field private g:Z

.field private h:Z


# direct methods
.method public constructor <init>(Lcom/google/android/location/fused/ap;Landroid/content/Context;Landroid/os/Looper;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 49
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/location/fused/ay;->d:Ljava/util/ArrayList;

    .line 42
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/fused/ay;->e:Ljava/util/Collection;

    .line 44
    iput-boolean v1, p0, Lcom/google/android/location/fused/ay;->f:Z

    .line 45
    iput-boolean v1, p0, Lcom/google/android/location/fused/ay;->g:Z

    .line 46
    iput-boolean v1, p0, Lcom/google/android/location/fused/ay;->h:Z

    .line 50
    iput-object p1, p0, Lcom/google/android/location/fused/ay;->a:Lcom/google/android/location/fused/ap;

    .line 51
    iput-object p2, p0, Lcom/google/android/location/fused/ay;->b:Landroid/content/Context;

    .line 52
    new-instance v0, Lcom/google/android/location/n/ab;

    invoke-direct {v0, p2, p0, p3}, Lcom/google/android/location/n/ab;-><init>(Landroid/content/Context;Lcom/google/android/location/n/ad;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/google/android/location/fused/ay;->c:Lcom/google/android/location/n/ab;

    .line 53
    return-void
.end method

.method private c(Z)V
    .locals 3

    .prologue
    .line 149
    iget-boolean v0, p0, Lcom/google/android/location/fused/ay;->f:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/location/fused/ay;->d:Ljava/util/ArrayList;

    .line 151
    :goto_0
    iget-object v2, p0, Lcom/google/android/location/fused/ay;->a:Lcom/google/android/location/fused/ap;

    iget-boolean v1, p0, Lcom/google/android/location/fused/ay;->f:Z

    if-nez v1, :cond_3

    if-eqz p1, :cond_3

    const/4 v1, 0x1

    :goto_1
    invoke-interface {v2, v0, v1}, Lcom/google/android/location/fused/ap;->a(Ljava/util/Collection;Z)V

    .line 152
    iget-boolean v0, p0, Lcom/google/android/location/fused/ay;->f:Z

    if-eqz v0, :cond_4

    .line 153
    iget-boolean v0, p0, Lcom/google/android/location/fused/ay;->h:Z

    if-eqz v0, :cond_1

    .line 160
    :cond_0
    iget-object v0, p0, Lcom/google/android/location/fused/ay;->a:Lcom/google/android/location/fused/ap;

    invoke-interface {v0}, Lcom/google/android/location/fused/ap;->d()V

    .line 163
    :cond_1
    :goto_2
    return-void

    .line 149
    :cond_2
    iget-object v0, p0, Lcom/google/android/location/fused/ay;->e:Ljava/util/Collection;

    goto :goto_0

    .line 151
    :cond_3
    const/4 v1, 0x0

    goto :goto_1

    .line 157
    :cond_4
    iget-boolean v0, p0, Lcom/google/android/location/fused/ay;->h:Z

    if-eqz v0, :cond_0

    .line 158
    iget-object v0, p0, Lcom/google/android/location/fused/ay;->a:Lcom/google/android/location/fused/ap;

    invoke-interface {v0}, Lcom/google/android/location/fused/ap;->c()V

    goto :goto_2
.end method


# virtual methods
.method public final a(Z)Landroid/location/Location;
    .locals 1

    .prologue
    .line 120
    iget-object v0, p0, Lcom/google/android/location/fused/ay;->a:Lcom/google/android/location/fused/ap;

    invoke-interface {v0, p1}, Lcom/google/android/location/fused/ap;->a(Z)Landroid/location/Location;

    move-result-object v0

    return-object v0
.end method

.method public final a()V
    .locals 1

    .prologue
    .line 57
    iget-boolean v0, p0, Lcom/google/android/location/fused/ay;->g:Z

    if-nez v0, :cond_0

    .line 58
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/location/fused/ay;->g:Z

    .line 59
    iget-object v0, p0, Lcom/google/android/location/fused/ay;->b:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/gms/common/a/d;->a(Landroid/content/Context;)V

    .line 61
    iget-object v0, p0, Lcom/google/android/location/fused/ay;->c:Lcom/google/android/location/n/ab;

    invoke-virtual {v0}, Lcom/google/android/location/n/ab;->a()V

    .line 62
    iget-object v0, p0, Lcom/google/android/location/fused/ay;->a:Lcom/google/android/location/fused/ap;

    invoke-interface {v0}, Lcom/google/android/location/fused/ap;->a()V

    .line 64
    :cond_0
    return-void
.end method

.method public final a(Landroid/location/Location;I)V
    .locals 1

    .prologue
    .line 115
    iget-object v0, p0, Lcom/google/android/location/fused/ay;->a:Lcom/google/android/location/fused/ap;

    invoke-interface {v0, p1, p2}, Lcom/google/android/location/fused/ap;->a(Landroid/location/Location;I)V

    .line 116
    return-void
.end method

.method public final a(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 125
    iget-object v0, p0, Lcom/google/android/location/fused/ay;->a:Lcom/google/android/location/fused/ap;

    invoke-interface {v0, p1, p2, p3}, Lcom/google/android/location/fused/ap;->a(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V

    .line 126
    return-void
.end method

.method public final a(Ljava/util/Collection;Z)V
    .locals 8

    .prologue
    .line 77
    iput-object p1, p0, Lcom/google/android/location/fused/ay;->e:Ljava/util/Collection;

    .line 78
    sget-object v0, Lcom/google/android/location/x;->J:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    .line 79
    sget-object v0, Lcom/google/android/location/x;->K:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v4

    .line 80
    iget-object v0, p0, Lcom/google/android/location/fused/ay;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 81
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/location/internal/LocationRequestInternal;

    .line 82
    invoke-virtual {v0}, Lcom/google/android/gms/location/internal/LocationRequestInternal;->a()Lcom/google/android/gms/location/LocationRequest;

    move-result-object v1

    .line 83
    invoke-virtual {v1}, Lcom/google/android/gms/location/LocationRequest;->c()J

    move-result-wide v6

    cmp-long v6, v6, v2

    if-gez v6, :cond_0

    invoke-virtual {v1}, Lcom/google/android/gms/location/LocationRequest;->b()I

    move-result v6

    if-lt v6, v4, :cond_0

    .line 84
    new-instance v6, Lcom/google/android/gms/location/LocationRequest;

    invoke-direct {v6, v1}, Lcom/google/android/gms/location/LocationRequest;-><init>(Lcom/google/android/gms/location/LocationRequest;)V

    .line 85
    new-instance v1, Lcom/google/android/gms/location/internal/LocationRequestInternal;

    invoke-direct {v1, v0}, Lcom/google/android/gms/location/internal/LocationRequestInternal;-><init>(Lcom/google/android/gms/location/internal/LocationRequestInternal;)V

    .line 86
    invoke-virtual {v1, v6}, Lcom/google/android/gms/location/internal/LocationRequestInternal;->b(Lcom/google/android/gms/location/LocationRequest;)Lcom/google/android/gms/location/internal/LocationRequestInternal;

    .line 87
    invoke-virtual {v6, v2, v3}, Lcom/google/android/gms/location/LocationRequest;->a(J)Lcom/google/android/gms/location/LocationRequest;

    move-object v0, v1

    .line 89
    :cond_0
    iget-object v1, p0, Lcom/google/android/location/fused/ay;->d:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 92
    :cond_1
    invoke-direct {p0, p2}, Lcom/google/android/location/fused/ay;->c(Z)V

    .line 93
    return-void
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 68
    iget-boolean v0, p0, Lcom/google/android/location/fused/ay;->g:Z

    if-eqz v0, :cond_0

    .line 69
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/location/fused/ay;->g:Z

    .line 70
    iget-object v0, p0, Lcom/google/android/location/fused/ay;->c:Lcom/google/android/location/n/ab;

    invoke-virtual {v0}, Lcom/google/android/location/n/ab;->b()V

    .line 71
    iget-object v0, p0, Lcom/google/android/location/fused/ay;->a:Lcom/google/android/location/fused/ap;

    invoke-interface {v0}, Lcom/google/android/location/fused/ap;->b()V

    .line 73
    :cond_0
    return-void
.end method

.method public final b(Z)V
    .locals 1

    .prologue
    .line 138
    iget-boolean v0, p0, Lcom/google/android/location/fused/ay;->f:Z

    if-eq p1, v0, :cond_0

    .line 139
    iput-boolean p1, p0, Lcom/google/android/location/fused/ay;->f:Z

    .line 140
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/location/fused/ay;->c(Z)V

    .line 143
    :cond_0
    return-void
.end method

.method public final c()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 97
    iget-boolean v0, p0, Lcom/google/android/location/fused/ay;->h:Z

    if-nez v0, :cond_0

    .line 98
    iput-boolean v1, p0, Lcom/google/android/location/fused/ay;->h:Z

    .line 99
    invoke-direct {p0, v1}, Lcom/google/android/location/fused/ay;->c(Z)V

    .line 102
    :cond_0
    return-void
.end method

.method public final d()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 106
    iget-boolean v0, p0, Lcom/google/android/location/fused/ay;->h:Z

    if-eqz v0, :cond_0

    .line 107
    iput-boolean v1, p0, Lcom/google/android/location/fused/ay;->h:Z

    .line 108
    invoke-direct {p0, v1}, Lcom/google/android/location/fused/ay;->c(Z)V

    .line 111
    :cond_0
    return-void
.end method

.method public final e()Lcom/google/android/location/n/k;
    .locals 1

    .prologue
    .line 133
    iget-object v0, p0, Lcom/google/android/location/fused/ay;->a:Lcom/google/android/location/fused/ap;

    invoke-interface {v0}, Lcom/google/android/location/fused/ap;->e()Lcom/google/android/location/n/k;

    move-result-object v0

    return-object v0
.end method
