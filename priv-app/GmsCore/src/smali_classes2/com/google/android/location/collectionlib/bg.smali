.class Lcom/google/android/location/collectionlib/bg;
.super Lcom/google/android/location/collectionlib/cc;
.source "SourceFile"


# instance fields
.field private volatile e:Z

.field private final f:Ljava/util/concurrent/ThreadPoolExecutor;

.field private final g:Landroid/os/PowerManager;

.field private volatile h:Ljava/lang/String;

.field private final i:[B

.field private volatile j:Z

.field private volatile k:Ljava/lang/String;

.field private l:Ljava/lang/Object;

.field private volatile m:Lcom/google/android/location/collectionlib/c;

.field private final n:Lcom/google/android/location/o/n;


# direct methods
.method constructor <init>(Landroid/os/PowerManager;Ljava/lang/String;[BLcom/google/android/location/collectionlib/ar;Lcom/google/android/location/o/a/c;Lcom/google/android/location/collectionlib/cp;Lcom/google/android/location/o/n;)V
    .locals 6

    .prologue
    const/16 v5, 0x2f

    const/4 v4, 0x0

    .line 91
    invoke-direct {p0, p4, p5, p6}, Lcom/google/android/location/collectionlib/cc;-><init>(Lcom/google/android/location/collectionlib/ar;Lcom/google/android/location/o/a/c;Lcom/google/android/location/collectionlib/cp;)V

    .line 46
    iput-boolean v4, p0, Lcom/google/android/location/collectionlib/bg;->e:Z

    .line 49
    new-instance v0, Lcom/google/android/location/collectionlib/bh;

    sget-object v1, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    new-instance v2, Ljava/util/concurrent/ArrayBlockingQueue;

    const/16 v3, 0x32

    invoke-direct {v2, v3}, Ljava/util/concurrent/ArrayBlockingQueue;-><init>(I)V

    new-instance v3, Ljava/util/concurrent/ThreadPoolExecutor$AbortPolicy;

    invoke-direct {v3}, Ljava/util/concurrent/ThreadPoolExecutor$AbortPolicy;-><init>()V

    invoke-direct {v0, p0, v1, v2, v3}, Lcom/google/android/location/collectionlib/bh;-><init>(Lcom/google/android/location/collectionlib/bg;Ljava/util/concurrent/TimeUnit;Ljava/util/concurrent/BlockingQueue;Ljava/util/concurrent/RejectedExecutionHandler;)V

    iput-object v0, p0, Lcom/google/android/location/collectionlib/bg;->f:Ljava/util/concurrent/ThreadPoolExecutor;

    .line 82
    iput-boolean v4, p0, Lcom/google/android/location/collectionlib/bg;->j:Z

    .line 83
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/location/collectionlib/bg;->k:Ljava/lang/String;

    .line 84
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/location/collectionlib/bg;->l:Ljava/lang/Object;

    .line 92
    iput-object p1, p0, Lcom/google/android/location/collectionlib/bg;->g:Landroid/os/PowerManager;

    .line 93
    iput-object p2, p0, Lcom/google/android/location/collectionlib/bg;->h:Ljava/lang/String;

    .line 94
    iget-object v0, p0, Lcom/google/android/location/collectionlib/bg;->h:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/location/collectionlib/bg;->h:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v0

    if-eq v0, v5, :cond_0

    .line 95
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/google/android/location/collectionlib/bg;->h:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/collectionlib/bg;->h:Ljava/lang/String;

    .line 97
    :cond_0
    iput-object p3, p0, Lcom/google/android/location/collectionlib/bg;->i:[B

    .line 98
    iput-object p7, p0, Lcom/google/android/location/collectionlib/bg;->n:Lcom/google/android/location/o/n;

    .line 99
    return-void
.end method

.method static synthetic a(Lcom/google/android/location/collectionlib/bg;Lcom/google/p/a/b/b/a;)Lcom/google/android/location/collectionlib/cd;
    .locals 1

    .prologue
    .line 39
    invoke-direct {p0, p1}, Lcom/google/android/location/collectionlib/bg;->c(Lcom/google/p/a/b/b/a;)Lcom/google/android/location/collectionlib/cd;

    move-result-object v0

    return-object v0
.end method

.method private a(Ljava/lang/String;[B)Lcom/google/android/location/collectionlib/cd;
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 280
    invoke-direct {p0}, Lcom/google/android/location/collectionlib/bg;->b()Lcom/google/android/location/collectionlib/cd;

    .line 283
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/google/android/location/collectionlib/bg;->h:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 284
    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Lcom/google/android/location/collectionlib/bg;->h:Ljava/lang/String;

    invoke-direct {v0, v1, p1}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 287
    :try_start_0
    new-instance v1, Ljava/io/FileOutputStream;

    invoke-direct {v1, v0}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 288
    :try_start_1
    invoke-virtual {v1, p2}, Ljava/io/FileOutputStream;->write([B)V

    .line 289
    new-instance v0, Lcom/google/android/location/collectionlib/cd;

    const/4 v2, 0x1

    const/4 v4, 0x0

    invoke-direct {v0, v2, v3, v4}, Lcom/google/android/location/collectionlib/cd;-><init>(ZLjava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_4
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 294
    :try_start_2
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2

    .line 298
    :cond_0
    :goto_0
    return-object v0

    .line 290
    :catch_0
    move-exception v0

    move-object v1, v2

    .line 291
    :goto_1
    :try_start_3
    const-string v2, "Failed to save data: %s"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v5

    invoke-static {v2, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 292
    new-instance v0, Lcom/google/android/location/collectionlib/cd;

    const/4 v4, 0x0

    invoke-direct {v0, v4, v3, v2}, Lcom/google/android/location/collectionlib/cd;-><init>(ZLjava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 294
    if-eqz v1, :cond_0

    .line 296
    :try_start_4
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1

    goto :goto_0

    :catch_1
    move-exception v1

    goto :goto_0

    .line 294
    :catchall_0
    move-exception v0

    move-object v1, v2

    :goto_2
    if-eqz v1, :cond_1

    .line 296
    :try_start_5
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3

    .line 298
    :cond_1
    :goto_3
    throw v0

    :catch_2
    move-exception v1

    goto :goto_0

    :catch_3
    move-exception v1

    goto :goto_3

    .line 294
    :catchall_1
    move-exception v0

    goto :goto_2

    .line 290
    :catch_4
    move-exception v0

    goto :goto_1
.end method

.method static synthetic a(Lcom/google/android/location/collectionlib/bg;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lcom/google/android/location/collectionlib/bg;->h:Ljava/lang/String;

    return-object v0
.end method

.method private b()Lcom/google/android/location/collectionlib/cd;
    .locals 10

    .prologue
    const/4 v0, 0x0

    .line 308
    iget-object v2, p0, Lcom/google/android/location/collectionlib/bg;->l:Ljava/lang/Object;

    monitor-enter v2

    .line 309
    :try_start_0
    iget-boolean v1, p0, Lcom/google/android/location/collectionlib/bg;->j:Z

    if-eqz v1, :cond_0

    .line 310
    monitor-exit v2

    .line 327
    :goto_0
    return-object v0

    .line 313
    :cond_0
    iget-object v1, p0, Lcom/google/android/location/collectionlib/bg;->h:Ljava/lang/String;

    invoke-direct {p0, v1}, Lcom/google/android/location/collectionlib/bg;->b(Ljava/lang/String;)Lcom/google/android/location/collectionlib/cd;

    move-result-object v1

    .line 314
    if-eqz v1, :cond_1

    .line 315
    monitor-exit v2

    move-object v0, v1

    goto :goto_0

    .line 319
    :cond_1
    const-string v1, "%d-%d"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    invoke-static {}, Ljava/lang/Math;->random()D

    move-result-wide v6

    const-wide v8, 0x412e848000000000L    # 1000000.0

    mul-double/2addr v6, v8

    double-to-int v5, v6

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v1, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 321
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lcom/google/android/location/collectionlib/bg;->h:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/location/collectionlib/bg;->h:Ljava/lang/String;

    .line 322
    iget-object v1, p0, Lcom/google/android/location/collectionlib/bg;->h:Ljava/lang/String;

    invoke-direct {p0, v1}, Lcom/google/android/location/collectionlib/bg;->b(Ljava/lang/String;)Lcom/google/android/location/collectionlib/cd;

    move-result-object v1

    .line 323
    if-eqz v1, :cond_2

    .line 324
    monitor-exit v2

    move-object v0, v1

    goto :goto_0

    .line 326
    :cond_2
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/location/collectionlib/bg;->j:Z

    .line 327
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 329
    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0
.end method

.method private b(Ljava/lang/String;)Lcom/google/android/location/collectionlib/cd;
    .locals 7

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x1

    const/4 v6, 0x0

    .line 135
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 137
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v3

    if-nez v3, :cond_3

    .line 138
    sget-boolean v3, Lcom/google/android/location/i/a;->b:Z

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/google/android/location/collectionlib/bg;->b:Lcom/google/android/location/o/a/c;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "mkdir: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/google/android/location/o/a/c;->a(Ljava/lang/String;)V

    .line 139
    :cond_0
    invoke-virtual {v0}, Ljava/io/File;->mkdir()Z

    move-result v0

    .line 141
    :goto_0
    if-nez v0, :cond_2

    .line 142
    const-string v0, "Failed to create dir: %s"

    new-array v2, v2, [Ljava/lang/Object;

    aput-object p1, v2, v6

    invoke-static {v0, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 143
    sget-boolean v0, Lcom/google/android/location/i/a;->b:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/location/collectionlib/bg;->b:Lcom/google/android/location/o/a/c;

    invoke-virtual {v0, v2}, Lcom/google/android/location/o/a/c;->a(Ljava/lang/String;)V

    .line 144
    :cond_1
    new-instance v0, Lcom/google/android/location/collectionlib/cd;

    invoke-direct {v0, v6, v1, v2}, Lcom/google/android/location/collectionlib/cd;-><init>(ZLjava/lang/String;Ljava/lang/String;)V

    .line 146
    :goto_1
    return-object v0

    :cond_2
    move-object v0, v1

    goto :goto_1

    :cond_3
    move v0, v2

    goto :goto_0
.end method

.method static synthetic b(Lcom/google/android/location/collectionlib/bg;)Z
    .locals 1

    .prologue
    .line 39
    iget-boolean v0, p0, Lcom/google/android/location/collectionlib/bg;->j:Z

    return v0
.end method

.method private b(Lcom/google/p/a/b/b/a;)Z
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v1, 0x1

    const/4 v2, 0x0

    const/4 v4, 0x6

    .line 174
    invoke-virtual {p1, v4}, Lcom/google/p/a/b/b/a;->i(I)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1, v4}, Lcom/google/p/a/b/b/a;->f(I)Lcom/google/p/a/b/b/a;

    move-result-object v0

    invoke-virtual {v0, v5}, Lcom/google/p/a/b/b/a;->i(I)Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    const-string v3, "No sequence number specified!"

    invoke-static {v0, v3}, Lcom/google/android/location/collectionlib/df;->b(ZLjava/lang/Object;)V

    .line 178
    invoke-virtual {p1, v4}, Lcom/google/p/a/b/b/a;->f(I)Lcom/google/p/a/b/b/a;

    move-result-object v0

    invoke-virtual {v0, v5}, Lcom/google/p/a/b/b/a;->c(I)I

    move-result v0

    .line 181
    :try_start_0
    iget-object v3, p0, Lcom/google/android/location/collectionlib/bg;->f:Ljava/util/concurrent/ThreadPoolExecutor;

    new-instance v4, Lcom/google/android/location/collectionlib/bi;

    invoke-direct {v4, p0, v0, p1}, Lcom/google/android/location/collectionlib/bi;-><init>(Lcom/google/android/location/collectionlib/bg;ILcom/google/p/a/b/b/a;)V

    invoke-virtual {v3, v4}, Ljava/util/concurrent/ThreadPoolExecutor;->execute(Ljava/lang/Runnable;)V
    :try_end_0
    .catch Ljava/util/concurrent/RejectedExecutionException; {:try_start_0 .. :try_end_0} :catch_0

    .line 216
    :goto_1
    return v1

    :cond_0
    move v0, v2

    .line 174
    goto :goto_0

    .line 213
    :catch_0
    move-exception v1

    const-string v1, "Failed to write to file: work queue full."

    .line 214
    sget-boolean v3, Lcom/google/android/location/i/a;->b:Z

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/google/android/location/collectionlib/bg;->b:Lcom/google/android/location/o/a/c;

    invoke-virtual {v3, v1}, Lcom/google/android/location/o/a/c;->a(Ljava/lang/String;)V

    .line 215
    :cond_1
    iget-object v3, p0, Lcom/google/android/location/collectionlib/bg;->a:Lcom/google/android/location/collectionlib/ar;

    const/4 v4, 0x0

    invoke-interface {v3, v0, v4, v1}, Lcom/google/android/location/collectionlib/ar;->a(ILjava/lang/String;Ljava/lang/String;)V

    move v1, v2

    .line 216
    goto :goto_1
.end method

.method static synthetic c(Lcom/google/android/location/collectionlib/bg;)Landroid/os/PowerManager;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lcom/google/android/location/collectionlib/bg;->g:Landroid/os/PowerManager;

    return-object v0
.end method

.method private c(Lcom/google/p/a/b/b/a;)Lcom/google/android/location/collectionlib/cd;
    .locals 5

    .prologue
    .line 258
    invoke-direct {p0}, Lcom/google/android/location/collectionlib/bg;->b()Lcom/google/android/location/collectionlib/cd;

    .line 259
    monitor-enter p0

    .line 260
    :try_start_0
    iget-object v0, p0, Lcom/google/android/location/collectionlib/bg;->m:Lcom/google/android/location/collectionlib/c;

    if-nez v0, :cond_1

    .line 261
    iget-object v0, p0, Lcom/google/android/location/collectionlib/bg;->i:[B

    if-nez v0, :cond_0

    .line 262
    new-instance v0, Lcom/google/android/location/collectionlib/cd;

    const/4 v1, 0x0

    const/4 v2, 0x0

    const-string v3, "Encryption Key invalid."

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/location/collectionlib/cd;-><init>(ZLjava/lang/String;Ljava/lang/String;)V

    monitor-exit p0

    .line 272
    :goto_0
    return-object v0

    .line 267
    :cond_0
    new-instance v0, Lcom/google/android/location/collectionlib/c;

    iget-object v1, p0, Lcom/google/android/location/collectionlib/bg;->h:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/location/collectionlib/bg;->i:[B

    iget-object v3, p0, Lcom/google/android/location/collectionlib/bg;->b:Lcom/google/android/location/o/a/c;

    invoke-static {v2, v3}, Lcom/google/android/location/e/a;->a([BLcom/google/android/location/o/a/c;)Lcom/google/android/location/e/a;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/location/collectionlib/bg;->b:Lcom/google/android/location/o/a/c;

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/location/collectionlib/c;-><init>(Ljava/lang/String;Lcom/google/android/location/e/a;Lcom/google/android/location/o/a/c;)V

    iput-object v0, p0, Lcom/google/android/location/collectionlib/bg;->m:Lcom/google/android/location/collectionlib/c;

    .line 270
    :cond_1
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 272
    iget-object v0, p0, Lcom/google/android/location/collectionlib/bg;->m:Lcom/google/android/location/collectionlib/c;

    invoke-virtual {v0, p1}, Lcom/google/android/location/collectionlib/c;->a(Lcom/google/p/a/b/b/a;)Lcom/google/android/location/collectionlib/cd;

    move-result-object v0

    goto :goto_0

    .line 270
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method static synthetic d(Lcom/google/android/location/collectionlib/bg;)Lcom/google/android/location/o/n;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lcom/google/android/location/collectionlib/bg;->n:Lcom/google/android/location/o/n;

    return-object v0
.end method

.method static synthetic e(Lcom/google/android/location/collectionlib/bg;)Z
    .locals 6

    .prologue
    .line 39
    iget-boolean v0, p0, Lcom/google/android/location/collectionlib/bg;->e:Z

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-direct {p0}, Lcom/google/android/location/collectionlib/bg;->b()Lcom/google/android/location/collectionlib/cd;

    move-result-object v0

    if-eqz v0, :cond_2

    const/4 v0, 0x0

    goto :goto_0

    :cond_2
    invoke-static {}, Lcom/google/android/location/collectionlib/cb;->a()Lcom/google/android/location/collectionlib/cb;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/location/collectionlib/bg;->h:Ljava/lang/String;

    sget-object v2, Ljava/util/concurrent/TimeUnit;->DAYS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v4, 0x18

    invoke-virtual {v2, v4, v5}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/location/collectionlib/cb;->a(Ljava/lang/String;J)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/location/collectionlib/bg;->e:Z

    if-nez v0, :cond_0

    sget-boolean v1, Lcom/google/android/location/i/a;->b:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/location/collectionlib/bg;->b:Lcom/google/android/location/o/a/c;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Failed to lock dir "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/google/android/location/collectionlib/bg;->h:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/location/o/a/c;->a(Ljava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method public final declared-synchronized a(Ljava/lang/String;)Lcom/google/android/location/collectionlib/cd;
    .locals 4

    .prologue
    const/4 v0, 0x1

    .line 108
    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lcom/google/android/location/collectionlib/bg;->k:Ljava/lang/String;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/location/collectionlib/bg;->k:Ljava/lang/String;

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    :cond_0
    :goto_0
    const-string v1, "sessionId in two writes should be consistent."

    invoke-static {v0, v1}, Lcom/google/android/location/collectionlib/df;->a(ZLjava/lang/Object;)V

    .line 110
    iget-object v0, p0, Lcom/google/android/location/collectionlib/bg;->k:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 111
    new-instance v0, Lcom/google/android/location/collectionlib/cd;

    const/4 v1, 0x1

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/location/collectionlib/cd;-><init>(ZLjava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 118
    :cond_1
    :goto_1
    monitor-exit p0

    return-object v0

    .line 108
    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    .line 113
    :cond_3
    :try_start_1
    const-string v0, "sessionId"

    invoke-virtual {p1}, Ljava/lang/String;->getBytes()[B

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/google/android/location/collectionlib/bg;->a(Ljava/lang/String;[B)Lcom/google/android/location/collectionlib/cd;

    move-result-object v0

    .line 114
    iget-boolean v1, v0, Lcom/google/android/location/collectionlib/cd;->a:Z

    if-eqz v1, :cond_1

    .line 115
    iput-object p1, p0, Lcom/google/android/location/collectionlib/bg;->k:Ljava/lang/String;

    .line 116
    new-instance v0, Lcom/google/android/location/collectionlib/cd;

    const/4 v1, 0x1

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/location/collectionlib/cd;-><init>(ZLjava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 108
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected final a()V
    .locals 2

    .prologue
    .line 225
    iget-object v0, p0, Lcom/google/android/location/collectionlib/bg;->f:Ljava/util/concurrent/ThreadPoolExecutor;

    invoke-virtual {v0}, Ljava/util/concurrent/ThreadPoolExecutor;->shutdown()V

    .line 226
    sget-boolean v0, Lcom/google/android/location/i/a;->b:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/location/collectionlib/bg;->b:Lcom/google/android/location/o/a/c;

    const-string v1, "LocalScanResultWriter.workerThread is shutting down."

    invoke-virtual {v0, v1}, Lcom/google/android/location/o/a/c;->a(Ljava/lang/String;)V

    .line 227
    :cond_0
    return-void
.end method

.method final a(Lcom/google/android/location/collectionlib/cp;)V
    .locals 4

    .prologue
    .line 348
    if-nez p1, :cond_1

    .line 356
    :cond_0
    :goto_0
    return-void

    .line 352
    :cond_1
    :try_start_0
    iget-object v0, p0, Lcom/google/android/location/collectionlib/bg;->h:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/google/android/location/collectionlib/cp;->b(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 353
    :catch_0
    move-exception v0

    .line 354
    sget-boolean v1, Lcom/google/android/location/i/a;->b:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/location/collectionlib/bg;->b:Lcom/google/android/location/o/a/c;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Failed to create sessionSummary "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/FileNotFoundException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/google/android/location/o/a/c;->a(Ljava/lang/String;)V

    goto :goto_0
.end method

.method final declared-synchronized a(Lcom/google/p/a/b/b/a;)Z
    .locals 2

    .prologue
    .line 235
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/location/collectionlib/bg;->c:Z

    if-eqz v0, :cond_1

    .line 236
    sget-boolean v0, Lcom/google/android/location/i/a;->b:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/location/collectionlib/bg;->b:Lcom/google/android/location/o/a/c;

    const-string v1, "Writer closed, no data can be appended."

    invoke-virtual {v0, v1}, Lcom/google/android/location/o/a/c;->a(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 237
    :cond_0
    const/4 v0, 0x0

    .line 239
    :goto_0
    monitor-exit p0

    return v0

    :cond_1
    :try_start_1
    invoke-direct {p0, p1}, Lcom/google/android/location/collectionlib/bg;->b(Lcom/google/p/a/b/b/a;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v0

    goto :goto_0

    .line 235
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected final a(Lcom/google/p/a/b/b/a;Lcom/google/p/a/b/b/a;)Z
    .locals 1

    .prologue
    .line 249
    const/4 v0, 0x6

    invoke-virtual {p1, v0, p2}, Lcom/google/p/a/b/b/a;->b(ILcom/google/p/a/b/b/a;)Lcom/google/p/a/b/b/a;

    .line 250
    invoke-direct {p0, p1}, Lcom/google/android/location/collectionlib/bg;->b(Lcom/google/p/a/b/b/a;)Z

    move-result v0

    return v0
.end method

.method protected finalize()V
    .locals 2

    .prologue
    .line 335
    :try_start_0
    iget-object v0, p0, Lcom/google/android/location/collectionlib/bg;->f:Ljava/util/concurrent/ThreadPoolExecutor;

    invoke-virtual {v0}, Ljava/util/concurrent/ThreadPoolExecutor;->isShutdown()Z

    move-result v0

    if-nez v0, :cond_0

    .line 336
    sget-boolean v0, Lcom/google/android/location/i/a;->b:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/location/collectionlib/bg;->b:Lcom/google/android/location/o/a/c;

    const-string v1, "Might leak LocalScanResultWriter.workerThread."

    invoke-virtual {v0, v1}, Lcom/google/android/location/o/a/c;->a(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 339
    :cond_0
    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    .line 340
    return-void

    .line 339
    :catchall_0
    move-exception v0

    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    throw v0
.end method
