.class public final Lcom/google/android/location/places/as;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public final a:Landroid/content/Context;

.field public final b:Lcom/google/android/location/places/t;

.field public final c:Lcom/google/android/location/places/bi;

.field public final d:Lcom/google/android/location/places/f;

.field final e:Lcom/google/android/location/places/y;

.field public final f:Lcom/google/android/location/places/c/j;

.field public final g:Lcom/google/android/location/places/d/e;

.field public final h:Lcom/google/android/location/places/an;

.field public final i:Lcom/google/android/location/places/r;

.field public final j:Lcom/google/android/location/places/b/a;

.field public final k:Lcom/google/android/location/places/c/d;

.field public final l:Ljava/util/List;

.field m:Landroid/content/BroadcastReceiver;

.field private final n:Landroid/os/HandlerThread;

.field private final o:Ljava/util/concurrent/Executor;

.field private final p:Lcom/google/android/location/places/ay;

.field private final q:Lcom/google/android/location/n/ai;

.field private final r:Lcom/google/android/location/places/d/j;

.field private final s:Lcom/google/android/location/fused/g;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/location/fused/g;Lcom/google/android/location/geofencer/service/g;)V
    .locals 14

    .prologue
    .line 127
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 98
    new-instance v1, Landroid/os/HandlerThread;

    const-string v2, "Places"

    invoke-direct {v1, v2}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    iput-object v1, p0, Lcom/google/android/location/places/as;->n:Landroid/os/HandlerThread;

    .line 728
    new-instance v1, Lcom/google/android/location/places/at;

    invoke-direct {v1, p0}, Lcom/google/android/location/places/at;-><init>(Lcom/google/android/location/places/as;)V

    iput-object v1, p0, Lcom/google/android/location/places/as;->m:Landroid/content/BroadcastReceiver;

    .line 128
    iput-object p1, p0, Lcom/google/android/location/places/as;->a:Landroid/content/Context;

    .line 129
    new-instance v1, Lcom/google/android/location/places/aw;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lcom/google/android/location/places/aw;-><init>(B)V

    iput-object v1, p0, Lcom/google/android/location/places/as;->o:Ljava/util/concurrent/Executor;

    .line 130
    iget-object v1, p0, Lcom/google/android/location/places/as;->n:Landroid/os/HandlerThread;

    invoke-virtual {v1}, Landroid/os/HandlerThread;->start()V

    .line 131
    new-instance v1, Lcom/google/android/location/places/ay;

    iget-object v2, p0, Lcom/google/android/location/places/as;->n:Landroid/os/HandlerThread;

    invoke-virtual {v2}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-direct {v1, v2, p0}, Lcom/google/android/location/places/ay;-><init>(Landroid/os/Looper;Lcom/google/android/location/places/as;)V

    iput-object v1, p0, Lcom/google/android/location/places/as;->p:Lcom/google/android/location/places/ay;

    .line 132
    new-instance v1, Lcom/google/android/location/places/bi;

    invoke-direct {v1}, Lcom/google/android/location/places/bi;-><init>()V

    iput-object v1, p0, Lcom/google/android/location/places/as;->c:Lcom/google/android/location/places/bi;

    .line 134
    new-instance v1, Lcom/google/android/location/n/ai;

    invoke-static {}, Lcom/google/android/gms/common/util/r;->c()Lcom/google/android/gms/common/util/p;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/google/android/location/n/ai;-><init>(Lcom/google/android/gms/common/util/p;)V

    iput-object v1, p0, Lcom/google/android/location/places/as;->q:Lcom/google/android/location/n/ai;

    .line 135
    new-instance v1, Lcom/google/android/location/places/t;

    iget-object v2, p0, Lcom/google/android/location/places/as;->q:Lcom/google/android/location/n/ai;

    iget-object v3, p0, Lcom/google/android/location/places/as;->p:Lcom/google/android/location/places/ay;

    invoke-direct {v1, v2, v3}, Lcom/google/android/location/places/t;-><init>(Lcom/google/android/location/n/ai;Landroid/os/Handler;)V

    iput-object v1, p0, Lcom/google/android/location/places/as;->b:Lcom/google/android/location/places/t;

    .line 136
    iget-object v2, p0, Lcom/google/android/location/places/as;->a:Landroid/content/Context;

    iget-object v3, p0, Lcom/google/android/location/places/as;->p:Lcom/google/android/location/places/ay;

    iget-object v4, p0, Lcom/google/android/location/places/as;->b:Lcom/google/android/location/places/t;

    iget-object v5, p0, Lcom/google/android/location/places/as;->c:Lcom/google/android/location/places/bi;

    iget-object v6, p0, Lcom/google/android/location/places/as;->q:Lcom/google/android/location/n/ai;

    new-instance v1, Lcom/google/android/location/places/f;

    invoke-virtual {v2}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v7

    invoke-static {v7}, Lcom/google/android/location/d/j;->a(Landroid/content/Context;)Lcom/google/android/location/d/j;

    move-result-object v7

    invoke-direct/range {v1 .. v7}, Lcom/google/android/location/places/f;-><init>(Landroid/content/Context;Landroid/os/Handler;Lcom/google/android/location/places/t;Lcom/google/android/location/places/bi;Lcom/google/android/location/n/ai;Lcom/google/android/location/d/j;)V

    iput-object v1, p0, Lcom/google/android/location/places/as;->d:Lcom/google/android/location/places/f;

    .line 140
    move-object/from16 v0, p2

    iput-object v0, p0, Lcom/google/android/location/places/as;->s:Lcom/google/android/location/fused/g;

    .line 142
    sget-object v1, Lcom/google/android/location/x;->l:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v1}, Lcom/google/android/gms/common/a/d;->c()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/location/places/as;->l:Ljava/util/List;

    .line 145
    new-instance v1, Lcom/google/android/location/places/c/l;

    iget-object v2, p0, Lcom/google/android/location/places/as;->a:Landroid/content/Context;

    invoke-direct {v1, v2}, Lcom/google/android/location/places/c/l;-><init>(Landroid/content/Context;)V

    .line 147
    new-instance v2, Lcom/google/android/location/places/c/k;

    iget-object v3, p0, Lcom/google/android/location/places/as;->a:Landroid/content/Context;

    invoke-direct {v2, v3}, Lcom/google/android/location/places/c/k;-><init>(Landroid/content/Context;)V

    .line 148
    new-instance v3, Lcom/google/android/location/places/c/j;

    iget-object v4, p0, Lcom/google/android/location/places/as;->a:Landroid/content/Context;

    invoke-direct {v3, v4, v2, v1}, Lcom/google/android/location/places/c/j;-><init>(Landroid/content/Context;Lcom/google/android/location/places/c/k;Lcom/google/android/location/places/c/l;)V

    iput-object v3, p0, Lcom/google/android/location/places/as;->f:Lcom/google/android/location/places/c/j;

    .line 150
    new-instance v2, Lcom/google/android/location/places/c/c;

    iget-object v3, p0, Lcom/google/android/location/places/as;->a:Landroid/content/Context;

    invoke-direct {v2, v3}, Lcom/google/android/location/places/c/c;-><init>(Landroid/content/Context;)V

    .line 151
    new-instance v4, Lcom/google/android/location/places/c/b;

    iget-object v3, p0, Lcom/google/android/location/places/as;->a:Landroid/content/Context;

    invoke-direct {v4, v3, v2, v1}, Lcom/google/android/location/places/c/b;-><init>(Landroid/content/Context;Lcom/google/android/location/places/c/c;Lcom/google/android/location/places/c/l;)V

    .line 154
    new-instance v13, Lcom/google/android/location/places/d/i;

    invoke-direct {v13}, Lcom/google/android/location/places/d/i;-><init>()V

    .line 156
    new-instance v1, Lcom/google/android/location/places/b/k;

    iget-object v2, p0, Lcom/google/android/location/places/as;->a:Landroid/content/Context;

    iget-object v3, p0, Lcom/google/android/location/places/as;->s:Lcom/google/android/location/fused/g;

    new-instance v5, Lcom/google/android/location/geofencer/service/ah;

    invoke-direct {v5}, Lcom/google/android/location/geofencer/service/ah;-><init>()V

    move-object/from16 v0, p3

    invoke-direct {v1, v2, v0, v3}, Lcom/google/android/location/places/b/k;-><init>(Landroid/content/Context;Lcom/google/android/location/geofencer/service/g;Lcom/google/android/location/fused/g;)V

    .line 161
    new-instance v2, Lcom/google/android/location/places/b/a;

    invoke-direct {v2, v1}, Lcom/google/android/location/places/b/a;-><init>(Lcom/google/android/location/places/b/k;)V

    iput-object v2, p0, Lcom/google/android/location/places/as;->j:Lcom/google/android/location/places/b/a;

    .line 163
    iget-object v1, p0, Lcom/google/android/location/places/as;->a:Landroid/content/Context;

    const-string v2, "alarm"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Landroid/app/AlarmManager;

    .line 165
    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Runtime;->availableProcessors()I

    move-result v1

    .line 166
    new-instance v2, Lcom/google/android/location/places/c/d;

    iget-object v3, p0, Lcom/google/android/location/places/as;->o:Ljava/util/concurrent/Executor;

    add-int/lit8 v1, v1, 0x1

    invoke-static {v1}, Ljava/util/concurrent/Executors;->newFixedThreadPool(I)Ljava/util/concurrent/ExecutorService;

    move-result-object v1

    invoke-direct {v2, v3, v1}, Lcom/google/android/location/places/c/d;-><init>(Ljava/util/concurrent/Executor;Ljava/util/concurrent/Executor;)V

    iput-object v2, p0, Lcom/google/android/location/places/as;->k:Lcom/google/android/location/places/c/d;

    .line 168
    iget-object v8, p0, Lcom/google/android/location/places/as;->a:Landroid/content/Context;

    iget-object v10, p0, Lcom/google/android/location/places/as;->d:Lcom/google/android/location/places/f;

    iget-object v3, p0, Lcom/google/android/location/places/as;->o:Ljava/util/concurrent/Executor;

    iget-object v1, p0, Lcom/google/android/location/places/as;->k:Lcom/google/android/location/places/c/d;

    new-instance v2, Lcom/google/android/location/os/real/ap;

    invoke-direct {v2}, Lcom/google/android/location/os/real/ap;-><init>()V

    new-instance v6, Lcom/google/android/location/places/bp;

    invoke-direct {v6, v10, v4, v1}, Lcom/google/android/location/places/bp;-><init>(Lcom/google/android/location/places/f;Lcom/google/android/location/places/c/b;Lcom/google/android/location/places/c/d;)V

    new-instance v5, Lcom/google/android/location/places/af;

    invoke-direct {v5, v2}, Lcom/google/android/location/places/af;-><init>(Lcom/google/android/location/j/b;)V

    sget-object v1, Lcom/google/android/location/x;->V:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v1}, Lcom/google/android/gms/common/a/d;->c()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    const-string v4, ","

    invoke-virtual {v1, v4}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v11

    const/4 v1, 0x0

    const-string v4, "ReverseGeocoding"

    invoke-interface {v11, v4}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    const/4 v4, 0x0

    sget-object v1, Lcom/google/android/location/x;->s:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v1}, Lcom/google/android/gms/common/a/d;->c()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-static {v8}, Lcom/google/android/location/places/br;->a(Landroid/content/Context;)Lcom/google/android/location/places/br;

    move-result-object v1

    :goto_0
    const-string v4, "Places"

    const/4 v7, 0x4

    invoke-static {v4, v7}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_0

    const-string v4, "Places"

    const-string v7, "Creating ReverseGeocodingPlaceInferenceModule"

    invoke-static {v4, v7}, Lcom/google/android/location/n/aa;->c(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    new-instance v4, Lcom/google/android/location/places/bv;

    invoke-direct {v4, v5, v6, v2, v1}, Lcom/google/android/location/places/bv;-><init>(Lcom/google/android/location/places/af;Lcom/google/android/location/places/be;Lcom/google/android/location/j/b;Lcom/google/android/location/places/f/c;)V

    move-object v7, v4

    :goto_1
    const/4 v1, 0x0

    const-string v4, "WifiDecisionTree"

    invoke-interface {v11, v4}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    const-string v1, "Places"

    const/4 v4, 0x4

    invoke-static {v1, v4}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_1

    const-string v1, "Places"

    const-string v4, "Creating WifiDecisionTreePlaceInferenceModule"

    invoke-static {v1, v4}, Lcom/google/android/location/n/aa;->c(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    new-instance v6, Lcom/google/android/location/places/ba;

    invoke-direct {v6, v8, v10, v2}, Lcom/google/android/location/places/ba;-><init>(Landroid/content/Context;Lcom/google/android/location/places/f;Lcom/google/android/location/j/b;)V

    new-instance v1, Lcom/google/android/location/places/f/a;

    invoke-static {}, Ljava/util/concurrent/Executors;->newSingleThreadExecutor()Ljava/util/concurrent/ExecutorService;

    move-result-object v4

    invoke-direct/range {v1 .. v6}, Lcom/google/android/location/places/f/a;-><init>(Lcom/google/android/location/j/b;Ljava/util/concurrent/Executor;Ljava/util/concurrent/Executor;Lcom/google/android/location/places/af;Lcom/google/android/location/g/s;)V

    :cond_2
    new-instance v3, Lcom/google/android/location/places/y;

    invoke-direct {v3, v2, v5, v1, v7}, Lcom/google/android/location/places/y;-><init>(Lcom/google/android/location/j/b;Lcom/google/android/location/places/af;Lcom/google/android/location/places/f/a;Lcom/google/android/location/places/bv;)V

    iput-object v3, p0, Lcom/google/android/location/places/as;->e:Lcom/google/android/location/places/y;

    .line 174
    new-instance v1, Lcom/google/android/location/places/d/e;

    iget-object v2, p0, Lcom/google/android/location/places/as;->a:Landroid/content/Context;

    iget-object v3, p0, Lcom/google/android/location/places/as;->f:Lcom/google/android/location/places/c/j;

    new-instance v4, Lcom/google/android/location/places/bz;

    invoke-virtual {p1}, Landroid/content/Context;->getMainLooper()Landroid/os/Looper;

    move-result-object v5

    invoke-direct {v4, v5}, Lcom/google/android/location/places/bz;-><init>(Landroid/os/Looper;)V

    iget-object v5, p0, Lcom/google/android/location/places/as;->k:Lcom/google/android/location/places/c/d;

    iget-object v7, p0, Lcom/google/android/location/places/as;->s:Lcom/google/android/location/fused/g;

    iget-object v8, p0, Lcom/google/android/location/places/as;->j:Lcom/google/android/location/places/b/a;

    new-instance v10, Lcom/google/android/location/geofencer/service/ah;

    invoke-direct {v10}, Lcom/google/android/location/geofencer/service/ah;-><init>()V

    iget-object v6, p0, Lcom/google/android/location/places/as;->a:Landroid/content/Context;

    invoke-static {v6}, Landroid/support/v4/a/m;->a(Landroid/content/Context;)Landroid/support/v4/a/m;

    move-result-object v11

    invoke-static {}, Lcom/google/android/gms/common/util/r;->c()Lcom/google/android/gms/common/util/p;

    move-result-object v12

    move-object v6, v13

    invoke-direct/range {v1 .. v12}, Lcom/google/android/location/places/d/e;-><init>(Landroid/content/Context;Lcom/google/android/location/places/c/j;Lcom/google/android/location/places/bz;Lcom/google/android/location/places/c/d;Lcom/google/android/location/places/d/i;Lcom/google/android/location/fused/g;Lcom/google/android/location/places/b/a;Landroid/app/AlarmManager;Lcom/google/android/location/geofencer/service/ah;Landroid/support/v4/a/m;Lcom/google/android/gms/common/util/p;)V

    iput-object v1, p0, Lcom/google/android/location/places/as;->g:Lcom/google/android/location/places/d/e;

    .line 186
    sget-object v1, Lcom/google/android/location/x;->z:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v1}, Lcom/google/android/gms/common/a/d;->c()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 188
    invoke-static {v1}, Lcom/google/android/location/places/d/j;->a(Ljava/lang/String;)Lcom/google/android/location/places/d/j;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/location/places/as;->r:Lcom/google/android/location/places/d/j;

    .line 189
    new-instance v12, Lcom/google/android/location/places/b/d;

    new-instance v1, Lcom/google/android/location/places/bz;

    invoke-virtual {p1}, Landroid/content/Context;->getMainLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/google/android/location/places/bz;-><init>(Landroid/os/Looper;)V

    iget-object v2, p0, Lcom/google/android/location/places/as;->s:Lcom/google/android/location/fused/g;

    iget-object v3, p0, Lcom/google/android/location/places/as;->j:Lcom/google/android/location/places/b/a;

    iget-object v4, p0, Lcom/google/android/location/places/as;->d:Lcom/google/android/location/places/f;

    invoke-direct {v12, v1, v2, v3, v4}, Lcom/google/android/location/places/b/d;-><init>(Lcom/google/android/location/places/bz;Lcom/google/android/location/fused/g;Lcom/google/android/location/places/b/a;Lcom/google/android/location/places/f;)V

    .line 195
    new-instance v7, Lcom/google/android/location/places/ak;

    invoke-direct {v7, p1}, Lcom/google/android/location/places/ak;-><init>(Landroid/content/Context;)V

    .line 197
    new-instance v1, Lcom/google/android/location/places/an;

    iget-object v3, p0, Lcom/google/android/location/places/as;->p:Lcom/google/android/location/places/ay;

    new-instance v4, Lcom/google/android/location/places/bx;

    const/4 v2, 0x3

    invoke-direct {v4, p1, v2}, Lcom/google/android/location/places/bx;-><init>(Landroid/content/Context;I)V

    iget-object v5, p0, Lcom/google/android/location/places/as;->s:Lcom/google/android/location/fused/g;

    iget-object v6, p0, Lcom/google/android/location/places/as;->e:Lcom/google/android/location/places/y;

    iget-object v8, p0, Lcom/google/android/location/places/as;->g:Lcom/google/android/location/places/d/e;

    iget-object v9, p0, Lcom/google/android/location/places/as;->d:Lcom/google/android/location/places/f;

    invoke-static {}, Lcom/google/android/gms/common/util/r;->c()Lcom/google/android/gms/common/util/p;

    move-result-object v10

    move-object v2, p1

    invoke-direct/range {v1 .. v10}, Lcom/google/android/location/places/an;-><init>(Landroid/content/Context;Landroid/os/Handler;Lcom/google/android/location/places/bx;Lcom/google/android/location/fused/g;Lcom/google/android/location/places/y;Lcom/google/android/location/places/ak;Lcom/google/android/location/places/d/e;Lcom/google/android/location/places/f;Lcom/google/android/gms/common/util/p;)V

    iput-object v1, p0, Lcom/google/android/location/places/as;->h:Lcom/google/android/location/places/an;

    .line 208
    new-instance v3, Lcom/google/android/location/places/r;

    iget-object v5, p0, Lcom/google/android/location/places/as;->p:Lcom/google/android/location/places/ay;

    new-instance v6, Lcom/google/android/location/places/bx;

    const/4 v1, 0x5

    invoke-direct {v6, p1, v1}, Lcom/google/android/location/places/bx;-><init>(Landroid/content/Context;I)V

    iget-object v8, p0, Lcom/google/android/location/places/as;->g:Lcom/google/android/location/places/d/e;

    iget-object v10, p0, Lcom/google/android/location/places/as;->j:Lcom/google/android/location/places/b/a;

    iget-object v11, p0, Lcom/google/android/location/places/as;->d:Lcom/google/android/location/places/f;

    move-object v4, p1

    move-object v9, v12

    invoke-direct/range {v3 .. v11}, Lcom/google/android/location/places/r;-><init>(Landroid/content/Context;Landroid/os/Handler;Lcom/google/android/location/places/bx;Lcom/google/android/location/places/ak;Lcom/google/android/location/places/d/e;Lcom/google/android/location/places/b/d;Lcom/google/android/location/places/b/a;Lcom/google/android/location/places/f;)V

    iput-object v3, p0, Lcom/google/android/location/places/as;->i:Lcom/google/android/location/places/r;

    .line 218
    new-instance v1, Landroid/content/IntentFilter;

    invoke-direct {v1}, Landroid/content/IntentFilter;-><init>()V

    const-string v2, "android.intent.action.PACKAGE_REMOVED"

    invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v2, "android.intent.action.PACKAGE_DATA_CLEARED"

    invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v2, "package"

    invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->addDataScheme(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/location/places/as;->a:Landroid/content/Context;

    iget-object v3, p0, Lcom/google/android/location/places/as;->m:Landroid/content/BroadcastReceiver;

    invoke-virtual {v2, v3, v1}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 219
    return-void

    :cond_3
    move-object v1, v4

    goto/16 :goto_0

    :cond_4
    move-object v7, v1

    goto/16 :goto_1
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/location/places/internal/PlacesParams;Ljava/util/Collection;)V
    .locals 4

    .prologue
    .line 463
    iget-object v1, p1, Lcom/google/android/gms/location/places/internal/PlacesParams;->c:Ljava/lang/String;

    .line 465
    const-string v0, "com.google.android.gms"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 466
    invoke-interface {p2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/location/places/UserDataType;

    .line 467
    iget-object v2, p0, Lcom/google/android/location/places/as;->r:Lcom/google/android/location/places/d/j;

    iget-object v3, p1, Lcom/google/android/gms/location/places/internal/PlacesParams;->f:Ljava/lang/String;

    iget-object v2, v2, Lcom/google/android/location/places/d/j;->b:Lcom/google/k/c/dn;

    invoke-static {v3, v2, v0}, Lcom/google/android/location/places/d/j;->a(Ljava/lang/String;Lcom/google/k/c/dn;Lcom/google/android/gms/location/places/UserDataType;)V

    goto :goto_0

    .line 471
    :cond_0
    invoke-interface {p2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/location/places/UserDataType;

    .line 472
    iget-object v3, p0, Lcom/google/android/location/places/as;->r:Lcom/google/android/location/places/d/j;

    iget-object v3, v3, Lcom/google/android/location/places/d/j;->a:Lcom/google/k/c/dn;

    invoke-static {v1, v3, v0}, Lcom/google/android/location/places/d/j;->a(Ljava/lang/String;Lcom/google/k/c/dn;Lcom/google/android/gms/location/places/UserDataType;)V

    goto :goto_1

    .line 475
    :cond_1
    return-void
.end method
