.class public final Lcom/google/android/location/os/real/bh;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/net/wifi/WifiScanner$ScanListener;


# instance fields
.field private final a:Landroid/net/wifi/WifiScanner;

.field private final b:Lcom/google/android/location/os/real/be;

.field private final c:Ljava/util/ArrayList;

.field private final d:Z


# direct methods
.method public constructor <init>(Landroid/net/wifi/WifiScanner;ZLcom/google/android/location/os/real/be;)V
    .locals 1

    .prologue
    .line 45
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 46
    iput-object p1, p0, Lcom/google/android/location/os/real/bh;->a:Landroid/net/wifi/WifiScanner;

    .line 47
    iput-object p3, p0, Lcom/google/android/location/os/real/bh;->b:Lcom/google/android/location/os/real/be;

    .line 48
    iput-boolean p2, p0, Lcom/google/android/location/os/real/bh;->d:Z

    .line 52
    if-eqz p2, :cond_0

    .line 53
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 58
    :goto_0
    iput-object v0, p0, Lcom/google/android/location/os/real/bh;->c:Ljava/util/ArrayList;

    .line 59
    return-void

    .line 55
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final onFailure(ILjava/lang/String;)V
    .locals 3

    .prologue
    .line 97
    sget-boolean v0, Lcom/google/android/location/i/a;->b:Z

    if-eqz v0, :cond_0

    const-string v0, "WifiScanListener"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "onFailure, c is "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " and s is "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/location/o/a/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 98
    :cond_0
    return-void
.end method

.method public final onFullResult(Landroid/net/wifi/ScanResult;)V
    .locals 3

    .prologue
    .line 88
    sget-boolean v0, Lcom/google/android/location/i/a;->b:Z

    if-eqz v0, :cond_0

    const-string v0, "WifiScanListener"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "onFullResult: returned "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", oneShot is "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/google/android/location/os/real/bh;->d:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/location/o/a/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 90
    :cond_0
    iget-boolean v0, p0, Lcom/google/android/location/os/real/bh;->d:Z

    if-eqz v0, :cond_1

    .line 91
    iget-object v0, p0, Lcom/google/android/location/os/real/bh;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 93
    :cond_1
    return-void
.end method

.method public final onPeriodChanged(I)V
    .locals 3

    .prologue
    .line 63
    sget-boolean v0, Lcom/google/android/location/i/a;->b:Z

    if-eqz v0, :cond_0

    const-string v0, "WifiScanListener"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "onPeriodChanged: new is "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/location/o/a/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 64
    :cond_0
    return-void
.end method

.method public final onResults([Landroid/net/wifi/ScanResult;)V
    .locals 3

    .prologue
    .line 68
    sget-boolean v0, Lcom/google/android/location/i/a;->b:Z

    if-eqz v0, :cond_0

    const-string v0, "WifiScanListener"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "onResults, length is "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    array-length v2, p1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", cancel flag is "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/google/android/location/os/real/bh;->d:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/location/o/a/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 70
    :cond_0
    iget-boolean v0, p0, Lcom/google/android/location/os/real/bh;->d:Z

    if-eqz v0, :cond_1

    .line 72
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/google/android/location/os/real/bh;->c:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 73
    iget-object v1, p0, Lcom/google/android/location/os/real/bh;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 74
    iget-object v1, p0, Lcom/google/android/location/os/real/bh;->b:Lcom/google/android/location/os/real/be;

    invoke-interface {v1, v0}, Lcom/google/android/location/os/real/be;->a(Ljava/util/List;)V

    .line 76
    iget-object v0, p0, Lcom/google/android/location/os/real/bh;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 78
    iget-object v0, p0, Lcom/google/android/location/os/real/bh;->a:Landroid/net/wifi/WifiScanner;

    invoke-virtual {v0, p0}, Landroid/net/wifi/WifiScanner;->stopBackgroundScan(Landroid/net/wifi/WifiScanner$ScanListener;)V

    .line 84
    :goto_0
    return-void

    .line 81
    :cond_1
    iget-object v0, p0, Lcom/google/android/location/os/real/bh;->b:Lcom/google/android/location/os/real/be;

    invoke-static {p1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/location/os/real/be;->a(Ljava/util/List;)V

    goto :goto_0
.end method

.method public final onSuccess()V
    .locals 2

    .prologue
    .line 102
    sget-boolean v0, Lcom/google/android/location/i/a;->b:Z

    if-eqz v0, :cond_0

    const-string v0, "WifiScanListener"

    const-string v1, "onSuccess"

    invoke-static {v0, v1}, Lcom/google/android/location/o/a/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 103
    :cond_0
    return-void
.end method
