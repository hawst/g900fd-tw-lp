.class final Lcom/google/android/location/copresence/r/ae;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final a:Landroid/net/wifi/WifiManager;


# direct methods
.method constructor <init>(Landroid/net/wifi/WifiManager;)V
    .locals 0

    .prologue
    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    iput-object p1, p0, Lcom/google/android/location/copresence/r/ae;->a:Landroid/net/wifi/WifiManager;

    .line 35
    return-void
.end method


# virtual methods
.method final a()Z
    .locals 2

    .prologue
    .line 115
    invoke-virtual {p0}, Lcom/google/android/location/copresence/r/ae;->b()Landroid/net/wifi/WifiConfiguration;

    move-result-object v0

    .line 116
    if-eqz v0, :cond_0

    iget-object v1, v0, Landroid/net/wifi/WifiConfiguration;->SSID:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 117
    iget-object v0, v0, Landroid/net/wifi/WifiConfiguration;->SSID:Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/libraries/a/a/a/a;->a(Ljava/lang/String;)Z

    move-result v0

    .line 119
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method final a(Landroid/net/wifi/WifiConfiguration;)Z
    .locals 4

    .prologue
    .line 162
    const/4 v0, 0x3

    :try_start_0
    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 163
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "WifiManagerWrapper: setWifiApConfiguration: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->b(Ljava/lang/String;)I

    .line 165
    :cond_0
    iget-object v0, p0, Lcom/google/android/location/copresence/r/ae;->a:Landroid/net/wifi/WifiManager;

    const-class v1, Ljava/lang/Boolean;

    const-string v2, "setWifiApConfiguration"

    const-class v3, Landroid/net/wifi/WifiConfiguration;

    invoke-static {v0, v1, v2, v3, p1}, Lcom/google/android/location/copresence/bd;->a(Ljava/lang/Object;Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Class;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    const-string v1, "setWifiApConfiguration"

    invoke-static {v0, v1}, Lcom/google/android/location/copresence/bd;->a(Ljava/lang/Boolean;Ljava/lang/String;)Z
    :try_end_0
    .catch Lcom/google/android/location/copresence/be; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 173
    :goto_0
    return v0

    .line 169
    :catch_0
    move-exception v0

    .line 170
    const/4 v1, 0x6

    invoke-static {v1}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 171
    const-string v1, "WifiManagerWrapper: Failed to invoke setWifiApConfiguration"

    invoke-static {v1, v0}, Lcom/google/android/location/copresence/ag;->a(Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 173
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method final a(Landroid/net/wifi/WifiConfiguration;Z)Z
    .locals 7

    .prologue
    .line 145
    const/4 v0, 0x3

    :try_start_0
    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 146
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "WifiManagerWrapper: setWifiApEnabled: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " enabled:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->b(Ljava/lang/String;)I

    .line 148
    :cond_0
    iget-object v0, p0, Lcom/google/android/location/copresence/r/ae;->a:Landroid/net/wifi/WifiManager;

    const-class v1, Ljava/lang/Boolean;

    const-string v2, "setWifiApEnabled"

    const-class v3, Landroid/net/wifi/WifiConfiguration;

    sget-object v5, Ljava/lang/Boolean;->TYPE:Ljava/lang/Class;

    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    move-object v4, p1

    invoke-static/range {v0 .. v6}, Lcom/google/android/location/copresence/bd;->a(Ljava/lang/Object;Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Class;Ljava/lang/Object;Ljava/lang/Class;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    const-string v1, "setWifiApEnabled"

    invoke-static {v0, v1}, Lcom/google/android/location/copresence/bd;->a(Ljava/lang/Boolean;Ljava/lang/String;)Z
    :try_end_0
    .catch Lcom/google/android/location/copresence/be; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 156
    :goto_0
    return v0

    .line 152
    :catch_0
    move-exception v0

    .line 153
    const/4 v1, 0x6

    invoke-static {v1}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 154
    const-string v1, "WifiManagerWrapper: Failed to invoke setWifiApEnabled"

    invoke-static {v1, v0}, Lcom/google/android/location/copresence/ag;->a(Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 156
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method final a(Z)Z
    .locals 2

    .prologue
    .line 137
    const/4 v0, 0x3

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 138
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "WifiManagerWrapper: setWifiEnabled: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->b(Ljava/lang/String;)I

    .line 140
    :cond_0
    iget-object v0, p0, Lcom/google/android/location/copresence/r/ae;->a:Landroid/net/wifi/WifiManager;

    invoke-virtual {v0, p1}, Landroid/net/wifi/WifiManager;->setWifiEnabled(Z)Z

    move-result v0

    return v0
.end method

.method final b()Landroid/net/wifi/WifiConfiguration;
    .locals 3

    .prologue
    .line 124
    const/4 v0, 0x3

    :try_start_0
    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 125
    const-string v0, "WifiManagerWrapper: getWifiApConfiguration"

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->b(Ljava/lang/String;)I

    .line 127
    :cond_0
    iget-object v0, p0, Lcom/google/android/location/copresence/r/ae;->a:Landroid/net/wifi/WifiManager;

    const-class v1, Landroid/net/wifi/WifiConfiguration;

    const-string v2, "getWifiApConfiguration"

    invoke-static {v0, v1, v2}, Lcom/google/android/location/copresence/bd;->a(Ljava/lang/Object;Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/wifi/WifiConfiguration;
    :try_end_0
    .catch Lcom/google/android/location/copresence/be; {:try_start_0 .. :try_end_0} :catch_0

    .line 132
    :goto_0
    return-object v0

    .line 128
    :catch_0
    move-exception v0

    .line 129
    const/4 v1, 0x6

    invoke-static {v1}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 130
    const-string v1, "WifiManagerWrapper: Failed to invoke getWifiApConfiguration"

    invoke-static {v1, v0}, Lcom/google/android/location/copresence/ag;->a(Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 132
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method final c()Z
    .locals 3

    .prologue
    .line 179
    :try_start_0
    iget-object v0, p0, Lcom/google/android/location/copresence/r/ae;->a:Landroid/net/wifi/WifiManager;

    const-class v1, Ljava/lang/Boolean;

    const-string v2, "isWifiApEnabled"

    invoke-static {v0, v1, v2}, Lcom/google/android/location/copresence/bd;->a(Ljava/lang/Object;Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    const-string v1, "isWifiApEnabled"

    invoke-static {v0, v1}, Lcom/google/android/location/copresence/bd;->a(Ljava/lang/Boolean;Ljava/lang/String;)Z
    :try_end_0
    .catch Lcom/google/android/location/copresence/be; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 187
    :goto_0
    return v0

    .line 181
    :catch_0
    move-exception v0

    .line 182
    const/4 v1, 0x6

    invoke-static {v1}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 183
    const-string v1, "WifiManagerWrapper: Failed to invoke isWifiApEnabled"

    invoke-static {v1, v0}, Lcom/google/android/location/copresence/ag;->a(Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 187
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final d()Z
    .locals 6
    .annotation build Landroid/annotation/TargetApi;
        value = 0xe
    .end annotation

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 194
    const/4 v0, 0x3

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v3, "WifiManagerWrapper: connectionInfo="

    invoke-direct {v0, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/google/android/location/copresence/r/ae;->a:Landroid/net/wifi/WifiManager;

    invoke-virtual {v3}, Landroid/net/wifi/WifiManager;->getConnectionInfo()Landroid/net/wifi/WifiInfo;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->b(Ljava/lang/String;)I

    :cond_0
    iget-object v0, p0, Lcom/google/android/location/copresence/r/ae;->a:Landroid/net/wifi/WifiManager;

    invoke-virtual {v0}, Landroid/net/wifi/WifiManager;->getConnectionInfo()Landroid/net/wifi/WifiInfo;

    move-result-object v3

    .line 195
    if-nez v3, :cond_2

    .line 228
    :cond_1
    :goto_0
    return v1

    .line 198
    :cond_2
    sget-object v0, Lcom/google/android/location/copresence/r/af;->a:[I

    invoke-virtual {v3}, Landroid/net/wifi/WifiInfo;->getSupplicantState()Landroid/net/wifi/SupplicantState;

    move-result-object v4

    invoke-virtual {v4}, Landroid/net/wifi/SupplicantState;->ordinal()I

    move-result v4

    aget v0, v0, v4

    packed-switch v0, :pswitch_data_0

    move v0, v2

    .line 218
    :goto_1
    sget v4, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v5, 0xe

    if-lt v4, v5, :cond_3

    .line 221
    invoke-virtual {v3}, Landroid/net/wifi/WifiInfo;->getSupplicantState()Landroid/net/wifi/SupplicantState;

    move-result-object v4

    sget-object v5, Landroid/net/wifi/SupplicantState;->INTERFACE_DISABLED:Landroid/net/wifi/SupplicantState;

    if-eq v4, v5, :cond_1

    .line 223
    invoke-virtual {v3}, Landroid/net/wifi/WifiInfo;->getSupplicantState()Landroid/net/wifi/SupplicantState;

    move-result-object v1

    sget-object v3, Landroid/net/wifi/SupplicantState;->AUTHENTICATING:Landroid/net/wifi/SupplicantState;

    if-ne v1, v3, :cond_3

    move v1, v2

    .line 224
    goto :goto_0

    :pswitch_0
    move v0, v1

    .line 205
    goto :goto_1

    :cond_3
    move v1, v0

    goto :goto_0

    .line 198
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method
