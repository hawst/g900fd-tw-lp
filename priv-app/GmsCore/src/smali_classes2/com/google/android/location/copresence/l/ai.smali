.class final Lcom/google/android/location/copresence/l/ai;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field static final a:Ljava/util/Map;


# instance fields
.field private final b:Ljava/lang/String;

.field private final c:Landroid/content/Context;

.field private final d:Lcom/google/android/location/copresence/debug/CopresDatabaseHelper$HttpRecord;

.field private e:J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 28
    sget-object v0, Ljava/util/Collections;->EMPTY_MAP:Ljava/util/Map;

    sput-object v0, Lcom/google/android/location/copresence/l/ai;->a:Ljava/util/Map;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    new-instance v0, Lcom/google/android/location/copresence/debug/CopresDatabaseHelper$HttpRecord;

    invoke-direct {v0}, Lcom/google/android/location/copresence/debug/CopresDatabaseHelper$HttpRecord;-><init>()V

    iput-object v0, p0, Lcom/google/android/location/copresence/l/ai;->d:Lcom/google/android/location/copresence/debug/CopresDatabaseHelper$HttpRecord;

    .line 38
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/location/copresence/l/ai;->e:J

    .line 41
    iput-object p1, p0, Lcom/google/android/location/copresence/l/ai;->c:Landroid/content/Context;

    .line 42
    iput-object p2, p0, Lcom/google/android/location/copresence/l/ai;->b:Ljava/lang/String;

    .line 43
    return-void
.end method

.method public static a(Lcom/android/volley/ac;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 111
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 113
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 114
    const-string v1, ": "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 116
    iget-object v1, p0, Lcom/android/volley/ac;->a:Lcom/android/volley/m;

    .line 117
    if-eqz v1, :cond_0

    .line 118
    const-string v2, "("

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 119
    iget v1, v1, Lcom/android/volley/m;->a:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 120
    const-string v1, ") "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 123
    :cond_0
    invoke-virtual {p0}, Lcom/android/volley/ac;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 125
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(Lcom/google/ac/b/c/dc;Lcom/google/protobuf/nano/j;ILjava/util/Map;I)V
    .locals 7

    .prologue
    const/4 v6, 0x3

    const/4 v5, 0x2

    .line 75
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/google/android/location/copresence/l/ai;->e:J

    sub-long/2addr v0, v2

    .line 77
    invoke-static {v5}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 78
    const-string v2, "Network Response: [%dms] [%d] %s"

    new-array v3, v6, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    aput-object v0, v3, v4

    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/location/copresence/l/ai;->d:Lcom/google/android/location/copresence/debug/CopresDatabaseHelper$HttpRecord;

    iget-object v1, v1, Lcom/google/android/location/copresence/debug/CopresDatabaseHelper$HttpRecord;->responseStatus:Ljava/lang/Integer;

    aput-object v1, v3, v0

    iget-object v0, p0, Lcom/google/android/location/copresence/l/ai;->d:Lcom/google/android/location/copresence/debug/CopresDatabaseHelper$HttpRecord;

    iget-object v0, v0, Lcom/google/android/location/copresence/debug/CopresDatabaseHelper$HttpRecord;->url:Ljava/lang/String;

    aput-object v0, v3, v5

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 83
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "ServerTaskLogRecord: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->a(Ljava/lang/String;)I

    .line 84
    if-eqz p1, :cond_0

    iget-object v0, p1, Lcom/google/ac/b/c/dc;->b:Lcom/google/ac/b/c/aa;

    if-eqz v0, :cond_0

    iget-object v0, p1, Lcom/google/ac/b/c/dc;->b:Lcom/google/ac/b/c/aa;

    iget-object v0, v0, Lcom/google/ac/b/c/aa;->a:Ljava/lang/String;

    if-eqz v0, :cond_0

    invoke-static {v6}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Backend server: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p1, Lcom/google/ac/b/c/dc;->b:Lcom/google/ac/b/c/aa;

    iget-object v1, v1, Lcom/google/ac/b/c/aa;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->b(Ljava/lang/String;)I

    .line 87
    :cond_0
    invoke-static {}, Lcom/google/android/location/copresence/f/a;->a()Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/location/copresence/l/ai;->d:Lcom/google/android/location/copresence/debug/CopresDatabaseHelper$HttpRecord;

    iget-object v0, v0, Lcom/google/android/location/copresence/debug/CopresDatabaseHelper$HttpRecord;->requestBody:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 88
    iget-object v0, p0, Lcom/google/android/location/copresence/l/ai;->d:Lcom/google/android/location/copresence/debug/CopresDatabaseHelper$HttpRecord;

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, v0, Lcom/google/android/location/copresence/debug/CopresDatabaseHelper$HttpRecord;->responseStatus:Ljava/lang/Integer;

    .line 89
    iget-object v0, p0, Lcom/google/android/location/copresence/l/ai;->d:Lcom/google/android/location/copresence/debug/CopresDatabaseHelper$HttpRecord;

    invoke-static {p5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, v0, Lcom/google/android/location/copresence/debug/CopresDatabaseHelper$HttpRecord;->copresenceStatus:Ljava/lang/Integer;

    .line 90
    iget-object v0, p0, Lcom/google/android/location/copresence/l/ai;->d:Lcom/google/android/location/copresence/debug/CopresDatabaseHelper$HttpRecord;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    iput-object v1, v0, Lcom/google/android/location/copresence/debug/CopresDatabaseHelper$HttpRecord;->responseTimestamp:Ljava/lang/Long;

    .line 91
    if-eqz p4, :cond_1

    .line 92
    iget-object v1, p0, Lcom/google/android/location/copresence/l/ai;->d:Lcom/google/android/location/copresence/debug/CopresDatabaseHelper$HttpRecord;

    if-nez p4, :cond_4

    sget-object v0, Lcom/google/android/location/copresence/l/ai;->a:Ljava/util/Map;

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    iput-object v0, v1, Lcom/google/android/location/copresence/debug/CopresDatabaseHelper$HttpRecord;->responseHeaders:Ljava/lang/String;

    .line 94
    :cond_1
    if-eqz p2, :cond_2

    .line 95
    iget-object v0, p0, Lcom/google/android/location/copresence/l/ai;->d:Lcom/google/android/location/copresence/debug/CopresDatabaseHelper$HttpRecord;

    invoke-virtual {p2}, Lcom/google/protobuf/nano/j;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/google/android/location/copresence/debug/CopresDatabaseHelper$HttpRecord;->responseBody:Ljava/lang/String;

    .line 98
    :cond_2
    iget-object v0, p0, Lcom/google/android/location/copresence/l/ai;->c:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/location/copresence/l/ai;->d:Lcom/google/android/location/copresence/debug/CopresDatabaseHelper$HttpRecord;

    invoke-static {v0, v1}, Lcom/google/android/location/copresence/debug/CopresDatabaseHelper;->a(Landroid/content/Context;Lcom/google/android/location/copresence/debug/a;)V

    .line 100
    :cond_3
    return-void

    .line 92
    :cond_4
    invoke-virtual {p4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/location/copresence/a/a;Lcom/google/protobuf/nano/j;Ljava/util/Map;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 53
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/location/copresence/l/ai;->e:J

    .line 55
    invoke-static {}, Lcom/google/android/location/copresence/f/a;->a()Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 56
    iget-object v0, p0, Lcom/google/android/location/copresence/l/ai;->d:Lcom/google/android/location/copresence/debug/CopresDatabaseHelper$HttpRecord;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    iput-object v1, v0, Lcom/google/android/location/copresence/debug/CopresDatabaseHelper$HttpRecord;->requestTimestamp:Ljava/lang/Long;

    .line 57
    iget-object v0, p0, Lcom/google/android/location/copresence/l/ai;->d:Lcom/google/android/location/copresence/debug/CopresDatabaseHelper$HttpRecord;

    iget-object v1, p3, Lcom/google/android/location/copresence/a/a;->a:Landroid/accounts/Account;

    iget-object v1, v1, Landroid/accounts/Account;->name:Ljava/lang/String;

    iput-object v1, v0, Lcom/google/android/location/copresence/debug/CopresDatabaseHelper$HttpRecord;->accountName:Ljava/lang/String;

    .line 58
    iget-object v0, p0, Lcom/google/android/location/copresence/l/ai;->d:Lcom/google/android/location/copresence/debug/CopresDatabaseHelper$HttpRecord;

    iget-object v1, p0, Lcom/google/android/location/copresence/l/ai;->b:Ljava/lang/String;

    iput-object v1, v0, Lcom/google/android/location/copresence/debug/CopresDatabaseHelper$HttpRecord;->methodName:Ljava/lang/String;

    .line 59
    iget-object v0, p0, Lcom/google/android/location/copresence/l/ai;->d:Lcom/google/android/location/copresence/debug/CopresDatabaseHelper$HttpRecord;

    iput-object p1, v0, Lcom/google/android/location/copresence/debug/CopresDatabaseHelper$HttpRecord;->metadata:Ljava/lang/String;

    .line 60
    iget-object v0, p0, Lcom/google/android/location/copresence/l/ai;->d:Lcom/google/android/location/copresence/debug/CopresDatabaseHelper$HttpRecord;

    iput-object p2, v0, Lcom/google/android/location/copresence/debug/CopresDatabaseHelper$HttpRecord;->url:Ljava/lang/String;

    .line 61
    iget-object v0, p0, Lcom/google/android/location/copresence/l/ai;->d:Lcom/google/android/location/copresence/debug/CopresDatabaseHelper$HttpRecord;

    invoke-virtual {p4}, Lcom/google/protobuf/nano/j;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/google/android/location/copresence/debug/CopresDatabaseHelper$HttpRecord;->requestBody:Ljava/lang/String;

    .line 62
    iget-object v0, p0, Lcom/google/android/location/copresence/l/ai;->d:Lcom/google/android/location/copresence/debug/CopresDatabaseHelper$HttpRecord;

    invoke-virtual {p5}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/google/android/location/copresence/debug/CopresDatabaseHelper$HttpRecord;->requestHeaders:Ljava/lang/String;

    .line 63
    iget-object v0, p0, Lcom/google/android/location/copresence/l/ai;->d:Lcom/google/android/location/copresence/debug/CopresDatabaseHelper$HttpRecord;

    iput-object p6, v0, Lcom/google/android/location/copresence/debug/CopresDatabaseHelper$HttpRecord;->authToken:Ljava/lang/String;

    .line 65
    :cond_0
    return-void
.end method
