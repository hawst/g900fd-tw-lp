.class public Lcom/google/android/location/network/NetworkLocationService;
.super Landroid/app/IntentService;
.source "SourceFile"


# static fields
.field private static b:Lcom/google/android/location/network/NetworkLocationService;


# instance fields
.field private a:Lcom/google/android/location/network/NetworkLocationProvider;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 45
    const-string v0, "GCoreNetworkLocationService"

    invoke-direct {p0, v0}, Landroid/app/IntentService;-><init>(Ljava/lang/String;)V

    .line 46
    sput-object p0, Lcom/google/android/location/network/NetworkLocationService;->b:Lcom/google/android/location/network/NetworkLocationService;

    .line 47
    return-void
.end method

.method static declared-synchronized a(Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 116
    const-class v1, Lcom/google/android/location/network/NetworkLocationService;

    monitor-enter v1

    :try_start_0
    invoke-static {p0}, Lcom/google/android/location/network/NetworkLocationService;->b(Landroid/content/Context;)V
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 121
    :goto_0
    monitor-exit v1

    return-void

    .line 118
    :catch_0
    move-exception v0

    const/4 v0, 0x0

    :try_start_1
    invoke-static {p0, v0}, Lcom/google/android/location/network/NetworkLocationService;->b(Landroid/content/Context;Z)V

    .line 119
    const-string v0, "GCoreNlp"

    const-string v2, "Conservatively disabling NLP because tryApplySettings() failed"

    invoke-static {v0, v2}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 116
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static a(Landroid/content/Context;Z)V
    .locals 3

    .prologue
    .line 215
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "network_location_opt_in"

    if-eqz p1, :cond_0

    const-string v0, "1"

    :goto_0
    invoke-static {v1, v2, v0}, Lcom/google/android/gsf/e;->a(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z

    .line 217
    return-void

    .line 215
    :cond_0
    const-string v0, "0"

    goto :goto_0
.end method

.method private static declared-synchronized b(Landroid/content/Context;)V
    .locals 10

    .prologue
    const/4 v6, 0x1

    const/4 v7, 0x0

    .line 124
    const-class v8, Lcom/google/android/location/network/NetworkLocationService;

    monitor-enter v8

    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "network"

    invoke-static {v0, v1}, Landroid/provider/Settings$Secure;->isLocationProviderEnabled(Landroid/content/ContentResolver;Ljava/lang/String;)Z

    move-result v0

    .line 127
    invoke-static {p0}, Lcom/google/android/location/n/ae;->a(Landroid/content/Context;)Lcom/google/android/location/n/ae;

    move-result-object v1

    .line 128
    invoke-virtual {v1}, Lcom/google/android/location/n/ae;->b()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    if-eqz v1, :cond_1

    .line 199
    :cond_0
    :goto_0
    monitor-exit v8

    return-void

    .line 136
    :cond_1
    :try_start_1
    new-instance v1, Lcom/google/android/location/n/w;

    invoke-direct {v1, p0}, Lcom/google/android/location/n/w;-><init>(Landroid/content/Context;)V

    .line 137
    invoke-virtual {v1}, Lcom/google/android/location/n/w;->a()Z

    move-result v1

    if-nez v1, :cond_3

    .line 141
    const-string v1, "GCoreNlp"

    const/4 v2, 0x4

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 142
    const-string v1, "GCoreNlp"

    const-string v2, "!shouldConfirmNlp, ensuring user opted into NLP"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 144
    :cond_2
    const/4 v1, 0x1

    invoke-static {p0, v1}, Lcom/google/android/location/network/NetworkLocationService;->a(Landroid/content/Context;Z)V

    .line 146
    if-eqz v0, :cond_0

    invoke-static {p0}, Lcom/google/android/location/network/NetworkLocationService;->c(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 148
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.google.android.gsf.GOOGLE_LOCATION_SETTINGS"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 149
    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 150
    invoke-virtual {p0, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 151
    const-string v0, "GCoreNlp"

    const/4 v1, 0x4

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 152
    const-string v0, "GCoreNlp"

    const-string v1, "!shouldConfirmNlp, but user has LGAAYL off"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 124
    :catchall_0
    move-exception v0

    monitor-exit v8

    throw v0

    .line 158
    :cond_3
    if-eqz v0, :cond_6

    .line 160
    :try_start_2
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/google/android/gsf/e;->a:Landroid/net/Uri;

    const/4 v2, 0x0

    const-string v3, "(name=?)"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    const-string v9, "network_location_opt_in"

    aput-object v9, v4, v5

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 166
    if-nez v0, :cond_4

    .line 167
    const-string v0, "GCoreNlp"

    const-string v1, "applySettings(): provider not available"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 171
    :cond_4
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 174
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "network_location_opt_in"

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/google/android/gsf/e;->b(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    if-eqz v0, :cond_5

    move v0, v6

    :goto_1
    if-nez v0, :cond_0

    .line 176
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "network"

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$Secure;->setLocationProviderEnabled(Landroid/content/ContentResolver;Ljava/lang/String;Z)V

    .line 180
    invoke-static {p0}, Lcom/google/android/location/network/NetworkLocationService;->c(Landroid/content/Context;)Z

    move-result v0

    .line 183
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/google/android/location/network/ConfirmAlertActivity;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 184
    const-string v2, "confirmLgaayl"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 185
    const/high16 v0, 0x10000000

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 186
    invoke-virtual {p0, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 188
    const-string v0, "GCoreNlp"

    const/4 v1, 0x4

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 189
    const-string v0, "GCoreNlp"

    const-string v1, "shouldConfirmNlp, asking to opt in"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    :cond_5
    move v0, v7

    .line 174
    goto :goto_1

    .line 194
    :cond_6
    const-string v0, "GCoreNlp"

    const/4 v1, 0x4

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 195
    const-string v0, "GCoreNlp"

    const-string v1, "shouldConfirmNlp, NLP off. Ensuring opt-in disabled"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 197
    :cond_7
    const/4 v0, 0x0

    invoke-static {p0, v0}, Lcom/google/android/location/network/NetworkLocationService;->a(Landroid/content/Context;Z)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_0
.end method

.method static b(Landroid/content/Context;Z)V
    .locals 2

    .prologue
    .line 240
    invoke-static {p0, p1}, Lcom/google/android/location/network/NetworkLocationService;->a(Landroid/content/Context;Z)V

    .line 244
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "network"

    invoke-static {v0, v1, p1}, Landroid/provider/Settings$Secure;->setLocationProviderEnabled(Landroid/content/ContentResolver;Ljava/lang/String;Z)V

    .line 246
    return-void
.end method

.method private static c(Landroid/content/Context;)Z
    .locals 1

    .prologue
    .line 226
    invoke-static {p0}, Lcom/google/android/gms/common/util/y;->c(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p0}, Lcom/google/android/gms/common/util/y;->a(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 2

    .prologue
    .line 51
    iget-object v0, p0, Lcom/google/android/location/network/NetworkLocationService;->a:Lcom/google/android/location/network/NetworkLocationProvider;

    if-nez v0, :cond_0

    .line 52
    new-instance v0, Lcom/google/android/location/network/NetworkLocationProvider;

    invoke-virtual {p0}, Lcom/google/android/location/network/NetworkLocationService;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/location/network/NetworkLocationProvider;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/location/network/NetworkLocationService;->a:Lcom/google/android/location/network/NetworkLocationProvider;

    .line 60
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/location/network/NetworkLocationService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/location/network/NetworkLocationService;->a(Landroid/content/Context;)V

    .line 65
    iget-object v0, p0, Lcom/google/android/location/network/NetworkLocationService;->a:Lcom/google/android/location/network/NetworkLocationProvider;

    invoke-virtual {v0}, Lcom/google/android/location/network/NetworkLocationProvider;->getBinder()Landroid/os/IBinder;

    move-result-object v0

    return-object v0
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 78
    invoke-super {p0}, Landroid/app/IntentService;->onDestroy()V

    .line 79
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/location/network/NetworkLocationService;->a:Lcom/google/android/location/network/NetworkLocationProvider;

    .line 80
    return-void
.end method

.method protected onHandleIntent(Landroid/content/Intent;)V
    .locals 3

    .prologue
    .line 88
    invoke-static {p1}, Lcom/google/android/location/clientlib/c;->a(Landroid/content/Intent;)Lcom/google/android/location/clientlib/NlpLocation;

    move-result-object v0

    .line 91
    iget-object v1, p0, Lcom/google/android/location/network/NetworkLocationService;->a:Lcom/google/android/location/network/NetworkLocationProvider;

    if-eqz v1, :cond_0

    if-eqz v0, :cond_0

    iget-object v0, v0, Lcom/google/android/location/clientlib/NlpLocation;->a:Landroid/location/Location;

    if-eqz v0, :cond_0

    .line 94
    const-string v1, "noGPSLocation"

    new-instance v2, Landroid/location/Location;

    invoke-direct {v2, v0}, Landroid/location/Location;-><init>(Landroid/location/Location;)V

    invoke-static {v0, v1, v2}, Lcom/google/android/location/n/z;->a(Landroid/location/Location;Ljava/lang/String;Landroid/location/Location;)V

    .line 98
    iget-object v1, p0, Lcom/google/android/location/network/NetworkLocationService;->a:Lcom/google/android/location/network/NetworkLocationProvider;

    invoke-virtual {v1, v0}, Lcom/google/android/location/network/NetworkLocationProvider;->reportLocation(Landroid/location/Location;)V

    .line 100
    :cond_0
    return-void
.end method

.method public onUnbind(Landroid/content/Intent;)Z
    .locals 1

    .prologue
    .line 70
    iget-object v0, p0, Lcom/google/android/location/network/NetworkLocationService;->a:Lcom/google/android/location/network/NetworkLocationProvider;

    if-eqz v0, :cond_0

    .line 71
    iget-object v0, p0, Lcom/google/android/location/network/NetworkLocationService;->a:Lcom/google/android/location/network/NetworkLocationProvider;

    invoke-virtual {v0}, Lcom/google/android/location/network/NetworkLocationProvider;->onDisable()V

    .line 73
    :cond_0
    const/4 v0, 0x0

    return v0
.end method
