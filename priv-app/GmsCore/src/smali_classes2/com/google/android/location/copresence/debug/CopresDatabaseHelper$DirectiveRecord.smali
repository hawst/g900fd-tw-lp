.class public Lcom/google/android/location/copresence/debug/CopresDatabaseHelper$DirectiveRecord;
.super Lcom/google/android/location/copresence/debug/a;
.source "SourceFile"


# instance fields
.field public accountName:Ljava/lang/String;
    .annotation runtime Lcom/google/android/location/copresence/debug/b;
        a = false
    .end annotation
.end field

.field public cachedDirectiveCounter:Ljava/lang/Integer;
    .annotation runtime Lcom/google/android/location/copresence/debug/b;
        a = false
    .end annotation
.end field

.field public delayMillis:Ljava/lang/Long;
    .annotation runtime Lcom/google/android/location/copresence/debug/b;
        a = true
        c = "0"
    .end annotation
.end field

.field public gcmPingRowId:Ljava/lang/Long;
    .annotation runtime Lcom/google/android/location/copresence/debug/b;
        a = false
    .end annotation
.end field

.field public instructionType:Ljava/lang/Integer;
    .annotation runtime Lcom/google/android/location/copresence/debug/b;
        a = true
        c = "0"
    .end annotation
.end field

.field public isBeingRemoved:Ljava/lang/Boolean;
    .annotation runtime Lcom/google/android/location/copresence/debug/b;
        a = false
    .end annotation
.end field

.field public medium:Ljava/lang/Integer;
    .annotation runtime Lcom/google/android/location/copresence/debug/b;
        a = false
    .end annotation
.end field

.field public publishedMessageIds:Ljava/lang/String;
    .annotation runtime Lcom/google/android/location/copresence/debug/b;
        a = false
    .end annotation
.end field

.field public subscriptionIds:Ljava/lang/String;
    .annotation runtime Lcom/google/android/location/copresence/debug/b;
        a = false
    .end annotation
.end field

.field public timestamp:Ljava/lang/Long;
    .annotation runtime Lcom/google/android/location/copresence/debug/b;
        a = true
        d = 0x2
    .end annotation
.end field

.field public tokenId:Ljava/lang/String;
    .annotation runtime Lcom/google/android/location/copresence/debug/b;
        a = false
    .end annotation
.end field

.field public tokenInstructionType:Ljava/lang/Integer;
    .annotation runtime Lcom/google/android/location/copresence/debug/b;
        a = false
    .end annotation
.end field

.field public ttlMillis:Ljava/lang/Long;
    .annotation runtime Lcom/google/android/location/copresence/debug/b;
        a = true
        c = "0"
    .end annotation
.end field


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 452
    invoke-direct {p0}, Lcom/google/android/location/copresence/debug/a;-><init>()V

    .line 453
    return-void
.end method

.method public constructor <init>(Lcom/google/ac/b/c/ah;Lcom/google/android/location/copresence/debug/CopresDatabaseHelper$GcmPingRecord;Lcom/google/android/location/copresence/a/a;ZI)V
    .locals 2

    .prologue
    .line 458
    invoke-direct {p0}, Lcom/google/android/location/copresence/debug/CopresDatabaseHelper$DirectiveRecord;-><init>()V

    .line 460
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/copresence/debug/CopresDatabaseHelper$DirectiveRecord;->timestamp:Ljava/lang/Long;

    .line 461
    if-eqz p3, :cond_0

    .line 462
    iget-object v0, p3, Lcom/google/android/location/copresence/a/a;->a:Landroid/accounts/Account;

    iget-object v0, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/location/copresence/debug/CopresDatabaseHelper$DirectiveRecord;->accountName:Ljava/lang/String;

    .line 465
    :cond_0
    iget-object v0, p1, Lcom/google/ac/b/c/ah;->a:Ljava/lang/Integer;

    iput-object v0, p0, Lcom/google/android/location/copresence/debug/CopresDatabaseHelper$DirectiveRecord;->instructionType:Ljava/lang/Integer;

    .line 466
    iget-object v0, p1, Lcom/google/ac/b/c/ah;->c:Ljava/lang/Long;

    iput-object v0, p0, Lcom/google/android/location/copresence/debug/CopresDatabaseHelper$DirectiveRecord;->delayMillis:Ljava/lang/Long;

    .line 467
    iget-object v0, p1, Lcom/google/ac/b/c/ah;->d:Ljava/lang/Long;

    iput-object v0, p0, Lcom/google/android/location/copresence/debug/CopresDatabaseHelper$DirectiveRecord;->ttlMillis:Ljava/lang/Long;

    .line 468
    iget-object v0, p1, Lcom/google/ac/b/c/ah;->e:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/location/copresence/debug/CopresDatabaseHelper$DirectiveRecord;->publishedMessageIds:Ljava/lang/String;

    .line 469
    iget-object v0, p1, Lcom/google/ac/b/c/ah;->f:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/location/copresence/debug/CopresDatabaseHelper$DirectiveRecord;->subscriptionIds:Ljava/lang/String;

    .line 470
    invoke-static {p4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/copresence/debug/CopresDatabaseHelper$DirectiveRecord;->isBeingRemoved:Ljava/lang/Boolean;

    .line 471
    invoke-static {p5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/copresence/debug/CopresDatabaseHelper$DirectiveRecord;->cachedDirectiveCounter:Ljava/lang/Integer;

    .line 473
    iget-object v0, p1, Lcom/google/ac/b/c/ah;->b:Lcom/google/ac/b/c/bt;

    if-eqz v0, :cond_1

    .line 474
    iget-object v0, p1, Lcom/google/ac/b/c/ah;->b:Lcom/google/ac/b/c/bt;

    iget-object v0, v0, Lcom/google/ac/b/c/bt;->a:Ljava/lang/Integer;

    iput-object v0, p0, Lcom/google/android/location/copresence/debug/CopresDatabaseHelper$DirectiveRecord;->tokenInstructionType:Ljava/lang/Integer;

    .line 475
    iget-object v0, p1, Lcom/google/ac/b/c/ah;->b:Lcom/google/ac/b/c/bt;

    iget-object v0, v0, Lcom/google/ac/b/c/bt;->b:Ljava/lang/Integer;

    iput-object v0, p0, Lcom/google/android/location/copresence/debug/CopresDatabaseHelper$DirectiveRecord;->medium:Ljava/lang/Integer;

    .line 476
    iget-object v0, p1, Lcom/google/ac/b/c/ah;->b:Lcom/google/ac/b/c/bt;

    iget-object v0, v0, Lcom/google/ac/b/c/bt;->c:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/location/copresence/debug/CopresDatabaseHelper$DirectiveRecord;->tokenId:Ljava/lang/String;

    .line 479
    :cond_1
    if-eqz p2, :cond_2

    .line 480
    invoke-virtual {p2}, Lcom/google/android/location/copresence/debug/CopresDatabaseHelper$GcmPingRecord;->isNewRecord()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 481
    const/4 v0, 0x6

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 482
    const-string v0, "GcmPingRecord needs to be saved, so we have a row ID."

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->e(Ljava/lang/String;)I

    .line 488
    :cond_2
    :goto_0
    return-void

    .line 485
    :cond_3
    invoke-virtual {p2}, Lcom/google/android/location/copresence/debug/CopresDatabaseHelper$GcmPingRecord;->getRowId()Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/copresence/debug/CopresDatabaseHelper$DirectiveRecord;->gcmPingRowId:Ljava/lang/Long;

    goto :goto_0
.end method
