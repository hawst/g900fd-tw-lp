.class public final Lcom/google/android/location/places/h;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/v/b/n;


# instance fields
.field public a:Lcom/google/android/gms/location/places/internal/PlacesParams;

.field public b:Lcom/google/android/location/places/b;

.field public c:Ljava/lang/Exception;

.field public d:Lcom/google/v/b/o;

.field public e:Lcom/google/android/location/places/o;

.field public f:Ljava/lang/Object;

.field public g:J

.field final synthetic h:Lcom/google/android/location/places/f;


# direct methods
.method public constructor <init>(Lcom/google/android/location/places/f;Lcom/google/android/gms/location/places/internal/PlacesParams;Lcom/google/android/location/places/b;Lcom/google/android/location/places/o;Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 395
    iput-object p1, p0, Lcom/google/android/location/places/h;->h:Lcom/google/android/location/places/f;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 396
    iput-object p2, p0, Lcom/google/android/location/places/h;->a:Lcom/google/android/gms/location/places/internal/PlacesParams;

    .line 397
    iput-object p3, p0, Lcom/google/android/location/places/h;->b:Lcom/google/android/location/places/b;

    .line 398
    iput-object p4, p0, Lcom/google/android/location/places/h;->e:Lcom/google/android/location/places/o;

    .line 399
    iput-object p5, p0, Lcom/google/android/location/places/h;->f:Ljava/lang/Object;

    .line 400
    iget-object v0, p1, Lcom/google/android/location/places/f;->c:Lcom/google/android/location/n/ai;

    invoke-virtual {v0}, Lcom/google/android/location/n/ai;->a()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/location/places/h;->g:J

    .line 401
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/v/b/m;Lcom/google/v/b/o;)V
    .locals 2

    .prologue
    .line 421
    iput-object p2, p0, Lcom/google/android/location/places/h;->d:Lcom/google/v/b/o;

    .line 422
    iget-object v0, p0, Lcom/google/android/location/places/h;->h:Lcom/google/android/location/places/f;

    iget-object v0, v0, Lcom/google/android/location/places/f;->a:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/location/places/j;

    invoke-direct {v1, p0}, Lcom/google/android/location/places/j;-><init>(Lcom/google/android/location/places/h;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 428
    return-void
.end method

.method public final a(Lcom/google/v/b/m;Ljava/lang/Exception;)V
    .locals 2

    .prologue
    .line 405
    iput-object p2, p0, Lcom/google/android/location/places/h;->c:Ljava/lang/Exception;

    .line 406
    const-string v0, "Places"

    const/4 v1, 0x6

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 407
    const-string v0, "Places"

    const-string v1, "masf request failed"

    invoke-static {v0, v1, p2}, Lcom/google/android/location/n/aa;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 409
    :cond_0
    iget-object v0, p0, Lcom/google/android/location/places/h;->h:Lcom/google/android/location/places/f;

    iget-object v0, v0, Lcom/google/android/location/places/f;->a:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/location/places/i;

    invoke-direct {v1, p0}, Lcom/google/android/location/places/i;-><init>(Lcom/google/android/location/places/h;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 417
    return-void
.end method
