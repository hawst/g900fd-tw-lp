.class final Lcom/google/android/location/geofencer/service/aj;
.super Landroid/hardware/location/GeofenceHardwareCallback;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/location/geofencer/service/ai;

.field private final b:Ljava/lang/Object;

.field private c:Landroid/location/Location;

.field private volatile d:Ljava/util/concurrent/CountDownLatch;

.field private volatile e:Ljava/util/concurrent/CountDownLatch;

.field private final f:Landroid/util/SparseIntArray;


# direct methods
.method constructor <init>(Lcom/google/android/location/geofencer/service/ai;)V
    .locals 2

    .prologue
    .line 382
    iput-object p1, p0, Lcom/google/android/location/geofencer/service/aj;->a:Lcom/google/android/location/geofencer/service/ai;

    invoke-direct {p0}, Landroid/hardware/location/GeofenceHardwareCallback;-><init>()V

    .line 388
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/location/geofencer/service/aj;->b:Ljava/lang/Object;

    .line 391
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/location/geofencer/service/aj;->c:Landroid/location/Location;

    .line 395
    new-instance v0, Landroid/util/SparseIntArray;

    const/16 v1, 0x32

    invoke-direct {v0, v1}, Landroid/util/SparseIntArray;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/location/geofencer/service/aj;->f:Landroid/util/SparseIntArray;

    return-void
.end method


# virtual methods
.method public final a()Z
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 433
    iget-object v1, p0, Lcom/google/android/location/geofencer/service/aj;->d:Ljava/util/concurrent/CountDownLatch;

    const-wide/16 v2, 0x2710

    sget-object v4, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v1, v2, v3, v4}, Ljava/util/concurrent/CountDownLatch;->await(JLjava/util/concurrent/TimeUnit;)Z

    move-result v1

    .line 434
    if-nez v1, :cond_1

    .line 436
    sget-boolean v1, Lcom/google/android/location/geofencer/a/a;->a:Z

    if-eqz v1, :cond_0

    .line 437
    const-string v1, "GeofenceHardware"

    const-string v2, "waitForAddGeofenceStatus: time out."

    invoke-static {v1, v2}, Lcom/google/android/location/geofencer/a/a;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 439
    :cond_0
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/location/geofencer/service/aj;->d:Ljava/util/concurrent/CountDownLatch;

    .line 449
    :goto_0
    return v0

    .line 442
    :cond_1
    iget-object v2, p0, Lcom/google/android/location/geofencer/service/aj;->f:Landroid/util/SparseIntArray;

    monitor-enter v2

    move v1, v0

    .line 443
    :goto_1
    :try_start_0
    iget-object v3, p0, Lcom/google/android/location/geofencer/service/aj;->f:Landroid/util/SparseIntArray;

    invoke-virtual {v3}, Landroid/util/SparseIntArray;->size()I

    move-result v3

    if-ge v1, v3, :cond_3

    .line 444
    iget-object v3, p0, Lcom/google/android/location/geofencer/service/aj;->f:Landroid/util/SparseIntArray;

    invoke-virtual {v3, v1}, Landroid/util/SparseIntArray;->valueAt(I)I

    move-result v3

    if-eqz v3, :cond_2

    .line 445
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 448
    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0

    .line 443
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 448
    :cond_3
    monitor-exit v2

    .line 449
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final a(I)Z
    .locals 4

    .prologue
    .line 404
    iget-object v0, p0, Lcom/google/android/location/geofencer/service/aj;->d:Ljava/util/concurrent/CountDownLatch;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/location/geofencer/service/aj;->d:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->getCount()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    .line 405
    const-string v0, "GeofenceHardware"

    const-string v1, "Ongoing add geofence operation."

    invoke-static {v0, v1}, Lcom/google/android/location/geofencer/a/a;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 406
    const/4 v0, 0x0

    .line 410
    :goto_0
    return v0

    .line 408
    :cond_0
    new-instance v0, Ljava/util/concurrent/CountDownLatch;

    invoke-direct {v0, p1}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/location/geofencer/service/aj;->d:Ljava/util/concurrent/CountDownLatch;

    .line 409
    iget-object v0, p0, Lcom/google/android/location/geofencer/service/aj;->f:Landroid/util/SparseIntArray;

    invoke-virtual {v0}, Landroid/util/SparseIntArray;->clear()V

    .line 410
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final b(I)V
    .locals 3

    .prologue
    .line 419
    iget-object v1, p0, Lcom/google/android/location/geofencer/service/aj;->f:Landroid/util/SparseIntArray;

    monitor-enter v1

    .line 420
    :try_start_0
    iget-object v0, p0, Lcom/google/android/location/geofencer/service/aj;->f:Landroid/util/SparseIntArray;

    const/4 v2, 0x5

    invoke-virtual {v0, p1, v2}, Landroid/util/SparseIntArray;->put(II)V

    .line 421
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 422
    iget-object v0, p0, Lcom/google/android/location/geofencer/service/aj;->d:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    .line 423
    return-void

    .line 421
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final b()Z
    .locals 4

    .prologue
    .line 458
    iget-object v0, p0, Lcom/google/android/location/geofencer/service/aj;->e:Ljava/util/concurrent/CountDownLatch;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/location/geofencer/service/aj;->e:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->getCount()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    .line 459
    const-string v0, "GeofenceHardware"

    const-string v1, "Ongoing remove geofence operation."

    invoke-static {v0, v1}, Lcom/google/android/location/geofencer/a/a;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 460
    const/4 v0, 0x0

    .line 463
    :goto_0
    return v0

    .line 462
    :cond_0
    new-instance v0, Ljava/util/concurrent/CountDownLatch;

    const/16 v1, 0x32

    invoke-direct {v0, v1}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/location/geofencer/service/aj;->e:Ljava/util/concurrent/CountDownLatch;

    .line 463
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 471
    iget-object v0, p0, Lcom/google/android/location/geofencer/service/aj;->e:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    .line 472
    return-void
.end method

.method public final d()V
    .locals 4

    .prologue
    .line 479
    iget-object v0, p0, Lcom/google/android/location/geofencer/service/aj;->e:Ljava/util/concurrent/CountDownLatch;

    const-wide/16 v2, 0x2710

    sget-object v1, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v2, v3, v1}, Ljava/util/concurrent/CountDownLatch;->await(JLjava/util/concurrent/TimeUnit;)Z

    move-result v0

    .line 481
    sget-boolean v1, Lcom/google/android/location/geofencer/a/a;->a:Z

    if-eqz v1, :cond_0

    if-nez v0, :cond_0

    .line 482
    const-string v0, "GeofenceHardware"

    const-string v1, "waitForRemoveGeofenceStatus: time out."

    invoke-static {v0, v1}, Lcom/google/android/location/geofencer/a/a;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 484
    :cond_0
    return-void
.end method

.method public final onGeofenceAdd(II)V
    .locals 2

    .prologue
    .line 488
    invoke-super {p0, p1, p2}, Landroid/hardware/location/GeofenceHardwareCallback;->onGeofenceAdd(II)V

    .line 489
    iget-object v1, p0, Lcom/google/android/location/geofencer/service/aj;->f:Landroid/util/SparseIntArray;

    monitor-enter v1

    .line 490
    :try_start_0
    iget-object v0, p0, Lcom/google/android/location/geofencer/service/aj;->f:Landroid/util/SparseIntArray;

    invoke-virtual {v0, p1, p2}, Landroid/util/SparseIntArray;->put(II)V

    .line 491
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 492
    iget-object v0, p0, Lcom/google/android/location/geofencer/service/aj;->d:Ljava/util/concurrent/CountDownLatch;

    if-eqz v0, :cond_0

    .line 493
    iget-object v0, p0, Lcom/google/android/location/geofencer/service/aj;->d:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    .line 495
    :cond_0
    return-void

    .line 491
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final onGeofenceRemove(II)V
    .locals 3

    .prologue
    .line 499
    invoke-super {p0, p1, p2}, Landroid/hardware/location/GeofenceHardwareCallback;->onGeofenceRemove(II)V

    .line 500
    if-eqz p2, :cond_0

    sget-boolean v0, Lcom/google/android/location/geofencer/a/a;->a:Z

    if-eqz v0, :cond_0

    .line 501
    const-string v0, "GeofenceHardware"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Failed to remove geofence "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/location/geofencer/a/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 503
    :cond_0
    iget-object v0, p0, Lcom/google/android/location/geofencer/service/aj;->e:Ljava/util/concurrent/CountDownLatch;

    if-eqz v0, :cond_1

    .line 504
    iget-object v0, p0, Lcom/google/android/location/geofencer/service/aj;->e:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    .line 506
    :cond_1
    return-void
.end method

.method public final onGeofenceTransition(IILandroid/location/Location;JI)V
    .locals 8

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    const-wide v6, 0x3ee4f8b580000000L    # 9.999999747378752E-6

    .line 528
    invoke-super/range {p0 .. p6}, Landroid/hardware/location/GeofenceHardwareCallback;->onGeofenceTransition(IILandroid/location/Location;JI)V

    .line 529
    iget-object v2, p0, Lcom/google/android/location/geofencer/service/aj;->b:Ljava/lang/Object;

    monitor-enter v2

    .line 530
    if-eqz p3, :cond_4

    :try_start_0
    invoke-virtual {p3}, Landroid/location/Location;->getLatitude()D

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Math;->abs(D)D

    move-result-wide v4

    cmpg-double v3, v4, v6

    if-gtz v3, :cond_0

    invoke-virtual {p3}, Landroid/location/Location;->getLongitude()D

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Math;->abs(D)D

    move-result-wide v4

    cmpg-double v3, v4, v6

    if-lez v3, :cond_4

    :cond_0
    :goto_0
    if-eqz v0, :cond_5

    .line 534
    iget-object v0, p0, Lcom/google/android/location/geofencer/service/aj;->c:Landroid/location/Location;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/location/geofencer/service/aj;->c:Landroid/location/Location;

    invoke-virtual {v0}, Landroid/location/Location;->getTime()J

    move-result-wide v0

    invoke-virtual {p3}, Landroid/location/Location;->getTime()J

    move-result-wide v4

    cmp-long v0, v0, v4

    if-eqz v0, :cond_2

    .line 536
    :cond_1
    iput-object p3, p0, Lcom/google/android/location/geofencer/service/aj;->c:Landroid/location/Location;

    .line 537
    iget-object v0, p0, Lcom/google/android/location/geofencer/service/aj;->a:Lcom/google/android/location/geofencer/service/ai;

    iget-object v0, v0, Lcom/google/android/location/geofencer/service/ai;->d:Lcom/google/android/location/geofencer/service/k;

    invoke-virtual {v0, p3}, Lcom/google/android/location/geofencer/service/k;->a(Landroid/location/Location;)V

    .line 542
    :cond_2
    :goto_1
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 543
    sget-boolean v0, Lcom/google/android/location/geofencer/a/a;->a:Z

    if-eqz v0, :cond_3

    .line 545
    packed-switch p2, :pswitch_data_0

    .line 556
    :pswitch_0
    const-string v0, "unknown"

    move-object v1, v0

    .line 559
    :goto_2
    iget-object v0, p0, Lcom/google/android/location/geofencer/service/aj;->a:Lcom/google/android/location/geofencer/service/ai;

    iget-object v2, v0, Lcom/google/android/location/geofencer/service/ai;->c:Ljava/util/ArrayList;

    monitor-enter v2

    .line 560
    :try_start_1
    iget-object v0, p0, Lcom/google/android/location/geofencer/service/aj;->a:Lcom/google/android/location/geofencer/service/ai;

    iget-object v0, v0, Lcom/google/android/location/geofencer/service/ai;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/geofencer/data/g;

    .line 561
    sget-object v3, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v4, "onGeofenceTransition: geofenceState = %s, geofenceId=%d, transition=%s location=%s"

    const/4 v5, 0x4

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object v0, v5, v6

    const/4 v0, 0x1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v0

    const/4 v0, 0x2

    aput-object v1, v5, v0

    const/4 v0, 0x3

    aput-object p3, v5, v0

    invoke-static {v3, v4, v5}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 565
    const-string v1, "GeofenceHardware"

    invoke-static {v1, v0}, Lcom/google/android/location/geofencer/a/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 566
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 568
    :cond_3
    return-void

    :cond_4
    move v0, v1

    .line 530
    goto :goto_0

    .line 539
    :cond_5
    :try_start_2
    const-string v0, "GeofenceHardware"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 540
    const-string v0, "GeofenceHardware"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "Ignored invalid location:"

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    .line 542
    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0

    .line 547
    :pswitch_1
    const-string v0, "enter"

    move-object v1, v0

    .line 548
    goto :goto_2

    .line 550
    :pswitch_2
    const-string v0, "exit"

    move-object v1, v0

    .line 551
    goto :goto_2

    .line 553
    :pswitch_3
    const-string v0, "uncertain"

    move-object v1, v0

    .line 554
    goto :goto_2

    .line 566
    :catchall_1
    move-exception v0

    monitor-exit v2

    throw v0

    .line 545
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method
