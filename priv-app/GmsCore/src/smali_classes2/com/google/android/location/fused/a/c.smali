.class public final Lcom/google/android/location/fused/a/c;
.super Lcom/google/android/location/fused/a/m;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/android/location/fused/a/f;

.field private final b:Lcom/google/android/location/fused/a/d;

.field private c:Lcom/google/android/location/fused/a/m;


# direct methods
.method public constructor <init>(Lcom/google/android/location/fused/a/f;Lcom/google/android/location/fused/a/d;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 21
    invoke-direct {p0}, Lcom/google/android/location/fused/a/m;-><init>()V

    .line 22
    iput-object p1, p0, Lcom/google/android/location/fused/a/c;->a:Lcom/google/android/location/fused/a/f;

    .line 23
    iput-object p2, p0, Lcom/google/android/location/fused/a/c;->b:Lcom/google/android/location/fused/a/d;

    .line 24
    iput-object p1, p0, Lcom/google/android/location/fused/a/c;->c:Lcom/google/android/location/fused/a/m;

    .line 25
    invoke-virtual {p2, v2}, Lcom/google/android/location/fused/a/d;->a(Z)V

    .line 28
    iget-object v0, p0, Lcom/google/android/location/fused/a/c;->b:Lcom/google/android/location/fused/a/d;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/location/fused/a/d;->c_(Z)V

    .line 29
    iget-object v0, p0, Lcom/google/android/location/fused/a/c;->b:Lcom/google/android/location/fused/a/d;

    invoke-virtual {v0, v2}, Lcom/google/android/location/fused/a/d;->b(Z)V

    .line 30
    iget-object v0, p0, Lcom/google/android/location/fused/a/c;->b:Lcom/google/android/location/fused/a/d;

    invoke-virtual {v0}, Lcom/google/android/location/fused/a/d;->b()V

    .line 31
    return-void
.end method

.method private b()Z
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 63
    const/16 v0, 0x15

    invoke-static {v0}, Lcom/google/android/gms/common/util/ao;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/location/fused/a/c;->b:Lcom/google/android/location/fused/a/d;

    iget-object v0, v0, Lcom/google/android/location/fused/a/d;->b:Lcom/google/android/location/fused/service/a;

    invoke-virtual {v0}, Lcom/google/android/location/fused/service/a;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/google/android/location/x;->D:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    move v0, v2

    :goto_0
    if-eqz v0, :cond_0

    move v0, v1

    :goto_1
    return v0

    :pswitch_0
    move v0, v1

    goto :goto_0

    :cond_0
    move v0, v2

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method


# virtual methods
.method protected final a()V
    .locals 4

    .prologue
    .line 36
    invoke-direct {p0}, Lcom/google/android/location/fused/a/c;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 37
    iget-object v0, p0, Lcom/google/android/location/fused/a/c;->b:Lcom/google/android/location/fused/a/d;

    .line 44
    :goto_0
    iget-object v1, p0, Lcom/google/android/location/fused/a/c;->c:Lcom/google/android/location/fused/a/m;

    if-eq v0, v1, :cond_0

    .line 45
    iget-object v1, p0, Lcom/google/android/location/fused/a/c;->c:Lcom/google/android/location/fused/a/m;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/google/android/location/fused/a/m;->a(Z)V

    .line 46
    iget-object v1, p0, Lcom/google/android/location/fused/a/c;->c:Lcom/google/android/location/fused/a/m;

    invoke-virtual {v1}, Lcom/google/android/location/fused/a/m;->f()V

    .line 47
    iget-boolean v1, p0, Lcom/google/android/location/fused/a/q;->i:Z

    invoke-virtual {v0, v1}, Lcom/google/android/location/fused/a/m;->a(Z)V

    .line 50
    :cond_0
    iget-boolean v1, p0, Lcom/google/android/location/fused/a/q;->i:Z

    invoke-virtual {v0, v1}, Lcom/google/android/location/fused/a/m;->a(Z)V

    .line 51
    iget-wide v2, p0, Lcom/google/android/location/fused/a/m;->f:J

    invoke-virtual {v0, v2, v3}, Lcom/google/android/location/fused/a/m;->a(J)V

    .line 52
    iget-object v1, p0, Lcom/google/android/location/fused/a/q;->j:Ljava/util/Collection;

    invoke-virtual {v0, v1}, Lcom/google/android/location/fused/a/m;->a(Ljava/util/Collection;)V

    .line 53
    iget-boolean v1, p0, Lcom/google/android/location/fused/a/m;->g:Z

    iput-boolean v1, v0, Lcom/google/android/location/fused/a/m;->g:Z

    .line 54
    iget-boolean v1, p0, Lcom/google/android/location/fused/a/q;->h:Z

    if-eqz v1, :cond_2

    .line 55
    invoke-virtual {v0}, Lcom/google/android/location/fused/a/m;->e()V

    .line 59
    :goto_1
    return-void

    .line 39
    :cond_1
    iget-object v0, p0, Lcom/google/android/location/fused/a/c;->a:Lcom/google/android/location/fused/a/f;

    goto :goto_0

    .line 57
    :cond_2
    invoke-virtual {v0}, Lcom/google/android/location/fused/a/m;->f()V

    goto :goto_1
.end method

.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 80
    invoke-direct {p0}, Lcom/google/android/location/fused/a/c;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 81
    iget-object v0, p0, Lcom/google/android/location/fused/a/c;->b:Lcom/google/android/location/fused/a/d;

    invoke-virtual {v0}, Lcom/google/android/location/fused/a/d;->toString()Ljava/lang/String;

    move-result-object v0

    .line 84
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/location/fused/a/c;->a:Lcom/google/android/location/fused/a/f;

    invoke-virtual {v0}, Lcom/google/android/location/fused/a/f;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
