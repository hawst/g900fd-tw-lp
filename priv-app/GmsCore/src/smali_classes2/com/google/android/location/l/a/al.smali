.class public final Lcom/google/android/location/l/a/al;
.super Lcom/google/protobuf/nano/d;
.source "SourceFile"


# static fields
.field private static volatile r:[Lcom/google/android/location/l/a/al;


# instance fields
.field public a:Lcom/google/android/location/l/a/h;

.field public b:Lcom/google/android/location/l/a/cu;

.field public c:Lcom/google/android/location/l/a/am;

.field public d:Lcom/google/android/location/l/a/w;

.field public e:Lcom/google/android/location/l/a/aq;

.field public f:[I

.field public g:[[B

.field public h:Lcom/google/android/location/l/a/af;

.field public i:Lcom/google/android/location/l/a/cc;

.field public j:Ljava/lang/Integer;

.field public k:[Lcom/google/android/location/l/a/cg;

.field public l:Lcom/google/android/location/l/a/cm;

.field public m:Ljava/lang/Long;

.field public n:Lcom/google/android/location/l/a/bm;

.field public o:Lcom/google/android/location/l/a/da;

.field public p:Lcom/google/android/location/l/a/bx;

.field public q:Lcom/google/android/location/l/a/q;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 82
    invoke-direct {p0}, Lcom/google/protobuf/nano/d;-><init>()V

    .line 83
    iput-object v1, p0, Lcom/google/android/location/l/a/al;->a:Lcom/google/android/location/l/a/h;

    iput-object v1, p0, Lcom/google/android/location/l/a/al;->b:Lcom/google/android/location/l/a/cu;

    iput-object v1, p0, Lcom/google/android/location/l/a/al;->c:Lcom/google/android/location/l/a/am;

    iput-object v1, p0, Lcom/google/android/location/l/a/al;->d:Lcom/google/android/location/l/a/w;

    iput-object v1, p0, Lcom/google/android/location/l/a/al;->e:Lcom/google/android/location/l/a/aq;

    sget-object v0, Lcom/google/protobuf/nano/m;->a:[I

    iput-object v0, p0, Lcom/google/android/location/l/a/al;->f:[I

    sget-object v0, Lcom/google/protobuf/nano/m;->g:[[B

    iput-object v0, p0, Lcom/google/android/location/l/a/al;->g:[[B

    iput-object v1, p0, Lcom/google/android/location/l/a/al;->h:Lcom/google/android/location/l/a/af;

    iput-object v1, p0, Lcom/google/android/location/l/a/al;->i:Lcom/google/android/location/l/a/cc;

    iput-object v1, p0, Lcom/google/android/location/l/a/al;->j:Ljava/lang/Integer;

    invoke-static {}, Lcom/google/android/location/l/a/cg;->a()[Lcom/google/android/location/l/a/cg;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/l/a/al;->k:[Lcom/google/android/location/l/a/cg;

    iput-object v1, p0, Lcom/google/android/location/l/a/al;->l:Lcom/google/android/location/l/a/cm;

    iput-object v1, p0, Lcom/google/android/location/l/a/al;->m:Ljava/lang/Long;

    iput-object v1, p0, Lcom/google/android/location/l/a/al;->n:Lcom/google/android/location/l/a/bm;

    iput-object v1, p0, Lcom/google/android/location/l/a/al;->o:Lcom/google/android/location/l/a/da;

    iput-object v1, p0, Lcom/google/android/location/l/a/al;->p:Lcom/google/android/location/l/a/bx;

    iput-object v1, p0, Lcom/google/android/location/l/a/al;->q:Lcom/google/android/location/l/a/q;

    iput-object v1, p0, Lcom/google/android/location/l/a/al;->unknownFieldData:Lcom/google/protobuf/nano/f;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/location/l/a/al;->cachedSize:I

    .line 84
    return-void
.end method

.method public static a()[Lcom/google/android/location/l/a/al;
    .locals 2

    .prologue
    .line 20
    sget-object v0, Lcom/google/android/location/l/a/al;->r:[Lcom/google/android/location/l/a/al;

    if-nez v0, :cond_1

    .line 21
    sget-object v1, Lcom/google/protobuf/nano/h;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 23
    :try_start_0
    sget-object v0, Lcom/google/android/location/l/a/al;->r:[Lcom/google/android/location/l/a/al;

    if-nez v0, :cond_0

    .line 24
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/android/location/l/a/al;

    sput-object v0, Lcom/google/android/location/l/a/al;->r:[Lcom/google/android/location/l/a/al;

    .line 26
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 28
    :cond_1
    sget-object v0, Lcom/google/android/location/l/a/al;->r:[Lcom/google/android/location/l/a/al;

    return-object v0

    .line 26
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 367
    invoke-super {p0}, Lcom/google/protobuf/nano/d;->computeSerializedSize()I

    move-result v0

    .line 368
    iget-object v1, p0, Lcom/google/android/location/l/a/al;->a:Lcom/google/android/location/l/a/h;

    if-eqz v1, :cond_0

    .line 369
    const/4 v1, 0x1

    iget-object v3, p0, Lcom/google/android/location/l/a/al;->a:Lcom/google/android/location/l/a/h;

    invoke-static {v1, v3}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 372
    :cond_0
    iget-object v1, p0, Lcom/google/android/location/l/a/al;->b:Lcom/google/android/location/l/a/cu;

    if-eqz v1, :cond_1

    .line 373
    const/4 v1, 0x2

    iget-object v3, p0, Lcom/google/android/location/l/a/al;->b:Lcom/google/android/location/l/a/cu;

    invoke-static {v1, v3}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 376
    :cond_1
    iget-object v1, p0, Lcom/google/android/location/l/a/al;->c:Lcom/google/android/location/l/a/am;

    if-eqz v1, :cond_2

    .line 377
    const/4 v1, 0x3

    iget-object v3, p0, Lcom/google/android/location/l/a/al;->c:Lcom/google/android/location/l/a/am;

    invoke-static {v1, v3}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 380
    :cond_2
    iget-object v1, p0, Lcom/google/android/location/l/a/al;->d:Lcom/google/android/location/l/a/w;

    if-eqz v1, :cond_3

    .line 381
    const/4 v1, 0x4

    iget-object v3, p0, Lcom/google/android/location/l/a/al;->d:Lcom/google/android/location/l/a/w;

    invoke-static {v1, v3}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 384
    :cond_3
    iget-object v1, p0, Lcom/google/android/location/l/a/al;->e:Lcom/google/android/location/l/a/aq;

    if-eqz v1, :cond_4

    .line 385
    const/4 v1, 0x5

    iget-object v3, p0, Lcom/google/android/location/l/a/al;->e:Lcom/google/android/location/l/a/aq;

    invoke-static {v1, v3}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 388
    :cond_4
    iget-object v1, p0, Lcom/google/android/location/l/a/al;->f:[I

    if-eqz v1, :cond_6

    iget-object v1, p0, Lcom/google/android/location/l/a/al;->f:[I

    array-length v1, v1

    if-lez v1, :cond_6

    move v1, v2

    move v3, v2

    .line 390
    :goto_0
    iget-object v4, p0, Lcom/google/android/location/l/a/al;->f:[I

    array-length v4, v4

    if-ge v1, v4, :cond_5

    .line 391
    iget-object v4, p0, Lcom/google/android/location/l/a/al;->f:[I

    aget v4, v4, v1

    .line 392
    invoke-static {v4}, Lcom/google/protobuf/nano/b;->b(I)I

    move-result v4

    add-int/2addr v3, v4

    .line 390
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 395
    :cond_5
    add-int/2addr v0, v3

    .line 396
    iget-object v1, p0, Lcom/google/android/location/l/a/al;->f:[I

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 398
    :cond_6
    iget-object v1, p0, Lcom/google/android/location/l/a/al;->g:[[B

    if-eqz v1, :cond_9

    iget-object v1, p0, Lcom/google/android/location/l/a/al;->g:[[B

    array-length v1, v1

    if-lez v1, :cond_9

    move v1, v2

    move v3, v2

    move v4, v2

    .line 401
    :goto_1
    iget-object v5, p0, Lcom/google/android/location/l/a/al;->g:[[B

    array-length v5, v5

    if-ge v1, v5, :cond_8

    .line 402
    iget-object v5, p0, Lcom/google/android/location/l/a/al;->g:[[B

    aget-object v5, v5, v1

    .line 403
    if-eqz v5, :cond_7

    .line 404
    add-int/lit8 v4, v4, 0x1

    .line 405
    invoke-static {v5}, Lcom/google/protobuf/nano/b;->b([B)I

    move-result v5

    add-int/2addr v3, v5

    .line 401
    :cond_7
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 409
    :cond_8
    add-int/2addr v0, v3

    .line 410
    mul-int/lit8 v1, v4, 0x1

    add-int/2addr v0, v1

    .line 412
    :cond_9
    iget-object v1, p0, Lcom/google/android/location/l/a/al;->h:Lcom/google/android/location/l/a/af;

    if-eqz v1, :cond_a

    .line 413
    const/16 v1, 0x8

    iget-object v3, p0, Lcom/google/android/location/l/a/al;->h:Lcom/google/android/location/l/a/af;

    invoke-static {v1, v3}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 416
    :cond_a
    iget-object v1, p0, Lcom/google/android/location/l/a/al;->i:Lcom/google/android/location/l/a/cc;

    if-eqz v1, :cond_b

    .line 417
    const/16 v1, 0x9

    iget-object v3, p0, Lcom/google/android/location/l/a/al;->i:Lcom/google/android/location/l/a/cc;

    invoke-static {v1, v3}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 420
    :cond_b
    iget-object v1, p0, Lcom/google/android/location/l/a/al;->j:Ljava/lang/Integer;

    if-eqz v1, :cond_c

    .line 421
    const/16 v1, 0xa

    iget-object v3, p0, Lcom/google/android/location/l/a/al;->j:Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v1, v3}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 424
    :cond_c
    iget-object v1, p0, Lcom/google/android/location/l/a/al;->k:[Lcom/google/android/location/l/a/cg;

    if-eqz v1, :cond_e

    iget-object v1, p0, Lcom/google/android/location/l/a/al;->k:[Lcom/google/android/location/l/a/cg;

    array-length v1, v1

    if-lez v1, :cond_e

    .line 425
    :goto_2
    iget-object v1, p0, Lcom/google/android/location/l/a/al;->k:[Lcom/google/android/location/l/a/cg;

    array-length v1, v1

    if-ge v2, v1, :cond_e

    .line 426
    iget-object v1, p0, Lcom/google/android/location/l/a/al;->k:[Lcom/google/android/location/l/a/cg;

    aget-object v1, v1, v2

    .line 427
    if-eqz v1, :cond_d

    .line 428
    const/16 v3, 0xc

    invoke-static {v3, v1}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 425
    :cond_d
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 433
    :cond_e
    iget-object v1, p0, Lcom/google/android/location/l/a/al;->l:Lcom/google/android/location/l/a/cm;

    if-eqz v1, :cond_f

    .line 434
    const/16 v1, 0xd

    iget-object v2, p0, Lcom/google/android/location/l/a/al;->l:Lcom/google/android/location/l/a/cm;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 437
    :cond_f
    iget-object v1, p0, Lcom/google/android/location/l/a/al;->m:Ljava/lang/Long;

    if-eqz v1, :cond_10

    .line 438
    const/16 v1, 0xe

    iget-object v2, p0, Lcom/google/android/location/l/a/al;->m:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/b;->g(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 441
    :cond_10
    iget-object v1, p0, Lcom/google/android/location/l/a/al;->n:Lcom/google/android/location/l/a/bm;

    if-eqz v1, :cond_11

    .line 442
    const/16 v1, 0xf

    iget-object v2, p0, Lcom/google/android/location/l/a/al;->n:Lcom/google/android/location/l/a/bm;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 445
    :cond_11
    iget-object v1, p0, Lcom/google/android/location/l/a/al;->o:Lcom/google/android/location/l/a/da;

    if-eqz v1, :cond_12

    .line 446
    const/16 v1, 0x10

    iget-object v2, p0, Lcom/google/android/location/l/a/al;->o:Lcom/google/android/location/l/a/da;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 449
    :cond_12
    iget-object v1, p0, Lcom/google/android/location/l/a/al;->p:Lcom/google/android/location/l/a/bx;

    if-eqz v1, :cond_13

    .line 450
    const/16 v1, 0x11

    iget-object v2, p0, Lcom/google/android/location/l/a/al;->p:Lcom/google/android/location/l/a/bx;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 453
    :cond_13
    iget-object v1, p0, Lcom/google/android/location/l/a/al;->q:Lcom/google/android/location/l/a/q;

    if-eqz v1, :cond_14

    .line 454
    const/16 v1, 0x63

    iget-object v2, p0, Lcom/google/android/location/l/a/al;->q:Lcom/google/android/location/l/a/q;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 457
    :cond_14
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 111
    if-ne p1, p0, :cond_1

    .line 112
    const/4 v0, 0x1

    .line 252
    :cond_0
    :goto_0
    return v0

    .line 114
    :cond_1
    instance-of v1, p1, Lcom/google/android/location/l/a/al;

    if-eqz v1, :cond_0

    .line 117
    check-cast p1, Lcom/google/android/location/l/a/al;

    .line 118
    iget-object v1, p0, Lcom/google/android/location/l/a/al;->a:Lcom/google/android/location/l/a/h;

    if-nez v1, :cond_10

    .line 119
    iget-object v1, p1, Lcom/google/android/location/l/a/al;->a:Lcom/google/android/location/l/a/h;

    if-nez v1, :cond_0

    .line 127
    :cond_2
    iget-object v1, p0, Lcom/google/android/location/l/a/al;->b:Lcom/google/android/location/l/a/cu;

    if-nez v1, :cond_11

    .line 128
    iget-object v1, p1, Lcom/google/android/location/l/a/al;->b:Lcom/google/android/location/l/a/cu;

    if-nez v1, :cond_0

    .line 136
    :cond_3
    iget-object v1, p0, Lcom/google/android/location/l/a/al;->c:Lcom/google/android/location/l/a/am;

    if-nez v1, :cond_12

    .line 137
    iget-object v1, p1, Lcom/google/android/location/l/a/al;->c:Lcom/google/android/location/l/a/am;

    if-nez v1, :cond_0

    .line 145
    :cond_4
    iget-object v1, p0, Lcom/google/android/location/l/a/al;->d:Lcom/google/android/location/l/a/w;

    if-nez v1, :cond_13

    .line 146
    iget-object v1, p1, Lcom/google/android/location/l/a/al;->d:Lcom/google/android/location/l/a/w;

    if-nez v1, :cond_0

    .line 154
    :cond_5
    iget-object v1, p0, Lcom/google/android/location/l/a/al;->e:Lcom/google/android/location/l/a/aq;

    if-nez v1, :cond_14

    .line 155
    iget-object v1, p1, Lcom/google/android/location/l/a/al;->e:Lcom/google/android/location/l/a/aq;

    if-nez v1, :cond_0

    .line 163
    :cond_6
    iget-object v1, p0, Lcom/google/android/location/l/a/al;->f:[I

    iget-object v2, p1, Lcom/google/android/location/l/a/al;->f:[I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/h;->a([I[I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 167
    iget-object v1, p0, Lcom/google/android/location/l/a/al;->g:[[B

    iget-object v2, p1, Lcom/google/android/location/l/a/al;->g:[[B

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/h;->a([[B[[B)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 171
    iget-object v1, p0, Lcom/google/android/location/l/a/al;->h:Lcom/google/android/location/l/a/af;

    if-nez v1, :cond_15

    .line 172
    iget-object v1, p1, Lcom/google/android/location/l/a/al;->h:Lcom/google/android/location/l/a/af;

    if-nez v1, :cond_0

    .line 180
    :cond_7
    iget-object v1, p0, Lcom/google/android/location/l/a/al;->i:Lcom/google/android/location/l/a/cc;

    if-nez v1, :cond_16

    .line 181
    iget-object v1, p1, Lcom/google/android/location/l/a/al;->i:Lcom/google/android/location/l/a/cc;

    if-nez v1, :cond_0

    .line 189
    :cond_8
    iget-object v1, p0, Lcom/google/android/location/l/a/al;->j:Ljava/lang/Integer;

    if-nez v1, :cond_17

    .line 190
    iget-object v1, p1, Lcom/google/android/location/l/a/al;->j:Ljava/lang/Integer;

    if-nez v1, :cond_0

    .line 196
    :cond_9
    iget-object v1, p0, Lcom/google/android/location/l/a/al;->k:[Lcom/google/android/location/l/a/cg;

    iget-object v2, p1, Lcom/google/android/location/l/a/al;->k:[Lcom/google/android/location/l/a/cg;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 200
    iget-object v1, p0, Lcom/google/android/location/l/a/al;->l:Lcom/google/android/location/l/a/cm;

    if-nez v1, :cond_18

    .line 201
    iget-object v1, p1, Lcom/google/android/location/l/a/al;->l:Lcom/google/android/location/l/a/cm;

    if-nez v1, :cond_0

    .line 209
    :cond_a
    iget-object v1, p0, Lcom/google/android/location/l/a/al;->m:Ljava/lang/Long;

    if-nez v1, :cond_19

    .line 210
    iget-object v1, p1, Lcom/google/android/location/l/a/al;->m:Ljava/lang/Long;

    if-nez v1, :cond_0

    .line 216
    :cond_b
    iget-object v1, p0, Lcom/google/android/location/l/a/al;->n:Lcom/google/android/location/l/a/bm;

    if-nez v1, :cond_1a

    .line 217
    iget-object v1, p1, Lcom/google/android/location/l/a/al;->n:Lcom/google/android/location/l/a/bm;

    if-nez v1, :cond_0

    .line 225
    :cond_c
    iget-object v1, p0, Lcom/google/android/location/l/a/al;->o:Lcom/google/android/location/l/a/da;

    if-nez v1, :cond_1b

    .line 226
    iget-object v1, p1, Lcom/google/android/location/l/a/al;->o:Lcom/google/android/location/l/a/da;

    if-nez v1, :cond_0

    .line 234
    :cond_d
    iget-object v1, p0, Lcom/google/android/location/l/a/al;->p:Lcom/google/android/location/l/a/bx;

    if-nez v1, :cond_1c

    .line 235
    iget-object v1, p1, Lcom/google/android/location/l/a/al;->p:Lcom/google/android/location/l/a/bx;

    if-nez v1, :cond_0

    .line 243
    :cond_e
    iget-object v1, p0, Lcom/google/android/location/l/a/al;->q:Lcom/google/android/location/l/a/q;

    if-nez v1, :cond_1d

    .line 244
    iget-object v1, p1, Lcom/google/android/location/l/a/al;->q:Lcom/google/android/location/l/a/q;

    if-nez v1, :cond_0

    .line 252
    :cond_f
    invoke-virtual {p0, p1}, Lcom/google/android/location/l/a/al;->unknownFieldDataEquals(Lcom/google/protobuf/nano/d;)Z

    move-result v0

    goto/16 :goto_0

    .line 123
    :cond_10
    iget-object v1, p0, Lcom/google/android/location/l/a/al;->a:Lcom/google/android/location/l/a/h;

    iget-object v2, p1, Lcom/google/android/location/l/a/al;->a:Lcom/google/android/location/l/a/h;

    invoke-virtual {v1, v2}, Lcom/google/android/location/l/a/h;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    goto/16 :goto_0

    .line 132
    :cond_11
    iget-object v1, p0, Lcom/google/android/location/l/a/al;->b:Lcom/google/android/location/l/a/cu;

    iget-object v2, p1, Lcom/google/android/location/l/a/al;->b:Lcom/google/android/location/l/a/cu;

    invoke-virtual {v1, v2}, Lcom/google/android/location/l/a/cu;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    goto/16 :goto_0

    .line 141
    :cond_12
    iget-object v1, p0, Lcom/google/android/location/l/a/al;->c:Lcom/google/android/location/l/a/am;

    iget-object v2, p1, Lcom/google/android/location/l/a/al;->c:Lcom/google/android/location/l/a/am;

    invoke-virtual {v1, v2}, Lcom/google/android/location/l/a/am;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    goto/16 :goto_0

    .line 150
    :cond_13
    iget-object v1, p0, Lcom/google/android/location/l/a/al;->d:Lcom/google/android/location/l/a/w;

    iget-object v2, p1, Lcom/google/android/location/l/a/al;->d:Lcom/google/android/location/l/a/w;

    invoke-virtual {v1, v2}, Lcom/google/android/location/l/a/w;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_5

    goto/16 :goto_0

    .line 159
    :cond_14
    iget-object v1, p0, Lcom/google/android/location/l/a/al;->e:Lcom/google/android/location/l/a/aq;

    iget-object v2, p1, Lcom/google/android/location/l/a/al;->e:Lcom/google/android/location/l/a/aq;

    invoke-virtual {v1, v2}, Lcom/google/android/location/l/a/aq;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_6

    goto/16 :goto_0

    .line 176
    :cond_15
    iget-object v1, p0, Lcom/google/android/location/l/a/al;->h:Lcom/google/android/location/l/a/af;

    iget-object v2, p1, Lcom/google/android/location/l/a/al;->h:Lcom/google/android/location/l/a/af;

    invoke-virtual {v1, v2}, Lcom/google/android/location/l/a/af;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_7

    goto/16 :goto_0

    .line 185
    :cond_16
    iget-object v1, p0, Lcom/google/android/location/l/a/al;->i:Lcom/google/android/location/l/a/cc;

    iget-object v2, p1, Lcom/google/android/location/l/a/al;->i:Lcom/google/android/location/l/a/cc;

    invoke-virtual {v1, v2}, Lcom/google/android/location/l/a/cc;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_8

    goto/16 :goto_0

    .line 193
    :cond_17
    iget-object v1, p0, Lcom/google/android/location/l/a/al;->j:Ljava/lang/Integer;

    iget-object v2, p1, Lcom/google/android/location/l/a/al;->j:Ljava/lang/Integer;

    invoke-virtual {v1, v2}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_9

    goto/16 :goto_0

    .line 205
    :cond_18
    iget-object v1, p0, Lcom/google/android/location/l/a/al;->l:Lcom/google/android/location/l/a/cm;

    iget-object v2, p1, Lcom/google/android/location/l/a/al;->l:Lcom/google/android/location/l/a/cm;

    invoke-virtual {v1, v2}, Lcom/google/android/location/l/a/cm;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_a

    goto/16 :goto_0

    .line 213
    :cond_19
    iget-object v1, p0, Lcom/google/android/location/l/a/al;->m:Ljava/lang/Long;

    iget-object v2, p1, Lcom/google/android/location/l/a/al;->m:Ljava/lang/Long;

    invoke-virtual {v1, v2}, Ljava/lang/Long;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_b

    goto/16 :goto_0

    .line 221
    :cond_1a
    iget-object v1, p0, Lcom/google/android/location/l/a/al;->n:Lcom/google/android/location/l/a/bm;

    iget-object v2, p1, Lcom/google/android/location/l/a/al;->n:Lcom/google/android/location/l/a/bm;

    invoke-virtual {v1, v2}, Lcom/google/android/location/l/a/bm;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_c

    goto/16 :goto_0

    .line 230
    :cond_1b
    iget-object v1, p0, Lcom/google/android/location/l/a/al;->o:Lcom/google/android/location/l/a/da;

    iget-object v2, p1, Lcom/google/android/location/l/a/al;->o:Lcom/google/android/location/l/a/da;

    invoke-virtual {v1, v2}, Lcom/google/android/location/l/a/da;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_d

    goto/16 :goto_0

    .line 239
    :cond_1c
    iget-object v1, p0, Lcom/google/android/location/l/a/al;->p:Lcom/google/android/location/l/a/bx;

    iget-object v2, p1, Lcom/google/android/location/l/a/al;->p:Lcom/google/android/location/l/a/bx;

    invoke-virtual {v1, v2}, Lcom/google/android/location/l/a/bx;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_e

    goto/16 :goto_0

    .line 248
    :cond_1d
    iget-object v1, p0, Lcom/google/android/location/l/a/al;->q:Lcom/google/android/location/l/a/q;

    iget-object v2, p1, Lcom/google/android/location/l/a/al;->q:Lcom/google/android/location/l/a/q;

    invoke-virtual {v1, v2}, Lcom/google/android/location/l/a/q;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_f

    goto/16 :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 257
    iget-object v0, p0, Lcom/google/android/location/l/a/al;->a:Lcom/google/android/location/l/a/h;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    .line 260
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/location/l/a/al;->b:Lcom/google/android/location/l/a/cu;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    .line 262
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/location/l/a/al;->c:Lcom/google/android/location/l/a/am;

    if-nez v0, :cond_2

    move v0, v1

    :goto_2
    add-int/2addr v0, v2

    .line 264
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/location/l/a/al;->d:Lcom/google/android/location/l/a/w;

    if-nez v0, :cond_3

    move v0, v1

    :goto_3
    add-int/2addr v0, v2

    .line 266
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/location/l/a/al;->e:Lcom/google/android/location/l/a/aq;

    if-nez v0, :cond_4

    move v0, v1

    :goto_4
    add-int/2addr v0, v2

    .line 268
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/android/location/l/a/al;->f:[I

    invoke-static {v2}, Lcom/google/protobuf/nano/h;->a([I)I

    move-result v2

    add-int/2addr v0, v2

    .line 270
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/android/location/l/a/al;->g:[[B

    invoke-static {v2}, Lcom/google/protobuf/nano/h;->a([[B)I

    move-result v2

    add-int/2addr v0, v2

    .line 272
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/location/l/a/al;->h:Lcom/google/android/location/l/a/af;

    if-nez v0, :cond_5

    move v0, v1

    :goto_5
    add-int/2addr v0, v2

    .line 274
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/location/l/a/al;->i:Lcom/google/android/location/l/a/cc;

    if-nez v0, :cond_6

    move v0, v1

    :goto_6
    add-int/2addr v0, v2

    .line 276
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/location/l/a/al;->j:Ljava/lang/Integer;

    if-nez v0, :cond_7

    move v0, v1

    :goto_7
    add-int/2addr v0, v2

    .line 278
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/android/location/l/a/al;->k:[Lcom/google/android/location/l/a/cg;

    invoke-static {v2}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;)I

    move-result v2

    add-int/2addr v0, v2

    .line 280
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/location/l/a/al;->l:Lcom/google/android/location/l/a/cm;

    if-nez v0, :cond_8

    move v0, v1

    :goto_8
    add-int/2addr v0, v2

    .line 282
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/location/l/a/al;->m:Ljava/lang/Long;

    if-nez v0, :cond_9

    move v0, v1

    :goto_9
    add-int/2addr v0, v2

    .line 284
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/location/l/a/al;->n:Lcom/google/android/location/l/a/bm;

    if-nez v0, :cond_a

    move v0, v1

    :goto_a
    add-int/2addr v0, v2

    .line 286
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/location/l/a/al;->o:Lcom/google/android/location/l/a/da;

    if-nez v0, :cond_b

    move v0, v1

    :goto_b
    add-int/2addr v0, v2

    .line 288
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/location/l/a/al;->p:Lcom/google/android/location/l/a/bx;

    if-nez v0, :cond_c

    move v0, v1

    :goto_c
    add-int/2addr v0, v2

    .line 290
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/android/location/l/a/al;->q:Lcom/google/android/location/l/a/q;

    if-nez v2, :cond_d

    :goto_d
    add-int/2addr v0, v1

    .line 292
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/google/android/location/l/a/al;->unknownFieldDataHashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 293
    return v0

    .line 257
    :cond_0
    iget-object v0, p0, Lcom/google/android/location/l/a/al;->a:Lcom/google/android/location/l/a/h;

    invoke-virtual {v0}, Lcom/google/android/location/l/a/h;->hashCode()I

    move-result v0

    goto/16 :goto_0

    .line 260
    :cond_1
    iget-object v0, p0, Lcom/google/android/location/l/a/al;->b:Lcom/google/android/location/l/a/cu;

    invoke-virtual {v0}, Lcom/google/android/location/l/a/cu;->hashCode()I

    move-result v0

    goto/16 :goto_1

    .line 262
    :cond_2
    iget-object v0, p0, Lcom/google/android/location/l/a/al;->c:Lcom/google/android/location/l/a/am;

    invoke-virtual {v0}, Lcom/google/android/location/l/a/am;->hashCode()I

    move-result v0

    goto/16 :goto_2

    .line 264
    :cond_3
    iget-object v0, p0, Lcom/google/android/location/l/a/al;->d:Lcom/google/android/location/l/a/w;

    invoke-virtual {v0}, Lcom/google/android/location/l/a/w;->hashCode()I

    move-result v0

    goto/16 :goto_3

    .line 266
    :cond_4
    iget-object v0, p0, Lcom/google/android/location/l/a/al;->e:Lcom/google/android/location/l/a/aq;

    invoke-virtual {v0}, Lcom/google/android/location/l/a/aq;->hashCode()I

    move-result v0

    goto/16 :goto_4

    .line 272
    :cond_5
    iget-object v0, p0, Lcom/google/android/location/l/a/al;->h:Lcom/google/android/location/l/a/af;

    invoke-virtual {v0}, Lcom/google/android/location/l/a/af;->hashCode()I

    move-result v0

    goto :goto_5

    .line 274
    :cond_6
    iget-object v0, p0, Lcom/google/android/location/l/a/al;->i:Lcom/google/android/location/l/a/cc;

    invoke-virtual {v0}, Lcom/google/android/location/l/a/cc;->hashCode()I

    move-result v0

    goto :goto_6

    .line 276
    :cond_7
    iget-object v0, p0, Lcom/google/android/location/l/a/al;->j:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->hashCode()I

    move-result v0

    goto :goto_7

    .line 280
    :cond_8
    iget-object v0, p0, Lcom/google/android/location/l/a/al;->l:Lcom/google/android/location/l/a/cm;

    invoke-virtual {v0}, Lcom/google/android/location/l/a/cm;->hashCode()I

    move-result v0

    goto :goto_8

    .line 282
    :cond_9
    iget-object v0, p0, Lcom/google/android/location/l/a/al;->m:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->hashCode()I

    move-result v0

    goto :goto_9

    .line 284
    :cond_a
    iget-object v0, p0, Lcom/google/android/location/l/a/al;->n:Lcom/google/android/location/l/a/bm;

    invoke-virtual {v0}, Lcom/google/android/location/l/a/bm;->hashCode()I

    move-result v0

    goto :goto_a

    .line 286
    :cond_b
    iget-object v0, p0, Lcom/google/android/location/l/a/al;->o:Lcom/google/android/location/l/a/da;

    invoke-virtual {v0}, Lcom/google/android/location/l/a/da;->hashCode()I

    move-result v0

    goto :goto_b

    .line 288
    :cond_c
    iget-object v0, p0, Lcom/google/android/location/l/a/al;->p:Lcom/google/android/location/l/a/bx;

    invoke-virtual {v0}, Lcom/google/android/location/l/a/bx;->hashCode()I

    move-result v0

    goto :goto_c

    .line 290
    :cond_d
    iget-object v1, p0, Lcom/google/android/location/l/a/al;->q:Lcom/google/android/location/l/a/q;

    invoke-virtual {v1}, Lcom/google/android/location/l/a/q;->hashCode()I

    move-result v1

    goto :goto_d
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 5
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/android/location/l/a/al;->storeUnknownField(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Lcom/google/android/location/l/a/al;->a:Lcom/google/android/location/l/a/h;

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/android/location/l/a/h;

    invoke-direct {v0}, Lcom/google/android/location/l/a/h;-><init>()V

    iput-object v0, p0, Lcom/google/android/location/l/a/al;->a:Lcom/google/android/location/l/a/h;

    :cond_1
    iget-object v0, p0, Lcom/google/android/location/l/a/al;->a:Lcom/google/android/location/l/a/h;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Lcom/google/android/location/l/a/al;->b:Lcom/google/android/location/l/a/cu;

    if-nez v0, :cond_2

    new-instance v0, Lcom/google/android/location/l/a/cu;

    invoke-direct {v0}, Lcom/google/android/location/l/a/cu;-><init>()V

    iput-object v0, p0, Lcom/google/android/location/l/a/al;->b:Lcom/google/android/location/l/a/cu;

    :cond_2
    iget-object v0, p0, Lcom/google/android/location/l/a/al;->b:Lcom/google/android/location/l/a/cu;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Lcom/google/android/location/l/a/al;->c:Lcom/google/android/location/l/a/am;

    if-nez v0, :cond_3

    new-instance v0, Lcom/google/android/location/l/a/am;

    invoke-direct {v0}, Lcom/google/android/location/l/a/am;-><init>()V

    iput-object v0, p0, Lcom/google/android/location/l/a/al;->c:Lcom/google/android/location/l/a/am;

    :cond_3
    iget-object v0, p0, Lcom/google/android/location/l/a/al;->c:Lcom/google/android/location/l/a/am;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_4
    iget-object v0, p0, Lcom/google/android/location/l/a/al;->d:Lcom/google/android/location/l/a/w;

    if-nez v0, :cond_4

    new-instance v0, Lcom/google/android/location/l/a/w;

    invoke-direct {v0}, Lcom/google/android/location/l/a/w;-><init>()V

    iput-object v0, p0, Lcom/google/android/location/l/a/al;->d:Lcom/google/android/location/l/a/w;

    :cond_4
    iget-object v0, p0, Lcom/google/android/location/l/a/al;->d:Lcom/google/android/location/l/a/w;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_5
    iget-object v0, p0, Lcom/google/android/location/l/a/al;->e:Lcom/google/android/location/l/a/aq;

    if-nez v0, :cond_5

    new-instance v0, Lcom/google/android/location/l/a/aq;

    invoke-direct {v0}, Lcom/google/android/location/l/a/aq;-><init>()V

    iput-object v0, p0, Lcom/google/android/location/l/a/al;->e:Lcom/google/android/location/l/a/aq;

    :cond_5
    iget-object v0, p0, Lcom/google/android/location/l/a/al;->e:Lcom/google/android/location/l/a/aq;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_6
    const/16 v0, 0x30

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->b(Lcom/google/protobuf/nano/a;I)I

    move-result v2

    iget-object v0, p0, Lcom/google/android/location/l/a/al;->f:[I

    if-nez v0, :cond_7

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [I

    if-eqz v0, :cond_6

    iget-object v3, p0, Lcom/google/android/location/l/a/al;->f:[I

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_6
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_8

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v3

    aput v3, v2, v0

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_7
    iget-object v0, p0, Lcom/google/android/location/l/a/al;->f:[I

    array-length v0, v0

    goto :goto_1

    :cond_8
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v3

    aput v3, v2, v0

    iput-object v2, p0, Lcom/google/android/location/l/a/al;->f:[I

    goto/16 :goto_0

    :sswitch_7
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->c(I)I

    move-result v3

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->o()I

    move-result v2

    move v0, v1

    :goto_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->m()I

    move-result v4

    if-lez v4, :cond_9

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    :cond_9
    invoke-virtual {p1, v2}, Lcom/google/protobuf/nano/a;->e(I)V

    iget-object v2, p0, Lcom/google/android/location/l/a/al;->f:[I

    if-nez v2, :cond_b

    move v2, v1

    :goto_4
    add-int/2addr v0, v2

    new-array v0, v0, [I

    if-eqz v2, :cond_a

    iget-object v4, p0, Lcom/google/android/location/l/a/al;->f:[I

    invoke-static {v4, v1, v0, v1, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_a
    :goto_5
    array-length v4, v0

    if-ge v2, v4, :cond_c

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v4

    aput v4, v0, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_5

    :cond_b
    iget-object v2, p0, Lcom/google/android/location/l/a/al;->f:[I

    array-length v2, v2

    goto :goto_4

    :cond_c
    iput-object v0, p0, Lcom/google/android/location/l/a/al;->f:[I

    invoke-virtual {p1, v3}, Lcom/google/protobuf/nano/a;->d(I)V

    goto/16 :goto_0

    :sswitch_8
    const/16 v0, 0x3a

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->b(Lcom/google/protobuf/nano/a;I)I

    move-result v2

    iget-object v0, p0, Lcom/google/android/location/l/a/al;->g:[[B

    if-nez v0, :cond_e

    move v0, v1

    :goto_6
    add-int/2addr v2, v0

    new-array v2, v2, [[B

    if-eqz v0, :cond_d

    iget-object v3, p0, Lcom/google/android/location/l/a/al;->g:[[B

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_d
    :goto_7
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_f

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->f()[B

    move-result-object v3

    aput-object v3, v2, v0

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_7

    :cond_e
    iget-object v0, p0, Lcom/google/android/location/l/a/al;->g:[[B

    array-length v0, v0

    goto :goto_6

    :cond_f
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->f()[B

    move-result-object v3

    aput-object v3, v2, v0

    iput-object v2, p0, Lcom/google/android/location/l/a/al;->g:[[B

    goto/16 :goto_0

    :sswitch_9
    iget-object v0, p0, Lcom/google/android/location/l/a/al;->h:Lcom/google/android/location/l/a/af;

    if-nez v0, :cond_10

    new-instance v0, Lcom/google/android/location/l/a/af;

    invoke-direct {v0}, Lcom/google/android/location/l/a/af;-><init>()V

    iput-object v0, p0, Lcom/google/android/location/l/a/al;->h:Lcom/google/android/location/l/a/af;

    :cond_10
    iget-object v0, p0, Lcom/google/android/location/l/a/al;->h:Lcom/google/android/location/l/a/af;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto/16 :goto_0

    :sswitch_a
    iget-object v0, p0, Lcom/google/android/location/l/a/al;->i:Lcom/google/android/location/l/a/cc;

    if-nez v0, :cond_11

    new-instance v0, Lcom/google/android/location/l/a/cc;

    invoke-direct {v0}, Lcom/google/android/location/l/a/cc;-><init>()V

    iput-object v0, p0, Lcom/google/android/location/l/a/al;->i:Lcom/google/android/location/l/a/cc;

    :cond_11
    iget-object v0, p0, Lcom/google/android/location/l/a/al;->i:Lcom/google/android/location/l/a/cc;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto/16 :goto_0

    :sswitch_b
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/l/a/al;->j:Ljava/lang/Integer;

    goto/16 :goto_0

    :sswitch_c
    const/16 v0, 0x62

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->b(Lcom/google/protobuf/nano/a;I)I

    move-result v2

    iget-object v0, p0, Lcom/google/android/location/l/a/al;->k:[Lcom/google/android/location/l/a/cg;

    if-nez v0, :cond_13

    move v0, v1

    :goto_8
    add-int/2addr v2, v0

    new-array v2, v2, [Lcom/google/android/location/l/a/cg;

    if-eqz v0, :cond_12

    iget-object v3, p0, Lcom/google/android/location/l/a/al;->k:[Lcom/google/android/location/l/a/cg;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_12
    :goto_9
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_14

    new-instance v3, Lcom/google/android/location/l/a/cg;

    invoke-direct {v3}, Lcom/google/android/location/l/a/cg;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_9

    :cond_13
    iget-object v0, p0, Lcom/google/android/location/l/a/al;->k:[Lcom/google/android/location/l/a/cg;

    array-length v0, v0

    goto :goto_8

    :cond_14
    new-instance v3, Lcom/google/android/location/l/a/cg;

    invoke-direct {v3}, Lcom/google/android/location/l/a/cg;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    iput-object v2, p0, Lcom/google/android/location/l/a/al;->k:[Lcom/google/android/location/l/a/cg;

    goto/16 :goto_0

    :sswitch_d
    iget-object v0, p0, Lcom/google/android/location/l/a/al;->l:Lcom/google/android/location/l/a/cm;

    if-nez v0, :cond_15

    new-instance v0, Lcom/google/android/location/l/a/cm;

    invoke-direct {v0}, Lcom/google/android/location/l/a/cm;-><init>()V

    iput-object v0, p0, Lcom/google/android/location/l/a/al;->l:Lcom/google/android/location/l/a/cm;

    :cond_15
    iget-object v0, p0, Lcom/google/android/location/l/a/al;->l:Lcom/google/android/location/l/a/cm;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto/16 :goto_0

    :sswitch_e
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->h()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/l/a/al;->m:Ljava/lang/Long;

    goto/16 :goto_0

    :sswitch_f
    iget-object v0, p0, Lcom/google/android/location/l/a/al;->n:Lcom/google/android/location/l/a/bm;

    if-nez v0, :cond_16

    new-instance v0, Lcom/google/android/location/l/a/bm;

    invoke-direct {v0}, Lcom/google/android/location/l/a/bm;-><init>()V

    iput-object v0, p0, Lcom/google/android/location/l/a/al;->n:Lcom/google/android/location/l/a/bm;

    :cond_16
    iget-object v0, p0, Lcom/google/android/location/l/a/al;->n:Lcom/google/android/location/l/a/bm;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto/16 :goto_0

    :sswitch_10
    iget-object v0, p0, Lcom/google/android/location/l/a/al;->o:Lcom/google/android/location/l/a/da;

    if-nez v0, :cond_17

    new-instance v0, Lcom/google/android/location/l/a/da;

    invoke-direct {v0}, Lcom/google/android/location/l/a/da;-><init>()V

    iput-object v0, p0, Lcom/google/android/location/l/a/al;->o:Lcom/google/android/location/l/a/da;

    :cond_17
    iget-object v0, p0, Lcom/google/android/location/l/a/al;->o:Lcom/google/android/location/l/a/da;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto/16 :goto_0

    :sswitch_11
    iget-object v0, p0, Lcom/google/android/location/l/a/al;->p:Lcom/google/android/location/l/a/bx;

    if-nez v0, :cond_18

    new-instance v0, Lcom/google/android/location/l/a/bx;

    invoke-direct {v0}, Lcom/google/android/location/l/a/bx;-><init>()V

    iput-object v0, p0, Lcom/google/android/location/l/a/al;->p:Lcom/google/android/location/l/a/bx;

    :cond_18
    iget-object v0, p0, Lcom/google/android/location/l/a/al;->p:Lcom/google/android/location/l/a/bx;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto/16 :goto_0

    :sswitch_12
    iget-object v0, p0, Lcom/google/android/location/l/a/al;->q:Lcom/google/android/location/l/a/q;

    if-nez v0, :cond_19

    new-instance v0, Lcom/google/android/location/l/a/q;

    invoke-direct {v0}, Lcom/google/android/location/l/a/q;-><init>()V

    iput-object v0, p0, Lcom/google/android/location/l/a/al;->q:Lcom/google/android/location/l/a/q;

    :cond_19
    iget-object v0, p0, Lcom/google/android/location/l/a/al;->q:Lcom/google/android/location/l/a/q;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x30 -> :sswitch_6
        0x32 -> :sswitch_7
        0x3a -> :sswitch_8
        0x42 -> :sswitch_9
        0x4a -> :sswitch_a
        0x50 -> :sswitch_b
        0x62 -> :sswitch_c
        0x6a -> :sswitch_d
        0x70 -> :sswitch_e
        0x7a -> :sswitch_f
        0x82 -> :sswitch_10
        0x8a -> :sswitch_11
        0x31a -> :sswitch_12
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 299
    iget-object v0, p0, Lcom/google/android/location/l/a/al;->a:Lcom/google/android/location/l/a/h;

    if-eqz v0, :cond_0

    .line 300
    const/4 v0, 0x1

    iget-object v2, p0, Lcom/google/android/location/l/a/al;->a:Lcom/google/android/location/l/a/h;

    invoke-virtual {p1, v0, v2}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 302
    :cond_0
    iget-object v0, p0, Lcom/google/android/location/l/a/al;->b:Lcom/google/android/location/l/a/cu;

    if-eqz v0, :cond_1

    .line 303
    const/4 v0, 0x2

    iget-object v2, p0, Lcom/google/android/location/l/a/al;->b:Lcom/google/android/location/l/a/cu;

    invoke-virtual {p1, v0, v2}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 305
    :cond_1
    iget-object v0, p0, Lcom/google/android/location/l/a/al;->c:Lcom/google/android/location/l/a/am;

    if-eqz v0, :cond_2

    .line 306
    const/4 v0, 0x3

    iget-object v2, p0, Lcom/google/android/location/l/a/al;->c:Lcom/google/android/location/l/a/am;

    invoke-virtual {p1, v0, v2}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 308
    :cond_2
    iget-object v0, p0, Lcom/google/android/location/l/a/al;->d:Lcom/google/android/location/l/a/w;

    if-eqz v0, :cond_3

    .line 309
    const/4 v0, 0x4

    iget-object v2, p0, Lcom/google/android/location/l/a/al;->d:Lcom/google/android/location/l/a/w;

    invoke-virtual {p1, v0, v2}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 311
    :cond_3
    iget-object v0, p0, Lcom/google/android/location/l/a/al;->e:Lcom/google/android/location/l/a/aq;

    if-eqz v0, :cond_4

    .line 312
    const/4 v0, 0x5

    iget-object v2, p0, Lcom/google/android/location/l/a/al;->e:Lcom/google/android/location/l/a/aq;

    invoke-virtual {p1, v0, v2}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 314
    :cond_4
    iget-object v0, p0, Lcom/google/android/location/l/a/al;->f:[I

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/google/android/location/l/a/al;->f:[I

    array-length v0, v0

    if-lez v0, :cond_5

    move v0, v1

    .line 315
    :goto_0
    iget-object v2, p0, Lcom/google/android/location/l/a/al;->f:[I

    array-length v2, v2

    if-ge v0, v2, :cond_5

    .line 316
    const/4 v2, 0x6

    iget-object v3, p0, Lcom/google/android/location/l/a/al;->f:[I

    aget v3, v3, v0

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 315
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 319
    :cond_5
    iget-object v0, p0, Lcom/google/android/location/l/a/al;->g:[[B

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/google/android/location/l/a/al;->g:[[B

    array-length v0, v0

    if-lez v0, :cond_7

    move v0, v1

    .line 320
    :goto_1
    iget-object v2, p0, Lcom/google/android/location/l/a/al;->g:[[B

    array-length v2, v2

    if-ge v0, v2, :cond_7

    .line 321
    iget-object v2, p0, Lcom/google/android/location/l/a/al;->g:[[B

    aget-object v2, v2, v0

    .line 322
    if-eqz v2, :cond_6

    .line 323
    const/4 v3, 0x7

    invoke-virtual {p1, v3, v2}, Lcom/google/protobuf/nano/b;->a(I[B)V

    .line 320
    :cond_6
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 327
    :cond_7
    iget-object v0, p0, Lcom/google/android/location/l/a/al;->h:Lcom/google/android/location/l/a/af;

    if-eqz v0, :cond_8

    .line 328
    const/16 v0, 0x8

    iget-object v2, p0, Lcom/google/android/location/l/a/al;->h:Lcom/google/android/location/l/a/af;

    invoke-virtual {p1, v0, v2}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 330
    :cond_8
    iget-object v0, p0, Lcom/google/android/location/l/a/al;->i:Lcom/google/android/location/l/a/cc;

    if-eqz v0, :cond_9

    .line 331
    const/16 v0, 0x9

    iget-object v2, p0, Lcom/google/android/location/l/a/al;->i:Lcom/google/android/location/l/a/cc;

    invoke-virtual {p1, v0, v2}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 333
    :cond_9
    iget-object v0, p0, Lcom/google/android/location/l/a/al;->j:Ljava/lang/Integer;

    if-eqz v0, :cond_a

    .line 334
    const/16 v0, 0xa

    iget-object v2, p0, Lcom/google/android/location/l/a/al;->j:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {p1, v0, v2}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 336
    :cond_a
    iget-object v0, p0, Lcom/google/android/location/l/a/al;->k:[Lcom/google/android/location/l/a/cg;

    if-eqz v0, :cond_c

    iget-object v0, p0, Lcom/google/android/location/l/a/al;->k:[Lcom/google/android/location/l/a/cg;

    array-length v0, v0

    if-lez v0, :cond_c

    .line 337
    :goto_2
    iget-object v0, p0, Lcom/google/android/location/l/a/al;->k:[Lcom/google/android/location/l/a/cg;

    array-length v0, v0

    if-ge v1, v0, :cond_c

    .line 338
    iget-object v0, p0, Lcom/google/android/location/l/a/al;->k:[Lcom/google/android/location/l/a/cg;

    aget-object v0, v0, v1

    .line 339
    if-eqz v0, :cond_b

    .line 340
    const/16 v2, 0xc

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 337
    :cond_b
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 344
    :cond_c
    iget-object v0, p0, Lcom/google/android/location/l/a/al;->l:Lcom/google/android/location/l/a/cm;

    if-eqz v0, :cond_d

    .line 345
    const/16 v0, 0xd

    iget-object v1, p0, Lcom/google/android/location/l/a/al;->l:Lcom/google/android/location/l/a/cm;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 347
    :cond_d
    iget-object v0, p0, Lcom/google/android/location/l/a/al;->m:Ljava/lang/Long;

    if-eqz v0, :cond_e

    .line 348
    const/16 v0, 0xe

    iget-object v1, p0, Lcom/google/android/location/l/a/al;->m:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/b;->d(IJ)V

    .line 350
    :cond_e
    iget-object v0, p0, Lcom/google/android/location/l/a/al;->n:Lcom/google/android/location/l/a/bm;

    if-eqz v0, :cond_f

    .line 351
    const/16 v0, 0xf

    iget-object v1, p0, Lcom/google/android/location/l/a/al;->n:Lcom/google/android/location/l/a/bm;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 353
    :cond_f
    iget-object v0, p0, Lcom/google/android/location/l/a/al;->o:Lcom/google/android/location/l/a/da;

    if-eqz v0, :cond_10

    .line 354
    const/16 v0, 0x10

    iget-object v1, p0, Lcom/google/android/location/l/a/al;->o:Lcom/google/android/location/l/a/da;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 356
    :cond_10
    iget-object v0, p0, Lcom/google/android/location/l/a/al;->p:Lcom/google/android/location/l/a/bx;

    if-eqz v0, :cond_11

    .line 357
    const/16 v0, 0x11

    iget-object v1, p0, Lcom/google/android/location/l/a/al;->p:Lcom/google/android/location/l/a/bx;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 359
    :cond_11
    iget-object v0, p0, Lcom/google/android/location/l/a/al;->q:Lcom/google/android/location/l/a/q;

    if-eqz v0, :cond_12

    .line 360
    const/16 v0, 0x63

    iget-object v1, p0, Lcom/google/android/location/l/a/al;->q:Lcom/google/android/location/l/a/q;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 362
    :cond_12
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/d;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 363
    return-void
.end method
