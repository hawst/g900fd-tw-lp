.class final Lcom/google/android/location/copresence/r/ao;
.super Landroid/content/BroadcastReceiver;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/location/copresence/r/an;


# direct methods
.method constructor <init>(Lcom/google/android/location/copresence/r/an;)V
    .locals 0

    .prologue
    .line 41
    iput-object p1, p0, Lcom/google/android/location/copresence/r/ao;->a:Lcom/google/android/location/copresence/r/an;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public final onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 9

    .prologue
    const/4 v8, 0x3

    const/4 v2, 0x0

    .line 45
    if-eqz p2, :cond_7

    iget-object v0, p0, Lcom/google/android/location/copresence/r/ao;->a:Lcom/google/android/location/copresence/r/an;

    invoke-static {v0}, Lcom/google/android/location/copresence/r/an;->a(Lcom/google/android/location/copresence/r/an;)Lcom/google/android/location/copresence/ab;

    move-result-object v0

    if-eqz v0, :cond_7

    const-string v0, "android.net.wifi.SCAN_RESULTS"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 47
    iget-object v0, p0, Lcom/google/android/location/copresence/r/ao;->a:Lcom/google/android/location/copresence/r/an;

    invoke-static {v0}, Lcom/google/android/location/copresence/r/an;->b(Lcom/google/android/location/copresence/r/an;)Lcom/google/android/location/copresence/r/ae;

    move-result-object v0

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iget-object v0, v0, Lcom/google/android/location/copresence/r/ae;->a:Landroid/net/wifi/WifiManager;

    invoke-virtual {v0}, Landroid/net/wifi/WifiManager;->getScanResults()Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/wifi/ScanResult;

    if-eqz v0, :cond_0

    iget-object v4, v0, Landroid/net/wifi/ScanResult;->SSID:Ljava/lang/String;

    if-eqz v4, :cond_1

    iget-object v4, v0, Landroid/net/wifi/ScanResult;->SSID:Ljava/lang/String;

    invoke-static {v4}, Lcom/google/android/libraries/a/a/a/a;->a(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_0

    :cond_1
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_2
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_3
    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/wifi/ScanResult;

    .line 50
    iget-object v3, v0, Landroid/net/wifi/ScanResult;->SSID:Ljava/lang/String;

    .line 51
    if-eqz v3, :cond_3

    .line 52
    const/4 v1, 0x0

    .line 56
    const-string v5, "DIRECT-"

    invoke-virtual {v3, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_5

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v5

    invoke-static {}, Lcom/google/android/location/copresence/r/an;->d()I

    move-result v6

    if-le v5, v6, :cond_5

    .line 59
    invoke-static {}, Lcom/google/android/location/copresence/r/an;->d()I

    move-result v5

    invoke-virtual {v3, v5}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v3

    .line 60
    invoke-static {v8}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 61
    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "WifiSsidTokenObserver: Wifi direct prefix detected: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/google/android/location/copresence/ag;->b(Ljava/lang/String;)I

    .line 63
    :cond_4
    iget-object v5, p0, Lcom/google/android/location/copresence/r/ao;->a:Lcom/google/android/location/copresence/r/an;

    invoke-static {v5}, Lcom/google/android/location/copresence/r/an;->c(Lcom/google/android/location/copresence/r/an;)Lcom/google/ac/b/c/o;

    move-result-object v5

    if-eqz v5, :cond_8

    iget-object v5, p0, Lcom/google/android/location/copresence/r/ao;->a:Lcom/google/android/location/copresence/r/an;

    invoke-static {v5}, Lcom/google/android/location/copresence/r/an;->c(Lcom/google/android/location/copresence/r/an;)Lcom/google/ac/b/c/o;

    move-result-object v5

    iget-object v5, v5, Lcom/google/ac/b/c/o;->b:Ljava/lang/String;

    invoke-virtual {v3, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_8

    .line 64
    iget-object v1, p0, Lcom/google/android/location/copresence/r/ao;->a:Lcom/google/android/location/copresence/r/an;

    invoke-static {v1}, Lcom/google/android/location/copresence/r/an;->c(Lcom/google/android/location/copresence/r/an;)Lcom/google/ac/b/c/o;

    move-result-object v1

    iget-object v1, v1, Lcom/google/ac/b/c/o;->b:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    invoke-virtual {v3, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v3

    .line 65
    const/4 v1, 0x4

    .line 73
    :goto_2
    if-eqz v3, :cond_3

    .line 75
    :try_start_0
    invoke-static {v3}, Lcom/google/android/gms/location/copresence/x;->a(Ljava/lang/String;)Lcom/google/android/gms/location/copresence/x;

    move-result-object v5

    .line 76
    new-instance v6, Lcom/google/ac/b/c/bu;

    invoke-direct {v6}, Lcom/google/ac/b/c/bu;-><init>()V

    .line 77
    invoke-virtual {v5}, Lcom/google/android/gms/location/copresence/x;->a()Ljava/lang/String;

    move-result-object v7

    iput-object v7, v6, Lcom/google/ac/b/c/bu;->a:Ljava/lang/String;

    .line 78
    const/4 v7, 0x1

    new-array v7, v7, [Lcom/google/ac/b/c/bv;

    iput-object v7, v6, Lcom/google/ac/b/c/bu;->b:[Lcom/google/ac/b/c/bv;

    .line 79
    new-instance v7, Lcom/google/ac/b/c/bv;

    invoke-direct {v7}, Lcom/google/ac/b/c/bv;-><init>()V

    .line 80
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, v7, Lcom/google/ac/b/c/bv;->a:Ljava/lang/Integer;

    .line 81
    iget v1, v0, Landroid/net/wifi/ScanResult;->level:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, v7, Lcom/google/ac/b/c/bv;->b:Ljava/lang/Integer;

    .line 82
    new-instance v1, Lcom/google/ac/b/c/by;

    invoke-direct {v1}, Lcom/google/ac/b/c/by;-><init>()V

    .line 83
    iget v0, v0, Landroid/net/wifi/ScanResult;->frequency:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, v1, Lcom/google/ac/b/c/by;->a:Ljava/lang/Integer;

    .line 84
    iput-object v1, v7, Lcom/google/ac/b/c/bv;->d:Lcom/google/ac/b/c/by;

    .line 85
    iget-object v0, v6, Lcom/google/ac/b/c/bu;->b:[Lcom/google/ac/b/c/bv;

    const/4 v1, 0x0

    aput-object v7, v0, v1

    .line 86
    iget-object v0, p0, Lcom/google/android/location/copresence/r/ao;->a:Lcom/google/android/location/copresence/r/an;

    invoke-static {v0}, Lcom/google/android/location/copresence/r/an;->a(Lcom/google/android/location/copresence/r/an;)Lcom/google/android/location/copresence/ab;

    move-result-object v0

    invoke-interface {v0, v5, v6}, Lcom/google/android/location/copresence/ab;->a(Lcom/google/android/gms/location/copresence/x;Lcom/google/ac/b/c/bu;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_1

    .line 88
    :catch_0
    move-exception v0

    invoke-static {v8}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 89
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "WifiSsidTokenObserver: Copresence prefix, but bad token \'"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->b(Ljava/lang/String;)I

    goto/16 :goto_1

    .line 67
    :cond_5
    iget-object v5, p0, Lcom/google/android/location/copresence/r/ao;->a:Lcom/google/android/location/copresence/r/an;

    invoke-static {v5}, Lcom/google/android/location/copresence/r/an;->c(Lcom/google/android/location/copresence/r/an;)Lcom/google/ac/b/c/o;

    move-result-object v5

    if-eqz v5, :cond_8

    iget-object v5, p0, Lcom/google/android/location/copresence/r/ao;->a:Lcom/google/android/location/copresence/r/an;

    invoke-static {v5}, Lcom/google/android/location/copresence/r/an;->c(Lcom/google/android/location/copresence/r/an;)Lcom/google/ac/b/c/o;

    move-result-object v5

    iget-object v5, v5, Lcom/google/ac/b/c/o;->b:Ljava/lang/String;

    invoke-virtual {v3, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_8

    .line 69
    iget-object v1, p0, Lcom/google/android/location/copresence/r/ao;->a:Lcom/google/android/location/copresence/r/an;

    invoke-static {v1}, Lcom/google/android/location/copresence/r/an;->c(Lcom/google/android/location/copresence/r/an;)Lcom/google/ac/b/c/o;

    move-result-object v1

    iget-object v1, v1, Lcom/google/ac/b/c/o;->b:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    invoke-virtual {v3, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v3

    .line 70
    const/4 v1, 0x7

    goto/16 :goto_2

    .line 95
    :cond_6
    iget-object v0, p0, Lcom/google/android/location/copresence/r/ao;->a:Lcom/google/android/location/copresence/r/an;

    invoke-static {v0}, Lcom/google/android/location/copresence/r/an;->d(Lcom/google/android/location/copresence/r/an;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 97
    iget-object v0, p0, Lcom/google/android/location/copresence/r/ao;->a:Lcom/google/android/location/copresence/r/an;

    invoke-static {v0}, Lcom/google/android/location/copresence/r/an;->f(Lcom/google/android/location/copresence/r/an;)Lcom/google/android/location/copresence/ap;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/location/copresence/ap;->d()Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/location/copresence/r/ao;->a:Lcom/google/android/location/copresence/r/an;

    invoke-static {v1}, Lcom/google/android/location/copresence/r/an;->e(Lcom/google/android/location/copresence/r/an;)Ljava/lang/Runnable;

    move-result-object v1

    invoke-static {}, Lcom/google/android/location/copresence/f/b;->b()Lcom/google/ac/b/c/g;

    move-result-object v2

    iget-object v2, v2, Lcom/google/ac/b/c/g;->j:Lcom/google/ac/b/c/p;

    iget-object v2, v2, Lcom/google/ac/b/c/p;->a:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 100
    :cond_7
    return-void

    :cond_8
    move-object v3, v1

    move v1, v2

    goto/16 :goto_2
.end method
