.class public final Lcom/google/android/location/collectionlib/bt;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public a:Ljava/util/Set;

.field public b:Lcom/google/android/location/collectionlib/aj;

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/String;

.field public e:[B

.field public f:J

.field public g:Z

.field public h:Lcom/google/android/location/collectionlib/SensorScannerConfig;

.field public i:Z

.field public j:Lcom/google/android/location/o/n;

.field private k:J


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 256
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 261
    iput-object v2, p0, Lcom/google/android/location/collectionlib/bt;->c:Ljava/lang/String;

    .line 263
    iput-object v2, p0, Lcom/google/android/location/collectionlib/bt;->d:Ljava/lang/String;

    .line 265
    iput-object v2, p0, Lcom/google/android/location/collectionlib/bt;->e:[B

    .line 268
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/location/collectionlib/bt;->k:J

    .line 271
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/location/collectionlib/bt;->g:Z

    .line 272
    iput-object v2, p0, Lcom/google/android/location/collectionlib/bt;->h:Lcom/google/android/location/collectionlib/SensorScannerConfig;

    .line 275
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/location/collectionlib/bt;->i:Z

    .line 277
    iput-object v2, p0, Lcom/google/android/location/collectionlib/bt;->j:Lcom/google/android/location/o/n;

    return-void
.end method


# virtual methods
.method public final a()Lcom/google/android/location/collectionlib/RealCollectorConfig;
    .locals 18

    .prologue
    .line 475
    new-instance v3, Lcom/google/android/location/collectionlib/RealCollectorConfig;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/location/collectionlib/bt;->a:Ljava/util/Set;

    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/google/android/location/collectionlib/bt;->g:Z

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/google/android/location/collectionlib/bt;->k:J

    move-object/from16 v0, p0

    iget-wide v8, v0, Lcom/google/android/location/collectionlib/bt;->f:J

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/android/location/collectionlib/bt;->h:Lcom/google/android/location/collectionlib/SensorScannerConfig;

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/android/location/collectionlib/bt;->b:Lcom/google/android/location/collectionlib/aj;

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/location/collectionlib/bt;->c:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/location/collectionlib/bt;->d:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/location/collectionlib/bt;->e:[B

    move-object/from16 v0, p0

    iget-boolean v15, v0, Lcom/google/android/location/collectionlib/bt;->i:Z

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/location/collectionlib/bt;->j:Lcom/google/android/location/o/n;

    move-object/from16 v16, v0

    const/16 v17, 0x0

    invoke-direct/range {v3 .. v17}, Lcom/google/android/location/collectionlib/RealCollectorConfig;-><init>(Ljava/util/Set;ZJJLcom/google/android/location/collectionlib/SensorScannerConfig;Lcom/google/android/location/collectionlib/aj;Ljava/lang/String;Ljava/lang/String;[BZLcom/google/android/location/o/n;B)V

    return-object v3
.end method

.method public final a(J)Lcom/google/android/location/collectionlib/bt;
    .locals 1

    .prologue
    .line 401
    iput-wide p1, p0, Lcom/google/android/location/collectionlib/bt;->f:J

    .line 402
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/location/collectionlib/bt;->g:Z

    .line 403
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/location/collectionlib/bt;->h:Lcom/google/android/location/collectionlib/SensorScannerConfig;

    .line 404
    return-object p0
.end method
