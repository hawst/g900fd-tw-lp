.class public final Lcom/google/android/location/l/a/dd;
.super Lcom/google/protobuf/nano/d;
.source "SourceFile"


# instance fields
.field public a:[J

.field public b:[Lcom/google/android/location/l/a/de;

.field public c:[[B

.field public d:[[B

.field public e:Lcom/google/android/location/l/a/cw;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 38
    invoke-direct {p0}, Lcom/google/protobuf/nano/d;-><init>()V

    .line 39
    sget-object v0, Lcom/google/protobuf/nano/m;->b:[J

    iput-object v0, p0, Lcom/google/android/location/l/a/dd;->a:[J

    invoke-static {}, Lcom/google/android/location/l/a/de;->a()[Lcom/google/android/location/l/a/de;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/l/a/dd;->b:[Lcom/google/android/location/l/a/de;

    sget-object v0, Lcom/google/protobuf/nano/m;->g:[[B

    iput-object v0, p0, Lcom/google/android/location/l/a/dd;->c:[[B

    sget-object v0, Lcom/google/protobuf/nano/m;->g:[[B

    iput-object v0, p0, Lcom/google/android/location/l/a/dd;->d:[[B

    iput-object v1, p0, Lcom/google/android/location/l/a/dd;->e:Lcom/google/android/location/l/a/cw;

    iput-object v1, p0, Lcom/google/android/location/l/a/dd;->unknownFieldData:Lcom/google/protobuf/nano/f;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/location/l/a/dd;->cachedSize:I

    .line 40
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 147
    invoke-super {p0}, Lcom/google/protobuf/nano/d;->computeSerializedSize()I

    move-result v3

    .line 148
    iget-object v0, p0, Lcom/google/android/location/l/a/dd;->a:[J

    if-eqz v0, :cond_b

    iget-object v0, p0, Lcom/google/android/location/l/a/dd;->a:[J

    array-length v0, v0

    if-lez v0, :cond_b

    move v0, v1

    move v2, v1

    .line 150
    :goto_0
    iget-object v4, p0, Lcom/google/android/location/l/a/dd;->a:[J

    array-length v4, v4

    if-ge v0, v4, :cond_0

    .line 151
    iget-object v4, p0, Lcom/google/android/location/l/a/dd;->a:[J

    aget-wide v4, v4, v0

    .line 152
    invoke-static {v4, v5}, Lcom/google/protobuf/nano/b;->a(J)I

    move-result v4

    add-int/2addr v2, v4

    .line 150
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 155
    :cond_0
    add-int v0, v3, v2

    .line 156
    iget-object v2, p0, Lcom/google/android/location/l/a/dd;->a:[J

    array-length v2, v2

    mul-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 158
    :goto_1
    iget-object v2, p0, Lcom/google/android/location/l/a/dd;->b:[Lcom/google/android/location/l/a/de;

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/google/android/location/l/a/dd;->b:[Lcom/google/android/location/l/a/de;

    array-length v2, v2

    if-lez v2, :cond_3

    move v2, v0

    move v0, v1

    .line 159
    :goto_2
    iget-object v3, p0, Lcom/google/android/location/l/a/dd;->b:[Lcom/google/android/location/l/a/de;

    array-length v3, v3

    if-ge v0, v3, :cond_2

    .line 160
    iget-object v3, p0, Lcom/google/android/location/l/a/dd;->b:[Lcom/google/android/location/l/a/de;

    aget-object v3, v3, v0

    .line 161
    if-eqz v3, :cond_1

    .line 162
    const/4 v4, 0x3

    invoke-static {v4, v3}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v3

    add-int/2addr v2, v3

    .line 159
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    move v0, v2

    .line 167
    :cond_3
    iget-object v2, p0, Lcom/google/android/location/l/a/dd;->c:[[B

    if-eqz v2, :cond_6

    iget-object v2, p0, Lcom/google/android/location/l/a/dd;->c:[[B

    array-length v2, v2

    if-lez v2, :cond_6

    move v2, v1

    move v3, v1

    move v4, v1

    .line 170
    :goto_3
    iget-object v5, p0, Lcom/google/android/location/l/a/dd;->c:[[B

    array-length v5, v5

    if-ge v2, v5, :cond_5

    .line 171
    iget-object v5, p0, Lcom/google/android/location/l/a/dd;->c:[[B

    aget-object v5, v5, v2

    .line 172
    if-eqz v5, :cond_4

    .line 173
    add-int/lit8 v4, v4, 0x1

    .line 174
    invoke-static {v5}, Lcom/google/protobuf/nano/b;->b([B)I

    move-result v5

    add-int/2addr v3, v5

    .line 170
    :cond_4
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    .line 178
    :cond_5
    add-int/2addr v0, v3

    .line 179
    mul-int/lit8 v2, v4, 0x1

    add-int/2addr v0, v2

    .line 181
    :cond_6
    iget-object v2, p0, Lcom/google/android/location/l/a/dd;->d:[[B

    if-eqz v2, :cond_9

    iget-object v2, p0, Lcom/google/android/location/l/a/dd;->d:[[B

    array-length v2, v2

    if-lez v2, :cond_9

    move v2, v1

    move v3, v1

    .line 184
    :goto_4
    iget-object v4, p0, Lcom/google/android/location/l/a/dd;->d:[[B

    array-length v4, v4

    if-ge v1, v4, :cond_8

    .line 185
    iget-object v4, p0, Lcom/google/android/location/l/a/dd;->d:[[B

    aget-object v4, v4, v1

    .line 186
    if-eqz v4, :cond_7

    .line 187
    add-int/lit8 v3, v3, 0x1

    .line 188
    invoke-static {v4}, Lcom/google/protobuf/nano/b;->b([B)I

    move-result v4

    add-int/2addr v2, v4

    .line 184
    :cond_7
    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    .line 192
    :cond_8
    add-int/2addr v0, v2

    .line 193
    mul-int/lit8 v1, v3, 0x1

    add-int/2addr v0, v1

    .line 195
    :cond_9
    iget-object v1, p0, Lcom/google/android/location/l/a/dd;->e:Lcom/google/android/location/l/a/cw;

    if-eqz v1, :cond_a

    .line 196
    const/4 v1, 0x6

    iget-object v2, p0, Lcom/google/android/location/l/a/dd;->e:Lcom/google/android/location/l/a/cw;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 199
    :cond_a
    return v0

    :cond_b
    move v0, v3

    goto :goto_1
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 55
    if-ne p1, p0, :cond_1

    .line 56
    const/4 v0, 0x1

    .line 87
    :cond_0
    :goto_0
    return v0

    .line 58
    :cond_1
    instance-of v1, p1, Lcom/google/android/location/l/a/dd;

    if-eqz v1, :cond_0

    .line 61
    check-cast p1, Lcom/google/android/location/l/a/dd;

    .line 62
    iget-object v1, p0, Lcom/google/android/location/l/a/dd;->a:[J

    iget-object v2, p1, Lcom/google/android/location/l/a/dd;->a:[J

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/h;->a([J[J)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 66
    iget-object v1, p0, Lcom/google/android/location/l/a/dd;->b:[Lcom/google/android/location/l/a/de;

    iget-object v2, p1, Lcom/google/android/location/l/a/dd;->b:[Lcom/google/android/location/l/a/de;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 70
    iget-object v1, p0, Lcom/google/android/location/l/a/dd;->c:[[B

    iget-object v2, p1, Lcom/google/android/location/l/a/dd;->c:[[B

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/h;->a([[B[[B)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 74
    iget-object v1, p0, Lcom/google/android/location/l/a/dd;->d:[[B

    iget-object v2, p1, Lcom/google/android/location/l/a/dd;->d:[[B

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/h;->a([[B[[B)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 78
    iget-object v1, p0, Lcom/google/android/location/l/a/dd;->e:Lcom/google/android/location/l/a/cw;

    if-nez v1, :cond_3

    .line 79
    iget-object v1, p1, Lcom/google/android/location/l/a/dd;->e:Lcom/google/android/location/l/a/cw;

    if-nez v1, :cond_0

    .line 87
    :cond_2
    invoke-virtual {p0, p1}, Lcom/google/android/location/l/a/dd;->unknownFieldDataEquals(Lcom/google/protobuf/nano/d;)Z

    move-result v0

    goto :goto_0

    .line 83
    :cond_3
    iget-object v1, p0, Lcom/google/android/location/l/a/dd;->e:Lcom/google/android/location/l/a/cw;

    iget-object v2, p1, Lcom/google/android/location/l/a/dd;->e:Lcom/google/android/location/l/a/cw;

    invoke-virtual {v1, v2}, Lcom/google/android/location/l/a/cw;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 2

    .prologue
    .line 92
    iget-object v0, p0, Lcom/google/android/location/l/a/dd;->a:[J

    invoke-static {v0}, Lcom/google/protobuf/nano/h;->a([J)I

    move-result v0

    add-int/lit16 v0, v0, 0x20f

    .line 95
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/google/android/location/l/a/dd;->b:[Lcom/google/android/location/l/a/de;

    invoke-static {v1}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 97
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/google/android/location/l/a/dd;->c:[[B

    invoke-static {v1}, Lcom/google/protobuf/nano/h;->a([[B)I

    move-result v1

    add-int/2addr v0, v1

    .line 99
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/google/android/location/l/a/dd;->d:[[B

    invoke-static {v1}, Lcom/google/protobuf/nano/h;->a([[B)I

    move-result v1

    add-int/2addr v0, v1

    .line 101
    mul-int/lit8 v1, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/location/l/a/dd;->e:Lcom/google/android/location/l/a/cw;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    add-int/2addr v0, v1

    .line 103
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/google/android/location/l/a/dd;->unknownFieldDataHashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 104
    return v0

    .line 101
    :cond_0
    iget-object v0, p0, Lcom/google/android/location/l/a/dd;->e:Lcom/google/android/location/l/a/cw;

    invoke-virtual {v0}, Lcom/google/android/location/l/a/cw;->hashCode()I

    move-result v0

    goto :goto_0
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 5
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/android/location/l/a/dd;->storeUnknownField(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    const/16 v0, 0x10

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->b(Lcom/google/protobuf/nano/a;I)I

    move-result v2

    iget-object v0, p0, Lcom/google/android/location/l/a/dd;->a:[J

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [J

    if-eqz v0, :cond_1

    iget-object v3, p0, Lcom/google/android/location/l/a/dd;->a:[J

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_1
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_3

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->j()J

    move-result-wide v4

    aput-wide v4, v2, v0

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    iget-object v0, p0, Lcom/google/android/location/l/a/dd;->a:[J

    array-length v0, v0

    goto :goto_1

    :cond_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->j()J

    move-result-wide v4

    aput-wide v4, v2, v0

    iput-object v2, p0, Lcom/google/android/location/l/a/dd;->a:[J

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->c(I)I

    move-result v3

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->o()I

    move-result v2

    move v0, v1

    :goto_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->m()I

    move-result v4

    if-lez v4, :cond_4

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->j()J

    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    :cond_4
    invoke-virtual {p1, v2}, Lcom/google/protobuf/nano/a;->e(I)V

    iget-object v2, p0, Lcom/google/android/location/l/a/dd;->a:[J

    if-nez v2, :cond_6

    move v2, v1

    :goto_4
    add-int/2addr v0, v2

    new-array v0, v0, [J

    if-eqz v2, :cond_5

    iget-object v4, p0, Lcom/google/android/location/l/a/dd;->a:[J

    invoke-static {v4, v1, v0, v1, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_5
    :goto_5
    array-length v4, v0

    if-ge v2, v4, :cond_7

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->j()J

    move-result-wide v4

    aput-wide v4, v0, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_5

    :cond_6
    iget-object v2, p0, Lcom/google/android/location/l/a/dd;->a:[J

    array-length v2, v2

    goto :goto_4

    :cond_7
    iput-object v0, p0, Lcom/google/android/location/l/a/dd;->a:[J

    invoke-virtual {p1, v3}, Lcom/google/protobuf/nano/a;->d(I)V

    goto/16 :goto_0

    :sswitch_3
    const/16 v0, 0x1a

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->b(Lcom/google/protobuf/nano/a;I)I

    move-result v2

    iget-object v0, p0, Lcom/google/android/location/l/a/dd;->b:[Lcom/google/android/location/l/a/de;

    if-nez v0, :cond_9

    move v0, v1

    :goto_6
    add-int/2addr v2, v0

    new-array v2, v2, [Lcom/google/android/location/l/a/de;

    if-eqz v0, :cond_8

    iget-object v3, p0, Lcom/google/android/location/l/a/dd;->b:[Lcom/google/android/location/l/a/de;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_8
    :goto_7
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_a

    new-instance v3, Lcom/google/android/location/l/a/de;

    invoke-direct {v3}, Lcom/google/android/location/l/a/de;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_7

    :cond_9
    iget-object v0, p0, Lcom/google/android/location/l/a/dd;->b:[Lcom/google/android/location/l/a/de;

    array-length v0, v0

    goto :goto_6

    :cond_a
    new-instance v3, Lcom/google/android/location/l/a/de;

    invoke-direct {v3}, Lcom/google/android/location/l/a/de;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    iput-object v2, p0, Lcom/google/android/location/l/a/dd;->b:[Lcom/google/android/location/l/a/de;

    goto/16 :goto_0

    :sswitch_4
    const/16 v0, 0x22

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->b(Lcom/google/protobuf/nano/a;I)I

    move-result v2

    iget-object v0, p0, Lcom/google/android/location/l/a/dd;->c:[[B

    if-nez v0, :cond_c

    move v0, v1

    :goto_8
    add-int/2addr v2, v0

    new-array v2, v2, [[B

    if-eqz v0, :cond_b

    iget-object v3, p0, Lcom/google/android/location/l/a/dd;->c:[[B

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_b
    :goto_9
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_d

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->f()[B

    move-result-object v3

    aput-object v3, v2, v0

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_9

    :cond_c
    iget-object v0, p0, Lcom/google/android/location/l/a/dd;->c:[[B

    array-length v0, v0

    goto :goto_8

    :cond_d
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->f()[B

    move-result-object v3

    aput-object v3, v2, v0

    iput-object v2, p0, Lcom/google/android/location/l/a/dd;->c:[[B

    goto/16 :goto_0

    :sswitch_5
    const/16 v0, 0x2a

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->b(Lcom/google/protobuf/nano/a;I)I

    move-result v2

    iget-object v0, p0, Lcom/google/android/location/l/a/dd;->d:[[B

    if-nez v0, :cond_f

    move v0, v1

    :goto_a
    add-int/2addr v2, v0

    new-array v2, v2, [[B

    if-eqz v0, :cond_e

    iget-object v3, p0, Lcom/google/android/location/l/a/dd;->d:[[B

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_e
    :goto_b
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_10

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->f()[B

    move-result-object v3

    aput-object v3, v2, v0

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_b

    :cond_f
    iget-object v0, p0, Lcom/google/android/location/l/a/dd;->d:[[B

    array-length v0, v0

    goto :goto_a

    :cond_10
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->f()[B

    move-result-object v3

    aput-object v3, v2, v0

    iput-object v2, p0, Lcom/google/android/location/l/a/dd;->d:[[B

    goto/16 :goto_0

    :sswitch_6
    iget-object v0, p0, Lcom/google/android/location/l/a/dd;->e:Lcom/google/android/location/l/a/cw;

    if-nez v0, :cond_11

    new-instance v0, Lcom/google/android/location/l/a/cw;

    invoke-direct {v0}, Lcom/google/android/location/l/a/cw;-><init>()V

    iput-object v0, p0, Lcom/google/android/location/l/a/dd;->e:Lcom/google/android/location/l/a/cw;

    :cond_11
    iget-object v0, p0, Lcom/google/android/location/l/a/dd;->e:Lcom/google/android/location/l/a/cw;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x10 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 110
    iget-object v0, p0, Lcom/google/android/location/l/a/dd;->a:[J

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/location/l/a/dd;->a:[J

    array-length v0, v0

    if-lez v0, :cond_0

    move v0, v1

    .line 111
    :goto_0
    iget-object v2, p0, Lcom/google/android/location/l/a/dd;->a:[J

    array-length v2, v2

    if-ge v0, v2, :cond_0

    .line 112
    const/4 v2, 0x2

    iget-object v3, p0, Lcom/google/android/location/l/a/dd;->a:[J

    aget-wide v4, v3, v0

    invoke-virtual {p1, v2, v4, v5}, Lcom/google/protobuf/nano/b;->b(IJ)V

    .line 111
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 115
    :cond_0
    iget-object v0, p0, Lcom/google/android/location/l/a/dd;->b:[Lcom/google/android/location/l/a/de;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/location/l/a/dd;->b:[Lcom/google/android/location/l/a/de;

    array-length v0, v0

    if-lez v0, :cond_2

    move v0, v1

    .line 116
    :goto_1
    iget-object v2, p0, Lcom/google/android/location/l/a/dd;->b:[Lcom/google/android/location/l/a/de;

    array-length v2, v2

    if-ge v0, v2, :cond_2

    .line 117
    iget-object v2, p0, Lcom/google/android/location/l/a/dd;->b:[Lcom/google/android/location/l/a/de;

    aget-object v2, v2, v0

    .line 118
    if-eqz v2, :cond_1

    .line 119
    const/4 v3, 0x3

    invoke-virtual {p1, v3, v2}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 116
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 123
    :cond_2
    iget-object v0, p0, Lcom/google/android/location/l/a/dd;->c:[[B

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/google/android/location/l/a/dd;->c:[[B

    array-length v0, v0

    if-lez v0, :cond_4

    move v0, v1

    .line 124
    :goto_2
    iget-object v2, p0, Lcom/google/android/location/l/a/dd;->c:[[B

    array-length v2, v2

    if-ge v0, v2, :cond_4

    .line 125
    iget-object v2, p0, Lcom/google/android/location/l/a/dd;->c:[[B

    aget-object v2, v2, v0

    .line 126
    if-eqz v2, :cond_3

    .line 127
    const/4 v3, 0x4

    invoke-virtual {p1, v3, v2}, Lcom/google/protobuf/nano/b;->a(I[B)V

    .line 124
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 131
    :cond_4
    iget-object v0, p0, Lcom/google/android/location/l/a/dd;->d:[[B

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/google/android/location/l/a/dd;->d:[[B

    array-length v0, v0

    if-lez v0, :cond_6

    .line 132
    :goto_3
    iget-object v0, p0, Lcom/google/android/location/l/a/dd;->d:[[B

    array-length v0, v0

    if-ge v1, v0, :cond_6

    .line 133
    iget-object v0, p0, Lcom/google/android/location/l/a/dd;->d:[[B

    aget-object v0, v0, v1

    .line 134
    if-eqz v0, :cond_5

    .line 135
    const/4 v2, 0x5

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/b;->a(I[B)V

    .line 132
    :cond_5
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 139
    :cond_6
    iget-object v0, p0, Lcom/google/android/location/l/a/dd;->e:Lcom/google/android/location/l/a/cw;

    if-eqz v0, :cond_7

    .line 140
    const/4 v0, 0x6

    iget-object v1, p0, Lcom/google/android/location/l/a/dd;->e:Lcom/google/android/location/l/a/cw;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 142
    :cond_7
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/d;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 143
    return-void
.end method
