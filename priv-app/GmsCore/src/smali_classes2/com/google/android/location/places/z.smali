.class final Lcom/google/android/location/places/z;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/location/places/ac;


# instance fields
.field final synthetic a:Lcom/google/android/location/places/y;


# direct methods
.method constructor <init>(Lcom/google/android/location/places/y;)V
    .locals 0

    .prologue
    .line 155
    iput-object p1, p0, Lcom/google/android/location/places/z;->a:Lcom/google/android/location/places/y;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/location/places/ae;)V
    .locals 5

    .prologue
    .line 158
    invoke-static {}, Lcom/google/android/location/places/p;->a()Lcom/google/android/location/o/a/c;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Received inference result:  "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/location/o/a/c;->a(Ljava/lang/String;)V

    .line 159
    iget-object v0, p0, Lcom/google/android/location/places/z;->a:Lcom/google/android/location/places/y;

    iget-object v1, v0, Lcom/google/android/location/places/y;->g:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v2, v0, Lcom/google/android/location/places/y;->f:Lcom/google/android/location/places/ae;

    if-eqz v2, :cond_1

    invoke-static {}, Lcom/google/android/location/places/p;->a()Lcom/google/android/location/o/a/c;

    move-result-object v2

    const-string v3, "Fusing inferences..."

    invoke-virtual {v2, v3}, Lcom/google/android/location/o/a/c;->a(Ljava/lang/String;)V

    invoke-static {}, Lcom/google/android/location/places/p;->a()Lcom/google/android/location/o/a/c;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Inference result: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, v0, Lcom/google/android/location/places/y;->f:Lcom/google/android/location/places/ae;

    invoke-virtual {v4}, Lcom/google/android/location/places/ae;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/android/location/o/a/c;->a(Ljava/lang/String;)V

    iget-object v2, v0, Lcom/google/android/location/places/y;->a:Lcom/google/android/location/places/aa;

    iget-object v3, v0, Lcom/google/android/location/places/y;->f:Lcom/google/android/location/places/ae;

    invoke-virtual {v2, v3, p1}, Lcom/google/android/location/places/aa;->a(Lcom/google/android/location/places/ae;Lcom/google/android/location/places/ae;)Lcom/google/android/location/places/ae;

    move-result-object v2

    iput-object v2, v0, Lcom/google/android/location/places/y;->f:Lcom/google/android/location/places/ae;

    invoke-static {}, Lcom/google/android/location/places/p;->a()Lcom/google/android/location/o/a/c;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Inference result after fusion: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, v0, Lcom/google/android/location/places/y;->f:Lcom/google/android/location/places/ae;

    invoke-virtual {v4}, Lcom/google/android/location/places/ae;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/android/location/o/a/c;->a(Ljava/lang/String;)V

    :goto_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v1, v0, Lcom/google/android/location/places/y;->h:Lcom/google/android/location/places/x;

    if-eqz v1, :cond_0

    iget-object v1, v0, Lcom/google/android/location/places/y;->h:Lcom/google/android/location/places/x;

    iget-object v0, v0, Lcom/google/android/location/places/y;->f:Lcom/google/android/location/places/ae;

    iget-object v0, v0, Lcom/google/android/location/places/ae;->a:Ljava/util/List;

    invoke-interface {v1, v0}, Lcom/google/android/location/places/x;->a(Ljava/util/List;)V

    .line 160
    :cond_0
    return-void

    .line 159
    :cond_1
    :try_start_1
    iput-object p1, v0, Lcom/google/android/location/places/y;->f:Lcom/google/android/location/places/ae;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method
