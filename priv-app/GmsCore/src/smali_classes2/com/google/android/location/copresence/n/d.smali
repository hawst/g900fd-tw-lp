.class final Lcom/google/android/location/copresence/n/d;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/content/ServiceConnection;


# instance fields
.field final synthetic a:Lcom/google/android/location/copresence/n/c;


# direct methods
.method private constructor <init>(Lcom/google/android/location/copresence/n/c;)V
    .locals 0

    .prologue
    .line 91
    iput-object p1, p0, Lcom/google/android/location/copresence/n/d;->a:Lcom/google/android/location/copresence/n/c;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/location/copresence/n/c;B)V
    .locals 0

    .prologue
    .line 91
    invoke-direct {p0, p1}, Lcom/google/android/location/copresence/n/d;-><init>(Lcom/google/android/location/copresence/n/c;)V

    return-void
.end method


# virtual methods
.method public final onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 3

    .prologue
    .line 94
    iget-object v0, p0, Lcom/google/android/location/copresence/n/d;->a:Lcom/google/android/location/copresence/n/c;

    invoke-static {v0}, Lcom/google/android/location/copresence/n/c;->a(Lcom/google/android/location/copresence/n/c;)Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1

    .line 95
    :try_start_0
    iget-object v0, p0, Lcom/google/android/location/copresence/n/d;->a:Lcom/google/android/location/copresence/n/c;

    invoke-static {p2}, Lcom/google/android/location/reporting/service/m;->a(Landroid/os/IBinder;)Lcom/google/android/location/reporting/service/l;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/google/android/location/copresence/n/c;->a(Lcom/google/android/location/copresence/n/c;Lcom/google/android/location/reporting/service/l;)Lcom/google/android/location/reporting/service/l;

    .line 96
    iget-object v0, p0, Lcom/google/android/location/copresence/n/d;->a:Lcom/google/android/location/copresence/n/c;

    invoke-static {v0}, Lcom/google/android/location/copresence/n/c;->a(Lcom/google/android/location/copresence/n/c;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->notifyAll()V

    .line 97
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 3

    .prologue
    .line 102
    iget-object v0, p0, Lcom/google/android/location/copresence/n/d;->a:Lcom/google/android/location/copresence/n/c;

    invoke-static {v0}, Lcom/google/android/location/copresence/n/c;->a(Lcom/google/android/location/copresence/n/c;)Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1

    .line 103
    :try_start_0
    iget-object v0, p0, Lcom/google/android/location/copresence/n/d;->a:Lcom/google/android/location/copresence/n/c;

    const/4 v2, 0x0

    invoke-static {v0, v2}, Lcom/google/android/location/copresence/n/c;->a(Lcom/google/android/location/copresence/n/c;Lcom/google/android/location/reporting/service/l;)Lcom/google/android/location/reporting/service/l;

    .line 104
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method
