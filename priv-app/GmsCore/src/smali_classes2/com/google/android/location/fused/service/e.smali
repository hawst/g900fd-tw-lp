.class public final Lcom/google/android/location/fused/service/e;
.super Lcom/android/location/provider/FusedProvider;
.source "SourceFile"


# static fields
.field private static volatile a:Z

.field private static final b:Ljava/lang/Object;

.field private static c:Lcom/google/android/location/fused/service/e;


# instance fields
.field private volatile d:Lcom/android/location/provider/FusedLocationHardware;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 17
    const/4 v0, 0x0

    sput-boolean v0, Lcom/google/android/location/fused/service/e;->a:Z

    .line 18
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/google/android/location/fused/service/e;->b:Ljava/lang/Object;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0}, Lcom/android/location/provider/FusedProvider;-><init>()V

    return-void
.end method

.method public static a()Lcom/google/android/location/fused/service/e;
    .locals 2

    .prologue
    .line 25
    sget-object v1, Lcom/google/android/location/fused/service/e;->b:Ljava/lang/Object;

    monitor-enter v1

    .line 26
    :try_start_0
    sget-object v0, Lcom/google/android/location/fused/service/e;->c:Lcom/google/android/location/fused/service/e;

    if-nez v0, :cond_0

    .line 27
    new-instance v0, Lcom/google/android/location/fused/service/e;

    invoke-direct {v0}, Lcom/google/android/location/fused/service/e;-><init>()V

    sput-object v0, Lcom/google/android/location/fused/service/e;->c:Lcom/google/android/location/fused/service/e;

    .line 30
    :cond_0
    sget-object v0, Lcom/google/android/location/fused/service/e;->c:Lcom/google/android/location/fused/service/e;

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v0

    .line 31
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method static c()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 64
    sget-boolean v0, Lcom/google/android/location/fused/service/e;->a:Z

    if-nez v0, :cond_0

    invoke-static {}, Lcom/google/android/location/fused/service/a;->a()Z

    move-result v0

    if-nez v0, :cond_1

    .line 78
    :cond_0
    :goto_0
    return-void

    .line 69
    :cond_1
    sput-boolean v2, Lcom/google/android/location/fused/service/e;->a:Z

    .line 70
    invoke-static {}, Lcom/google/android/location/fused/service/e;->a()Lcom/google/android/location/fused/service/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/location/fused/service/e;->b()Lcom/android/location/provider/FusedLocationHardware;

    move-result-object v0

    .line 71
    if-eqz v0, :cond_0

    .line 72
    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Lcom/android/location/provider/FusedLocationHardware;->stopBatching(I)V

    .line 73
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/android/location/provider/FusedLocationHardware;->stopBatching(I)V

    .line 74
    invoke-virtual {v0, v2}, Lcom/android/location/provider/FusedLocationHardware;->stopBatching(I)V

    .line 76
    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/android/location/provider/FusedLocationHardware;->stopBatching(I)V

    goto :goto_0
.end method


# virtual methods
.method public final b()Lcom/android/location/provider/FusedLocationHardware;
    .locals 1

    .prologue
    .line 35
    invoke-static {}, Lcom/google/android/location/fused/service/a;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 36
    const/4 v0, 0x0

    .line 38
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/location/fused/service/e;->d:Lcom/android/location/provider/FusedLocationHardware;

    goto :goto_0
.end method

.method public final setFusedLocationHardware(Lcom/android/location/provider/FusedLocationHardware;)V
    .locals 4

    .prologue
    .line 44
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v0

    .line 45
    const/16 v1, 0x3e8

    if-eq v0, v1, :cond_0

    .line 46
    const-string v1, "GCoreFlp"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Ignoring calls from non-system server. Uid:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 54
    :goto_0
    return-void

    .line 50
    :cond_0
    iput-object p1, p0, Lcom/google/android/location/fused/service/e;->d:Lcom/android/location/provider/FusedLocationHardware;

    .line 53
    invoke-static {}, Lcom/google/android/location/fused/service/e;->c()V

    goto :goto_0
.end method
