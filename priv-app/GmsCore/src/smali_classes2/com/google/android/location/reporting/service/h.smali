.class abstract Lcom/google/android/location/reporting/service/h;
.super Landroid/os/Handler;
.source "SourceFile"


# instance fields
.field private final a:Landroid/os/Looper;

.field final synthetic b:Lcom/google/android/location/reporting/service/DispatchingService;


# direct methods
.method private constructor <init>(Lcom/google/android/location/reporting/service/DispatchingService;Landroid/os/Looper;)V
    .locals 0

    .prologue
    .line 389
    iput-object p1, p0, Lcom/google/android/location/reporting/service/h;->b:Lcom/google/android/location/reporting/service/DispatchingService;

    .line 390
    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 391
    iput-object p2, p0, Lcom/google/android/location/reporting/service/h;->a:Landroid/os/Looper;

    .line 392
    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/location/reporting/service/DispatchingService;Landroid/os/Looper;B)V
    .locals 0

    .prologue
    .line 386
    invoke-direct {p0, p1, p2}, Lcom/google/android/location/reporting/service/h;-><init>(Lcom/google/android/location/reporting/service/DispatchingService;Landroid/os/Looper;)V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 395
    iget-object v0, p0, Lcom/google/android/location/reporting/service/h;->a:Landroid/os/Looper;

    invoke-virtual {v0}, Landroid/os/Looper;->quit()V

    .line 396
    return-void
.end method

.method protected abstract a(Landroid/content/Intent;)V
.end method

.method public handleMessage(Landroid/os/Message;)V
    .locals 4

    .prologue
    .line 400
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Landroid/content/Intent;

    .line 401
    iget-object v1, p0, Lcom/google/android/location/reporting/service/h;->b:Lcom/google/android/location/reporting/service/DispatchingService;

    invoke-static {v1}, Lcom/google/android/location/reporting/service/DispatchingService;->a(Lcom/google/android/location/reporting/service/DispatchingService;)Lcom/google/android/location/n/ae;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/location/n/ae;->a()Z

    move-result v1

    if-nez v1, :cond_1

    .line 402
    const-string v1, "GCoreUlr"

    const/4 v2, 0x3

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 403
    const-string v1, "GCoreUlr"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "DispatchingService ignoring intent for non-foreground user: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 414
    :cond_0
    :goto_0
    return-void

    .line 410
    :cond_1
    :try_start_0
    iget-object v1, p0, Lcom/google/android/location/reporting/service/h;->b:Lcom/google/android/location/reporting/service/DispatchingService;

    invoke-static {v1}, Lcom/google/android/location/reporting/service/DispatchingService;->b(Lcom/google/android/location/reporting/service/DispatchingService;)Landroid/os/PowerManager$WakeLock;

    move-result-object v1

    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->acquire()V

    .line 411
    invoke-virtual {p0, v0}, Lcom/google/android/location/reporting/service/h;->a(Landroid/content/Intent;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 413
    iget-object v0, p0, Lcom/google/android/location/reporting/service/h;->b:Lcom/google/android/location/reporting/service/DispatchingService;

    invoke-static {v0}, Lcom/google/android/location/reporting/service/DispatchingService;->b(Lcom/google/android/location/reporting/service/DispatchingService;)Landroid/os/PowerManager$WakeLock;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V

    goto :goto_0

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/google/android/location/reporting/service/h;->b:Lcom/google/android/location/reporting/service/DispatchingService;

    invoke-static {v1}, Lcom/google/android/location/reporting/service/DispatchingService;->b(Lcom/google/android/location/reporting/service/DispatchingService;)Landroid/os/PowerManager$WakeLock;

    move-result-object v1

    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->release()V

    throw v0
.end method
