.class final Lcom/google/android/location/copresence/i/b;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/location/copresence/i/d;


# instance fields
.field private final a:Landroid/app/NotificationManager;

.field private final b:Landroid/content/Context;

.field private final c:Landroid/app/PendingIntent;


# direct methods
.method constructor <init>(Landroid/content/Context;)V
    .locals 4

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    const-string v0, "notification"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    iput-object v0, p0, Lcom/google/android/location/copresence/i/b;->a:Landroid/app/NotificationManager;

    .line 32
    iput-object p1, p0, Lcom/google/android/location/copresence/i/b;->b:Landroid/content/Context;

    .line 33
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const/4 v1, 0x0

    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    const/high16 v3, 0x8000000

    invoke-static {v0, v1, v2, v3}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/copresence/i/b;->c:Landroid/app/PendingIntent;

    .line 35
    return-void
.end method


# virtual methods
.method public final a(Z)V
    .locals 8

    .prologue
    .line 53
    iget-object v0, p0, Lcom/google/android/location/copresence/i/b;->a:Landroid/app/NotificationManager;

    const v1, 0x1ffff

    iget-object v2, p0, Lcom/google/android/location/copresence/i/b;->b:Landroid/content/Context;

    sget v3, Lcom/google/android/gms/p;->fn:I

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Landroid/support/v4/app/bk;

    invoke-direct {v4, v2}, Landroid/support/v4/app/bk;-><init>(Landroid/content/Context;)V

    sget v2, Lcom/google/android/gms/h;->dh:I

    invoke-virtual {v4, v2}, Landroid/support/v4/app/bk;->a(I)Landroid/support/v4/app/bk;

    move-result-object v2

    invoke-virtual {v2, v3}, Landroid/support/v4/app/bk;->a(Ljava/lang/CharSequence;)Landroid/support/v4/app/bk;

    move-result-object v2

    invoke-virtual {v2}, Landroid/support/v4/app/bk;->a()Landroid/support/v4/app/bk;

    move-result-object v2

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    invoke-virtual {v2, v6, v7}, Landroid/support/v4/app/bk;->a(J)Landroid/support/v4/app/bk;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/location/copresence/i/b;->c:Landroid/app/PendingIntent;

    iput-object v3, v2, Landroid/support/v4/app/bk;->d:Landroid/app/PendingIntent;

    invoke-virtual {v4}, Landroid/support/v4/app/bk;->b()Landroid/app/Notification;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    .line 54
    return-void
.end method

.method public final b(Z)V
    .locals 2

    .prologue
    .line 58
    iget-object v0, p0, Lcom/google/android/location/copresence/i/b;->a:Landroid/app/NotificationManager;

    const v1, 0x1ffff

    invoke-virtual {v0, v1}, Landroid/app/NotificationManager;->cancel(I)V

    .line 59
    return-void
.end method
