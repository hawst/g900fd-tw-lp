.class public final Lcom/google/android/location/e/b;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public final a:J

.field public final b:J


# direct methods
.method public constructor <init>(JJ)V
    .locals 1

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    iput-wide p1, p0, Lcom/google/android/location/e/b;->a:J

    .line 24
    iput-wide p3, p0, Lcom/google/android/location/e/b;->b:J

    .line 25
    return-void
.end method


# virtual methods
.method public final equals(Ljava/lang/Object;)Z
    .locals 8

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 59
    if-eqz p1, :cond_1

    instance-of v2, p1, Lcom/google/android/location/e/b;

    if-eqz v2, :cond_1

    .line 60
    check-cast p1, Lcom/google/android/location/e/b;

    .line 61
    if-eq p0, p1, :cond_0

    iget-wide v2, p1, Lcom/google/android/location/e/b;->a:J

    iget-wide v4, p1, Lcom/google/android/location/e/b;->b:J

    iget-wide v6, p0, Lcom/google/android/location/e/b;->a:J

    cmp-long v2, v6, v2

    if-nez v2, :cond_2

    iget-wide v2, p0, Lcom/google/android/location/e/b;->b:J

    cmp-long v2, v2, v4

    if-nez v2, :cond_2

    move v2, v1

    :goto_0
    if-eqz v2, :cond_1

    :cond_0
    move v0, v1

    .line 63
    :cond_1
    return v0

    :cond_2
    move v2, v0

    .line 61
    goto :goto_0
.end method

.method public final hashCode()I
    .locals 7

    .prologue
    const/16 v6, 0x20

    .line 50
    iget-wide v0, p0, Lcom/google/android/location/e/b;->b:J

    iget-wide v2, p0, Lcom/google/android/location/e/b;->b:J

    ushr-long/2addr v2, v6

    xor-long/2addr v0, v2

    long-to-int v0, v0

    add-int/lit8 v0, v0, 0x1f

    .line 53
    mul-int/lit8 v0, v0, 0x1f

    iget-wide v2, p0, Lcom/google/android/location/e/b;->a:J

    iget-wide v4, p0, Lcom/google/android/location/e/b;->a:J

    ushr-long/2addr v4, v6

    xor-long/2addr v2, v4

    long-to-int v1, v2

    add-int/2addr v0, v1

    .line 54
    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 8

    .prologue
    .line 44
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v1, "AlarmWindow[begin=%d, length=%d, end=%d]"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-wide v4, p0, Lcom/google/android/location/e/b;->a:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    iget-wide v4, p0, Lcom/google/android/location/e/b;->b:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x2

    iget-wide v4, p0, Lcom/google/android/location/e/b;->a:J

    iget-wide v6, p0, Lcom/google/android/location/e/b;->b:J

    add-long/2addr v4, v6

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
