.class public final Lcom/google/android/location/copresence/e/d;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static c:Lcom/google/android/location/copresence/e/d;


# instance fields
.field final a:Landroid/content/Context;

.field private final b:Ljava/util/HashSet;

.field private final d:Ljava/util/HashMap;

.field private final e:Ljava/util/HashMap;

.field private final f:Lcom/google/android/location/copresence/ap;

.field private g:Ljava/lang/Runnable;


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 277
    invoke-static {p1}, Lcom/google/android/location/copresence/ap;->a(Landroid/content/Context;)Lcom/google/android/location/copresence/ap;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lcom/google/android/location/copresence/e/d;-><init>(Landroid/content/Context;Lcom/google/android/location/copresence/ap;)V

    .line 278
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Lcom/google/android/location/copresence/ap;)V
    .locals 1

    .prologue
    .line 284
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 358
    new-instance v0, Lcom/google/android/location/copresence/e/e;

    invoke-direct {v0, p0}, Lcom/google/android/location/copresence/e/e;-><init>(Lcom/google/android/location/copresence/e/d;)V

    iput-object v0, p0, Lcom/google/android/location/copresence/e/d;->g:Ljava/lang/Runnable;

    .line 285
    iput-object p1, p0, Lcom/google/android/location/copresence/e/d;->a:Landroid/content/Context;

    .line 286
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/location/copresence/e/d;->d:Ljava/util/HashMap;

    .line 287
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/location/copresence/e/d;->e:Ljava/util/HashMap;

    .line 288
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/location/copresence/e/d;->b:Ljava/util/HashSet;

    .line 289
    iput-object p2, p0, Lcom/google/android/location/copresence/e/d;->f:Lcom/google/android/location/copresence/ap;

    .line 290
    return-void
.end method

.method public static declared-synchronized a(Landroid/content/Context;)Lcom/google/android/location/copresence/e/d;
    .locals 2

    .prologue
    .line 265
    const-class v1, Lcom/google/android/location/copresence/e/d;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/google/android/location/copresence/e/d;->c:Lcom/google/android/location/copresence/e/d;

    if-nez v0, :cond_0

    .line 266
    new-instance v0, Lcom/google/android/location/copresence/e/d;

    invoke-direct {v0, p0}, Lcom/google/android/location/copresence/e/d;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/google/android/location/copresence/e/d;->c:Lcom/google/android/location/copresence/e/d;

    .line 268
    :cond_0
    sget-object v0, Lcom/google/android/location/copresence/e/d;->c:Lcom/google/android/location/copresence/e/d;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 265
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private a(Lcom/google/android/location/copresence/e/g;)Lcom/google/android/location/copresence/e/f;
    .locals 1

    .prologue
    .line 459
    invoke-virtual {p0}, Lcom/google/android/location/copresence/e/d;->a()V

    .line 461
    iget-object v0, p0, Lcom/google/android/location/copresence/e/d;->d:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/copresence/e/f;

    return-object v0
.end method

.method private a(Lcom/google/android/location/copresence/e/g;Lcom/google/android/gms/location/copresence/MessageFilter;Lcom/google/android/gms/location/copresence/u;)Lcom/google/android/location/copresence/e/f;
    .locals 5

    .prologue
    const/4 v4, 0x3

    .line 332
    iget-object v0, p0, Lcom/google/android/location/copresence/e/d;->d:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/copresence/e/f;

    .line 333
    if-eqz v0, :cond_1

    .line 334
    invoke-static {v4}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 335
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "MessageFilterCache: Removed entry(size="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/google/android/location/copresence/e/d;->c()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "): "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/location/copresence/ag;->b(Ljava/lang/String;)I

    .line 338
    :cond_0
    invoke-direct {p0, v0}, Lcom/google/android/location/copresence/e/d;->d(Lcom/google/android/location/copresence/e/f;)V

    .line 339
    invoke-direct {p0, v0}, Lcom/google/android/location/copresence/e/d;->c(Lcom/google/android/location/copresence/e/f;)V

    .line 343
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/location/copresence/e/d;->a()V

    .line 346
    new-instance v1, Lcom/google/android/location/copresence/e/f;

    invoke-direct {v1, p0, p1, p2, p3}, Lcom/google/android/location/copresence/e/f;-><init>(Lcom/google/android/location/copresence/e/d;Lcom/google/android/location/copresence/e/g;Lcom/google/android/gms/location/copresence/MessageFilter;Lcom/google/android/gms/location/copresence/u;)V

    .line 347
    iget-object v0, p0, Lcom/google/android/location/copresence/e/d;->d:Ljava/util/HashMap;

    invoke-virtual {v0, p1, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 348
    invoke-direct {p0, v1}, Lcom/google/android/location/copresence/e/d;->b(Lcom/google/android/location/copresence/e/f;)V

    .line 349
    invoke-virtual {p2}, Lcom/google/android/gms/location/copresence/MessageFilter;->b()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/location/copresence/MessageFilter$FilterPrimitive;

    .line 350
    iget-object v3, p0, Lcom/google/android/location/copresence/e/d;->e:Ljava/util/HashMap;

    iget-object v0, v0, Lcom/google/android/gms/location/copresence/MessageFilter$FilterPrimitive;->f:Ljava/lang/String;

    invoke-virtual {v3, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 352
    :cond_2
    invoke-static {v4}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 353
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "MessageFilterCache: Added entry(size="

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/google/android/location/copresence/e/d;->c()I

    move-result v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "): "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->b(Ljava/lang/String;)I

    .line 355
    :cond_3
    return-object v1
.end method

.method private declared-synchronized a(Ljava/lang/String;Ljava/lang/String;Landroid/app/PendingIntent;)Lcom/google/android/location/copresence/e/f;
    .locals 1

    .prologue
    .line 471
    monitor-enter p0

    :try_start_0
    new-instance v0, Lcom/google/android/location/copresence/e/g;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/google/android/location/copresence/e/g;-><init>(Lcom/google/android/location/copresence/e/d;Ljava/lang/String;Ljava/lang/String;Landroid/app/PendingIntent;)V

    invoke-direct {p0, v0}, Lcom/google/android/location/copresence/e/d;->a(Lcom/google/android/location/copresence/e/g;)Lcom/google/android/location/copresence/e/f;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized a(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/location/copresence/internal/o;)Lcom/google/android/location/copresence/e/f;
    .locals 1

    .prologue
    .line 414
    monitor-enter p0

    :try_start_0
    new-instance v0, Lcom/google/android/location/copresence/e/g;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/google/android/location/copresence/e/g;-><init>(Lcom/google/android/location/copresence/e/d;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/location/copresence/internal/o;)V

    invoke-direct {p0, v0}, Lcom/google/android/location/copresence/e/d;->a(Lcom/google/android/location/copresence/e/g;)Lcom/google/android/location/copresence/e/f;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized a(Ljava/lang/String;Ljava/lang/String;)Ljava/util/List;
    .locals 4

    .prologue
    .line 504
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/location/copresence/e/d;->a()V

    .line 506
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 507
    iget-object v0, p0, Lcom/google/android/location/copresence/e/d;->d:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/copresence/e/g;

    .line 508
    invoke-virtual {v0, p1, p2}, Lcom/google/android/location/copresence/e/g;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 509
    iget-object v3, p0, Lcom/google/android/location/copresence/e/d;->d:Ljava/util/HashMap;

    invoke-virtual {v3, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 504
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 512
    :cond_1
    monitor-exit p0

    return-object v1
.end method

.method private declared-synchronized b()Ljava/util/List;
    .locals 4

    .prologue
    .line 520
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/location/copresence/e/d;->a()V

    .line 522
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 523
    iget-object v0, p0, Lcom/google/android/location/copresence/e/d;->d:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/copresence/e/g;

    .line 524
    iget-object v3, p0, Lcom/google/android/location/copresence/e/d;->d:Ljava/util/HashMap;

    invoke-virtual {v3, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 520
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 526
    :cond_0
    monitor-exit p0

    return-object v1
.end method

.method private declared-synchronized b(Ljava/lang/String;Ljava/lang/String;)Ljava/util/Set;
    .locals 4

    .prologue
    .line 733
    monitor-enter p0

    :try_start_0
    invoke-static {p1}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 734
    invoke-static {p2}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 735
    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    .line 736
    iget-object v0, p0, Lcom/google/android/location/copresence/e/d;->d:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/copresence/e/g;

    .line 737
    iget-object v1, v0, Lcom/google/android/location/copresence/e/g;->b:Landroid/app/PendingIntent;

    if-eqz v1, :cond_1

    invoke-virtual {v0, p1, p2}, Lcom/google/android/location/copresence/e/g;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x1

    :goto_1
    if-eqz v1, :cond_0

    .line 738
    iget-object v0, v0, Lcom/google/android/location/copresence/e/g;->b:Landroid/app/PendingIntent;

    invoke-interface {v2, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 733
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 737
    :cond_1
    const/4 v1, 0x0

    goto :goto_1

    .line 741
    :cond_2
    monitor-exit p0

    return-object v2
.end method

.method private b(Lcom/google/android/location/copresence/e/f;)V
    .locals 2

    .prologue
    .line 302
    iget-object v0, p0, Lcom/google/android/location/copresence/e/d;->b:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/copresence/e/h;

    .line 303
    invoke-interface {v0, p1}, Lcom/google/android/location/copresence/e/h;->a(Lcom/google/android/location/copresence/e/f;)V

    goto :goto_0

    .line 305
    :cond_0
    return-void
.end method

.method private declared-synchronized c()I
    .locals 1

    .prologue
    .line 548
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/location/copresence/e/d;->d:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->size()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private c(Lcom/google/android/location/copresence/e/f;)V
    .locals 2

    .prologue
    .line 308
    iget-object v0, p0, Lcom/google/android/location/copresence/e/d;->b:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/copresence/e/h;

    .line 309
    invoke-interface {v0, p1}, Lcom/google/android/location/copresence/e/h;->b(Lcom/google/android/location/copresence/e/f;)V

    goto :goto_0

    .line 311
    :cond_0
    return-void
.end method

.method private d(Lcom/google/android/location/copresence/e/f;)V
    .locals 3

    .prologue
    .line 552
    iget-object v0, p1, Lcom/google/android/location/copresence/e/f;->b:Lcom/google/android/gms/location/copresence/MessageFilter;

    invoke-virtual {v0}, Lcom/google/android/gms/location/copresence/MessageFilter;->b()Ljava/util/List;

    move-result-object v0

    .line 553
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/location/copresence/MessageFilter$FilterPrimitive;

    .line 556
    iget-object v2, v0, Lcom/google/android/gms/location/copresence/MessageFilter$FilterPrimitive;->f:Ljava/lang/String;

    if-eqz v2, :cond_0

    .line 557
    iget-object v2, p0, Lcom/google/android/location/copresence/e/d;->e:Ljava/util/HashMap;

    iget-object v0, v0, Lcom/google/android/gms/location/copresence/MessageFilter$FilterPrimitive;->f:Ljava/lang/String;

    invoke-virtual {v2, v0}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 560
    :cond_1
    return-void
.end method


# virtual methods
.method public final declared-synchronized a(Ljava/lang/String;)Lcom/google/android/location/copresence/e/f;
    .locals 1

    .prologue
    .line 664
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/location/copresence/e/d;->e:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/copresence/e/f;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Ljava/lang/String;Ljava/lang/String;Landroid/app/PendingIntent;Lcom/google/android/gms/location/copresence/MessageFilter;Lcom/google/android/gms/location/copresence/u;)Lcom/google/android/location/copresence/e/f;
    .locals 1

    .prologue
    .line 328
    monitor-enter p0

    :try_start_0
    new-instance v0, Lcom/google/android/location/copresence/e/g;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/google/android/location/copresence/e/g;-><init>(Lcom/google/android/location/copresence/e/d;Ljava/lang/String;Ljava/lang/String;Landroid/app/PendingIntent;)V

    invoke-direct {p0, v0, p4, p5}, Lcom/google/android/location/copresence/e/d;->a(Lcom/google/android/location/copresence/e/g;Lcom/google/android/gms/location/copresence/MessageFilter;Lcom/google/android/gms/location/copresence/u;)Lcom/google/android/location/copresence/e/f;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/location/copresence/internal/SubscribeOperation;)Lcom/google/android/location/copresence/e/f;
    .locals 2

    .prologue
    .line 423
    monitor-enter p0

    :try_start_0
    iget v0, p3, Lcom/google/android/gms/location/copresence/internal/SubscribeOperation;->a:I

    packed-switch v0, :pswitch_data_0

    .line 429
    const/4 v0, 0x6

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 430
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "MessageFilterCache: Can\'t get entry for subscribe op type: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v1, p3, Lcom/google/android/gms/location/copresence/internal/SubscribeOperation;->a:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1}, Ljava/lang/IllegalStateException;-><init>()V

    invoke-static {v0, v1}, Lcom/google/android/location/copresence/ag;->a(Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 433
    :cond_0
    const/4 v0, 0x0

    :goto_0
    monitor-exit p0

    return-object v0

    .line 425
    :pswitch_0
    :try_start_1
    iget-object v0, p3, Lcom/google/android/gms/location/copresence/internal/SubscribeOperation;->d:Lcom/google/android/gms/location/copresence/internal/o;

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/location/copresence/e/d;->a(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/location/copresence/internal/o;)Lcom/google/android/location/copresence/e/f;

    move-result-object v0

    goto :goto_0

    .line 427
    :pswitch_1
    iget-object v0, p3, Lcom/google/android/gms/location/copresence/internal/SubscribeOperation;->e:Landroid/app/PendingIntent;

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/location/copresence/e/d;->a(Ljava/lang/String;Ljava/lang/String;Landroid/app/PendingIntent;)Lcom/google/android/location/copresence/e/f;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    goto :goto_0

    .line 423
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final declared-synchronized a(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/location/copresence/internal/UnsubscribeOperation;)Lcom/google/android/location/copresence/e/f;
    .locals 2

    .prologue
    .line 443
    monitor-enter p0

    :try_start_0
    iget v0, p3, Lcom/google/android/gms/location/copresence/internal/UnsubscribeOperation;->a:I

    packed-switch v0, :pswitch_data_0

    .line 449
    const/4 v0, 0x6

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 450
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "MessageFilterCache: Can\'t get entry for unsubscribe op type: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v1, p3, Lcom/google/android/gms/location/copresence/internal/UnsubscribeOperation;->a:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1}, Ljava/lang/IllegalStateException;-><init>()V

    invoke-static {v0, v1}, Lcom/google/android/location/copresence/ag;->a(Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 453
    :cond_0
    const/4 v0, 0x0

    :goto_0
    monitor-exit p0

    return-object v0

    .line 445
    :pswitch_0
    :try_start_1
    iget-object v0, p3, Lcom/google/android/gms/location/copresence/internal/UnsubscribeOperation;->b:Lcom/google/android/gms/location/copresence/internal/o;

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/location/copresence/e/d;->a(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/location/copresence/internal/o;)Lcom/google/android/location/copresence/e/f;

    move-result-object v0

    goto :goto_0

    .line 447
    :pswitch_1
    iget-object v0, p3, Lcom/google/android/gms/location/copresence/internal/UnsubscribeOperation;->c:Landroid/app/PendingIntent;

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/location/copresence/e/d;->a(Ljava/lang/String;Ljava/lang/String;Landroid/app/PendingIntent;)Lcom/google/android/location/copresence/e/f;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    goto :goto_0

    .line 443
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final declared-synchronized a(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/location/copresence/internal/o;Lcom/google/android/gms/location/copresence/MessageFilter;Lcom/google/android/gms/location/copresence/u;)Lcom/google/android/location/copresence/e/f;
    .locals 1

    .prologue
    .line 319
    monitor-enter p0

    :try_start_0
    new-instance v0, Lcom/google/android/location/copresence/e/g;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/google/android/location/copresence/e/g;-><init>(Lcom/google/android/location/copresence/e/d;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/location/copresence/internal/o;)V

    invoke-direct {p0, v0, p4, p5}, Lcom/google/android/location/copresence/e/d;->a(Lcom/google/android/location/copresence/e/g;Lcom/google/android/gms/location/copresence/MessageFilter;Lcom/google/android/gms/location/copresence/u;)Lcom/google/android/location/copresence/e/f;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method final a()V
    .locals 15

    .prologue
    const/4 v14, 0x3

    const-wide v4, 0x7fffffffffffffffL

    .line 366
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v8

    .line 367
    iget-object v0, p0, Lcom/google/android/location/copresence/e/d;->d:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v0

    .line 368
    if-nez v0, :cond_1

    .line 406
    :cond_0
    :goto_0
    return-void

    .line 371
    :cond_1
    new-instance v10, Ljava/util/HashSet;

    invoke-direct {v10}, Ljava/util/HashSet;-><init>()V

    .line 373
    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v11

    move-wide v2, v4

    :cond_2
    :goto_1
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/copresence/e/f;

    .line 374
    iget-object v1, v0, Lcom/google/android/location/copresence/e/f;->b:Lcom/google/android/gms/location/copresence/MessageFilter;

    invoke-virtual {v1}, Lcom/google/android/gms/location/copresence/MessageFilter;->d()J

    move-result-wide v6

    .line 375
    cmp-long v1, v6, v4

    if-eqz v1, :cond_2

    .line 377
    iget-wide v12, v0, Lcom/google/android/location/copresence/e/f;->d:J

    add-long/2addr v6, v12

    .line 380
    cmp-long v1, v8, v6

    if-lez v1, :cond_3

    .line 381
    invoke-interface {v10, v0}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 383
    :cond_3
    cmp-long v0, v6, v2

    if-gez v0, :cond_8

    move-wide v0, v6

    :goto_2
    move-wide v2, v0

    .line 387
    goto :goto_1

    .line 388
    :cond_4
    invoke-interface {v10}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/copresence/e/f;

    .line 389
    invoke-static {v14}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v6

    if-eqz v6, :cond_5

    .line 390
    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "MessageFilterCache: Removing expired entry for package:"

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v7, v0, Lcom/google/android/location/copresence/e/f;->a:Lcom/google/android/location/copresence/e/g;

    iget-object v7, v7, Lcom/google/android/location/copresence/e/g;->a:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/google/android/location/copresence/ag;->b(Ljava/lang/String;)I

    .line 393
    :cond_5
    invoke-virtual {p0, v0}, Lcom/google/android/location/copresence/e/d;->a(Lcom/google/android/location/copresence/e/f;)V

    goto :goto_3

    .line 395
    :cond_6
    cmp-long v0, v2, v4

    if-eqz v0, :cond_0

    .line 396
    invoke-static {v14}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 397
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "MessageFilterCache: Queuing up next expiration for "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sub-long v4, v2, v8

    invoke-virtual {v0, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " ms from now."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->b(Ljava/lang/String;)I

    .line 402
    :cond_7
    iget-object v0, p0, Lcom/google/android/location/copresence/e/d;->f:Lcom/google/android/location/copresence/ap;

    invoke-virtual {v0}, Lcom/google/android/location/copresence/ap;->b()Lcom/google/android/location/copresence/c;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/location/copresence/e/d;->g:Ljava/lang/Runnable;

    sub-long/2addr v2, v8

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/location/copresence/c;->a(Ljava/lang/Runnable;J)V

    goto/16 :goto_0

    :cond_8
    move-wide v0, v2

    goto :goto_2
.end method

.method public final declared-synchronized a(Lcom/google/android/location/copresence/e/f;)V
    .locals 2

    .prologue
    .line 668
    monitor-enter p0

    if-nez p1, :cond_1

    .line 678
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 672
    :cond_1
    :try_start_0
    iget-object v0, p0, Lcom/google/android/location/copresence/e/d;->d:Ljava/util/HashMap;

    iget-object v1, p1, Lcom/google/android/location/copresence/e/f;->a:Lcom/google/android/location/copresence/e/g;

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 673
    invoke-direct {p0, p1}, Lcom/google/android/location/copresence/e/d;->d(Lcom/google/android/location/copresence/e/f;)V

    .line 674
    invoke-direct {p0, p1}, Lcom/google/android/location/copresence/e/d;->c(Lcom/google/android/location/copresence/e/f;)V

    .line 675
    const/4 v0, 0x3

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 676
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "MessageFilterCache: Removed entry(size="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/google/android/location/copresence/e/d;->c()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "): "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->b(Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 668
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final a(Lcom/google/android/location/copresence/e/h;)V
    .locals 1

    .prologue
    .line 294
    iget-object v0, p0, Lcom/google/android/location/copresence/e/d;->b:Ljava/util/HashSet;

    invoke-virtual {v0, p1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 295
    return-void
.end method

.method public final a(Ljava/io/PrintWriter;)V
    .locals 3

    .prologue
    .line 792
    const/4 v0, 0x2

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 793
    const-string v0, "Dump of active subscriptions:"

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 794
    invoke-direct {p0}, Lcom/google/android/location/copresence/e/d;->b()Ljava/util/List;

    move-result-object v0

    .line 795
    if-eqz v0, :cond_0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 796
    :cond_0
    const-string v0, "  <No active subscription>"

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 804
    :cond_1
    return-void

    .line 798
    :cond_2
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/copresence/e/f;

    .line 799
    const-string v2, "  "

    invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 800
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public final declared-synchronized a(Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;Ljava/util/ArrayList;)Z
    .locals 4

    .prologue
    .line 753
    monitor-enter p0

    :try_start_0
    invoke-direct {p0, p1, p2}, Lcom/google/android/location/copresence/e/d;->b(Ljava/lang/String;Ljava/lang/String;)Ljava/util/Set;

    move-result-object v1

    .line 756
    invoke-virtual {p4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    :pswitch_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/location/copresence/internal/UnsubscribeOperation;

    .line 757
    iget v3, v0, Lcom/google/android/gms/location/copresence/internal/UnsubscribeOperation;->a:I

    .line 758
    packed-switch v3, :pswitch_data_0

    .line 772
    const/4 v0, 0x6

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 773
    const-string v0, "MessageFilterCache: new unsupported unsubscribeOp type"

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->e(Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 753
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 760
    :pswitch_1
    :try_start_1
    invoke-interface {v1}, Ljava/util/Set;->clear()V

    goto :goto_0

    .line 763
    :pswitch_2
    iget-object v3, v0, Lcom/google/android/gms/location/copresence/internal/UnsubscribeOperation;->c:Landroid/app/PendingIntent;

    if-eqz v3, :cond_0

    .line 764
    iget-object v0, v0, Lcom/google/android/gms/location/copresence/internal/UnsubscribeOperation;->c:Landroid/app/PendingIntent;

    invoke-interface {v1, v0}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    goto :goto_0

    .line 780
    :cond_1
    invoke-virtual {p3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_2
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/location/copresence/internal/SubscribeOperation;

    .line 781
    iget-object v3, v0, Lcom/google/android/gms/location/copresence/internal/SubscribeOperation;->e:Landroid/app/PendingIntent;

    if-eqz v3, :cond_2

    .line 782
    iget-object v0, v0, Lcom/google/android/gms/location/copresence/internal/SubscribeOperation;->e:Landroid/app/PendingIntent;

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 785
    :cond_3
    invoke-interface {v1}, Ljava/util/Set;->size()I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v0

    const/4 v1, 0x5

    if-le v0, v1, :cond_4

    const/4 v0, 0x1

    :goto_2
    monitor-exit p0

    return v0

    :cond_4
    const/4 v0, 0x0

    goto :goto_2

    .line 758
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method public final declared-synchronized b(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/location/copresence/internal/UnsubscribeOperation;)V
    .locals 2

    .prologue
    .line 692
    monitor-enter p0

    :try_start_0
    iget v0, p3, Lcom/google/android/gms/location/copresence/internal/UnsubscribeOperation;->a:I

    packed-switch v0, :pswitch_data_0

    .line 703
    const/4 v0, 0x6

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 704
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Unknown unsubscribe operation type: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v1, p3, Lcom/google/android/gms/location/copresence/internal/UnsubscribeOperation;->a:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1}, Ljava/lang/IllegalStateException;-><init>()V

    invoke-static {v0, v1}, Lcom/google/android/location/copresence/ag;->a(Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 707
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 694
    :pswitch_0
    :try_start_1
    invoke-static {p1}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p2}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-direct {p0, p1, p2}, Lcom/google/android/location/copresence/e/d;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/copresence/e/f;

    invoke-virtual {p0, v0}, Lcom/google/android/location/copresence/e/d;->a(Lcom/google/android/location/copresence/e/f;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 692
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 697
    :pswitch_1
    :try_start_2
    iget-object v0, p3, Lcom/google/android/gms/location/copresence/internal/UnsubscribeOperation;->b:Lcom/google/android/gms/location/copresence/internal/o;

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/location/copresence/e/d;->a(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/location/copresence/internal/o;)Lcom/google/android/location/copresence/e/f;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/location/copresence/e/d;->a(Lcom/google/android/location/copresence/e/f;)V

    goto :goto_0

    .line 700
    :pswitch_2
    iget-object v0, p3, Lcom/google/android/gms/location/copresence/internal/UnsubscribeOperation;->c:Landroid/app/PendingIntent;

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/location/copresence/e/d;->a(Ljava/lang/String;Ljava/lang/String;Landroid/app/PendingIntent;)Lcom/google/android/location/copresence/e/f;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/location/copresence/e/d;->a(Lcom/google/android/location/copresence/e/f;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 692
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_0
    .end packed-switch
.end method
