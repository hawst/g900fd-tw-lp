.class Lcom/google/android/location/activity/y;
.super Lcom/google/android/location/activity/s;
.source "SourceFile"


# instance fields
.field private final b:Ljava/util/List;

.field protected c:Lcom/google/android/location/activity/aj;


# direct methods
.method public constructor <init>(Lcom/google/android/location/activity/k;Lcom/google/android/location/os/bi;)V
    .locals 2

    .prologue
    .line 28
    invoke-direct {p0, p1, p2}, Lcom/google/android/location/activity/s;-><init>(Lcom/google/android/location/activity/k;Lcom/google/android/location/os/bi;)V

    .line 24
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/location/activity/y;->b:Ljava/util/List;

    .line 29
    new-instance v0, Lcom/google/android/location/activity/aj;

    invoke-interface {p2}, Lcom/google/android/location/os/bi;->y()Z

    move-result v1

    invoke-direct {v0, p1, v1}, Lcom/google/android/location/activity/aj;-><init>(Lcom/google/android/location/activity/k;Z)V

    iput-object v0, p0, Lcom/google/android/location/activity/y;->c:Lcom/google/android/location/activity/aj;

    .line 30
    return-void
.end method


# virtual methods
.method public a(Lcom/google/android/gms/location/ActivityRecognitionResult;J)J
    .locals 8

    .prologue
    const-wide/16 v4, 0x1388

    const-wide/16 v2, -0x1

    .line 56
    invoke-virtual {p0}, Lcom/google/android/location/activity/y;->k()V

    .line 57
    iget-object v0, p0, Lcom/google/android/location/activity/y;->d:Lcom/google/android/location/activity/k;

    invoke-virtual {v0, p1}, Lcom/google/android/location/activity/k;->b(Lcom/google/android/gms/location/ActivityRecognitionResult;)V

    .line 58
    iget-boolean v0, p0, Lcom/google/android/location/activity/y;->a:Z

    if-eqz v0, :cond_0

    move-wide v0, v2

    .line 92
    :goto_0
    return-wide v0

    .line 62
    :cond_0
    if-nez p1, :cond_1

    .line 64
    new-instance v0, Lcom/google/android/location/activity/ao;

    iget-object v1, p0, Lcom/google/android/location/activity/y;->d:Lcom/google/android/location/activity/k;

    iget-object v4, p0, Lcom/google/android/location/activity/y;->e:Lcom/google/android/location/os/bi;

    invoke-direct {v0, v1, v4}, Lcom/google/android/location/activity/ao;-><init>(Lcom/google/android/location/activity/k;Lcom/google/android/location/os/bi;)V

    invoke-virtual {p0, v0}, Lcom/google/android/location/activity/y;->a(Lcom/google/android/location/activity/av;)V

    move-wide v0, v2

    .line 65
    goto :goto_0

    .line 67
    :cond_1
    iget-object v0, p0, Lcom/google/android/location/activity/y;->c:Lcom/google/android/location/activity/aj;

    invoke-virtual {v0, p1}, Lcom/google/android/location/activity/aj;->a(Lcom/google/android/gms/location/ActivityRecognitionResult;)Lcom/google/android/location/e/aj;

    move-result-object v1

    .line 69
    iget-object v0, v1, Lcom/google/android/location/e/aj;->a:Ljava/lang/Object;

    if-eqz v0, :cond_2

    .line 70
    iget-object v0, v1, Lcom/google/android/location/e/aj;->a:Ljava/lang/Object;

    check-cast v0, Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_1
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/location/ActivityRecognitionResult;

    .line 71
    iget-object v7, p0, Lcom/google/android/location/activity/y;->d:Lcom/google/android/location/activity/k;

    invoke-virtual {v7, v0}, Lcom/google/android/location/activity/k;->a(Lcom/google/android/gms/location/ActivityRecognitionResult;)V

    goto :goto_1

    .line 75
    :cond_2
    iget-object v0, v1, Lcom/google/android/location/e/aj;->b:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 78
    invoke-virtual {p0}, Lcom/google/android/location/activity/y;->n()V

    move-wide v0, v2

    .line 79
    goto :goto_0

    .line 81
    :cond_3
    cmp-long v0, v4, p2

    if-gtz v0, :cond_4

    move-wide v0, v4

    .line 82
    goto :goto_0

    .line 86
    :cond_4
    sget-boolean v0, Lcom/google/android/location/i/a;->e:Z

    if-eqz v0, :cond_5

    const-string v0, "ActivityScheduler"

    const-string v1, "The batching data is not long enough. "

    invoke-static {v0, v1}, Lcom/google/android/location/o/a/a;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 87
    :cond_5
    iget-object v0, p0, Lcom/google/android/location/activity/y;->e:Lcom/google/android/location/os/bi;

    invoke-interface {v0}, Lcom/google/android/location/os/bi;->y()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 88
    invoke-virtual {p0}, Lcom/google/android/location/activity/y;->o()V

    :goto_2
    move-wide v0, v2

    .line 92
    goto :goto_0

    .line 90
    :cond_6
    iget-object v0, p0, Lcom/google/android/location/activity/y;->d:Lcom/google/android/location/activity/k;

    iget-object v1, p0, Lcom/google/android/location/activity/y;->e:Lcom/google/android/location/os/bi;

    invoke-interface {v1}, Lcom/google/android/location/os/bi;->c()Lcom/google/android/location/j/b;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/location/j/b;->c()J

    move-result-wide v6

    add-long/2addr v4, v6

    invoke-virtual {v0, v4, v5}, Lcom/google/android/location/activity/k;->e(J)V

    goto :goto_2
.end method

.method public final a(D)V
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lcom/google/android/location/activity/y;->d:Lcom/google/android/location/activity/k;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/location/activity/k;->a(D)V

    .line 41
    return-void
.end method

.method public final a(Lcom/google/android/gms/location/ActivityRecognitionResult;)V
    .locals 3

    .prologue
    .line 113
    invoke-virtual {p1}, Lcom/google/android/gms/location/ActivityRecognitionResult;->a()Lcom/google/android/gms/location/DetectedActivity;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/location/DetectedActivity;->a()I

    move-result v0

    .line 114
    iget-object v1, p0, Lcom/google/android/location/activity/y;->b:Ljava/util/List;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 119
    :goto_0
    return-void

    .line 117
    :cond_0
    iget-object v1, p0, Lcom/google/android/location/activity/y;->b:Ljava/util/List;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 118
    invoke-super {p0, p1}, Lcom/google/android/location/activity/s;->a(Lcom/google/android/gms/location/ActivityRecognitionResult;)V

    goto :goto_0
.end method

.method protected final a(Z)V
    .locals 5

    .prologue
    .line 128
    sget-boolean v0, Lcom/google/android/location/i/a;->b:Z

    if-eqz v0, :cond_0

    const-string v0, "ActivityScheduler"

    const-string v1, "newClientAdded(%s)."

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/location/o/a/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 131
    :cond_0
    iget-object v0, p0, Lcom/google/android/location/activity/y;->e:Lcom/google/android/location/os/bi;

    invoke-interface {v0}, Lcom/google/android/location/os/bi;->y()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/location/activity/y;->d:Lcom/google/android/location/activity/k;

    invoke-virtual {v0}, Lcom/google/android/location/activity/k;->c()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 132
    new-instance v0, Lcom/google/android/location/activity/w;

    iget-object v1, p0, Lcom/google/android/location/activity/y;->d:Lcom/google/android/location/activity/k;

    iget-object v2, p0, Lcom/google/android/location/activity/y;->e:Lcom/google/android/location/os/bi;

    invoke-direct {v0, v1, v2}, Lcom/google/android/location/activity/w;-><init>(Lcom/google/android/location/activity/k;Lcom/google/android/location/os/bi;)V

    invoke-virtual {p0, v0}, Lcom/google/android/location/activity/y;->a(Lcom/google/android/location/activity/av;)V

    .line 134
    :cond_1
    return-void
.end method

.method protected final ao_()V
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lcom/google/android/location/activity/y;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 35
    invoke-super {p0}, Lcom/google/android/location/activity/s;->ao_()V

    .line 36
    return-void
.end method

.method protected final av_()J
    .locals 2

    .prologue
    .line 123
    const-wide v0, 0x861c46800L

    return-wide v0
.end method

.method protected final c()V
    .locals 0

    .prologue
    .line 108
    invoke-virtual {p0}, Lcom/google/android/location/activity/y;->ao_()V

    .line 109
    return-void
.end method

.method protected d()Lcom/google/android/location/activity/a/o;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcom/google/android/location/activity/y;->d:Lcom/google/android/location/activity/k;

    invoke-virtual {v0}, Lcom/google/android/location/activity/k;->m()Lcom/google/android/location/activity/a/o;

    move-result-object v0

    return-object v0
.end method

.method protected final e()J
    .locals 2

    .prologue
    .line 103
    const-wide v0, 0x9502f9000L

    return-wide v0
.end method

.method protected f()Ljava/lang/String;
    .locals 1

    .prologue
    .line 50
    const-string v0, "FullDetectorInPast"

    return-object v0
.end method

.method public p()Ljava/lang/String;
    .locals 1

    .prologue
    .line 137
    const-string v0, "FullDetectingInPastState"

    return-object v0
.end method
