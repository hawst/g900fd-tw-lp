.class final Lcom/google/android/location/geofencer/service/w;
.super Lcom/google/android/location/geofencer/service/o;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/location/geofencer/service/k;

.field private e:Ljava/util/Calendar;

.field private f:Z

.field private g:J

.field private h:Landroid/location/Location;

.field private i:S

.field private final j:Lcom/google/android/location/geofencer/service/ae;

.field private final k:Landroid/net/wifi/WifiManager;


# direct methods
.method public constructor <init>(Lcom/google/android/location/geofencer/service/k;Lcom/google/android/location/geofencer/service/i;Lcom/google/android/location/activity/at;Landroid/net/wifi/WifiManager;)V
    .locals 3

    .prologue
    .line 1879
    iput-object p1, p0, Lcom/google/android/location/geofencer/service/w;->a:Lcom/google/android/location/geofencer/service/k;

    .line 1880
    invoke-direct {p0, p1, p2}, Lcom/google/android/location/geofencer/service/o;-><init>(Lcom/google/android/location/geofencer/service/k;Lcom/google/android/location/geofencer/service/i;)V

    .line 1868
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/geofencer/service/w;->e:Ljava/util/Calendar;

    .line 1871
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/location/geofencer/service/w;->f:Z

    .line 1881
    iput-object p4, p0, Lcom/google/android/location/geofencer/service/w;->k:Landroid/net/wifi/WifiManager;

    .line 1882
    new-instance v0, Lcom/google/android/location/geofencer/service/ae;

    iget-object v1, p0, Lcom/google/android/location/geofencer/service/w;->c:Lcom/google/android/location/geofencer/service/i;

    iget-object v1, v1, Lcom/google/android/location/geofencer/service/i;->k:Lcom/google/android/location/h/c;

    iget-object v2, p0, Lcom/google/android/location/geofencer/service/w;->k:Landroid/net/wifi/WifiManager;

    invoke-direct {v0, p1, v1, p3, v2}, Lcom/google/android/location/geofencer/service/ae;-><init>(Lcom/google/android/location/geofencer/service/k;Lcom/google/android/location/h/c;Lcom/google/android/location/activity/at;Landroid/net/wifi/WifiManager;)V

    iput-object v0, p0, Lcom/google/android/location/geofencer/service/w;->j:Lcom/google/android/location/geofencer/service/ae;

    .line 1884
    return-void
.end method

.method private m()Z
    .locals 10

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 1974
    iget-object v0, p0, Lcom/google/android/location/geofencer/service/w;->e:Ljava/util/Calendar;

    iget-object v3, p0, Lcom/google/android/location/geofencer/service/w;->a:Lcom/google/android/location/geofencer/service/k;

    invoke-static {v3}, Lcom/google/android/location/geofencer/service/k;->u(Lcom/google/android/location/geofencer/service/k;)Lcom/google/android/gms/common/util/p;

    move-result-object v3

    invoke-interface {v3}, Lcom/google/android/gms/common/util/p;->a()J

    move-result-wide v4

    invoke-virtual {v0, v4, v5}, Ljava/util/Calendar;->setTimeInMillis(J)V

    iget-object v0, p0, Lcom/google/android/location/geofencer/service/w;->e:Ljava/util/Calendar;

    const/16 v3, 0xb

    invoke-virtual {v0, v3}, Ljava/util/Calendar;->get(I)I

    move-result v0

    const/4 v3, 0x7

    if-lt v0, v3, :cond_0

    const/16 v3, 0x17

    if-lt v0, v3, :cond_4

    :cond_0
    move v0, v2

    :goto_0
    iget-object v3, p0, Lcom/google/android/location/geofencer/service/w;->c:Lcom/google/android/location/geofencer/service/i;

    iget-object v3, v3, Lcom/google/android/location/geofencer/service/i;->f:Lcom/google/android/location/h/a;

    iget-object v3, v3, Lcom/google/android/location/h/a;->h:Lcom/google/android/location/h/d;

    invoke-virtual {v3}, Lcom/google/android/location/h/d;->d()Z

    move-result v4

    iget-object v3, p0, Lcom/google/android/location/geofencer/service/w;->c:Lcom/google/android/location/geofencer/service/i;

    sget-boolean v5, Lcom/google/android/location/geofencer/a/a;->a:Z

    if-eqz v5, :cond_1

    const-string v5, "GeofencerStateInfo"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "hasRecentUserInteraction="

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v7, v3, Lcom/google/android/location/geofencer/service/i;->k:Lcom/google/android/location/h/c;

    iget-wide v8, v7, Lcom/google/android/location/h/c;->b:J

    invoke-virtual {v6, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/google/android/location/geofencer/a/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    iget-object v5, v3, Lcom/google/android/location/geofencer/service/i;->c:Lcom/google/android/gms/common/util/p;

    invoke-interface {v5}, Lcom/google/android/gms/common/util/p;->b()J

    move-result-wide v6

    iget-object v3, v3, Lcom/google/android/location/geofencer/service/i;->k:Lcom/google/android/location/h/c;

    iget-wide v8, v3, Lcom/google/android/location/h/c;->b:J

    sub-long/2addr v6, v8

    const-wide/32 v8, 0x36ee80

    cmp-long v3, v6, v8

    if-gtz v3, :cond_5

    move v3, v2

    :goto_1
    if-nez v3, :cond_6

    move v3, v2

    :goto_2
    sget-boolean v5, Lcom/google/android/location/geofencer/a/a;->a:Z

    if-eqz v5, :cond_2

    const-string v5, "GeofencerStateMachine"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "sleepTime="

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " stillForALongTime="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " noActivityForALongTime="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/google/android/location/geofencer/a/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_2
    if-eqz v0, :cond_7

    if-eqz v4, :cond_7

    if-eqz v3, :cond_7

    move v0, v2

    .line 1975
    :goto_3
    iget-boolean v3, p0, Lcom/google/android/location/geofencer/service/w;->f:Z

    if-eq v3, v0, :cond_8

    .line 1976
    iput-boolean v0, p0, Lcom/google/android/location/geofencer/service/w;->f:Z

    .line 1977
    sget-boolean v0, Lcom/google/android/location/geofencer/a/a;->a:Z

    if-eqz v0, :cond_3

    .line 1978
    const-string v0, "GeofencerStateMachine"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "mightUpdateInDeepStill: mInDeepStill="

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v3, p0, Lcom/google/android/location/geofencer/service/w;->f:Z

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/location/geofencer/a/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1982
    :cond_3
    :goto_4
    return v2

    :cond_4
    move v0, v1

    .line 1974
    goto/16 :goto_0

    :cond_5
    move v3, v1

    goto :goto_1

    :cond_6
    move v3, v1

    goto :goto_2

    :cond_7
    move v0, v1

    goto :goto_3

    :cond_8
    move v2, v1

    .line 1982
    goto :goto_4
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2095
    const-string v0, "StillActivityState"

    return-object v0
.end method

.method public final a(Ljava/io/PrintWriter;)V
    .locals 2

    .prologue
    .line 2100
    invoke-super {p0, p1}, Lcom/google/android/location/geofencer/service/o;->a(Ljava/io/PrintWriter;)V

    .line 2101
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "\n    Stable location="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/location/geofencer/service/w;->h:Landroid/location/Location;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", close to geofences="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-short v1, p0, Lcom/google/android/location/geofencer/service/w;->i:S

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 2103
    return-void
.end method

.method protected final a(Landroid/content/Intent;)Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 1957
    invoke-super {p0, p1}, Lcom/google/android/location/geofencer/service/o;->a(Landroid/content/Intent;)Z

    .line 1958
    invoke-direct {p0}, Lcom/google/android/location/geofencer/service/w;->m()Z

    move-result v1

    .line 1959
    if-eqz v1, :cond_0

    .line 1960
    const/4 v1, -0x1

    invoke-virtual {p0, v1, v0}, Lcom/google/android/location/geofencer/service/w;->a(IZ)V

    .line 1961
    const/4 v0, 0x1

    .line 1963
    :cond_0
    return v0
.end method

.method protected final a(Landroid/util/Pair;)Z
    .locals 11

    .prologue
    const/4 v8, 0x1

    const/4 v9, 0x0

    .line 1936
    invoke-direct {p0}, Lcom/google/android/location/geofencer/service/w;->m()Z

    .line 1937
    iget-object v10, p0, Lcom/google/android/location/geofencer/service/w;->j:Lcom/google/android/location/geofencer/service/ae;

    iget-object v0, v10, Lcom/google/android/location/geofencer/service/ae;->c:Landroid/location/Location;

    if-nez v0, :cond_2

    iput-boolean v9, v10, Lcom/google/android/location/geofencer/service/ae;->e:Z

    const-string v0, "LocationFilter"

    const-string v1, "Stable location not established yet. Returning original location."

    invoke-static {v0, v1}, Lcom/google/android/location/geofencer/a/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1938
    :cond_0
    :goto_0
    invoke-super {p0, p1}, Lcom/google/android/location/geofencer/service/o;->a(Landroid/util/Pair;)Z

    move-result v0

    .line 1939
    iget-object v1, p0, Lcom/google/android/location/geofencer/service/w;->h:Landroid/location/Location;

    if-nez v1, :cond_1

    .line 1940
    iget-object v1, p0, Lcom/google/android/location/geofencer/service/w;->c:Lcom/google/android/location/geofencer/service/i;

    iget-object v1, v1, Lcom/google/android/location/geofencer/service/i;->d:Lcom/google/android/location/geofencer/data/n;

    iget-object v2, p0, Lcom/google/android/location/geofencer/service/w;->a:Lcom/google/android/location/geofencer/service/k;

    invoke-static {v2}, Lcom/google/android/location/geofencer/service/k;->u(Lcom/google/android/location/geofencer/service/k;)Lcom/google/android/gms/common/util/p;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/gms/common/util/p;->b()J

    move-result-wide v2

    iget-object v4, p0, Lcom/google/android/location/geofencer/service/w;->a:Lcom/google/android/location/geofencer/service/k;

    invoke-static {v4}, Lcom/google/android/location/geofencer/service/k;->u(Lcom/google/android/location/geofencer/service/k;)Lcom/google/android/gms/common/util/p;

    move-result-object v4

    invoke-interface {v4}, Lcom/google/android/gms/common/util/p;->a()J

    move-result-wide v4

    invoke-virtual {v1, v2, v3, v4, v5}, Lcom/google/android/location/geofencer/data/n;->a(JJ)Landroid/location/Location;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/location/geofencer/service/w;->h:Landroid/location/Location;

    .line 1947
    iget-object v1, p0, Lcom/google/android/location/geofencer/service/w;->h:Landroid/location/Location;

    if-eqz v1, :cond_1

    .line 1948
    const-string v1, "GeofencerStateMachine"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Got stable location: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/google/android/location/geofencer/service/w;->h:Landroid/location/Location;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/location/geofencer/a/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1949
    iget-object v1, p0, Lcom/google/android/location/geofencer/service/w;->j:Lcom/google/android/location/geofencer/service/ae;

    iget-object v2, p0, Lcom/google/android/location/geofencer/service/w;->h:Landroid/location/Location;

    iput-object v2, v1, Lcom/google/android/location/geofencer/service/ae;->c:Landroid/location/Location;

    invoke-virtual {v1}, Lcom/google/android/location/geofencer/service/ae;->a()Landroid/net/wifi/WifiInfo;

    move-result-object v2

    iput-object v2, v1, Lcom/google/android/location/geofencer/service/ae;->d:Landroid/net/wifi/WifiInfo;

    const-string v2, "LocationFilter"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "WifiInfo associated with stable location: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, v1, Lcom/google/android/location/geofencer/service/ae;->d:Landroid/net/wifi/WifiInfo;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/location/geofencer/a/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1}, Lcom/google/android/location/geofencer/service/ae;->b()V

    .line 1952
    :cond_1
    return v0

    .line 1937
    :cond_2
    iget-object v0, v10, Lcom/google/android/location/geofencer/service/ae;->a:Lcom/google/android/location/activity/at;

    invoke-interface {v0}, Lcom/google/android/location/activity/at;->c()Z

    move-result v0

    if-eqz v0, :cond_3

    const-string v0, "LocationFilter"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Forcing stable location. Original location:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p1, Landroid/util/Pair;->second:Ljava/lang/Object;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " because SMD didn\'t trigger."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/location/geofencer/a/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    iput-boolean v9, v10, Lcom/google/android/location/geofencer/service/ae;->e:Z

    invoke-virtual {v10, p1}, Lcom/google/android/location/geofencer/service/ae;->a(Landroid/util/Pair;)Landroid/util/Pair;

    move-result-object p1

    goto/16 :goto_0

    :cond_3
    invoke-virtual {v10}, Lcom/google/android/location/geofencer/service/ae;->a()Landroid/net/wifi/WifiInfo;

    move-result-object v0

    if-eqz v0, :cond_4

    iget-object v1, v10, Lcom/google/android/location/geofencer/service/ae;->d:Landroid/net/wifi/WifiInfo;

    if-eqz v1, :cond_4

    invoke-virtual {v0}, Landroid/net/wifi/WifiInfo;->getBSSID()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_4

    invoke-virtual {v0}, Landroid/net/wifi/WifiInfo;->getBSSID()Ljava/lang/String;

    move-result-object v1

    iget-object v2, v10, Lcom/google/android/location/geofencer/service/ae;->d:Landroid/net/wifi/WifiInfo;

    invoke-virtual {v2}, Landroid/net/wifi/WifiInfo;->getBSSID()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    const-string v1, "LocationFilter"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Forcing stable location. Original location:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p1, Landroid/util/Pair;->second:Ljava/lang/Object;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " because connected WiFi network is the same, bssid="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Landroid/net/wifi/WifiInfo;->getBSSID()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/google/android/location/geofencer/a/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    iput-boolean v9, v10, Lcom/google/android/location/geofencer/service/ae;->e:Z

    invoke-virtual {v10, p1}, Lcom/google/android/location/geofencer/service/ae;->a(Landroid/util/Pair;)Landroid/util/Pair;

    move-result-object p1

    goto/16 :goto_0

    :cond_4
    iget-object v0, p1, Landroid/util/Pair;->second:Ljava/lang/Object;

    move-object v6, v0

    check-cast v6, Landroid/location/Location;

    invoke-virtual {v6}, Landroid/location/Location;->getAccuracy()F

    move-result v0

    const/high16 v1, 0x42c80000    # 100.0f

    cmpg-float v0, v0, v1

    if-gez v0, :cond_6

    iget-object v0, v10, Lcom/google/android/location/geofencer/service/ae;->c:Landroid/location/Location;

    invoke-virtual {v0}, Landroid/location/Location;->getLatitude()D

    move-result-wide v0

    iget-object v2, v10, Lcom/google/android/location/geofencer/service/ae;->c:Landroid/location/Location;

    invoke-virtual {v2}, Landroid/location/Location;->getLongitude()D

    move-result-wide v2

    invoke-virtual {v6}, Landroid/location/Location;->getLatitude()D

    move-result-wide v4

    invoke-virtual {v6}, Landroid/location/Location;->getLongitude()D

    move-result-wide v6

    invoke-static/range {v0 .. v7}, Lcom/google/android/location/g/d;->c(DDDD)D

    move-result-wide v2

    const-wide/high16 v0, 0x4059000000000000L    # 100.0

    cmpg-double v0, v2, v0

    if-gez v0, :cond_5

    move v0, v8

    :goto_1
    const-string v1, "LocationFilter"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Location jumpped by "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "m. Will ignore change="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/location/geofencer/a/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    iput-boolean v9, v10, Lcom/google/android/location/geofencer/service/ae;->e:Z

    if-eqz v0, :cond_0

    invoke-virtual {v10, p1}, Lcom/google/android/location/geofencer/service/ae;->a(Landroid/util/Pair;)Landroid/util/Pair;

    move-result-object p1

    goto/16 :goto_0

    :cond_5
    move v0, v9

    goto :goto_1

    :cond_6
    iget-boolean v0, v10, Lcom/google/android/location/geofencer/service/ae;->e:Z

    if-nez v0, :cond_0

    const-string v0, "LocationFilter"

    const-string v1, "Need at least one more cell location to confirm location change."

    invoke-static {v0, v1}, Lcom/google/android/location/geofencer/a/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    iput-boolean v8, v10, Lcom/google/android/location/geofencer/service/ae;->e:Z

    invoke-virtual {v10, p1}, Lcom/google/android/location/geofencer/service/ae;->a(Landroid/util/Pair;)Landroid/util/Pair;

    move-result-object p1

    goto/16 :goto_0
.end method

.method public final b()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1888
    invoke-super {p0}, Lcom/google/android/location/geofencer/service/o;->b()V

    .line 1889
    iput-boolean v2, p0, Lcom/google/android/location/geofencer/service/w;->f:Z

    .line 1890
    iget-object v0, p0, Lcom/google/android/location/geofencer/service/w;->a:Lcom/google/android/location/geofencer/service/k;

    invoke-static {v0}, Lcom/google/android/location/geofencer/service/k;->u(Lcom/google/android/location/geofencer/service/k;)Lcom/google/android/gms/common/util/p;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/common/util/p;->b()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/location/geofencer/service/w;->g:J

    .line 1891
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/location/geofencer/service/w;->h:Landroid/location/Location;

    .line 1892
    iput-short v2, p0, Lcom/google/android/location/geofencer/service/w;->i:S

    .line 1893
    return-void
.end method

.method public final c()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 1897
    invoke-super {p0}, Lcom/google/android/location/geofencer/service/o;->c()V

    .line 1898
    iput-boolean v1, p0, Lcom/google/android/location/geofencer/service/w;->f:Z

    .line 1899
    iget-object v0, p0, Lcom/google/android/location/geofencer/service/w;->j:Lcom/google/android/location/geofencer/service/ae;

    iput-object v2, v0, Lcom/google/android/location/geofencer/service/ae;->c:Landroid/location/Location;

    iput-object v2, v0, Lcom/google/android/location/geofencer/service/ae;->d:Landroid/net/wifi/WifiInfo;

    iput-boolean v1, v0, Lcom/google/android/location/geofencer/service/ae;->e:Z

    iget-object v0, v0, Lcom/google/android/location/geofencer/service/ae;->a:Lcom/google/android/location/activity/at;

    invoke-interface {v0}, Lcom/google/android/location/activity/at;->a()Z

    .line 1900
    return-void
.end method

.method protected final d()I
    .locals 4

    .prologue
    const/16 v0, 0x12c

    .line 2030
    iget-object v1, p0, Lcom/google/android/location/geofencer/service/w;->c:Lcom/google/android/location/geofencer/service/i;

    invoke-virtual {v1}, Lcom/google/android/location/geofencer/service/i;->a()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2050
    :cond_0
    :goto_0
    return v0

    .line 2036
    :cond_1
    iget-object v1, p0, Lcom/google/android/location/geofencer/service/w;->c:Lcom/google/android/location/geofencer/service/i;

    const-wide v2, 0x40f86a0000000000L    # 100000.0

    invoke-virtual {v1, v2, v3}, Lcom/google/android/location/geofencer/service/i;->c(D)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 2037
    const/4 v0, -0x1

    goto :goto_0

    .line 2041
    :cond_2
    iget-object v1, p0, Lcom/google/android/location/geofencer/service/w;->c:Lcom/google/android/location/geofencer/service/i;

    const-wide v2, 0x4072c00000000000L    # 300.0

    invoke-virtual {v1, v2, v3}, Lcom/google/android/location/geofencer/service/i;->c(D)Z

    move-result v1

    if-nez v1, :cond_0

    .line 2047
    iget-boolean v1, p0, Lcom/google/android/location/geofencer/service/w;->f:Z

    if-nez v1, :cond_0

    .line 2050
    const/16 v0, 0xb4

    goto :goto_0
.end method

.method protected final e()I
    .locals 12

    .prologue
    const/16 v3, 0x384

    const/16 v2, 0x3c

    const/4 v4, 0x0

    const/4 v1, 0x1

    .line 1993
    iget-object v0, p0, Lcom/google/android/location/geofencer/service/w;->c:Lcom/google/android/location/geofencer/service/i;

    invoke-virtual {v0}, Lcom/google/android/location/geofencer/service/i;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v2

    .line 2015
    :goto_0
    return v0

    .line 1996
    :cond_0
    iget-object v0, p0, Lcom/google/android/location/geofencer/service/w;->a:Lcom/google/android/location/geofencer/service/k;

    invoke-static {v0}, Lcom/google/android/location/geofencer/service/k;->u(Lcom/google/android/location/geofencer/service/k;)Lcom/google/android/gms/common/util/p;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/common/util/p;->b()J

    move-result-wide v6

    iget-wide v8, p0, Lcom/google/android/location/geofencer/service/w;->g:J

    sub-long v8, v6, v8

    const-wide/32 v10, 0x927c0

    cmp-long v0, v8, v10

    if-gtz v0, :cond_5

    iget-object v0, p0, Lcom/google/android/location/geofencer/service/w;->h:Landroid/location/Location;

    if-nez v0, :cond_5

    iget-short v0, p0, Lcom/google/android/location/geofencer/service/w;->i:S

    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/google/android/location/geofencer/service/w;->c:Lcom/google/android/location/geofencer/service/i;

    iget-object v0, v0, Lcom/google/android/location/geofencer/service/i;->d:Lcom/google/android/location/geofencer/data/n;

    iget-object v5, v0, Lcom/google/android/location/geofencer/data/n;->c:Landroid/util/Pair;

    if-eqz v5, :cond_1

    iget-object v0, v5, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Landroid/location/Location;

    invoke-virtual {v0}, Landroid/location/Location;->getAccuracy()F

    move-result v0

    const/high16 v8, 0x42c80000    # 100.0f

    cmpl-float v0, v0, v8

    if-gtz v0, :cond_1

    iget-object v0, v5, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    sub-long/2addr v6, v8

    const-wide/32 v8, 0x2bf20

    cmp-long v0, v6, v8

    if-lez v0, :cond_3

    :cond_1
    const-string v0, "GeofencerStateMachine"

    const-string v4, "Waiting for an accurate location to see if we want to get stable locations."

    invoke-static {v0, v4}, Lcom/google/android/location/geofencer/a/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_2
    :goto_1
    if-eqz v1, :cond_7

    move v0, v2

    .line 1997
    goto :goto_0

    .line 1996
    :cond_3
    iget-object v0, p0, Lcom/google/android/location/geofencer/service/w;->c:Lcom/google/android/location/geofencer/service/i;

    invoke-virtual {v0, v1}, Lcom/google/android/location/geofencer/service/i;->a(I)Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v5

    if-eqz v5, :cond_2

    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/geofencer/data/g;

    invoke-virtual {v0}, Lcom/google/android/location/geofencer/data/g;->a()D

    move-result-wide v6

    const-wide v8, 0x407f400000000000L    # 500.0

    cmpg-double v0, v6, v8

    if-gez v0, :cond_6

    move v0, v1

    :goto_2
    iput-short v0, p0, Lcom/google/android/location/geofencer/service/w;->i:S

    :cond_4
    iget-short v0, p0, Lcom/google/android/location/geofencer/service/w;->i:S

    if-eq v0, v1, :cond_2

    :cond_5
    move v1, v4

    goto :goto_1

    :cond_6
    const/4 v0, 0x2

    goto :goto_2

    .line 2001
    :cond_7
    iget-object v0, p0, Lcom/google/android/location/geofencer/service/w;->c:Lcom/google/android/location/geofencer/service/i;

    const-wide v4, 0x40aa0aaaaaaaaaabL    # 3333.3333333333335

    invoke-virtual {v0, v4, v5}, Lcom/google/android/location/geofencer/service/i;->c(D)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 2003
    const/16 v0, 0x708

    goto/16 :goto_0

    .line 2007
    :cond_8
    iget-object v0, p0, Lcom/google/android/location/geofencer/service/w;->c:Lcom/google/android/location/geofencer/service/i;

    const-wide v4, 0x4072c00000000000L    # 300.0

    invoke-virtual {v0, v4, v5}, Lcom/google/android/location/geofencer/service/i;->c(D)Z

    move-result v0

    if-eqz v0, :cond_9

    move v0, v3

    .line 2008
    goto/16 :goto_0

    .line 2012
    :cond_9
    iget-boolean v0, p0, Lcom/google/android/location/geofencer/service/w;->f:Z

    if-eqz v0, :cond_a

    move v0, v3

    .line 2013
    goto/16 :goto_0

    .line 2015
    :cond_a
    const/16 v0, 0x168

    goto/16 :goto_0
.end method

.method protected final f()I
    .locals 2

    .prologue
    .line 2021
    iget-object v0, p0, Lcom/google/android/location/geofencer/service/w;->c:Lcom/google/android/location/geofencer/service/i;

    iget v0, v0, Lcom/google/android/location/geofencer/service/i;->b:I

    .line 2022
    const v1, 0x7fffffff

    if-ne v0, v1, :cond_0

    .line 2023
    const/4 v0, -0x1

    .line 2025
    :cond_0
    return v0
.end method

.method protected final i()D
    .locals 2

    .prologue
    .line 1988
    const-wide/high16 v0, 0x3ff8000000000000L    # 1.5

    return-wide v0
.end method

.method protected final k()Z
    .locals 2

    .prologue
    .line 2076
    iget-object v0, p0, Lcom/google/android/location/geofencer/service/w;->j:Lcom/google/android/location/geofencer/service/ae;

    iget-object v1, v0, Lcom/google/android/location/geofencer/service/ae;->b:Lcom/google/android/location/h/c;

    invoke-static {}, Lcom/google/android/location/activity/k;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/google/android/location/geofencer/service/ae;->b()V

    .line 2077
    :goto_0
    const/4 v0, 0x0

    return v0

    .line 2076
    :cond_0
    iget-object v0, v0, Lcom/google/android/location/geofencer/service/ae;->a:Lcom/google/android/location/activity/at;

    invoke-interface {v0}, Lcom/google/android/location/activity/at;->a()Z

    const-string v0, "LocationFilter"

    const-string v1, "Significant motion detection disabled."

    invoke-static {v0, v1}, Lcom/google/android/location/geofencer/a/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected final l()Z
    .locals 2

    .prologue
    .line 2086
    const-string v0, "GeofencerStateMachine"

    const-string v1, "Significant motion detected. Computing a location in 30 seconds."

    invoke-static {v0, v1}, Lcom/google/android/location/geofencer/a/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2087
    const/16 v0, 0x1e

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/google/android/location/geofencer/service/w;->a(IZ)V

    .line 2088
    const/4 v0, 0x1

    return v0
.end method
