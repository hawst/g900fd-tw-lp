.class public final Lcom/google/android/location/settings/n;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final a:Lcom/google/android/location/settings/j;

.field b:Z

.field c:Z

.field d:Z

.field e:Z

.field f:Z

.field private final g:Z

.field private final h:Z

.field private final i:Z


# direct methods
.method private constructor <init>(Lcom/google/android/location/settings/j;ZZZ)V
    .locals 0

    .prologue
    .line 89
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 90
    iput-object p1, p0, Lcom/google/android/location/settings/n;->a:Lcom/google/android/location/settings/j;

    .line 91
    iput-boolean p2, p0, Lcom/google/android/location/settings/n;->g:Z

    .line 92
    iput-boolean p3, p0, Lcom/google/android/location/settings/n;->i:Z

    .line 93
    iput-boolean p4, p0, Lcom/google/android/location/settings/n;->h:Z

    .line 94
    invoke-virtual {p0}, Lcom/google/android/location/settings/n;->a()Z

    .line 95
    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/location/settings/j;ZZZB)V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/android/location/settings/n;-><init>(Lcom/google/android/location/settings/j;ZZZ)V

    return-void
.end method


# virtual methods
.method public final a()Z
    .locals 5

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 101
    .line 102
    iput-boolean v1, p0, Lcom/google/android/location/settings/n;->b:Z

    .line 103
    iput-boolean v1, p0, Lcom/google/android/location/settings/n;->c:Z

    .line 104
    iput-boolean v1, p0, Lcom/google/android/location/settings/n;->d:Z

    .line 105
    iput-boolean v1, p0, Lcom/google/android/location/settings/n;->e:Z

    .line 106
    iput-boolean v1, p0, Lcom/google/android/location/settings/n;->f:Z

    .line 108
    iget-boolean v0, p0, Lcom/google/android/location/settings/n;->i:Z

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/google/android/location/settings/n;->a:Lcom/google/android/location/settings/j;

    invoke-virtual {v0}, Lcom/google/android/location/settings/j;->a()Z

    move-result v0

    if-nez v0, :cond_7

    iget-object v0, p0, Lcom/google/android/location/settings/n;->a:Lcom/google/android/location/settings/j;

    iget-object v0, v0, Lcom/google/android/location/settings/j;->d:Landroid/content/pm/PackageManager;

    const-string v3, "android.hardware.location.gps"

    invoke-virtual {v0, v3}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 109
    iput-boolean v2, p0, Lcom/google/android/location/settings/n;->b:Z

    move v0, v1

    .line 112
    :goto_0
    iget-boolean v3, p0, Lcom/google/android/location/settings/n;->h:Z

    if-eqz v3, :cond_1

    .line 113
    iget-object v3, p0, Lcom/google/android/location/settings/n;->a:Lcom/google/android/location/settings/j;

    invoke-virtual {v3}, Lcom/google/android/location/settings/j;->b()Z

    move-result v3

    if-nez v3, :cond_0

    .line 114
    iput-boolean v2, p0, Lcom/google/android/location/settings/n;->c:Z

    move v0, v1

    .line 117
    :cond_0
    iget-object v3, p0, Lcom/google/android/location/settings/n;->a:Lcom/google/android/location/settings/j;

    iget-object v3, v3, Lcom/google/android/location/settings/j;->d:Landroid/content/pm/PackageManager;

    const-string v4, "android.hardware.wifi"

    invoke-virtual {v3, v4}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/google/android/location/settings/n;->a:Lcom/google/android/location/settings/j;

    iget-object v3, v3, Lcom/google/android/location/settings/j;->c:Landroid/net/wifi/WifiManager;

    invoke-virtual {v3}, Landroid/net/wifi/WifiManager;->isWifiEnabled()Z

    move-result v3

    if-nez v3, :cond_1

    iget-object v3, p0, Lcom/google/android/location/settings/n;->a:Lcom/google/android/location/settings/j;

    invoke-static {}, Lcom/google/android/location/settings/j;->c()Z

    move-result v4

    if-eqz v4, :cond_4

    iget-object v3, v3, Lcom/google/android/location/settings/j;->c:Landroid/net/wifi/WifiManager;

    invoke-virtual {v3}, Landroid/net/wifi/WifiManager;->isScanAlwaysAvailable()Z

    move-result v3

    :goto_1
    if-nez v3, :cond_1

    .line 119
    iget-object v0, p0, Lcom/google/android/location/settings/n;->a:Lcom/google/android/location/settings/j;

    invoke-static {}, Lcom/google/android/location/settings/j;->c()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 120
    iput-boolean v2, p0, Lcom/google/android/location/settings/n;->e:Z

    move v0, v1

    .line 129
    :cond_1
    :goto_2
    iget-boolean v3, p0, Lcom/google/android/location/settings/n;->i:Z

    if-nez v3, :cond_2

    iget-boolean v3, p0, Lcom/google/android/location/settings/n;->h:Z

    if-eqz v3, :cond_6

    :cond_2
    move v3, v2

    .line 130
    :goto_3
    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/google/android/location/settings/n;->a:Lcom/google/android/location/settings/j;

    iget-object v3, v3, Lcom/google/android/location/settings/j;->a:Landroid/content/Context;

    invoke-static {v3}, Lcom/google/android/gms/common/util/y;->a(Landroid/content/Context;)Z

    move-result v3

    if-nez v3, :cond_3

    iget-boolean v3, p0, Lcom/google/android/location/settings/n;->g:Z

    if-eqz v3, :cond_3

    .line 131
    iput-boolean v2, p0, Lcom/google/android/location/settings/n;->f:Z

    move v0, v1

    .line 135
    :cond_3
    return v0

    :cond_4
    move v3, v1

    .line 117
    goto :goto_1

    .line 123
    :cond_5
    iput-boolean v2, p0, Lcom/google/android/location/settings/n;->d:Z

    move v0, v1

    .line 124
    goto :goto_2

    :cond_6
    move v3, v1

    .line 129
    goto :goto_3

    :cond_7
    move v0, v2

    goto :goto_0
.end method
