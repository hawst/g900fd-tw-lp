.class public abstract Lcom/google/android/location/activity/f;
.super Lcom/google/android/location/collectionlib/cy;
.source "SourceFile"


# instance fields
.field protected a:Ljava/util/List;

.field final synthetic b:Lcom/google/android/location/activity/d;


# direct methods
.method protected constructor <init>(Lcom/google/android/location/activity/d;)V
    .locals 0

    .prologue
    .line 300
    iput-object p1, p0, Lcom/google/android/location/activity/f;->b:Lcom/google/android/location/activity/d;

    invoke-direct {p0}, Lcom/google/android/location/collectionlib/cy;-><init>()V

    return-void
.end method


# virtual methods
.method protected final a(JJLjava/util/List;J)J
    .locals 19

    .prologue
    .line 351
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/location/activity/f;->b:Lcom/google/android/location/activity/d;

    iget-object v3, v2, Lcom/google/android/location/activity/d;->c:Lcom/google/android/location/activity/a/o;

    move-wide/from16 v4, p1

    move-wide/from16 v6, p3

    move-object/from16 v8, p5

    invoke-interface/range {v3 .. v8}, Lcom/google/android/location/activity/a/o;->a(JJLjava/util/List;)Ljava/util/List;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/location/activity/f;->a:Ljava/util/List;

    .line 352
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/location/activity/f;->a:Ljava/util/List;

    const/4 v2, 0x0

    invoke-interface {v3, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/location/activity/a/s;

    iget-object v2, v2, Lcom/google/android/location/activity/a/s;->a:Lcom/google/android/location/activity/a/t;

    sget-object v4, Lcom/google/android/location/activity/a/t;->d:Lcom/google/android/location/activity/a/t;

    if-ne v2, v4, :cond_0

    const/4 v2, 0x1

    invoke-interface {v3, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 355
    :cond_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/location/activity/f;->b:Lcom/google/android/location/activity/d;

    invoke-static {v2}, Lcom/google/android/location/activity/d;->b(Lcom/google/android/location/activity/d;)Lcom/google/android/location/activity/ay;

    move-result-object v8

    const/4 v2, 0x3

    new-array v9, v2, [D

    const/4 v3, 0x0

    const/4 v2, 0x0

    move v4, v3

    move v3, v2

    :goto_0
    invoke-interface/range {p5 .. p5}, Ljava/util/List;->size()I

    move-result v2

    if-ge v3, v2, :cond_1

    if-ltz v3, :cond_1

    move-object/from16 v0, p5

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/location/d/i;

    iget-wide v6, v2, Lcom/google/android/location/d/i;->a:J

    const-wide v10, 0x7fffffffffffffffL

    cmp-long v5, v6, v10

    if-gtz v5, :cond_1

    const/4 v5, 0x0

    aget-wide v6, v9, v5

    iget-object v10, v2, Lcom/google/android/location/d/i;->b:[F

    const/4 v11, 0x0

    aget v10, v10, v11

    float-to-double v10, v10

    add-double/2addr v6, v10

    aput-wide v6, v9, v5

    const/4 v5, 0x1

    aget-wide v6, v9, v5

    iget-object v10, v2, Lcom/google/android/location/d/i;->b:[F

    const/4 v11, 0x1

    aget v10, v10, v11

    float-to-double v10, v10

    add-double/2addr v6, v10

    aput-wide v6, v9, v5

    const/4 v5, 0x2

    aget-wide v6, v9, v5

    iget-object v2, v2, Lcom/google/android/location/d/i;->b:[F

    const/4 v10, 0x2

    aget v2, v2, v10

    float-to-double v10, v2

    add-double/2addr v6, v10

    aput-wide v6, v9, v5

    add-int/lit8 v4, v4, 0x1

    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_0

    :cond_1
    const/4 v2, 0x0

    aget-wide v6, v9, v2

    int-to-double v10, v4

    div-double/2addr v6, v10

    aput-wide v6, v9, v2

    const/4 v2, 0x1

    aget-wide v6, v9, v2

    int-to-double v10, v4

    div-double/2addr v6, v10

    aput-wide v6, v9, v2

    const/4 v2, 0x2

    aget-wide v6, v9, v2

    int-to-double v4, v4

    div-double v4, v6, v4

    aput-wide v4, v9, v2

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/location/activity/f;->a:Ljava/util/List;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/location/activity/f;->b:Lcom/google/android/location/activity/d;

    invoke-static {v3}, Lcom/google/android/location/activity/d;->a(Lcom/google/android/location/activity/d;)D

    move-result-wide v4

    iget-wide v6, v8, Lcom/google/android/location/activity/ay;->c:J

    cmp-long v3, p3, v6

    if-gtz v3, :cond_6

    const/4 v2, 0x0

    .line 359
    :goto_1
    if-eqz v2, :cond_3

    .line 365
    sget-boolean v3, Lcom/google/android/location/i/a;->b:Z

    if-eqz v3, :cond_2

    const-string v3, "ActivityDetectionRunner"

    const-string v4, "Significant tilt detected between activities"

    invoke-static {v3, v4}, Lcom/google/android/location/o/a/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 366
    :cond_2
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/location/activity/f;->b:Lcom/google/android/location/activity/d;

    iget-object v3, v3, Lcom/google/android/location/activity/d;->a:Lcom/google/android/location/os/bi;

    invoke-interface {v3}, Lcom/google/android/location/os/bi;->v()Lcom/google/android/location/os/j;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v3, v2, v4}, Lcom/google/android/location/os/j;->a(Lcom/google/android/gms/location/ActivityRecognitionResult;Z)V

    .line 367
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/location/activity/f;->b:Lcom/google/android/location/activity/d;

    iget-object v3, v3, Lcom/google/android/location/activity/d;->d:Lcom/google/android/location/activity/e;

    invoke-interface {v3, v2}, Lcom/google/android/location/activity/e;->a(Lcom/google/android/gms/location/ActivityRecognitionResult;)V

    .line 370
    :cond_3
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/location/activity/f;->a:Ljava/util/List;

    invoke-static {v2}, Lcom/google/android/location/activity/d;->c(Ljava/util/List;)Ljava/util/List;

    move-result-object v3

    .line 372
    new-instance v2, Lcom/google/android/gms/location/ActivityRecognitionResult;

    move-wide/from16 v4, p1

    move-wide/from16 v6, p3

    invoke-direct/range {v2 .. v7}, Lcom/google/android/gms/location/ActivityRecognitionResult;-><init>(Ljava/util/List;JJ)V

    .line 374
    sget-boolean v3, Lcom/google/android/location/i/a;->b:Z

    if-eqz v3, :cond_4

    const-string v3, "ActivityDetectionRunner"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Activity detection result: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/android/location/o/a/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 375
    :cond_4
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/location/activity/f;->b:Lcom/google/android/location/activity/d;

    iget-object v3, v3, Lcom/google/android/location/activity/d;->a:Lcom/google/android/location/os/bi;

    invoke-interface {v3}, Lcom/google/android/location/os/bi;->v()Lcom/google/android/location/os/j;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v3, v2, v4}, Lcom/google/android/location/os/j;->a(Lcom/google/android/gms/location/ActivityRecognitionResult;Z)V

    .line 376
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/location/activity/f;->b:Lcom/google/android/location/activity/d;

    iget-object v3, v3, Lcom/google/android/location/activity/d;->d:Lcom/google/android/location/activity/e;

    move-wide/from16 v0, p6

    invoke-interface {v3, v2, v0, v1}, Lcom/google/android/location/activity/e;->a(Lcom/google/android/gms/location/ActivityRecognitionResult;J)J

    move-result-wide v2

    .line 377
    sget-boolean v4, Lcom/google/android/location/i/a;->b:Z

    if-eqz v4, :cond_5

    const-string v4, "ActivityDetectionRunner"

    const-string v5, "onAccelData: callback.onActivityDetected(nlpActivity, %d) returns %d"

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    invoke-static/range {p6 .. p7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x1

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/android/location/o/a/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 380
    :cond_5
    return-wide v2

    .line 355
    :cond_6
    const/4 v3, 0x0

    iget-object v6, v8, Lcom/google/android/location/activity/ay;->a:[D

    if-eqz v6, :cond_9

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v6

    if-lez v6, :cond_8

    const/4 v6, 0x0

    invoke-interface {v2, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/location/activity/a/s;

    iget-object v2, v2, Lcom/google/android/location/activity/a/s;->a:Lcom/google/android/location/activity/a/t;

    sget-object v6, Lcom/google/android/location/activity/a/t;->i:Lcom/google/android/location/activity/a/t;

    if-ne v2, v6, :cond_8

    const/4 v2, 0x1

    :goto_2
    if-nez v2, :cond_9

    iget-object v2, v8, Lcom/google/android/location/activity/ay;->a:[D

    new-instance v6, Lcom/google/android/location/o/k;

    invoke-direct {v6, v9}, Lcom/google/android/location/o/k;-><init>([D)V

    new-instance v7, Lcom/google/android/location/o/k;

    invoke-direct {v7, v2}, Lcom/google/android/location/o/k;-><init>([D)V

    iget-wide v10, v7, Lcom/google/android/location/o/k;->a:D

    iget-wide v12, v7, Lcom/google/android/location/o/k;->b:D

    iget-wide v14, v7, Lcom/google/android/location/o/k;->c:D

    iget-wide v0, v6, Lcom/google/android/location/o/k;->a:D

    move-wide/from16 v16, v0

    mul-double v10, v10, v16

    iget-wide v0, v6, Lcom/google/android/location/o/k;->b:D

    move-wide/from16 v16, v0

    mul-double v12, v12, v16

    add-double/2addr v10, v12

    iget-wide v12, v6, Lcom/google/android/location/o/k;->c:D

    mul-double/2addr v12, v14

    add-double/2addr v10, v12

    invoke-virtual {v6}, Lcom/google/android/location/o/k;->a()D

    move-result-wide v12

    invoke-virtual {v7}, Lcom/google/android/location/o/k;->a()D

    move-result-wide v6

    mul-double/2addr v6, v12

    div-double v6, v10, v6

    invoke-static {v6, v7}, Ljava/lang/Math;->acos(D)D

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Math;->toDegrees(D)D

    move-result-wide v6

    sget-boolean v2, Lcom/google/android/location/i/a;->b:Z

    if-eqz v2, :cond_7

    const-string v2, "TiltDetector"

    new-instance v10, Ljava/lang/StringBuilder;

    const-string v11, "Angle change since last classification is: "

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10, v6, v7}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v2, v10}, Lcom/google/android/location/o/a/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_7
    cmpl-double v2, v6, v4

    if-ltz v2, :cond_9

    iget-wide v2, v8, Lcom/google/android/location/activity/ay;->b:J

    sub-long v2, p1, v2

    const-wide/16 v4, 0x2

    div-long/2addr v2, v4

    sub-long v4, p1, v2

    iget-wide v2, v8, Lcom/google/android/location/activity/ay;->c:J

    sub-long v2, p3, v2

    const-wide/16 v6, 0x2

    div-long/2addr v2, v6

    sub-long v6, p3, v2

    new-instance v2, Lcom/google/android/gms/location/ActivityRecognitionResult;

    new-instance v3, Lcom/google/android/gms/location/DetectedActivity;

    const/4 v10, 0x5

    const/16 v11, 0x64

    invoke-direct {v3, v10, v11}, Lcom/google/android/gms/location/DetectedActivity;-><init>(II)V

    invoke-direct/range {v2 .. v7}, Lcom/google/android/gms/location/ActivityRecognitionResult;-><init>(Lcom/google/android/gms/location/DetectedActivity;JJ)V

    :goto_3
    iput-object v9, v8, Lcom/google/android/location/activity/ay;->a:[D

    move-wide/from16 v0, p1

    iput-wide v0, v8, Lcom/google/android/location/activity/ay;->b:J

    move-wide/from16 v0, p3

    iput-wide v0, v8, Lcom/google/android/location/activity/ay;->c:J

    goto/16 :goto_1

    :cond_8
    const/4 v2, 0x0

    goto :goto_2

    :cond_9
    move-object v2, v3

    goto :goto_3
.end method

.method protected abstract a(JJLjava/util/List;)Ljava/lang/String;
.end method

.method public final a(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 458
    iget-object v0, p0, Lcom/google/android/location/activity/f;->b:Lcom/google/android/location/activity/d;

    iget-boolean v0, v0, Lcom/google/android/location/activity/d;->b:Z

    if-nez v0, :cond_0

    .line 463
    :goto_0
    return-void

    .line 461
    :cond_0
    iget-object v0, p0, Lcom/google/android/location/activity/f;->b:Lcom/google/android/location/activity/d;

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/google/android/location/activity/d;->b:Z

    .line 462
    iget-object v0, p0, Lcom/google/android/location/activity/f;->b:Lcom/google/android/location/activity/d;

    iget-object v0, v0, Lcom/google/android/location/activity/d;->d:Lcom/google/android/location/activity/e;

    invoke-interface {v0, p1}, Lcom/google/android/location/activity/e;->a(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final a(Ljava/util/Map;)V
    .locals 10

    .prologue
    .line 311
    iget-object v0, p0, Lcom/google/android/location/activity/f;->b:Lcom/google/android/location/activity/d;

    iget-boolean v0, v0, Lcom/google/android/location/activity/d;->b:Z

    if-nez v0, :cond_1

    .line 312
    sget-boolean v0, Lcom/google/android/location/i/a;->b:Z

    if-eqz v0, :cond_0

    const-string v0, "ActivityDetectionRunner"

    const-string v1, "Not processing accel data since activity detection was canceled."

    invoke-static {v0, v1}, Lcom/google/android/location/o/a/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 334
    :cond_0
    :goto_0
    return-void

    .line 315
    :cond_1
    iget-object v0, p0, Lcom/google/android/location/activity/f;->b:Lcom/google/android/location/activity/d;

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/google/android/location/activity/d;->b:Z

    .line 316
    iget-object v0, p0, Lcom/google/android/location/activity/f;->b:Lcom/google/android/location/activity/d;

    iget-object v0, v0, Lcom/google/android/location/activity/d;->a:Lcom/google/android/location/os/bi;

    invoke-interface {v0}, Lcom/google/android/location/os/bi;->c()Lcom/google/android/location/j/b;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/location/j/b;->b()J

    move-result-wide v2

    .line 317
    iget-object v0, p0, Lcom/google/android/location/activity/f;->b:Lcom/google/android/location/activity/d;

    iget-object v0, v0, Lcom/google/android/location/activity/d;->a:Lcom/google/android/location/os/bi;

    invoke-interface {v0}, Lcom/google/android/location/os/bi;->c()Lcom/google/android/location/j/b;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/location/j/b;->c()J

    move-result-wide v4

    .line 318
    sget-object v0, Lcom/google/android/location/collectionlib/cg;->d:Lcom/google/android/location/collectionlib/cg;

    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 319
    if-nez v0, :cond_2

    .line 320
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    .line 324
    :cond_2
    iget-object v1, p0, Lcom/google/android/location/activity/f;->b:Lcom/google/android/location/activity/d;

    invoke-virtual {v1, v0}, Lcom/google/android/location/activity/d;->b(Ljava/util/List;)Ljava/util/List;

    move-result-object v6

    .line 325
    iget-object v1, p0, Lcom/google/android/location/activity/f;->b:Lcom/google/android/location/activity/d;

    invoke-virtual {v1, v6}, Lcom/google/android/location/activity/d;->d(Ljava/util/List;)V

    .line 326
    invoke-static {v0, v6}, Lcom/google/android/location/activity/d;->a(Ljava/util/List;Ljava/util/List;)V

    .line 327
    iget-object v1, p0, Lcom/google/android/location/activity/f;->b:Lcom/google/android/location/activity/d;

    iget-object v1, v1, Lcom/google/android/location/activity/d;->d:Lcom/google/android/location/activity/e;

    invoke-static {v0}, Lcom/google/android/location/activity/d;->a(Ljava/util/List;)D

    move-result-wide v8

    invoke-interface {v1, v8, v9}, Lcom/google/android/location/activity/e;->a(D)V

    move-object v1, p0

    .line 328
    invoke-virtual/range {v1 .. v6}, Lcom/google/android/location/activity/f;->a(JJLjava/util/List;)Ljava/lang/String;

    goto :goto_0
.end method

.method protected final b(JJLjava/util/List;)V
    .locals 37

    .prologue
    .line 385
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/location/activity/f;->b:Lcom/google/android/location/activity/d;

    iget-object v7, v2, Lcom/google/android/location/activity/d;->f:Lcom/google/android/location/activity/a/a/b;

    iget-wide v2, v7, Lcom/google/android/location/activity/a/a/b;->a:J

    cmp-long v2, p3, v2

    if-gtz v2, :cond_2

    const/4 v2, 0x0

    .line 387
    :goto_0
    if-eqz v2, :cond_1

    .line 388
    invoke-static {v2}, Lcom/google/android/location/activity/d;->c(Ljava/util/List;)Ljava/util/List;

    move-result-object v3

    .line 390
    new-instance v2, Lcom/google/android/gms/location/ActivityRecognitionResult;

    move-wide/from16 v4, p1

    move-wide/from16 v6, p3

    invoke-direct/range {v2 .. v7}, Lcom/google/android/gms/location/ActivityRecognitionResult;-><init>(Ljava/util/List;JJ)V

    .line 392
    sget-boolean v3, Lcom/google/android/location/i/a;->b:Z

    if-eqz v3, :cond_0

    const-string v3, "ActivityDetectionRunner"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Phone position detection result: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/android/location/o/a/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 393
    :cond_0
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/location/activity/f;->b:Lcom/google/android/location/activity/d;

    iget-object v3, v3, Lcom/google/android/location/activity/d;->a:Lcom/google/android/location/os/bi;

    invoke-interface {v3}, Lcom/google/android/location/os/bi;->v()Lcom/google/android/location/os/j;

    move-result-object v3

    const/4 v4, 0x1

    invoke-virtual {v3, v2, v4}, Lcom/google/android/location/os/j;->a(Lcom/google/android/gms/location/ActivityRecognitionResult;Z)V

    .line 394
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/location/activity/f;->b:Lcom/google/android/location/activity/d;

    iget-object v3, v3, Lcom/google/android/location/activity/d;->a:Lcom/google/android/location/os/bi;

    invoke-interface {v3, v2}, Lcom/google/android/location/os/bi;->a(Lcom/google/android/gms/location/ActivityRecognitionResult;)V

    .line 396
    :cond_1
    return-void

    .line 385
    :cond_2
    const-wide/high16 v2, 0x4049000000000000L    # 50.0

    move-object/from16 v0, p5

    invoke-static {v0, v2, v3}, Lcom/google/android/location/activity/a/y;->a(Ljava/util/List;D)[[D

    move-result-object v5

    const/4 v2, 0x0

    aget-object v3, v5, v2

    const/4 v2, 0x1

    aget-object v4, v5, v2

    const/4 v2, 0x2

    aget-object v6, v5, v2

    array-length v8, v3

    new-array v9, v8, [D

    new-array v10, v8, [D

    const/4 v2, 0x0

    :goto_1
    if-ge v2, v8, :cond_3

    aget-wide v12, v3, v2

    invoke-static {v12, v13}, Ljava/lang/Math;->abs(D)D

    move-result-wide v12

    aget-wide v14, v4, v2

    invoke-static {v14, v15}, Ljava/lang/Math;->abs(D)D

    move-result-wide v14

    add-double/2addr v12, v14

    aget-wide v14, v6, v2

    invoke-static {v14, v15}, Ljava/lang/Math;->abs(D)D

    move-result-wide v14

    add-double/2addr v12, v14

    aput-wide v12, v9, v2

    aget-wide v12, v3, v2

    aget-wide v14, v3, v2

    mul-double/2addr v12, v14

    aget-wide v14, v4, v2

    aget-wide v16, v4, v2

    mul-double v14, v14, v16

    add-double/2addr v12, v14

    aget-wide v14, v6, v2

    aget-wide v16, v6, v2

    mul-double v14, v14, v16

    add-double/2addr v12, v14

    invoke-static {v12, v13}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v12

    aput-wide v12, v10, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_3
    new-instance v8, Lcom/google/android/location/activity/a/a/d;

    invoke-direct {v8}, Lcom/google/android/location/activity/a/a/d;-><init>()V

    invoke-static {v3}, Lcom/google/android/location/activity/a/y;->a([D)D

    move-result-wide v12

    invoke-static {v4}, Lcom/google/android/location/activity/a/y;->a([D)D

    move-result-wide v14

    invoke-static {v6}, Lcom/google/android/location/activity/a/y;->a([D)D

    move-result-wide v16

    invoke-static {v9}, Lcom/google/android/location/activity/a/y;->a([D)D

    move-result-wide v18

    invoke-static {v10}, Lcom/google/android/location/activity/a/y;->a([D)D

    move-result-wide v20

    double-to-float v2, v12

    iput v2, v8, Lcom/google/android/location/activity/a/a/d;->a:F

    double-to-float v2, v14

    iput v2, v8, Lcom/google/android/location/activity/a/a/d;->b:F

    move-wide/from16 v0, v16

    double-to-float v2, v0

    iput v2, v8, Lcom/google/android/location/activity/a/a/d;->c:F

    move-wide/from16 v0, v18

    double-to-float v2, v0

    iput v2, v8, Lcom/google/android/location/activity/a/a/d;->d:F

    move-wide/from16 v0, v20

    double-to-float v2, v0

    iput v2, v8, Lcom/google/android/location/activity/a/a/d;->e:F

    invoke-static {v3}, Lcom/google/android/location/activity/a/y;->c([D)D

    move-result-wide v22

    mul-double v24, v12, v12

    sub-double v22, v22, v24

    invoke-static {v4}, Lcom/google/android/location/activity/a/y;->c([D)D

    move-result-wide v24

    mul-double v26, v14, v14

    sub-double v24, v24, v26

    invoke-static {v6}, Lcom/google/android/location/activity/a/y;->c([D)D

    move-result-wide v26

    mul-double v28, v16, v16

    sub-double v26, v26, v28

    invoke-static {v10}, Lcom/google/android/location/activity/a/y;->c([D)D

    move-result-wide v28

    mul-double v30, v20, v20

    sub-double v28, v28, v30

    const-wide v30, 0x3ddb7cdfd9d7bdbbL    # 1.0E-10

    cmpg-double v2, v28, v30

    if-gez v2, :cond_4

    const/4 v2, 0x0

    :goto_2
    iput v2, v8, Lcom/google/android/location/activity/a/a/d;->G:F

    const-wide v22, 0x3ddb7cdfd9d7bdbbL    # 1.0E-10

    cmpg-double v2, v28, v22

    if-gez v2, :cond_5

    const/4 v2, 0x0

    :goto_3
    iput v2, v8, Lcom/google/android/location/activity/a/a/d;->H:F

    const-wide v22, 0x3ddb7cdfd9d7bdbbL    # 1.0E-10

    cmpg-double v2, v28, v22

    if-gez v2, :cond_6

    const/4 v2, 0x0

    :goto_4
    iput v2, v8, Lcom/google/android/location/activity/a/a/d;->I:F

    invoke-static {v3, v12, v13}, Lcom/google/android/location/activity/a/a/c;->a([DD)D

    move-result-wide v22

    invoke-static {v4, v14, v15}, Lcom/google/android/location/activity/a/a/c;->a([DD)D

    move-result-wide v24

    move-wide/from16 v0, v16

    invoke-static {v6, v0, v1}, Lcom/google/android/location/activity/a/a/c;->a([DD)D

    move-result-wide v26

    move-wide/from16 v0, v18

    invoke-static {v9, v0, v1}, Lcom/google/android/location/activity/a/a/c;->a([DD)D

    move-result-wide v28

    move-wide/from16 v0, v20

    invoke-static {v10, v0, v1}, Lcom/google/android/location/activity/a/a/c;->a([DD)D

    move-result-wide v30

    move-wide/from16 v0, v22

    double-to-float v2, v0

    iput v2, v8, Lcom/google/android/location/activity/a/a/d;->u:F

    move-wide/from16 v0, v24

    double-to-float v2, v0

    iput v2, v8, Lcom/google/android/location/activity/a/a/d;->v:F

    move-wide/from16 v0, v26

    double-to-float v2, v0

    iput v2, v8, Lcom/google/android/location/activity/a/a/d;->w:F

    move-wide/from16 v0, v28

    double-to-float v2, v0

    iput v2, v8, Lcom/google/android/location/activity/a/a/d;->x:F

    move-wide/from16 v0, v30

    double-to-float v2, v0

    iput v2, v8, Lcom/google/android/location/activity/a/a/d;->y:F

    invoke-static {v12, v13}, Ljava/lang/Math;->abs(D)D

    move-result-wide v32

    const-wide v34, 0x3ddb7cdfd9d7bdbbL    # 1.0E-10

    cmpg-double v2, v32, v34

    if-gez v2, :cond_7

    const/4 v2, 0x0

    :goto_5
    iput v2, v8, Lcom/google/android/location/activity/a/a/d;->z:F

    invoke-static {v14, v15}, Ljava/lang/Math;->abs(D)D

    move-result-wide v22

    const-wide v32, 0x3ddb7cdfd9d7bdbbL    # 1.0E-10

    cmpg-double v2, v22, v32

    if-gez v2, :cond_8

    const/4 v2, 0x0

    :goto_6
    iput v2, v8, Lcom/google/android/location/activity/a/a/d;->A:F

    invoke-static/range {v16 .. v17}, Ljava/lang/Math;->abs(D)D

    move-result-wide v22

    const-wide v24, 0x3ddb7cdfd9d7bdbbL    # 1.0E-10

    cmpg-double v2, v22, v24

    if-gez v2, :cond_9

    const/4 v2, 0x0

    :goto_7
    iput v2, v8, Lcom/google/android/location/activity/a/a/d;->B:F

    const-wide v22, 0x3ddb7cdfd9d7bdbbL    # 1.0E-10

    cmpg-double v2, v18, v22

    if-gez v2, :cond_a

    const/4 v2, 0x0

    :goto_8
    iput v2, v8, Lcom/google/android/location/activity/a/a/d;->C:F

    const-wide v22, 0x3ddb7cdfd9d7bdbbL    # 1.0E-10

    cmpg-double v2, v20, v22

    if-gez v2, :cond_b

    const/4 v2, 0x0

    :goto_9
    iput v2, v8, Lcom/google/android/location/activity/a/a/d;->D:F

    invoke-static {v3}, Lcom/google/android/location/activity/a/a/c;->a([D)D

    move-result-wide v22

    move-wide/from16 v0, v22

    double-to-float v2, v0

    iput v2, v8, Lcom/google/android/location/activity/a/a/d;->p:F

    invoke-static {v4}, Lcom/google/android/location/activity/a/a/c;->a([D)D

    move-result-wide v22

    move-wide/from16 v0, v22

    double-to-float v2, v0

    iput v2, v8, Lcom/google/android/location/activity/a/a/d;->q:F

    invoke-static {v6}, Lcom/google/android/location/activity/a/a/c;->a([D)D

    move-result-wide v22

    move-wide/from16 v0, v22

    double-to-float v2, v0

    iput v2, v8, Lcom/google/android/location/activity/a/a/d;->r:F

    invoke-static {v9}, Lcom/google/android/location/activity/a/a/c;->a([D)D

    move-result-wide v22

    move-wide/from16 v0, v22

    double-to-float v2, v0

    iput v2, v8, Lcom/google/android/location/activity/a/a/d;->s:F

    invoke-static {v10}, Lcom/google/android/location/activity/a/a/c;->a([D)D

    move-result-wide v22

    move-wide/from16 v0, v22

    double-to-float v2, v0

    iput v2, v8, Lcom/google/android/location/activity/a/a/d;->t:F

    invoke-static {v3}, Lcom/google/android/location/activity/a/a/c;->b([D)F

    move-result v2

    iput v2, v8, Lcom/google/android/location/activity/a/a/d;->k:F

    invoke-static {v4}, Lcom/google/android/location/activity/a/a/c;->b([D)F

    move-result v2

    iput v2, v8, Lcom/google/android/location/activity/a/a/d;->l:F

    invoke-static {v6}, Lcom/google/android/location/activity/a/a/c;->b([D)F

    move-result v2

    iput v2, v8, Lcom/google/android/location/activity/a/a/d;->m:F

    invoke-static {v9}, Lcom/google/android/location/activity/a/a/c;->b([D)F

    move-result v2

    iput v2, v8, Lcom/google/android/location/activity/a/a/d;->n:F

    invoke-static {v10}, Lcom/google/android/location/activity/a/a/c;->b([D)F

    move-result v2

    iput v2, v8, Lcom/google/android/location/activity/a/a/d;->o:F

    invoke-static {v3, v12, v13}, Lcom/google/android/location/activity/a/a/c;->b([DD)F

    move-result v2

    iput v2, v8, Lcom/google/android/location/activity/a/a/d;->f:F

    invoke-static {v4, v14, v15}, Lcom/google/android/location/activity/a/a/c;->b([DD)F

    move-result v2

    iput v2, v8, Lcom/google/android/location/activity/a/a/d;->g:F

    move-wide/from16 v0, v16

    invoke-static {v6, v0, v1}, Lcom/google/android/location/activity/a/a/c;->b([DD)F

    move-result v2

    iput v2, v8, Lcom/google/android/location/activity/a/a/d;->h:F

    move-wide/from16 v0, v18

    invoke-static {v9, v0, v1}, Lcom/google/android/location/activity/a/a/c;->b([DD)F

    move-result v2

    iput v2, v8, Lcom/google/android/location/activity/a/a/d;->i:F

    move-wide/from16 v0, v20

    invoke-static {v10, v0, v1}, Lcom/google/android/location/activity/a/a/c;->b([DD)F

    move-result v2

    iput v2, v8, Lcom/google/android/location/activity/a/a/d;->j:F

    array-length v6, v5

    const/4 v2, 0x0

    aget-object v2, v5, v2

    array-length v2, v2

    rem-int/lit8 v9, v2, 0x5

    sub-int/2addr v2, v9

    div-int/lit8 v10, v2, 0x5

    filled-new-array {v10, v6}, [I

    move-result-object v2

    sget-object v3, Ljava/lang/Double;->TYPE:Ljava/lang/Class;

    invoke-static {v3, v2}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;[I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [[D

    const/4 v3, 0x0

    move v4, v3

    :goto_a
    if-ge v4, v10, :cond_d

    new-array v11, v6, [D

    mul-int/lit8 v3, v4, 0x5

    add-int v12, v3, v9

    const/4 v3, 0x0

    :goto_b
    if-ge v3, v6, :cond_c

    const/4 v13, 0x5

    new-array v13, v13, [D

    aget-object v14, v5, v3

    const/4 v15, 0x0

    const/16 v16, 0x5

    move/from16 v0, v16

    invoke-static {v14, v12, v13, v15, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    invoke-static {v13}, Lcom/google/android/location/activity/a/y;->a([D)D

    move-result-wide v14

    aput-wide v14, v11, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_b

    :cond_4
    div-double v22, v22, v28

    move-wide/from16 v0, v22

    double-to-float v2, v0

    goto/16 :goto_2

    :cond_5
    div-double v22, v24, v28

    move-wide/from16 v0, v22

    double-to-float v2, v0

    goto/16 :goto_3

    :cond_6
    div-double v22, v26, v28

    move-wide/from16 v0, v22

    double-to-float v2, v0

    goto/16 :goto_4

    :cond_7
    div-double v22, v22, v12

    move-wide/from16 v0, v22

    double-to-float v2, v0

    goto/16 :goto_5

    :cond_8
    div-double v22, v24, v14

    move-wide/from16 v0, v22

    double-to-float v2, v0

    goto/16 :goto_6

    :cond_9
    div-double v22, v26, v16

    move-wide/from16 v0, v22

    double-to-float v2, v0

    goto/16 :goto_7

    :cond_a
    div-double v22, v28, v18

    move-wide/from16 v0, v22

    double-to-float v2, v0

    goto/16 :goto_8

    :cond_b
    div-double v22, v30, v20

    move-wide/from16 v0, v22

    double-to-float v2, v0

    goto/16 :goto_9

    :cond_c
    const/4 v3, 0x0

    aget-object v12, v2, v4

    const/4 v13, 0x0

    invoke-static {v11, v3, v12, v13, v6}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    add-int/lit8 v3, v4, 0x1

    move v4, v3

    goto :goto_a

    :cond_d
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    new-array v9, v3, [D

    const/4 v3, 0x0

    move v6, v3

    :goto_c
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v6, v3, :cond_f

    aget-object v10, v2, v6

    add-int/lit8 v3, v6, 0x1

    aget-object v11, v2, v3

    const-wide/16 v4, 0x0

    const/4 v3, 0x0

    :goto_d
    array-length v12, v10

    if-ge v3, v12, :cond_e

    aget-wide v12, v10, v3

    aget-wide v14, v11, v3

    mul-double/2addr v12, v14

    add-double/2addr v4, v12

    add-int/lit8 v3, v3, 0x1

    goto :goto_d

    :cond_e
    invoke-static {v10}, Lcom/google/android/location/activity/a/y;->d([D)D

    move-result-wide v12

    invoke-static {v11}, Lcom/google/android/location/activity/a/y;->d([D)D

    move-result-wide v10

    mul-double/2addr v10, v12

    invoke-static {v10, v11}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v10

    div-double/2addr v4, v10

    aput-wide v4, v9, v6

    add-int/lit8 v3, v6, 0x1

    move v6, v3

    goto :goto_c

    :cond_f
    invoke-static {v9}, Lcom/google/android/location/activity/a/y;->a([D)D

    move-result-wide v2

    double-to-float v4, v2

    iput v4, v8, Lcom/google/android/location/activity/a/a/d;->E:F

    invoke-static {v9}, Lcom/google/android/location/activity/a/y;->c([D)D

    move-result-wide v4

    mul-double/2addr v2, v2

    sub-double v2, v4, v2

    double-to-float v2, v2

    iput v2, v8, Lcom/google/android/location/activity/a/a/d;->F:F

    iget-object v3, v7, Lcom/google/android/location/activity/a/a/b;->b:Lcom/google/android/location/activity/a/a/a;

    const/4 v2, 0x0

    iget-object v4, v3, Lcom/google/android/location/activity/a/a/a;->a:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    mul-int/lit8 v4, v4, 0xf

    invoke-static {}, Lcom/google/android/location/activity/a/t;->values()[Lcom/google/android/location/activity/a/t;

    move-result-object v5

    const/16 v6, 0xb

    aget-object v5, v5, v6

    iget-object v3, v3, Lcom/google/android/location/activity/a/a/a;->a:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    move v3, v2

    :goto_e
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_10

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/location/activity/a/a/o;

    invoke-virtual {v2, v8}, Lcom/google/android/location/activity/a/a/o;->a(Lcom/google/android/location/activity/a/a/d;)Lcom/google/android/location/activity/a/s;

    move-result-object v2

    iget v2, v2, Lcom/google/android/location/activity/a/s;->b:I

    add-int/2addr v2, v3

    move v3, v2

    goto :goto_e

    :cond_10
    mul-int/lit8 v2, v3, 0x64

    int-to-float v2, v2

    int-to-float v3, v4

    div-float/2addr v2, v3

    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    move-result v3

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    new-instance v4, Lcom/google/android/location/activity/a/s;

    invoke-direct {v4, v5, v3}, Lcom/google/android/location/activity/a/s;-><init>(Lcom/google/android/location/activity/a/t;I)V

    invoke-interface {v2, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move-wide/from16 v0, p3

    iput-wide v0, v7, Lcom/google/android/location/activity/a/a/b;->a:J

    goto/16 :goto_0
.end method
