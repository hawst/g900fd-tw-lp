.class final Lcom/google/android/location/places/d/c;
.super Landroid/content/BroadcastReceiver;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/location/places/d/b;


# direct methods
.method constructor <init>(Lcom/google/android/location/places/d/b;)V
    .locals 0

    .prologue
    .line 172
    iput-object p1, p0, Lcom/google/android/location/places/d/c;->a:Lcom/google/android/location/places/d/b;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public final onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 5

    .prologue
    const/4 v0, 0x0

    const/4 v4, -0x1

    .line 175
    const-string v1, "com.google.android.location.internal.action.PLACES_REFRESH_USER_DATA_SUBSCRIPTION"

    invoke-static {v1}, Lcom/google/android/location/internal/PendingIntentCallbackService;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 177
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 178
    iget-object v1, p0, Lcom/google/android/location/places/d/c;->a:Lcom/google/android/location/places/d/b;

    if-nez p2, :cond_3

    :cond_0
    :goto_0
    if-eqz v0, :cond_4

    iget-object v2, v1, Lcom/google/android/location/places/d/b;->g:Ljava/util/Map;

    invoke-interface {v2, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    const-string v2, "Places"

    const/4 v3, 0x4

    invoke-static {v2, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_1

    const-string v2, "Places"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Alarm received, time to refresh subscription "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    iget-object v1, v1, Lcom/google/android/location/places/d/b;->f:Lcom/google/android/location/places/d/d;

    invoke-interface {v1, v0}, Lcom/google/android/location/places/d/d;->a(Lcom/google/android/location/places/Subscription;)V

    .line 180
    :cond_2
    :goto_1
    return-void

    .line 178
    :cond_3
    const-string v2, "extra_subscription_code"

    invoke-virtual {p2, v2, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v3

    if-eq v3, v4, :cond_0

    iget-object v0, v1, Lcom/google/android/location/places/d/b;->h:Ljava/util/Map;

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/places/Subscription;

    goto :goto_0

    :cond_4
    const-string v0, "Places"

    const/4 v1, 0x6

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, "Places"

    const-string v1, "Unable to extract subscription from received intent"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method
