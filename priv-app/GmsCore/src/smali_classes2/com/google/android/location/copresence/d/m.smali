.class final Lcom/google/android/location/copresence/d/m;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/location/copresence/w;


# static fields
.field private static final b:Ljava/util/Set;


# instance fields
.field private final c:Lcom/google/android/location/copresence/d/j;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 25
    const/4 v0, 0x2

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Collections;->singleton(Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v0

    sput-object v0, Lcom/google/android/location/copresence/d/m;->b:Ljava/util/Set;

    return-void
.end method

.method constructor <init>(Lcom/google/android/location/copresence/d/j;)V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    iput-object p1, p0, Lcom/google/android/location/copresence/d/m;->c:Lcom/google/android/location/copresence/d/j;

    .line 32
    return-void
.end method

.method public static a(Ljava/lang/String;Lcom/google/ac/b/c/o;)Z
    .locals 1

    .prologue
    .line 71
    if-eqz p0, :cond_0

    iget-object v0, p1, Lcom/google/ac/b/c/o;->b:Ljava/lang/String;

    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b(Ljava/lang/String;Lcom/google/ac/b/c/o;)Lcom/google/android/gms/location/copresence/x;
    .locals 2

    .prologue
    .line 75
    invoke-static {p0, p1}, Lcom/google/android/location/copresence/d/m;->a(Ljava/lang/String;Lcom/google/ac/b/c/o;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 76
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Not a valid token in bluetooth name"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 78
    :cond_0
    iget-object v0, p1, Lcom/google/ac/b/c/o;->b:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    invoke-virtual {p0, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/location/copresence/x;->a(Ljava/lang/String;)Lcom/google/android/gms/location/copresence/x;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(Lcom/google/android/location/copresence/b;)V
    .locals 2

    .prologue
    .line 47
    invoke-virtual {p0}, Lcom/google/android/location/copresence/d/m;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 48
    new-instance v0, Lcom/google/android/location/copresence/y;

    invoke-direct {v0}, Lcom/google/android/location/copresence/y;-><init>()V

    throw v0

    .line 50
    :cond_0
    iget-object v0, p0, Lcom/google/android/location/copresence/d/m;->c:Lcom/google/android/location/copresence/d/j;

    invoke-virtual {p1}, Lcom/google/android/location/copresence/b;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/location/copresence/d/j;->a(Ljava/lang/String;)V

    .line 51
    const/4 v0, 0x3

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 52
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "E2E Advertise: step 4b) BluetoothNameBeacon: broadcasted token "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p1, Lcom/google/android/location/copresence/b;->a:Lcom/google/android/gms/location/copresence/x;

    invoke-virtual {v1}, Lcom/google/android/gms/location/copresence/x;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->b(Ljava/lang/String;)I

    .line 55
    :cond_1
    sget-object v0, Lcom/google/android/location/copresence/d/m;->b:Ljava/util/Set;

    .line 56
    return-void
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 41
    invoke-static {}, Lcom/google/android/location/copresence/f/b;->b()Lcom/google/ac/b/c/g;

    move-result-object v0

    iget-object v0, v0, Lcom/google/ac/b/c/g;->e:Lcom/google/ac/b/c/m;

    iget-object v0, v0, Lcom/google/ac/b/c/m;->e:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lcom/google/android/location/copresence/d/m;->c:Lcom/google/android/location/copresence/d/j;

    invoke-virtual {v0}, Lcom/google/android/location/copresence/d/j;->g()V

    .line 61
    sget-object v0, Lcom/google/android/location/copresence/d/m;->b:Ljava/util/Set;

    .line 62
    return-void
.end method
