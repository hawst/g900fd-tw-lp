.class public abstract Lcom/google/android/location/activity/o;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field protected final a:Lcom/google/android/location/activity/k;

.field protected final b:Lcom/google/android/location/j/b;


# direct methods
.method public constructor <init>(Lcom/google/android/location/activity/k;Lcom/google/android/location/j/b;)V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    iput-object p1, p0, Lcom/google/android/location/activity/o;->a:Lcom/google/android/location/activity/k;

    .line 26
    iput-object p2, p0, Lcom/google/android/location/activity/o;->b:Lcom/google/android/location/j/b;

    .line 27
    return-void
.end method

.method private static a(Lcom/google/android/location/activity/a/o;)J
    .locals 4

    .prologue
    .line 91
    invoke-interface {p0}, Lcom/google/android/location/activity/a/o;->a()J

    move-result-wide v0

    const-wide/32 v2, 0xf4240

    div-long/2addr v0, v2

    const-wide/16 v2, 0x5dc

    add-long/2addr v0, v2

    return-wide v0
.end method

.method protected static a(JJJJ)Z
    .locals 6

    .prologue
    .line 112
    sget-boolean v0, Lcom/google/android/location/i/a;->b:Z

    if-eqz v0, :cond_0

    const-string v0, "ActivityScheduler"

    const-string v1, "inRange(time1=%d, minPeriod1=%d, time2=%d, minPeriod2=%d)"

    const/4 v2, 0x4

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p0, p1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    invoke-static {p4, p5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x2

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x3

    invoke-static {p6, p7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/location/o/a/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 114
    :cond_0
    const-wide v0, 0x7fffffffffffffffL

    cmp-long v0, p0, v0

    if-eqz v0, :cond_1

    const-wide v0, 0x7fffffffffffffffL

    cmp-long v0, p2, v0

    if-nez v0, :cond_2

    .line 115
    :cond_1
    const/4 v0, 0x0

    .line 121
    :goto_0
    return v0

    .line 117
    :cond_2
    const-wide/16 v0, 0x2

    div-long v0, p4, v0

    .line 118
    sub-long v2, p0, p2

    invoke-static {v2, v3}, Ljava/lang/Math;->abs(J)J

    move-result-wide v2

    .line 119
    const-wide/16 v4, 0x2

    div-long v4, p6, v4

    .line 121
    cmp-long v0, v2, v0

    if-gtz v0, :cond_3

    cmp-long v0, v2, v4

    if-gtz v0, :cond_3

    const/4 v0, 0x1

    goto :goto_0

    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static b(JJ)J
    .locals 4

    .prologue
    const-wide v0, 0x7fffffffffffffffL

    .line 96
    cmp-long v2, p2, v0

    if-nez v2, :cond_0

    .line 99
    :goto_0
    return-wide v0

    :cond_0
    add-long v0, p0, p2

    goto :goto_0
.end method

.method private e()J
    .locals 6

    .prologue
    const-wide v0, 0x7fffffffffffffffL

    .line 58
    iget-object v2, p0, Lcom/google/android/location/activity/o;->a:Lcom/google/android/location/activity/k;

    invoke-virtual {v2}, Lcom/google/android/location/activity/k;->q()J

    move-result-wide v2

    cmp-long v2, v2, v0

    if-nez v2, :cond_0

    .line 72
    :goto_0
    return-wide v0

    .line 61
    :cond_0
    iget-object v0, p0, Lcom/google/android/location/activity/o;->a:Lcom/google/android/location/activity/k;

    invoke-virtual {v0}, Lcom/google/android/location/activity/k;->h()Lcom/google/android/location/activity/a;

    move-result-object v0

    iget-boolean v0, v0, Lcom/google/android/location/activity/a;->a:Z

    if-eqz v0, :cond_2

    .line 62
    sget-boolean v0, Lcom/google/android/location/i/a;->b:Z

    if-eqz v0, :cond_1

    const-string v0, "ActivityScheduler"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "getMinPeriod: accelBatchingMode is on. Return minPeriodMillis="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/location/activity/o;->a:Lcom/google/android/location/activity/k;

    invoke-virtual {v2}, Lcom/google/android/location/activity/k;->q()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/location/o/a/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 65
    :cond_1
    iget-object v0, p0, Lcom/google/android/location/activity/o;->a:Lcom/google/android/location/activity/k;

    invoke-virtual {v0}, Lcom/google/android/location/activity/k;->q()J

    move-result-wide v0

    goto :goto_0

    .line 67
    :cond_2
    invoke-static {}, Lcom/google/android/location/activity/k;->b()Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/google/android/location/activity/o;->a:Lcom/google/android/location/activity/k;

    invoke-virtual {v0}, Lcom/google/android/location/activity/k;->o()Lcom/google/android/location/activity/a/o;

    move-result-object v0

    :goto_1
    invoke-static {v0}, Lcom/google/android/location/activity/o;->a(Lcom/google/android/location/activity/a/o;)J

    move-result-wide v0

    .line 70
    sget-boolean v2, Lcom/google/android/location/i/a;->b:Z

    if-eqz v2, :cond_3

    const-string v2, "ActivityScheduler"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "getMinPeriod: accelBatchingMode is off. minPeriodMillis="

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/google/android/location/activity/o;->a:Lcom/google/android/location/activity/k;

    invoke-virtual {v4}, Lcom/google/android/location/activity/k;->q()J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", detectionTimeMillis="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/location/o/a/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 72
    :cond_3
    const-wide/16 v2, 0x0

    iget-object v4, p0, Lcom/google/android/location/activity/o;->a:Lcom/google/android/location/activity/k;

    invoke-virtual {v4}, Lcom/google/android/location/activity/k;->q()J

    move-result-wide v4

    sub-long v0, v4, v0

    invoke-static {v2, v3, v0, v1}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v0

    goto :goto_0

    .line 67
    :cond_4
    iget-object v0, p0, Lcom/google/android/location/activity/o;->a:Lcom/google/android/location/activity/k;

    invoke-virtual {v0}, Lcom/google/android/location/activity/k;->m()Lcom/google/android/location/activity/a/o;

    move-result-object v0

    goto :goto_1
.end method


# virtual methods
.method protected final a()J
    .locals 4

    .prologue
    .line 34
    iget-object v0, p0, Lcom/google/android/location/activity/o;->a:Lcom/google/android/location/activity/k;

    invoke-virtual {v0}, Lcom/google/android/location/activity/k;->x()J

    move-result-wide v0

    invoke-direct {p0}, Lcom/google/android/location/activity/o;->e()J

    move-result-wide v2

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/location/activity/o;->b(JJ)J

    move-result-wide v0

    return-wide v0
.end method

.method protected final a(JJ)Z
    .locals 9

    .prologue
    .line 103
    cmp-long v0, p1, p3

    if-gez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/location/activity/o;->a:Lcom/google/android/location/activity/k;

    invoke-virtual {v0}, Lcom/google/android/location/activity/k;->r()J

    move-result-wide v4

    const-wide v6, 0x7fffffffffffffffL

    move-wide v0, p1

    move-wide v2, p3

    invoke-static/range {v0 .. v7}, Lcom/google/android/location/activity/o;->a(JJJJ)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/location/activity/o;->a:Lcom/google/android/location/activity/k;

    invoke-virtual {v0}, Lcom/google/android/location/activity/k;->l()Lcom/google/android/location/activity/bn;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/location/activity/bn;->b()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected final b()J
    .locals 4

    .prologue
    .line 43
    invoke-static {}, Lcom/google/android/location/activity/k;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/location/activity/o;->e()J

    move-result-wide v0

    const-wide/32 v2, 0x2bf20

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v0

    .line 46
    :goto_0
    iget-object v2, p0, Lcom/google/android/location/activity/o;->a:Lcom/google/android/location/activity/k;

    invoke-virtual {v2}, Lcom/google/android/location/activity/k;->x()J

    move-result-wide v2

    invoke-static {v2, v3, v0, v1}, Lcom/google/android/location/activity/o;->b(JJ)J

    move-result-wide v0

    return-wide v0

    .line 43
    :cond_0
    invoke-direct {p0}, Lcom/google/android/location/activity/o;->e()J

    move-result-wide v0

    goto :goto_0
.end method

.method protected final c()J
    .locals 8

    .prologue
    const-wide v0, 0x7fffffffffffffffL

    .line 54
    iget-object v2, p0, Lcom/google/android/location/activity/o;->a:Lcom/google/android/location/activity/k;

    invoke-virtual {v2}, Lcom/google/android/location/activity/k;->y()J

    move-result-wide v2

    iget-object v4, p0, Lcom/google/android/location/activity/o;->a:Lcom/google/android/location/activity/k;

    invoke-virtual {v4}, Lcom/google/android/location/activity/k;->r()J

    move-result-wide v4

    cmp-long v4, v4, v0

    if-nez v4, :cond_0

    :goto_0
    invoke-static {v2, v3, v0, v1}, Lcom/google/android/location/activity/o;->b(JJ)J

    move-result-wide v0

    return-wide v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/location/activity/o;->a:Lcom/google/android/location/activity/k;

    invoke-virtual {v0}, Lcom/google/android/location/activity/k;->p()Lcom/google/android/location/activity/a/o;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/location/activity/o;->a(Lcom/google/android/location/activity/a/o;)J

    move-result-wide v0

    const-wide/16 v4, 0x0

    iget-object v6, p0, Lcom/google/android/location/activity/o;->a:Lcom/google/android/location/activity/k;

    invoke-virtual {v6}, Lcom/google/android/location/activity/k;->r()J

    move-result-wide v6

    sub-long v0, v6, v0

    invoke-static {v4, v5, v0, v1}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v0

    goto :goto_0
.end method

.method public abstract d()Lcom/google/android/location/activity/p;
.end method
