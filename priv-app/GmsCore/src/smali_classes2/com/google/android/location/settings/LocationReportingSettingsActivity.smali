.class public Lcom/google/android/location/settings/LocationReportingSettingsActivity;
.super Lcom/google/android/location/settings/b;
.source "SourceFile"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0}, Lcom/google/android/location/settings/b;-><init>()V

    return-void
.end method


# virtual methods
.method protected final a(Lcom/google/android/location/reporting/config/AccountConfig;)I
    .locals 1

    .prologue
    .line 28
    invoke-virtual {p1}, Lcom/google/android/location/reporting/config/AccountConfig;->r()I

    move-result v0

    return v0
.end method

.method protected final b(Z)V
    .locals 2

    .prologue
    .line 33
    iget-object v0, p0, Lcom/google/android/location/settings/LocationReportingSettingsActivity;->e:Lcom/google/android/location/reporting/service/l;

    iget-object v1, p0, Lcom/google/android/location/settings/LocationReportingSettingsActivity;->g:Landroid/accounts/Account;

    invoke-interface {v0, v1, p1}, Lcom/google/android/location/reporting/service/l;->a(Landroid/accounts/Account;Z)V

    .line 34
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 20
    invoke-super {p0, p1}, Lcom/google/android/location/settings/b;->onCreate(Landroid/os/Bundle;)V

    .line 21
    invoke-virtual {p0}, Lcom/google/android/location/settings/LocationReportingSettingsActivity;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_0

    .line 22
    sget v0, Lcom/google/android/gms/l;->cz:I

    invoke-virtual {p0, v0}, Lcom/google/android/location/settings/LocationReportingSettingsActivity;->setContentView(I)V

    .line 24
    :cond_0
    return-void
.end method
