.class final Lcom/google/android/location/collectionlib/bh;
.super Ljava/util/concurrent/ThreadPoolExecutor;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/location/collectionlib/bg;


# direct methods
.method constructor <init>(Lcom/google/android/location/collectionlib/bg;Ljava/util/concurrent/TimeUnit;Ljava/util/concurrent/BlockingQueue;Ljava/util/concurrent/RejectedExecutionHandler;)V
    .locals 9

    .prologue
    const/4 v2, 0x1

    .line 56
    iput-object p1, p0, Lcom/google/android/location/collectionlib/bh;->a:Lcom/google/android/location/collectionlib/bg;

    const-wide/16 v4, 0x3c

    move-object v1, p0

    move v3, v2

    move-object v6, p2

    move-object v7, p3

    move-object v8, p4

    invoke-direct/range {v1 .. v8}, Ljava/util/concurrent/ThreadPoolExecutor;-><init>(IIJLjava/util/concurrent/TimeUnit;Ljava/util/concurrent/BlockingQueue;Ljava/util/concurrent/RejectedExecutionHandler;)V

    return-void
.end method


# virtual methods
.method protected final terminated()V
    .locals 3

    .prologue
    .line 59
    sget-boolean v0, Lcom/google/android/location/i/a;->b:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/location/collectionlib/bh;->a:Lcom/google/android/location/collectionlib/bg;

    iget-object v0, v0, Lcom/google/android/location/collectionlib/bg;->b:Lcom/google/android/location/o/a/c;

    const-string v1, "LocalScanResultWriter terminated."

    invoke-virtual {v0, v1}, Lcom/google/android/location/o/a/c;->a(Ljava/lang/String;)V

    .line 61
    :cond_0
    :try_start_0
    invoke-static {}, Lcom/google/android/location/collectionlib/cb;->a()Lcom/google/android/location/collectionlib/cb;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/location/collectionlib/bh;->a:Lcom/google/android/location/collectionlib/bg;

    invoke-static {v1}, Lcom/google/android/location/collectionlib/bg;->a(Lcom/google/android/location/collectionlib/bg;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/location/collectionlib/cb;->b(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 63
    iget-object v0, p0, Lcom/google/android/location/collectionlib/bh;->a:Lcom/google/android/location/collectionlib/bg;

    iget-object v0, v0, Lcom/google/android/location/collectionlib/bg;->a:Lcom/google/android/location/collectionlib/ar;

    if-eqz v0, :cond_1

    .line 64
    iget-object v0, p0, Lcom/google/android/location/collectionlib/bh;->a:Lcom/google/android/location/collectionlib/bg;

    iget-object v0, v0, Lcom/google/android/location/collectionlib/bg;->a:Lcom/google/android/location/collectionlib/ar;

    iget-object v1, p0, Lcom/google/android/location/collectionlib/bh;->a:Lcom/google/android/location/collectionlib/bg;

    iget-object v1, v1, Lcom/google/android/location/collectionlib/bg;->d:Lcom/google/android/location/collectionlib/cp;

    invoke-interface {v0, v1}, Lcom/google/android/location/collectionlib/ar;->a(Lcom/google/android/location/collectionlib/cp;)V

    .line 69
    :cond_1
    iget-object v0, p0, Lcom/google/android/location/collectionlib/bh;->a:Lcom/google/android/location/collectionlib/bg;

    invoke-static {v0}, Lcom/google/android/location/collectionlib/bg;->b(Lcom/google/android/location/collectionlib/bg;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/location/collectionlib/bh;->a:Lcom/google/android/location/collectionlib/bg;

    iget-object v0, v0, Lcom/google/android/location/collectionlib/bg;->d:Lcom/google/android/location/collectionlib/cp;

    if-eqz v0, :cond_2

    .line 70
    iget-object v0, p0, Lcom/google/android/location/collectionlib/bh;->a:Lcom/google/android/location/collectionlib/bg;

    iget-object v1, p0, Lcom/google/android/location/collectionlib/bh;->a:Lcom/google/android/location/collectionlib/bg;

    iget-object v1, v1, Lcom/google/android/location/collectionlib/bg;->d:Lcom/google/android/location/collectionlib/cp;

    invoke-virtual {v0, v1}, Lcom/google/android/location/collectionlib/bg;->a(Lcom/google/android/location/collectionlib/cp;)V

    .line 72
    :cond_2
    return-void

    .line 63
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/google/android/location/collectionlib/bh;->a:Lcom/google/android/location/collectionlib/bg;

    iget-object v1, v1, Lcom/google/android/location/collectionlib/bg;->a:Lcom/google/android/location/collectionlib/ar;

    if-eqz v1, :cond_3

    .line 64
    iget-object v1, p0, Lcom/google/android/location/collectionlib/bh;->a:Lcom/google/android/location/collectionlib/bg;

    iget-object v1, v1, Lcom/google/android/location/collectionlib/bg;->a:Lcom/google/android/location/collectionlib/ar;

    iget-object v2, p0, Lcom/google/android/location/collectionlib/bh;->a:Lcom/google/android/location/collectionlib/bg;

    iget-object v2, v2, Lcom/google/android/location/collectionlib/bg;->d:Lcom/google/android/location/collectionlib/cp;

    invoke-interface {v1, v2}, Lcom/google/android/location/collectionlib/ar;->a(Lcom/google/android/location/collectionlib/cp;)V

    :cond_3
    throw v0
.end method
