.class final Lcom/google/android/location/copresence/d/x;
.super Lcom/google/android/location/copresence/k/f;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/location/copresence/d/n;


# direct methods
.method varargs constructor <init>(Lcom/google/android/location/copresence/d/n;Ljava/lang/String;[Lcom/google/android/location/copresence/k/f;)V
    .locals 0

    .prologue
    .line 211
    iput-object p1, p0, Lcom/google/android/location/copresence/d/x;->a:Lcom/google/android/location/copresence/d/n;

    invoke-direct {p0, p2, p3}, Lcom/google/android/location/copresence/k/f;-><init>(Ljava/lang/String;[Lcom/google/android/location/copresence/k/f;)V

    return-void
.end method


# virtual methods
.method public final a()Z
    .locals 4

    .prologue
    .line 220
    iget-object v0, p0, Lcom/google/android/location/copresence/d/x;->a:Lcom/google/android/location/copresence/d/n;

    iget-object v0, v0, Lcom/google/android/location/copresence/d/n;->b:Lcom/google/android/location/copresence/d/e;

    const/4 v1, 0x2

    invoke-static {v1}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "BluetoothAdapterWrapper: cancelDiscovery"

    invoke-static {v1}, Lcom/google/android/location/copresence/ag;->a(Ljava/lang/String;)I

    :cond_0
    const-string v1, "cancelDiscovery"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/google/android/location/copresence/d/e;->a(Ljava/lang/String;Ljava/lang/Boolean;)V

    iget-object v1, v0, Lcom/google/android/location/copresence/d/e;->a:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v1}, Landroid/bluetooth/BluetoothAdapter;->cancelDiscovery()Z

    move-result v1

    const-string v2, "cancelDiscovery"

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lcom/google/android/location/copresence/d/e;->a(Ljava/lang/String;Ljava/lang/Boolean;)V

    return v1
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 215
    iget-object v0, p0, Lcom/google/android/location/copresence/d/x;->a:Lcom/google/android/location/copresence/d/n;

    iget-object v0, v0, Lcom/google/android/location/copresence/d/n;->b:Lcom/google/android/location/copresence/d/e;

    iget-object v0, v0, Lcom/google/android/location/copresence/d/e;->a:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothAdapter;->isDiscovering()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
