.class public final Lcom/google/android/location/copresence/j;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/location/copresence/a/g;
.implements Lcom/google/android/location/copresence/n/i;


# static fields
.field private static r:Lcom/google/android/location/copresence/j;


# instance fields
.field final a:Landroid/content/Context;

.field final b:Lcom/google/android/location/copresence/ap;

.field c:Lcom/google/android/location/copresence/a;

.field d:Lcom/google/android/location/copresence/f;

.field e:Lcom/google/android/location/copresence/a/b;

.field public f:Lcom/google/android/location/copresence/n/f;

.field g:Lcom/google/android/location/copresence/ah;

.field h:Lcom/google/android/location/copresence/an;

.field i:Lcom/google/android/location/copresence/l/j;

.field j:Lcom/google/android/location/copresence/l/h;

.field k:Lcom/google/android/location/copresence/at;

.field l:Lcom/google/android/location/copresence/ay;

.field m:Lcom/google/android/location/copresence/bg;

.field n:Z

.field o:Lcom/google/android/location/e/a;

.field p:Lcom/google/android/location/copresence/h;

.field q:Lcom/google/android/location/copresence/debug/c;


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 78
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 79
    iput-object p1, p0, Lcom/google/android/location/copresence/j;->a:Landroid/content/Context;

    .line 80
    invoke-static {p1}, Lcom/google/android/location/copresence/ap;->a(Landroid/content/Context;)Lcom/google/android/location/copresence/ap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/copresence/j;->b:Lcom/google/android/location/copresence/ap;

    .line 81
    return-void
.end method

.method public static declared-synchronized a(Landroid/content/Context;)Lcom/google/android/location/copresence/j;
    .locals 2

    .prologue
    .line 65
    const-class v1, Lcom/google/android/location/copresence/j;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/google/android/location/copresence/j;->r:Lcom/google/android/location/copresence/j;

    if-nez v0, :cond_0

    .line 66
    new-instance v0, Lcom/google/android/location/copresence/j;

    invoke-direct {v0, p0}, Lcom/google/android/location/copresence/j;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/google/android/location/copresence/j;->r:Lcom/google/android/location/copresence/j;

    .line 68
    :cond_0
    sget-object v0, Lcom/google/android/location/copresence/j;->r:Lcom/google/android/location/copresence/j;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 65
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private b()V
    .locals 1

    .prologue
    .line 313
    invoke-virtual {p0}, Lcom/google/android/location/copresence/j;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 321
    :goto_0
    return-void

    .line 317
    :cond_0
    const/4 v0, 0x2

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 318
    const-string v0, "updateStateIfCopresenceEnabled"

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->a(Ljava/lang/String;)I

    .line 320
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/location/copresence/j;->a()Z

    goto :goto_0
.end method

.method private c()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    .line 324
    invoke-virtual {p0}, Lcom/google/android/location/copresence/j;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 341
    :goto_0
    return-void

    .line 328
    :cond_0
    invoke-static {v4}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 329
    const-string v0, "updateStateIfCopresenceDisabled"

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->a(Ljava/lang/String;)I

    .line 331
    :cond_1
    iget-object v0, p0, Lcom/google/android/location/copresence/j;->c:Lcom/google/android/location/copresence/a;

    if-eqz v0, :cond_5

    .line 332
    invoke-static {v4}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 333
    const-string v0, "updateStateIfCopresenceDisabled turning off activeActiveController"

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->a(Ljava/lang/String;)I

    .line 335
    :cond_2
    iget-object v1, p0, Lcom/google/android/location/copresence/j;->m:Lcom/google/android/location/copresence/bg;

    sget-object v0, Lcom/google/android/location/copresence/ax;->a:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iget-object v3, v1, Lcom/google/android/location/copresence/bg;->a:Landroid/content/Context;

    invoke-static {v3, v0}, Lcom/google/android/location/copresence/ax;->a(Landroid/content/Context;I)Lcom/google/android/location/copresence/w;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/location/copresence/w;->b()V

    goto :goto_1

    :cond_3
    iget-object v0, v1, Lcom/google/android/location/copresence/bg;->b:Lcom/google/android/location/copresence/bf;

    invoke-virtual {v0}, Lcom/google/android/location/copresence/bf;->a()V

    .line 336
    iget-object v0, p0, Lcom/google/android/location/copresence/j;->c:Lcom/google/android/location/copresence/a;

    invoke-static {v4}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v1

    if-eqz v1, :cond_4

    const-string v1, "ActiveActiveController: stopListening"

    invoke-static {v1}, Lcom/google/android/location/copresence/ag;->a(Ljava/lang/String;)I

    :cond_4
    iget-object v1, v0, Lcom/google/android/location/copresence/a;->b:Lcom/google/android/location/copresence/bi;

    if-eqz v1, :cond_5

    iget-object v0, v0, Lcom/google/android/location/copresence/a;->b:Lcom/google/android/location/copresence/bi;

    invoke-virtual {v0}, Lcom/google/android/location/copresence/bi;->b()V

    .line 338
    :cond_5
    iget-object v0, p0, Lcom/google/android/location/copresence/j;->h:Lcom/google/android/location/copresence/an;

    invoke-virtual {v0}, Lcom/google/android/location/copresence/an;->b()V

    .line 340
    invoke-virtual {p0}, Lcom/google/android/location/copresence/j;->a()Z

    goto :goto_0
.end method

.method private c(Lcom/google/android/location/copresence/a/a;)V
    .locals 2

    .prologue
    .line 306
    iget-object v0, p0, Lcom/google/android/location/copresence/j;->f:Lcom/google/android/location/copresence/n/f;

    invoke-virtual {v0, p1}, Lcom/google/android/location/copresence/n/f;->a(Lcom/google/android/location/copresence/a/a;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 310
    :goto_0
    return-void

    .line 309
    :cond_0
    iget-object v0, p0, Lcom/google/android/location/copresence/j;->g:Lcom/google/android/location/copresence/ah;

    const/4 v1, 0x1

    invoke-virtual {v0, p1, v1}, Lcom/google/android/location/copresence/ah;->a(Lcom/google/android/location/copresence/a/a;Z)V

    goto :goto_0
.end method


# virtual methods
.method public final a(Lcom/google/android/location/copresence/a/a;)V
    .locals 2

    .prologue
    .line 259
    iget-object v0, p0, Lcom/google/android/location/copresence/j;->f:Lcom/google/android/location/copresence/n/f;

    invoke-virtual {v0, p1}, Lcom/google/android/location/copresence/n/f;->c(Lcom/google/android/location/copresence/a/a;)V

    .line 260
    iget-object v0, p0, Lcom/google/android/location/copresence/j;->g:Lcom/google/android/location/copresence/ah;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Lcom/google/android/location/copresence/ah;->a(Lcom/google/android/location/copresence/a/a;Z)V

    .line 264
    invoke-direct {p0}, Lcom/google/android/location/copresence/j;->c()V

    .line 265
    return-void
.end method

.method public final a(Lcom/google/android/location/copresence/a/a;Z)V
    .locals 2

    .prologue
    .line 241
    if-eqz p2, :cond_1

    .line 243
    invoke-direct {p0}, Lcom/google/android/location/copresence/j;->b()V

    .line 244
    invoke-direct {p0, p1}, Lcom/google/android/location/copresence/j;->c(Lcom/google/android/location/copresence/a/a;)V

    .line 245
    iget-object v0, p1, Lcom/google/android/location/copresence/a/a;->a:Landroid/accounts/Account;

    iget-object v0, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/location/copresence/j;->e:Lcom/google/android/location/copresence/a/b;

    invoke-virtual {v1, v0}, Lcom/google/android/location/copresence/a/b;->a(Ljava/lang/String;)Lcom/google/android/location/copresence/a/a;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/google/android/location/copresence/j;->f:Lcom/google/android/location/copresence/n/f;

    invoke-virtual {v1, v0}, Lcom/google/android/location/copresence/n/f;->a(Lcom/google/android/location/copresence/a/a;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/location/copresence/j;->k:Lcom/google/android/location/copresence/at;

    iget-object v0, v0, Lcom/google/android/location/copresence/a/a;->a:Landroid/accounts/Account;

    invoke-virtual {v1, v0}, Lcom/google/android/location/copresence/at;->a(Landroid/accounts/Account;)V

    .line 251
    :cond_0
    :goto_0
    return-void

    .line 248
    :cond_1
    invoke-direct {p0}, Lcom/google/android/location/copresence/j;->c()V

    goto :goto_0
.end method

.method public final a(Ljava/util/List;)V
    .locals 7

    .prologue
    const/4 v6, 0x2

    .line 175
    invoke-static {v6}, Lcom/google/android/location/copresence/b/a;->c(I)V

    .line 177
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/copresence/ao;

    .line 178
    iget-object v2, p0, Lcom/google/android/location/copresence/j;->c:Lcom/google/android/location/copresence/a;

    iget-boolean v3, v0, Lcom/google/android/location/copresence/ao;->d:Z

    iget-object v4, v0, Lcom/google/android/location/copresence/ao;->a:Lcom/google/ac/b/c/ah;

    iget-object v0, v4, Lcom/google/ac/b/c/ah;->b:Lcom/google/ac/b/c/bt;

    iget-object v0, v0, Lcom/google/ac/b/c/bt;->b:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->c(I)Ljava/lang/String;

    iget v0, v2, Lcom/google/android/location/copresence/a;->f:I

    if-ne v0, v6, :cond_1

    const/4 v0, 0x3

    invoke-static {v0}, Lcom/google/android/location/copresence/b/a;->c(I)V

    :cond_1
    iget-object v0, v2, Lcom/google/android/location/copresence/a;->b:Lcom/google/android/location/copresence/bi;

    if-eqz v0, :cond_0

    iget-object v2, v2, Lcom/google/android/location/copresence/a;->b:Lcom/google/android/location/copresence/bi;

    iget-object v0, v4, Lcom/google/ac/b/c/ah;->b:Lcom/google/ac/b/c/bt;

    iget-object v0, v0, Lcom/google/ac/b/c/bt;->b:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v5

    if-eqz v3, :cond_2

    const/4 v0, 0x1

    :goto_1
    iget-object v3, v4, Lcom/google/ac/b/c/ah;->g:Lcom/google/ac/b/c/i;

    invoke-static {v3}, Lcom/google/android/location/copresence/f/b;->a(Lcom/google/ac/b/c/i;)Lcom/google/ac/b/c/o;

    move-result-object v3

    invoke-virtual {v2, v5, v0, v3}, Lcom/google/android/location/copresence/bi;->a(IILcom/google/ac/b/c/o;)V

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_1

    .line 181
    :cond_3
    iget-object v0, p0, Lcom/google/android/location/copresence/j;->c:Lcom/google/android/location/copresence/a;

    iget-object v1, v0, Lcom/google/android/location/copresence/a;->b:Lcom/google/android/location/copresence/bi;

    if-eqz v1, :cond_4

    iget-object v0, v0, Lcom/google/android/location/copresence/a;->b:Lcom/google/android/location/copresence/bi;

    invoke-virtual {v0}, Lcom/google/android/location/copresence/bi;->a()V

    .line 182
    :cond_4
    return-void
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 220
    iget-object v0, p0, Lcom/google/android/location/copresence/j;->f:Lcom/google/android/location/copresence/n/f;

    invoke-virtual {v0}, Lcom/google/android/location/copresence/n/f;->c()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Lcom/google/android/location/copresence/a/a;)V
    .locals 1

    .prologue
    .line 269
    iget-object v0, p0, Lcom/google/android/location/copresence/j;->f:Lcom/google/android/location/copresence/n/f;

    invoke-virtual {v0, p1}, Lcom/google/android/location/copresence/n/f;->b(Lcom/google/android/location/copresence/a/a;)V

    .line 270
    invoke-direct {p0, p1}, Lcom/google/android/location/copresence/j;->c(Lcom/google/android/location/copresence/a/a;)V

    .line 271
    invoke-direct {p0}, Lcom/google/android/location/copresence/j;->b()V

    .line 274
    iget-object v0, p0, Lcom/google/android/location/copresence/j;->g:Lcom/google/android/location/copresence/ah;

    invoke-virtual {v0}, Lcom/google/android/location/copresence/ah;->c()V

    .line 275
    return-void
.end method

.method public final b(Ljava/util/List;)V
    .locals 7

    .prologue
    const/4 v6, 0x1

    .line 188
    invoke-static {v6}, Lcom/google/android/location/copresence/b/a;->c(I)V

    .line 190
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/copresence/ao;

    .line 191
    iget-object v2, p0, Lcom/google/android/location/copresence/j;->m:Lcom/google/android/location/copresence/bg;

    iget-object v3, v0, Lcom/google/android/location/copresence/ao;->a:Lcom/google/ac/b/c/ah;

    iget-object v3, v3, Lcom/google/ac/b/c/ah;->b:Lcom/google/ac/b/c/bt;

    iget-object v3, v3, Lcom/google/ac/b/c/bt;->b:Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    new-instance v4, Lcom/google/android/location/copresence/b;

    iget-object v5, v0, Lcom/google/android/location/copresence/ao;->a:Lcom/google/ac/b/c/ah;

    iget-object v5, v5, Lcom/google/ac/b/c/ah;->b:Lcom/google/ac/b/c/bt;

    iget-object v5, v5, Lcom/google/ac/b/c/bt;->c:Ljava/lang/String;

    invoke-static {v5}, Lcom/google/android/gms/location/copresence/x;->a(Ljava/lang/String;)Lcom/google/android/gms/location/copresence/x;

    move-result-object v5

    iget-object v0, v0, Lcom/google/android/location/copresence/ao;->a:Lcom/google/ac/b/c/ah;

    iget-object v0, v0, Lcom/google/ac/b/c/ah;->g:Lcom/google/ac/b/c/i;

    invoke-static {v0}, Lcom/google/android/location/copresence/f/b;->a(Lcom/google/ac/b/c/i;)Lcom/google/ac/b/c/o;

    move-result-object v0

    invoke-direct {v4, v5, v0}, Lcom/google/android/location/copresence/b;-><init>(Lcom/google/android/gms/location/copresence/x;Lcom/google/ac/b/c/o;)V

    iget-object v0, v2, Lcom/google/android/location/copresence/bg;->a:Landroid/content/Context;

    invoke-static {v0, v3}, Lcom/google/android/location/copresence/ax;->a(Landroid/content/Context;I)Lcom/google/android/location/copresence/w;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {v0}, Lcom/google/android/location/copresence/w;->a()Z

    move-result v5

    if-eqz v5, :cond_0

    :try_start_0
    invoke-interface {v0, v4}, Lcom/google/android/location/copresence/w;->a(Lcom/google/android/location/copresence/b;)V

    iget-object v0, v2, Lcom/google/android/location/copresence/bg;->b:Lcom/google/android/location/copresence/bf;

    invoke-virtual {v0, v3}, Lcom/google/android/location/copresence/bf;->b(I)V

    iget-object v0, v2, Lcom/google/android/location/copresence/bg;->d:Lcom/google/android/location/copresence/bh;

    if-eqz v0, :cond_0

    iget-object v0, v2, Lcom/google/android/location/copresence/bg;->d:Lcom/google/android/location/copresence/bh;
    :try_end_0
    .catch Lcom/google/android/location/copresence/y; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    const/4 v2, 0x6

    invoke-static {v2}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v2

    if-eqz v2, :cond_0

    const-string v2, "TokenAdvertiser: StartAdvertising called even though not available."

    invoke-static {v2, v0}, Lcom/google/android/location/copresence/ag;->a(Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 197
    :cond_1
    iget-object v1, p0, Lcom/google/android/location/copresence/j;->m:Lcom/google/android/location/copresence/bg;

    sget-object v0, Lcom/google/android/location/copresence/ax;->a:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_2
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iget-object v3, v1, Lcom/google/android/location/copresence/bg;->c:Lcom/google/android/location/copresence/an;

    invoke-virtual {v3, v6, v0}, Lcom/google/android/location/copresence/an;->a(II)Lcom/google/android/location/copresence/ao;

    move-result-object v3

    if-nez v3, :cond_2

    iget-object v3, v1, Lcom/google/android/location/copresence/bg;->b:Lcom/google/android/location/copresence/bf;

    invoke-virtual {v3, v0}, Lcom/google/android/location/copresence/bf;->a(I)V

    iget-object v3, v1, Lcom/google/android/location/copresence/bg;->a:Landroid/content/Context;

    invoke-static {v3, v0}, Lcom/google/android/location/copresence/ax;->a(Landroid/content/Context;I)Lcom/google/android/location/copresence/w;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-interface {v0}, Lcom/google/android/location/copresence/w;->b()V

    goto :goto_1

    .line 199
    :cond_3
    iget-object v0, p0, Lcom/google/android/location/copresence/j;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/location/copresence/f;->b(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 200
    iget-object v0, p0, Lcom/google/android/location/copresence/j;->d:Lcom/google/android/location/copresence/f;

    invoke-virtual {v0}, Lcom/google/android/location/copresence/f;->a()V

    .line 202
    :cond_4
    return-void
.end method
