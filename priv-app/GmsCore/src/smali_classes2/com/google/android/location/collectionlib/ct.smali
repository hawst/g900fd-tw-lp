.class final Lcom/google/android/location/collectionlib/ct;
.super Landroid/os/Handler;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/location/collectionlib/cr;


# direct methods
.method constructor <init>(Lcom/google/android/location/collectionlib/cr;)V
    .locals 0

    .prologue
    .line 257
    iput-object p1, p0, Lcom/google/android/location/collectionlib/ct;->a:Lcom/google/android/location/collectionlib/cr;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public final handleMessage(Landroid/os/Message;)V
    .locals 9

    .prologue
    .line 261
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 296
    :goto_0
    invoke-super {p0, p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    .line 297
    return-void

    .line 263
    :pswitch_0
    iget-object v0, p0, Lcom/google/android/location/collectionlib/ct;->a:Lcom/google/android/location/collectionlib/cr;

    invoke-static {v0}, Lcom/google/android/location/collectionlib/cr;->b(Lcom/google/android/location/collectionlib/cr;)Ljava/lang/Object;

    move-result-object v8

    monitor-enter v8

    .line 264
    :try_start_0
    sget-boolean v0, Lcom/google/android/location/i/a;->b:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/location/collectionlib/ct;->a:Lcom/google/android/location/collectionlib/cr;

    invoke-static {v0}, Lcom/google/android/location/collectionlib/cr;->c(Lcom/google/android/location/collectionlib/cr;)Lcom/google/android/location/o/a/c;

    move-result-object v0

    const-string v1, "PreScanning finished."

    invoke-virtual {v0, v1}, Lcom/google/android/location/o/a/c;->a(Ljava/lang/String;)V

    .line 265
    :cond_0
    iget-object v0, p0, Lcom/google/android/location/collectionlib/ct;->a:Lcom/google/android/location/collectionlib/cr;

    invoke-static {v0}, Lcom/google/android/location/collectionlib/cr;->e(Lcom/google/android/location/collectionlib/cr;)Lcom/google/android/location/collectionlib/bf;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/location/collectionlib/ct;->a:Lcom/google/android/location/collectionlib/cr;

    invoke-static {v1}, Lcom/google/android/location/collectionlib/cr;->d(Lcom/google/android/location/collectionlib/cr;)Lcom/google/p/a/b/b/a;

    invoke-interface {v0}, Lcom/google/android/location/collectionlib/bf;->a()Z

    move-result v1

    .line 266
    iget-object v0, p0, Lcom/google/android/location/collectionlib/ct;->a:Lcom/google/android/location/collectionlib/cr;

    invoke-static {v0}, Lcom/google/android/location/collectionlib/cr;->g(Lcom/google/android/location/collectionlib/cr;)Lcom/google/android/location/collectionlib/ar;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/location/collectionlib/ct;->a:Lcom/google/android/location/collectionlib/cr;

    invoke-static {v2}, Lcom/google/android/location/collectionlib/cr;->f(Lcom/google/android/location/collectionlib/cr;)Z

    move-result v2

    invoke-interface {v0, v1, v2}, Lcom/google/android/location/collectionlib/ar;->a(ZZ)V

    .line 267
    const/4 v0, 0x0

    .line 268
    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/google/android/location/collectionlib/ct;->a:Lcom/google/android/location/collectionlib/cr;

    invoke-static {v1}, Lcom/google/android/location/collectionlib/cr;->f(Lcom/google/android/location/collectionlib/cr;)Z

    move-result v1

    if-nez v1, :cond_4

    .line 269
    iget-object v0, p0, Lcom/google/android/location/collectionlib/ct;->a:Lcom/google/android/location/collectionlib/cr;

    new-instance v1, Lcom/google/android/location/collectionlib/bq;

    iget-object v2, p0, Lcom/google/android/location/collectionlib/ct;->a:Lcom/google/android/location/collectionlib/cr;

    invoke-static {v2}, Lcom/google/android/location/collectionlib/cr;->c(Lcom/google/android/location/collectionlib/cr;)Lcom/google/android/location/o/a/c;

    move-result-object v2

    invoke-direct {v1, p0, v2}, Lcom/google/android/location/collectionlib/bq;-><init>(Landroid/os/Handler;Lcom/google/android/location/o/a/c;)V

    invoke-static {v0, v1}, Lcom/google/android/location/collectionlib/cr;->a(Lcom/google/android/location/collectionlib/cr;Lcom/google/android/location/collectionlib/bq;)Lcom/google/android/location/collectionlib/bq;

    .line 270
    iget-object v0, p0, Lcom/google/android/location/collectionlib/ct;->a:Lcom/google/android/location/collectionlib/cr;

    invoke-static {v0}, Lcom/google/android/location/collectionlib/cr;->n(Lcom/google/android/location/collectionlib/cr;)Lcom/google/android/location/collectionlib/bq;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/location/collectionlib/ct;->a:Lcom/google/android/location/collectionlib/cr;

    invoke-static {v1}, Lcom/google/android/location/collectionlib/cr;->h(Lcom/google/android/location/collectionlib/cr;)Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/location/collectionlib/ct;->a:Lcom/google/android/location/collectionlib/cr;

    invoke-static {v2}, Lcom/google/android/location/collectionlib/cr;->i(Lcom/google/android/location/collectionlib/cr;)Lcom/google/android/location/collectionlib/ai;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/location/collectionlib/ct;->a:Lcom/google/android/location/collectionlib/cr;

    invoke-static {v3}, Lcom/google/android/location/collectionlib/cr;->j(Lcom/google/android/location/collectionlib/cr;)Lcom/google/android/location/d/b;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/location/collectionlib/ct;->a:Lcom/google/android/location/collectionlib/cr;

    invoke-static {v4}, Lcom/google/android/location/collectionlib/cr;->k(Lcom/google/android/location/collectionlib/cr;)Lcom/google/android/location/os/bn;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/location/collectionlib/ct;->a:Lcom/google/android/location/collectionlib/cr;

    invoke-static {v5}, Lcom/google/android/location/collectionlib/cr;->l(Lcom/google/android/location/collectionlib/cr;)Ljava/lang/Integer;

    move-result-object v5

    iget-object v6, p0, Lcom/google/android/location/collectionlib/ct;->a:Lcom/google/android/location/collectionlib/cr;

    invoke-static {v6}, Lcom/google/android/location/collectionlib/cr;->m(Lcom/google/android/location/collectionlib/cr;)Lcom/google/p/a/b/b/a;

    move-result-object v6

    iget-object v7, p0, Lcom/google/android/location/collectionlib/ct;->a:Lcom/google/android/location/collectionlib/cr;

    invoke-static {v7}, Lcom/google/android/location/collectionlib/cr;->g(Lcom/google/android/location/collectionlib/cr;)Lcom/google/android/location/collectionlib/ar;

    move-result-object v7

    invoke-virtual/range {v0 .. v7}, Lcom/google/android/location/collectionlib/bq;->a(Landroid/content/Context;Lcom/google/android/location/collectionlib/ai;Lcom/google/android/location/d/b;Lcom/google/android/location/os/bn;Ljava/lang/Integer;Lcom/google/p/a/b/b/a;Lcom/google/android/location/collectionlib/ar;)Z

    move-result v0

    .line 276
    if-nez v0, :cond_2

    .line 277
    const-string v1, "RealScanner: Nothing to scan."

    .line 278
    sget-boolean v2, Lcom/google/android/location/i/a;->b:Z

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/google/android/location/collectionlib/ct;->a:Lcom/google/android/location/collectionlib/cr;

    invoke-static {v2}, Lcom/google/android/location/collectionlib/cr;->c(Lcom/google/android/location/collectionlib/cr;)Lcom/google/android/location/o/a/c;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/google/android/location/o/a/c;->a(Ljava/lang/String;)V

    .line 279
    :cond_1
    iget-object v2, p0, Lcom/google/android/location/collectionlib/ct;->a:Lcom/google/android/location/collectionlib/cr;

    invoke-static {v2}, Lcom/google/android/location/collectionlib/cr;->g(Lcom/google/android/location/collectionlib/cr;)Lcom/google/android/location/collectionlib/ar;

    move-result-object v2

    if-eqz v2, :cond_2

    .line 280
    iget-object v2, p0, Lcom/google/android/location/collectionlib/ct;->a:Lcom/google/android/location/collectionlib/cr;

    invoke-static {v2}, Lcom/google/android/location/collectionlib/cr;->g(Lcom/google/android/location/collectionlib/cr;)Lcom/google/android/location/collectionlib/ar;

    move-result-object v2

    invoke-interface {v2, v1}, Lcom/google/android/location/collectionlib/ar;->a(Ljava/lang/String;)V

    .line 287
    :cond_2
    :goto_1
    if-nez v0, :cond_3

    .line 288
    invoke-virtual {p0}, Lcom/google/android/location/collectionlib/ct;->getLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Looper;->quit()V

    .line 290
    :cond_3
    monitor-exit v8
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto/16 :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v8

    throw v0

    .line 284
    :cond_4
    :try_start_1
    sget-boolean v1, Lcom/google/android/location/i/a;->b:Z

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/google/android/location/collectionlib/ct;->a:Lcom/google/android/location/collectionlib/cr;

    invoke-static {v1}, Lcom/google/android/location/collectionlib/cr;->c(Lcom/google/android/location/collectionlib/cr;)Lcom/google/android/location/o/a/c;

    move-result-object v2

    iget-object v1, p0, Lcom/google/android/location/collectionlib/ct;->a:Lcom/google/android/location/collectionlib/cr;

    invoke-static {v1}, Lcom/google/android/location/collectionlib/cr;->f(Lcom/google/android/location/collectionlib/cr;)Z

    move-result v1

    if-eqz v1, :cond_5

    const-string v1, "Stopped by client during prescan."

    :goto_2
    invoke-virtual {v2, v1}, Lcom/google/android/location/o/a/c;->a(Ljava/lang/String;)V

    goto :goto_1

    :cond_5
    const-string v1, "PreScanDecision returns false. Will not do real collection."
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_2

    .line 291
    :pswitch_1
    invoke-virtual {p0}, Lcom/google/android/location/collectionlib/ct;->getLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Looper;->quit()V

    goto/16 :goto_0

    .line 261
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
