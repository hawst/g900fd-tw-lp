.class public final Lcom/google/android/location/copresence/a/b;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final a:Lcom/google/android/location/copresence/a/d;

.field private static final b:Lcom/google/android/location/copresence/a/e;

.field private static j:Lcom/google/android/location/copresence/a/b;


# instance fields
.field private final c:Ljava/util/Set;

.field private final d:Landroid/content/BroadcastReceiver;

.field private final e:Landroid/content/Context;

.field private final f:Lcom/google/android/location/copresence/ap;

.field private final g:Lcom/google/android/location/copresence/a/i;

.field private final h:Lcom/google/android/location/copresence/q/w;

.field private i:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 33
    new-instance v0, Lcom/google/android/location/copresence/a/d;

    invoke-direct {v0, v1}, Lcom/google/android/location/copresence/a/d;-><init>(B)V

    sput-object v0, Lcom/google/android/location/copresence/a/b;->a:Lcom/google/android/location/copresence/a/d;

    .line 35
    new-instance v0, Lcom/google/android/location/copresence/a/e;

    invoke-direct {v0, v1}, Lcom/google/android/location/copresence/a/e;-><init>(B)V

    sput-object v0, Lcom/google/android/location/copresence/a/b;->b:Lcom/google/android/location/copresence/a/e;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 71
    invoke-static {p1}, Lcom/google/android/location/copresence/ap;->a(Landroid/content/Context;)Lcom/google/android/location/copresence/ap;

    move-result-object v0

    invoke-static {p1}, Lcom/google/android/location/copresence/a/i;->a(Landroid/content/Context;)Lcom/google/android/location/copresence/a/i;

    move-result-object v1

    invoke-direct {p0, p1, v0, v1}, Lcom/google/android/location/copresence/a/b;-><init>(Landroid/content/Context;Lcom/google/android/location/copresence/ap;Lcom/google/android/location/copresence/a/i;)V

    .line 72
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Lcom/google/android/location/copresence/ap;Lcom/google/android/location/copresence/a/i;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 75
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/location/copresence/a/b;->c:Ljava/util/Set;

    .line 39
    new-instance v0, Lcom/google/android/location/copresence/a/f;

    invoke-direct {v0, p0, v1}, Lcom/google/android/location/copresence/a/f;-><init>(Lcom/google/android/location/copresence/a/b;B)V

    iput-object v0, p0, Lcom/google/android/location/copresence/a/b;->d:Landroid/content/BroadcastReceiver;

    .line 46
    iput-boolean v1, p0, Lcom/google/android/location/copresence/a/b;->i:Z

    .line 76
    iput-object p1, p0, Lcom/google/android/location/copresence/a/b;->e:Landroid/content/Context;

    .line 77
    iput-object p2, p0, Lcom/google/android/location/copresence/a/b;->f:Lcom/google/android/location/copresence/ap;

    .line 78
    iput-object p3, p0, Lcom/google/android/location/copresence/a/b;->g:Lcom/google/android/location/copresence/a/i;

    .line 79
    new-instance v0, Lcom/google/android/location/copresence/q/w;

    invoke-direct {v0, p2}, Lcom/google/android/location/copresence/q/w;-><init>(Lcom/google/android/location/copresence/ap;)V

    iput-object v0, p0, Lcom/google/android/location/copresence/a/b;->h:Lcom/google/android/location/copresence/q/w;

    .line 80
    return-void
.end method

.method private a(Landroid/accounts/Account;)Lcom/google/android/location/copresence/a/a;
    .locals 7

    .prologue
    const/4 v0, 0x0

    const/4 v6, 0x0

    .line 273
    iget-object v1, p0, Lcom/google/android/location/copresence/a/b;->f:Lcom/google/android/location/copresence/ap;

    invoke-virtual {v1}, Lcom/google/android/location/copresence/ap;->c()V

    .line 275
    iget-object v2, p0, Lcom/google/android/location/copresence/a/b;->g:Lcom/google/android/location/copresence/a/i;

    const-string v1, "This operation can be IO intensive."

    invoke-static {v1}, Lcom/google/android/gms/common/internal/bx;->c(Ljava/lang/String;)V

    iget-object v3, p1, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lcom/google/android/location/copresence/a/i;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_1

    const/4 v1, 0x6

    invoke-static {v1}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "GaiaId for account \'%s\', not found in cache."

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v3}, Lcom/google/android/location/copresence/ag;->f(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-static {v1, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "ObfuscatedGaiaIdLookup: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/location/copresence/ag;->e(Ljava/lang/String;)I

    :cond_0
    new-instance v1, Lcom/google/android/location/copresence/a/k;

    invoke-direct {v1, v2, p1, v6}, Lcom/google/android/location/copresence/a/k;-><init>(Lcom/google/android/location/copresence/a/i;Landroid/accounts/Account;B)V

    invoke-virtual {v1}, Lcom/google/android/location/copresence/a/k;->a()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_1

    iget-object v4, v2, Lcom/google/android/location/copresence/a/i;->a:Ljava/util/HashMap;

    monitor-enter v4

    :try_start_0
    iget-object v5, v2, Lcom/google/android/location/copresence/a/i;->a:Ljava/util/HashMap;

    invoke-virtual {v5, v3, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-virtual {v2}, Lcom/google/android/location/copresence/a/i;->a()V

    :cond_1
    move-object v2, v1

    if-nez v2, :cond_2

    move-object v1, v0

    .line 276
    :goto_0
    if-nez v1, :cond_3

    .line 281
    :goto_1
    return-object v0

    .line 275
    :catchall_0
    move-exception v0

    monitor-exit v4

    throw v0

    :cond_2
    new-instance v1, Lcom/google/android/location/copresence/a/a;

    invoke-direct {v1, p1, v2}, Lcom/google/android/location/copresence/a/a;-><init>(Landroid/accounts/Account;Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/location/copresence/a/b;->c:Ljava/util/Set;

    invoke-interface {v2, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 280
    :cond_3
    iget-object v0, v1, Lcom/google/android/location/copresence/a/a;->a:Landroid/accounts/Account;

    iget-object v0, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->f(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const/4 v2, 0x2

    invoke-static {v2}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v2

    if-eqz v2, :cond_4

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "CopresenceAccountManager: Account has been added: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->a(Ljava/lang/String;)I

    :cond_4
    iget-object v0, p0, Lcom/google/android/location/copresence/a/b;->h:Lcom/google/android/location/copresence/q/w;

    sget-object v2, Lcom/google/android/location/copresence/a/b;->a:Lcom/google/android/location/copresence/a/d;

    invoke-virtual {v0, v2, v1}, Lcom/google/android/location/copresence/q/w;->a(Lcom/google/android/location/copresence/q/ac;Ljava/lang/Object;)V

    move-object v0, v1

    .line 281
    goto :goto_1
.end method

.method static synthetic a(Lcom/google/android/location/copresence/a/b;Ljava/lang/String;)Lcom/google/android/location/copresence/a/a;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 31
    invoke-direct {p0}, Lcom/google/android/location/copresence/a/b;->b()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/accounts/Account;

    iget-object v3, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v3, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    :goto_0
    if-nez v0, :cond_3

    const/4 v0, 0x3

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "%s is an unsupported account."

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p1}, Lcom/google/android/location/copresence/ag;->f(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v2}, Lcom/google/android/location/copresence/ag;->a(Ljava/lang/String;[Ljava/lang/Object;)I

    :cond_1
    move-object v0, v1

    :goto_1
    return-object v0

    :cond_2
    move-object v0, v1

    goto :goto_0

    :cond_3
    invoke-direct {p0, v0}, Lcom/google/android/location/copresence/a/b;->a(Landroid/accounts/Account;)Lcom/google/android/location/copresence/a/a;

    move-result-object v0

    goto :goto_1
.end method

.method public static declared-synchronized a(Landroid/content/Context;)Lcom/google/android/location/copresence/a/b;
    .locals 2

    .prologue
    .line 64
    const-class v1, Lcom/google/android/location/copresence/a/b;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/google/android/location/copresence/a/b;->j:Lcom/google/android/location/copresence/a/b;

    if-nez v0, :cond_0

    .line 65
    new-instance v0, Lcom/google/android/location/copresence/a/b;

    invoke-direct {v0, p0}, Lcom/google/android/location/copresence/a/b;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/google/android/location/copresence/a/b;->j:Lcom/google/android/location/copresence/a/b;

    .line 67
    :cond_0
    sget-object v0, Lcom/google/android/location/copresence/a/b;->j:Lcom/google/android/location/copresence/a/b;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 64
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static a(Landroid/accounts/AccountManager;)Ljava/util/List;
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 179
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 180
    invoke-virtual {p0}, Landroid/accounts/AccountManager;->getAccounts()[Landroid/accounts/Account;

    move-result-object v4

    .line 181
    array-length v5, v4

    move v2, v1

    :goto_0
    if-ge v2, v5, :cond_3

    aget-object v6, v4, v2

    .line 182
    if-eqz v6, :cond_0

    iget-object v0, v6, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "com.google"

    iget-object v7, v6, Landroid/accounts/Account;->type:Ljava/lang/String;

    invoke-virtual {v0, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    :cond_0
    move v0, v1

    :goto_1
    if-eqz v0, :cond_1

    .line 183
    invoke-interface {v3, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 181
    :cond_1
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 182
    :cond_2
    const/4 v0, 0x1

    goto :goto_1

    .line 186
    :cond_3
    return-object v3
.end method

.method static synthetic a(Lcom/google/android/location/copresence/a/b;)V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0}, Lcom/google/android/location/copresence/a/b;->c()V

    return-void
.end method

.method private b()Ljava/util/List;
    .locals 1

    .prologue
    .line 190
    iget-object v0, p0, Lcom/google/android/location/copresence/a/b;->e:Landroid/content/Context;

    invoke-static {v0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/location/copresence/a/b;->a(Landroid/accounts/AccountManager;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method private c()V
    .locals 7

    .prologue
    .line 208
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 211
    invoke-direct {p0}, Lcom/google/android/location/copresence/a/b;->b()Ljava/util/List;

    move-result-object v2

    .line 212
    iget-object v0, p0, Lcom/google/android/location/copresence/a/b;->c:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/copresence/a/a;

    .line 213
    iget-object v4, v0, Lcom/google/android/location/copresence/a/a;->a:Landroid/accounts/Account;

    .line 214
    invoke-interface {v2, v4}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 215
    invoke-interface {v2, v4}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    goto :goto_0

    .line 217
    :cond_0
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 222
    :cond_1
    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/copresence/a/a;

    .line 223
    iget-object v3, p0, Lcom/google/android/location/copresence/a/b;->c:Ljava/util/Set;

    invoke-interface {v3, v0}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    iget-object v3, p0, Lcom/google/android/location/copresence/a/b;->g:Lcom/google/android/location/copresence/a/i;

    iget-object v4, v0, Lcom/google/android/location/copresence/a/a;->a:Landroid/accounts/Account;

    iget-object v4, v4, Landroid/accounts/Account;->name:Ljava/lang/String;

    iget-object v5, v3, Lcom/google/android/location/copresence/a/i;->a:Ljava/util/HashMap;

    monitor-enter v5

    :try_start_0
    iget-object v6, v3, Lcom/google/android/location/copresence/a/i;->a:Ljava/util/HashMap;

    invoke-virtual {v6, v4}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    monitor-exit v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-virtual {v3}, Lcom/google/android/location/copresence/a/i;->a()V

    iget-object v3, v0, Lcom/google/android/location/copresence/a/a;->a:Landroid/accounts/Account;

    iget-object v3, v3, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-static {v3}, Lcom/google/android/location/copresence/ag;->f(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x2

    invoke-static {v4}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v4

    if-eqz v4, :cond_2

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "CopresenceAccountManager: Account has been removed: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/location/copresence/ag;->a(Ljava/lang/String;)I

    :cond_2
    iget-object v3, p0, Lcom/google/android/location/copresence/a/b;->h:Lcom/google/android/location/copresence/q/w;

    sget-object v4, Lcom/google/android/location/copresence/a/b;->b:Lcom/google/android/location/copresence/a/e;

    invoke-virtual {v3, v4, v0}, Lcom/google/android/location/copresence/q/w;->a(Lcom/google/android/location/copresence/q/ac;Ljava/lang/Object;)V

    goto :goto_1

    :catchall_0
    move-exception v0

    monitor-exit v5

    throw v0

    .line 227
    :cond_3
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/accounts/Account;

    .line 229
    invoke-direct {p0, v0}, Lcom/google/android/location/copresence/a/b;->a(Landroid/accounts/Account;)Lcom/google/android/location/copresence/a/a;

    goto :goto_2

    .line 231
    :cond_4
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Lcom/google/android/location/copresence/a/a;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 132
    iget-object v0, p0, Lcom/google/android/location/copresence/a/b;->c:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/copresence/a/a;

    .line 133
    iget-object v3, v0, Lcom/google/android/location/copresence/a/a;->a:Landroid/accounts/Account;

    iget-object v3, v3, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v3, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 147
    :goto_0
    return-object v0

    .line 140
    :cond_1
    iget-object v0, p0, Lcom/google/android/location/copresence/a/b;->f:Lcom/google/android/location/copresence/ap;

    new-instance v2, Lcom/google/android/location/copresence/a/c;

    invoke-direct {v2, p0, p1}, Lcom/google/android/location/copresence/a/c;-><init>(Lcom/google/android/location/copresence/a/b;Ljava/lang/String;)V

    invoke-virtual {v0, v2, v1}, Lcom/google/android/location/copresence/ap;->a(Ljava/lang/Runnable;Landroid/os/WorkSource;)V

    move-object v0, v1

    .line 147
    goto :goto_0
.end method

.method public final a()Ljava/util/Set;
    .locals 1

    .prologue
    .line 166
    iget-object v0, p0, Lcom/google/android/location/copresence/a/b;->c:Ljava/util/Set;

    return-object v0
.end method

.method public final a(Lcom/google/android/location/copresence/a/g;)V
    .locals 1

    .prologue
    .line 118
    iget-object v0, p0, Lcom/google/android/location/copresence/a/b;->h:Lcom/google/android/location/copresence/q/w;

    invoke-virtual {v0, p1}, Lcom/google/android/location/copresence/q/w;->a(Ljava/lang/Object;)V

    .line 119
    return-void
.end method

.method public final a(Lcom/google/android/location/e/a;)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 83
    iget-boolean v0, p0, Lcom/google/android/location/copresence/a/b;->i:Z

    if-eqz v0, :cond_0

    .line 107
    :goto_0
    return-void

    .line 87
    :cond_0
    const/4 v0, 0x2

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 88
    const-string v0, "CopresenceAccountManager: init started"

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->a(Ljava/lang/String;)I

    .line 90
    :cond_1
    iget-object v0, p0, Lcom/google/android/location/copresence/a/b;->g:Lcom/google/android/location/copresence/a/i;

    new-instance v1, Ljava/io/File;

    iget-object v2, v0, Lcom/google/android/location/copresence/a/i;->b:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v2

    const-string v3, "copresence_gaia_id"

    invoke-direct {v1, v2, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    new-instance v2, Lcom/google/android/location/os/bl;

    invoke-direct {v2, v5, v1, p1}, Lcom/google/android/location/os/bl;-><init>(ILjava/io/File;Lcom/google/android/location/e/a;)V

    iput-object v2, v0, Lcom/google/android/location/copresence/a/i;->c:Lcom/google/android/location/os/bl;

    iget-object v1, v0, Lcom/google/android/location/copresence/a/i;->a:Ljava/util/HashMap;

    monitor-enter v1

    :try_start_0
    iget-object v2, v0, Lcom/google/android/location/copresence/a/i;->a:Ljava/util/HashMap;

    invoke-virtual {v0}, Lcom/google/android/location/copresence/a/i;->b()Ljava/util/HashMap;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/util/HashMap;->putAll(Ljava/util/Map;)V

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 91
    iget-object v0, p0, Lcom/google/android/location/copresence/a/b;->e:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/location/copresence/a/b;->d:Landroid/content/BroadcastReceiver;

    new-instance v2, Landroid/content/IntentFilter;

    const-string v3, "android.accounts.LOGIN_ACCOUNTS_CHANGED"

    invoke-direct {v2, v3}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/android/location/copresence/a/b;->f:Lcom/google/android/location/copresence/ap;

    invoke-virtual {v4}, Lcom/google/android/location/copresence/ap;->d()Landroid/os/Handler;

    move-result-object v4

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;Ljava/lang/String;Landroid/os/Handler;)Landroid/content/Intent;

    .line 99
    invoke-direct {p0}, Lcom/google/android/location/copresence/a/b;->c()V

    .line 101
    const/4 v0, 0x5

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 102
    iget-object v0, p0, Lcom/google/android/location/copresence/a/b;->c:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 103
    const-string v0, "CopresenceAccountManager: No copresence accounts found after init."

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->d(Ljava/lang/String;)I

    .line 106
    :cond_2
    iput-boolean v5, p0, Lcom/google/android/location/copresence/a/b;->i:Z

    goto :goto_0

    .line 90
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final b(Ljava/lang/String;)Lcom/google/android/location/copresence/a/a;
    .locals 3

    .prologue
    .line 157
    iget-object v0, p0, Lcom/google/android/location/copresence/a/b;->c:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/copresence/a/a;

    .line 158
    iget-object v2, v0, Lcom/google/android/location/copresence/a/a;->b:Ljava/lang/String;

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 162
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
