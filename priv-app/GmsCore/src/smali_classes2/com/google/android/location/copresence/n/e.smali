.class final Lcom/google/android/location/copresence/n/e;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final a:Ljava/util/Map;

.field final b:Lcom/google/android/location/copresence/n/c;

.field private final c:Landroid/content/Context;

.field private final d:Lcom/google/android/location/copresence/a/b;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/location/copresence/a/b;)V
    .locals 2

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/location/copresence/n/e;->a:Ljava/util/Map;

    .line 28
    iput-object p1, p0, Lcom/google/android/location/copresence/n/e;->c:Landroid/content/Context;

    .line 29
    iput-object p2, p0, Lcom/google/android/location/copresence/n/e;->d:Lcom/google/android/location/copresence/a/b;

    .line 30
    new-instance v0, Lcom/google/android/location/copresence/n/c;

    iget-object v1, p0, Lcom/google/android/location/copresence/n/e;->c:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/google/android/location/copresence/n/c;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/location/copresence/n/e;->b:Lcom/google/android/location/copresence/n/c;

    .line 31
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/location/copresence/a/a;)Lcom/google/android/location/copresence/n/b;
    .locals 2

    .prologue
    .line 43
    iget-object v0, p0, Lcom/google/android/location/copresence/n/e;->a:Ljava/util/Map;

    iget-object v1, p1, Lcom/google/android/location/copresence/a/a;->a:Landroid/accounts/Account;

    iget-object v1, v1, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/copresence/n/b;

    return-object v0
.end method

.method public final a()Ljava/util/Map;
    .locals 6

    .prologue
    .line 86
    new-instance v0, Ljava/util/HashMap;

    iget-object v1, p0, Lcom/google/android/location/copresence/n/e;->a:Ljava/util/Map;

    invoke-direct {v0, v1}, Ljava/util/HashMap;-><init>(Ljava/util/Map;)V

    .line 88
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    .line 91
    iget-object v1, p0, Lcom/google/android/location/copresence/n/e;->a:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->clear()V

    .line 92
    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 93
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 94
    iget-object v4, p0, Lcom/google/android/location/copresence/n/e;->d:Lcom/google/android/location/copresence/a/b;

    invoke-virtual {v4, v1}, Lcom/google/android/location/copresence/a/b;->a(Ljava/lang/String;)Lcom/google/android/location/copresence/a/a;

    move-result-object v4

    .line 96
    if-eqz v4, :cond_0

    .line 97
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/copresence/n/b;

    .line 98
    invoke-virtual {p0, v4}, Lcom/google/android/location/copresence/n/e;->b(Lcom/google/android/location/copresence/a/a;)Lcom/google/android/location/copresence/n/b;

    move-result-object v4

    .line 100
    if-eqz v4, :cond_0

    iget v0, v0, Lcom/google/android/location/copresence/n/b;->a:I

    iget v5, v4, Lcom/google/android/location/copresence/n/b;->a:I

    if-eq v0, v5, :cond_0

    .line 101
    invoke-interface {v2, v1, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 106
    :cond_1
    return-object v2
.end method

.method public final b(Lcom/google/android/location/copresence/a/a;)Lcom/google/android/location/copresence/n/b;
    .locals 10

    .prologue
    const/4 v9, 0x6

    const/16 v8, 0x9c9

    const/4 v2, 0x1

    const/4 v3, 0x0

    const/4 v1, 0x0

    .line 54
    :try_start_0
    iget-object v0, p0, Lcom/google/android/location/copresence/n/e;->b:Lcom/google/android/location/copresence/n/c;

    invoke-virtual {v0}, Lcom/google/android/location/copresence/n/c;->b()Lcom/google/android/location/copresence/n/a;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v4

    .line 62
    iget-object v0, p1, Lcom/google/android/location/copresence/a/a;->a:Landroid/accounts/Account;

    iget-object v5, v4, Lcom/google/android/location/copresence/n/a;->b:Lcom/google/android/location/reporting/config/ReportingConfig;

    invoke-virtual {v5, v0}, Lcom/google/android/location/reporting/config/ReportingConfig;->a(Landroid/accounts/Account;)Lcom/google/android/location/reporting/config/AccountConfig;

    move-result-object v5

    if-nez v5, :cond_2

    move-object v0, v1

    .line 64
    :goto_0
    if-nez v0, :cond_11

    .line 65
    invoke-static {v9}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 66
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "CopresencePreference.Status not found for account: "

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p1, Lcom/google/android/location/copresence/a/a;->a:Landroid/accounts/Account;

    iget-object v2, v2, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-static {v2}, Lcom/google/android/location/copresence/ag;->f(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->e(Ljava/lang/String;)I

    :cond_0
    move-object v0, v1

    .line 73
    :goto_1
    return-object v0

    .line 55
    :catch_0
    move-exception v0

    .line 56
    invoke-static {v9}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 57
    const-string v2, "Error adding account in PreferenceStatus Map"

    invoke-static {v2, v0}, Lcom/google/android/location/copresence/ag;->a(Ljava/lang/String;Ljava/lang/Throwable;)I

    :cond_1
    move-object v0, v1

    .line 59
    goto :goto_1

    .line 62
    :cond_2
    iget-object v0, v4, Lcom/google/android/location/copresence/n/a;->b:Lcom/google/android/location/reporting/config/ReportingConfig;

    invoke-virtual {v0}, Lcom/google/android/location/reporting/config/ReportingConfig;->d()Lcom/google/android/location/reporting/config/Conditions;

    move-result-object v6

    iget-object v0, v4, Lcom/google/android/location/copresence/n/a;->b:Lcom/google/android/location/reporting/config/ReportingConfig;

    invoke-virtual {v0}, Lcom/google/android/location/reporting/config/ReportingConfig;->d()Lcom/google/android/location/reporting/config/Conditions;

    move-result-object v7

    sget-object v0, Lcom/google/android/location/copresence/k;->j:Lcom/google/android/location/copresence/l;

    invoke-virtual {v0}, Lcom/google/android/location/copresence/l;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-virtual {v7}, Lcom/google/android/location/reporting/config/Conditions;->isIneligibleDueToGeoOnly()Z

    move-result v7

    if-nez v7, :cond_3

    if-nez v0, :cond_4

    :cond_3
    move v0, v2

    :goto_2
    if-eqz v0, :cond_6

    invoke-static {}, Lcom/google/android/location/reporting/b/n;->b()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_5

    iget-object v4, v4, Lcom/google/android/location/copresence/n/a;->a:Landroid/content/Context;

    sget v5, Lcom/google/android/gms/p;->qH:I

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    new-array v2, v2, [Ljava/lang/Object;

    aput-object v0, v2, v3

    invoke-static {v4, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    :goto_3
    new-instance v2, Lcom/google/android/location/copresence/n/b;

    const/16 v3, 0x9cc

    invoke-direct {v2, v3, v0}, Lcom/google/android/location/copresence/n/b;-><init>(ILjava/lang/String;)V

    move-object v0, v2

    goto :goto_0

    :cond_4
    move v0, v3

    goto :goto_2

    :cond_5
    iget-object v0, v4, Lcom/google/android/location/copresence/n/a;->a:Landroid/content/Context;

    sget v2, Lcom/google/android/gms/p;->qI:I

    invoke-virtual {v0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_3

    :cond_6
    invoke-virtual {v6}, Lcom/google/android/location/reporting/config/Conditions;->g()Z

    move-result v0

    if-nez v0, :cond_7

    new-instance v0, Lcom/google/android/location/copresence/n/b;

    const/16 v2, 0x9cd

    iget-object v3, v4, Lcom/google/android/location/copresence/n/a;->a:Landroid/content/Context;

    sget v4, Lcom/google/android/gms/p;->fp:I

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v2, v3}, Lcom/google/android/location/copresence/n/b;-><init>(ILjava/lang/String;)V

    goto/16 :goto_0

    :cond_7
    invoke-virtual {v6}, Lcom/google/android/location/reporting/config/Conditions;->f()Z

    move-result v0

    if-nez v0, :cond_8

    new-instance v0, Lcom/google/android/location/copresence/n/b;

    const/16 v2, 0x9ce

    iget-object v3, v4, Lcom/google/android/location/copresence/n/a;->a:Landroid/content/Context;

    sget v4, Lcom/google/android/gms/p;->qT:I

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v2, v3}, Lcom/google/android/location/copresence/n/b;-><init>(ILjava/lang/String;)V

    goto/16 :goto_0

    :cond_8
    invoke-virtual {v6}, Lcom/google/android/location/reporting/config/Conditions;->e()Z

    move-result v0

    if-eqz v0, :cond_9

    iget-object v0, v4, Lcom/google/android/location/copresence/n/a;->a:Landroid/content/Context;

    sget v2, Lcom/google/android/gms/p;->qA:I

    invoke-virtual {v0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-instance v0, Lcom/google/android/location/copresence/n/b;

    const/16 v3, 0x9d0

    invoke-direct {v0, v3, v2}, Lcom/google/android/location/copresence/n/b;-><init>(ILjava/lang/String;)V

    goto/16 :goto_0

    :cond_9
    iget-object v0, v4, Lcom/google/android/location/copresence/n/a;->b:Lcom/google/android/location/reporting/config/ReportingConfig;

    invoke-virtual {v0}, Lcom/google/android/location/reporting/config/ReportingConfig;->d()Lcom/google/android/location/reporting/config/Conditions;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/location/reporting/config/Conditions;->c()Z

    move-result v0

    if-eqz v0, :cond_a

    iget-object v0, v4, Lcom/google/android/location/copresence/n/a;->a:Landroid/content/Context;

    sget v2, Lcom/google/android/gms/p;->qA:I

    invoke-virtual {v0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-instance v0, Lcom/google/android/location/copresence/n/b;

    invoke-direct {v0, v8, v2}, Lcom/google/android/location/copresence/n/b;-><init>(ILjava/lang/String;)V

    goto/16 :goto_0

    :cond_a
    iget-object v0, v4, Lcom/google/android/location/copresence/n/a;->b:Lcom/google/android/location/reporting/config/ReportingConfig;

    invoke-virtual {v0}, Lcom/google/android/location/reporting/config/ReportingConfig;->g()Z

    move-result v0

    if-nez v0, :cond_b

    iget-object v0, v4, Lcom/google/android/location/copresence/n/a;->a:Landroid/content/Context;

    sget v2, Lcom/google/android/gms/p;->qA:I

    invoke-virtual {v0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-instance v0, Lcom/google/android/location/copresence/n/b;

    invoke-direct {v0, v8, v2}, Lcom/google/android/location/copresence/n/b;-><init>(ILjava/lang/String;)V

    goto/16 :goto_0

    :cond_b
    if-eqz v5, :cond_e

    invoke-virtual {v5}, Lcom/google/android/location/reporting/config/AccountConfig;->u()Z

    move-result v0

    if-eqz v0, :cond_c

    new-instance v0, Lcom/google/android/location/copresence/n/b;

    iget-object v2, v4, Lcom/google/android/location/copresence/n/a;->a:Landroid/content/Context;

    sget v3, Lcom/google/android/gms/p;->fo:I

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v8, v2}, Lcom/google/android/location/copresence/n/b;-><init>(ILjava/lang/String;)V

    goto/16 :goto_0

    :cond_c
    invoke-virtual {v5}, Lcom/google/android/location/reporting/config/AccountConfig;->h()Z

    move-result v0

    if-nez v0, :cond_d

    new-instance v0, Lcom/google/android/location/copresence/n/b;

    const/16 v2, 0x9cb

    invoke-direct {v0, v2, v1}, Lcom/google/android/location/copresence/n/b;-><init>(ILjava/lang/String;)V

    goto/16 :goto_0

    :cond_d
    invoke-virtual {v5}, Lcom/google/android/location/reporting/config/AccountConfig;->g()Z

    move-result v0

    if-nez v0, :cond_e

    new-instance v0, Lcom/google/android/location/copresence/n/b;

    const/16 v2, 0x9ca

    invoke-direct {v0, v2, v1}, Lcom/google/android/location/copresence/n/b;-><init>(ILjava/lang/String;)V

    goto/16 :goto_0

    :cond_e
    iget-object v0, v4, Lcom/google/android/location/copresence/n/a;->a:Landroid/content/Context;

    invoke-static {v0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/location/copresence/a/b;->a(Landroid/accounts/AccountManager;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_f

    move v0, v2

    :goto_4
    if-nez v0, :cond_10

    new-instance v0, Lcom/google/android/location/copresence/n/b;

    iget-object v2, v4, Lcom/google/android/location/copresence/n/a;->a:Landroid/content/Context;

    sget v4, Lcom/google/android/gms/p;->fq:I

    invoke-virtual {v2, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v3, v2}, Lcom/google/android/location/copresence/n/b;-><init>(ILjava/lang/String;)V

    goto/16 :goto_0

    :cond_f
    move v0, v3

    goto :goto_4

    :cond_10
    new-instance v0, Lcom/google/android/location/copresence/n/b;

    invoke-direct {v0, v3, v1}, Lcom/google/android/location/copresence/n/b;-><init>(ILjava/lang/String;)V

    goto/16 :goto_0

    .line 72
    :cond_11
    iget-object v1, p0, Lcom/google/android/location/copresence/n/e;->a:Ljava/util/Map;

    iget-object v2, p1, Lcom/google/android/location/copresence/a/a;->a:Landroid/accounts/Account;

    iget-object v2, v2, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_1
.end method
