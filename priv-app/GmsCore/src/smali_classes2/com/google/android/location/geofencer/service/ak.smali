.class final Lcom/google/android/location/geofencer/service/ak;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final a:J


# instance fields
.field private final b:Ljava/util/LinkedList;

.field private c:Z


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 597
    sget-object v0, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0xa

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    sput-wide v0, Lcom/google/android/location/geofencer/service/ak;->a:J

    return-void
.end method

.method constructor <init>()V
    .locals 1

    .prologue
    .line 596
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 599
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/google/android/location/geofencer/service/ak;->b:Ljava/util/LinkedList;

    .line 600
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/location/geofencer/service/ak;->c:Z

    return-void
.end method


# virtual methods
.method final a(J)V
    .locals 9

    .prologue
    const/4 v6, 0x4

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 603
    iget-object v0, p0, Lcom/google/android/location/geofencer/service/ak;->b:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->size()I

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/location/geofencer/service/ak;->b:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->getLast()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    cmp-long v0, p1, v4

    if-ltz v0, :cond_2

    :cond_0
    move v0, v2

    :goto_0
    const-string v3, "Unable to add events in the past."

    invoke-static {v0, v3}, Lcom/google/android/gms/common/internal/bx;->b(ZLjava/lang/Object;)V

    .line 606
    iget-object v0, p0, Lcom/google/android/location/geofencer/service/ak;->b:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->size()I

    move-result v0

    if-lt v0, v6, :cond_1

    .line 607
    iget-object v0, p0, Lcom/google/android/location/geofencer/service/ak;->b:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->removeFirst()Ljava/lang/Object;

    .line 609
    :cond_1
    iget-object v0, p0, Lcom/google/android/location/geofencer/service/ak;->b:Ljava/util/LinkedList;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 610
    iget-object v0, p0, Lcom/google/android/location/geofencer/service/ak;->b:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->size()I

    move-result v0

    if-lt v0, v6, :cond_3

    iget-object v0, p0, Lcom/google/android/location/geofencer/service/ak;->b:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->getFirst()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    sub-long v4, p1, v4

    sget-wide v6, Lcom/google/android/location/geofencer/service/ak;->a:J

    cmp-long v0, v4, v6

    if-gtz v0, :cond_3

    :goto_1
    iput-boolean v2, p0, Lcom/google/android/location/geofencer/service/ak;->c:Z

    .line 611
    return-void

    :cond_2
    move v0, v1

    .line 603
    goto :goto_0

    :cond_3
    move v2, v1

    .line 610
    goto :goto_1
.end method

.method final a()Z
    .locals 1

    .prologue
    .line 614
    iget-boolean v0, p0, Lcom/google/android/location/geofencer/service/ak;->c:Z

    return v0
.end method
