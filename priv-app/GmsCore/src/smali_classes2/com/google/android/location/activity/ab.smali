.class public final Lcom/google/android/location/activity/ab;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/location/activity/at;


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x12
.end annotation


# instance fields
.field final a:Ljava/lang/Object;

.field b:Lcom/google/android/location/activity/au;

.field private final c:Landroid/hardware/SensorManager;

.field private final d:Landroid/hardware/Sensor;

.field private final e:Z

.field private final f:Lcom/google/android/location/os/bi;

.field private g:Lcom/google/android/location/activity/ac;


# direct methods
.method public constructor <init>(Landroid/hardware/SensorManager;)V
    .locals 1

    .prologue
    .line 45
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/location/activity/ab;-><init>(Landroid/hardware/SensorManager;Lcom/google/android/location/os/bi;)V

    .line 46
    return-void
.end method

.method public constructor <init>(Landroid/hardware/SensorManager;Lcom/google/android/location/os/bi;)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 53
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/location/activity/ab;->a:Ljava/lang/Object;

    .line 54
    iput-object p1, p0, Lcom/google/android/location/activity/ab;->c:Landroid/hardware/SensorManager;

    .line 55
    const/16 v0, 0x11

    invoke-virtual {p1, v0}, Landroid/hardware/SensorManager;->getDefaultSensor(I)Landroid/hardware/Sensor;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/activity/ab;->d:Landroid/hardware/Sensor;

    .line 56
    iput-object p2, p0, Lcom/google/android/location/activity/ab;->f:Lcom/google/android/location/os/bi;

    .line 57
    iget-object v0, p0, Lcom/google/android/location/activity/ab;->d:Landroid/hardware/Sensor;

    if-eqz v0, :cond_6

    const-string v0, "Nexus 4"

    sget-object v3, Landroid/os/Build;->MODEL:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x12

    if-ne v0, v3, :cond_1

    sget-boolean v0, Lcom/google/android/location/i/a;->b:Z

    if-eqz v0, :cond_0

    const-string v0, "HardSigMotion"

    const-string v3, "Significant motion not properly supported on Nexus 4 JBMR2"

    invoke-static {v0, v3}, Lcom/google/android/location/o/a/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    move v0, v2

    :goto_0
    if-eqz v0, :cond_6

    move v0, v1

    :goto_1
    iput-boolean v0, p0, Lcom/google/android/location/activity/ab;->e:Z

    .line 58
    return-void

    .line 57
    :cond_1
    const-string v0, "Nexus 7"

    sget-object v3, Landroid/os/Build;->MODEL:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    sget-boolean v0, Lcom/google/android/location/i/a;->b:Z

    if-eqz v0, :cond_2

    const-string v0, "HardSigMotion"

    const-string v3, "Significant motion not properly supported on Nexus 7"

    invoke-static {v0, v3}, Lcom/google/android/location/o/a/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_2
    move v0, v2

    goto :goto_0

    :cond_3
    sget-object v0, Lcom/google/android/location/d/a;->a:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_5

    sget-boolean v0, Lcom/google/android/location/i/a;->b:Z

    if-eqz v0, :cond_4

    const-string v0, "HardSigMotion"

    const-string v3, "Significant motion disabled by Gservices on this device"

    invoke-static {v0, v3}, Lcom/google/android/location/o/a/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_4
    move v0, v2

    goto :goto_0

    :cond_5
    move v0, v1

    goto :goto_0

    :cond_6
    move v0, v2

    goto :goto_1
.end method


# virtual methods
.method public final a()Z
    .locals 4

    .prologue
    .line 97
    iget-object v1, p0, Lcom/google/android/location/activity/ab;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 98
    :try_start_0
    iget-object v0, p0, Lcom/google/android/location/activity/ab;->b:Lcom/google/android/location/activity/au;

    if-nez v0, :cond_0

    .line 99
    const/4 v0, 0x0

    monitor-exit v1

    .line 102
    :goto_0
    return v0

    .line 101
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/location/activity/ab;->b:Lcom/google/android/location/activity/au;

    .line 102
    iget-object v0, p0, Lcom/google/android/location/activity/ab;->c:Landroid/hardware/SensorManager;

    iget-object v2, p0, Lcom/google/android/location/activity/ab;->g:Lcom/google/android/location/activity/ac;

    iget-object v3, p0, Lcom/google/android/location/activity/ab;->d:Landroid/hardware/Sensor;

    invoke-virtual {v0, v2, v3}, Landroid/hardware/SensorManager;->cancelTriggerSensor(Landroid/hardware/TriggerEventListener;Landroid/hardware/Sensor;)Z

    move-result v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 103
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final a(Lcom/google/android/location/activity/au;)Z
    .locals 4

    .prologue
    .line 82
    iget-object v1, p0, Lcom/google/android/location/activity/ab;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 83
    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/location/activity/ab;->e:Z

    if-nez v0, :cond_0

    .line 84
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v2, "Significant motion is not supported on this device"

    invoke-direct {v0, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 92
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 86
    :cond_0
    :try_start_1
    invoke-virtual {p0}, Lcom/google/android/location/activity/ab;->c()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 87
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v2, "Significant motion already enabled"

    invoke-direct {v0, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 89
    :cond_1
    iput-object p1, p0, Lcom/google/android/location/activity/ab;->b:Lcom/google/android/location/activity/au;

    .line 90
    new-instance v0, Lcom/google/android/location/activity/ac;

    iget-object v2, p0, Lcom/google/android/location/activity/ab;->f:Lcom/google/android/location/os/bi;

    const/4 v3, 0x0

    invoke-direct {v0, v2, p0, v3}, Lcom/google/android/location/activity/ac;-><init>(Lcom/google/android/location/os/bi;Lcom/google/android/location/activity/ab;B)V

    iput-object v0, p0, Lcom/google/android/location/activity/ab;->g:Lcom/google/android/location/activity/ac;

    .line 91
    iget-object v0, p0, Lcom/google/android/location/activity/ab;->c:Landroid/hardware/SensorManager;

    iget-object v2, p0, Lcom/google/android/location/activity/ab;->g:Lcom/google/android/location/activity/ac;

    iget-object v3, p0, Lcom/google/android/location/activity/ab;->d:Landroid/hardware/Sensor;

    invoke-virtual {v0, v2, v3}, Landroid/hardware/SensorManager;->requestTriggerSensor(Landroid/hardware/TriggerEventListener;Landroid/hardware/Sensor;)Z

    move-result v0

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return v0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 110
    iget-boolean v0, p0, Lcom/google/android/location/activity/ab;->e:Z

    return v0
.end method

.method public final c()Z
    .locals 2

    .prologue
    .line 115
    iget-object v1, p0, Lcom/google/android/location/activity/ab;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 116
    :try_start_0
    iget-object v0, p0, Lcom/google/android/location/activity/ab;->b:Lcom/google/android/location/activity/au;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 117
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method
