.class public final Lcom/google/android/location/copresence/c/m;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/location/copresence/w;


# instance fields
.field private final b:Lcom/google/android/location/copresence/c/h;

.field private final c:I

.field private d:Lcom/google/android/gms/location/copresence/x;

.field private e:Z


# direct methods
.method public constructor <init>(Lcom/google/android/location/copresence/c/h;I)V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    iput-object p1, p0, Lcom/google/android/location/copresence/c/m;->b:Lcom/google/android/location/copresence/c/h;

    .line 30
    iput p2, p0, Lcom/google/android/location/copresence/c/m;->c:I

    .line 31
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/location/copresence/b;)V
    .locals 10

    .prologue
    const/4 v9, 0x2

    const/4 v8, 0x0

    const/4 v7, 0x3

    const/4 v6, 0x1

    .line 61
    iget-object v1, p1, Lcom/google/android/location/copresence/b;->a:Lcom/google/android/gms/location/copresence/x;

    .line 62
    if-eqz v1, :cond_0

    iget-object v0, v1, Lcom/google/android/gms/location/copresence/x;->a:[B

    if-nez v0, :cond_2

    .line 63
    :cond_0
    const/4 v0, 0x5

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 64
    const-string v0, "Audio Stack: AudioTokenBeacon: Null TokenId"

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->d(Ljava/lang/String;)I

    .line 85
    :cond_1
    :goto_0
    return-void

    .line 68
    :cond_2
    invoke-static {v7}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 69
    const-string v0, "E2E Advertise: AudioTokenBeacon: Requested to start advertising"

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->b(Ljava/lang/String;)I

    .line 70
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "Audio Stack: AudioTokenBeacon: Requested to start advertising token"

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p1, Lcom/google/android/location/copresence/b;->a:Lcom/google/android/gms/location/copresence/x;

    invoke-virtual {v2}, Lcom/google/android/gms/location/copresence/x;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->b(Ljava/lang/String;)I

    .line 73
    :cond_3
    invoke-virtual {p0}, Lcom/google/android/location/copresence/c/m;->a()Z

    move-result v0

    if-nez v0, :cond_4

    .line 74
    new-instance v0, Lcom/google/android/location/copresence/y;

    invoke-direct {v0}, Lcom/google/android/location/copresence/y;-><init>()V

    throw v0

    .line 77
    :cond_4
    iget-boolean v0, p0, Lcom/google/android/location/copresence/c/m;->e:Z

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/google/android/location/copresence/c/m;->d:Lcom/google/android/gms/location/copresence/x;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/location/copresence/x;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 81
    :cond_5
    iput-boolean v6, p0, Lcom/google/android/location/copresence/c/m;->e:Z

    .line 82
    iput-object v1, p0, Lcom/google/android/location/copresence/c/m;->d:Lcom/google/android/gms/location/copresence/x;

    .line 83
    iget-object v2, p0, Lcom/google/android/location/copresence/c/m;->b:Lcom/google/android/location/copresence/c/h;

    iget v0, p0, Lcom/google/android/location/copresence/c/m;->c:I

    sparse-switch v0, :sswitch_data_0

    const-string v0, "Audio Stack: AudioTokenBeacon: Invalid medium"

    new-instance v3, Ljava/lang/IllegalStateException;

    const-string v4, "getTokenBroadcasterParams called with invalid medium"

    invoke-direct {v3, v4}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    invoke-static {v0, v3}, Lcom/google/android/location/copresence/ag;->b(Ljava/lang/String;Ljava/lang/Throwable;)I

    iget-object v0, v1, Lcom/google/android/gms/location/copresence/x;->a:[B

    invoke-static {v0}, Lcom/google/android/gms/audiomodem/TokenBroadcaster$Params;->a([B)Lcom/google/android/gms/audiomodem/TokenBroadcaster$Params;

    move-result-object v0

    :goto_1
    invoke-static {v7}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v1

    if-eqz v1, :cond_6

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "Audio Stack: AudioStateMachine: "

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, v2, Lcom/google/android/location/copresence/c/h;->a:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "requesting to start advertising token"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/location/copresence/ag;->b(Ljava/lang/String;)I

    :cond_6
    iput-object v0, v2, Lcom/google/android/location/copresence/c/h;->h:Lcom/google/android/gms/audiomodem/TokenBroadcaster$Params;

    iget v0, v2, Lcom/google/android/location/copresence/c/h;->c:I

    packed-switch v0, :pswitch_data_0

    :cond_7
    :goto_2
    iget v0, v2, Lcom/google/android/location/copresence/c/h;->c:I

    invoke-virtual {v2, v0}, Lcom/google/android/location/copresence/c/h;->a(I)V

    goto/16 :goto_0

    :sswitch_0
    iget-object v0, v1, Lcom/google/android/gms/location/copresence/x;->a:[B

    array-length v0, v0

    invoke-static {v0}, Lcom/google/android/location/copresence/c/b;->b(I)Lcom/google/android/gms/audiomodem/Encoding;

    move-result-object v3

    new-instance v0, Lcom/google/android/gms/audiomodem/TokenBroadcaster$Params;

    iget-object v1, v1, Lcom/google/android/gms/location/copresence/x;->a:[B

    const/4 v4, -0x1

    new-array v5, v6, [Lcom/google/android/gms/audiomodem/Encoding;

    aput-object v3, v5, v8

    invoke-direct {v0, v1, v4, v5}, Lcom/google/android/gms/audiomodem/TokenBroadcaster$Params;-><init>([BI[Lcom/google/android/gms/audiomodem/Encoding;)V

    goto :goto_1

    :sswitch_1
    iget-object v0, v1, Lcom/google/android/gms/location/copresence/x;->a:[B

    array-length v0, v0

    invoke-static {v0}, Lcom/google/android/location/copresence/c/b;->a(I)Lcom/google/android/gms/audiomodem/Encoding;

    move-result-object v3

    new-instance v0, Lcom/google/android/gms/audiomodem/TokenBroadcaster$Params;

    iget-object v1, v1, Lcom/google/android/gms/location/copresence/x;->a:[B

    invoke-static {}, Lcom/google/android/location/copresence/f/b;->b()Lcom/google/ac/b/c/g;

    move-result-object v4

    iget-object v4, v4, Lcom/google/ac/b/c/g;->g:Lcom/google/ac/b/c/b;

    iget-object v4, v4, Lcom/google/ac/b/c/b;->a:Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    new-array v5, v6, [Lcom/google/android/gms/audiomodem/Encoding;

    aput-object v3, v5, v8

    invoke-direct {v0, v1, v4, v5}, Lcom/google/android/gms/audiomodem/TokenBroadcaster$Params;-><init>([BI[Lcom/google/android/gms/audiomodem/Encoding;)V

    goto :goto_1

    :pswitch_0
    iput v9, v2, Lcom/google/android/location/copresence/c/h;->c:I

    goto :goto_2

    :pswitch_1
    iget-boolean v0, v2, Lcom/google/android/location/copresence/c/h;->d:Z

    if-eqz v0, :cond_8

    iget v0, v2, Lcom/google/android/location/copresence/c/h;->b:I

    if-ne v0, v6, :cond_8

    iput v6, v2, Lcom/google/android/location/copresence/c/h;->c:I

    goto :goto_2

    :cond_8
    iput v9, v2, Lcom/google/android/location/copresence/c/h;->c:I

    goto :goto_2

    :pswitch_2
    invoke-static {v7}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v0

    if-eqz v0, :cond_7

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Audio Stack: AudioStateMachine: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, v2, Lcom/google/android/location/copresence/c/h;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "Remaining in state "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, v2, Lcom/google/android/location/copresence/c/h;->c:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->b(Ljava/lang/String;)I

    goto :goto_2

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_1
        0x8 -> :sswitch_0
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method public final a()Z
    .locals 3

    .prologue
    .line 40
    iget v0, p0, Lcom/google/android/location/copresence/c/m;->c:I

    sparse-switch v0, :sswitch_data_0

    .line 46
    const-string v0, "Audio Stack: AudioTokenBeacon: Invalid medium"

    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "canAdvertise called with invalid medium"

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    invoke-static {v0, v1}, Lcom/google/android/location/copresence/ag;->b(Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 49
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 42
    :sswitch_0
    invoke-static {}, Lcom/google/android/location/copresence/f/b;->b()Lcom/google/ac/b/c/g;

    move-result-object v0

    iget-object v0, v0, Lcom/google/ac/b/c/g;->e:Lcom/google/ac/b/c/m;

    iget-object v0, v0, Lcom/google/ac/b/c/m;->c:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    goto :goto_0

    .line 44
    :sswitch_1
    invoke-static {}, Lcom/google/android/location/copresence/f/b;->b()Lcom/google/ac/b/c/g;

    move-result-object v0

    iget-object v0, v0, Lcom/google/ac/b/c/g;->e:Lcom/google/ac/b/c/m;

    iget-object v0, v0, Lcom/google/ac/b/c/m;->B:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    goto :goto_0

    .line 40
    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x8 -> :sswitch_1
    .end sparse-switch
.end method

.method public final b()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x0

    const/4 v3, 0x3

    .line 89
    invoke-static {v3}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 90
    const-string v0, "E2E Advertise: AudioTokenBeacon: Requested to stop advertising"

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->b(Ljava/lang/String;)I

    .line 91
    const-string v0, "Audio Stack: AudioTokenBeacon: Requested to stop advertising"

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->b(Ljava/lang/String;)I

    .line 93
    :cond_0
    iget-boolean v0, p0, Lcom/google/android/location/copresence/c/m;->e:Z

    if-nez v0, :cond_1

    .line 99
    :goto_0
    return-void

    .line 96
    :cond_1
    iget-object v0, p0, Lcom/google/android/location/copresence/c/m;->b:Lcom/google/android/location/copresence/c/h;

    invoke-static {v3}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v1

    if-eqz v1, :cond_2

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Audio Stack: AudioStateMachine: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, v0, Lcom/google/android/location/copresence/c/h;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " requesting to stop advertising token"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/location/copresence/ag;->b(Ljava/lang/String;)I

    :cond_2
    iput-object v5, v0, Lcom/google/android/location/copresence/c/h;->h:Lcom/google/android/gms/audiomodem/TokenBroadcaster$Params;

    iget v1, v0, Lcom/google/android/location/copresence/c/h;->c:I

    packed-switch v1, :pswitch_data_0

    :cond_3
    :goto_1
    iget v1, v0, Lcom/google/android/location/copresence/c/h;->c:I

    invoke-virtual {v0, v1}, Lcom/google/android/location/copresence/c/h;->a(I)V

    .line 97
    iput-object v5, p0, Lcom/google/android/location/copresence/c/m;->d:Lcom/google/android/gms/location/copresence/x;

    .line 98
    iput-boolean v4, p0, Lcom/google/android/location/copresence/c/m;->e:Z

    goto :goto_0

    .line 96
    :pswitch_0
    iput v3, v0, Lcom/google/android/location/copresence/c/h;->c:I

    goto :goto_1

    :pswitch_1
    iput v4, v0, Lcom/google/android/location/copresence/c/h;->c:I

    goto :goto_1

    :pswitch_2
    invoke-static {v3}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v1

    if-eqz v1, :cond_3

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Audio Stack: AudioStateMachine: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, v0, Lcom/google/android/location/copresence/c/h;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "Remaining in state "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, v0, Lcom/google/android/location/copresence/c/h;->c:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/location/copresence/ag;->b(Ljava/lang/String;)I

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
