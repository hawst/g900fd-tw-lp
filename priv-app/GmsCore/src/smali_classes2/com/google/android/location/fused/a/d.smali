.class public final Lcom/google/android/location/fused/a/d;
.super Lcom/google/android/location/fused/a/b;
.source "SourceFile"


# instance fields
.field final b:Lcom/google/android/location/fused/service/a;

.field final c:Lcom/google/android/gms/common/util/p;

.field d:I

.field e:J

.field private final l:I

.field private final m:Landroid/content/BroadcastReceiver;

.field private final n:Landroid/app/PendingIntent;

.field private final o:Landroid/content/Context;

.field private final p:Lcom/google/android/location/n/c;

.field private final q:Landroid/os/Handler;

.field private r:D

.field private s:Z

.field private t:Z

.field private u:J

.field private v:Z

.field private w:Z


# direct methods
.method public constructor <init>(Lcom/google/android/location/fused/service/a;ILandroid/content/Context;Lcom/google/android/location/n/c;Lcom/google/android/gms/common/util/p;Landroid/os/Looper;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 69
    invoke-direct {p0}, Lcom/google/android/location/fused/a/b;-><init>()V

    .line 49
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/location/fused/a/d;->r:D

    .line 50
    iput-boolean v2, p0, Lcom/google/android/location/fused/a/d;->s:Z

    .line 51
    iput-boolean v2, p0, Lcom/google/android/location/fused/a/d;->t:Z

    .line 53
    iput v2, p0, Lcom/google/android/location/fused/a/d;->d:I

    .line 55
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/location/fused/a/d;->u:J

    .line 57
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/google/android/location/fused/a/d;->e:J

    .line 59
    iput-boolean v2, p0, Lcom/google/android/location/fused/a/d;->v:Z

    .line 61
    iput-boolean v2, p0, Lcom/google/android/location/fused/a/d;->w:Z

    .line 70
    iput-object p1, p0, Lcom/google/android/location/fused/a/d;->b:Lcom/google/android/location/fused/service/a;

    .line 71
    iput p2, p0, Lcom/google/android/location/fused/a/d;->l:I

    .line 72
    iput-object p3, p0, Lcom/google/android/location/fused/a/d;->o:Landroid/content/Context;

    .line 73
    iput-object p4, p0, Lcom/google/android/location/fused/a/d;->p:Lcom/google/android/location/n/c;

    .line 74
    iput-object p5, p0, Lcom/google/android/location/fused/a/d;->c:Lcom/google/android/gms/common/util/p;

    .line 75
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0, p6}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/google/android/location/fused/a/d;->q:Landroid/os/Handler;

    .line 78
    new-instance v0, Lcom/google/android/location/fused/a/e;

    invoke-direct {v0, p0}, Lcom/google/android/location/fused/a/e;-><init>(Lcom/google/android/location/fused/a/d;)V

    iput-object v0, p0, Lcom/google/android/location/fused/a/d;->m:Landroid/content/BroadcastReceiver;

    .line 88
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.google.android.gms.flp.HAL_ALARM"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const/high16 v1, 0x8000000

    invoke-static {p3, v2, v0, v1}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/fused/a/d;->n:Landroid/app/PendingIntent;

    .line 93
    return-void
.end method


# virtual methods
.method protected final a()V
    .locals 14

    .prologue
    const-wide/16 v2, 0x3e8

    const/4 v8, 0x1

    const/4 v4, 0x0

    const-wide v12, 0x7fffffffffffffffL

    .line 97
    iget-object v0, p0, Lcom/google/android/location/fused/a/d;->b:Lcom/google/android/location/fused/service/a;

    invoke-virtual {v0}, Lcom/google/android/location/fused/service/a;->b()Z

    move-result v0

    if-nez v0, :cond_0

    .line 148
    :goto_0
    return-void

    .line 101
    :cond_0
    iput v4, p0, Lcom/google/android/location/fused/a/d;->d:I

    .line 102
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/location/fused/a/d;->u:J

    .line 103
    iget-boolean v0, p0, Lcom/google/android/location/fused/a/q;->h:Z

    if-eqz v0, :cond_7

    iget-boolean v0, p0, Lcom/google/android/location/fused/a/q;->i:Z

    if-eqz v0, :cond_7

    iget-wide v0, p0, Lcom/google/android/location/fused/a/m;->f:J

    cmp-long v0, v0, v12

    if-gez v0, :cond_7

    .line 104
    const/4 v0, 0x4

    .line 105
    iget-boolean v1, p0, Lcom/google/android/location/fused/a/d;->s:Z

    if-eqz v1, :cond_1

    .line 106
    const/4 v0, 0x5

    .line 108
    :cond_1
    iget-boolean v1, p0, Lcom/google/android/location/fused/a/d;->t:Z

    if-eqz v1, :cond_8

    .line 109
    or-int/lit8 v6, v0, 0xa

    .line 112
    :goto_1
    iget-wide v4, p0, Lcom/google/android/location/fused/a/m;->f:J

    .line 113
    iget-wide v10, p0, Lcom/google/android/location/fused/a/b;->a:J

    .line 114
    const/4 v7, 0x2

    .line 115
    cmp-long v0, v4, v12

    if-gez v0, :cond_2

    const-wide/16 v0, 0x2

    mul-long/2addr v0, v4

    cmp-long v0, v10, v0

    if-ltz v0, :cond_2

    .line 118
    cmp-long v0, v4, v2

    if-gez v0, :cond_4

    move-wide v0, v2

    .line 119
    :goto_2
    div-long v0, v10, v0

    long-to-int v0, v0

    iput v0, p0, Lcom/google/android/location/fused/a/d;->d:I

    .line 120
    const-wide/16 v0, 0x7d0

    add-long/2addr v0, v10

    iput-wide v0, p0, Lcom/google/android/location/fused/a/d;->u:J

    move v7, v8

    .line 123
    :cond_2
    cmp-long v0, v4, v12

    if-gez v0, :cond_5

    const-wide/32 v0, 0xf4240

    mul-long v2, v4, v0

    .line 126
    :goto_3
    iget-boolean v0, p0, Lcom/google/android/location/fused/a/d;->w:Z

    if-nez v0, :cond_6

    .line 127
    iget-object v0, p0, Lcom/google/android/location/fused/a/d;->b:Lcom/google/android/location/fused/service/a;

    iget v1, p0, Lcom/google/android/location/fused/a/d;->l:I

    iget-wide v4, p0, Lcom/google/android/location/fused/a/d;->r:D

    invoke-virtual/range {v0 .. v7}, Lcom/google/android/location/fused/service/a;->a(IJDII)V

    .line 133
    iput-boolean v8, p0, Lcom/google/android/location/fused/a/d;->w:Z

    .line 147
    :cond_3
    :goto_4
    invoke-virtual {p0}, Lcom/google/android/location/fused/a/d;->d()V

    goto :goto_0

    :cond_4
    move-wide v0, v4

    .line 118
    goto :goto_2

    :cond_5
    move-wide v2, v4

    .line 123
    goto :goto_3

    .line 135
    :cond_6
    iget-object v0, p0, Lcom/google/android/location/fused/a/d;->b:Lcom/google/android/location/fused/service/a;

    iget v1, p0, Lcom/google/android/location/fused/a/d;->l:I

    iget-wide v4, p0, Lcom/google/android/location/fused/a/d;->r:D

    invoke-virtual/range {v0 .. v7}, Lcom/google/android/location/fused/service/a;->b(IJDII)V

    goto :goto_4

    .line 142
    :cond_7
    iget-boolean v0, p0, Lcom/google/android/location/fused/a/d;->w:Z

    if-eqz v0, :cond_3

    .line 143
    iget-object v0, p0, Lcom/google/android/location/fused/a/d;->b:Lcom/google/android/location/fused/service/a;

    iget v1, p0, Lcom/google/android/location/fused/a/d;->l:I

    invoke-virtual {v0, v1}, Lcom/google/android/location/fused/service/a;->a(I)V

    .line 144
    iput-boolean v4, p0, Lcom/google/android/location/fused/a/d;->w:Z

    goto :goto_4

    :cond_8
    move v6, v0

    goto :goto_1
.end method

.method public final b()V
    .locals 4

    .prologue
    const-wide v2, 0x40c3878000000000L    # 9999.0

    .line 154
    iget-wide v0, p0, Lcom/google/android/location/fused/a/d;->r:D

    cmpl-double v0, v0, v2

    if-eqz v0, :cond_0

    .line 155
    iput-wide v2, p0, Lcom/google/android/location/fused/a/d;->r:D

    .line 156
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/location/fused/a/q;->k:Z

    .line 158
    :cond_0
    return-void
.end method

.method public final b(Z)V
    .locals 1

    .prologue
    .line 174
    iget-boolean v0, p0, Lcom/google/android/location/fused/a/d;->t:Z

    if-eq v0, p1, :cond_0

    .line 175
    iput-boolean p1, p0, Lcom/google/android/location/fused/a/d;->t:Z

    .line 176
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/location/fused/a/q;->k:Z

    .line 178
    :cond_0
    return-void
.end method

.method public final c()V
    .locals 2

    .prologue
    .line 184
    iget-object v0, p0, Lcom/google/android/location/fused/a/d;->c:Lcom/google/android/gms/common/util/p;

    invoke-interface {v0}, Lcom/google/android/gms/common/util/p;->b()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/location/fused/a/d;->e:J

    .line 185
    invoke-virtual {p0}, Lcom/google/android/location/fused/a/d;->d()V

    .line 186
    return-void
.end method

.method public final c_(Z)V
    .locals 1

    .prologue
    .line 164
    iget-boolean v0, p0, Lcom/google/android/location/fused/a/d;->s:Z

    if-eq v0, p1, :cond_0

    .line 165
    iput-boolean p1, p0, Lcom/google/android/location/fused/a/d;->s:Z

    .line 166
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/location/fused/a/q;->k:Z

    .line 168
    :cond_0
    return-void
.end method

.method final d()V
    .locals 6

    .prologue
    .line 194
    iget v0, p0, Lcom/google/android/location/fused/a/d;->d:I

    if-nez v0, :cond_1

    .line 195
    iget-boolean v0, p0, Lcom/google/android/location/fused/a/d;->v:Z

    if-eqz v0, :cond_0

    .line 196
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/google/android/location/fused/a/d;->e:J

    .line 197
    iget-object v0, p0, Lcom/google/android/location/fused/a/d;->p:Lcom/google/android/location/n/c;

    iget-object v1, p0, Lcom/google/android/location/fused/a/d;->n:Landroid/app/PendingIntent;

    invoke-virtual {v0, v1}, Lcom/google/android/location/n/c;->a(Landroid/app/PendingIntent;)V

    .line 198
    iget-object v0, p0, Lcom/google/android/location/fused/a/d;->o:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/location/fused/a/d;->m:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 199
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/location/fused/a/d;->v:Z

    .line 220
    :cond_0
    :goto_0
    return-void

    .line 202
    :cond_1
    iget-boolean v0, p0, Lcom/google/android/location/fused/a/d;->v:Z

    if-nez v0, :cond_2

    .line 203
    new-instance v0, Landroid/content/IntentFilter;

    const-string v1, "com.google.android.gms.flp.HAL_ALARM"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 204
    iget-object v1, p0, Lcom/google/android/location/fused/a/d;->o:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/location/fused/a/d;->m:Landroid/content/BroadcastReceiver;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/android/location/fused/a/d;->q:Landroid/os/Handler;

    invoke-virtual {v1, v2, v0, v3, v4}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;Ljava/lang/String;Landroid/os/Handler;)Landroid/content/Intent;

    .line 209
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/location/fused/a/d;->v:Z

    .line 212
    :cond_2
    iget-wide v0, p0, Lcom/google/android/location/fused/a/d;->e:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-gez v0, :cond_3

    iget-object v0, p0, Lcom/google/android/location/fused/a/d;->c:Lcom/google/android/gms/common/util/p;

    invoke-interface {v0}, Lcom/google/android/gms/common/util/p;->b()J

    move-result-wide v0

    .line 214
    :goto_1
    iget-object v2, p0, Lcom/google/android/location/fused/a/d;->p:Lcom/google/android/location/n/c;

    const/4 v3, 0x2

    iget-wide v4, p0, Lcom/google/android/location/fused/a/d;->u:J

    add-long/2addr v0, v4

    iget-object v4, p0, Lcom/google/android/location/fused/a/q;->j:Ljava/util/Collection;

    iget-object v4, p0, Lcom/google/android/location/fused/a/d;->n:Landroid/app/PendingIntent;

    invoke-virtual {v2, v3, v0, v1, v4}, Lcom/google/android/location/n/c;->a(IJLandroid/app/PendingIntent;)V

    goto :goto_0

    .line 212
    :cond_3
    iget-wide v0, p0, Lcom/google/android/location/fused/a/d;->e:J

    goto :goto_1
.end method

.method public final toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 224
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Hardware FLP [clientId="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v1, p0, Lcom/google/android/location/fused/a/d;->l:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 225
    const-string v1, ", maxPower[mW]="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p0, Lcom/google/android/location/fused/a/d;->r:D

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    .line 226
    const-string v1, ", connectedToHal="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/location/fused/a/d;->b:Lcom/google/android/location/fused/service/a;

    invoke-virtual {v2}, Lcom/google/android/location/fused/service/a;->b()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 227
    invoke-virtual {p0, v0}, Lcom/google/android/location/fused/a/d;->a(Ljava/lang/StringBuilder;)V

    .line 228
    const/16 v1, 0x5d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 229
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
