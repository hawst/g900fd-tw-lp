.class public final Lcom/google/android/location/activity/z;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field private final a:Lcom/google/android/location/os/bi;

.field private final b:J

.field private final c:Z


# direct methods
.method public constructor <init>(Lcom/google/android/location/os/bi;JZ)V
    .locals 0

    .prologue
    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    iput-object p1, p0, Lcom/google/android/location/activity/z;->a:Lcom/google/android/location/os/bi;

    .line 38
    iput-wide p2, p0, Lcom/google/android/location/activity/z;->b:J

    .line 39
    iput-boolean p4, p0, Lcom/google/android/location/activity/z;->c:Z

    .line 40
    return-void
.end method

.method static synthetic a(Lcom/google/android/location/activity/z;)Lcom/google/android/location/os/bi;
    .locals 1

    .prologue
    .line 24
    iget-object v0, p0, Lcom/google/android/location/activity/z;->a:Lcom/google/android/location/os/bi;

    return-object v0
.end method

.method static synthetic b(Lcom/google/android/location/activity/z;)Z
    .locals 1

    .prologue
    .line 24
    iget-boolean v0, p0, Lcom/google/android/location/activity/z;->c:Z

    return v0
.end method


# virtual methods
.method public final run()V
    .locals 15

    .prologue
    .line 44
    invoke-static {}, Lcom/android/location/provider/ActivityRecognitionProviderWatcher;->getInstance()Lcom/android/location/provider/ActivityRecognitionProviderWatcher;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/location/provider/ActivityRecognitionProviderWatcher;->getActivityRecognitionProvider()Lcom/android/location/provider/ActivityRecognitionProvider;

    move-result-object v9

    .line 46
    if-nez v9, :cond_2

    .line 47
    sget-boolean v0, Lcom/google/android/location/i/a;->e:Z

    if-eqz v0, :cond_0

    const-string v0, "HardwareActivityRecognitionLogger"

    const-string v1, "Provider is not supported."

    invoke-static {v0, v1}, Lcom/google/android/location/o/a/a;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 48
    :cond_0
    iget-boolean v0, p0, Lcom/google/android/location/activity/z;->c:Z

    if-eqz v0, :cond_1

    .line 49
    iget-object v0, p0, Lcom/google/android/location/activity/z;->a:Lcom/google/android/location/os/bi;

    const-string v1, "ar"

    const-string v2, "hw_not_supported"

    const-string v3, "provider is null"

    const-wide/16 v4, 0x1

    const/4 v6, 0x0

    invoke-interface/range {v0 .. v6}, Lcom/google/android/location/os/bi;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JZ)V

    .line 128
    :cond_1
    :goto_0
    return-void

    .line 57
    :cond_2
    :try_start_0
    invoke-virtual {v9}, Lcom/android/location/provider/ActivityRecognitionProvider;->getSupportedActivities()[Ljava/lang/String;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v10

    .line 66
    array-length v1, v10

    const/4 v0, 0x0

    :goto_1
    if-ge v0, v1, :cond_5

    aget-object v2, v10, v0

    .line 67
    sget-boolean v3, Lcom/google/android/location/i/a;->b:Z

    if-eqz v3, :cond_3

    const-string v3, "HardwareActivityRecognitionLogger"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "SupportedActivity: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v2}, Lcom/google/android/location/o/a/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 66
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 58
    :catch_0
    move-exception v0

    .line 59
    sget-boolean v1, Lcom/google/android/location/i/a;->e:Z

    if-eqz v1, :cond_4

    const-string v1, "HardwareActivityRecognitionLogger"

    const-string v2, "Error reading supported activities"

    invoke-static {v1, v2, v0}, Lcom/google/android/location/o/a/a;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 60
    :cond_4
    iget-boolean v0, p0, Lcom/google/android/location/activity/z;->c:Z

    if-eqz v0, :cond_1

    .line 61
    iget-object v0, p0, Lcom/google/android/location/activity/z;->a:Lcom/google/android/location/os/bi;

    const-string v1, "ar"

    const-string v2, "hw_not_supported"

    const-string v3, "remote exception"

    const-wide/16 v4, 0x1

    const/4 v6, 0x0

    invoke-interface/range {v0 .. v6}, Lcom/google/android/location/os/bi;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JZ)V

    goto :goto_0

    .line 69
    :cond_5
    invoke-static {v10}, Ljava/util/Arrays;->sort([Ljava/lang/Object;)V

    .line 70
    iget-boolean v0, p0, Lcom/google/android/location/activity/z;->c:Z

    if-eqz v0, :cond_6

    .line 71
    iget-object v0, p0, Lcom/google/android/location/activity/z;->a:Lcom/google/android/location/os/bi;

    const-string v1, "ar"

    const-string v2, "hw_supported_activities"

    const-string v3, ","

    invoke-static {v3, v10}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    const-wide/16 v4, 0x1

    const/4 v6, 0x1

    invoke-interface/range {v0 .. v6}, Lcom/google/android/location/os/bi;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JZ)V

    .line 75
    :cond_6
    new-instance v0, Lcom/google/android/location/activity/aa;

    invoke-direct {v0, p0}, Lcom/google/android/location/activity/aa;-><init>(Lcom/google/android/location/activity/z;)V

    invoke-virtual {v9, v0}, Lcom/android/location/provider/ActivityRecognitionProvider;->registerSink(Lcom/android/location/provider/ActivityRecognitionProvider$Sink;)V

    .line 98
    const/4 v0, 0x2

    new-array v11, v0, [I

    fill-array-data v11, :array_0

    .line 102
    sget-boolean v0, Lcom/google/android/location/i/a;->b:Z

    if-eqz v0, :cond_7

    const-string v0, "HardwareActivityRecognitionLogger"

    const-string v1, "Enabling HW AR."

    invoke-static {v0, v1}, Lcom/google/android/location/o/a/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 104
    :cond_7
    array-length v12, v10

    const/4 v0, 0x0

    move v8, v0

    :goto_2
    if-ge v8, v12, :cond_c

    aget-object v13, v10, v8

    .line 105
    array-length v14, v11

    const/4 v0, 0x0

    move v7, v0

    :goto_3
    if-ge v7, v14, :cond_b

    aget v4, v11, v7

    .line 106
    :try_start_1
    iget-wide v0, p0, Lcom/google/android/location/activity/z;->b:J

    invoke-virtual {v9, v13, v4, v0, v1}, Lcom/android/location/provider/ActivityRecognitionProvider;->enableActivityEvent(Ljava/lang/String;IJ)Z

    move-result v0

    .line 110
    const-string v1, "Enable result=%s, activity=%s, eventType=%s"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    aput-object v5, v2, v3

    const/4 v3, 0x1

    aput-object v13, v2, v3

    const/4 v3, 0x2

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 112
    sget-boolean v2, Lcom/google/android/location/i/a;->b:Z

    if-eqz v2, :cond_8

    const-string v2, "HardwareActivityRecognitionLogger"

    invoke-static {v2, v1}, Lcom/google/android/location/o/a/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 113
    :cond_8
    invoke-static {v0}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v0

    move-object v3, v0

    .line 118
    :goto_4
    iget-boolean v0, p0, Lcom/google/android/location/activity/z;->c:Z

    if-eqz v0, :cond_9

    .line 119
    iget-object v0, p0, Lcom/google/android/location/activity/z;->a:Lcom/google/android/location/os/bi;

    const-string v1, "ar"

    const-string v2, "hw_enable_activity_event"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ":"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ":"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const-wide/16 v4, 0x1

    const/4 v6, 0x1

    invoke-interface/range {v0 .. v6}, Lcom/google/android/location/os/bi;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JZ)V

    .line 105
    :cond_9
    add-int/lit8 v0, v7, 0x1

    move v7, v0

    goto :goto_3

    .line 114
    :catch_1
    move-exception v0

    .line 115
    sget-boolean v1, Lcom/google/android/location/i/a;->e:Z

    if-eqz v1, :cond_a

    const-string v1, "HardwareActivityRecognitionLogger"

    const-string v2, "Error on enable."

    invoke-static {v1, v2, v0}, Lcom/google/android/location/o/a/a;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 116
    :cond_a
    const-string v0, "RemoteException"

    move-object v3, v0

    goto :goto_4

    .line 104
    :cond_b
    add-int/lit8 v0, v8, 0x1

    move v8, v0

    goto/16 :goto_2

    .line 125
    :cond_c
    iget-boolean v0, p0, Lcom/google/android/location/activity/z;->c:Z

    if-eqz v0, :cond_1

    .line 126
    iget-object v0, p0, Lcom/google/android/location/activity/z;->a:Lcom/google/android/location/os/bi;

    iget-object v1, p0, Lcom/google/android/location/activity/z;->a:Lcom/google/android/location/os/bi;

    invoke-interface {v1}, Lcom/google/android/location/os/bi;->c()Lcom/google/android/location/j/b;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/location/j/b;->c()J

    move-result-wide v2

    invoke-interface {v0, v2, v3}, Lcom/google/android/location/os/bi;->a(J)V

    goto/16 :goto_0

    .line 98
    nop

    :array_0
    .array-data 4
        0x1
        0x2
    .end array-data
.end method
