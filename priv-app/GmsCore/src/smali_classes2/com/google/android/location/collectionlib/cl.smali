.class final Lcom/google/android/location/collectionlib/cl;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x11
.end annotation


# static fields
.field private static final c:[I


# instance fields
.field a:Lcom/google/android/location/collectionlib/cn;

.field final b:Ljava/lang/Runnable;

.field private final d:Lcom/google/android/location/o/a/c;

.field private final e:Landroid/hardware/SensorManager;

.field private final f:Lcom/google/android/location/o/h;

.field private final g:Lcom/google/android/location/j/b;

.field private final h:Ljava/lang/Object;

.field private i:Z

.field private j:Z

.field private k:J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 67
    const/4 v0, 0x3

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/google/android/location/collectionlib/cl;->c:[I

    .line 69
    return-void

    .line 67
    nop

    :array_0
    .array-data 4
        0x1
        0x2
        0x4
    .end array-data
.end method

.method private constructor <init>(Landroid/content/Context;Lcom/google/android/location/j/b;Lcom/google/android/location/o/h;Lcom/google/android/location/o/a/c;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 77
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 44
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/location/collectionlib/cl;->h:Ljava/lang/Object;

    .line 45
    iput-boolean v1, p0, Lcom/google/android/location/collectionlib/cl;->i:Z

    .line 48
    iput-boolean v1, p0, Lcom/google/android/location/collectionlib/cl;->j:Z

    .line 49
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/location/collectionlib/cl;->k:J

    .line 52
    new-instance v0, Lcom/google/android/location/collectionlib/cm;

    invoke-direct {v0, p0}, Lcom/google/android/location/collectionlib/cm;-><init>(Lcom/google/android/location/collectionlib/cl;)V

    iput-object v0, p0, Lcom/google/android/location/collectionlib/cl;->b:Ljava/lang/Runnable;

    .line 78
    const-string v0, "sensor"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/SensorManager;

    iput-object v0, p0, Lcom/google/android/location/collectionlib/cl;->e:Landroid/hardware/SensorManager;

    .line 79
    iput-object p2, p0, Lcom/google/android/location/collectionlib/cl;->g:Lcom/google/android/location/j/b;

    .line 80
    invoke-static {p4}, Lcom/google/android/location/collectionlib/df;->a(Lcom/google/android/location/o/a/c;)Lcom/google/android/location/o/a/c;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/collectionlib/cl;->d:Lcom/google/android/location/o/a/c;

    .line 81
    iput-object p3, p0, Lcom/google/android/location/collectionlib/cl;->f:Lcom/google/android/location/o/h;

    .line 82
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/android/location/o/a/c;)V
    .locals 7

    .prologue
    .line 72
    new-instance v6, Lcom/google/android/location/os/real/ap;

    invoke-direct {v6}, Lcom/google/android/location/os/real/ap;-><init>()V

    const-string v0, "power"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/os/PowerManager;

    new-instance v0, Lcom/google/android/location/o/h;

    const/4 v2, 0x1

    const/4 v3, 0x0

    const-string v4, "SensorTimestampCalibrator"

    sget-object v5, Lcom/google/android/location/o/h;->b:[S

    invoke-direct/range {v0 .. v5}, Lcom/google/android/location/o/h;-><init>(Landroid/os/PowerManager;IZLjava/lang/String;[S)V

    invoke-direct {p0, p1, v6, v0, p2}, Lcom/google/android/location/collectionlib/cl;-><init>(Landroid/content/Context;Lcom/google/android/location/j/b;Lcom/google/android/location/o/h;Lcom/google/android/location/o/a/c;)V

    .line 73
    return-void
.end method

.method static synthetic a(Lcom/google/android/location/collectionlib/cl;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 24
    iget-object v0, p0, Lcom/google/android/location/collectionlib/cl;->h:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/location/collectionlib/cl;ZJ)V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/location/collectionlib/cl;->a(ZJ)V

    return-void
.end method

.method private a(ZJ)V
    .locals 2

    .prologue
    .line 159
    iget-object v0, p0, Lcom/google/android/location/collectionlib/cl;->a:Lcom/google/android/location/collectionlib/cn;

    if-eqz v0, :cond_0

    .line 160
    iget-object v0, p0, Lcom/google/android/location/collectionlib/cl;->e:Landroid/hardware/SensorManager;

    iget-object v1, p0, Lcom/google/android/location/collectionlib/cl;->a:Lcom/google/android/location/collectionlib/cn;

    invoke-virtual {v0, v1}, Landroid/hardware/SensorManager;->unregisterListener(Landroid/hardware/SensorEventListener;)V

    .line 162
    :cond_0
    iput-boolean p1, p0, Lcom/google/android/location/collectionlib/cl;->i:Z

    .line 163
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/location/collectionlib/cl;->j:Z

    .line 164
    iput-wide p2, p0, Lcom/google/android/location/collectionlib/cl;->k:J

    .line 165
    sget-boolean v0, Lcom/google/android/location/i/a;->b:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/location/collectionlib/cl;->d:Lcom/google/android/location/o/a/c;

    const-string v1, "Released wakelock."

    invoke-virtual {v0, v1}, Lcom/google/android/location/o/a/c;->a(Ljava/lang/String;)V

    .line 166
    :cond_1
    iget-object v0, p0, Lcom/google/android/location/collectionlib/cl;->f:Lcom/google/android/location/o/h;

    invoke-virtual {v0}, Lcom/google/android/location/o/h;->b()V

    .line 167
    return-void
.end method

.method static synthetic b(Lcom/google/android/location/collectionlib/cl;)Z
    .locals 1

    .prologue
    .line 24
    iget-boolean v0, p0, Lcom/google/android/location/collectionlib/cl;->i:Z

    return v0
.end method

.method static synthetic c(Lcom/google/android/location/collectionlib/cl;)Lcom/google/android/location/o/a/c;
    .locals 1

    .prologue
    .line 24
    iget-object v0, p0, Lcom/google/android/location/collectionlib/cl;->d:Lcom/google/android/location/o/a/c;

    return-object v0
.end method

.method static synthetic d(Lcom/google/android/location/collectionlib/cl;)Lcom/google/android/location/j/b;
    .locals 1

    .prologue
    .line 24
    iget-object v0, p0, Lcom/google/android/location/collectionlib/cl;->g:Lcom/google/android/location/j/b;

    return-object v0
.end method

.method static synthetic e(Lcom/google/android/location/collectionlib/cl;)J
    .locals 2

    .prologue
    .line 24
    iget-wide v0, p0, Lcom/google/android/location/collectionlib/cl;->k:J

    return-wide v0
.end method


# virtual methods
.method public final a()Z
    .locals 8

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 96
    iget-object v4, p0, Lcom/google/android/location/collectionlib/cl;->h:Ljava/lang/Object;

    monitor-enter v4

    .line 97
    :try_start_0
    iget-boolean v3, p0, Lcom/google/android/location/collectionlib/cl;->j:Z

    if-eqz v3, :cond_1

    .line 98
    sget-boolean v1, Lcom/google/android/location/i/a;->b:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/location/collectionlib/cl;->d:Lcom/google/android/location/o/a/c;

    const-string v2, "Already calibrating."

    invoke-virtual {v1, v2}, Lcom/google/android/location/o/a/c;->a(Ljava/lang/String;)V

    .line 99
    :cond_0
    monitor-exit v4

    .line 125
    :goto_0
    return v0

    .line 101
    :cond_1
    iget-boolean v3, p0, Lcom/google/android/location/collectionlib/cl;->i:Z

    if-eqz v3, :cond_3

    .line 102
    sget-boolean v1, Lcom/google/android/location/i/a;->b:Z

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/google/android/location/collectionlib/cl;->d:Lcom/google/android/location/o/a/c;

    const-string v2, "Already calibrated."

    invoke-virtual {v1, v2}, Lcom/google/android/location/o/a/c;->a(Ljava/lang/String;)V

    .line 103
    :cond_2
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 126
    :catchall_0
    move-exception v0

    monitor-exit v4

    throw v0

    .line 105
    :cond_3
    :try_start_1
    sget-object v5, Lcom/google/android/location/collectionlib/cl;->c:[I

    array-length v6, v5

    move v3, v1

    :goto_1
    if-ge v3, v6, :cond_6

    aget v0, v5, v3

    iget-object v7, p0, Lcom/google/android/location/collectionlib/cl;->e:Landroid/hardware/SensorManager;

    invoke-virtual {v7, v0}, Landroid/hardware/SensorManager;->getDefaultSensor(I)Landroid/hardware/Sensor;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 106
    :goto_2
    if-nez v0, :cond_7

    .line 107
    sget-boolean v0, Lcom/google/android/location/i/a;->b:Z

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/google/android/location/collectionlib/cl;->d:Lcom/google/android/location/o/a/c;

    const-string v2, "Couldn\'t find sensor for calibration"

    invoke-virtual {v0, v2}, Lcom/google/android/location/o/a/c;->a(Ljava/lang/String;)V

    .line 108
    :cond_4
    monitor-exit v4

    move v0, v1

    goto :goto_0

    .line 105
    :cond_5
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_1

    :cond_6
    move-object v0, v2

    goto :goto_2

    .line 111
    :cond_7
    iget-object v1, p0, Lcom/google/android/location/collectionlib/cl;->f:Lcom/google/android/location/o/h;

    invoke-virtual {v1}, Lcom/google/android/location/o/h;->a()V

    .line 112
    sget-boolean v1, Lcom/google/android/location/i/a;->b:Z

    if-eqz v1, :cond_8

    iget-object v1, p0, Lcom/google/android/location/collectionlib/cl;->d:Lcom/google/android/location/o/a/c;

    const-string v2, "Acquired wakelock."

    invoke-virtual {v1, v2}, Lcom/google/android/location/o/a/c;->a(Ljava/lang/String;)V

    .line 113
    :cond_8
    new-instance v1, Lcom/google/android/location/collectionlib/cn;

    invoke-direct {v1, p0}, Lcom/google/android/location/collectionlib/cn;-><init>(Lcom/google/android/location/collectionlib/cl;)V

    iput-object v1, p0, Lcom/google/android/location/collectionlib/cl;->a:Lcom/google/android/location/collectionlib/cn;

    .line 114
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/location/collectionlib/cl;->j:Z

    .line 115
    new-instance v1, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 116
    iget-object v2, p0, Lcom/google/android/location/collectionlib/cl;->e:Landroid/hardware/SensorManager;

    iget-object v3, p0, Lcom/google/android/location/collectionlib/cl;->a:Lcom/google/android/location/collectionlib/cn;

    const/4 v5, 0x1

    invoke-virtual {v2, v3, v0, v5, v1}, Landroid/hardware/SensorManager;->registerListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;ILandroid/os/Handler;)Z

    move-result v0

    .line 118
    if-nez v0, :cond_9

    .line 119
    const/4 v1, 0x0

    const-wide/16 v2, 0x0

    invoke-direct {p0, v1, v2, v3}, Lcom/google/android/location/collectionlib/cl;->a(ZJ)V

    .line 120
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/location/collectionlib/cl;->a:Lcom/google/android/location/collectionlib/cn;

    .line 125
    :goto_3
    monitor-exit v4

    goto :goto_0

    .line 122
    :cond_9
    sget-boolean v2, Lcom/google/android/location/i/a;->b:Z

    if-eqz v2, :cond_a

    iget-object v2, p0, Lcom/google/android/location/collectionlib/cl;->d:Lcom/google/android/location/o/a/c;

    const-string v3, "Calibration started."

    invoke-virtual {v2, v3}, Lcom/google/android/location/o/a/c;->a(Ljava/lang/String;)V

    .line 123
    :cond_a
    iget-object v2, p0, Lcom/google/android/location/collectionlib/cl;->b:Ljava/lang/Runnable;

    const-wide/16 v6, 0x1388

    invoke-virtual {v1, v2, v6, v7}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_3
.end method

.method public final b()Z
    .locals 2

    .prologue
    .line 133
    iget-object v1, p0, Lcom/google/android/location/collectionlib/cl;->h:Ljava/lang/Object;

    monitor-enter v1

    .line 134
    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/location/collectionlib/cl;->i:Z

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return v0

    .line 135
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final c()J
    .locals 4

    .prologue
    .line 143
    iget-object v1, p0, Lcom/google/android/location/collectionlib/cl;->h:Ljava/lang/Object;

    monitor-enter v1

    .line 144
    :try_start_0
    iget-wide v2, p0, Lcom/google/android/location/collectionlib/cl;->k:J

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-wide v2

    .line 145
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method
