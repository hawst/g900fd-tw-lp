.class public final Lcom/google/android/location/copresence/bg;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static e:Lcom/google/android/location/copresence/bg;


# instance fields
.field final a:Landroid/content/Context;

.field final b:Lcom/google/android/location/copresence/bf;

.field final c:Lcom/google/android/location/copresence/an;

.field d:Lcom/google/android/location/copresence/bh;


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    iput-object p1, p0, Lcom/google/android/location/copresence/bg;->a:Landroid/content/Context;

    .line 40
    new-instance v0, Lcom/google/android/location/copresence/bf;

    const/4 v1, 0x1

    invoke-static {p1}, Lcom/google/android/location/copresence/i/c;->a(Landroid/content/Context;)Lcom/google/android/location/copresence/i/c;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/google/android/location/copresence/bf;-><init>(ZLcom/google/android/location/copresence/i/c;)V

    iput-object v0, p0, Lcom/google/android/location/copresence/bg;->b:Lcom/google/android/location/copresence/bf;

    .line 42
    invoke-static {p1}, Lcom/google/android/location/copresence/an;->a(Landroid/content/Context;)Lcom/google/android/location/copresence/an;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/copresence/bg;->c:Lcom/google/android/location/copresence/an;

    .line 43
    return-void
.end method

.method public static declared-synchronized a(Landroid/content/Context;)Lcom/google/android/location/copresence/bg;
    .locals 2

    .prologue
    .line 32
    const-class v1, Lcom/google/android/location/copresence/bg;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/google/android/location/copresence/bg;->e:Lcom/google/android/location/copresence/bg;

    if-nez v0, :cond_0

    .line 33
    new-instance v0, Lcom/google/android/location/copresence/bg;

    invoke-direct {v0, p0}, Lcom/google/android/location/copresence/bg;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/google/android/location/copresence/bg;->e:Lcom/google/android/location/copresence/bg;

    .line 35
    :cond_0
    sget-object v0, Lcom/google/android/location/copresence/bg;->e:Lcom/google/android/location/copresence/bg;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 32
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method
