.class final Lcom/google/android/location/settings/m;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/widget/CompoundButton$OnCheckedChangeListener;


# instance fields
.field final synthetic a:Landroid/app/AlertDialog;

.field final synthetic b:Lcom/google/android/location/settings/LocationSettingsCheckerActivity;


# direct methods
.method constructor <init>(Lcom/google/android/location/settings/LocationSettingsCheckerActivity;Landroid/app/AlertDialog;)V
    .locals 0

    .prologue
    .line 262
    iput-object p1, p0, Lcom/google/android/location/settings/m;->b:Lcom/google/android/location/settings/LocationSettingsCheckerActivity;

    iput-object p2, p0, Lcom/google/android/location/settings/m;->a:Landroid/app/AlertDialog;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onCheckedChanged(Landroid/widget/CompoundButton;Z)V
    .locals 2

    .prologue
    .line 265
    iget-object v0, p0, Lcom/google/android/location/settings/m;->b:Lcom/google/android/location/settings/LocationSettingsCheckerActivity;

    invoke-static {v0, p2}, Lcom/google/android/location/settings/LocationSettingsCheckerActivity;->a(Lcom/google/android/location/settings/LocationSettingsCheckerActivity;Z)Z

    .line 267
    iget-object v0, p0, Lcom/google/android/location/settings/m;->a:Landroid/app/AlertDialog;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->getButton(I)Landroid/widget/Button;

    move-result-object v1

    .line 268
    if-nez p2, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/Button;->setEnabled(Z)V

    .line 270
    iget-object v0, p0, Lcom/google/android/location/settings/m;->a:Landroid/app/AlertDialog;

    const/4 v1, -0x2

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->getButton(I)Landroid/widget/Button;

    move-result-object v1

    .line 271
    if-eqz p2, :cond_1

    sget v0, Lcom/google/android/gms/p;->qe:I

    :goto_1
    invoke-virtual {v1, v0}, Landroid/widget/Button;->setText(I)V

    .line 274
    return-void

    .line 268
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 271
    :cond_1
    sget v0, Lcom/google/android/gms/p;->qd:I

    goto :goto_1
.end method
