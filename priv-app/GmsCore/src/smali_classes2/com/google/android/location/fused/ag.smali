.class final Lcom/google/android/location/fused/ag;
.super Lcom/google/android/location/fused/bg;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/location/fused/ad;


# direct methods
.method public constructor <init>(Lcom/google/android/location/fused/ad;Lcom/google/android/location/fused/a/s;Lcom/google/android/location/fused/a/r;)V
    .locals 2

    .prologue
    .line 550
    iput-object p1, p0, Lcom/google/android/location/fused/ag;->a:Lcom/google/android/location/fused/ad;

    .line 551
    new-instance v0, Lcom/google/android/location/fused/c;

    invoke-direct {v0}, Lcom/google/android/location/fused/c;-><init>()V

    invoke-static {p1}, Lcom/google/android/location/fused/ad;->a(Lcom/google/android/location/fused/ad;)Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {p0, p2, p3, v0, v1}, Lcom/google/android/location/fused/bg;-><init>(Lcom/google/android/location/fused/a/s;Lcom/google/android/location/fused/a/r;Lcom/google/android/location/fused/c;Landroid/os/Looper;)V

    .line 556
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 578
    iget-object v0, p0, Lcom/google/android/location/fused/ag;->a:Lcom/google/android/location/fused/ad;

    invoke-static {v0}, Lcom/google/android/location/fused/ad;->h(Lcom/google/android/location/fused/ad;)Lcom/google/android/location/fused/a/n;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/location/fused/a/n;->f()V

    .line 579
    return-void
.end method

.method public final a(Z)V
    .locals 5

    .prologue
    const-wide/32 v2, 0x5265c00

    .line 561
    invoke-virtual {p0}, Lcom/google/android/location/fused/ag;->e()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/location/fused/ag;->a:Lcom/google/android/location/fused/ad;

    invoke-static {v0}, Lcom/google/android/location/fused/ad;->e(Lcom/google/android/location/fused/ad;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 562
    :cond_0
    iget-object v0, p0, Lcom/google/android/location/fused/bg;->b:Lcom/google/android/location/fused/bl;

    invoke-virtual {v0}, Lcom/google/android/location/fused/bl;->e()J

    move-result-wide v0

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v0

    .line 566
    :goto_0
    iget-object v4, p0, Lcom/google/android/location/fused/ag;->a:Lcom/google/android/location/fused/ad;

    invoke-static {v4}, Lcom/google/android/location/fused/ad;->h(Lcom/google/android/location/fused/ad;)Lcom/google/android/location/fused/a/n;

    move-result-object v4

    invoke-virtual {v4, v0, v1}, Lcom/google/android/location/fused/a/n;->a(J)V

    .line 569
    iget-object v4, p0, Lcom/google/android/location/fused/ag;->a:Lcom/google/android/location/fused/ad;

    invoke-static {v4}, Lcom/google/android/location/fused/ad;->h(Lcom/google/android/location/fused/ad;)Lcom/google/android/location/fused/a/n;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/location/fused/a/n;->b()V

    .line 570
    iget-object v4, p0, Lcom/google/android/location/fused/ag;->a:Lcom/google/android/location/fused/ad;

    invoke-static {v4}, Lcom/google/android/location/fused/ad;->h(Lcom/google/android/location/fused/ad;)Lcom/google/android/location/fused/a/n;

    move-result-object v4

    if-eqz p1, :cond_2

    cmp-long v0, v0, v2

    if-gez v0, :cond_2

    const/4 v0, 0x1

    :goto_1
    iput-boolean v0, v4, Lcom/google/android/location/fused/a/m;->g:Z

    .line 572
    iget-object v0, p0, Lcom/google/android/location/fused/ag;->a:Lcom/google/android/location/fused/ad;

    invoke-static {v0}, Lcom/google/android/location/fused/ad;->h(Lcom/google/android/location/fused/ad;)Lcom/google/android/location/fused/a/n;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/location/fused/bg;->b:Lcom/google/android/location/fused/bl;

    invoke-virtual {v1}, Lcom/google/android/location/fused/bl;->f()Ljava/util/Collection;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/location/fused/a/n;->a(Ljava/util/Collection;)V

    .line 573
    iget-object v0, p0, Lcom/google/android/location/fused/ag;->a:Lcom/google/android/location/fused/ad;

    invoke-static {v0}, Lcom/google/android/location/fused/ad;->h(Lcom/google/android/location/fused/ad;)Lcom/google/android/location/fused/a/n;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/location/fused/a/n;->e()V

    .line 574
    return-void

    :cond_1
    move-wide v0, v2

    .line 564
    goto :goto_0

    .line 570
    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method
