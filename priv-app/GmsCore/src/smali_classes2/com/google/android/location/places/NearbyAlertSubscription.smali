.class public final Lcom/google/android/location/places/NearbyAlertSubscription;
.super Lcom/google/android/location/places/Subscription;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:Lcom/google/android/location/places/q;

.field private static final b:Lcom/google/android/gms/location/places/NearbyAlertRequest;


# instance fields
.field public final a:Lcom/google/android/gms/location/places/NearbyAlertRequest;

.field private final c:I

.field private final d:Lcom/google/android/gms/location/places/internal/PlacesParams;

.field private final e:Landroid/app/PendingIntent;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 36
    new-instance v0, Lcom/google/android/location/places/q;

    invoke-direct {v0}, Lcom/google/android/location/places/q;-><init>()V

    sput-object v0, Lcom/google/android/location/places/NearbyAlertSubscription;->CREATOR:Lcom/google/android/location/places/q;

    .line 39
    const/4 v0, 0x0

    invoke-static {}, Lcom/google/android/gms/location/places/PlaceFilter;->h()Lcom/google/android/gms/location/places/PlaceFilter;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/location/places/NearbyAlertRequest;->a(ILcom/google/android/gms/location/places/PlaceFilter;)Lcom/google/android/gms/location/places/NearbyAlertRequest;

    move-result-object v0

    sput-object v0, Lcom/google/android/location/places/NearbyAlertSubscription;->b:Lcom/google/android/gms/location/places/NearbyAlertRequest;

    return-void
.end method

.method constructor <init>(ILcom/google/android/gms/location/places/NearbyAlertRequest;Lcom/google/android/gms/location/places/internal/PlacesParams;Landroid/app/PendingIntent;)V
    .locals 1

    .prologue
    .line 99
    invoke-direct {p0}, Lcom/google/android/location/places/Subscription;-><init>()V

    .line 100
    iput p1, p0, Lcom/google/android/location/places/NearbyAlertSubscription;->c:I

    .line 101
    invoke-static {p2}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/location/places/NearbyAlertRequest;

    iput-object v0, p0, Lcom/google/android/location/places/NearbyAlertSubscription;->a:Lcom/google/android/gms/location/places/NearbyAlertRequest;

    .line 102
    iput-object p3, p0, Lcom/google/android/location/places/NearbyAlertSubscription;->d:Lcom/google/android/gms/location/places/internal/PlacesParams;

    .line 103
    invoke-static {p4}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/PendingIntent;

    iput-object v0, p0, Lcom/google/android/location/places/NearbyAlertSubscription;->e:Landroid/app/PendingIntent;

    .line 104
    return-void
.end method

.method public static a(Landroid/app/PendingIntent;)Lcom/google/android/location/places/NearbyAlertSubscription;
    .locals 4

    .prologue
    .line 85
    new-instance v0, Lcom/google/android/location/places/NearbyAlertSubscription;

    const/4 v1, 0x0

    sget-object v2, Lcom/google/android/location/places/NearbyAlertSubscription;->b:Lcom/google/android/gms/location/places/NearbyAlertRequest;

    sget-object v3, Lcom/google/android/gms/location/places/internal/PlacesParams;->a:Lcom/google/android/gms/location/places/internal/PlacesParams;

    invoke-direct {v0, v1, v2, v3, p0}, Lcom/google/android/location/places/NearbyAlertSubscription;-><init>(ILcom/google/android/gms/location/places/NearbyAlertRequest;Lcom/google/android/gms/location/places/internal/PlacesParams;Landroid/app/PendingIntent;)V

    return-object v0
.end method

.method public static a(Lcom/google/android/gms/location/places/NearbyAlertRequest;Lcom/google/android/gms/location/places/internal/PlacesParams;Landroid/app/PendingIntent;)Lcom/google/android/location/places/NearbyAlertSubscription;
    .locals 2

    .prologue
    .line 73
    new-instance v0, Lcom/google/android/location/places/NearbyAlertSubscription;

    const/4 v1, 0x0

    invoke-direct {v0, v1, p0, p1, p2}, Lcom/google/android/location/places/NearbyAlertSubscription;-><init>(ILcom/google/android/gms/location/places/NearbyAlertRequest;Lcom/google/android/gms/location/places/internal/PlacesParams;Landroid/app/PendingIntent;)V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/android/gms/location/places/internal/PlacesParams;
    .locals 1

    .prologue
    .line 108
    iget-object v0, p0, Lcom/google/android/location/places/NearbyAlertSubscription;->d:Lcom/google/android/gms/location/places/internal/PlacesParams;

    return-object v0
.end method

.method public final b()Lcom/google/android/gms/location/places/PlaceFilter;
    .locals 1

    .prologue
    .line 113
    iget-object v0, p0, Lcom/google/android/location/places/NearbyAlertSubscription;->a:Lcom/google/android/gms/location/places/NearbyAlertRequest;

    invoke-virtual {v0}, Lcom/google/android/gms/location/places/NearbyAlertRequest;->d()Lcom/google/android/gms/location/places/PlaceFilter;

    move-result-object v0

    return-object v0
.end method

.method public final c()Landroid/app/PendingIntent;
    .locals 1

    .prologue
    .line 118
    iget-object v0, p0, Lcom/google/android/location/places/NearbyAlertSubscription;->e:Landroid/app/PendingIntent;

    return-object v0
.end method

.method public final d()Z
    .locals 1

    .prologue
    .line 123
    const/4 v0, 0x1

    return v0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 166
    sget-object v0, Lcom/google/android/location/places/NearbyAlertSubscription;->CREATOR:Lcom/google/android/location/places/q;

    const/4 v0, 0x0

    return v0
.end method

.method public final e()I
    .locals 1

    .prologue
    .line 159
    iget v0, p0, Lcom/google/android/location/places/NearbyAlertSubscription;->c:I

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 143
    if-ne p0, p1, :cond_1

    .line 150
    :cond_0
    :goto_0
    return v0

    .line 146
    :cond_1
    instance-of v2, p1, Lcom/google/android/location/places/NearbyAlertSubscription;

    if-nez v2, :cond_2

    move v0, v1

    .line 147
    goto :goto_0

    .line 149
    :cond_2
    check-cast p1, Lcom/google/android/location/places/NearbyAlertSubscription;

    .line 150
    iget-object v2, p0, Lcom/google/android/location/places/NearbyAlertSubscription;->a:Lcom/google/android/gms/location/places/NearbyAlertRequest;

    iget-object v3, p1, Lcom/google/android/location/places/NearbyAlertSubscription;->a:Lcom/google/android/gms/location/places/NearbyAlertRequest;

    invoke-virtual {v2, v3}, Lcom/google/android/gms/location/places/NearbyAlertRequest;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/google/android/location/places/NearbyAlertSubscription;->d:Lcom/google/android/gms/location/places/internal/PlacesParams;

    iget-object v3, p1, Lcom/google/android/location/places/NearbyAlertSubscription;->d:Lcom/google/android/gms/location/places/internal/PlacesParams;

    invoke-virtual {v2, v3}, Lcom/google/android/gms/location/places/internal/PlacesParams;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/google/android/location/places/NearbyAlertSubscription;->e:Landroid/app/PendingIntent;

    iget-object v3, p1, Lcom/google/android/location/places/NearbyAlertSubscription;->e:Landroid/app/PendingIntent;

    invoke-virtual {v2, v3}, Landroid/app/PendingIntent;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    .line 138
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/location/places/NearbyAlertSubscription;->a:Lcom/google/android/gms/location/places/NearbyAlertRequest;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/location/places/NearbyAlertSubscription;->d:Lcom/google/android/gms/location/places/internal/PlacesParams;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/location/places/NearbyAlertSubscription;->e:Landroid/app/PendingIntent;

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 3
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "DefaultLocale"
        }
    .end annotation

    .prologue
    .line 129
    invoke-static {p0}, Lcom/google/android/gms/common/internal/bu;->a(Ljava/lang/Object;)Lcom/google/android/gms/common/internal/bv;

    move-result-object v0

    const-string v1, "nearbyAlertRequest"

    iget-object v2, p0, Lcom/google/android/location/places/NearbyAlertSubscription;->a:Lcom/google/android/gms/location/places/NearbyAlertRequest;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/common/internal/bv;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/android/gms/common/internal/bv;

    move-result-object v0

    const-string v1, "params"

    iget-object v2, p0, Lcom/google/android/location/places/NearbyAlertSubscription;->d:Lcom/google/android/gms/location/places/internal/PlacesParams;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/common/internal/bv;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/android/gms/common/internal/bv;

    move-result-object v0

    const-string v1, "callbackIntent"

    iget-object v2, p0, Lcom/google/android/location/places/NearbyAlertSubscription;->e:Landroid/app/PendingIntent;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/common/internal/bv;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/android/gms/common/internal/bv;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/common/internal/bv;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 173
    sget-object v0, Lcom/google/android/location/places/NearbyAlertSubscription;->CREATOR:Lcom/google/android/location/places/q;

    invoke-static {p0, p1, p2}, Lcom/google/android/location/places/q;->a(Lcom/google/android/location/places/NearbyAlertSubscription;Landroid/os/Parcel;I)V

    .line 174
    return-void
.end method
