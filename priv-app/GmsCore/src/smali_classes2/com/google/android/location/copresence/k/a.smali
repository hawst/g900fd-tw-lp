.class public final Lcom/google/android/location/copresence/k/a;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final a:I

.field final b:J

.field final c:J

.field final d:Landroid/os/Handler;

.field final e:Lcom/google/android/location/copresence/k/c;

.field final f:Ljava/util/LinkedList;

.field g:I

.field h:J

.field i:J

.field j:Z

.field private final k:Ljava/lang/Runnable;


# direct methods
.method public constructor <init>(Landroid/os/Handler;Lcom/google/android/location/copresence/k/c;JJ)V
    .locals 7

    .prologue
    const/4 v1, 0x0

    const-wide/16 v4, 0x0

    const/4 v0, 0x1

    .line 158
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    iput v1, p0, Lcom/google/android/location/copresence/k/a;->g:I

    .line 36
    iput-wide v4, p0, Lcom/google/android/location/copresence/k/a;->h:J

    .line 37
    iput-wide v4, p0, Lcom/google/android/location/copresence/k/a;->i:J

    .line 38
    iput-boolean v0, p0, Lcom/google/android/location/copresence/k/a;->j:Z

    .line 39
    new-instance v2, Lcom/google/android/location/copresence/k/b;

    invoke-direct {v2, p0}, Lcom/google/android/location/copresence/k/b;-><init>(Lcom/google/android/location/copresence/k/a;)V

    iput-object v2, p0, Lcom/google/android/location/copresence/k/a;->k:Ljava/lang/Runnable;

    .line 159
    invoke-static {p1}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 160
    invoke-static {p2}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 161
    invoke-static {v0}, Lcom/google/android/gms/common/internal/bx;->a(Z)V

    .line 162
    cmp-long v2, p3, v4

    if-ltz v2, :cond_0

    :goto_0
    invoke-static {v0}, Lcom/google/android/gms/common/internal/bx;->a(Z)V

    .line 163
    iput-object p1, p0, Lcom/google/android/location/copresence/k/a;->d:Landroid/os/Handler;

    .line 164
    iput-object p2, p0, Lcom/google/android/location/copresence/k/a;->e:Lcom/google/android/location/copresence/k/c;

    .line 165
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/google/android/location/copresence/k/a;->f:Ljava/util/LinkedList;

    .line 166
    const/4 v0, 0x3

    iput v0, p0, Lcom/google/android/location/copresence/k/a;->a:I

    .line 167
    iput-wide p3, p0, Lcom/google/android/location/copresence/k/a;->b:J

    .line 168
    iput-wide p5, p0, Lcom/google/android/location/copresence/k/a;->c:J

    .line 169
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/location/copresence/k/a;->i:J

    .line 170
    return-void

    :cond_0
    move v0, v1

    .line 162
    goto :goto_0
.end method

.method private d()V
    .locals 2

    .prologue
    .line 210
    iget-object v0, p0, Lcom/google/android/location/copresence/k/a;->d:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/location/copresence/k/a;->k:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 211
    return-void
.end method


# virtual methods
.method final a()V
    .locals 2

    .prologue
    .line 202
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/location/copresence/k/a;->j:Z

    .line 203
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/location/copresence/k/a;->g:I

    .line 204
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/location/copresence/k/a;->h:J

    .line 205
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/location/copresence/k/a;->i:J

    .line 206
    invoke-direct {p0}, Lcom/google/android/location/copresence/k/a;->d()V

    .line 207
    return-void
.end method

.method public final varargs a([Lcom/google/android/location/copresence/k/f;)V
    .locals 3

    .prologue
    .line 178
    invoke-static {p1}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 179
    array-length v0, p1

    add-int/lit8 v0, v0, -0x1

    :goto_0
    if-ltz v0, :cond_0

    .line 180
    aget-object v1, p1, v0

    .line 181
    invoke-static {v1}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 182
    iget-object v2, p0, Lcom/google/android/location/copresence/k/a;->f:Ljava/util/LinkedList;

    invoke-virtual {v2, v1}, Ljava/util/LinkedList;->addFirst(Ljava/lang/Object;)V

    .line 179
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    .line 184
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/location/copresence/k/a;->a()V

    .line 185
    return-void
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 215
    iget-object v0, p0, Lcom/google/android/location/copresence/k/a;->f:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->clear()V

    .line 216
    iget-object v0, p0, Lcom/google/android/location/copresence/k/a;->d:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/location/copresence/k/a;->k:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 217
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/location/copresence/k/a;->g:I

    .line 218
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/location/copresence/k/a;->h:J

    .line 219
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/location/copresence/k/a;->i:J

    .line 220
    return-void
.end method

.method public final varargs b([Lcom/google/android/location/copresence/k/f;)V
    .locals 4

    .prologue
    .line 193
    if-eqz p1, :cond_0

    .line 194
    array-length v1, p1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    aget-object v2, p1, v0

    .line 195
    iget-object v3, p0, Lcom/google/android/location/copresence/k/a;->f:Ljava/util/LinkedList;

    invoke-virtual {v3, v2}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 194
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 198
    :cond_0
    invoke-direct {p0}, Lcom/google/android/location/copresence/k/a;->d()V

    .line 199
    return-void
.end method

.method public final c()V
    .locals 2

    .prologue
    .line 226
    iget-object v0, p0, Lcom/google/android/location/copresence/k/a;->f:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->peek()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/copresence/k/f;

    .line 227
    iget-object v1, p0, Lcom/google/android/location/copresence/k/a;->f:Ljava/util/LinkedList;

    invoke-virtual {v1}, Ljava/util/LinkedList;->clear()V

    .line 228
    if-eqz v0, :cond_0

    .line 229
    iget-object v1, p0, Lcom/google/android/location/copresence/k/a;->f:Ljava/util/LinkedList;

    invoke-virtual {v1, v0}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 231
    :cond_0
    return-void
.end method

.method public final varargs c([Lcom/google/android/location/copresence/k/f;)V
    .locals 4
    .annotation build Landroid/annotation/TargetApi;
        value = 0xa
    .end annotation

    .prologue
    .line 236
    if-eqz p1, :cond_1

    .line 237
    array-length v1, p1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_1

    aget-object v2, p1, v0

    .line 239
    :goto_1
    iget-object v3, p0, Lcom/google/android/location/copresence/k/a;->f:Ljava/util/LinkedList;

    invoke-virtual {v3, v2}, Ljava/util/LinkedList;->lastIndexOf(Ljava/lang/Object;)I

    move-result v3

    if-lez v3, :cond_0

    .line 240
    iget-object v3, p0, Lcom/google/android/location/copresence/k/a;->f:Ljava/util/LinkedList;

    invoke-virtual {v3, v2}, Ljava/util/LinkedList;->removeLastOccurrence(Ljava/lang/Object;)Z

    goto :goto_1

    .line 237
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 244
    :cond_1
    invoke-direct {p0}, Lcom/google/android/location/copresence/k/a;->d()V

    .line 245
    return-void
.end method
