.class public final Lcom/google/android/location/os/b;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private a:Ljava/io/File;

.field private final b:J

.field private final c:Lcom/google/android/location/os/g;

.field private d:Ljava/io/DataOutputStream;

.field private e:I

.field private f:[I

.field private g:J


# direct methods
.method public constructor <init>(Ljava/io/File;J)V
    .locals 2

    .prologue
    .line 93
    new-instance v0, Lcom/google/android/location/os/c;

    invoke-direct {v0}, Lcom/google/android/location/os/c;-><init>()V

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/google/android/location/os/b;-><init>(Ljava/io/File;JLcom/google/android/location/os/g;)V

    .line 100
    return-void
.end method

.method private constructor <init>(Ljava/io/File;JLcom/google/android/location/os/g;)V
    .locals 4

    .prologue
    .line 79
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 66
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/location/os/b;->d:Ljava/io/DataOutputStream;

    .line 68
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/location/os/b;->e:I

    .line 70
    invoke-static {}, Lcom/google/android/location/os/au;->values()[Lcom/google/android/location/os/au;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/google/android/location/os/b;->f:[I

    .line 75
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/google/android/location/os/b;->g:J

    .line 80
    iput-object p1, p0, Lcom/google/android/location/os/b;->a:Ljava/io/File;

    .line 81
    iput-wide p2, p0, Lcom/google/android/location/os/b;->b:J

    .line 82
    iput-object p4, p0, Lcom/google/android/location/os/b;->c:Lcom/google/android/location/os/g;

    .line 84
    sget-boolean v0, Lcom/google/android/location/i/a;->b:Z

    if-eqz v0, :cond_0

    const-string v0, "CompactLogger"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Log file directory is "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/location/o/a/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 85
    :cond_0
    invoke-direct {p0}, Lcom/google/android/location/os/b;->a()V

    .line 86
    return-void
.end method

.method private a()V
    .locals 10

    .prologue
    const/4 v2, 0x0

    const/4 v4, 0x0

    .line 149
    iget-object v0, p0, Lcom/google/android/location/os/b;->a:Ljava/io/File;

    if-nez v0, :cond_1

    .line 150
    sget-boolean v0, Lcom/google/android/location/i/a;->e:Z

    if-eqz v0, :cond_0

    const-string v0, "CompactLogger"

    const-string v1, "dir is null"

    invoke-static {v0, v1}, Lcom/google/android/location/o/a/a;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 189
    :cond_0
    :goto_0
    return-void

    :cond_1
    move v3, v2

    move-object v0, v4

    .line 154
    :goto_1
    const/16 v1, 0x8

    if-ge v3, v1, :cond_8

    .line 155
    new-instance v1, Ljava/io/File;

    iget-object v5, p0, Lcom/google/android/location/os/b;->a:Ljava/io/File;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ".clog"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v1, v5, v6}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 156
    invoke-virtual {v1}, Ljava/io/File;->length()J

    move-result-wide v6

    const-wide/16 v8, 0x0

    cmp-long v5, v6, v8

    if-nez v5, :cond_3

    .line 165
    :goto_2
    :try_start_0
    iget-object v0, p0, Lcom/google/android/location/os/b;->d:Ljava/io/DataOutputStream;

    if-eqz v0, :cond_2

    .line 166
    iget-object v0, p0, Lcom/google/android/location/os/b;->d:Ljava/io/DataOutputStream;

    invoke-virtual {v0}, Ljava/io/DataOutputStream;->close()V

    .line 168
    :cond_2
    new-instance v0, Ljava/io/DataOutputStream;

    new-instance v3, Ljava/io/BufferedOutputStream;

    new-instance v5, Ljava/io/FileOutputStream;

    invoke-direct {v5, v1}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    invoke-direct {v3, v5}, Ljava/io/BufferedOutputStream;-><init>(Ljava/io/OutputStream;)V

    invoke-direct {v0, v3}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    iput-object v0, p0, Lcom/google/android/location/os/b;->d:Ljava/io/DataOutputStream;

    .line 170
    iget-object v0, p0, Lcom/google/android/location/os/b;->d:Ljava/io/DataOutputStream;

    const v3, -0x1ddad98d

    invoke-virtual {v0, v3}, Ljava/io/DataOutputStream;->writeInt(I)V

    .line 171
    iget-object v0, p0, Lcom/google/android/location/os/b;->d:Ljava/io/DataOutputStream;

    const/4 v3, 0x3

    invoke-virtual {v0, v3}, Ljava/io/DataOutputStream;->writeInt(I)V

    .line 172
    iget-object v0, p0, Lcom/google/android/location/os/b;->d:Ljava/io/DataOutputStream;

    iget-wide v6, p0, Lcom/google/android/location/os/b;->b:J

    invoke-virtual {v0, v6, v7}, Ljava/io/DataOutputStream;->writeLong(J)V

    .line 178
    iget-object v0, p0, Lcom/google/android/location/os/b;->d:Ljava/io/DataOutputStream;

    invoke-static {}, Lcom/google/android/location/os/au;->values()[Lcom/google/android/location/os/au;

    move-result-object v3

    array-length v3, v3

    invoke-virtual {v0, v3}, Ljava/io/DataOutputStream;->writeInt(I)V

    .line 179
    invoke-static {}, Lcom/google/android/location/os/au;->values()[Lcom/google/android/location/os/au;

    move-result-object v3

    array-length v5, v3

    move v0, v2

    :goto_3
    if-ge v0, v5, :cond_6

    aget-object v2, v3, v0

    .line 180
    iget-object v6, p0, Lcom/google/android/location/os/b;->d:Ljava/io/DataOutputStream;

    invoke-virtual {v2}, Lcom/google/android/location/os/au;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    invoke-virtual {v6, v2}, Ljava/io/DataOutputStream;->writeShort(I)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 179
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 160
    :cond_3
    if-eqz v0, :cond_4

    invoke-virtual {v0}, Ljava/io/File;->lastModified()J

    move-result-wide v6

    invoke-virtual {v1}, Ljava/io/File;->lastModified()J

    move-result-wide v8

    cmp-long v5, v6, v8

    if-lez v5, :cond_5

    :cond_4
    move-object v0, v1

    .line 154
    :cond_5
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto/16 :goto_1

    .line 182
    :cond_6
    const-wide/16 v2, -0x1

    :try_start_1
    iput-wide v2, p0, Lcom/google/android/location/os/b;->g:J
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    .line 188
    :cond_7
    :goto_4
    sget-boolean v0, Lcom/google/android/location/i/a;->b:Z

    if-eqz v0, :cond_0

    const-string v0, "CompactLogger"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "pickNextFile "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/location/o/a/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 183
    :catch_0
    move-exception v0

    .line 184
    iput-object v4, p0, Lcom/google/android/location/os/b;->a:Ljava/io/File;

    .line 185
    iput-object v4, p0, Lcom/google/android/location/os/b;->d:Ljava/io/DataOutputStream;

    .line 186
    sget-boolean v2, Lcom/google/android/location/i/a;->e:Z

    if-eqz v2, :cond_7

    const-string v2, "CompactLogger"

    const-string v3, ""

    invoke-static {v2, v3, v0}, Lcom/google/android/location/o/a/a;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_4

    :cond_8
    move-object v1, v0

    goto/16 :goto_2
.end method


# virtual methods
.method final a(Lcom/google/android/location/os/au;JIIILjava/lang/String;)V
    .locals 6

    .prologue
    const/16 v4, 0x40

    .line 113
    iget-object v0, p0, Lcom/google/android/location/os/b;->d:Ljava/io/DataOutputStream;

    if-nez v0, :cond_1

    .line 114
    sget-boolean v0, Lcom/google/android/location/i/a;->e:Z

    if-eqz v0, :cond_0

    const-string v0, "CompactLogger"

    const-string v1, "logStream is null"

    invoke-static {v0, v1}, Lcom/google/android/location/o/a/a;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 143
    :cond_0
    :goto_0
    return-void

    .line 118
    :cond_1
    :try_start_0
    iget-object v0, p0, Lcom/google/android/location/os/b;->f:[I

    invoke-virtual {p1}, Lcom/google/android/location/os/au;->ordinal()I

    move-result v1

    aget v2, v0, v1

    add-int/lit8 v2, v2, 0x1

    aput v2, v0, v1

    .line 119
    iget-wide v0, p0, Lcom/google/android/location/os/b;->g:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-nez v0, :cond_2

    .line 120
    iget-object v0, p0, Lcom/google/android/location/os/b;->d:Ljava/io/DataOutputStream;

    invoke-virtual {v0, p2, p3}, Ljava/io/DataOutputStream;->writeLong(J)V

    .line 121
    iput-wide p2, p0, Lcom/google/android/location/os/b;->g:J

    .line 123
    :cond_2
    iget-object v0, p0, Lcom/google/android/location/os/b;->d:Ljava/io/DataOutputStream;

    invoke-virtual {p1}, Lcom/google/android/location/os/au;->ordinal()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/io/DataOutputStream;->writeByte(I)V

    .line 124
    iget-object v0, p0, Lcom/google/android/location/os/b;->d:Ljava/io/DataOutputStream;

    iget-wide v2, p0, Lcom/google/android/location/os/b;->g:J

    sub-long v2, p2, v2

    long-to-int v1, v2

    invoke-virtual {v0, v1}, Ljava/io/DataOutputStream;->writeInt(I)V

    .line 125
    iput-wide p2, p0, Lcom/google/android/location/os/b;->g:J

    .line 126
    iget-object v0, p0, Lcom/google/android/location/os/b;->d:Ljava/io/DataOutputStream;

    invoke-virtual {v0, p4}, Ljava/io/DataOutputStream;->writeInt(I)V

    .line 127
    iget-object v0, p0, Lcom/google/android/location/os/b;->d:Ljava/io/DataOutputStream;

    invoke-virtual {v0, p5}, Ljava/io/DataOutputStream;->writeInt(I)V

    .line 128
    iget-object v0, p0, Lcom/google/android/location/os/b;->d:Ljava/io/DataOutputStream;

    invoke-virtual {v0, p6}, Ljava/io/DataOutputStream;->writeInt(I)V

    .line 129
    if-nez p7, :cond_4

    .line 130
    const-string p7, ""

    .line 134
    :cond_3
    :goto_1
    iget-object v0, p0, Lcom/google/android/location/os/b;->d:Ljava/io/DataOutputStream;

    invoke-virtual {v0, p7}, Ljava/io/DataOutputStream;->writeUTF(Ljava/lang/String;)V

    .line 135
    iget v0, p0, Lcom/google/android/location/os/b;->e:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/location/os/b;->e:I

    .line 136
    iget v0, p0, Lcom/google/android/location/os/b;->e:I

    const/16 v1, 0x2000

    if-ne v0, v1, :cond_0

    .line 137
    invoke-direct {p0}, Lcom/google/android/location/os/b;->a()V

    .line 138
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/location/os/b;->e:I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 140
    :catch_0
    move-exception v0

    .line 141
    sget-boolean v1, Lcom/google/android/location/i/a;->e:Z

    if-eqz v1, :cond_0

    const-string v1, "CompactLogger"

    const-string v2, ""

    invoke-static {v1, v2, v0}, Lcom/google/android/location/o/a/a;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 131
    :cond_4
    :try_start_1
    invoke-virtual {p7}, Ljava/lang/String;->length()I

    move-result v0

    if-le v0, v4, :cond_3

    .line 132
    const/4 v0, 0x0

    const/16 v1, 0x40

    invoke-virtual {p7, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    move-result-object p7

    goto :goto_1
.end method

.method public final a(Ljava/io/PrintWriter;Lcom/google/android/location/os/at;)V
    .locals 25

    .prologue
    .line 195
    sget-boolean v2, Lcom/google/android/location/i/a;->c:Z

    if-eqz v2, :cond_0

    const-string v2, "CompactLogger"

    const-string v3, "dump"

    invoke-static {v2, v3}, Lcom/google/android/location/o/a/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 196
    :cond_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/location/os/b;->a:Ljava/io/File;

    if-nez v2, :cond_2

    .line 197
    sget-boolean v2, Lcom/google/android/location/i/a;->e:Z

    if-eqz v2, :cond_1

    const-string v2, "CompactLogger"

    const-string v3, "dir is null"

    invoke-static {v2, v3}, Lcom/google/android/location/o/a/a;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 357
    :cond_1
    :goto_0
    return-void

    .line 202
    :cond_2
    invoke-interface/range {p2 .. p2}, Lcom/google/android/location/os/at;->a()J

    move-result-wide v2

    const-wide/16 v4, 0x9c4

    add-long v12, v2, v4

    .line 204
    const-string v2, "#### com.google.android.location.utils.logging.CompactLogger Start ####"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 207
    :try_start_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/location/os/b;->a:Ljava/io/File;

    new-instance v3, Lcom/google/android/location/os/d;

    move-object/from16 v0, p0

    invoke-direct {v3, v0}, Lcom/google/android/location/os/d;-><init>(Lcom/google/android/location/os/b;)V

    invoke-virtual {v2, v3}, Ljava/io/File;->listFiles(Ljava/io/FilenameFilter;)[Ljava/io/File;

    move-result-object v11

    .line 215
    new-instance v2, Lcom/google/android/location/os/e;

    move-object/from16 v0, p0

    invoke-direct {v2, v0}, Lcom/google/android/location/os/e;-><init>(Lcom/google/android/location/os/b;)V

    invoke-static {v11, v2}, Ljava/util/Arrays;->sort([Ljava/lang/Object;Ljava/util/Comparator;)V

    .line 224
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/location/os/b;->d:Ljava/io/DataOutputStream;

    if-eqz v2, :cond_3

    .line 225
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/location/os/b;->d:Ljava/io/DataOutputStream;

    invoke-virtual {v2}, Ljava/io/DataOutputStream;->flush()V

    .line 228
    :cond_3
    new-instance v14, Ljava/io/DataOutputStream;

    new-instance v2, Ljava/io/BufferedOutputStream;

    new-instance v3, Ljava/util/zip/DeflaterOutputStream;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/location/os/b;->c:Lcom/google/android/location/os/g;

    new-instance v5, Lcom/google/android/location/os/h;

    move-object/from16 v0, p1

    invoke-direct {v5, v0}, Lcom/google/android/location/os/h;-><init>(Ljava/io/Writer;)V

    invoke-interface {v4, v5}, Lcom/google/android/location/os/g;->a(Ljava/io/Writer;)Ljava/io/OutputStream;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/util/zip/DeflaterOutputStream;-><init>(Ljava/io/OutputStream;)V

    invoke-direct {v2, v3}, Ljava/io/BufferedOutputStream;-><init>(Ljava/io/OutputStream;)V

    invoke-direct {v14, v2}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 232
    const/16 v2, 0x2000

    new-array v15, v2, [B

    .line 233
    const/16 v2, 0x2000

    new-array v0, v2, [I

    move-object/from16 v16, v0

    .line 234
    const/4 v2, 0x3

    const/16 v3, 0x2000

    filled-new-array {v2, v3}, [I

    move-result-object v2

    sget-object v3, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    invoke-static {v3, v2}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;[I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [[I

    .line 235
    const/16 v3, 0x2000

    new-array v0, v3, [Ljava/lang/String;

    move-object/from16 v17, v0

    .line 245
    invoke-static {}, Lcom/google/android/location/os/au;->values()[Lcom/google/android/location/os/au;

    move-result-object v3

    array-length v3, v3

    new-array v0, v3, [I

    move-object/from16 v18, v0

    .line 246
    const/16 v3, 0x2000

    new-array v0, v3, [I

    move-object/from16 v19, v0

    .line 248
    const v3, -0x1ddad98d

    invoke-virtual {v14, v3}, Ljava/io/DataOutputStream;->writeInt(I)V

    .line 249
    const/4 v3, 0x3

    invoke-virtual {v14, v3}, Ljava/io/DataOutputStream;->writeInt(I)V

    .line 252
    invoke-static {}, Lcom/google/android/location/os/au;->values()[Lcom/google/android/location/os/au;

    move-result-object v3

    array-length v3, v3

    invoke-virtual {v14, v3}, Ljava/io/DataOutputStream;->writeInt(I)V

    .line 253
    invoke-static {}, Lcom/google/android/location/os/au;->values()[Lcom/google/android/location/os/au;

    move-result-object v4

    array-length v5, v4

    const/4 v3, 0x0

    :goto_1
    if-ge v3, v5, :cond_4

    aget-object v6, v4, v3

    .line 254
    invoke-virtual {v6}, Lcom/google/android/location/os/au;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->hashCode()I

    move-result v6

    invoke-virtual {v14, v6}, Ljava/io/DataOutputStream;->writeShort(I)V

    .line 253
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 256
    :cond_4
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/location/os/b;->f:[I

    array-length v5, v4

    const/4 v3, 0x0

    :goto_2
    if-ge v3, v5, :cond_5

    aget v6, v4, v3

    .line 257
    invoke-virtual {v14, v6}, Ljava/io/DataOutputStream;->writeInt(I)V

    .line 256
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    .line 261
    :cond_5
    array-length v3, v11

    invoke-virtual {v14, v3}, Ljava/io/DataOutputStream;->writeInt(I)V

    .line 262
    array-length v0, v11

    move/from16 v20, v0

    const/4 v3, 0x0

    move v10, v3

    :goto_3
    move/from16 v0, v20

    if-ge v10, v0, :cond_6

    aget-object v3, v11, v10

    .line 263
    invoke-interface/range {p2 .. p2}, Lcom/google/android/location/os/at;->a()J

    move-result-wide v4

    cmp-long v4, v4, v12

    if-lez v4, :cond_7

    .line 264
    sget-boolean v2, Lcom/google/android/location/i/a;->d:Z

    if-eqz v2, :cond_6

    const-string v2, "CompactLogger"

    const-string v3, "Timeout"

    invoke-static {v2, v3}, Lcom/google/android/location/o/a/a;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 350
    :cond_6
    invoke-virtual {v14}, Ljava/io/DataOutputStream;->close()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 355
    :goto_4
    invoke-virtual/range {p1 .. p1}, Ljava/io/PrintWriter;->println()V

    .line 356
    const-string v2, "#### com.google.android.location.utils.logging.CompactLogger End ####"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 267
    :cond_7
    :try_start_1
    sget-boolean v4, Lcom/google/android/location/i/a;->b:Z

    if-eqz v4, :cond_8

    const-string v4, "CompactLogger"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Dump file "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "..."

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/android/location/o/a/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 270
    :cond_8
    new-instance v21, Ljava/io/DataInputStream;

    new-instance v4, Ljava/io/BufferedInputStream;

    new-instance v5, Ljava/io/FileInputStream;

    invoke-direct {v5, v3}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    invoke-direct {v4, v5}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;)V

    move-object/from16 v0, v21

    invoke-direct {v0, v4}, Ljava/io/DataInputStream;-><init>(Ljava/io/InputStream;)V

    .line 272
    const/4 v8, 0x0

    .line 273
    const-wide/16 v6, -0x1

    .line 274
    const/4 v3, 0x0

    new-array v3, v3, [S
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 275
    const-wide/16 v4, -0x1

    .line 277
    :try_start_2
    invoke-virtual/range {v21 .. v21}, Ljava/io/DataInputStream;->readInt()I

    move-result v9

    const v22, -0x1ddad98d

    move/from16 v0, v22

    if-eq v9, v0, :cond_9

    .line 278
    new-instance v9, Ljava/io/EOFException;

    invoke-direct {v9}, Ljava/io/EOFException;-><init>()V

    throw v9
    :try_end_2
    .catch Ljava/io/EOFException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    :catch_0
    move-exception v9

    move-wide/from16 v23, v6

    move-object v6, v3

    move v7, v8

    move-wide/from16 v8, v23

    .line 300
    :goto_5
    :try_start_3
    invoke-virtual/range {v21 .. v21}, Ljava/io/DataInputStream;->close()V

    .line 303
    const/4 v3, -0x1

    move-object/from16 v0, v18

    invoke-static {v0, v3}, Ljava/util/Arrays;->fill([II)V

    .line 304
    add-int/lit8 v3, v7, -0x1

    :goto_6
    if-ltz v3, :cond_d

    .line 305
    aget-byte v21, v15, v3

    .line 306
    aget v22, v18, v21

    aput v22, v19, v3

    .line 307
    aput v3, v18, v21
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1

    .line 304
    add-int/lit8 v3, v3, -0x1

    goto :goto_6

    .line 280
    :cond_9
    :try_start_4
    invoke-virtual/range {v21 .. v21}, Ljava/io/DataInputStream;->readInt()I

    move-result v9

    const/16 v22, 0x3

    move/from16 v0, v22

    if-eq v9, v0, :cond_a

    .line 281
    new-instance v9, Ljava/io/EOFException;

    invoke-direct {v9}, Ljava/io/EOFException;-><init>()V

    throw v9
    :try_end_4
    .catch Ljava/io/EOFException; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_1

    .line 351
    :catch_1
    move-exception v2

    move-object/from16 v0, p1

    invoke-virtual {v2, v0}, Ljava/lang/Exception;->printStackTrace(Ljava/io/PrintWriter;)V

    goto/16 :goto_4

    .line 283
    :cond_a
    :try_start_5
    invoke-virtual/range {v21 .. v21}, Ljava/io/DataInputStream;->readLong()J

    move-result-wide v6

    .line 284
    invoke-virtual/range {v21 .. v21}, Ljava/io/DataInputStream;->readInt()I

    move-result v9

    new-array v3, v9, [S

    .line 285
    const/4 v9, 0x0

    :goto_7
    array-length v0, v3

    move/from16 v22, v0

    move/from16 v0, v22

    if-ge v9, v0, :cond_b

    .line 286
    invoke-virtual/range {v21 .. v21}, Ljava/io/DataInputStream;->readShort()S

    move-result v22

    aput-short v22, v3, v9

    .line 285
    add-int/lit8 v9, v9, 0x1

    goto :goto_7

    .line 288
    :cond_b
    invoke-virtual/range {v21 .. v21}, Ljava/io/DataInputStream;->readLong()J

    move-result-wide v4

    .line 289
    :goto_8
    const/16 v9, 0x2000

    if-ge v8, v9, :cond_c

    .line 290
    invoke-virtual/range {v21 .. v21}, Ljava/io/DataInputStream;->readByte()B

    move-result v9

    aput-byte v9, v15, v8

    .line 291
    invoke-virtual/range {v21 .. v21}, Ljava/io/DataInputStream;->readInt()I

    move-result v9

    aput v9, v16, v8

    .line 292
    const/4 v9, 0x0

    aget-object v9, v2, v9

    invoke-virtual/range {v21 .. v21}, Ljava/io/DataInputStream;->readInt()I

    move-result v22

    aput v22, v9, v8

    .line 293
    const/4 v9, 0x1

    aget-object v9, v2, v9

    invoke-virtual/range {v21 .. v21}, Ljava/io/DataInputStream;->readInt()I

    move-result v22

    aput v22, v9, v8

    .line 294
    const/4 v9, 0x2

    aget-object v9, v2, v9

    invoke-virtual/range {v21 .. v21}, Ljava/io/DataInputStream;->readInt()I

    move-result v22

    aput v22, v9, v8

    .line 295
    invoke-virtual/range {v21 .. v21}, Ljava/io/DataInputStream;->readUTF()Ljava/lang/String;

    move-result-object v9

    aput-object v9, v17, v8
    :try_end_5
    .catch Ljava/io/EOFException; {:try_start_5 .. :try_end_5} :catch_0
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_1

    .line 289
    add-int/lit8 v8, v8, 0x1

    goto :goto_8

    :cond_c
    move-wide/from16 v23, v6

    move-object v6, v3

    move v7, v8

    move-wide/from16 v8, v23

    .line 299
    goto/16 :goto_5

    .line 311
    :cond_d
    const v3, -0x1ddad98d

    :try_start_6
    invoke-virtual {v14, v3}, Ljava/io/DataOutputStream;->writeInt(I)V

    .line 312
    invoke-virtual {v14, v8, v9}, Ljava/io/DataOutputStream;->writeLong(J)V

    .line 313
    array-length v3, v6

    invoke-virtual {v14, v3}, Ljava/io/DataOutputStream;->writeInt(I)V

    .line 314
    array-length v8, v6

    const/4 v3, 0x0

    :goto_9
    if-ge v3, v8, :cond_e

    aget-short v9, v6, v3

    .line 315
    invoke-virtual {v14, v9}, Ljava/io/DataOutputStream;->writeShort(I)V

    .line 314
    add-int/lit8 v3, v3, 0x1

    goto :goto_9

    .line 317
    :cond_e
    invoke-virtual {v14, v7}, Ljava/io/DataOutputStream;->writeInt(I)V

    .line 320
    const/4 v3, 0x0

    :goto_a
    if-ge v3, v7, :cond_f

    .line 321
    aget-byte v6, v15, v3

    invoke-virtual {v14, v6}, Ljava/io/DataOutputStream;->writeByte(I)V

    .line 320
    add-int/lit8 v3, v3, 0x1

    goto :goto_a

    .line 325
    :cond_f
    invoke-virtual {v14, v4, v5}, Ljava/io/DataOutputStream;->writeLong(J)V

    .line 326
    const/4 v3, 0x0

    move v4, v3

    :goto_b
    const/4 v3, 0x4

    if-ge v4, v3, :cond_11

    .line 327
    const/4 v3, 0x0

    :goto_c
    if-ge v3, v7, :cond_10

    .line 328
    aget v5, v16, v3

    mul-int/lit8 v6, v4, 0x8

    ushr-int/2addr v5, v6

    invoke-virtual {v14, v5}, Ljava/io/DataOutputStream;->writeByte(I)V

    .line 327
    add-int/lit8 v3, v3, 0x1

    goto :goto_c

    .line 326
    :cond_10
    add-int/lit8 v3, v4, 0x1

    move v4, v3

    goto :goto_b

    .line 334
    :cond_11
    const/4 v3, 0x0

    move v7, v3

    :goto_d
    invoke-static {}, Lcom/google/android/location/os/au;->values()[Lcom/google/android/location/os/au;

    move-result-object v3

    array-length v3, v3

    if-ge v7, v3, :cond_16

    .line 335
    const/4 v3, 0x0

    move v6, v3

    :goto_e
    const/4 v3, 0x3

    if-ge v6, v3, :cond_14

    .line 336
    const/4 v3, 0x0

    move v5, v3

    :goto_f
    const/4 v3, 0x4

    if-ge v5, v3, :cond_13

    .line 337
    const/4 v4, 0x0

    .line 338
    aget v3, v18, v7

    :goto_10
    const/4 v8, -0x1

    if-eq v3, v8, :cond_12

    .line 339
    aget-object v8, v2, v6

    aget v8, v8, v3

    sub-int v4, v8, v4

    mul-int/lit8 v8, v5, 0x8

    ushr-int/2addr v4, v8

    invoke-virtual {v14, v4}, Ljava/io/DataOutputStream;->writeByte(I)V

    .line 340
    aget-object v4, v2, v6

    aget v4, v4, v3

    .line 338
    aget v3, v19, v3

    goto :goto_10

    .line 336
    :cond_12
    add-int/lit8 v3, v5, 0x1

    move v5, v3

    goto :goto_f

    .line 335
    :cond_13
    add-int/lit8 v3, v6, 0x1

    move v6, v3

    goto :goto_e

    .line 344
    :cond_14
    aget v3, v18, v7

    :goto_11
    const/4 v4, -0x1

    if-eq v3, v4, :cond_15

    .line 345
    aget-object v4, v17, v3

    invoke-virtual {v14, v4}, Ljava/io/DataOutputStream;->writeUTF(Ljava/lang/String;)V

    .line 344
    aget v3, v19, v3
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_1

    goto :goto_11

    .line 334
    :cond_15
    add-int/lit8 v3, v7, 0x1

    move v7, v3

    goto :goto_d

    .line 262
    :cond_16
    add-int/lit8 v3, v10, 0x1

    move v10, v3

    goto/16 :goto_3
.end method
