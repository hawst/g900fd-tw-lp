.class public final Lcom/google/android/location/copresence/t;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Landroid/content/Intent;

.field final synthetic b:Landroid/content/Context;

.field final synthetic c:Lcom/google/android/location/copresence/r;


# direct methods
.method public constructor <init>(Lcom/google/android/location/copresence/r;Landroid/content/Intent;Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 491
    iput-object p1, p0, Lcom/google/android/location/copresence/t;->c:Lcom/google/android/location/copresence/r;

    iput-object p2, p0, Lcom/google/android/location/copresence/t;->a:Landroid/content/Intent;

    iput-object p3, p0, Lcom/google/android/location/copresence/t;->b:Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 18

    .prologue
    .line 494
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/location/copresence/t;->c:Lcom/google/android/location/copresence/r;

    iget-object v2, v2, Lcom/google/android/location/copresence/r;->b:Lcom/google/android/location/copresence/j;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/location/copresence/t;->a:Landroid/content/Intent;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/location/copresence/t;->b:Landroid/content/Context;

    iget-object v10, v2, Lcom/google/android/location/copresence/j;->j:Lcom/google/android/location/copresence/l/h;

    if-eqz v3, :cond_2

    invoke-virtual {v3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v11

    if-eqz v11, :cond_2

    const/4 v2, 0x3

    invoke-static {v2}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v2

    if-eqz v2, :cond_0

    const-string v2, "GcmHandler: Handling Gcm Message"

    invoke-static {v2}, Lcom/google/android/location/copresence/ag;->b(Ljava/lang/String;)I

    :cond_0
    invoke-static {v4}, Lcom/google/android/gms/gcm/ag;->a(Landroid/content/Context;)Lcom/google/android/gms/gcm/ag;

    invoke-static {v3}, Lcom/google/android/gms/gcm/ag;->a(Landroid/content/Intent;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v11}, Landroid/os/Bundle;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_2

    const-string v3, "gcm"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    const/4 v2, 0x3

    invoke-static {v2}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v2

    if-eqz v2, :cond_1

    const-string v2, "GcmHandler: Gcm Message has a valid type"

    invoke-static {v2}, Lcom/google/android/location/copresence/ag;->b(Ljava/lang/String;)I

    :cond_1
    invoke-static {v11}, Lcom/google/android/location/copresence/l/h;->a(Landroid/os/Bundle;)Lcom/google/ac/b/c/co;

    move-result-object v12

    if-nez v12, :cond_3

    const/4 v2, 0x3

    invoke-static {v2}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v2

    if-eqz v2, :cond_2

    const-string v2, "GcmHandler: Received GCM ping which did not contain a copresence message"

    invoke-static {v2}, Lcom/google/android/location/copresence/ag;->b(Ljava/lang/String;)I

    .line 495
    :cond_2
    :goto_0
    return-void

    .line 494
    :cond_3
    const-string v2, "OBFUSCATED_GAIA_ID"

    invoke-virtual {v11, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_4

    const/4 v2, 0x0

    :goto_1
    if-nez v2, :cond_6

    const/4 v2, 0x3

    invoke-static {v2}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v2

    if-eqz v2, :cond_2

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "GcmHandler: Can not handle message: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, v12, Lcom/google/ac/b/c/co;->a:Ljava/lang/Integer;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/location/copresence/ag;->b(Ljava/lang/String;)I

    goto :goto_0

    :cond_4
    if-eqz v12, :cond_5

    iget-object v3, v10, Lcom/google/android/location/copresence/l/h;->b:Lcom/google/android/location/copresence/a/b;

    invoke-virtual {v3, v2}, Lcom/google/android/location/copresence/a/b;->b(Ljava/lang/String;)Lcom/google/android/location/copresence/a/a;

    move-result-object v2

    if-eqz v2, :cond_5

    iget-object v2, v12, Lcom/google/ac/b/c/co;->a:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    :cond_5
    const/4 v2, 0x1

    goto :goto_1

    :cond_6
    invoke-static {}, Lcom/google/android/location/copresence/f/b;->b()Lcom/google/ac/b/c/g;

    move-result-object v2

    iget-object v2, v2, Lcom/google/ac/b/c/g;->a:Lcom/google/ac/b/c/l;

    iget-object v2, v2, Lcom/google/ac/b/c/l;->a:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_9

    iget-object v2, v12, Lcom/google/ac/b/c/co;->i:Lcom/google/ac/b/c/cp;

    if-eqz v2, :cond_8

    iget-object v2, v12, Lcom/google/ac/b/c/co;->i:Lcom/google/ac/b/c/cp;

    iget-object v2, v2, Lcom/google/ac/b/c/cp;->c:Lcom/google/ac/b/c/as;

    if-eqz v2, :cond_8

    iget-object v2, v10, Lcom/google/android/location/copresence/l/h;->f:Lcom/google/android/location/copresence/l/e;

    invoke-virtual {v2}, Lcom/google/android/location/copresence/l/e;->a()Lcom/google/ac/b/c/as;

    move-result-object v8

    if-eqz v8, :cond_8

    iget-object v2, v12, Lcom/google/ac/b/c/co;->i:Lcom/google/ac/b/c/cp;

    iget-object v4, v2, Lcom/google/ac/b/c/cp;->c:Lcom/google/ac/b/c/as;

    invoke-static {}, Lcom/google/android/location/copresence/f/b;->b()Lcom/google/ac/b/c/g;

    move-result-object v2

    iget-object v2, v2, Lcom/google/ac/b/c/g;->a:Lcom/google/ac/b/c/l;

    iget-object v2, v2, Lcom/google/ac/b/c/l;->b:Ljava/lang/Double;

    invoke-virtual {v2}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v14

    iget-object v2, v4, Lcom/google/ac/b/c/as;->a:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    int-to-double v2, v2

    const-wide v6, 0x3e7ad7f29abcaf48L    # 1.0E-7

    mul-double/2addr v2, v6

    iget-object v4, v4, Lcom/google/ac/b/c/as;->b:Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    int-to-double v4, v4

    const-wide v6, 0x3e7ad7f29abcaf48L    # 1.0E-7

    mul-double/2addr v4, v6

    iget-object v6, v8, Lcom/google/ac/b/c/as;->a:Ljava/lang/Integer;

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v6

    int-to-double v6, v6

    const-wide v16, 0x3e7ad7f29abcaf48L    # 1.0E-7

    mul-double v6, v6, v16

    iget-object v8, v8, Lcom/google/ac/b/c/as;->b:Ljava/lang/Integer;

    invoke-virtual {v8}, Ljava/lang/Integer;->intValue()I

    move-result v8

    int-to-double v8, v8

    const-wide v16, 0x3e7ad7f29abcaf48L    # 1.0E-7

    mul-double v8, v8, v16

    invoke-static/range {v2 .. v9}, Lcom/google/android/location/g/d;->a(DDDD)D

    move-result-wide v2

    cmpg-double v2, v2, v14

    if-gez v2, :cond_7

    const/4 v2, 0x1

    :goto_2
    if-nez v2, :cond_8

    const/4 v2, 0x1

    :goto_3
    if-eqz v2, :cond_9

    const/4 v2, 0x3

    invoke-static {v2}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v2

    if-eqz v2, :cond_2

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "GcmHandler: Server sent message for wrong location: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, v12, Lcom/google/ac/b/c/co;->a:Ljava/lang/Integer;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/location/copresence/ag;->b(Ljava/lang/String;)I

    goto/16 :goto_0

    :cond_7
    const/4 v2, 0x0

    goto :goto_2

    :cond_8
    const/4 v2, 0x0

    goto :goto_3

    :cond_9
    const-string v2, "OBFUSCATED_GAIA_ID"

    invoke-virtual {v11, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x3

    invoke-static {v3}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v3

    if-eqz v3, :cond_a

    const-string v3, "GcmHandler: Received valid PushMessage"

    invoke-static {v3}, Lcom/google/android/location/copresence/ag;->b(Ljava/lang/String;)I

    :cond_a
    iget-object v3, v10, Lcom/google/android/location/copresence/l/h;->b:Lcom/google/android/location/copresence/a/b;

    invoke-virtual {v3, v2}, Lcom/google/android/location/copresence/a/b;->b(Ljava/lang/String;)Lcom/google/android/location/copresence/a/a;

    move-result-object v2

    if-eqz v2, :cond_2

    invoke-static {}, Lcom/google/android/location/copresence/debug/d;->a()V

    iget-object v3, v10, Lcom/google/android/location/copresence/l/h;->a:Landroid/content/Context;

    if-eqz v3, :cond_b

    iget-object v3, v10, Lcom/google/android/location/copresence/l/h;->a:Landroid/content/Context;

    invoke-static {v3, v12, v2}, Lcom/google/android/location/copresence/debug/CopresDatabaseHelper;->a(Landroid/content/Context;Lcom/google/ac/b/c/co;Lcom/google/android/location/copresence/a/a;)V

    :cond_b
    iget-object v3, v12, Lcom/google/ac/b/c/co;->a:Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    sparse-switch v3, :sswitch_data_0

    const/4 v2, 0x6

    invoke-static {v2}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v2

    if-eqz v2, :cond_2

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "GcmHandler: Invalid PushMessage type: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, v12, Lcom/google/ac/b/c/co;->a:Ljava/lang/Integer;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/location/copresence/ag;->e(Ljava/lang/String;)I

    goto/16 :goto_0

    :sswitch_0
    const/4 v3, 0x3

    invoke-static {v3}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v3

    if-eqz v3, :cond_c

    const-string v3, "GcmHandler: Handling sync settings"

    invoke-static {v3}, Lcom/google/android/location/copresence/ag;->b(Ljava/lang/String;)I

    :cond_c
    iget-object v3, v10, Lcom/google/android/location/copresence/l/h;->a:Landroid/content/Context;

    iget-object v2, v2, Lcom/google/android/location/copresence/a/a;->a:Landroid/accounts/Account;

    invoke-static {v3, v2}, Lcom/google/android/location/copresence/m/a;->a(Landroid/content/Context;Landroid/accounts/Account;)V

    goto/16 :goto_0

    :sswitch_1
    iget-object v5, v12, Lcom/google/ac/b/c/co;->i:Lcom/google/ac/b/c/cp;

    const-string v2, "OBFUSCATED_GAIA_ID"

    invoke-virtual {v11, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iget-object v2, v5, Lcom/google/ac/b/c/cp;->b:[Lcom/google/ac/b/c/bm;

    if-eqz v2, :cond_f

    iget-object v2, v5, Lcom/google/ac/b/c/cp;->b:[Lcom/google/ac/b/c/bm;

    array-length v2, v2

    if-lez v2, :cond_f

    const/4 v2, 0x1

    :goto_4
    iget-object v3, v5, Lcom/google/ac/b/c/cp;->a:[Lcom/google/ac/b/c/ah;

    if-eqz v3, :cond_10

    iget-object v3, v5, Lcom/google/ac/b/c/cp;->a:[Lcom/google/ac/b/c/ah;

    array-length v3, v3

    if-lez v3, :cond_10

    const/4 v3, 0x1

    :goto_5
    const/4 v4, 0x3

    invoke-static {v4}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v4

    if-eqz v4, :cond_d

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v4, "GcmHandler: Handling a report message with "

    invoke-direct {v7, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    if-eqz v2, :cond_11

    const-string v4, "0"

    :goto_6
    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v7, " subscribed messages and "

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    if-eqz v3, :cond_12

    const-string v4, "0"

    :goto_7
    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v7, " directives"

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/google/android/location/copresence/ag;->b(Ljava/lang/String;)I

    :cond_d
    if-eqz v2, :cond_e

    iget-object v2, v10, Lcom/google/android/location/copresence/l/h;->d:Lcom/google/android/location/copresence/h/c;

    sget-object v4, Lcom/google/android/gms/common/api/Status;->a:Lcom/google/android/gms/common/api/Status;

    iget-object v4, v5, Lcom/google/ac/b/c/cp;->b:[Lcom/google/ac/b/c/bm;

    invoke-virtual {v2, v4}, Lcom/google/android/location/copresence/h/c;->a([Lcom/google/ac/b/c/bm;)V

    :cond_e
    if-eqz v3, :cond_2

    iget-object v2, v10, Lcom/google/android/location/copresence/l/h;->b:Lcom/google/android/location/copresence/a/b;

    invoke-virtual {v2, v6}, Lcom/google/android/location/copresence/a/b;->b(Ljava/lang/String;)Lcom/google/android/location/copresence/a/a;

    move-result-object v2

    if-eqz v2, :cond_13

    iget-object v3, v10, Lcom/google/android/location/copresence/l/h;->c:Lcom/google/android/location/copresence/n/f;

    invoke-virtual {v3, v2}, Lcom/google/android/location/copresence/n/f;->a(Lcom/google/android/location/copresence/a/a;)Z

    move-result v2

    if-eqz v2, :cond_13

    const/4 v2, 0x1

    :goto_8
    if-eqz v2, :cond_2

    sget-object v2, Lcom/google/android/location/copresence/k;->G:Lcom/google/android/location/copresence/l;

    invoke-virtual {v2}, Lcom/google/android/location/copresence/l;->c()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_14

    iget-object v2, v10, Lcom/google/android/location/copresence/l/h;->e:Lcom/google/android/location/copresence/h/a;

    sget-object v3, Lcom/google/android/gms/common/api/Status;->a:Lcom/google/android/gms/common/api/Status;

    iget-object v4, v5, Lcom/google/ac/b/c/cp;->a:[Lcom/google/ac/b/c/ah;

    const/4 v5, 0x1

    const/4 v6, 0x0

    invoke-virtual {v2, v3, v4, v5, v6}, Lcom/google/android/location/copresence/h/a;->a(Lcom/google/android/gms/common/api/Status;[Lcom/google/ac/b/c/ah;ZLjava/util/HashSet;)V

    goto/16 :goto_0

    :cond_f
    const/4 v2, 0x0

    goto :goto_4

    :cond_10
    const/4 v3, 0x0

    goto :goto_5

    :cond_11
    iget-object v4, v5, Lcom/google/ac/b/c/cp;->b:[Lcom/google/ac/b/c/bm;

    array-length v4, v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    goto :goto_6

    :cond_12
    iget-object v4, v5, Lcom/google/ac/b/c/cp;->a:[Lcom/google/ac/b/c/ah;

    array-length v4, v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    goto :goto_7

    :cond_13
    const/4 v2, 0x0

    goto :goto_8

    :cond_14
    const/4 v2, 0x3

    invoke-static {v2}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v2

    if-eqz v2, :cond_2

    const-string v2, "GcmHandler: Skipping GCM wakeup ping because the flag is disabled."

    invoke-static {v2}, Lcom/google/android/location/copresence/ag;->b(Ljava/lang/String;)I

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x3 -> :sswitch_0
        0x8 -> :sswitch_1
    .end sparse-switch
.end method
