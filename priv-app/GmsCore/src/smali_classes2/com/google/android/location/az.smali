.class final Lcom/google/android/location/az;
.super Lcom/google/android/location/collectionlib/cy;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/location/ay;

.field private b:Ljava/io/File;


# direct methods
.method private constructor <init>(Lcom/google/android/location/ay;)V
    .locals 0

    .prologue
    .line 655
    iput-object p1, p0, Lcom/google/android/location/az;->a:Lcom/google/android/location/ay;

    invoke-direct {p0}, Lcom/google/android/location/collectionlib/cy;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/location/ay;B)V
    .locals 0

    .prologue
    .line 655
    invoke-direct {p0, p1}, Lcom/google/android/location/az;-><init>(Lcom/google/android/location/ay;)V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 693
    iget-object v0, p0, Lcom/google/android/location/az;->a:Lcom/google/android/location/ay;

    iget-boolean v0, v0, Lcom/google/android/location/ay;->f:Z

    if-nez v0, :cond_1

    .line 694
    sget-boolean v0, Lcom/google/android/location/i/a;->b:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/location/az;->a:Lcom/google/android/location/ay;

    iget-object v0, v0, Lcom/google/android/location/ay;->a:Ljava/lang/String;

    const-string v1, "Skipping onAllScanComplete because NLP is disabled."

    invoke-static {v0, v1}, Lcom/google/android/location/o/a/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 708
    :cond_0
    :goto_0
    return-void

    .line 698
    :cond_1
    :try_start_0
    iget-object v0, p0, Lcom/google/android/location/az;->a:Lcom/google/android/location/ay;

    iget-object v0, v0, Lcom/google/android/location/ay;->p:Lcom/google/android/location/collectionlib/be;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/location/az;->a:Lcom/google/android/location/ay;

    iget-boolean v0, v0, Lcom/google/android/location/ay;->q:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/location/az;->a:Lcom/google/android/location/ay;

    iget-object v0, v0, Lcom/google/android/location/ay;->r:Lcom/google/p/a/b/b/a;

    if-eqz v0, :cond_2

    .line 699
    new-instance v0, Lcom/google/p/a/b/b/a;

    sget-object v1, Lcom/google/android/location/m/a;->F:Lcom/google/p/a/b/b/c;

    invoke-direct {v0, v1}, Lcom/google/p/a/b/b/a;-><init>(Lcom/google/p/a/b/b/c;)V

    .line 700
    iget-object v1, p0, Lcom/google/android/location/az;->a:Lcom/google/android/location/ay;

    iget-object v1, v1, Lcom/google/android/location/ay;->r:Lcom/google/p/a/b/b/a;

    invoke-virtual {v1}, Lcom/google/p/a/b/b/a;->f()[B

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/p/a/b/b/a;->a([B)V

    .line 701
    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Lcom/google/p/a/b/b/a;->a(I)V

    .line 702
    iget-object v1, p0, Lcom/google/android/location/az;->a:Lcom/google/android/location/ay;

    iget-object v1, v1, Lcom/google/android/location/ay;->p:Lcom/google/android/location/collectionlib/be;

    invoke-interface {v1, v0}, Lcom/google/android/location/collectionlib/be;->a(Lcom/google/p/a/b/b/a;)Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 707
    :cond_2
    :goto_1
    iget-object v0, p0, Lcom/google/android/location/az;->a:Lcom/google/android/location/ay;

    iget-object v0, v0, Lcom/google/android/location/ay;->p:Lcom/google/android/location/collectionlib/be;

    invoke-interface {v0}, Lcom/google/android/location/collectionlib/be;->c()V

    goto :goto_0

    .line 705
    :catch_0
    move-exception v0

    sget-boolean v0, Lcom/google/android/location/i/a;->b:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/location/az;->a:Lcom/google/android/location/ay;

    iget-object v0, v0, Lcom/google/android/location/ay;->a:Ljava/lang/String;

    const-string v1, "Unable to attach customized data."

    invoke-static {v0, v1}, Lcom/google/android/location/o/a/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method public final a(ILjava/lang/String;)V
    .locals 1

    .prologue
    .line 712
    invoke-super {p0, p1, p2}, Lcom/google/android/location/collectionlib/cy;->a(ILjava/lang/String;)V

    .line 713
    iget-object v0, p0, Lcom/google/android/location/az;->b:Ljava/io/File;

    if-nez v0, :cond_0

    .line 714
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/az;->b:Ljava/io/File;

    .line 716
    :cond_0
    return-void
.end method

.method public final a(ILjava/lang/String;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 677
    iget-object v0, p0, Lcom/google/android/location/az;->a:Lcom/google/android/location/ay;

    iget-boolean v0, v0, Lcom/google/android/location/ay;->f:Z

    if-nez v0, :cond_1

    .line 678
    sget-boolean v0, Lcom/google/android/location/i/a;->b:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/location/az;->a:Lcom/google/android/location/ay;

    iget-object v0, v0, Lcom/google/android/location/ay;->a:Ljava/lang/String;

    const-string v1, "Skipping onFileSavingFailed because NLP is disabled."

    invoke-static {v0, v1}, Lcom/google/android/location/o/a/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 689
    :cond_0
    :goto_0
    return-void

    .line 681
    :cond_1
    iget-object v0, p0, Lcom/google/android/location/az;->a:Lcom/google/android/location/ay;

    iget-object v0, v0, Lcom/google/android/location/ay;->p:Lcom/google/android/location/collectionlib/be;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/location/az;->a:Lcom/google/android/location/ay;

    iget-wide v0, v0, Lcom/google/android/location/ay;->o:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-eqz v0, :cond_3

    const/4 v0, 0x1

    :goto_1
    invoke-static {v0}, Lcom/google/android/location/o/j;->b(Z)V

    .line 685
    iget-object v0, p0, Lcom/google/android/location/az;->a:Lcom/google/android/location/ay;

    iget-object v0, v0, Lcom/google/android/location/ay;->p:Lcom/google/android/location/collectionlib/be;

    if-eqz v0, :cond_2

    .line 686
    iget-object v0, p0, Lcom/google/android/location/az;->a:Lcom/google/android/location/ay;

    iget-object v0, v0, Lcom/google/android/location/ay;->p:Lcom/google/android/location/collectionlib/be;

    invoke-interface {v0}, Lcom/google/android/location/collectionlib/be;->b()V

    .line 688
    :cond_2
    sget-boolean v0, Lcom/google/android/location/i/a;->b:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/location/az;->a:Lcom/google/android/location/ay;

    iget-object v0, v0, Lcom/google/android/location/ay;->a:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Failed to write file: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/location/o/a/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 681
    :cond_3
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public final a(Lcom/google/android/location/collectionlib/cp;)V
    .locals 12

    .prologue
    const/4 v0, 0x0

    const-wide/16 v10, 0x1

    const/4 v9, 0x1

    .line 720
    iget-object v1, p0, Lcom/google/android/location/az;->a:Lcom/google/android/location/ay;

    iget-boolean v1, v1, Lcom/google/android/location/ay;->f:Z

    if-nez v1, :cond_2

    .line 721
    sget-boolean v1, Lcom/google/android/location/i/a;->b:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/location/az;->a:Lcom/google/android/location/ay;

    iget-object v1, v1, Lcom/google/android/location/ay;->a:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Skipping onLastSegmentSaved because NLP is disabled. "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/google/android/location/az;->b:Ljava/io/File;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/location/o/a/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 722
    :cond_0
    iget-object v1, p0, Lcom/google/android/location/az;->b:Ljava/io/File;

    if-eqz v1, :cond_1

    .line 723
    iget-object v1, p0, Lcom/google/android/location/az;->b:Ljava/io/File;

    invoke-static {v1}, Lcom/google/android/location/o/j;->a(Ljava/io/File;)Z

    .line 724
    sget-boolean v1, Lcom/google/android/location/i/a;->b:Z

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/android/location/az;->a:Lcom/google/android/location/ay;

    iget-object v1, v1, Lcom/google/android/location/ay;->a:Ljava/lang/String;

    const-string v2, "%s removed because NLP is disabled."

    new-array v3, v9, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/google/android/location/az;->b:Ljava/io/File;

    invoke-virtual {v4}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v0

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/google/android/location/o/a/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 769
    :cond_1
    :goto_0
    return-void

    .line 729
    :cond_2
    iget-object v1, p0, Lcom/google/android/location/az;->a:Lcom/google/android/location/ay;

    iget-object v1, v1, Lcom/google/android/location/ay;->p:Lcom/google/android/location/collectionlib/be;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/google/android/location/az;->a:Lcom/google/android/location/ay;

    iget-wide v2, v1, Lcom/google/android/location/ay;->o:J

    const-wide/16 v4, -0x1

    cmp-long v1, v2, v4

    if-eqz v1, :cond_3

    move v0, v9

    :cond_3
    invoke-static {v0}, Lcom/google/android/location/o/j;->b(Z)V

    .line 731
    sget-boolean v0, Lcom/google/android/location/i/a;->b:Z

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/google/android/location/az;->a:Lcom/google/android/location/ay;

    iget-object v0, v0, Lcom/google/android/location/ay;->a:Ljava/lang/String;

    const-string v1, "Sensor collection completed successfully or unsuccessfully."

    invoke-static {v0, v1}, Lcom/google/android/location/o/a/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 733
    :cond_4
    iget-object v0, p0, Lcom/google/android/location/az;->a:Lcom/google/android/location/ay;

    iget-object v0, v0, Lcom/google/android/location/ay;->b:Lcom/google/android/location/os/bi;

    invoke-interface {v0}, Lcom/google/android/location/os/bi;->c()Lcom/google/android/location/j/b;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/location/j/b;->c()J

    move-result-wide v6

    .line 738
    if-eqz p1, :cond_8

    invoke-virtual {p1}, Lcom/google/android/location/collectionlib/cp;->a()I

    move-result v0

    if-nez v0, :cond_8

    .line 739
    const/4 v8, 0x3

    .line 745
    :goto_1
    iget-object v1, p0, Lcom/google/android/location/az;->a:Lcom/google/android/location/ay;

    iget-object v0, p0, Lcom/google/android/location/az;->a:Lcom/google/android/location/ay;

    iget-wide v2, v0, Lcom/google/android/location/ay;->o:J

    iget-object v0, p0, Lcom/google/android/location/az;->a:Lcom/google/android/location/ay;

    iget-wide v4, v0, Lcom/google/android/location/ay;->o:J

    invoke-static/range {v1 .. v8}, Lcom/google/android/location/ay;->a(Lcom/google/android/location/ay;JJJI)V

    .line 750
    iget-object v0, p0, Lcom/google/android/location/az;->a:Lcom/google/android/location/ay;

    sget-object v0, Lcom/google/android/location/collectionlib/cg;->p:Lcom/google/android/location/collectionlib/cg;

    invoke-static {v0}, Lcom/google/android/location/ay;->a(Lcom/google/android/location/collectionlib/cg;)Z

    move-result v7

    .line 751
    iget-object v0, p0, Lcom/google/android/location/az;->a:Lcom/google/android/location/ay;

    sget-object v0, Lcom/google/android/location/collectionlib/cg;->q:Lcom/google/android/location/collectionlib/cg;

    invoke-static {v0}, Lcom/google/android/location/ay;->a(Lcom/google/android/location/collectionlib/cg;)Z

    move-result v8

    .line 752
    if-eqz p1, :cond_1

    if-nez v7, :cond_5

    if-eqz v8, :cond_1

    .line 753
    :cond_5
    const-string v1, "gps"

    .line 754
    const-string v2, "data_type"

    .line 755
    invoke-virtual {p1}, Lcom/google/android/location/collectionlib/cp;->a()I

    move-result v0

    if-lez v0, :cond_6

    .line 758
    iget-object v0, p0, Lcom/google/android/location/az;->a:Lcom/google/android/location/ay;

    iget-object v0, v0, Lcom/google/android/location/ay;->b:Lcom/google/android/location/os/bi;

    const-string v3, "location"

    move-wide v4, v10

    move v6, v9

    invoke-interface/range {v0 .. v6}, Lcom/google/android/location/os/bi;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JZ)V

    .line 760
    :cond_6
    invoke-virtual {p1}, Lcom/google/android/location/collectionlib/cp;->c()I

    move-result v0

    if-lez v0, :cond_7

    if-eqz v7, :cond_7

    .line 761
    iget-object v0, p0, Lcom/google/android/location/az;->a:Lcom/google/android/location/ay;

    iget-object v0, v0, Lcom/google/android/location/ay;->b:Lcom/google/android/location/os/bi;

    const-string v3, "measurement"

    move-wide v4, v10

    move v6, v9

    invoke-interface/range {v0 .. v6}, Lcom/google/android/location/os/bi;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JZ)V

    .line 764
    :cond_7
    invoke-virtual {p1}, Lcom/google/android/location/collectionlib/cp;->d()I

    move-result v0

    if-lez v0, :cond_1

    if-eqz v8, :cond_1

    .line 765
    iget-object v0, p0, Lcom/google/android/location/az;->a:Lcom/google/android/location/ay;

    iget-object v0, v0, Lcom/google/android/location/ay;->b:Lcom/google/android/location/os/bi;

    const-string v3, "navigation_message"

    move-wide v4, v10

    move v6, v9

    invoke-interface/range {v0 .. v6}, Lcom/google/android/location/os/bi;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JZ)V

    goto/16 :goto_0

    .line 740
    :cond_8
    iget-object v0, p0, Lcom/google/android/location/az;->a:Lcom/google/android/location/ay;

    iget-boolean v0, v0, Lcom/google/android/location/ay;->s:Z

    if-eqz v0, :cond_9

    .line 741
    const/4 v8, 0x2

    goto :goto_1

    :cond_9
    move v8, v9

    .line 743
    goto :goto_1
.end method

.method public final a(Ljava/lang/String;)V
    .locals 9

    .prologue
    const-wide/16 v4, -0x1

    .line 661
    iget-object v0, p0, Lcom/google/android/location/az;->a:Lcom/google/android/location/ay;

    iget-boolean v0, v0, Lcom/google/android/location/ay;->f:Z

    if-nez v0, :cond_1

    .line 662
    sget-boolean v0, Lcom/google/android/location/i/a;->b:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/location/az;->a:Lcom/google/android/location/ay;

    iget-object v0, v0, Lcom/google/android/location/ay;->a:Ljava/lang/String;

    const-string v1, "Skipping onFatalError because NLP is disabled."

    invoke-static {v0, v1}, Lcom/google/android/location/o/a/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 673
    :cond_0
    :goto_0
    return-void

    .line 667
    :cond_1
    iget-object v0, p0, Lcom/google/android/location/az;->a:Lcom/google/android/location/ay;

    iget-object v0, v0, Lcom/google/android/location/ay;->p:Lcom/google/android/location/collectionlib/be;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/location/az;->a:Lcom/google/android/location/ay;

    iget-wide v0, v0, Lcom/google/android/location/ay;->o:J

    cmp-long v0, v0, v4

    if-eqz v0, :cond_3

    const/4 v0, 0x1

    :goto_1
    invoke-static {v0}, Lcom/google/android/location/o/j;->b(Z)V

    .line 669
    sget-boolean v0, Lcom/google/android/location/i/a;->b:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/location/az;->a:Lcom/google/android/location/ay;

    iget-object v0, v0, Lcom/google/android/location/ay;->a:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Failed to collect: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/location/o/a/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 670
    :cond_2
    iget-object v1, p0, Lcom/google/android/location/az;->a:Lcom/google/android/location/ay;

    iget-object v0, p0, Lcom/google/android/location/az;->a:Lcom/google/android/location/ay;

    iget-wide v2, v0, Lcom/google/android/location/ay;->o:J

    iget-object v0, p0, Lcom/google/android/location/az;->a:Lcom/google/android/location/ay;

    iget-object v0, v0, Lcom/google/android/location/ay;->b:Lcom/google/android/location/os/bi;

    invoke-interface {v0}, Lcom/google/android/location/os/bi;->c()Lcom/google/android/location/j/b;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/location/j/b;->c()J

    move-result-wide v6

    const/16 v8, 0x1d

    invoke-static/range {v1 .. v8}, Lcom/google/android/location/ay;->a(Lcom/google/android/location/ay;JJJI)V

    goto :goto_0

    .line 667
    :cond_3
    const/4 v0, 0x0

    goto :goto_1
.end method
