.class public final Lcom/google/android/location/copresence/l/d;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static c:Lcom/google/android/location/copresence/l/d;


# instance fields
.field public final a:Landroid/content/Context;

.field public final b:Lcom/google/android/location/copresence/ap;


# direct methods
.method private constructor <init>(Landroid/content/Context;Lcom/google/android/location/copresence/ap;)V
    .locals 0

    .prologue
    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    iput-object p1, p0, Lcom/google/android/location/copresence/l/d;->a:Landroid/content/Context;

    .line 40
    iput-object p2, p0, Lcom/google/android/location/copresence/l/d;->b:Lcom/google/android/location/copresence/ap;

    .line 41
    return-void
.end method

.method public static declared-synchronized a(Landroid/content/Context;)Lcom/google/android/location/copresence/l/d;
    .locals 3

    .prologue
    .line 32
    const-class v1, Lcom/google/android/location/copresence/l/d;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/google/android/location/copresence/l/d;->c:Lcom/google/android/location/copresence/l/d;

    if-nez v0, :cond_0

    if-eqz p0, :cond_0

    .line 33
    new-instance v0, Lcom/google/android/location/copresence/l/d;

    invoke-static {p0}, Lcom/google/android/location/copresence/ap;->a(Landroid/content/Context;)Lcom/google/android/location/copresence/ap;

    move-result-object v2

    invoke-direct {v0, p0, v2}, Lcom/google/android/location/copresence/l/d;-><init>(Landroid/content/Context;Lcom/google/android/location/copresence/ap;)V

    sput-object v0, Lcom/google/android/location/copresence/l/d;->c:Lcom/google/android/location/copresence/l/d;

    .line 35
    :cond_0
    sget-object v0, Lcom/google/android/location/copresence/l/d;->c:Lcom/google/android/location/copresence/l/d;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 32
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method
