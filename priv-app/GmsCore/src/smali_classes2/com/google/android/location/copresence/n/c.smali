.class public final Lcom/google/android/location/copresence/n/c;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final a:J


# instance fields
.field private final b:Landroid/content/Context;

.field private final c:Ljava/lang/Object;

.field private final d:Landroid/content/ServiceConnection;

.field private e:Lcom/google/android/location/reporting/service/l;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 30
    sget-object v0, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0xa

    sget-object v1, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v2, v3, v1}, Ljava/util/concurrent/TimeUnit;->convert(JLjava/util/concurrent/TimeUnit;)J

    move-result-wide v0

    sput-wide v0, Lcom/google/android/location/copresence/n/c;->a:J

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    iput-object p1, p0, Lcom/google/android/location/copresence/n/c;->b:Landroid/content/Context;

    .line 42
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/location/copresence/n/c;->c:Ljava/lang/Object;

    .line 43
    new-instance v0, Lcom/google/android/location/copresence/n/d;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/android/location/copresence/n/d;-><init>(Lcom/google/android/location/copresence/n/c;B)V

    iput-object v0, p0, Lcom/google/android/location/copresence/n/c;->d:Landroid/content/ServiceConnection;

    .line 44
    return-void
.end method

.method static synthetic a(Lcom/google/android/location/copresence/n/c;Lcom/google/android/location/reporting/service/l;)Lcom/google/android/location/reporting/service/l;
    .locals 0

    .prologue
    .line 29
    iput-object p1, p0, Lcom/google/android/location/copresence/n/c;->e:Lcom/google/android/location/reporting/service/l;

    return-object p1
.end method

.method static synthetic a(Lcom/google/android/location/copresence/n/c;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 29
    iget-object v0, p0, Lcom/google/android/location/copresence/n/c;->c:Ljava/lang/Object;

    return-object v0
.end method

.method private c()Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 72
    const-string v2, "This function cannot be called in the main thread."

    invoke-static {v2}, Lcom/google/android/gms/common/internal/bx;->c(Ljava/lang/String;)V

    .line 73
    iget-object v2, p0, Lcom/google/android/location/copresence/n/c;->c:Ljava/lang/Object;

    monitor-enter v2

    .line 74
    :try_start_0
    iget-object v3, p0, Lcom/google/android/location/copresence/n/c;->e:Lcom/google/android/location/reporting/service/l;

    if-eqz v3, :cond_0

    .line 75
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 87
    :goto_0
    return v0

    .line 79
    :cond_0
    :try_start_1
    iget-object v3, p0, Lcom/google/android/location/copresence/n/c;->c:Ljava/lang/Object;

    sget-wide v4, Lcom/google/android/location/copresence/n/c;->a:J

    invoke-virtual {v3, v4, v5}, Ljava/lang/Object;->wait(J)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 87
    :try_start_2
    iget-object v3, p0, Lcom/google/android/location/copresence/n/c;->e:Lcom/google/android/location/reporting/service/l;

    if-eqz v3, :cond_2

    :goto_1
    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 88
    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0

    .line 80
    :catch_0
    move-exception v0

    .line 81
    const/4 v3, 0x6

    :try_start_3
    invoke-static {v3}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 82
    const-string v3, "Error waiting for initialization"

    invoke-static {v3, v0}, Lcom/google/android/location/copresence/ag;->a(Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 84
    :cond_1
    monitor-exit v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move v0, v1

    goto :goto_0

    :cond_2
    move v0, v1

    .line 87
    goto :goto_1
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 47
    iget-object v0, p0, Lcom/google/android/location/copresence/n/c;->b:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/location/copresence/n/c;->d:Landroid/content/ServiceConnection;

    invoke-static {v0, v1}, Lcom/google/android/location/reporting/service/PreferenceService;->a(Landroid/content/Context;Landroid/content/ServiceConnection;)V

    .line 48
    return-void
.end method

.method public final b()Lcom/google/android/location/copresence/n/a;
    .locals 3

    .prologue
    .line 59
    invoke-direct {p0}, Lcom/google/android/location/copresence/n/c;->c()Z

    move-result v0

    if-nez v0, :cond_0

    .line 60
    new-instance v0, Ljava/io/IOException;

    const-string v1, "Timeout trying to connect to PreferenceService."

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 64
    :cond_0
    :try_start_0
    iget-object v0, p0, Lcom/google/android/location/copresence/n/c;->e:Lcom/google/android/location/reporting/service/l;

    invoke-interface {v0}, Lcom/google/android/location/reporting/service/l;->a()Lcom/google/android/location/reporting/config/ReportingConfig;

    move-result-object v0

    .line 65
    new-instance v1, Lcom/google/android/location/copresence/n/a;

    iget-object v2, p0, Lcom/google/android/location/copresence/n/c;->b:Landroid/content/Context;

    invoke-direct {v1, v2, v0}, Lcom/google/android/location/copresence/n/a;-><init>(Landroid/content/Context;Lcom/google/android/location/reporting/config/ReportingConfig;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v1

    .line 66
    :catch_0
    move-exception v0

    .line 67
    new-instance v1, Ljava/io/IOException;

    const-string v2, "Unable to get ReportingConfig."

    invoke-direct {v1, v2, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method
