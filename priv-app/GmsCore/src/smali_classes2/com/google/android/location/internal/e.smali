.class public final Lcom/google/android/location/internal/e;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final a:Landroid/content/Context;

.field final b:Lcom/google/android/location/fused/g;

.field final c:Lcom/google/android/location/geofencer/service/g;

.field d:Lcom/google/android/location/places/as;

.field e:Lcom/google/android/location/copresence/m/b;

.field f:Lcom/google/android/gms/audiomodem/b/a;

.field private final g:Landroid/content/pm/PackageManager;

.field private final h:Ljava/util/ArrayList;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 84
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 81
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/location/internal/e;->h:Ljava/util/ArrayList;

    .line 85
    iput-object p1, p0, Lcom/google/android/location/internal/e;->a:Landroid/content/Context;

    .line 86
    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/internal/e;->g:Landroid/content/pm/PackageManager;

    .line 87
    new-instance v0, Lcom/google/android/location/geofencer/service/g;

    invoke-direct {v0, p1}, Lcom/google/android/location/geofencer/service/g;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/location/internal/e;->c:Lcom/google/android/location/geofencer/service/g;

    .line 88
    invoke-static {p1}, Lcom/google/android/location/fused/g;->a(Landroid/content/Context;)Lcom/google/android/location/fused/g;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/internal/e;->b:Lcom/google/android/location/fused/g;

    .line 89
    return-void
.end method

.method static a(Landroid/app/PendingIntent;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 535
    if-nez p0, :cond_0

    .line 536
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "invalid pending intent: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 538
    :cond_0
    invoke-virtual {p0}, Landroid/app/PendingIntent;->getTargetPackage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 539
    new-instance v0, Ljava/lang/SecurityException;

    const-string v1, "PendingIntent\'s target package can\'t be different to the request package."

    invoke-direct {v0, v1}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 542
    :cond_1
    return-void
.end method

.method static a(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 640
    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    move-result v0

    .line 641
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v1

    .line 642
    invoke-static {p0, v0, v1}, Lcom/google/android/location/internal/e;->a(Landroid/content/Context;II)Z

    move-result v0

    if-nez v0, :cond_0

    .line 643
    new-instance v0, Ljava/lang/SecurityException;

    const-string v1, "Activity detection usage requires the com.google.android.gms.permission.ACTIVITY_RECOGNITION permission"

    invoke-direct {v0, v1}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 647
    :cond_0
    return-void
.end method

.method private a(Lcom/google/android/gms/location/internal/LocationRequestInternal;)V
    .locals 6

    .prologue
    const/4 v1, 0x2

    const/4 v2, 0x1

    .line 260
    invoke-virtual {p1}, Lcom/google/android/gms/location/internal/LocationRequestInternal;->a()Lcom/google/android/gms/location/LocationRequest;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/location/LocationRequest;->b()I

    move-result v0

    const/16 v3, 0x64

    if-ne v0, v3, :cond_1

    move v0, v1

    :goto_0
    invoke-virtual {p0, v0}, Lcom/google/android/location/internal/e;->a(I)V

    .line 265
    invoke-virtual {p1}, Lcom/google/android/gms/location/internal/LocationRequestInternal;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/location/internal/e;->e()Z

    move-result v0

    if-nez v0, :cond_0

    .line 266
    const-string v0, "GLMSImpl"

    const-string v3, "Uid %s must be signed by Google to use internal features: %s"

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v1, v4

    aput-object p1, v1, v2

    invoke-static {v3, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)I

    .line 271
    invoke-virtual {p1}, Lcom/google/android/gms/location/internal/LocationRequestInternal;->g()V

    .line 273
    :cond_0
    return-void

    :cond_1
    move v0, v2

    .line 260
    goto :goto_0
.end method

.method static a(Landroid/content/Context;II)Z
    .locals 12

    .prologue
    const/4 v11, 0x3

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 578
    const-string v2, "com.google.android.gms.permission.ACTIVITY_RECOGNITION"

    invoke-virtual {p0, v2, p1, p2}, Landroid/content/Context;->checkPermission(Ljava/lang/String;II)I

    move-result v2

    if-nez v2, :cond_1

    .line 610
    :cond_0
    :goto_0
    return v0

    .line 582
    :cond_1
    const-string v2, "activity_recognition_permission_whitelist"

    invoke-static {}, Lcom/google/android/location/internal/a;->a()I

    move-result v3

    invoke-virtual {p0, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v3

    .line 585
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v4

    .line 586
    invoke-virtual {v4, p2}, Landroid/content/pm/PackageManager;->getPackagesForUid(I)[Ljava/lang/String;

    move-result-object v5

    .line 592
    array-length v6, v5

    move v2, v1

    :goto_1
    if-ge v2, v6, :cond_4

    aget-object v7, v5, v2

    .line 593
    invoke-interface {v3, v7, v1}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v8

    if-eqz v8, :cond_3

    .line 594
    invoke-static {v7, v4}, Lcom/google/android/location/internal/e;->a(Ljava/lang/String;Landroid/content/pm/PackageManager;)Z

    move-result v8

    if-eqz v8, :cond_2

    .line 595
    const-string v1, "GLMSImpl"

    invoke-static {v1, v11}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 596
    const-string v1, "GLMSImpl"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Found package in activity recognition white-list: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 603
    :cond_2
    const-string v8, "GLMSImpl"

    invoke-static {v8, v11}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v8

    if-eqz v8, :cond_3

    .line 604
    const-string v8, "GLMSImpl"

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "Found whitelisted package that doesn\'t define the permission in its manifest: "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v8, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 592
    :cond_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_4
    move v0, v1

    .line 610
    goto :goto_0
.end method

.method static a(Ljava/lang/String;Landroid/content/pm/PackageManager;)Z
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 616
    const/16 v1, 0x1000

    :try_start_0
    invoke-virtual {p1, p0, v1}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v1

    .line 620
    iget-object v2, v1, Landroid/content/pm/PackageInfo;->requestedPermissions:[Ljava/lang/String;

    if-eqz v2, :cond_2

    .line 621
    iget-object v2, v1, Landroid/content/pm/PackageInfo;->requestedPermissions:[Ljava/lang/String;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_2

    aget-object v4, v2, v1

    .line 622
    const-string v5, "com.google.android.gms.permission.ACTIVITY_RECOGNITION"

    invoke-virtual {v5, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v4

    if-eqz v4, :cond_1

    .line 624
    const/4 v0, 0x1

    .line 636
    :cond_0
    :goto_1
    return v0

    .line 621
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 628
    :catch_0
    move-exception v1

    .line 629
    const-string v2, "GLMSImpl"

    const/4 v3, 0x5

    invoke-static {v2, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 630
    const-string v2, "GLMSImpl"

    invoke-virtual {v1}, Landroid/content/pm/PackageManager$NameNotFoundException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 633
    :cond_2
    const-string v1, "GLMSImpl"

    const/4 v2, 0x3

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 634
    const-string v1, "GLMSImpl"

    const-string v2, "Did not find the activity recognition permission in the app\'s manifest"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method private static b(Lcom/google/android/gms/location/places/internal/PlacesParams;)V
    .locals 2

    .prologue
    .line 457
    iget-object v0, p0, Lcom/google/android/gms/location/places/internal/PlacesParams;->e:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 458
    new-instance v0, Ljava/lang/SecurityException;

    const-string v1, "This feature requires an account to be set when connecting to the Places API"

    invoke-direct {v0, v1}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 461
    :cond_0
    return-void
.end method

.method private c(Landroid/content/Intent;)I
    .locals 3

    .prologue
    .line 756
    iget-object v0, p0, Lcom/google/android/location/internal/e;->h:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    .line 757
    const/4 v1, 0x0

    :goto_0
    if-ge v1, v2, :cond_1

    .line 758
    iget-object v0, p0, Lcom/google/android/location/internal/e;->h:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Intent;

    .line 759
    invoke-virtual {v0, p1}, Landroid/content/Intent;->filterEquals(Landroid/content/Intent;)Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    .line 764
    :goto_1
    return v0

    .line 757
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 764
    :cond_1
    const/4 v0, -0x1

    goto :goto_1
.end method

.method private c(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 467
    iget-object v0, p0, Lcom/google/android/location/internal/e;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/location/n/ag;->a(Landroid/content/Context;)I

    move-result v0

    .line 468
    const/4 v1, 0x2

    if-ge v0, v1, :cond_0

    .line 469
    new-instance v0, Ljava/lang/SecurityException;

    const-string v1, "Requested place updates requires ACCESS_FINE_LOCATION permission"

    invoke-direct {v0, v1}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 472
    :cond_0
    iget-object v0, p0, Lcom/google/android/location/internal/e;->a:Landroid/content/Context;

    invoke-static {v0, p1}, Lcom/google/android/gms/common/util/e;->c(Landroid/content/Context;Ljava/lang/String;)V

    .line 473
    return-void
.end method

.method private d(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 669
    iget-object v0, p0, Lcom/google/android/location/internal/e;->a:Landroid/content/Context;

    invoke-static {v0, p1}, Lcom/google/android/gms/common/util/e;->c(Landroid/content/Context;Ljava/lang/String;)V

    .line 670
    invoke-virtual {p0}, Lcom/google/android/location/internal/e;->e()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/location/internal/e;->a()Lcom/google/android/location/places/as;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/location/places/as;->l:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 672
    :cond_0
    return-void

    .line 674
    :cond_1
    new-instance v0, Ljava/lang/SecurityException;

    const-string v1, "Client not authorized to use Places API"

    invoke-direct {v0, v1}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private f()Z
    .locals 2

    .prologue
    .line 530
    iget-object v0, p0, Lcom/google/android/location/internal/e;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/location/n/ag;->a(Landroid/content/Context;)I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Landroid/location/Location;
    .locals 4

    .prologue
    .line 216
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/location/internal/e;->a(I)V

    .line 217
    iget-object v0, p0, Lcom/google/android/location/internal/e;->b:Lcom/google/android/location/fused/g;

    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v1

    invoke-direct {p0}, Lcom/google/android/location/internal/e;->f()Z

    move-result v2

    const/4 v3, 0x0

    invoke-virtual {v0, v1, p1, v2, v3}, Lcom/google/android/location/fused/g;->a(ILjava/lang/String;ZZ)Landroid/location/Location;

    move-result-object v0

    return-object v0
.end method

.method final declared-synchronized a()Lcom/google/android/location/places/as;
    .locals 4

    .prologue
    .line 93
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/location/internal/e;->d:Lcom/google/android/location/places/as;

    if-nez v0, :cond_0

    .line 94
    new-instance v0, Lcom/google/android/location/places/as;

    iget-object v1, p0, Lcom/google/android/location/internal/e;->a:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/location/internal/e;->b:Lcom/google/android/location/fused/g;

    iget-object v3, p0, Lcom/google/android/location/internal/e;->c:Lcom/google/android/location/geofencer/service/g;

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/location/places/as;-><init>(Landroid/content/Context;Lcom/google/android/location/fused/g;Lcom/google/android/location/geofencer/service/g;)V

    iput-object v0, p0, Lcom/google/android/location/internal/e;->d:Lcom/google/android/location/places/as;

    .line 97
    :cond_0
    iget-object v0, p0, Lcom/google/android/location/internal/e;->d:Lcom/google/android/location/places/as;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 93
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method final a(I)V
    .locals 2

    .prologue
    .line 495
    iget-object v0, p0, Lcom/google/android/location/internal/e;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/location/n/ag;->a(Landroid/content/Context;)I

    move-result v0

    if-ge v0, p1, :cond_1

    .line 496
    const/4 v0, 0x2

    if-ne p1, v0, :cond_0

    .line 497
    new-instance v0, Ljava/lang/SecurityException;

    const-string v1, "Client must have ACCESS_FINE_LOCATION permission to request PRIORITY_HIGH_ACCURACY locations."

    invoke-direct {v0, v1}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 501
    :cond_0
    new-instance v0, Ljava/lang/SecurityException;

    const-string v1, "Client must have ACCESS_COARSE_LOCATION or ACCESS_FINE_LOCATION permission to perform any location operations."

    invoke-direct {v0, v1}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 506
    :cond_1
    return-void
.end method

.method final a(Landroid/content/Intent;)V
    .locals 2

    .prologue
    .line 722
    iget-object v1, p0, Lcom/google/android/location/internal/e;->h:Ljava/util/ArrayList;

    monitor-enter v1

    .line 723
    :try_start_0
    iget-object v0, p0, Lcom/google/android/location/internal/e;->h:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 725
    iget-object v0, p0, Lcom/google/android/location/internal/e;->b:Lcom/google/android/location/fused/g;

    invoke-static {}, Lcom/google/android/location/fused/g;->a()V

    .line 731
    :cond_0
    invoke-direct {p0, p1}, Lcom/google/android/location/internal/e;->c(Landroid/content/Intent;)I

    move-result v0

    if-gez v0, :cond_1

    .line 732
    iget-object v0, p0, Lcom/google/android/location/internal/e;->h:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 734
    :cond_1
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final a(Lcom/google/android/gms/location/LocationRequest;Lcom/google/android/gms/location/j;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 229
    invoke-static {p1}, Lcom/google/android/gms/location/internal/LocationRequestInternal;->a(Lcom/google/android/gms/location/LocationRequest;)Lcom/google/android/gms/location/internal/LocationRequestInternal;

    move-result-object v0

    invoke-virtual {p0, v0, p2, p3}, Lcom/google/android/location/internal/e;->a(Lcom/google/android/gms/location/internal/LocationRequestInternal;Lcom/google/android/gms/location/j;Ljava/lang/String;)V

    .line 231
    return-void
.end method

.method public final a(Lcom/google/android/gms/location/internal/LocationRequestInternal;Landroid/app/PendingIntent;)V
    .locals 2

    .prologue
    .line 253
    invoke-direct {p0, p1}, Lcom/google/android/location/internal/e;->a(Lcom/google/android/gms/location/internal/LocationRequestInternal;)V

    .line 254
    iget-object v0, p0, Lcom/google/android/location/internal/e;->b:Lcom/google/android/location/fused/g;

    invoke-direct {p0}, Lcom/google/android/location/internal/e;->f()Z

    move-result v1

    invoke-virtual {v0, p1, p2, v1}, Lcom/google/android/location/fused/g;->a(Lcom/google/android/gms/location/internal/LocationRequestInternal;Landroid/app/PendingIntent;Z)V

    .line 256
    return-void
.end method

.method public final a(Lcom/google/android/gms/location/internal/LocationRequestInternal;Lcom/google/android/gms/location/j;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 237
    invoke-direct {p0, p1}, Lcom/google/android/location/internal/e;->a(Lcom/google/android/gms/location/internal/LocationRequestInternal;)V

    .line 238
    iget-object v0, p0, Lcom/google/android/location/internal/e;->b:Lcom/google/android/location/fused/g;

    invoke-direct {p0}, Lcom/google/android/location/internal/e;->f()Z

    move-result v1

    invoke-virtual {v0, p1, p2, v1, p3}, Lcom/google/android/location/fused/g;->a(Lcom/google/android/gms/location/internal/LocationRequestInternal;Lcom/google/android/gms/location/j;ZLjava/lang/String;)V

    .line 243
    return-void
.end method

.method public final a(Lcom/google/android/gms/location/places/NearbyAlertRequest;Lcom/google/android/gms/location/places/internal/PlacesParams;Landroid/app/PendingIntent;)V
    .locals 5

    .prologue
    .line 385
    iget-object v0, p2, Lcom/google/android/gms/location/places/internal/PlacesParams;->c:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/google/android/location/internal/e;->d(Ljava/lang/String;)V

    .line 386
    iget-object v0, p2, Lcom/google/android/gms/location/places/internal/PlacesParams;->c:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/google/android/location/internal/e;->c(Ljava/lang/String;)V

    .line 387
    iget-object v0, p2, Lcom/google/android/gms/location/places/internal/PlacesParams;->c:Ljava/lang/String;

    invoke-static {p3, v0}, Lcom/google/android/location/internal/e;->a(Landroid/app/PendingIntent;Ljava/lang/String;)V

    .line 388
    invoke-virtual {p1}, Lcom/google/android/gms/location/places/NearbyAlertRequest;->d()Lcom/google/android/gms/location/places/PlaceFilter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/location/places/PlaceFilter;->g()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 389
    invoke-static {p2}, Lcom/google/android/location/internal/e;->b(Lcom/google/android/gms/location/places/internal/PlacesParams;)V

    .line 391
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/location/internal/e;->a()Lcom/google/android/location/places/as;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/android/gms/location/places/NearbyAlertRequest;->d()Lcom/google/android/gms/location/places/PlaceFilter;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/location/places/PlaceFilter;->f()Ljava/util/Set;

    move-result-object v1

    invoke-virtual {v0, p2, v1}, Lcom/google/android/location/places/as;->a(Lcom/google/android/gms/location/places/internal/PlacesParams;Ljava/util/Collection;)V

    invoke-static {p1, p2, p3}, Lcom/google/android/location/places/NearbyAlertSubscription;->a(Lcom/google/android/gms/location/places/NearbyAlertRequest;Lcom/google/android/gms/location/places/internal/PlacesParams;Landroid/app/PendingIntent;)Lcom/google/android/location/places/NearbyAlertSubscription;

    move-result-object v1

    iget-object v2, v0, Lcom/google/android/location/places/as;->i:Lcom/google/android/location/places/r;

    iget-object v3, v2, Lcom/google/android/location/places/r;->c:Ljava/lang/Object;

    monitor-enter v3

    :try_start_0
    iget-object v0, v2, Lcom/google/android/location/places/r;->d:Lcom/google/android/location/places/bx;

    invoke-virtual {v0, v1}, Lcom/google/android/location/places/bx;->b(Lcom/google/android/location/places/Subscription;)Lcom/google/android/location/places/Subscription;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/places/NearbyAlertSubscription;

    iget-object v4, v2, Lcom/google/android/location/places/r;->d:Lcom/google/android/location/places/bx;

    invoke-virtual {v4, v1}, Lcom/google/android/location/places/bx;->a(Lcom/google/android/location/places/Subscription;)V

    iget-object v4, v2, Lcom/google/android/location/places/r;->d:Lcom/google/android/location/places/bx;

    iget-boolean v4, v4, Lcom/google/android/location/places/bx;->a:Z

    if-nez v4, :cond_1

    monitor-exit v3

    :goto_0
    return-void

    :cond_1
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lcom/google/android/location/places/NearbyAlertSubscription;->f()Z

    move-result v4

    if-eqz v4, :cond_3

    iget-object v4, v2, Lcom/google/android/location/places/r;->a:Lcom/google/android/location/places/d/e;

    invoke-virtual {v4, v0}, Lcom/google/android/location/places/d/e;->c(Lcom/google/android/location/places/Subscription;)V

    :cond_2
    :goto_1
    invoke-virtual {v1}, Lcom/google/android/location/places/NearbyAlertSubscription;->f()Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, v2, Lcom/google/android/location/places/r;->a:Lcom/google/android/location/places/d/e;

    invoke-virtual {v0, v1}, Lcom/google/android/location/places/d/e;->b(Lcom/google/android/location/places/Subscription;)V

    :goto_2
    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v3

    throw v0

    :cond_3
    :try_start_1
    iget-object v4, v2, Lcom/google/android/location/places/r;->b:Lcom/google/android/location/places/b/d;

    invoke-virtual {v4, v0}, Lcom/google/android/location/places/b/d;->b(Lcom/google/android/location/places/NearbyAlertSubscription;)V

    goto :goto_1

    :cond_4
    iget-object v0, v2, Lcom/google/android/location/places/r;->b:Lcom/google/android/location/places/b/d;

    invoke-virtual {v0, v1}, Lcom/google/android/location/places/b/d;->a(Lcom/google/android/location/places/NearbyAlertSubscription;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_2
.end method

.method public final a(Lcom/google/android/gms/location/places/PlaceFilter;Lcom/google/android/gms/location/places/internal/PlacesParams;Lcom/google/android/gms/location/places/internal/d;)V
    .locals 6

    .prologue
    const/4 v5, 0x3

    .line 360
    iget-object v0, p2, Lcom/google/android/gms/location/places/internal/PlacesParams;->c:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/google/android/location/internal/e;->d(Ljava/lang/String;)V

    .line 363
    iget-object v0, p0, Lcom/google/android/location/internal/e;->b:Lcom/google/android/location/fused/g;

    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v1

    iget-object v2, p2, Lcom/google/android/gms/location/places/internal/PlacesParams;->c:Ljava/lang/String;

    invoke-direct {p0}, Lcom/google/android/location/internal/e;->f()Z

    move-result v3

    const/4 v4, 0x0

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/google/android/location/fused/g;->a(ILjava/lang/String;ZZ)Landroid/location/Location;

    move-result-object v0

    .line 369
    if-eqz v0, :cond_1

    .line 370
    const-string v1, "GLMSImpl"

    invoke-static {v1, v5}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 371
    const-string v1, "GLMSImpl"

    const-string v2, "getLastPlace: doing a nearby search"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 373
    :cond_0
    new-instance v1, Lcom/google/android/gms/maps/model/LatLng;

    invoke-virtual {v0}, Landroid/location/Location;->getLatitude()D

    move-result-wide v2

    invoke-virtual {v0}, Landroid/location/Location;->getLongitude()D

    move-result-wide v4

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/google/android/gms/maps/model/LatLng;-><init>(DD)V

    invoke-virtual {p0, v1, p1, p2, p3}, Lcom/google/android/location/internal/e;->a(Lcom/google/android/gms/maps/model/LatLng;Lcom/google/android/gms/location/places/PlaceFilter;Lcom/google/android/gms/location/places/internal/PlacesParams;Lcom/google/android/gms/location/places/internal/d;)V

    .line 381
    :goto_0
    return-void

    .line 376
    :cond_1
    const-string v0, "GLMSImpl"

    invoke-static {v0, v5}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 377
    const-string v0, "GLMSImpl"

    const-string v1, "getLastPlace: doing a nearby search, no location available"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 379
    :cond_2
    const/4 v0, 0x0

    invoke-virtual {p0, v0, p1, p2, p3}, Lcom/google/android/location/internal/e;->a(Lcom/google/android/gms/maps/model/LatLng;Lcom/google/android/gms/location/places/PlaceFilter;Lcom/google/android/gms/location/places/internal/PlacesParams;Lcom/google/android/gms/location/places/internal/d;)V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/location/places/PlaceReport;Lcom/google/android/gms/location/places/internal/PlacesParams;)V
    .locals 9

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    const/4 v3, 0x3

    .line 449
    iget-object v0, p2, Lcom/google/android/gms/location/places/internal/PlacesParams;->c:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/google/android/location/internal/e;->d(Ljava/lang/String;)V

    .line 450
    invoke-virtual {p0}, Lcom/google/android/location/internal/e;->a()Lcom/google/android/location/places/as;

    move-result-object v4

    const-string v0, "Places"

    invoke-static {v0, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "Places"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Reporting place: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v0, v5}, Lcom/google/android/location/n/aa;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    sget-object v0, Lcom/google/android/location/x;->d:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p2, Lcom/google/android/gms/location/places/internal/PlacesParams;->c:Ljava/lang/String;

    iget v5, p2, Lcom/google/android/gms/location/places/internal/PlacesParams;->b:I

    invoke-static {v3, v0, v5}, Lcom/google/android/location/places/bf;->a(ILjava/lang/String;I)Lcom/google/k/f/c/n;

    move-result-object v5

    new-instance v0, Lcom/google/k/f/c/j;

    invoke-direct {v0}, Lcom/google/k/f/c/j;-><init>()V

    iput-object v0, v5, Lcom/google/k/f/c/n;->i:Lcom/google/k/f/c/j;

    iget-object v0, v5, Lcom/google/k/f/c/n;->i:Lcom/google/k/f/c/j;

    invoke-virtual {p1}, Lcom/google/android/gms/location/places/PlaceReport;->a()Ljava/lang/String;

    move-result-object v6

    iput-object v6, v0, Lcom/google/k/f/c/j;->a:Ljava/lang/String;

    iget-object v0, v5, Lcom/google/k/f/c/n;->i:Lcom/google/k/f/c/j;

    invoke-virtual {p1}, Lcom/google/android/gms/location/places/PlaceReport;->b()Ljava/lang/String;

    move-result-object v6

    iput-object v6, v0, Lcom/google/k/f/c/j;->b:Ljava/lang/String;

    iget-object v6, v5, Lcom/google/k/f/c/n;->i:Lcom/google/k/f/c/j;

    invoke-virtual {p1}, Lcom/google/android/gms/location/places/PlaceReport;->c()Ljava/lang/String;

    move-result-object v7

    const/4 v0, -0x1

    invoke-virtual {v7}, Ljava/lang/String;->hashCode()I

    move-result v8

    sparse-switch v8, :sswitch_data_0

    :cond_1
    :goto_0
    packed-switch v0, :pswitch_data_0

    move v0, v2

    :goto_1
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, v6, Lcom/google/k/f/c/j;->c:Ljava/lang/Integer;

    iget-object v0, v4, Lcom/google/android/location/places/as;->a:Landroid/content/Context;

    invoke-static {v0, v5}, Lcom/google/android/location/places/PlaylogService;->a(Landroid/content/Context;Lcom/google/k/f/c/n;)V

    .line 451
    :cond_2
    return-void

    .line 450
    :sswitch_0
    const-string v3, "userReported"

    invoke-virtual {v7, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    move v0, v2

    goto :goto_0

    :sswitch_1
    const-string v3, "inferredGeofencing"

    invoke-virtual {v7, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    move v0, v1

    goto :goto_0

    :sswitch_2
    const-string v3, "inferredRadioSignals"

    invoke-virtual {v7, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    const/4 v0, 0x2

    goto :goto_0

    :sswitch_3
    const-string v8, "inferredReverseGeocoding"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_1

    move v0, v3

    goto :goto_0

    :pswitch_0
    move v0, v1

    goto :goto_1

    :pswitch_1
    const/16 v0, 0x21

    goto :goto_1

    :pswitch_2
    const/16 v0, 0x22

    goto :goto_1

    :pswitch_3
    const/16 v0, 0x23

    goto :goto_1

    :sswitch_data_0
    .sparse-switch
        -0x55a265e0 -> :sswitch_1
        -0x4739c642 -> :sswitch_0
        -0xfa92724 -> :sswitch_3
        0x4cb8af73 -> :sswitch_2
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public final a(Lcom/google/android/gms/location/places/PlaceRequest;Lcom/google/android/gms/location/places/internal/PlacesParams;Landroid/app/PendingIntent;)V
    .locals 4

    .prologue
    .line 402
    iget-object v0, p2, Lcom/google/android/gms/location/places/internal/PlacesParams;->c:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/google/android/location/internal/e;->d(Ljava/lang/String;)V

    .line 403
    iget-object v0, p2, Lcom/google/android/gms/location/places/internal/PlacesParams;->c:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/google/android/location/internal/e;->c(Ljava/lang/String;)V

    .line 404
    iget-object v0, p2, Lcom/google/android/gms/location/places/internal/PlacesParams;->c:Ljava/lang/String;

    invoke-static {p3, v0}, Lcom/google/android/location/internal/e;->a(Landroid/app/PendingIntent;Ljava/lang/String;)V

    .line 405
    invoke-virtual {p1}, Lcom/google/android/gms/location/places/PlaceRequest;->a()Lcom/google/android/gms/location/places/PlaceFilter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/location/places/PlaceFilter;->g()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 406
    invoke-static {p2}, Lcom/google/android/location/internal/e;->b(Lcom/google/android/gms/location/places/internal/PlacesParams;)V

    .line 408
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/location/internal/e;->a()Lcom/google/android/location/places/as;

    move-result-object v0

    invoke-static {p1, p2, p3}, Lcom/google/android/location/places/PlaceSubscription;->a(Lcom/google/android/gms/location/places/PlaceRequest;Lcom/google/android/gms/location/places/internal/PlacesParams;Landroid/app/PendingIntent;)Lcom/google/android/location/places/PlaceSubscription;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/android/gms/location/places/PlaceRequest;->a()Lcom/google/android/gms/location/places/PlaceFilter;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/gms/location/places/PlaceFilter;->f()Ljava/util/Set;

    move-result-object v2

    invoke-virtual {v0, p2, v2}, Lcom/google/android/location/places/as;->a(Lcom/google/android/gms/location/places/internal/PlacesParams;Ljava/util/Collection;)V

    iget-object v0, v0, Lcom/google/android/location/places/as;->h:Lcom/google/android/location/places/an;

    iget-object v2, v0, Lcom/google/android/location/places/an;->c:Ljava/lang/Object;

    monitor-enter v2

    :try_start_0
    invoke-virtual {v0, v1}, Lcom/google/android/location/places/an;->b(Lcom/google/android/location/places/PlaceSubscription;)V

    iget-object v3, v0, Lcom/google/android/location/places/an;->d:Lcom/google/android/location/places/bx;

    invoke-virtual {v3, v1}, Lcom/google/android/location/places/bx;->a(Lcom/google/android/location/places/Subscription;)V

    iget-object v3, v0, Lcom/google/android/location/places/an;->d:Lcom/google/android/location/places/bx;

    iget-boolean v3, v3, Lcom/google/android/location/places/bx;->a:Z

    if-nez v3, :cond_1

    monitor-exit v2

    :goto_0
    return-void

    :cond_1
    invoke-virtual {v0, v1}, Lcom/google/android/location/places/an;->a(Lcom/google/android/location/places/PlaceSubscription;)V

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0
.end method

.method public final a(Lcom/google/android/gms/location/places/UserAddedPlace;Lcom/google/android/gms/location/places/internal/PlacesParams;Lcom/google/android/gms/location/places/internal/d;)V
    .locals 11

    .prologue
    const/4 v2, 0x0

    .line 324
    iget-object v0, p2, Lcom/google/android/gms/location/places/internal/PlacesParams;->c:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/google/android/location/internal/e;->d(Ljava/lang/String;)V

    .line 325
    invoke-virtual {p0}, Lcom/google/android/location/internal/e;->a()Lcom/google/android/location/places/as;

    move-result-object v4

    iget-object v5, v4, Lcom/google/android/location/places/as;->d:Lcom/google/android/location/places/f;

    new-instance v6, Lcom/google/android/location/places/ax;

    invoke-direct {v6, p3}, Lcom/google/android/location/places/ax;-><init>(Lcom/google/android/gms/location/places/internal/d;)V

    iget-object v0, v5, Lcom/google/android/location/places/f;->d:Landroid/content/Context;

    const/4 v1, 0x0

    invoke-static {v0, v1, p2}, Lcom/google/android/location/places/bo;->a(Landroid/content/Context;Lcom/google/android/gms/location/places/PlaceFilter;Lcom/google/android/gms/location/places/internal/PlacesParams;)Lcom/google/android/location/l/a/bi;

    move-result-object v7

    new-instance v8, Lcom/google/android/location/l/a/at;

    invoke-direct {v8}, Lcom/google/android/location/l/a/at;-><init>()V

    invoke-virtual {p1}, Lcom/google/android/gms/location/places/UserAddedPlace;->b()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v8, Lcom/google/android/location/l/a/at;->a:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/google/android/gms/location/places/UserAddedPlace;->d()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v8, Lcom/google/android/location/l/a/at;->c:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/google/android/gms/location/places/UserAddedPlace;->f()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v8, Lcom/google/android/location/l/a/at;->d:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/google/android/gms/location/places/UserAddedPlace;->g()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v8, Lcom/google/android/location/l/a/at;->e:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/google/android/gms/location/places/UserAddedPlace;->e()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_0

    invoke-virtual {p1}, Lcom/google/android/gms/location/places/UserAddedPlace;->e()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    new-array v0, v0, [Ljava/lang/String;

    iput-object v0, v8, Lcom/google/android/location/l/a/at;->f:[Ljava/lang/String;

    invoke-virtual {p1}, Lcom/google/android/gms/location/places/UserAddedPlace;->e()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v9

    move v1, v2

    :goto_0
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/location/places/PlaceType;

    iget-object v10, v8, Lcom/google/android/location/l/a/at;->f:[Ljava/lang/String;

    add-int/lit8 v3, v1, 0x1

    invoke-virtual {v0}, Lcom/google/android/gms/location/places/PlaceType;->a()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v10, v1

    move v1, v3

    goto :goto_0

    :cond_0
    invoke-virtual {p1}, Lcom/google/android/gms/location/places/UserAddedPlace;->c()Lcom/google/android/gms/maps/model/LatLng;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/location/places/bo;->a(Lcom/google/android/gms/maps/model/LatLng;)Lcom/google/android/location/l/a/ag;

    move-result-object v0

    iput-object v0, v8, Lcom/google/android/location/l/a/at;->b:Lcom/google/android/location/l/a/ag;

    iput-object v8, v7, Lcom/google/android/location/l/a/bi;->i:Lcom/google/android/location/l/a/at;

    invoke-static {p2, v7}, Lcom/google/android/location/places/bo;->a(Lcom/google/android/gms/location/places/internal/PlacesParams;Lcom/google/android/location/l/a/bi;)Lcom/google/android/location/l/a/ak;

    move-result-object v0

    new-instance v1, Lcom/google/android/location/places/k;

    sget-object v3, Lcom/google/android/location/places/o;->g:Lcom/google/android/location/places/o;

    invoke-direct {v1, v5, p2, v6, v3}, Lcom/google/android/location/places/k;-><init>(Lcom/google/android/location/places/f;Lcom/google/android/gms/location/places/internal/PlacesParams;Lcom/google/android/location/places/aj;Lcom/google/android/location/places/o;)V

    invoke-virtual {v5, v0, v1}, Lcom/google/android/location/places/f;->a(Lcom/google/android/location/l/a/ak;Lcom/google/v/b/n;)V

    sget-object v0, Lcom/google/android/location/x;->d:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    iget-object v1, p2, Lcom/google/android/gms/location/places/internal/PlacesParams;->c:Ljava/lang/String;

    iget v3, p2, Lcom/google/android/gms/location/places/internal/PlacesParams;->b:I

    invoke-static {v0, v1, v3}, Lcom/google/android/location/places/bf;->a(ILjava/lang/String;I)Lcom/google/k/f/c/n;

    move-result-object v1

    const/4 v0, 0x3

    iget-object v3, p2, Lcom/google/android/gms/location/places/internal/PlacesParams;->d:Ljava/lang/String;

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v0, v3, v5}, Lcom/google/android/location/places/bf;->a(ILjava/lang/String;Ljava/lang/String;)Lcom/google/k/f/c/p;

    move-result-object v0

    iput-object v0, v1, Lcom/google/k/f/c/n;->g:Lcom/google/k/f/c/p;

    iget-object v0, v1, Lcom/google/k/f/c/n;->g:Lcom/google/k/f/c/p;

    new-instance v3, Lcom/google/k/f/c/m;

    invoke-direct {v3}, Lcom/google/k/f/c/m;-><init>()V

    iput-object v3, v0, Lcom/google/k/f/c/p;->g:Lcom/google/k/f/c/m;

    iget-object v0, v1, Lcom/google/k/f/c/n;->g:Lcom/google/k/f/c/p;

    iget-object v0, v0, Lcom/google/k/f/c/p;->g:Lcom/google/k/f/c/m;

    invoke-virtual {p1}, Lcom/google/android/gms/location/places/UserAddedPlace;->b()Ljava/lang/String;

    move-result-object v3

    iput-object v3, v0, Lcom/google/k/f/c/m;->a:Ljava/lang/String;

    iget-object v0, v1, Lcom/google/k/f/c/n;->g:Lcom/google/k/f/c/p;

    iget-object v0, v0, Lcom/google/k/f/c/p;->g:Lcom/google/k/f/c/m;

    invoke-virtual {p1}, Lcom/google/android/gms/location/places/UserAddedPlace;->c()Lcom/google/android/gms/maps/model/LatLng;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/location/places/bf;->a(Lcom/google/android/gms/maps/model/LatLng;)Lcom/google/k/f/c/g;

    move-result-object v3

    iput-object v3, v0, Lcom/google/k/f/c/m;->b:Lcom/google/k/f/c/g;

    iget-object v0, v1, Lcom/google/k/f/c/n;->g:Lcom/google/k/f/c/p;

    iget-object v0, v0, Lcom/google/k/f/c/p;->g:Lcom/google/k/f/c/m;

    invoke-virtual {p1}, Lcom/google/android/gms/location/places/UserAddedPlace;->d()Ljava/lang/String;

    move-result-object v3

    iput-object v3, v0, Lcom/google/k/f/c/m;->c:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/google/android/gms/location/places/UserAddedPlace;->e()Ljava/util/List;

    move-result-object v3

    if-eqz v3, :cond_1

    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, v1, Lcom/google/k/f/c/n;->g:Lcom/google/k/f/c/p;

    iget-object v0, v0, Lcom/google/k/f/c/p;->g:Lcom/google/k/f/c/m;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v5

    new-array v5, v5, [Ljava/lang/String;

    iput-object v5, v0, Lcom/google/k/f/c/m;->d:[Ljava/lang/String;

    :goto_1
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_1

    iget-object v0, v1, Lcom/google/k/f/c/n;->g:Lcom/google/k/f/c/p;

    iget-object v0, v0, Lcom/google/k/f/c/p;->g:Lcom/google/k/f/c/m;

    iget-object v5, v0, Lcom/google/k/f/c/m;->d:[Ljava/lang/String;

    invoke-interface {v3, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/location/places/PlaceType;

    invoke-virtual {v0}, Lcom/google/android/gms/location/places/PlaceType;->a()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v5, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_1
    iget-object v0, v1, Lcom/google/k/f/c/n;->g:Lcom/google/k/f/c/p;

    iget-object v0, v0, Lcom/google/k/f/c/p;->g:Lcom/google/k/f/c/m;

    invoke-virtual {p1}, Lcom/google/android/gms/location/places/UserAddedPlace;->f()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v0, Lcom/google/k/f/c/m;->e:Ljava/lang/String;

    iget-object v0, v1, Lcom/google/k/f/c/n;->g:Lcom/google/k/f/c/p;

    iget-object v0, v0, Lcom/google/k/f/c/p;->g:Lcom/google/k/f/c/m;

    invoke-virtual {p1}, Lcom/google/android/gms/location/places/UserAddedPlace;->g()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v0, Lcom/google/k/f/c/m;->f:Ljava/lang/String;

    iget-object v0, v4, Lcom/google/android/location/places/as;->a:Landroid/content/Context;

    invoke-static {v0, v1}, Lcom/google/android/location/places/PlaylogService;->a(Landroid/content/Context;Lcom/google/k/f/c/n;)V

    .line 326
    :cond_2
    return-void
.end method

.method public final a(Lcom/google/android/gms/location/places/UserDataType;Lcom/google/android/gms/maps/model/LatLngBounds;Ljava/util/List;Lcom/google/android/gms/location/places/internal/PlacesParams;Lcom/google/android/gms/location/places/internal/d;)V
    .locals 10

    .prologue
    const/4 v2, 0x1

    const/4 v0, 0x0

    .line 317
    iget-object v1, p4, Lcom/google/android/gms/location/places/internal/PlacesParams;->c:Ljava/lang/String;

    invoke-direct {p0, v1}, Lcom/google/android/location/internal/e;->d(Ljava/lang/String;)V

    .line 318
    invoke-static {p4}, Lcom/google/android/location/internal/e;->b(Lcom/google/android/gms/location/places/internal/PlacesParams;)V

    .line 319
    invoke-virtual {p0}, Lcom/google/android/location/internal/e;->a()Lcom/google/android/location/places/as;

    move-result-object v6

    new-array v1, v2, [Lcom/google/android/gms/location/places/UserDataType;

    aput-object p1, v1, v0

    invoke-static {v1}, Lcom/google/k/c/em;->a([Ljava/lang/Object;)Ljava/util/HashSet;

    move-result-object v3

    invoke-virtual {v6, p4, v3}, Lcom/google/android/location/places/as;->a(Lcom/google/android/gms/location/places/internal/PlacesParams;Ljava/util/Collection;)V

    if-nez p3, :cond_0

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v5

    :goto_0
    invoke-static {}, Lcom/google/android/gms/common/data/j;->g()Lcom/google/android/gms/common/data/m;

    move-result-object v4

    if-nez p2, :cond_2

    invoke-interface {v5}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_2

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    move v1, v0

    :goto_1
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iget-object v8, v6, Lcom/google/android/location/places/as;->g:Lcom/google/android/location/places/d/e;

    iget-object v9, p4, Lcom/google/android/gms/location/places/internal/PlacesParams;->e:Ljava/lang/String;

    invoke-virtual {v8, v9, v0, v3}, Lcom/google/android/location/places/d/e;->a(Ljava/lang/String;Ljava/lang/String;Ljava/util/Set;)Lcom/google/android/gms/location/places/personalized/PlaceUserData;

    move-result-object v0

    if-eqz v0, :cond_5

    invoke-static {v4, v0}, Lcom/google/android/gms/common/data/j;->a(Lcom/google/android/gms/common/data/m;Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;)V

    move v0, v2

    :goto_2
    move v1, v0

    goto :goto_1

    :cond_0
    move-object v5, p3

    goto :goto_0

    :cond_1
    move v0, v1

    :cond_2
    if-eqz v0, :cond_4

    const/4 v0, 0x0

    :try_start_0
    invoke-virtual {v4, v0}, Lcom/google/android/gms/common/data/m;->a(I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    invoke-interface {p5, v0}, Lcom/google/android/gms/location/places/internal/d;->c(Lcom/google/android/gms/common/data/DataHolder;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 320
    :cond_3
    :goto_3
    return-void

    .line 319
    :catch_0
    move-exception v0

    const-string v1, "Places"

    const/4 v2, 0x4

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_3

    const-string v1, "Places"

    const-string v2, "place user data callback failed"

    invoke-static {v1, v2, v0}, Lcom/google/android/location/n/aa;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_3

    :cond_4
    new-instance v0, Lcom/google/android/location/places/c/n;

    iget-object v1, v6, Lcom/google/android/location/places/as;->f:Lcom/google/android/location/places/c/j;

    iget-object v2, p4, Lcom/google/android/gms/location/places/internal/PlacesParams;->e:Ljava/lang/String;

    move-object v3, p1

    move-object v4, p2

    invoke-direct/range {v0 .. v5}, Lcom/google/android/location/places/c/n;-><init>(Lcom/google/android/location/places/c/j;Ljava/lang/String;Lcom/google/android/gms/location/places/UserDataType;Lcom/google/android/gms/maps/model/LatLngBounds;Ljava/util/List;)V

    iget-object v1, v6, Lcom/google/android/location/places/as;->k:Lcom/google/android/location/places/c/d;

    new-instance v2, Lcom/google/android/location/places/az;

    iget-object v3, p4, Lcom/google/android/gms/location/places/internal/PlacesParams;->e:Ljava/lang/String;

    invoke-direct {v2, v3, p5}, Lcom/google/android/location/places/az;-><init>(Ljava/lang/String;Lcom/google/android/gms/location/places/internal/d;)V

    invoke-virtual {v1, v0, v2}, Lcom/google/android/location/places/c/d;->a(Ljava/util/concurrent/Callable;Lcom/google/android/location/places/c/m;)V

    goto :goto_3

    :cond_5
    move v0, v1

    goto :goto_2
.end method

.method public final a(Lcom/google/android/gms/location/places/internal/PlacesParams;)V
    .locals 1

    .prologue
    .line 354
    iget-object v0, p1, Lcom/google/android/gms/location/places/internal/PlacesParams;->c:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/google/android/location/internal/e;->d(Ljava/lang/String;)V

    .line 356
    return-void
.end method

.method public final a(Lcom/google/android/gms/location/places/internal/PlacesParams;Landroid/app/PendingIntent;)V
    .locals 2

    .prologue
    .line 395
    iget-object v0, p1, Lcom/google/android/gms/location/places/internal/PlacesParams;->c:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/google/android/location/internal/e;->d(Ljava/lang/String;)V

    .line 396
    iget-object v0, p1, Lcom/google/android/gms/location/places/internal/PlacesParams;->c:Ljava/lang/String;

    invoke-static {p2, v0}, Lcom/google/android/location/internal/e;->a(Landroid/app/PendingIntent;Ljava/lang/String;)V

    .line 397
    invoke-virtual {p0}, Lcom/google/android/location/internal/e;->a()Lcom/google/android/location/places/as;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/location/places/as;->i:Lcom/google/android/location/places/r;

    invoke-static {p2}, Lcom/google/android/location/places/NearbyAlertSubscription;->a(Landroid/app/PendingIntent;)Lcom/google/android/location/places/NearbyAlertSubscription;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/location/places/r;->a(Lcom/google/android/location/places/NearbyAlertSubscription;)V

    .line 398
    return-void
.end method

.method public final a(Lcom/google/android/gms/maps/model/LatLng;Lcom/google/android/gms/location/places/PlaceFilter;Lcom/google/android/gms/location/places/internal/PlacesParams;Lcom/google/android/gms/location/places/internal/d;)V
    .locals 6

    .prologue
    .line 330
    iget-object v0, p3, Lcom/google/android/gms/location/places/internal/PlacesParams;->c:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/google/android/location/internal/e;->d(Ljava/lang/String;)V

    .line 331
    invoke-virtual {p0}, Lcom/google/android/location/internal/e;->a()Lcom/google/android/location/places/as;

    move-result-object v1

    new-instance v0, Lcom/google/android/location/places/ax;

    invoke-direct {v0, p4}, Lcom/google/android/location/places/ax;-><init>(Lcom/google/android/gms/location/places/internal/d;)V

    if-eqz p1, :cond_1

    iget-object v2, v1, Lcom/google/android/location/places/as;->d:Lcom/google/android/location/places/f;

    invoke-virtual {v2, p1, p2, p3, v0}, Lcom/google/android/location/places/f;->a(Lcom/google/android/gms/maps/model/LatLng;Lcom/google/android/gms/location/places/PlaceFilter;Lcom/google/android/gms/location/places/internal/PlacesParams;Lcom/google/android/location/places/aj;)V

    sget-object v0, Lcom/google/android/location/x;->d:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    iget-object v2, p3, Lcom/google/android/gms/location/places/internal/PlacesParams;->c:Ljava/lang/String;

    iget v3, p3, Lcom/google/android/gms/location/places/internal/PlacesParams;->b:I

    invoke-static {v0, v2, v3}, Lcom/google/android/location/places/bf;->a(ILjava/lang/String;I)Lcom/google/k/f/c/n;

    move-result-object v0

    invoke-virtual {p2}, Lcom/google/android/gms/location/places/PlaceFilter;->d()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x2

    iget-object v4, p3, Lcom/google/android/gms/location/places/internal/PlacesParams;->d:Ljava/lang/String;

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/google/android/location/places/bf;->a(ILjava/lang/String;Ljava/lang/String;)Lcom/google/k/f/c/p;

    move-result-object v3

    iput-object v3, v0, Lcom/google/k/f/c/n;->g:Lcom/google/k/f/c/p;

    iget-object v3, v0, Lcom/google/k/f/c/n;->g:Lcom/google/k/f/c/p;

    invoke-static {p2, v2}, Lcom/google/android/location/places/bf;->a(Lcom/google/android/gms/location/places/PlaceFilter;Ljava/lang/String;)Lcom/google/k/f/c/k;

    move-result-object v2

    iput-object v2, v3, Lcom/google/k/f/c/p;->d:Lcom/google/k/f/c/k;

    iget-object v2, v0, Lcom/google/k/f/c/n;->g:Lcom/google/k/f/c/p;

    new-instance v3, Lcom/google/k/f/c/o;

    invoke-direct {v3}, Lcom/google/k/f/c/o;-><init>()V

    iput-object v3, v2, Lcom/google/k/f/c/p;->f:Lcom/google/k/f/c/o;

    iget-object v2, v0, Lcom/google/k/f/c/n;->g:Lcom/google/k/f/c/p;

    iget-object v2, v2, Lcom/google/k/f/c/p;->f:Lcom/google/k/f/c/o;

    invoke-static {p1}, Lcom/google/android/location/places/bf;->a(Lcom/google/android/gms/maps/model/LatLng;)Lcom/google/k/f/c/g;

    move-result-object v3

    iput-object v3, v2, Lcom/google/k/f/c/o;->a:Lcom/google/k/f/c/g;

    iget-object v1, v1, Lcom/google/android/location/places/as;->a:Landroid/content/Context;

    invoke-static {v1, v0}, Lcom/google/android/location/places/PlaylogService;->a(Landroid/content/Context;Lcom/google/k/f/c/n;)V

    .line 332
    :cond_0
    :goto_0
    return-void

    .line 331
    :cond_1
    const/16 v1, 0x8

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/location/places/ax;->a(ILjava/util/List;)V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/maps/model/LatLngBounds;ILjava/lang/String;Lcom/google/android/gms/location/places/PlaceFilter;Lcom/google/android/gms/location/places/internal/PlacesParams;Lcom/google/android/gms/location/places/internal/d;)V
    .locals 9

    .prologue
    const/4 v8, 0x1

    .line 348
    iget-object v0, p5, Lcom/google/android/gms/location/places/internal/PlacesParams;->c:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/google/android/location/internal/e;->d(Ljava/lang/String;)V

    .line 349
    invoke-virtual {p0}, Lcom/google/android/location/internal/e;->a()Lcom/google/android/location/places/as;

    move-result-object v7

    new-instance v6, Lcom/google/android/location/places/ax;

    invoke-direct {v6, p6}, Lcom/google/android/location/places/ax;-><init>(Lcom/google/android/gms/location/places/internal/d;)V

    iget-object v0, v7, Lcom/google/android/location/places/as;->d:Lcom/google/android/location/places/f;

    move-object v1, p1

    move v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/location/places/f;->a(Lcom/google/android/gms/maps/model/LatLngBounds;ILjava/lang/String;Lcom/google/android/gms/location/places/PlaceFilter;Lcom/google/android/gms/location/places/internal/PlacesParams;Lcom/google/android/location/places/aj;)V

    sget-object v0, Lcom/google/android/location/x;->d:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p5, Lcom/google/android/gms/location/places/internal/PlacesParams;->c:Ljava/lang/String;

    iget v1, p5, Lcom/google/android/gms/location/places/internal/PlacesParams;->b:I

    invoke-static {v8, v0, v1}, Lcom/google/android/location/places/bf;->a(ILjava/lang/String;I)Lcom/google/k/f/c/n;

    move-result-object v0

    iget-object v1, p5, Lcom/google/android/gms/location/places/internal/PlacesParams;->d:Ljava/lang/String;

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v8, v1, v2}, Lcom/google/android/location/places/bf;->a(ILjava/lang/String;Ljava/lang/String;)Lcom/google/k/f/c/p;

    move-result-object v1

    iput-object v1, v0, Lcom/google/k/f/c/n;->g:Lcom/google/k/f/c/p;

    iget-object v1, v0, Lcom/google/k/f/c/n;->g:Lcom/google/k/f/c/p;

    invoke-static {p4, p3}, Lcom/google/android/location/places/bf;->a(Lcom/google/android/gms/location/places/PlaceFilter;Ljava/lang/String;)Lcom/google/k/f/c/k;

    move-result-object v2

    iput-object v2, v1, Lcom/google/k/f/c/p;->d:Lcom/google/k/f/c/k;

    iget-object v1, v0, Lcom/google/k/f/c/n;->g:Lcom/google/k/f/c/p;

    new-instance v2, Lcom/google/k/f/c/q;

    invoke-direct {v2}, Lcom/google/k/f/c/q;-><init>()V

    iput-object v2, v1, Lcom/google/k/f/c/p;->e:Lcom/google/k/f/c/q;

    iget-object v1, v0, Lcom/google/k/f/c/n;->g:Lcom/google/k/f/c/p;

    iget-object v1, v1, Lcom/google/k/f/c/p;->e:Lcom/google/k/f/c/q;

    invoke-static {p1}, Lcom/google/android/location/places/bf;->a(Lcom/google/android/gms/maps/model/LatLngBounds;)Lcom/google/k/f/c/h;

    move-result-object v2

    iput-object v2, v1, Lcom/google/k/f/c/q;->a:Lcom/google/k/f/c/h;

    iget-object v1, v7, Lcom/google/android/location/places/as;->a:Landroid/content/Context;

    invoke-static {v1, v0}, Lcom/google/android/location/places/PlaylogService;->a(Landroid/content/Context;Lcom/google/k/f/c/n;)V

    .line 350
    :cond_0
    return-void
.end method

.method public final a(Ljava/lang/String;Lcom/google/android/gms/location/places/internal/PlacesParams;Lcom/google/android/gms/location/places/internal/d;)V
    .locals 2

    .prologue
    .line 306
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-virtual {p0, v0, p2, p3}, Lcom/google/android/location/internal/e;->a(Ljava/util/List;Lcom/google/android/gms/location/places/internal/PlacesParams;Lcom/google/android/gms/location/places/internal/d;)V

    .line 307
    return-void
.end method

.method public final a(Ljava/lang/String;Lcom/google/android/gms/maps/model/LatLngBounds;Lcom/google/android/gms/location/places/AutocompleteFilter;Lcom/google/android/gms/location/places/internal/PlacesParams;Lcom/google/android/gms/location/places/internal/d;)V
    .locals 9

    .prologue
    const/4 v8, 0x0

    .line 336
    iget-object v0, p4, Lcom/google/android/gms/location/places/internal/PlacesParams;->c:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/google/android/location/internal/e;->d(Ljava/lang/String;)V

    .line 337
    invoke-virtual {p0}, Lcom/google/android/location/internal/e;->a()Lcom/google/android/location/places/as;

    move-result-object v6

    new-instance v7, Lcom/google/android/location/places/au;

    invoke-direct {v7, p5}, Lcom/google/android/location/places/au;-><init>(Lcom/google/android/gms/location/places/internal/d;)V

    iget-object v0, p4, Lcom/google/android/gms/location/places/internal/PlacesParams;->e:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/android/location/places/c/a;

    iget-object v1, v6, Lcom/google/android/location/places/as;->f:Lcom/google/android/location/places/c/j;

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/location/places/c/a;-><init>(Lcom/google/android/location/places/c/j;Ljava/lang/String;Lcom/google/android/gms/maps/model/LatLngBounds;Lcom/google/android/gms/location/places/AutocompleteFilter;Lcom/google/android/gms/location/places/internal/PlacesParams;)V

    new-instance v1, Lcom/google/android/location/places/av;

    invoke-direct {v1, v7, v8}, Lcom/google/android/location/places/av;-><init>(Lcom/google/android/location/places/b;B)V

    iget-object v2, v6, Lcom/google/android/location/places/as;->k:Lcom/google/android/location/places/c/d;

    invoke-virtual {v2, v0, v1}, Lcom/google/android/location/places/c/d;->a(Ljava/util/concurrent/Callable;Lcom/google/android/location/places/c/m;)V

    .line 338
    :goto_0
    return-void

    .line 337
    :cond_0
    iget-object v1, v6, Lcom/google/android/location/places/as;->d:Lcom/google/android/location/places/f;

    iget-object v0, v1, Lcom/google/android/location/places/f;->c:Lcom/google/android/location/n/ai;

    invoke-virtual {v0}, Lcom/google/android/location/n/ai;->a()J

    move-result-wide v2

    new-instance v5, Lcom/google/android/location/places/bl;

    iget-object v0, p4, Lcom/google/android/gms/location/places/internal/PlacesParams;->d:Ljava/lang/String;

    invoke-direct {v5, p2, v0, p1, p3}, Lcom/google/android/location/places/bl;-><init>(Lcom/google/android/gms/maps/model/LatLngBounds;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/location/places/AutocompleteFilter;)V

    iget-object v0, v1, Lcom/google/android/location/places/f;->b:Lcom/google/android/location/places/bi;

    invoke-virtual {v0, v5, v2, v3}, Lcom/google/android/location/places/bi;->a(Lcom/google/android/location/places/bl;J)Ljava/util/List;

    move-result-object v0

    if-nez v0, :cond_1

    iget-object v0, v1, Lcom/google/android/location/places/f;->d:Landroid/content/Context;

    invoke-static {v0, p2, p1, p3, p4}, Lcom/google/android/location/places/bo;->a(Landroid/content/Context;Lcom/google/android/gms/maps/model/LatLngBounds;Ljava/lang/String;Lcom/google/android/gms/location/places/AutocompleteFilter;Lcom/google/android/gms/location/places/internal/PlacesParams;)Lcom/google/android/location/l/a/bi;

    move-result-object v0

    invoke-static {p4, v0}, Lcom/google/android/location/places/bo;->a(Lcom/google/android/gms/location/places/internal/PlacesParams;Lcom/google/android/location/l/a/bi;)Lcom/google/android/location/l/a/ak;

    move-result-object v6

    new-instance v0, Lcom/google/android/location/places/h;

    sget-object v4, Lcom/google/android/location/places/o;->b:Lcom/google/android/location/places/o;

    move-object v2, p4

    move-object v3, v7

    invoke-direct/range {v0 .. v5}, Lcom/google/android/location/places/h;-><init>(Lcom/google/android/location/places/f;Lcom/google/android/gms/location/places/internal/PlacesParams;Lcom/google/android/location/places/b;Lcom/google/android/location/places/o;Ljava/lang/Object;)V

    invoke-virtual {v1, v6, v0}, Lcom/google/android/location/places/f;->a(Lcom/google/android/location/l/a/ak;Lcom/google/v/b/n;)V

    goto :goto_0

    :cond_1
    invoke-interface {v7, v8, v0}, Lcom/google/android/location/places/b;->a(ILjava/util/List;)V

    goto :goto_0
.end method

.method public final a(Ljava/util/List;Lcom/google/android/gms/location/places/internal/PlacesParams;Lcom/google/android/gms/location/places/internal/d;)V
    .locals 2

    .prologue
    .line 311
    iget-object v0, p2, Lcom/google/android/gms/location/places/internal/PlacesParams;->c:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/google/android/location/internal/e;->d(Ljava/lang/String;)V

    .line 312
    invoke-virtual {p0}, Lcom/google/android/location/internal/e;->a()Lcom/google/android/location/places/as;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/location/places/as;->d:Lcom/google/android/location/places/f;

    new-instance v1, Lcom/google/android/location/places/ax;

    invoke-direct {v1, p3}, Lcom/google/android/location/places/ax;-><init>(Lcom/google/android/gms/location/places/internal/d;)V

    invoke-virtual {v0, p1, p2, v1}, Lcom/google/android/location/places/f;->a(Ljava/util/Collection;Lcom/google/android/gms/location/places/internal/PlacesParams;Lcom/google/android/location/places/aj;)V

    .line 313
    return-void
.end method

.method final declared-synchronized b()Lcom/google/android/location/copresence/m/b;
    .locals 1

    .prologue
    .line 102
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/location/internal/e;->e:Lcom/google/android/location/copresence/m/b;

    if-nez v0, :cond_0

    .line 103
    iget-object v0, p0, Lcom/google/android/location/internal/e;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/location/copresence/m/b;->a(Landroid/content/Context;)Lcom/google/android/location/copresence/m/b;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/internal/e;->e:Lcom/google/android/location/copresence/m/b;

    .line 105
    :cond_0
    iget-object v0, p0, Lcom/google/android/location/internal/e;->e:Lcom/google/android/location/copresence/m/b;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 102
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method final b(Landroid/content/Intent;)V
    .locals 3

    .prologue
    .line 741
    iget-object v1, p0, Lcom/google/android/location/internal/e;->h:Ljava/util/ArrayList;

    monitor-enter v1

    .line 742
    :try_start_0
    invoke-direct {p0, p1}, Lcom/google/android/location/internal/e;->c(Landroid/content/Intent;)I

    move-result v0

    .line 743
    if-ltz v0, :cond_0

    .line 744
    iget-object v2, p0, Lcom/google/android/location/internal/e;->h:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 747
    :cond_0
    iget-object v0, p0, Lcom/google/android/location/internal/e;->h:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 749
    iget-object v0, p0, Lcom/google/android/location/internal/e;->b:Lcom/google/android/location/fused/g;

    invoke-virtual {v0}, Lcom/google/android/location/fused/g;->b()V

    .line 751
    :cond_1
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final b(Lcom/google/android/gms/location/places/internal/PlacesParams;Landroid/app/PendingIntent;)V
    .locals 2

    .prologue
    .line 412
    iget-object v0, p1, Lcom/google/android/gms/location/places/internal/PlacesParams;->c:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/google/android/location/internal/e;->d(Ljava/lang/String;)V

    .line 413
    iget-object v0, p1, Lcom/google/android/gms/location/places/internal/PlacesParams;->c:Ljava/lang/String;

    invoke-static {p2, v0}, Lcom/google/android/location/internal/e;->a(Landroid/app/PendingIntent;Ljava/lang/String;)V

    .line 414
    invoke-virtual {p0}, Lcom/google/android/location/internal/e;->a()Lcom/google/android/location/places/as;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/location/places/as;->h:Lcom/google/android/location/places/an;

    invoke-static {p2}, Lcom/google/android/location/places/PlaceSubscription;->a(Landroid/app/PendingIntent;)Lcom/google/android/location/places/PlaceSubscription;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/location/places/an;->b(Lcom/google/android/location/places/PlaceSubscription;)V

    .line 415
    return-void
.end method

.method final b(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 480
    iget-object v0, p0, Lcom/google/android/location/internal/e;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/location/n/ag;->a(Landroid/content/Context;)I

    move-result v0

    .line 481
    const/4 v1, 0x2

    if-ge v0, v1, :cond_0

    .line 482
    new-instance v0, Ljava/lang/SecurityException;

    const-string v1, "Geofence usage requires ACCESS_FINE_LOCATION permission"

    invoke-direct {v0, v1}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 485
    :cond_0
    iget-object v0, p0, Lcom/google/android/location/internal/e;->a:Landroid/content/Context;

    invoke-static {v0, p1}, Lcom/google/android/gms/common/util/e;->c(Landroid/content/Context;Ljava/lang/String;)V

    .line 486
    return-void
.end method

.method final declared-synchronized c()Lcom/google/android/gms/audiomodem/b/a;
    .locals 1

    .prologue
    .line 110
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/location/internal/e;->f:Lcom/google/android/gms/audiomodem/b/a;

    if-nez v0, :cond_0

    .line 111
    iget-object v0, p0, Lcom/google/android/location/internal/e;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/gms/audiomodem/b/a;->a(Landroid/content/Context;)Lcom/google/android/gms/audiomodem/b/a;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/internal/e;->f:Lcom/google/android/gms/audiomodem/b/a;

    .line 113
    :cond_0
    iget-object v0, p0, Lcom/google/android/location/internal/e;->f:Lcom/google/android/gms/audiomodem/b/a;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 110
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method final d()V
    .locals 2

    .prologue
    .line 513
    iget-object v0, p0, Lcom/google/android/location/internal/e;->a:Landroid/content/Context;

    const-string v1, "android.permission.ACCESS_MOCK_LOCATION"

    invoke-virtual {v0, v1}, Landroid/content/Context;->checkCallingPermission(Ljava/lang/String;)I

    move-result v0

    if-eqz v0, :cond_0

    .line 515
    new-instance v0, Ljava/lang/SecurityException;

    const-string v1, "Client must have ACCESS_MOCK_LOCATION permission to perform mock operations."

    invoke-direct {v0, v1}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 520
    :cond_0
    iget-object v0, p0, Lcom/google/android/location/internal/e;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "mock_location"

    invoke-static {v0, v1}, Landroid/provider/Settings$Secure;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "1"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contentEquals(Ljava/lang/CharSequence;)Z

    move-result v0

    .line 523
    if-nez v0, :cond_1

    .line 524
    new-instance v0, Ljava/lang/SecurityException;

    const-string v1, "The Settings.Secure.ALLOW_MOCK_LOCATION system setting is not enabled."

    invoke-direct {v0, v1}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 527
    :cond_1
    return-void
.end method

.method final e()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 650
    iget-object v1, p0, Lcom/google/android/location/internal/e;->g:Landroid/content/pm/PackageManager;

    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/content/pm/PackageManager;->getPackagesForUid(I)[Ljava/lang/String;

    move-result-object v1

    array-length v2, v1

    if-lez v2, :cond_0

    iget-object v2, p0, Lcom/google/android/location/internal/e;->g:Landroid/content/pm/PackageManager;

    aget-object v0, v1, v0

    invoke-static {v2, v0}, Lcom/google/android/gms/common/ew;->b(Landroid/content/pm/PackageManager;Ljava/lang/String;)Z

    move-result v0

    :cond_0
    return v0
.end method
