.class public final Lcom/google/android/location/fused/a/s;
.super Lcom/google/android/location/fused/a/q;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/location/activity/au;


# instance fields
.field public a:Lcom/google/android/location/activity/au;

.field public b:Landroid/os/Handler;

.field private final c:Lcom/google/android/location/activity/at;


# direct methods
.method public constructor <init>(Lcom/google/android/location/activity/at;)V
    .locals 1

    .prologue
    .line 19
    invoke-direct {p0}, Lcom/google/android/location/fused/a/q;-><init>()V

    .line 20
    iput-object p1, p0, Lcom/google/android/location/fused/a/s;->c:Lcom/google/android/location/activity/at;

    .line 21
    iget-object v0, p0, Lcom/google/android/location/fused/a/s;->c:Lcom/google/android/location/activity/at;

    invoke-interface {v0}, Lcom/google/android/location/activity/at;->b()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/location/fused/a/s;->a(Z)V

    .line 22
    return-void
.end method


# virtual methods
.method protected final a()V
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lcom/google/android/location/fused/a/s;->c:Lcom/google/android/location/activity/at;

    invoke-interface {v0}, Lcom/google/android/location/activity/at;->b()Z

    move-result v0

    if-nez v0, :cond_0

    .line 44
    :goto_0
    return-void

    .line 38
    :cond_0
    iget-boolean v0, p0, Lcom/google/android/location/fused/a/q;->h:Z

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/google/android/location/fused/a/q;->i:Z

    if-eqz v0, :cond_1

    .line 39
    iget-object v0, p0, Lcom/google/android/location/fused/a/s;->c:Lcom/google/android/location/activity/at;

    invoke-interface {v0, p0}, Lcom/google/android/location/activity/at;->a(Lcom/google/android/location/activity/au;)Z

    goto :goto_0

    .line 41
    :cond_1
    iget-object v0, p0, Lcom/google/android/location/fused/a/s;->c:Lcom/google/android/location/activity/at;

    invoke-interface {v0}, Lcom/google/android/location/activity/at;->a()Z

    .line 42
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/location/fused/a/s;->a:Lcom/google/android/location/activity/au;

    goto :goto_0
.end method

.method public final l()V
    .locals 2

    .prologue
    .line 48
    iget-object v0, p0, Lcom/google/android/location/fused/a/s;->b:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/location/fused/a/t;

    invoke-direct {v1, p0}, Lcom/google/android/location/fused/a/t;-><init>(Lcom/google/android/location/fused/a/s;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 58
    return-void
.end method
