.class public final Lcom/google/android/location/copresence/b;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public a:Lcom/google/android/gms/location/copresence/x;

.field public b:Lcom/google/ac/b/c/o;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/location/copresence/x;Lcom/google/ac/b/c/o;)V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    iput-object p1, p0, Lcom/google/android/location/copresence/b;->a:Lcom/google/android/gms/location/copresence/x;

    .line 18
    iput-object p2, p0, Lcom/google/android/location/copresence/b;->b:Lcom/google/ac/b/c/o;

    .line 19
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 2

    .prologue
    .line 30
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/google/android/location/copresence/b;->b:Lcom/google/ac/b/c/o;

    iget-object v1, v1, Lcom/google/ac/b/c/o;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/location/copresence/b;->a:Lcom/google/android/gms/location/copresence/x;

    invoke-virtual {v1}, Lcom/google/android/gms/location/copresence/x;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 35
    if-ne p0, p1, :cond_1

    .line 43
    :cond_0
    :goto_0
    return v0

    .line 38
    :cond_1
    instance-of v2, p1, Lcom/google/android/location/copresence/b;

    if-eqz v2, :cond_3

    .line 39
    check-cast p1, Lcom/google/android/location/copresence/b;

    .line 40
    iget-object v2, p0, Lcom/google/android/location/copresence/b;->a:Lcom/google/android/gms/location/copresence/x;

    iget-object v3, p1, Lcom/google/android/location/copresence/b;->a:Lcom/google/android/gms/location/copresence/x;

    invoke-static {v2, v3}, Lcom/google/android/gms/common/internal/bu;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/google/android/location/copresence/b;->b:Lcom/google/ac/b/c/o;

    iget-object v3, p1, Lcom/google/android/location/copresence/b;->b:Lcom/google/ac/b/c/o;

    invoke-static {v2, v3}, Lcom/google/android/gms/common/internal/bu;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_2
    move v0, v1

    goto :goto_0

    :cond_3
    move v0, v1

    .line 43
    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    .line 48
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/location/copresence/b;->a:Lcom/google/android/gms/location/copresence/x;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/location/copresence/b;->b:Lcom/google/ac/b/c/o;

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method
