.class final Lcom/google/android/location/copresence/d/n;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final p:J


# instance fields
.field protected final a:Ljava/lang/String;

.field protected final b:Lcom/google/android/location/copresence/d/e;

.field protected final c:Landroid/content/SharedPreferences;

.field final d:Lcom/google/android/location/copresence/k/f;

.field final e:Lcom/google/android/location/copresence/k/f;

.field final f:Lcom/google/android/location/copresence/k/f;

.field final g:Lcom/google/android/location/copresence/k/f;

.field final h:Lcom/google/android/location/copresence/k/f;

.field final i:Lcom/google/android/location/copresence/k/f;

.field final j:Lcom/google/android/location/copresence/k/f;

.field final k:Lcom/google/android/location/copresence/k/f;

.field final l:Lcom/google/android/location/copresence/k/f;

.field final m:Lcom/google/android/location/copresence/k/f;

.field final n:Lcom/google/android/location/copresence/k/f;

.field final o:Lcom/google/android/location/copresence/k/f;

.field private final q:Lcom/google/android/location/copresence/k/f;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 26
    sget-object v0, Ljava/util/concurrent/TimeUnit;->DAYS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0x1

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    sput-wide v0, Lcom/google/android/location/copresence/d/n;->p:J

    return-void
.end method

.method public constructor <init>(Lcom/google/android/location/copresence/d/e;Landroid/content/SharedPreferences;)V
    .locals 7

    .prologue
    const/4 v5, 0x2

    const/4 v6, 0x1

    const/4 v4, 0x0

    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    const-string v0, "BluetoothStates: "

    iput-object v0, p0, Lcom/google/android/location/copresence/d/n;->a:Ljava/lang/String;

    .line 86
    new-instance v0, Lcom/google/android/location/copresence/d/o;

    const-string v1, "UserEnabledSettingSaved"

    new-array v2, v4, [Lcom/google/android/location/copresence/k/f;

    invoke-direct {v0, p0, v1, v2}, Lcom/google/android/location/copresence/d/o;-><init>(Lcom/google/android/location/copresence/d/n;Ljava/lang/String;[Lcom/google/android/location/copresence/k/f;)V

    iput-object v0, p0, Lcom/google/android/location/copresence/d/n;->d:Lcom/google/android/location/copresence/k/f;

    .line 102
    new-instance v0, Lcom/google/android/location/copresence/d/t;

    const-string v1, "Disabled"

    new-array v2, v6, [Lcom/google/android/location/copresence/k/f;

    iget-object v3, p0, Lcom/google/android/location/copresence/d/n;->d:Lcom/google/android/location/copresence/k/f;

    aput-object v3, v2, v4

    invoke-direct {v0, p0, v1, v2}, Lcom/google/android/location/copresence/d/t;-><init>(Lcom/google/android/location/copresence/d/n;Ljava/lang/String;[Lcom/google/android/location/copresence/k/f;)V

    iput-object v0, p0, Lcom/google/android/location/copresence/d/n;->e:Lcom/google/android/location/copresence/k/f;

    .line 121
    new-instance v0, Lcom/google/android/location/copresence/d/u;

    const-string v1, "Enabled"

    new-array v2, v6, [Lcom/google/android/location/copresence/k/f;

    iget-object v3, p0, Lcom/google/android/location/copresence/d/n;->d:Lcom/google/android/location/copresence/k/f;

    aput-object v3, v2, v4

    invoke-direct {v0, p0, v1, v2}, Lcom/google/android/location/copresence/d/u;-><init>(Lcom/google/android/location/copresence/d/n;Ljava/lang/String;[Lcom/google/android/location/copresence/k/f;)V

    iput-object v0, p0, Lcom/google/android/location/copresence/d/n;->f:Lcom/google/android/location/copresence/k/f;

    .line 141
    new-instance v0, Lcom/google/android/location/copresence/d/v;

    const-string v1, "UserSettingsSaved"

    new-array v2, v5, [Lcom/google/android/location/copresence/k/f;

    iget-object v3, p0, Lcom/google/android/location/copresence/d/n;->d:Lcom/google/android/location/copresence/k/f;

    aput-object v3, v2, v4

    iget-object v3, p0, Lcom/google/android/location/copresence/d/n;->f:Lcom/google/android/location/copresence/k/f;

    aput-object v3, v2, v6

    invoke-direct {v0, p0, v1, v2}, Lcom/google/android/location/copresence/d/v;-><init>(Lcom/google/android/location/copresence/d/n;Ljava/lang/String;[Lcom/google/android/location/copresence/k/f;)V

    iput-object v0, p0, Lcom/google/android/location/copresence/d/n;->g:Lcom/google/android/location/copresence/k/f;

    .line 192
    new-instance v0, Lcom/google/android/location/copresence/d/w;

    const-string v1, "Discovering"

    new-array v2, v5, [Lcom/google/android/location/copresence/k/f;

    iget-object v3, p0, Lcom/google/android/location/copresence/d/n;->d:Lcom/google/android/location/copresence/k/f;

    aput-object v3, v2, v4

    iget-object v3, p0, Lcom/google/android/location/copresence/d/n;->f:Lcom/google/android/location/copresence/k/f;

    aput-object v3, v2, v6

    invoke-direct {v0, p0, v1, v2}, Lcom/google/android/location/copresence/d/w;-><init>(Lcom/google/android/location/copresence/d/n;Ljava/lang/String;[Lcom/google/android/location/copresence/k/f;)V

    iput-object v0, p0, Lcom/google/android/location/copresence/d/n;->h:Lcom/google/android/location/copresence/k/f;

    .line 211
    new-instance v0, Lcom/google/android/location/copresence/d/x;

    const-string v1, "NotDiscovering"

    new-array v2, v4, [Lcom/google/android/location/copresence/k/f;

    invoke-direct {v0, p0, v1, v2}, Lcom/google/android/location/copresence/d/x;-><init>(Lcom/google/android/location/copresence/d/n;Ljava/lang/String;[Lcom/google/android/location/copresence/k/f;)V

    iput-object v0, p0, Lcom/google/android/location/copresence/d/n;->i:Lcom/google/android/location/copresence/k/f;

    .line 308
    new-instance v0, Lcom/google/android/location/copresence/d/y;

    const-string v1, "Discoverable"

    new-array v2, v5, [Lcom/google/android/location/copresence/k/f;

    iget-object v3, p0, Lcom/google/android/location/copresence/d/n;->g:Lcom/google/android/location/copresence/k/f;

    aput-object v3, v2, v4

    iget-object v3, p0, Lcom/google/android/location/copresence/d/n;->f:Lcom/google/android/location/copresence/k/f;

    aput-object v3, v2, v6

    invoke-direct {v0, p0, v1, v2}, Lcom/google/android/location/copresence/d/y;-><init>(Lcom/google/android/location/copresence/d/n;Ljava/lang/String;[Lcom/google/android/location/copresence/k/f;)V

    iput-object v0, p0, Lcom/google/android/location/copresence/d/n;->j:Lcom/google/android/location/copresence/k/f;

    .line 331
    new-instance v0, Lcom/google/android/location/copresence/d/z;

    const-string v1, "RevertedDiscoverable"

    new-array v2, v4, [Lcom/google/android/location/copresence/k/f;

    invoke-direct {v0, p0, v1, v2}, Lcom/google/android/location/copresence/d/z;-><init>(Lcom/google/android/location/copresence/d/n;Ljava/lang/String;[Lcom/google/android/location/copresence/k/f;)V

    iput-object v0, p0, Lcom/google/android/location/copresence/d/n;->k:Lcom/google/android/location/copresence/k/f;

    .line 423
    new-instance v0, Lcom/google/android/location/copresence/d/aa;

    const-string v1, "StopBleUuidAndTokenAdvertiseState"

    new-array v2, v4, [Lcom/google/android/location/copresence/k/f;

    invoke-direct {v0, p0, v1, v2}, Lcom/google/android/location/copresence/d/aa;-><init>(Lcom/google/android/location/copresence/d/n;Ljava/lang/String;[Lcom/google/android/location/copresence/k/f;)V

    iput-object v0, p0, Lcom/google/android/location/copresence/d/n;->l:Lcom/google/android/location/copresence/k/f;

    .line 446
    new-instance v0, Lcom/google/android/location/copresence/d/p;

    const-string v1, "RevertedName"

    new-array v2, v4, [Lcom/google/android/location/copresence/k/f;

    invoke-direct {v0, p0, v1, v2}, Lcom/google/android/location/copresence/d/p;-><init>(Lcom/google/android/location/copresence/d/n;Ljava/lang/String;[Lcom/google/android/location/copresence/k/f;)V

    iput-object v0, p0, Lcom/google/android/location/copresence/d/n;->m:Lcom/google/android/location/copresence/k/f;

    .line 483
    new-instance v0, Lcom/google/android/location/copresence/d/q;

    const-string v1, "RevertedEnabled"

    new-array v2, v4, [Lcom/google/android/location/copresence/k/f;

    invoke-direct {v0, p0, v1, v2}, Lcom/google/android/location/copresence/d/q;-><init>(Lcom/google/android/location/copresence/d/n;Ljava/lang/String;[Lcom/google/android/location/copresence/k/f;)V

    iput-object v0, p0, Lcom/google/android/location/copresence/d/n;->n:Lcom/google/android/location/copresence/k/f;

    .line 531
    new-instance v0, Lcom/google/android/location/copresence/d/r;

    const-string v1, "RemovedUserSettings"

    new-array v2, v4, [Lcom/google/android/location/copresence/k/f;

    invoke-direct {v0, p0, v1, v2}, Lcom/google/android/location/copresence/d/r;-><init>(Lcom/google/android/location/copresence/d/n;Ljava/lang/String;[Lcom/google/android/location/copresence/k/f;)V

    iput-object v0, p0, Lcom/google/android/location/copresence/d/n;->q:Lcom/google/android/location/copresence/k/f;

    .line 552
    new-instance v0, Lcom/google/android/location/copresence/d/s;

    const-string v1, "Unmodified"

    const/4 v2, 0x4

    new-array v2, v2, [Lcom/google/android/location/copresence/k/f;

    iget-object v3, p0, Lcom/google/android/location/copresence/d/n;->k:Lcom/google/android/location/copresence/k/f;

    aput-object v3, v2, v4

    iget-object v3, p0, Lcom/google/android/location/copresence/d/n;->m:Lcom/google/android/location/copresence/k/f;

    aput-object v3, v2, v6

    iget-object v3, p0, Lcom/google/android/location/copresence/d/n;->q:Lcom/google/android/location/copresence/k/f;

    aput-object v3, v2, v5

    const/4 v3, 0x3

    iget-object v4, p0, Lcom/google/android/location/copresence/d/n;->n:Lcom/google/android/location/copresence/k/f;

    aput-object v4, v2, v3

    invoke-direct {v0, p0, v1, v2}, Lcom/google/android/location/copresence/d/s;-><init>(Lcom/google/android/location/copresence/d/n;Ljava/lang/String;[Lcom/google/android/location/copresence/k/f;)V

    iput-object v0, p0, Lcom/google/android/location/copresence/d/n;->o:Lcom/google/android/location/copresence/k/f;

    .line 42
    iput-object p1, p0, Lcom/google/android/location/copresence/d/n;->b:Lcom/google/android/location/copresence/d/e;

    .line 43
    iput-object p2, p0, Lcom/google/android/location/copresence/d/n;->c:Landroid/content/SharedPreferences;

    .line 44
    iget-object v0, p0, Lcom/google/android/location/copresence/d/n;->c:Landroid/content/SharedPreferences;

    const-string v1, "updated"

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iget-object v2, p0, Lcom/google/android/location/copresence/d/n;->c:Landroid/content/SharedPreferences;

    const-string v3, "updated"

    const-wide/16 v4, 0x0

    invoke-interface {v2, v3, v4, v5}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v2

    sub-long/2addr v0, v2

    sget-wide v2, Lcom/google/android/location/copresence/d/n;->p:J

    cmp-long v2, v0, v2

    if-lez v2, :cond_1

    sget-object v2, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v2, v0, v1}, Ljava/util/concurrent/TimeUnit;->toHours(J)J

    move-result-wide v0

    const/4 v2, 0x6

    invoke-static {v2}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v2

    if-eqz v2, :cond_0

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "BluetoothStates: Very old original state file: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "h"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->e(Ljava/lang/String;)I

    :cond_0
    invoke-static {v6}, Lcom/google/android/location/copresence/b/a;->a(I)V

    .line 45
    :cond_1
    return-void
.end method

.method static synthetic a(Landroid/content/SharedPreferences$Editor;Ljava/lang/String;)Z
    .locals 4

    .prologue
    .line 24
    const-string v0, "updated"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-interface {p0, v0, v2, v3}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v1, 0x3

    invoke-static {v1}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v1

    if-eqz v1, :cond_0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "BluetoothStates: Completed store to disk and "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/location/copresence/ag;->b(Ljava/lang/String;)I

    :cond_0
    :goto_0
    return v0

    :cond_1
    const/4 v1, 0x6

    invoke-static {v1}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v1

    if-eqz v1, :cond_0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "BluetoothStates: Failed to store to disk and "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/location/copresence/ag;->e(Ljava/lang/String;)I

    goto :goto_0
.end method
