.class public final Lcom/google/android/location/copresence/q/w;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Ljava/util/Set;

.field private final b:Lcom/google/android/location/copresence/ap;


# direct methods
.method public constructor <init>(Lcom/google/android/location/copresence/ap;)V
    .locals 1

    .prologue
    .line 44
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/location/copresence/q/w;->a:Ljava/util/Set;

    .line 45
    iput-object p1, p0, Lcom/google/android/location/copresence/q/w;->b:Lcom/google/android/location/copresence/ap;

    .line 46
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/location/copresence/q/ac;Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 66
    new-instance v0, Lcom/google/android/location/copresence/q/y;

    invoke-direct {v0, p1, p2}, Lcom/google/android/location/copresence/q/y;-><init>(Lcom/google/android/location/copresence/q/ac;Ljava/lang/Object;)V

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/google/android/location/copresence/q/w;->a(Lcom/google/android/location/copresence/q/x;Landroid/os/WorkSource;)V

    .line 68
    return-void
.end method

.method public final a(Lcom/google/android/location/copresence/q/x;Landroid/os/WorkSource;)V
    .locals 4

    .prologue
    .line 96
    iget-object v1, p0, Lcom/google/android/location/copresence/q/w;->a:Ljava/util/Set;

    monitor-enter v1

    .line 97
    :try_start_0
    new-instance v0, Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/google/android/location/copresence/q/w;->a:Ljava/util/Set;

    invoke-direct {v0, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 98
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 100
    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 101
    invoke-interface {p1, v1}, Lcom/google/android/location/copresence/q/x;->a(Ljava/lang/Object;)Ljava/lang/Runnable;

    move-result-object v1

    .line 102
    iget-object v2, p0, Lcom/google/android/location/copresence/q/w;->b:Lcom/google/android/location/copresence/ap;

    const/4 v3, 0x0

    invoke-virtual {v2, v1, v3}, Lcom/google/android/location/copresence/ap;->a(Ljava/lang/Runnable;Landroid/os/WorkSource;)V

    goto :goto_0

    .line 98
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 104
    :cond_0
    return-void
.end method

.method public final a(Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 49
    invoke-static {p1}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 51
    iget-object v1, p0, Lcom/google/android/location/copresence/q/w;->a:Ljava/util/Set;

    monitor-enter v1

    .line 52
    :try_start_0
    iget-object v0, p0, Lcom/google/android/location/copresence/q/w;->a:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 53
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method
