.class public final Lcom/google/android/location/places/ui/ac;
.super Landroid/support/v4/app/Fragment;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/location/places/ui/ab;
.implements Lcom/google/android/location/places/ui/ay;
.implements Lcom/google/android/location/places/ui/bh;
.implements Lcom/google/android/location/places/ui/o;


# instance fields
.field private A:Z

.field private B:Lcom/google/android/gms/maps/model/LatLngBounds;

.field private C:Ljava/lang/String;

.field private D:Lcom/google/android/gms/maps/model/LatLngBounds;

.field private E:Z

.field private a:Lcom/google/android/location/places/ui/s;

.field private b:Lcom/google/android/location/places/ui/r;

.field private c:Lcom/google/android/location/places/ui/av;

.field private d:Lcom/google/android/location/places/ui/am;

.field private e:Lcom/google/android/location/places/ui/MarkerMapFragment;

.field private f:Lcom/google/android/location/places/ui/be;

.field private g:Lcom/google/android/location/places/ui/k;

.field private h:Ljava/lang/String;

.field private i:Ljava/lang/String;

.field private j:Ljava/lang/String;

.field private k:Ljava/lang/String;

.field private l:Ljava/lang/String;

.field private m:Ljava/lang/String;

.field private n:Ljava/lang/String;

.field private o:Landroid/view/ViewGroup;

.field private p:Landroid/view/ViewGroup;

.field private q:Landroid/view/View;

.field private r:I

.field private s:I

.field private t:I

.field private u:I

.field private v:I

.field private w:I

.field private x:Z

.field private y:Z

.field private z:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 29
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    .line 90
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/location/places/ui/ac;->z:Z

    return-void
.end method

.method private a(IILjava/lang/Runnable;)V
    .locals 3

    .prologue
    .line 654
    invoke-virtual {p0}, Lcom/google/android/location/places/ui/ac;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/q;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/location/places/ui/ac;->p:Landroid/view/ViewGroup;

    const/4 v2, 0x0

    invoke-virtual {v0, p1, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/places/ui/ac;->q:Landroid/view/View;

    .line 655
    if-eqz p3, :cond_0

    .line 656
    iget-object v0, p0, Lcom/google/android/location/places/ui/ac;->q:Landroid/view/View;

    invoke-virtual {v0, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    new-instance v1, Lcom/google/android/location/places/ui/al;

    invoke-direct {v1, p0, p3}, Lcom/google/android/location/places/ui/al;-><init>(Lcom/google/android/location/places/ui/ac;Ljava/lang/Runnable;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 668
    :goto_0
    iget-object v0, p0, Lcom/google/android/location/places/ui/ac;->p:Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/google/android/location/places/ui/ac;->q:Landroid/view/View;

    iget-object v2, p0, Lcom/google/android/location/places/ui/ac;->p:Landroid/view/ViewGroup;

    invoke-virtual {v2}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v0, v1, v2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;I)V

    .line 670
    iget-object v0, p0, Lcom/google/android/location/places/ui/ac;->q:Landroid/view/View;

    const/16 v1, 0x20

    invoke-virtual {v0, v1}, Landroid/view/View;->sendAccessibilityEvent(I)V

    .line 671
    return-void

    .line 665
    :cond_0
    iget-object v0, p0, Lcom/google/android/location/places/ui/ac;->q:Landroid/view/View;

    invoke-virtual {v0, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method static synthetic a(Lcom/google/android/location/places/ui/ac;Ljava/lang/Runnable;)V
    .locals 2

    .prologue
    .line 29
    invoke-direct {p0}, Lcom/google/android/location/places/ui/ac;->p()V

    sget v0, Lcom/google/android/gms/l;->cX:I

    sget v1, Lcom/google/android/gms/j;->J:I

    invoke-direct {p0, v0, v1, p1}, Lcom/google/android/location/places/ui/ac;->a(IILjava/lang/Runnable;)V

    return-void
.end method

.method static synthetic a(Lcom/google/android/location/places/ui/ac;)Z
    .locals 1

    .prologue
    .line 29
    iget-boolean v0, p0, Lcom/google/android/location/places/ui/ac;->y:Z

    return v0
.end method

.method static synthetic b(Lcom/google/android/location/places/ui/ac;)Lcom/google/android/location/places/ui/k;
    .locals 1

    .prologue
    .line 29
    iget-object v0, p0, Lcom/google/android/location/places/ui/ac;->g:Lcom/google/android/location/places/ui/k;

    return-object v0
.end method

.method static synthetic b(Lcom/google/android/location/places/ui/ac;Ljava/lang/Runnable;)V
    .locals 2

    .prologue
    .line 29
    invoke-direct {p0}, Lcom/google/android/location/places/ui/ac;->p()V

    sget v0, Lcom/google/android/gms/l;->cY:I

    sget v1, Lcom/google/android/gms/j;->oz:I

    invoke-direct {p0, v0, v1, p1}, Lcom/google/android/location/places/ui/ac;->a(IILjava/lang/Runnable;)V

    return-void
.end method

.method static synthetic c(Lcom/google/android/location/places/ui/ac;)Z
    .locals 1

    .prologue
    .line 29
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/location/places/ui/ac;->y:Z

    return v0
.end method

.method static synthetic d(Lcom/google/android/location/places/ui/ac;)Lcom/google/android/location/places/ui/MarkerMapFragment;
    .locals 1

    .prologue
    .line 29
    iget-object v0, p0, Lcom/google/android/location/places/ui/ac;->e:Lcom/google/android/location/places/ui/MarkerMapFragment;

    return-object v0
.end method

.method static synthetic e(Lcom/google/android/location/places/ui/ac;)Z
    .locals 1

    .prologue
    .line 29
    iget-boolean v0, p0, Lcom/google/android/location/places/ui/ac;->A:Z

    return v0
.end method

.method static synthetic f(Lcom/google/android/location/places/ui/ac;)Lcom/google/android/location/places/ui/s;
    .locals 1

    .prologue
    .line 29
    iget-object v0, p0, Lcom/google/android/location/places/ui/ac;->a:Lcom/google/android/location/places/ui/s;

    return-object v0
.end method

.method static synthetic g(Lcom/google/android/location/places/ui/ac;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 29
    iget-object v0, p0, Lcom/google/android/location/places/ui/ac;->k:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic h(Lcom/google/android/location/places/ui/ac;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 29
    iget-object v0, p0, Lcom/google/android/location/places/ui/ac;->l:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic i(Lcom/google/android/location/places/ui/ac;)Lcom/google/android/location/places/ui/be;
    .locals 1

    .prologue
    .line 29
    iget-object v0, p0, Lcom/google/android/location/places/ui/ac;->f:Lcom/google/android/location/places/ui/be;

    return-object v0
.end method

.method public static j()Lcom/google/android/location/places/ui/ac;
    .locals 1

    .prologue
    .line 103
    new-instance v0, Lcom/google/android/location/places/ui/ac;

    invoke-direct {v0}, Lcom/google/android/location/places/ui/ac;-><init>()V

    .line 104
    return-object v0
.end method

.method static synthetic j(Lcom/google/android/location/places/ui/ac;)Z
    .locals 1

    .prologue
    .line 29
    iget-boolean v0, p0, Lcom/google/android/location/places/ui/ac;->z:Z

    return v0
.end method

.method static synthetic k(Lcom/google/android/location/places/ui/ac;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 29
    iget-object v0, p0, Lcom/google/android/location/places/ui/ac;->n:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic l(Lcom/google/android/location/places/ui/ac;)V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0}, Lcom/google/android/location/places/ui/ac;->p()V

    return-void
.end method

.method private m()V
    .locals 3

    .prologue
    .line 321
    iget-object v0, p0, Lcom/google/android/location/places/ui/ac;->a:Lcom/google/android/location/places/ui/s;

    iget-boolean v0, v0, Lcom/google/android/location/places/ui/s;->f:Z

    if-eqz v0, :cond_0

    .line 322
    iget-object v0, p0, Lcom/google/android/location/places/ui/ac;->e:Lcom/google/android/location/places/ui/MarkerMapFragment;

    invoke-virtual {v0}, Lcom/google/android/location/places/ui/MarkerMapFragment;->j()V

    .line 334
    :goto_0
    return-void

    .line 326
    :cond_0
    iget-object v0, p0, Lcom/google/android/location/places/ui/ac;->e:Lcom/google/android/location/places/ui/MarkerMapFragment;

    invoke-virtual {v0}, Lcom/google/android/location/places/ui/MarkerMapFragment;->j()V

    .line 327
    iget-object v0, p0, Lcom/google/android/location/places/ui/ac;->g:Lcom/google/android/location/places/ui/k;

    iget-object v1, p0, Lcom/google/android/location/places/ui/ac;->i:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/location/places/ui/k;->b(Ljava/lang/String;)V

    .line 329
    iget-boolean v0, p0, Lcom/google/android/location/places/ui/ac;->E:Z

    if-eqz v0, :cond_1

    .line 330
    iget-object v0, p0, Lcom/google/android/location/places/ui/ac;->c:Lcom/google/android/location/places/ui/av;

    invoke-virtual {v0}, Lcom/google/android/location/places/ui/av;->e()V

    goto :goto_0

    .line 332
    :cond_1
    iget-object v0, p0, Lcom/google/android/location/places/ui/ac;->c:Lcom/google/android/location/places/ui/av;

    const-string v1, "*"

    iget-object v2, p0, Lcom/google/android/location/places/ui/ac;->e:Lcom/google/android/location/places/ui/MarkerMapFragment;

    invoke-virtual {v2}, Lcom/google/android/location/places/ui/MarkerMapFragment;->g()Lcom/google/android/gms/maps/model/LatLngBounds;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/location/places/ui/av;->a(Ljava/lang/String;Lcom/google/android/gms/maps/model/LatLngBounds;)V

    goto :goto_0
.end method

.method static synthetic m(Lcom/google/android/location/places/ui/ac;)Z
    .locals 1

    .prologue
    .line 29
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/location/places/ui/ac;->A:Z

    return v0
.end method

.method private n()V
    .locals 6

    .prologue
    .line 338
    iget-object v0, p0, Lcom/google/android/location/places/ui/ac;->a:Lcom/google/android/location/places/ui/s;

    iget-boolean v0, v0, Lcom/google/android/location/places/ui/s;->h:Z

    if-eqz v0, :cond_0

    .line 363
    :goto_0
    return-void

    .line 342
    :cond_0
    iget-boolean v0, p0, Lcom/google/android/location/places/ui/ac;->x:Z

    if-eqz v0, :cond_1

    .line 343
    new-instance v1, Lcom/google/android/location/places/ui/ad;

    invoke-direct {v1, p0}, Lcom/google/android/location/places/ui/ad;-><init>(Lcom/google/android/location/places/ui/ac;)V

    .line 356
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/location/places/ui/ac;->y:Z

    .line 357
    new-instance v2, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-direct {v2, v0}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    sget-object v0, Lcom/google/android/location/x;->S:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    int-to-long v4, v0

    invoke-virtual {v2, v1, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 361
    :cond_1
    iget-object v0, p0, Lcom/google/android/location/places/ui/ac;->e:Lcom/google/android/location/places/ui/MarkerMapFragment;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/location/places/ui/MarkerMapFragment;->b(Z)V

    .line 362
    iget-object v0, p0, Lcom/google/android/location/places/ui/ac;->c:Lcom/google/android/location/places/ui/av;

    iget-object v1, p0, Lcom/google/android/location/places/ui/ac;->e:Lcom/google/android/location/places/ui/MarkerMapFragment;

    invoke-virtual {v1}, Lcom/google/android/location/places/ui/MarkerMapFragment;->f()Lcom/google/android/gms/maps/model/LatLng;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/location/places/ui/av;->a(Lcom/google/android/gms/maps/model/LatLng;)V

    goto :goto_0
.end method

.method static synthetic n(Lcom/google/android/location/places/ui/ac;)V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0}, Lcom/google/android/location/places/ui/ac;->o()V

    return-void
.end method

.method private o()V
    .locals 3

    .prologue
    .line 367
    iget-object v0, p0, Lcom/google/android/location/places/ui/ac;->e:Lcom/google/android/location/places/ui/MarkerMapFragment;

    invoke-virtual {v0}, Lcom/google/android/location/places/ui/MarkerMapFragment;->j()V

    .line 368
    iget-object v0, p0, Lcom/google/android/location/places/ui/ac;->e:Lcom/google/android/location/places/ui/MarkerMapFragment;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/location/places/ui/MarkerMapFragment;->b(Z)V

    .line 369
    iget-object v0, p0, Lcom/google/android/location/places/ui/ac;->g:Lcom/google/android/location/places/ui/k;

    iget-object v1, p0, Lcom/google/android/location/places/ui/ac;->j:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/location/places/ui/k;->b(Ljava/lang/String;)V

    .line 370
    iget-object v0, p0, Lcom/google/android/location/places/ui/ac;->e:Lcom/google/android/location/places/ui/MarkerMapFragment;

    invoke-virtual {v0}, Lcom/google/android/location/places/ui/MarkerMapFragment;->g()Lcom/google/android/gms/maps/model/LatLngBounds;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/places/ui/ac;->D:Lcom/google/android/gms/maps/model/LatLngBounds;

    .line 371
    iget-object v0, p0, Lcom/google/android/location/places/ui/ac;->f:Lcom/google/android/location/places/ui/be;

    invoke-virtual {v0}, Lcom/google/android/location/places/ui/be;->b()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/places/ui/ac;->C:Ljava/lang/String;

    .line 372
    iget-object v0, p0, Lcom/google/android/location/places/ui/ac;->c:Lcom/google/android/location/places/ui/av;

    iget-object v1, p0, Lcom/google/android/location/places/ui/ac;->f:Lcom/google/android/location/places/ui/be;

    invoke-virtual {v1}, Lcom/google/android/location/places/ui/be;->b()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/location/places/ui/ac;->e:Lcom/google/android/location/places/ui/MarkerMapFragment;

    invoke-virtual {v2}, Lcom/google/android/location/places/ui/MarkerMapFragment;->g()Lcom/google/android/gms/maps/model/LatLngBounds;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/location/places/ui/av;->a(Ljava/lang/String;Lcom/google/android/gms/maps/model/LatLngBounds;)V

    .line 373
    return-void
.end method

.method private p()V
    .locals 2

    .prologue
    .line 646
    iget-object v0, p0, Lcom/google/android/location/places/ui/ac;->q:Landroid/view/View;

    if-nez v0, :cond_0

    .line 651
    :goto_0
    return-void

    .line 649
    :cond_0
    iget-object v0, p0, Lcom/google/android/location/places/ui/ac;->p:Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/google/android/location/places/ui/ac;->q:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 650
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/location/places/ui/ac;->q:Landroid/view/View;

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 619
    iget-object v0, p0, Lcom/google/android/location/places/ui/ac;->d:Lcom/google/android/location/places/ui/am;

    invoke-interface {v0}, Lcom/google/android/location/places/ui/am;->b()V

    .line 620
    return-void
.end method

.method public final a(Lcom/google/android/gms/location/places/UserAddedPlace;)V
    .locals 0

    .prologue
    .line 613
    return-void
.end method

.method public final a(Lcom/google/android/gms/location/places/f;)V
    .locals 1

    .prologue
    .line 385
    iget-object v0, p0, Lcom/google/android/location/places/ui/ac;->g:Lcom/google/android/location/places/ui/k;

    invoke-virtual {v0, p1}, Lcom/google/android/location/places/ui/k;->a(Lcom/google/android/gms/location/places/f;)V

    .line 386
    return-void
.end method

.method public final a(Lcom/google/android/gms/location/places/f;II)V
    .locals 2

    .prologue
    .line 471
    const/4 v0, 0x2

    if-ne p2, v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/location/places/ui/ac;->A:Z

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 473
    const/4 p2, 0x3

    .line 476
    :cond_0
    iget-object v0, p0, Lcom/google/android/location/places/ui/ac;->d:Lcom/google/android/location/places/ui/am;

    iget-object v1, p0, Lcom/google/android/location/places/ui/ac;->f:Lcom/google/android/location/places/ui/be;

    invoke-virtual {v1}, Lcom/google/android/location/places/ui/be;->b()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, p1, p2, p3, v1}, Lcom/google/android/location/places/ui/am;->a(Lcom/google/android/gms/location/places/f;IILjava/lang/String;)V

    .line 477
    return-void
.end method

.method public final a(Lcom/google/android/gms/maps/model/LatLng;)V
    .locals 1

    .prologue
    .line 402
    iget-boolean v0, p0, Lcom/google/android/location/places/ui/ac;->z:Z

    if-eqz v0, :cond_0

    .line 403
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/location/places/ui/ac;->z:Z

    .line 404
    iget-object v0, p0, Lcom/google/android/location/places/ui/ac;->e:Lcom/google/android/location/places/ui/MarkerMapFragment;

    invoke-virtual {v0, p1}, Lcom/google/android/location/places/ui/MarkerMapFragment;->a(Lcom/google/android/gms/maps/model/LatLng;)V

    .line 408
    :goto_0
    return-void

    .line 406
    :cond_0
    iget-object v0, p0, Lcom/google/android/location/places/ui/ac;->e:Lcom/google/android/location/places/ui/MarkerMapFragment;

    invoke-virtual {v0, p1}, Lcom/google/android/location/places/ui/MarkerMapFragment;->b(Lcom/google/android/gms/maps/model/LatLng;)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 428
    iget-object v0, p0, Lcom/google/android/location/places/ui/ac;->f:Lcom/google/android/location/places/ui/be;

    invoke-virtual {v0}, Lcom/google/android/location/places/ui/be;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/google/android/location/x;->Q:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 429
    iget-object v0, p0, Lcom/google/android/location/places/ui/ac;->c:Lcom/google/android/location/places/ui/av;

    iget-object v1, p0, Lcom/google/android/location/places/ui/ac;->e:Lcom/google/android/location/places/ui/MarkerMapFragment;

    invoke-virtual {v1}, Lcom/google/android/location/places/ui/MarkerMapFragment;->g()Lcom/google/android/gms/maps/model/LatLngBounds;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/android/location/places/ui/av;->b(Ljava/lang/String;Lcom/google/android/gms/maps/model/LatLngBounds;)V

    .line 431
    :cond_0
    return-void
.end method

.method public final a([Lcom/google/android/gms/location/places/AutocompletePrediction;)V
    .locals 2

    .prologue
    .line 518
    invoke-virtual {p0}, Lcom/google/android/location/places/ui/ac;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    new-instance v1, Lcom/google/android/location/places/ui/ag;

    invoke-direct {v1, p0, p1}, Lcom/google/android/location/places/ui/ag;-><init>(Lcom/google/android/location/places/ui/ac;[Lcom/google/android/gms/location/places/AutocompletePrediction;)V

    invoke-virtual {v0, v1}, Landroid/support/v4/app/q;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 526
    return-void
.end method

.method public final a([Lcom/google/android/gms/location/places/f;)V
    .locals 2

    .prologue
    .line 497
    invoke-virtual {p0}, Lcom/google/android/location/places/ui/ac;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    new-instance v1, Lcom/google/android/location/places/ui/af;

    invoke-direct {v1, p0, p1}, Lcom/google/android/location/places/ui/af;-><init>(Lcom/google/android/location/places/ui/ac;[Lcom/google/android/gms/location/places/f;)V

    invoke-virtual {v0, v1}, Landroid/support/v4/app/q;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 513
    return-void
.end method

.method public final b()V
    .locals 10

    .prologue
    const/4 v7, 0x0

    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 282
    iput-boolean v7, p0, Lcom/google/android/location/places/ui/ac;->E:Z

    .line 283
    iget-object v0, p0, Lcom/google/android/location/places/ui/ac;->f:Lcom/google/android/location/places/ui/be;

    invoke-virtual {v0}, Lcom/google/android/location/places/ui/be;->g()V

    .line 284
    iget-object v0, p0, Lcom/google/android/location/places/ui/ac;->f:Lcom/google/android/location/places/ui/be;

    invoke-virtual {v0}, Lcom/google/android/location/places/ui/be;->e()V

    .line 285
    iget-object v0, p0, Lcom/google/android/location/places/ui/ac;->f:Lcom/google/android/location/places/ui/be;

    invoke-virtual {v0}, Lcom/google/android/location/places/ui/be;->f()V

    .line 286
    iput-boolean v1, p0, Lcom/google/android/location/places/ui/ac;->x:Z

    .line 287
    iget-object v9, p0, Lcom/google/android/location/places/ui/ac;->g:Lcom/google/android/location/places/ui/k;

    iget-boolean v0, v9, Lcom/google/android/location/places/ui/k;->p:Z

    if-nez v0, :cond_0

    iput-boolean v1, v9, Lcom/google/android/location/places/ui/k;->p:Z

    iget v0, v9, Lcom/google/android/location/places/ui/k;->o:I

    iget v3, v9, Lcom/google/android/location/places/ui/k;->m:I

    add-int/2addr v0, v3

    const/high16 v3, 0x42900000    # 72.0f

    invoke-virtual {v9}, Lcom/google/android/location/places/ui/k;->getActivity()Landroid/support/v4/app/q;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/android/location/places/ui/bm;->a(FLandroid/content/Context;)I

    move-result v3

    sub-int v3, v0, v3

    new-instance v0, Landroid/view/animation/TranslateAnimation;

    iget-object v4, v9, Lcom/google/android/location/places/ui/k;->j:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getTop()I

    move-result v4

    sub-int/2addr v3, v4

    int-to-float v8, v3

    move v3, v1

    move v4, v2

    move v5, v1

    move v6, v2

    invoke-direct/range {v0 .. v8}, Landroid/view/animation/TranslateAnimation;-><init>(IFIFIFIF)V

    invoke-virtual {v0, v1}, Landroid/view/animation/TranslateAnimation;->setFillAfter(Z)V

    const-wide/16 v2, 0x190

    invoke-virtual {v0, v2, v3}, Landroid/view/animation/TranslateAnimation;->setDuration(J)V

    iget-object v1, v9, Lcom/google/android/location/places/ui/k;->i:Landroid/view/View;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    iget-object v1, v9, Lcom/google/android/location/places/ui/k;->i:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 288
    :cond_0
    iget-object v0, p0, Lcom/google/android/location/places/ui/ac;->e:Lcom/google/android/location/places/ui/MarkerMapFragment;

    invoke-virtual {v0}, Lcom/google/android/location/places/ui/MarkerMapFragment;->d()V

    .line 289
    iget-object v0, p0, Lcom/google/android/location/places/ui/ac;->e:Lcom/google/android/location/places/ui/MarkerMapFragment;

    iget v1, p0, Lcom/google/android/location/places/ui/ac;->u:I

    iget v2, p0, Lcom/google/android/location/places/ui/ac;->w:I

    sub-int/2addr v1, v2

    invoke-virtual {v0, v1}, Lcom/google/android/location/places/ui/MarkerMapFragment;->a(I)V

    .line 290
    return-void
.end method

.method public final b(Lcom/google/android/gms/location/places/f;)V
    .locals 2

    .prologue
    .line 483
    invoke-virtual {p0}, Lcom/google/android/location/places/ui/ac;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    new-instance v1, Lcom/google/android/location/places/ui/ae;

    invoke-direct {v1, p0, p1}, Lcom/google/android/location/places/ui/ae;-><init>(Lcom/google/android/location/places/ui/ac;Lcom/google/android/gms/location/places/f;)V

    invoke-virtual {v0, v1}, Landroid/support/v4/app/q;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 492
    return-void
.end method

.method public final b([Lcom/google/android/gms/location/places/f;)V
    .locals 3

    .prologue
    .line 530
    iget-object v0, p0, Lcom/google/android/location/places/ui/ac;->c:Lcom/google/android/location/places/ui/av;

    invoke-virtual {v0}, Lcom/google/android/location/places/ui/av;->b()Ljava/lang/String;

    move-result-object v0

    .line 532
    invoke-virtual {p0}, Lcom/google/android/location/places/ui/ac;->getActivity()Landroid/support/v4/app/q;

    move-result-object v1

    new-instance v2, Lcom/google/android/location/places/ui/ah;

    invoke-direct {v2, p0, p1, v0}, Lcom/google/android/location/places/ui/ah;-><init>(Lcom/google/android/location/places/ui/ac;[Lcom/google/android/gms/location/places/f;Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Landroid/support/v4/app/q;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 577
    return-void
.end method

.method public final c()V
    .locals 10

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    const/4 v5, 0x0

    .line 294
    iput-boolean v5, p0, Lcom/google/android/location/places/ui/ac;->x:Z

    .line 295
    iget-object v0, p0, Lcom/google/android/location/places/ui/ac;->e:Lcom/google/android/location/places/ui/MarkerMapFragment;

    invoke-virtual {v0}, Lcom/google/android/location/places/ui/MarkerMapFragment;->e()V

    .line 296
    iget-object v9, p0, Lcom/google/android/location/places/ui/ac;->g:Lcom/google/android/location/places/ui/k;

    iget-boolean v0, v9, Lcom/google/android/location/places/ui/k;->p:Z

    if-eqz v0, :cond_0

    iget v0, v9, Lcom/google/android/location/places/ui/k;->o:I

    iget v3, v9, Lcom/google/android/location/places/ui/k;->m:I

    add-int/2addr v0, v3

    const/high16 v3, 0x42900000    # 72.0f

    invoke-virtual {v9}, Lcom/google/android/location/places/ui/k;->getActivity()Landroid/support/v4/app/q;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/android/location/places/ui/bm;->a(FLandroid/content/Context;)I

    move-result v3

    sub-int v3, v0, v3

    new-instance v0, Landroid/view/animation/TranslateAnimation;

    iget-object v4, v9, Lcom/google/android/location/places/ui/k;->j:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getTop()I

    move-result v4

    sub-int/2addr v3, v4

    int-to-float v6, v3

    move v3, v1

    move v4, v2

    move v7, v1

    move v8, v2

    invoke-direct/range {v0 .. v8}, Landroid/view/animation/TranslateAnimation;-><init>(IFIFIFIF)V

    invoke-virtual {v0, v1}, Landroid/view/animation/TranslateAnimation;->setFillAfter(Z)V

    const-wide/16 v2, 0x190

    invoke-virtual {v0, v2, v3}, Landroid/view/animation/TranslateAnimation;->setDuration(J)V

    iget-object v1, v9, Lcom/google/android/location/places/ui/k;->i:Landroid/view/View;

    invoke-virtual {v1, v5}, Landroid/view/View;->setVisibility(I)V

    iget-object v1, v9, Lcom/google/android/location/places/ui/k;->i:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    iput-boolean v5, v9, Lcom/google/android/location/places/ui/k;->p:Z

    .line 297
    :cond_0
    iget-object v0, p0, Lcom/google/android/location/places/ui/ac;->e:Lcom/google/android/location/places/ui/MarkerMapFragment;

    iget v1, p0, Lcom/google/android/location/places/ui/ac;->u:I

    iget v2, p0, Lcom/google/android/location/places/ui/ac;->v:I

    sub-int/2addr v1, v2

    invoke-virtual {v0, v1}, Lcom/google/android/location/places/ui/MarkerMapFragment;->a(I)V

    .line 298
    iget-object v0, p0, Lcom/google/android/location/places/ui/ac;->f:Lcom/google/android/location/places/ui/be;

    invoke-virtual {v0}, Lcom/google/android/location/places/ui/be;->e()V

    .line 299
    return-void
.end method

.method public final c(Lcom/google/android/gms/location/places/f;)V
    .locals 0

    .prologue
    .line 608
    return-void
.end method

.method public final d()V
    .locals 1

    .prologue
    .line 304
    iget-object v0, p0, Lcom/google/android/location/places/ui/ac;->e:Lcom/google/android/location/places/ui/MarkerMapFragment;

    invoke-virtual {v0}, Lcom/google/android/location/places/ui/MarkerMapFragment;->g()Lcom/google/android/gms/maps/model/LatLngBounds;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/places/ui/ac;->B:Lcom/google/android/gms/maps/model/LatLngBounds;

    .line 305
    iget-boolean v0, p0, Lcom/google/android/location/places/ui/ac;->z:Z

    if-eqz v0, :cond_1

    .line 317
    :cond_0
    :goto_0
    return-void

    .line 309
    :cond_1
    iget-object v0, p0, Lcom/google/android/location/places/ui/ac;->e:Lcom/google/android/location/places/ui/MarkerMapFragment;

    invoke-virtual {v0}, Lcom/google/android/location/places/ui/MarkerMapFragment;->i()V

    .line 311
    iget-boolean v0, p0, Lcom/google/android/location/places/ui/ac;->A:Z

    if-nez v0, :cond_0

    .line 312
    invoke-direct {p0}, Lcom/google/android/location/places/ui/ac;->n()V

    .line 313
    iget-boolean v0, p0, Lcom/google/android/location/places/ui/ac;->x:Z

    if-nez v0, :cond_0

    .line 314
    invoke-direct {p0}, Lcom/google/android/location/places/ui/ac;->m()V

    goto :goto_0
.end method

.method public final e()V
    .locals 2

    .prologue
    .line 390
    iget-boolean v0, p0, Lcom/google/android/location/places/ui/ac;->A:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/location/places/ui/ac;->a:Lcom/google/android/location/places/ui/s;

    iget-boolean v0, v0, Lcom/google/android/location/places/ui/s;->h:Z

    if-nez v0, :cond_1

    .line 392
    iget-object v0, p0, Lcom/google/android/location/places/ui/ac;->e:Lcom/google/android/location/places/ui/MarkerMapFragment;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/location/places/ui/MarkerMapFragment;->b(Z)V

    .line 393
    iget-object v0, p0, Lcom/google/android/location/places/ui/ac;->c:Lcom/google/android/location/places/ui/av;

    iget-object v1, p0, Lcom/google/android/location/places/ui/ac;->e:Lcom/google/android/location/places/ui/MarkerMapFragment;

    invoke-virtual {v1}, Lcom/google/android/location/places/ui/MarkerMapFragment;->f()Lcom/google/android/gms/maps/model/LatLng;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/location/places/ui/av;->a(Lcom/google/android/gms/maps/model/LatLng;)V

    .line 398
    :cond_0
    :goto_0
    return-void

    .line 394
    :cond_1
    iget-object v0, p0, Lcom/google/android/location/places/ui/ac;->a:Lcom/google/android/location/places/ui/s;

    iget-boolean v0, v0, Lcom/google/android/location/places/ui/s;->h:Z

    if-eqz v0, :cond_0

    .line 395
    iget-object v0, p0, Lcom/google/android/location/places/ui/ac;->e:Lcom/google/android/location/places/ui/MarkerMapFragment;

    invoke-virtual {v0}, Lcom/google/android/location/places/ui/MarkerMapFragment;->k()V

    .line 396
    iget-object v0, p0, Lcom/google/android/location/places/ui/ac;->g:Lcom/google/android/location/places/ui/k;

    iget-object v1, p0, Lcom/google/android/location/places/ui/ac;->k:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/location/places/ui/k;->a(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final f()V
    .locals 1

    .prologue
    .line 412
    iget-boolean v0, p0, Lcom/google/android/location/places/ui/ac;->z:Z

    if-eqz v0, :cond_0

    .line 413
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/location/places/ui/ac;->z:Z

    .line 414
    iget-object v0, p0, Lcom/google/android/location/places/ui/ac;->e:Lcom/google/android/location/places/ui/MarkerMapFragment;

    invoke-virtual {v0}, Lcom/google/android/location/places/ui/MarkerMapFragment;->i()V

    .line 415
    invoke-direct {p0}, Lcom/google/android/location/places/ui/ac;->n()V

    .line 416
    invoke-direct {p0}, Lcom/google/android/location/places/ui/ac;->m()V

    .line 418
    :cond_0
    return-void
.end method

.method public final g()V
    .locals 1

    .prologue
    .line 422
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/location/places/ui/ac;->E:Z

    .line 423
    return-void
.end method

.method public final h()V
    .locals 2

    .prologue
    .line 582
    invoke-virtual {p0}, Lcom/google/android/location/places/ui/ac;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    new-instance v1, Lcom/google/android/location/places/ui/aj;

    invoke-direct {v1, p0}, Lcom/google/android/location/places/ui/aj;-><init>(Lcom/google/android/location/places/ui/ac;)V

    invoke-virtual {v0, v1}, Landroid/support/v4/app/q;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 603
    return-void
.end method

.method public final i()V
    .locals 1

    .prologue
    .line 462
    iget-boolean v0, p0, Lcom/google/android/location/places/ui/ac;->A:Z

    if-nez v0, :cond_0

    .line 463
    invoke-direct {p0}, Lcom/google/android/location/places/ui/ac;->n()V

    .line 464
    invoke-direct {p0}, Lcom/google/android/location/places/ui/ac;->m()V

    .line 466
    :cond_0
    return-void
.end method

.method public final k()V
    .locals 1

    .prologue
    .line 437
    iget-boolean v0, p0, Lcom/google/android/location/places/ui/ac;->z:Z

    if-eqz v0, :cond_0

    .line 438
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/location/places/ui/ac;->z:Z

    .line 439
    iget-object v0, p0, Lcom/google/android/location/places/ui/ac;->e:Lcom/google/android/location/places/ui/MarkerMapFragment;

    invoke-virtual {v0}, Lcom/google/android/location/places/ui/MarkerMapFragment;->i()V

    .line 442
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/location/places/ui/ac;->A:Z

    .line 443
    invoke-direct {p0}, Lcom/google/android/location/places/ui/ac;->p()V

    .line 444
    invoke-direct {p0}, Lcom/google/android/location/places/ui/ac;->o()V

    .line 445
    return-void
.end method

.method public final l()V
    .locals 1

    .prologue
    .line 451
    iget-boolean v0, p0, Lcom/google/android/location/places/ui/ac;->A:Z

    if-eqz v0, :cond_0

    .line 452
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/location/places/ui/ac;->A:Z

    .line 453
    invoke-direct {p0}, Lcom/google/android/location/places/ui/ac;->p()V

    .line 454
    invoke-direct {p0}, Lcom/google/android/location/places/ui/ac;->n()V

    .line 455
    invoke-direct {p0}, Lcom/google/android/location/places/ui/ac;->m()V

    .line 457
    :cond_0
    return-void
.end method

.method public final onActivityCreated(Landroid/os/Bundle;)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 128
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 130
    invoke-virtual {p0}, Lcom/google/android/location/places/ui/ac;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/places/ui/PlacePickerActivity;

    invoke-virtual {v0}, Lcom/google/android/location/places/ui/PlacePickerActivity;->e()Lcom/google/android/location/places/ui/av;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/places/ui/ac;->c:Lcom/google/android/location/places/ui/av;

    .line 131
    invoke-virtual {p0}, Lcom/google/android/location/places/ui/ac;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/places/ui/PlacePickerActivity;

    invoke-virtual {v0}, Lcom/google/android/location/places/ui/PlacePickerActivity;->d()Lcom/google/android/location/places/ui/r;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/places/ui/ac;->b:Lcom/google/android/location/places/ui/r;

    .line 132
    invoke-virtual {p0}, Lcom/google/android/location/places/ui/ac;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/places/ui/PlacePickerActivity;

    invoke-virtual {v0}, Lcom/google/android/location/places/ui/PlacePickerActivity;->c()Lcom/google/android/location/places/ui/s;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/places/ui/ac;->a:Lcom/google/android/location/places/ui/s;

    .line 134
    sget v0, Lcom/google/android/gms/p;->sB:I

    invoke-virtual {p0, v0}, Lcom/google/android/location/places/ui/ac;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/places/ui/ac;->h:Ljava/lang/String;

    .line 135
    sget v0, Lcom/google/android/gms/p;->sC:I

    invoke-virtual {p0, v0}, Lcom/google/android/location/places/ui/ac;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/places/ui/ac;->i:Ljava/lang/String;

    .line 136
    sget v0, Lcom/google/android/gms/p;->sy:I

    invoke-virtual {p0, v0}, Lcom/google/android/location/places/ui/ac;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/places/ui/ac;->j:Ljava/lang/String;

    .line 137
    sget v0, Lcom/google/android/gms/p;->sz:I

    invoke-virtual {p0, v0}, Lcom/google/android/location/places/ui/ac;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/places/ui/ac;->k:Ljava/lang/String;

    .line 138
    sget v0, Lcom/google/android/gms/p;->sv:I

    invoke-virtual {p0, v0}, Lcom/google/android/location/places/ui/ac;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/places/ui/ac;->l:Ljava/lang/String;

    .line 139
    sget v0, Lcom/google/android/gms/p;->sw:I

    invoke-virtual {p0, v0}, Lcom/google/android/location/places/ui/ac;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/places/ui/ac;->m:Ljava/lang/String;

    .line 140
    sget v0, Lcom/google/android/gms/p;->sx:I

    invoke-virtual {p0, v0}, Lcom/google/android/location/places/ui/ac;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/places/ui/ac;->n:Ljava/lang/String;

    .line 143
    invoke-virtual {p0}, Lcom/google/android/location/places/ui/ac;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    sget v1, Lcom/google/android/gms/j;->om:I

    invoke-virtual {v0, v1}, Landroid/support/v4/app/q;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/google/android/location/places/ui/ac;->o:Landroid/view/ViewGroup;

    .line 144
    iget-object v0, p0, Lcom/google/android/location/places/ui/ac;->o:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 145
    iget v1, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    iget-object v2, p0, Lcom/google/android/location/places/ui/ac;->o:Landroid/view/ViewGroup;

    invoke-virtual {v2}, Landroid/view/ViewGroup;->getPaddingLeft()I

    move-result v2

    sub-int/2addr v1, v2

    iget-object v2, p0, Lcom/google/android/location/places/ui/ac;->o:Landroid/view/ViewGroup;

    invoke-virtual {v2}, Landroid/view/ViewGroup;->getPaddingRight()I

    move-result v2

    sub-int/2addr v1, v2

    iput v1, p0, Lcom/google/android/location/places/ui/ac;->r:I

    .line 146
    iget v0, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    iget-object v1, p0, Lcom/google/android/location/places/ui/ac;->o:Landroid/view/ViewGroup;

    invoke-virtual {v1}, Landroid/view/ViewGroup;->getPaddingTop()I

    move-result v1

    sub-int/2addr v0, v1

    iget-object v1, p0, Lcom/google/android/location/places/ui/ac;->o:Landroid/view/ViewGroup;

    invoke-virtual {v1}, Landroid/view/ViewGroup;->getPaddingBottom()I

    move-result v1

    sub-int/2addr v0, v1

    iput v0, p0, Lcom/google/android/location/places/ui/ac;->s:I

    .line 147
    iget v0, p0, Lcom/google/android/location/places/ui/ac;->r:I

    iput v0, p0, Lcom/google/android/location/places/ui/ac;->t:I

    .line 148
    iget v0, p0, Lcom/google/android/location/places/ui/ac;->s:I

    iput v0, p0, Lcom/google/android/location/places/ui/ac;->u:I

    .line 149
    iget v0, p0, Lcom/google/android/location/places/ui/ac;->u:I

    const/high16 v1, 0x42900000    # 72.0f

    invoke-virtual {p0}, Lcom/google/android/location/places/ui/ac;->getActivity()Landroid/support/v4/app/q;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/location/places/ui/bm;->a(FLandroid/content/Context;)I

    move-result v1

    sub-int/2addr v0, v1

    iput v0, p0, Lcom/google/android/location/places/ui/ac;->w:I

    .line 151
    iget-object v0, p0, Lcom/google/android/location/places/ui/ac;->a:Lcom/google/android/location/places/ui/s;

    iget-boolean v0, v0, Lcom/google/android/location/places/ui/s;->f:Z

    if-eqz v0, :cond_2

    .line 152
    iget v0, p0, Lcom/google/android/location/places/ui/ac;->w:I

    iput v0, p0, Lcom/google/android/location/places/ui/ac;->v:I

    .line 158
    :goto_0
    invoke-virtual {p0}, Lcom/google/android/location/places/ui/ac;->getFragmentManager()Landroid/support/v4/app/v;

    move-result-object v0

    sget v1, Lcom/google/android/gms/j;->os:I

    invoke-virtual {v0, v1}, Landroid/support/v4/app/v;->a(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/places/ui/MarkerMapFragment;

    iput-object v0, p0, Lcom/google/android/location/places/ui/ac;->e:Lcom/google/android/location/places/ui/MarkerMapFragment;

    .line 160
    const/high16 v0, 0x42400000    # 48.0f

    invoke-virtual {p0}, Lcom/google/android/location/places/ui/ac;->getActivity()Landroid/support/v4/app/q;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/location/places/ui/bm;->a(FLandroid/content/Context;)I

    move-result v0

    .line 161
    iget-object v1, p0, Lcom/google/android/location/places/ui/ac;->e:Lcom/google/android/location/places/ui/MarkerMapFragment;

    iget v2, p0, Lcom/google/android/location/places/ui/ac;->t:I

    iget v3, p0, Lcom/google/android/location/places/ui/ac;->u:I

    iget v4, p0, Lcom/google/android/location/places/ui/ac;->u:I

    iget v5, p0, Lcom/google/android/location/places/ui/ac;->v:I

    sub-int/2addr v4, v5

    invoke-virtual {v1, v2, v3, v0, v4}, Lcom/google/android/location/places/ui/MarkerMapFragment;->a(IIII)Lcom/google/android/location/places/ui/MarkerMapFragment;

    .line 163
    iget-object v0, p0, Lcom/google/android/location/places/ui/ac;->e:Lcom/google/android/location/places/ui/MarkerMapFragment;

    iget-object v1, p0, Lcom/google/android/location/places/ui/ac;->a:Lcom/google/android/location/places/ui/s;

    iget-object v1, v1, Lcom/google/android/location/places/ui/s;->a:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/location/places/ui/ac;->a:Lcom/google/android/location/places/ui/s;

    iget v2, v2, Lcom/google/android/location/places/ui/s;->m:I

    iget-object v3, p0, Lcom/google/android/location/places/ui/ac;->a:Lcom/google/android/location/places/ui/s;

    iget v3, v3, Lcom/google/android/location/places/ui/s;->n:I

    iget-object v4, p0, Lcom/google/android/location/places/ui/ac;->a:Lcom/google/android/location/places/ui/s;

    iget v4, v4, Lcom/google/android/location/places/ui/s;->o:I

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/google/android/location/places/ui/MarkerMapFragment;->a(Ljava/lang/String;III)V

    .line 167
    invoke-virtual {p0}, Lcom/google/android/location/places/ui/ac;->getChildFragmentManager()Landroid/support/v4/app/v;

    move-result-object v0

    sget v1, Lcom/google/android/gms/j;->oj:I

    invoke-virtual {v0, v1}, Landroid/support/v4/app/v;->a(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/places/ui/k;

    iput-object v0, p0, Lcom/google/android/location/places/ui/ac;->g:Lcom/google/android/location/places/ui/k;

    .line 169
    iget-object v0, p0, Lcom/google/android/location/places/ui/ac;->g:Lcom/google/android/location/places/ui/k;

    if-nez v0, :cond_0

    .line 170
    new-instance v0, Lcom/google/android/location/places/ui/k;

    invoke-direct {v0}, Lcom/google/android/location/places/ui/k;-><init>()V

    iput-object v0, p0, Lcom/google/android/location/places/ui/ac;->g:Lcom/google/android/location/places/ui/k;

    .line 171
    invoke-virtual {p0}, Lcom/google/android/location/places/ui/ac;->getChildFragmentManager()Landroid/support/v4/app/v;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/v;->a()Landroid/support/v4/app/aj;

    move-result-object v0

    sget v1, Lcom/google/android/gms/j;->oj:I

    iget-object v2, p0, Lcom/google/android/location/places/ui/ac;->g:Lcom/google/android/location/places/ui/k;

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/app/aj;->a(ILandroid/support/v4/app/Fragment;)Landroid/support/v4/app/aj;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/aj;->a()I

    .line 174
    :cond_0
    iget-object v0, p0, Lcom/google/android/location/places/ui/ac;->g:Lcom/google/android/location/places/ui/k;

    iget-object v1, p0, Lcom/google/android/location/places/ui/ac;->b:Lcom/google/android/location/places/ui/r;

    iput-object v1, v0, Lcom/google/android/location/places/ui/k;->l:Lcom/google/android/location/places/ui/r;

    iget v1, p0, Lcom/google/android/location/places/ui/ac;->u:I

    iget v2, p0, Lcom/google/android/location/places/ui/ac;->v:I

    sub-int/2addr v1, v2

    iput v1, v0, Lcom/google/android/location/places/ui/k;->o:I

    iget v1, p0, Lcom/google/android/location/places/ui/ac;->t:I

    iget v2, p0, Lcom/google/android/location/places/ui/ac;->v:I

    iput v1, v0, Lcom/google/android/location/places/ui/k;->n:I

    iput v2, v0, Lcom/google/android/location/places/ui/k;->m:I

    iget-object v1, p0, Lcom/google/android/location/places/ui/ac;->a:Lcom/google/android/location/places/ui/s;

    iget-boolean v1, v1, Lcom/google/android/location/places/ui/s;->h:Z

    iput-boolean v1, v0, Lcom/google/android/location/places/ui/k;->q:Z

    iget-object v1, p0, Lcom/google/android/location/places/ui/ac;->a:Lcom/google/android/location/places/ui/s;

    iget-boolean v1, v1, Lcom/google/android/location/places/ui/s;->f:Z

    iput-boolean v1, v0, Lcom/google/android/location/places/ui/k;->r:Z

    .line 180
    invoke-virtual {p0}, Lcom/google/android/location/places/ui/ac;->getChildFragmentManager()Landroid/support/v4/app/v;

    move-result-object v0

    sget v1, Lcom/google/android/gms/j;->oC:I

    invoke-virtual {v0, v1}, Landroid/support/v4/app/v;->a(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/places/ui/be;

    iput-object v0, p0, Lcom/google/android/location/places/ui/ac;->f:Lcom/google/android/location/places/ui/be;

    .line 182
    iget-object v0, p0, Lcom/google/android/location/places/ui/ac;->f:Lcom/google/android/location/places/ui/be;

    if-nez v0, :cond_1

    .line 183
    invoke-static {}, Lcom/google/android/location/places/ui/be;->a()Lcom/google/android/location/places/ui/be;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/places/ui/ac;->f:Lcom/google/android/location/places/ui/be;

    .line 184
    invoke-virtual {p0}, Lcom/google/android/location/places/ui/ac;->getChildFragmentManager()Landroid/support/v4/app/v;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/v;->a()Landroid/support/v4/app/aj;

    move-result-object v0

    sget v1, Lcom/google/android/gms/j;->oC:I

    iget-object v2, p0, Lcom/google/android/location/places/ui/ac;->f:Lcom/google/android/location/places/ui/be;

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/app/aj;->a(ILandroid/support/v4/app/Fragment;)Landroid/support/v4/app/aj;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/aj;->a()I

    .line 187
    :cond_1
    iget-object v0, p0, Lcom/google/android/location/places/ui/ac;->f:Lcom/google/android/location/places/ui/be;

    iget-object v1, p0, Lcom/google/android/location/places/ui/ac;->b:Lcom/google/android/location/places/ui/r;

    invoke-virtual {v0, v1}, Lcom/google/android/location/places/ui/be;->a(Lcom/google/android/location/places/ui/r;)Lcom/google/android/location/places/ui/be;

    .line 189
    if-nez p1, :cond_4

    .line 190
    iget-object v0, p0, Lcom/google/android/location/places/ui/ac;->a:Lcom/google/android/location/places/ui/s;

    iget-object v0, v0, Lcom/google/android/location/places/ui/s;->j:Lcom/google/android/gms/maps/model/LatLngBounds;

    if-eqz v0, :cond_3

    .line 191
    iput-boolean v6, p0, Lcom/google/android/location/places/ui/ac;->z:Z

    .line 192
    iput-boolean v6, p0, Lcom/google/android/location/places/ui/ac;->E:Z

    .line 204
    :goto_1
    return-void

    .line 154
    :cond_2
    iget v0, p0, Lcom/google/android/location/places/ui/ac;->u:I

    int-to-double v0, v0

    const-wide v2, 0x3fe3c6a7ef9db22dL    # 0.618

    mul-double/2addr v0, v2

    double-to-int v0, v0

    iput v0, p0, Lcom/google/android/location/places/ui/ac;->v:I

    goto/16 :goto_0

    .line 194
    :cond_3
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/location/places/ui/ac;->E:Z

    goto :goto_1

    .line 197
    :cond_4
    iput-boolean v6, p0, Lcom/google/android/location/places/ui/ac;->E:Z

    .line 198
    const-string v0, "waiting_for_location"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/location/places/ui/ac;->z:Z

    .line 199
    const-string v0, "displaying_search_results"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/location/places/ui/ac;->A:Z

    .line 200
    const-string v0, "last_exploration_bound"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/maps/model/LatLngBounds;

    iput-object v0, p0, Lcom/google/android/location/places/ui/ac;->B:Lcom/google/android/gms/maps/model/LatLngBounds;

    .line 201
    const-string v0, "last_search_query"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/places/ui/ac;->C:Ljava/lang/String;

    .line 202
    const-string v0, "last_search_bound"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/maps/model/LatLngBounds;

    iput-object v0, p0, Lcom/google/android/location/places/ui/ac;->D:Lcom/google/android/gms/maps/model/LatLngBounds;

    goto :goto_1
.end method

.method public final onAttach(Landroid/app/Activity;)V
    .locals 4

    .prologue
    .line 109
    instance-of v1, p1, Lcom/google/android/location/places/ui/PlacePickerActivity;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " must be an instance of PlacePickerActivity."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/gms/common/internal/bx;->b(ZLjava/lang/Object;)V

    .line 111
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onAttach(Landroid/app/Activity;)V

    .line 113
    :try_start_0
    move-object v0, p1

    check-cast v0, Lcom/google/android/location/places/ui/am;

    move-object v1, v0

    iput-object v1, p0, Lcom/google/android/location/places/ui/ac;->d:Lcom/google/android/location/places/ui/am;
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    .line 117
    return-void

    .line 115
    :catch_0
    move-exception v1

    new-instance v1, Ljava/lang/ClassCastException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " must implement PickAPlaceFragment.Listener"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/ClassCastException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2

    .prologue
    .line 122
    sget v0, Lcom/google/android/gms/l;->dc:I

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/google/android/location/places/ui/ac;->p:Landroid/view/ViewGroup;

    .line 123
    iget-object v0, p0, Lcom/google/android/location/places/ui/ac;->p:Landroid/view/ViewGroup;

    return-object v0
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 253
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 254
    const-string v0, "waiting_for_location"

    iget-boolean v1, p0, Lcom/google/android/location/places/ui/ac;->z:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 255
    const-string v0, "displaying_search_results"

    iget-boolean v1, p0, Lcom/google/android/location/places/ui/ac;->A:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 256
    iget-object v0, p0, Lcom/google/android/location/places/ui/ac;->B:Lcom/google/android/gms/maps/model/LatLngBounds;

    if-eqz v0, :cond_0

    .line 257
    const-string v0, "last_exploration_bound"

    iget-object v1, p0, Lcom/google/android/location/places/ui/ac;->B:Lcom/google/android/gms/maps/model/LatLngBounds;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 259
    :cond_0
    iget-object v0, p0, Lcom/google/android/location/places/ui/ac;->C:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 260
    const-string v0, "last_search_query"

    iget-object v1, p0, Lcom/google/android/location/places/ui/ac;->C:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 262
    :cond_1
    iget-object v0, p0, Lcom/google/android/location/places/ui/ac;->D:Lcom/google/android/gms/maps/model/LatLngBounds;

    if-eqz v0, :cond_2

    .line 263
    const-string v0, "last_search_bound"

    iget-object v1, p0, Lcom/google/android/location/places/ui/ac;->D:Lcom/google/android/gms/maps/model/LatLngBounds;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 265
    :cond_2
    return-void
.end method

.method public final onStart()V
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 208
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onStart()V

    .line 210
    iget-object v1, p0, Lcom/google/android/location/places/ui/ac;->c:Lcom/google/android/location/places/ui/av;

    invoke-virtual {v1, p0}, Lcom/google/android/location/places/ui/av;->a(Lcom/google/android/location/places/ui/ay;)V

    .line 212
    iget-object v1, p0, Lcom/google/android/location/places/ui/ac;->f:Lcom/google/android/location/places/ui/be;

    iget-object v2, p0, Lcom/google/android/location/places/ui/ac;->g:Lcom/google/android/location/places/ui/k;

    invoke-virtual {v2}, Lcom/google/android/location/places/ui/k;->a()Landroid/widget/ListView;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/location/places/ui/be;->a(Landroid/widget/ListView;)V

    .line 213
    iget-object v1, p0, Lcom/google/android/location/places/ui/ac;->a:Lcom/google/android/location/places/ui/s;

    iget-boolean v1, v1, Lcom/google/android/location/places/ui/s;->h:Z

    if-eqz v1, :cond_0

    .line 214
    iget-object v1, p0, Lcom/google/android/location/places/ui/ac;->e:Lcom/google/android/location/places/ui/MarkerMapFragment;

    invoke-virtual {v1, v0}, Lcom/google/android/location/places/ui/MarkerMapFragment;->b(Z)V

    .line 218
    :cond_0
    iget-boolean v1, p0, Lcom/google/android/location/places/ui/ac;->A:Z

    if-eqz v1, :cond_4

    .line 219
    iget-object v1, p0, Lcom/google/android/location/places/ui/ac;->e:Lcom/google/android/location/places/ui/MarkerMapFragment;

    invoke-virtual {v1}, Lcom/google/android/location/places/ui/MarkerMapFragment;->i()V

    .line 220
    iget-object v1, p0, Lcom/google/android/location/places/ui/ac;->C:Ljava/lang/String;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/google/android/location/places/ui/ac;->D:Lcom/google/android/gms/maps/model/LatLngBounds;

    if-eqz v1, :cond_2

    .line 221
    iget-object v1, p0, Lcom/google/android/location/places/ui/ac;->C:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/location/places/ui/ac;->D:Lcom/google/android/gms/maps/model/LatLngBounds;

    iget-object v3, p0, Lcom/google/android/location/places/ui/ac;->e:Lcom/google/android/location/places/ui/MarkerMapFragment;

    invoke-virtual {v3}, Lcom/google/android/location/places/ui/MarkerMapFragment;->j()V

    iget-object v3, p0, Lcom/google/android/location/places/ui/ac;->e:Lcom/google/android/location/places/ui/MarkerMapFragment;

    invoke-virtual {v3, v0}, Lcom/google/android/location/places/ui/MarkerMapFragment;->b(Z)V

    iget-object v3, p0, Lcom/google/android/location/places/ui/ac;->g:Lcom/google/android/location/places/ui/k;

    iget-object v4, p0, Lcom/google/android/location/places/ui/ac;->j:Ljava/lang/String;

    invoke-virtual {v3, v4}, Lcom/google/android/location/places/ui/k;->b(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/google/android/location/places/ui/ac;->c:Lcom/google/android/location/places/ui/av;

    invoke-virtual {v3, v1, v2}, Lcom/google/android/location/places/ui/av;->a(Ljava/lang/String;Lcom/google/android/gms/maps/model/LatLngBounds;)V

    .line 243
    :goto_0
    iget-object v1, p0, Lcom/google/android/location/places/ui/ac;->e:Lcom/google/android/location/places/ui/MarkerMapFragment;

    invoke-virtual {v1, p0}, Lcom/google/android/location/places/ui/MarkerMapFragment;->a(Lcom/google/android/location/places/ui/ab;)Lcom/google/android/location/places/ui/MarkerMapFragment;

    .line 244
    iget-object v1, p0, Lcom/google/android/location/places/ui/ac;->f:Lcom/google/android/location/places/ui/be;

    invoke-virtual {v1, p0}, Lcom/google/android/location/places/ui/be;->a(Lcom/google/android/location/places/ui/bh;)Lcom/google/android/location/places/ui/be;

    .line 245
    iget-object v1, p0, Lcom/google/android/location/places/ui/ac;->g:Lcom/google/android/location/places/ui/k;

    iput-object p0, v1, Lcom/google/android/location/places/ui/k;->k:Lcom/google/android/location/places/ui/o;

    .line 246
    if-eqz v0, :cond_1

    .line 247
    invoke-virtual {p0}, Lcom/google/android/location/places/ui/ac;->d()V

    .line 249
    :cond_1
    return-void

    .line 223
    :cond_2
    iput-boolean v0, p0, Lcom/google/android/location/places/ui/ac;->A:Z

    .line 239
    :cond_3
    const/4 v0, 0x1

    goto :goto_0

    .line 227
    :cond_4
    iget-boolean v1, p0, Lcom/google/android/location/places/ui/ac;->z:Z

    if-eqz v1, :cond_5

    .line 228
    iget-object v1, p0, Lcom/google/android/location/places/ui/ac;->e:Lcom/google/android/location/places/ui/MarkerMapFragment;

    invoke-virtual {v1, p0}, Lcom/google/android/location/places/ui/MarkerMapFragment;->a(Lcom/google/android/location/places/ui/ab;)Lcom/google/android/location/places/ui/MarkerMapFragment;

    .line 229
    iget-object v1, p0, Lcom/google/android/location/places/ui/ac;->e:Lcom/google/android/location/places/ui/MarkerMapFragment;

    invoke-virtual {v1}, Lcom/google/android/location/places/ui/MarkerMapFragment;->h()V

    .line 230
    iget-object v1, p0, Lcom/google/android/location/places/ui/ac;->g:Lcom/google/android/location/places/ui/k;

    iget-object v2, p0, Lcom/google/android/location/places/ui/ac;->h:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/google/android/location/places/ui/k;->b(Ljava/lang/String;)V

    .line 231
    iget-object v1, p0, Lcom/google/android/location/places/ui/ac;->e:Lcom/google/android/location/places/ui/MarkerMapFragment;

    invoke-virtual {v1}, Lcom/google/android/location/places/ui/MarkerMapFragment;->l()V

    goto :goto_0

    .line 232
    :cond_5
    iget-object v1, p0, Lcom/google/android/location/places/ui/ac;->B:Lcom/google/android/gms/maps/model/LatLngBounds;

    if-eqz v1, :cond_6

    .line 233
    iget-object v1, p0, Lcom/google/android/location/places/ui/ac;->e:Lcom/google/android/location/places/ui/MarkerMapFragment;

    invoke-virtual {v1, p0}, Lcom/google/android/location/places/ui/MarkerMapFragment;->a(Lcom/google/android/location/places/ui/ab;)Lcom/google/android/location/places/ui/MarkerMapFragment;

    .line 234
    iget-object v1, p0, Lcom/google/android/location/places/ui/ac;->e:Lcom/google/android/location/places/ui/MarkerMapFragment;

    iget-object v2, p0, Lcom/google/android/location/places/ui/ac;->B:Lcom/google/android/gms/maps/model/LatLngBounds;

    invoke-virtual {v1, v2}, Lcom/google/android/location/places/ui/MarkerMapFragment;->a(Lcom/google/android/gms/maps/model/LatLngBounds;)V

    goto :goto_0

    .line 235
    :cond_6
    iget-object v1, p0, Lcom/google/android/location/places/ui/ac;->a:Lcom/google/android/location/places/ui/s;

    iget-object v1, v1, Lcom/google/android/location/places/ui/s;->j:Lcom/google/android/gms/maps/model/LatLngBounds;

    if-eqz v1, :cond_3

    .line 236
    iget-object v1, p0, Lcom/google/android/location/places/ui/ac;->e:Lcom/google/android/location/places/ui/MarkerMapFragment;

    invoke-virtual {v1, p0}, Lcom/google/android/location/places/ui/MarkerMapFragment;->a(Lcom/google/android/location/places/ui/ab;)Lcom/google/android/location/places/ui/MarkerMapFragment;

    .line 237
    iget-object v1, p0, Lcom/google/android/location/places/ui/ac;->e:Lcom/google/android/location/places/ui/MarkerMapFragment;

    iget-object v2, p0, Lcom/google/android/location/places/ui/ac;->a:Lcom/google/android/location/places/ui/s;

    iget-object v2, v2, Lcom/google/android/location/places/ui/s;->j:Lcom/google/android/gms/maps/model/LatLngBounds;

    invoke-virtual {v1, v2}, Lcom/google/android/location/places/ui/MarkerMapFragment;->a(Lcom/google/android/gms/maps/model/LatLngBounds;)V

    goto :goto_0
.end method

.method public final onStop()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 269
    iget-object v0, p0, Lcom/google/android/location/places/ui/ac;->c:Lcom/google/android/location/places/ui/av;

    if-eqz v0, :cond_0

    .line 270
    iget-object v0, p0, Lcom/google/android/location/places/ui/ac;->c:Lcom/google/android/location/places/ui/av;

    invoke-virtual {v0}, Lcom/google/android/location/places/ui/av;->a()V

    .line 271
    iget-object v0, p0, Lcom/google/android/location/places/ui/ac;->c:Lcom/google/android/location/places/ui/av;

    invoke-virtual {v0, v1}, Lcom/google/android/location/places/ui/av;->a(Lcom/google/android/location/places/ui/ay;)V

    .line 274
    :cond_0
    iget-object v0, p0, Lcom/google/android/location/places/ui/ac;->e:Lcom/google/android/location/places/ui/MarkerMapFragment;

    if-eqz v0, :cond_1

    .line 275
    iget-object v0, p0, Lcom/google/android/location/places/ui/ac;->e:Lcom/google/android/location/places/ui/MarkerMapFragment;

    invoke-virtual {v0, v1}, Lcom/google/android/location/places/ui/MarkerMapFragment;->a(Lcom/google/android/location/places/ui/ab;)Lcom/google/android/location/places/ui/MarkerMapFragment;

    .line 277
    :cond_1
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onStop()V

    .line 278
    return-void
.end method
