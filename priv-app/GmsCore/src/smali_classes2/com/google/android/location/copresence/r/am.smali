.class final Lcom/google/android/location/copresence/r/am;
.super Landroid/content/BroadcastReceiver;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/location/copresence/r/al;


# direct methods
.method constructor <init>(Lcom/google/android/location/copresence/r/al;)V
    .locals 0

    .prologue
    .line 25
    iput-object p1, p0, Lcom/google/android/location/copresence/r/am;->a:Lcom/google/android/location/copresence/r/al;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public final onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 5

    .prologue
    .line 28
    if-eqz p2, :cond_0

    const-string v0, "android.net.wifi.SCAN_RESULTS"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 30
    iget-object v0, p0, Lcom/google/android/location/copresence/r/am;->a:Lcom/google/android/location/copresence/r/al;

    new-instance v1, Lcom/google/ac/b/c/bz;

    invoke-direct {v1}, Lcom/google/ac/b/c/bz;-><init>()V

    iput-object v1, v0, Lcom/google/android/location/copresence/r/al;->b:Lcom/google/ac/b/c/bz;

    .line 31
    iget-object v0, p0, Lcom/google/android/location/copresence/r/am;->a:Lcom/google/android/location/copresence/r/al;

    iget-object v0, v0, Lcom/google/android/location/copresence/r/al;->b:Lcom/google/ac/b/c/bz;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    iput-object v1, v0, Lcom/google/ac/b/c/bz;->a:Ljava/lang/Long;

    .line 33
    iget-object v0, p0, Lcom/google/android/location/copresence/r/am;->a:Lcom/google/android/location/copresence/r/al;

    iget-object v0, v0, Lcom/google/android/location/copresence/r/al;->a:Lcom/google/android/location/copresence/r/ae;

    iget-object v0, v0, Lcom/google/android/location/copresence/r/ae;->a:Landroid/net/wifi/WifiManager;

    invoke-virtual {v0}, Landroid/net/wifi/WifiManager;->getScanResults()Ljava/util/List;

    move-result-object v0

    if-nez v0, :cond_1

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    move-object v1, v0

    :goto_0
    invoke-static {}, Lcom/google/android/location/d/k;->a()Lcom/google/android/location/d/k;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/google/android/location/d/k;->a(Ljava/util/List;)V

    .line 34
    iget-object v0, p0, Lcom/google/android/location/copresence/r/am;->a:Lcom/google/android/location/copresence/r/al;

    iget-object v0, v0, Lcom/google/android/location/copresence/r/al;->b:Lcom/google/ac/b/c/bz;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v2

    new-array v2, v2, [Lcom/google/ac/b/c/ca;

    iput-object v2, v0, Lcom/google/ac/b/c/bz;->b:[Lcom/google/ac/b/c/ca;

    .line 35
    const/4 v0, 0x0

    move v2, v0

    :goto_1
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_0

    .line 36
    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/wifi/ScanResult;

    .line 37
    new-instance v3, Lcom/google/ac/b/c/ca;

    invoke-direct {v3}, Lcom/google/ac/b/c/ca;-><init>()V

    .line 38
    iget-object v4, v0, Landroid/net/wifi/ScanResult;->BSSID:Ljava/lang/String;

    iput-object v4, v3, Lcom/google/ac/b/c/ca;->a:Ljava/lang/String;

    .line 39
    iget v4, v0, Landroid/net/wifi/ScanResult;->level:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    iput-object v4, v3, Lcom/google/ac/b/c/ca;->b:Ljava/lang/Integer;

    .line 40
    iget v0, v0, Landroid/net/wifi/ScanResult;->frequency:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, v3, Lcom/google/ac/b/c/ca;->c:Ljava/lang/Integer;

    .line 41
    iget-object v0, p0, Lcom/google/android/location/copresence/r/am;->a:Lcom/google/android/location/copresence/r/al;

    iget-object v0, v0, Lcom/google/android/location/copresence/r/al;->b:Lcom/google/ac/b/c/bz;

    iget-object v0, v0, Lcom/google/ac/b/c/bz;->b:[Lcom/google/ac/b/c/ca;

    aput-object v3, v0, v2

    .line 35
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    .line 44
    :cond_0
    return-void

    :cond_1
    move-object v1, v0

    goto :goto_0
.end method
