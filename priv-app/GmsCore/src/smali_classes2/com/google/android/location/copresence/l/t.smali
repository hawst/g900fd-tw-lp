.class public final Lcom/google/android/location/copresence/l/t;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static i:Lcom/google/android/location/copresence/l/t;


# instance fields
.field final a:Landroid/content/Context;

.field final b:Lcom/google/android/location/copresence/l/d;

.field final c:Lcom/google/android/location/copresence/n/f;

.field final d:Lcom/google/android/location/copresence/o/v;

.field private final e:Lcom/google/android/location/copresence/l/e;

.field private final f:Lcom/google/android/location/copresence/an;

.field private final g:Lcom/google/android/location/copresence/ap;

.field private final h:Lcom/google/android/location/copresence/r/ag;


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 91
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 92
    invoke-static {p1}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 93
    iput-object p1, p0, Lcom/google/android/location/copresence/l/t;->a:Landroid/content/Context;

    .line 94
    invoke-static {p1}, Lcom/google/android/location/copresence/l/d;->a(Landroid/content/Context;)Lcom/google/android/location/copresence/l/d;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/copresence/l/t;->b:Lcom/google/android/location/copresence/l/d;

    .line 95
    invoke-static {p1}, Lcom/google/android/location/copresence/ap;->a(Landroid/content/Context;)Lcom/google/android/location/copresence/ap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/copresence/l/t;->g:Lcom/google/android/location/copresence/ap;

    .line 96
    invoke-static {p1}, Lcom/google/android/location/copresence/n/f;->a(Landroid/content/Context;)Lcom/google/android/location/copresence/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/copresence/l/t;->c:Lcom/google/android/location/copresence/n/f;

    .line 97
    invoke-static {p1}, Lcom/google/android/location/copresence/l/e;->a(Landroid/content/Context;)Lcom/google/android/location/copresence/l/e;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/copresence/l/t;->e:Lcom/google/android/location/copresence/l/e;

    .line 98
    invoke-static {p1}, Lcom/google/android/location/copresence/r/ag;->a(Landroid/content/Context;)Lcom/google/android/location/copresence/r/ag;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/copresence/l/t;->h:Lcom/google/android/location/copresence/r/ag;

    .line 99
    invoke-static {p1}, Lcom/google/android/location/copresence/an;->a(Landroid/content/Context;)Lcom/google/android/location/copresence/an;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/copresence/l/t;->f:Lcom/google/android/location/copresence/an;

    .line 100
    invoke-static {}, Lcom/google/android/location/copresence/o/v;->a()Lcom/google/android/location/copresence/o/v;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/copresence/l/t;->d:Lcom/google/android/location/copresence/o/v;

    .line 101
    return-void
.end method

.method public static declared-synchronized a(Landroid/content/Context;)Lcom/google/android/location/copresence/l/t;
    .locals 2

    .prologue
    .line 85
    const-class v1, Lcom/google/android/location/copresence/l/t;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/google/android/location/copresence/l/t;->i:Lcom/google/android/location/copresence/l/t;

    if-nez v0, :cond_0

    .line 86
    new-instance v0, Lcom/google/android/location/copresence/l/t;

    invoke-direct {v0, p0}, Lcom/google/android/location/copresence/l/t;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/google/android/location/copresence/l/t;->i:Lcom/google/android/location/copresence/l/t;

    .line 88
    :cond_0
    sget-object v0, Lcom/google/android/location/copresence/l/t;->i:Lcom/google/android/location/copresence/l/t;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 85
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method static a(Lcom/google/android/location/copresence/l/x;Lcom/google/android/location/copresence/l/y;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 344
    if-eqz p0, :cond_0

    .line 345
    sget-object v0, Lcom/google/android/gms/common/api/Status;->c:Lcom/google/android/gms/common/api/Status;

    const/4 v1, 0x0

    invoke-interface {p0, v0, v2, v1, v2}, Lcom/google/android/location/copresence/l/x;->a(Lcom/google/android/gms/common/api/Status;[Lcom/google/ac/b/c/ah;ZLjava/util/HashSet;)V

    .line 348
    :cond_0
    if-eqz p1, :cond_1

    .line 349
    sget-object v0, Lcom/google/android/gms/common/api/Status;->c:Lcom/google/android/gms/common/api/Status;

    invoke-interface {p1, v2}, Lcom/google/android/location/copresence/l/y;->a([Lcom/google/ac/b/c/bm;)V

    .line 351
    :cond_1
    return-void
.end method

.method static a([I)Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 295
    if-nez p0, :cond_1

    .line 303
    :cond_0
    :goto_0
    return v1

    :cond_1
    move v0, v1

    .line 298
    :goto_1
    array-length v2, p0

    if-ge v0, v2, :cond_0

    .line 299
    aget v2, p0, v0

    const/4 v3, 0x2

    if-ne v2, v3, :cond_2

    .line 300
    const/4 v1, 0x1

    goto :goto_0

    .line 298
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method


# virtual methods
.method public final a(Lcom/google/android/location/copresence/a/a;Ljava/lang/String;[Lcom/google/ac/b/c/bf;[Ljava/lang/String;[Lcom/google/ac/b/c/bo;[Ljava/lang/String;Ljava/util/HashSet;Lcom/google/android/location/copresence/l/w;Lcom/google/android/location/copresence/l/y;Lcom/google/android/location/copresence/l/x;Lcom/google/android/location/copresence/o/k;Lcom/google/android/location/copresence/l/p;)V
    .locals 20

    .prologue
    .line 143
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/location/copresence/l/t;->a:Landroid/content/Context;

    if-eqz v2, :cond_3

    .line 144
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/location/copresence/l/t;->a:Landroid/content/Context;

    const-string v3, "copresence_gcm_pref"

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v2

    const-string v3, "copresence_uuid"

    const/4 v4, 0x0

    invoke-interface {v2, v3, v4}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 148
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_3

    if-eqz p3, :cond_0

    move-object/from16 v0, p3

    array-length v2, v0

    if-nez v2, :cond_3

    :cond_0
    if-eqz p5, :cond_1

    move-object/from16 v0, p5

    array-length v2, v0

    if-nez v2, :cond_3

    .line 157
    :cond_1
    const/4 v2, 0x3

    invoke-static {v2}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 158
    const-string v2, "ReportRpcManager: Skipping report call since there\'s no device id."

    invoke-static {v2}, Lcom/google/android/location/copresence/ag;->b(Ljava/lang/String;)I

    .line 259
    :cond_2
    :goto_0
    return-void

    .line 164
    :cond_3
    invoke-static/range {p1 .. p1}, Lcom/google/android/location/copresence/p/a;->a(Lcom/google/android/location/copresence/a/a;)Lcom/google/android/location/copresence/p/a;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/location/copresence/p/a;->b()[Lcom/google/ac/b/c/bu;

    move-result-object v14

    .line 167
    invoke-static/range {p1 .. p1}, Lcom/google/android/location/copresence/p/a;->a(Lcom/google/android/location/copresence/a/a;)Lcom/google/android/location/copresence/p/a;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/location/copresence/p/a;->a()Ljava/util/Set;

    move-result-object v11

    invoke-interface {v11}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/gms/location/copresence/x;

    invoke-static/range {p1 .. p1}, Lcom/google/android/location/copresence/p/a;->a(Lcom/google/android/location/copresence/a/a;)Lcom/google/android/location/copresence/p/a;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/location/copresence/l/t;->a:Landroid/content/Context;

    const/4 v6, 0x2

    invoke-virtual {v4, v5, v2, v6}, Lcom/google/android/location/copresence/p/a;->a(Landroid/content/Context;Lcom/google/android/gms/location/copresence/x;I)Z

    goto :goto_1

    .line 169
    :cond_4
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/location/copresence/l/t;->e:Lcom/google/android/location/copresence/l/e;

    invoke-virtual {v2}, Lcom/google/android/location/copresence/l/e;->a()Lcom/google/ac/b/c/as;

    move-result-object v15

    .line 171
    new-instance v2, Lcom/google/android/location/copresence/l/v;

    move-object/from16 v3, p0

    move-object/from16 v4, p11

    move-object/from16 v5, p7

    move-object/from16 v6, p8

    move-object/from16 v7, p1

    move-object/from16 v8, p9

    move-object/from16 v9, p10

    move-object/from16 v10, p12

    invoke-direct/range {v2 .. v11}, Lcom/google/android/location/copresence/l/v;-><init>(Lcom/google/android/location/copresence/l/t;Lcom/google/android/location/copresence/o/k;Ljava/util/HashSet;Lcom/google/android/location/copresence/l/w;Lcom/google/android/location/copresence/a/a;Lcom/google/android/location/copresence/l/y;Lcom/google/android/location/copresence/l/x;Lcom/google/android/location/copresence/l/p;Ljava/util/Set;)V

    .line 178
    if-eqz p3, :cond_5

    move-object/from16 v0, p3

    array-length v3, v0

    if-gtz v3, :cond_8

    :cond_5
    if-eqz p4, :cond_6

    move-object/from16 v0, p4

    array-length v3, v0

    if-gtz v3, :cond_8

    :cond_6
    if-eqz p5, :cond_7

    move-object/from16 v0, p5

    array-length v3, v0

    if-gtz v3, :cond_8

    :cond_7
    if-eqz p6, :cond_b

    move-object/from16 v0, p6

    array-length v3, v0

    if-lez v3, :cond_b

    .line 182
    :cond_8
    const/4 v3, 0x0

    .line 183
    if-eqz p2, :cond_a

    .line 185
    :try_start_0
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/location/copresence/l/t;->a:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v4

    const/4 v5, 0x0

    move-object/from16 v0, p2

    invoke-virtual {v4, v0, v5}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 188
    const/4 v3, 0x1

    .line 201
    :cond_9
    :goto_2
    if-nez v3, :cond_b

    .line 203
    const/4 v3, 0x0

    const/16 v4, 0xd

    invoke-interface {v2, v3, v4}, Lcom/google/android/location/copresence/l/ab;->a(Lcom/google/android/location/copresence/l/aa;I)V

    goto/16 :goto_0

    .line 190
    :catch_0
    move-exception v4

    const/4 v4, 0x6

    invoke-static {v4}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v4

    if-eqz v4, :cond_9

    .line 191
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "ReportRpcManager: Failed to find package for "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p2

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/google/android/location/copresence/ag;->e(Ljava/lang/String;)I

    goto :goto_2

    .line 195
    :cond_a
    const/4 v4, 0x6

    invoke-static {v4}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v4

    if-eqz v4, :cond_9

    .line 196
    const-string v4, "ReportRpcManager: Attempting to perform a publish/subscribe with a null packageName"

    invoke-static {v4}, Lcom/google/android/location/copresence/ag;->e(Ljava/lang/String;)I

    goto :goto_2

    .line 216
    :cond_b
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/location/copresence/l/t;->a:Landroid/content/Context;

    move-object/from16 v0, p3

    move-object/from16 v1, p5

    invoke-static {v3, v0, v1}, Lcom/google/android/location/copresence/q/e;->a(Landroid/content/Context;[Lcom/google/ac/b/c/bf;[Lcom/google/ac/b/c/bo;)Lcom/google/ac/b/c/ac;

    move-result-object v16

    .line 219
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/location/copresence/l/t;->h:Lcom/google/android/location/copresence/r/ag;

    iget-object v4, v3, Lcom/google/android/location/copresence/r/ag;->e:Lcom/google/android/location/copresence/r/al;

    if-nez v4, :cond_c

    const/16 v17, 0x0

    .line 222
    :goto_3
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/location/copresence/l/t;->f:Lcom/google/android/location/copresence/an;

    if-eqz v3, :cond_d

    .line 223
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/location/copresence/l/t;->f:Lcom/google/android/location/copresence/an;

    invoke-virtual {v3}, Lcom/google/android/location/copresence/an;->a()Ljava/util/Set;

    move-result-object v3

    .line 224
    invoke-interface {v3}, Ljava/util/Set;->size()I

    move-result v4

    new-array v4, v4, [Lcom/google/ac/b/c/ah;

    invoke-interface {v3, v4}, Ljava/util/Set;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v3

    check-cast v3, [Lcom/google/ac/b/c/ah;

    move-object v13, v3

    .line 230
    :goto_4
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/location/copresence/l/t;->g:Lcom/google/android/location/copresence/ap;

    if-eqz v3, :cond_2

    .line 231
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/location/copresence/l/t;->g:Lcom/google/android/location/copresence/ap;

    move-object/from16 v19, v0

    new-instance v3, Lcom/google/android/location/copresence/l/u;

    move-object/from16 v4, p0

    move-object/from16 v5, p3

    move-object/from16 v6, p5

    move-object/from16 v7, p10

    move-object/from16 v8, p9

    move-object/from16 v9, p1

    move-object/from16 v10, p2

    move-object/from16 v11, p4

    move-object/from16 v12, p6

    move-object/from16 v18, v2

    invoke-direct/range {v3 .. v18}, Lcom/google/android/location/copresence/l/u;-><init>(Lcom/google/android/location/copresence/l/t;[Lcom/google/ac/b/c/bf;[Lcom/google/ac/b/c/bo;Lcom/google/android/location/copresence/l/x;Lcom/google/android/location/copresence/l/y;Lcom/google/android/location/copresence/a/a;Ljava/lang/String;[Ljava/lang/String;[Ljava/lang/String;[Lcom/google/ac/b/c/ah;[Lcom/google/ac/b/c/bu;Lcom/google/ac/b/c/as;Lcom/google/ac/b/c/ac;Lcom/google/ac/b/c/bz;Lcom/google/android/location/copresence/l/ab;)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/location/copresence/l/t;->a:Landroid/content/Context;

    move-object/from16 v0, p2

    invoke-static {v2, v0}, Lcom/google/android/gms/common/util/be;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/os/WorkSource;

    move-result-object v2

    move-object/from16 v0, v19

    invoke-virtual {v0, v3, v2}, Lcom/google/android/location/copresence/ap;->a(Ljava/lang/Runnable;Landroid/os/WorkSource;)V

    goto/16 :goto_0

    .line 219
    :cond_c
    iget-object v3, v3, Lcom/google/android/location/copresence/r/ag;->e:Lcom/google/android/location/copresence/r/al;

    iget-object v0, v3, Lcom/google/android/location/copresence/r/al;->b:Lcom/google/ac/b/c/bz;

    move-object/from16 v17, v0

    goto :goto_3

    .line 227
    :cond_d
    const/4 v3, 0x0

    new-array v13, v3, [Lcom/google/ac/b/c/ah;

    goto :goto_4
.end method
