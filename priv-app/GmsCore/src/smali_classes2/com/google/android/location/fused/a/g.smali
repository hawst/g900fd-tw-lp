.class public final Lcom/google/android/location/fused/a/g;
.super Lcom/google/android/location/fused/a/m;
.source "SourceFile"

# interfaces
.implements Landroid/location/LocationListener;


# instance fields
.field private final a:Lcom/google/android/location/fused/a/i;

.field private final b:Lcom/google/android/location/fused/a/i;

.field private final c:Lcom/google/android/location/fused/a/i;

.field private final d:Lcom/google/android/location/fused/ar;

.field private final e:Lcom/google/android/location/n/c;

.field private final l:Landroid/content/Context;

.field private final m:Landroid/content/BroadcastReceiver;

.field private final n:Landroid/content/IntentFilter;

.field private final o:Landroid/app/PendingIntent;

.field private final p:Landroid/os/Handler;

.field private final q:Lcom/google/android/gms/common/util/p;

.field private r:Z

.field private s:Lcom/google/android/location/fused/a/i;


# direct methods
.method public constructor <init>(Lcom/google/android/location/fused/ar;Lcom/google/android/location/n/c;Landroid/content/Context;Landroid/os/Looper;Lcom/google/android/gms/common/util/p;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 72
    invoke-direct {p0}, Lcom/google/android/location/fused/a/m;-><init>()V

    .line 41
    new-instance v0, Lcom/google/android/location/fused/a/j;

    invoke-direct {v0, p0, v2}, Lcom/google/android/location/fused/a/j;-><init>(Lcom/google/android/location/fused/a/g;B)V

    iput-object v0, p0, Lcom/google/android/location/fused/a/g;->a:Lcom/google/android/location/fused/a/i;

    .line 42
    new-instance v0, Lcom/google/android/location/fused/a/l;

    invoke-direct {v0, p0, v2}, Lcom/google/android/location/fused/a/l;-><init>(Lcom/google/android/location/fused/a/g;B)V

    iput-object v0, p0, Lcom/google/android/location/fused/a/g;->b:Lcom/google/android/location/fused/a/i;

    .line 43
    new-instance v0, Lcom/google/android/location/fused/a/k;

    invoke-direct {v0, p0, v2}, Lcom/google/android/location/fused/a/k;-><init>(Lcom/google/android/location/fused/a/g;B)V

    iput-object v0, p0, Lcom/google/android/location/fused/a/g;->c:Lcom/google/android/location/fused/a/i;

    .line 56
    iget-object v0, p0, Lcom/google/android/location/fused/a/g;->a:Lcom/google/android/location/fused/a/i;

    iput-object v0, p0, Lcom/google/android/location/fused/a/g;->s:Lcom/google/android/location/fused/a/i;

    .line 73
    iput-object p1, p0, Lcom/google/android/location/fused/a/g;->d:Lcom/google/android/location/fused/ar;

    .line 74
    iput-object p2, p0, Lcom/google/android/location/fused/a/g;->e:Lcom/google/android/location/n/c;

    .line 75
    iput-object p3, p0, Lcom/google/android/location/fused/a/g;->l:Landroid/content/Context;

    .line 76
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0, p4}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/google/android/location/fused/a/g;->p:Landroid/os/Handler;

    .line 77
    iput-object p5, p0, Lcom/google/android/location/fused/a/g;->q:Lcom/google/android/gms/common/util/p;

    .line 79
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    iput-object v0, p0, Lcom/google/android/location/fused/a/g;->n:Landroid/content/IntentFilter;

    .line 80
    iget-object v0, p0, Lcom/google/android/location/fused/a/g;->n:Landroid/content/IntentFilter;

    const-string v1, "com.google.android.gms.location.fused.GPS_ALARM"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 81
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.google.android.gms.location.fused.GPS_ALARM"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const/high16 v1, 0x8000000

    invoke-static {p3, v2, v0, v1}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/fused/a/g;->o:Landroid/app/PendingIntent;

    .line 86
    new-instance v0, Lcom/google/android/location/fused/a/h;

    invoke-direct {v0, p0}, Lcom/google/android/location/fused/a/h;-><init>(Lcom/google/android/location/fused/a/g;)V

    iput-object v0, p0, Lcom/google/android/location/fused/a/g;->m:Landroid/content/BroadcastReceiver;

    .line 92
    return-void
.end method

.method static synthetic a(Lcom/google/android/location/fused/a/g;)Landroid/app/PendingIntent;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lcom/google/android/location/fused/a/g;->o:Landroid/app/PendingIntent;

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/location/fused/a/g;J)V
    .locals 5

    .prologue
    .line 35
    iget-object v0, p0, Lcom/google/android/location/fused/a/g;->q:Lcom/google/android/gms/common/util/p;

    invoke-interface {v0}, Lcom/google/android/gms/common/util/p;->b()J

    move-result-wide v0

    add-long/2addr v0, p1

    iget-object v2, p0, Lcom/google/android/location/fused/a/g;->e:Lcom/google/android/location/n/c;

    const/4 v3, 0x3

    iget-object v4, p0, Lcom/google/android/location/fused/a/q;->j:Ljava/util/Collection;

    iget-object v4, p0, Lcom/google/android/location/fused/a/g;->o:Landroid/app/PendingIntent;

    invoke-virtual {v2, v3, v0, v1, v4}, Lcom/google/android/location/n/c;->a(IJLandroid/app/PendingIntent;)V

    return-void
.end method

.method static synthetic a(Lcom/google/android/location/fused/a/g;Lcom/google/android/location/fused/a/i;)V
    .locals 0

    .prologue
    .line 35
    invoke-direct {p0, p1}, Lcom/google/android/location/fused/a/g;->a(Lcom/google/android/location/fused/a/i;)V

    return-void
.end method

.method private a(Lcom/google/android/location/fused/a/i;)V
    .locals 1

    .prologue
    .line 163
    iget-object v0, p0, Lcom/google/android/location/fused/a/g;->s:Lcom/google/android/location/fused/a/i;

    if-eq v0, p1, :cond_0

    .line 164
    iget-object v0, p0, Lcom/google/android/location/fused/a/g;->s:Lcom/google/android/location/fused/a/i;

    invoke-interface {v0}, Lcom/google/android/location/fused/a/i;->b()V

    .line 165
    invoke-interface {p1}, Lcom/google/android/location/fused/a/i;->a()V

    .line 166
    iput-object p1, p0, Lcom/google/android/location/fused/a/g;->s:Lcom/google/android/location/fused/a/i;

    .line 168
    :cond_0
    return-void
.end method

.method static synthetic b(Lcom/google/android/location/fused/a/g;)Lcom/google/android/location/n/c;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lcom/google/android/location/fused/a/g;->e:Lcom/google/android/location/n/c;

    return-object v0
.end method

.method static synthetic c(Lcom/google/android/location/fused/a/g;)J
    .locals 4

    .prologue
    .line 35
    sget-object v0, Lcom/google/android/location/x;->E:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/google/android/location/fused/a/m;->f:J

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v0

    return-wide v0
.end method

.method static synthetic d(Lcom/google/android/location/fused/a/g;)Lcom/google/android/location/fused/a/i;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lcom/google/android/location/fused/a/g;->c:Lcom/google/android/location/fused/a/i;

    return-object v0
.end method

.method static synthetic e(Lcom/google/android/location/fused/a/g;)Lcom/google/android/location/fused/ar;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lcom/google/android/location/fused/a/g;->d:Lcom/google/android/location/fused/ar;

    return-object v0
.end method

.method static synthetic f(Lcom/google/android/location/fused/a/g;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lcom/google/android/location/fused/a/g;->p:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic g(Lcom/google/android/location/fused/a/g;)Lcom/google/android/location/fused/a/i;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lcom/google/android/location/fused/a/g;->b:Lcom/google/android/location/fused/a/i;

    return-object v0
.end method


# virtual methods
.method protected final a()V
    .locals 5

    .prologue
    .line 96
    iget-boolean v0, p0, Lcom/google/android/location/fused/a/q;->h:Z

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/google/android/location/fused/a/q;->i:Z

    if-eqz v0, :cond_1

    .line 97
    iget-boolean v0, p0, Lcom/google/android/location/fused/a/g;->r:Z

    if-nez v0, :cond_0

    .line 98
    iget-object v0, p0, Lcom/google/android/location/fused/a/g;->l:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/location/fused/a/g;->m:Landroid/content/BroadcastReceiver;

    iget-object v2, p0, Lcom/google/android/location/fused/a/g;->n:Landroid/content/IntentFilter;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/android/location/fused/a/g;->p:Landroid/os/Handler;

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;Ljava/lang/String;Landroid/os/Handler;)Landroid/content/Intent;

    .line 99
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/location/fused/a/g;->r:Z

    .line 102
    :cond_0
    iget-object v0, p0, Lcom/google/android/location/fused/a/g;->b:Lcom/google/android/location/fused/a/i;

    invoke-direct {p0, v0}, Lcom/google/android/location/fused/a/g;->a(Lcom/google/android/location/fused/a/i;)V

    .line 111
    :goto_0
    return-void

    .line 104
    :cond_1
    iget-boolean v0, p0, Lcom/google/android/location/fused/a/g;->r:Z

    if-eqz v0, :cond_2

    .line 105
    iget-object v0, p0, Lcom/google/android/location/fused/a/g;->l:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/location/fused/a/g;->m:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 106
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/location/fused/a/g;->r:Z

    .line 109
    :cond_2
    iget-object v0, p0, Lcom/google/android/location/fused/a/g;->a:Lcom/google/android/location/fused/a/i;

    invoke-direct {p0, v0}, Lcom/google/android/location/fused/a/g;->a(Lcom/google/android/location/fused/a/i;)V

    goto :goto_0
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 118
    iget-object v0, p0, Lcom/google/android/location/fused/a/g;->a:Lcom/google/android/location/fused/a/i;

    invoke-direct {p0, v0}, Lcom/google/android/location/fused/a/g;->a(Lcom/google/android/location/fused/a/i;)V

    .line 119
    invoke-virtual {p0}, Lcom/google/android/location/fused/a/g;->a()V

    .line 120
    return-void
.end method

.method final c()V
    .locals 1

    .prologue
    .line 124
    iget-object v0, p0, Lcom/google/android/location/fused/a/g;->s:Lcom/google/android/location/fused/a/i;

    invoke-interface {v0}, Lcom/google/android/location/fused/a/i;->c()V

    .line 125
    return-void
.end method

.method public final onLocationChanged(Landroid/location/Location;)V
    .locals 0

    .prologue
    .line 146
    return-void
.end method

.method public final onProviderDisabled(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 151
    return-void
.end method

.method public final onProviderEnabled(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 156
    return-void
.end method

.method public final onStatusChanged(Ljava/lang/String;ILandroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 160
    return-void
.end method

.method public final toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 172
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Gps pulse location ["

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 173
    invoke-virtual {p0, v0}, Lcom/google/android/location/fused/a/g;->a(Ljava/lang/StringBuilder;)V

    .line 174
    const-string v1, ", state="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/location/fused/a/g;->s:Lcom/google/android/location/fused/a/i;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 175
    const/16 v1, 0x5d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 176
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
