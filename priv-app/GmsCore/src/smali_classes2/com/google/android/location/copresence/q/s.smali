.class final Lcom/google/android/location/copresence/q/s;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final a:Ljava/lang/Object;

.field final b:Ljava/lang/Runnable;

.field c:J

.field d:Z

.field final synthetic e:Lcom/google/android/location/copresence/q/r;

.field private final f:Ljava/lang/Runnable;


# direct methods
.method public constructor <init>(Lcom/google/android/location/copresence/q/r;Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 49
    iput-object p1, p0, Lcom/google/android/location/copresence/q/s;->e:Lcom/google/android/location/copresence/q/r;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 50
    iput-object p2, p0, Lcom/google/android/location/copresence/q/s;->a:Ljava/lang/Object;

    .line 51
    new-instance v0, Lcom/google/android/location/copresence/q/t;

    invoke-direct {v0, p0, p1}, Lcom/google/android/location/copresence/q/t;-><init>(Lcom/google/android/location/copresence/q/s;Lcom/google/android/location/copresence/q/r;)V

    iput-object v0, p0, Lcom/google/android/location/copresence/q/s;->f:Ljava/lang/Runnable;

    .line 65
    new-instance v0, Lcom/google/android/location/copresence/q/u;

    invoke-direct {v0, p0, p1}, Lcom/google/android/location/copresence/q/u;-><init>(Lcom/google/android/location/copresence/q/s;Lcom/google/android/location/copresence/q/r;)V

    iput-object v0, p0, Lcom/google/android/location/copresence/q/s;->b:Ljava/lang/Runnable;

    .line 71
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/google/android/location/copresence/q/s;->c:J

    .line 72
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/location/copresence/q/s;->d:Z

    .line 73
    return-void
.end method


# virtual methods
.method final a()V
    .locals 4

    .prologue
    .line 109
    iget-wide v0, p0, Lcom/google/android/location/copresence/q/s;->c:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 110
    iget-object v0, p0, Lcom/google/android/location/copresence/q/s;->e:Lcom/google/android/location/copresence/q/r;

    iget-object v0, v0, Lcom/google/android/location/copresence/q/r;->a:Lcom/google/android/location/copresence/ap;

    iget-object v1, p0, Lcom/google/android/location/copresence/q/s;->f:Ljava/lang/Runnable;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/google/android/location/copresence/ap;->a(Ljava/lang/Runnable;Landroid/os/WorkSource;)V

    .line 115
    :goto_0
    return-void

    .line 112
    :cond_0
    iget-object v0, p0, Lcom/google/android/location/copresence/q/s;->e:Lcom/google/android/location/copresence/q/r;

    iget-object v0, v0, Lcom/google/android/location/copresence/q/r;->a:Lcom/google/android/location/copresence/ap;

    invoke-virtual {v0}, Lcom/google/android/location/copresence/ap;->b()Lcom/google/android/location/copresence/c;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/location/copresence/q/s;->f:Ljava/lang/Runnable;

    iget-wide v2, p0, Lcom/google/android/location/copresence/q/s;->c:J

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/location/copresence/c;->a(Ljava/lang/Runnable;J)V

    goto :goto_0
.end method

.method final a(J)Z
    .locals 7

    .prologue
    const/4 v0, 0x1

    .line 121
    iget-wide v2, p0, Lcom/google/android/location/copresence/q/s;->c:J

    const-wide/16 v4, -0x1

    cmp-long v1, v2, v4

    if-nez v1, :cond_0

    .line 122
    iput-wide p1, p0, Lcom/google/android/location/copresence/q/s;->c:J

    .line 129
    :goto_0
    return v0

    .line 125
    :cond_0
    iget-wide v2, p0, Lcom/google/android/location/copresence/q/s;->c:J

    cmp-long v1, p1, v2

    if-gez v1, :cond_1

    .line 126
    iput-wide p1, p0, Lcom/google/android/location/copresence/q/s;->c:J

    goto :goto_0

    .line 129
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
