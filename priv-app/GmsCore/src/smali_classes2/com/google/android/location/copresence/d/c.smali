.class final Lcom/google/android/location/copresence/d/c;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/location/copresence/d/f;


# instance fields
.field final synthetic a:Lcom/google/android/location/copresence/d/b;


# direct methods
.method constructor <init>(Lcom/google/android/location/copresence/d/b;)V
    .locals 0

    .prologue
    .line 40
    iput-object p1, p0, Lcom/google/android/location/copresence/d/c;->a:Lcom/google/android/location/copresence/d/b;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/location/copresence/d/a/k;)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    .line 43
    invoke-interface {p1}, Lcom/google/android/location/copresence/d/a/k;->a()Landroid/bluetooth/BluetoothDevice;

    move-result-object v0

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object v0

    .line 44
    invoke-interface {p1}, Lcom/google/android/location/copresence/d/a/k;->c()Ljava/util/UUID;

    move-result-object v1

    .line 46
    iget-object v2, p0, Lcom/google/android/location/copresence/d/c;->a:Lcom/google/android/location/copresence/d/b;

    invoke-static {v2}, Lcom/google/android/location/copresence/d/b;->a(Lcom/google/android/location/copresence/d/b;)Lcom/google/android/gms/common/internal/w;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/google/android/gms/common/internal/w;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    if-nez v2, :cond_2

    sget-object v2, Lcom/google/android/location/copresence/d/j;->a:Ljava/util/UUID;

    invoke-virtual {v2, v1}, Ljava/util/UUID;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 48
    invoke-static {v3}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 49
    const-string v1, "(BleAdvertisementCopresenceProvider) Found copresence device"

    invoke-static {v1}, Lcom/google/android/location/copresence/ag;->a(Ljava/lang/String;)I

    .line 51
    :cond_0
    iget-object v1, p0, Lcom/google/android/location/copresence/d/c;->a:Lcom/google/android/location/copresence/d/b;

    invoke-static {v1}, Lcom/google/android/location/copresence/d/b;->a(Lcom/google/android/location/copresence/d/b;)Lcom/google/android/gms/common/internal/w;

    move-result-object v1

    const-string v2, ""

    invoke-virtual {v1, v0, v2}, Lcom/google/android/gms/common/internal/w;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 52
    invoke-interface {p1}, Lcom/google/android/location/copresence/d/a/k;->d()Lcom/google/android/gms/location/copresence/x;

    move-result-object v0

    .line 53
    if-eqz v0, :cond_2

    .line 54
    invoke-static {v3}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 55
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "(BleAdvertisementCopresenceProvider) Received tokenId from ble advertised record: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " (rssi: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-interface {p1}, Lcom/google/android/location/copresence/d/a/k;->b()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/location/copresence/ag;->a(Ljava/lang/String;)I

    .line 58
    :cond_1
    iget-object v1, p0, Lcom/google/android/location/copresence/d/c;->a:Lcom/google/android/location/copresence/d/b;

    invoke-interface {p1}, Lcom/google/android/location/copresence/d/a/k;->b()I

    move-result v2

    invoke-static {v1, v0, v2}, Lcom/google/android/location/copresence/d/b;->a(Lcom/google/android/location/copresence/d/b;Lcom/google/android/gms/location/copresence/x;I)V

    .line 61
    :cond_2
    return-void
.end method
