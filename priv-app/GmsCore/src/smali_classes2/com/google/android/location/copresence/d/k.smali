.class final Lcom/google/android/location/copresence/d/k;
.super Lcom/google/android/location/copresence/k/d;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/location/copresence/d/j;


# direct methods
.method constructor <init>(Lcom/google/android/location/copresence/d/j;)V
    .locals 0

    .prologue
    .line 70
    iput-object p1, p0, Lcom/google/android/location/copresence/d/k;->a:Lcom/google/android/location/copresence/d/j;

    invoke-direct {p0}, Lcom/google/android/location/copresence/k/d;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 114
    iget-object v0, p0, Lcom/google/android/location/copresence/d/k;->a:Lcom/google/android/location/copresence/d/j;

    invoke-virtual {v0}, Lcom/google/android/location/copresence/d/j;->i()V

    .line 115
    return-void
.end method

.method public final a(Lcom/google/android/location/copresence/k/f;)V
    .locals 5

    .prologue
    const/4 v1, 0x3

    const/4 v2, 0x2

    const/4 v4, 0x1

    .line 73
    iget-object v0, p0, Lcom/google/android/location/copresence/d/k;->a:Lcom/google/android/location/copresence/d/j;

    invoke-static {v0, p1}, Lcom/google/android/location/copresence/d/j;->a(Lcom/google/android/location/copresence/d/j;Lcom/google/android/location/copresence/k/f;)V

    .line 74
    iget-object v0, p0, Lcom/google/android/location/copresence/d/k;->a:Lcom/google/android/location/copresence/d/j;

    invoke-static {v0}, Lcom/google/android/location/copresence/d/j;->a(Lcom/google/android/location/copresence/d/j;)Lcom/google/android/location/copresence/d/n;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/location/copresence/d/n;->e:Lcom/google/android/location/copresence/k/f;

    if-ne p1, v0, :cond_0

    .line 76
    const-string v0, "STATE_DISABLED"

    invoke-static {v0, v4}, Lcom/google/android/location/copresence/b/a;->a(Ljava/lang/String;Z)V

    .line 77
    iget-object v0, p0, Lcom/google/android/location/copresence/d/k;->a:Lcom/google/android/location/copresence/d/j;

    invoke-static {v0}, Lcom/google/android/location/copresence/d/j;->b(Lcom/google/android/location/copresence/d/j;)V

    .line 110
    :goto_0
    return-void

    .line 78
    :cond_0
    iget-object v0, p0, Lcom/google/android/location/copresence/d/k;->a:Lcom/google/android/location/copresence/d/j;

    invoke-static {v0}, Lcom/google/android/location/copresence/d/j;->a(Lcom/google/android/location/copresence/d/j;)Lcom/google/android/location/copresence/d/n;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/location/copresence/d/n;->d:Lcom/google/android/location/copresence/k/f;

    if-ne p1, v0, :cond_1

    .line 80
    const-string v0, "USER_ENABLED_SETTING_SAVED"

    invoke-static {v0, v4}, Lcom/google/android/location/copresence/b/a;->a(Ljava/lang/String;Z)V

    .line 81
    iget-object v0, p0, Lcom/google/android/location/copresence/d/k;->a:Lcom/google/android/location/copresence/d/j;

    invoke-static {v0}, Lcom/google/android/location/copresence/d/j;->b(Lcom/google/android/location/copresence/d/j;)V

    goto :goto_0

    .line 84
    :cond_1
    iget-object v0, p0, Lcom/google/android/location/copresence/d/k;->a:Lcom/google/android/location/copresence/d/j;

    invoke-static {v0}, Lcom/google/android/location/copresence/d/j;->c(Lcom/google/android/location/copresence/d/j;)Lcom/google/android/location/copresence/d/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/location/copresence/d/e;->d()I

    move-result v0

    if-ne v0, v2, :cond_3

    .line 85
    invoke-static {v1}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 86
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "BluetoothMedium: Bluetooth connection state: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/location/copresence/d/k;->a:Lcom/google/android/location/copresence/d/j;

    invoke-static {v1}, Lcom/google/android/location/copresence/d/j;->c(Lcom/google/android/location/copresence/d/j;)Lcom/google/android/location/copresence/d/e;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/location/copresence/d/e;->d()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ". Cannot restart bluetooth."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->b(Ljava/lang/String;)I

    .line 90
    :cond_2
    invoke-static {v2}, Lcom/google/android/location/copresence/b/a;->a(I)V

    .line 91
    const-string v0, "STATE_ALREADY_CONNECTED"

    invoke-static {v0, v4}, Lcom/google/android/location/copresence/b/a;->a(Ljava/lang/String;Z)V

    .line 92
    iget-object v0, p0, Lcom/google/android/location/copresence/d/k;->a:Lcom/google/android/location/copresence/d/j;

    invoke-static {v0}, Lcom/google/android/location/copresence/d/j;->b(Lcom/google/android/location/copresence/d/j;)V

    goto :goto_0

    .line 94
    :cond_3
    iget-object v0, p0, Lcom/google/android/location/copresence/d/k;->a:Lcom/google/android/location/copresence/d/j;

    invoke-static {v0}, Lcom/google/android/location/copresence/d/j;->d(Lcom/google/android/location/copresence/d/j;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 95
    invoke-static {v1}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 96
    const-string v0, "BluetoothMedium: Already attempted restart. Failing."

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->b(Ljava/lang/String;)I

    .line 98
    :cond_4
    const-string v0, "STATE_RESTARTED"

    invoke-static {v0, v4}, Lcom/google/android/location/copresence/b/a;->a(Ljava/lang/String;Z)V

    .line 99
    iget-object v0, p0, Lcom/google/android/location/copresence/d/k;->a:Lcom/google/android/location/copresence/d/j;

    invoke-static {v0}, Lcom/google/android/location/copresence/d/j;->b(Lcom/google/android/location/copresence/d/j;)V

    goto :goto_0

    .line 101
    :cond_5
    invoke-static {v1}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 102
    const-string v0, "BluetoothMedium: Attempting restart. Failing."

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->b(Ljava/lang/String;)I

    .line 104
    :cond_6
    iget-object v0, p0, Lcom/google/android/location/copresence/d/k;->a:Lcom/google/android/location/copresence/d/j;

    invoke-static {v0}, Lcom/google/android/location/copresence/d/j;->e(Lcom/google/android/location/copresence/d/j;)Z

    .line 105
    iget-object v0, p0, Lcom/google/android/location/copresence/d/k;->a:Lcom/google/android/location/copresence/d/j;

    invoke-static {v0}, Lcom/google/android/location/copresence/d/j;->f(Lcom/google/android/location/copresence/d/j;)Lcom/google/android/location/copresence/k/a;

    move-result-object v0

    new-array v1, v2, [Lcom/google/android/location/copresence/k/f;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/android/location/copresence/d/k;->a:Lcom/google/android/location/copresence/d/j;

    invoke-static {v3}, Lcom/google/android/location/copresence/d/j;->a(Lcom/google/android/location/copresence/d/j;)Lcom/google/android/location/copresence/d/n;

    move-result-object v3

    iget-object v3, v3, Lcom/google/android/location/copresence/d/n;->e:Lcom/google/android/location/copresence/k/f;

    aput-object v3, v1, v2

    iget-object v2, p0, Lcom/google/android/location/copresence/d/k;->a:Lcom/google/android/location/copresence/d/j;

    invoke-static {v2}, Lcom/google/android/location/copresence/d/j;->a(Lcom/google/android/location/copresence/d/j;)Lcom/google/android/location/copresence/d/n;

    move-result-object v2

    iget-object v2, v2, Lcom/google/android/location/copresence/d/n;->f:Lcom/google/android/location/copresence/k/f;

    aput-object v2, v1, v4

    invoke-virtual {v0, v1}, Lcom/google/android/location/copresence/k/a;->a([Lcom/google/android/location/copresence/k/f;)V

    goto/16 :goto_0
.end method

.method public final b(Lcom/google/android/location/copresence/k/f;)V
    .locals 2

    .prologue
    .line 119
    const-string v0, "STATE_COMPLETE"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/google/android/location/copresence/b/a;->a(Ljava/lang/String;Z)V

    .line 120
    iget-object v0, p0, Lcom/google/android/location/copresence/d/k;->a:Lcom/google/android/location/copresence/d/j;

    invoke-static {v0, p1}, Lcom/google/android/location/copresence/d/j;->b(Lcom/google/android/location/copresence/d/j;Lcom/google/android/location/copresence/k/f;)V

    .line 122
    iget-object v0, p0, Lcom/google/android/location/copresence/d/k;->a:Lcom/google/android/location/copresence/d/j;

    invoke-static {v0}, Lcom/google/android/location/copresence/d/j;->g(Lcom/google/android/location/copresence/d/j;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/location/copresence/d/k;->a:Lcom/google/android/location/copresence/d/j;

    invoke-static {v0}, Lcom/google/android/location/copresence/d/j;->a(Lcom/google/android/location/copresence/d/j;)Lcom/google/android/location/copresence/d/n;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/location/copresence/d/n;->o:Lcom/google/android/location/copresence/k/f;

    if-ne p1, v0, :cond_1

    .line 123
    const/4 v0, 0x3

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 124
    const-string v0, "BluetoothMedium: Success reverting."

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->b(Ljava/lang/String;)I

    .line 127
    :cond_0
    iget-object v0, p0, Lcom/google/android/location/copresence/d/k;->a:Lcom/google/android/location/copresence/d/j;

    invoke-static {v0}, Lcom/google/android/location/copresence/d/j;->h(Lcom/google/android/location/copresence/d/j;)Z

    .line 129
    :cond_1
    return-void
.end method
