.class public final Lcom/google/android/location/places/c/a;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/util/concurrent/Callable;


# instance fields
.field private final a:Lcom/google/android/location/places/c/j;

.field private final b:Ljava/lang/String;

.field private final c:Lcom/google/android/gms/maps/model/LatLngBounds;

.field private final d:Lcom/google/android/gms/location/places/AutocompleteFilter;

.field private final e:Lcom/google/android/gms/location/places/internal/PlacesParams;


# direct methods
.method public constructor <init>(Lcom/google/android/location/places/c/j;Ljava/lang/String;Lcom/google/android/gms/maps/model/LatLngBounds;Lcom/google/android/gms/location/places/AutocompleteFilter;Lcom/google/android/gms/location/places/internal/PlacesParams;)V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    iput-object p1, p0, Lcom/google/android/location/places/c/a;->a:Lcom/google/android/location/places/c/j;

    .line 32
    iput-object p2, p0, Lcom/google/android/location/places/c/a;->b:Ljava/lang/String;

    .line 33
    iput-object p3, p0, Lcom/google/android/location/places/c/a;->c:Lcom/google/android/gms/maps/model/LatLngBounds;

    .line 34
    iput-object p4, p0, Lcom/google/android/location/places/c/a;->d:Lcom/google/android/gms/location/places/AutocompleteFilter;

    .line 35
    iput-object p5, p0, Lcom/google/android/location/places/c/a;->e:Lcom/google/android/gms/location/places/internal/PlacesParams;

    .line 36
    return-void
.end method


# virtual methods
.method public final synthetic call()Ljava/lang/Object;
    .locals 9

    .prologue
    .line 17
    iget-object v1, p0, Lcom/google/android/location/places/c/a;->a:Lcom/google/android/location/places/c/j;

    iget-object v2, p0, Lcom/google/android/location/places/c/a;->b:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/location/places/c/a;->c:Lcom/google/android/gms/maps/model/LatLngBounds;

    iget-object v4, p0, Lcom/google/android/location/places/c/a;->d:Lcom/google/android/gms/location/places/AutocompleteFilter;

    iget-object v5, p0, Lcom/google/android/location/places/c/a;->e:Lcom/google/android/gms/location/places/internal/PlacesParams;

    sget-object v0, Lcom/google/android/location/x;->w:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Lcom/android/volley/ac;

    const-string v1, "Communication with the server is not allowed."

    invoke-direct {v0, v1}, Lcom/android/volley/ac;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget-object v0, v1, Lcom/google/android/location/places/c/j;->a:Landroid/content/Context;

    invoke-static {v0, v3, v2, v4, v5}, Lcom/google/android/location/places/bo;->a(Landroid/content/Context;Lcom/google/android/gms/maps/model/LatLngBounds;Ljava/lang/String;Lcom/google/android/gms/location/places/AutocompleteFilter;Lcom/google/android/gms/location/places/internal/PlacesParams;)Lcom/google/android/location/l/a/bi;

    move-result-object v0

    iget-object v2, v1, Lcom/google/android/location/places/c/j;->b:Lcom/google/android/location/places/c/k;

    iget-object v3, v5, Lcom/google/android/gms/location/places/internal/PlacesParams;->e:Ljava/lang/String;

    invoke-virtual {v1, v3}, Lcom/google/android/location/places/c/j;->a(Ljava/lang/String;)Lcom/google/android/gms/common/server/ClientContext;

    move-result-object v3

    invoke-virtual {v2, v3, v0}, Lcom/google/android/location/places/c/k;->a(Lcom/google/android/gms/common/server/ClientContext;Lcom/google/android/location/l/a/bi;)Lcom/google/android/location/l/a/bj;

    move-result-object v0

    iget-object v1, v1, Lcom/google/android/location/places/c/j;->c:Lcom/google/android/location/places/c/l;

    iget-object v2, v0, Lcom/google/android/location/l/a/bj;->d:[Lcom/google/android/location/l/a/br;

    new-instance v3, Ljava/util/ArrayList;

    array-length v0, v2

    invoke-direct {v3, v0}, Ljava/util/ArrayList;-><init>(I)V

    array-length v4, v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v4, :cond_2

    aget-object v5, v2, v1

    iget-object v0, v5, Lcom/google/android/location/l/a/br;->f:Ljava/lang/Integer;

    if-eqz v0, :cond_1

    iget-object v0, v5, Lcom/google/android/location/l/a/br;->f:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    :goto_1
    iget-object v6, v5, Lcom/google/android/location/l/a/br;->a:Ljava/lang/String;

    iget-object v7, v5, Lcom/google/android/location/l/a/br;->b:Ljava/lang/String;

    iget-object v8, v5, Lcom/google/android/location/l/a/br;->c:[Ljava/lang/String;

    invoke-static {v8}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v8

    iget-object v5, v5, Lcom/google/android/location/l/a/br;->d:[Lcom/google/android/location/l/a/bt;

    invoke-static {v5}, Lcom/google/android/location/places/c/l;->a([Lcom/google/android/location/l/a/bt;)Ljava/util/List;

    move-result-object v5

    invoke-static {v6, v7, v8, v5, v0}, Lcom/google/android/gms/location/places/AutocompletePrediction;->a(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/util/List;I)Lcom/google/android/gms/location/places/AutocompletePrediction;

    move-result-object v0

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_1
    const/4 v0, 0x6

    goto :goto_1

    :cond_2
    return-object v3
.end method
