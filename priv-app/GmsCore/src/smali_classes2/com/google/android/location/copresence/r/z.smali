.class final Lcom/google/android/location/copresence/r/z;
.super Lcom/google/android/location/copresence/r/ad;
.source "SourceFile"


# instance fields
.field final a:Landroid/net/wifi/p2p/WifiP2pManager$GroupInfoListener;

.field b:Z

.field final synthetic c:Lcom/google/android/location/copresence/r/p;

.field private final f:Lcom/google/android/location/copresence/r/ak;

.field private final g:Landroid/os/Messenger;


# direct methods
.method public constructor <init>(Lcom/google/android/location/copresence/r/p;Ljava/lang/String;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 271
    iput-object p1, p0, Lcom/google/android/location/copresence/r/z;->c:Lcom/google/android/location/copresence/r/p;

    .line 272
    const-string v0, "TransientGroupCreated"

    const/4 v1, 0x2

    new-array v1, v1, [Lcom/google/android/location/copresence/k/f;

    invoke-static {p1}, Lcom/google/android/location/copresence/r/p;->h(Lcom/google/android/location/copresence/r/p;)Lcom/google/android/location/copresence/r/ad;

    move-result-object v2

    aput-object v2, v1, v4

    const/4 v2, 0x1

    new-instance v3, Lcom/google/android/location/copresence/r/y;

    invoke-direct {v3, p1, p2}, Lcom/google/android/location/copresence/r/y;-><init>(Lcom/google/android/location/copresence/r/p;Ljava/lang/String;)V

    aput-object v3, v1, v2

    invoke-direct {p0, p1, v0, v1}, Lcom/google/android/location/copresence/r/ad;-><init>(Lcom/google/android/location/copresence/r/p;Ljava/lang/String;[Lcom/google/android/location/copresence/k/f;)V

    .line 197
    new-instance v0, Lcom/google/android/location/copresence/r/aa;

    invoke-direct {v0, p0}, Lcom/google/android/location/copresence/r/aa;-><init>(Lcom/google/android/location/copresence/r/z;)V

    iput-object v0, p0, Lcom/google/android/location/copresence/r/z;->a:Landroid/net/wifi/p2p/WifiP2pManager$GroupInfoListener;

    .line 224
    new-instance v0, Lcom/google/android/location/copresence/r/ab;

    invoke-direct {v0, p0}, Lcom/google/android/location/copresence/r/ab;-><init>(Lcom/google/android/location/copresence/r/z;)V

    iput-object v0, p0, Lcom/google/android/location/copresence/r/z;->f:Lcom/google/android/location/copresence/r/ak;

    .line 243
    new-instance v0, Landroid/os/Messenger;

    new-instance v1, Lcom/google/android/location/copresence/r/ac;

    invoke-direct {v1, p0}, Lcom/google/android/location/copresence/r/ac;-><init>(Lcom/google/android/location/copresence/r/z;)V

    invoke-direct {v0, v1}, Landroid/os/Messenger;-><init>(Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/google/android/location/copresence/r/z;->g:Landroid/os/Messenger;

    .line 273
    iput-boolean v4, p0, Lcom/google/android/location/copresence/r/z;->b:Z

    .line 274
    return-void
.end method


# virtual methods
.method public final a()Z
    .locals 6

    .prologue
    const/4 v5, 0x6

    const/4 v4, 0x0

    .line 283
    iget-object v0, p0, Lcom/google/android/location/copresence/r/z;->c:Lcom/google/android/location/copresence/r/p;

    invoke-static {v0}, Lcom/google/android/location/copresence/r/p;->b(Lcom/google/android/location/copresence/r/p;)Lcom/google/android/location/copresence/r/ah;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/location/copresence/r/z;->f:Lcom/google/android/location/copresence/r/ak;

    iput-object v1, v0, Lcom/google/android/location/copresence/r/ah;->f:Lcom/google/android/location/copresence/r/ak;

    .line 284
    iget-object v0, p0, Lcom/google/android/location/copresence/r/z;->c:Lcom/google/android/location/copresence/r/p;

    invoke-static {v0}, Lcom/google/android/location/copresence/r/p;->i(Lcom/google/android/location/copresence/r/p;)V

    .line 285
    iget-object v0, p0, Lcom/google/android/location/copresence/r/z;->c:Lcom/google/android/location/copresence/r/p;

    invoke-static {v0}, Lcom/google/android/location/copresence/r/p;->b(Lcom/google/android/location/copresence/r/p;)Lcom/google/android/location/copresence/r/ah;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/location/copresence/r/z;->g:Landroid/os/Messenger;

    const/4 v2, 0x2

    invoke-static {v2}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v2

    if-eqz v2, :cond_0

    const-string v2, "WifiP2pManager: Creating transient group."

    invoke-static {v2}, Lcom/google/android/location/copresence/ag;->a(Ljava/lang/String;)I

    :cond_0
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v2

    const v3, 0x2200d

    iput v3, v2, Landroid/os/Message;->what:I

    iput v4, v2, Landroid/os/Message;->arg1:I

    iput-object v1, v2, Landroid/os/Message;->replyTo:Landroid/os/Messenger;

    :try_start_0
    invoke-virtual {v0, v2}, Lcom/google/android/location/copresence/r/ah;->a(Landroid/os/Message;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 286
    :cond_1
    :goto_0
    const/4 v0, 0x1

    return v0

    .line 285
    :catch_0
    move-exception v0

    invoke-static {v5}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v2

    if-eqz v2, :cond_2

    const-string v2, "WifiP2pManager: Error sending create transient group message."

    invoke-static {v2, v0}, Lcom/google/android/location/copresence/ag;->a(Ljava/lang/String;Ljava/lang/Throwable;)I

    :cond_2
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v0

    const v2, 0x2200e

    iput v2, v0, Landroid/os/Message;->what:I

    iput v4, v0, Landroid/os/Message;->arg1:I

    :try_start_1
    invoke-virtual {v1, v0}, Landroid/os/Messenger;->send(Landroid/os/Message;)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    :catch_1
    move-exception v0

    invoke-static {v5}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v1

    if-eqz v1, :cond_1

    const-string v1, "WifiP2pManager: Error replying to local messenger."

    invoke-static {v1, v0}, Lcom/google/android/location/copresence/ag;->a(Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 278
    iget-boolean v0, p0, Lcom/google/android/location/copresence/r/z;->b:Z

    return v0
.end method

.method public final d()J
    .locals 2

    .prologue
    .line 291
    const-wide/16 v0, 0x2710

    return-wide v0
.end method
