.class public final Lcom/google/android/location/copresence/n/f;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/location/copresence/n/o;


# static fields
.field private static final a:Lcom/google/android/location/copresence/n/k;

.field private static k:Lcom/google/android/location/copresence/n/f;


# instance fields
.field private final b:Landroid/os/Bundle;

.field private final c:Landroid/content/BroadcastReceiver;

.field private final d:Lcom/google/android/location/copresence/n/p;

.field private final e:Lcom/google/android/location/copresence/l/d;

.field private final f:Landroid/content/Context;

.field private final g:Lcom/google/android/location/copresence/a/b;

.field private final h:Lcom/google/android/location/copresence/n/m;

.field private final i:Lcom/google/android/location/copresence/n/e;

.field private final j:Lcom/google/android/location/copresence/q/w;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 49
    new-instance v0, Lcom/google/android/location/copresence/n/k;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/google/android/location/copresence/n/k;-><init>(B)V

    sput-object v0, Lcom/google/android/location/copresence/n/f;->a:Lcom/google/android/location/copresence/n/k;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 84
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 52
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    iput-object v0, p0, Lcom/google/android/location/copresence/n/f;->b:Landroid/os/Bundle;

    .line 53
    new-instance v0, Lcom/google/android/location/copresence/n/j;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/android/location/copresence/n/j;-><init>(Lcom/google/android/location/copresence/n/f;B)V

    iput-object v0, p0, Lcom/google/android/location/copresence/n/f;->c:Landroid/content/BroadcastReceiver;

    .line 85
    iput-object p1, p0, Lcom/google/android/location/copresence/n/f;->f:Landroid/content/Context;

    .line 86
    invoke-static {p1}, Lcom/google/android/location/copresence/l/d;->a(Landroid/content/Context;)Lcom/google/android/location/copresence/l/d;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/copresence/n/f;->e:Lcom/google/android/location/copresence/l/d;

    .line 87
    new-instance v0, Lcom/google/android/location/copresence/n/p;

    invoke-direct {v0}, Lcom/google/android/location/copresence/n/p;-><init>()V

    iput-object v0, p0, Lcom/google/android/location/copresence/n/f;->d:Lcom/google/android/location/copresence/n/p;

    .line 88
    invoke-static {p1}, Lcom/google/android/location/copresence/a/b;->a(Landroid/content/Context;)Lcom/google/android/location/copresence/a/b;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/copresence/n/f;->g:Lcom/google/android/location/copresence/a/b;

    .line 89
    new-instance v0, Lcom/google/android/location/copresence/n/m;

    invoke-direct {v0, p1, p0}, Lcom/google/android/location/copresence/n/m;-><init>(Landroid/content/Context;Lcom/google/android/location/copresence/n/o;)V

    iput-object v0, p0, Lcom/google/android/location/copresence/n/f;->h:Lcom/google/android/location/copresence/n/m;

    .line 90
    new-instance v0, Lcom/google/android/location/copresence/n/e;

    iget-object v1, p0, Lcom/google/android/location/copresence/n/f;->g:Lcom/google/android/location/copresence/a/b;

    invoke-direct {v0, p1, v1}, Lcom/google/android/location/copresence/n/e;-><init>(Landroid/content/Context;Lcom/google/android/location/copresence/a/b;)V

    iput-object v0, p0, Lcom/google/android/location/copresence/n/f;->i:Lcom/google/android/location/copresence/n/e;

    .line 93
    new-instance v0, Lcom/google/android/location/copresence/q/w;

    invoke-static {p1}, Lcom/google/android/location/copresence/ap;->a(Landroid/content/Context;)Lcom/google/android/location/copresence/ap;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/location/copresence/q/w;-><init>(Lcom/google/android/location/copresence/ap;)V

    iput-object v0, p0, Lcom/google/android/location/copresence/n/f;->j:Lcom/google/android/location/copresence/q/w;

    .line 94
    return-void
.end method

.method static synthetic a(Lcom/google/android/location/copresence/n/f;Lcom/google/android/location/copresence/a/a;Lcom/google/android/gms/location/copresence/CopresenceSettings;)Lcom/google/android/gms/location/copresence/CopresenceSettings;
    .locals 9

    .prologue
    const/4 v4, 0x0

    const/4 v8, 0x3

    .line 45
    iget-object v0, p1, Lcom/google/android/location/copresence/a/a;->a:Landroid/accounts/Account;

    iget-object v5, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-direct {p0, p1}, Lcom/google/android/location/copresence/n/f;->d(Lcom/google/android/location/copresence/a/a;)Lcom/google/android/location/copresence/n/b;

    move-result-object v6

    iget-object v7, p0, Lcom/google/android/location/copresence/n/f;->d:Lcom/google/android/location/copresence/n/p;

    invoke-static {v8}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "setSettings: settings="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v5, p2}, Lcom/google/android/location/copresence/ag;->a(Ljava/lang/String;Lcom/google/android/gms/location/copresence/CopresenceSettings;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->b(Ljava/lang/String;)I

    :cond_0
    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_3

    const/4 v0, 0x6

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "Account not specified."

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->e(Ljava/lang/String;)I

    :cond_1
    move-object v0, v4

    :goto_0
    if-nez v0, :cond_9

    invoke-static {v8}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, "CopresenceSettingsManager: Unable to write settings locally after they are written remotely. Will sync in next sync cycle."

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->b(Ljava/lang/String;)I

    :cond_2
    :goto_1
    invoke-direct {p0, p1, v6}, Lcom/google/android/location/copresence/n/f;->a(Lcom/google/android/location/copresence/a/a;Lcom/google/android/location/copresence/n/b;)V

    return-object p2

    :cond_3
    iget-object v0, v7, Lcom/google/android/location/copresence/n/p;->a:Ljava/util/Map;

    invoke-interface {v0, v5}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/location/copresence/CopresenceSettings;

    if-nez v0, :cond_5

    invoke-static {p2}, Lcom/google/android/location/copresence/n/p;->a(Lcom/google/android/gms/location/copresence/CopresenceSettings;)Landroid/util/Pair;

    move-result-object v0

    move-object v1, v0

    :goto_2
    iget-object v0, v1, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-nez v0, :cond_8

    invoke-static {v8}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v0

    if-eqz v0, :cond_4

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v0, "sanitized settings: "

    invoke-direct {v2, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v0, v1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/gms/location/copresence/CopresenceSettings;

    invoke-static {v5, v0}, Lcom/google/android/location/copresence/ag;->a(Ljava/lang/String;Lcom/google/android/gms/location/copresence/CopresenceSettings;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->b(Ljava/lang/String;)I

    :cond_4
    iget-object v0, v7, Lcom/google/android/location/copresence/n/p;->a:Ljava/util/Map;

    iget-object v2, v1, Landroid/util/Pair;->second:Ljava/lang/Object;

    invoke-interface {v0, v5, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {v7}, Lcom/google/android/location/copresence/n/p;->a()Z

    move-result v0

    if-eqz v0, :cond_8

    iget-object v0, v1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/gms/location/copresence/CopresenceSettings;

    goto :goto_0

    :cond_5
    invoke-virtual {p2}, Lcom/google/android/gms/location/copresence/CopresenceSettings;->b()Z

    move-result v1

    if-eqz v1, :cond_7

    invoke-virtual {p2}, Lcom/google/android/gms/location/copresence/CopresenceSettings;->a()Z

    move-result v1

    :goto_3
    invoke-virtual {p2}, Lcom/google/android/gms/location/copresence/CopresenceSettings;->c()[Lcom/google/android/gms/location/copresence/internal/CopresenceNamedAcl;

    move-result-object v2

    if-nez v2, :cond_6

    invoke-virtual {v0}, Lcom/google/android/gms/location/copresence/CopresenceSettings;->c()[Lcom/google/android/gms/location/copresence/internal/CopresenceNamedAcl;

    move-result-object v2

    :cond_6
    invoke-virtual {p2}, Lcom/google/android/gms/location/copresence/CopresenceSettings;->d()[Lcom/google/android/gms/location/copresence/internal/CopresenceFeatureOptIn;

    move-result-object v3

    if-nez v3, :cond_a

    invoke-virtual {v0}, Lcom/google/android/gms/location/copresence/CopresenceSettings;->d()[Lcom/google/android/gms/location/copresence/internal/CopresenceFeatureOptIn;

    move-result-object v0

    :goto_4
    invoke-static {v1, v2, v0}, Lcom/google/android/gms/location/copresence/CopresenceSettings;->a(Z[Lcom/google/android/gms/location/copresence/internal/CopresenceNamedAcl;[Lcom/google/android/gms/location/copresence/internal/CopresenceFeatureOptIn;)Lcom/google/android/gms/location/copresence/CopresenceSettings;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/location/copresence/n/p;->a(Lcom/google/android/gms/location/copresence/CopresenceSettings;)Landroid/util/Pair;

    move-result-object v0

    move-object v1, v0

    goto :goto_2

    :cond_7
    invoke-virtual {v0}, Lcom/google/android/gms/location/copresence/CopresenceSettings;->a()Z

    move-result v1

    goto :goto_3

    :cond_8
    move-object v0, v4

    goto/16 :goto_0

    :cond_9
    move-object p2, v0

    goto :goto_1

    :cond_a
    move-object v0, v3

    goto :goto_4
.end method

.method public static declared-synchronized a(Landroid/content/Context;)Lcom/google/android/location/copresence/n/f;
    .locals 2

    .prologue
    .line 78
    const-class v1, Lcom/google/android/location/copresence/n/f;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/google/android/location/copresence/n/f;->k:Lcom/google/android/location/copresence/n/f;

    if-nez v0, :cond_0

    .line 79
    new-instance v0, Lcom/google/android/location/copresence/n/f;

    invoke-direct {v0, p0}, Lcom/google/android/location/copresence/n/f;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/google/android/location/copresence/n/f;->k:Lcom/google/android/location/copresence/n/f;

    .line 81
    :cond_0
    sget-object v0, Lcom/google/android/location/copresence/n/f;->k:Lcom/google/android/location/copresence/n/f;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 78
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method static synthetic a(Lcom/google/android/location/copresence/n/f;)Lcom/google/android/location/copresence/n/m;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcom/google/android/location/copresence/n/f;->h:Lcom/google/android/location/copresence/n/m;

    return-object v0
.end method

.method public static a()V
    .locals 0

    .prologue
    .line 130
    return-void
.end method

.method private a(Lcom/google/android/location/copresence/a/a;Lcom/google/android/location/copresence/n/b;)V
    .locals 5

    .prologue
    .line 317
    iget-object v0, p1, Lcom/google/android/location/copresence/a/a;->a:Landroid/accounts/Account;

    iget-object v0, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    .line 318
    invoke-direct {p0, p1}, Lcom/google/android/location/copresence/n/f;->d(Lcom/google/android/location/copresence/a/a;)Lcom/google/android/location/copresence/n/b;

    move-result-object v1

    .line 320
    const/4 v2, 0x2

    invoke-static {v2}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 321
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "CopresenceSettingsManager: Settings changed for account: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->f(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->a(Ljava/lang/String;)I

    .line 323
    :cond_0
    iget-object v0, p0, Lcom/google/android/location/copresence/n/f;->j:Lcom/google/android/location/copresence/q/w;

    sget-object v2, Lcom/google/android/location/copresence/n/f;->a:Lcom/google/android/location/copresence/n/k;

    invoke-virtual {v1}, Lcom/google/android/location/copresence/n/b;->a()Z

    move-result v3

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    new-instance v4, Lcom/google/android/location/copresence/q/aa;

    invoke-direct {v4, v2, p1, v3}, Lcom/google/android/location/copresence/q/aa;-><init>(Lcom/google/android/location/copresence/q/ad;Ljava/lang/Object;Ljava/lang/Object;)V

    const/4 v2, 0x0

    invoke-virtual {v0, v4, v2}, Lcom/google/android/location/copresence/q/w;->a(Lcom/google/android/location/copresence/q/x;Landroid/os/WorkSource;)V

    .line 327
    new-instance v0, Landroid/content/Intent;

    const-string v2, "com.google.android.gms.location.copresence.SETTINGS_CHANGED"

    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 328
    iget-object v2, p0, Lcom/google/android/location/copresence/n/f;->f:Landroid/content/Context;

    invoke-virtual {v2, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 330
    invoke-virtual {p2}, Lcom/google/android/location/copresence/n/b;->a()Z

    move-result v0

    if-nez v0, :cond_2

    invoke-virtual {v1}, Lcom/google/android/location/copresence/n/b;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    .line 331
    :goto_0
    if-eqz v0, :cond_1

    .line 332
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 333
    const-string v1, "com.google.android.gms.location.copresence.OPT_IN"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 334
    iget-object v1, p0, Lcom/google/android/location/copresence/n/f;->f:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 336
    :cond_1
    return-void

    .line 330
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic b(Lcom/google/android/location/copresence/n/f;)Lcom/google/android/location/copresence/n/e;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcom/google/android/location/copresence/n/f;->i:Lcom/google/android/location/copresence/n/e;

    return-object v0
.end method

.method static synthetic c(Lcom/google/android/location/copresence/n/f;)V
    .locals 6

    .prologue
    .line 45
    iget-object v0, p0, Lcom/google/android/location/copresence/n/f;->g:Lcom/google/android/location/copresence/a/b;

    invoke-virtual {v0}, Lcom/google/android/location/copresence/a/b;->a()Ljava/util/Set;

    move-result-object v0

    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/copresence/a/a;

    iget-object v4, v0, Lcom/google/android/location/copresence/a/a;->a:Landroid/accounts/Account;

    iget-object v4, v4, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/google/android/location/copresence/n/f;->d(Lcom/google/android/location/copresence/a/a;)Lcom/google/android/location/copresence/n/b;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {v2, v4, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/google/android/location/copresence/n/f;->i:Lcom/google/android/location/copresence/n/e;

    invoke-virtual {v0}, Lcom/google/android/location/copresence/n/e;->a()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_1
    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v2, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/location/copresence/a/a;

    if-eqz v1, :cond_1

    invoke-virtual {v3, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/copresence/n/b;

    invoke-direct {p0, v1, v0}, Lcom/google/android/location/copresence/n/f;->a(Lcom/google/android/location/copresence/a/a;Lcom/google/android/location/copresence/n/b;)V

    goto :goto_1

    :cond_2
    return-void
.end method

.method private d(Lcom/google/android/location/copresence/a/a;)Lcom/google/android/location/copresence/n/b;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 257
    iget-object v0, p0, Lcom/google/android/location/copresence/n/f;->d:Lcom/google/android/location/copresence/n/p;

    iget-object v1, p1, Lcom/google/android/location/copresence/a/a;->a:Landroid/accounts/Account;

    iget-object v1, v1, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/location/copresence/n/p;->a(Ljava/lang/String;)Lcom/google/android/gms/location/copresence/CopresenceSettings;

    move-result-object v0

    .line 258
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/google/android/gms/location/copresence/CopresenceSettings;->a()Z

    move-result v0

    if-nez v0, :cond_2

    .line 260
    :cond_0
    new-instance v0, Lcom/google/android/location/copresence/n/b;

    const/16 v1, 0x9cf

    invoke-direct {v0, v1, v2}, Lcom/google/android/location/copresence/n/b;-><init>(ILjava/lang/String;)V

    .line 273
    :cond_1
    :goto_0
    return-object v0

    .line 265
    :cond_2
    iget-object v0, p0, Lcom/google/android/location/copresence/n/f;->i:Lcom/google/android/location/copresence/n/e;

    invoke-virtual {v0, p1}, Lcom/google/android/location/copresence/n/e;->a(Lcom/google/android/location/copresence/a/a;)Lcom/google/android/location/copresence/n/b;

    move-result-object v0

    .line 266
    if-nez v0, :cond_1

    .line 268
    new-instance v0, Lcom/google/android/location/copresence/n/b;

    const/16 v1, 0x9c9

    invoke-direct {v0, v1, v2}, Lcom/google/android/location/copresence/n/b;-><init>(ILjava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method public final a(Lcom/google/android/location/copresence/a/a;Lcom/google/android/gms/location/copresence/CopresenceSettings;Lcom/google/android/gms/location/copresence/internal/i;)V
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 191
    invoke-virtual {p2}, Lcom/google/android/gms/location/copresence/CopresenceSettings;->b()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-virtual {p2}, Lcom/google/android/gms/location/copresence/CopresenceSettings;->a()Z

    move-result v0

    if-nez v0, :cond_4

    move v0, v1

    .line 192
    :goto_0
    iget-object v3, p0, Lcom/google/android/location/copresence/n/f;->i:Lcom/google/android/location/copresence/n/e;

    invoke-virtual {v3, p1}, Lcom/google/android/location/copresence/n/e;->a(Lcom/google/android/location/copresence/a/a;)Lcom/google/android/location/copresence/n/b;

    move-result-object v4

    .line 193
    if-eqz v4, :cond_2

    iget v3, v4, Lcom/google/android/location/copresence/n/b;->a:I

    const/16 v5, 0x9cb

    if-eq v3, v5, :cond_0

    iget v3, v4, Lcom/google/android/location/copresence/n/b;->a:I

    const/16 v5, 0x9ca

    if-ne v3, v5, :cond_5

    :cond_0
    move v3, v1

    :goto_1
    invoke-virtual {v4}, Lcom/google/android/location/copresence/n/b;->a()Z

    move-result v4

    if-nez v4, :cond_1

    if-eqz v3, :cond_6

    :cond_1
    move v3, v1

    :goto_2
    if-nez v3, :cond_7

    if-nez v0, :cond_7

    .line 196
    :cond_2
    const/4 v0, 0x6

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 197
    const-string v0, "Copresence not available in the current state for account: %s"

    new-array v1, v1, [Ljava/lang/Object;

    iget-object v3, p1, Lcom/google/android/location/copresence/a/a;->a:Landroid/accounts/Account;

    iget-object v3, v3, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-static {v3}, Lcom/google/android/location/copresence/ag;->f(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 200
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "CopresenceSettingsManager: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->e(Ljava/lang/String;)I

    .line 202
    :cond_3
    const/16 v0, 0xd

    const/4 v1, 0x0

    invoke-static {p3, v0, v1}, Lcom/google/android/location/copresence/r;->a(Lcom/google/android/gms/location/copresence/internal/i;ILcom/google/android/gms/location/copresence/CopresenceSettings;)V

    .line 211
    :goto_3
    return-void

    :cond_4
    move v0, v2

    .line 191
    goto :goto_0

    :cond_5
    move v3, v2

    .line 193
    goto :goto_1

    :cond_6
    move v3, v2

    goto :goto_2

    .line 207
    :cond_7
    invoke-virtual {p2}, Lcom/google/android/gms/location/copresence/CopresenceSettings;->b()Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-virtual {p2}, Lcom/google/android/gms/location/copresence/CopresenceSettings;->a()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 208
    :goto_4
    new-instance v5, Lcom/google/android/location/copresence/n/l;

    invoke-direct {v5, p0, p3, v1}, Lcom/google/android/location/copresence/n/l;-><init>(Lcom/google/android/location/copresence/n/f;Lcom/google/android/gms/location/copresence/internal/i;Z)V

    .line 210
    iget-object v2, p0, Lcom/google/android/location/copresence/n/f;->e:Lcom/google/android/location/copresence/l/d;

    new-instance v0, Lcom/google/android/location/copresence/l/aj;

    iget-object v1, v2, Lcom/google/android/location/copresence/l/d;->a:Landroid/content/Context;

    iget-object v2, v2, Lcom/google/android/location/copresence/l/d;->b:Lcom/google/android/location/copresence/ap;

    move-object v3, p1

    move-object v4, p2

    invoke-direct/range {v0 .. v5}, Lcom/google/android/location/copresence/l/aj;-><init>(Landroid/content/Context;Lcom/google/android/location/copresence/ap;Lcom/google/android/location/copresence/a/a;Lcom/google/android/gms/location/copresence/CopresenceSettings;Lcom/google/android/location/copresence/l/ab;)V

    invoke-virtual {v0}, Lcom/google/android/location/copresence/l/ac;->d()V

    goto :goto_3

    :cond_8
    move v1, v2

    .line 207
    goto :goto_4
.end method

.method public final a(Lcom/google/android/location/copresence/a/a;Lcom/google/android/gms/location/copresence/internal/i;)V
    .locals 2

    .prologue
    .line 142
    iget-object v0, p0, Lcom/google/android/location/copresence/n/f;->d:Lcom/google/android/location/copresence/n/p;

    iget-object v1, p1, Lcom/google/android/location/copresence/a/a;->a:Landroid/accounts/Account;

    iget-object v1, v1, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/location/copresence/n/p;->a(Ljava/lang/String;)Lcom/google/android/gms/location/copresence/CopresenceSettings;

    move-result-object v0

    .line 143
    if-nez v0, :cond_1

    .line 144
    const/4 v0, 0x3

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 145
    const-string v0, "CopresenceSettingsManager: Empty settings. Fetching from server."

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->b(Ljava/lang/String;)I

    .line 154
    :cond_0
    :goto_0
    return-void

    .line 149
    :cond_1
    const/4 v1, -0x1

    invoke-static {p2, v1, v0}, Lcom/google/android/location/copresence/r;->a(Lcom/google/android/gms/location/copresence/internal/i;ILcom/google/android/gms/location/copresence/CopresenceSettings;)V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/location/copresence/n/i;)V
    .locals 1

    .prologue
    .line 134
    iget-object v0, p0, Lcom/google/android/location/copresence/n/f;->j:Lcom/google/android/location/copresence/q/w;

    invoke-virtual {v0, p1}, Lcom/google/android/location/copresence/q/w;->a(Ljava/lang/Object;)V

    .line 135
    return-void
.end method

.method public final a(Lcom/google/android/location/e/a;Lcom/google/android/location/copresence/ap;)V
    .locals 5

    .prologue
    const/4 v4, 0x2

    .line 113
    iget-object v0, p0, Lcom/google/android/location/copresence/n/f;->d:Lcom/google/android/location/copresence/n/p;

    iget-object v1, p0, Lcom/google/android/location/copresence/n/f;->f:Landroid/content/Context;

    new-instance v2, Ljava/io/File;

    invoke-virtual {v1}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v1

    const-string v3, "copresence_settings"

    invoke-direct {v2, v1, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    new-instance v1, Lcom/google/android/location/os/bl;

    invoke-direct {v1, v4, v2, p1}, Lcom/google/android/location/os/bl;-><init>(ILjava/io/File;Lcom/google/android/location/e/a;)V

    iput-object v1, v0, Lcom/google/android/location/copresence/n/p;->b:Lcom/google/android/location/os/bl;

    new-instance v1, Lcom/google/android/location/copresence/j/e;

    invoke-direct {v1}, Lcom/google/android/location/copresence/j/e;-><init>()V

    :try_start_0
    iget-object v2, v0, Lcom/google/android/location/copresence/n/p;->b:Lcom/google/android/location/os/bl;

    invoke-virtual {v2, v1}, Lcom/google/android/location/os/bl;->a(Lcom/google/protobuf/nano/j;)Lcom/google/protobuf/nano/j;

    invoke-virtual {v0, v1}, Lcom/google/android/location/copresence/n/p;->a(Lcom/google/android/location/copresence/j/e;)V

    const/4 v1, 0x2

    invoke-static {v1}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v0, v0, Lcom/google/android/location/copresence/n/p;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v1, "Loaded settings: "

    invoke-direct {v3, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/location/copresence/CopresenceSettings;

    invoke-static {v1, v0}, Lcom/google/android/location/copresence/ag;->a(Ljava/lang/String;Lcom/google/android/gms/location/copresence/CopresenceSettings;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->a(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    const/4 v1, 0x6

    invoke-static {v1}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v1

    if-eqz v1, :cond_0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Couldn\'t load settings: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->e(Ljava/lang/String;)I

    .line 114
    :cond_0
    iget-object v0, p0, Lcom/google/android/location/copresence/n/f;->h:Lcom/google/android/location/copresence/n/m;

    iget-object v0, v0, Lcom/google/android/location/copresence/n/m;->a:Lcom/google/android/gms/location/reporting/i;

    invoke-virtual {v0}, Lcom/google/android/gms/location/reporting/i;->a()V

    .line 115
    iget-object v0, p0, Lcom/google/android/location/copresence/n/f;->i:Lcom/google/android/location/copresence/n/e;

    iget-object v0, v0, Lcom/google/android/location/copresence/n/e;->b:Lcom/google/android/location/copresence/n/c;

    invoke-virtual {v0}, Lcom/google/android/location/copresence/n/c;->a()V

    .line 116
    iget-object v0, p0, Lcom/google/android/location/copresence/n/f;->f:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/location/copresence/n/f;->c:Landroid/content/BroadcastReceiver;

    new-instance v2, Landroid/content/IntentFilter;

    const-string v3, "com.google.android.gms.location.reporting.SETTINGS_CHANGED"

    invoke-direct {v2, v3}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    const/4 v3, 0x0

    invoke-virtual {p2}, Lcom/google/android/location/copresence/ap;->d()Landroid/os/Handler;

    move-result-object v4

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;Ljava/lang/String;Landroid/os/Handler;)Landroid/content/Intent;

    .line 121
    return-void
.end method

.method public final a(Lcom/google/android/location/copresence/a/a;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 280
    iget-object v1, p0, Lcom/google/android/location/copresence/n/f;->d:Lcom/google/android/location/copresence/n/p;

    iget-object v2, p1, Lcom/google/android/location/copresence/a/a;->a:Landroid/accounts/Account;

    iget-object v2, v2, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/google/android/location/copresence/n/p;->a(Ljava/lang/String;)Lcom/google/android/gms/location/copresence/CopresenceSettings;

    move-result-object v1

    .line 281
    if-nez v1, :cond_1

    .line 291
    :cond_0
    :goto_0
    return v0

    .line 286
    :cond_1
    iget-object v2, p0, Lcom/google/android/location/copresence/n/f;->i:Lcom/google/android/location/copresence/n/e;

    invoke-virtual {v2, p1}, Lcom/google/android/location/copresence/n/e;->a(Lcom/google/android/location/copresence/a/a;)Lcom/google/android/location/copresence/n/b;

    move-result-object v2

    .line 287
    if-eqz v2, :cond_0

    .line 291
    invoke-virtual {v1}, Lcom/google/android/gms/location/copresence/CopresenceSettings;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v2}, Lcom/google/android/location/copresence/n/b;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final b()Ljava/util/Set;
    .locals 1

    .prologue
    .line 341
    iget-object v0, p0, Lcom/google/android/location/copresence/n/f;->g:Lcom/google/android/location/copresence/a/b;

    invoke-virtual {v0}, Lcom/google/android/location/copresence/a/b;->a()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public final b(Lcom/google/android/location/copresence/a/a;)V
    .locals 1

    .prologue
    .line 356
    iget-object v0, p0, Lcom/google/android/location/copresence/n/f;->i:Lcom/google/android/location/copresence/n/e;

    invoke-virtual {v0, p1}, Lcom/google/android/location/copresence/n/e;->b(Lcom/google/android/location/copresence/a/a;)Lcom/google/android/location/copresence/n/b;

    .line 357
    return-void
.end method

.method public final c()Ljava/util/Set;
    .locals 6

    .prologue
    .line 346
    new-instance v1, Lcom/google/android/location/copresence/n/g;

    invoke-direct {v1, p0}, Lcom/google/android/location/copresence/n/g;-><init>(Lcom/google/android/location/copresence/n/f;)V

    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    iget-object v0, p0, Lcom/google/android/location/copresence/n/f;->g:Lcom/google/android/location/copresence/a/b;

    invoke-virtual {v0}, Lcom/google/android/location/copresence/a/b;->a()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/copresence/a/a;

    invoke-interface {v1, v0}, Lcom/google/android/location/copresence/n/h;->a(Lcom/google/android/location/copresence/a/a;)Z

    move-result v4

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/google/android/location/copresence/n/f;->d:Lcom/google/android/location/copresence/n/p;

    iget-object v5, v0, Lcom/google/android/location/copresence/a/a;->a:Landroid/accounts/Account;

    iget-object v5, v5, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v4, v5}, Lcom/google/android/location/copresence/n/p;->a(Ljava/lang/String;)Lcom/google/android/gms/location/copresence/CopresenceSettings;

    move-result-object v4

    if-eqz v4, :cond_0

    invoke-virtual {v4}, Lcom/google/android/gms/location/copresence/CopresenceSettings;->a()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v2, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    return-object v2
.end method

.method public final c(Lcom/google/android/location/copresence/a/a;)V
    .locals 3

    .prologue
    .line 383
    iget-object v0, p1, Lcom/google/android/location/copresence/a/a;->a:Landroid/accounts/Account;

    iget-object v0, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    .line 384
    iget-object v1, p0, Lcom/google/android/location/copresence/n/f;->d:Lcom/google/android/location/copresence/n/p;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, v1, Lcom/google/android/location/copresence/n/p;->a:Ljava/util/Map;

    invoke-interface {v2, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {v1}, Lcom/google/android/location/copresence/n/p;->a()Z

    .line 385
    :cond_0
    iget-object v1, p0, Lcom/google/android/location/copresence/n/f;->i:Lcom/google/android/location/copresence/n/e;

    iget-object v1, v1, Lcom/google/android/location/copresence/n/e;->a:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 386
    return-void
.end method
