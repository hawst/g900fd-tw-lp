.class public Lcom/google/android/location/copresence/debug/CopresDatabaseHelper$PublishedMessageRecord;
.super Lcom/google/android/location/copresence/debug/CopresDatabaseHelper$BaseMessageRecord;
.source "SourceFile"


# instance fields
.field public accessKey:Ljava/lang/String;
    .annotation runtime Lcom/google/android/location/copresence/debug/b;
        a = false
    .end annotation
.end field

.field public aclNamespace:Ljava/lang/String;
    .annotation runtime Lcom/google/android/location/copresence/debug/b;
        a = false
    .end annotation
.end field

.field public aclType:Ljava/lang/Integer;
    .annotation runtime Lcom/google/android/location/copresence/debug/b;
        a = false
    .end annotation
.end field

.field public isActive:Ljava/lang/Boolean;
    .annotation runtime Lcom/google/android/location/copresence/debug/b;
        a = true
        c = "True"
    .end annotation
.end field

.field public publishedId:Ljava/lang/String;
    .annotation runtime Lcom/google/android/location/copresence/debug/b;
        a = false
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 355
    invoke-direct {p0}, Lcom/google/android/location/copresence/debug/CopresDatabaseHelper$BaseMessageRecord;-><init>()V

    .line 356
    const-string v0, "PublishedMessage"

    iput-object v0, p0, Lcom/google/android/location/copresence/debug/CopresDatabaseHelper$PublishedMessageRecord;->messageType:Ljava/lang/String;

    .line 357
    return-void
.end method
