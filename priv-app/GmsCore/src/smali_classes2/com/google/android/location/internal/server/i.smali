.class final Lcom/google/android/location/internal/server/i;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private final b:I

.field private final c:Ljava/util/HashMap;

.field private final d:Ljava/util/Map;

.field private e:Lcom/google/android/location/os/j;

.field private f:I

.field private g:I

.field private h:I

.field private i:Lcom/google/android/location/o/n;

.field private j:J

.field private k:I

.field private l:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 41
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-boolean v0, Lcom/google/android/location/i/a;->f:Z

    if-eqz v0, :cond_0

    const-string v0, "Gms"

    :goto_0
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "NetworkLocationListeners"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/location/internal/server/i;->a:Ljava/lang/String;

    return-void

    :cond_0
    const-string v0, "Gmm"

    goto :goto_0
.end method

.method constructor <init>(I)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x0

    const v2, 0x7fffffff

    .line 81
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 45
    new-instance v0, Ljava/util/HashMap;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/HashMap;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/location/internal/server/i;->c:Ljava/util/HashMap;

    .line 48
    new-instance v0, Ljava/util/HashMap;

    const/16 v1, 0x28

    invoke-direct {v0, v1}, Ljava/util/HashMap;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/location/internal/server/i;->d:Ljava/util/Map;

    .line 51
    iput-object v4, p0, Lcom/google/android/location/internal/server/i;->e:Lcom/google/android/location/os/j;

    .line 57
    iput v2, p0, Lcom/google/android/location/internal/server/i;->f:I

    .line 62
    iput v2, p0, Lcom/google/android/location/internal/server/i;->g:I

    .line 69
    iput v2, p0, Lcom/google/android/location/internal/server/i;->h:I

    .line 72
    iput-object v4, p0, Lcom/google/android/location/internal/server/i;->i:Lcom/google/android/location/o/n;

    .line 75
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/google/android/location/internal/server/i;->j:J

    .line 78
    iput v3, p0, Lcom/google/android/location/internal/server/i;->k:I

    .line 79
    iput v3, p0, Lcom/google/android/location/internal/server/i;->l:I

    .line 82
    iput p1, p0, Lcom/google/android/location/internal/server/i;->b:I

    .line 83
    return-void
.end method

.method static a(J)Ljava/lang/String;
    .locals 6

    .prologue
    .line 312
    const-wide/16 v0, 0x3e8

    div-long v0, p0, v0

    .line 313
    const-wide/16 v2, 0x3c

    div-long v2, v0, v2

    .line 314
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " sec. ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " min.)"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/location/internal/server/i;)Ljava/util/HashMap;
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lcom/google/android/location/internal/server/i;->c:Ljava/util/HashMap;

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/location/internal/server/i;Lcom/google/android/location/internal/server/j;J)V
    .locals 4

    .prologue
    .line 40
    iget-object v0, p1, Lcom/google/android/location/internal/server/j;->b:Landroid/app/PendingIntent;

    invoke-virtual {v0}, Landroid/app/PendingIntent;->getTargetPackage()Ljava/lang/String;

    move-result-object v1

    iget-object v0, p0, Lcom/google/android/location/internal/server/i;->d:Ljava/util/Map;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/internal/server/k;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/location/internal/server/i;->d:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v0

    const/16 v2, 0x28

    if-le v0, v2, :cond_2

    const/4 v0, 0x0

    :cond_0
    :goto_0
    if-eqz v0, :cond_1

    iget-boolean v1, p1, Lcom/google/android/location/internal/server/j;->k:Z

    if-eqz v1, :cond_3

    iget-object v0, v0, Lcom/google/android/location/internal/server/k;->b:Lcom/google/android/location/internal/server/o;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/location/internal/server/o;->a(Lcom/google/android/location/internal/server/g;J)V

    :cond_1
    :goto_1
    return-void

    :cond_2
    new-instance v0, Lcom/google/android/location/internal/server/k;

    const/4 v2, 0x0

    invoke-direct {v0, p0, v2}, Lcom/google/android/location/internal/server/k;-><init>(Lcom/google/android/location/internal/server/i;B)V

    iget-object v2, p0, Lcom/google/android/location/internal/server/i;->d:Ljava/util/Map;

    invoke-interface {v2, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_3
    iget-object v0, v0, Lcom/google/android/location/internal/server/k;->a:Lcom/google/android/location/internal/server/o;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/location/internal/server/o;->a(Lcom/google/android/location/internal/server/g;J)V

    goto :goto_1
.end method

.method private static a(Ljava/io/PrintWriter;JLcom/google/android/location/internal/server/p;Ljava/lang/String;Z)V
    .locals 3

    .prologue
    .line 303
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "NLPClient: duration="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {p1, p2}, Lcom/google/android/location/internal/server/i;->a(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " period: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p3, Lcom/google/android/location/internal/server/p;->first:Ljava/lang/Object;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " low power: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " package: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 305
    const-string v0, "com.google.android.gms"

    invoke-virtual {v0, p4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 306
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v0, " tag: "

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v0, p3, Lcom/google/android/location/internal/server/p;->second:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 308
    :cond_0
    invoke-virtual {p0}, Ljava/io/PrintWriter;->println()V

    .line 309
    return-void
.end method

.method private a(Ljava/util/Collection;)V
    .locals 7

    .prologue
    const v6, 0x7fffffff

    .line 533
    iput v6, p0, Lcom/google/android/location/internal/server/i;->g:I

    .line 534
    iput v6, p0, Lcom/google/android/location/internal/server/i;->h:I

    .line 535
    iput v6, p0, Lcom/google/android/location/internal/server/i;->f:I

    .line 537
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/internal/server/j;

    .line 538
    iget v1, v0, Lcom/google/android/location/internal/server/j;->d:I

    .line 540
    sget-boolean v3, Lcom/google/android/location/i/a;->b:Z

    if-eqz v3, :cond_0

    sget-object v3, Lcom/google/android/location/internal/server/i;->a:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Still have location intent receiver "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/android/location/o/a/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 542
    :cond_0
    iget-boolean v3, v0, Lcom/google/android/location/internal/server/j;->k:Z

    if-eqz v3, :cond_1

    .line 543
    iget v3, p0, Lcom/google/android/location/internal/server/i;->g:I

    invoke-static {v3, v1}, Ljava/lang/Math;->min(II)I

    move-result v3

    iput v3, p0, Lcom/google/android/location/internal/server/i;->g:I

    .line 555
    :goto_1
    iget v0, v0, Lcom/google/android/location/internal/server/j;->e:I

    .line 556
    if-ne v0, v6, :cond_2

    move v0, v1

    .line 558
    :goto_2
    iget v1, p0, Lcom/google/android/location/internal/server/i;->h:I

    invoke-static {v1, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    iput v0, p0, Lcom/google/android/location/internal/server/i;->h:I

    goto :goto_0

    .line 546
    :cond_1
    iget v3, p0, Lcom/google/android/location/internal/server/i;->f:I

    invoke-static {v3, v1}, Ljava/lang/Math;->min(II)I

    move-result v3

    iput v3, p0, Lcom/google/android/location/internal/server/i;->f:I

    goto :goto_1

    .line 556
    :cond_2
    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    goto :goto_2

    .line 567
    :cond_3
    iget v0, p0, Lcom/google/android/location/internal/server/i;->h:I

    if-eq v0, v6, :cond_4

    .line 568
    iget v0, p0, Lcom/google/android/location/internal/server/i;->f:I

    if-gtz v0, :cond_8

    const/4 v0, 0x0

    .line 574
    :goto_3
    const/4 v1, 0x2

    if-le v0, v1, :cond_9

    .line 575
    iget v0, p0, Lcom/google/android/location/internal/server/i;->h:I

    iget v1, p0, Lcom/google/android/location/internal/server/i;->f:I

    div-int/2addr v0, v1

    iget v1, p0, Lcom/google/android/location/internal/server/i;->f:I

    mul-int/2addr v0, v1

    iput v0, p0, Lcom/google/android/location/internal/server/i;->h:I

    .line 582
    :cond_4
    :goto_4
    sget-boolean v0, Lcom/google/android/location/i/a;->b:Z

    if-eqz v0, :cond_5

    sget-object v0, Lcom/google/android/location/internal/server/i;->a:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "BATCH: batchInterval is "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/google/android/location/internal/server/i;->h:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/location/o/a/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 586
    :cond_5
    iget v0, p0, Lcom/google/android/location/internal/server/i;->f:I

    iget v1, p0, Lcom/google/android/location/internal/server/i;->g:I

    if-gt v0, v1, :cond_6

    .line 587
    iput v6, p0, Lcom/google/android/location/internal/server/i;->g:I

    .line 592
    :cond_6
    invoke-static {}, Lcom/google/android/location/o/n;->a()Lcom/google/android/location/o/n;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/internal/server/i;->i:Lcom/google/android/location/o/n;

    .line 593
    iget v0, p0, Lcom/google/android/location/internal/server/i;->f:I

    iget v1, p0, Lcom/google/android/location/internal/server/i;->g:I

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 594
    add-int/lit8 v0, v0, 0x1

    mul-int/lit8 v0, v0, 0x3

    div-int/lit8 v1, v0, 0x2

    .line 595
    iget-object v0, p0, Lcom/google/android/location/internal/server/i;->c:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_7
    :goto_5
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_a

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/internal/server/j;

    .line 596
    iget v3, v0, Lcom/google/android/location/internal/server/j;->d:I

    if-ge v3, v1, :cond_7

    iget-object v3, v0, Lcom/google/android/location/internal/server/j;->j:Lcom/google/android/location/o/n;

    if-eqz v3, :cond_7

    .line 597
    iget-object v3, p0, Lcom/google/android/location/internal/server/i;->i:Lcom/google/android/location/o/n;

    iget-object v0, v0, Lcom/google/android/location/internal/server/j;->j:Lcom/google/android/location/o/n;

    invoke-virtual {v3, v0}, Lcom/google/android/location/o/n;->a(Lcom/google/android/location/o/n;)V

    goto :goto_5

    .line 568
    :cond_8
    iget v0, p0, Lcom/google/android/location/internal/server/i;->h:I

    iget v1, p0, Lcom/google/android/location/internal/server/i;->f:I

    div-int/2addr v0, v1

    goto :goto_3

    .line 578
    :cond_9
    iput v6, p0, Lcom/google/android/location/internal/server/i;->h:I

    goto :goto_4

    .line 600
    :cond_a
    return-void
.end method

.method private a(JLandroid/content/Context;Landroid/location/Location;ZZ)Z
    .locals 9

    .prologue
    .line 453
    const/4 v1, 0x0

    .line 454
    const/4 v0, 0x0

    .line 456
    iget-object v2, p0, Lcom/google/android/location/internal/server/i;->c:Ljava/util/HashMap;

    invoke-virtual {v2}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v2, v0

    move-object v0, v1

    .line 457
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_7

    .line 458
    if-nez v0, :cond_0

    .line 459
    invoke-direct {p0}, Lcom/google/android/location/internal/server/i;->g()Landroid/content/Intent;

    move-result-object v0

    .line 460
    const-string v1, "location"

    invoke-virtual {v0, v1, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    :cond_0
    move-object v1, v0

    .line 462
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/internal/server/j;

    .line 464
    iget-boolean v4, v0, Lcom/google/android/location/internal/server/j;->a:Z

    if-ne p5, v4, :cond_6

    if-eqz p6, :cond_1

    iget-boolean v4, v0, Lcom/google/android/location/internal/server/j;->k:Z

    if-eqz v4, :cond_6

    .line 470
    :cond_1
    iget-wide v4, v0, Lcom/google/android/location/internal/server/j;->g:J

    cmp-long v4, v4, p1

    if-gez v4, :cond_5

    .line 471
    sget-boolean v4, Lcom/google/android/location/i/a;->b:Z

    if-eqz v4, :cond_2

    sget-object v4, Lcom/google/android/location/internal/server/i;->a:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Delivering a location to a listener registered at "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v6, v0, Lcom/google/android/location/internal/server/j;->g:J

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", location timestamp is "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/android/location/o/a/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 474
    :cond_2
    invoke-virtual {v0, p3, v1}, Lcom/google/android/location/internal/server/j;->a(Landroid/content/Context;Landroid/content/Intent;)Z

    move-result v4

    if-nez v4, :cond_6

    .line 475
    sget-boolean v2, Lcom/google/android/location/i/a;->b:Z

    if-eqz v2, :cond_3

    sget-object v2, Lcom/google/android/location/internal/server/i;->a:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "dropping intent receiver"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Lcom/google/android/location/o/a/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 476
    :cond_3
    iget-object v2, p0, Lcom/google/android/location/internal/server/i;->e:Lcom/google/android/location/os/j;

    if-eqz v2, :cond_4

    .line 477
    iget-object v2, p0, Lcom/google/android/location/internal/server/i;->e:Lcom/google/android/location/os/j;

    iget-object v4, v0, Lcom/google/android/location/internal/server/j;->b:Landroid/app/PendingIntent;

    invoke-virtual {v4}, Landroid/app/PendingIntent;->hashCode()I

    move-result v4

    iget-object v0, v0, Lcom/google/android/location/internal/server/j;->b:Landroid/app/PendingIntent;

    invoke-virtual {v0}, Landroid/app/PendingIntent;->getTargetPackage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v4, v0}, Lcom/google/android/location/os/j;->a(ILjava/lang/String;)V

    .line 480
    :cond_4
    invoke-interface {v3}, Ljava/util/Iterator;->remove()V

    .line 481
    const/4 v0, 0x1

    move v2, v0

    move-object v0, v1

    goto/16 :goto_0

    .line 484
    :cond_5
    sget-boolean v4, Lcom/google/android/location/i/a;->b:Z

    if-eqz v4, :cond_6

    sget-object v4, Lcom/google/android/location/internal/server/i;->a:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Not delivering a location to a listener registered at "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v6, v0, Lcom/google/android/location/internal/server/j;->g:J

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v5, ", location timestamp is "

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v4, v0}, Lcom/google/android/location/o/a/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_6
    move-object v0, v1

    .line 489
    goto/16 :goto_0

    .line 490
    :cond_7
    return v2
.end method

.method private g()Landroid/content/Intent;
    .locals 3

    .prologue
    .line 318
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 319
    const-string v1, "com.google.android.location.internal.EXTRA_RELEASE_VERSION"

    iget v2, p0, Lcom/google/android/location/internal/server/i;->b:I

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 320
    return-object v0
.end method


# virtual methods
.method final a()I
    .locals 1

    .prologue
    .line 93
    iget v0, p0, Lcom/google/android/location/internal/server/i;->f:I

    return v0
.end method

.method final a(JLandroid/content/Context;Landroid/location/Location;Landroid/location/Location;Z)Ljava/util/List;
    .locals 9

    .prologue
    .line 400
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 404
    const/4 v6, 0x0

    move-object v1, p0

    move-wide v2, p1

    move-object v4, p3

    move-object v5, p4

    move v7, p6

    invoke-direct/range {v1 .. v7}, Lcom/google/android/location/internal/server/i;->a(JLandroid/content/Context;Landroid/location/Location;ZZ)Z

    move-result v8

    .line 406
    const/4 v6, 0x1

    move-object v1, p0

    move-wide v2, p1

    move-object v4, p3

    move-object v5, p5

    move v7, p6

    invoke-direct/range {v1 .. v7}, Lcom/google/android/location/internal/server/i;->a(JLandroid/content/Context;Landroid/location/Location;ZZ)Z

    move-result v1

    or-int/2addr v1, v8

    .line 408
    if-eqz v1, :cond_0

    .line 409
    iget-object v1, p0, Lcom/google/android/location/internal/server/i;->c:Ljava/util/HashMap;

    invoke-virtual {v1}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/google/android/location/internal/server/i;->a(Ljava/util/Collection;)V

    .line 411
    :cond_0
    return-object v0
.end method

.method final a(Landroid/app/PendingIntent;)V
    .locals 14

    .prologue
    const/4 v11, -0x1

    .line 175
    iget-object v0, p0, Lcom/google/android/location/internal/server/i;->e:Lcom/google/android/location/os/j;

    if-eqz v0, :cond_0

    .line 176
    iget-object v2, p0, Lcom/google/android/location/internal/server/i;->e:Lcom/google/android/location/os/j;

    invoke-virtual {p1}, Landroid/app/PendingIntent;->hashCode()I

    move-result v6

    invoke-virtual {p1}, Landroid/app/PendingIntent;->getTargetPackage()Ljava/lang/String;

    move-result-object v7

    new-instance v1, Lcom/google/android/location/os/ae;

    sget-object v3, Lcom/google/android/location/os/au;->ai:Lcom/google/android/location/os/au;

    iget-object v0, v2, Lcom/google/android/location/os/j;->a:Lcom/google/android/location/os/at;

    invoke-interface {v0}, Lcom/google/android/location/os/at;->a()J

    move-result-wide v4

    invoke-direct/range {v1 .. v7}, Lcom/google/android/location/os/ae;-><init>(Lcom/google/android/location/os/j;Lcom/google/android/location/os/au;JILjava/lang/String;)V

    move-object v8, v2

    move-object v9, v1

    move v10, v6

    move v12, v11

    move-object v13, v7

    invoke-virtual/range {v8 .. v13}, Lcom/google/android/location/os/j;->a(Lcom/google/android/location/os/as;IIILjava/lang/String;)V

    .line 179
    :cond_0
    iget-object v0, p0, Lcom/google/android/location/internal/server/i;->c:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/internal/server/j;

    .line 180
    if-eqz v0, :cond_1

    .line 181
    invoke-virtual {v0}, Lcom/google/android/location/internal/server/j;->b()V

    .line 182
    iget-object v0, p0, Lcom/google/android/location/internal/server/i;->c:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/location/internal/server/i;->a(Ljava/util/Collection;)V

    .line 184
    :cond_1
    return-void
.end method

.method final a(Landroid/content/Context;II)V
    .locals 6

    .prologue
    .line 415
    iput p2, p0, Lcom/google/android/location/internal/server/i;->k:I

    .line 416
    iput p3, p0, Lcom/google/android/location/internal/server/i;->l:I

    .line 417
    const/4 v0, 0x0

    .line 419
    const/4 v1, 0x0

    .line 420
    iget-object v2, p0, Lcom/google/android/location/internal/server/i;->c:Ljava/util/HashMap;

    invoke-virtual {v2}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v2, v0

    .line 421
    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 422
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/internal/server/j;

    .line 423
    iget v4, v0, Lcom/google/android/location/internal/server/j;->l:I

    if-ne v4, p2, :cond_1

    iget v4, v0, Lcom/google/android/location/internal/server/j;->m:I

    if-eq v4, p3, :cond_0

    .line 425
    :cond_1
    iput p2, v0, Lcom/google/android/location/internal/server/j;->l:I

    .line 426
    iput p3, v0, Lcom/google/android/location/internal/server/j;->m:I

    .line 427
    if-nez v1, :cond_2

    .line 428
    invoke-direct {p0}, Lcom/google/android/location/internal/server/i;->g()Landroid/content/Intent;

    move-result-object v1

    .line 429
    const-string v4, "com.google.android.location.internal.WIFI_LOCATION_STATUS"

    invoke-virtual {v1, v4, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 430
    const-string v4, "com.google.android.location.internal.CELL_LOCATION_STATUS"

    invoke-virtual {v1, v4, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 433
    :cond_2
    invoke-virtual {v0, p1, v1}, Lcom/google/android/location/internal/server/j;->a(Landroid/content/Context;Landroid/content/Intent;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 434
    sget-boolean v2, Lcom/google/android/location/i/a;->b:Z

    if-eqz v2, :cond_3

    sget-object v2, Lcom/google/android/location/internal/server/i;->a:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "dropping intent receiver"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Lcom/google/android/location/o/a/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 435
    :cond_3
    iget-object v2, p0, Lcom/google/android/location/internal/server/i;->e:Lcom/google/android/location/os/j;

    if-eqz v2, :cond_4

    .line 436
    iget-object v2, p0, Lcom/google/android/location/internal/server/i;->e:Lcom/google/android/location/os/j;

    iget-object v4, v0, Lcom/google/android/location/internal/server/j;->b:Landroid/app/PendingIntent;

    invoke-virtual {v4}, Landroid/app/PendingIntent;->hashCode()I

    move-result v4

    iget-object v0, v0, Lcom/google/android/location/internal/server/j;->b:Landroid/app/PendingIntent;

    invoke-virtual {v0}, Landroid/app/PendingIntent;->getTargetPackage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v4, v0}, Lcom/google/android/location/os/j;->a(ILjava/lang/String;)V

    .line 439
    :cond_4
    invoke-interface {v3}, Ljava/util/Iterator;->remove()V

    .line 440
    const/4 v0, 0x1

    move v2, v0

    .line 441
    goto :goto_0

    .line 446
    :cond_5
    if-eqz v2, :cond_6

    .line 447
    iget-object v0, p0, Lcom/google/android/location/internal/server/i;->c:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/location/internal/server/i;->a(Ljava/util/Collection;)V

    .line 449
    :cond_6
    return-void
.end method

.method final a(Landroid/content/Context;Landroid/app/PendingIntent;IZIZZLcom/google/android/location/o/n;Ljava/lang/String;)V
    .locals 13

    .prologue
    .line 137
    iget-object v2, p0, Lcom/google/android/location/internal/server/i;->e:Lcom/google/android/location/os/j;

    if-eqz v2, :cond_7

    .line 138
    iget-object v4, p0, Lcom/google/android/location/internal/server/i;->e:Lcom/google/android/location/os/j;

    invoke-virtual {p2}, Landroid/app/PendingIntent;->hashCode()I

    move-result v8

    if-nez p8, :cond_3

    invoke-virtual {p2}, Landroid/app/PendingIntent;->getTargetPackage()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v11

    :goto_0
    new-instance v3, Lcom/google/android/location/os/ad;

    sget-object v5, Lcom/google/android/location/os/au;->ah:Lcom/google/android/location/os/au;

    iget-object v2, v4, Lcom/google/android/location/os/j;->a:Lcom/google/android/location/os/at;

    invoke-interface {v2}, Lcom/google/android/location/os/at;->a()J

    move-result-wide v6

    move/from16 v9, p3

    move-object/from16 v10, p9

    invoke-direct/range {v3 .. v11}, Lcom/google/android/location/os/ad;-><init>(Lcom/google/android/location/os/j;Lcom/google/android/location/os/au;JIILjava/lang/String;Ljava/util/List;)V

    if-eqz p4, :cond_4

    const/4 v10, 0x1

    :goto_1
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    if-eqz p9, :cond_0

    move-object/from16 v0, p9

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_0
    const-string v2, "/"

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    if-eqz v11, :cond_6

    const/4 v2, 0x0

    move v5, v2

    :goto_2
    invoke-interface {v11}, Ljava/util/List;->size()I

    move-result v2

    if-ge v5, v2, :cond_6

    if-eqz v5, :cond_1

    const-string v2, ","

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_1
    invoke-interface {v11, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    if-nez v2, :cond_2

    const-string v2, "?"

    :cond_2
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v7

    const/16 v9, 0xa

    if-gt v7, v9, :cond_5

    :goto_3
    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    add-int/lit8 v2, v5, 0x1

    move v5, v2

    goto :goto_2

    :cond_3
    invoke-virtual/range {p8 .. p8}, Lcom/google/android/location/o/n;->c()Ljava/util/List;

    move-result-object v11

    goto :goto_0

    :cond_4
    const/4 v10, 0x0

    goto :goto_1

    :cond_5
    new-instance v9, Ljava/lang/StringBuilder;

    const-string v12, "."

    invoke-direct {v9, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    add-int/lit8 v12, v7, -0xa

    add-int/lit8 v12, v12, 0x1

    invoke-virtual {v2, v12, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v9, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    goto :goto_3

    :cond_6
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    move-object v6, v4

    move-object v7, v3

    move/from16 v9, p3

    invoke-virtual/range {v6 .. v11}, Lcom/google/android/location/os/j;->a(Lcom/google/android/location/os/as;IIILjava/lang/String;)V

    .line 142
    :cond_7
    iget-wide v2, p0, Lcom/google/android/location/internal/server/i;->j:J

    const-wide/16 v4, -0x1

    cmp-long v2, v2, v4

    if-nez v2, :cond_8

    .line 143
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/android/location/internal/server/i;->j:J

    .line 145
    :cond_8
    const-string v2, "power"

    invoke-virtual {p1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/os/PowerManager;

    .line 147
    new-instance v7, Lcom/google/android/location/o/h;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "NLP PendingIntent client in "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p2}, Landroid/app/PendingIntent;->getTargetPackage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    sget-object v4, Lcom/google/android/location/o/h;->b:[S

    invoke-direct {v7, v2, v3, v4}, Lcom/google/android/location/o/h;-><init>(Landroid/os/PowerManager;Ljava/lang/String;[S)V

    .line 151
    move-object/from16 v0, p8

    invoke-virtual {v7, v0}, Lcom/google/android/location/o/h;->a(Lcom/google/android/location/o/n;)V

    .line 152
    new-instance v2, Lcom/google/android/location/internal/server/j;

    move-object v3, p0

    move-object v4, p2

    move/from16 v5, p3

    move/from16 v6, p5

    move/from16 v8, p6

    move/from16 v9, p7

    move-object/from16 v10, p8

    move-object/from16 v11, p9

    invoke-direct/range {v2 .. v11}, Lcom/google/android/location/internal/server/j;-><init>(Lcom/google/android/location/internal/server/i;Landroid/app/PendingIntent;IILcom/google/android/location/o/h;ZZLcom/google/android/location/o/n;Ljava/lang/String;)V

    .line 154
    iget-object v3, p0, Lcom/google/android/location/internal/server/i;->c:Ljava/util/HashMap;

    invoke-virtual {v3, p2, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/location/internal/server/j;

    .line 156
    if-eqz v2, :cond_9

    .line 157
    invoke-virtual {v2}, Lcom/google/android/location/internal/server/j;->b()V

    .line 160
    :cond_9
    iget-object v2, p0, Lcom/google/android/location/internal/server/i;->c:Ljava/util/HashMap;

    invoke-virtual {v2}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/google/android/location/internal/server/i;->a(Ljava/util/Collection;)V

    .line 168
    iget v2, p0, Lcom/google/android/location/internal/server/i;->k:I

    iget v3, p0, Lcom/google/android/location/internal/server/i;->l:I

    invoke-virtual {p0, p1, v2, v3}, Lcom/google/android/location/internal/server/i;->a(Landroid/content/Context;II)V

    .line 169
    return-void
.end method

.method final a(Landroid/content/Context;Z)V
    .locals 6

    .prologue
    .line 503
    const/4 v2, 0x0

    .line 506
    const/4 v0, 0x0

    .line 507
    iget-object v1, p0, Lcom/google/android/location/internal/server/i;->c:Ljava/util/HashMap;

    invoke-virtual {v1}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .line 508
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 509
    if-nez v0, :cond_0

    .line 510
    invoke-direct {p0}, Lcom/google/android/location/internal/server/i;->g()Landroid/content/Intent;

    move-result-object v0

    .line 511
    const-string v1, "providerEnabled"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    :cond_0
    move-object v1, v0

    .line 513
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/internal/server/g;

    .line 514
    invoke-virtual {v0, p1, v1}, Lcom/google/android/location/internal/server/g;->a(Landroid/content/Context;Landroid/content/Intent;)Z

    move-result v4

    if-nez v4, :cond_5

    .line 515
    sget-boolean v2, Lcom/google/android/location/i/a;->b:Z

    if-eqz v2, :cond_1

    sget-object v2, Lcom/google/android/location/internal/server/i;->a:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "dropping intent receiver"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Lcom/google/android/location/o/a/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 516
    :cond_1
    iget-object v2, p0, Lcom/google/android/location/internal/server/i;->e:Lcom/google/android/location/os/j;

    if-eqz v2, :cond_2

    .line 517
    iget-object v2, p0, Lcom/google/android/location/internal/server/i;->e:Lcom/google/android/location/os/j;

    iget-object v4, v0, Lcom/google/android/location/internal/server/g;->b:Landroid/app/PendingIntent;

    invoke-virtual {v4}, Landroid/app/PendingIntent;->hashCode()I

    move-result v4

    iget-object v0, v0, Lcom/google/android/location/internal/server/g;->b:Landroid/app/PendingIntent;

    invoke-virtual {v0}, Landroid/app/PendingIntent;->getTargetPackage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v4, v0}, Lcom/google/android/location/os/j;->a(ILjava/lang/String;)V

    .line 520
    :cond_2
    invoke-interface {v3}, Ljava/util/Iterator;->remove()V

    .line 521
    const/4 v0, 0x1

    :goto_1
    move v2, v0

    move-object v0, v1

    .line 523
    goto :goto_0

    .line 525
    :cond_3
    if-eqz v2, :cond_4

    .line 526
    iget-object v0, p0, Lcom/google/android/location/internal/server/i;->c:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/location/internal/server/i;->a(Ljava/util/Collection;)V

    .line 528
    :cond_4
    return-void

    :cond_5
    move v0, v2

    goto :goto_1
.end method

.method final a(Lcom/google/android/location/os/j;)V
    .locals 0

    .prologue
    .line 86
    iput-object p1, p0, Lcom/google/android/location/internal/server/i;->e:Lcom/google/android/location/os/j;

    .line 87
    return-void
.end method

.method public final a(Ljava/io/PrintWriter;)V
    .locals 12

    .prologue
    .line 236
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v10

    .line 237
    iget-wide v0, p0, Lcom/google/android/location/internal/server/i;->j:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-nez v0, :cond_1

    const-string v0, "0"

    .line 239
    :goto_0
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "####NLP PendingIntent Location Client Stats: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 240
    new-instance v8, Ljava/util/HashSet;

    invoke-direct {v8}, Ljava/util/HashSet;-><init>()V

    .line 241
    new-instance v7, Ljava/util/HashSet;

    invoke-direct {v7}, Ljava/util/HashSet;-><init>()V

    .line 244
    const-string v0, "-Currently connected location PendingIntents-"

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 245
    iget-object v0, p0, Lcom/google/android/location/internal/server/i;->c:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :goto_1
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/internal/server/j;

    .line 246
    iget-boolean v1, v0, Lcom/google/android/location/internal/server/j;->k:Z

    if-eqz v1, :cond_2

    move-object v1, v7

    .line 247
    :goto_2
    iget-object v2, v0, Lcom/google/android/location/internal/server/j;->f:Ljava/lang/String;

    iget v3, v0, Lcom/google/android/location/internal/server/j;->d:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/location/e/aj;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/android/location/e/aj;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 248
    iget-wide v2, v0, Lcom/google/android/location/internal/server/j;->g:J

    sub-long v2, v10, v2

    .line 251
    iget-object v1, p0, Lcom/google/android/location/internal/server/i;->d:Ljava/util/Map;

    iget-object v4, v0, Lcom/google/android/location/internal/server/j;->f:Ljava/lang/String;

    invoke-interface {v1, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/location/internal/server/k;

    .line 253
    if-eqz v1, :cond_0

    .line 254
    iget-boolean v4, v0, Lcom/google/android/location/internal/server/j;->k:Z

    if-eqz v4, :cond_3

    iget-object v1, v1, Lcom/google/android/location/internal/server/k;->b:Lcom/google/android/location/internal/server/o;

    .line 256
    :goto_3
    iget-object v1, v1, Lcom/google/android/location/internal/server/o;->a:Ljava/util/Map;

    iget v4, v0, Lcom/google/android/location/internal/server/j;->d:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v1, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Long;

    .line 257
    if-eqz v1, :cond_0

    .line 258
    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    add-long/2addr v2, v4

    .line 261
    :cond_0
    new-instance v4, Lcom/google/android/location/internal/server/p;

    iget v1, v0, Lcom/google/android/location/internal/server/j;->d:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iget-object v5, v0, Lcom/google/android/location/internal/server/j;->h:Ljava/lang/String;

    invoke-direct {v4, v1, v5}, Lcom/google/android/location/internal/server/p;-><init>(Ljava/lang/Integer;Ljava/lang/String;)V

    iget-object v5, v0, Lcom/google/android/location/internal/server/j;->f:Ljava/lang/String;

    iget-boolean v6, v0, Lcom/google/android/location/internal/server/j;->k:Z

    move-object v1, p1

    invoke-static/range {v1 .. v6}, Lcom/google/android/location/internal/server/i;->a(Ljava/io/PrintWriter;JLcom/google/android/location/internal/server/p;Ljava/lang/String;Z)V

    goto :goto_1

    .line 237
    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "totalTime="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v2, p0, Lcom/google/android/location/internal/server/i;->j:J

    sub-long v2, v10, v2

    invoke-static {v2, v3}, Lcom/google/android/location/internal/server/i;->a(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    :cond_2
    move-object v1, v8

    .line 246
    goto :goto_2

    .line 254
    :cond_3
    iget-object v1, v1, Lcom/google/android/location/internal/server/k;->a:Lcom/google/android/location/internal/server/o;

    goto :goto_3

    .line 266
    :cond_4
    const-string v0, "-Previous packages-"

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 269
    iget-object v0, p0, Lcom/google/android/location/internal/server/i;->d:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :cond_5
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_9

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 270
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    .line 271
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/internal/server/k;

    .line 273
    iget-object v1, v0, Lcom/google/android/location/internal/server/k;->a:Lcom/google/android/location/internal/server/o;

    iget-object v1, v1, Lcom/google/android/location/internal/server/o;->a:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :cond_6
    :goto_4
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_7

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    .line 274
    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/location/internal/server/p;

    .line 275
    invoke-static {v5, v4}, Lcom/google/android/location/e/aj;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/android/location/e/aj;

    move-result-object v2

    invoke-interface {v8, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    .line 277
    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    .line 280
    const/4 v6, 0x0

    move-object v1, p1

    invoke-static/range {v1 .. v6}, Lcom/google/android/location/internal/server/i;->a(Ljava/io/PrintWriter;JLcom/google/android/location/internal/server/p;Ljava/lang/String;Z)V

    goto :goto_4

    .line 284
    :cond_7
    iget-object v0, v0, Lcom/google/android/location/internal/server/k;->b:Lcom/google/android/location/internal/server/o;

    iget-object v0, v0, Lcom/google/android/location/internal/server/o;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :cond_8
    :goto_5
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 285
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/location/internal/server/p;

    .line 286
    invoke-static {v5, v4}, Lcom/google/android/location/e/aj;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/android/location/e/aj;

    move-result-object v1

    invoke-interface {v7, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_8

    .line 288
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    .line 291
    const/4 v6, 0x1

    move-object v1, p1

    invoke-static/range {v1 .. v6}, Lcom/google/android/location/internal/server/i;->a(Ljava/io/PrintWriter;JLcom/google/android/location/internal/server/p;Ljava/lang/String;Z)V

    goto :goto_5

    .line 294
    :cond_9
    const-string v0, "####Finished NLP PendingIntent Location Client Stats"

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 295
    return-void
.end method

.method final b()I
    .locals 1

    .prologue
    .line 101
    iget v0, p0, Lcom/google/android/location/internal/server/i;->g:I

    return v0
.end method

.method final c()I
    .locals 1

    .prologue
    .line 108
    iget v0, p0, Lcom/google/android/location/internal/server/i;->h:I

    return v0
.end method

.method final d()Lcom/google/android/location/o/n;
    .locals 1

    .prologue
    .line 115
    iget-object v0, p0, Lcom/google/android/location/internal/server/i;->i:Lcom/google/android/location/o/n;

    return-object v0
.end method

.method final e()V
    .locals 2

    .prologue
    .line 190
    sget-boolean v0, Lcom/google/android/location/i/a;->b:Z

    if-eqz v0, :cond_0

    sget-object v0, Lcom/google/android/location/internal/server/i;->a:Ljava/lang/String;

    const-string v1, "Removing all location pending intents."

    invoke-static {v0, v1}, Lcom/google/android/location/o/a/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 191
    :cond_0
    iget-object v0, p0, Lcom/google/android/location/internal/server/i;->c:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->size()I

    move-result v0

    if-nez v0, :cond_1

    .line 202
    :goto_0
    return-void

    .line 194
    :cond_1
    iget-object v0, p0, Lcom/google/android/location/internal/server/i;->c:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 196
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 197
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 198
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/internal/server/j;

    invoke-virtual {v0}, Lcom/google/android/location/internal/server/j;->b()V

    .line 199
    invoke-interface {v1}, Ljava/util/Iterator;->remove()V

    goto :goto_1

    .line 201
    :cond_2
    iget-object v0, p0, Lcom/google/android/location/internal/server/i;->c:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/location/internal/server/i;->a(Ljava/util/Collection;)V

    goto :goto_0
.end method

.method final f()Ljava/util/Map;
    .locals 8

    .prologue
    const v3, 0x7fffffff

    .line 328
    new-instance v4, Ljava/util/HashMap;

    iget-object v0, p0, Lcom/google/android/location/internal/server/i;->c:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->size()I

    move-result v0

    invoke-direct {v4, v0}, Ljava/util/HashMap;-><init>(I)V

    .line 334
    iget-object v0, p0, Lcom/google/android/location/internal/server/i;->c:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/internal/server/j;

    .line 336
    iget-object v1, v0, Lcom/google/android/location/internal/server/j;->f:Ljava/lang/String;

    .line 337
    iget-object v1, v0, Lcom/google/android/location/internal/server/j;->j:Lcom/google/android/location/o/n;

    .line 339
    if-eqz v1, :cond_0

    .line 340
    invoke-virtual {v1}, Lcom/google/android/location/o/n;->c()Ljava/util/List;

    move-result-object v1

    .line 350
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v2

    const/4 v6, 0x1

    if-le v2, v6, :cond_4

    move v2, v3

    .line 356
    :goto_0
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_1
    :goto_1
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 364
    const-string v1, "com.google.android.gms"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 365
    invoke-virtual {v4, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    .line 367
    if-nez v1, :cond_2

    .line 368
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    .line 371
    :cond_2
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v7

    if-ge v2, v7, :cond_3

    .line 372
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    .line 375
    :cond_3
    invoke-virtual {v4, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 353
    :cond_4
    iget v0, v0, Lcom/google/android/location/internal/server/j;->d:I

    move v2, v0

    goto :goto_0

    .line 381
    :cond_5
    return-object v4
.end method
