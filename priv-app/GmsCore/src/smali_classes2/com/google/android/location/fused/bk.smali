.class Lcom/google/android/location/fused/bk;
.super Lcom/google/android/location/fused/bl;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/location/activity/au;


# instance fields
.field private a:J

.field final synthetic b:Lcom/google/android/location/fused/bg;


# direct methods
.method private constructor <init>(Lcom/google/android/location/fused/bg;)V
    .locals 1

    .prologue
    .line 364
    iput-object p1, p0, Lcom/google/android/location/fused/bk;->b:Lcom/google/android/location/fused/bg;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/location/fused/bl;-><init>(Lcom/google/android/location/fused/bg;B)V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/location/fused/bg;B)V
    .locals 0

    .prologue
    .line 364
    invoke-direct {p0, p1}, Lcom/google/android/location/fused/bk;-><init>(Lcom/google/android/location/fused/bg;)V

    return-void
.end method


# virtual methods
.method public final a(Z)V
    .locals 3

    .prologue
    .line 371
    iget-object v0, p0, Lcom/google/android/location/fused/bk;->b:Lcom/google/android/location/fused/bg;

    invoke-static {v0}, Lcom/google/android/location/fused/bg;->d(Lcom/google/android/location/fused/bg;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 372
    new-instance v0, Lcom/google/android/location/fused/bh;

    iget-object v1, p0, Lcom/google/android/location/fused/bk;->b:Lcom/google/android/location/fused/bg;

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/google/android/location/fused/bh;-><init>(Lcom/google/android/location/fused/bg;B)V

    invoke-virtual {p0, v0}, Lcom/google/android/location/fused/bk;->a(Lcom/google/android/location/fused/bl;)Lcom/google/android/location/fused/bl;

    .line 380
    :goto_0
    return-void

    .line 374
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/location/fused/bk;->d()V

    .line 378
    iget-object v0, p0, Lcom/google/android/location/fused/bk;->b:Lcom/google/android/location/fused/bg;

    invoke-virtual {v0, p1}, Lcom/google/android/location/fused/bg;->a(Z)V

    goto :goto_0
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 384
    iget-object v0, p0, Lcom/google/android/location/fused/bk;->b:Lcom/google/android/location/fused/bg;

    invoke-static {v0}, Lcom/google/android/location/fused/bg;->c(Lcom/google/android/location/fused/bg;)Lcom/google/android/location/fused/c;

    invoke-static {}, Lcom/google/android/location/fused/c;->a()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/location/fused/bk;->a:J

    .line 385
    return-void
.end method

.method public c()V
    .locals 1

    .prologue
    .line 389
    iget-object v0, p0, Lcom/google/android/location/fused/bk;->b:Lcom/google/android/location/fused/bg;

    invoke-static {v0}, Lcom/google/android/location/fused/bg;->e(Lcom/google/android/location/fused/bg;)Lcom/google/android/location/fused/a/s;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/location/fused/a/s;->f()V

    .line 390
    return-void
.end method

.method d()V
    .locals 3

    .prologue
    .line 433
    iget-object v0, p0, Lcom/google/android/location/fused/bk;->b:Lcom/google/android/location/fused/bg;

    invoke-static {v0}, Lcom/google/android/location/fused/bg;->e(Lcom/google/android/location/fused/bg;)Lcom/google/android/location/fused/a/s;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/location/fused/bk;->b:Lcom/google/android/location/fused/bg;

    invoke-static {v1}, Lcom/google/android/location/fused/bg;->f(Lcom/google/android/location/fused/bg;)Landroid/os/Looper;

    move-result-object v1

    iput-object p0, v0, Lcom/google/android/location/fused/a/s;->a:Lcom/google/android/location/activity/au;

    new-instance v2, Landroid/os/Handler;

    invoke-direct {v2, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v2, v0, Lcom/google/android/location/fused/a/s;->b:Landroid/os/Handler;

    .line 434
    iget-object v0, p0, Lcom/google/android/location/fused/bk;->b:Lcom/google/android/location/fused/bg;

    invoke-static {v0}, Lcom/google/android/location/fused/bg;->e(Lcom/google/android/location/fused/bg;)Lcom/google/android/location/fused/a/s;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/location/fused/a/s;->e()V

    .line 435
    return-void
.end method

.method public final e()J
    .locals 2

    .prologue
    .line 399
    iget-object v0, p0, Lcom/google/android/location/fused/bk;->b:Lcom/google/android/location/fused/bg;

    invoke-static {v0}, Lcom/google/android/location/fused/bg;->a(Lcom/google/android/location/fused/bg;)Lcom/google/android/location/fused/av;

    move-result-object v0

    iget-wide v0, v0, Lcom/google/android/location/fused/av;->c:J

    return-wide v0
.end method

.method public final f()Ljava/util/Collection;
    .locals 1

    .prologue
    .line 411
    iget-object v0, p0, Lcom/google/android/location/fused/bk;->b:Lcom/google/android/location/fused/bg;

    invoke-static {v0}, Lcom/google/android/location/fused/bg;->a(Lcom/google/android/location/fused/bg;)Lcom/google/android/location/fused/av;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/location/fused/av;->g:Ljava/util/Collection;

    return-object v0
.end method

.method public final l()V
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 416
    iget-object v0, p0, Lcom/google/android/location/fused/bk;->b:Lcom/google/android/location/fused/bg;

    invoke-static {v0}, Lcom/google/android/location/fused/bg;->c(Lcom/google/android/location/fused/bg;)Lcom/google/android/location/fused/c;

    invoke-static {}, Lcom/google/android/location/fused/c;->a()J

    move-result-wide v2

    iget-wide v4, p0, Lcom/google/android/location/fused/bk;->a:J

    sub-long/2addr v2, v4

    sget-object v0, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    iget-object v4, p0, Lcom/google/android/location/fused/bk;->b:Lcom/google/android/location/fused/bg;

    invoke-static {v4}, Lcom/google/android/location/fused/bg;->a(Lcom/google/android/location/fused/bg;)Lcom/google/android/location/fused/av;

    move-result-object v4

    iget-wide v4, v4, Lcom/google/android/location/fused/av;->b:J

    invoke-virtual {v0, v4, v5}, Ljava/util/concurrent/TimeUnit;->toNanos(J)J

    move-result-wide v4

    const-wide/16 v6, 0x2

    div-long/2addr v4, v6

    cmp-long v0, v2, v4

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    new-instance v2, Lcom/google/android/location/fused/bh;

    iget-object v3, p0, Lcom/google/android/location/fused/bk;->b:Lcom/google/android/location/fused/bg;

    invoke-direct {v2, v3, v1}, Lcom/google/android/location/fused/bh;-><init>(Lcom/google/android/location/fused/bg;B)V

    invoke-virtual {p0, v2}, Lcom/google/android/location/fused/bk;->a(Lcom/google/android/location/fused/bl;)Lcom/google/android/location/fused/bl;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/android/location/fused/bl;->a(Z)V

    .line 417
    return-void

    :cond_0
    move v0, v1

    .line 416
    goto :goto_0
.end method
