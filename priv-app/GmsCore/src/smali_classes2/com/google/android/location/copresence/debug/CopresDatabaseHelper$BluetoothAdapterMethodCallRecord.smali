.class public Lcom/google/android/location/copresence/debug/CopresDatabaseHelper$BluetoothAdapterMethodCallRecord;
.super Lcom/google/android/location/copresence/debug/a;
.source "SourceFile"


# instance fields
.field public bleDiscovering:Ljava/lang/Integer;
    .annotation runtime Lcom/google/android/location/copresence/debug/b;
        a = true
    .end annotation
.end field

.field public bluetoothConnection:Ljava/lang/Integer;
    .annotation runtime Lcom/google/android/location/copresence/debug/b;
        a = true
    .end annotation
.end field

.field public bluetoothDiscoverabilityState:Ljava/lang/Integer;
    .annotation runtime Lcom/google/android/location/copresence/debug/b;
        a = true
    .end annotation
.end field

.field public bluetoothDiscoverabilityTimeout:Ljava/lang/Integer;
    .annotation runtime Lcom/google/android/location/copresence/debug/b;
        a = true
    .end annotation
.end field

.field public bluetoothDiscovering:Ljava/lang/Integer;
    .annotation runtime Lcom/google/android/location/copresence/debug/b;
        a = true
    .end annotation
.end field

.field public bluetoothName:Ljava/lang/String;
    .annotation runtime Lcom/google/android/location/copresence/debug/b;
        a = false
    .end annotation
.end field

.field public bluetoothState:Ljava/lang/Integer;
    .annotation runtime Lcom/google/android/location/copresence/debug/b;
        a = true
    .end annotation
.end field

.field public callTimestamp:Ljava/lang/Long;
    .annotation runtime Lcom/google/android/location/copresence/debug/b;
        a = true
        d = 0x2
    .end annotation
.end field

.field public methodName:Ljava/lang/String;
    .annotation runtime Lcom/google/android/location/copresence/debug/b;
        a = true
    .end annotation
.end field

.field public startedSuccessfully:Ljava/lang/Integer;
    .annotation runtime Lcom/google/android/location/copresence/debug/b;
        a = true
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 375
    invoke-direct {p0}, Lcom/google/android/location/copresence/debug/a;-><init>()V

    return-void
.end method
