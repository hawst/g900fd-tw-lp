.class public final Lcom/google/android/location/copresence/an;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static e:Lcom/google/android/location/copresence/an;

.field private static f:I


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Ljava/util/HashMap;

.field private final c:Ljava/util/HashMap;

.field private final d:Lcom/google/android/gms/common/util/p;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 37
    const/4 v0, 0x0

    sput v0, Lcom/google/android/location/copresence/an;->f:I

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 40
    invoke-static {}, Lcom/google/android/gms/common/util/r;->c()Lcom/google/android/gms/common/util/p;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lcom/google/android/location/copresence/an;-><init>(Landroid/content/Context;Lcom/google/android/gms/common/util/p;)V

    .line 41
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Lcom/google/android/gms/common/util/p;)V
    .locals 1

    .prologue
    .line 44
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 45
    iput-object p1, p0, Lcom/google/android/location/copresence/an;->a:Landroid/content/Context;

    .line 46
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/location/copresence/an;->b:Ljava/util/HashMap;

    .line 47
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/location/copresence/an;->c:Ljava/util/HashMap;

    .line 48
    iput-object p2, p0, Lcom/google/android/location/copresence/an;->d:Lcom/google/android/gms/common/util/p;

    .line 49
    return-void
.end method

.method public static declared-synchronized a(Landroid/content/Context;)Lcom/google/android/location/copresence/an;
    .locals 2

    .prologue
    .line 52
    const-class v1, Lcom/google/android/location/copresence/an;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/google/android/location/copresence/an;->e:Lcom/google/android/location/copresence/an;

    if-nez v0, :cond_0

    .line 53
    new-instance v0, Lcom/google/android/location/copresence/an;

    invoke-direct {v0, p0}, Lcom/google/android/location/copresence/an;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/google/android/location/copresence/an;->e:Lcom/google/android/location/copresence/an;

    .line 55
    :cond_0
    sget-object v0, Lcom/google/android/location/copresence/an;->e:Lcom/google/android/location/copresence/an;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 52
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private declared-synchronized a(Lcom/google/ac/b/c/ah;)Lcom/google/android/location/copresence/ao;
    .locals 3

    .prologue
    .line 111
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/location/copresence/an;->b:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/copresence/ao;

    .line 112
    iget-object v2, v0, Lcom/google/android/location/copresence/ao;->a:Lcom/google/ac/b/c/ah;

    invoke-virtual {v2, p1}, Lcom/google/ac/b/c/ah;->equals(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v2

    if-eqz v2, :cond_0

    .line 116
    :goto_0
    monitor-exit p0

    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 111
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private a(J)V
    .locals 5

    .prologue
    .line 273
    iget-object v0, p0, Lcom/google/android/location/copresence/an;->b:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 274
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 275
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/copresence/ao;

    .line 276
    invoke-virtual {v0}, Lcom/google/android/location/copresence/ao;->b()J

    move-result-wide v2

    cmp-long v2, v2, p1

    if-gez v2, :cond_0

    .line 277
    invoke-interface {v1}, Ljava/util/Iterator;->remove()V

    .line 278
    const/4 v2, 0x1

    invoke-direct {p0, v0, v2}, Lcom/google/android/location/copresence/an;->a(Lcom/google/android/location/copresence/ao;Z)V

    goto :goto_0

    .line 281
    :cond_1
    return-void
.end method

.method private a(Lcom/google/android/location/copresence/ao;Z)V
    .locals 3

    .prologue
    .line 310
    iget-object v0, p0, Lcom/google/android/location/copresence/an;->a:Landroid/content/Context;

    iget-object v1, p1, Lcom/google/android/location/copresence/ao;->a:Lcom/google/ac/b/c/ah;

    iget v2, p1, Lcom/google/android/location/copresence/ao;->e:I

    invoke-static {v0, v1, p2, v2}, Lcom/google/android/location/copresence/debug/CopresDatabaseHelper;->a(Landroid/content/Context;Lcom/google/ac/b/c/ah;ZI)V

    .line 313
    return-void
.end method

.method private static a(Lcom/google/ac/b/c/ah;II)Z
    .locals 1

    .prologue
    .line 197
    if-eqz p0, :cond_0

    iget-object v0, p0, Lcom/google/ac/b/c/ah;->a:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/ac/b/c/ah;->a:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-ne v0, p1, :cond_0

    iget-object v0, p0, Lcom/google/ac/b/c/ah;->b:Lcom/google/ac/b/c/bt;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/ac/b/c/ah;->b:Lcom/google/ac/b/c/bt;

    iget-object v0, v0, Lcom/google/ac/b/c/bt;->a:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/ac/b/c/ah;->b:Lcom/google/ac/b/c/bt;

    iget-object v0, v0, Lcom/google/ac/b/c/bt;->a:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-ne v0, p2, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(Ljava/lang/String;Ljava/lang/Long;)Z
    .locals 6

    .prologue
    const/4 v1, 0x1

    .line 296
    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    :cond_0
    move v0, v1

    .line 306
    :goto_0
    return v0

    .line 301
    :cond_1
    iget-object v0, p0, Lcom/google/android/location/copresence/an;->c:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    .line 302
    if-eqz v0, :cond_2

    invoke-virtual {p2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    cmp-long v0, v2, v4

    if-ltz v0, :cond_3

    .line 303
    :cond_2
    iget-object v0, p0, Lcom/google/android/location/copresence/an;->c:Ljava/util/HashMap;

    invoke-virtual {v0, p1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move v0, v1

    .line 304
    goto :goto_0

    .line 306
    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic d()I
    .locals 2

    .prologue
    .line 30
    sget v0, Lcom/google/android/location/copresence/an;->f:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lcom/google/android/location/copresence/an;->f:I

    return v0
.end method


# virtual methods
.method public final declared-synchronized a(II)Lcom/google/android/location/copresence/ao;
    .locals 3

    .prologue
    .line 130
    monitor-enter p0

    const/4 v0, 0x1

    :try_start_0
    invoke-virtual {p0, v0, p1}, Lcom/google/android/location/copresence/an;->b(II)Ljava/util/List;

    move-result-object v0

    .line 132
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/copresence/ao;

    .line 133
    iget-object v2, v0, Lcom/google/android/location/copresence/ao;->a:Lcom/google/ac/b/c/ah;

    iget-object v2, v2, Lcom/google/ac/b/c/ah;->b:Lcom/google/ac/b/c/bt;

    iget-object v2, v2, Lcom/google/ac/b/c/bt;->b:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v2

    if-ne v2, p2, :cond_0

    .line 137
    :goto_0
    monitor-exit p0

    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 130
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final a()Ljava/util/Set;
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lcom/google/android/location/copresence/an;->b:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public final declared-synchronized a(Lcom/google/ac/b/c/ah;ZLjava/util/HashSet;)V
    .locals 8

    .prologue
    .line 78
    monitor-enter p0

    const/4 v1, 0x0

    if-eqz p3, :cond_0

    const-wide/16 v0, 0x0

    :try_start_0
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {p3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    if-eqz v0, :cond_6

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    cmp-long v3, v4, v6

    if-lez v3, :cond_6

    :goto_1
    move-object v1, v0

    goto :goto_0

    .line 79
    :cond_0
    iget-object v0, p1, Lcom/google/ac/b/c/ah;->f:Ljava/lang/String;

    if-nez v0, :cond_1

    iget-object v0, p1, Lcom/google/ac/b/c/ah;->e:Ljava/lang/String;

    .line 81
    :goto_2
    invoke-direct {p0, v0, v1}, Lcom/google/android/location/copresence/an;->a(Ljava/lang/String;Ljava/lang/Long;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_2

    .line 108
    :goto_3
    monitor-exit p0

    return-void

    .line 79
    :cond_1
    :try_start_1
    iget-object v0, p1, Lcom/google/ac/b/c/ah;->f:Ljava/lang/String;

    goto :goto_2

    .line 86
    :cond_2
    invoke-static {}, Lcom/google/android/location/copresence/f/b;->b()Lcom/google/ac/b/c/g;

    move-result-object v0

    iget-object v0, v0, Lcom/google/ac/b/c/g;->e:Lcom/google/ac/b/c/m;

    iget-object v0, v0, Lcom/google/ac/b/c/m;->v:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    .line 87
    const/4 v2, 0x1

    const/4 v3, 0x1

    invoke-static {p1, v2, v3}, Lcom/google/android/location/copresence/an;->a(Lcom/google/ac/b/c/ah;II)Z

    move-result v2

    if-eqz v2, :cond_4

    iget-object v2, p1, Lcom/google/ac/b/c/ah;->d:Ljava/lang/Long;

    if-eqz v2, :cond_3

    iget-object v2, p1, Lcom/google/ac/b/c/ah;->d:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    cmp-long v2, v0, v2

    if-gez v2, :cond_4

    .line 89
    :cond_3
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p1, Lcom/google/ac/b/c/ah;->d:Ljava/lang/Long;

    .line 92
    :cond_4
    iget-object v0, p0, Lcom/google/android/location/copresence/an;->d:Lcom/google/android/gms/common/util/p;

    invoke-interface {v0}, Lcom/google/android/gms/common/util/p;->a()J

    move-result-wide v2

    .line 94
    iget-object v0, p0, Lcom/google/android/location/copresence/an;->b:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 97
    invoke-direct {p0, p1}, Lcom/google/android/location/copresence/an;->a(Lcom/google/ac/b/c/ah;)Lcom/google/android/location/copresence/ao;

    move-result-object v0

    .line 98
    iget-object v1, v0, Lcom/google/android/location/copresence/ao;->g:Lcom/google/android/location/copresence/an;

    iget-object v1, v1, Lcom/google/android/location/copresence/an;->d:Lcom/google/android/gms/common/util/p;

    invoke-interface {v1}, Lcom/google/android/gms/common/util/p;->a()J

    move-result-wide v4

    iput-wide v4, v0, Lcom/google/android/location/copresence/ao;->f:J

    .line 99
    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/google/android/location/copresence/an;->a(Lcom/google/android/location/copresence/ao;Z)V

    .line 107
    :goto_4
    invoke-direct {p0, v2, v3}, Lcom/google/android/location/copresence/an;->a(J)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_3

    .line 78
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 102
    :cond_5
    :try_start_2
    new-instance v0, Lcom/google/android/location/copresence/ao;

    move-object v1, p0

    move-object v4, p1

    move v5, p2

    invoke-direct/range {v0 .. v5}, Lcom/google/android/location/copresence/ao;-><init>(Lcom/google/android/location/copresence/an;JLcom/google/ac/b/c/ah;Z)V

    .line 104
    iget-object v1, p0, Lcom/google/android/location/copresence/an;->b:Ljava/util/HashMap;

    invoke-virtual {v1, p1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 105
    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/google/android/location/copresence/an;->a(Lcom/google/android/location/copresence/ao;Z)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_4

    :cond_6
    move-object v0, v1

    goto :goto_1
.end method

.method public final a(Ljava/lang/String;J)V
    .locals 4

    .prologue
    .line 210
    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lcom/google/android/location/copresence/an;->a(Ljava/lang/String;Ljava/lang/Long;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 222
    :cond_0
    return-void

    .line 213
    :cond_1
    iget-object v0, p0, Lcom/google/android/location/copresence/an;->b:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 214
    :cond_2
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 215
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/copresence/ao;

    .line 216
    iget-object v2, v0, Lcom/google/android/location/copresence/ao;->c:Ljava/util/Set;

    invoke-interface {v2, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 217
    invoke-virtual {v0}, Lcom/google/android/location/copresence/ao;->c()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 218
    invoke-interface {v1}, Ljava/util/Iterator;->remove()V

    .line 219
    const/4 v2, 0x1

    invoke-direct {p0, v0, v2}, Lcom/google/android/location/copresence/an;->a(Lcom/google/android/location/copresence/ao;Z)V

    goto :goto_0
.end method

.method public final varargs declared-synchronized a([I)Z
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 179
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/location/copresence/an;->d:Lcom/google/android/gms/common/util/p;

    invoke-interface {v0}, Lcom/google/android/gms/common/util/p;->a()J

    move-result-wide v2

    .line 180
    invoke-direct {p0, v2, v3}, Lcom/google/android/location/copresence/an;->a(J)V

    .line 181
    iget-object v0, p0, Lcom/google/android/location/copresence/an;->b:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/copresence/ao;

    .line 182
    invoke-virtual {v0}, Lcom/google/android/location/copresence/ao;->a()J

    move-result-wide v6

    cmp-long v5, v6, v2

    if-gtz v5, :cond_0

    invoke-virtual {v0}, Lcom/google/android/location/copresence/ao;->b()J

    move-result-wide v6

    cmp-long v5, v6, v2

    if-lez v5, :cond_0

    .line 184
    iget-object v0, v0, Lcom/google/android/location/copresence/ao;->a:Lcom/google/ac/b/c/ah;

    iget-object v0, v0, Lcom/google/ac/b/c/ah;->b:Lcom/google/ac/b/c/bt;

    iget-object v0, v0, Lcom/google/ac/b/c/bt;->b:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v5

    .line 185
    array-length v6, p1

    move v0, v1

    :goto_0
    if-ge v0, v6, :cond_0

    aget v7, p1, v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 186
    if-ne v5, v7, :cond_1

    .line 187
    const/4 v0, 0x1

    .line 192
    :goto_1
    monitor-exit p0

    return v0

    .line 185
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    move v0, v1

    .line 192
    goto :goto_1

    .line 179
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b(II)Ljava/util/List;
    .locals 12

    .prologue
    .line 149
    monitor-enter p0

    :try_start_0
    new-instance v2, Landroid/util/SparseArray;

    invoke-direct {v2}, Landroid/util/SparseArray;-><init>()V

    .line 151
    iget-object v0, p0, Lcom/google/android/location/copresence/an;->d:Lcom/google/android/gms/common/util/p;

    invoke-interface {v0}, Lcom/google/android/gms/common/util/p;->a()J

    move-result-wide v4

    .line 152
    invoke-direct {p0, v4, v5}, Lcom/google/android/location/copresence/an;->a(J)V

    .line 153
    iget-object v0, p0, Lcom/google/android/location/copresence/an;->b:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/copresence/ao;

    .line 154
    iget-object v1, v0, Lcom/google/android/location/copresence/ao;->a:Lcom/google/ac/b/c/ah;

    .line 155
    const/4 v6, 0x1

    invoke-static {v1, v6, p2}, Lcom/google/android/location/copresence/an;->a(Lcom/google/ac/b/c/ah;II)Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-virtual {v0}, Lcom/google/android/location/copresence/ao;->a()J

    move-result-wide v6

    cmp-long v6, v6, v4

    if-gtz v6, :cond_0

    invoke-virtual {v0}, Lcom/google/android/location/copresence/ao;->b()J

    move-result-wide v6

    cmp-long v6, v6, v4

    if-lez v6, :cond_0

    .line 158
    iget-object v1, v1, Lcom/google/ac/b/c/ah;->b:Lcom/google/ac/b/c/bt;

    iget-object v1, v1, Lcom/google/ac/b/c/bt;->b:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v6

    .line 159
    invoke-virtual {v2, v6}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/location/copresence/ao;

    .line 160
    if-nez v1, :cond_1

    .line 161
    invoke-virtual {v2, v6, v0}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 149
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 162
    :cond_1
    :try_start_1
    invoke-virtual {v1}, Lcom/google/android/location/copresence/ao;->a()J

    move-result-wide v8

    invoke-virtual {v0}, Lcom/google/android/location/copresence/ao;->a()J

    move-result-wide v10

    cmp-long v1, v8, v10

    if-gez v1, :cond_0

    .line 163
    invoke-virtual {v2, v6}, Landroid/util/SparseArray;->remove(I)V

    .line 164
    invoke-virtual {v2, v6, v0}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    goto :goto_0

    .line 168
    :cond_2
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 169
    const/4 v0, 0x0

    :goto_1
    invoke-virtual {v2}, Landroid/util/SparseArray;->size()I

    move-result v3

    if-ge v0, v3, :cond_3

    .line 170
    invoke-virtual {v2, v0}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v3

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 169
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 172
    :cond_3
    monitor-exit p0

    return-object v1
.end method

.method public final declared-synchronized b()V
    .locals 1

    .prologue
    .line 206
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/location/copresence/an;->b:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 207
    monitor-exit p0

    return-void

    .line 206
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final b(Ljava/lang/String;J)V
    .locals 4

    .prologue
    .line 254
    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lcom/google/android/location/copresence/an;->a(Ljava/lang/String;Ljava/lang/Long;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 266
    :cond_0
    return-void

    .line 257
    :cond_1
    iget-object v0, p0, Lcom/google/android/location/copresence/an;->b:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 258
    :cond_2
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 259
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/copresence/ao;

    .line 260
    iget-object v2, v0, Lcom/google/android/location/copresence/ao;->b:Ljava/util/Set;

    invoke-interface {v2, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 261
    invoke-virtual {v0}, Lcom/google/android/location/copresence/ao;->c()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 262
    invoke-interface {v1}, Ljava/util/Iterator;->remove()V

    .line 263
    const/4 v2, 0x1

    invoke-direct {p0, v0, v2}, Lcom/google/android/location/copresence/an;->a(Lcom/google/android/location/copresence/ao;Z)V

    goto :goto_0
.end method

.method public final c()Ljava/lang/Long;
    .locals 14

    .prologue
    const-wide/16 v12, 0x0

    const-wide v4, 0x7fffffffffffffffL

    .line 231
    iget-object v0, p0, Lcom/google/android/location/copresence/an;->d:Lcom/google/android/gms/common/util/p;

    invoke-interface {v0}, Lcom/google/android/gms/common/util/p;->a()J

    move-result-wide v8

    .line 233
    iget-object v0, p0, Lcom/google/android/location/copresence/an;->b:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v10

    move-wide v2, v4

    :goto_0
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/copresence/ao;

    .line 234
    invoke-virtual {v0}, Lcom/google/android/location/copresence/ao;->a()J

    move-result-wide v6

    sub-long/2addr v6, v8

    .line 235
    cmp-long v1, v12, v6

    if-gez v1, :cond_0

    cmp-long v1, v6, v2

    if-gez v1, :cond_0

    move-wide v2, v6

    .line 238
    :cond_0
    invoke-virtual {v0}, Lcom/google/android/location/copresence/ao;->b()J

    move-result-wide v0

    .line 241
    cmp-long v6, v0, v4

    if-eqz v6, :cond_3

    .line 242
    sub-long/2addr v0, v8

    .line 243
    cmp-long v6, v0, v2

    if-gez v6, :cond_3

    .line 246
    invoke-static {v12, v13, v0, v1}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v2

    move-wide v0, v2

    :goto_1
    move-wide v2, v0

    .line 249
    goto :goto_0

    .line 250
    :cond_1
    cmp-long v0, v2, v4

    if-nez v0, :cond_2

    const/4 v0, 0x0

    :goto_2
    return-object v0

    :cond_2
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    goto :goto_2

    :cond_3
    move-wide v0, v2

    goto :goto_1
.end method
