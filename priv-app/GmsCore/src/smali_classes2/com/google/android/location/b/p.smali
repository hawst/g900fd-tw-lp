.class public final Lcom/google/android/location/b/p;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final A:Lcom/google/android/location/b/ac;

.field private final B:Lcom/google/android/location/j/i;

.field private C:I

.field private final D:Z

.field public final a:Lcom/google/android/location/b/bd;

.field final b:Ljava/util/List;

.field final c:Ljava/util/List;

.field public final d:Ljava/util/List;

.field public volatile e:J

.field public final f:Ljava/util/concurrent/atomic/AtomicIntegerArray;

.field volatile g:Z

.field public final h:Ljava/util/concurrent/atomic/AtomicLong;

.field public final i:Ljava/util/concurrent/atomic/AtomicLong;

.field public final j:Ljava/util/concurrent/atomic/AtomicLong;

.field public final k:Ljava/util/concurrent/atomic/AtomicLong;

.field public final l:Ljava/util/concurrent/atomic/AtomicLong;

.field public final m:Ljava/util/concurrent/atomic/AtomicLong;

.field public final n:Ljava/util/concurrent/atomic/AtomicLong;

.field public final o:Ljava/util/concurrent/atomic/AtomicLong;

.field public final p:Ljava/util/concurrent/atomic/AtomicLong;

.field public final q:Ljava/util/concurrent/atomic/AtomicLong;

.field public final r:Ljava/util/concurrent/atomic/AtomicLong;

.field final s:S

.field public final t:I

.field final u:I

.field public final v:Ljava/io/File;

.field final w:Lcom/google/android/location/e/a;

.field final x:Lcom/google/android/location/b/y;

.field public final y:Z

.field final z:Lcom/google/android/location/b/z;


# direct methods
.method private constructor <init>(SIIILjava/io/File;Lcom/google/android/location/e/a;Lcom/google/android/location/b/y;Lcom/google/android/location/b/z;Lcom/google/android/location/b/ac;Lcom/google/android/location/j/i;ZZ)V
    .locals 6

    .prologue
    .line 501
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 278
    const/4 v1, 0x0

    iput v1, p0, Lcom/google/android/location/b/p;->C:I

    .line 279
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/google/android/location/b/p;->g:Z

    .line 282
    new-instance v1, Ljava/util/concurrent/atomic/AtomicLong;

    invoke-direct {v1}, Ljava/util/concurrent/atomic/AtomicLong;-><init>()V

    iput-object v1, p0, Lcom/google/android/location/b/p;->h:Ljava/util/concurrent/atomic/AtomicLong;

    .line 283
    new-instance v1, Ljava/util/concurrent/atomic/AtomicLong;

    invoke-direct {v1}, Ljava/util/concurrent/atomic/AtomicLong;-><init>()V

    iput-object v1, p0, Lcom/google/android/location/b/p;->i:Ljava/util/concurrent/atomic/AtomicLong;

    .line 284
    new-instance v1, Ljava/util/concurrent/atomic/AtomicLong;

    invoke-direct {v1}, Ljava/util/concurrent/atomic/AtomicLong;-><init>()V

    iput-object v1, p0, Lcom/google/android/location/b/p;->j:Ljava/util/concurrent/atomic/AtomicLong;

    .line 285
    new-instance v1, Ljava/util/concurrent/atomic/AtomicLong;

    invoke-direct {v1}, Ljava/util/concurrent/atomic/AtomicLong;-><init>()V

    iput-object v1, p0, Lcom/google/android/location/b/p;->k:Ljava/util/concurrent/atomic/AtomicLong;

    .line 286
    new-instance v1, Ljava/util/concurrent/atomic/AtomicLong;

    invoke-direct {v1}, Ljava/util/concurrent/atomic/AtomicLong;-><init>()V

    iput-object v1, p0, Lcom/google/android/location/b/p;->l:Ljava/util/concurrent/atomic/AtomicLong;

    .line 287
    new-instance v1, Ljava/util/concurrent/atomic/AtomicLong;

    invoke-direct {v1}, Ljava/util/concurrent/atomic/AtomicLong;-><init>()V

    iput-object v1, p0, Lcom/google/android/location/b/p;->m:Ljava/util/concurrent/atomic/AtomicLong;

    .line 288
    new-instance v1, Ljava/util/concurrent/atomic/AtomicLong;

    invoke-direct {v1}, Ljava/util/concurrent/atomic/AtomicLong;-><init>()V

    iput-object v1, p0, Lcom/google/android/location/b/p;->n:Ljava/util/concurrent/atomic/AtomicLong;

    .line 289
    new-instance v1, Ljava/util/concurrent/atomic/AtomicLong;

    invoke-direct {v1}, Ljava/util/concurrent/atomic/AtomicLong;-><init>()V

    iput-object v1, p0, Lcom/google/android/location/b/p;->o:Ljava/util/concurrent/atomic/AtomicLong;

    .line 290
    new-instance v1, Ljava/util/concurrent/atomic/AtomicLong;

    invoke-direct {v1}, Ljava/util/concurrent/atomic/AtomicLong;-><init>()V

    iput-object v1, p0, Lcom/google/android/location/b/p;->p:Ljava/util/concurrent/atomic/AtomicLong;

    .line 291
    new-instance v1, Ljava/util/concurrent/atomic/AtomicLong;

    invoke-direct {v1}, Ljava/util/concurrent/atomic/AtomicLong;-><init>()V

    iput-object v1, p0, Lcom/google/android/location/b/p;->q:Ljava/util/concurrent/atomic/AtomicLong;

    .line 292
    new-instance v1, Ljava/util/concurrent/atomic/AtomicLong;

    invoke-direct {v1}, Ljava/util/concurrent/atomic/AtomicLong;-><init>()V

    iput-object v1, p0, Lcom/google/android/location/b/p;->r:Ljava/util/concurrent/atomic/AtomicLong;

    .line 502
    iput-short p1, p0, Lcom/google/android/location/b/p;->s:S

    .line 503
    iput p3, p0, Lcom/google/android/location/b/p;->t:I

    .line 504
    iput p4, p0, Lcom/google/android/location/b/p;->u:I

    .line 505
    iput-object p5, p0, Lcom/google/android/location/b/p;->v:Ljava/io/File;

    .line 506
    iput-object p6, p0, Lcom/google/android/location/b/p;->w:Lcom/google/android/location/e/a;

    .line 507
    iput-object p7, p0, Lcom/google/android/location/b/p;->x:Lcom/google/android/location/b/y;

    .line 508
    iput-object p8, p0, Lcom/google/android/location/b/p;->z:Lcom/google/android/location/b/z;

    .line 509
    iput-object p9, p0, Lcom/google/android/location/b/p;->A:Lcom/google/android/location/b/ac;

    .line 510
    move-object/from16 v0, p10

    iput-object v0, p0, Lcom/google/android/location/b/p;->B:Lcom/google/android/location/j/i;

    .line 511
    move/from16 v0, p11

    iput-boolean v0, p0, Lcom/google/android/location/b/p;->y:Z

    .line 512
    move/from16 v0, p12

    iput-boolean v0, p0, Lcom/google/android/location/b/p;->D:Z

    .line 514
    new-instance v1, Lcom/google/android/location/b/q;

    invoke-direct {v1, p0, p2}, Lcom/google/android/location/b/q;-><init>(Lcom/google/android/location/b/p;I)V

    iput-object v1, p0, Lcom/google/android/location/b/p;->a:Lcom/google/android/location/b/bd;

    .line 522
    if-eqz p11, :cond_0

    .line 523
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/location/b/p;->b:Ljava/util/List;

    .line 524
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/location/b/p;->c:Ljava/util/List;

    .line 525
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/location/b/p;->d:Ljava/util/List;

    .line 526
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/location/b/p;->f:Ljava/util/concurrent/atomic/AtomicIntegerArray;

    .line 546
    :goto_0
    return-void

    .line 529
    :cond_0
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1, p3}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v1, p0, Lcom/google/android/location/b/p;->b:Ljava/util/List;

    .line 530
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1, p3}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v1, p0, Lcom/google/android/location/b/p;->c:Ljava/util/List;

    .line 531
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1, p3}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v1, p0, Lcom/google/android/location/b/p;->d:Ljava/util/List;

    .line 532
    const/4 v1, 0x0

    :goto_1
    if-ge v1, p3, :cond_1

    .line 533
    iget-object v2, p0, Lcom/google/android/location/b/p;->c:Ljava/util/List;

    new-instance v3, Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-direct {v3}, Ljava/util/concurrent/ConcurrentLinkedQueue;-><init>()V

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 534
    iget-object v2, p0, Lcom/google/android/location/b/p;->d:Ljava/util/List;

    new-instance v3, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v3}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 532
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 538
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/location/b/p;->b()Z

    .line 539
    const/4 v1, 0x0

    :goto_2
    if-ge v1, p3, :cond_2

    .line 540
    iget-object v2, p0, Lcom/google/android/location/b/p;->b:Ljava/util/List;

    new-instance v3, Ljava/io/File;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ".chunk"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, p5, v4}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 539
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 542
    :cond_2
    invoke-direct {p0}, Lcom/google/android/location/b/p;->c()V

    .line 544
    new-instance v1, Ljava/util/concurrent/atomic/AtomicIntegerArray;

    invoke-direct {v1, p3}, Ljava/util/concurrent/atomic/AtomicIntegerArray;-><init>(I)V

    iput-object v1, p0, Lcom/google/android/location/b/p;->f:Ljava/util/concurrent/atomic/AtomicIntegerArray;

    goto :goto_0
.end method

.method public synthetic constructor <init>(SIIILjava/io/File;Lcom/google/android/location/e/a;Lcom/google/android/location/b/y;Lcom/google/android/location/b/z;Lcom/google/android/location/b/ac;Lcom/google/android/location/j/i;ZZB)V
    .locals 0

    .prologue
    .line 77
    invoke-direct/range {p0 .. p12}, Lcom/google/android/location/b/p;-><init>(SIIILjava/io/File;Lcom/google/android/location/e/a;Lcom/google/android/location/b/y;Lcom/google/android/location/b/z;Lcom/google/android/location/b/ac;Lcom/google/android/location/j/i;ZZ)V

    return-void
.end method

.method private a(Ljava/lang/Object;)I
    .locals 2

    .prologue
    .line 748
    iget-object v0, p0, Lcom/google/android/location/b/p;->A:Lcom/google/android/location/b/ac;

    if-eqz v0, :cond_0

    .line 749
    iget-object v0, p0, Lcom/google/android/location/b/p;->A:Lcom/google/android/location/b/ac;

    invoke-interface {v0}, Lcom/google/android/location/b/ac;->a()I

    move-result v0

    .line 753
    :goto_0
    iget v1, p0, Lcom/google/android/location/b/p;->t:I

    rem-int/2addr v0, v1

    iget v1, p0, Lcom/google/android/location/b/p;->t:I

    add-int/2addr v0, v1

    iget v1, p0, Lcom/google/android/location/b/p;->t:I

    rem-int/2addr v0, v1

    return v0

    .line 751
    :cond_0
    invoke-virtual {p1}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0
.end method

.method private a(Lcom/google/android/location/b/ab;Lcom/google/android/location/e/ax;)V
    .locals 4

    .prologue
    .line 789
    iget-object v0, p0, Lcom/google/android/location/b/p;->l:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicLong;->incrementAndGet()J

    .line 790
    iget-wide v0, p1, Lcom/google/android/location/b/ab;->c:J

    invoke-virtual {p2, v0, v1}, Lcom/google/android/location/e/ax;->a(J)Ljava/lang/Object;

    move-result-object v0

    .line 791
    iget-object v1, p0, Lcom/google/android/location/b/p;->a:Lcom/google/android/location/b/bd;

    monitor-enter v1

    .line 792
    :try_start_0
    iget-object v2, p0, Lcom/google/android/location/b/p;->a:Lcom/google/android/location/b/bd;

    iget-object v3, p1, Lcom/google/android/location/b/ab;->a:Ljava/lang/Object;

    invoke-virtual {v2, v3, p2}, Lcom/google/android/location/b/bd;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 793
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 795
    iget-object v1, p1, Lcom/google/android/location/b/ab;->b:Lcom/google/android/location/b/x;

    if-eqz v1, :cond_0

    .line 796
    iget-object v1, p1, Lcom/google/android/location/b/ab;->b:Lcom/google/android/location/b/x;

    invoke-interface {v1, v0}, Lcom/google/android/location/b/x;->a(Ljava/lang/Object;)V

    .line 798
    :cond_0
    return-void

    .line 793
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private a(Lcom/google/android/location/b/ab;Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 780
    iget-object v0, p0, Lcom/google/android/location/b/p;->m:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicLong;->incrementAndGet()J

    .line 781
    iget-object v0, p1, Lcom/google/android/location/b/ab;->b:Lcom/google/android/location/b/x;

    if-eqz v0, :cond_0

    .line 782
    iget-object v0, p1, Lcom/google/android/location/b/ab;->b:Lcom/google/android/location/b/x;

    invoke-interface {v0}, Lcom/google/android/location/b/x;->a()V

    .line 784
    :cond_0
    iget-object v0, p0, Lcom/google/android/location/b/p;->B:Lcom/google/android/location/j/i;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/location/b/p;->z:Lcom/google/android/location/b/z;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/location/b/p;->B:Lcom/google/android/location/j/i;

    new-instance v1, Lcom/google/android/location/b/u;

    invoke-direct {v1, p0, p2}, Lcom/google/android/location/b/u;-><init>(Lcom/google/android/location/b/p;Ljava/lang/Object;)V

    invoke-virtual {v0, v1}, Lcom/google/android/location/j/i;->execute(Ljava/lang/Runnable;)V

    .line 785
    :cond_1
    return-void
.end method

.method static varargs a([Ljava/io/InputStream;)V
    .locals 3

    .prologue
    .line 1001
    array-length v1, p0

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_1

    aget-object v2, p0, v0

    .line 1002
    if-eqz v2, :cond_0

    .line 1004
    :try_start_0
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1001
    :cond_0
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :catch_0
    move-exception v2

    goto :goto_1

    .line 1010
    :cond_1
    return-void
.end method

.method private static varargs a([Ljava/io/OutputStream;)V
    .locals 3

    .prologue
    .line 1013
    array-length v1, p0

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_1

    aget-object v2, p0, v0

    .line 1014
    if-eqz v2, :cond_0

    .line 1016
    :try_start_0
    invoke-virtual {v2}, Ljava/io/OutputStream;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1013
    :cond_0
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :catch_0
    move-exception v2

    goto :goto_1

    .line 1022
    :cond_1
    return-void
.end method

.method private c()V
    .locals 5

    .prologue
    .line 914
    iget-object v0, p0, Lcom/google/android/location/b/p;->v:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    if-nez v0, :cond_1

    .line 926
    :cond_0
    return-void

    .line 918
    :cond_1
    iget-object v0, p0, Lcom/google/android/location/b/p;->v:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v1

    .line 919
    if-eqz v1, :cond_0

    .line 920
    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    .line 921
    iget-object v4, p0, Lcom/google/android/location/b/p;->b:Ljava/util/List;

    invoke-interface {v4, v3}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_2

    .line 922
    invoke-virtual {v3}, Ljava/io/File;->delete()Z

    .line 920
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method public final a(Ljava/lang/Object;J)Ljava/lang/Object;
    .locals 6

    .prologue
    const/4 v0, 0x1

    .line 578
    iget-boolean v1, p0, Lcom/google/android/location/b/p;->y:Z

    if-eqz v1, :cond_1

    .line 579
    iget-object v0, p0, Lcom/google/android/location/b/p;->h:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicLong;->incrementAndGet()J

    .line 581
    iget-object v1, p0, Lcom/google/android/location/b/p;->a:Lcom/google/android/location/b/bd;

    monitor-enter v1

    .line 582
    :try_start_0
    iget-object v0, p0, Lcom/google/android/location/b/p;->a:Lcom/google/android/location/b/bd;

    invoke-virtual {v0, p1}, Lcom/google/android/location/b/bd;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/e/ax;

    .line 583
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 584
    if-nez v0, :cond_0

    .line 585
    iget-object v0, p0, Lcom/google/android/location/b/p;->j:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicLong;->incrementAndGet()J

    .line 586
    const/4 v0, 0x0

    .line 616
    :goto_0
    return-object v0

    .line 583
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 588
    :cond_0
    iget-object v1, p0, Lcom/google/android/location/b/p;->i:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicLong;->incrementAndGet()J

    .line 589
    invoke-virtual {v0, p2, p3}, Lcom/google/android/location/e/ax;->a(J)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0

    .line 593
    :cond_1
    new-instance v1, Ljava/util/concurrent/CountDownLatch;

    invoke-direct {v1, v0}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V

    .line 594
    new-instance v2, Ljava/util/concurrent/atomic/AtomicReference;

    invoke-direct {v2}, Ljava/util/concurrent/atomic/AtomicReference;-><init>()V

    .line 596
    new-instance v3, Lcom/google/android/location/b/r;

    invoke-direct {v3, p0, v2, v1}, Lcom/google/android/location/b/r;-><init>(Lcom/google/android/location/b/p;Ljava/util/concurrent/atomic/AtomicReference;Ljava/util/concurrent/CountDownLatch;)V

    iget-object v4, p0, Lcom/google/android/location/b/p;->B:Lcom/google/android/location/j/i;

    if-eqz v4, :cond_3

    :goto_1
    const-string v4, "getAsync() could only be called when executor is not null."

    invoke-static {v0, v4}, Lcom/google/android/location/o/j;->a(ZLjava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/location/b/p;->h:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicLong;->incrementAndGet()J

    invoke-direct {p0, p1}, Lcom/google/android/location/b/p;->a(Ljava/lang/Object;)I

    move-result v4

    iget-object v5, p0, Lcom/google/android/location/b/p;->a:Lcom/google/android/location/b/bd;

    monitor-enter v5

    :try_start_1
    iget-object v0, p0, Lcom/google/android/location/b/p;->a:Lcom/google/android/location/b/bd;

    invoke-virtual {v0, p1}, Lcom/google/android/location/b/bd;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/e/ax;

    monitor-exit v5
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    if-eqz v0, :cond_4

    iget-object v4, p0, Lcom/google/android/location/b/p;->i:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v4}, Ljava/util/concurrent/atomic/AtomicLong;->incrementAndGet()J

    invoke-virtual {v0, p2, p3}, Lcom/google/android/location/e/ax;->a(J)Ljava/lang/Object;

    move-result-object v0

    iget-object v4, p0, Lcom/google/android/location/b/p;->B:Lcom/google/android/location/j/i;

    new-instance v5, Lcom/google/android/location/b/s;

    invoke-direct {v5, p0, v3, p1, v0}, Lcom/google/android/location/b/s;-><init>(Lcom/google/android/location/b/p;Lcom/google/android/location/b/x;Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-virtual {v4, v5}, Lcom/google/android/location/j/i;->execute(Ljava/lang/Runnable;)V

    :goto_2
    iget-boolean v0, p0, Lcom/google/android/location/b/p;->D:Z

    if-nez v0, :cond_2

    invoke-virtual {p0}, Lcom/google/android/location/b/p;->a()V

    .line 611
    :cond_2
    const-wide/16 v4, 0x1388

    :try_start_2
    sget-object v0, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v1, v4, v5, v0}, Ljava/util/concurrent/CountDownLatch;->await(JLjava/util/concurrent/TimeUnit;)Z
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_0

    .line 616
    invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    goto :goto_0

    .line 596
    :cond_3
    const/4 v0, 0x0

    goto :goto_1

    :catchall_1
    move-exception v0

    monitor-exit v5

    throw v0

    :cond_4
    iget-object v0, p0, Lcom/google/android/location/b/p;->j:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicLong;->incrementAndGet()J

    iget-object v0, p0, Lcom/google/android/location/b/p;->c:Ljava/util/List;

    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/ConcurrentLinkedQueue;

    new-instance v4, Lcom/google/android/location/b/ab;

    invoke-direct {v4, p1, v3, p2, p3}, Lcom/google/android/location/b/ab;-><init>(Ljava/lang/Object;Lcom/google/android/location/b/x;J)V

    invoke-virtual {v0, v4}, Ljava/util/concurrent/ConcurrentLinkedQueue;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 613
    :catch_0
    move-exception v0

    invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    goto/16 :goto_0
.end method

.method public final a()V
    .locals 4

    .prologue
    const/4 v3, -0x1

    .line 726
    iget-boolean v0, p0, Lcom/google/android/location/b/p;->y:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/location/b/p;->g:Z

    if-eqz v0, :cond_1

    .line 744
    :cond_0
    :goto_0
    return-void

    .line 730
    :cond_1
    iget v1, p0, Lcom/google/android/location/b/p;->C:I

    const/4 v0, 0x0

    move v2, v1

    move v1, v0

    :goto_1
    iget v0, p0, Lcom/google/android/location/b/p;->t:I

    if-ge v1, v0, :cond_4

    iget-object v0, p0, Lcom/google/android/location/b/p;->c:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/location/b/p;->d:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentHashMap;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/location/b/p;->f:Ljava/util/concurrent/atomic/AtomicIntegerArray;

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicIntegerArray;->get(I)I

    move-result v0

    if-eqz v0, :cond_3

    .line 731
    :cond_2
    :goto_2
    if-eq v2, v3, :cond_0

    .line 732
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/location/b/p;->g:Z

    .line 733
    iget-object v0, p0, Lcom/google/android/location/b/p;->B:Lcom/google/android/location/j/i;

    new-instance v1, Lcom/google/android/location/b/t;

    invoke-direct {v1, p0, v2}, Lcom/google/android/location/b/t;-><init>(Lcom/google/android/location/b/p;I)V

    invoke-virtual {v0, v1}, Lcom/google/android/location/j/i;->execute(Ljava/lang/Runnable;)V

    goto :goto_0

    .line 730
    :cond_3
    add-int/lit8 v0, v2, 0x1

    iget v2, p0, Lcom/google/android/location/b/p;->t:I

    rem-int v2, v0, v2

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    :cond_4
    move v2, v3

    goto :goto_2
.end method

.method final a(Lcom/google/android/location/b/bd;Ljava/util/concurrent/ConcurrentHashMap;Ljava/util/concurrent/ConcurrentLinkedQueue;Z)V
    .locals 8

    .prologue
    .line 864
    :goto_0
    invoke-virtual {p3}, Ljava/util/concurrent/ConcurrentLinkedQueue;->poll()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/b/ab;

    if-eqz v0, :cond_4

    .line 865
    iget-object v2, v0, Lcom/google/android/location/b/ab;->a:Ljava/lang/Object;

    .line 866
    invoke-virtual {p2, v2}, Ljava/util/concurrent/ConcurrentHashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 867
    invoke-virtual {p2, v2}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/location/e/ax;

    .line 868
    if-eqz p4, :cond_0

    iget-wide v4, v1, Lcom/google/android/location/e/ax;->b:J

    iget-wide v6, p0, Lcom/google/android/location/b/p;->e:J

    cmp-long v3, v4, v6

    if-ltz v3, :cond_3

    .line 869
    :cond_0
    invoke-direct {p0, v0, v1}, Lcom/google/android/location/b/p;->a(Lcom/google/android/location/b/ab;Lcom/google/android/location/e/ax;)V

    goto :goto_0

    .line 873
    :cond_1
    invoke-virtual {p1, v2}, Lcom/google/android/location/b/bd;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 874
    invoke-virtual {p1, v2}, Lcom/google/android/location/b/bd;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/location/e/ax;

    .line 875
    if-eqz p4, :cond_2

    iget-wide v4, v1, Lcom/google/android/location/e/ax;->b:J

    iget-wide v6, p0, Lcom/google/android/location/b/p;->e:J

    cmp-long v3, v4, v6

    if-ltz v3, :cond_3

    .line 876
    :cond_2
    invoke-direct {p0, v0, v1}, Lcom/google/android/location/b/p;->a(Lcom/google/android/location/b/ab;Lcom/google/android/location/e/ax;)V

    goto :goto_0

    .line 881
    :cond_3
    invoke-direct {p0, v0, v2}, Lcom/google/android/location/b/p;->a(Lcom/google/android/location/b/ab;Ljava/lang/Object;)V

    goto :goto_0

    .line 884
    :cond_4
    return-void
.end method

.method final a(Ljava/io/File;Lcom/google/android/location/b/bd;)V
    .locals 9

    .prologue
    const/4 v8, 0x2

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 929
    iget-object v0, p0, Lcom/google/android/location/b/p;->q:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicLong;->incrementAndGet()J

    .line 932
    invoke-virtual {p0}, Lcom/google/android/location/b/p;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 933
    invoke-virtual {p1}, Ljava/io/File;->delete()Z

    .line 937
    :cond_0
    :try_start_0
    invoke-virtual {p1}, Ljava/io/File;->createNewFile()Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    .line 943
    new-instance v2, Lcom/google/android/location/b/ar;

    iget-short v0, p0, Lcom/google/android/location/b/p;->s:S

    iget-object v1, p0, Lcom/google/android/location/b/p;->w:Lcom/google/android/location/e/a;

    invoke-direct {v2, v0, p1, v1}, Lcom/google/android/location/b/ar;-><init>(ILjava/io/File;Lcom/google/android/location/e/a;)V

    .line 945
    new-instance v3, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v3}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 946
    new-instance v4, Ljava/io/DataOutputStream;

    invoke-direct {v4, v3}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 948
    :try_start_1
    invoke-virtual {p2}, Lcom/google/android/location/b/bd;->size()I

    move-result v0

    invoke-virtual {v4, v0}, Ljava/io/DataOutputStream;->writeInt(I)V

    .line 949
    invoke-virtual {p2}, Lcom/google/android/location/b/bd;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 950
    iget-object v1, p0, Lcom/google/android/location/b/p;->x:Lcom/google/android/location/b/y;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    .line 951
    iget-object v1, p0, Lcom/google/android/location/b/p;->x:Lcom/google/android/location/b/y;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/location/e/ax;

    iget-object v1, v1, Lcom/google/android/location/e/ax;->a:Ljava/lang/Object;

    .line 952
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/e/ax;

    iget-wide v0, v0, Lcom/google/android/location/e/ax;->b:J

    invoke-virtual {v4, v0, v1}, Ljava/io/DataOutputStream;->writeLong(J)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 957
    :catch_0
    move-exception v0

    :try_start_2
    iget-object v0, p0, Lcom/google/android/location/b/p;->r:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicLong;->incrementAndGet()J
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 959
    new-array v0, v8, [Ljava/io/OutputStream;

    aput-object v4, v0, v6

    aput-object v3, v0, v7

    invoke-static {v0}, Lcom/google/android/location/b/p;->a([Ljava/io/OutputStream;)V

    .line 960
    :goto_1
    return-void

    .line 939
    :catch_1
    move-exception v0

    iget-object v0, p0, Lcom/google/android/location/b/p;->r:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicLong;->incrementAndGet()J

    goto :goto_1

    .line 954
    :cond_1
    :try_start_3
    invoke-virtual {v3}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/google/android/location/b/ar;->a([B)V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 959
    new-array v0, v8, [Ljava/io/OutputStream;

    aput-object v4, v0, v6

    aput-object v3, v0, v7

    invoke-static {v0}, Lcom/google/android/location/b/p;->a([Ljava/io/OutputStream;)V

    goto :goto_1

    :catchall_0
    move-exception v0

    new-array v1, v8, [Ljava/io/OutputStream;

    aput-object v4, v1, v6

    aput-object v3, v1, v7

    invoke-static {v1}, Lcom/google/android/location/b/p;->a([Ljava/io/OutputStream;)V

    throw v0
.end method

.method public final a(Ljava/lang/Object;Ljava/lang/Object;J)V
    .locals 3

    .prologue
    .line 668
    new-instance v1, Lcom/google/android/location/e/ax;

    invoke-direct {v1, p2, p3, p4}, Lcom/google/android/location/e/ax;-><init>(Ljava/lang/Object;J)V

    .line 670
    iget-object v2, p0, Lcom/google/android/location/b/p;->a:Lcom/google/android/location/b/bd;

    monitor-enter v2

    .line 671
    :try_start_0
    iget-object v0, p0, Lcom/google/android/location/b/p;->a:Lcom/google/android/location/b/bd;

    invoke-virtual {v0, p1, v1}, Lcom/google/android/location/b/bd;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 672
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 674
    iget-boolean v0, p0, Lcom/google/android/location/b/p;->y:Z

    if-nez v0, :cond_0

    .line 675
    invoke-direct {p0, p1}, Lcom/google/android/location/b/p;->a(Ljava/lang/Object;)I

    move-result v0

    .line 676
    iget-object v2, p0, Lcom/google/android/location/b/p;->d:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0, p1, v1}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 678
    iget-boolean v0, p0, Lcom/google/android/location/b/p;->D:Z

    if-nez v0, :cond_0

    .line 679
    invoke-virtual {p0}, Lcom/google/android/location/b/p;->a()V

    .line 682
    :cond_0
    return-void

    .line 672
    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0
.end method

.method final b()Z
    .locals 1

    .prologue
    .line 891
    iget-object v0, p0, Lcom/google/android/location/b/p;->v:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 892
    const/4 v0, 0x1

    .line 896
    :goto_0
    return v0

    .line 895
    :cond_0
    iget-object v0, p0, Lcom/google/android/location/b/p;->v:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    .line 896
    const/4 v0, 0x0

    goto :goto_0
.end method
