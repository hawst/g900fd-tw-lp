.class final Lcom/google/android/location/copresence/d/ae;
.super Lcom/google/android/location/copresence/k/f;
.source "SourceFile"


# instance fields
.field final a:Ljava/lang/String;

.field final synthetic b:Lcom/google/android/location/copresence/d/n;


# direct methods
.method constructor <init>(Lcom/google/android/location/copresence/d/n;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 286
    iput-object p1, p0, Lcom/google/android/location/copresence/d/ae;->b:Lcom/google/android/location/copresence/d/n;

    .line 287
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Name="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x2

    new-array v1, v1, [Lcom/google/android/location/copresence/k/f;

    const/4 v2, 0x0

    iget-object v3, p1, Lcom/google/android/location/copresence/d/n;->g:Lcom/google/android/location/copresence/k/f;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-object v3, p1, Lcom/google/android/location/copresence/d/n;->f:Lcom/google/android/location/copresence/k/f;

    aput-object v3, v1, v2

    invoke-direct {p0, v0, v1}, Lcom/google/android/location/copresence/k/f;-><init>(Ljava/lang/String;[Lcom/google/android/location/copresence/k/f;)V

    .line 288
    iput-object p2, p0, Lcom/google/android/location/copresence/d/ae;->a:Ljava/lang/String;

    .line 289
    return-void
.end method


# virtual methods
.method public final a()Z
    .locals 2

    .prologue
    .line 298
    iget-object v0, p0, Lcom/google/android/location/copresence/d/ae;->b:Lcom/google/android/location/copresence/d/n;

    iget-object v0, v0, Lcom/google/android/location/copresence/d/n;->g:Lcom/google/android/location/copresence/k/f;

    invoke-virtual {v0}, Lcom/google/android/location/copresence/k/f;->b()Z

    move-result v0

    if-nez v0, :cond_1

    .line 299
    const/4 v0, 0x6

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 300
    const-string v0, "BluetoothStates: Cannot set name if user settings not saved"

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->e(Ljava/lang/String;)I

    .line 302
    :cond_0
    const/4 v0, 0x0

    .line 304
    :goto_0
    return v0

    :cond_1
    iget-object v0, p0, Lcom/google/android/location/copresence/d/ae;->b:Lcom/google/android/location/copresence/d/n;

    iget-object v0, v0, Lcom/google/android/location/copresence/d/n;->b:Lcom/google/android/location/copresence/d/e;

    iget-object v1, p0, Lcom/google/android/location/copresence/d/ae;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/location/copresence/d/e;->a(Ljava/lang/String;)Z

    move-result v0

    goto :goto_0
.end method

.method public final b()Z
    .locals 2

    .prologue
    .line 293
    iget-object v0, p0, Lcom/google/android/location/copresence/d/ae;->a:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/location/copresence/d/ae;->b:Lcom/google/android/location/copresence/d/n;

    iget-object v1, v1, Lcom/google/android/location/copresence/d/n;->b:Lcom/google/android/location/copresence/d/e;

    iget-object v1, v1, Lcom/google/android/location/copresence/d/e;->a:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v1}, Landroid/bluetooth/BluetoothAdapter;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method
