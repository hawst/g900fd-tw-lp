.class final Lcom/google/android/location/settings/e;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final a:Landroid/accounts/Account;

.field final b:Landroid/view/ViewGroup;

.field final c:Landroid/view/ViewGroup;

.field final d:Landroid/view/ViewGroup;

.field final synthetic e:Lcom/google/android/location/settings/GoogleLocationSettingsActivity;


# direct methods
.method public constructor <init>(Lcom/google/android/location/settings/GoogleLocationSettingsActivity;Landroid/accounts/Account;)V
    .locals 3

    .prologue
    .line 582
    iput-object p1, p0, Lcom/google/android/location/settings/e;->e:Lcom/google/android/location/settings/GoogleLocationSettingsActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 583
    iput-object p2, p0, Lcom/google/android/location/settings/e;->a:Landroid/accounts/Account;

    .line 585
    invoke-virtual {p1}, Lcom/google/android/location/settings/GoogleLocationSettingsActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    sget v1, Lcom/google/android/gms/l;->cx:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/google/android/location/settings/e;->b:Landroid/view/ViewGroup;

    .line 587
    iget-object v0, p0, Lcom/google/android/location/settings/e;->b:Landroid/view/ViewGroup;

    iget-object v1, p2, Landroid/accounts/Account;->name:Ljava/lang/String;

    sget v2, Lcom/google/android/gms/j;->jw:I

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v1}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 589
    iget-object v0, p0, Lcom/google/android/location/settings/e;->b:Landroid/view/ViewGroup;

    sget v1, Lcom/google/android/gms/j;->lq:I

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/google/android/location/settings/e;->c:Landroid/view/ViewGroup;

    .line 590
    iget-object v0, p0, Lcom/google/android/location/settings/e;->b:Landroid/view/ViewGroup;

    sget v1, Lcom/google/android/gms/j;->lo:I

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/google/android/location/settings/e;->d:Landroid/view/ViewGroup;

    .line 591
    return-void
.end method


# virtual methods
.method final a(Landroid/view/ViewGroup;ZILjava/lang/String;Ljava/lang/Class;)Landroid/view/ViewGroup;
    .locals 4

    .prologue
    .line 599
    iget-object v1, p0, Lcom/google/android/location/settings/e;->e:Lcom/google/android/location/settings/GoogleLocationSettingsActivity;

    .line 600
    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2, v1, p5}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 601
    const-string v0, "com.google.android.location.settings.extra.account"

    iget-object v3, p0, Lcom/google/android/location/settings/e;->a:Landroid/accounts/Account;

    iget-object v3, v3, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v2, v0, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 602
    invoke-static {p1, p3}, Lcom/google/android/location/settings/r;->a(Landroid/view/ViewGroup;I)V

    if-eqz p4, :cond_0

    const v0, 0x1020010

    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, p4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_0
    new-instance v0, Lcom/google/android/location/settings/s;

    invoke-direct {v0, v1, v2}, Lcom/google/android/location/settings/s;-><init>(Landroid/content/Context;Landroid/content/Intent;)V

    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 603
    invoke-static {p1, p2}, Lcom/google/android/gms/common/util/aw;->a(Landroid/view/View;Z)V

    .line 604
    return-object p1
.end method

.method final a(ILcom/google/android/location/reporting/config/AccountConfig;Z)Ljava/lang/String;
    .locals 5

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 641
    invoke-virtual {p2}, Lcom/google/android/location/reporting/config/AccountConfig;->n()Lcom/google/android/location/reporting/config/Conditions;

    move-result-object v0

    .line 642
    iget-object v3, p0, Lcom/google/android/location/settings/e;->e:Lcom/google/android/location/settings/GoogleLocationSettingsActivity;

    invoke-virtual {v3}, Lcom/google/android/location/settings/GoogleLocationSettingsActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    .line 644
    invoke-virtual {p2}, Lcom/google/android/location/reporting/config/AccountConfig;->u()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 645
    sget v0, Lcom/google/android/gms/p;->qF:I

    invoke-virtual {v3, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 670
    :goto_0
    return-object v0

    .line 646
    :cond_0
    invoke-virtual {v0}, Lcom/google/android/location/reporting/config/Conditions;->isIneligibleDueToGeoOnly()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 647
    iget-object v0, p0, Lcom/google/android/location/settings/e;->e:Lcom/google/android/location/settings/GoogleLocationSettingsActivity;

    invoke-virtual {v0}, Lcom/google/android/location/settings/GoogleLocationSettingsActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iget-object v3, p0, Lcom/google/android/location/settings/e;->e:Lcom/google/android/location/settings/GoogleLocationSettingsActivity;

    invoke-static {v3}, Lcom/google/android/location/settings/GoogleLocationSettingsActivity;->b(Lcom/google/android/location/settings/GoogleLocationSettingsActivity;)Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_1

    sget v3, Lcom/google/android/gms/p;->qH:I

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    new-array v1, v1, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/google/android/location/settings/e;->e:Lcom/google/android/location/settings/GoogleLocationSettingsActivity;

    invoke-static {v3}, Lcom/google/android/location/settings/GoogleLocationSettingsActivity;->b(Lcom/google/android/location/settings/GoogleLocationSettingsActivity;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_1
    sget v1, Lcom/google/android/gms/p;->qI:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Lcom/google/android/location/settings/f;

    invoke-direct {v1, p0}, Lcom/google/android/location/settings/f;-><init>(Lcom/google/android/location/settings/e;)V

    new-array v2, v2, [Ljava/lang/Void;

    invoke-virtual {v1, v2}, Lcom/google/android/location/settings/f;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0

    .line 648
    :cond_2
    invoke-virtual {v0}, Lcom/google/android/location/reporting/config/Conditions;->g()Z

    move-result v4

    if-nez v4, :cond_3

    .line 649
    sget v0, Lcom/google/android/gms/p;->qU:I

    invoke-virtual {v3, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 650
    :cond_3
    invoke-virtual {v0}, Lcom/google/android/location/reporting/config/Conditions;->f()Z

    move-result v0

    if-nez v0, :cond_4

    .line 651
    sget v0, Lcom/google/android/gms/p;->qT:I

    invoke-virtual {v3, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 652
    :cond_4
    invoke-virtual {p2}, Lcom/google/android/location/reporting/config/AccountConfig;->l()Z

    move-result v0

    if-nez v0, :cond_5

    .line 653
    sget v0, Lcom/google/android/gms/p;->qy:I

    invoke-virtual {v3, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 654
    :cond_5
    const/4 v0, -0x2

    if-ne p1, v0, :cond_6

    .line 657
    sget v0, Lcom/google/android/gms/p;->qx:I

    invoke-virtual {v3, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 658
    :cond_6
    iget-object v0, p0, Lcom/google/android/location/settings/e;->e:Lcom/google/android/location/settings/GoogleLocationSettingsActivity;

    invoke-static {v0}, Lcom/google/android/location/settings/GoogleLocationSettingsActivity;->a(Lcom/google/android/location/settings/GoogleLocationSettingsActivity;)I

    move-result v0

    if-ne v0, v1, :cond_7

    invoke-virtual {p2}, Lcom/google/android/location/reporting/config/AccountConfig;->i()Z

    move-result v0

    if-nez v0, :cond_7

    .line 664
    sget v0, Lcom/google/android/gms/p;->qB:I

    invoke-virtual {v3, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 666
    :cond_7
    if-lez p1, :cond_8

    move v0, v1

    .line 667
    :goto_1
    if-eqz p3, :cond_b

    iget-object v2, p0, Lcom/google/android/location/settings/e;->e:Lcom/google/android/location/settings/GoogleLocationSettingsActivity;

    invoke-static {v2}, Lcom/google/android/location/settings/GoogleLocationSettingsActivity;->a(Lcom/google/android/location/settings/GoogleLocationSettingsActivity;)I

    move-result v2

    if-ne v2, v1, :cond_a

    if-eqz v0, :cond_9

    sget v0, Lcom/google/android/gms/p;->qt:I

    .line 670
    :goto_2
    invoke-virtual {v3, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    :cond_8
    move v0, v2

    .line 666
    goto :goto_1

    .line 667
    :cond_9
    sget v0, Lcom/google/android/gms/p;->qs:I

    goto :goto_2

    :cond_a
    invoke-static {v2}, Lcom/google/android/location/reporting/service/p;->a(I)I

    move-result v0

    goto :goto_2

    :cond_b
    iget-object v2, p0, Lcom/google/android/location/settings/e;->e:Lcom/google/android/location/settings/GoogleLocationSettingsActivity;

    invoke-static {v2}, Lcom/google/android/location/settings/GoogleLocationSettingsActivity;->a(Lcom/google/android/location/settings/GoogleLocationSettingsActivity;)I

    move-result v2

    if-ne v2, v1, :cond_d

    if-eqz v0, :cond_c

    sget v0, Lcom/google/android/gms/p;->qE:I

    goto :goto_2

    :cond_c
    sget v0, Lcom/google/android/gms/p;->qC:I

    goto :goto_2

    :cond_d
    invoke-static {v2}, Lcom/google/android/location/reporting/service/p;->a(I)I

    move-result v0

    goto :goto_2
.end method
