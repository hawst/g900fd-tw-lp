.class public final Lcom/google/android/location/activity/ad;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/location/activity/bn;


# instance fields
.field final a:Lcom/google/android/location/os/bi;

.field b:Lcom/google/android/location/activity/bo;

.field private final c:Landroid/hardware/SensorManager;

.field private final d:Landroid/hardware/Sensor;

.field private final e:Z

.field private f:Lcom/google/android/location/activity/ae;


# direct methods
.method public constructor <init>(Landroid/hardware/SensorManager;Lcom/google/android/location/os/bi;)V
    .locals 3

    .prologue
    .line 52
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 53
    iput-object p1, p0, Lcom/google/android/location/activity/ad;->c:Landroid/hardware/SensorManager;

    .line 56
    const/16 v0, 0x16

    invoke-virtual {p1, v0}, Landroid/hardware/SensorManager;->getDefaultSensor(I)Landroid/hardware/Sensor;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/activity/ad;->d:Landroid/hardware/Sensor;

    .line 57
    iput-object p2, p0, Lcom/google/android/location/activity/ad;->a:Lcom/google/android/location/os/bi;

    .line 58
    sget-object v0, Lcom/google/android/location/d/a;->n:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/location/activity/ad;->d:Landroid/hardware/Sensor;

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/location/activity/ad;->e:Z

    .line 59
    sget-boolean v0, Lcom/google/android/location/i/a;->b:Z

    if-eqz v0, :cond_0

    const-string v0, "HwTilt"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "HardwareWakeUpTiltDetector.isSupportedDevice="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v2, p0, Lcom/google/android/location/activity/ad;->e:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/location/o/a/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 60
    :cond_0
    return-void

    .line 58
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a()Z
    .locals 2

    .prologue
    .line 84
    iget-object v0, p0, Lcom/google/android/location/activity/ad;->b:Lcom/google/android/location/activity/bo;

    if-nez v0, :cond_1

    .line 85
    sget-boolean v0, Lcom/google/android/location/i/a;->d:Z

    if-eqz v0, :cond_0

    const-string v0, "HwTilt"

    const-string v1, "disable: already disabled"

    invoke-static {v0, v1}, Lcom/google/android/location/o/a/a;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 86
    :cond_0
    const/4 v0, 0x0

    .line 91
    :goto_0
    return v0

    .line 88
    :cond_1
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/location/activity/ad;->b:Lcom/google/android/location/activity/bo;

    .line 89
    iget-object v0, p0, Lcom/google/android/location/activity/ad;->c:Landroid/hardware/SensorManager;

    iget-object v1, p0, Lcom/google/android/location/activity/ad;->f:Lcom/google/android/location/activity/ae;

    invoke-virtual {v0, v1}, Landroid/hardware/SensorManager;->unregisterListener(Landroid/hardware/SensorEventListener;)V

    .line 90
    sget-boolean v0, Lcom/google/android/location/i/a;->b:Z

    if-eqz v0, :cond_2

    const-string v0, "HwTilt"

    const-string v1, "disabled"

    invoke-static {v0, v1}, Lcom/google/android/location/o/a/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 91
    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final a(Lcom/google/android/location/activity/bo;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 64
    iget-boolean v1, p0, Lcom/google/android/location/activity/ad;->e:Z

    if-nez v1, :cond_0

    .line 65
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Tilt detector is not supported on this device"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 67
    :cond_0
    iget-object v1, p0, Lcom/google/android/location/activity/ad;->b:Lcom/google/android/location/activity/bo;

    if-eqz v1, :cond_1

    .line 68
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Tilt detector already enabled"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 70
    :cond_1
    iput-object p1, p0, Lcom/google/android/location/activity/ad;->b:Lcom/google/android/location/activity/bo;

    .line 72
    new-instance v1, Lcom/google/android/location/activity/ae;

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    invoke-direct {v1, p0, v2, v3}, Lcom/google/android/location/activity/ae;-><init>(Lcom/google/android/location/activity/ad;J)V

    iput-object v1, p0, Lcom/google/android/location/activity/ad;->f:Lcom/google/android/location/activity/ae;

    .line 73
    iget-object v1, p0, Lcom/google/android/location/activity/ad;->c:Landroid/hardware/SensorManager;

    iget-object v2, p0, Lcom/google/android/location/activity/ad;->f:Lcom/google/android/location/activity/ae;

    iget-object v3, p0, Lcom/google/android/location/activity/ad;->d:Landroid/hardware/Sensor;

    invoke-virtual {v1, v2, v3, v0}, Landroid/hardware/SensorManager;->registerListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;I)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 74
    sget-boolean v0, Lcom/google/android/location/i/a;->b:Z

    if-eqz v0, :cond_2

    const-string v0, "HwTilt"

    const-string v1, "enabled"

    invoke-static {v0, v1}, Lcom/google/android/location/o/a/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 75
    :cond_2
    const/4 v0, 0x1

    .line 78
    :cond_3
    :goto_0
    return v0

    .line 77
    :cond_4
    sget-boolean v1, Lcom/google/android/location/i/a;->d:Z

    if-eqz v1, :cond_3

    const-string v1, "HwTilt"

    const-string v2, "enable: registerListener failed"

    invoke-static {v1, v2}, Lcom/google/android/location/o/a/a;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 96
    iget-boolean v0, p0, Lcom/google/android/location/activity/ad;->e:Z

    return v0
.end method
