.class public final Lcom/google/android/location/n/n;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/api/x;
.implements Lcom/google/android/gms/common/api/y;


# instance fields
.field final a:Lcom/google/android/gms/common/api/v;

.field final b:Ljava/util/LinkedHashMap;

.field final c:Lcom/google/android/gms/location/d;

.field private d:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 55
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 48
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/location/n/n;->b:Ljava/util/LinkedHashMap;

    .line 53
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/location/n/n;->d:Z

    .line 56
    sget-object v0, Lcom/google/android/gms/location/p;->b:Lcom/google/android/gms/location/d;

    iput-object v0, p0, Lcom/google/android/location/n/n;->c:Lcom/google/android/gms/location/d;

    .line 57
    new-instance v0, Lcom/google/android/gms/common/api/w;

    invoke-direct {v0, p1}, Lcom/google/android/gms/common/api/w;-><init>(Landroid/content/Context;)V

    sget-object v1, Lcom/google/android/gms/location/p;->a:Lcom/google/android/gms/common/api/c;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/api/w;->a(Lcom/google/android/gms/common/api/c;)Lcom/google/android/gms/common/api/w;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/android/gms/common/api/w;->a(Lcom/google/android/gms/common/api/y;)Lcom/google/android/gms/common/api/w;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/android/gms/common/api/w;->a(Lcom/google/android/gms/common/api/x;)Lcom/google/android/gms/common/api/w;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/common/api/w;->a()Lcom/google/android/gms/common/api/v;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/n/n;->a:Lcom/google/android/gms/common/api/v;

    .line 62
    return-void
.end method

.method private c()V
    .locals 7

    .prologue
    .line 237
    iget-object v1, p0, Lcom/google/android/location/n/n;->b:Ljava/util/LinkedHashMap;

    monitor-enter v1

    .line 238
    :try_start_0
    new-instance v0, Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/google/android/location/n/n;->b:Ljava/util/LinkedHashMap;

    invoke-virtual {v2}, Ljava/util/LinkedHashMap;->entrySet()Ljava/util/Set;

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 239
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 240
    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 241
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    .line 242
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/n/t;

    .line 243
    :try_start_1
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J
    :try_end_1
    .catch Ljava/lang/IllegalStateException; {:try_start_1 .. :try_end_1} :catch_0

    move-result-wide v4

    :try_start_2
    invoke-virtual {v0}, Lcom/google/android/location/n/t;->a()Lcom/google/android/gms/common/api/am;

    move-result-object v3

    new-instance v6, Lcom/google/android/location/n/s;

    invoke-direct {v6, p0, v0, v2}, Lcom/google/android/location/n/s;-><init>(Lcom/google/android/location/n/n;Lcom/google/android/location/n/t;Ljava/lang/Object;)V

    invoke-interface {v3, v6}, Lcom/google/android/gms/common/api/am;->a(Lcom/google/android/gms/common/api/aq;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    :try_start_3
    invoke-static {v4, v5}, Landroid/os/Binder;->restoreCallingIdentity(J)V
    :try_end_3
    .catch Ljava/lang/IllegalStateException; {:try_start_3 .. :try_end_3} :catch_0

    goto :goto_0

    .line 244
    :catch_0
    move-exception v0

    goto :goto_0

    .line 239
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 243
    :catchall_1
    move-exception v0

    :try_start_4
    invoke-static {v4, v5}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    throw v0
    :try_end_4
    .catch Ljava/lang/IllegalStateException; {:try_start_4 .. :try_end_4} :catch_0

    .line 245
    :cond_0
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 77
    iget-object v1, p0, Lcom/google/android/location/n/n;->a:Lcom/google/android/gms/common/api/v;

    monitor-enter v1

    .line 78
    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Lcom/google/android/location/n/n;->d:Z

    .line 79
    iget-object v0, p0, Lcom/google/android/location/n/n;->a:Lcom/google/android/gms/common/api/v;

    invoke-interface {v0}, Lcom/google/android/gms/common/api/v;->b()V

    .line 80
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final a(Lcom/google/android/gms/common/c;)V
    .locals 2

    .prologue
    .line 178
    const-string v0, "FlpInternalHelper"

    const-string v1, "Failed to connect to FLP from within location process"

    invoke-static {v0, v1}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)I

    .line 179
    return-void
.end method

.method public final a(Lcom/google/android/gms/location/internal/LocationRequestInternal;Lcom/google/android/gms/location/n;Landroid/os/Looper;)V
    .locals 1

    .prologue
    .line 111
    new-instance v0, Lcom/google/android/location/n/o;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/google/android/location/n/o;-><init>(Lcom/google/android/location/n/n;Lcom/google/android/gms/location/internal/LocationRequestInternal;Lcom/google/android/gms/location/n;Landroid/os/Looper;)V

    invoke-virtual {p0, p2, v0}, Lcom/google/android/location/n/n;->a(Ljava/lang/Object;Lcom/google/android/location/n/t;)V

    .line 118
    return-void
.end method

.method public final a(Lcom/google/android/gms/location/n;)V
    .locals 1

    .prologue
    .line 143
    new-instance v0, Lcom/google/android/location/n/q;

    invoke-direct {v0, p0, p1}, Lcom/google/android/location/n/q;-><init>(Lcom/google/android/location/n/n;Lcom/google/android/gms/location/n;)V

    invoke-virtual {p0, p1, v0}, Lcom/google/android/location/n/n;->a(Ljava/lang/Object;Lcom/google/android/location/n/t;)V

    .line 150
    return-void
.end method

.method public final a(Ljava/lang/Object;Lcom/google/android/location/n/t;)V
    .locals 2

    .prologue
    .line 183
    iget-object v1, p0, Lcom/google/android/location/n/n;->b:Ljava/util/LinkedHashMap;

    monitor-enter v1

    .line 184
    :try_start_0
    iget-object v0, p0, Lcom/google/android/location/n/n;->b:Ljava/util/LinkedHashMap;

    invoke-virtual {v0, p1, p2}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 185
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 187
    iget-object v0, p0, Lcom/google/android/location/n/n;->a:Lcom/google/android/gms/common/api/v;

    invoke-interface {v0}, Lcom/google/android/gms/common/api/v;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 188
    invoke-direct {p0}, Lcom/google/android/location/n/n;->c()V

    .line 194
    :goto_0
    return-void

    .line 185
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 190
    :cond_0
    iget-object v1, p0, Lcom/google/android/location/n/n;->a:Lcom/google/android/gms/common/api/v;

    monitor-enter v1

    .line 191
    :try_start_1
    iget-boolean v0, p0, Lcom/google/android/location/n/n;->d:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/location/n/n;->a:Lcom/google/android/gms/common/api/v;

    invoke-interface {v0}, Lcom/google/android/gms/common/api/v;->g()Z

    move-result v0

    if-nez v0, :cond_1

    .line 192
    iget-object v0, p0, Lcom/google/android/location/n/n;->a:Lcom/google/android/gms/common/api/v;

    invoke-interface {v0}, Lcom/google/android/gms/common/api/v;->b()V

    .line 194
    :cond_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    goto :goto_0

    :catchall_1
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 87
    iget-object v1, p0, Lcom/google/android/location/n/n;->a:Lcom/google/android/gms/common/api/v;

    monitor-enter v1

    .line 88
    const/4 v0, 0x0

    :try_start_0
    iput-boolean v0, p0, Lcom/google/android/location/n/n;->d:Z

    .line 89
    iget-object v0, p0, Lcom/google/android/location/n/n;->a:Lcom/google/android/gms/common/api/v;

    invoke-interface {v0}, Lcom/google/android/gms/common/api/v;->d()V

    .line 90
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final b_(Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 168
    invoke-direct {p0}, Lcom/google/android/location/n/n;->c()V

    .line 169
    return-void
.end method

.method public final f_(I)V
    .locals 2

    .prologue
    .line 173
    const-string v0, "FlpInternalHelper"

    const-string v1, "Connection suspended within location process for FLP connection"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 174
    return-void
.end method
