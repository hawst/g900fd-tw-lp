.class public final Lcom/google/android/location/copresence/q/g;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final a:Lcom/google/android/location/copresence/q/m;


# instance fields
.field private final b:Lcom/google/android/gms/common/util/p;

.field private final c:Landroid/os/Handler;

.field private final d:Ljava/util/HashMap;

.field private final e:Ljava/util/ArrayList;

.field private final f:Ljava/util/HashSet;

.field private final g:Ljava/lang/Runnable;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 120
    new-instance v0, Lcom/google/android/location/copresence/q/h;

    invoke-direct {v0}, Lcom/google/android/location/copresence/q/h;-><init>()V

    sput-object v0, Lcom/google/android/location/copresence/q/g;->a:Lcom/google/android/location/copresence/q/m;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 162
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    invoke-direct {p0, v0}, Lcom/google/android/location/copresence/q/g;-><init>(Landroid/os/Handler;)V

    .line 163
    return-void
.end method

.method public constructor <init>(Landroid/os/Handler;)V
    .locals 1

    .prologue
    .line 166
    invoke-static {}, Lcom/google/android/gms/common/util/r;->c()Lcom/google/android/gms/common/util/p;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lcom/google/android/location/copresence/q/g;-><init>(Landroid/os/Handler;Lcom/google/android/gms/common/util/p;)V

    .line 167
    return-void
.end method

.method private constructor <init>(Landroid/os/Handler;Lcom/google/android/gms/common/util/p;)V
    .locals 1

    .prologue
    .line 170
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 141
    new-instance v0, Lcom/google/android/location/copresence/q/i;

    invoke-direct {v0, p0}, Lcom/google/android/location/copresence/q/i;-><init>(Lcom/google/android/location/copresence/q/g;)V

    iput-object v0, p0, Lcom/google/android/location/copresence/q/g;->g:Ljava/lang/Runnable;

    .line 171
    iput-object p2, p0, Lcom/google/android/location/copresence/q/g;->b:Lcom/google/android/gms/common/util/p;

    .line 172
    iput-object p1, p0, Lcom/google/android/location/copresence/q/g;->c:Landroid/os/Handler;

    .line 173
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/location/copresence/q/g;->d:Ljava/util/HashMap;

    .line 174
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/location/copresence/q/g;->e:Ljava/util/ArrayList;

    .line 175
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/location/copresence/q/g;->f:Ljava/util/HashSet;

    .line 176
    return-void
.end method

.method static synthetic a()Lcom/google/android/location/copresence/q/m;
    .locals 1

    .prologue
    .line 34
    sget-object v0, Lcom/google/android/location/copresence/q/g;->a:Lcom/google/android/location/copresence/q/m;

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/location/copresence/q/g;Lcom/google/android/location/copresence/q/n;)Lcom/google/android/location/copresence/q/o;
    .locals 1

    .prologue
    .line 34
    invoke-direct {p0, p1}, Lcom/google/android/location/copresence/q/g;->b(Lcom/google/android/location/copresence/q/n;)Lcom/google/android/location/copresence/q/o;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/location/copresence/q/g;)Ljava/util/HashSet;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lcom/google/android/location/copresence/q/g;->f:Ljava/util/HashSet;

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/location/copresence/q/g;Lcom/google/android/location/copresence/q/o;J)V
    .locals 0

    .prologue
    .line 34
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/location/copresence/q/g;->a(Lcom/google/android/location/copresence/q/o;J)V

    return-void
.end method

.method private a(Lcom/google/android/location/copresence/q/o;J)V
    .locals 2

    .prologue
    .line 244
    iget-object v0, p0, Lcom/google/android/location/copresence/q/g;->c:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/location/copresence/q/k;

    invoke-direct {v1, p0, p1}, Lcom/google/android/location/copresence/q/k;-><init>(Lcom/google/android/location/copresence/q/g;Lcom/google/android/location/copresence/q/o;)V

    invoke-virtual {v0, v1, p2, p3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 245
    return-void
.end method

.method private b(Lcom/google/android/location/copresence/q/n;)Lcom/google/android/location/copresence/q/o;
    .locals 2

    .prologue
    .line 179
    iget-object v1, p0, Lcom/google/android/location/copresence/q/g;->d:Ljava/util/HashMap;

    monitor-enter v1

    .line 180
    :try_start_0
    iget-object v0, p0, Lcom/google/android/location/copresence/q/g;->d:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/copresence/q/o;

    .line 181
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v0

    .line 182
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method static synthetic b(Lcom/google/android/location/copresence/q/g;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lcom/google/android/location/copresence/q/g;->e:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic b(Lcom/google/android/location/copresence/q/g;Lcom/google/android/location/copresence/q/o;J)V
    .locals 2

    .prologue
    .line 34
    iget-object v0, p0, Lcom/google/android/location/copresence/q/g;->c:Landroid/os/Handler;

    invoke-virtual {v0, p1, p2, p3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    return-void
.end method

.method static synthetic c(Lcom/google/android/location/copresence/q/g;)Ljava/lang/Runnable;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lcom/google/android/location/copresence/q/g;->g:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic d(Lcom/google/android/location/copresence/q/g;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lcom/google/android/location/copresence/q/g;->c:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic e(Lcom/google/android/location/copresence/q/g;)Lcom/google/android/gms/common/util/p;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lcom/google/android/location/copresence/q/g;->b:Lcom/google/android/gms/common/util/p;

    return-object v0
.end method


# virtual methods
.method public final a(Lcom/google/android/location/copresence/q/n;)V
    .locals 2

    .prologue
    .line 218
    iget-object v1, p0, Lcom/google/android/location/copresence/q/g;->d:Ljava/util/HashMap;

    monitor-enter v1

    .line 219
    :try_start_0
    iget-object v0, p0, Lcom/google/android/location/copresence/q/g;->d:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/copresence/q/o;

    .line 220
    if-eqz v0, :cond_0

    .line 221
    invoke-virtual {v0}, Lcom/google/android/location/copresence/q/o;->c()V

    .line 223
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final varargs a(Lcom/google/android/location/copresence/q/n;Lcom/google/android/location/copresence/q/j;[Lcom/google/android/location/copresence/q/n;)V
    .locals 4

    .prologue
    .line 205
    iget-object v1, p0, Lcom/google/android/location/copresence/q/g;->d:Ljava/util/HashMap;

    monitor-enter v1

    .line 207
    :try_start_0
    invoke-virtual {p0, p1}, Lcom/google/android/location/copresence/q/g;->a(Lcom/google/android/location/copresence/q/n;)V

    .line 209
    new-instance v0, Lcom/google/android/location/copresence/q/o;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/google/android/location/copresence/q/o;-><init>(Lcom/google/android/location/copresence/q/g;Lcom/google/android/location/copresence/q/n;Lcom/google/android/location/copresence/q/j;[Lcom/google/android/location/copresence/q/n;)V

    .line 210
    iget-object v2, p0, Lcom/google/android/location/copresence/q/g;->d:Ljava/util/HashMap;

    invoke-virtual {v2, p1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 212
    const-wide/16 v2, 0x0

    invoke-direct {p0, v0, v2, v3}, Lcom/google/android/location/copresence/q/g;->a(Lcom/google/android/location/copresence/q/o;J)V

    .line 213
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method
