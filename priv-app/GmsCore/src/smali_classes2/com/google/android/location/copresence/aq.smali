.class final Lcom/google/android/location/copresence/aq;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lcom/google/android/location/copresence/ap;


# direct methods
.method constructor <init>(Lcom/google/android/location/copresence/ap;)V
    .locals 0

    .prologue
    .line 129
    iput-object p1, p0, Lcom/google/android/location/copresence/aq;->a:Lcom/google/android/location/copresence/ap;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 8

    .prologue
    const/4 v7, 0x0

    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 132
    const/4 v2, 0x3

    invoke-static {v2}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 133
    const-string v2, "Initializing EventLoop."

    invoke-static {v2}, Lcom/google/android/location/copresence/ag;->b(Ljava/lang/String;)I

    .line 135
    :cond_0
    iget-object v2, p0, Lcom/google/android/location/copresence/aq;->a:Lcom/google/android/location/copresence/ap;

    iget-object v3, p0, Lcom/google/android/location/copresence/aq;->a:Lcom/google/android/location/copresence/ap;

    invoke-static {v3}, Lcom/google/android/location/copresence/ap;->a(Lcom/google/android/location/copresence/ap;)Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/location/copresence/c;->a(Landroid/content/Context;)Lcom/google/android/location/copresence/c;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/location/copresence/ap;->a(Lcom/google/android/location/copresence/ap;Lcom/google/android/location/copresence/c;)Lcom/google/android/location/copresence/c;

    .line 136
    iget-object v2, p0, Lcom/google/android/location/copresence/aq;->a:Lcom/google/android/location/copresence/ap;

    iget-object v3, p0, Lcom/google/android/location/copresence/aq;->a:Lcom/google/android/location/copresence/ap;

    invoke-static {v3}, Lcom/google/android/location/copresence/ap;->a(Lcom/google/android/location/copresence/ap;)Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/location/copresence/j;->a(Landroid/content/Context;)Lcom/google/android/location/copresence/j;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/location/copresence/ap;->a(Lcom/google/android/location/copresence/ap;Lcom/google/android/location/copresence/j;)Lcom/google/android/location/copresence/j;

    .line 137
    iget-object v2, p0, Lcom/google/android/location/copresence/aq;->a:Lcom/google/android/location/copresence/ap;

    invoke-static {v2}, Lcom/google/android/location/copresence/ap;->b(Lcom/google/android/location/copresence/ap;)Ljava/util/concurrent/CountDownLatch;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    .line 139
    iget-object v2, p0, Lcom/google/android/location/copresence/aq;->a:Lcom/google/android/location/copresence/ap;

    invoke-static {v2}, Lcom/google/android/location/copresence/ap;->c(Lcom/google/android/location/copresence/ap;)Lcom/google/android/location/copresence/j;

    move-result-object v3

    iget-object v2, v3, Lcom/google/android/location/copresence/j;->b:Lcom/google/android/location/copresence/ap;

    invoke-virtual {v2}, Lcom/google/android/location/copresence/ap;->c()V

    iget-boolean v2, v3, Lcom/google/android/location/copresence/j;->n:Z

    if-nez v2, :cond_5

    iget-object v2, v3, Lcom/google/android/location/copresence/j;->a:Landroid/content/Context;

    iget-object v2, v3, Lcom/google/android/location/copresence/j;->a:Landroid/content/Context;

    invoke-static {v2}, Lcom/google/android/location/copresence/l;->b(Landroid/content/Context;)V

    iget-object v2, v3, Lcom/google/android/location/copresence/j;->a:Landroid/content/Context;

    invoke-static {v2}, Lcom/google/android/location/copresence/a/b;->a(Landroid/content/Context;)Lcom/google/android/location/copresence/a/b;

    move-result-object v2

    iput-object v2, v3, Lcom/google/android/location/copresence/j;->e:Lcom/google/android/location/copresence/a/b;

    iget-object v2, v3, Lcom/google/android/location/copresence/j;->a:Landroid/content/Context;

    invoke-static {v2}, Lcom/google/android/location/copresence/n/f;->a(Landroid/content/Context;)Lcom/google/android/location/copresence/n/f;

    move-result-object v2

    iput-object v2, v3, Lcom/google/android/location/copresence/j;->f:Lcom/google/android/location/copresence/n/f;

    iget-object v2, v3, Lcom/google/android/location/copresence/j;->a:Landroid/content/Context;

    invoke-static {v2}, Lcom/google/android/location/copresence/a;->a(Landroid/content/Context;)Lcom/google/android/location/copresence/a;

    move-result-object v2

    iput-object v2, v3, Lcom/google/android/location/copresence/j;->c:Lcom/google/android/location/copresence/a;

    iget-object v2, v3, Lcom/google/android/location/copresence/j;->a:Landroid/content/Context;

    invoke-static {v2}, Lcom/google/android/location/copresence/ah;->a(Landroid/content/Context;)Lcom/google/android/location/copresence/ah;

    move-result-object v2

    iput-object v2, v3, Lcom/google/android/location/copresence/j;->g:Lcom/google/android/location/copresence/ah;

    iget-object v2, v3, Lcom/google/android/location/copresence/j;->a:Landroid/content/Context;

    invoke-static {v2}, Lcom/google/android/location/copresence/l/h;->a(Landroid/content/Context;)Lcom/google/android/location/copresence/l/h;

    move-result-object v2

    iput-object v2, v3, Lcom/google/android/location/copresence/j;->j:Lcom/google/android/location/copresence/l/h;

    iget-object v2, v3, Lcom/google/android/location/copresence/j;->a:Landroid/content/Context;

    invoke-static {v2}, Lcom/google/android/location/copresence/at;->a(Landroid/content/Context;)Lcom/google/android/location/copresence/at;

    move-result-object v2

    iput-object v2, v3, Lcom/google/android/location/copresence/j;->k:Lcom/google/android/location/copresence/at;

    iget-object v2, v3, Lcom/google/android/location/copresence/j;->a:Landroid/content/Context;

    invoke-static {v2}, Lcom/google/android/location/copresence/an;->a(Landroid/content/Context;)Lcom/google/android/location/copresence/an;

    move-result-object v2

    iput-object v2, v3, Lcom/google/android/location/copresence/j;->h:Lcom/google/android/location/copresence/an;

    iget-object v2, v3, Lcom/google/android/location/copresence/j;->a:Landroid/content/Context;

    invoke-static {v2}, Lcom/google/android/location/copresence/bg;->a(Landroid/content/Context;)Lcom/google/android/location/copresence/bg;

    move-result-object v2

    iput-object v2, v3, Lcom/google/android/location/copresence/j;->m:Lcom/google/android/location/copresence/bg;

    iget-object v2, v3, Lcom/google/android/location/copresence/j;->a:Landroid/content/Context;

    invoke-static {v2}, Lcom/google/android/location/copresence/l/j;->a(Landroid/content/Context;)Lcom/google/android/location/copresence/l/j;

    move-result-object v2

    iput-object v2, v3, Lcom/google/android/location/copresence/j;->i:Lcom/google/android/location/copresence/l/j;

    iget-object v2, v3, Lcom/google/android/location/copresence/j;->a:Landroid/content/Context;

    invoke-static {v2}, Lcom/google/android/location/copresence/ay;->a(Landroid/content/Context;)Lcom/google/android/location/copresence/ay;

    move-result-object v2

    iput-object v2, v3, Lcom/google/android/location/copresence/j;->l:Lcom/google/android/location/copresence/ay;

    iget-object v2, v3, Lcom/google/android/location/copresence/j;->a:Landroid/content/Context;

    invoke-static {v2}, Lcom/google/android/location/copresence/f;->a(Landroid/content/Context;)Lcom/google/android/location/copresence/f;

    move-result-object v2

    iput-object v2, v3, Lcom/google/android/location/copresence/j;->d:Lcom/google/android/location/copresence/f;

    new-instance v2, Lcom/google/android/location/copresence/h;

    invoke-direct {v2, v3}, Lcom/google/android/location/copresence/h;-><init>(Lcom/google/android/location/copresence/j;)V

    iput-object v2, v3, Lcom/google/android/location/copresence/j;->p:Lcom/google/android/location/copresence/h;

    iget-object v2, v3, Lcom/google/android/location/copresence/j;->p:Lcom/google/android/location/copresence/h;

    iget-object v4, v3, Lcom/google/android/location/copresence/j;->i:Lcom/google/android/location/copresence/l/j;

    invoke-virtual {v2, v4}, Lcom/google/android/location/copresence/h;->a(Lcom/google/android/location/copresence/i;)V

    iget-object v2, v3, Lcom/google/android/location/copresence/j;->p:Lcom/google/android/location/copresence/h;

    iget-object v4, v3, Lcom/google/android/location/copresence/j;->g:Lcom/google/android/location/copresence/ah;

    invoke-virtual {v2, v4}, Lcom/google/android/location/copresence/h;->a(Lcom/google/android/location/copresence/i;)V

    iget-object v2, v3, Lcom/google/android/location/copresence/j;->a:Landroid/content/Context;

    iget-object v4, v3, Lcom/google/android/location/copresence/j;->p:Lcom/google/android/location/copresence/h;

    invoke-static {}, Lcom/google/android/location/copresence/h;->a()Landroid/content/IntentFilter;

    move-result-object v5

    iget-object v6, v3, Lcom/google/android/location/copresence/j;->b:Lcom/google/android/location/copresence/ap;

    invoke-virtual {v6}, Lcom/google/android/location/copresence/ap;->d()Landroid/os/Handler;

    move-result-object v6

    invoke-virtual {v2, v4, v5, v7, v6}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;Ljava/lang/String;Landroid/os/Handler;)Landroid/content/Intent;

    iput-object v7, v3, Lcom/google/android/location/copresence/j;->q:Lcom/google/android/location/copresence/debug/c;

    iget-object v2, v3, Lcom/google/android/location/copresence/j;->q:Lcom/google/android/location/copresence/debug/c;

    if-eqz v2, :cond_1

    iget-object v2, v3, Lcom/google/android/location/copresence/j;->a:Landroid/content/Context;

    iget-object v4, v3, Lcom/google/android/location/copresence/j;->q:Lcom/google/android/location/copresence/debug/c;

    iget-object v5, v3, Lcom/google/android/location/copresence/j;->b:Lcom/google/android/location/copresence/ap;

    invoke-virtual {v5}, Lcom/google/android/location/copresence/ap;->d()Landroid/os/Handler;

    move-result-object v5

    invoke-virtual {v2, v4, v7, v7, v5}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;Ljava/lang/String;Landroid/os/Handler;)Landroid/content/Intent;

    :cond_1
    iget-object v2, v3, Lcom/google/android/location/copresence/j;->a:Landroid/content/Context;

    new-instance v4, Lcom/google/android/location/copresence/v;

    invoke-direct {v4, v2}, Lcom/google/android/location/copresence/v;-><init>(Landroid/content/Context;)V

    invoke-virtual {v4}, Lcom/google/android/location/copresence/v;->a()V

    invoke-virtual {v4}, Lcom/google/android/location/copresence/v;->b()Lcom/google/android/location/e/a;

    move-result-object v2

    iput-object v2, v3, Lcom/google/android/location/copresence/j;->o:Lcom/google/android/location/e/a;

    iget-object v2, v3, Lcom/google/android/location/copresence/j;->a:Landroid/content/Context;

    invoke-static {v2}, Lcom/google/android/location/copresence/i/c;->a(Landroid/content/Context;)Lcom/google/android/location/copresence/i/c;

    move-result-object v2

    iget-object v4, v2, Lcom/google/android/location/copresence/i/c;->a:Ljava/lang/Object;

    monitor-enter v4

    const/4 v5, 0x0

    :try_start_0
    iput-boolean v5, v2, Lcom/google/android/location/copresence/i/c;->b:Z

    const/4 v5, 0x0

    iput-boolean v5, v2, Lcom/google/android/location/copresence/i/c;->c:Z

    const/4 v5, 0x1

    iput-boolean v5, v2, Lcom/google/android/location/copresence/i/c;->d:Z

    const/4 v5, 0x1

    invoke-virtual {v2, v5}, Lcom/google/android/location/copresence/i/c;->a(Z)V

    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v2, v3, Lcom/google/android/location/copresence/j;->e:Lcom/google/android/location/copresence/a/b;

    invoke-virtual {v2, v3}, Lcom/google/android/location/copresence/a/b;->a(Lcom/google/android/location/copresence/a/g;)V

    iget-object v2, v3, Lcom/google/android/location/copresence/j;->e:Lcom/google/android/location/copresence/a/b;

    iget-object v4, v3, Lcom/google/android/location/copresence/j;->o:Lcom/google/android/location/e/a;

    invoke-virtual {v2, v4}, Lcom/google/android/location/copresence/a/b;->a(Lcom/google/android/location/e/a;)V

    iget-object v2, v3, Lcom/google/android/location/copresence/j;->f:Lcom/google/android/location/copresence/n/f;

    invoke-virtual {v2, v3}, Lcom/google/android/location/copresence/n/f;->a(Lcom/google/android/location/copresence/n/i;)V

    iget-object v2, v3, Lcom/google/android/location/copresence/j;->f:Lcom/google/android/location/copresence/n/f;

    iget-object v4, v3, Lcom/google/android/location/copresence/j;->o:Lcom/google/android/location/e/a;

    iget-object v5, v3, Lcom/google/android/location/copresence/j;->b:Lcom/google/android/location/copresence/ap;

    invoke-virtual {v2, v4, v5}, Lcom/google/android/location/copresence/n/f;->a(Lcom/google/android/location/e/a;Lcom/google/android/location/copresence/ap;)V

    iget-object v4, v3, Lcom/google/android/location/copresence/j;->c:Lcom/google/android/location/copresence/a;

    const-string v2, "init"

    iget v5, v4, Lcom/google/android/location/copresence/a;->f:I

    if-nez v5, :cond_6

    move v2, v1

    :goto_0
    if-eqz v2, :cond_4

    iget-object v2, v4, Lcom/google/android/location/copresence/a;->c:Lcom/google/android/location/copresence/c/a;

    iget-object v2, v2, Lcom/google/android/location/copresence/c/a;->b:Lcom/google/android/location/copresence/z;

    invoke-interface {v2}, Lcom/google/android/location/copresence/z;->a()Z

    move-result v2

    if-nez v2, :cond_2

    iget-object v2, v4, Lcom/google/android/location/copresence/a;->c:Lcom/google/android/location/copresence/c/a;

    iget-object v2, v2, Lcom/google/android/location/copresence/c/a;->d:Lcom/google/android/location/copresence/z;

    invoke-interface {v2}, Lcom/google/android/location/copresence/z;->a()Z

    move-result v2

    if-nez v2, :cond_2

    iget-object v2, v4, Lcom/google/android/location/copresence/a;->d:Lcom/google/android/location/copresence/d/j;

    invoke-virtual {v2}, Lcom/google/android/location/copresence/d/j;->m()Lcom/google/android/location/copresence/z;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/location/copresence/z;->a()Z

    move-result v2

    if-nez v2, :cond_2

    iget-object v2, v4, Lcom/google/android/location/copresence/a;->d:Lcom/google/android/location/copresence/d/j;

    invoke-virtual {v2}, Lcom/google/android/location/copresence/d/j;->k()Lcom/google/android/location/copresence/z;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/location/copresence/z;->a()Z

    move-result v2

    if-nez v2, :cond_2

    iget-object v2, v4, Lcom/google/android/location/copresence/a;->e:Lcom/google/android/location/copresence/r/ag;

    iget-object v2, v2, Lcom/google/android/location/copresence/r/ag;->c:Lcom/google/android/location/copresence/z;

    invoke-interface {v2}, Lcom/google/android/location/copresence/z;->a()Z

    move-result v2

    if-eqz v2, :cond_3

    :cond_2
    move v0, v1

    :cond_3
    if-eqz v0, :cond_4

    iget-object v0, v4, Lcom/google/android/location/copresence/a;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/location/copresence/bi;->a(Landroid/content/Context;)Lcom/google/android/location/copresence/bi;

    move-result-object v0

    iput-object v0, v4, Lcom/google/android/location/copresence/a;->b:Lcom/google/android/location/copresence/bi;

    iput v1, v4, Lcom/google/android/location/copresence/a;->f:I

    :cond_4
    iget-object v0, v3, Lcom/google/android/location/copresence/j;->l:Lcom/google/android/location/copresence/ay;

    invoke-virtual {v0}, Lcom/google/android/location/copresence/ay;->a()V

    iget-object v0, v3, Lcom/google/android/location/copresence/j;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/location/copresence/debug/CopresDatabaseHelper;->a(Landroid/content/Context;)V

    iput-boolean v1, v3, Lcom/google/android/location/copresence/j;->n:Z

    .line 140
    :cond_5
    return-void

    .line 139
    :catchall_0
    move-exception v0

    monitor-exit v4

    throw v0

    :cond_6
    const/4 v5, 0x2

    invoke-static {v5}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v5

    if-eqz v5, :cond_7

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "ActiveActiveController: Active-active WRONG STATE: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v5, " expected state: "

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {v0}, Lcom/google/android/location/copresence/a;->a(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v5, " current state: "

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v5, v4, Lcom/google/android/location/copresence/a;->f:I

    invoke-static {v5}, Lcom/google/android/location/copresence/a;->a(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/location/copresence/ag;->a(Ljava/lang/String;)I

    :cond_7
    move v2, v0

    goto/16 :goto_0
.end method
