.class public Lcom/google/android/location/reporting/service/GcmBroadcastReceiver;
.super Landroid/support/v4/a/z;
.source "SourceFile"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0}, Landroid/support/v4/a/z;-><init>()V

    return-void
.end method

.method private static a(Landroid/content/Intent;Lcom/google/android/location/reporting/a/h;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 96
    const-string v1, "ulr_notification"

    invoke-virtual {p0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 97
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 98
    :cond_0
    const-string v1, "GCoreUlr"

    const-string v2, "Received GCM intent with empty extra."

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 118
    :goto_0
    return v0

    .line 103
    :cond_1
    const/4 v2, 0x0

    :try_start_0
    invoke-static {v1, v2}, Landroid/util/Base64;->decode(Ljava/lang/String;I)[B
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 109
    :try_start_1
    invoke-static {p1, v1}, Lcom/google/protobuf/nano/j;->mergeFrom(Lcom/google/protobuf/nano/j;[B)Lcom/google/protobuf/nano/j;
    :try_end_1
    .catch Lcom/google/protobuf/nano/i; {:try_start_1 .. :try_end_1} :catch_1

    .line 114
    iget-object v1, p1, Lcom/google/android/location/reporting/a/h;->a:Ljava/lang/String;

    if-eqz v1, :cond_2

    iget-object v1, p1, Lcom/google/android/location/reporting/a/h;->a:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 115
    :cond_2
    const-string v1, "GCoreUlr"

    const-string v2, "Received notification missing account name"

    invoke-static {v1, v2}, Lcom/google/android/location/reporting/b/d;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 104
    :catch_0
    move-exception v1

    .line 105
    const-string v2, "GCoreUlr"

    const-string v3, "Error decoding notification"

    invoke-static {v2, v3, v1}, Lcom/google/android/location/reporting/b/d;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 110
    :catch_1
    move-exception v1

    .line 111
    const-string v2, "GCoreUlr"

    const-string v3, "Error parsing notification"

    invoke-static {v2, v3, v1}, Lcom/google/android/location/reporting/b/d;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 118
    :cond_3
    const/4 v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 5

    .prologue
    const/4 v4, 0x4

    .line 36
    invoke-static {p1}, Lcom/google/android/location/reporting/b/n;->a(Landroid/content/Context;)V

    .line 38
    const-string v0, "GCoreUlr"

    invoke-static {v0, v4}, Lcom/google/android/location/reporting/b/d;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 39
    const-string v0, "GCoreUlr"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "GCM message received "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/location/reporting/b/d;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 41
    :cond_0
    sget-object v0, Lcom/google/android/location/reporting/service/ab;->V:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_2

    .line 70
    :cond_1
    :goto_0
    return-void

    .line 44
    :cond_2
    invoke-static {p1}, Lcom/google/android/gms/gcm/ag;->a(Landroid/content/Context;)Lcom/google/android/gms/gcm/ag;

    invoke-static {p2}, Lcom/google/android/gms/gcm/ag;->a(Landroid/content/Intent;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "send_error"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    const-string v0, "GCoreUlr"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "GCM send error: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/location/reporting/b/d;->e(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x0

    :goto_1
    if-eqz v0, :cond_1

    .line 47
    const-string v0, "ulr_notification"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 50
    new-instance v0, Lcom/google/android/location/reporting/a/h;

    invoke-direct {v0}, Lcom/google/android/location/reporting/a/h;-><init>()V

    .line 51
    invoke-static {p2, v0}, Lcom/google/android/location/reporting/service/GcmBroadcastReceiver;->a(Landroid/content/Intent;Lcom/google/android/location/reporting/a/h;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 54
    new-instance v1, Landroid/accounts/Account;

    iget-object v2, v0, Lcom/google/android/location/reporting/a/h;->a:Ljava/lang/String;

    const-string v3, "com.google"

    invoke-direct {v1, v2, v3}, Landroid/accounts/Account;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 55
    const-string v2, "GCoreUlr"

    invoke-static {v2, v4}, Lcom/google/android/location/reporting/b/d;->a(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 56
    const-string v2, "GCoreUlr"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "GCM settings changed notification for "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v1}, Lcom/google/android/gms/location/reporting/a/d;->a(Landroid/accounts/Account;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " timestamp:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v0, v0, Lcom/google/android/location/reporting/a/h;->b:Ljava/lang/Long;

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/google/android/location/reporting/b/d;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 59
    :cond_3
    invoke-static {}, Lcom/google/android/location/reporting/b/l;->l()V

    .line 61
    new-instance v0, Lcom/google/android/location/n/b;

    invoke-direct {v0, p1}, Lcom/google/android/location/n/b;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1}, Lcom/google/android/location/n/a;->b(Landroid/accounts/Account;)Z

    move-result v0

    if-nez v0, :cond_8

    .line 62
    const-string v0, "GCoreUlr"

    const/4 v1, 0x5

    invoke-static {v0, v1}, Lcom/google/android/location/reporting/b/d;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 63
    const-string v0, "GCoreUlr"

    const-string v1, "Received GCM message for invalid account"

    invoke-static {v0, v1}, Lcom/google/android/location/reporting/b/d;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 65
    :cond_4
    invoke-static {}, Lcom/google/android/location/reporting/b/l;->m()V

    goto/16 :goto_0

    .line 44
    :cond_5
    const-string v1, "deleted_messages"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_7

    const-string v0, "GCoreUlr"

    invoke-static {v0, v4}, Lcom/google/android/location/reporting/b/d;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_6

    const-string v0, "GCoreUlr"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "GCM server deleted pending messages because they were collapsible."

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/location/reporting/b/d;->c(Ljava/lang/String;Ljava/lang/String;)V

    :cond_6
    const/4 v0, 0x1

    goto/16 :goto_1

    :cond_7
    const-string v1, "gcm"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto/16 :goto_1

    .line 69
    :cond_8
    const-string v0, "GcmBroadcastReceiver"

    invoke-static {p1, v0, v1}, Lcom/google/android/location/reporting/service/v;->a(Landroid/content/Context;Ljava/lang/String;Landroid/accounts/Account;)Z

    goto/16 :goto_0
.end method
