.class final Lcom/google/android/location/copresence/r/e;
.super Lcom/google/android/location/copresence/k/d;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/location/copresence/r/c;


# direct methods
.method constructor <init>(Lcom/google/android/location/copresence/r/c;)V
    .locals 0

    .prologue
    .line 64
    iput-object p1, p0, Lcom/google/android/location/copresence/r/e;->a:Lcom/google/android/location/copresence/r/c;

    invoke-direct {p0}, Lcom/google/android/location/copresence/k/d;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/location/copresence/k/f;)V
    .locals 2

    .prologue
    const/4 v1, 0x6

    .line 83
    iget-object v0, p0, Lcom/google/android/location/copresence/r/e;->a:Lcom/google/android/location/copresence/r/c;

    invoke-static {v0}, Lcom/google/android/location/copresence/r/c;->c(Lcom/google/android/location/copresence/r/c;)Lcom/google/android/location/copresence/k/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/location/copresence/k/a;->b()V

    .line 84
    iget-object v0, p0, Lcom/google/android/location/copresence/r/e;->a:Lcom/google/android/location/copresence/r/c;

    invoke-static {v0}, Lcom/google/android/location/copresence/r/c;->d(Lcom/google/android/location/copresence/r/c;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 85
    invoke-static {v1}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 86
    const-string v0, "WifiApBeacon2: Failed to revert."

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->e(Ljava/lang/String;)I

    .line 88
    :cond_0
    const/4 v0, 0x4

    invoke-static {v0}, Lcom/google/android/location/copresence/b/a;->b(I)V

    .line 90
    iget-object v0, p0, Lcom/google/android/location/copresence/r/e;->a:Lcom/google/android/location/copresence/r/c;

    invoke-static {v0}, Lcom/google/android/location/copresence/r/c;->b(Lcom/google/android/location/copresence/r/c;)Z

    .line 98
    :goto_0
    return-void

    .line 92
    :cond_1
    invoke-static {v1}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 93
    const-string v0, "WifiApBeacon2: State machine failed!"

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->e(Ljava/lang/String;)I

    .line 95
    :cond_2
    const/4 v0, 0x5

    invoke-static {v0}, Lcom/google/android/location/copresence/b/a;->b(I)V

    .line 96
    iget-object v0, p0, Lcom/google/android/location/copresence/r/e;->a:Lcom/google/android/location/copresence/r/c;

    invoke-virtual {v0}, Lcom/google/android/location/copresence/r/c;->c()V

    goto :goto_0
.end method

.method public final b(Lcom/google/android/location/copresence/k/f;)V
    .locals 1

    .prologue
    .line 68
    instance-of v0, p1, Lcom/google/android/location/copresence/r/m;

    if-eqz v0, :cond_2

    .line 69
    const/4 v0, 0x3

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 70
    const-string v0, "E2E Advertise: step 2d) Advertise on Wifi Ap started."

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->b(Ljava/lang/String;)I

    .line 72
    :cond_0
    check-cast p1, Lcom/google/android/location/copresence/r/m;

    iget-object v0, p1, Lcom/google/android/location/copresence/r/m;->a:Ljava/lang/String;

    .line 76
    :cond_1
    :goto_0
    return-void

    .line 74
    :cond_2
    iget-object v0, p0, Lcom/google/android/location/copresence/r/e;->a:Lcom/google/android/location/copresence/r/c;

    invoke-static {v0}, Lcom/google/android/location/copresence/r/c;->a(Lcom/google/android/location/copresence/r/c;)Lcom/google/android/location/copresence/r/f;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/location/copresence/r/f;->b:Lcom/google/android/location/copresence/k/f;

    if-ne p1, v0, :cond_1

    .line 75
    iget-object v0, p0, Lcom/google/android/location/copresence/r/e;->a:Lcom/google/android/location/copresence/r/c;

    invoke-static {v0}, Lcom/google/android/location/copresence/r/c;->b(Lcom/google/android/location/copresence/r/c;)Z

    goto :goto_0
.end method
