.class final Lcom/google/android/location/places/bc;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/v/b/n;


# instance fields
.field final synthetic a:J

.field final synthetic b:Lcom/google/android/location/places/ba;


# direct methods
.method constructor <init>(Lcom/google/android/location/places/ba;J)V
    .locals 0

    .prologue
    .line 188
    iput-object p1, p0, Lcom/google/android/location/places/bc;->b:Lcom/google/android/location/places/ba;

    iput-wide p2, p0, Lcom/google/android/location/places/bc;->a:J

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/v/b/m;Lcom/google/v/b/o;)V
    .locals 4

    .prologue
    .line 191
    const-string v0, "Places"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 192
    const-string v0, "Places"

    const-string v1, "Mac to cluster mapping request completed"

    invoke-static {v0, v1}, Lcom/google/android/location/n/aa;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 195
    :cond_0
    sget-object v0, Lcom/google/android/location/places/o;->h:Lcom/google/android/location/places/o;

    invoke-static {p2}, Lcom/google/android/location/places/f;->a(Lcom/google/v/b/o;)Lcom/google/android/location/l/a/ai;

    move-result-object v0

    .line 199
    :try_start_0
    new-instance v1, Lcom/google/p/a/b/b/a;

    sget-object v2, Lcom/google/android/location/m/a;->T:Lcom/google/p/a/b/b/c;

    invoke-direct {v1, v2}, Lcom/google/p/a/b/b/a;-><init>(Lcom/google/p/a/b/b/c;)V

    invoke-static {v0}, Lcom/google/protobuf/nano/j;->toByteArray(Lcom/google/protobuf/nano/j;)[B

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/google/p/a/b/b/a;->b([B)Lcom/google/p/a/b/b/a;

    move-result-object v0

    .line 201
    iget-object v1, p0, Lcom/google/android/location/places/bc;->b:Lcom/google/android/location/places/ba;

    invoke-static {v1}, Lcom/google/android/location/places/ba;->b(Lcom/google/android/location/places/ba;)Lcom/google/android/location/b/ad;

    move-result-object v1

    iget-wide v2, p0, Lcom/google/android/location/places/bc;->a:J

    invoke-virtual {v1, v0, v2, v3}, Lcom/google/android/location/b/ad;->a(Lcom/google/p/a/b/b/a;J)Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 207
    :cond_1
    :goto_0
    return-void

    .line 202
    :catch_0
    move-exception v0

    .line 203
    const-string v1, "Places"

    const/4 v2, 0x6

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 204
    const-string v1, "Places"

    const-string v2, "Error converting nano proto to J2ME."

    invoke-static {v1, v2, v0}, Lcom/google/android/location/n/aa;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public final a(Lcom/google/v/b/m;Ljava/lang/Exception;)V
    .locals 2

    .prologue
    .line 211
    const-string v0, "Places"

    const/4 v1, 0x6

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 212
    const-string v0, "Places"

    const-string v1, "Mac to cluster mapping request failed."

    invoke-static {v0, v1}, Lcom/google/android/location/n/aa;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 214
    :cond_0
    return-void
.end method
