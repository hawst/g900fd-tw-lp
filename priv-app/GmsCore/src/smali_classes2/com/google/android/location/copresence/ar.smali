.class final Lcom/google/android/location/copresence/ar;
.super Landroid/os/Handler;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/location/copresence/ap;

.field private final b:Landroid/os/PowerManager$WakeLock;


# direct methods
.method public constructor <init>(Lcom/google/android/location/copresence/ap;Landroid/os/Looper;Landroid/os/PowerManager;)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 149
    iput-object p1, p0, Lcom/google/android/location/copresence/ar;->a:Lcom/google/android/location/copresence/ap;

    .line 150
    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 151
    const-string v0, "copresGcore_EventLoop"

    invoke-virtual {p3, v1, v0}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/copresence/ar;->b:Landroid/os/PowerManager$WakeLock;

    .line 153
    iget-object v0, p0, Lcom/google/android/location/copresence/ar;->b:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0, v1}, Landroid/os/PowerManager$WakeLock;->setReferenceCounted(Z)V

    .line 154
    return-void
.end method

.method private static a(Ljava/lang/Runnable;J)V
    .locals 7

    .prologue
    .line 193
    const/4 v0, 0x4

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 194
    new-instance v1, Ljava/text/SimpleDateFormat;

    const-string v0, "mm:ss.SSS"

    invoke-direct {v1, v0}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 195
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    .line 196
    const-string v2, "Event %s ran for %s"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_0

    const-string v0, "<anonymous>"

    :cond_0
    aput-object v0, v3, v4

    const/4 v0, 0x1

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/text/SimpleDateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v3, v0

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 200
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "EventLoop: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->c(Ljava/lang/String;)I

    .line 202
    :cond_1
    return-void
.end method


# virtual methods
.method final a(Ljava/lang/Runnable;Landroid/os/WorkSource;)V
    .locals 2

    .prologue
    .line 177
    iget-object v1, p0, Lcom/google/android/location/copresence/ar;->b:Landroid/os/PowerManager$WakeLock;

    monitor-enter v1

    .line 178
    if-eqz p2, :cond_0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/location/copresence/ar;->a:Lcom/google/android/location/copresence/ap;

    invoke-static {v0}, Lcom/google/android/location/copresence/ap;->a(Lcom/google/android/location/copresence/ap;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/common/util/be;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 179
    iget-object v0, p0, Lcom/google/android/location/copresence/ar;->b:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0, p2}, Landroid/os/PowerManager$WakeLock;->setWorkSource(Landroid/os/WorkSource;)V

    .line 181
    :cond_0
    iget-object v0, p0, Lcom/google/android/location/copresence/ar;->b:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->acquire()V

    .line 182
    const/4 v0, 0x3

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 183
    const-string v0, "EventLoop: Wakelock Acquire"

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->b(Ljava/lang/String;)I

    .line 185
    :cond_1
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 187
    new-instance v0, Landroid/os/Message;

    invoke-direct {v0}, Landroid/os/Message;-><init>()V

    .line 188
    iput-object p1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 189
    invoke-virtual {p0, v0}, Lcom/google/android/location/copresence/ar;->sendMessage(Landroid/os/Message;)Z

    .line 190
    return-void

    .line 185
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final handleMessage(Landroid/os/Message;)V
    .locals 6

    .prologue
    .line 158
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Runnable;

    .line 159
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    .line 161
    :try_start_0
    invoke-interface {v0}, Ljava/lang/Runnable;->run()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 163
    iget-object v1, p0, Lcom/google/android/location/copresence/ar;->b:Landroid/os/PowerManager$WakeLock;

    monitor-enter v1

    .line 164
    :try_start_1
    iget-object v4, p0, Lcom/google/android/location/copresence/ar;->b:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v4}, Landroid/os/PowerManager$WakeLock;->release()V

    .line 165
    const/4 v4, 0x3

    invoke-static {v4}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 166
    const-string v4, "EventLoop: Wakelock Release"

    invoke-static {v4}, Lcom/google/android/location/copresence/ag;->b(Ljava/lang/String;)I

    .line 168
    :cond_0
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 169
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v4

    sub-long v2, v4, v2

    .line 170
    invoke-static {}, Lcom/google/android/location/copresence/ap;->e()J

    move-result-wide v4

    cmp-long v1, v2, v4

    if-lez v1, :cond_1

    .line 171
    invoke-static {v0, v2, v3}, Lcom/google/android/location/copresence/ar;->a(Ljava/lang/Runnable;J)V

    .line 173
    :cond_1
    return-void

    .line 168
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 163
    :catchall_1
    move-exception v1

    iget-object v4, p0, Lcom/google/android/location/copresence/ar;->b:Landroid/os/PowerManager$WakeLock;

    monitor-enter v4

    .line 164
    :try_start_2
    iget-object v5, p0, Lcom/google/android/location/copresence/ar;->b:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v5}, Landroid/os/PowerManager$WakeLock;->release()V

    .line 165
    const/4 v5, 0x3

    invoke-static {v5}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 166
    const-string v5, "EventLoop: Wakelock Release"

    invoke-static {v5}, Lcom/google/android/location/copresence/ag;->b(Ljava/lang/String;)I

    .line 168
    :cond_2
    monitor-exit v4
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 169
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v4

    sub-long v2, v4, v2

    .line 170
    invoke-static {}, Lcom/google/android/location/copresence/ap;->e()J

    move-result-wide v4

    cmp-long v4, v2, v4

    if-lez v4, :cond_3

    .line 171
    invoke-static {v0, v2, v3}, Lcom/google/android/location/copresence/ar;->a(Ljava/lang/Runnable;J)V

    .line 173
    :cond_3
    throw v1

    .line 168
    :catchall_2
    move-exception v0

    monitor-exit v4

    throw v0
.end method
