.class public abstract Lcom/google/android/location/activity/a/ae;
.super Lcom/google/android/location/activity/a/n;
.source "SourceFile"


# static fields
.field private static final a:[Lcom/google/android/location/activity/a/t;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 24
    const/4 v0, 0x6

    new-array v0, v0, [Lcom/google/android/location/activity/a/t;

    const/4 v1, 0x0

    sget-object v2, Lcom/google/android/location/activity/a/t;->k:Lcom/google/android/location/activity/a/t;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    sget-object v2, Lcom/google/android/location/activity/a/t;->a:Lcom/google/android/location/activity/a/t;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    sget-object v2, Lcom/google/android/location/activity/a/t;->c:Lcom/google/android/location/activity/a/t;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    sget-object v2, Lcom/google/android/location/activity/a/t;->f:Lcom/google/android/location/activity/a/t;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    sget-object v2, Lcom/google/android/location/activity/a/t;->g:Lcom/google/android/location/activity/a/t;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    sget-object v2, Lcom/google/android/location/activity/a/t;->e:Lcom/google/android/location/activity/a/t;

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/location/activity/a/ae;->a:[Lcom/google/android/location/activity/a/t;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 12
    invoke-direct {p0}, Lcom/google/android/location/activity/a/n;-><init>()V

    return-void
.end method


# virtual methods
.method protected abstract a(FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF)I
.end method

.method public final a(Lcom/google/android/location/activity/a/r;)Lcom/google/android/location/activity/a/s;
    .locals 40

    .prologue
    .line 30
    move-object/from16 v0, p1

    iget v2, v0, Lcom/google/android/location/activity/a/r;->b:F

    move-object/from16 v0, p1

    iget v3, v0, Lcom/google/android/location/activity/a/r;->s:F

    move-object/from16 v0, p1

    iget-object v1, v0, Lcom/google/android/location/activity/a/r;->n:[F

    const/4 v4, 0x0

    aget v4, v1, v4

    move-object/from16 v0, p1

    iget-object v1, v0, Lcom/google/android/location/activity/a/r;->n:[F

    const/4 v5, 0x1

    aget v5, v1, v5

    move-object/from16 v0, p1

    iget-object v1, v0, Lcom/google/android/location/activity/a/r;->n:[F

    const/4 v6, 0x2

    aget v6, v1, v6

    move-object/from16 v0, p1

    iget-object v1, v0, Lcom/google/android/location/activity/a/r;->n:[F

    const/4 v7, 0x3

    aget v7, v1, v7

    move-object/from16 v0, p1

    iget-object v1, v0, Lcom/google/android/location/activity/a/r;->q:[F

    const/4 v8, 0x0

    aget v8, v1, v8

    move-object/from16 v0, p1

    iget-object v1, v0, Lcom/google/android/location/activity/a/r;->q:[F

    const/4 v9, 0x1

    aget v9, v1, v9

    move-object/from16 v0, p1

    iget-object v1, v0, Lcom/google/android/location/activity/a/r;->q:[F

    const/4 v10, 0x2

    aget v10, v1, v10

    move-object/from16 v0, p1

    iget-object v1, v0, Lcom/google/android/location/activity/a/r;->q:[F

    const/4 v11, 0x3

    aget v11, v1, v11

    move-object/from16 v0, p1

    iget v12, v0, Lcom/google/android/location/activity/a/r;->r:F

    move-object/from16 v0, p1

    iget-object v1, v0, Lcom/google/android/location/activity/a/r;->m:[F

    const/4 v13, 0x0

    aget v13, v1, v13

    move-object/from16 v0, p1

    iget-object v1, v0, Lcom/google/android/location/activity/a/r;->m:[F

    const/4 v14, 0x1

    aget v14, v1, v14

    move-object/from16 v0, p1

    iget-object v1, v0, Lcom/google/android/location/activity/a/r;->m:[F

    const/4 v15, 0x2

    aget v15, v1, v15

    move-object/from16 v0, p1

    iget-object v1, v0, Lcom/google/android/location/activity/a/r;->m:[F

    const/16 v16, 0x3

    aget v16, v1, v16

    move-object/from16 v0, p1

    iget-object v1, v0, Lcom/google/android/location/activity/a/r;->p:[F

    const/16 v17, 0x0

    aget v17, v1, v17

    move-object/from16 v0, p1

    iget-object v1, v0, Lcom/google/android/location/activity/a/r;->p:[F

    const/16 v18, 0x1

    aget v18, v1, v18

    move-object/from16 v0, p1

    iget-object v1, v0, Lcom/google/android/location/activity/a/r;->p:[F

    const/16 v19, 0x2

    aget v19, v1, v19

    move-object/from16 v0, p1

    iget-object v1, v0, Lcom/google/android/location/activity/a/r;->p:[F

    const/16 v20, 0x3

    aget v20, v1, v20

    move-object/from16 v0, p1

    iget-object v1, v0, Lcom/google/android/location/activity/a/r;->o:[F

    const/16 v21, 0x0

    aget v21, v1, v21

    move-object/from16 v0, p1

    iget-object v1, v0, Lcom/google/android/location/activity/a/r;->o:[F

    const/16 v22, 0x1

    aget v22, v1, v22

    move-object/from16 v0, p1

    iget-object v1, v0, Lcom/google/android/location/activity/a/r;->o:[F

    const/16 v23, 0x2

    aget v23, v1, v23

    move-object/from16 v0, p1

    iget-object v1, v0, Lcom/google/android/location/activity/a/r;->o:[F

    const/16 v24, 0x3

    aget v24, v1, v24

    move-object/from16 v0, p1

    iget v0, v0, Lcom/google/android/location/activity/a/r;->a:F

    move/from16 v25, v0

    move-object/from16 v0, p1

    iget v0, v0, Lcom/google/android/location/activity/a/r;->w:F

    move/from16 v26, v0

    move-object/from16 v0, p1

    iget v0, v0, Lcom/google/android/location/activity/a/r;->v:F

    move/from16 v27, v0

    move-object/from16 v0, p1

    iget v0, v0, Lcom/google/android/location/activity/a/r;->u:F

    move/from16 v28, v0

    move-object/from16 v0, p1

    iget v0, v0, Lcom/google/android/location/activity/a/r;->l:F

    move/from16 v29, v0

    move-object/from16 v0, p1

    iget v0, v0, Lcom/google/android/location/activity/a/r;->t:F

    move/from16 v30, v0

    move-object/from16 v0, p1

    iget v0, v0, Lcom/google/android/location/activity/a/r;->k:F

    move/from16 v31, v0

    move-object/from16 v0, p1

    iget v0, v0, Lcom/google/android/location/activity/a/r;->e:F

    move/from16 v32, v0

    move-object/from16 v0, p1

    iget v0, v0, Lcom/google/android/location/activity/a/r;->g:F

    move/from16 v33, v0

    move-object/from16 v0, p1

    iget v0, v0, Lcom/google/android/location/activity/a/r;->i:F

    move/from16 v34, v0

    move-object/from16 v0, p1

    iget v0, v0, Lcom/google/android/location/activity/a/r;->d:F

    move/from16 v35, v0

    move-object/from16 v0, p1

    iget v0, v0, Lcom/google/android/location/activity/a/r;->j:F

    move/from16 v36, v0

    move-object/from16 v0, p1

    iget v0, v0, Lcom/google/android/location/activity/a/r;->h:F

    move/from16 v37, v0

    move-object/from16 v0, p1

    iget v0, v0, Lcom/google/android/location/activity/a/r;->f:F

    move/from16 v38, v0

    move-object/from16 v0, p1

    iget v0, v0, Lcom/google/android/location/activity/a/r;->c:F

    move/from16 v39, v0

    move-object/from16 v1, p0

    invoke-virtual/range {v1 .. v39}, Lcom/google/android/location/activity/a/ae;->a(FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF)I

    move-result v1

    .line 69
    new-instance v2, Lcom/google/android/location/activity/a/s;

    sget-object v3, Lcom/google/android/location/activity/a/ae;->a:[Lcom/google/android/location/activity/a/t;

    aget-object v1, v3, v1

    const/16 v3, 0x64

    invoke-direct {v2, v1, v3}, Lcom/google/android/location/activity/a/s;-><init>(Lcom/google/android/location/activity/a/t;I)V

    return-object v2
.end method
