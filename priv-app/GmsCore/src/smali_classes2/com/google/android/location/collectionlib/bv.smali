.class Lcom/google/android/location/collectionlib/bv;
.super Lcom/google/android/location/collectionlib/cc;
.source "SourceFile"


# instance fields
.field private final e:Ljava/lang/String;

.field private final f:Ljava/lang/String;

.field private final g:[B

.field private final h:Landroid/os/PowerManager;

.field private final i:Landroid/content/Context;

.field private volatile j:Lcom/google/android/location/collectionlib/bj;

.field private volatile k:Z

.field private final l:Ljava/lang/String;

.field private volatile m:Lcom/google/android/location/collectionlib/bg;

.field private n:Ljava/lang/Object;

.field private final o:Lcom/google/android/location/collectionlib/by;

.field private p:Lcom/google/android/location/collectionlib/bz;

.field private volatile q:Z

.field private final r:Lcom/google/android/location/o/n;


# direct methods
.method constructor <init>(Landroid/content/Context;Lcom/google/android/location/collectionlib/by;Lcom/google/android/location/collectionlib/cp;Ljava/lang/String;Ljava/lang/String;[BLcom/google/android/location/collectionlib/ar;Ljava/lang/String;Lcom/google/android/location/o/a/c;Lcom/google/android/location/o/n;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 108
    invoke-direct {p0, p7, p9, p3}, Lcom/google/android/location/collectionlib/cc;-><init>(Lcom/google/android/location/collectionlib/ar;Lcom/google/android/location/o/a/c;Lcom/google/android/location/collectionlib/cp;)V

    .line 58
    iput-boolean v1, p0, Lcom/google/android/location/collectionlib/bv;->k:Z

    .line 62
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/location/collectionlib/bv;->n:Ljava/lang/Object;

    .line 66
    iput-boolean v1, p0, Lcom/google/android/location/collectionlib/bv;->q:Z

    .line 109
    const-string v0, "Session id should not be null. Please make sure you called the correct constructor."

    invoke-static {p8, v0}, Lcom/google/android/location/collectionlib/df;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 111
    iput-object p1, p0, Lcom/google/android/location/collectionlib/bv;->i:Landroid/content/Context;

    .line 112
    iput-object p4, p0, Lcom/google/android/location/collectionlib/bv;->f:Ljava/lang/String;

    .line 113
    const-string v0, "power"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    iput-object v0, p0, Lcom/google/android/location/collectionlib/bv;->h:Landroid/os/PowerManager;

    .line 114
    iput-object p5, p0, Lcom/google/android/location/collectionlib/bv;->e:Ljava/lang/String;

    .line 115
    iput-object p6, p0, Lcom/google/android/location/collectionlib/bv;->g:[B

    .line 116
    iput-object p8, p0, Lcom/google/android/location/collectionlib/bv;->l:Ljava/lang/String;

    .line 117
    iput-object p2, p0, Lcom/google/android/location/collectionlib/bv;->o:Lcom/google/android/location/collectionlib/by;

    .line 118
    iput-object p10, p0, Lcom/google/android/location/collectionlib/bv;->r:Lcom/google/android/location/o/n;

    .line 119
    return-void
.end method

.method constructor <init>(Landroid/content/Context;Lcom/google/android/location/collectionlib/by;Lcom/google/android/location/collectionlib/cp;Ljava/lang/String;[BLcom/google/android/location/collectionlib/ar;Lcom/google/android/location/o/a/c;Lcom/google/android/location/o/n;)V
    .locals 11

    .prologue
    .line 86
    const/4 v4, 0x0

    invoke-static {}, Lcom/google/android/location/collectionlib/bv;->b()Ljava/lang/String;

    move-result-object v8

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v9, p7

    move-object/from16 v10, p8

    invoke-direct/range {v0 .. v10}, Lcom/google/android/location/collectionlib/bv;-><init>(Landroid/content/Context;Lcom/google/android/location/collectionlib/by;Lcom/google/android/location/collectionlib/cp;Ljava/lang/String;Ljava/lang/String;[BLcom/google/android/location/collectionlib/ar;Ljava/lang/String;Lcom/google/android/location/o/a/c;Lcom/google/android/location/o/n;)V

    .line 96
    return-void
.end method

.method static synthetic a(Lcom/google/android/location/collectionlib/bv;)Landroid/os/PowerManager;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lcom/google/android/location/collectionlib/bv;->h:Landroid/os/PowerManager;

    return-object v0
.end method

.method static synthetic a(Lcom/google/p/a/b/b/a;Lcom/google/p/a/b/b/a;Ljava/lang/String;)Lcom/google/p/a/b/b/a;
    .locals 1

    .prologue
    .line 41
    invoke-static {p0, p1, p2}, Lcom/google/android/location/collectionlib/bv;->b(Lcom/google/p/a/b/b/a;Lcom/google/p/a/b/b/a;Ljava/lang/String;)Lcom/google/p/a/b/b/a;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/location/collectionlib/bv;Lcom/google/p/a/b/b/a;Lcom/google/android/location/collectionlib/cd;ILjava/lang/String;)V
    .locals 0

    .prologue
    .line 41
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/android/location/collectionlib/bv;->a(Lcom/google/p/a/b/b/a;Lcom/google/android/location/collectionlib/cd;ILjava/lang/String;)V

    return-void
.end method

.method private a(Lcom/google/p/a/b/b/a;Lcom/google/android/location/collectionlib/cd;ILjava/lang/String;)V
    .locals 9

    .prologue
    const/4 v6, 0x6

    const/4 v5, 0x3

    .line 486
    iget-object v0, p0, Lcom/google/android/location/collectionlib/bv;->a:Lcom/google/android/location/collectionlib/ar;

    if-eqz v0, :cond_0

    .line 487
    iget-object v0, p0, Lcom/google/android/location/collectionlib/bv;->a:Lcom/google/android/location/collectionlib/ar;

    iget-object v1, p2, Lcom/google/android/location/collectionlib/cd;->d:Ljava/lang/String;

    invoke-interface {v0, p4, p3, v1}, Lcom/google/android/location/collectionlib/ar;->a(Ljava/lang/String;ILjava/lang/String;)V

    .line 489
    :cond_0
    iget-object v0, p0, Lcom/google/android/location/collectionlib/bv;->e:Ljava/lang/String;

    if-nez v0, :cond_2

    sget-boolean v0, Lcom/google/android/location/i/a;->b:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/location/collectionlib/bv;->b:Lcom/google/android/location/o/a/c;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "No backup data path specified, dropping data seqNum:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1, v6}, Lcom/google/p/a/b/b/a;->f(I)Lcom/google/p/a/b/b/a;

    move-result-object v2

    invoke-virtual {v2, v5}, Lcom/google/p/a/b/b/a;->c(I)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/location/o/a/c;->a(Ljava/lang/String;)V

    .line 490
    :cond_1
    :goto_0
    return-void

    .line 489
    :cond_2
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->getName()Ljava/lang/String;

    move-result-object v0

    sget-boolean v1, Lcom/google/android/location/i/a;->b:Z

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/google/android/location/collectionlib/bv;->b:Lcom/google/android/location/o/a/c;

    const-string v2, "%s: Backing up %s/%s to path: %s"

    const/4 v3, 0x4

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    const/4 v0, 0x1

    invoke-virtual {p1, v6}, Lcom/google/p/a/b/b/a;->f(I)Lcom/google/p/a/b/b/a;

    move-result-object v4

    invoke-virtual {v4, v5}, Lcom/google/p/a/b/b/a;->c(I)I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v0

    const/4 v0, 0x2

    aput-object p4, v3, v0

    iget-object v0, p0, Lcom/google/android/location/collectionlib/bv;->e:Ljava/lang/String;

    aput-object v0, v3, v5

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/google/android/location/o/a/c;->a(Ljava/lang/String;)V

    :cond_3
    iget-object v8, p0, Lcom/google/android/location/collectionlib/bv;->n:Ljava/lang/Object;

    monitor-enter v8

    :try_start_0
    iget-object v0, p0, Lcom/google/android/location/collectionlib/bv;->m:Lcom/google/android/location/collectionlib/bg;

    if-nez v0, :cond_4

    new-instance v0, Lcom/google/android/location/collectionlib/bg;

    iget-object v1, p0, Lcom/google/android/location/collectionlib/bv;->h:Landroid/os/PowerManager;

    iget-object v2, p0, Lcom/google/android/location/collectionlib/bv;->e:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/location/collectionlib/bv;->g:[B

    iget-object v4, p0, Lcom/google/android/location/collectionlib/bv;->a:Lcom/google/android/location/collectionlib/ar;

    iget-object v5, p0, Lcom/google/android/location/collectionlib/bv;->b:Lcom/google/android/location/o/a/c;

    const/4 v6, 0x0

    iget-object v7, p0, Lcom/google/android/location/collectionlib/bv;->r:Lcom/google/android/location/o/n;

    invoke-direct/range {v0 .. v7}, Lcom/google/android/location/collectionlib/bg;-><init>(Landroid/os/PowerManager;Ljava/lang/String;[BLcom/google/android/location/collectionlib/ar;Lcom/google/android/location/o/a/c;Lcom/google/android/location/collectionlib/cp;Lcom/google/android/location/o/n;)V

    iput-object v0, p0, Lcom/google/android/location/collectionlib/bv;->m:Lcom/google/android/location/collectionlib/bg;

    :cond_4
    monitor-exit v8
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz p4, :cond_5

    iget-object v0, p0, Lcom/google/android/location/collectionlib/bv;->m:Lcom/google/android/location/collectionlib/bg;

    invoke-virtual {v0, p4}, Lcom/google/android/location/collectionlib/bg;->a(Ljava/lang/String;)Lcom/google/android/location/collectionlib/cd;

    move-result-object v0

    iget-boolean v0, v0, Lcom/google/android/location/collectionlib/cd;->a:Z

    if-nez v0, :cond_5

    sget-boolean v0, Lcom/google/android/location/i/a;->b:Z

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/google/android/location/collectionlib/bv;->b:Lcom/google/android/location/o/a/c;

    const-string v1, "Failed to write session ID, will try later."

    invoke-virtual {v0, v1}, Lcom/google/android/location/o/a/c;->a(Ljava/lang/String;)V

    :cond_5
    iget-object v0, p0, Lcom/google/android/location/collectionlib/bv;->m:Lcom/google/android/location/collectionlib/bg;

    invoke-virtual {v0, p1}, Lcom/google/android/location/collectionlib/bg;->a(Lcom/google/p/a/b/b/a;)Z

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v8

    throw v0
.end method

.method static synthetic b(Lcom/google/android/location/collectionlib/bv;)Lcom/google/android/location/o/n;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lcom/google/android/location/collectionlib/bv;->r:Lcom/google/android/location/o/n;

    return-object v0
.end method

.method private static b(Lcom/google/p/a/b/b/a;Lcom/google/p/a/b/b/a;Ljava/lang/String;)Lcom/google/p/a/b/b/a;
    .locals 1

    .prologue
    .line 473
    if-eqz p2, :cond_0

    .line 474
    const/4 v0, 0x2

    invoke-virtual {p1, v0, p2}, Lcom/google/p/a/b/b/a;->a(ILjava/lang/String;)Lcom/google/p/a/b/b/a;

    .line 476
    :cond_0
    const/4 v0, 0x6

    invoke-virtual {p0, v0, p1}, Lcom/google/p/a/b/b/a;->b(ILcom/google/p/a/b/b/a;)Lcom/google/p/a/b/b/a;

    .line 477
    return-object p0
.end method

.method static b()Ljava/lang/String;
    .locals 2

    .prologue
    .line 141
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "@"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic c(Lcom/google/android/location/collectionlib/bv;)Lcom/google/android/location/collectionlib/bz;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lcom/google/android/location/collectionlib/bv;->p:Lcom/google/android/location/collectionlib/bz;

    return-object v0
.end method

.method static synthetic d(Lcom/google/android/location/collectionlib/bv;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lcom/google/android/location/collectionlib/bv;->l:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic e(Lcom/google/android/location/collectionlib/bv;)Z
    .locals 1

    .prologue
    .line 41
    iget-boolean v0, p0, Lcom/google/android/location/collectionlib/bv;->k:Z

    return v0
.end method

.method static synthetic f(Lcom/google/android/location/collectionlib/bv;)Lcom/google/android/location/collectionlib/bj;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lcom/google/android/location/collectionlib/bv;->j:Lcom/google/android/location/collectionlib/bj;

    return-object v0
.end method

.method static synthetic g(Lcom/google/android/location/collectionlib/bv;)Lcom/google/android/location/collectionlib/bg;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lcom/google/android/location/collectionlib/bv;->m:Lcom/google/android/location/collectionlib/bg;

    return-object v0
.end method


# virtual methods
.method protected final a()V
    .locals 2

    .prologue
    .line 564
    sget-boolean v0, Lcom/google/android/location/i/a;->b:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/location/collectionlib/bv;->b:Lcom/google/android/location/o/a/c;

    const-string v1, "RemoteScanResultWriter.workerThread will be closed."

    invoke-virtual {v0, v1}, Lcom/google/android/location/o/a/c;->a(Ljava/lang/String;)V

    .line 565
    :cond_0
    iget-object v0, p0, Lcom/google/android/location/collectionlib/bv;->p:Lcom/google/android/location/collectionlib/bz;

    if-eqz v0, :cond_1

    .line 566
    iget-object v0, p0, Lcom/google/android/location/collectionlib/bv;->p:Lcom/google/android/location/collectionlib/bz;

    invoke-virtual {v0}, Lcom/google/android/location/collectionlib/bz;->a()V

    .line 568
    :cond_1
    return-void
.end method

.method protected final declared-synchronized a(Lcom/google/p/a/b/b/a;Lcom/google/p/a/b/b/a;)Z
    .locals 6

    .prologue
    .line 531
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/location/collectionlib/bv;->q:Z

    if-nez v0, :cond_1

    .line 532
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/location/collectionlib/bv;->q:Z

    .line 533
    new-instance v0, Lcom/google/android/location/collectionlib/bj;

    iget-object v1, p0, Lcom/google/android/location/collectionlib/bv;->i:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/location/collectionlib/bv;->b:Lcom/google/android/location/o/a/c;

    invoke-direct {v0, v1, v2}, Lcom/google/android/location/collectionlib/bj;-><init>(Landroid/content/Context;Lcom/google/android/location/o/a/c;)V

    iput-object v0, p0, Lcom/google/android/location/collectionlib/bv;->j:Lcom/google/android/location/collectionlib/bj;

    new-instance v0, Lcom/google/android/location/collectionlib/bx;

    const-string v1, "RemoteScanResultWriter.workerThread"

    invoke-direct {v0, p0, v1}, Lcom/google/android/location/collectionlib/bx;-><init>(Lcom/google/android/location/collectionlib/bv;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/google/android/location/collectionlib/bx;->start()V

    sget-boolean v1, Lcom/google/android/location/i/a;->b:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/location/collectionlib/bv;->b:Lcom/google/android/location/o/a/c;

    const-string v2, "Waiting for the RemoteScanResultWriter.workerThread to start."

    invoke-virtual {v1, v2}, Lcom/google/android/location/o/a/c;->a(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v0}, Lcom/google/android/location/collectionlib/bx;->getLooper()Landroid/os/Looper;

    move-result-object v0

    new-instance v1, Lcom/google/android/location/collectionlib/bz;

    invoke-direct {v1, p0, v0}, Lcom/google/android/location/collectionlib/bz;-><init>(Lcom/google/android/location/collectionlib/bv;Landroid/os/Looper;)V

    iput-object v1, p0, Lcom/google/android/location/collectionlib/bv;->p:Lcom/google/android/location/collectionlib/bz;

    sget-boolean v0, Lcom/google/android/location/i/a;->b:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/location/collectionlib/bv;->b:Lcom/google/android/location/o/a/c;

    const-string v1, "RemoteScanResultWriter.workerThread started."

    invoke-virtual {v0, v1}, Lcom/google/android/location/o/a/c;->a(Ljava/lang/String;)V

    .line 537
    :cond_1
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Lcom/google/p/a/b/b/a;->i(I)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/location/collectionlib/bv;->f:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 538
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Lcom/google/p/a/b/b/a;->f(I)Lcom/google/p/a/b/b/a;

    move-result-object v0

    .line 539
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/android/location/collectionlib/bv;->f:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/google/p/a/b/b/a;->a(ILjava/lang/String;)Lcom/google/p/a/b/b/a;

    .line 541
    :cond_2
    iget-object v0, p0, Lcom/google/android/location/collectionlib/bv;->o:Lcom/google/android/location/collectionlib/by;

    sget-object v1, Lcom/google/android/location/collectionlib/by;->a:Lcom/google/android/location/collectionlib/by;

    if-ne v0, v1, :cond_4

    .line 542
    iget-object v0, p0, Lcom/google/android/location/collectionlib/bv;->p:Lcom/google/android/location/collectionlib/bz;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, p2, v1}, Lcom/google/android/location/collectionlib/bz;->a(Lcom/google/p/a/b/b/a;Lcom/google/p/a/b/b/a;Z)Z

    move-result v0

    .line 543
    if-nez v0, :cond_3

    .line 545
    iget-object v1, p0, Lcom/google/android/location/collectionlib/bv;->l:Ljava/lang/String;

    invoke-static {p1, p2, v1}, Lcom/google/android/location/collectionlib/bv;->b(Lcom/google/p/a/b/b/a;Lcom/google/p/a/b/b/a;Ljava/lang/String;)Lcom/google/p/a/b/b/a;

    move-result-object v1

    new-instance v2, Lcom/google/android/location/collectionlib/cd;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const-string v5, "To many data in upload queue."

    invoke-direct {v2, v3, v4, v5}, Lcom/google/android/location/collectionlib/cd;-><init>(ZLcom/google/p/a/b/b/a;Ljava/lang/String;)V

    const/4 v3, 0x6

    invoke-virtual {v1, v3}, Lcom/google/p/a/b/b/a;->f(I)Lcom/google/p/a/b/b/a;

    move-result-object v3

    const/4 v4, 0x3

    invoke-virtual {v3, v4}, Lcom/google/p/a/b/b/a;->c(I)I

    move-result v3

    iget-object v4, p0, Lcom/google/android/location/collectionlib/bv;->l:Ljava/lang/String;

    invoke-direct {p0, v1, v2, v3, v4}, Lcom/google/android/location/collectionlib/bv;->a(Lcom/google/p/a/b/b/a;Lcom/google/android/location/collectionlib/cd;ILjava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 549
    :cond_3
    :goto_0
    monitor-exit p0

    return v0

    :cond_4
    :try_start_1
    iget-object v0, p0, Lcom/google/android/location/collectionlib/bv;->p:Lcom/google/android/location/collectionlib/bz;

    const/4 v1, 0x1

    invoke-virtual {v0, p1, p2, v1}, Lcom/google/android/location/collectionlib/bz;->a(Lcom/google/p/a/b/b/a;Lcom/google/p/a/b/b/a;Z)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v0

    goto :goto_0

    .line 531
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final c()V
    .locals 2

    .prologue
    .line 572
    sget-boolean v0, Lcom/google/android/location/i/a;->b:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/location/collectionlib/bv;->b:Lcom/google/android/location/o/a/c;

    const-string v1, "RemoteScanResultWriter interrupted."

    invoke-virtual {v0, v1}, Lcom/google/android/location/o/a/c;->a(Ljava/lang/String;)V

    .line 573
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/location/collectionlib/bv;->k:Z

    .line 574
    return-void
.end method
