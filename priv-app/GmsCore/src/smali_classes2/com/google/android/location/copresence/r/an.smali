.class final Lcom/google/android/location/copresence/r/an;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/location/copresence/z;


# static fields
.field private static final b:I


# instance fields
.field private final c:Landroid/content/Context;

.field private final d:Lcom/google/android/location/copresence/r/ae;

.field private final e:Lcom/google/android/location/copresence/ap;

.field private final f:Z

.field private final g:Landroid/content/BroadcastReceiver;

.field private final h:Ljava/lang/Runnable;

.field private i:Lcom/google/android/location/copresence/ab;

.field private j:Lcom/google/ac/b/c/o;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 36
    const/16 v0, 0xa

    sput v0, Lcom/google/android/location/copresence/r/an;->b:I

    return-void
.end method

.method constructor <init>(Landroid/content/Context;Lcom/google/android/location/copresence/r/ae;Lcom/google/android/location/copresence/ap;Z)V
    .locals 1

    .prologue
    .line 122
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    new-instance v0, Lcom/google/android/location/copresence/r/ao;

    invoke-direct {v0, p0}, Lcom/google/android/location/copresence/r/ao;-><init>(Lcom/google/android/location/copresence/r/an;)V

    iput-object v0, p0, Lcom/google/android/location/copresence/r/an;->g:Landroid/content/BroadcastReceiver;

    .line 102
    new-instance v0, Lcom/google/android/location/copresence/r/ap;

    invoke-direct {v0, p0}, Lcom/google/android/location/copresence/r/ap;-><init>(Lcom/google/android/location/copresence/r/an;)V

    iput-object v0, p0, Lcom/google/android/location/copresence/r/an;->h:Ljava/lang/Runnable;

    .line 123
    iput-object p1, p0, Lcom/google/android/location/copresence/r/an;->c:Landroid/content/Context;

    .line 124
    iput-object p2, p0, Lcom/google/android/location/copresence/r/an;->d:Lcom/google/android/location/copresence/r/ae;

    .line 125
    iput-object p3, p0, Lcom/google/android/location/copresence/r/an;->e:Lcom/google/android/location/copresence/ap;

    .line 126
    iput-boolean p4, p0, Lcom/google/android/location/copresence/r/an;->f:Z

    .line 127
    return-void
.end method

.method static synthetic a(Lcom/google/android/location/copresence/r/an;)Lcom/google/android/location/copresence/ab;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lcom/google/android/location/copresence/r/an;->i:Lcom/google/android/location/copresence/ab;

    return-object v0
.end method

.method static synthetic b(Lcom/google/android/location/copresence/r/an;)Lcom/google/android/location/copresence/r/ae;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lcom/google/android/location/copresence/r/an;->d:Lcom/google/android/location/copresence/r/ae;

    return-object v0
.end method

.method static synthetic c(Lcom/google/android/location/copresence/r/an;)Lcom/google/ac/b/c/o;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lcom/google/android/location/copresence/r/an;->j:Lcom/google/ac/b/c/o;

    return-object v0
.end method

.method static synthetic d()I
    .locals 1

    .prologue
    .line 30
    sget v0, Lcom/google/android/location/copresence/r/an;->b:I

    return v0
.end method

.method static synthetic d(Lcom/google/android/location/copresence/r/an;)Z
    .locals 1

    .prologue
    .line 30
    iget-boolean v0, p0, Lcom/google/android/location/copresence/r/an;->f:Z

    return v0
.end method

.method static synthetic e(Lcom/google/android/location/copresence/r/an;)Ljava/lang/Runnable;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lcom/google/android/location/copresence/r/an;->h:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic f(Lcom/google/android/location/copresence/r/an;)Lcom/google/android/location/copresence/ap;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lcom/google/android/location/copresence/r/an;->e:Lcom/google/android/location/copresence/ap;

    return-object v0
.end method


# virtual methods
.method public final a(Lcom/google/android/location/copresence/ab;Lcom/google/android/location/copresence/as;Lcom/google/android/location/copresence/q/al;)V
    .locals 5

    .prologue
    .line 147
    invoke-virtual {p0}, Lcom/google/android/location/copresence/r/an;->b()Z

    move-result v0

    if-nez v0, :cond_0

    .line 148
    new-instance v0, Lcom/google/android/location/copresence/ac;

    invoke-direct {v0}, Lcom/google/android/location/copresence/ac;-><init>()V

    throw v0

    .line 150
    :cond_0
    iget-object v0, p0, Lcom/google/android/location/copresence/r/an;->i:Lcom/google/android/location/copresence/ab;

    if-nez v0, :cond_1

    .line 151
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 152
    const-string v1, "android.net.wifi.SCAN_RESULTS"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 153
    iget-object v1, p0, Lcom/google/android/location/copresence/r/an;->c:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/location/copresence/r/an;->g:Landroid/content/BroadcastReceiver;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/android/location/copresence/r/an;->e:Lcom/google/android/location/copresence/ap;

    invoke-virtual {v4}, Lcom/google/android/location/copresence/ap;->d()Landroid/os/Handler;

    move-result-object v4

    invoke-virtual {v1, v2, v0, v3, v4}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;Ljava/lang/String;Landroid/os/Handler;)Landroid/content/Intent;

    .line 154
    const/4 v0, 0x3

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 155
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v0, "E2E Listen: step 2d) Listen on Wifi started "

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v0, p0, Lcom/google/android/location/copresence/r/an;->f:Z

    if-eqz v0, :cond_4

    const-string v0, "(active)"

    :goto_0
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->b(Ljava/lang/String;)I

    .line 159
    :cond_1
    iget-object v0, p2, Lcom/google/android/location/copresence/as;->b:Lcom/google/ac/b/c/o;

    iput-object v0, p0, Lcom/google/android/location/copresence/r/an;->j:Lcom/google/ac/b/c/o;

    .line 160
    iput-object p1, p0, Lcom/google/android/location/copresence/r/an;->i:Lcom/google/android/location/copresence/ab;

    .line 161
    iget-boolean v0, p0, Lcom/google/android/location/copresence/r/an;->f:Z

    if-eqz v0, :cond_2

    .line 162
    iget-object v0, p0, Lcom/google/android/location/copresence/r/an;->h:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    .line 164
    :cond_2
    if-eqz p3, :cond_3

    .line 165
    invoke-interface {p3}, Lcom/google/android/location/copresence/q/al;->a()V

    .line 167
    :cond_3
    return-void

    .line 155
    :cond_4
    const-string v0, "(passive)"

    goto :goto_0
.end method

.method public final a(Lcom/google/android/location/copresence/q/al;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 171
    iget-object v0, p0, Lcom/google/android/location/copresence/r/an;->i:Lcom/google/android/location/copresence/ab;

    if-eqz v0, :cond_1

    .line 173
    iput-object v1, p0, Lcom/google/android/location/copresence/r/an;->i:Lcom/google/android/location/copresence/ab;

    .line 174
    iput-object v1, p0, Lcom/google/android/location/copresence/r/an;->j:Lcom/google/ac/b/c/o;

    .line 176
    :try_start_0
    iget-object v0, p0, Lcom/google/android/location/copresence/r/an;->c:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/location/copresence/r/an;->g:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 182
    :cond_0
    :goto_0
    const/4 v0, 0x3

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 183
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v0, "WifiSsidTokenObserver: stopped listening "

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v0, p0, Lcom/google/android/location/copresence/r/an;->f:Z

    if-eqz v0, :cond_3

    const-string v0, "(active)"

    :goto_1
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->b(Ljava/lang/String;)I

    .line 187
    :cond_1
    if-eqz p1, :cond_2

    .line 188
    invoke-interface {p1}, Lcom/google/android/location/copresence/q/al;->a()V

    .line 190
    :cond_2
    return-void

    .line 178
    :catch_0
    move-exception v0

    const/4 v0, 0x5

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 179
    const-string v0, "Unregistered wifi broadcast receiver when it wasn\'t registered."

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->d(Ljava/lang/String;)I

    goto :goto_0

    .line 183
    :cond_3
    const-string v0, "(passive)"

    goto :goto_1
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 131
    const/4 v0, 0x1

    return v0
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 136
    iget-boolean v1, p0, Lcom/google/android/location/copresence/r/an;->f:Z

    if-eqz v1, :cond_3

    .line 137
    invoke-static {}, Lcom/google/android/location/copresence/f/b;->b()Lcom/google/ac/b/c/g;

    move-result-object v1

    iget-object v1, v1, Lcom/google/ac/b/c/g;->e:Lcom/google/ac/b/c/m;

    iget-object v1, v1, Lcom/google/ac/b/c/m;->n:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/android/location/copresence/r/an;->d:Lcom/google/android/location/copresence/r/ae;

    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x12

    if-lt v2, v3, :cond_2

    iget-object v1, v1, Lcom/google/android/location/copresence/r/ae;->a:Landroid/net/wifi/WifiManager;

    invoke-virtual {v1}, Landroid/net/wifi/WifiManager;->isScanAlwaysAvailable()Z

    move-result v1

    :goto_0
    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/location/copresence/r/an;->d:Lcom/google/android/location/copresence/r/ae;

    iget-object v1, v1, Lcom/google/android/location/copresence/r/ae;->a:Landroid/net/wifi/WifiManager;

    invoke-virtual {v1}, Landroid/net/wifi/WifiManager;->isWifiEnabled()Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    .line 140
    :cond_1
    :goto_1
    return v0

    :cond_2
    move v1, v0

    .line 137
    goto :goto_0

    .line 140
    :cond_3
    invoke-static {}, Lcom/google/android/location/copresence/f/b;->b()Lcom/google/ac/b/c/g;

    move-result-object v0

    iget-object v0, v0, Lcom/google/ac/b/c/g;->e:Lcom/google/ac/b/c/m;

    iget-object v0, v0, Lcom/google/ac/b/c/m;->o:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    goto :goto_1
.end method

.method public final c()V
    .locals 0

    .prologue
    .line 195
    return-void
.end method
