.class public final Lcom/google/android/location/reporting/service/r;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Lcom/google/android/location/reporting/config/ConfigGetter;

.field private final c:Lcom/google/android/location/reporting/e;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/location/reporting/config/ConfigGetter;Lcom/google/android/location/reporting/e;)V
    .locals 0

    .prologue
    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    iput-object p1, p0, Lcom/google/android/location/reporting/service/r;->a:Landroid/content/Context;

    .line 39
    iput-object p2, p0, Lcom/google/android/location/reporting/service/r;->b:Lcom/google/android/location/reporting/config/ConfigGetter;

    .line 40
    iput-object p3, p0, Lcom/google/android/location/reporting/service/r;->c:Lcom/google/android/location/reporting/e;

    .line 41
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;Landroid/accounts/Account;Lcom/google/android/gms/location/places/PlaceReport;J)I
    .locals 8

    .prologue
    .line 48
    const-string v0, "GCoreUlr"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/location/reporting/b/d;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 49
    const-string v0, "GCoreUlr"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "reportPlace("

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p4, p5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/location/reporting/b/d;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 54
    :cond_0
    iget-object v0, p0, Lcom/google/android/location/reporting/service/r;->b:Lcom/google/android/location/reporting/config/ConfigGetter;

    invoke-interface {v0, p2}, Lcom/google/android/location/reporting/config/ConfigGetter;->a(Landroid/accounts/Account;)Lcom/google/android/location/reporting/config/AccountConfig;

    move-result-object v7

    .line 55
    iget-object v0, p0, Lcom/google/android/location/reporting/service/r;->a:Landroid/content/Context;

    invoke-static {v0, v7}, Lcom/google/android/location/reporting/service/InitializerService;->a(Landroid/content/Context;Lcom/google/android/location/reporting/config/AccountConfig;)I

    .line 56
    invoke-virtual {v7}, Lcom/google/android/location/reporting/config/AccountConfig;->z()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {v7}, Lcom/google/android/location/reporting/config/AccountConfig;->h()Z

    move-result v0

    if-nez v0, :cond_3

    .line 57
    :cond_1
    const-string v0, "GCoreUlr"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/location/reporting/b/d;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 58
    const-string v0, "GCoreUlr"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " for inactive account "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/location/reporting/b/d;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 60
    :cond_2
    const/4 v0, 0x1

    .line 87
    :goto_0
    return v0

    .line 64
    :cond_3
    invoke-virtual {p3}, Lcom/google/android/gms/location/places/PlaceReport;->c()Ljava/lang/String;

    move-result-object v0

    new-instance v4, Lcom/google/android/ulr/ApiPlaceReport;

    invoke-virtual {p3}, Lcom/google/android/gms/location/places/PlaceReport;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p3}, Lcom/google/android/gms/location/places/PlaceReport;->a()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v4, v1, p1, v2, v0}, Lcom/google/android/ulr/ApiPlaceReport;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    new-instance v0, Lcom/google/android/ulr/ApiMetadata;

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v5, 0x0

    invoke-static {p4, p5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-direct/range {v0 .. v6}, Lcom/google/android/ulr/ApiMetadata;-><init>(Lcom/google/android/ulr/ApiActivationChange;Lcom/google/android/ulr/ApiBleScanReport;Lcom/google/android/ulr/ApiLocationStatus;Lcom/google/android/ulr/ApiPlaceReport;Lcom/google/android/ulr/ApiRate;Ljava/lang/Long;)V

    .line 65
    const/4 v1, 0x1

    invoke-static {v1}, Lcom/google/android/location/reporting/b/l;->a(I)V

    .line 67
    :try_start_0
    iget-object v1, p0, Lcom/google/android/location/reporting/service/r;->c:Lcom/google/android/location/reporting/e;

    invoke-virtual {v7}, Lcom/google/android/location/reporting/config/AccountConfig;->h()Z

    move-result v2

    invoke-virtual {v1, p2, v0, v2}, Lcom/google/android/location/reporting/e;->a(Landroid/accounts/Account;Ljava/lang/Object;Z)Z

    move-result v0

    if-nez v0, :cond_6

    .line 68
    const-string v0, "GCoreUlr"

    const/4 v1, 0x5

    invoke-static {v0, v1}, Lcom/google/android/location/reporting/b/d;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 69
    const-string v0, "GCoreUlr"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Error saving ApiPlaceReport to DB for "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {p2}, Lcom/google/android/gms/location/reporting/a/d;->a(Landroid/accounts/Account;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/location/reporting/b/d;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Lcom/google/android/location/reporting/f; {:try_start_0 .. :try_end_0} :catch_0

    .line 72
    :cond_4
    const/4 v0, 0x2

    goto :goto_0

    .line 74
    :catch_0
    move-exception v0

    .line 75
    const-string v1, "GCoreUlr"

    const/4 v2, 0x5

    invoke-static {v1, v2}, Lcom/google/android/location/reporting/b/d;->a(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 76
    const-string v1, "GCoreUlr"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Error saving ApiPlaceReport to DB for "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {p2}, Lcom/google/android/gms/location/reporting/a/d;->a(Landroid/accounts/Account;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lcom/google/android/location/reporting/b/d;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 79
    :cond_5
    const/4 v0, 0x2

    goto :goto_0

    .line 83
    :cond_6
    iget-object v0, p0, Lcom/google/android/location/reporting/service/r;->a:Landroid/content/Context;

    const-string v1, "com.google.android.location.reporting.ACTION_RESET_WIFI_ATTACHMENT"

    invoke-static {v0, v1}, Lcom/google/android/location/reporting/service/DispatchingService;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 85
    iget-object v1, p0, Lcom/google/android/location/reporting/service/r;->a:Landroid/content/Context;

    invoke-static {v1, v0}, Lcom/google/android/location/reporting/b/n;->a(Landroid/content/Context;Landroid/content/Intent;)Z

    .line 87
    const/4 v0, 0x0

    goto/16 :goto_0
.end method
