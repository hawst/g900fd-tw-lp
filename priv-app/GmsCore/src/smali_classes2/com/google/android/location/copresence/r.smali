.class public final Lcom/google/android/location/copresence/r;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/location/copresence/a/g;
.implements Lcom/google/android/location/copresence/az;
.implements Lcom/google/android/location/copresence/e/b;
.implements Lcom/google/android/location/copresence/n/i;


# static fields
.field private static k:Lcom/google/android/location/copresence/r;


# instance fields
.field public final a:Lcom/google/android/location/copresence/ap;

.field public final b:Lcom/google/android/location/copresence/j;

.field public final c:Lcom/google/android/location/copresence/e/a;

.field final d:Lcom/google/android/location/copresence/e/d;

.field public final e:Lcom/google/android/location/copresence/debug/e;

.field private final f:Landroid/content/Context;

.field private final g:Lcom/google/android/location/copresence/o/k;

.field private final h:Lcom/google/android/location/copresence/a/b;

.field private final i:Lcom/google/android/location/copresence/n/f;

.field private final j:Lcom/google/android/location/copresence/l/m;

.field private l:Ljava/lang/Boolean;


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .locals 12

    .prologue
    .line 82
    invoke-static {p1}, Lcom/google/android/location/copresence/ap;->a(Landroid/content/Context;)Lcom/google/android/location/copresence/ap;

    move-result-object v2

    invoke-static {p1}, Lcom/google/android/location/copresence/j;->a(Landroid/content/Context;)Lcom/google/android/location/copresence/j;

    move-result-object v3

    invoke-static {p1}, Lcom/google/android/location/copresence/o/k;->a(Landroid/content/Context;)Lcom/google/android/location/copresence/o/k;

    move-result-object v4

    invoke-static {p1}, Lcom/google/android/location/copresence/a/b;->a(Landroid/content/Context;)Lcom/google/android/location/copresence/a/b;

    move-result-object v5

    invoke-static {}, Lcom/google/android/location/copresence/e/a;->a()Lcom/google/android/location/copresence/e/a;

    move-result-object v6

    invoke-static {p1}, Lcom/google/android/location/copresence/e/d;->a(Landroid/content/Context;)Lcom/google/android/location/copresence/e/d;

    move-result-object v7

    invoke-static {p1}, Lcom/google/android/location/copresence/n/f;->a(Landroid/content/Context;)Lcom/google/android/location/copresence/n/f;

    move-result-object v8

    invoke-static {p1}, Lcom/google/android/location/copresence/ay;->a(Landroid/content/Context;)Lcom/google/android/location/copresence/ay;

    move-result-object v9

    invoke-static {p1}, Lcom/google/android/location/copresence/l/m;->a(Landroid/content/Context;)Lcom/google/android/location/copresence/l/m;

    move-result-object v10

    invoke-static {}, Lcom/google/android/location/copresence/debug/e;->a()Lcom/google/android/location/copresence/debug/e;

    move-result-object v11

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v11}, Lcom/google/android/location/copresence/r;-><init>(Landroid/content/Context;Lcom/google/android/location/copresence/ap;Lcom/google/android/location/copresence/j;Lcom/google/android/location/copresence/o/k;Lcom/google/android/location/copresence/a/b;Lcom/google/android/location/copresence/e/a;Lcom/google/android/location/copresence/e/d;Lcom/google/android/location/copresence/n/f;Lcom/google/android/location/copresence/ay;Lcom/google/android/location/copresence/l/m;Lcom/google/android/location/copresence/debug/e;)V

    .line 94
    const/4 v0, 0x3

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 95
    const-string v0, "CopresenceHelper: created!"

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->b(Ljava/lang/String;)I

    .line 100
    :cond_0
    invoke-static {p1}, Lcom/google/android/location/copresence/l/j;->a(Landroid/content/Context;)Lcom/google/android/location/copresence/l/j;

    .line 101
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Lcom/google/android/location/copresence/ap;Lcom/google/android/location/copresence/j;Lcom/google/android/location/copresence/o/k;Lcom/google/android/location/copresence/a/b;Lcom/google/android/location/copresence/e/a;Lcom/google/android/location/copresence/e/d;Lcom/google/android/location/copresence/n/f;Lcom/google/android/location/copresence/ay;Lcom/google/android/location/copresence/l/m;Lcom/google/android/location/copresence/debug/e;)V
    .locals 1

    .prologue
    .line 115
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 116
    iput-object p1, p0, Lcom/google/android/location/copresence/r;->f:Landroid/content/Context;

    .line 117
    iput-object p2, p0, Lcom/google/android/location/copresence/r;->a:Lcom/google/android/location/copresence/ap;

    .line 118
    iput-object p3, p0, Lcom/google/android/location/copresence/r;->b:Lcom/google/android/location/copresence/j;

    .line 119
    iput-object p4, p0, Lcom/google/android/location/copresence/r;->g:Lcom/google/android/location/copresence/o/k;

    .line 120
    iput-object p5, p0, Lcom/google/android/location/copresence/r;->h:Lcom/google/android/location/copresence/a/b;

    .line 121
    iput-object p6, p0, Lcom/google/android/location/copresence/r;->c:Lcom/google/android/location/copresence/e/a;

    .line 122
    iput-object p7, p0, Lcom/google/android/location/copresence/r;->d:Lcom/google/android/location/copresence/e/d;

    .line 123
    iput-object p8, p0, Lcom/google/android/location/copresence/r;->i:Lcom/google/android/location/copresence/n/f;

    .line 124
    iput-object p10, p0, Lcom/google/android/location/copresence/r;->j:Lcom/google/android/location/copresence/l/m;

    .line 125
    iput-object p11, p0, Lcom/google/android/location/copresence/r;->e:Lcom/google/android/location/copresence/debug/e;

    .line 127
    iget-object v0, p0, Lcom/google/android/location/copresence/r;->h:Lcom/google/android/location/copresence/a/b;

    if-eqz v0, :cond_0

    .line 128
    iget-object v0, p0, Lcom/google/android/location/copresence/r;->h:Lcom/google/android/location/copresence/a/b;

    invoke-virtual {v0, p0}, Lcom/google/android/location/copresence/a/b;->a(Lcom/google/android/location/copresence/a/g;)V

    .line 130
    :cond_0
    iget-object v0, p0, Lcom/google/android/location/copresence/r;->c:Lcom/google/android/location/copresence/e/a;

    if-eqz v0, :cond_1

    .line 131
    iget-object v0, p0, Lcom/google/android/location/copresence/r;->c:Lcom/google/android/location/copresence/e/a;

    invoke-virtual {v0, p0}, Lcom/google/android/location/copresence/e/a;->a(Lcom/google/android/location/copresence/e/b;)V

    .line 133
    :cond_1
    iget-object v0, p0, Lcom/google/android/location/copresence/r;->i:Lcom/google/android/location/copresence/n/f;

    if-eqz v0, :cond_2

    .line 134
    iget-object v0, p0, Lcom/google/android/location/copresence/r;->i:Lcom/google/android/location/copresence/n/f;

    invoke-virtual {v0, p0}, Lcom/google/android/location/copresence/n/f;->a(Lcom/google/android/location/copresence/n/i;)V

    .line 136
    :cond_2
    if-eqz p9, :cond_3

    .line 137
    invoke-virtual {p9, p0}, Lcom/google/android/location/copresence/ay;->a(Lcom/google/android/location/copresence/az;)V

    .line 139
    :cond_3
    return-void
.end method

.method public static declared-synchronized a(Landroid/content/Context;)Lcom/google/android/location/copresence/r;
    .locals 2

    .prologue
    .line 75
    const-class v1, Lcom/google/android/location/copresence/r;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/google/android/location/copresence/r;->k:Lcom/google/android/location/copresence/r;

    if-nez v0, :cond_0

    .line 76
    new-instance v0, Lcom/google/android/location/copresence/r;

    invoke-direct {v0, p0}, Lcom/google/android/location/copresence/r;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/google/android/location/copresence/r;->k:Lcom/google/android/location/copresence/r;

    .line 78
    :cond_0
    sget-object v0, Lcom/google/android/location/copresence/r;->k:Lcom/google/android/location/copresence/r;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 75
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static a(Lcom/google/android/gms/location/copresence/internal/i;I)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x2

    .line 518
    new-instance v0, Lcom/google/android/gms/common/api/Status;

    invoke-direct {v0, p1, v2, v2}, Lcom/google/android/gms/common/api/Status;-><init>(ILjava/lang/String;Landroid/app/PendingIntent;)V

    if-nez p0, :cond_1

    invoke-static {v1}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v1

    if-eqz v1, :cond_0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Callback is null. Could not send statusCode:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->a(Ljava/lang/String;)I

    .line 519
    :cond_0
    :goto_0
    return-void

    .line 518
    :cond_1
    :try_start_0
    invoke-interface {p0, v0}, Lcom/google/android/gms/location/copresence/internal/i;->a(Lcom/google/android/gms/common/api/Status;)V

    const/4 v1, 0x2

    invoke-static {v1}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v1

    if-eqz v1, :cond_0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Sent callback message statusCode:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->a(Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    const/4 v1, 0x5

    invoke-static {v1}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v1

    if-eqz v1, :cond_0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Failed to send callback message"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->d(Ljava/lang/String;)I

    goto :goto_0
.end method

.method public static a(Lcom/google/android/gms/location/copresence/internal/i;ILcom/google/android/gms/location/copresence/CopresenceSettings;)V
    .locals 1

    .prologue
    .line 568
    if-eqz p0, :cond_0

    .line 570
    :try_start_0
    invoke-interface {p0, p1, p2}, Lcom/google/android/gms/location/copresence/internal/i;->a(ILcom/google/android/gms/location/copresence/CopresenceSettings;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 575
    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;)V
    .locals 4

    .prologue
    .line 373
    invoke-virtual {p3}, Ljava/util/ArrayList;->listIterator()Ljava/util/ListIterator;

    move-result-object v1

    .line 374
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/ListIterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 375
    invoke-interface {v1}, Ljava/util/ListIterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/location/copresence/internal/Operation;

    .line 376
    iget v2, v0, Lcom/google/android/gms/location/copresence/internal/Operation;->a:I

    const/4 v3, 0x3

    if-ne v2, v3, :cond_0

    .line 377
    iget-object v0, v0, Lcom/google/android/gms/location/copresence/internal/Operation;->d:Lcom/google/android/gms/location/copresence/internal/SubscribeOperation;

    .line 378
    iget-object v2, p0, Lcom/google/android/location/copresence/r;->d:Lcom/google/android/location/copresence/e/d;

    invoke-virtual {v2, p1, p2, v0}, Lcom/google/android/location/copresence/e/d;->a(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/location/copresence/internal/SubscribeOperation;)Lcom/google/android/location/copresence/e/f;

    move-result-object v2

    .line 380
    if-eqz v2, :cond_0

    .line 382
    invoke-static {v0}, Lcom/google/android/gms/location/copresence/internal/UnsubscribeOperation;->a(Lcom/google/android/gms/location/copresence/internal/SubscribeOperation;)Lcom/google/android/gms/location/copresence/internal/UnsubscribeOperation;

    move-result-object v0

    .line 383
    if-eqz v0, :cond_0

    .line 388
    invoke-interface {v1}, Ljava/util/ListIterator;->previous()Ljava/lang/Object;

    .line 390
    invoke-static {v0}, Lcom/google/android/gms/location/copresence/internal/Operation;->a(Lcom/google/android/gms/location/copresence/internal/UnsubscribeOperation;)Lcom/google/android/gms/location/copresence/internal/Operation;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/ListIterator;->add(Ljava/lang/Object;)V

    .line 392
    invoke-interface {v1}, Ljava/util/ListIterator;->next()Ljava/lang/Object;

    goto :goto_0

    .line 397
    :cond_1
    return-void
.end method

.method private static a(Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/ArrayList;)V
    .locals 3

    .prologue
    .line 242
    invoke-virtual {p0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/location/copresence/internal/Operation;

    .line 243
    iget v2, v0, Lcom/google/android/gms/location/copresence/internal/Operation;->a:I

    packed-switch v2, :pswitch_data_0

    goto :goto_0

    .line 245
    :pswitch_0
    iget-object v0, v0, Lcom/google/android/gms/location/copresence/internal/Operation;->d:Lcom/google/android/gms/location/copresence/internal/SubscribeOperation;

    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 248
    :pswitch_1
    iget-object v0, v0, Lcom/google/android/gms/location/copresence/internal/Operation;->e:Lcom/google/android/gms/location/copresence/internal/UnsubscribeOperation;

    invoke-virtual {p2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 255
    :cond_0
    return-void

    .line 243
    nop

    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private c(Lcom/google/android/location/copresence/a/a;)V
    .locals 4

    .prologue
    .line 355
    iget-object v0, p0, Lcom/google/android/location/copresence/r;->g:Lcom/google/android/location/copresence/o/k;

    iget-object v1, p1, Lcom/google/android/location/copresence/a/a;->a:Landroid/accounts/Account;

    iget-object v1, v1, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/location/copresence/o/k;->a(Ljava/lang/String;)Ljava/util/Set;

    move-result-object v0

    .line 358
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 359
    invoke-static {}, Lcom/google/android/gms/location/copresence/internal/UnpublishOperation;->a()Lcom/google/android/gms/location/copresence/internal/UnpublishOperation;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/gms/location/copresence/internal/Operation;->a(Lcom/google/android/gms/location/copresence/internal/UnpublishOperation;)Lcom/google/android/gms/location/copresence/internal/Operation;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 360
    invoke-static {}, Lcom/google/android/gms/location/copresence/internal/UnsubscribeOperation;->a()Lcom/google/android/gms/location/copresence/internal/UnsubscribeOperation;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/gms/location/copresence/internal/Operation;->a(Lcom/google/android/gms/location/copresence/internal/UnsubscribeOperation;)Lcom/google/android/gms/location/copresence/internal/Operation;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 362
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 363
    const/4 v3, 0x0

    invoke-virtual {p0, v0, p1, v1, v3}, Lcom/google/android/location/copresence/r;->a(Ljava/lang/String;Lcom/google/android/location/copresence/a/a;Ljava/util/ArrayList;Lcom/google/android/gms/location/copresence/internal/i;)V

    goto :goto_0

    .line 365
    :cond_0
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 198
    iget-object v0, p0, Lcom/google/android/location/copresence/r;->j:Lcom/google/android/location/copresence/l/m;

    if-eqz v0, :cond_0

    .line 199
    iget-object v0, p0, Lcom/google/android/location/copresence/r;->j:Lcom/google/android/location/copresence/l/m;

    invoke-virtual {v0}, Lcom/google/android/location/copresence/l/m;->a()V

    .line 201
    :cond_0
    return-void
.end method

.method public final a(Lcom/google/android/location/copresence/a/a;)V
    .locals 0

    .prologue
    .line 309
    invoke-direct {p0, p1}, Lcom/google/android/location/copresence/r;->c(Lcom/google/android/location/copresence/a/a;)V

    .line 310
    invoke-virtual {p0}, Lcom/google/android/location/copresence/r;->a()V

    .line 311
    return-void
.end method

.method public final a(Lcom/google/android/location/copresence/a/a;Z)V
    .locals 1

    .prologue
    .line 342
    iget-object v0, p0, Lcom/google/android/location/copresence/r;->l:Ljava/lang/Boolean;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/location/copresence/r;->l:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-ne v0, p2, :cond_1

    .line 352
    :cond_0
    :goto_0
    return-void

    .line 345
    :cond_1
    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/copresence/r;->l:Ljava/lang/Boolean;

    .line 346
    if-nez p2, :cond_0

    .line 349
    invoke-direct {p0, p1}, Lcom/google/android/location/copresence/r;->c(Lcom/google/android/location/copresence/a/a;)V

    .line 350
    invoke-virtual {p0}, Lcom/google/android/location/copresence/r;->a()V

    goto :goto_0
.end method

.method final a(Ljava/lang/String;)V
    .locals 5

    .prologue
    .line 260
    iget-object v0, p0, Lcom/google/android/location/copresence/r;->a:Lcom/google/android/location/copresence/ap;

    invoke-virtual {v0}, Lcom/google/android/location/copresence/ap;->c()V

    .line 261
    const/4 v0, 0x3

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 262
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "CopresenceHelper: Client app died: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->b(Ljava/lang/String;)I

    .line 266
    :cond_0
    iget-object v0, p0, Lcom/google/android/location/copresence/r;->g:Lcom/google/android/location/copresence/o/k;

    iget-object v1, v0, Lcom/google/android/location/copresence/o/k;->b:Lcom/google/android/location/copresence/ap;

    invoke-virtual {v1}, Lcom/google/android/location/copresence/ap;->c()V

    iget-object v0, v0, Lcom/google/android/location/copresence/o/k;->a:Lcom/google/android/location/copresence/o/d;

    invoke-virtual {v0, p1}, Lcom/google/android/location/copresence/o/d;->a(Ljava/lang/String;)Ljava/util/Set;

    move-result-object v0

    .line 268
    if-eqz v0, :cond_1

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 287
    :cond_1
    return-void

    .line 273
    :cond_2
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 274
    invoke-static {}, Lcom/google/android/gms/location/copresence/internal/UnpublishOperation;->a()Lcom/google/android/gms/location/copresence/internal/UnpublishOperation;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/gms/location/copresence/internal/Operation;->a(Lcom/google/android/gms/location/copresence/internal/UnpublishOperation;)Lcom/google/android/gms/location/copresence/internal/Operation;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 275
    invoke-static {}, Lcom/google/android/gms/location/copresence/internal/UnsubscribeOperation;->a()Lcom/google/android/gms/location/copresence/internal/UnsubscribeOperation;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/gms/location/copresence/internal/Operation;->a(Lcom/google/android/gms/location/copresence/internal/UnsubscribeOperation;)Lcom/google/android/gms/location/copresence/internal/Operation;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 277
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_3
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 278
    invoke-virtual {p0, v0}, Lcom/google/android/location/copresence/r;->c(Ljava/lang/String;)Lcom/google/android/location/copresence/a/a;

    move-result-object v3

    .line 279
    if-eqz v3, :cond_4

    .line 280
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v3, v1, v0}, Lcom/google/android/location/copresence/r;->a(Ljava/lang/String;Lcom/google/android/location/copresence/a/a;Ljava/util/ArrayList;Lcom/google/android/gms/location/copresence/internal/i;)V

    goto :goto_0

    .line 282
    :cond_4
    const/4 v3, 0x6

    invoke-static {v3}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 283
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "CopresenceHelper: account is invalid: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->f(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->e(Ljava/lang/String;)I

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Lcom/google/android/location/copresence/a/a;Ljava/util/ArrayList;Lcom/google/android/gms/location/copresence/internal/i;)V
    .locals 4

    .prologue
    .line 208
    iget-object v0, p0, Lcom/google/android/location/copresence/r;->a:Lcom/google/android/location/copresence/ap;

    invoke-virtual {v0}, Lcom/google/android/location/copresence/ap;->c()V

    .line 209
    invoke-static {p1}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 210
    invoke-static {p2}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 211
    invoke-static {p3}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 213
    iget-object v0, p2, Lcom/google/android/location/copresence/a/a;->a:Landroid/accounts/Account;

    iget-object v0, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    .line 214
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 215
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 216
    invoke-static {p3, v1, v2}, Lcom/google/android/location/copresence/r;->a(Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/ArrayList;)V

    .line 220
    invoke-direct {p0, p1, v0, p3}, Lcom/google/android/location/copresence/r;->a(Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 224
    iget-object v3, p0, Lcom/google/android/location/copresence/r;->d:Lcom/google/android/location/copresence/e/d;

    invoke-virtual {v3, p1, v0, v1, v2}, Lcom/google/android/location/copresence/e/d;->a(Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;Ljava/util/ArrayList;)Z

    move-result v0

    .line 226
    if-eqz v0, :cond_1

    .line 227
    if-eqz p4, :cond_0

    .line 228
    const/16 v0, 0x9c4

    invoke-static {p4, v0}, Lcom/google/android/location/copresence/r;->a(Lcom/google/android/gms/location/copresence/internal/i;I)V

    .line 236
    :cond_0
    :goto_0
    return-void

    .line 234
    :cond_1
    iget-object v0, p0, Lcom/google/android/location/copresence/r;->g:Lcom/google/android/location/copresence/o/k;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/google/android/location/copresence/o/k;->a(Ljava/lang/String;Lcom/google/android/location/copresence/a/a;Ljava/util/List;Lcom/google/android/gms/location/copresence/internal/i;)Z

    goto :goto_0
.end method

.method public final a(Ljava/util/Collection;)V
    .locals 2

    .prologue
    .line 316
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 317
    invoke-virtual {p0, v0}, Lcom/google/android/location/copresence/r;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 319
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/location/copresence/r;->a()V

    .line 320
    return-void
.end method

.method public final b(Lcom/google/android/location/copresence/a/a;)V
    .locals 0

    .prologue
    .line 304
    return-void
.end method

.method public final b(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 292
    iget-object v0, p0, Lcom/google/android/location/copresence/r;->a:Lcom/google/android/location/copresence/ap;

    new-instance v1, Lcom/google/android/location/copresence/s;

    invoke-direct {v1, p0, p1}, Lcom/google/android/location/copresence/s;-><init>(Lcom/google/android/location/copresence/r;Ljava/lang/String;)V

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/google/android/location/copresence/ap;->a(Ljava/lang/Runnable;Landroid/os/WorkSource;)V

    .line 299
    return-void
.end method

.method public final b(Ljava/util/Collection;)V
    .locals 2

    .prologue
    .line 325
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 326
    invoke-virtual {p0, v0}, Lcom/google/android/location/copresence/r;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 328
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/location/copresence/r;->a()V

    .line 329
    return-void
.end method

.method public final c(Ljava/lang/String;)Lcom/google/android/location/copresence/a/a;
    .locals 1

    .prologue
    .line 514
    iget-object v0, p0, Lcom/google/android/location/copresence/r;->h:Lcom/google/android/location/copresence/a/b;

    invoke-virtual {v0, p1}, Lcom/google/android/location/copresence/a/b;->a(Ljava/lang/String;)Lcom/google/android/location/copresence/a/a;

    move-result-object v0

    return-object v0
.end method

.method public final c(Ljava/util/Collection;)V
    .locals 2

    .prologue
    .line 334
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 335
    invoke-virtual {p0, v0}, Lcom/google/android/location/copresence/r;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 337
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/location/copresence/r;->a()V

    .line 338
    return-void
.end method
