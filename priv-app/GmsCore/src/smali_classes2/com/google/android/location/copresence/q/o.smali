.class final Lcom/google/android/location/copresence/q/o;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/location/copresence/q/l;
.implements Lcom/google/android/location/copresence/q/m;
.implements Ljava/lang/Runnable;


# instance fields
.field final a:Lcom/google/android/location/copresence/q/n;

.field final synthetic b:Lcom/google/android/location/copresence/q/g;

.field private final c:Lcom/google/android/location/copresence/q/j;

.field private final d:Ljava/util/HashSet;

.field private e:Z

.field private f:I

.field private g:J

.field private final h:Ljava/lang/Runnable;

.field private final i:Ljava/lang/Runnable;


# direct methods
.method public constructor <init>(Lcom/google/android/location/copresence/q/g;Lcom/google/android/location/copresence/q/n;Lcom/google/android/location/copresence/q/j;[Lcom/google/android/location/copresence/q/n;)V
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 272
    iput-object p1, p0, Lcom/google/android/location/copresence/q/o;->b:Lcom/google/android/location/copresence/q/g;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 394
    new-instance v0, Lcom/google/android/location/copresence/q/p;

    invoke-direct {v0, p0}, Lcom/google/android/location/copresence/q/p;-><init>(Lcom/google/android/location/copresence/q/o;)V

    iput-object v0, p0, Lcom/google/android/location/copresence/q/o;->h:Ljava/lang/Runnable;

    .line 411
    new-instance v0, Lcom/google/android/location/copresence/q/q;

    invoke-direct {v0, p0}, Lcom/google/android/location/copresence/q/q;-><init>(Lcom/google/android/location/copresence/q/o;)V

    iput-object v0, p0, Lcom/google/android/location/copresence/q/o;->i:Ljava/lang/Runnable;

    .line 273
    invoke-static {p2}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/copresence/q/n;

    iput-object v0, p0, Lcom/google/android/location/copresence/q/o;->a:Lcom/google/android/location/copresence/q/n;

    .line 274
    invoke-static {p3}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/copresence/q/j;

    iput-object v0, p0, Lcom/google/android/location/copresence/q/o;->c:Lcom/google/android/location/copresence/q/j;

    .line 275
    if-eqz p4, :cond_0

    array-length v0, p4

    if-nez v0, :cond_2

    .line 276
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/location/copresence/q/o;->d:Ljava/util/HashSet;

    .line 283
    :cond_1
    iput-boolean v1, p0, Lcom/google/android/location/copresence/q/o;->e:Z

    .line 284
    invoke-direct {p0, v1}, Lcom/google/android/location/copresence/q/o;->a(I)V

    .line 285
    return-void

    .line 278
    :cond_2
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/location/copresence/q/o;->d:Ljava/util/HashSet;

    .line 279
    array-length v2, p4

    move v0, v1

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, p4, v0

    .line 280
    iget-object v4, p0, Lcom/google/android/location/copresence/q/o;->d:Ljava/util/HashSet;

    invoke-virtual {v4, v3}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 279
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method private a(I)V
    .locals 3

    .prologue
    const/4 v2, 0x6

    .line 451
    :goto_0
    iput p1, p0, Lcom/google/android/location/copresence/q/o;->f:I

    .line 452
    packed-switch p1, :pswitch_data_0

    .line 489
    invoke-static {v2}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 490
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Unknown state "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/IllegalArgumentException;

    invoke-direct {v1}, Ljava/lang/IllegalArgumentException;-><init>()V

    invoke-static {v0, v1}, Lcom/google/android/location/copresence/ag;->b(Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 492
    :cond_0
    const/4 p1, 0x0

    goto :goto_0

    .line 454
    :pswitch_0
    iget-object v0, p0, Lcom/google/android/location/copresence/q/o;->b:Lcom/google/android/location/copresence/q/g;

    invoke-static {v0}, Lcom/google/android/location/copresence/q/g;->a(Lcom/google/android/location/copresence/q/g;)Ljava/util/HashSet;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/location/copresence/q/o;->a:Lcom/google/android/location/copresence/q/n;

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    .line 457
    iget-object v0, p0, Lcom/google/android/location/copresence/q/o;->b:Lcom/google/android/location/copresence/q/g;

    invoke-static {v0}, Lcom/google/android/location/copresence/q/g;->d(Lcom/google/android/location/copresence/q/g;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/location/copresence/q/o;->b:Lcom/google/android/location/copresence/q/g;

    invoke-static {v1}, Lcom/google/android/location/copresence/q/g;->c(Lcom/google/android/location/copresence/q/g;)Ljava/lang/Runnable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 495
    :cond_1
    :goto_1
    return-void

    .line 460
    :pswitch_1
    iget-object v0, p0, Lcom/google/android/location/copresence/q/o;->b:Lcom/google/android/location/copresence/q/g;

    invoke-static {v0}, Lcom/google/android/location/copresence/q/g;->a(Lcom/google/android/location/copresence/q/g;)Ljava/util/HashSet;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/location/copresence/q/o;->a:Lcom/google/android/location/copresence/q/n;

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 461
    invoke-static {v2}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 462
    const-string v0, "Should not be in blocking when transitioning to WARMUP"

    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1}, Ljava/lang/IllegalStateException;-><init>()V

    invoke-static {v0, v1}, Lcom/google/android/location/copresence/ag;->b(Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 466
    :cond_2
    iget-object v0, p0, Lcom/google/android/location/copresence/q/o;->b:Lcom/google/android/location/copresence/q/g;

    invoke-static {v0}, Lcom/google/android/location/copresence/q/g;->a(Lcom/google/android/location/copresence/q/g;)Ljava/util/HashSet;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/location/copresence/q/o;->a:Lcom/google/android/location/copresence/q/n;

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 469
    :pswitch_2
    iget-object v0, p0, Lcom/google/android/location/copresence/q/o;->b:Lcom/google/android/location/copresence/q/g;

    invoke-static {v0}, Lcom/google/android/location/copresence/q/g;->a(Lcom/google/android/location/copresence/q/g;)Ljava/util/HashSet;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/location/copresence/q/o;->a:Lcom/google/android/location/copresence/q/n;

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 470
    invoke-static {v2}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 471
    const-string v0, "Should already be in blocking when transitioning to ACTIVE."

    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1}, Ljava/lang/IllegalStateException;-><init>()V

    invoke-static {v0, v1}, Lcom/google/android/location/copresence/ag;->b(Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1

    .line 475
    :cond_3
    iget-object v0, p0, Lcom/google/android/location/copresence/q/o;->b:Lcom/google/android/location/copresence/q/g;

    invoke-static {v0}, Lcom/google/android/location/copresence/q/g;->a(Lcom/google/android/location/copresence/q/g;)Ljava/util/HashSet;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/location/copresence/q/o;->a:Lcom/google/android/location/copresence/q/n;

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 479
    :pswitch_3
    iget-object v0, p0, Lcom/google/android/location/copresence/q/o;->b:Lcom/google/android/location/copresence/q/g;

    invoke-static {v0}, Lcom/google/android/location/copresence/q/g;->a(Lcom/google/android/location/copresence/q/g;)Ljava/util/HashSet;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/location/copresence/q/o;->a:Lcom/google/android/location/copresence/q/n;

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 480
    invoke-static {v2}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 481
    const-string v0, "Should already be in blocking when transitioning to COOLDOWN."

    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1}, Ljava/lang/IllegalStateException;-><init>()V

    invoke-static {v0, v1}, Lcom/google/android/location/copresence/ag;->b(Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1

    .line 485
    :cond_4
    iget-object v0, p0, Lcom/google/android/location/copresence/q/o;->b:Lcom/google/android/location/copresence/q/g;

    invoke-static {v0}, Lcom/google/android/location/copresence/q/g;->a(Lcom/google/android/location/copresence/q/g;)Ljava/util/HashSet;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/location/copresence/q/o;->a:Lcom/google/android/location/copresence/q/n;

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 452
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private a(J)V
    .locals 3

    .prologue
    .line 437
    const-wide/16 v0, 0x0

    cmp-long v0, p1, v0

    if-lez v0, :cond_0

    .line 438
    iget-object v0, p0, Lcom/google/android/location/copresence/q/o;->b:Lcom/google/android/location/copresence/q/g;

    invoke-static {v0, p0, p1, p2}, Lcom/google/android/location/copresence/q/g;->b(Lcom/google/android/location/copresence/q/g;Lcom/google/android/location/copresence/q/o;J)V

    .line 443
    :goto_0
    return-void

    .line 441
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/location/copresence/q/o;->run()V

    goto :goto_0
.end method

.method private a(Lcom/google/android/location/copresence/q/n;)Z
    .locals 1

    .prologue
    .line 315
    iget-object v0, p0, Lcom/google/android/location/copresence/q/o;->d:Ljava/util/HashSet;

    if-nez v0, :cond_0

    .line 316
    const/4 v0, 0x0

    .line 318
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/location/copresence/q/o;->d:Ljava/util/HashSet;

    invoke-virtual {v0, p1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method static synthetic a(Lcom/google/android/location/copresence/q/o;I)Z
    .locals 1

    .prologue
    .line 264
    iget-boolean v0, p0, Lcom/google/android/location/copresence/q/o;->e:Z

    if-nez v0, :cond_0

    iget v0, p0, Lcom/google/android/location/copresence/q/o;->f:I

    if-ne v0, p1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 433
    iget-object v0, p0, Lcom/google/android/location/copresence/q/o;->b:Lcom/google/android/location/copresence/q/g;

    invoke-static {v0}, Lcom/google/android/location/copresence/q/g;->d(Lcom/google/android/location/copresence/q/g;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/location/copresence/q/o;->i:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 434
    return-void
.end method

.method public final a(Ljava/util/HashSet;)Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 296
    invoke-virtual {p1}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/copresence/q/n;

    .line 297
    invoke-direct {p0, v0}, Lcom/google/android/location/copresence/q/o;->a(Lcom/google/android/location/copresence/q/n;)Z

    move-result v3

    if-eqz v3, :cond_1

    move v0, v1

    .line 311
    :goto_0
    return v0

    .line 300
    :cond_1
    iget-object v3, p0, Lcom/google/android/location/copresence/q/o;->b:Lcom/google/android/location/copresence/q/g;

    invoke-static {v3, v0}, Lcom/google/android/location/copresence/q/g;->a(Lcom/google/android/location/copresence/q/g;Lcom/google/android/location/copresence/q/n;)Lcom/google/android/location/copresence/q/o;

    move-result-object v0

    .line 301
    if-eqz v0, :cond_0

    .line 304
    iget-object v3, p0, Lcom/google/android/location/copresence/q/o;->a:Lcom/google/android/location/copresence/q/n;

    invoke-direct {v0, v3}, Lcom/google/android/location/copresence/q/o;->a(Lcom/google/android/location/copresence/q/n;)Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    .line 308
    goto :goto_0

    .line 311
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 408
    iget-object v0, p0, Lcom/google/android/location/copresence/q/o;->b:Lcom/google/android/location/copresence/q/g;

    invoke-static {v0}, Lcom/google/android/location/copresence/q/g;->d(Lcom/google/android/location/copresence/q/g;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/location/copresence/q/o;->h:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 409
    return-void
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 325
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/location/copresence/q/o;->e:Z

    .line 326
    invoke-virtual {p0}, Lcom/google/android/location/copresence/q/o;->run()V

    .line 327
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 336
    if-ne p0, p1, :cond_0

    .line 337
    const/4 v0, 0x1

    .line 343
    :goto_0
    return v0

    .line 339
    :cond_0
    if-eqz p1, :cond_1

    instance-of v0, p1, Lcom/google/android/location/copresence/q/o;

    if-nez v0, :cond_2

    .line 340
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 342
    :cond_2
    check-cast p1, Lcom/google/android/location/copresence/q/o;

    .line 343
    iget-object v0, p0, Lcom/google/android/location/copresence/q/o;->a:Lcom/google/android/location/copresence/q/n;

    iget-object v1, p1, Lcom/google/android/location/copresence/q/o;->a:Lcom/google/android/location/copresence/q/n;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 331
    iget-object v0, p0, Lcom/google/android/location/copresence/q/o;->a:Lcom/google/android/location/copresence/q/n;

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    return v0
.end method

.method public final run()V
    .locals 4

    .prologue
    const/4 v1, 0x6

    const/4 v2, 0x0

    .line 353
    iget-boolean v0, p0, Lcom/google/android/location/copresence/q/o;->e:Z

    if-eqz v0, :cond_1

    .line 354
    iget v0, p0, Lcom/google/android/location/copresence/q/o;->f:I

    packed-switch v0, :pswitch_data_0

    invoke-static {v1}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "DutyCycler: Unknown state in runFinishSafely."

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->e(Ljava/lang/String;)I

    .line 392
    :cond_0
    :goto_0
    return-void

    .line 354
    :pswitch_0
    invoke-direct {p0, v2}, Lcom/google/android/location/copresence/q/o;->a(I)V

    goto :goto_0

    :pswitch_1
    iget-object v0, p0, Lcom/google/android/location/copresence/q/o;->a:Lcom/google/android/location/copresence/q/n;

    invoke-static {}, Lcom/google/android/location/copresence/q/g;->a()Lcom/google/android/location/copresence/q/m;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/location/copresence/q/n;->a(Lcom/google/android/location/copresence/q/m;)V

    invoke-direct {p0, v2}, Lcom/google/android/location/copresence/q/o;->a(I)V

    goto :goto_0

    .line 358
    :cond_1
    iget v0, p0, Lcom/google/android/location/copresence/q/o;->f:I

    packed-switch v0, :pswitch_data_1

    .line 385
    invoke-static {v1}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 386
    const-string v0, "DutyCycler: Unknown state.  Going to wait state immediately."

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->e(Ljava/lang/String;)I

    .line 388
    :cond_2
    invoke-direct {p0, v2}, Lcom/google/android/location/copresence/q/o;->a(I)V

    .line 389
    iget-object v0, p0, Lcom/google/android/location/copresence/q/o;->c:Lcom/google/android/location/copresence/q/j;

    iget-wide v0, v0, Lcom/google/android/location/copresence/q/j;->a:J

    invoke-direct {p0, v0, v1}, Lcom/google/android/location/copresence/q/o;->a(J)V

    goto :goto_0

    .line 361
    :pswitch_2
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/location/copresence/q/o;->a(I)V

    .line 362
    iget-object v0, p0, Lcom/google/android/location/copresence/q/o;->b:Lcom/google/android/location/copresence/q/g;

    invoke-static {v0}, Lcom/google/android/location/copresence/q/g;->e(Lcom/google/android/location/copresence/q/g;)Lcom/google/android/gms/common/util/p;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/common/util/p;->b()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/location/copresence/q/o;->g:J

    .line 363
    iget-object v0, p0, Lcom/google/android/location/copresence/q/o;->a:Lcom/google/android/location/copresence/q/n;

    invoke-interface {v0, p0}, Lcom/google/android/location/copresence/q/n;->a(Lcom/google/android/location/copresence/q/l;)V

    goto :goto_0

    .line 368
    :pswitch_3
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/google/android/location/copresence/q/o;->a(I)V

    .line 369
    iget-object v0, p0, Lcom/google/android/location/copresence/q/o;->c:Lcom/google/android/location/copresence/q/j;

    iget-wide v0, v0, Lcom/google/android/location/copresence/q/j;->b:J

    invoke-direct {p0, v0, v1}, Lcom/google/android/location/copresence/q/o;->a(J)V

    goto :goto_0

    .line 372
    :pswitch_4
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/google/android/location/copresence/q/o;->a(I)V

    .line 373
    iget-object v0, p0, Lcom/google/android/location/copresence/q/o;->a:Lcom/google/android/location/copresence/q/n;

    invoke-interface {v0, p0}, Lcom/google/android/location/copresence/q/n;->a(Lcom/google/android/location/copresence/q/m;)V

    goto :goto_0

    .line 378
    :pswitch_5
    invoke-direct {p0, v2}, Lcom/google/android/location/copresence/q/o;->a(I)V

    .line 380
    iget-object v0, p0, Lcom/google/android/location/copresence/q/o;->c:Lcom/google/android/location/copresence/q/j;

    iget-wide v0, v0, Lcom/google/android/location/copresence/q/j;->a:J

    iget-object v2, p0, Lcom/google/android/location/copresence/q/o;->b:Lcom/google/android/location/copresence/q/g;

    invoke-static {v2}, Lcom/google/android/location/copresence/q/g;->e(Lcom/google/android/location/copresence/q/g;)Lcom/google/android/gms/common/util/p;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/gms/common/util/p;->b()J

    move-result-wide v2

    sub-long/2addr v0, v2

    iget-wide v2, p0, Lcom/google/android/location/copresence/q/o;->g:J

    add-long/2addr v0, v2

    .line 382
    iget-object v2, p0, Lcom/google/android/location/copresence/q/o;->b:Lcom/google/android/location/copresence/q/g;

    invoke-static {v2, p0, v0, v1}, Lcom/google/android/location/copresence/q/g;->a(Lcom/google/android/location/copresence/q/g;Lcom/google/android/location/copresence/q/o;J)V

    goto :goto_0

    .line 354
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_0
    .end packed-switch

    .line 358
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method
