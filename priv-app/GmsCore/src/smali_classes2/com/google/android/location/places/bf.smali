.class public final Lcom/google/android/location/places/bf;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method public static a(Lcom/google/android/gms/maps/model/LatLng;)Lcom/google/k/f/c/g;
    .locals 6

    .prologue
    const-wide v4, 0x416312d000000000L    # 1.0E7

    .line 291
    new-instance v0, Lcom/google/k/f/c/g;

    invoke-direct {v0}, Lcom/google/k/f/c/g;-><init>()V

    .line 292
    iget-wide v2, p0, Lcom/google/android/gms/maps/model/LatLng;->a:D

    mul-double/2addr v2, v4

    double-to-int v1, v2

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, v0, Lcom/google/k/f/c/g;->a:Ljava/lang/Integer;

    .line 293
    iget-wide v2, p0, Lcom/google/android/gms/maps/model/LatLng;->b:D

    mul-double/2addr v2, v4

    double-to-int v1, v2

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, v0, Lcom/google/k/f/c/g;->b:Ljava/lang/Integer;

    .line 294
    return-object v0
.end method

.method public static a(Lcom/google/android/gms/maps/model/LatLngBounds;)Lcom/google/k/f/c/h;
    .locals 2

    .prologue
    .line 284
    new-instance v0, Lcom/google/k/f/c/h;

    invoke-direct {v0}, Lcom/google/k/f/c/h;-><init>()V

    .line 285
    iget-object v1, p0, Lcom/google/android/gms/maps/model/LatLngBounds;->b:Lcom/google/android/gms/maps/model/LatLng;

    invoke-static {v1}, Lcom/google/android/location/places/bf;->a(Lcom/google/android/gms/maps/model/LatLng;)Lcom/google/k/f/c/g;

    move-result-object v1

    iput-object v1, v0, Lcom/google/k/f/c/h;->b:Lcom/google/k/f/c/g;

    .line 286
    iget-object v1, p0, Lcom/google/android/gms/maps/model/LatLngBounds;->a:Lcom/google/android/gms/maps/model/LatLng;

    invoke-static {v1}, Lcom/google/android/location/places/bf;->a(Lcom/google/android/gms/maps/model/LatLng;)Lcom/google/k/f/c/g;

    move-result-object v1

    iput-object v1, v0, Lcom/google/k/f/c/h;->a:Lcom/google/k/f/c/g;

    .line 287
    return-object v0
.end method

.method public static a(Lcom/google/android/gms/location/places/PlaceFilter;Ljava/lang/String;)Lcom/google/k/f/c/k;
    .locals 5

    .prologue
    .line 269
    new-instance v1, Lcom/google/k/f/c/k;

    invoke-direct {v1}, Lcom/google/k/f/c/k;-><init>()V

    .line 270
    invoke-virtual {p0}, Lcom/google/android/gms/location/places/PlaceFilter;->c()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v0

    new-array v0, v0, [Ljava/lang/String;

    iput-object v0, v1, Lcom/google/k/f/c/k;->b:[Ljava/lang/String;

    .line 271
    invoke-virtual {p0}, Lcom/google/android/gms/location/places/PlaceFilter;->c()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/location/places/PlaceType;

    .line 273
    iget-object v3, v1, Lcom/google/k/f/c/k;->b:[Ljava/lang/String;

    const/4 v4, 0x0

    invoke-virtual {v0}, Lcom/google/android/gms/location/places/PlaceType;->a()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v3, v4

    goto :goto_0

    .line 276
    :cond_0
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 277
    iput-object p1, v1, Lcom/google/k/f/c/k;->d:Ljava/lang/String;

    .line 279
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/gms/location/places/PlaceFilter;->e()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, v1, Lcom/google/k/f/c/k;->e:Ljava/lang/Boolean;

    .line 280
    return-object v1
.end method

.method public static a(ILjava/lang/String;I)Lcom/google/k/f/c/n;
    .locals 3

    .prologue
    .line 249
    new-instance v0, Lcom/google/k/f/c/n;

    invoke-direct {v0}, Lcom/google/k/f/c/n;-><init>()V

    .line 250
    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, v0, Lcom/google/k/f/c/n;->a:Ljava/lang/Integer;

    .line 251
    new-instance v1, Lcom/google/k/f/c/b;

    invoke-direct {v1}, Lcom/google/k/f/c/b;-><init>()V

    iput-object v1, v0, Lcom/google/k/f/c/n;->b:Lcom/google/k/f/c/b;

    .line 252
    iget-object v1, v0, Lcom/google/k/f/c/n;->b:Lcom/google/k/f/c/b;

    iput-object p1, v1, Lcom/google/k/f/c/b;->a:Ljava/lang/String;

    .line 253
    iget-object v1, v0, Lcom/google/k/f/c/n;->b:Lcom/google/k/f/c/b;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iput-object v2, v1, Lcom/google/k/f/c/b;->b:Ljava/lang/Integer;

    .line 254
    return-object v0
.end method

.method public static a(ILjava/lang/String;Ljava/lang/String;)Lcom/google/k/f/c/p;
    .locals 2

    .prologue
    .line 259
    new-instance v0, Lcom/google/k/f/c/p;

    invoke-direct {v0}, Lcom/google/k/f/c/p;-><init>()V

    .line 260
    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, v0, Lcom/google/k/f/c/p;->a:Ljava/lang/Integer;

    .line 261
    iput-object p1, v0, Lcom/google/k/f/c/p;->b:Ljava/lang/String;

    .line 262
    if-eqz p1, :cond_0

    invoke-virtual {p1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 263
    iput-object p2, v0, Lcom/google/k/f/c/p;->c:Ljava/lang/String;

    .line 265
    :cond_0
    return-object v0
.end method

.method public static a(Lcom/google/k/f/c/n;Landroid/location/Location;Ljava/util/List;)V
    .locals 8

    .prologue
    const/4 v1, 0x2

    const/4 v0, 0x1

    const-wide v6, 0x416312d000000000L    # 1.0E7

    const/4 v2, 0x0

    .line 210
    if-eqz p1, :cond_2

    .line 211
    new-instance v3, Lcom/google/k/f/c/g;

    invoke-direct {v3}, Lcom/google/k/f/c/g;-><init>()V

    invoke-virtual {p1}, Landroid/location/Location;->getLatitude()D

    move-result-wide v4

    mul-double/2addr v4, v6

    double-to-int v4, v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    iput-object v4, v3, Lcom/google/k/f/c/g;->a:Ljava/lang/Integer;

    invoke-virtual {p1}, Landroid/location/Location;->getLongitude()D

    move-result-wide v4

    mul-double/2addr v4, v6

    double-to-int v4, v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    iput-object v4, v3, Lcom/google/k/f/c/g;->b:Ljava/lang/Integer;

    iput-object v3, p0, Lcom/google/k/f/c/n;->c:Lcom/google/k/f/c/g;

    .line 213
    invoke-virtual {p1}, Landroid/location/Location;->hasAccuracy()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 214
    invoke-virtual {p1}, Landroid/location/Location;->getAccuracy()F

    move-result v3

    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    iput-object v3, p0, Lcom/google/k/f/c/n;->e:Ljava/lang/Float;

    .line 217
    :cond_0
    invoke-virtual {p1}, Landroid/location/Location;->getExtras()Landroid/os/Bundle;

    move-result-object v3

    .line 218
    if-eqz v3, :cond_2

    const-string v4, "locationType"

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 220
    const-string v4, "locationType"

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 222
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_2

    .line 223
    const-string v3, "locationType is null or empty"

    invoke-static {v4, v3}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    const/4 v3, -0x1

    invoke-virtual {v4}, Ljava/lang/String;->hashCode()I

    move-result v5

    sparse-switch v5, :sswitch_data_0

    :cond_1
    :goto_0
    packed-switch v3, :pswitch_data_0

    move v0, v2

    :goto_1
    :pswitch_0
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/k/f/c/n;->d:Ljava/lang/Integer;

    .line 228
    :cond_2
    if-eqz p2, :cond_3

    .line 229
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v0

    new-array v0, v0, [Lcom/google/k/f/c/s;

    iput-object v0, p0, Lcom/google/k/f/c/n;->f:[Lcom/google/k/f/c/s;

    .line 230
    :goto_2
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_3

    .line 231
    invoke-interface {p2, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/reporting/a/l;

    .line 232
    iget-object v1, p0, Lcom/google/k/f/c/n;->f:[Lcom/google/k/f/c/s;

    new-instance v3, Lcom/google/k/f/c/s;

    invoke-direct {v3}, Lcom/google/k/f/c/s;-><init>()V

    aput-object v3, v1, v2

    .line 233
    iget-object v1, p0, Lcom/google/k/f/c/n;->f:[Lcom/google/k/f/c/s;

    aget-object v1, v1, v2

    iget-wide v4, v0, Lcom/google/android/location/reporting/a/l;->b:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    iput-object v3, v1, Lcom/google/k/f/c/s;->a:Ljava/lang/Long;

    .line 234
    iget-object v1, p0, Lcom/google/k/f/c/n;->f:[Lcom/google/k/f/c/s;

    aget-object v1, v1, v2

    iget v0, v0, Lcom/google/android/location/reporting/a/l;->d:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, v1, Lcom/google/k/f/c/s;->b:Ljava/lang/Integer;

    .line 230
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 223
    :sswitch_0
    const-string v5, "gps"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    move v3, v2

    goto :goto_0

    :sswitch_1
    const-string v5, "wifi"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    move v3, v0

    goto :goto_0

    :sswitch_2
    const-string v5, "cell"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    move v3, v1

    goto :goto_0

    :pswitch_1
    move v0, v1

    goto :goto_1

    :pswitch_2
    const/4 v0, 0x3

    goto :goto_1

    .line 237
    :cond_3
    return-void

    .line 223
    nop

    :sswitch_data_0
    .sparse-switch
        0x190aa -> :sswitch_0
        0x2e8962 -> :sswitch_2
        0x37af15 -> :sswitch_1
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
