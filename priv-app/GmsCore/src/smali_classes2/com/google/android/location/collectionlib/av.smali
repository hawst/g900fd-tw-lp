.class public final Lcom/google/android/location/collectionlib/av;
.super Lcom/google/android/location/collectionlib/g;
.source "SourceFile"


# instance fields
.field private final d:Lcom/google/android/location/collectionlib/SensorScannerConfig;

.field private e:I

.field private f:J


# direct methods
.method public constructor <init>(Lcom/google/android/location/collectionlib/ce;Lcom/google/android/location/collectionlib/ak;Lcom/google/android/location/collectionlib/SensorScannerConfig;)V
    .locals 2

    .prologue
    .line 20
    invoke-direct {p0, p1, p2}, Lcom/google/android/location/collectionlib/g;-><init>(Lcom/google/android/location/collectionlib/ce;Lcom/google/android/location/collectionlib/ak;)V

    .line 15
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/location/collectionlib/av;->e:I

    .line 16
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/google/android/location/collectionlib/av;->f:J

    .line 21
    iput-object p3, p0, Lcom/google/android/location/collectionlib/av;->d:Lcom/google/android/location/collectionlib/SensorScannerConfig;

    .line 22
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 4

    .prologue
    .line 28
    iget-object v0, p0, Lcom/google/android/location/collectionlib/g;->c:Lcom/google/android/location/collectionlib/ak;

    iget-object v1, p0, Lcom/google/android/location/collectionlib/av;->a:Ljava/lang/Runnable;

    iget-object v2, p0, Lcom/google/android/location/collectionlib/av;->d:Lcom/google/android/location/collectionlib/SensorScannerConfig;

    invoke-virtual {v2}, Lcom/google/android/location/collectionlib/SensorScannerConfig;->b()J

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/location/collectionlib/ak;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 29
    return-void
.end method

.method public final a(Lcom/google/android/location/collectionlib/cg;JLandroid/hardware/SensorEvent;)V
    .locals 6

    .prologue
    .line 35
    sget-object v0, Lcom/google/android/location/collectionlib/cg;->d:Lcom/google/android/location/collectionlib/cg;

    if-eq p1, v0, :cond_1

    .line 57
    :cond_0
    :goto_0
    return-void

    .line 38
    :cond_1
    iget v0, p0, Lcom/google/android/location/collectionlib/av;->e:I

    iget-object v1, p0, Lcom/google/android/location/collectionlib/av;->d:Lcom/google/android/location/collectionlib/SensorScannerConfig;

    invoke-virtual {v1}, Lcom/google/android/location/collectionlib/SensorScannerConfig;->a()I

    move-result v1

    if-ge v0, v1, :cond_2

    .line 39
    iget v0, p0, Lcom/google/android/location/collectionlib/av;->e:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/location/collectionlib/av;->e:I

    .line 40
    iget v0, p0, Lcom/google/android/location/collectionlib/av;->e:I

    iget-object v1, p0, Lcom/google/android/location/collectionlib/av;->d:Lcom/google/android/location/collectionlib/SensorScannerConfig;

    invoke-virtual {v1}, Lcom/google/android/location/collectionlib/SensorScannerConfig;->a()I

    move-result v1

    if-lt v0, v1, :cond_0

    .line 45
    iget-object v0, p0, Lcom/google/android/location/collectionlib/g;->c:Lcom/google/android/location/collectionlib/ak;

    iget-object v1, p0, Lcom/google/android/location/collectionlib/av;->a:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Lcom/google/android/location/collectionlib/ak;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 46
    iget-object v0, p0, Lcom/google/android/location/collectionlib/g;->c:Lcom/google/android/location/collectionlib/ak;

    iget-object v1, p0, Lcom/google/android/location/collectionlib/av;->a:Ljava/lang/Runnable;

    iget-object v2, p0, Lcom/google/android/location/collectionlib/av;->d:Lcom/google/android/location/collectionlib/SensorScannerConfig;

    invoke-virtual {v2}, Lcom/google/android/location/collectionlib/SensorScannerConfig;->c()J

    move-result-wide v2

    const-wide/16 v4, 0x3c

    add-long/2addr v2, v4

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/location/collectionlib/ak;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0

    .line 50
    :cond_2
    iget-wide v0, p0, Lcom/google/android/location/collectionlib/av;->f:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-gez v0, :cond_3

    .line 51
    iput-wide p2, p0, Lcom/google/android/location/collectionlib/av;->f:J

    goto :goto_0

    .line 52
    :cond_3
    iget-wide v0, p0, Lcom/google/android/location/collectionlib/av;->f:J

    sub-long v0, p2, v0

    iget-object v2, p0, Lcom/google/android/location/collectionlib/av;->d:Lcom/google/android/location/collectionlib/SensorScannerConfig;

    invoke-virtual {v2}, Lcom/google/android/location/collectionlib/SensorScannerConfig;->c()J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-ltz v0, :cond_0

    .line 54
    iget-object v0, p0, Lcom/google/android/location/collectionlib/g;->c:Lcom/google/android/location/collectionlib/ak;

    iget-object v1, p0, Lcom/google/android/location/collectionlib/av;->a:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Lcom/google/android/location/collectionlib/ak;->postAtFrontOfQueue(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 61
    iget-object v0, p0, Lcom/google/android/location/collectionlib/g;->c:Lcom/google/android/location/collectionlib/ak;

    iget-object v1, p0, Lcom/google/android/location/collectionlib/av;->a:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Lcom/google/android/location/collectionlib/ak;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 62
    return-void
.end method
