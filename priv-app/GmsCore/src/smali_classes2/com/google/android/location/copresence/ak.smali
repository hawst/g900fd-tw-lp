.class final Lcom/google/android/location/copresence/ak;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Z

.field final synthetic b:Lcom/google/android/location/copresence/a/a;

.field final synthetic c:Lcom/google/android/location/copresence/ah;


# direct methods
.method constructor <init>(Lcom/google/android/location/copresence/ah;ZLcom/google/android/location/copresence/a/a;)V
    .locals 0

    .prologue
    .line 189
    iput-object p1, p0, Lcom/google/android/location/copresence/ak;->c:Lcom/google/android/location/copresence/ah;

    iput-boolean p2, p0, Lcom/google/android/location/copresence/ak;->a:Z

    iput-object p3, p0, Lcom/google/android/location/copresence/ak;->b:Lcom/google/android/location/copresence/a/a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 193
    const/4 v0, 0x3

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 194
    iget-boolean v0, p0, Lcom/google/android/location/copresence/ak;->a:Z

    if-eqz v0, :cond_1

    const-string v0, "register"

    .line 195
    :goto_0
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "DeviceRegistrationHelper: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " Device with: id="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/location/copresence/ak;->c:Lcom/google/android/location/copresence/ah;

    iget-object v2, v2, Lcom/google/android/location/copresence/ah;->h:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " for account="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/location/copresence/ak;->b:Lcom/google/android/location/copresence/a/a;

    invoke-static {v2}, Lcom/google/android/location/copresence/ag;->a(Lcom/google/android/location/copresence/a/a;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->b(Ljava/lang/String;)I

    .line 199
    :cond_0
    iget-boolean v0, p0, Lcom/google/android/location/copresence/ak;->a:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/location/copresence/ak;->c:Lcom/google/android/location/copresence/ah;

    iget-object v4, v0, Lcom/google/android/location/copresence/ah;->h:Ljava/lang/String;

    .line 200
    :goto_1
    iget-object v0, p0, Lcom/google/android/location/copresence/ak;->c:Lcom/google/android/location/copresence/ah;

    iget-object v0, v0, Lcom/google/android/location/copresence/ah;->b:Landroid/content/Context;

    const-string v2, "copresence_gcm_pref"

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v2, "copresence_uuid"

    invoke-interface {v0, v2, v1}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 201
    iget-object v0, p0, Lcom/google/android/location/copresence/ak;->c:Lcom/google/android/location/copresence/ah;

    iget-object v0, v0, Lcom/google/android/location/copresence/ah;->e:Lcom/google/android/location/reporting/config/h;

    iget-object v1, p0, Lcom/google/android/location/copresence/ak;->b:Lcom/google/android/location/copresence/a/a;

    iget-object v1, v1, Lcom/google/android/location/copresence/a/a;->a:Landroid/accounts/Account;

    invoke-virtual {v0, v1}, Lcom/google/android/location/reporting/config/h;->b(Landroid/accounts/Account;)I

    move-result v6

    .line 202
    iget-object v0, p0, Lcom/google/android/location/copresence/ak;->c:Lcom/google/android/location/copresence/ah;

    iget-object v2, v0, Lcom/google/android/location/copresence/ah;->d:Lcom/google/android/location/copresence/l/d;

    iget-object v3, p0, Lcom/google/android/location/copresence/ak;->b:Lcom/google/android/location/copresence/a/a;

    iget-object v0, p0, Lcom/google/android/location/copresence/ak;->c:Lcom/google/android/location/copresence/ah;

    iget-object v7, v0, Lcom/google/android/location/copresence/ah;->g:Lcom/google/android/location/copresence/l/ab;

    new-instance v0, Lcom/google/android/location/copresence/l/r;

    iget-object v1, v2, Lcom/google/android/location/copresence/l/d;->a:Landroid/content/Context;

    iget-object v2, v2, Lcom/google/android/location/copresence/l/d;->b:Lcom/google/android/location/copresence/ap;

    invoke-direct/range {v0 .. v7}, Lcom/google/android/location/copresence/l/r;-><init>(Landroid/content/Context;Lcom/google/android/location/copresence/ap;Lcom/google/android/location/copresence/a/a;Ljava/lang/String;Ljava/lang/String;ILcom/google/android/location/copresence/l/ab;)V

    invoke-virtual {v0}, Lcom/google/android/location/copresence/l/ac;->d()V

    .line 203
    return-void

    .line 194
    :cond_1
    const-string v0, "unregister"

    goto :goto_0

    :cond_2
    move-object v4, v1

    .line 199
    goto :goto_1
.end method
