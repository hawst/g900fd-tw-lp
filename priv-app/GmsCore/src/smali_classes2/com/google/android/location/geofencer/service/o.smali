.class abstract Lcom/google/android/location/geofencer/service/o;
.super Lcom/google/android/location/geofencer/service/p;
.source "SourceFile"

# interfaces
.implements Landroid/app/PendingIntent$OnFinished;


# instance fields
.field final synthetic b:Lcom/google/android/location/geofencer/service/k;


# direct methods
.method public constructor <init>(Lcom/google/android/location/geofencer/service/k;Lcom/google/android/location/geofencer/service/i;)V
    .locals 0

    .prologue
    .line 1128
    iput-object p1, p0, Lcom/google/android/location/geofencer/service/o;->b:Lcom/google/android/location/geofencer/service/k;

    .line 1129
    invoke-direct {p0, p1, p2}, Lcom/google/android/location/geofencer/service/p;-><init>(Lcom/google/android/location/geofencer/service/k;Lcom/google/android/location/geofencer/service/i;)V

    .line 1130
    return-void
.end method

.method private static a(Ljava/util/ArrayList;)Ljava/util/ArrayList;
    .locals 3

    .prologue
    .line 1408
    new-instance v1, Ljava/util/ArrayList;

    invoke-virtual {p0}, Ljava/util/ArrayList;->size()I

    move-result v0

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 1410
    invoke-virtual {p0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/geofencer/data/g;

    .line 1411
    iget-object v0, v0, Lcom/google/android/location/geofencer/data/g;->a:Lcom/google/android/gms/location/internal/ParcelableGeofence;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1413
    :cond_0
    return-object v1
.end method

.method private static a(Ljava/util/List;)Ljava/util/HashMap;
    .locals 5

    .prologue
    .line 1434
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    .line 1436
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/geofencer/data/g;

    .line 1437
    iget-object v4, v0, Lcom/google/android/location/geofencer/data/g;->b:Landroid/app/PendingIntent;

    .line 1438
    invoke-virtual {v2, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/ArrayList;

    .line 1439
    if-nez v1, :cond_0

    .line 1440
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 1441
    invoke-virtual {v2, v4, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1443
    :cond_0
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1445
    :cond_1
    return-object v2
.end method

.method private a(Ljava/util/List;ILandroid/location/Location;)V
    .locals 10

    .prologue
    const/4 v9, 0x3

    const/4 v6, 0x0

    .line 1328
    const-string v0, "GeofencerStateMachine"

    invoke-static {v0, v9}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1329
    const-string v1, "GeofencerStateMachine"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v0, "Firing geofences, transition="

    invoke-direct {v2, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    packed-switch p2, :pswitch_data_0

    :pswitch_0
    const-string v0, "unknown"

    :goto_0
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/google/android/location/geofencer/a/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1335
    :cond_0
    iget-object v0, p0, Lcom/google/android/location/geofencer/service/o;->b:Lcom/google/android/location/geofencer/service/k;

    invoke-static {v0}, Lcom/google/android/location/geofencer/service/k;->d(Lcom/google/android/location/geofencer/service/k;)Lcom/google/android/location/n/ae;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/location/n/ae;->a()Z

    move-result v0

    if-nez v0, :cond_2

    .line 1336
    const-string v0, "GeofencerStateMachine"

    invoke-static {v0, v9}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1337
    const-string v0, "GeofencerStateMachine"

    const-string v1, "Ignoring geofence alerts because the service is not owned by foreground user."

    invoke-static {v0, v1}, Lcom/google/android/location/geofencer/a/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1392
    :cond_1
    return-void

    .line 1329
    :pswitch_1
    const-string v0, "dwell"

    goto :goto_0

    :pswitch_2
    const-string v0, "enter"

    goto :goto_0

    :pswitch_3
    const-string v0, "exit"

    goto :goto_0

    .line 1342
    :cond_2
    invoke-static {p1}, Lcom/google/android/location/geofencer/service/o;->a(Ljava/util/List;)Ljava/util/HashMap;

    move-result-object v0

    .line 1344
    iget-object v1, p0, Lcom/google/android/location/geofencer/service/o;->b:Lcom/google/android/location/geofencer/service/k;

    invoke-static {v1}, Lcom/google/android/location/geofencer/service/k;->r(Lcom/google/android/location/geofencer/service/k;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v7

    .line 1347
    invoke-virtual {v0}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :cond_3
    :goto_1
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Ljava/util/Map$Entry;

    .line 1348
    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/PendingIntent;

    .line 1349
    invoke-virtual {v0}, Landroid/app/PendingIntent;->getTargetPackage()Ljava/lang/String;

    move-result-object v2

    .line 1354
    const-string v3, "android.permission.ACCESS_FINE_LOCATION"

    invoke-virtual {v7, v3, v2}, Landroid/content/pm/PackageManager;->checkPermission(Ljava/lang/String;Ljava/lang/String;)I

    move-result v3

    const/4 v4, -0x1

    if-ne v3, v4, :cond_4

    .line 1356
    sget-boolean v1, Lcom/google/android/location/geofencer/a/a;->a:Z

    if-eqz v1, :cond_3

    .line 1357
    const-string v1, "GeofencerStateMachine"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "android.permission.ACCESS_FINE_LOCATION removed in "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Landroid/app/PendingIntent;->getTargetPackage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/google/android/location/geofencer/a/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 1363
    :cond_4
    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/ArrayList;

    .line 1365
    const-string v3, "GeofencerStateMachine"

    invoke-static {v3, v9}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 1366
    const-string v3, "GeofencerStateMachine"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Firing geofence: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/android/location/geofencer/a/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1369
    :cond_5
    new-instance v3, Landroid/content/Intent;

    invoke-direct {v3}, Landroid/content/Intent;-><init>()V

    .line 1370
    const-string v4, "com.google.android.location.intent.extra.transition"

    invoke-virtual {v3, v4, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1371
    invoke-static {v1}, Lcom/google/android/location/geofencer/service/o;->a(Ljava/util/ArrayList;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-static {v3, v1}, Lcom/google/android/gms/location/m;->a(Landroid/content/Intent;Ljava/util/ArrayList;)V

    .line 1372
    if-eqz p3, :cond_6

    iget-object v1, p0, Lcom/google/android/location/geofencer/service/o;->b:Lcom/google/android/location/geofencer/service/k;

    invoke-static {v1}, Lcom/google/android/location/geofencer/service/k;->s(Lcom/google/android/location/geofencer/service/k;)Lcom/google/android/location/o/a;

    move-result-object v1

    invoke-virtual {v1, v2}, Lcom/google/android/location/o/a;->a(Ljava/lang/String;)I

    move-result v1

    const v2, 0x4c4b40

    if-lt v1, v2, :cond_9

    const/4 v1, 0x1

    :goto_2
    if-eqz v1, :cond_6

    invoke-static {p3}, Lcom/google/android/location/n/z;->e(Landroid/location/Location;)Z

    move-result v1

    invoke-static {p3, v1}, Lcom/google/android/location/fused/aw;->a(Landroid/location/Location;Z)Landroid/location/Location;

    move-result-object v1

    const-string v2, "com.google.android.location.intent.extra.triggering_location"

    invoke-virtual {v3, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 1377
    :cond_6
    iget-object v1, p0, Lcom/google/android/location/geofencer/service/o;->c:Lcom/google/android/location/geofencer/service/i;

    invoke-virtual {v0}, Landroid/app/PendingIntent;->getTargetPackage()Ljava/lang/String;

    move-result-object v2

    :try_start_0
    iget-object v4, v1, Lcom/google/android/location/geofencer/service/i;->m:Landroid/content/pm/PackageManager;

    const/4 v5, 0x0

    invoke-virtual {v4, v2, v5}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object v4

    iget-object v5, v1, Lcom/google/android/location/geofencer/service/i;->n:Landroid/content/Context;

    invoke-static {v5}, Lcom/google/android/gms/common/util/be;->a(Landroid/content/Context;)Z

    move-result v5

    if-eqz v5, :cond_7

    iget v4, v4, Landroid/content/pm/ApplicationInfo;->uid:I

    invoke-static {v4, v2}, Lcom/google/android/gms/common/util/be;->a(ILjava/lang/String;)Landroid/os/WorkSource;

    move-result-object v2

    iget-object v4, v1, Lcom/google/android/location/geofencer/service/i;->j:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v4, v2}, Landroid/os/PowerManager$WakeLock;->setWorkSource(Landroid/os/WorkSource;)V
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_1

    :cond_7
    :goto_3
    iget-object v1, v1, Lcom/google/android/location/geofencer/service/i;->j:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->acquire()V

    .line 1379
    :try_start_1
    iget-object v1, p0, Lcom/google/android/location/geofencer/service/o;->b:Lcom/google/android/location/geofencer/service/k;

    invoke-static {v1}, Lcom/google/android/location/geofencer/service/k;->r(Lcom/google/android/location/geofencer/service/k;)Landroid/content/Context;

    move-result-object v1

    const/4 v2, 0x0

    const/4 v5, 0x0

    move-object v4, p0

    invoke-virtual/range {v0 .. v5}, Landroid/app/PendingIntent;->send(Landroid/content/Context;ILandroid/content/Intent;Landroid/app/PendingIntent$OnFinished;Landroid/os/Handler;)V
    :try_end_1
    .catch Landroid/app/PendingIntent$CanceledException; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_1

    .line 1381
    :catch_0
    move-exception v1

    sget-boolean v1, Lcom/google/android/location/geofencer/a/a;->a:Z

    if-eqz v1, :cond_8

    .line 1382
    const-string v1, "GeofencerStateMachine"

    const-string v2, "Removing canceled pending intent."

    invoke-static {v1, v2}, Lcom/google/android/location/geofencer/a/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1388
    :cond_8
    iget-object v1, p0, Lcom/google/android/location/geofencer/service/o;->c:Lcom/google/android/location/geofencer/service/i;

    invoke-virtual {v1, v0}, Lcom/google/android/location/geofencer/service/i;->a(Landroid/app/PendingIntent;)I

    .line 1389
    iget-object v0, p0, Lcom/google/android/location/geofencer/service/o;->c:Lcom/google/android/location/geofencer/service/i;

    invoke-virtual {v0}, Lcom/google/android/location/geofencer/service/i;->d()V

    goto/16 :goto_1

    :cond_9
    move v1, v6

    .line 1372
    goto :goto_2

    :catch_1
    move-exception v2

    goto :goto_3

    .line 1329
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private b(Z)V
    .locals 4

    .prologue
    .line 1480
    iget-object v0, p0, Lcom/google/android/location/geofencer/service/o;->c:Lcom/google/android/location/geofencer/service/i;

    invoke-virtual {p0}, Lcom/google/android/location/geofencer/service/o;->i()D

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/google/android/location/geofencer/service/i;->b(D)V

    .line 1481
    iget-object v0, p0, Lcom/google/android/location/geofencer/service/o;->c:Lcom/google/android/location/geofencer/service/i;

    invoke-virtual {v0}, Lcom/google/android/location/geofencer/service/i;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1482
    const/4 v0, -0x1

    invoke-virtual {p0, v0, p1}, Lcom/google/android/location/geofencer/service/o;->a(IZ)V

    .line 1486
    :goto_0
    return-void

    .line 1484
    :cond_0
    iget-object v0, p0, Lcom/google/android/location/geofencer/service/o;->b:Lcom/google/android/location/geofencer/service/k;

    iget-object v1, p0, Lcom/google/android/location/geofencer/service/o;->b:Lcom/google/android/location/geofencer/service/k;

    invoke-static {v1}, Lcom/google/android/location/geofencer/service/k;->n(Lcom/google/android/location/geofencer/service/k;)Lcom/google/android/location/geofencer/service/t;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/location/geofencer/service/k;->l(Lcom/google/android/location/geofencer/service/k;Lcom/google/android/gms/common/util/a/a;)V

    goto :goto_0
.end method


# virtual methods
.method protected a(IZ)V
    .locals 12

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    const/4 v0, -0x1

    .line 1464
    if-lez p1, :cond_7

    .line 1466
    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/google/android/location/geofencer/service/o;->c:Lcom/google/android/location/geofencer/service/i;

    invoke-virtual {p0}, Lcom/google/android/location/geofencer/service/o;->g()Ljava/util/Collection;

    move-result-object v5

    invoke-virtual {p0}, Lcom/google/android/location/geofencer/service/o;->d()I

    move-result v6

    invoke-virtual {p0}, Lcom/google/android/location/geofencer/service/o;->h()Ljava/util/Collection;

    move-result-object v4

    if-lez p1, :cond_a

    iget-object v7, v1, Lcom/google/android/location/geofencer/service/i;->g:Lcom/google/android/location/geofencer/service/ac;

    invoke-virtual {v7, p1, p2, v5}, Lcom/google/android/location/geofencer/service/ac;->a(IZLjava/util/Collection;)V

    :goto_1
    if-lez v6, :cond_e

    iget-object v5, v1, Lcom/google/android/location/geofencer/service/i;->f:Lcom/google/android/location/h/a;

    if-lez v6, :cond_b

    move v1, v2

    :goto_2
    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "Activity update interval should be positive: "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v1, v7}, Lcom/google/android/gms/common/internal/e;->a(ZLjava/lang/Object;)V

    if-nez v4, :cond_f

    sget-boolean v1, Lcom/google/android/location/geofencer/a/a;->a:Z

    if-eqz v1, :cond_1

    const-string v1, "ActivityDetector"

    const-string v4, "Blaming ourself for activity updates."

    invoke-static {v1, v4}, Lcom/google/android/location/geofencer/a/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    new-instance v1, Lcom/google/android/gms/location/internal/ClientIdentity;

    iget v4, v5, Lcom/google/android/location/h/a;->d:I

    iget-object v7, v5, Lcom/google/android/location/h/a;->e:Ljava/lang/String;

    invoke-direct {v1, v4, v7}, Lcom/google/android/gms/location/internal/ClientIdentity;-><init>(ILjava/lang/String;)V

    invoke-static {v1}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    :goto_3
    iget-object v4, v5, Lcom/google/android/location/h/a;->i:Ljava/lang/Object;

    monitor-enter v4

    :try_start_0
    iget v7, v5, Lcom/google/android/location/h/a;->k:I

    if-ne v6, v7, :cond_2

    iget-object v7, v5, Lcom/google/android/location/h/a;->l:Ljava/util/Collection;

    if-eqz v7, :cond_2

    iget-object v7, v5, Lcom/google/android/location/h/a;->l:Ljava/util/Collection;

    invoke-interface {v7, v1}, Ljava/util/Collection;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_d

    :cond_2
    iget-object v7, v5, Lcom/google/android/location/h/a;->h:Lcom/google/android/location/h/d;

    invoke-virtual {v7}, Lcom/google/android/location/h/d;->b()J

    move-result-wide v8

    const-wide/16 v10, 0x0

    cmp-long v7, v8, v10

    if-gez v7, :cond_c

    :cond_3
    :goto_4
    const-string v3, "ActivityDetector"

    const/4 v7, 0x3

    invoke-static {v3, v7}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_4

    const-string v3, "ActivityDetector"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "requestActivity: intervalSec="

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ", trigger="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ", clients="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v3, v7}, Lcom/google/android/location/geofencer/a/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_4
    iget v3, v5, Lcom/google/android/location/h/a;->k:I

    if-ne v3, v0, :cond_5

    iget-object v0, v5, Lcom/google/android/location/h/a;->h:Lcom/google/android/location/h/d;

    const/4 v3, 0x1

    invoke-virtual {v0, v3}, Lcom/google/android/location/h/d;->a(Z)V

    :cond_5
    iput v6, v5, Lcom/google/android/location/h/a;->k:I

    iput-object v1, v5, Lcom/google/android/location/h/a;->l:Ljava/util/Collection;

    invoke-virtual {v5, v6, v2, v1}, Lcom/google/android/location/h/a;->a(IZLjava/util/Collection;)V

    :cond_6
    :goto_5
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1469
    :goto_6
    return-void

    .line 1464
    :cond_7
    invoke-virtual {p0}, Lcom/google/android/location/geofencer/service/o;->e()I

    move-result v1

    invoke-virtual {p0}, Lcom/google/android/location/geofencer/service/o;->f()I

    move-result p1

    if-ne v1, v0, :cond_8

    if-ne p1, v0, :cond_0

    move p1, v0

    goto/16 :goto_0

    :cond_8
    if-ne p1, v0, :cond_9

    move p1, v1

    goto/16 :goto_0

    :cond_9
    invoke-static {v1, p1}, Ljava/lang/Math;->min(II)I

    move-result v1

    const/16 v4, 0x14

    const/16 v5, 0x708

    invoke-static {v5, v1}, Ljava/lang/Math;->min(II)I

    move-result v1

    invoke-static {v4, v1}, Ljava/lang/Math;->max(II)I

    move-result p1

    goto/16 :goto_0

    .line 1466
    :cond_a
    iget-object v5, v1, Lcom/google/android/location/geofencer/service/i;->g:Lcom/google/android/location/geofencer/service/ac;

    invoke-virtual {v5}, Lcom/google/android/location/geofencer/service/ac;->a()V

    goto/16 :goto_1

    :cond_b
    move v1, v3

    goto/16 :goto_2

    :cond_c
    :try_start_1
    iget-object v7, v5, Lcom/google/android/location/h/a;->c:Lcom/google/android/gms/common/util/p;

    invoke-interface {v7}, Lcom/google/android/gms/common/util/p;->b()J

    move-result-wide v10

    sub-long v8, v10, v8

    const-wide/32 v10, 0x15f90

    cmp-long v7, v8, v10

    if-gtz v7, :cond_3

    move v2, v3

    goto/16 :goto_4

    :cond_d
    sget-boolean v0, Lcom/google/android/location/geofencer/a/a;->a:Z

    if-eqz v0, :cond_6

    const-string v0, "ActivityDetector"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Ignoring requestActivity: intervalSec="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/location/geofencer/a/a;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_5

    :catchall_0
    move-exception v0

    monitor-exit v4

    throw v0

    :cond_e
    iget-object v0, v1, Lcom/google/android/location/geofencer/service/i;->f:Lcom/google/android/location/h/a;

    invoke-virtual {v0}, Lcom/google/android/location/h/a;->a()V

    goto :goto_6

    :cond_f
    move-object v1, v4

    goto/16 :goto_3
.end method

.method protected a(Landroid/content/Intent;)Z
    .locals 1

    .prologue
    .line 1596
    iget-object v0, p0, Lcom/google/android/location/geofencer/service/o;->c:Lcom/google/android/location/geofencer/service/i;

    invoke-virtual {v0, p1}, Lcom/google/android/location/geofencer/service/i;->a(Landroid/content/Intent;)V

    .line 1597
    const/4 v0, 0x0

    return v0
.end method

.method public final a(Landroid/os/Message;)Z
    .locals 13

    .prologue
    .line 1135
    sget-boolean v0, Lcom/google/android/location/geofencer/a/a;->a:Z

    if-eqz v0, :cond_0

    .line 1136
    const-string v0, "GeofencerStateMachine"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "processMessage, current state="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/android/location/geofencer/service/o;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " msg="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/location/geofencer/service/o;->b:Lcom/google/android/location/geofencer/service/k;

    iget v3, p1, Landroid/os/Message;->what:I

    invoke-virtual {v2, v3}, Lcom/google/android/location/geofencer/service/k;->b(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/location/geofencer/a/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1146
    :cond_0
    iget v3, p1, Landroid/os/Message;->what:I

    .line 1147
    const/4 v0, 0x0

    .line 1148
    const/4 v1, 0x4

    if-eq v3, v1, :cond_f

    const/4 v1, 0x5

    if-eq v3, v1, :cond_f

    .line 1149
    iget-object v0, p0, Lcom/google/android/location/geofencer/service/o;->c:Lcom/google/android/location/geofencer/service/i;

    iget-object v1, v0, Lcom/google/android/location/geofencer/service/i;->i:Lcom/google/android/location/geofencer/data/h;

    invoke-virtual {v1}, Lcom/google/android/location/geofencer/data/h;->c()Z

    iget-object v0, v0, Lcom/google/android/location/geofencer/service/i;->h:Lcom/google/android/location/geofencer/data/h;

    invoke-virtual {v0}, Lcom/google/android/location/geofencer/data/h;->c()Z

    move-result v0

    .line 1150
    if-eqz v0, :cond_2

    .line 1151
    iget-object v1, p0, Lcom/google/android/location/geofencer/service/o;->c:Lcom/google/android/location/geofencer/service/i;

    invoke-virtual {p0}, Lcom/google/android/location/geofencer/service/o;->i()D

    move-result-wide v4

    invoke-virtual {v1, v4, v5}, Lcom/google/android/location/geofencer/service/i;->b(D)V

    .line 1152
    iget-object v1, p0, Lcom/google/android/location/geofencer/service/o;->c:Lcom/google/android/location/geofencer/service/i;

    invoke-virtual {v1}, Lcom/google/android/location/geofencer/service/i;->c()Z

    move-result v1

    if-nez v1, :cond_2

    .line 1153
    iget-object v0, p0, Lcom/google/android/location/geofencer/service/o;->b:Lcom/google/android/location/geofencer/service/k;

    invoke-static {v0, p1}, Lcom/google/android/location/geofencer/service/k;->c(Lcom/google/android/location/geofencer/service/k;Landroid/os/Message;)V

    .line 1154
    iget-object v0, p0, Lcom/google/android/location/geofencer/service/o;->b:Lcom/google/android/location/geofencer/service/k;

    iget-object v1, p0, Lcom/google/android/location/geofencer/service/o;->b:Lcom/google/android/location/geofencer/service/k;

    invoke-static {v1}, Lcom/google/android/location/geofencer/service/k;->n(Lcom/google/android/location/geofencer/service/k;)Lcom/google/android/location/geofencer/service/t;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/location/geofencer/service/k;->i(Lcom/google/android/location/geofencer/service/k;Lcom/google/android/gms/common/util/a/a;)V

    .line 1155
    const/4 v0, 0x1

    .line 1250
    :cond_1
    :goto_0
    return v0

    :cond_2
    move v2, v0

    .line 1160
    :goto_1
    const/4 v1, 0x0

    .line 1161
    packed-switch v3, :pswitch_data_0

    .line 1229
    :pswitch_0
    const/4 v1, 0x0

    .line 1230
    invoke-virtual {p0, p1}, Lcom/google/android/location/geofencer/service/o;->b(Landroid/os/Message;)Z

    move-result v0

    .line 1242
    :goto_2
    iget-object v3, p0, Lcom/google/android/location/geofencer/service/o;->c:Lcom/google/android/location/geofencer/service/i;

    iget-object v3, v3, Lcom/google/android/location/geofencer/service/i;->f:Lcom/google/android/location/h/a;

    iget-object v3, v3, Lcom/google/android/location/h/a;->h:Lcom/google/android/location/h/d;

    invoke-virtual {v3}, Lcom/google/android/location/h/d;->e()V

    .line 1243
    if-eqz v2, :cond_1

    if-nez v1, :cond_1

    .line 1244
    sget-boolean v1, Lcom/google/android/location/geofencer/a/a;->a:Z

    if-eqz v1, :cond_3

    .line 1245
    const-string v1, "GeofencerStateMachine"

    const-string v2, "Geofence expires but message handling method does not update requirement, updating now."

    invoke-static {v1, v2}, Lcom/google/android/location/geofencer/a/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1248
    :cond_3
    const/4 v1, -0x1

    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2}, Lcom/google/android/location/geofencer/service/o;->a(IZ)V

    goto :goto_0

    .line 1164
    :pswitch_1
    iget-object v0, p0, Lcom/google/android/location/geofencer/service/o;->b:Lcom/google/android/location/geofencer/service/k;

    invoke-static {v0}, Lcom/google/android/location/geofencer/service/k;->k(Lcom/google/android/location/geofencer/service/k;)Lcom/google/android/location/geofencer/service/af;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/location/geofencer/service/af;->a()Z

    move-result v0

    if-nez v0, :cond_5

    .line 1165
    const-string v0, "GeofencerStateMachine"

    const-string v1, "Network location disabled."

    invoke-static {v0, v1}, Lcom/google/android/location/geofencer/a/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1166
    iget-object v0, p0, Lcom/google/android/location/geofencer/service/o;->b:Lcom/google/android/location/geofencer/service/k;

    iget-object v1, p0, Lcom/google/android/location/geofencer/service/o;->b:Lcom/google/android/location/geofencer/service/k;

    invoke-static {v1}, Lcom/google/android/location/geofencer/service/k;->o(Lcom/google/android/location/geofencer/service/k;)Lcom/google/android/location/geofencer/service/q;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/location/geofencer/service/k;->j(Lcom/google/android/location/geofencer/service/k;Lcom/google/android/gms/common/util/a/a;)V

    .line 1171
    :cond_4
    :goto_3
    const/4 v0, 0x1

    goto :goto_0

    .line 1167
    :cond_5
    iget-object v0, p0, Lcom/google/android/location/geofencer/service/o;->b:Lcom/google/android/location/geofencer/service/k;

    invoke-static {v0}, Lcom/google/android/location/geofencer/service/k;->k(Lcom/google/android/location/geofencer/service/k;)Lcom/google/android/location/geofencer/service/af;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/location/geofencer/service/af;->b()Z

    move-result v0

    if-nez v0, :cond_4

    .line 1168
    const-string v0, "GeofencerStateMachine"

    const-string v1, "GPS disabled."

    invoke-static {v0, v1}, Lcom/google/android/location/geofencer/a/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1169
    invoke-virtual {p0}, Lcom/google/android/location/geofencer/service/o;->j()V

    goto :goto_3

    .line 1173
    :pswitch_2
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/location/geofencer/service/a;

    iget-object v1, p0, Lcom/google/android/location/geofencer/service/o;->c:Lcom/google/android/location/geofencer/service/i;

    iget-object v1, v1, Lcom/google/android/location/geofencer/service/i;->d:Lcom/google/android/location/geofencer/data/n;

    iget-object v3, v1, Lcom/google/android/location/geofencer/data/n;->c:Landroid/util/Pair;

    iget-object v1, p0, Lcom/google/android/location/geofencer/service/o;->c:Lcom/google/android/location/geofencer/service/i;

    iget-object v4, v0, Lcom/google/android/location/geofencer/service/a;->a:Lcom/google/android/gms/location/GeofencingRequest;

    iget-object v5, v0, Lcom/google/android/location/geofencer/service/a;->b:Landroid/app/PendingIntent;

    invoke-virtual {v5}, Landroid/app/PendingIntent;->getTargetPackage()Ljava/lang/String;

    invoke-virtual {v4}, Lcom/google/android/gms/location/GeofencingRequest;->b()Ljava/util/List;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v6

    if-nez v6, :cond_8

    const/4 v1, 0x0

    :goto_4
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/google/android/location/geofencer/service/a;->a(Ljava/lang/Object;)V

    iget-object v4, p0, Lcom/google/android/location/geofencer/service/o;->c:Lcom/google/android/location/geofencer/service/i;

    invoke-virtual {p0}, Lcom/google/android/location/geofencer/service/o;->i()D

    move-result-wide v6

    iget-object v0, v4, Lcom/google/android/location/geofencer/service/i;->d:Lcom/google/android/location/geofencer/data/n;

    iget-object v5, v0, Lcom/google/android/location/geofencer/data/n;->c:Landroid/util/Pair;

    if-eqz v5, :cond_9

    iget-object v0, v4, Lcom/google/android/location/geofencer/service/i;->c:Lcom/google/android/gms/common/util/p;

    invoke-interface {v0}, Lcom/google/android/gms/common/util/p;->b()J

    move-result-wide v8

    iget-object v0, v5, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v10

    sub-long/2addr v8, v10

    const-wide/16 v10, 0x1388

    cmp-long v0, v8, v10

    if-gez v0, :cond_9

    const/4 v0, 0x0

    invoke-virtual {v4, v5, v0, v6, v7}, Lcom/google/android/location/geofencer/service/i;->a(Landroid/util/Pair;ZD)Lcom/google/android/location/geofencer/data/e;

    move-result-object v0

    :goto_5
    if-eqz v0, :cond_6

    iget-object v4, v0, Lcom/google/android/location/geofencer/data/e;->a:Ljava/util/ArrayList;

    if-eqz v4, :cond_6

    iget-object v4, v0, Lcom/google/android/location/geofencer/data/e;->a:Ljava/util/ArrayList;

    const/4 v5, 0x1

    if-nez v3, :cond_a

    const/4 v0, 0x0

    :goto_6
    invoke-direct {p0, v4, v5, v0}, Lcom/google/android/location/geofencer/service/o;->a(Ljava/util/List;ILandroid/location/Location;)V

    :cond_6
    if-nez v1, :cond_b

    const/4 v0, 0x1

    :goto_7
    invoke-direct {p0, v0}, Lcom/google/android/location/geofencer/service/o;->b(Z)V

    const-string v1, "GeofencerStateMachine"

    const/4 v3, 0x3

    invoke-static {v1, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_7

    const-string v1, "GeofencerStateMachine"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Number of valid geofences now: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/google/android/location/geofencer/service/o;->c:Lcom/google/android/location/geofencer/service/i;

    iget-object v4, v4, Lcom/google/android/location/geofencer/service/i;->h:Lcom/google/android/location/geofencer/data/h;

    invoke-virtual {v4}, Lcom/google/android/location/geofencer/data/h;->d()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Lcom/google/android/location/geofencer/a/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1174
    :cond_7
    const/4 v1, 0x1

    move v12, v1

    move v1, v0

    move v0, v12

    .line 1175
    goto/16 :goto_2

    .line 1173
    :cond_8
    iget-object v1, v1, Lcom/google/android/location/geofencer/service/i;->h:Lcom/google/android/location/geofencer/data/h;

    invoke-virtual {v1, v4, v5}, Lcom/google/android/location/geofencer/data/h;->a(Lcom/google/android/gms/location/GeofencingRequest;Landroid/app/PendingIntent;)I

    move-result v1

    goto :goto_4

    :cond_9
    const/4 v0, 0x0

    goto :goto_5

    :cond_a
    iget-object v0, v3, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Landroid/location/Location;

    goto :goto_6

    :cond_b
    const/4 v0, 0x0

    goto :goto_7

    .line 1177
    :pswitch_3
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/location/geofencer/service/an;

    sget-boolean v1, Lcom/google/android/location/geofencer/a/a;->a:Z

    if-eqz v1, :cond_c

    const-string v1, "GeofencerStateMachine"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "removeFence: request="

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Lcom/google/android/location/geofencer/a/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_c
    const/4 v1, 0x0

    iget v3, v0, Lcom/google/android/location/geofencer/service/an;->a:I

    packed-switch v3, :pswitch_data_1

    :goto_8
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/android/location/geofencer/service/an;->a(Ljava/lang/Object;)V

    if-nez v1, :cond_e

    const/4 v0, 0x1

    :goto_9
    const/4 v1, 0x0

    invoke-direct {p0, v1}, Lcom/google/android/location/geofencer/service/o;->b(Z)V

    sget-boolean v1, Lcom/google/android/location/geofencer/a/a;->a:Z

    if-eqz v1, :cond_d

    const-string v1, "GeofencerStateMachine"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Number of valid geofences now: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/google/android/location/geofencer/service/o;->c:Lcom/google/android/location/geofencer/service/i;

    iget-object v4, v4, Lcom/google/android/location/geofencer/service/i;->h:Lcom/google/android/location/geofencer/data/h;

    invoke-virtual {v4}, Lcom/google/android/location/geofencer/data/h;->d()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Lcom/google/android/location/geofencer/a/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1178
    :cond_d
    const/4 v1, 0x1

    move v12, v1

    move v1, v0

    move v0, v12

    .line 1179
    goto/16 :goto_2

    .line 1177
    :pswitch_4
    iget-object v1, p0, Lcom/google/android/location/geofencer/service/o;->c:Lcom/google/android/location/geofencer/service/i;

    iget-object v3, v0, Lcom/google/android/location/geofencer/service/an;->c:Ljava/lang/String;

    iget-object v4, v1, Lcom/google/android/location/geofencer/service/i;->i:Lcom/google/android/location/geofencer/data/h;

    invoke-virtual {v4, v3}, Lcom/google/android/location/geofencer/data/h;->a(Ljava/lang/String;)I

    iget-object v1, v1, Lcom/google/android/location/geofencer/service/i;->h:Lcom/google/android/location/geofencer/data/h;

    invoke-virtual {v1, v3}, Lcom/google/android/location/geofencer/data/h;->a(Ljava/lang/String;)I

    move-result v1

    goto :goto_8

    :pswitch_5
    iget-object v1, p0, Lcom/google/android/location/geofencer/service/o;->c:Lcom/google/android/location/geofencer/service/i;

    iget-object v3, v0, Lcom/google/android/location/geofencer/service/an;->e:Landroid/app/PendingIntent;

    invoke-virtual {v1, v3}, Lcom/google/android/location/geofencer/service/i;->a(Landroid/app/PendingIntent;)I

    move-result v1

    goto :goto_8

    :pswitch_6
    iget-object v1, p0, Lcom/google/android/location/geofencer/service/o;->c:Lcom/google/android/location/geofencer/service/i;

    iget-object v3, v0, Lcom/google/android/location/geofencer/service/an;->f:[Ljava/lang/String;

    iget-object v4, v0, Lcom/google/android/location/geofencer/service/an;->c:Ljava/lang/String;

    invoke-virtual {v1, v3, v4}, Lcom/google/android/location/geofencer/service/i;->a([Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    goto :goto_8

    :cond_e
    const/4 v0, 0x0

    goto :goto_9

    .line 1181
    :pswitch_7
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Landroid/util/Pair;

    invoke-virtual {p0, v0}, Lcom/google/android/location/geofencer/service/o;->a(Landroid/util/Pair;)Z

    move-result v1

    .line 1182
    const/4 v0, 0x1

    .line 1183
    goto/16 :goto_2

    .line 1185
    :pswitch_8
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Landroid/util/Pair;

    iget-object v0, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/location/h/e;

    invoke-virtual {p0, v0}, Lcom/google/android/location/geofencer/service/o;->a(Lcom/google/android/location/h/e;)Lcom/google/android/gms/common/util/a/b;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/common/internal/e;->a(Ljava/lang/Object;)V

    iget-object v1, p0, Lcom/google/android/location/geofencer/service/o;->b:Lcom/google/android/location/geofencer/service/k;

    invoke-static {v1}, Lcom/google/android/location/geofencer/service/k;->t(Lcom/google/android/location/geofencer/service/k;)V

    iget-object v1, p0, Lcom/google/android/location/geofencer/service/o;->b:Lcom/google/android/location/geofencer/service/k;

    invoke-static {v1, v0}, Lcom/google/android/location/geofencer/service/k;->m(Lcom/google/android/location/geofencer/service/k;Lcom/google/android/gms/common/util/a/a;)V

    const/4 v1, 0x0

    .line 1186
    const/4 v0, 0x1

    .line 1187
    goto/16 :goto_2

    .line 1191
    :pswitch_9
    iget-object v0, p0, Lcom/google/android/location/geofencer/service/o;->c:Lcom/google/android/location/geofencer/service/i;

    invoke-virtual {p0}, Lcom/google/android/location/geofencer/service/o;->i()D

    move-result-wide v4

    invoke-virtual {v0, v4, v5}, Lcom/google/android/location/geofencer/service/i;->b(D)V

    .line 1198
    const/4 v0, -0x1

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/location/geofencer/service/o;->a(IZ)V

    .line 1199
    const/4 v1, 0x1

    .line 1200
    const/4 v0, 0x1

    .line 1201
    goto/16 :goto_2

    .line 1203
    :pswitch_a
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Landroid/content/Intent;

    invoke-virtual {p0, v0}, Lcom/google/android/location/geofencer/service/o;->a(Landroid/content/Intent;)Z

    move-result v1

    .line 1204
    const/4 v0, 0x1

    .line 1205
    goto/16 :goto_2

    .line 1207
    :pswitch_b
    iget-object v0, p0, Lcom/google/android/location/geofencer/service/o;->c:Lcom/google/android/location/geofencer/service/i;

    invoke-virtual {v0}, Lcom/google/android/location/geofencer/service/i;->e()V

    .line 1208
    const/4 v1, 0x0

    .line 1209
    const/4 v0, 0x1

    .line 1210
    goto/16 :goto_2

    .line 1212
    :pswitch_c
    iget-object v0, p0, Lcom/google/android/location/geofencer/service/o;->b:Lcom/google/android/location/geofencer/service/k;

    iget-object v3, p0, Lcom/google/android/location/geofencer/service/o;->c:Lcom/google/android/location/geofencer/service/i;

    invoke-virtual {v3}, Lcom/google/android/location/geofencer/service/i;->f()Lcom/google/android/location/h/e;

    move-result-object v3

    invoke-virtual {p0, v3}, Lcom/google/android/location/geofencer/service/o;->a(Lcom/google/android/location/h/e;)Lcom/google/android/gms/common/util/a/b;

    move-result-object v3

    invoke-static {v0, v3}, Lcom/google/android/location/geofencer/service/k;->k(Lcom/google/android/location/geofencer/service/k;Lcom/google/android/gms/common/util/a/a;)V

    .line 1213
    iget-object v0, p0, Lcom/google/android/location/geofencer/service/o;->b:Lcom/google/android/location/geofencer/service/k;

    invoke-static {v0}, Lcom/google/android/location/geofencer/service/k;->q(Lcom/google/android/location/geofencer/service/k;)V

    .line 1214
    const/4 v0, 0x1

    .line 1215
    goto/16 :goto_2

    .line 1217
    :pswitch_d
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/location/geofencer/service/o;->a(Z)Z

    move-result v1

    .line 1218
    const/4 v0, 0x1

    .line 1219
    goto/16 :goto_2

    .line 1221
    :pswitch_e
    invoke-virtual {p0}, Lcom/google/android/location/geofencer/service/o;->k()Z

    move-result v1

    .line 1222
    const/4 v0, 0x1

    .line 1223
    goto/16 :goto_2

    .line 1225
    :pswitch_f
    invoke-virtual {p0}, Lcom/google/android/location/geofencer/service/o;->l()Z

    move-result v1

    .line 1226
    const/4 v0, 0x1

    .line 1227
    goto/16 :goto_2

    :cond_f
    move v2, v0

    goto/16 :goto_1

    .line 1161
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_0
        :pswitch_d
        :pswitch_e
        :pswitch_f
    .end packed-switch

    .line 1177
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_4
        :pswitch_6
        :pswitch_5
    .end packed-switch
.end method

.method protected a(Landroid/util/Pair;)Z
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 1294
    iget-object v0, p1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Landroid/location/Location;

    .line 1295
    iget-object v1, p0, Lcom/google/android/location/geofencer/service/o;->c:Lcom/google/android/location/geofencer/service/i;

    invoke-virtual {p0}, Lcom/google/android/location/geofencer/service/o;->i()D

    move-result-wide v2

    invoke-virtual {v1, p1, v5, v2, v3}, Lcom/google/android/location/geofencer/service/i;->a(Landroid/util/Pair;ZD)Lcom/google/android/location/geofencer/data/e;

    move-result-object v1

    .line 1296
    if-eqz v1, :cond_3

    .line 1297
    const-string v2, "GeofencerStateMachine"

    const/4 v3, 0x3

    invoke-static {v2, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1298
    const-string v2, "GeofencerStateMachine"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Triggering location: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p1, Landroid/util/Pair;->second:Ljava/lang/Object;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/location/geofencer/a/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1300
    :cond_0
    iget-object v2, v1, Lcom/google/android/location/geofencer/data/e;->a:Ljava/util/ArrayList;

    if-eqz v2, :cond_1

    .line 1301
    iget-object v2, v1, Lcom/google/android/location/geofencer/data/e;->a:Ljava/util/ArrayList;

    invoke-direct {p0, v2, v5, v0}, Lcom/google/android/location/geofencer/service/o;->a(Ljava/util/List;ILandroid/location/Location;)V

    .line 1304
    :cond_1
    iget-object v2, v1, Lcom/google/android/location/geofencer/data/e;->b:Ljava/util/ArrayList;

    if-eqz v2, :cond_2

    .line 1305
    iget-object v2, v1, Lcom/google/android/location/geofencer/data/e;->b:Ljava/util/ArrayList;

    const/4 v3, 0x2

    invoke-direct {p0, v2, v3, v0}, Lcom/google/android/location/geofencer/service/o;->a(Ljava/util/List;ILandroid/location/Location;)V

    .line 1308
    :cond_2
    iget-object v2, v1, Lcom/google/android/location/geofencer/data/e;->c:Ljava/util/ArrayList;

    if-eqz v2, :cond_3

    .line 1309
    iget-object v1, v1, Lcom/google/android/location/geofencer/data/e;->c:Ljava/util/ArrayList;

    const/4 v2, 0x4

    invoke-direct {p0, v1, v2, v0}, Lcom/google/android/location/geofencer/service/o;->a(Ljava/util/List;ILandroid/location/Location;)V

    .line 1313
    :cond_3
    const/4 v0, -0x1

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/google/android/location/geofencer/service/o;->a(IZ)V

    .line 1314
    return v5
.end method

.method protected a(Z)Z
    .locals 1

    .prologue
    .line 1261
    const/4 v0, 0x0

    return v0
.end method

.method protected abstract d()I
.end method

.method protected abstract e()I
.end method

.method protected abstract f()I
.end method

.method protected g()Ljava/util/Collection;
    .locals 4

    .prologue
    .line 1626
    iget-object v0, p0, Lcom/google/android/location/geofencer/service/o;->c:Lcom/google/android/location/geofencer/service/i;

    invoke-virtual {v0}, Lcom/google/android/location/geofencer/service/i;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1627
    const/4 v0, 0x0

    .line 1629
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/location/geofencer/service/o;->c:Lcom/google/android/location/geofencer/service/i;

    invoke-virtual {p0}, Lcom/google/android/location/geofencer/service/o;->i()D

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/google/android/location/geofencer/service/i;->a(D)Ljava/util/Collection;

    move-result-object v0

    goto :goto_0
.end method

.method protected h()Ljava/util/Collection;
    .locals 1

    .prologue
    .line 1634
    iget-object v0, p0, Lcom/google/android/location/geofencer/service/o;->c:Lcom/google/android/location/geofencer/service/i;

    invoke-virtual {v0}, Lcom/google/android/location/geofencer/service/i;->b()Ljava/util/Collection;

    move-result-object v0

    return-object v0
.end method

.method protected abstract i()D
.end method

.method protected j()V
    .locals 0

    .prologue
    .line 1268
    return-void
.end method

.method protected k()Z
    .locals 1

    .prologue
    .line 1274
    const/4 v0, 0x0

    return v0
.end method

.method protected l()Z
    .locals 1

    .prologue
    .line 1281
    const/4 v0, 0x0

    return v0
.end method

.method public onSendFinished(Landroid/app/PendingIntent;Landroid/content/Intent;ILjava/lang/String;Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 1451
    iget-object v0, p0, Lcom/google/android/location/geofencer/service/o;->c:Lcom/google/android/location/geofencer/service/i;

    invoke-virtual {v0}, Lcom/google/android/location/geofencer/service/i;->d()V

    .line 1452
    return-void
.end method
