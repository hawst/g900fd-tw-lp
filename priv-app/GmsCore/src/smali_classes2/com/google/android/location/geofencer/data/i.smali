.class public final Lcom/google/android/location/geofencer/data/i;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final a:I

.field b:Ljava/util/HashMap;

.field c:Ljava/util/HashMap;

.field d:Lcom/google/android/location/geofencer/data/b;


# direct methods
.method public constructor <init>(II)V
    .locals 1

    .prologue
    .line 83
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 76
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/location/geofencer/data/i;->b:Ljava/util/HashMap;

    .line 79
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/location/geofencer/data/i;->c:Ljava/util/HashMap;

    .line 84
    iput p1, p0, Lcom/google/android/location/geofencer/data/i;->a:I

    .line 85
    iget v0, p0, Lcom/google/android/location/geofencer/data/i;->a:I

    if-lez v0, :cond_0

    new-instance v0, Lcom/google/android/location/geofencer/data/o;

    invoke-direct {v0, p2}, Lcom/google/android/location/geofencer/data/o;-><init>(I)V

    :goto_0
    iput-object v0, p0, Lcom/google/android/location/geofencer/data/i;->d:Lcom/google/android/location/geofencer/data/b;

    .line 88
    return-void

    .line 85
    :cond_0
    new-instance v0, Lcom/google/android/location/geofencer/data/a;

    invoke-direct {v0}, Lcom/google/android/location/geofencer/data/a;-><init>()V

    goto :goto_0
.end method


# virtual methods
.method final a(Ljava/lang/String;)Lcom/google/android/location/geofencer/data/k;
    .locals 2

    .prologue
    .line 135
    iget-object v0, p0, Lcom/google/android/location/geofencer/data/i;->b:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/geofencer/data/k;

    .line 136
    if-nez v0, :cond_0

    .line 137
    new-instance v0, Lcom/google/android/location/geofencer/data/k;

    invoke-direct {v0, p1}, Lcom/google/android/location/geofencer/data/k;-><init>(Ljava/lang/String;)V

    .line 138
    iget-object v1, p0, Lcom/google/android/location/geofencer/data/i;->b:Ljava/util/HashMap;

    invoke-virtual {v1, p1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 140
    :cond_0
    return-object v0
.end method

.method public final a()V
    .locals 4

    .prologue
    .line 95
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    iget-object v0, p0, Lcom/google/android/location/geofencer/data/i;->b:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/geofencer/data/k;

    invoke-virtual {v0}, Lcom/google/android/location/geofencer/data/k;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/geofencer/data/k;

    invoke-virtual {v1, v3, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_0
    iput-object v1, p0, Lcom/google/android/location/geofencer/data/i;->c:Ljava/util/HashMap;

    .line 96
    return-void
.end method

.method final a(Ljava/lang/String;Ljava/util/List;)Z
    .locals 3

    .prologue
    .line 262
    new-instance v1, Ljava/util/ArrayList;

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v0

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 263
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/location/internal/ParcelableGeofence;

    .line 264
    invoke-virtual {v0}, Lcom/google/android/gms/location/internal/ParcelableGeofence;->a()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 266
    :cond_0
    invoke-virtual {p0, p1, v1}, Lcom/google/android/location/geofencer/data/i;->b(Ljava/lang/String;Ljava/util/List;)Z

    move-result v0

    return v0
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 104
    iget-object v0, p0, Lcom/google/android/location/geofencer/data/i;->c:Ljava/util/HashMap;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "Need to call backup() before calling this."

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/bx;->a(ZLjava/lang/Object;)V

    .line 105
    iget-object v0, p0, Lcom/google/android/location/geofencer/data/i;->c:Ljava/util/HashMap;

    iput-object v0, p0, Lcom/google/android/location/geofencer/data/i;->b:Ljava/util/HashMap;

    .line 106
    return-void

    .line 104
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Ljava/lang/String;Ljava/util/List;)Z
    .locals 4

    .prologue
    .line 275
    const/4 v1, 0x0

    .line 276
    iget-object v0, p0, Lcom/google/android/location/geofencer/data/i;->b:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/geofencer/data/k;

    .line 277
    if-eqz v0, :cond_2

    if-eqz p2, :cond_2

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v2

    if-lez v2, :cond_2

    .line 279
    invoke-virtual {v0, p2}, Lcom/google/android/location/geofencer/data/k;->a(Ljava/util/List;)Z

    move-result v1

    .line 282
    invoke-virtual {v0}, Lcom/google/android/location/geofencer/data/k;->a()I

    move-result v0

    if-nez v0, :cond_0

    .line 283
    iget-object v0, p0, Lcom/google/android/location/geofencer/data/i;->b:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    move v0, v1

    .line 286
    :goto_0
    if-eqz v0, :cond_1

    .line 287
    iget-object v1, p0, Lcom/google/android/location/geofencer/data/i;->d:Lcom/google/android/location/geofencer/data/b;

    iget-object v2, p0, Lcom/google/android/location/geofencer/data/i;->b:Ljava/util/HashMap;

    invoke-virtual {v2}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-virtual {p0}, Lcom/google/android/location/geofencer/data/i;->c()I

    move-result v3

    invoke-interface {v1, v2, v3}, Lcom/google/android/location/geofencer/data/b;->a(Ljava/lang/Iterable;I)V

    .line 289
    :cond_1
    return v0

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method public final c()I
    .locals 3

    .prologue
    .line 321
    const/4 v0, 0x0

    .line 322
    iget-object v1, p0, Lcom/google/android/location/geofencer/data/i;->b:Ljava/util/HashMap;

    invoke-virtual {v1}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/geofencer/data/k;

    .line 323
    invoke-virtual {v0}, Lcom/google/android/location/geofencer/data/k;->a()I

    move-result v0

    add-int/2addr v0, v1

    move v1, v0

    .line 324
    goto :goto_0

    .line 325
    :cond_0
    return v1
.end method
