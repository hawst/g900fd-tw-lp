.class public Lcom/google/android/location/geofencer/service/k;
.super Lcom/google/android/gms/common/util/a/c;
.source "SourceFile"


# static fields
.field static volatile j:Lcom/google/android/location/geofencer/service/k;


# instance fields
.field private volatile A:Z

.field private final B:Ljava/util/List;

.field private final C:Lcom/google/android/location/geofencer/service/af;

.field private final D:Lcom/google/android/location/o/a;

.field final h:Landroid/content/BroadcastReceiver;

.field volatile i:Z

.field private final k:Lcom/google/android/location/geofencer/service/u;

.field private final l:Lcom/google/android/location/geofencer/service/t;

.field private final m:Lcom/google/android/location/geofencer/service/q;

.field private final n:Lcom/google/android/location/geofencer/service/s;

.field private final o:Lcom/google/android/location/geofencer/service/x;

.field private final p:Lcom/google/android/location/geofencer/service/y;

.field private final q:Lcom/google/android/location/geofencer/service/w;

.field private final r:Lcom/google/android/location/geofencer/service/z;

.field private final s:Lcom/google/android/location/geofencer/service/r;

.field private final t:Lcom/google/android/location/geofencer/service/n;

.field private final u:Ljava/lang/Object;

.field private final v:Lcom/google/android/gms/common/util/p;

.field private final w:Landroid/content/Context;

.field private final x:Lcom/google/android/location/geofencer/service/i;

.field private final y:Lcom/google/android/location/n/ae;

.field private final z:Landroid/content/IntentFilter;


# direct methods
.method private constructor <init>(Landroid/content/Context;Lcom/google/android/gms/common/util/p;)V
    .locals 7

    .prologue
    .line 212
    const-class v3, Lcom/google/android/location/internal/GoogleLocationManagerService;

    new-instance v4, Lcom/google/android/location/o/a;

    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    invoke-direct {v4, v0}, Lcom/google/android/location/o/a;-><init>(Landroid/content/pm/PackageManager;)V

    const-string v0, "sensor"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/SensorManager;

    const/16 v1, 0x12

    invoke-static {v1}, Lcom/google/android/gms/common/util/ao;->a(I)Z

    move-result v1

    if-eqz v1, :cond_0

    new-instance v5, Lcom/google/android/location/activity/ab;

    invoke-direct {v5, v0}, Lcom/google/android/location/activity/ab;-><init>(Landroid/hardware/SensorManager;)V

    :goto_0
    const-string v0, "wifi"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/net/wifi/WifiManager;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    invoke-direct/range {v0 .. v6}, Lcom/google/android/location/geofencer/service/k;-><init>(Landroid/content/Context;Lcom/google/android/gms/common/util/p;Ljava/lang/Class;Lcom/google/android/location/o/a;Lcom/google/android/location/activity/at;Landroid/net/wifi/WifiManager;)V

    .line 222
    return-void

    .line 212
    :cond_0
    new-instance v5, Lcom/google/android/location/activity/ak;

    invoke-direct {v5}, Lcom/google/android/location/activity/ak;-><init>()V

    goto :goto_0
.end method

.method private constructor <init>(Landroid/content/Context;Lcom/google/android/gms/common/util/p;Ljava/lang/Class;Lcom/google/android/location/o/a;Lcom/google/android/location/activity/at;Landroid/net/wifi/WifiManager;)V
    .locals 9

    .prologue
    .line 237
    const-string v0, "GeofencerStateMachine"

    invoke-direct {p0, v0, p1}, Lcom/google/android/gms/common/util/a/c;-><init>(Ljava/lang/String;Landroid/content/Context;)V

    .line 157
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/location/geofencer/service/k;->u:Ljava/lang/Object;

    .line 168
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/location/geofencer/service/k;->A:Z

    .line 174
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/location/geofencer/service/k;->B:Ljava/util/List;

    .line 178
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/location/geofencer/service/k;->i:Z

    .line 238
    iput-object p1, p0, Lcom/google/android/location/geofencer/service/k;->w:Landroid/content/Context;

    .line 239
    iput-object p2, p0, Lcom/google/android/location/geofencer/service/k;->v:Lcom/google/android/gms/common/util/p;

    .line 240
    invoke-static {p1}, Lcom/google/android/location/n/ae;->a(Landroid/content/Context;)Lcom/google/android/location/n/ae;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/geofencer/service/k;->y:Lcom/google/android/location/n/ae;

    .line 241
    invoke-static {p1}, Lcom/google/android/gms/common/a/d;->a(Landroid/content/Context;)V

    .line 244
    new-instance v0, Lcom/google/android/location/geofencer/service/i;

    const/16 v1, 0x64

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    move-object v2, p2

    move-object v3, p1

    move-object v4, p0

    move-object v5, p3

    invoke-direct/range {v0 .. v8}, Lcom/google/android/location/geofencer/service/i;-><init>(ILcom/google/android/gms/common/util/p;Landroid/content/Context;Lcom/google/android/location/geofencer/service/k;Ljava/lang/Class;Lcom/google/android/location/geofencer/service/b;Lcom/google/android/location/geofencer/service/ab;Lcom/google/android/location/h/c;)V

    .line 249
    iput-object v0, p0, Lcom/google/android/location/geofencer/service/k;->x:Lcom/google/android/location/geofencer/service/i;

    .line 250
    new-instance v0, Lcom/google/android/location/geofencer/service/u;

    iget-object v1, p0, Lcom/google/android/location/geofencer/service/k;->x:Lcom/google/android/location/geofencer/service/i;

    invoke-direct {v0, p0, v1}, Lcom/google/android/location/geofencer/service/u;-><init>(Lcom/google/android/location/geofencer/service/k;Lcom/google/android/location/geofencer/service/i;)V

    iput-object v0, p0, Lcom/google/android/location/geofencer/service/k;->k:Lcom/google/android/location/geofencer/service/u;

    .line 251
    new-instance v0, Lcom/google/android/location/geofencer/service/q;

    iget-object v1, p0, Lcom/google/android/location/geofencer/service/k;->x:Lcom/google/android/location/geofencer/service/i;

    invoke-direct {v0, p0, v1}, Lcom/google/android/location/geofencer/service/q;-><init>(Lcom/google/android/location/geofencer/service/k;Lcom/google/android/location/geofencer/service/i;)V

    iput-object v0, p0, Lcom/google/android/location/geofencer/service/k;->m:Lcom/google/android/location/geofencer/service/q;

    .line 252
    new-instance v0, Lcom/google/android/location/geofencer/service/t;

    iget-object v1, p0, Lcom/google/android/location/geofencer/service/k;->x:Lcom/google/android/location/geofencer/service/i;

    invoke-direct {v0, p0, v1}, Lcom/google/android/location/geofencer/service/t;-><init>(Lcom/google/android/location/geofencer/service/k;Lcom/google/android/location/geofencer/service/i;)V

    iput-object v0, p0, Lcom/google/android/location/geofencer/service/k;->l:Lcom/google/android/location/geofencer/service/t;

    .line 253
    new-instance v0, Lcom/google/android/location/geofencer/service/s;

    iget-object v1, p0, Lcom/google/android/location/geofencer/service/k;->x:Lcom/google/android/location/geofencer/service/i;

    invoke-direct {v0, p0, v1}, Lcom/google/android/location/geofencer/service/s;-><init>(Lcom/google/android/location/geofencer/service/k;Lcom/google/android/location/geofencer/service/i;)V

    iput-object v0, p0, Lcom/google/android/location/geofencer/service/k;->n:Lcom/google/android/location/geofencer/service/s;

    .line 254
    new-instance v0, Lcom/google/android/location/geofencer/service/x;

    iget-object v1, p0, Lcom/google/android/location/geofencer/service/k;->x:Lcom/google/android/location/geofencer/service/i;

    invoke-direct {v0, p0, v1}, Lcom/google/android/location/geofencer/service/x;-><init>(Lcom/google/android/location/geofencer/service/k;Lcom/google/android/location/geofencer/service/i;)V

    iput-object v0, p0, Lcom/google/android/location/geofencer/service/k;->o:Lcom/google/android/location/geofencer/service/x;

    .line 255
    new-instance v0, Lcom/google/android/location/geofencer/service/y;

    iget-object v1, p0, Lcom/google/android/location/geofencer/service/k;->x:Lcom/google/android/location/geofencer/service/i;

    invoke-direct {v0, p0, v1}, Lcom/google/android/location/geofencer/service/y;-><init>(Lcom/google/android/location/geofencer/service/k;Lcom/google/android/location/geofencer/service/i;)V

    iput-object v0, p0, Lcom/google/android/location/geofencer/service/k;->p:Lcom/google/android/location/geofencer/service/y;

    .line 256
    new-instance v0, Lcom/google/android/location/geofencer/service/w;

    iget-object v1, p0, Lcom/google/android/location/geofencer/service/k;->x:Lcom/google/android/location/geofencer/service/i;

    invoke-direct {v0, p0, v1, p5, p6}, Lcom/google/android/location/geofencer/service/w;-><init>(Lcom/google/android/location/geofencer/service/k;Lcom/google/android/location/geofencer/service/i;Lcom/google/android/location/activity/at;Landroid/net/wifi/WifiManager;)V

    iput-object v0, p0, Lcom/google/android/location/geofencer/service/k;->q:Lcom/google/android/location/geofencer/service/w;

    .line 257
    new-instance v0, Lcom/google/android/location/geofencer/service/z;

    iget-object v1, p0, Lcom/google/android/location/geofencer/service/k;->x:Lcom/google/android/location/geofencer/service/i;

    invoke-direct {v0, p0, v1}, Lcom/google/android/location/geofencer/service/z;-><init>(Lcom/google/android/location/geofencer/service/k;Lcom/google/android/location/geofencer/service/i;)V

    iput-object v0, p0, Lcom/google/android/location/geofencer/service/k;->r:Lcom/google/android/location/geofencer/service/z;

    .line 258
    new-instance v0, Lcom/google/android/location/geofencer/service/r;

    iget-object v1, p0, Lcom/google/android/location/geofencer/service/k;->x:Lcom/google/android/location/geofencer/service/i;

    invoke-direct {v0, p0, v1}, Lcom/google/android/location/geofencer/service/r;-><init>(Lcom/google/android/location/geofencer/service/k;Lcom/google/android/location/geofencer/service/i;)V

    iput-object v0, p0, Lcom/google/android/location/geofencer/service/k;->s:Lcom/google/android/location/geofencer/service/r;

    .line 259
    new-instance v0, Lcom/google/android/location/geofencer/service/n;

    iget-object v1, p0, Lcom/google/android/location/geofencer/service/k;->x:Lcom/google/android/location/geofencer/service/i;

    invoke-direct {v0, p0, v1}, Lcom/google/android/location/geofencer/service/n;-><init>(Lcom/google/android/location/geofencer/service/k;Lcom/google/android/location/geofencer/service/i;)V

    iput-object v0, p0, Lcom/google/android/location/geofencer/service/k;->t:Lcom/google/android/location/geofencer/service/n;

    .line 261
    sget-boolean v0, Lcom/google/android/location/geofencer/a/a;->a:Z

    invoke-virtual {p0, v0}, Lcom/google/android/location/geofencer/service/k;->b(Z)V

    .line 262
    sget-boolean v0, Lcom/google/android/location/geofencer/a/a;->a:Z

    if-eqz v0, :cond_0

    const/16 v0, 0x64

    :goto_0
    invoke-virtual {p0, v0}, Lcom/google/android/location/geofencer/service/k;->a(I)V

    .line 267
    new-instance v0, Lcom/google/android/location/geofencer/service/v;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/android/location/geofencer/service/v;-><init>(Lcom/google/android/location/geofencer/service/k;B)V

    iput-object v0, p0, Lcom/google/android/location/geofencer/service/k;->h:Landroid/content/BroadcastReceiver;

    .line 268
    iget-object v0, p0, Lcom/google/android/location/geofencer/service/k;->x:Lcom/google/android/location/geofencer/service/i;

    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    const-string v1, "android.intent.action.ACTION_POWER_CONNECTED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v1, "android.intent.action.ACTION_POWER_DISCONNECTED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v1, "android.intent.action.SCREEN_OFF"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v1, "android.intent.action.SCREEN_ON"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/location/geofencer/service/k;->z:Landroid/content/IntentFilter;

    .line 271
    iget-object v0, p0, Lcom/google/android/location/geofencer/service/k;->k:Lcom/google/android/location/geofencer/service/u;

    invoke-virtual {p0, v0}, Lcom/google/android/location/geofencer/service/k;->a(Lcom/google/android/gms/common/util/a/b;)V

    .line 272
    iget-object v0, p0, Lcom/google/android/location/geofencer/service/k;->m:Lcom/google/android/location/geofencer/service/q;

    invoke-virtual {p0, v0}, Lcom/google/android/location/geofencer/service/k;->a(Lcom/google/android/gms/common/util/a/b;)V

    .line 273
    iget-object v0, p0, Lcom/google/android/location/geofencer/service/k;->l:Lcom/google/android/location/geofencer/service/t;

    invoke-virtual {p0, v0}, Lcom/google/android/location/geofencer/service/k;->a(Lcom/google/android/gms/common/util/a/b;)V

    .line 274
    iget-object v0, p0, Lcom/google/android/location/geofencer/service/k;->n:Lcom/google/android/location/geofencer/service/s;

    invoke-virtual {p0, v0}, Lcom/google/android/location/geofencer/service/k;->a(Lcom/google/android/gms/common/util/a/b;)V

    .line 275
    iget-object v0, p0, Lcom/google/android/location/geofencer/service/k;->o:Lcom/google/android/location/geofencer/service/x;

    invoke-virtual {p0, v0}, Lcom/google/android/location/geofencer/service/k;->a(Lcom/google/android/gms/common/util/a/b;)V

    .line 276
    iget-object v0, p0, Lcom/google/android/location/geofencer/service/k;->p:Lcom/google/android/location/geofencer/service/y;

    invoke-virtual {p0, v0}, Lcom/google/android/location/geofencer/service/k;->a(Lcom/google/android/gms/common/util/a/b;)V

    .line 277
    iget-object v0, p0, Lcom/google/android/location/geofencer/service/k;->q:Lcom/google/android/location/geofencer/service/w;

    invoke-virtual {p0, v0}, Lcom/google/android/location/geofencer/service/k;->a(Lcom/google/android/gms/common/util/a/b;)V

    .line 278
    iget-object v0, p0, Lcom/google/android/location/geofencer/service/k;->r:Lcom/google/android/location/geofencer/service/z;

    invoke-virtual {p0, v0}, Lcom/google/android/location/geofencer/service/k;->a(Lcom/google/android/gms/common/util/a/b;)V

    .line 279
    iget-object v0, p0, Lcom/google/android/location/geofencer/service/k;->s:Lcom/google/android/location/geofencer/service/r;

    invoke-virtual {p0, v0}, Lcom/google/android/location/geofencer/service/k;->a(Lcom/google/android/gms/common/util/a/b;)V

    .line 280
    iget-object v0, p0, Lcom/google/android/location/geofencer/service/k;->t:Lcom/google/android/location/geofencer/service/n;

    invoke-virtual {p0, v0}, Lcom/google/android/location/geofencer/service/k;->a(Lcom/google/android/gms/common/util/a/b;)V

    .line 281
    iget-object v0, p0, Lcom/google/android/location/geofencer/service/k;->k:Lcom/google/android/location/geofencer/service/u;

    invoke-virtual {p0, v0}, Lcom/google/android/location/geofencer/service/k;->b(Lcom/google/android/gms/common/util/a/b;)V

    .line 284
    new-instance v0, Lcom/google/android/location/geofencer/service/l;

    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    invoke-direct {v0, p0, v1}, Lcom/google/android/location/geofencer/service/l;-><init>(Lcom/google/android/location/geofencer/service/k;Landroid/os/Handler;)V

    .line 291
    new-instance v1, Lcom/google/android/location/geofencer/service/af;

    invoke-direct {v1, p1, v0}, Lcom/google/android/location/geofencer/service/af;-><init>(Landroid/content/Context;Landroid/database/ContentObserver;)V

    .line 293
    iput-object v1, p0, Lcom/google/android/location/geofencer/service/k;->C:Lcom/google/android/location/geofencer/service/af;

    .line 294
    iput-object p4, p0, Lcom/google/android/location/geofencer/service/k;->D:Lcom/google/android/location/o/a;

    .line 295
    return-void

    .line 262
    :cond_0
    const/16 v0, 0xa

    goto :goto_0
.end method

.method static synthetic a(Lcom/google/android/location/geofencer/service/k;)Landroid/content/IntentFilter;
    .locals 1

    .prologue
    .line 116
    iget-object v0, p0, Lcom/google/android/location/geofencer/service/k;->z:Landroid/content/IntentFilter;

    return-object v0
.end method

.method public static a(Landroid/content/Context;)Lcom/google/android/location/geofencer/service/k;
    .locals 3

    .prologue
    .line 195
    const-class v1, Lcom/google/android/location/geofencer/service/k;

    monitor-enter v1

    .line 196
    :try_start_0
    sget-object v0, Lcom/google/android/location/geofencer/service/k;->j:Lcom/google/android/location/geofencer/service/k;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/google/android/location/geofencer/service/k;->j:Lcom/google/android/location/geofencer/service/k;

    invoke-direct {v0}, Lcom/google/android/location/geofencer/service/k;->j()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 197
    :cond_0
    const-string v0, "GeofencerStateMachine"

    const-string v2, "Creating GeofencerStateMachine"

    invoke-static {v0, v2}, Lcom/google/android/location/geofencer/a/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 198
    new-instance v0, Lcom/google/android/location/geofencer/service/k;

    invoke-static {}, Lcom/google/android/gms/common/util/r;->c()Lcom/google/android/gms/common/util/p;

    move-result-object v2

    invoke-direct {v0, p0, v2}, Lcom/google/android/location/geofencer/service/k;-><init>(Landroid/content/Context;Lcom/google/android/gms/common/util/p;)V

    .line 199
    sput-object v0, Lcom/google/android/location/geofencer/service/k;->j:Lcom/google/android/location/geofencer/service/k;

    invoke-virtual {v0}, Lcom/google/android/location/geofencer/service/k;->d()V

    .line 201
    :cond_1
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 202
    sget-object v0, Lcom/google/android/location/geofencer/service/k;->j:Lcom/google/android/location/geofencer/service/k;

    return-object v0

    .line 201
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method static synthetic a(Lcom/google/android/location/geofencer/service/k;Landroid/content/Intent;)V
    .locals 2

    .prologue
    .line 116
    const-string v0, "android.intent.extra.DATA_REMOVED"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_1

    sget-boolean v0, Lcom/google/android/location/geofencer/a/a;->a:Z

    if-eqz v0, :cond_0

    const-string v0, "GeofencerStateMachine"

    const-string v1, "Package removed."

    invoke-static {v0, v1}, Lcom/google/android/location/geofencer/a/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    invoke-direct {p0, p1}, Lcom/google/android/location/geofencer/service/k;->c(Landroid/content/Intent;)V

    :cond_1
    return-void
.end method

.method static synthetic a(Lcom/google/android/location/geofencer/service/k;Landroid/os/Message;)V
    .locals 0

    .prologue
    .line 116
    invoke-virtual {p0, p1}, Lcom/google/android/location/geofencer/service/k;->a(Landroid/os/Message;)V

    return-void
.end method

.method static synthetic a(Lcom/google/android/location/geofencer/service/k;Lcom/google/android/gms/common/util/a/a;)V
    .locals 0

    .prologue
    .line 116
    invoke-virtual {p0, p1}, Lcom/google/android/location/geofencer/service/k;->a(Lcom/google/android/gms/common/util/a/a;)V

    return-void
.end method

.method static synthetic b(Lcom/google/android/location/geofencer/service/k;)V
    .locals 3

    .prologue
    .line 116
    iget-object v1, p0, Lcom/google/android/location/geofencer/service/k;->u:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/location/geofencer/service/k;->i:Z

    if-eqz v0, :cond_0

    const-string v0, "GeofencerStateMachine"

    const-string v2, "SM quitted, ignoring sendUserSwitched."

    invoke-static {v0, v2}, Lcom/google/android/location/geofencer/a/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    monitor-exit v1

    :goto_0
    return-void

    :cond_0
    const-string v0, "GeofencerStateMachine"

    const/4 v2, 0x3

    invoke-static {v0, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "GeofencerStateMachine"

    const-string v2, "sendUserSwitched"

    invoke-static {v0, v2}, Lcom/google/android/location/geofencer/a/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    const/16 v0, 0xb

    invoke-virtual {p0, v0}, Lcom/google/android/location/geofencer/service/k;->c(I)V

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method static synthetic b(Lcom/google/android/location/geofencer/service/k;Landroid/content/Intent;)V
    .locals 0

    .prologue
    .line 116
    invoke-direct {p0, p1}, Lcom/google/android/location/geofencer/service/k;->c(Landroid/content/Intent;)V

    return-void
.end method

.method static synthetic b(Lcom/google/android/location/geofencer/service/k;Landroid/os/Message;)V
    .locals 0

    .prologue
    .line 116
    invoke-virtual {p0, p1}, Lcom/google/android/location/geofencer/service/k;->a(Landroid/os/Message;)V

    return-void
.end method

.method static synthetic b(Lcom/google/android/location/geofencer/service/k;Lcom/google/android/gms/common/util/a/a;)V
    .locals 0

    .prologue
    .line 116
    invoke-virtual {p0, p1}, Lcom/google/android/location/geofencer/service/k;->a(Lcom/google/android/gms/common/util/a/a;)V

    return-void
.end method

.method static synthetic c(Lcom/google/android/location/geofencer/service/k;)Lcom/google/android/gms/common/util/a/a;
    .locals 1

    .prologue
    .line 116
    invoke-virtual {p0}, Lcom/google/android/location/geofencer/service/k;->a()Lcom/google/android/gms/common/util/a/a;

    move-result-object v0

    return-object v0
.end method

.method private c(Landroid/content/Intent;)V
    .locals 4

    .prologue
    .line 419
    invoke-virtual {p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    .line 420
    if-nez v0, :cond_1

    .line 434
    :cond_0
    :goto_0
    return-void

    .line 424
    :cond_1
    invoke-virtual {v0}, Landroid/net/Uri;->getSchemeSpecificPart()Ljava/lang/String;

    move-result-object v0

    .line 425
    if-eqz v0, :cond_0

    .line 428
    sget-boolean v1, Lcom/google/android/location/geofencer/a/a;->a:Z

    if-eqz v1, :cond_2

    .line 429
    const-string v1, "GeofencerStateMachine"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "App data cleared. Removing geofences from: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/location/geofencer/a/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 431
    :cond_2
    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/google/android/location/geofencer/service/an;->a(Ljava/lang/String;Lcom/google/android/location/geofencer/service/f;)Lcom/google/android/location/geofencer/service/an;

    move-result-object v0

    .line 433
    invoke-virtual {p0, v0}, Lcom/google/android/location/geofencer/service/k;->a(Lcom/google/android/location/geofencer/service/an;)V

    goto :goto_0
.end method

.method static synthetic c(Lcom/google/android/location/geofencer/service/k;Landroid/os/Message;)V
    .locals 0

    .prologue
    .line 116
    invoke-virtual {p0, p1}, Lcom/google/android/location/geofencer/service/k;->a(Landroid/os/Message;)V

    return-void
.end method

.method static synthetic c(Lcom/google/android/location/geofencer/service/k;Lcom/google/android/gms/common/util/a/a;)V
    .locals 0

    .prologue
    .line 116
    invoke-virtual {p0, p1}, Lcom/google/android/location/geofencer/service/k;->a(Lcom/google/android/gms/common/util/a/a;)V

    return-void
.end method

.method static synthetic d(Lcom/google/android/location/geofencer/service/k;)Lcom/google/android/location/n/ae;
    .locals 1

    .prologue
    .line 116
    iget-object v0, p0, Lcom/google/android/location/geofencer/service/k;->y:Lcom/google/android/location/n/ae;

    return-object v0
.end method

.method static synthetic d(Lcom/google/android/location/geofencer/service/k;Lcom/google/android/gms/common/util/a/a;)V
    .locals 0

    .prologue
    .line 116
    invoke-virtual {p0, p1}, Lcom/google/android/location/geofencer/service/k;->a(Lcom/google/android/gms/common/util/a/a;)V

    return-void
.end method

.method public static e()Lcom/google/android/location/geofencer/service/k;
    .locals 2

    .prologue
    .line 206
    const-class v1, Lcom/google/android/location/geofencer/service/k;

    monitor-enter v1

    .line 207
    :try_start_0
    sget-object v0, Lcom/google/android/location/geofencer/service/k;->j:Lcom/google/android/location/geofencer/service/k;

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v0

    .line 208
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method static synthetic e(Lcom/google/android/location/geofencer/service/k;)Lcom/google/android/location/geofencer/service/x;
    .locals 1

    .prologue
    .line 116
    iget-object v0, p0, Lcom/google/android/location/geofencer/service/k;->o:Lcom/google/android/location/geofencer/service/x;

    return-object v0
.end method

.method static synthetic e(Lcom/google/android/location/geofencer/service/k;Lcom/google/android/gms/common/util/a/a;)V
    .locals 0

    .prologue
    .line 116
    invoke-virtual {p0, p1}, Lcom/google/android/location/geofencer/service/k;->a(Lcom/google/android/gms/common/util/a/a;)V

    return-void
.end method

.method static synthetic f(Lcom/google/android/location/geofencer/service/k;)Lcom/google/android/location/geofencer/service/y;
    .locals 1

    .prologue
    .line 116
    iget-object v0, p0, Lcom/google/android/location/geofencer/service/k;->p:Lcom/google/android/location/geofencer/service/y;

    return-object v0
.end method

.method static synthetic f(Lcom/google/android/location/geofencer/service/k;Lcom/google/android/gms/common/util/a/a;)V
    .locals 0

    .prologue
    .line 116
    invoke-virtual {p0, p1}, Lcom/google/android/location/geofencer/service/k;->a(Lcom/google/android/gms/common/util/a/a;)V

    return-void
.end method

.method static synthetic g(Lcom/google/android/location/geofencer/service/k;)Lcom/google/android/location/geofencer/service/w;
    .locals 1

    .prologue
    .line 116
    iget-object v0, p0, Lcom/google/android/location/geofencer/service/k;->q:Lcom/google/android/location/geofencer/service/w;

    return-object v0
.end method

.method static synthetic g(Lcom/google/android/location/geofencer/service/k;Lcom/google/android/gms/common/util/a/a;)V
    .locals 0

    .prologue
    .line 116
    invoke-virtual {p0, p1}, Lcom/google/android/location/geofencer/service/k;->a(Lcom/google/android/gms/common/util/a/a;)V

    return-void
.end method

.method static synthetic h(Lcom/google/android/location/geofencer/service/k;)Lcom/google/android/location/geofencer/service/z;
    .locals 1

    .prologue
    .line 116
    iget-object v0, p0, Lcom/google/android/location/geofencer/service/k;->r:Lcom/google/android/location/geofencer/service/z;

    return-object v0
.end method

.method static synthetic h(Lcom/google/android/location/geofencer/service/k;Lcom/google/android/gms/common/util/a/a;)V
    .locals 0

    .prologue
    .line 116
    invoke-virtual {p0, p1}, Lcom/google/android/location/geofencer/service/k;->a(Lcom/google/android/gms/common/util/a/a;)V

    return-void
.end method

.method static synthetic i(Lcom/google/android/location/geofencer/service/k;)Lcom/google/android/location/geofencer/service/r;
    .locals 1

    .prologue
    .line 116
    iget-object v0, p0, Lcom/google/android/location/geofencer/service/k;->s:Lcom/google/android/location/geofencer/service/r;

    return-object v0
.end method

.method static synthetic i(Lcom/google/android/location/geofencer/service/k;Lcom/google/android/gms/common/util/a/a;)V
    .locals 0

    .prologue
    .line 116
    invoke-virtual {p0, p1}, Lcom/google/android/location/geofencer/service/k;->a(Lcom/google/android/gms/common/util/a/a;)V

    return-void
.end method

.method static synthetic j(Lcom/google/android/location/geofencer/service/k;)Lcom/google/android/location/geofencer/service/n;
    .locals 1

    .prologue
    .line 116
    iget-object v0, p0, Lcom/google/android/location/geofencer/service/k;->t:Lcom/google/android/location/geofencer/service/n;

    return-object v0
.end method

.method static synthetic j(Lcom/google/android/location/geofencer/service/k;Lcom/google/android/gms/common/util/a/a;)V
    .locals 0

    .prologue
    .line 116
    invoke-virtual {p0, p1}, Lcom/google/android/location/geofencer/service/k;->a(Lcom/google/android/gms/common/util/a/a;)V

    return-void
.end method

.method private j()Z
    .locals 2

    .prologue
    .line 496
    iget-object v1, p0, Lcom/google/android/location/geofencer/service/k;->u:Ljava/lang/Object;

    monitor-enter v1

    .line 497
    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/location/geofencer/service/k;->i:Z

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return v0

    .line 498
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method static synthetic k(Lcom/google/android/location/geofencer/service/k;)Lcom/google/android/location/geofencer/service/af;
    .locals 1

    .prologue
    .line 116
    iget-object v0, p0, Lcom/google/android/location/geofencer/service/k;->C:Lcom/google/android/location/geofencer/service/af;

    return-object v0
.end method

.method static synthetic k(Lcom/google/android/location/geofencer/service/k;Lcom/google/android/gms/common/util/a/a;)V
    .locals 0

    .prologue
    .line 116
    invoke-virtual {p0, p1}, Lcom/google/android/location/geofencer/service/k;->a(Lcom/google/android/gms/common/util/a/a;)V

    return-void
.end method

.method static synthetic l(Lcom/google/android/location/geofencer/service/k;)Lcom/google/android/location/geofencer/service/s;
    .locals 1

    .prologue
    .line 116
    iget-object v0, p0, Lcom/google/android/location/geofencer/service/k;->n:Lcom/google/android/location/geofencer/service/s;

    return-object v0
.end method

.method static synthetic l(Lcom/google/android/location/geofencer/service/k;Lcom/google/android/gms/common/util/a/a;)V
    .locals 0

    .prologue
    .line 116
    invoke-virtual {p0, p1}, Lcom/google/android/location/geofencer/service/k;->a(Lcom/google/android/gms/common/util/a/a;)V

    return-void
.end method

.method static synthetic m(Lcom/google/android/location/geofencer/service/k;)V
    .locals 1

    .prologue
    .line 116
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lcom/google/android/location/geofencer/service/k;->d(I)V

    return-void
.end method

.method static synthetic m(Lcom/google/android/location/geofencer/service/k;Lcom/google/android/gms/common/util/a/a;)V
    .locals 0

    .prologue
    .line 116
    invoke-virtual {p0, p1}, Lcom/google/android/location/geofencer/service/k;->a(Lcom/google/android/gms/common/util/a/a;)V

    return-void
.end method

.method static synthetic n(Lcom/google/android/location/geofencer/service/k;)Lcom/google/android/location/geofencer/service/t;
    .locals 1

    .prologue
    .line 116
    iget-object v0, p0, Lcom/google/android/location/geofencer/service/k;->l:Lcom/google/android/location/geofencer/service/t;

    return-object v0
.end method

.method static synthetic o(Lcom/google/android/location/geofencer/service/k;)Lcom/google/android/location/geofencer/service/q;
    .locals 1

    .prologue
    .line 116
    iget-object v0, p0, Lcom/google/android/location/geofencer/service/k;->m:Lcom/google/android/location/geofencer/service/q;

    return-object v0
.end method

.method static synthetic p(Lcom/google/android/location/geofencer/service/k;)V
    .locals 1

    .prologue
    .line 116
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lcom/google/android/location/geofencer/service/k;->d(I)V

    return-void
.end method

.method static synthetic q(Lcom/google/android/location/geofencer/service/k;)V
    .locals 1

    .prologue
    .line 116
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lcom/google/android/location/geofencer/service/k;->d(I)V

    return-void
.end method

.method static synthetic r(Lcom/google/android/location/geofencer/service/k;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 116
    iget-object v0, p0, Lcom/google/android/location/geofencer/service/k;->w:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic s(Lcom/google/android/location/geofencer/service/k;)Lcom/google/android/location/o/a;
    .locals 1

    .prologue
    .line 116
    iget-object v0, p0, Lcom/google/android/location/geofencer/service/k;->D:Lcom/google/android/location/o/a;

    return-object v0
.end method

.method static synthetic t(Lcom/google/android/location/geofencer/service/k;)V
    .locals 1

    .prologue
    .line 116
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lcom/google/android/location/geofencer/service/k;->d(I)V

    return-void
.end method

.method static synthetic u(Lcom/google/android/location/geofencer/service/k;)Lcom/google/android/gms/common/util/p;
    .locals 1

    .prologue
    .line 116
    iget-object v0, p0, Lcom/google/android/location/geofencer/service/k;->v:Lcom/google/android/gms/common/util/p;

    return-object v0
.end method


# virtual methods
.method public final a(Landroid/content/Intent;)V
    .locals 4

    .prologue
    .line 534
    iget-object v1, p0, Lcom/google/android/location/geofencer/service/k;->u:Ljava/lang/Object;

    monitor-enter v1

    .line 535
    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/location/geofencer/service/k;->A:Z

    if-eqz v0, :cond_0

    .line 536
    const-string v0, "GeofencerStateMachine"

    const-string v2, "sendInitialize called more than once."

    invoke-static {v0, v2}, Lcom/google/android/location/geofencer/a/a;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 537
    monitor-exit v1

    .line 556
    :goto_0
    return-void

    .line 539
    :cond_0
    iget-boolean v0, p0, Lcom/google/android/location/geofencer/service/k;->i:Z

    if-eqz v0, :cond_2

    .line 540
    const-string v0, "GeofencerStateMachine"

    const-string v2, "SM quit, ignoring sendInitialize."

    invoke-static {v0, v2}, Lcom/google/android/location/geofencer/a/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 543
    iget-object v0, p0, Lcom/google/android/location/geofencer/service/k;->B:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/geofencer/service/e;

    .line 544
    const/16 v3, 0x3e8

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/android/location/geofencer/service/e;->a(Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    .line 556
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 546
    :cond_1
    :try_start_1
    monitor-exit v1

    goto :goto_0

    .line 548
    :cond_2
    const-string v0, "GeofencerStateMachine"

    const/4 v2, 0x3

    invoke-static {v0, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 549
    const-string v0, "GeofencerStateMachine"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "sendInitialize: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/google/android/location/geofencer/a/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 551
    :cond_3
    const/4 v0, 0x2

    invoke-virtual {p0, v0, p1}, Lcom/google/android/location/geofencer/service/k;->b(ILjava/lang/Object;)V

    .line 552
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/location/geofencer/service/k;->A:Z

    .line 553
    iget-object v0, p0, Lcom/google/android/location/geofencer/service/k;->B:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/geofencer/service/e;

    .line 554
    invoke-virtual {v0, p0}, Lcom/google/android/location/geofencer/service/e;->a(Lcom/google/android/gms/common/util/a/c;)V

    goto :goto_2

    .line 556
    :cond_4
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0
.end method

.method public final a(Landroid/location/Location;)V
    .locals 4

    .prologue
    .line 642
    iget-object v1, p0, Lcom/google/android/location/geofencer/service/k;->u:Ljava/lang/Object;

    monitor-enter v1

    .line 643
    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/location/geofencer/service/k;->i:Z

    if-eqz v0, :cond_0

    .line 644
    const-string v0, "GeofencerStateMachine"

    const-string v2, "SM quitted, ignoring sendNewLocation."

    invoke-static {v0, v2}, Lcom/google/android/location/geofencer/a/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 645
    monitor-exit v1

    .line 651
    :goto_0
    return-void

    .line 647
    :cond_0
    const-string v0, "GeofencerStateMachine"

    const/4 v2, 0x3

    invoke-static {v0, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 648
    const-string v0, "GeofencerStateMachine"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "sendNewLocation: location="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/google/android/location/geofencer/a/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 650
    :cond_1
    const/4 v0, 0x6

    iget-object v2, p0, Lcom/google/android/location/geofencer/service/k;->v:Lcom/google/android/gms/common/util/p;

    invoke-interface {v2}, Lcom/google/android/gms/common/util/p;->b()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-static {v2, p1}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v2

    invoke-virtual {p0, v0, v2}, Lcom/google/android/location/geofencer/service/k;->b(ILjava/lang/Object;)V

    .line 651
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final a(Lcom/google/android/gms/location/GeofencingRequest;Lcom/google/android/location/geofencer/service/f;Landroid/app/PendingIntent;)V
    .locals 4

    .prologue
    .line 586
    iget-object v1, p0, Lcom/google/android/location/geofencer/service/k;->u:Ljava/lang/Object;

    monitor-enter v1

    .line 587
    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/location/geofencer/service/k;->i:Z

    if-eqz v0, :cond_0

    .line 588
    const-string v0, "GeofencerStateMachine"

    const-string v2, "SM quit, ignoring addGeofences."

    invoke-static {v0, v2}, Lcom/google/android/location/geofencer/a/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 589
    const/16 v0, 0x3e8

    invoke-static {p2, v0, p1}, Lcom/google/android/location/geofencer/service/a;->a(Lcom/google/android/location/geofencer/service/f;ILcom/google/android/gms/location/GeofencingRequest;)V

    .line 591
    monitor-exit v1

    .line 607
    :goto_0
    return-void

    .line 593
    :cond_0
    const-string v0, "GeofencerStateMachine"

    const/4 v2, 0x3

    invoke-static {v0, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 594
    const-string v0, "GeofencerStateMachine"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "addGeofences: geofencingRequest="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " intent="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/google/android/location/geofencer/a/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 597
    :cond_1
    new-instance v0, Lcom/google/android/location/geofencer/service/a;

    invoke-direct {v0, p1, p3, p2}, Lcom/google/android/location/geofencer/service/a;-><init>(Lcom/google/android/gms/location/GeofencingRequest;Landroid/app/PendingIntent;Lcom/google/android/location/geofencer/service/f;)V

    .line 598
    iget-boolean v2, p0, Lcom/google/android/location/geofencer/service/k;->A:Z

    if-eqz v2, :cond_2

    .line 599
    invoke-virtual {v0, p0}, Lcom/google/android/location/geofencer/service/a;->a(Lcom/google/android/gms/common/util/a/c;)V

    .line 607
    :goto_1
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 601
    :cond_2
    :try_start_1
    sget-boolean v2, Lcom/google/android/location/geofencer/a/a;->a:Z

    if-eqz v2, :cond_3

    .line 602
    const-string v2, "GeofencerStateMachine"

    const-string v3, "State machine not initialized, putting request to pending requests."

    invoke-static {v2, v3}, Lcom/google/android/location/geofencer/a/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 605
    :cond_3
    iget-object v2, p0, Lcom/google/android/location/geofencer/service/k;->B:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1
.end method

.method public final a(Lcom/google/android/location/geofencer/service/an;)V
    .locals 4

    .prologue
    .line 615
    iget-object v1, p0, Lcom/google/android/location/geofencer/service/k;->u:Ljava/lang/Object;

    monitor-enter v1

    .line 616
    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/location/geofencer/service/k;->i:Z

    if-eqz v0, :cond_0

    .line 617
    const-string v0, "GeofencerStateMachine"

    const-string v2, "SM quitted, ignoring removeGeofences."

    invoke-static {v0, v2}, Lcom/google/android/location/geofencer/a/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 618
    const/16 v0, 0x3e8

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/google/android/location/geofencer/service/an;->a(Ljava/lang/Object;)V

    .line 619
    monitor-exit v1

    .line 633
    :goto_0
    return-void

    .line 621
    :cond_0
    const-string v0, "GeofencerStateMachine"

    const/4 v2, 0x3

    invoke-static {v0, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 622
    const-string v0, "GeofencerStateMachine"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "removeGeofences: removeRequest="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/google/android/location/geofencer/a/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 624
    :cond_1
    iget-boolean v0, p0, Lcom/google/android/location/geofencer/service/k;->A:Z

    if-eqz v0, :cond_2

    .line 625
    invoke-virtual {p1, p0}, Lcom/google/android/location/geofencer/service/an;->a(Lcom/google/android/gms/common/util/a/c;)V

    .line 633
    :goto_1
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 627
    :cond_2
    :try_start_1
    sget-boolean v0, Lcom/google/android/location/geofencer/a/a;->a:Z

    if-eqz v0, :cond_3

    .line 628
    const-string v0, "GeofencerStateMachine"

    const-string v2, "State machine not initialized, putting request to pending requests."

    invoke-static {v0, v2}, Lcom/google/android/location/geofencer/a/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 631
    :cond_3
    iget-object v0, p0, Lcom/google/android/location/geofencer/service/k;->B:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1
.end method

.method public final a(Lcom/google/android/location/h/e;Lcom/google/android/location/h/e;)V
    .locals 4

    .prologue
    .line 661
    iget-object v1, p0, Lcom/google/android/location/geofencer/service/k;->u:Ljava/lang/Object;

    monitor-enter v1

    .line 662
    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/location/geofencer/service/k;->i:Z

    if-eqz v0, :cond_0

    .line 663
    const-string v0, "GeofencerStateMachine"

    const-string v2, "SM quitted, ignoring sendMovementChange."

    invoke-static {v0, v2}, Lcom/google/android/location/geofencer/a/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 664
    monitor-exit v1

    .line 671
    :goto_0
    return-void

    .line 666
    :cond_0
    const-string v0, "GeofencerStateMachine"

    const/4 v2, 0x3

    invoke-static {v0, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 667
    const-string v0, "GeofencerStateMachine"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "sendMovementChange: previousMovement="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ",currentMovement="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/google/android/location/geofencer/a/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 670
    :cond_1
    const/4 v0, 0x7

    invoke-static {p1, p2}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v2

    invoke-virtual {p0, v0, v2}, Lcom/google/android/location/geofencer/service/k;->b(ILjava/lang/Object;)V

    .line 671
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method protected final b(I)Ljava/lang/String;
    .locals 2

    .prologue
    .line 455
    sparse-switch p1, :sswitch_data_0

    .line 491
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " (Message not named in getWhatToString)"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    .line 457
    :sswitch_0
    const-string v0, "SM_QUERY_LOCATION_OPT_IN_CMD"

    goto :goto_0

    .line 459
    :sswitch_1
    const-string v0, "SM_INITIALIZE_CMD"

    goto :goto_0

    .line 461
    :sswitch_2
    const-string v0, "SM_STOP_CMD"

    goto :goto_0

    .line 463
    :sswitch_3
    const-string v0, "SM_ADD_GEOFENCE_LIST_CMD"

    goto :goto_0

    .line 465
    :sswitch_4
    const-string v0, "SM_REMOVE_GEOFENCE_CMD"

    goto :goto_0

    .line 467
    :sswitch_5
    const-string v0, "SM_LOCATION_CMD"

    goto :goto_0

    .line 469
    :sswitch_6
    const-string v0, "SM_ACTIVITY_CMD"

    goto :goto_0

    .line 471
    :sswitch_7
    const-string v0, "SM_UPDATE_DETECTOR_REQUIREMENT_CMD"

    goto :goto_0

    .line 473
    :sswitch_8
    const-string v0, "SM_SYSTEM_EVENT_CMD"

    goto :goto_0

    .line 475
    :sswitch_9
    const-string v0, "SM_DUMP_CMD"

    goto :goto_0

    .line 477
    :sswitch_a
    const-string v0, "SM_SEND_AND_WAIT_FOR_TEST_CMD"

    goto :goto_0

    .line 479
    :sswitch_b
    const-string v0, "SM_SAVE_ACTIVITY_STATE_CMD"

    goto :goto_0

    .line 481
    :sswitch_c
    const-string v0, "SM_USER_SWITCH_CMD"

    goto :goto_0

    .line 483
    :sswitch_d
    const-string v0, "SM_HARDWARE_GEOFENCE_CHANGED_CMD"

    goto :goto_0

    .line 485
    :sswitch_e
    const-string v0, "SM_HARDWARE_GEFOENCE_AVAILABILITY_CMD"

    goto :goto_0

    .line 487
    :sswitch_f
    const-string v0, "SM_AR_LOW_POWER_MODE"

    goto :goto_0

    .line 489
    :sswitch_10
    const-string v0, "SM_SIGNIFICANT_MOTION_CMD"

    goto :goto_0

    .line 455
    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x2 -> :sswitch_1
        0x3 -> :sswitch_2
        0x4 -> :sswitch_3
        0x5 -> :sswitch_4
        0x6 -> :sswitch_5
        0x7 -> :sswitch_6
        0x8 -> :sswitch_7
        0x9 -> :sswitch_8
        0xa -> :sswitch_b
        0xb -> :sswitch_c
        0xc -> :sswitch_d
        0xd -> :sswitch_e
        0xe -> :sswitch_f
        0xf -> :sswitch_10
        0x62 -> :sswitch_a
        0x63 -> :sswitch_9
    .end sparse-switch
.end method

.method protected final b()V
    .locals 2

    .prologue
    .line 508
    invoke-super {p0}, Lcom/google/android/gms/common/util/a/c;->b()V

    .line 509
    sget-boolean v0, Lcom/google/android/location/geofencer/a/a;->a:Z

    if-eqz v0, :cond_0

    .line 510
    const-string v0, "GeofencerStateMachine"

    const-string v1, "onQuitting"

    invoke-static {v0, v1}, Lcom/google/android/location/geofencer/a/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 512
    :cond_0
    iget-object v0, p0, Lcom/google/android/location/geofencer/service/k;->C:Lcom/google/android/location/geofencer/service/af;

    iget-object v1, v0, Lcom/google/android/location/geofencer/service/af;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    iget-object v0, v0, Lcom/google/android/location/geofencer/service/af;->b:Landroid/database/ContentObserver;

    invoke-virtual {v1, v0}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    .line 518
    iget-object v0, p0, Lcom/google/android/location/geofencer/service/k;->w:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/location/geofencer/service/k;->h:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 519
    return-void
.end method

.method public final b(Landroid/content/Intent;)V
    .locals 4

    .prologue
    .line 564
    iget-object v1, p0, Lcom/google/android/location/geofencer/service/k;->u:Ljava/lang/Object;

    monitor-enter v1

    .line 565
    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/location/geofencer/service/k;->i:Z

    if-eqz v0, :cond_0

    .line 566
    const-string v0, "GeofencerStateMachine"

    const-string v2, "SM quit, ignoring sendSystemEvent."

    invoke-static {v0, v2}, Lcom/google/android/location/geofencer/a/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 567
    monitor-exit v1

    .line 573
    :goto_0
    return-void

    .line 569
    :cond_0
    sget-boolean v0, Lcom/google/android/location/geofencer/a/a;->a:Z

    if-eqz v0, :cond_1

    .line 570
    const-string v0, "GeofencerStateMachine"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "sendSystemEvent: intent="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/google/android/location/geofencer/a/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 572
    :cond_1
    const/16 v0, 0x9

    invoke-virtual {p0, v0, p1}, Lcom/google/android/location/geofencer/service/k;->b(ILjava/lang/Object;)V

    .line 573
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final b(Ljava/io/PrintWriter;)V
    .locals 3

    .prologue
    .line 733
    iget-object v1, p0, Lcom/google/android/location/geofencer/service/k;->u:Ljava/lang/Object;

    monitor-enter v1

    .line 734
    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/location/geofencer/service/k;->i:Z

    if-eqz v0, :cond_0

    .line 735
    const-string v0, "GeofencerStateMachine"

    const-string v2, "SM quitted, ignoring dump."

    invoke-static {v0, v2}, Lcom/google/android/location/geofencer/a/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 736
    const-string v0, "State machine quitted."

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 737
    monitor-exit v1

    .line 747
    :goto_0
    return-void

    .line 739
    :cond_0
    new-instance v0, Lcom/google/android/location/geofencer/service/c;

    invoke-direct {v0, p1}, Lcom/google/android/location/geofencer/service/c;-><init>(Ljava/lang/Object;)V

    .line 741
    invoke-virtual {v0, p0}, Lcom/google/android/location/geofencer/service/c;->a(Lcom/google/android/gms/common/util/a/c;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 743
    :try_start_1
    invoke-virtual {v0}, Lcom/google/android/location/geofencer/service/c;->b()Ljava/lang/Object;
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 747
    :goto_1
    :try_start_2
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 745
    :catch_0
    move-exception v0

    :try_start_3
    const-string v0, "Dump interrupted.\n"

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->write(Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1
.end method

.method public final c(Z)V
    .locals 4

    .prologue
    .line 715
    iget-object v1, p0, Lcom/google/android/location/geofencer/service/k;->u:Ljava/lang/Object;

    monitor-enter v1

    .line 716
    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/location/geofencer/service/k;->i:Z

    if-eqz v0, :cond_0

    .line 717
    const-string v0, "GeofencerStateMachine"

    const-string v2, "SM quitted, ignoring sendHardwareGeofenceAvailability."

    invoke-static {v0, v2}, Lcom/google/android/location/geofencer/a/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 718
    monitor-exit v1

    .line 724
    :goto_0
    return-void

    .line 720
    :cond_0
    sget-boolean v0, Lcom/google/android/location/geofencer/a/a;->a:Z

    if-eqz v0, :cond_1

    .line 721
    const-string v0, "GeofencerStateMachine"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "sendHardwareGeofenceAvailability: availabile="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/google/android/location/geofencer/a/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 723
    :cond_1
    const/16 v0, 0xd

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {p0, v0, v2}, Lcom/google/android/location/geofencer/service/k;->b(ILjava/lang/Object;)V

    .line 724
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final d()V
    .locals 5
    .annotation build Landroid/annotation/TargetApi;
        value = 0x11
    .end annotation

    .prologue
    const/4 v4, 0x1

    .line 335
    invoke-super {p0}, Lcom/google/android/gms/common/util/a/c;->d()V

    .line 336
    iget-object v0, p0, Lcom/google/android/location/geofencer/service/k;->C:Lcom/google/android/location/geofencer/service/af;

    iget-object v1, v0, Lcom/google/android/location/geofencer/service/af;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    sget-object v2, Lcom/google/android/gsf/e;->a:Landroid/net/Uri;

    iget-object v3, v0, Lcom/google/android/location/geofencer/service/af;->b:Landroid/database/ContentObserver;

    invoke-virtual {v1, v2, v4, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    iget-object v1, v0, Lcom/google/android/location/geofencer/service/af;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "location_providers_allowed"

    invoke-static {v2}, Landroid/provider/Settings$Secure;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    iget-object v0, v0, Lcom/google/android/location/geofencer/service/af;->b:Landroid/database/ContentObserver;

    invoke-virtual {v1, v2, v4, v0}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 337
    iget-object v0, p0, Lcom/google/android/location/geofencer/service/k;->x:Lcom/google/android/location/geofencer/service/i;

    iget-object v1, v0, Lcom/google/android/location/geofencer/service/i;->h:Lcom/google/android/location/geofencer/data/h;

    invoke-virtual {v1}, Lcom/google/android/location/geofencer/data/h;->a()V

    iget-object v0, v0, Lcom/google/android/location/geofencer/service/i;->i:Lcom/google/android/location/geofencer/data/h;

    invoke-virtual {v0}, Lcom/google/android/location/geofencer/data/h;->a()V

    .line 347
    iget-object v0, p0, Lcom/google/android/location/geofencer/service/k;->w:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/location/geofencer/service/k;->h:Landroid/content/BroadcastReceiver;

    iget-object v2, p0, Lcom/google/android/location/geofencer/service/k;->z:Landroid/content/IntentFilter;

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 350
    const/16 v0, 0x11

    invoke-static {v0}, Lcom/google/android/gms/common/util/ao;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 351
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 352
    const-string v1, "android.intent.action.USER_FOREGROUND"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 353
    const-string v1, "android.intent.action.USER_BACKGROUND"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 354
    iget-object v1, p0, Lcom/google/android/location/geofencer/service/k;->w:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/location/geofencer/service/k;->h:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 358
    :cond_0
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 359
    const-string v1, "android.intent.action.PACKAGE_REMOVED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 360
    const-string v1, "android.intent.action.PACKAGE_DATA_CLEARED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 361
    const-string v1, "package"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addDataScheme(Ljava/lang/String;)V

    .line 362
    iget-object v1, p0, Lcom/google/android/location/geofencer/service/k;->w:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/location/geofencer/service/k;->h:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 364
    new-instance v0, Landroid/content/IntentFilter;

    const-string v1, "com.google.android.location.activity.LOW_POWER_MODE_ENABLED"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 366
    const-string v1, "com.google.android.location.activity.LOW_POWER_MODE_DISABLED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 367
    iget-object v1, p0, Lcom/google/android/location/geofencer/service/k;->w:Landroid/content/Context;

    invoke-static {v1}, Landroid/support/v4/a/m;->a(Landroid/content/Context;)Landroid/support/v4/a/m;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/location/geofencer/service/k;->h:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/support/v4/a/m;->a(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)V

    .line 368
    return-void
.end method

.method final f()V
    .locals 3

    .prologue
    .line 372
    iget-object v1, p0, Lcom/google/android/location/geofencer/service/k;->u:Ljava/lang/Object;

    monitor-enter v1

    .line 373
    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/location/geofencer/service/k;->i:Z

    if-eqz v0, :cond_0

    .line 374
    const-string v0, "GeofencerStateMachine"

    const-string v2, "SM quitted, ignoring sendQueryLocationOptIn."

    invoke-static {v0, v2}, Lcom/google/android/location/geofencer/a/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 375
    monitor-exit v1

    .line 381
    :goto_0
    return-void

    .line 377
    :cond_0
    const-string v0, "GeofencerStateMachine"

    const/4 v2, 0x3

    invoke-static {v0, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 378
    const-string v0, "GeofencerStateMachine"

    const-string v2, "sendQueryLocationOptIn"

    invoke-static {v0, v2}, Lcom/google/android/location/geofencer/a/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 380
    :cond_1
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/location/geofencer/service/k;->c(I)V

    .line 381
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final g()V
    .locals 3

    .prologue
    .line 437
    iget-object v1, p0, Lcom/google/android/location/geofencer/service/k;->u:Ljava/lang/Object;

    monitor-enter v1

    .line 438
    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/location/geofencer/service/k;->i:Z

    if-eqz v0, :cond_0

    .line 439
    const-string v0, "GeofencerStateMachine"

    const-string v2, "SM quitted, ignoring sendSignificantMotion."

    invoke-static {v0, v2}, Lcom/google/android/location/geofencer/a/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 440
    monitor-exit v1

    .line 446
    :goto_0
    return-void

    .line 442
    :cond_0
    const-string v0, "GeofencerStateMachine"

    const/4 v2, 0x3

    invoke-static {v0, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 443
    const-string v0, "GeofencerStateMachine"

    const-string v2, "sendSignificantMotion"

    invoke-static {v0, v2}, Lcom/google/android/location/geofencer/a/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 445
    :cond_1
    const/16 v0, 0xf

    invoke-virtual {p0, v0}, Lcom/google/android/location/geofencer/service/k;->c(I)V

    .line 446
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final h()V
    .locals 3

    .prologue
    .line 679
    iget-object v1, p0, Lcom/google/android/location/geofencer/service/k;->u:Ljava/lang/Object;

    monitor-enter v1

    .line 680
    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/location/geofencer/service/k;->i:Z

    if-eqz v0, :cond_0

    .line 681
    const-string v0, "GeofencerStateMachine"

    const-string v2, "SM quitted, ignoring sendSaveActivityState."

    invoke-static {v0, v2}, Lcom/google/android/location/geofencer/a/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 682
    monitor-exit v1

    .line 688
    :goto_0
    return-void

    .line 684
    :cond_0
    sget-boolean v0, Lcom/google/android/location/geofencer/a/a;->a:Z

    if-eqz v0, :cond_1

    .line 685
    const-string v0, "GeofencerStateMachine"

    const-string v2, "sendSaveActivityState"

    invoke-static {v0, v2}, Lcom/google/android/location/geofencer/a/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 687
    :cond_1
    const/16 v0, 0xa

    const/4 v2, 0x0

    invoke-virtual {p0, v0, v2}, Lcom/google/android/location/geofencer/service/k;->b(ILjava/lang/Object;)V

    .line 688
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final i()V
    .locals 3

    .prologue
    .line 696
    iget-object v1, p0, Lcom/google/android/location/geofencer/service/k;->u:Ljava/lang/Object;

    monitor-enter v1

    .line 697
    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/location/geofencer/service/k;->i:Z

    if-eqz v0, :cond_0

    .line 698
    const-string v0, "GeofencerStateMachine"

    const-string v2, "SM quitted, ignoring sendHardwareGeofenceChanged."

    invoke-static {v0, v2}, Lcom/google/android/location/geofencer/a/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 699
    monitor-exit v1

    .line 705
    :goto_0
    return-void

    .line 701
    :cond_0
    sget-boolean v0, Lcom/google/android/location/geofencer/a/a;->a:Z

    if-eqz v0, :cond_1

    .line 702
    const-string v0, "GeofencerStateMachine"

    const-string v2, "sendHardwareGeofenceChanged"

    invoke-static {v0, v2}, Lcom/google/android/location/geofencer/a/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 704
    :cond_1
    const/16 v0, 0xc

    invoke-virtual {p0, v0}, Lcom/google/android/location/geofencer/service/k;->c(I)V

    .line 705
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method
