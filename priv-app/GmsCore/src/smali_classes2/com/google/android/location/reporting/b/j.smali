.class public final Lcom/google/android/location/reporting/b/j;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/android/location/reporting/a/f;


# direct methods
.method private constructor <init>(Lcom/google/android/location/reporting/a/f;)V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    iput-object p1, p0, Lcom/google/android/location/reporting/b/j;->a:Lcom/google/android/location/reporting/a/f;

    .line 26
    return-void
.end method

.method private static a(IF)I
    .locals 2

    .prologue
    .line 174
    const/4 v0, 0x0

    cmpl-float v0, p1, v0

    if-eqz v0, :cond_0

    invoke-static {p1}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v0

    .line 175
    :goto_0
    mul-int/lit8 v1, p0, 0x1f

    add-int/2addr v0, v1

    return v0

    .line 174
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static a(IJ)I
    .locals 5

    .prologue
    .line 170
    mul-int/lit8 v0, p0, 0x1f

    const/16 v1, 0x20

    ushr-long v2, p1, v1

    xor-long/2addr v2, p1

    long-to-int v1, v2

    add-int/2addr v0, v1

    return v0
.end method

.method public static wrap(Ljava/lang/Iterable;)Ljava/lang/Iterable;
    .locals 4
    .annotation build Lcom/google/android/gms/common/util/RetainForClient;
    .end annotation

    .prologue
    .line 31
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 32
    invoke-interface {p0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/reporting/a/f;

    .line 33
    new-instance v3, Lcom/google/android/location/reporting/b/j;

    invoke-direct {v3, v0}, Lcom/google/android/location/reporting/b/j;-><init>(Lcom/google/android/location/reporting/a/f;)V

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 35
    :cond_0
    return-object v1
.end method


# virtual methods
.method public final equals(Ljava/lang/Object;)Z
    .locals 10

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 128
    instance-of v2, p1, Lcom/google/android/location/reporting/b/j;

    if-nez v2, :cond_1

    .line 134
    :cond_0
    :goto_0
    return v0

    .line 131
    :cond_1
    check-cast p1, Lcom/google/android/location/reporting/b/j;

    iget-object v3, p1, Lcom/google/android/location/reporting/b/j;->a:Lcom/google/android/location/reporting/a/f;

    .line 132
    iget-object v2, p0, Lcom/google/android/location/reporting/b/j;->a:Lcom/google/android/location/reporting/a/f;

    iget-object v2, v2, Lcom/google/android/location/reporting/a/f;->b:Lcom/google/android/location/reporting/a/i;

    .line 133
    iget-object v4, v3, Lcom/google/android/location/reporting/a/f;->b:Lcom/google/android/location/reporting/a/i;

    .line 134
    iget-object v5, p0, Lcom/google/android/location/reporting/b/j;->a:Lcom/google/android/location/reporting/a/f;

    iget-wide v6, v5, Lcom/google/android/location/reporting/a/f;->f:J

    iget-wide v8, v3, Lcom/google/android/location/reporting/a/f;->f:J

    cmp-long v5, v6, v8

    if-nez v5, :cond_0

    iget v5, v2, Lcom/google/android/location/reporting/a/i;->b:I

    iget v6, v4, Lcom/google/android/location/reporting/a/i;->b:I

    if-ne v5, v6, :cond_0

    iget v2, v2, Lcom/google/android/location/reporting/a/i;->d:I

    iget v4, v4, Lcom/google/android/location/reporting/a/i;->d:I

    if-ne v2, v4, :cond_0

    iget-object v2, p0, Lcom/google/android/location/reporting/b/j;->a:Lcom/google/android/location/reporting/a/f;

    iget v2, v2, Lcom/google/android/location/reporting/a/f;->d:I

    iget v4, v3, Lcom/google/android/location/reporting/a/f;->d:I

    if-ne v2, v4, :cond_0

    iget-object v2, p0, Lcom/google/android/location/reporting/b/j;->a:Lcom/google/android/location/reporting/a/f;

    iget v2, v2, Lcom/google/android/location/reporting/a/f;->h:F

    iget v4, v3, Lcom/google/android/location/reporting/a/f;->h:F

    cmpl-float v2, v2, v4

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/google/android/location/reporting/b/j;->a:Lcom/google/android/location/reporting/a/f;

    iget v2, v2, Lcom/google/android/location/reporting/a/f;->j:F

    iget v4, v3, Lcom/google/android/location/reporting/a/f;->j:F

    cmpl-float v2, v2, v4

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/google/android/location/reporting/b/j;->a:Lcom/google/android/location/reporting/a/f;

    iget-wide v4, v2, Lcom/google/android/location/reporting/a/f;->l:D

    iget-wide v6, v3, Lcom/google/android/location/reporting/a/f;->l:D

    cmpl-double v2, v4, v6

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/google/android/location/reporting/b/j;->a:Lcom/google/android/location/reporting/a/f;

    iget v2, v2, Lcom/google/android/location/reporting/a/f;->n:F

    iget v4, v3, Lcom/google/android/location/reporting/a/f;->n:F

    cmpl-float v2, v2, v4

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/google/android/location/reporting/b/j;->a:Lcom/google/android/location/reporting/a/f;

    iget v2, v2, Lcom/google/android/location/reporting/a/f;->o:I

    iget v4, v3, Lcom/google/android/location/reporting/a/f;->o:I

    if-ne v2, v4, :cond_0

    iget-object v2, p0, Lcom/google/android/location/reporting/b/j;->a:Lcom/google/android/location/reporting/a/f;

    iget-object v2, v2, Lcom/google/android/location/reporting/a/f;->x:Lcom/google/android/location/reporting/a/b;

    iget-object v4, v3, Lcom/google/android/location/reporting/a/f;->x:Lcom/google/android/location/reporting/a/b;

    if-eqz v2, :cond_2

    if-nez v4, :cond_4

    :cond_2
    if-ne v2, v4, :cond_3

    move v2, v1

    :goto_1
    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/location/reporting/b/j;->a:Lcom/google/android/location/reporting/a/f;

    iget-boolean v2, v2, Lcom/google/android/location/reporting/a/f;->q:Z

    iget-boolean v4, v3, Lcom/google/android/location/reporting/a/f;->q:Z

    if-ne v2, v4, :cond_0

    iget-object v2, p0, Lcom/google/android/location/reporting/b/j;->a:Lcom/google/android/location/reporting/a/f;

    iget-object v2, v2, Lcom/google/android/location/reporting/a/f;->s:Ljava/lang/String;

    iget-object v4, v3, Lcom/google/android/location/reporting/a/f;->s:Ljava/lang/String;

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/location/reporting/b/j;->a:Lcom/google/android/location/reporting/a/f;

    iget v2, v2, Lcom/google/android/location/reporting/a/f;->u:I

    iget v3, v3, Lcom/google/android/location/reporting/a/f;->u:I

    if-ne v2, v3, :cond_0

    move v0, v1

    goto/16 :goto_0

    :cond_3
    move v2, v0

    goto :goto_1

    :cond_4
    iget v5, v2, Lcom/google/android/location/reporting/a/b;->b:I

    iget v6, v4, Lcom/google/android/location/reporting/a/b;->b:I

    if-ne v5, v6, :cond_5

    iget v5, v2, Lcom/google/android/location/reporting/a/b;->c:I

    iget v6, v4, Lcom/google/android/location/reporting/a/b;->c:I

    if-ne v5, v6, :cond_5

    iget v5, v2, Lcom/google/android/location/reporting/a/b;->d:I

    iget v6, v4, Lcom/google/android/location/reporting/a/b;->d:I

    if-ne v5, v6, :cond_5

    iget v2, v2, Lcom/google/android/location/reporting/a/b;->e:I

    iget v4, v4, Lcom/google/android/location/reporting/a/b;->e:I

    if-ne v2, v4, :cond_5

    move v2, v1

    goto :goto_1

    :cond_5
    move v2, v0

    goto :goto_1
.end method

.method public final hashCode()I
    .locals 8

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 152
    iget-object v0, p0, Lcom/google/android/location/reporting/b/j;->a:Lcom/google/android/location/reporting/a/f;

    iget-wide v0, v0, Lcom/google/android/location/reporting/a/f;->f:J

    invoke-static {v2, v0, v1}, Lcom/google/android/location/reporting/b/j;->a(IJ)I

    move-result v0

    .line 154
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/google/android/location/reporting/b/j;->a:Lcom/google/android/location/reporting/a/f;

    iget-object v1, v1, Lcom/google/android/location/reporting/a/f;->b:Lcom/google/android/location/reporting/a/i;

    iget v1, v1, Lcom/google/android/location/reporting/a/i;->b:I

    add-int/2addr v0, v1

    .line 155
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/google/android/location/reporting/b/j;->a:Lcom/google/android/location/reporting/a/f;

    iget-object v1, v1, Lcom/google/android/location/reporting/a/f;->b:Lcom/google/android/location/reporting/a/i;

    iget v1, v1, Lcom/google/android/location/reporting/a/i;->d:I

    add-int/2addr v0, v1

    .line 156
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/google/android/location/reporting/b/j;->a:Lcom/google/android/location/reporting/a/f;

    iget v1, v1, Lcom/google/android/location/reporting/a/f;->d:I

    add-int/2addr v0, v1

    .line 157
    iget-object v1, p0, Lcom/google/android/location/reporting/b/j;->a:Lcom/google/android/location/reporting/a/f;

    iget v1, v1, Lcom/google/android/location/reporting/a/f;->h:F

    invoke-static {v0, v1}, Lcom/google/android/location/reporting/b/j;->a(IF)I

    move-result v0

    .line 158
    iget-object v1, p0, Lcom/google/android/location/reporting/b/j;->a:Lcom/google/android/location/reporting/a/f;

    iget v1, v1, Lcom/google/android/location/reporting/a/f;->j:F

    invoke-static {v0, v1}, Lcom/google/android/location/reporting/b/j;->a(IF)I

    move-result v4

    .line 159
    iget-object v0, p0, Lcom/google/android/location/reporting/b/j;->a:Lcom/google/android/location/reporting/a/f;

    iget-wide v0, v0, Lcom/google/android/location/reporting/a/f;->l:D

    const-wide/16 v6, 0x0

    cmpl-double v5, v0, v6

    if-eqz v5, :cond_1

    invoke-static {v0, v1}, Ljava/lang/Double;->doubleToLongBits(D)J

    move-result-wide v0

    :goto_0
    invoke-static {v4, v0, v1}, Lcom/google/android/location/reporting/b/j;->a(IJ)I

    move-result v0

    .line 160
    iget-object v1, p0, Lcom/google/android/location/reporting/b/j;->a:Lcom/google/android/location/reporting/a/f;

    iget v1, v1, Lcom/google/android/location/reporting/a/f;->n:F

    invoke-static {v0, v1}, Lcom/google/android/location/reporting/b/j;->a(IF)I

    move-result v0

    .line 161
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/google/android/location/reporting/b/j;->a:Lcom/google/android/location/reporting/a/f;

    iget v1, v1, Lcom/google/android/location/reporting/a/f;->o:I

    add-int/2addr v0, v1

    .line 162
    mul-int/lit8 v1, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/location/reporting/b/j;->a:Lcom/google/android/location/reporting/a/f;

    iget-object v0, v0, Lcom/google/android/location/reporting/a/f;->x:Lcom/google/android/location/reporting/a/b;

    if-nez v0, :cond_2

    move v0, v2

    :goto_1
    add-int/2addr v0, v1

    .line 163
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/google/android/location/reporting/b/j;->a:Lcom/google/android/location/reporting/a/f;

    iget-boolean v1, v1, Lcom/google/android/location/reporting/a/f;->q:Z

    if-eqz v1, :cond_0

    move v2, v3

    :cond_0
    add-int/2addr v0, v2

    .line 164
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/google/android/location/reporting/b/j;->a:Lcom/google/android/location/reporting/a/f;

    iget-object v1, v1, Lcom/google/android/location/reporting/a/f;->s:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 165
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/google/android/location/reporting/b/j;->a:Lcom/google/android/location/reporting/a/f;

    iget v1, v1, Lcom/google/android/location/reporting/a/f;->u:I

    add-int/2addr v0, v1

    .line 166
    return v0

    .line 159
    :cond_1
    const-wide/16 v0, 0x0

    goto :goto_0

    .line 162
    :cond_2
    const/4 v4, 0x4

    new-array v4, v4, [Ljava/lang/Object;

    iget v5, v0, Lcom/google/android/location/reporting/a/b;->b:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v2

    iget v5, v0, Lcom/google/android/location/reporting/a/b;->c:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v3

    const/4 v5, 0x2

    iget v6, v0, Lcom/google/android/location/reporting/a/b;->d:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x3

    iget v0, v0, Lcom/google/android/location/reporting/a/b;->e:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v4, v5

    invoke-static {v4}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v0

    goto :goto_1
.end method

.method public final toString()Ljava/lang/String;
    .locals 6

    .prologue
    .line 106
    iget-object v0, p0, Lcom/google/android/location/reporting/b/j;->a:Lcom/google/android/location/reporting/a/f;

    iget-object v0, v0, Lcom/google/android/location/reporting/a/f;->b:Lcom/google/android/location/reporting/a/i;

    .line 107
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "{"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 108
    const-string v2, "time: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/location/reporting/b/j;->a:Lcom/google/android/location/reporting/a/f;

    iget-wide v4, v3, Lcom/google/android/location/reporting/a/f;->f:J

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 109
    const-string v2, ", latE7: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, v0, Lcom/google/android/location/reporting/a/i;->b:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 110
    const-string v2, ", lngE7: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v0, v0, Lcom/google/android/location/reporting/a/i;->d:I

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 111
    const-string v0, ", source:  "

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/location/reporting/b/j;->a:Lcom/google/android/location/reporting/a/f;

    iget v2, v2, Lcom/google/android/location/reporting/a/f;->d:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 112
    const-string v0, ", speed: "

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/location/reporting/b/j;->a:Lcom/google/android/location/reporting/a/f;

    iget v2, v2, Lcom/google/android/location/reporting/a/f;->h:F

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    .line 113
    const-string v0, ", heading: "

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/location/reporting/b/j;->a:Lcom/google/android/location/reporting/a/f;

    iget v2, v2, Lcom/google/android/location/reporting/a/f;->j:F

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    .line 114
    const-string v0, ", altitude: "

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/location/reporting/b/j;->a:Lcom/google/android/location/reporting/a/f;

    iget-wide v2, v2, Lcom/google/android/location/reporting/a/f;->l:D

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    .line 115
    const-string v0, ", accuracy: "

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/location/reporting/b/j;->a:Lcom/google/android/location/reporting/a/f;

    iget v2, v2, Lcom/google/android/location/reporting/a/f;->n:F

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    .line 116
    const-string v0, ", gmmNlpVersion: "

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/location/reporting/b/j;->a:Lcom/google/android/location/reporting/a/f;

    iget v2, v2, Lcom/google/android/location/reporting/a/f;->o:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 117
    const-string v0, ", batteryCondition: "

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/location/reporting/b/j;->a:Lcom/google/android/location/reporting/a/f;

    iget-object v2, v2, Lcom/google/android/location/reporting/a/f;->x:Lcom/google/android/location/reporting/a/b;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "{"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    if-eqz v2, :cond_0

    const-string v4, "charging: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, v2, Lcom/google/android/location/reporting/a/b;->b:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v4, ", level: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, v2, Lcom/google/android/location/reporting/a/b;->c:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v4, ", scale: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, v2, Lcom/google/android/location/reporting/a/b;->d:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v4, ", voltage: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v2, v2, Lcom/google/android/location/reporting/a/b;->e:I

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    :cond_0
    const-string v2, "}"

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 119
    const-string v0, ", stationary: "

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/location/reporting/b/j;->a:Lcom/google/android/location/reporting/a/f;

    iget-boolean v2, v2, Lcom/google/android/location/reporting/a/f;->q:Z

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 120
    const-string v0, ", levelId: "

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/location/reporting/b/j;->a:Lcom/google/android/location/reporting/a/f;

    iget-object v2, v2, Lcom/google/android/location/reporting/a/f;->s:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 121
    const-string v0, ", levenNumberE3: "

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/location/reporting/b/j;->a:Lcom/google/android/location/reporting/a/f;

    iget v2, v2, Lcom/google/android/location/reporting/a/f;->u:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 122
    const-string v0, "}"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 123
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
