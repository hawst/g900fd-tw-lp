.class final Lcom/google/android/location/activity/j;
.super Lcom/google/android/location/activity/f;
.source "SourceFile"


# instance fields
.field final synthetic c:Lcom/google/android/location/activity/i;


# direct methods
.method constructor <init>(Lcom/google/android/location/activity/i;)V
    .locals 0

    .prologue
    .line 55
    iput-object p1, p0, Lcom/google/android/location/activity/j;->c:Lcom/google/android/location/activity/i;

    invoke-direct {p0, p1}, Lcom/google/android/location/activity/f;-><init>(Lcom/google/android/location/activity/d;)V

    return-void
.end method

.method private static a(Ljava/util/List;IJ)I
    .locals 8

    .prologue
    const/4 v2, 0x0

    .line 194
    invoke-interface {p0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/d/i;

    iget-wide v0, v0, Lcom/google/android/location/d/i;->a:J

    sub-long v4, v0, p2

    move v1, p1

    .line 195
    :goto_0
    if-ltz v1, :cond_1

    .line 196
    invoke-interface {p0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/d/i;

    iget-wide v6, v0, Lcom/google/android/location/d/i;->a:J

    cmp-long v0, v6, v4

    if-gtz v0, :cond_0

    move v0, v1

    .line 208
    :goto_1
    return v0

    .line 195
    :cond_0
    add-int/lit8 v1, v1, -0x1

    goto :goto_0

    .line 205
    :cond_1
    invoke-interface {p0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/d/i;

    iget-wide v0, v0, Lcom/google/android/location/d/i;->a:J

    sub-long/2addr v0, v4

    const-wide/32 v4, 0xbebc200

    cmp-long v0, v0, v4

    if-gtz v0, :cond_2

    move v0, v2

    .line 206
    goto :goto_1

    .line 208
    :cond_2
    const/4 v0, -0x1

    goto :goto_1
.end method


# virtual methods
.method protected final a(JJLjava/util/List;)Ljava/lang/String;
    .locals 15

    .prologue
    .line 65
    invoke-interface/range {p5 .. p5}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 67
    iget-object v2, p0, Lcom/google/android/location/activity/j;->c:Lcom/google/android/location/activity/i;

    iget-object v2, v2, Lcom/google/android/location/activity/i;->d:Lcom/google/android/location/activity/e;

    const-string v3, "accel data is empty"

    invoke-interface {v2, v3}, Lcom/google/android/location/activity/e;->a(Ljava/lang/String;)V

    .line 68
    iget-object v2, p0, Lcom/google/android/location/activity/j;->c:Lcom/google/android/location/activity/i;

    iget-object v2, v2, Lcom/google/android/location/activity/i;->a:Lcom/google/android/location/os/bi;

    invoke-interface {v2}, Lcom/google/android/location/os/bi;->v()Lcom/google/android/location/os/j;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/google/android/location/os/j;->a(I)V

    .line 69
    const-string v2, "accel data is empty"

    .line 129
    :goto_0
    return-object v2

    .line 71
    :cond_0
    iget-object v2, p0, Lcom/google/android/location/activity/j;->c:Lcom/google/android/location/activity/i;

    invoke-static {v2}, Lcom/google/android/location/activity/i;->a(Lcom/google/android/location/activity/i;)Lcom/google/android/location/d/i;

    move-result-object v2

    if-eqz v2, :cond_5

    invoke-interface/range {p5 .. p5}, Ljava/util/List;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    move-object/from16 v0, p5

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/location/d/i;

    iget-object v3, p0, Lcom/google/android/location/activity/j;->c:Lcom/google/android/location/activity/i;

    invoke-static {v3}, Lcom/google/android/location/activity/i;->a(Lcom/google/android/location/activity/i;)Lcom/google/android/location/d/i;

    move-result-object v4

    iget-wide v6, v2, Lcom/google/android/location/d/i;->a:J

    iget-wide v8, v4, Lcom/google/android/location/d/i;->a:J

    cmp-long v3, v6, v8

    if-nez v3, :cond_1

    iget-object v3, v2, Lcom/google/android/location/d/i;->b:[F

    array-length v3, v3

    iget-object v5, v4, Lcom/google/android/location/d/i;->b:[F

    array-length v5, v5

    if-eq v3, v5, :cond_2

    :cond_1
    const/4 v2, 0x0

    :goto_1
    if-eqz v2, :cond_5

    .line 74
    iget-object v2, p0, Lcom/google/android/location/activity/j;->c:Lcom/google/android/location/activity/i;

    iget-object v2, v2, Lcom/google/android/location/activity/i;->d:Lcom/google/android/location/activity/e;

    const-string v3, "same accel data as previous one"

    invoke-interface {v2, v3}, Lcom/google/android/location/activity/e;->a(Ljava/lang/String;)V

    .line 75
    const-string v2, "same accel data as previous one"

    goto :goto_0

    .line 71
    :cond_2
    const/4 v3, 0x0

    :goto_2
    iget-object v5, v2, Lcom/google/android/location/d/i;->b:[F

    array-length v5, v5

    if-ge v3, v5, :cond_4

    iget-object v5, v2, Lcom/google/android/location/d/i;->b:[F

    aget v5, v5, v3

    iget-object v6, v4, Lcom/google/android/location/d/i;->b:[F

    aget v6, v6, v3

    sub-float/2addr v5, v6

    invoke-static {v5}, Ljava/lang/Math;->abs(F)F

    move-result v5

    const v6, 0x358637bd    # 1.0E-6f

    cmpl-float v5, v5, v6

    if-lez v5, :cond_3

    const/4 v2, 0x0

    goto :goto_1

    :cond_3
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    :cond_4
    const/4 v2, 0x1

    goto :goto_1

    .line 77
    :cond_5
    iget-object v3, p0, Lcom/google/android/location/activity/j;->c:Lcom/google/android/location/activity/i;

    invoke-interface/range {p5 .. p5}, Ljava/util/List;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    move-object/from16 v0, p5

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/location/d/i;

    invoke-static {v3, v2}, Lcom/google/android/location/activity/i;->a(Lcom/google/android/location/activity/i;Lcom/google/android/location/d/i;)Lcom/google/android/location/d/i;

    .line 78
    sget-boolean v2, Lcom/google/android/location/i/a;->b:Z

    if-eqz v2, :cond_6

    const-string v3, "ActivityDetectionRunnerInPast"

    const-string v4, "onAccelData: %d events, %f seconds"

    const/4 v2, 0x2

    new-array v5, v2, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-interface/range {p5 .. p5}, Ljava/util/List;->size()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v2

    const/4 v6, 0x1

    invoke-interface/range {p5 .. p5}, Ljava/util/List;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    move-object/from16 v0, p5

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/location/d/i;

    iget-wide v8, v2, Lcom/google/android/location/d/i;->a:J

    const/4 v2, 0x0

    move-object/from16 v0, p5

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/location/d/i;

    iget-wide v10, v2, Lcom/google/android/location/d/i;->a:J

    sub-long/2addr v8, v10

    long-to-double v8, v8

    const-wide v10, 0x3e112e0be826d695L    # 1.0E-9

    mul-double/2addr v8, v10

    invoke-static {v8, v9}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    aput-object v2, v5, v6

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v2}, Lcom/google/android/location/o/a/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 81
    :cond_6
    invoke-static/range {p5 .. p5}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v12

    .line 83
    iget-object v2, p0, Lcom/google/android/location/activity/j;->c:Lcom/google/android/location/activity/i;

    iget-boolean v2, v2, Lcom/google/android/location/activity/i;->e:Z

    if-eqz v2, :cond_a

    iget-object v2, p0, Lcom/google/android/location/activity/j;->c:Lcom/google/android/location/activity/i;

    iget-object v2, v2, Lcom/google/android/location/activity/i;->f:Lcom/google/android/location/activity/a/a/b;

    if-eqz v2, :cond_a

    .line 84
    invoke-interface {v12}, Ljava/util/List;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    new-instance v13, Ljava/util/ArrayList;

    invoke-direct {v13}, Ljava/util/ArrayList;-><init>()V

    :goto_3
    if-ltz v2, :cond_7

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v13, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v3, p0, Lcom/google/android/location/activity/j;->c:Lcom/google/android/location/activity/i;

    iget-object v3, v3, Lcom/google/android/location/activity/i;->c:Lcom/google/android/location/activity/a/o;

    invoke-interface {v3}, Lcom/google/android/location/activity/a/o;->a()J

    move-result-wide v4

    invoke-static {v12, v2, v4, v5}, Lcom/google/android/location/activity/j;->a(Ljava/util/List;IJ)I

    move-result v2

    goto :goto_3

    :cond_7
    const/4 v3, 0x0

    const/4 v9, 0x0

    invoke-virtual {v13}, Ljava/util/ArrayList;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    move v11, v2

    :goto_4
    if-ltz v11, :cond_9

    invoke-virtual {v13, v11}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v10

    add-int/lit8 v2, v10, 0x1

    invoke-interface {v12, v3, v2}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v8

    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v2

    const/4 v4, 0x4

    if-ge v2, v4, :cond_8

    move v2, v9

    move v3, v10

    :goto_5
    add-int/lit8 v4, v11, -0x1

    move v11, v4

    move v9, v2

    goto :goto_4

    :cond_8
    invoke-interface {v12}, Ljava/util/List;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-interface {v12, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/location/d/i;

    iget-wide v4, v2, Lcom/google/android/location/d/i;->a:J

    invoke-interface {v12, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/location/d/i;

    iget-wide v2, v2, Lcom/google/android/location/d/i;->a:J

    sub-long v2, v4, v2

    const-wide/32 v4, 0xf4240

    div-long/2addr v2, v4

    sub-long v4, p1, v2

    sub-long v6, p3, v2

    move-object v3, p0

    invoke-virtual/range {v3 .. v8}, Lcom/google/android/location/activity/j;->b(JJLjava/util/List;)V

    add-int/lit8 v2, v9, 0x1

    move v3, v10

    goto :goto_5

    :cond_9
    sget-boolean v2, Lcom/google/android/location/i/a;->b:Z

    if-eqz v2, :cond_a

    const-string v2, "ActivityDetectionRunnerInPast"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Number of times phone position detection was run for this batch: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/location/o/a/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 90
    :cond_a
    invoke-interface {v12}, Ljava/util/List;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    move v3, v2

    .line 93
    :goto_6
    iget-object v2, p0, Lcom/google/android/location/activity/j;->c:Lcom/google/android/location/activity/i;

    iget-object v2, v2, Lcom/google/android/location/activity/i;->c:Lcom/google/android/location/activity/a/o;

    invoke-interface {v2}, Lcom/google/android/location/activity/a/o;->a()J

    move-result-wide v4

    invoke-static {v12, v3, v4, v5}, Lcom/google/android/location/activity/j;->a(Ljava/util/List;IJ)I

    move-result v11

    .line 94
    const/4 v2, -0x1

    if-eq v11, v2, :cond_e

    .line 95
    add-int/lit8 v2, v3, 0x1

    invoke-interface {v12, v11, v2}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v8

    .line 98
    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v2

    const/4 v4, 0x4

    if-ge v2, v4, :cond_b

    move v3, v11

    .line 101
    goto :goto_6

    .line 103
    :cond_b
    invoke-interface {v12, v11}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/location/d/i;

    iget-wide v4, v2, Lcom/google/android/location/d/i;->a:J

    const/4 v2, 0x0

    invoke-interface {v12, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/location/d/i;

    iget-wide v6, v2, Lcom/google/android/location/d/i;->a:J

    sub-long/2addr v4, v6

    iget-object v2, p0, Lcom/google/android/location/activity/j;->c:Lcom/google/android/location/activity/i;

    iget-object v2, v2, Lcom/google/android/location/activity/i;->c:Lcom/google/android/location/activity/a/o;

    invoke-interface {v2}, Lcom/google/android/location/activity/a/o;->a()J

    move-result-wide v6

    sub-long/2addr v4, v6

    const-wide/32 v6, 0xf4240

    div-long v9, v4, v6

    .line 106
    invoke-interface {v12}, Ljava/util/List;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-interface {v12, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/location/d/i;

    iget-wide v4, v2, Lcom/google/android/location/d/i;->a:J

    invoke-interface {v12, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/location/d/i;

    iget-wide v2, v2, Lcom/google/android/location/d/i;->a:J

    sub-long v2, v4, v2

    const-wide/32 v4, 0xf4240

    div-long/2addr v2, v4

    .line 108
    sub-long v4, p1, v2

    sub-long v6, p3, v2

    move-object v3, p0

    invoke-virtual/range {v3 .. v10}, Lcom/google/android/location/activity/j;->a(JJLjava/util/List;J)J

    move-result-wide v2

    .line 110
    const-wide/16 v4, 0x0

    cmp-long v4, v2, v4

    if-gez v4, :cond_c

    .line 112
    iget-object v2, p0, Lcom/google/android/location/activity/j;->a:Ljava/util/List;

    const/4 v3, 0x0

    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/location/activity/a/s;

    iget-object v2, v2, Lcom/google/android/location/activity/a/s;->a:Lcom/google/android/location/activity/a/t;

    invoke-virtual {v2}, Lcom/google/android/location/activity/a/t;->name()Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_0

    .line 114
    :cond_c
    cmp-long v4, v2, v9

    if-lez v4, :cond_d

    .line 115
    new-instance v2, Ljava/lang/RuntimeException;

    const-string v3, "Return value of detectActivity cannot be larger than maxSkipTimeToNextDetectionMillis."

    invoke-direct {v2, v3}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 120
    :cond_d
    const-wide/32 v4, 0xf4240

    mul-long/2addr v2, v4

    invoke-static {v12, v11, v2, v3}, Lcom/google/android/location/activity/j;->a(Ljava/util/List;IJ)I

    move-result v2

    .line 121
    const/4 v3, -0x1

    if-eq v2, v3, :cond_e

    .line 122
    add-int/lit8 v2, v2, 0x1

    move v3, v2

    .line 125
    goto/16 :goto_6

    .line 126
    :cond_e
    sget-boolean v2, Lcom/google/android/location/i/a;->b:Z

    if-eqz v2, :cond_f

    const-string v2, "ActivityDetectionRunnerInPast"

    const-string v3, "The time duration of sensor batching data is too short"

    invoke-static {v2, v3}, Lcom/google/android/location/o/a/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 127
    :cond_f
    iget-object v2, p0, Lcom/google/android/location/activity/j;->c:Lcom/google/android/location/activity/i;

    iget-object v2, v2, Lcom/google/android/location/activity/i;->d:Lcom/google/android/location/activity/e;

    invoke-interface {v2}, Lcom/google/android/location/activity/e;->ap_()V

    .line 128
    iget-object v2, p0, Lcom/google/android/location/activity/j;->c:Lcom/google/android/location/activity/i;

    iget-object v2, v2, Lcom/google/android/location/activity/i;->a:Lcom/google/android/location/os/bi;

    invoke-interface {v2}, Lcom/google/android/location/os/bi;->v()Lcom/google/android/location/os/j;

    move-result-object v2

    invoke-interface {v12}, Ljava/util/List;->size()I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/google/android/location/os/j;->a(I)V

    .line 129
    const-string v2, "insufficient_samples"

    goto/16 :goto_0
.end method
