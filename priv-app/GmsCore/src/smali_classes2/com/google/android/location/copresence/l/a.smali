.class public final Lcom/google/android/location/copresence/l/a;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method public static a(Lcom/google/android/gms/common/people/data/Audience;)Lcom/google/ac/b/c/dm;
    .locals 10

    .prologue
    const/4 v1, 0x0

    .line 70
    invoke-virtual {p0}, Lcom/google/android/gms/common/people/data/Audience;->a()Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/common/people/data/c;->b(Ljava/util/List;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/common/util/m;->c(Ljava/lang/String;)[B

    move-result-object v0

    .line 71
    invoke-static {v0}, Lcom/google/ac/b/c/dm;->a([B)Lcom/google/ac/b/c/dm;

    move-result-object v4

    new-instance v5, Lcom/google/ac/b/c/dn;

    invoke-direct {v5}, Lcom/google/ac/b/c/dn;-><init>()V

    new-instance v0, Lcom/google/ac/b/c/dr;

    invoke-direct {v0}, Lcom/google/ac/b/c/dr;-><init>()V

    iput-object v0, v5, Lcom/google/ac/b/c/dn;->b:Lcom/google/ac/b/c/dr;

    iget-object v0, v5, Lcom/google/ac/b/c/dn;->b:Lcom/google/ac/b/c/dr;

    iget-object v2, v4, Lcom/google/ac/b/c/dm;->c:[Lcom/google/ac/b/c/ds;

    array-length v2, v2

    new-array v2, v2, [Lcom/google/ac/b/c/dt;

    iput-object v2, v0, Lcom/google/ac/b/c/dr;->a:[Lcom/google/ac/b/c/dt;

    iget-object v6, v4, Lcom/google/ac/b/c/dm;->c:[Lcom/google/ac/b/c/ds;

    array-length v7, v6

    move v0, v1

    move v2, v1

    :goto_0
    if-ge v0, v7, :cond_0

    aget-object v8, v6, v0

    iget-object v3, v5, Lcom/google/ac/b/c/dn;->b:Lcom/google/ac/b/c/dr;

    iget-object v9, v3, Lcom/google/ac/b/c/dr;->a:[Lcom/google/ac/b/c/dt;

    add-int/lit8 v3, v2, 0x1

    iget-object v8, v8, Lcom/google/ac/b/c/ds;->a:Lcom/google/ac/b/c/dt;

    aput-object v8, v9, v2

    add-int/lit8 v0, v0, 0x1

    move v2, v3

    goto :goto_0

    :cond_0
    const/4 v0, 0x1

    new-array v0, v0, [Lcom/google/ac/b/c/dn;

    aput-object v5, v0, v1

    iput-object v0, v4, Lcom/google/ac/b/c/dm;->a:[Lcom/google/ac/b/c/dn;

    return-object v4
.end method

.method public static a(Lcom/google/ac/b/c/dm;)Lcom/google/android/gms/common/people/data/Audience;
    .locals 2

    .prologue
    .line 80
    if-nez p0, :cond_0

    .line 81
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    .line 87
    :goto_0
    new-instance v1, Lcom/google/android/gms/common/people/data/a;

    invoke-direct {v1}, Lcom/google/android/gms/common/people/data/a;-><init>()V

    invoke-virtual {v1, v0}, Lcom/google/android/gms/common/people/data/a;->a(Ljava/util/Collection;)Lcom/google/android/gms/common/people/data/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/common/people/data/a;->a()Lcom/google/android/gms/common/people/data/Audience;

    move-result-object v0

    return-object v0

    .line 83
    :cond_0
    invoke-static {p0}, Lcom/google/ac/b/c/dm;->toByteArray(Lcom/google/protobuf/nano/j;)[B

    move-result-object v0

    .line 84
    invoke-static {v0}, Lcom/google/android/gms/common/people/data/c;->a([B)Ljava/util/List;

    move-result-object v0

    goto :goto_0
.end method
