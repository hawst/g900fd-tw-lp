.class final Lcom/google/android/location/os/f;
.super Ljava/io/OutputStream;
.source "SourceFile"


# instance fields
.field private final a:Ljava/io/Writer;


# direct methods
.method public constructor <init>(Ljava/io/Writer;)V
    .locals 0

    .prologue
    .line 376
    invoke-direct {p0}, Ljava/io/OutputStream;-><init>()V

    .line 377
    iput-object p1, p0, Lcom/google/android/location/os/f;->a:Ljava/io/Writer;

    .line 378
    return-void
.end method


# virtual methods
.method public final close()V
    .locals 1

    .prologue
    .line 395
    iget-object v0, p0, Lcom/google/android/location/os/f;->a:Ljava/io/Writer;

    invoke-virtual {v0}, Ljava/io/Writer;->close()V

    .line 396
    return-void
.end method

.method public final flush()V
    .locals 1

    .prologue
    .line 390
    iget-object v0, p0, Lcom/google/android/location/os/f;->a:Ljava/io/Writer;

    invoke-virtual {v0}, Ljava/io/Writer;->flush()V

    .line 391
    return-void
.end method

.method public final write(I)V
    .locals 3

    .prologue
    .line 382
    and-int/lit8 v0, p1, -0x80

    if-eqz v0, :cond_0

    .line 383
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Not an ASCII character "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 385
    :cond_0
    iget-object v0, p0, Lcom/google/android/location/os/f;->a:Ljava/io/Writer;

    invoke-virtual {v0, p1}, Ljava/io/Writer;->write(I)V

    .line 386
    return-void
.end method
