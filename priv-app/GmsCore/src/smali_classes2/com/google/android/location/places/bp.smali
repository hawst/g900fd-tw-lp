.class public final Lcom/google/android/location/places/bp;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/location/places/be;


# instance fields
.field private final a:Lcom/google/android/location/places/f;

.field private final b:Lcom/google/android/location/places/c/b;

.field private final c:Lcom/google/android/location/places/c/d;


# direct methods
.method public constructor <init>(Lcom/google/android/location/places/f;Lcom/google/android/location/places/c/b;Lcom/google/android/location/places/c/d;)V
    .locals 0

    .prologue
    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    iput-object p1, p0, Lcom/google/android/location/places/bp;->a:Lcom/google/android/location/places/f;

    .line 36
    iput-object p2, p0, Lcom/google/android/location/places/bp;->b:Lcom/google/android/location/places/c/b;

    .line 37
    iput-object p3, p0, Lcom/google/android/location/places/bp;->c:Lcom/google/android/location/places/c/d;

    .line 38
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/location/e/al;Lcom/google/android/location/places/x;)V
    .locals 8

    .prologue
    const-wide v6, 0x416312d000000000L    # 1.0E7

    .line 44
    new-instance v0, Lcom/google/android/gms/maps/model/LatLng;

    iget v1, p1, Lcom/google/android/location/e/al;->d:I

    int-to-double v2, v1

    div-double/2addr v2, v6

    iget v1, p1, Lcom/google/android/location/e/al;->e:I

    int-to-double v4, v1

    div-double/2addr v4, v6

    invoke-direct {v0, v2, v3, v4, v5}, Lcom/google/android/gms/maps/model/LatLng;-><init>(DD)V

    .line 47
    iget-object v1, p0, Lcom/google/android/location/places/bp;->a:Lcom/google/android/location/places/f;

    invoke-static {}, Lcom/google/android/gms/location/places/PlaceFilter;->h()Lcom/google/android/gms/location/places/PlaceFilter;

    move-result-object v2

    sget-object v3, Lcom/google/android/gms/location/places/internal/PlacesParams;->a:Lcom/google/android/gms/location/places/internal/PlacesParams;

    new-instance v4, Lcom/google/android/location/places/bq;

    invoke-direct {v4, p2}, Lcom/google/android/location/places/bq;-><init>(Lcom/google/android/location/places/x;)V

    invoke-virtual {v1, v0, v2, v3, v4}, Lcom/google/android/location/places/f;->a(Lcom/google/android/gms/maps/model/LatLng;Lcom/google/android/gms/location/places/PlaceFilter;Lcom/google/android/gms/location/places/internal/PlacesParams;Lcom/google/android/location/places/aj;)V

    .line 52
    return-void
.end method
