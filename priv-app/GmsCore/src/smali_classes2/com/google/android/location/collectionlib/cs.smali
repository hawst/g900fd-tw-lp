.class final Lcom/google/android/location/collectionlib/cs;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/location/collectionlib/ar;


# instance fields
.field final synthetic a:Lcom/google/android/location/collectionlib/cr;


# direct methods
.method constructor <init>(Lcom/google/android/location/collectionlib/cr;)V
    .locals 0

    .prologue
    .line 300
    iput-object p1, p0, Lcom/google/android/location/collectionlib/cs;->a:Lcom/google/android/location/collectionlib/cr;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 0

    .prologue
    .line 352
    return-void
.end method

.method public final a(ILcom/google/p/a/b/b/a;)V
    .locals 0

    .prologue
    .line 401
    return-void
.end method

.method public final a(ILjava/lang/String;)V
    .locals 0

    .prologue
    .line 368
    return-void
.end method

.method public final a(ILjava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 372
    return-void
.end method

.method public final a(Lcom/google/android/location/collectionlib/cp;)V
    .locals 0

    .prologue
    .line 376
    return-void
.end method

.method public final a(Lcom/google/p/a/b/b/a;)V
    .locals 2

    .prologue
    .line 387
    if-nez p1, :cond_0

    .line 389
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Prescan duration too long."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 391
    :cond_0
    iget-object v0, p0, Lcom/google/android/location/collectionlib/cs;->a:Lcom/google/android/location/collectionlib/cr;

    invoke-static {v0, p1}, Lcom/google/android/location/collectionlib/cr;->a(Lcom/google/android/location/collectionlib/cr;Lcom/google/p/a/b/b/a;)Lcom/google/p/a/b/b/a;

    .line 392
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 380
    iget-object v0, p0, Lcom/google/android/location/collectionlib/cs;->a:Lcom/google/android/location/collectionlib/cr;

    invoke-static {v0}, Lcom/google/android/location/collectionlib/cr;->g(Lcom/google/android/location/collectionlib/cr;)Lcom/google/android/location/collectionlib/ar;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 381
    iget-object v0, p0, Lcom/google/android/location/collectionlib/cs;->a:Lcom/google/android/location/collectionlib/cr;

    invoke-static {v0}, Lcom/google/android/location/collectionlib/cr;->g(Lcom/google/android/location/collectionlib/cr;)Lcom/google/android/location/collectionlib/ar;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/google/android/location/collectionlib/ar;->a(Ljava/lang/String;)V

    .line 383
    :cond_0
    return-void
.end method

.method public final a(Ljava/lang/String;ILcom/google/p/a/b/b/a;)V
    .locals 0

    .prologue
    .line 356
    return-void
.end method

.method public final a(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0

    .prologue
    .line 360
    return-void
.end method

.method public final a(Ljava/util/Map;)V
    .locals 0

    .prologue
    .line 397
    return-void
.end method

.method public final a(ZZ)V
    .locals 0

    .prologue
    .line 409
    return-void
.end method

.method public final b()V
    .locals 0

    .prologue
    .line 316
    return-void
.end method

.method public final c()V
    .locals 0

    .prologue
    .line 312
    return-void
.end method

.method public final d()V
    .locals 0

    .prologue
    .line 405
    return-void
.end method

.method public final e()V
    .locals 0

    .prologue
    .line 340
    return-void
.end method

.method public final f()V
    .locals 0

    .prologue
    .line 336
    return-void
.end method

.method public final g()V
    .locals 0

    .prologue
    .line 308
    return-void
.end method

.method public final h()V
    .locals 0

    .prologue
    .line 344
    return-void
.end method

.method public final h_(I)V
    .locals 0

    .prologue
    .line 304
    return-void
.end method

.method public final i()V
    .locals 0

    .prologue
    .line 348
    return-void
.end method

.method public final j()V
    .locals 0

    .prologue
    .line 364
    return-void
.end method
