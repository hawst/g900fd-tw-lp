.class final Lcom/google/android/location/copresence/l/o;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public final a:Lcom/google/android/location/copresence/a/a;

.field public final b:Lcom/google/android/location/copresence/l/w;


# direct methods
.method public constructor <init>(Lcom/google/android/location/copresence/a/a;Lcom/google/android/location/copresence/l/w;)V
    .locals 0

    .prologue
    .line 48
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 49
    iput-object p1, p0, Lcom/google/android/location/copresence/l/o;->a:Lcom/google/android/location/copresence/a/a;

    .line 50
    iput-object p2, p0, Lcom/google/android/location/copresence/l/o;->b:Lcom/google/android/location/copresence/l/w;

    .line 51
    return-void
.end method


# virtual methods
.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 60
    if-ne p0, p1, :cond_1

    .line 61
    const/4 v0, 0x1

    .line 70
    :cond_0
    :goto_0
    return v0

    .line 63
    :cond_1
    if-eqz p1, :cond_0

    .line 66
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    if-ne v1, v2, :cond_0

    .line 69
    check-cast p1, Lcom/google/android/location/copresence/l/o;

    .line 70
    iget-object v0, p0, Lcom/google/android/location/copresence/l/o;->a:Lcom/google/android/location/copresence/a/a;

    iget-object v1, p1, Lcom/google/android/location/copresence/l/o;->a:Lcom/google/android/location/copresence/a/a;

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/bu;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    .line 55
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/location/copresence/l/o;->a:Lcom/google/android/location/copresence/a/a;

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method
