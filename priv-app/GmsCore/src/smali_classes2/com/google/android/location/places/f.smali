.class public final Lcom/google/android/location/places/f;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final a:Landroid/os/Handler;

.field public b:Lcom/google/android/location/places/bi;

.field public c:Lcom/google/android/location/n/ai;

.field public final d:Landroid/content/Context;

.field private e:Lcom/google/android/location/places/t;

.field private final f:Lcom/google/android/location/d/j;


# direct methods
.method constructor <init>(Landroid/content/Context;Landroid/os/Handler;Lcom/google/android/location/places/t;Lcom/google/android/location/places/bi;Lcom/google/android/location/n/ai;Lcom/google/android/location/d/j;)V
    .locals 1

    .prologue
    .line 112
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 113
    invoke-static {p1}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iput-object v0, p0, Lcom/google/android/location/places/f;->d:Landroid/content/Context;

    .line 114
    invoke-static {p2}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Handler;

    iput-object v0, p0, Lcom/google/android/location/places/f;->a:Landroid/os/Handler;

    .line 115
    invoke-static {p3}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/places/t;

    iput-object v0, p0, Lcom/google/android/location/places/f;->e:Lcom/google/android/location/places/t;

    .line 116
    invoke-static {p4}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/places/bi;

    iput-object v0, p0, Lcom/google/android/location/places/f;->b:Lcom/google/android/location/places/bi;

    .line 117
    invoke-static {p5}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/n/ai;

    iput-object v0, p0, Lcom/google/android/location/places/f;->c:Lcom/google/android/location/n/ai;

    .line 118
    invoke-static {p6}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/d/j;

    iput-object v0, p0, Lcom/google/android/location/places/f;->f:Lcom/google/android/location/d/j;

    .line 119
    return-void
.end method

.method static a(Lcom/google/v/b/o;)Lcom/google/android/location/l/a/ai;
    .locals 4

    .prologue
    const/4 v0, 0x0

    const/4 v3, 0x6

    .line 587
    iget v1, p0, Lcom/google/v/b/o;->d:I

    const/16 v2, 0xc8

    if-eq v1, v2, :cond_1

    .line 588
    const-string v1, "Places"

    invoke-static {v1, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 589
    const-string v1, "Places"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "GLS returned HTTP response code "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v3, p0, Lcom/google/v/b/o;->d:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/location/n/aa;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 605
    :cond_0
    :goto_0
    return-object v0

    .line 596
    :cond_1
    :try_start_0
    invoke-virtual {p0}, Lcom/google/v/b/o;->ay_()Ljava/io/InputStream;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/gms/common/util/ab;->a(Ljava/io/InputStream;)[B

    move-result-object v1

    .line 597
    invoke-static {v1}, Lcom/google/android/location/l/a/ai;->a([B)Lcom/google/android/location/l/a/ai;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_0

    .line 598
    :catch_0
    move-exception v1

    .line 599
    const-string v2, "Places"

    invoke-static {v2, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 600
    const-string v2, "Places"

    const-string v3, "could not parse response"

    invoke-static {v2, v3, v1}, Lcom/google/android/location/n/aa;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method private static a(Lcom/google/android/location/l/a/ai;)Lcom/google/android/location/l/a/bk;
    .locals 5

    .prologue
    const/4 v3, 0x0

    const/4 v0, 0x0

    const/4 v4, 0x5

    .line 610
    iget-object v1, p0, Lcom/google/android/location/l/a/ai;->a:Ljava/lang/Integer;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/gms/common/internal/bu;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 611
    const-string v1, "Places"

    invoke-static {v1, v4}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 612
    const-string v1, "Places"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "RPC failed with status "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/google/android/location/l/a/ai;->a:Ljava/lang/Integer;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/location/n/aa;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 642
    :cond_0
    :goto_0
    return-object v0

    .line 619
    :cond_1
    iget-object v1, p0, Lcom/google/android/location/l/a/ai;->b:[Lcom/google/android/location/l/a/aj;

    array-length v1, v1

    if-nez v1, :cond_2

    .line 620
    const-string v1, "Places"

    invoke-static {v1, v4}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 621
    const-string v1, "Places"

    const-string v2, "ReplyElement with zero length"

    invoke-static {v1, v2}, Lcom/google/android/location/n/aa;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 626
    :cond_2
    iget-object v1, p0, Lcom/google/android/location/l/a/ai;->b:[Lcom/google/android/location/l/a/aj;

    aget-object v1, v1, v3

    .line 627
    iget-object v2, v1, Lcom/google/android/location/l/a/aj;->a:Ljava/lang/Integer;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/gms/common/internal/bu;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 628
    const-string v2, "Places"

    invoke-static {v2, v4}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 629
    const-string v2, "Places"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "GLS failed with status "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, v1, Lcom/google/android/location/l/a/aj;->a:Ljava/lang/Integer;

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Lcom/google/android/location/n/aa;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 634
    :cond_3
    iget-object v2, v1, Lcom/google/android/location/l/a/aj;->g:Lcom/google/android/location/l/a/bk;

    if-nez v2, :cond_4

    .line 635
    const-string v1, "Places"

    invoke-static {v1, v4}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 636
    const-string v1, "Places"

    const-string v2, "no place reply in ReplyElement"

    invoke-static {v1, v2}, Lcom/google/android/location/n/aa;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 641
    :cond_4
    iget-object v0, v1, Lcom/google/android/location/l/a/aj;->g:Lcom/google/android/location/l/a/bk;

    goto :goto_0
.end method

.method private static a(Lcom/google/android/location/l/a/bj;[Lcom/google/android/gms/location/places/internal/PlaceImpl;Ljava/util/Locale;)Ljava/util/List;
    .locals 10

    .prologue
    const/4 v1, 0x0

    const/4 v9, 0x3

    .line 686
    iget-object v0, p0, Lcom/google/android/location/l/a/bj;->b:Lcom/google/android/location/l/a/bc;

    if-nez v0, :cond_1

    .line 687
    const-string v0, "Places"

    invoke-static {v0, v9}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 688
    const-string v0, "Places"

    const-string v2, "gPlaceQueryResult.placeList is null"

    invoke-static {v0, v2}, Lcom/google/android/location/n/aa;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    move-object v0, v1

    .line 734
    :goto_0
    return-object v0

    .line 693
    :cond_1
    iget-object v4, p0, Lcom/google/android/location/l/a/bj;->b:Lcom/google/android/location/l/a/bc;

    .line 695
    iget-object v0, v4, Lcom/google/android/location/l/a/bc;->a:[I

    array-length v5, v0

    .line 696
    iget-object v0, v4, Lcom/google/android/location/l/a/bc;->b:[I

    array-length v6, v0

    .line 697
    if-eqz v6, :cond_3

    if-eq v6, v5, :cond_3

    .line 698
    const-string v0, "Places"

    invoke-static {v0, v9}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 699
    const-string v0, "Places"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "invalid numProbabilities: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/google/android/location/n/aa;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_2
    move-object v0, v1

    .line 701
    goto :goto_0

    .line 704
    :cond_3
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2, v5}, Ljava/util/ArrayList;-><init>(I)V

    .line 705
    const/4 v0, 0x0

    move v3, v0

    :goto_1
    if-ge v3, v5, :cond_b

    .line 706
    iget-object v0, v4, Lcom/google/android/location/l/a/bc;->a:[I

    aget v0, v0, v3

    .line 707
    array-length v7, p1

    if-lt v0, v7, :cond_5

    .line 708
    const-string v2, "Places"

    invoke-static {v2, v9}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 709
    const-string v2, "Places"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "invalid placeIndex: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/google/android/location/n/aa;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_4
    move-object v0, v1

    .line 711
    goto :goto_0

    .line 713
    :cond_5
    aget-object v7, p1, v0

    if-nez v7, :cond_7

    .line 714
    const-string v2, "Places"

    invoke-static {v2, v9}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 715
    const-string v2, "Places"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "invalid place at index: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/google/android/location/n/aa;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_6
    move-object v0, v1

    .line 717
    goto :goto_0

    .line 720
    :cond_7
    aget-object v7, p1, v0

    .line 721
    invoke-virtual {v7}, Lcom/google/android/gms/location/places/internal/PlaceImpl;->h()Ljava/util/Locale;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v0, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_9

    .line 722
    const-string v0, "Places"

    invoke-static {v0, v9}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 723
    const-string v0, "Places"

    const-string v2, "place is using a different locale than the device"

    invoke-static {v0, v2}, Lcom/google/android/location/n/aa;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_8
    move-object v0, v1

    .line 725
    goto/16 :goto_0

    .line 728
    :cond_9
    const/high16 v0, 0x3f800000    # 1.0f

    int-to-float v8, v5

    div-float/2addr v0, v8

    .line 729
    if-ge v3, v6, :cond_a

    .line 730
    iget-object v0, v4, Lcom/google/android/location/l/a/bc;->b:[I

    aget v0, v0, v3

    int-to-float v0, v0

    const/high16 v8, 0x42c80000    # 100.0f

    div-float/2addr v0, v8

    .line 732
    :cond_a
    invoke-static {v7, v0}, Lcom/google/android/gms/location/places/PlaceLikelihood;->a(Lcom/google/android/gms/location/places/internal/PlaceImpl;F)Lcom/google/android/gms/location/places/PlaceLikelihood;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 705
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto/16 :goto_1

    :cond_b
    move-object v0, v2

    .line 734
    goto/16 :goto_0
.end method

.method private static a(ILjava/util/List;Lcom/google/android/location/places/aj;)V
    .locals 0

    .prologue
    .line 441
    invoke-interface {p2, p0, p1}, Lcom/google/android/location/places/aj;->a(ILjava/util/List;)V

    .line 442
    return-void
.end method

.method private static a(ILjava/util/List;Lcom/google/android/location/places/b;)V
    .locals 0

    .prologue
    .line 582
    invoke-interface {p2, p0, p1}, Lcom/google/android/location/places/b;->a(ILjava/util/List;)V

    .line 583
    return-void
.end method

.method private static a(Lcom/google/android/location/places/aj;)V
    .locals 2

    .prologue
    .line 433
    const/4 v0, 0x7

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v1

    invoke-static {v0, v1, p0}, Lcom/google/android/location/places/f;->a(ILjava/util/List;Lcom/google/android/location/places/aj;)V

    .line 435
    return-void
.end method

.method private static a(Lcom/google/android/location/places/b;)V
    .locals 2

    .prologue
    .line 574
    const/4 v0, 0x7

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v1

    invoke-static {v0, v1, p0}, Lcom/google/android/location/places/f;->a(ILjava/util/List;Lcom/google/android/location/places/b;)V

    .line 576
    return-void
.end method

.method private static a(Lcom/google/android/location/places/o;)V
    .locals 1

    .prologue
    .line 742
    sget-object v0, Lcom/google/android/location/places/g;->a:[I

    invoke-virtual {p0}, Lcom/google/android/location/places/o;->ordinal()I

    .line 746
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/maps/model/LatLng;Lcom/google/android/gms/location/places/PlaceFilter;Lcom/google/android/gms/location/places/internal/PlacesParams;Lcom/google/android/location/places/aj;)V
    .locals 10

    .prologue
    const-wide v8, 0x416312d000000000L    # 1.0E7

    .line 199
    iget-object v0, p0, Lcom/google/android/location/places/f;->d:Landroid/content/Context;

    invoke-static {v0, p2, p3}, Lcom/google/android/location/places/bo;->a(Landroid/content/Context;Lcom/google/android/gms/location/places/PlaceFilter;Lcom/google/android/gms/location/places/internal/PlacesParams;)Lcom/google/android/location/l/a/bi;

    move-result-object v0

    new-instance v1, Lcom/google/android/location/l/a/bn;

    invoke-direct {v1}, Lcom/google/android/location/l/a/bn;-><init>()V

    new-instance v2, Landroid/location/Location;

    const-string v3, "places api"

    invoke-direct {v2, v3}, Landroid/location/Location;-><init>(Ljava/lang/String;)V

    iget-wide v4, p1, Lcom/google/android/gms/maps/model/LatLng;->a:D

    invoke-virtual {v2, v4, v5}, Landroid/location/Location;->setLatitude(D)V

    iget-wide v4, p1, Lcom/google/android/gms/maps/model/LatLng;->b:D

    invoke-virtual {v2, v4, v5}, Landroid/location/Location;->setLongitude(D)V

    new-instance v3, Lcom/google/android/location/l/a/am;

    invoke-direct {v3}, Lcom/google/android/location/l/a/am;-><init>()V

    new-instance v4, Lcom/google/android/location/l/a/ag;

    invoke-direct {v4}, Lcom/google/android/location/l/a/ag;-><init>()V

    invoke-virtual {v2}, Landroid/location/Location;->getLatitude()D

    move-result-wide v6

    mul-double/2addr v6, v8

    double-to-int v5, v6

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    iput-object v5, v4, Lcom/google/android/location/l/a/ag;->a:Ljava/lang/Integer;

    invoke-virtual {v2}, Landroid/location/Location;->getLongitude()D

    move-result-wide v6

    mul-double/2addr v6, v8

    double-to-int v5, v6

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    iput-object v5, v4, Lcom/google/android/location/l/a/ag;->b:Ljava/lang/Integer;

    iput-object v4, v3, Lcom/google/android/location/l/a/am;->a:Lcom/google/android/location/l/a/ag;

    invoke-virtual {v2}, Landroid/location/Location;->getTime()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    iput-object v4, v3, Lcom/google/android/location/l/a/am;->f:Ljava/lang/Long;

    invoke-virtual {v2}, Landroid/location/Location;->hasAccuracy()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-virtual {v2}, Landroid/location/Location;->getAccuracy()F

    move-result v4

    float-to-int v4, v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    iput-object v4, v3, Lcom/google/android/location/l/a/am;->c:Ljava/lang/Integer;

    :cond_0
    invoke-virtual {v2}, Landroid/location/Location;->hasAltitude()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-virtual {v2}, Landroid/location/Location;->getAltitude()D

    move-result-wide v4

    double-to-int v4, v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    iput-object v4, v3, Lcom/google/android/location/l/a/am;->j:Ljava/lang/Integer;

    :cond_1
    invoke-virtual {v2}, Landroid/location/Location;->hasBearing()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-virtual {v2}, Landroid/location/Location;->getBearing()F

    move-result v4

    float-to-int v4, v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    iput-object v4, v3, Lcom/google/android/location/l/a/am;->m:Ljava/lang/Integer;

    :cond_2
    invoke-virtual {v2}, Landroid/location/Location;->hasSpeed()Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-virtual {v2}, Landroid/location/Location;->getSpeed()F

    move-result v2

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    iput-object v2, v3, Lcom/google/android/location/l/a/am;->p:Ljava/lang/Float;

    :cond_3
    iput-object v3, v1, Lcom/google/android/location/l/a/bn;->a:Lcom/google/android/location/l/a/am;

    iput-object v1, v0, Lcom/google/android/location/l/a/bi;->c:Lcom/google/android/location/l/a/bn;

    .line 201
    invoke-static {p3, v0}, Lcom/google/android/location/places/bo;->a(Lcom/google/android/gms/location/places/internal/PlacesParams;Lcom/google/android/location/l/a/bi;)Lcom/google/android/location/l/a/ak;

    move-result-object v0

    .line 203
    new-instance v1, Lcom/google/android/location/places/k;

    sget-object v2, Lcom/google/android/location/places/o;->d:Lcom/google/android/location/places/o;

    invoke-direct {v1, p0, p3, p4, v2}, Lcom/google/android/location/places/k;-><init>(Lcom/google/android/location/places/f;Lcom/google/android/gms/location/places/internal/PlacesParams;Lcom/google/android/location/places/aj;Lcom/google/android/location/places/o;)V

    invoke-virtual {p0, v0, v1}, Lcom/google/android/location/places/f;->a(Lcom/google/android/location/l/a/ak;Lcom/google/v/b/n;)V

    .line 205
    return-void
.end method

.method public final a(Lcom/google/android/gms/maps/model/LatLngBounds;ILjava/lang/String;Lcom/google/android/gms/location/places/PlaceFilter;Lcom/google/android/gms/location/places/internal/PlacesParams;Lcom/google/android/location/places/aj;)V
    .locals 4

    .prologue
    .line 134
    iget-object v0, p0, Lcom/google/android/location/places/f;->d:Landroid/content/Context;

    invoke-static {v0, p4, p5}, Lcom/google/android/location/places/bo;->a(Landroid/content/Context;Lcom/google/android/gms/location/places/PlaceFilter;Lcom/google/android/gms/location/places/internal/PlacesParams;)Lcom/google/android/location/l/a/bi;

    move-result-object v0

    new-instance v1, Lcom/google/android/location/l/a/bq;

    invoke-direct {v1}, Lcom/google/android/location/l/a/bq;-><init>()V

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iput-object v2, v1, Lcom/google/android/location/l/a/bq;->b:Ljava/lang/Integer;

    if-eqz p1, :cond_0

    new-instance v2, Lcom/google/android/location/l/a/ca;

    invoke-direct {v2}, Lcom/google/android/location/l/a/ca;-><init>()V

    iget-object v3, p1, Lcom/google/android/gms/maps/model/LatLngBounds;->a:Lcom/google/android/gms/maps/model/LatLng;

    invoke-static {v3}, Lcom/google/android/location/places/bo;->a(Lcom/google/android/gms/maps/model/LatLng;)Lcom/google/android/location/l/a/ag;

    move-result-object v3

    iput-object v3, v2, Lcom/google/android/location/l/a/ca;->a:Lcom/google/android/location/l/a/ag;

    iget-object v3, p1, Lcom/google/android/gms/maps/model/LatLngBounds;->b:Lcom/google/android/gms/maps/model/LatLng;

    invoke-static {v3}, Lcom/google/android/location/places/bo;->a(Lcom/google/android/gms/maps/model/LatLng;)Lcom/google/android/location/l/a/ag;

    move-result-object v3

    iput-object v3, v2, Lcom/google/android/location/l/a/ca;->b:Lcom/google/android/location/l/a/ag;

    iput-object v2, v1, Lcom/google/android/location/l/a/bq;->a:Lcom/google/android/location/l/a/ca;

    :cond_0
    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    iput-object p3, v1, Lcom/google/android/location/l/a/bq;->c:Ljava/lang/String;

    :cond_1
    iput-object v1, v0, Lcom/google/android/location/l/a/bi;->e:Lcom/google/android/location/l/a/bq;

    .line 136
    invoke-static {p5, v0}, Lcom/google/android/location/places/bo;->a(Lcom/google/android/gms/location/places/internal/PlacesParams;Lcom/google/android/location/l/a/bi;)Lcom/google/android/location/l/a/ak;

    move-result-object v0

    .line 138
    new-instance v1, Lcom/google/android/location/places/k;

    sget-object v2, Lcom/google/android/location/places/o;->a:Lcom/google/android/location/places/o;

    invoke-direct {v1, p0, p5, p6, v2}, Lcom/google/android/location/places/k;-><init>(Lcom/google/android/location/places/f;Lcom/google/android/gms/location/places/internal/PlacesParams;Lcom/google/android/location/places/aj;Lcom/google/android/location/places/o;)V

    invoke-virtual {p0, v0, v1}, Lcom/google/android/location/places/f;->a(Lcom/google/android/location/l/a/ak;Lcom/google/v/b/n;)V

    .line 140
    return-void
.end method

.method public final a(Lcom/google/android/location/l/a/ak;Lcom/google/v/b/n;)V
    .locals 3

    .prologue
    .line 329
    invoke-static {p1}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 332
    new-instance v0, Lcom/google/v/b/j;

    const-string v1, "g:loc/qp"

    invoke-static {p1}, Lcom/google/protobuf/nano/j;->toByteArray(Lcom/google/protobuf/nano/j;)[B

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/google/v/b/j;-><init>(Ljava/lang/String;[B)V

    .line 334
    invoke-virtual {v0, p2}, Lcom/google/v/b/m;->a(Lcom/google/v/b/n;)V

    .line 335
    iget-object v1, p0, Lcom/google/android/location/places/f;->f:Lcom/google/android/location/d/j;

    invoke-virtual {v1, v0}, Lcom/google/android/location/d/j;->a(Lcom/google/v/b/m;)V

    .line 336
    return-void
.end method

.method public final a(Lcom/google/android/location/places/h;)V
    .locals 10

    .prologue
    const/4 v3, 0x0

    .line 517
    iget-object v0, p0, Lcom/google/android/location/places/f;->c:Lcom/google/android/location/n/ai;

    invoke-virtual {v0}, Lcom/google/android/location/n/ai;->a()J

    move-result-wide v4

    .line 519
    iget-object v0, p1, Lcom/google/android/location/places/h;->e:Lcom/google/android/location/places/o;

    .line 521
    iget-object v6, p1, Lcom/google/android/location/places/h;->b:Lcom/google/android/location/places/b;

    .line 522
    iget-object v1, p1, Lcom/google/android/location/places/h;->c:Ljava/lang/Exception;

    if-eqz v1, :cond_0

    .line 523
    invoke-static {v6}, Lcom/google/android/location/places/f;->a(Lcom/google/android/location/places/b;)V

    .line 570
    :goto_0
    return-void

    .line 527
    :cond_0
    iget-object v1, p1, Lcom/google/android/location/places/h;->d:Lcom/google/v/b/o;

    .line 528
    invoke-static {v1}, Lcom/google/android/location/places/f;->a(Lcom/google/v/b/o;)Lcom/google/android/location/l/a/ai;

    move-result-object v1

    .line 529
    if-nez v1, :cond_1

    .line 530
    invoke-static {v6}, Lcom/google/android/location/places/f;->a(Lcom/google/android/location/places/b;)V

    goto :goto_0

    .line 535
    :cond_1
    iget-object v2, v1, Lcom/google/android/location/l/a/ai;->a:Ljava/lang/Integer;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-static {v2, v7}, Lcom/google/android/gms/common/internal/bu;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 536
    iget-object v2, v1, Lcom/google/android/location/l/a/ai;->c:Ljava/lang/String;

    .line 537
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_2

    .line 538
    iget-object v7, p0, Lcom/google/android/location/places/f;->d:Landroid/content/Context;

    invoke-virtual {v7}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v7

    invoke-static {v7, v2}, Lcom/google/android/location/os/real/ah;->a(Landroid/content/Context;Ljava/lang/String;)V

    .line 542
    :cond_2
    invoke-static {v1}, Lcom/google/android/location/places/f;->a(Lcom/google/android/location/l/a/ai;)Lcom/google/android/location/l/a/bk;

    move-result-object v1

    .line 545
    if-nez v1, :cond_3

    .line 546
    invoke-static {v6}, Lcom/google/android/location/places/f;->a(Lcom/google/android/location/places/b;)V

    goto :goto_0

    .line 550
    :cond_3
    iget-object v1, v1, Lcom/google/android/location/l/a/bk;->b:[Lcom/google/android/location/l/a/bj;

    aget-object v7, v1, v3

    .line 551
    if-nez v7, :cond_4

    .line 552
    invoke-static {v6}, Lcom/google/android/location/places/f;->a(Lcom/google/android/location/places/b;)V

    goto :goto_0

    .line 556
    :cond_4
    iget-object v1, v7, Lcom/google/android/location/l/a/bj;->a:Ljava/lang/Integer;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/gms/common/internal/bu;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_5

    .line 558
    invoke-static {v6}, Lcom/google/android/location/places/f;->a(Lcom/google/android/location/places/b;)V

    goto :goto_0

    .line 562
    :cond_5
    iget-wide v8, p1, Lcom/google/android/location/places/h;->g:J

    invoke-static {v0}, Lcom/google/android/location/places/f;->a(Lcom/google/android/location/places/o;)V

    .line 564
    iget-object v0, p1, Lcom/google/android/location/places/h;->f:Ljava/lang/Object;

    iget-object v1, v7, Lcom/google/android/location/l/a/bj;->d:[Lcom/google/android/location/l/a/br;

    array-length v8, v1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1, v8}, Ljava/util/ArrayList;-><init>(I)V

    move v2, v3

    :goto_1
    if-ge v2, v8, :cond_7

    iget-object v9, v7, Lcom/google/android/location/l/a/bj;->d:[Lcom/google/android/location/l/a/br;

    aget-object v9, v9, v2

    invoke-static {v9}, Lcom/google/android/location/places/v;->a(Lcom/google/android/location/l/a/br;)Lcom/google/android/gms/location/places/AutocompletePrediction;

    move-result-object v9

    if-nez v9, :cond_6

    const/4 v0, 0x0

    .line 569
    :goto_2
    invoke-static {v3, v0, v6}, Lcom/google/android/location/places/f;->a(ILjava/util/List;Lcom/google/android/location/places/b;)V

    goto :goto_0

    .line 564
    :cond_6
    invoke-interface {v1, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_7
    instance-of v2, v0, Lcom/google/android/location/places/bl;

    if-eqz v2, :cond_8

    check-cast v0, Lcom/google/android/location/places/bl;

    iget-object v2, p0, Lcom/google/android/location/places/f;->b:Lcom/google/android/location/places/bi;

    invoke-virtual {v2, v0, v1, v4, v5}, Lcom/google/android/location/places/bi;->a(Lcom/google/android/location/places/bl;Ljava/util/List;J)V

    :cond_8
    move-object v0, v1

    goto :goto_2
.end method

.method public final a(Lcom/google/android/location/places/k;)V
    .locals 43

    .prologue
    .line 446
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/location/places/f;->c:Lcom/google/android/location/n/ai;

    invoke-virtual {v6}, Lcom/google/android/location/n/ai;->a()J

    move-result-wide v32

    .line 448
    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/google/android/location/places/k;->e:Lcom/google/android/location/places/o;

    move-object/from16 v34, v0

    .line 450
    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/google/android/location/places/k;->b:Lcom/google/android/location/places/aj;

    move-object/from16 v35, v0

    .line 451
    move-object/from16 v0, p1

    iget-object v6, v0, Lcom/google/android/location/places/k;->c:Ljava/lang/Exception;

    if-eqz v6, :cond_0

    .line 452
    invoke-static/range {v35 .. v35}, Lcom/google/android/location/places/f;->a(Lcom/google/android/location/places/aj;)V

    .line 512
    :goto_0
    return-void

    .line 456
    :cond_0
    move-object/from16 v0, p1

    iget-object v6, v0, Lcom/google/android/location/places/k;->d:Lcom/google/v/b/o;

    .line 457
    invoke-static {v6}, Lcom/google/android/location/places/f;->a(Lcom/google/v/b/o;)Lcom/google/android/location/l/a/ai;

    move-result-object v6

    .line 458
    if-nez v6, :cond_1

    .line 459
    invoke-static/range {v35 .. v35}, Lcom/google/android/location/places/f;->a(Lcom/google/android/location/places/aj;)V

    goto :goto_0

    .line 464
    :cond_1
    iget-object v7, v6, Lcom/google/android/location/l/a/ai;->a:Ljava/lang/Integer;

    const/4 v8, 0x0

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/google/android/gms/common/internal/bu;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 465
    iget-object v7, v6, Lcom/google/android/location/l/a/ai;->c:Ljava/lang/String;

    .line 466
    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_2

    .line 467
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/location/places/f;->d:Landroid/content/Context;

    invoke-virtual {v8}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v8

    invoke-static {v8, v7}, Lcom/google/android/location/os/real/ah;->a(Landroid/content/Context;Ljava/lang/String;)V

    .line 471
    :cond_2
    invoke-static {v6}, Lcom/google/android/location/places/f;->a(Lcom/google/android/location/l/a/ai;)Lcom/google/android/location/l/a/bk;

    move-result-object v36

    .line 472
    if-nez v36, :cond_3

    .line 473
    invoke-static/range {v35 .. v35}, Lcom/google/android/location/places/f;->a(Lcom/google/android/location/places/aj;)V

    goto :goto_0

    .line 477
    :cond_3
    move-object/from16 v0, v36

    iget-object v6, v0, Lcom/google/android/location/l/a/bk;->a:[Lcom/google/android/location/l/a/as;

    if-nez v6, :cond_4

    const/4 v6, 0x0

    .line 481
    :goto_1
    if-nez v6, :cond_1b

    .line 482
    invoke-static/range {v35 .. v35}, Lcom/google/android/location/places/f;->a(Lcom/google/android/location/places/aj;)V

    goto :goto_0

    .line 477
    :cond_4
    move-object/from16 v0, v36

    iget-object v6, v0, Lcom/google/android/location/l/a/bk;->a:[Lcom/google/android/location/l/a/as;

    array-length v6, v6

    new-array v0, v6, [Lcom/google/android/gms/location/places/internal/PlaceImpl;

    move-object/from16 v29, v0

    const/4 v7, 0x0

    move-object/from16 v0, v36

    iget-object v0, v0, Lcom/google/android/location/l/a/bk;->a:[Lcom/google/android/location/l/a/as;

    move-object/from16 v37, v0

    move-object/from16 v0, v37

    array-length v0, v0

    move/from16 v38, v0

    const/4 v6, 0x0

    move/from16 v30, v6

    move/from16 v31, v7

    :goto_2
    move/from16 v0, v30

    move/from16 v1, v38

    if-ge v0, v1, :cond_1a

    aget-object v19, v37, v30

    move-object/from16 v0, v19

    iget-object v6, v0, Lcom/google/android/location/l/a/as;->a:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/location/places/f;->e:Lcom/google/android/location/places/t;

    move-wide/from16 v0, v32

    invoke-virtual {v7, v6, v0, v1}, Lcom/google/android/location/places/t;->a(Ljava/lang/String;J)Lcom/google/android/gms/location/places/internal/PlaceImpl;

    move-result-object v6

    if-eqz v6, :cond_5

    invoke-virtual {v6}, Lcom/google/android/gms/location/places/internal/PlaceImpl;->p()J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    move-object/from16 v0, v19

    iget-object v8, v0, Lcom/google/android/location/l/a/as;->l:Ljava/lang/Long;

    invoke-static {v7, v8}, Lcom/google/android/gms/common/internal/bu;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_7

    :cond_5
    move-object/from16 v0, v19

    iget-object v6, v0, Lcom/google/android/location/l/a/as;->a:Ljava/lang/String;

    if-nez v6, :cond_9

    const-string v6, "Places"

    const/4 v7, 0x6

    invoke-static {v6, v7}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v6

    if-eqz v6, :cond_6

    const-string v6, "Places"

    const-string v7, "received place lacks id"

    invoke-static {v6, v7}, Lcom/google/android/location/n/aa;->b(Ljava/lang/String;Ljava/lang/String;)V

    :cond_6
    const/4 v6, 0x0

    :goto_3
    if-eqz v6, :cond_7

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/location/places/f;->e:Lcom/google/android/location/places/t;

    move-wide/from16 v0, v32

    invoke-virtual {v7, v6, v0, v1}, Lcom/google/android/location/places/t;->a(Lcom/google/android/gms/location/places/internal/PlaceImpl;J)V

    :cond_7
    if-nez v6, :cond_8

    const-string v7, "Places"

    const/4 v8, 0x5

    invoke-static {v7, v8}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v7

    if-eqz v7, :cond_8

    const-string v7, "Places"

    const-string v8, "could not parse place in reply"

    invoke-static {v7, v8}, Lcom/google/android/location/n/aa;->e(Ljava/lang/String;Ljava/lang/String;)V

    :cond_8
    add-int/lit8 v7, v31, 0x1

    aput-object v6, v29, v31

    add-int/lit8 v6, v30, 0x1

    move/from16 v30, v6

    move/from16 v31, v7

    goto :goto_2

    :cond_9
    move-object/from16 v0, v19

    iget-object v6, v0, Lcom/google/android/location/l/a/as;->l:Ljava/lang/Long;

    if-nez v6, :cond_b

    const-string v6, "Places"

    const/4 v7, 0x6

    invoke-static {v6, v7}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v6

    if-eqz v6, :cond_a

    const-string v6, "Places"

    const-string v7, "received place lacks timestamp"

    invoke-static {v6, v7}, Lcom/google/android/location/n/aa;->b(Ljava/lang/String;Ljava/lang/String;)V

    :cond_a
    const/4 v6, 0x0

    goto :goto_3

    :cond_b
    move-object/from16 v0, v19

    iget-object v6, v0, Lcom/google/android/location/l/a/as;->b:[Ljava/lang/String;

    array-length v7, v6

    if-nez v7, :cond_d

    const-string v6, "Places"

    const/4 v8, 0x5

    invoke-static {v6, v8}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v6

    if-eqz v6, :cond_c

    const-string v6, "Places"

    const-string v8, "place is missing type. Defaulting to \'other\'"

    invoke-static {v6, v8}, Lcom/google/android/location/n/aa;->e(Ljava/lang/String;Ljava/lang/String;)V

    :cond_c
    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/String;

    const/4 v8, 0x0

    sget-object v9, Lcom/google/android/gms/location/places/PlaceType;->bv:Lcom/google/android/gms/location/places/PlaceType;

    invoke-virtual {v9}, Lcom/google/android/gms/location/places/PlaceType;->toString()Ljava/lang/String;

    move-result-object v9

    aput-object v9, v6, v8

    move-object/from16 v0, v19

    iput-object v6, v0, Lcom/google/android/location/l/a/as;->b:[Ljava/lang/String;

    :cond_d
    move-object/from16 v0, v19

    iget-object v6, v0, Lcom/google/android/location/l/a/as;->e:Lcom/google/android/location/l/a/ba;

    if-nez v6, :cond_f

    const-string v6, "Places"

    const/4 v7, 0x6

    invoke-static {v6, v7}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v6

    if-eqz v6, :cond_e

    const-string v6, "Places"

    const-string v7, "received place lacks geometry"

    invoke-static {v6, v7}, Lcom/google/android/location/n/aa;->b(Ljava/lang/String;Ljava/lang/String;)V

    :cond_e
    const/4 v6, 0x0

    goto :goto_3

    :cond_f
    move-object/from16 v0, v19

    iget-object v6, v0, Lcom/google/android/location/l/a/as;->e:Lcom/google/android/location/l/a/ba;

    iget-object v6, v6, Lcom/google/android/location/l/a/ba;->a:Lcom/google/android/location/l/a/ag;

    if-nez v6, :cond_11

    const-string v6, "Places"

    const/4 v7, 0x6

    invoke-static {v6, v7}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v6

    if-eqz v6, :cond_10

    const-string v6, "Places"

    const-string v7, "received place lacks latlng"

    invoke-static {v6, v7}, Lcom/google/android/location/n/aa;->b(Ljava/lang/String;Ljava/lang/String;)V

    :cond_10
    const/4 v6, 0x0

    goto/16 :goto_3

    :cond_11
    move-object/from16 v0, v19

    iget-object v0, v0, Lcom/google/android/location/l/a/as;->a:Ljava/lang/String;

    move-object/from16 v20, v0

    move-object/from16 v0, v19

    iget-object v6, v0, Lcom/google/android/location/l/a/as;->l:Ljava/lang/Long;

    invoke-virtual {v6}, Ljava/lang/Long;->longValue()J

    move-result-wide v22

    new-instance v21, Ljava/util/ArrayList;

    move-object/from16 v0, v21

    invoke-direct {v0, v7}, Ljava/util/ArrayList;-><init>(I)V

    const/4 v6, 0x0

    :goto_4
    if-ge v6, v7, :cond_12

    move-object/from16 v0, v19

    iget-object v8, v0, Lcom/google/android/location/l/a/as;->b:[Ljava/lang/String;

    aget-object v8, v8, v6

    invoke-static {v8}, Lcom/google/android/gms/location/places/PlaceType;->a(Ljava/lang/String;)Lcom/google/android/gms/location/places/PlaceType;

    move-result-object v8

    move-object/from16 v0, v21

    invoke-interface {v0, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v6, v6, 0x1

    goto :goto_4

    :cond_12
    invoke-static/range {v19 .. v19}, Lcom/google/android/location/places/v;->a(Lcom/google/android/location/l/a/as;)Landroid/os/Bundle;

    move-result-object v24

    move-object/from16 v0, v19

    iget-object v9, v0, Lcom/google/android/location/l/a/as;->e:Lcom/google/android/location/l/a/ba;

    iget-object v6, v9, Lcom/google/android/location/l/a/ba;->a:Lcom/google/android/location/l/a/ag;

    invoke-static {v6}, Lcom/google/android/location/places/v;->a(Lcom/google/android/location/l/a/ag;)Lcom/google/android/gms/maps/model/LatLng;

    move-result-object v25

    const/4 v6, 0x0

    iget-object v7, v9, Lcom/google/android/location/l/a/ba;->b:Ljava/lang/Integer;

    if-eqz v7, :cond_13

    iget-object v6, v9, Lcom/google/android/location/l/a/ba;->b:Ljava/lang/Integer;

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v6

    int-to-float v6, v6

    const/high16 v7, 0x447a0000    # 1000.0f

    div-float/2addr v6, v7

    :cond_13
    const/4 v7, 0x0

    iget-object v8, v9, Lcom/google/android/location/l/a/ba;->c:Lcom/google/android/location/l/a/ca;

    if-eqz v8, :cond_14

    iget-object v7, v9, Lcom/google/android/location/l/a/ba;->c:Lcom/google/android/location/l/a/ca;

    iget-object v8, v7, Lcom/google/android/location/l/a/ca;->a:Lcom/google/android/location/l/a/ag;

    invoke-static {v8}, Lcom/google/android/location/places/v;->a(Lcom/google/android/location/l/a/ag;)Lcom/google/android/gms/maps/model/LatLng;

    move-result-object v8

    iget-object v7, v7, Lcom/google/android/location/l/a/ca;->b:Lcom/google/android/location/l/a/ag;

    invoke-static {v7}, Lcom/google/android/location/places/v;->a(Lcom/google/android/location/l/a/ag;)Lcom/google/android/gms/maps/model/LatLng;

    move-result-object v10

    new-instance v7, Lcom/google/android/gms/maps/model/LatLngBounds;

    invoke-direct {v7, v8, v10}, Lcom/google/android/gms/maps/model/LatLngBounds;-><init>(Lcom/google/android/gms/maps/model/LatLng;Lcom/google/android/gms/maps/model/LatLng;)V

    :cond_14
    const-string v8, ""

    iget-object v10, v9, Lcom/google/android/location/l/a/ba;->d:Ljava/lang/String;

    if-eqz v10, :cond_15

    iget-object v8, v9, Lcom/google/android/location/l/a/ba;->d:Ljava/lang/String;

    :cond_15
    const/4 v9, 0x0

    move-object/from16 v0, v19

    iget-object v10, v0, Lcom/google/android/location/l/a/as;->f:Ljava/lang/String;

    if-eqz v10, :cond_16

    move-object/from16 v0, v19

    iget-object v9, v0, Lcom/google/android/location/l/a/as;->f:Ljava/lang/String;

    invoke-static {v9}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v9

    :cond_16
    const/4 v10, 0x0

    move-object/from16 v0, v19

    iget-object v11, v0, Lcom/google/android/location/l/a/as;->h:Ljava/lang/Boolean;

    if-eqz v11, :cond_17

    move-object/from16 v0, v19

    iget-object v10, v0, Lcom/google/android/location/l/a/as;->h:Ljava/lang/Boolean;

    invoke-virtual {v10}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v10

    :cond_17
    const/high16 v11, -0x40800000    # -1.0f

    move-object/from16 v0, v19

    iget-object v12, v0, Lcom/google/android/location/l/a/as;->j:Ljava/lang/Integer;

    if-eqz v12, :cond_18

    move-object/from16 v0, v19

    iget-object v11, v0, Lcom/google/android/location/l/a/as;->j:Ljava/lang/Integer;

    invoke-virtual {v11}, Ljava/lang/Integer;->intValue()I

    move-result v11

    int-to-float v11, v11

    const/high16 v12, 0x41200000    # 10.0f

    div-float/2addr v11, v12

    :cond_18
    const/4 v12, -0x1

    move-object/from16 v0, v19

    iget-object v13, v0, Lcom/google/android/location/l/a/as;->k:Ljava/lang/Integer;

    if-eqz v13, :cond_19

    move-object/from16 v0, v19

    iget-object v12, v0, Lcom/google/android/location/l/a/as;->k:Ljava/lang/Integer;

    invoke-virtual {v12}, Ljava/lang/Integer;->intValue()I

    move-result v12

    :cond_19
    const/16 v18, 0x0

    const/16 v17, 0x0

    const/16 v16, 0x0

    const/4 v15, 0x0

    const/4 v14, 0x0

    const/4 v13, 0x0

    move-object/from16 v0, v19

    iget-object v0, v0, Lcom/google/android/location/l/a/as;->c:[Lcom/google/android/location/l/a/bd;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    array-length v0, v0

    move/from16 v26, v0

    if-lez v26, :cond_1d

    move-object/from16 v0, v19

    iget-object v13, v0, Lcom/google/android/location/l/a/as;->c:[Lcom/google/android/location/l/a/bd;

    const/4 v14, 0x0

    aget-object v13, v13, v14

    iget-object v0, v13, Lcom/google/android/location/l/a/bd;->a:Ljava/lang/String;

    move-object/from16 v18, v0

    iget-object v0, v13, Lcom/google/android/location/l/a/bd;->b:Ljava/lang/String;

    move-object/from16 v17, v0

    iget-object v0, v13, Lcom/google/android/location/l/a/bd;->c:Ljava/lang/String;

    move-object/from16 v16, v0

    iget-object v15, v13, Lcom/google/android/location/l/a/bd;->d:Ljava/lang/String;

    iget-object v14, v13, Lcom/google/android/location/l/a/bd;->e:Ljava/lang/String;

    iget-object v13, v13, Lcom/google/android/location/l/a/bd;->g:[Ljava/lang/String;

    invoke-static {v13}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v13

    move-object/from16 v28, v18

    :goto_5
    new-instance v27, Lcom/google/android/gms/location/places/internal/g;

    invoke-direct/range {v27 .. v27}, Lcom/google/android/gms/location/places/internal/g;-><init>()V

    move-object/from16 v0, v20

    move-object/from16 v1, v27

    iput-object v0, v1, Lcom/google/android/gms/location/places/internal/g;->b:Ljava/lang/String;

    move-object/from16 v0, v21

    move-object/from16 v1, v27

    iput-object v0, v1, Lcom/google/android/gms/location/places/internal/g;->n:Ljava/util/List;

    move-object/from16 v0, v24

    move-object/from16 v1, v27

    iput-object v0, v1, Lcom/google/android/gms/location/places/internal/g;->c:Landroid/os/Bundle;

    move-object/from16 v0, v17

    move-object/from16 v1, v27

    iput-object v0, v1, Lcom/google/android/gms/location/places/internal/g;->d:Ljava/lang/String;

    move-object/from16 v0, v16

    move-object/from16 v1, v27

    iput-object v0, v1, Lcom/google/android/gms/location/places/internal/g;->o:Ljava/lang/String;

    move-object/from16 v0, v27

    iput-object v15, v0, Lcom/google/android/gms/location/places/internal/g;->p:Ljava/lang/String;

    move-object/from16 v0, v27

    iput-object v14, v0, Lcom/google/android/gms/location/places/internal/g;->q:Ljava/lang/String;

    move-object/from16 v0, v27

    iput-object v13, v0, Lcom/google/android/gms/location/places/internal/g;->r:Ljava/util/List;

    move-object/from16 v0, v25

    move-object/from16 v1, v27

    iput-object v0, v1, Lcom/google/android/gms/location/places/internal/g;->e:Lcom/google/android/gms/maps/model/LatLng;

    move-object/from16 v0, v27

    iput v6, v0, Lcom/google/android/gms/location/places/internal/g;->f:F

    move-object/from16 v0, v27

    iput-object v7, v0, Lcom/google/android/gms/location/places/internal/g;->g:Lcom/google/android/gms/maps/model/LatLngBounds;

    move-object/from16 v0, v27

    iput-object v8, v0, Lcom/google/android/gms/location/places/internal/g;->h:Ljava/lang/String;

    move-object/from16 v0, v27

    iput-object v9, v0, Lcom/google/android/gms/location/places/internal/g;->i:Landroid/net/Uri;

    move-object/from16 v0, v27

    iput-boolean v10, v0, Lcom/google/android/gms/location/places/internal/g;->j:Z

    move-object/from16 v0, v27

    iput v11, v0, Lcom/google/android/gms/location/places/internal/g;->k:F

    move-object/from16 v0, v27

    iput v12, v0, Lcom/google/android/gms/location/places/internal/g;->l:I

    move-wide/from16 v0, v22

    move-object/from16 v2, v27

    iput-wide v0, v2, Lcom/google/android/gms/location/places/internal/g;->m:J

    sget-object v6, Lcom/google/android/location/x;->e:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v6}, Lcom/google/android/gms/common/a/d;->c()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Boolean;

    invoke-virtual {v6}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v6

    move-object/from16 v0, v27

    iput-boolean v6, v0, Lcom/google/android/gms/location/places/internal/g;->s:Z

    new-instance v6, Lcom/google/android/gms/location/places/internal/PlaceImpl;

    move-object/from16 v0, v27

    iget v7, v0, Lcom/google/android/gms/location/places/internal/g;->a:I

    move-object/from16 v0, v27

    iget-object v8, v0, Lcom/google/android/gms/location/places/internal/g;->b:Ljava/lang/String;

    move-object/from16 v0, v27

    iget-object v9, v0, Lcom/google/android/gms/location/places/internal/g;->n:Ljava/util/List;

    move-object/from16 v0, v27

    iget-object v10, v0, Lcom/google/android/gms/location/places/internal/g;->c:Landroid/os/Bundle;

    move-object/from16 v0, v27

    iget-object v11, v0, Lcom/google/android/gms/location/places/internal/g;->d:Ljava/lang/String;

    move-object/from16 v0, v27

    iget-object v12, v0, Lcom/google/android/gms/location/places/internal/g;->o:Ljava/lang/String;

    move-object/from16 v0, v27

    iget-object v13, v0, Lcom/google/android/gms/location/places/internal/g;->p:Ljava/lang/String;

    move-object/from16 v0, v27

    iget-object v14, v0, Lcom/google/android/gms/location/places/internal/g;->q:Ljava/lang/String;

    move-object/from16 v0, v27

    iget-object v15, v0, Lcom/google/android/gms/location/places/internal/g;->r:Ljava/util/List;

    move-object/from16 v0, v27

    iget-object v0, v0, Lcom/google/android/gms/location/places/internal/g;->e:Lcom/google/android/gms/maps/model/LatLng;

    move-object/from16 v16, v0

    move-object/from16 v0, v27

    iget v0, v0, Lcom/google/android/gms/location/places/internal/g;->f:F

    move/from16 v17, v0

    move-object/from16 v0, v27

    iget-object v0, v0, Lcom/google/android/gms/location/places/internal/g;->g:Lcom/google/android/gms/maps/model/LatLngBounds;

    move-object/from16 v18, v0

    move-object/from16 v0, v27

    iget-object v0, v0, Lcom/google/android/gms/location/places/internal/g;->h:Ljava/lang/String;

    move-object/from16 v19, v0

    move-object/from16 v0, v27

    iget-object v0, v0, Lcom/google/android/gms/location/places/internal/g;->i:Landroid/net/Uri;

    move-object/from16 v20, v0

    move-object/from16 v0, v27

    iget-boolean v0, v0, Lcom/google/android/gms/location/places/internal/g;->j:Z

    move/from16 v21, v0

    move-object/from16 v0, v27

    iget v0, v0, Lcom/google/android/gms/location/places/internal/g;->k:F

    move/from16 v22, v0

    move-object/from16 v0, v27

    iget v0, v0, Lcom/google/android/gms/location/places/internal/g;->l:I

    move/from16 v23, v0

    move-object/from16 v0, v27

    iget-wide v0, v0, Lcom/google/android/gms/location/places/internal/g;->m:J

    move-wide/from16 v24, v0

    move-object/from16 v0, v27

    iget-boolean v0, v0, Lcom/google/android/gms/location/places/internal/g;->s:Z

    move/from16 v26, v0

    move-object/from16 v0, v27

    iget-object v0, v0, Lcom/google/android/gms/location/places/internal/g;->d:Ljava/lang/String;

    move-object/from16 v39, v0

    move-object/from16 v0, v27

    iget-object v0, v0, Lcom/google/android/gms/location/places/internal/g;->o:Ljava/lang/String;

    move-object/from16 v40, v0

    move-object/from16 v0, v27

    iget-object v0, v0, Lcom/google/android/gms/location/places/internal/g;->p:Ljava/lang/String;

    move-object/from16 v41, v0

    move-object/from16 v0, v27

    iget-object v0, v0, Lcom/google/android/gms/location/places/internal/g;->q:Ljava/lang/String;

    move-object/from16 v42, v0

    move-object/from16 v0, v27

    iget-object v0, v0, Lcom/google/android/gms/location/places/internal/g;->r:Ljava/util/List;

    move-object/from16 v27, v0

    move-object/from16 v0, v39

    move-object/from16 v1, v40

    move-object/from16 v2, v41

    move-object/from16 v3, v42

    move-object/from16 v4, v27

    invoke-static {v0, v1, v2, v3, v4}, Lcom/google/android/gms/location/places/internal/PlaceLocalization;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;)Lcom/google/android/gms/location/places/internal/PlaceLocalization;

    move-result-object v27

    invoke-direct/range {v6 .. v27}, Lcom/google/android/gms/location/places/internal/PlaceImpl;-><init>(ILjava/lang/String;Ljava/util/List;Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Lcom/google/android/gms/maps/model/LatLng;FLcom/google/android/gms/maps/model/LatLngBounds;Ljava/lang/String;Landroid/net/Uri;ZFIJZLcom/google/android/gms/location/places/internal/PlaceLocalization;)V

    new-instance v7, Ljava/util/Locale;

    move-object/from16 v0, v28

    invoke-direct {v7, v0}, Ljava/util/Locale;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v7}, Lcom/google/android/gms/location/places/internal/PlaceImpl;->a(Ljava/util/Locale;)V

    goto/16 :goto_3

    :cond_1a
    move-object/from16 v6, v29

    goto/16 :goto_1

    .line 486
    :cond_1b
    move-object/from16 v0, p1

    iget-wide v8, v0, Lcom/google/android/location/places/k;->f:J

    invoke-static/range {v34 .. v34}, Lcom/google/android/location/places/f;->a(Lcom/google/android/location/places/o;)V

    .line 488
    move-object/from16 v0, v36

    iget-object v7, v0, Lcom/google/android/location/l/a/bk;->b:[Lcom/google/android/location/l/a/bj;

    const/4 v8, 0x0

    aget-object v7, v7, v8

    .line 490
    sget-object v8, Lcom/google/android/location/places/g;->a:[I

    invoke-virtual/range {v34 .. v34}, Lcom/google/android/location/places/o;->ordinal()I

    move-result v9

    aget v8, v8, v9

    packed-switch v8, :pswitch_data_0

    .line 505
    new-instance v6, Ljava/lang/RuntimeException;

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "Unsupported requestType: "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v34

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v6

    .line 496
    :pswitch_0
    new-instance v8, Ljava/util/Locale;

    move-object/from16 v0, p1

    iget-object v9, v0, Lcom/google/android/location/places/k;->a:Lcom/google/android/gms/location/places/internal/PlacesParams;

    iget-object v9, v9, Lcom/google/android/gms/location/places/internal/PlacesParams;->d:Ljava/lang/String;

    invoke-direct {v8, v9}, Ljava/util/Locale;-><init>(Ljava/lang/String;)V

    invoke-static {v7, v6, v8}, Lcom/google/android/location/places/f;->a(Lcom/google/android/location/l/a/bj;[Lcom/google/android/gms/location/places/internal/PlaceImpl;Ljava/util/Locale;)Ljava/util/List;

    move-result-object v6

    .line 507
    if-eqz v6, :cond_1c

    .line 508
    const/4 v7, 0x0

    move-object/from16 v0, v35

    invoke-static {v7, v6, v0}, Lcom/google/android/location/places/f;->a(ILjava/util/List;Lcom/google/android/location/places/aj;)V

    goto/16 :goto_0

    .line 510
    :cond_1c
    invoke-static/range {v35 .. v35}, Lcom/google/android/location/places/f;->a(Lcom/google/android/location/places/aj;)V

    goto/16 :goto_0

    :cond_1d
    move-object/from16 v28, v18

    goto/16 :goto_5

    .line 490
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final a(Ljava/lang/String;Lcom/google/android/gms/location/places/internal/PlacesParams;Lcom/google/android/location/places/aj;)V
    .locals 1

    .prologue
    .line 216
    invoke-static {p1}, Ljava/util/Collections;->singleton(Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v0

    invoke-virtual {p0, v0, p2, p3}, Lcom/google/android/location/places/f;->a(Ljava/util/Collection;Lcom/google/android/gms/location/places/internal/PlacesParams;Lcom/google/android/location/places/aj;)V

    .line 217
    return-void
.end method

.method public final a(Ljava/util/Collection;Lcom/google/android/gms/location/places/internal/PlacesParams;Lcom/google/android/location/places/aj;)V
    .locals 8

    .prologue
    const/4 v7, 0x0

    .line 230
    iget-object v0, p0, Lcom/google/android/location/places/f;->c:Lcom/google/android/location/n/ai;

    invoke-virtual {v0}, Lcom/google/android/location/n/ai;->a()J

    move-result-wide v2

    .line 231
    new-instance v1, Ljava/util/ArrayList;

    invoke-interface {p1}, Ljava/util/Collection;->size()I

    move-result v0

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 232
    new-instance v4, Ljava/util/ArrayList;

    invoke-interface {p1}, Ljava/util/Collection;->size()I

    move-result v0

    invoke-direct {v4, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 233
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 234
    iget-object v6, p0, Lcom/google/android/location/places/f;->e:Lcom/google/android/location/places/t;

    invoke-virtual {v6, v0, v2, v3}, Lcom/google/android/location/places/t;->a(Ljava/lang/String;J)Lcom/google/android/gms/location/places/internal/PlaceImpl;

    move-result-object v6

    .line 235
    if-nez v6, :cond_0

    .line 236
    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 238
    :cond_0
    const/high16 v0, 0x3f800000    # 1.0f

    invoke-static {v6, v0}, Lcom/google/android/gms/location/places/PlaceLikelihood;->a(Lcom/google/android/gms/location/places/internal/PlaceImpl;F)Lcom/google/android/gms/location/places/PlaceLikelihood;

    move-result-object v0

    .line 239
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 242
    :cond_1
    invoke-interface {v4}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 243
    invoke-interface {p3, v7, v1}, Lcom/google/android/location/places/aj;->a(ILjava/util/List;)V

    .line 253
    :goto_1
    return-void

    .line 245
    :cond_2
    iget-object v0, p0, Lcom/google/android/location/places/f;->d:Landroid/content/Context;

    const/4 v2, 0x0

    invoke-static {v0, v2, p2}, Lcom/google/android/location/places/bo;->a(Landroid/content/Context;Lcom/google/android/gms/location/places/PlaceFilter;Lcom/google/android/gms/location/places/internal/PlacesParams;)Lcom/google/android/location/l/a/bi;

    move-result-object v2

    new-instance v3, Lcom/google/android/location/l/a/bb;

    invoke-direct {v3}, Lcom/google/android/location/l/a/bb;-><init>()V

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v0

    new-array v0, v0, [Ljava/lang/String;

    invoke-interface {v4, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    iput-object v0, v3, Lcom/google/android/location/l/a/bb;->a:[Ljava/lang/String;

    iput-object v3, v2, Lcom/google/android/location/l/a/bi;->f:Lcom/google/android/location/l/a/bb;

    .line 247
    invoke-static {p2, v2}, Lcom/google/android/location/places/bo;->a(Lcom/google/android/gms/location/places/internal/PlacesParams;Lcom/google/android/location/l/a/bi;)Lcom/google/android/location/l/a/ak;

    move-result-object v0

    .line 248
    new-instance v2, Lcom/google/android/location/places/n;

    invoke-direct {v2, p3, v1, v7}, Lcom/google/android/location/places/n;-><init>(Lcom/google/android/location/places/aj;Ljava/util/List;B)V

    .line 250
    new-instance v1, Lcom/google/android/location/places/k;

    sget-object v3, Lcom/google/android/location/places/o;->e:Lcom/google/android/location/places/o;

    invoke-direct {v1, p0, p2, v2, v3}, Lcom/google/android/location/places/k;-><init>(Lcom/google/android/location/places/f;Lcom/google/android/gms/location/places/internal/PlacesParams;Lcom/google/android/location/places/aj;Lcom/google/android/location/places/o;)V

    invoke-virtual {p0, v0, v1}, Lcom/google/android/location/places/f;->a(Lcom/google/android/location/l/a/ak;Lcom/google/v/b/n;)V

    goto :goto_1
.end method

.method public final a(Ljava/util/List;Lcom/google/v/b/n;J)V
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v2, 0x0

    .line 283
    new-instance v3, Lcom/google/android/location/l/a/al;

    invoke-direct {v3}, Lcom/google/android/location/l/a/al;-><init>()V

    new-instance v0, Lcom/google/android/location/l/a/cg;

    invoke-direct {v0}, Lcom/google/android/location/l/a/cg;-><init>()V

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, v0, Lcom/google/android/location/l/a/cg;->a:Ljava/lang/Integer;

    new-array v1, v6, [Lcom/google/android/location/l/a/cg;

    aput-object v0, v1, v2

    iput-object v1, v3, Lcom/google/android/location/l/a/al;->k:[Lcom/google/android/location/l/a/cg;

    new-instance v0, Lcom/google/android/location/l/a/cu;

    invoke-direct {v0}, Lcom/google/android/location/l/a/cu;-><init>()V

    iput-object v0, v3, Lcom/google/android/location/l/a/al;->b:Lcom/google/android/location/l/a/cu;

    iget-object v0, v3, Lcom/google/android/location/l/a/al;->b:Lcom/google/android/location/l/a/cu;

    invoke-static {p3, p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    iput-object v1, v0, Lcom/google/android/location/l/a/cu;->a:Ljava/lang/Long;

    iget-object v0, v3, Lcom/google/android/location/l/a/al;->b:Lcom/google/android/location/l/a/cu;

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v1

    new-array v1, v1, [Lcom/google/android/location/l/a/cs;

    iput-object v1, v0, Lcom/google/android/location/l/a/cu;->b:[Lcom/google/android/location/l/a/cs;

    move v1, v2

    :goto_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    new-instance v0, Lcom/google/android/location/l/a/cs;

    invoke-direct {v0}, Lcom/google/android/location/l/a/cs;-><init>()V

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    iput-object v4, v0, Lcom/google/android/location/l/a/cs;->h:Ljava/lang/Long;

    const-string v4, ""

    iput-object v4, v0, Lcom/google/android/location/l/a/cs;->a:Ljava/lang/String;

    iget-object v4, v3, Lcom/google/android/location/l/a/al;->b:Lcom/google/android/location/l/a/cu;

    iget-object v4, v4, Lcom/google/android/location/l/a/cu;->b:[Lcom/google/android/location/l/a/cs;

    aput-object v0, v4, v1

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_0
    new-instance v0, Lcom/google/android/location/l/a/ak;

    invoke-direct {v0}, Lcom/google/android/location/l/a/ak;-><init>()V

    new-array v1, v6, [Lcom/google/android/location/l/a/al;

    aput-object v3, v1, v2

    iput-object v1, v0, Lcom/google/android/location/l/a/ak;->d:[Lcom/google/android/location/l/a/al;

    .line 284
    const-string v1, "Places"

    const/4 v2, 0x2

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 285
    const-string v1, "Places"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "getting mac-cluster mapping from server. Request: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/location/n/aa;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 287
    :cond_1
    invoke-virtual {p0, v0, p2}, Lcom/google/android/location/places/f;->a(Lcom/google/android/location/l/a/ak;Lcom/google/v/b/n;)V

    .line 288
    return-void
.end method
