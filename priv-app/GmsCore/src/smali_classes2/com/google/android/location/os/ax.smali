.class public final Lcom/google/android/location/os/ax;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/location/os/bi;


# instance fields
.field protected a:Lcom/google/android/location/j/f;

.field protected b:Lcom/google/android/location/j/i;

.field protected c:Lcom/google/android/location/j/b;

.field protected d:Lcom/google/android/location/j/d;

.field protected e:Lcom/google/android/location/j/e;

.field protected f:Lcom/google/android/location/j/j;

.field protected g:Lcom/google/android/location/os/bn;

.field protected h:Lcom/google/android/location/os/bj;

.field protected i:Lcom/google/android/location/os/j;

.field protected j:Lcom/google/android/location/activity/at;

.field protected k:Lcom/google/android/location/activity/bn;

.field private final m:Lcom/google/android/location/os/bh;


# direct methods
.method public constructor <init>()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 44
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 46
    new-instance v0, Lcom/google/android/location/os/bh;

    invoke-direct {v0}, Lcom/google/android/location/os/bh;-><init>()V

    iput-object v0, p0, Lcom/google/android/location/os/ax;->m:Lcom/google/android/location/os/bh;

    .line 48
    new-instance v0, Lcom/google/android/location/j/h;

    invoke-direct {v0}, Lcom/google/android/location/j/h;-><init>()V

    iput-object v0, p0, Lcom/google/android/location/os/ax;->a:Lcom/google/android/location/j/f;

    .line 50
    new-instance v0, Lcom/google/android/location/j/i;

    invoke-direct {v0}, Lcom/google/android/location/j/i;-><init>()V

    iput-object v0, p0, Lcom/google/android/location/os/ax;->b:Lcom/google/android/location/j/i;

    .line 52
    new-instance v0, Lcom/google/android/location/os/ba;

    invoke-direct {v0}, Lcom/google/android/location/os/ba;-><init>()V

    iput-object v0, p0, Lcom/google/android/location/os/ax;->c:Lcom/google/android/location/j/b;

    .line 76
    new-instance v0, Lcom/google/android/location/os/bb;

    invoke-direct {v0}, Lcom/google/android/location/os/bb;-><init>()V

    iput-object v0, p0, Lcom/google/android/location/os/ax;->d:Lcom/google/android/location/j/d;

    .line 97
    new-instance v0, Lcom/google/android/location/os/bc;

    invoke-direct {v0, p0}, Lcom/google/android/location/os/bc;-><init>(Lcom/google/android/location/os/ax;)V

    iput-object v0, p0, Lcom/google/android/location/os/ax;->e:Lcom/google/android/location/j/e;

    .line 134
    new-instance v0, Lcom/google/android/location/os/bf;

    invoke-direct {v0}, Lcom/google/android/location/os/bf;-><init>()V

    iput-object v0, p0, Lcom/google/android/location/os/ax;->f:Lcom/google/android/location/j/j;

    .line 169
    new-instance v0, Lcom/google/android/location/os/bg;

    invoke-direct {v0}, Lcom/google/android/location/os/bg;-><init>()V

    iput-object v0, p0, Lcom/google/android/location/os/ax;->g:Lcom/google/android/location/os/bn;

    .line 263
    new-instance v0, Lcom/google/android/location/os/bj;

    const-string v1, "null"

    const-string v2, "null"

    const-string v3, "null/null/null"

    const/4 v4, 0x0

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/location/os/bj;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    iput-object v0, p0, Lcom/google/android/location/os/ax;->h:Lcom/google/android/location/os/bj;

    .line 301
    new-instance v0, Lcom/google/android/location/os/j;

    new-instance v1, Lcom/google/android/location/os/ay;

    invoke-direct {v1, p0}, Lcom/google/android/location/os/ay;-><init>(Lcom/google/android/location/os/ax;)V

    invoke-direct {v0, v1, v5, v5, v5}, Lcom/google/android/location/os/j;-><init>(Lcom/google/android/location/os/at;Lcom/google/android/location/o/a/b;Ljava/io/PrintWriter;Lcom/google/android/location/os/b;)V

    iput-object v0, p0, Lcom/google/android/location/os/ax;->i:Lcom/google/android/location/os/j;

    .line 308
    new-instance v0, Lcom/google/android/location/activity/ak;

    invoke-direct {v0}, Lcom/google/android/location/activity/ak;-><init>()V

    iput-object v0, p0, Lcom/google/android/location/os/ax;->j:Lcom/google/android/location/activity/at;

    .line 311
    new-instance v0, Lcom/google/android/location/activity/al;

    invoke-direct {v0}, Lcom/google/android/location/activity/al;-><init>()V

    iput-object v0, p0, Lcom/google/android/location/os/ax;->k:Lcom/google/android/location/activity/bn;

    return-void
.end method


# virtual methods
.method public final A()Lcom/google/android/location/os/bm;
    .locals 1

    .prologue
    .line 538
    iget-object v0, p0, Lcom/google/android/location/os/ax;->m:Lcom/google/android/location/os/bh;

    return-object v0
.end method

.method public final B()Lcom/google/android/location/j/f;
    .locals 1

    .prologue
    .line 345
    iget-object v0, p0, Lcom/google/android/location/os/ax;->a:Lcom/google/android/location/j/f;

    return-object v0
.end method

.method public final a(Ljava/lang/String;Lcom/google/android/location/collectionlib/ab;Ljava/lang/String;Lcom/google/android/location/o/n;)Lcom/google/android/location/collectionlib/bd;
    .locals 1

    .prologue
    .line 451
    new-instance v0, Lcom/google/android/location/os/az;

    invoke-direct {v0}, Lcom/google/android/location/os/az;-><init>()V

    return-object v0
.end method

.method public final a(Ljava/util/Set;Ljava/util/Map;JLjava/lang/String;Ljava/lang/Integer;ZLcom/google/p/a/b/b/a;ZLcom/google/android/location/collectionlib/ar;Ljava/lang/String;Lcom/google/android/location/o/n;)Lcom/google/android/location/collectionlib/be;
    .locals 1

    .prologue
    .line 406
    new-instance v0, Lcom/google/android/location/os/be;

    invoke-direct {v0}, Lcom/google/android/location/os/be;-><init>()V

    return-object v0
.end method

.method public final a(ZLjava/util/Set;Ljava/util/Map;JLcom/google/android/location/collectionlib/SensorScannerConfig;Lcom/google/android/location/collectionlib/ar;Ljava/lang/String;Lcom/google/android/location/o/n;)Lcom/google/android/location/collectionlib/be;
    .locals 1

    .prologue
    .line 397
    new-instance v0, Lcom/google/android/location/os/be;

    invoke-direct {v0}, Lcom/google/android/location/os/be;-><init>()V

    return-object v0
.end method

.method public final a(Ljava/lang/String;)Ljava/io/InputStream;
    .locals 2

    .prologue
    .line 374
    new-instance v0, Ljava/io/ByteArrayInputStream;

    const/4 v1, 0x0

    new-array v1, v1, [B

    invoke-direct {v0, v1}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    return-object v0
.end method

.method public final a(II)V
    .locals 0

    .prologue
    .line 365
    return-void
.end method

.method public final a(J)V
    .locals 0

    .prologue
    .line 321
    return-void
.end method

.method public final a(Lcom/google/android/gms/location/ActivityRecognitionResult;)V
    .locals 0

    .prologue
    .line 468
    return-void
.end method

.method public final a(Lcom/google/android/location/activity/bd;)V
    .locals 0

    .prologue
    .line 465
    return-void
.end method

.method public final a(Lcom/google/android/location/e/ag;Lcom/google/android/location/e/ay;)V
    .locals 0

    .prologue
    .line 362
    return-void
.end method

.method public final a(Lcom/google/android/location/e/bh;)V
    .locals 0

    .prologue
    .line 531
    return-void
.end method

.method public final a(Lcom/google/android/location/j/k;Z)V
    .locals 0

    .prologue
    .line 354
    return-void
.end method

.method public final a(Ljava/lang/String;Lcom/google/android/location/collectionlib/cg;)V
    .locals 0

    .prologue
    .line 431
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JZ)V
    .locals 0

    .prologue
    .line 318
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/util/Map;)V
    .locals 0

    .prologue
    .line 514
    return-void
.end method

.method public final a(I)Z
    .locals 1

    .prologue
    .line 421
    const/4 v0, 0x0

    return v0
.end method

.method public final a(Lcom/google/android/location/collectionlib/cg;)Z
    .locals 1

    .prologue
    .line 416
    const/4 v0, 0x0

    return v0
.end method

.method public final a(Lcom/google/android/location/os/bk;Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 487
    const/4 v0, 0x0

    return v0
.end method

.method public final a(Ljava/lang/String;Lcom/google/android/location/collectionlib/cg;ILjava/lang/String;)Z
    .locals 1

    .prologue
    .line 427
    const/4 v0, 0x0

    return v0
.end method

.method public final b()V
    .locals 0

    .prologue
    .line 314
    return-void
.end method

.method public final b(J)V
    .locals 0

    .prologue
    .line 543
    return-void
.end method

.method public final b(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 511
    return-void
.end method

.method public final c()Lcom/google/android/location/j/b;
    .locals 1

    .prologue
    .line 325
    iget-object v0, p0, Lcom/google/android/location/os/ax;->c:Lcom/google/android/location/j/b;

    return-object v0
.end method

.method public final d()Lcom/google/android/location/j/d;
    .locals 1

    .prologue
    .line 330
    iget-object v0, p0, Lcom/google/android/location/os/ax;->d:Lcom/google/android/location/j/d;

    return-object v0
.end method

.method public final e()Lcom/google/android/location/j/e;
    .locals 1

    .prologue
    .line 335
    iget-object v0, p0, Lcom/google/android/location/os/ax;->e:Lcom/google/android/location/j/e;

    return-object v0
.end method

.method public final f()Lcom/google/android/location/j/j;
    .locals 1

    .prologue
    .line 340
    iget-object v0, p0, Lcom/google/android/location/os/ax;->f:Lcom/google/android/location/j/j;

    return-object v0
.end method

.method public final g()Lcom/google/android/location/os/bn;
    .locals 1

    .prologue
    .line 350
    iget-object v0, p0, Lcom/google/android/location/os/ax;->g:Lcom/google/android/location/os/bn;

    return-object v0
.end method

.method public final h()Z
    .locals 1

    .prologue
    .line 358
    const/4 v0, 0x0

    return v0
.end method

.method public final i()Lcom/google/android/location/os/aw;
    .locals 1

    .prologue
    .line 369
    new-instance v0, Lcom/google/android/location/os/bd;

    invoke-direct {v0}, Lcom/google/android/location/os/bd;-><init>()V

    return-object v0
.end method

.method public final j()Lcom/google/android/location/os/bj;
    .locals 1

    .prologue
    .line 379
    iget-object v0, p0, Lcom/google/android/location/os/ax;->h:Lcom/google/android/location/os/bj;

    return-object v0
.end method

.method public final k()Ljava/lang/String;
    .locals 1

    .prologue
    .line 384
    const-string v0, "us"

    return-object v0
.end method

.method public final l()Ljava/lang/String;
    .locals 1

    .prologue
    .line 389
    const-string v0, "us"

    return-object v0
.end method

.method public final m()Z
    .locals 1

    .prologue
    .line 411
    const/4 v0, 0x1

    return v0
.end method

.method public final n()Ljava/io/File;
    .locals 2

    .prologue
    .line 435
    new-instance v0, Ljava/io/File;

    const-string v1, ""

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public final o()Ljava/io/File;
    .locals 2

    .prologue
    .line 440
    new-instance v0, Ljava/io/File;

    const-string v1, ""

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public final p()Ljava/io/File;
    .locals 2

    .prologue
    .line 445
    new-instance v0, Ljava/io/File;

    const-string v1, ""

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public final q()I
    .locals 1

    .prologue
    .line 456
    const/4 v0, 0x0

    return v0
.end method

.method public final r()I
    .locals 1

    .prologue
    .line 461
    const/4 v0, 0x0

    return v0
.end method

.method public final s()Z
    .locals 1

    .prologue
    .line 472
    const/4 v0, 0x1

    return v0
.end method

.method public final t()J
    .locals 2

    .prologue
    .line 482
    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public final u()Z
    .locals 1

    .prologue
    .line 492
    const/4 v0, 0x1

    return v0
.end method

.method public final v()Lcom/google/android/location/os/j;
    .locals 1

    .prologue
    .line 497
    iget-object v0, p0, Lcom/google/android/location/os/ax;->i:Lcom/google/android/location/os/j;

    return-object v0
.end method

.method public final w()Lcom/google/android/location/activity/at;
    .locals 1

    .prologue
    .line 502
    iget-object v0, p0, Lcom/google/android/location/os/ax;->j:Lcom/google/android/location/activity/at;

    return-object v0
.end method

.method public final x()Lcom/google/android/location/activity/bn;
    .locals 1

    .prologue
    .line 507
    iget-object v0, p0, Lcom/google/android/location/os/ax;->k:Lcom/google/android/location/activity/bn;

    return-object v0
.end method

.method public final y()Z
    .locals 1

    .prologue
    .line 527
    const/4 v0, 0x0

    return v0
.end method

.method public final z()V
    .locals 0

    .prologue
    .line 534
    return-void
.end method
