.class final Lcom/google/android/location/copresence/d/v;
.super Lcom/google/android/location/copresence/k/f;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/location/copresence/d/n;


# direct methods
.method varargs constructor <init>(Lcom/google/android/location/copresence/d/n;Ljava/lang/String;[Lcom/google/android/location/copresence/k/f;)V
    .locals 0

    .prologue
    .line 142
    iput-object p1, p0, Lcom/google/android/location/copresence/d/v;->a:Lcom/google/android/location/copresence/d/n;

    invoke-direct {p0, p2, p3}, Lcom/google/android/location/copresence/k/f;-><init>(Ljava/lang/String;[Lcom/google/android/location/copresence/k/f;)V

    return-void
.end method


# virtual methods
.method public final a()Z
    .locals 4

    .prologue
    const/4 v2, 0x6

    const/4 v0, 0x0

    .line 157
    iget-object v1, p0, Lcom/google/android/location/copresence/d/v;->a:Lcom/google/android/location/copresence/d/n;

    iget-object v1, v1, Lcom/google/android/location/copresence/d/n;->d:Lcom/google/android/location/copresence/k/f;

    invoke-virtual {v1}, Lcom/google/android/location/copresence/k/f;->b()Z

    move-result v1

    if-nez v1, :cond_1

    .line 158
    invoke-static {v2}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 159
    const-string v1, "BluetoothStates: Cannot save user prefs if enabled state not saved."

    invoke-static {v1}, Lcom/google/android/location/copresence/ag;->e(Ljava/lang/String;)I

    .line 183
    :cond_0
    :goto_0
    return v0

    .line 163
    :cond_1
    iget-object v1, p0, Lcom/google/android/location/copresence/d/v;->a:Lcom/google/android/location/copresence/d/n;

    iget-object v1, v1, Lcom/google/android/location/copresence/d/n;->f:Lcom/google/android/location/copresence/k/f;

    invoke-virtual {v1}, Lcom/google/android/location/copresence/k/f;->b()Z

    move-result v1

    if-nez v1, :cond_2

    .line 164
    invoke-static {v2}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 165
    const-string v1, "BluetoothStates: Cannot save user prefs if bluetooth not enabled."

    invoke-static {v1}, Lcom/google/android/location/copresence/ag;->e(Ljava/lang/String;)I

    goto :goto_0

    .line 170
    :cond_2
    iget-object v0, p0, Lcom/google/android/location/copresence/d/v;->a:Lcom/google/android/location/copresence/d/n;

    iget-object v0, v0, Lcom/google/android/location/copresence/d/n;->b:Lcom/google/android/location/copresence/d/e;

    invoke-virtual {v0}, Lcom/google/android/location/copresence/d/e;->c()I

    move-result v1

    .line 171
    iget-object v0, p0, Lcom/google/android/location/copresence/d/v;->a:Lcom/google/android/location/copresence/d/n;

    iget-object v0, v0, Lcom/google/android/location/copresence/d/n;->b:Lcom/google/android/location/copresence/d/e;

    iget-object v0, v0, Lcom/google/android/location/copresence/d/e;->a:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothAdapter;->getScanMode()I

    move-result v0

    .line 178
    const/16 v2, 0x17

    if-ne v0, v2, :cond_3

    if-eqz v1, :cond_3

    .line 180
    const/16 v0, 0x15

    .line 183
    :cond_3
    iget-object v2, p0, Lcom/google/android/location/copresence/d/v;->a:Lcom/google/android/location/copresence/d/n;

    iget-object v2, p0, Lcom/google/android/location/copresence/d/v;->a:Lcom/google/android/location/copresence/d/n;

    iget-object v2, v2, Lcom/google/android/location/copresence/d/n;->c:Landroid/content/SharedPreferences;

    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    const-string v3, "discoverableTimeout"

    invoke-interface {v2, v3, v1}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v2, "scanMode"

    invoke-interface {v1, v2, v0}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "deviceName"

    iget-object v2, p0, Lcom/google/android/location/copresence/d/v;->a:Lcom/google/android/location/copresence/d/n;

    iget-object v2, v2, Lcom/google/android/location/copresence/d/n;->b:Lcom/google/android/location/copresence/d/e;

    iget-object v2, v2, Lcom/google/android/location/copresence/d/e;->a:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v2}, Landroid/bluetooth/BluetoothAdapter;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "save name and discoverable state"

    invoke-static {v0, v1}, Lcom/google/android/location/copresence/d/n;->a(Landroid/content/SharedPreferences$Editor;Ljava/lang/String;)Z

    move-result v0

    goto :goto_0
.end method

.method public final b()Z
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    const/4 v2, -0x1

    .line 148
    iget-object v0, p0, Lcom/google/android/location/copresence/d/v;->a:Lcom/google/android/location/copresence/d/n;

    iget-object v0, v0, Lcom/google/android/location/copresence/d/n;->c:Landroid/content/SharedPreferences;

    const-string v1, "bluetoothEnabled"

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/location/copresence/d/v;->a:Lcom/google/android/location/copresence/d/n;

    iget-object v0, v0, Lcom/google/android/location/copresence/d/n;->c:Landroid/content/SharedPreferences;

    const-string v1, "discoverableTimeout"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    if-eq v0, v2, :cond_0

    iget-object v0, p0, Lcom/google/android/location/copresence/d/v;->a:Lcom/google/android/location/copresence/d/n;

    iget-object v0, v0, Lcom/google/android/location/copresence/d/n;->c:Landroid/content/SharedPreferences;

    const-string v1, "scanMode"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    if-eq v0, v2, :cond_0

    iget-object v0, p0, Lcom/google/android/location/copresence/d/v;->a:Lcom/google/android/location/copresence/d/n;

    iget-object v0, v0, Lcom/google/android/location/copresence/d/n;->c:Landroid/content/SharedPreferences;

    const-string v1, "deviceName"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/location/copresence/d/v;->a:Lcom/google/android/location/copresence/d/n;

    iget-object v0, v0, Lcom/google/android/location/copresence/d/n;->c:Landroid/content/SharedPreferences;

    const-string v1, "updated"

    invoke-interface {v0, v1, v4, v5}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    cmp-long v0, v0, v4

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
