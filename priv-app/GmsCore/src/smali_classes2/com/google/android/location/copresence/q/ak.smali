.class public final Lcom/google/android/location/copresence/q/ak;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public final a:Ljava/lang/StringBuilder;


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 53
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 54
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iput-object v0, p0, Lcom/google/android/location/copresence/q/ak;->a:Ljava/lang/StringBuilder;

    .line 55
    return-void
.end method

.method synthetic constructor <init>(B)V
    .locals 0

    .prologue
    .line 50
    invoke-direct {p0}, Lcom/google/android/location/copresence/q/ak;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Lcom/google/android/location/copresence/q/ak;
    .locals 2

    .prologue
    .line 96
    iget-object v0, p0, Lcom/google/android/location/copresence/q/ak;->a:Ljava/lang/StringBuilder;

    const-string v1, " AND"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 97
    return-object p0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/android/location/copresence/q/ak;
    .locals 2

    .prologue
    .line 105
    iget-object v0, p0, Lcom/google/android/location/copresence/q/ak;->a:Ljava/lang/StringBuilder;

    const/16 v1, 0x20

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x3d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 106
    iget-object v0, p0, Lcom/google/android/location/copresence/q/ak;->a:Ljava/lang/StringBuilder;

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 110
    return-object p0
.end method

.method public final a(Ljava/lang/String;Ljava/util/List;Z)Lcom/google/android/location/copresence/q/ak;
    .locals 2

    .prologue
    .line 62
    invoke-interface {p2}, Ljava/util/List;->toArray()[Ljava/lang/Object;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {p0, p1, v0, v1}, Lcom/google/android/location/copresence/q/ak;->a(Ljava/lang/String;[Ljava/lang/Object;Z)Lcom/google/android/location/copresence/q/ak;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;[Ljava/lang/Object;Z)Lcom/google/android/location/copresence/q/ak;
    .locals 8

    .prologue
    const/16 v7, 0x22

    const/4 v1, 0x0

    .line 67
    iget-object v0, p0, Lcom/google/android/location/copresence/q/ak;->a:Ljava/lang/StringBuilder;

    const/16 v2, 0x20

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 68
    iget-object v0, p0, Lcom/google/android/location/copresence/q/ak;->a:Ljava/lang/StringBuilder;

    const-string v2, " IN ("

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 69
    const/4 v0, 0x1

    .line 70
    array-length v3, p2

    move v2, v1

    :goto_0
    if-ge v2, v3, :cond_3

    aget-object v4, p2, v2

    .line 71
    if-eqz v0, :cond_2

    move v0, v1

    .line 76
    :goto_1
    if-eqz p3, :cond_0

    .line 77
    iget-object v5, p0, Lcom/google/android/location/copresence/q/ak;->a:Ljava/lang/StringBuilder;

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 79
    :cond_0
    iget-object v5, p0, Lcom/google/android/location/copresence/q/ak;->a:Ljava/lang/StringBuilder;

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 80
    if-eqz p3, :cond_1

    .line 81
    iget-object v4, p0, Lcom/google/android/location/copresence/q/ak;->a:Ljava/lang/StringBuilder;

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 70
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 74
    :cond_2
    iget-object v5, p0, Lcom/google/android/location/copresence/q/ak;->a:Ljava/lang/StringBuilder;

    const/16 v6, 0x2c

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 84
    :cond_3
    iget-object v0, p0, Lcom/google/android/location/copresence/q/ak;->a:Ljava/lang/StringBuilder;

    const/16 v1, 0x29

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 85
    return-object p0
.end method
