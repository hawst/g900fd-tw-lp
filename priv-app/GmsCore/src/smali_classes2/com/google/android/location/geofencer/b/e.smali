.class public final Lcom/google/android/location/geofencer/b/e;
.super Lcom/google/protobuf/a/f;
.source "SourceFile"


# instance fields
.field public a:Z

.field public b:Lcom/google/android/location/geofencer/b/d;

.field public c:Z

.field public d:Ljava/lang/String;

.field public e:Z

.field public f:I

.field public g:Z

.field public h:I

.field public i:Z

.field public j:J

.field public k:Z

.field public l:Z

.field public m:Z

.field private n:Z

.field private o:I


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 695
    invoke-direct {p0}, Lcom/google/protobuf/a/f;-><init>()V

    .line 700
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/location/geofencer/b/e;->b:Lcom/google/android/location/geofencer/b/d;

    .line 720
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/location/geofencer/b/e;->d:Ljava/lang/String;

    .line 737
    iput v2, p0, Lcom/google/android/location/geofencer/b/e;->f:I

    .line 754
    iput v2, p0, Lcom/google/android/location/geofencer/b/e;->h:I

    .line 771
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/location/geofencer/b/e;->j:J

    .line 788
    iput-boolean v2, p0, Lcom/google/android/location/geofencer/b/e;->l:Z

    .line 805
    iput-boolean v2, p0, Lcom/google/android/location/geofencer/b/e;->m:Z

    .line 861
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/location/geofencer/b/e;->o:I

    .line 695
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 864
    iget v0, p0, Lcom/google/android/location/geofencer/b/e;->o:I

    if-gez v0, :cond_0

    .line 866
    invoke-virtual {p0}, Lcom/google/android/location/geofencer/b/e;->b()I

    .line 868
    :cond_0
    iget v0, p0, Lcom/google/android/location/geofencer/b/e;->o:I

    return v0
.end method

.method public final a(I)Lcom/google/android/location/geofencer/b/e;
    .locals 1

    .prologue
    .line 741
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/location/geofencer/b/e;->e:Z

    .line 742
    iput p1, p0, Lcom/google/android/location/geofencer/b/e;->f:I

    .line 743
    return-object p0
.end method

.method public final a(J)Lcom/google/android/location/geofencer/b/e;
    .locals 1

    .prologue
    .line 775
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/location/geofencer/b/e;->i:Z

    .line 776
    iput-wide p1, p0, Lcom/google/android/location/geofencer/b/e;->j:J

    .line 777
    return-object p0
.end method

.method public final a(Lcom/google/android/location/geofencer/b/d;)Lcom/google/android/location/geofencer/b/e;
    .locals 1

    .prologue
    .line 704
    if-nez p1, :cond_0

    .line 705
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 707
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/location/geofencer/b/e;->a:Z

    .line 708
    iput-object p1, p0, Lcom/google/android/location/geofencer/b/e;->b:Lcom/google/android/location/geofencer/b/d;

    .line 709
    return-object p0
.end method

.method public final a(Ljava/lang/String;)Lcom/google/android/location/geofencer/b/e;
    .locals 1

    .prologue
    .line 724
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/location/geofencer/b/e;->c:Z

    .line 725
    iput-object p1, p0, Lcom/google/android/location/geofencer/b/e;->d:Ljava/lang/String;

    .line 726
    return-object p0
.end method

.method public final a(Z)Lcom/google/android/location/geofencer/b/e;
    .locals 1

    .prologue
    .line 792
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/location/geofencer/b/e;->k:Z

    .line 793
    iput-boolean p1, p0, Lcom/google/android/location/geofencer/b/e;->l:Z

    .line 794
    return-object p0
.end method

.method public final synthetic a(Lcom/google/protobuf/a/b;)Lcom/google/protobuf/a/f;
    .locals 2

    .prologue
    .line 692
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/a/b;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/a/b;->b(I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    new-instance v0, Lcom/google/android/location/geofencer/b/d;

    invoke-direct {v0}, Lcom/google/android/location/geofencer/b/d;-><init>()V

    invoke-virtual {p1, v0}, Lcom/google/protobuf/a/b;->a(Lcom/google/protobuf/a/f;)V

    invoke-virtual {p0, v0}, Lcom/google/android/location/geofencer/b/e;->a(Lcom/google/android/location/geofencer/b/d;)Lcom/google/android/location/geofencer/b/e;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/a/b;->e()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/location/geofencer/b/e;->a(Ljava/lang/String;)Lcom/google/android/location/geofencer/b/e;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/a/b;->h()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/location/geofencer/b/e;->a(I)Lcom/google/android/location/geofencer/b/e;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/a/b;->h()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/location/geofencer/b/e;->b(I)Lcom/google/android/location/geofencer/b/e;

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/a/b;->i()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lcom/google/android/location/geofencer/b/e;->a(J)Lcom/google/android/location/geofencer/b/e;

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/a/b;->d()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/location/geofencer/b/e;->a(Z)Lcom/google/android/location/geofencer/b/e;

    goto :goto_0

    :sswitch_7
    invoke-virtual {p1}, Lcom/google/protobuf/a/b;->d()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/location/geofencer/b/e;->b(Z)Lcom/google/android/location/geofencer/b/e;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
        0x30 -> :sswitch_6
        0x38 -> :sswitch_7
    .end sparse-switch
.end method

.method public final a(Lcom/google/protobuf/a/c;)V
    .locals 4

    .prologue
    .line 838
    iget-boolean v0, p0, Lcom/google/android/location/geofencer/b/e;->a:Z

    if-eqz v0, :cond_0

    .line 839
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/location/geofencer/b/e;->b:Lcom/google/android/location/geofencer/b/d;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/a/c;->b(ILcom/google/protobuf/a/f;)V

    .line 841
    :cond_0
    iget-boolean v0, p0, Lcom/google/android/location/geofencer/b/e;->c:Z

    if-eqz v0, :cond_1

    .line 842
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/android/location/geofencer/b/e;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/a/c;->a(ILjava/lang/String;)V

    .line 844
    :cond_1
    iget-boolean v0, p0, Lcom/google/android/location/geofencer/b/e;->e:Z

    if-eqz v0, :cond_2

    .line 845
    const/4 v0, 0x3

    iget v1, p0, Lcom/google/android/location/geofencer/b/e;->f:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/a/c;->a(II)V

    .line 847
    :cond_2
    iget-boolean v0, p0, Lcom/google/android/location/geofencer/b/e;->g:Z

    if-eqz v0, :cond_3

    .line 848
    const/4 v0, 0x4

    iget v1, p0, Lcom/google/android/location/geofencer/b/e;->h:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/a/c;->a(II)V

    .line 850
    :cond_3
    iget-boolean v0, p0, Lcom/google/android/location/geofencer/b/e;->i:Z

    if-eqz v0, :cond_4

    .line 851
    const/4 v0, 0x5

    iget-wide v2, p0, Lcom/google/android/location/geofencer/b/e;->j:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/a/c;->b(IJ)V

    .line 853
    :cond_4
    iget-boolean v0, p0, Lcom/google/android/location/geofencer/b/e;->k:Z

    if-eqz v0, :cond_5

    .line 854
    const/4 v0, 0x6

    iget-boolean v1, p0, Lcom/google/android/location/geofencer/b/e;->l:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/a/c;->a(IZ)V

    .line 856
    :cond_5
    iget-boolean v0, p0, Lcom/google/android/location/geofencer/b/e;->n:Z

    if-eqz v0, :cond_6

    .line 857
    const/4 v0, 0x7

    iget-boolean v1, p0, Lcom/google/android/location/geofencer/b/e;->m:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/a/c;->a(IZ)V

    .line 859
    :cond_6
    return-void
.end method

.method public final b()I
    .locals 4

    .prologue
    .line 873
    const/4 v0, 0x0

    .line 874
    iget-boolean v1, p0, Lcom/google/android/location/geofencer/b/e;->a:Z

    if-eqz v1, :cond_0

    .line 875
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/location/geofencer/b/e;->b:Lcom/google/android/location/geofencer/b/d;

    invoke-static {v0, v1}, Lcom/google/protobuf/a/c;->d(ILcom/google/protobuf/a/f;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 878
    :cond_0
    iget-boolean v1, p0, Lcom/google/android/location/geofencer/b/e;->c:Z

    if-eqz v1, :cond_1

    .line 879
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/location/geofencer/b/e;->d:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/a/c;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 882
    :cond_1
    iget-boolean v1, p0, Lcom/google/android/location/geofencer/b/e;->e:Z

    if-eqz v1, :cond_2

    .line 883
    const/4 v1, 0x3

    iget v2, p0, Lcom/google/android/location/geofencer/b/e;->f:I

    invoke-static {v1, v2}, Lcom/google/protobuf/a/c;->d(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 886
    :cond_2
    iget-boolean v1, p0, Lcom/google/android/location/geofencer/b/e;->g:Z

    if-eqz v1, :cond_3

    .line 887
    const/4 v1, 0x4

    iget v2, p0, Lcom/google/android/location/geofencer/b/e;->h:I

    invoke-static {v1, v2}, Lcom/google/protobuf/a/c;->d(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 890
    :cond_3
    iget-boolean v1, p0, Lcom/google/android/location/geofencer/b/e;->i:Z

    if-eqz v1, :cond_4

    .line 891
    const/4 v1, 0x5

    iget-wide v2, p0, Lcom/google/android/location/geofencer/b/e;->j:J

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/a/c;->e(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 894
    :cond_4
    iget-boolean v1, p0, Lcom/google/android/location/geofencer/b/e;->k:Z

    if-eqz v1, :cond_5

    .line 895
    const/4 v1, 0x6

    iget-boolean v2, p0, Lcom/google/android/location/geofencer/b/e;->l:Z

    invoke-static {v1}, Lcom/google/protobuf/a/c;->b(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 898
    :cond_5
    iget-boolean v1, p0, Lcom/google/android/location/geofencer/b/e;->n:Z

    if-eqz v1, :cond_6

    .line 899
    const/4 v1, 0x7

    iget-boolean v2, p0, Lcom/google/android/location/geofencer/b/e;->m:Z

    invoke-static {v1}, Lcom/google/protobuf/a/c;->b(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 902
    :cond_6
    iput v0, p0, Lcom/google/android/location/geofencer/b/e;->o:I

    .line 903
    return v0
.end method

.method public final b(I)Lcom/google/android/location/geofencer/b/e;
    .locals 1

    .prologue
    .line 758
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/location/geofencer/b/e;->g:Z

    .line 759
    iput p1, p0, Lcom/google/android/location/geofencer/b/e;->h:I

    .line 760
    return-object p0
.end method

.method public final b(Z)Lcom/google/android/location/geofencer/b/e;
    .locals 1

    .prologue
    .line 809
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/location/geofencer/b/e;->n:Z

    .line 810
    iput-boolean p1, p0, Lcom/google/android/location/geofencer/b/e;->m:Z

    .line 811
    return-object p0
.end method
