.class public final Lcom/google/android/location/activity/ag;
.super Lcom/google/android/location/activity/o;
.source "SourceFile"


# direct methods
.method public constructor <init>(Lcom/google/android/location/activity/k;Lcom/google/android/location/j/b;)V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0, p1, p2}, Lcom/google/android/location/activity/o;-><init>(Lcom/google/android/location/activity/k;Lcom/google/android/location/j/b;)V

    .line 27
    return-void
.end method

.method private a(J)Z
    .locals 7

    .prologue
    const-wide/16 v4, 0x0

    .line 125
    iget-object v0, p0, Lcom/google/android/location/activity/ag;->a:Lcom/google/android/location/activity/k;

    invoke-virtual {v0}, Lcom/google/android/location/activity/k;->u()J

    move-result-wide v0

    const-wide v2, 0x1f3fffffc18L

    cmp-long v0, v0, v2

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/location/activity/ag;->a:Lcom/google/android/location/activity/k;

    invoke-virtual {v0}, Lcom/google/android/location/activity/k;->u()J

    move-result-wide v0

    cmp-long v0, v0, v4

    if-eqz v0, :cond_1

    cmp-long v0, p1, v4

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/location/activity/ag;->a:Lcom/google/android/location/activity/k;

    invoke-virtual {v0}, Lcom/google/android/location/activity/k;->u()J

    move-result-wide v0

    rem-long/2addr v0, p1

    cmp-long v0, v0, v4

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/location/activity/ag;->a:Lcom/google/android/location/activity/k;

    invoke-virtual {v0}, Lcom/google/android/location/activity/k;->u()J

    move-result-wide v0

    rem-long v0, p1, v0

    cmp-long v0, v0, v4

    if-nez v0, :cond_1

    :cond_0
    const-wide/32 v0, 0x493e0

    cmp-long v0, p1, v0

    if-gez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final d()Lcom/google/android/location/activity/p;
    .locals 12

    .prologue
    .line 39
    invoke-virtual {p0}, Lcom/google/android/location/activity/ag;->a()J

    move-result-wide v10

    .line 40
    invoke-virtual {p0}, Lcom/google/android/location/activity/ag;->c()J

    move-result-wide v0

    .line 41
    iget-object v2, p0, Lcom/google/android/location/activity/ag;->a:Lcom/google/android/location/activity/k;

    invoke-virtual {v2}, Lcom/google/android/location/activity/k;->w()Lcom/google/android/location/e/b;

    move-result-object v2

    .line 42
    if-nez v2, :cond_3

    const-wide/16 v2, -0x1

    .line 44
    :goto_0
    sget-boolean v4, Lcom/google/android/location/i/a;->b:Z

    if-eqz v4, :cond_0

    const-string v4, "ActivityScheduler"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "nextFullTrigger: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " nextTiltOnlyTrigger: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " nextLocatorTrigger: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/android/location/o/a/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 48
    :cond_0
    iget-object v4, p0, Lcom/google/android/location/activity/ag;->a:Lcom/google/android/location/activity/k;

    invoke-virtual {v4}, Lcom/google/android/location/activity/k;->r()J

    move-result-wide v4

    invoke-direct {p0, v4, v5}, Lcom/google/android/location/activity/ag;->a(J)Z

    move-result v4

    if-eqz v4, :cond_b

    iget-object v4, p0, Lcom/google/android/location/activity/ag;->a:Lcom/google/android/location/activity/k;

    invoke-virtual {v4}, Lcom/google/android/location/activity/k;->r()J

    move-result-wide v4

    iget-object v6, p0, Lcom/google/android/location/activity/ag;->a:Lcom/google/android/location/activity/k;

    invoke-virtual {v6}, Lcom/google/android/location/activity/k;->u()J

    move-result-wide v6

    invoke-static/range {v0 .. v7}, Lcom/google/android/location/activity/ag;->a(JJJJ)Z

    move-result v4

    if-eqz v4, :cond_b

    .line 51
    const-wide/16 v0, 0x3e8

    add-long/2addr v0, v2

    move-wide v8, v0

    .line 58
    :goto_1
    iget-object v0, p0, Lcom/google/android/location/activity/ag;->a:Lcom/google/android/location/activity/k;

    invoke-virtual {v0}, Lcom/google/android/location/activity/k;->q()J

    move-result-wide v0

    invoke-direct {p0, v0, v1}, Lcom/google/android/location/activity/ag;->a(J)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 59
    iget-object v0, p0, Lcom/google/android/location/activity/ag;->a:Lcom/google/android/location/activity/k;

    invoke-virtual {v0}, Lcom/google/android/location/activity/k;->q()J

    move-result-wide v4

    iget-object v0, p0, Lcom/google/android/location/activity/ag;->a:Lcom/google/android/location/activity/k;

    invoke-virtual {v0}, Lcom/google/android/location/activity/k;->u()J

    move-result-wide v6

    move-wide v0, v10

    invoke-static/range {v0 .. v7}, Lcom/google/android/location/activity/ag;->a(JJJJ)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 61
    const-wide/16 v0, 0x3e8

    add-long/2addr v0, v2

    .line 86
    :goto_2
    iget-object v2, p0, Lcom/google/android/location/activity/ag;->a:Lcom/google/android/location/activity/k;

    invoke-virtual {v2}, Lcom/google/android/location/activity/k;->D()J

    move-result-wide v2

    .line 87
    const-wide/16 v4, -0x1

    cmp-long v2, v2, v4

    if-eqz v2, :cond_8

    .line 88
    iget-object v2, p0, Lcom/google/android/location/activity/ag;->a:Lcom/google/android/location/activity/k;

    invoke-virtual {v2}, Lcom/google/android/location/activity/k;->D()J

    move-result-wide v2

    const-wide/32 v4, 0xafc8

    add-long/2addr v2, v4

    .line 90
    iget-object v4, p0, Lcom/google/android/location/activity/ag;->b:Lcom/google/android/location/j/b;

    invoke-interface {v4}, Lcom/google/android/location/j/b;->c()J

    move-result-wide v4

    cmp-long v4, v2, v4

    if-gez v4, :cond_7

    .line 93
    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v2

    .line 95
    :goto_3
    iget-object v4, p0, Lcom/google/android/location/activity/ag;->a:Lcom/google/android/location/activity/k;

    const-wide/16 v6, -0x1

    invoke-virtual {v4, v6, v7}, Lcom/google/android/location/activity/k;->d(J)V

    .line 98
    :goto_4
    const/4 v4, 0x1

    .line 99
    invoke-virtual {p0, v8, v9, v2, v3}, Lcom/google/android/location/activity/ag;->a(JJ)Z

    move-result v5

    if-eqz v5, :cond_6

    .line 101
    sget-boolean v2, Lcom/google/android/location/i/a;->b:Z

    if-eqz v2, :cond_1

    const-string v2, "ActivityScheduler"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Running tilt only detector next: FullTrigger: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " TiltOnlyTrigger: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/google/android/location/o/a/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 104
    :cond_1
    const/4 v0, 0x2

    .line 106
    :goto_5
    iget-object v1, p0, Lcom/google/android/location/activity/ag;->a:Lcom/google/android/location/activity/k;

    invoke-virtual {v1}, Lcom/google/android/location/activity/k;->q()J

    move-result-wide v2

    iget-object v1, p0, Lcom/google/android/location/activity/ag;->a:Lcom/google/android/location/activity/k;

    invoke-virtual {v1}, Lcom/google/android/location/activity/k;->r()J

    move-result-wide v4

    invoke-static {v2, v3, v4, v5}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v2

    .line 108
    iget-object v1, p0, Lcom/google/android/location/activity/ag;->a:Lcom/google/android/location/activity/k;

    invoke-virtual {v1}, Lcom/google/android/location/activity/k;->B()Z

    move-result v1

    if-eqz v1, :cond_5

    const-wide/16 v4, 0x3

    div-long/2addr v2, v4

    .line 110
    :goto_6
    sget-boolean v1, Lcom/google/android/location/i/a;->b:Z

    if-eqz v1, :cond_2

    const-string v1, "ActivityScheduler"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "nextTriggerTime: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " detectorType: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " alarmWindowMillis: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v4}, Lcom/google/android/location/o/a/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 112
    :cond_2
    new-instance v1, Lcom/google/android/location/activity/p;

    new-instance v4, Lcom/google/android/location/e/b;

    invoke-direct {v4, v8, v9, v2, v3}, Lcom/google/android/location/e/b;-><init>(JJ)V

    invoke-direct {v1, p0, v4, v0}, Lcom/google/android/location/activity/p;-><init>(Lcom/google/android/location/activity/o;Lcom/google/android/location/e/b;I)V

    return-object v1

    .line 42
    :cond_3
    iget-wide v2, v2, Lcom/google/android/location/e/b;->a:J

    goto/16 :goto_0

    .line 62
    :cond_4
    invoke-static {}, Lcom/google/android/location/activity/k;->b()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 63
    invoke-virtual {p0}, Lcom/google/android/location/activity/ag;->b()J

    move-result-wide v0

    .line 64
    cmp-long v4, v10, v0

    if-eqz v4, :cond_9

    .line 68
    cmp-long v4, v0, v8

    if-lez v4, :cond_a

    move-wide v10, v8

    .line 76
    :goto_7
    cmp-long v4, v2, v10

    if-gez v4, :cond_9

    iget-object v4, p0, Lcom/google/android/location/activity/ag;->a:Lcom/google/android/location/activity/k;

    invoke-virtual {v4}, Lcom/google/android/location/activity/k;->q()J

    move-result-wide v4

    iget-object v6, p0, Lcom/google/android/location/activity/ag;->a:Lcom/google/android/location/activity/k;

    invoke-virtual {v6}, Lcom/google/android/location/activity/k;->u()J

    move-result-wide v6

    invoke-static/range {v0 .. v7}, Lcom/google/android/location/activity/ag;->a(JJJJ)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 79
    const-wide/16 v0, 0x3e8

    add-long/2addr v0, v2

    goto/16 :goto_2

    .line 108
    :cond_5
    const-wide/16 v4, 0x8

    div-long/2addr v2, v4

    goto :goto_6

    :cond_6
    move v0, v4

    move-wide v8, v2

    goto/16 :goto_5

    :cond_7
    move-wide v2, v0

    goto/16 :goto_3

    :cond_8
    move-wide v2, v0

    goto/16 :goto_4

    :cond_9
    move-wide v0, v10

    goto/16 :goto_2

    :cond_a
    move-wide v10, v0

    goto :goto_7

    :cond_b
    move-wide v8, v0

    goto/16 :goto_1
.end method
