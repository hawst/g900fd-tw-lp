.class final Lcom/google/android/location/copresence/o/p;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final a:Ljava/util/ArrayList;

.field final synthetic b:Lcom/google/android/location/copresence/o/k;


# direct methods
.method public constructor <init>(Lcom/google/android/location/copresence/o/k;)V
    .locals 1

    .prologue
    .line 172
    iput-object p1, p0, Lcom/google/android/location/copresence/o/p;->b:Lcom/google/android/location/copresence/o/k;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 173
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/location/copresence/o/p;->a:Ljava/util/ArrayList;

    .line 174
    return-void
.end method


# virtual methods
.method public final a()Ljava/util/ArrayList;
    .locals 6

    .prologue
    .line 182
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 183
    iget-object v0, p0, Lcom/google/android/location/copresence/o/p;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/copresence/o/q;

    .line 184
    iget-wide v4, v0, Lcom/google/android/location/copresence/o/q;->a:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 186
    :cond_0
    return-object v1
.end method

.method public final a(JILjava/lang/String;Lcom/google/android/gms/location/copresence/internal/SubscribeOperation;Lcom/google/android/gms/location/copresence/internal/UnsubscribeOperation;)V
    .locals 11

    .prologue
    .line 178
    iget-object v9, p0, Lcom/google/android/location/copresence/o/p;->a:Ljava/util/ArrayList;

    new-instance v0, Lcom/google/android/location/copresence/o/q;

    const/4 v6, 0x0

    move-object v1, p0

    move-wide v2, p1

    move v4, p3

    move-object v5, p4

    move-object/from16 v7, p5

    move-object/from16 v8, p6

    invoke-direct/range {v0 .. v8}, Lcom/google/android/location/copresence/o/q;-><init>(Lcom/google/android/location/copresence/o/p;JILjava/lang/String;ILcom/google/android/gms/location/copresence/internal/SubscribeOperation;Lcom/google/android/gms/location/copresence/internal/UnsubscribeOperation;)V

    invoke-virtual {v9, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 179
    return-void
.end method
