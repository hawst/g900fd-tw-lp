.class final Lcom/google/android/location/geofencer/service/a;
.super Lcom/google/android/location/geofencer/service/e;
.source "SourceFile"


# instance fields
.field public final a:Lcom/google/android/gms/location/GeofencingRequest;

.field public final b:Landroid/app/PendingIntent;

.field public final c:Lcom/google/android/location/geofencer/service/f;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/location/GeofencingRequest;Landroid/app/PendingIntent;Lcom/google/android/location/geofencer/service/f;)V
    .locals 1

    .prologue
    .line 25
    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lcom/google/android/location/geofencer/service/e;-><init>(I)V

    .line 26
    iput-object p1, p0, Lcom/google/android/location/geofencer/service/a;->a:Lcom/google/android/gms/location/GeofencingRequest;

    .line 27
    iput-object p2, p0, Lcom/google/android/location/geofencer/service/a;->b:Landroid/app/PendingIntent;

    .line 28
    iput-object p3, p0, Lcom/google/android/location/geofencer/service/a;->c:Lcom/google/android/location/geofencer/service/f;

    .line 29
    return-void
.end method

.method public static a(Lcom/google/android/location/geofencer/service/f;ILcom/google/android/gms/location/GeofencingRequest;)V
    .locals 4

    .prologue
    .line 38
    if-eqz p0, :cond_1

    .line 39
    invoke-virtual {p2}, Lcom/google/android/gms/location/GeofencingRequest;->b()Ljava/util/List;

    move-result-object v1

    .line 40
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v0

    new-array v2, v0, [Ljava/lang/String;

    .line 41
    const/4 v0, 0x0

    .line 42
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v1, v0

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/location/internal/ParcelableGeofence;

    .line 43
    invoke-virtual {v0}, Lcom/google/android/gms/location/internal/ParcelableGeofence;->a()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v2, v1

    .line 44
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    .line 45
    goto :goto_0

    .line 46
    :cond_0
    invoke-interface {p0, p1, v2}, Lcom/google/android/location/geofencer/service/f;->a(I[Ljava/lang/String;)V

    .line 48
    :cond_1
    return-void
.end method


# virtual methods
.method protected final a()V
    .locals 3

    .prologue
    .line 33
    iget-object v1, p0, Lcom/google/android/location/geofencer/service/a;->c:Lcom/google/android/location/geofencer/service/f;

    invoke-virtual {p0}, Lcom/google/android/location/geofencer/service/a;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iget-object v2, p0, Lcom/google/android/location/geofencer/service/a;->a:Lcom/google/android/gms/location/GeofencingRequest;

    invoke-static {v1, v0, v2}, Lcom/google/android/location/geofencer/service/a;->a(Lcom/google/android/location/geofencer/service/f;ILcom/google/android/gms/location/GeofencingRequest;)V

    .line 34
    return-void
.end method
