.class final Lcom/google/android/location/copresence/d/h;
.super Landroid/content/BroadcastReceiver;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/location/copresence/d/g;


# direct methods
.method constructor <init>(Lcom/google/android/location/copresence/d/g;)V
    .locals 0

    .prologue
    .line 32
    iput-object p1, p0, Lcom/google/android/location/copresence/d/h;->a:Lcom/google/android/location/copresence/d/g;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public final onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 4

    .prologue
    .line 36
    if-eqz p2, :cond_1

    const-string v0, "android.bluetooth.device.action.FOUND"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 37
    iget-object v0, p0, Lcom/google/android/location/copresence/d/h;->a:Lcom/google/android/location/copresence/d/g;

    invoke-static {v0}, Lcom/google/android/location/copresence/d/g;->a(Lcom/google/android/location/copresence/d/g;)Lcom/google/android/location/copresence/ab;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 38
    invoke-virtual {p2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "android.bluetooth.device.extra.DEVICE"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/bluetooth/BluetoothDevice;

    .line 40
    invoke-virtual {p2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "android.bluetooth.device.extra.RSSI"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Short;

    .line 41
    if-nez v1, :cond_2

    const/high16 v1, -0x80000000

    .line 42
    :goto_0
    const/4 v2, 0x2

    invoke-static {v2}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 43
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "BluetoothDiscoveryTokenListener: Discovered device: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothDevice;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " (rssi: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ")"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/location/copresence/ag;->a(Ljava/lang/String;)I

    .line 46
    :cond_0
    iget-object v2, p0, Lcom/google/android/location/copresence/d/h;->a:Lcom/google/android/location/copresence/d/g;

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothDevice;->getName()Ljava/lang/String;

    move-result-object v0

    iget-object v3, p0, Lcom/google/android/location/copresence/d/h;->a:Lcom/google/android/location/copresence/d/g;

    invoke-static {v3}, Lcom/google/android/location/copresence/d/g;->b(Lcom/google/android/location/copresence/d/g;)Lcom/google/ac/b/c/o;

    move-result-object v3

    invoke-static {v2, v0, v1, v3}, Lcom/google/android/location/copresence/d/g;->a(Lcom/google/android/location/copresence/d/g;Ljava/lang/String;ILcom/google/ac/b/c/o;)V

    .line 49
    :cond_1
    return-void

    .line 41
    :cond_2
    invoke-virtual {v1}, Ljava/lang/Short;->shortValue()S

    move-result v1

    goto :goto_0
.end method
