.class public final Lcom/google/android/location/places/ui/k;
.super Landroid/support/v4/app/ar;
.source "SourceFile"


# instance fields
.field private A:Landroid/view/View;

.field private B:Landroid/widget/Button;

.field private C:Landroid/widget/Button;

.field private D:Landroid/view/View;

.field private E:Landroid/view/View;

.field private F:Landroid/widget/TextView;

.field private G:Landroid/view/View;

.field private H:Landroid/view/View;

.field private I:Ljava/lang/String;

.field private J:Ljava/lang/String;

.field private K:Lcom/google/android/location/places/ui/p;

.field private L:Lcom/google/android/gms/location/places/f;

.field i:Landroid/view/View;

.field j:Landroid/view/View;

.field k:Lcom/google/android/location/places/ui/o;

.field l:Lcom/google/android/location/places/ui/r;

.field m:I

.field n:I

.field o:I

.field p:Z

.field q:Z

.field r:Z

.field private s:Landroid/widget/TextView;

.field private t:Landroid/widget/TextView;

.field private u:Landroid/widget/TextView;

.field private v:Lcom/google/android/location/places/ui/TransparentView;

.field private w:Landroid/view/View;

.field private x:Landroid/widget/TextView;

.field private y:Landroid/view/View;

.field private z:Landroid/view/View;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 91
    invoke-direct {p0}, Landroid/support/v4/app/ar;-><init>()V

    .line 92
    return-void
.end method

.method static synthetic a(Lcom/google/android/location/places/ui/k;)Landroid/view/View;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lcom/google/android/location/places/ui/k;->j:Landroid/view/View;

    return-object v0
.end method

.method static synthetic b(Lcom/google/android/location/places/ui/k;)Lcom/google/android/gms/location/places/f;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lcom/google/android/location/places/ui/k;->L:Lcom/google/android/gms/location/places/f;

    return-object v0
.end method

.method static synthetic c(Lcom/google/android/location/places/ui/k;)Lcom/google/android/location/places/ui/o;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lcom/google/android/location/places/ui/k;->k:Lcom/google/android/location/places/ui/o;

    return-object v0
.end method

.method static synthetic d(Lcom/google/android/location/places/ui/k;)Lcom/google/android/location/places/ui/r;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lcom/google/android/location/places/ui/k;->l:Lcom/google/android/location/places/ui/r;

    return-object v0
.end method

.method private e()V
    .locals 2

    .prologue
    const/16 v1, 0x8

    .line 342
    iget-object v0, p0, Lcom/google/android/location/places/ui/k;->K:Lcom/google/android/location/places/ui/p;

    invoke-virtual {v0}, Lcom/google/android/location/places/ui/p;->clear()V

    .line 343
    iget-object v0, p0, Lcom/google/android/location/places/ui/k;->y:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 344
    iget-object v0, p0, Lcom/google/android/location/places/ui/k;->A:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 345
    iget-object v0, p0, Lcom/google/android/location/places/ui/k;->E:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 346
    iget-object v0, p0, Lcom/google/android/location/places/ui/k;->F:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 347
    iget-object v0, p0, Lcom/google/android/location/places/ui/k;->H:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 348
    iget-object v0, p0, Lcom/google/android/location/places/ui/k;->x:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 349
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/location/places/f;)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 258
    iput-object p1, p0, Lcom/google/android/location/places/ui/k;->L:Lcom/google/android/gms/location/places/f;

    .line 259
    iget-object v2, p0, Lcom/google/android/location/places/ui/k;->j:Landroid/view/View;

    if-eqz p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v2, v0}, Landroid/view/View;->setClickable(Z)V

    .line 261
    iget-object v0, p0, Lcom/google/android/location/places/ui/k;->u:Landroid/widget/TextView;

    const/4 v2, 0x4

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 262
    iget-object v0, p0, Lcom/google/android/location/places/ui/k;->s:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 263
    iget-object v0, p0, Lcom/google/android/location/places/ui/k;->t:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 265
    if-eqz p1, :cond_4

    .line 266
    invoke-virtual {p1}, Lcom/google/android/gms/location/places/f;->d()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p1}, Lcom/google/android/gms/location/places/f;->c()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 267
    iget-object v0, p0, Lcom/google/android/location/places/ui/k;->s:Landroid/widget/TextView;

    invoke-virtual {p1}, Lcom/google/android/gms/location/places/f;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 268
    iget-object v0, p0, Lcom/google/android/location/places/ui/k;->t:Landroid/widget/TextView;

    invoke-virtual {p1}, Lcom/google/android/gms/location/places/f;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 281
    :goto_1
    return-void

    :cond_0
    move v0, v1

    .line 259
    goto :goto_0

    .line 269
    :cond_1
    invoke-virtual {p1}, Lcom/google/android/gms/location/places/f;->d()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 270
    iget-object v0, p0, Lcom/google/android/location/places/ui/k;->s:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/google/android/location/places/ui/k;->J:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 271
    iget-object v0, p0, Lcom/google/android/location/places/ui/k;->t:Landroid/widget/TextView;

    invoke-virtual {p1}, Lcom/google/android/gms/location/places/f;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 272
    :cond_2
    invoke-virtual {p1}, Lcom/google/android/gms/location/places/f;->c()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 273
    iget-object v0, p0, Lcom/google/android/location/places/ui/k;->s:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/google/android/location/places/ui/k;->J:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 274
    iget-object v0, p0, Lcom/google/android/location/places/ui/k;->t:Landroid/widget/TextView;

    invoke-virtual {p1}, Lcom/google/android/gms/location/places/f;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 276
    :cond_3
    iget-object v0, p0, Lcom/google/android/location/places/ui/k;->I:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/google/android/location/places/ui/k;->a(Ljava/lang/String;)V

    goto :goto_1

    .line 279
    :cond_4
    iget-object v0, p0, Lcom/google/android/location/places/ui/k;->I:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/google/android/location/places/ui/k;->a(Ljava/lang/String;)V

    goto :goto_1
.end method

.method public final a(Ljava/lang/String;)V
    .locals 3

    .prologue
    const/4 v2, 0x4

    const/4 v1, 0x0

    .line 284
    iget-object v0, p0, Lcom/google/android/location/places/ui/k;->j:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setClickable(Z)V

    .line 285
    iget-object v0, p0, Lcom/google/android/location/places/ui/k;->u:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 286
    iget-object v0, p0, Lcom/google/android/location/places/ui/k;->s:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 287
    iget-object v0, p0, Lcom/google/android/location/places/ui/k;->t:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 288
    iget-object v0, p0, Lcom/google/android/location/places/ui/k;->u:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 289
    return-void
.end method

.method public final a(Ljava/lang/String;[Lcom/google/android/gms/location/places/f;Ljava/lang/String;)V
    .locals 5
    .annotation build Landroid/annotation/TargetApi;
        value = 0x10
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 294
    iget-boolean v0, p0, Lcom/google/android/location/places/ui/k;->r:Z

    if-eqz v0, :cond_0

    .line 306
    :goto_0
    return-void

    .line 298
    :cond_0
    iget-object v0, p0, Lcom/google/android/location/places/ui/k;->x:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 300
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x10

    if-lt v0, v2, :cond_1

    .line 301
    invoke-virtual {p0}, Lcom/google/android/location/places/ui/k;->a()Landroid/widget/ListView;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setImportantForAccessibility(I)V

    .line 304
    :cond_1
    invoke-direct {p0}, Lcom/google/android/location/places/ui/k;->e()V

    if-eqz p2, :cond_2

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0xb

    if-lt v0, v2, :cond_3

    iget-object v0, p0, Lcom/google/android/location/places/ui/k;->K:Lcom/google/android/location/places/ui/p;

    invoke-virtual {v0, p2}, Lcom/google/android/location/places/ui/p;->addAll([Ljava/lang/Object;)V

    .line 305
    :cond_2
    iget-object v0, p0, Lcom/google/android/location/places/ui/k;->E:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/google/android/location/places/ui/k;->F:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0

    .line 304
    :cond_3
    array-length v2, p2

    move v0, v1

    :goto_1
    if-ge v0, v2, :cond_2

    aget-object v3, p2, v0

    iget-object v4, p0, Lcom/google/android/location/places/ui/k;->K:Lcom/google/android/location/places/ui/p;

    invoke-virtual {v4, v3}, Lcom/google/android/location/places/ui/p;->add(Ljava/lang/Object;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 305
    :cond_4
    iget-object v0, p0, Lcom/google/android/location/places/ui/k;->F:Landroid/widget/TextView;

    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    iget-object v0, p0, Lcom/google/android/location/places/ui/k;->F:Landroid/widget/TextView;

    invoke-static {p3}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/android/location/places/ui/k;->F:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method

.method public final b(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 320
    invoke-direct {p0}, Lcom/google/android/location/places/ui/k;->e()V

    .line 321
    invoke-virtual {p0, p1}, Lcom/google/android/location/places/ui/k;->a(Ljava/lang/String;)V

    .line 322
    iget-object v0, p0, Lcom/google/android/location/places/ui/k;->H:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 323
    return-void
.end method

.method public final c()V
    .locals 2

    .prologue
    .line 309
    invoke-direct {p0}, Lcom/google/android/location/places/ui/k;->e()V

    .line 310
    iget-object v0, p0, Lcom/google/android/location/places/ui/k;->x:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 311
    iget-object v0, p0, Lcom/google/android/location/places/ui/k;->y:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 312
    return-void
.end method

.method public final d()V
    .locals 2

    .prologue
    .line 315
    iget-object v0, p0, Lcom/google/android/location/places/ui/k;->A:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 316
    return-void
.end method

.method public final onActivityCreated(Landroid/os/Bundle;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 133
    invoke-super {p0, p1}, Landroid/support/v4/app/ar;->onActivityCreated(Landroid/os/Bundle;)V

    .line 135
    sget v0, Lcom/google/android/gms/p;->xF:I

    invoke-virtual {p0, v0}, Lcom/google/android/location/places/ui/k;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/places/ui/k;->I:Ljava/lang/String;

    .line 137
    invoke-virtual {p0}, Lcom/google/android/location/places/ui/k;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/q;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    sget v1, Lcom/google/android/gms/l;->dh:I

    invoke-virtual {p0}, Lcom/google/android/location/places/ui/k;->a()Landroid/widget/ListView;

    move-result-object v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/places/ui/TransparentView;

    iput-object v0, p0, Lcom/google/android/location/places/ui/k;->v:Lcom/google/android/location/places/ui/TransparentView;

    .line 139
    invoke-virtual {p0}, Lcom/google/android/location/places/ui/k;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/q;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    sget v1, Lcom/google/android/gms/l;->df:I

    invoke-virtual {p0}, Lcom/google/android/location/places/ui/k;->a()Landroid/widget/ListView;

    move-result-object v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/places/ui/k;->j:Landroid/view/View;

    .line 141
    invoke-virtual {p0}, Lcom/google/android/location/places/ui/k;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/q;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    sget v1, Lcom/google/android/gms/l;->db:I

    invoke-virtual {p0}, Lcom/google/android/location/places/ui/k;->a()Landroid/widget/ListView;

    move-result-object v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/places/ui/k;->w:Landroid/view/View;

    .line 143
    invoke-virtual {p0}, Lcom/google/android/location/places/ui/k;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/q;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    sget v1, Lcom/google/android/gms/l;->dd:I

    invoke-virtual {p0}, Lcom/google/android/location/places/ui/k;->a()Landroid/widget/ListView;

    move-result-object v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/places/ui/k;->G:Landroid/view/View;

    .line 145
    invoke-virtual {p0}, Lcom/google/android/location/places/ui/k;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/q;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    sget v1, Lcom/google/android/gms/l;->cU:I

    invoke-virtual {p0}, Lcom/google/android/location/places/ui/k;->a()Landroid/widget/ListView;

    move-result-object v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/places/ui/k;->z:Landroid/view/View;

    .line 147
    invoke-virtual {p0}, Lcom/google/android/location/places/ui/k;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/q;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    sget v1, Lcom/google/android/gms/l;->cW:I

    invoke-virtual {p0}, Lcom/google/android/location/places/ui/k;->a()Landroid/widget/ListView;

    move-result-object v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/places/ui/k;->D:Landroid/view/View;

    .line 149
    invoke-virtual {p0}, Lcom/google/android/location/places/ui/k;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    sget v1, Lcom/google/android/gms/j;->ok:I

    invoke-virtual {v0, v1}, Landroid/support/v4/app/q;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/places/ui/k;->i:Landroid/view/View;

    .line 150
    iget-object v0, p0, Lcom/google/android/location/places/ui/k;->w:Landroid/view/View;

    sget v1, Lcom/google/android/gms/j;->oy:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/location/places/ui/k;->x:Landroid/widget/TextView;

    .line 152
    iget-object v0, p0, Lcom/google/android/location/places/ui/k;->w:Landroid/view/View;

    sget v1, Lcom/google/android/gms/j;->ow:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/places/ui/k;->y:Landroid/view/View;

    .line 154
    iget-object v0, p0, Lcom/google/android/location/places/ui/k;->y:Landroid/view/View;

    sget v1, Lcom/google/android/gms/j;->ox:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/google/android/location/places/ui/k;->C:Landroid/widget/Button;

    .line 156
    iget-object v0, p0, Lcom/google/android/location/places/ui/k;->G:Landroid/view/View;

    sget v1, Lcom/google/android/gms/j;->pR:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/places/ui/k;->H:Landroid/view/View;

    .line 157
    iget-object v0, p0, Lcom/google/android/location/places/ui/k;->z:Landroid/view/View;

    sget v1, Lcom/google/android/gms/j;->K:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/places/ui/k;->A:Landroid/view/View;

    .line 158
    iget-object v0, p0, Lcom/google/android/location/places/ui/k;->A:Landroid/view/View;

    sget v1, Lcom/google/android/gms/j;->G:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/google/android/location/places/ui/k;->B:Landroid/widget/Button;

    .line 159
    iget-object v0, p0, Lcom/google/android/location/places/ui/k;->D:Landroid/view/View;

    sget v1, Lcom/google/android/gms/j;->aQ:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/places/ui/k;->E:Landroid/view/View;

    .line 160
    iget-object v0, p0, Lcom/google/android/location/places/ui/k;->D:Landroid/view/View;

    sget v1, Lcom/google/android/gms/j;->aR:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/location/places/ui/k;->F:Landroid/widget/TextView;

    .line 163
    invoke-virtual {p0, v4}, Lcom/google/android/location/places/ui/k;->a(Landroid/widget/ListAdapter;)V

    .line 166
    invoke-virtual {p0}, Lcom/google/android/location/places/ui/k;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    sget v1, Lcom/google/android/gms/j;->oo:I

    invoke-virtual {v0, v1}, Landroid/support/v4/app/q;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 167
    iget-object v1, p0, Lcom/google/android/location/places/ui/k;->v:Lcom/google/android/location/places/ui/TransparentView;

    invoke-virtual {v1, v0}, Lcom/google/android/location/places/ui/TransparentView;->a(Landroid/view/View;)V

    .line 168
    iget-object v0, p0, Lcom/google/android/location/places/ui/k;->v:Lcom/google/android/location/places/ui/TransparentView;

    iget v1, p0, Lcom/google/android/location/places/ui/k;->n:I

    iget v2, p0, Lcom/google/android/location/places/ui/k;->m:I

    invoke-virtual {v0, v1, v2}, Lcom/google/android/location/places/ui/TransparentView;->a(II)V

    .line 169
    invoke-virtual {p0}, Lcom/google/android/location/places/ui/k;->a()Landroid/widget/ListView;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/location/places/ui/k;->v:Lcom/google/android/location/places/ui/TransparentView;

    invoke-virtual {v0, v1, v4, v3}, Landroid/widget/ListView;->addHeaderView(Landroid/view/View;Ljava/lang/Object;Z)V

    .line 172
    sget v0, Lcom/google/android/gms/p;->sA:I

    invoke-virtual {p0, v0}, Lcom/google/android/location/places/ui/k;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/places/ui/k;->J:Ljava/lang/String;

    .line 173
    iget-object v0, p0, Lcom/google/android/location/places/ui/k;->j:Landroid/view/View;

    sget v1, Lcom/google/android/gms/j;->rh:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/location/places/ui/k;->s:Landroid/widget/TextView;

    .line 174
    iget-object v0, p0, Lcom/google/android/location/places/ui/k;->j:Landroid/view/View;

    sget v1, Lcom/google/android/gms/j;->rg:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/location/places/ui/k;->t:Landroid/widget/TextView;

    .line 175
    iget-object v0, p0, Lcom/google/android/location/places/ui/k;->j:Landroid/view/View;

    sget v1, Lcom/google/android/gms/j;->rf:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/location/places/ui/k;->u:Landroid/widget/TextView;

    .line 176
    invoke-virtual {p0}, Lcom/google/android/location/places/ui/k;->a()Landroid/widget/ListView;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/location/places/ui/k;->j:Landroid/view/View;

    invoke-virtual {v0, v1, v4, v3}, Landroid/widget/ListView;->addHeaderView(Landroid/view/View;Ljava/lang/Object;Z)V

    .line 178
    iget-boolean v0, p0, Lcom/google/android/location/places/ui/k;->r:Z

    if-nez v0, :cond_0

    .line 179
    invoke-virtual {p0}, Lcom/google/android/location/places/ui/k;->a()Landroid/widget/ListView;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/location/places/ui/k;->w:Landroid/view/View;

    invoke-virtual {v0, v1, v4, v3}, Landroid/widget/ListView;->addHeaderView(Landroid/view/View;Ljava/lang/Object;Z)V

    .line 180
    invoke-virtual {p0}, Lcom/google/android/location/places/ui/k;->a()Landroid/widget/ListView;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/location/places/ui/k;->G:Landroid/view/View;

    invoke-virtual {v0, v1, v4, v3}, Landroid/widget/ListView;->addHeaderView(Landroid/view/View;Ljava/lang/Object;Z)V

    .line 182
    invoke-virtual {p0}, Lcom/google/android/location/places/ui/k;->a()Landroid/widget/ListView;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/location/places/ui/k;->z:Landroid/view/View;

    invoke-virtual {v0, v1, v4, v3}, Landroid/widget/ListView;->addFooterView(Landroid/view/View;Ljava/lang/Object;Z)V

    .line 184
    invoke-virtual {p0}, Lcom/google/android/location/places/ui/k;->a()Landroid/widget/ListView;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/location/places/ui/k;->D:Landroid/view/View;

    invoke-virtual {v0, v1, v4, v3}, Landroid/widget/ListView;->addFooterView(Landroid/view/View;Ljava/lang/Object;Z)V

    .line 188
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/location/places/ui/k;->a()Landroid/widget/ListView;

    move-result-object v0

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOverScrollMode(I)V

    .line 190
    invoke-virtual {p0}, Lcom/google/android/location/places/ui/k;->a()Landroid/widget/ListView;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/widget/ListView;->setDivider(Landroid/graphics/drawable/Drawable;)V

    .line 192
    invoke-virtual {p0}, Lcom/google/android/location/places/ui/k;->a()Landroid/widget/ListView;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/widget/ListView;->setVerticalScrollBarEnabled(Z)V

    .line 194
    new-instance v0, Lcom/google/android/location/places/ui/p;

    invoke-virtual {p0}, Lcom/google/android/location/places/ui/k;->getActivity()Landroid/support/v4/app/q;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/google/android/location/places/ui/p;-><init>(Lcom/google/android/location/places/ui/k;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/location/places/ui/k;->K:Lcom/google/android/location/places/ui/p;

    .line 195
    iget-object v0, p0, Lcom/google/android/location/places/ui/k;->K:Lcom/google/android/location/places/ui/p;

    invoke-virtual {p0, v0}, Lcom/google/android/location/places/ui/k;->a(Landroid/widget/ListAdapter;)V

    .line 196
    invoke-virtual {p0}, Lcom/google/android/location/places/ui/k;->a()Landroid/widget/ListView;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/location/places/ui/k;->K:Lcom/google/android/location/places/ui/p;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 198
    iget-object v0, p0, Lcom/google/android/location/places/ui/k;->j:Landroid/view/View;

    new-instance v1, Lcom/google/android/location/places/ui/l;

    invoke-direct {v1, p0}, Lcom/google/android/location/places/ui/l;-><init>(Lcom/google/android/location/places/ui/k;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 209
    iget-object v0, p0, Lcom/google/android/location/places/ui/k;->B:Landroid/widget/Button;

    new-instance v1, Lcom/google/android/location/places/ui/m;

    invoke-direct {v1, p0}, Lcom/google/android/location/places/ui/m;-><init>(Lcom/google/android/location/places/ui/k;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 216
    iget-object v0, p0, Lcom/google/android/location/places/ui/k;->C:Landroid/widget/Button;

    new-instance v1, Lcom/google/android/location/places/ui/n;

    invoke-direct {v1, p0}, Lcom/google/android/location/places/ui/n;-><init>(Lcom/google/android/location/places/ui/k;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 222
    return-void
.end method

.method public final onDestroyView()V
    .locals 1

    .prologue
    .line 127
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/location/places/ui/k;->K:Lcom/google/android/location/places/ui/p;

    .line 128
    invoke-super {p0}, Landroid/support/v4/app/ar;->onDestroyView()V

    .line 129
    return-void
.end method
