.class public abstract Lcom/google/android/location/places/Subscription;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/os/Parcelable;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract a()Lcom/google/android/gms/location/places/internal/PlacesParams;
.end method

.method public final a(Landroid/content/pm/PackageManager;)Z
    .locals 2

    .prologue
    .line 51
    invoke-virtual {p0}, Lcom/google/android/location/places/Subscription;->c()Landroid/app/PendingIntent;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/google/android/location/n/ag;->a(Landroid/app/PendingIntent;Landroid/content/pm/PackageManager;)I

    move-result v0

    .line 53
    const/4 v1, 0x2

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public abstract b()Lcom/google/android/gms/location/places/PlaceFilter;
.end method

.method public abstract c()Landroid/app/PendingIntent;
.end method

.method public abstract d()Z
.end method

.method public final f()Z
    .locals 1

    .prologue
    .line 44
    invoke-virtual {p0}, Lcom/google/android/location/places/Subscription;->b()Lcom/google/android/gms/location/places/PlaceFilter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/location/places/PlaceFilter;->g()Z

    move-result v0

    return v0
.end method
