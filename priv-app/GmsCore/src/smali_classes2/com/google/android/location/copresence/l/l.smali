.class public final Lcom/google/android/location/copresence/l/l;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method public static a([Lcom/google/android/gms/location/copresence/internal/CopresenceNamedAcl;)[Lcom/google/ac/b/c/aw;
    .locals 7

    .prologue
    .line 27
    invoke-static {p0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 29
    array-length v1, p0

    .line 30
    new-array v2, v1, [Lcom/google/ac/b/c/aw;

    .line 31
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    .line 32
    aget-object v3, p0, v0

    .line 34
    new-instance v4, Lcom/google/ac/b/c/aw;

    invoke-direct {v4}, Lcom/google/ac/b/c/aw;-><init>()V

    .line 35
    new-instance v5, Lcom/google/ac/b/c/ax;

    invoke-direct {v5}, Lcom/google/ac/b/c/ax;-><init>()V

    iput-object v5, v4, Lcom/google/ac/b/c/aw;->a:Lcom/google/ac/b/c/ax;

    .line 36
    iget-object v5, v4, Lcom/google/ac/b/c/aw;->a:Lcom/google/ac/b/c/ax;

    invoke-virtual {v3}, Lcom/google/android/gms/location/copresence/internal/CopresenceNamedAcl;->a()Ljava/lang/String;

    move-result-object v6

    iput-object v6, v5, Lcom/google/ac/b/c/ax;->a:Ljava/lang/String;

    .line 37
    invoke-virtual {v3}, Lcom/google/android/gms/location/copresence/internal/CopresenceNamedAcl;->b()Lcom/google/android/gms/common/people/data/Audience;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/location/copresence/l/a;->a(Lcom/google/android/gms/common/people/data/Audience;)Lcom/google/ac/b/c/dm;

    move-result-object v3

    iput-object v3, v4, Lcom/google/ac/b/c/aw;->b:Lcom/google/ac/b/c/dm;

    .line 39
    aput-object v4, v2, v0

    .line 31
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 41
    :cond_0
    return-object v2
.end method

.method public static a([Lcom/google/ac/b/c/aw;)[Lcom/google/android/gms/location/copresence/internal/CopresenceNamedAcl;
    .locals 6

    .prologue
    .line 46
    if-nez p0, :cond_1

    .line 47
    const/4 v0, 0x0

    .line 59
    :cond_0
    return-object v0

    .line 50
    :cond_1
    array-length v2, p0

    .line 51
    new-array v0, v2, [Lcom/google/android/gms/location/copresence/internal/CopresenceNamedAcl;

    .line 52
    const/4 v1, 0x0

    :goto_0
    if-ge v1, v2, :cond_0

    .line 53
    aget-object v3, p0, v1

    .line 55
    new-instance v4, Lcom/google/android/gms/location/copresence/internal/CopresenceNamedAcl;

    iget-object v5, v3, Lcom/google/ac/b/c/aw;->a:Lcom/google/ac/b/c/ax;

    iget-object v5, v5, Lcom/google/ac/b/c/ax;->a:Ljava/lang/String;

    iget-object v3, v3, Lcom/google/ac/b/c/aw;->b:Lcom/google/ac/b/c/dm;

    invoke-static {v3}, Lcom/google/android/location/copresence/l/a;->a(Lcom/google/ac/b/c/dm;)Lcom/google/android/gms/common/people/data/Audience;

    move-result-object v3

    invoke-direct {v4, v5, v3}, Lcom/google/android/gms/location/copresence/internal/CopresenceNamedAcl;-><init>(Ljava/lang/String;Lcom/google/android/gms/common/people/data/Audience;)V

    aput-object v4, v0, v1

    .line 52
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method
