.class public final Lcom/google/android/location/copresence/b/a;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static c:Ljava/util/Map;


# instance fields
.field protected a:Ljava/lang/String;

.field protected b:Landroid/content/Context;

.field private d:Ljava/util/List;

.field private e:Ljava/util/List;

.field private f:Ljava/util/List;

.field private g:Ljava/util/Map;

.field private h:Ljava/util/Map;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 100
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/google/android/location/copresence/b/a;->c:Ljava/util/Map;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 137
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 138
    iput-object p1, p0, Lcom/google/android/location/copresence/b/a;->a:Ljava/lang/String;

    .line 139
    invoke-static {}, Lcom/google/android/gms/common/app/GmsApplication;->b()Lcom/google/android/gms/common/app/GmsApplication;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/copresence/b/a;->b:Landroid/content/Context;

    .line 141
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/location/copresence/b/a;->d:Ljava/util/List;

    .line 142
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/location/copresence/b/a;->e:Ljava/util/List;

    .line 143
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/location/copresence/b/a;->f:Ljava/util/List;

    .line 145
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/location/copresence/b/a;->g:Ljava/util/Map;

    .line 146
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/location/copresence/b/a;->h:Ljava/util/Map;

    .line 147
    return-void
.end method

.method private a(Lcom/google/protobuf/nano/j;)I
    .locals 2

    .prologue
    .line 658
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/google/android/location/copresence/b/a;->d:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_1

    .line 659
    iget-object v1, p0, Lcom/google/android/location/copresence/b/a;->d:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    if-ne v1, p1, :cond_0

    .line 663
    :goto_1
    return v0

    .line 658
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 663
    :cond_1
    const/4 v0, -0x1

    goto :goto_1
.end method

.method private static a([Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 651
    if-nez p0, :cond_0

    .line 652
    const/4 v0, 0x0

    .line 654
    :goto_0
    return v0

    :cond_0
    array-length v0, p0

    goto :goto_0
.end method

.method private a(Ljava/lang/String;Lcom/google/protobuf/nano/j;Lcom/google/protobuf/nano/j;I)Lcom/google/ac/b/b/a/b;
    .locals 16

    .prologue
    .line 261
    new-instance v4, Lcom/google/ac/b/b/a/b;

    invoke-direct {v4}, Lcom/google/ac/b/b/a/b;-><init>()V

    .line 263
    new-instance v2, Lcom/google/ac/b/b/a/c;

    invoke-direct {v2}, Lcom/google/ac/b/b/a/c;-><init>()V

    iput-object v2, v4, Lcom/google/ac/b/b/a/b;->a:Lcom/google/ac/b/b/a/c;

    .line 264
    iget-object v2, v4, Lcom/google/ac/b/b/a/b;->a:Lcom/google/ac/b/b/a/c;

    new-instance v3, Lcom/google/ac/b/b/a/e;

    invoke-direct {v3}, Lcom/google/ac/b/b/a/e;-><init>()V

    iput-object v3, v2, Lcom/google/ac/b/b/a/c;->b:Lcom/google/ac/b/b/a/e;

    .line 266
    move-object/from16 v0, p2

    instance-of v2, v0, Lcom/google/ac/b/c/cz;

    if-eqz v2, :cond_12

    .line 267
    iget-object v2, v4, Lcom/google/ac/b/b/a/b;->a:Lcom/google/ac/b/b/a/c;

    iget-object v2, v2, Lcom/google/ac/b/b/a/c;->b:Lcom/google/ac/b/b/a/e;

    const/4 v3, 0x1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    iput-object v3, v2, Lcom/google/ac/b/b/a/e;->a:Ljava/lang/Integer;

    .line 269
    iget-object v2, v4, Lcom/google/ac/b/b/a/b;->a:Lcom/google/ac/b/b/a/c;

    iget-object v2, v2, Lcom/google/ac/b/b/a/c;->b:Lcom/google/ac/b/b/a/e;

    new-instance v3, Lcom/google/ac/b/b/a/z;

    invoke-direct {v3}, Lcom/google/ac/b/b/a/z;-><init>()V

    iput-object v3, v2, Lcom/google/ac/b/b/a/e;->d:Lcom/google/ac/b/b/a/z;

    .line 270
    iget-object v2, v4, Lcom/google/ac/b/b/a/b;->a:Lcom/google/ac/b/b/a/c;

    iget-object v2, v2, Lcom/google/ac/b/b/a/c;->b:Lcom/google/ac/b/b/a/e;

    iget-object v2, v2, Lcom/google/ac/b/b/a/e;->d:Lcom/google/ac/b/b/a/z;

    new-instance v3, Lcom/google/ac/b/b/a/aa;

    invoke-direct {v3}, Lcom/google/ac/b/b/a/aa;-><init>()V

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/location/copresence/b/a;->b:Landroid/content/Context;

    const-string v6, "copresence_gcm_pref"

    const/4 v7, 0x0

    invoke-virtual {v5, v6, v7}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v5

    const-string v6, "copresence_uuid"

    const/4 v7, 0x0

    invoke-interface {v5, v6, v7}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/location/copresence/b/a;->a:Ljava/lang/String;

    if-nez v6, :cond_0

    iput-object v5, v3, Lcom/google/ac/b/b/a/aa;->e:Ljava/lang/String;

    :cond_0
    if-eqz v5, :cond_1

    const/4 v6, 0x0

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v7

    const/4 v8, 0x3

    invoke-static {v7, v8}, Ljava/lang/Math;->min(II)I

    move-result v7

    invoke-virtual {v5, v6, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v3, Lcom/google/ac/b/b/a/aa;->f:Ljava/lang/String;

    :cond_1
    new-instance v5, Lcom/google/ac/b/b/a/i;

    invoke-direct {v5}, Lcom/google/ac/b/b/a/i;-><init>()V

    iput-object v5, v3, Lcom/google/ac/b/b/a/aa;->c:Lcom/google/ac/b/b/a/i;

    iget-object v5, v3, Lcom/google/ac/b/b/a/aa;->c:Lcom/google/ac/b/b/a/i;

    const-string v6, "com.google.android.gms"

    iput-object v6, v5, Lcom/google/ac/b/b/a/i;->a:Ljava/lang/String;

    iget-object v5, v3, Lcom/google/ac/b/b/a/aa;->c:Lcom/google/ac/b/b/a/i;

    invoke-static {}, Lcom/google/android/gms/common/util/ay;->b()I

    move-result v6

    int-to-long v6, v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    iput-object v6, v5, Lcom/google/ac/b/b/a/i;->c:Ljava/lang/Long;

    iget-object v5, v3, Lcom/google/ac/b/b/a/aa;->c:Lcom/google/ac/b/b/a/i;

    invoke-static {}, Lcom/google/android/gms/common/util/ay;->a()Ljava/lang/String;

    move-result-object v6

    iput-object v6, v5, Lcom/google/ac/b/b/a/i;->b:Ljava/lang/String;

    if-eqz p1, :cond_2

    invoke-direct/range {p0 .. p1}, Lcom/google/android/location/copresence/b/a;->c(Ljava/lang/String;)Lcom/google/ac/b/b/a/i;

    move-result-object v5

    iput-object v5, v3, Lcom/google/ac/b/b/a/aa;->b:Lcom/google/ac/b/b/a/i;

    :cond_2
    new-instance v5, Lcom/google/ac/b/b/a/k;

    invoke-direct {v5}, Lcom/google/ac/b/b/a/k;-><init>()V

    iput-object v5, v3, Lcom/google/ac/b/b/a/aa;->g:Lcom/google/ac/b/b/a/k;

    iget-object v5, v3, Lcom/google/ac/b/b/a/aa;->g:Lcom/google/ac/b/b/a/k;

    const/4 v6, 0x6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    iput-object v6, v5, Lcom/google/ac/b/b/a/k;->c:Ljava/lang/Integer;

    iget-object v5, v3, Lcom/google/ac/b/b/a/aa;->g:Lcom/google/ac/b/b/a/k;

    sget-object v6, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    iput-object v6, v5, Lcom/google/ac/b/b/a/k;->a:Ljava/lang/String;

    iget-object v5, v3, Lcom/google/ac/b/b/a/aa;->g:Lcom/google/ac/b/b/a/k;

    sget-object v6, Landroid/os/Build;->MODEL:Ljava/lang/String;

    iput-object v6, v5, Lcom/google/ac/b/b/a/k;->b:Ljava/lang/String;

    iget-object v5, v3, Lcom/google/ac/b/b/a/aa;->g:Lcom/google/ac/b/b/a/k;

    sget-object v6, Landroid/os/Build$VERSION;->RELEASE:Ljava/lang/String;

    iput-object v6, v5, Lcom/google/ac/b/b/a/k;->d:Ljava/lang/String;

    iput-object v3, v2, Lcom/google/ac/b/b/a/z;->a:Lcom/google/ac/b/b/a/aa;

    .line 271
    iget-object v2, v4, Lcom/google/ac/b/b/a/b;->a:Lcom/google/ac/b/b/a/c;

    iget-object v2, v2, Lcom/google/ac/b/b/a/c;->b:Lcom/google/ac/b/b/a/e;

    iget-object v5, v2, Lcom/google/ac/b/b/a/e;->d:Lcom/google/ac/b/b/a/z;

    move-object/from16 v2, p2

    check-cast v2, Lcom/google/ac/b/c/cz;

    new-instance v6, Lcom/google/ac/b/b/a/x;

    invoke-direct {v6}, Lcom/google/ac/b/b/a/x;-><init>()V

    iget-object v3, v2, Lcom/google/ac/b/c/cz;->b:Lcom/google/ac/b/c/ct;

    if-eqz v3, :cond_4

    new-instance v7, Lcom/google/ac/b/b/a/m;

    invoke-direct {v7}, Lcom/google/ac/b/b/a/m;-><init>()V

    iput-object v7, v6, Lcom/google/ac/b/b/a/x;->b:Lcom/google/ac/b/b/a/m;

    iget-object v8, v2, Lcom/google/ac/b/c/cz;->b:Lcom/google/ac/b/c/ct;

    iget-object v3, v8, Lcom/google/ac/b/c/ct;->b:[Ljava/lang/String;

    iput-object v3, v7, Lcom/google/ac/b/b/a/m;->b:[Ljava/lang/String;

    iget-object v3, v8, Lcom/google/ac/b/c/ct;->a:[Lcom/google/ac/b/c/bf;

    invoke-static {v3}, Lcom/google/android/location/copresence/b/a;->a([Ljava/lang/Object;)I

    move-result v9

    if-lez v9, :cond_4

    new-array v3, v9, [Lcom/google/ac/b/b/a/v;

    iput-object v3, v7, Lcom/google/ac/b/b/a/m;->a:[Lcom/google/ac/b/b/a/v;

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v9, :cond_4

    new-instance v10, Lcom/google/ac/b/b/a/v;

    invoke-direct {v10}, Lcom/google/ac/b/b/a/v;-><init>()V

    iget-object v11, v8, Lcom/google/ac/b/c/ct;->a:[Lcom/google/ac/b/c/bf;

    aget-object v11, v11, v3

    iget-object v12, v7, Lcom/google/ac/b/b/a/m;->a:[Lcom/google/ac/b/b/a/v;

    aput-object v10, v12, v3

    iget-object v12, v11, Lcom/google/ac/b/c/bf;->a:Ljava/lang/String;

    iput-object v12, v10, Lcom/google/ac/b/b/a/v;->a:Ljava/lang/String;

    iget-object v12, v11, Lcom/google/ac/b/c/bf;->d:Ljava/lang/Integer;

    iput-object v12, v10, Lcom/google/ac/b/b/a/v;->d:Ljava/lang/Integer;

    iget-object v12, v11, Lcom/google/ac/b/c/bf;->b:Lcom/google/ac/b/c/t;

    if-eqz v12, :cond_3

    new-instance v12, Lcom/google/ac/b/b/a/g;

    invoke-direct {v12}, Lcom/google/ac/b/b/a/g;-><init>()V

    iput-object v12, v10, Lcom/google/ac/b/b/a/v;->b:Lcom/google/ac/b/b/a/g;

    iget-object v12, v10, Lcom/google/ac/b/b/a/v;->b:Lcom/google/ac/b/b/a/g;

    iget-object v13, v11, Lcom/google/ac/b/c/bf;->b:Lcom/google/ac/b/c/t;

    iget-object v13, v13, Lcom/google/ac/b/c/t;->a:Ljava/lang/Long;

    iput-object v13, v12, Lcom/google/ac/b/b/a/g;->a:Ljava/lang/Long;

    iget-object v12, v11, Lcom/google/ac/b/c/bf;->b:Lcom/google/ac/b/c/t;

    iget-object v12, v12, Lcom/google/ac/b/c/t;->b:Lcom/google/ac/b/c/u;

    if-eqz v12, :cond_3

    iget-object v12, v10, Lcom/google/ac/b/b/a/v;->b:Lcom/google/ac/b/b/a/g;

    new-instance v13, Lcom/google/ac/b/b/a/h;

    invoke-direct {v13}, Lcom/google/ac/b/b/a/h;-><init>()V

    iput-object v13, v12, Lcom/google/ac/b/b/a/g;->b:Lcom/google/ac/b/b/a/h;

    iget-object v12, v10, Lcom/google/ac/b/b/a/v;->b:Lcom/google/ac/b/b/a/g;

    iget-object v12, v12, Lcom/google/ac/b/b/a/g;->b:Lcom/google/ac/b/b/a/h;

    iget-object v13, v11, Lcom/google/ac/b/c/bf;->b:Lcom/google/ac/b/c/t;

    iget-object v13, v13, Lcom/google/ac/b/c/t;->b:Lcom/google/ac/b/c/u;

    iget-object v13, v13, Lcom/google/ac/b/c/u;->a:Ljava/lang/Integer;

    iput-object v13, v12, Lcom/google/ac/b/b/a/h;->a:Ljava/lang/Integer;

    iget-object v12, v10, Lcom/google/ac/b/b/a/v;->b:Lcom/google/ac/b/b/a/g;

    iget-object v12, v12, Lcom/google/ac/b/b/a/g;->b:Lcom/google/ac/b/b/a/h;

    iget-object v13, v11, Lcom/google/ac/b/c/bf;->b:Lcom/google/ac/b/c/t;

    iget-object v13, v13, Lcom/google/ac/b/c/t;->b:Lcom/google/ac/b/c/u;

    iget-object v13, v13, Lcom/google/ac/b/c/u;->b:Ljava/lang/String;

    iput-object v13, v12, Lcom/google/ac/b/b/a/h;->b:Ljava/lang/String;

    :cond_3
    iget-object v11, v11, Lcom/google/ac/b/c/bf;->c:Lcom/google/ac/b/c/at;

    invoke-static {v11}, Lcom/google/android/location/copresence/b/a;->a(Lcom/google/ac/b/c/at;)Lcom/google/ac/b/b/a/q;

    move-result-object v11

    iput-object v11, v10, Lcom/google/ac/b/b/a/v;->c:Lcom/google/ac/b/b/a/q;

    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_4
    iget-object v3, v2, Lcom/google/ac/b/c/cz;->c:Lcom/google/ac/b/c/cv;

    if-eqz v3, :cond_5

    new-instance v7, Lcom/google/ac/b/b/a/o;

    invoke-direct {v7}, Lcom/google/ac/b/b/a/o;-><init>()V

    iput-object v7, v6, Lcom/google/ac/b/b/a/x;->c:Lcom/google/ac/b/b/a/o;

    iget-object v8, v2, Lcom/google/ac/b/c/cz;->c:Lcom/google/ac/b/c/cv;

    iget-object v3, v8, Lcom/google/ac/b/c/cv;->b:[Ljava/lang/String;

    iput-object v3, v7, Lcom/google/ac/b/b/a/o;->b:[Ljava/lang/String;

    iget-object v3, v8, Lcom/google/ac/b/c/cv;->a:[Lcom/google/ac/b/c/bo;

    invoke-static {v3}, Lcom/google/android/location/copresence/b/a;->a([Ljava/lang/Object;)I

    move-result v9

    if-lez v9, :cond_5

    new-array v3, v9, [Lcom/google/ac/b/b/a/ag;

    iput-object v3, v7, Lcom/google/ac/b/b/a/o;->a:[Lcom/google/ac/b/b/a/ag;

    const/4 v3, 0x0

    :goto_1
    if-ge v3, v9, :cond_5

    new-instance v10, Lcom/google/ac/b/b/a/ag;

    invoke-direct {v10}, Lcom/google/ac/b/b/a/ag;-><init>()V

    iget-object v11, v8, Lcom/google/ac/b/c/cv;->a:[Lcom/google/ac/b/c/bo;

    aget-object v11, v11, v3

    iget-object v12, v7, Lcom/google/ac/b/b/a/o;->a:[Lcom/google/ac/b/b/a/ag;

    aput-object v10, v12, v3

    iget-object v12, v11, Lcom/google/ac/b/c/bo;->a:Ljava/lang/String;

    iput-object v12, v10, Lcom/google/ac/b/b/a/ag;->a:Ljava/lang/String;

    iget-object v12, v11, Lcom/google/ac/b/c/bo;->b:Ljava/lang/Integer;

    iput-object v12, v10, Lcom/google/ac/b/b/a/ag;->b:Ljava/lang/Integer;

    iget-object v12, v11, Lcom/google/ac/b/c/bo;->c:Ljava/lang/Long;

    iput-object v12, v10, Lcom/google/ac/b/b/a/ag;->c:Ljava/lang/Long;

    iget-object v11, v11, Lcom/google/ac/b/c/bo;->d:Lcom/google/ac/b/c/av;

    invoke-static {v11}, Lcom/google/android/location/copresence/b/a;->a(Lcom/google/ac/b/c/av;)Lcom/google/ac/b/b/a/s;

    move-result-object v11

    iput-object v11, v10, Lcom/google/ac/b/b/a/ag;->d:Lcom/google/ac/b/b/a/s;

    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    :cond_5
    iget-object v3, v2, Lcom/google/ac/b/c/cz;->d:Lcom/google/ac/b/c/df;

    if-eqz v3, :cond_7

    new-instance v7, Lcom/google/ac/b/b/a/an;

    invoke-direct {v7}, Lcom/google/ac/b/b/a/an;-><init>()V

    iput-object v7, v6, Lcom/google/ac/b/b/a/x;->d:Lcom/google/ac/b/b/a/an;

    iget-object v8, v2, Lcom/google/ac/b/c/cz;->d:Lcom/google/ac/b/c/df;

    iget-object v2, v8, Lcom/google/ac/b/c/df;->a:[Lcom/google/ac/b/c/bu;

    invoke-static {v2}, Lcom/google/android/location/copresence/b/a;->a([Ljava/lang/Object;)I

    move-result v9

    if-lez v9, :cond_7

    new-array v2, v9, [Lcom/google/ac/b/b/a/ak;

    iput-object v2, v7, Lcom/google/ac/b/b/a/an;->a:[Lcom/google/ac/b/b/a/ak;

    const/4 v2, 0x0

    move v3, v2

    :goto_2
    if-ge v3, v9, :cond_7

    new-instance v10, Lcom/google/ac/b/b/a/ak;

    invoke-direct {v10}, Lcom/google/ac/b/b/a/ak;-><init>()V

    iget-object v2, v8, Lcom/google/ac/b/c/df;->a:[Lcom/google/ac/b/c/bu;

    aget-object v11, v2, v3

    iget-object v2, v7, Lcom/google/ac/b/b/a/an;->a:[Lcom/google/ac/b/b/a/ak;

    aput-object v10, v2, v3

    iget-object v2, v11, Lcom/google/ac/b/c/bu;->b:[Lcom/google/ac/b/c/bv;

    invoke-static {v2}, Lcom/google/android/location/copresence/b/a;->a([Ljava/lang/Object;)I

    move-result v12

    if-lez v12, :cond_6

    new-array v2, v12, [Lcom/google/ac/b/b/a/al;

    iput-object v2, v10, Lcom/google/ac/b/b/a/ak;->a:[Lcom/google/ac/b/b/a/al;

    const/4 v2, 0x0

    :goto_3
    if-ge v2, v12, :cond_6

    new-instance v13, Lcom/google/ac/b/b/a/al;

    invoke-direct {v13}, Lcom/google/ac/b/b/a/al;-><init>()V

    iget-object v14, v11, Lcom/google/ac/b/c/bu;->b:[Lcom/google/ac/b/c/bv;

    aget-object v14, v14, v2

    iget-object v15, v10, Lcom/google/ac/b/b/a/ak;->a:[Lcom/google/ac/b/b/a/al;

    aput-object v13, v15, v2

    iget-object v15, v14, Lcom/google/ac/b/c/bv;->a:Ljava/lang/Integer;

    iput-object v15, v13, Lcom/google/ac/b/b/a/al;->a:Ljava/lang/Integer;

    iget-object v14, v14, Lcom/google/ac/b/c/bv;->b:Ljava/lang/Integer;

    iput-object v14, v13, Lcom/google/ac/b/b/a/al;->b:Ljava/lang/Integer;

    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    :cond_6
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_2

    :cond_7
    iput-object v6, v5, Lcom/google/ac/b/b/a/z;->d:Lcom/google/ac/b/b/a/x;

    .line 274
    if-eqz p3, :cond_11

    .line 275
    iget-object v2, v4, Lcom/google/ac/b/b/a/b;->a:Lcom/google/ac/b/b/a/c;

    iget-object v2, v2, Lcom/google/ac/b/b/a/c;->b:Lcom/google/ac/b/b/a/e;

    new-instance v3, Lcom/google/ac/b/b/a/ab;

    invoke-direct {v3}, Lcom/google/ac/b/b/a/ab;-><init>()V

    iput-object v3, v2, Lcom/google/ac/b/b/a/e;->e:Lcom/google/ac/b/b/a/ab;

    .line 276
    iget-object v2, v4, Lcom/google/ac/b/b/a/b;->a:Lcom/google/ac/b/b/a/c;

    iget-object v2, v2, Lcom/google/ac/b/b/a/c;->b:Lcom/google/ac/b/b/a/e;

    iget-object v3, v2, Lcom/google/ac/b/b/a/e;->e:Lcom/google/ac/b/b/a/ab;

    move-object/from16 v2, p3

    check-cast v2, Lcom/google/ac/b/c/da;

    iget-object v5, v2, Lcom/google/ac/b/c/da;->a:Lcom/google/ac/b/c/dc;

    if-nez v5, :cond_9

    const/4 v2, 0x0

    :cond_8
    :goto_4
    iput-object v2, v3, Lcom/google/ac/b/b/a/ab;->a:Lcom/google/ac/b/b/a/ac;

    .line 278
    iget-object v2, v4, Lcom/google/ac/b/b/a/b;->a:Lcom/google/ac/b/b/a/c;

    iget-object v2, v2, Lcom/google/ac/b/b/a/c;->b:Lcom/google/ac/b/b/a/e;

    iget-object v5, v2, Lcom/google/ac/b/b/a/e;->e:Lcom/google/ac/b/b/a/ab;

    check-cast p3, Lcom/google/ac/b/c/da;

    new-instance v6, Lcom/google/ac/b/b/a/y;

    invoke-direct {v6}, Lcom/google/ac/b/b/a/y;-><init>()V

    move-object/from16 v0, p3

    iget-object v2, v0, Lcom/google/ac/b/c/da;->b:Lcom/google/ac/b/c/cu;

    if-eqz v2, :cond_a

    new-instance v3, Lcom/google/ac/b/b/a/n;

    invoke-direct {v3}, Lcom/google/ac/b/b/a/n;-><init>()V

    iput-object v3, v6, Lcom/google/ac/b/b/a/y;->a:Lcom/google/ac/b/b/a/n;

    move-object/from16 v0, p3

    iget-object v7, v0, Lcom/google/ac/b/c/da;->b:Lcom/google/ac/b/c/cu;

    iget-object v2, v7, Lcom/google/ac/b/c/cu;->a:Ljava/lang/Integer;

    iput-object v2, v3, Lcom/google/ac/b/b/a/n;->a:Ljava/lang/Integer;

    iget-object v2, v7, Lcom/google/ac/b/c/cu;->b:[Lcom/google/ac/b/c/au;

    invoke-static {v2}, Lcom/google/android/location/copresence/b/a;->a([Ljava/lang/Object;)I

    move-result v8

    if-lez v8, :cond_a

    new-array v2, v8, [Lcom/google/ac/b/b/a/r;

    iput-object v2, v3, Lcom/google/ac/b/b/a/n;->b:[Lcom/google/ac/b/b/a/r;

    const/4 v2, 0x0

    :goto_5
    if-ge v2, v8, :cond_a

    new-instance v9, Lcom/google/ac/b/b/a/r;

    invoke-direct {v9}, Lcom/google/ac/b/b/a/r;-><init>()V

    iget-object v10, v7, Lcom/google/ac/b/c/cu;->b:[Lcom/google/ac/b/c/au;

    aget-object v10, v10, v2

    iget-object v11, v3, Lcom/google/ac/b/b/a/n;->b:[Lcom/google/ac/b/b/a/r;

    aput-object v9, v11, v2

    iget-object v10, v10, Lcom/google/ac/b/c/au;->a:Ljava/lang/String;

    iput-object v10, v9, Lcom/google/ac/b/b/a/r;->a:Ljava/lang/String;

    add-int/lit8 v2, v2, 0x1

    goto :goto_5

    .line 276
    :cond_9
    new-instance v2, Lcom/google/ac/b/b/a/ac;

    invoke-direct {v2}, Lcom/google/ac/b/b/a/ac;-><init>()V

    iget-object v6, v5, Lcom/google/ac/b/c/dc;->c:Lcom/google/ac/b/c/bl;

    if-eqz v6, :cond_8

    new-instance v6, Lcom/google/ac/b/b/a/ae;

    invoke-direct {v6}, Lcom/google/ac/b/b/a/ae;-><init>()V

    iput-object v6, v2, Lcom/google/ac/b/b/a/ac;->a:Lcom/google/ac/b/b/a/ae;

    iget-object v6, v2, Lcom/google/ac/b/b/a/ac;->a:Lcom/google/ac/b/b/a/ae;

    iget-object v5, v5, Lcom/google/ac/b/c/dc;->c:Lcom/google/ac/b/c/bl;

    iget-object v5, v5, Lcom/google/ac/b/c/bl;->a:Ljava/lang/Integer;

    iput-object v5, v6, Lcom/google/ac/b/b/a/ae;->a:Ljava/lang/Integer;

    goto :goto_4

    .line 278
    :cond_a
    move-object/from16 v0, p3

    iget-object v2, v0, Lcom/google/ac/b/c/da;->c:Lcom/google/ac/b/c/cw;

    if-eqz v2, :cond_b

    new-instance v3, Lcom/google/ac/b/b/a/p;

    invoke-direct {v3}, Lcom/google/ac/b/b/a/p;-><init>()V

    iput-object v3, v6, Lcom/google/ac/b/b/a/y;->b:Lcom/google/ac/b/b/a/p;

    move-object/from16 v0, p3

    iget-object v7, v0, Lcom/google/ac/b/c/da;->c:Lcom/google/ac/b/c/cw;

    iget-object v2, v7, Lcom/google/ac/b/c/cw;->a:Ljava/lang/Integer;

    iput-object v2, v3, Lcom/google/ac/b/b/a/p;->a:Ljava/lang/Integer;

    iget-object v2, v7, Lcom/google/ac/b/c/cw;->b:[Lcom/google/ac/b/c/bp;

    invoke-static {v2}, Lcom/google/android/location/copresence/b/a;->a([Ljava/lang/Object;)I

    move-result v8

    if-lez v8, :cond_b

    new-array v2, v8, [Lcom/google/ac/b/b/a/ah;

    iput-object v2, v3, Lcom/google/ac/b/b/a/p;->b:[Lcom/google/ac/b/b/a/ah;

    const/4 v2, 0x0

    :goto_6
    if-ge v2, v8, :cond_b

    new-instance v9, Lcom/google/ac/b/b/a/ah;

    invoke-direct {v9}, Lcom/google/ac/b/b/a/ah;-><init>()V

    iget-object v10, v7, Lcom/google/ac/b/c/cw;->b:[Lcom/google/ac/b/c/bp;

    aget-object v10, v10, v2

    iget-object v11, v3, Lcom/google/ac/b/b/a/p;->b:[Lcom/google/ac/b/b/a/ah;

    aput-object v9, v11, v2

    iget-object v10, v10, Lcom/google/ac/b/c/bp;->a:Ljava/lang/String;

    iput-object v10, v9, Lcom/google/ac/b/b/a/ah;->a:Ljava/lang/String;

    add-int/lit8 v2, v2, 0x1

    goto :goto_6

    :cond_b
    move-object/from16 v0, p3

    iget-object v2, v0, Lcom/google/ac/b/c/da;->d:Lcom/google/ac/b/c/dg;

    if-eqz v2, :cond_10

    new-instance v7, Lcom/google/ac/b/b/a/ao;

    invoke-direct {v7}, Lcom/google/ac/b/b/a/ao;-><init>()V

    iput-object v7, v6, Lcom/google/ac/b/b/a/y;->c:Lcom/google/ac/b/b/a/ao;

    move-object/from16 v0, p3

    iget-object v8, v0, Lcom/google/ac/b/c/da;->d:Lcom/google/ac/b/c/dg;

    iget-object v2, v8, Lcom/google/ac/b/c/dg;->a:Ljava/lang/Integer;

    iput-object v2, v7, Lcom/google/ac/b/b/a/ao;->a:Ljava/lang/Integer;

    iget-object v2, v8, Lcom/google/ac/b/c/dg;->b:[Lcom/google/ac/b/c/bq;

    invoke-static {v2}, Lcom/google/android/location/copresence/b/a;->a([Ljava/lang/Object;)I

    move-result v3

    if-lez v3, :cond_c

    new-array v2, v3, [Lcom/google/ac/b/b/a/ai;

    iput-object v2, v7, Lcom/google/ac/b/b/a/ao;->b:[Lcom/google/ac/b/b/a/ai;

    const/4 v2, 0x0

    :goto_7
    if-ge v2, v3, :cond_c

    new-instance v9, Lcom/google/ac/b/b/a/ai;

    invoke-direct {v9}, Lcom/google/ac/b/b/a/ai;-><init>()V

    iget-object v10, v8, Lcom/google/ac/b/c/dg;->b:[Lcom/google/ac/b/c/bq;

    aget-object v10, v10, v2

    iget-object v11, v7, Lcom/google/ac/b/b/a/ao;->b:[Lcom/google/ac/b/b/a/ai;

    aput-object v9, v11, v2

    iget-object v11, v10, Lcom/google/ac/b/c/bq;->a:Ljava/lang/String;

    iput-object v11, v9, Lcom/google/ac/b/b/a/ai;->a:Ljava/lang/String;

    iget-object v10, v10, Lcom/google/ac/b/c/bq;->c:Ljava/lang/Integer;

    iput-object v10, v9, Lcom/google/ac/b/b/a/ai;->b:Ljava/lang/Integer;

    add-int/lit8 v2, v2, 0x1

    goto :goto_7

    :cond_c
    iget-object v2, v8, Lcom/google/ac/b/c/dg;->c:[Lcom/google/ac/b/c/bm;

    invoke-static {v2}, Lcom/google/android/location/copresence/b/a;->a([Ljava/lang/Object;)I

    move-result v9

    if-lez v9, :cond_e

    new-array v2, v9, [Lcom/google/ac/b/b/a/af;

    iput-object v2, v7, Lcom/google/ac/b/b/a/ao;->c:[Lcom/google/ac/b/b/a/af;

    const/4 v2, 0x0

    move v3, v2

    :goto_8
    if-ge v3, v9, :cond_e

    new-instance v10, Lcom/google/ac/b/b/a/af;

    invoke-direct {v10}, Lcom/google/ac/b/b/a/af;-><init>()V

    iget-object v2, v8, Lcom/google/ac/b/c/dg;->c:[Lcom/google/ac/b/c/bm;

    aget-object v11, v2, v3

    iget-object v2, v7, Lcom/google/ac/b/b/a/ao;->c:[Lcom/google/ac/b/b/a/af;

    aput-object v10, v2, v3

    iget-object v2, v11, Lcom/google/ac/b/c/bm;->b:Lcom/google/ac/b/c/at;

    invoke-static {v2}, Lcom/google/android/location/copresence/b/a;->a(Lcom/google/ac/b/c/at;)Lcom/google/ac/b/b/a/q;

    move-result-object v2

    iput-object v2, v10, Lcom/google/ac/b/b/a/af;->b:Lcom/google/ac/b/b/a/q;

    iget-object v2, v11, Lcom/google/ac/b/c/bm;->a:[Ljava/lang/String;

    invoke-static {v2}, Lcom/google/android/location/copresence/b/a;->a([Ljava/lang/Object;)I

    move-result v12

    if-lez v12, :cond_d

    new-array v2, v12, [Ljava/lang/String;

    iput-object v2, v10, Lcom/google/ac/b/b/a/af;->a:[Ljava/lang/String;

    const/4 v2, 0x0

    :goto_9
    if-ge v2, v12, :cond_d

    iget-object v13, v10, Lcom/google/ac/b/b/a/af;->a:[Ljava/lang/String;

    iget-object v14, v11, Lcom/google/ac/b/c/bm;->a:[Ljava/lang/String;

    aget-object v14, v14, v2

    aput-object v14, v13, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_9

    :cond_d
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_8

    :cond_e
    iget-object v2, v8, Lcom/google/ac/b/c/dg;->d:[Lcom/google/ac/b/c/ah;

    invoke-static {v2}, Lcom/google/android/location/copresence/b/a;->a([Ljava/lang/Object;)I

    move-result v3

    if-lez v3, :cond_10

    new-array v2, v3, [Lcom/google/ac/b/b/a/l;

    iput-object v2, v7, Lcom/google/ac/b/b/a/ao;->d:[Lcom/google/ac/b/b/a/l;

    const/4 v2, 0x0

    :goto_a
    if-ge v2, v3, :cond_10

    new-instance v9, Lcom/google/ac/b/b/a/l;

    invoke-direct {v9}, Lcom/google/ac/b/b/a/l;-><init>()V

    iget-object v10, v8, Lcom/google/ac/b/c/dg;->d:[Lcom/google/ac/b/c/ah;

    aget-object v10, v10, v2

    iget-object v11, v7, Lcom/google/ac/b/b/a/ao;->d:[Lcom/google/ac/b/b/a/l;

    aput-object v9, v11, v2

    iget-object v11, v10, Lcom/google/ac/b/c/ah;->a:Ljava/lang/Integer;

    iput-object v11, v9, Lcom/google/ac/b/b/a/l;->a:Ljava/lang/Integer;

    iget-object v11, v10, Lcom/google/ac/b/c/ah;->f:Ljava/lang/String;

    iput-object v11, v9, Lcom/google/ac/b/b/a/l;->d:Ljava/lang/String;

    iget-object v11, v10, Lcom/google/ac/b/c/ah;->e:Ljava/lang/String;

    iput-object v11, v9, Lcom/google/ac/b/b/a/l;->c:Ljava/lang/String;

    iget-object v11, v10, Lcom/google/ac/b/c/ah;->b:Lcom/google/ac/b/c/bt;

    if-eqz v11, :cond_f

    new-instance v11, Lcom/google/ac/b/b/a/aj;

    invoke-direct {v11}, Lcom/google/ac/b/b/a/aj;-><init>()V

    iput-object v11, v9, Lcom/google/ac/b/b/a/l;->b:Lcom/google/ac/b/b/a/aj;

    iget-object v9, v10, Lcom/google/ac/b/c/ah;->b:Lcom/google/ac/b/c/bt;

    iget-object v10, v9, Lcom/google/ac/b/c/bt;->a:Ljava/lang/Integer;

    iput-object v10, v11, Lcom/google/ac/b/b/a/aj;->a:Ljava/lang/Integer;

    iget-object v10, v9, Lcom/google/ac/b/c/bt;->b:Ljava/lang/Integer;

    iput-object v10, v11, Lcom/google/ac/b/b/a/aj;->b:Ljava/lang/Integer;

    iget-object v9, v9, Lcom/google/ac/b/c/bt;->c:Ljava/lang/String;

    iput-object v9, v11, Lcom/google/ac/b/b/a/aj;->c:Ljava/lang/String;

    :cond_f
    add-int/lit8 v2, v2, 0x1

    goto :goto_a

    :cond_10
    iput-object v6, v5, Lcom/google/ac/b/b/a/ab;->b:Lcom/google/ac/b/b/a/y;

    .line 285
    :cond_11
    iget-object v2, v4, Lcom/google/ac/b/b/a/b;->a:Lcom/google/ac/b/b/a/c;

    iget-object v5, v2, Lcom/google/ac/b/b/a/c;->b:Lcom/google/ac/b/b/a/e;

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-direct {v0, v1}, Lcom/google/android/location/copresence/b/a;->a(Lcom/google/protobuf/nano/j;)I

    move-result v2

    if-gez v2, :cond_13

    const-wide/16 v2, -0x1

    :goto_b
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    iput-object v2, v5, Lcom/google/ac/b/b/a/e;->c:Ljava/lang/Long;

    .line 286
    iget-object v2, v4, Lcom/google/ac/b/b/a/b;->a:Lcom/google/ac/b/b/a/c;

    iget-object v2, v2, Lcom/google/ac/b/b/a/c;->b:Lcom/google/ac/b/b/a/e;

    invoke-static/range {p4 .. p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    iput-object v3, v2, Lcom/google/ac/b/b/a/e;->b:Ljava/lang/Integer;

    move-object v2, v4

    .line 288
    :goto_c
    return-object v2

    .line 282
    :cond_12
    const/4 v2, 0x0

    goto :goto_c

    .line 285
    :cond_13
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v6

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/location/copresence/b/a;->f:Ljava/util/List;

    invoke-interface {v3, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    sub-long v2, v6, v2

    goto :goto_b
.end method

.method private static a(Lcom/google/ac/b/c/at;)Lcom/google/ac/b/b/a/q;
    .locals 2

    .prologue
    .line 633
    if-nez p0, :cond_0

    .line 634
    const/4 v0, 0x0

    .line 638
    :goto_0
    return-object v0

    .line 636
    :cond_0
    new-instance v0, Lcom/google/ac/b/b/a/q;

    invoke-direct {v0}, Lcom/google/ac/b/b/a/q;-><init>()V

    .line 637
    iget-object v1, p0, Lcom/google/ac/b/c/at;->a:Lcom/google/ac/b/c/av;

    invoke-static {v1}, Lcom/google/android/location/copresence/b/a;->a(Lcom/google/ac/b/c/av;)Lcom/google/ac/b/b/a/s;

    move-result-object v1

    iput-object v1, v0, Lcom/google/ac/b/b/a/q;->a:Lcom/google/ac/b/b/a/s;

    goto :goto_0
.end method

.method private static a(Lcom/google/ac/b/c/av;)Lcom/google/ac/b/b/a/s;
    .locals 2

    .prologue
    .line 642
    if-nez p0, :cond_0

    .line 643
    const/4 v0, 0x0

    .line 647
    :goto_0
    return-object v0

    .line 645
    :cond_0
    new-instance v0, Lcom/google/ac/b/b/a/s;

    invoke-direct {v0}, Lcom/google/ac/b/b/a/s;-><init>()V

    .line 646
    iget-object v1, p0, Lcom/google/ac/b/c/av;->b:Ljava/lang/String;

    iput-object v1, v0, Lcom/google/ac/b/b/a/s;->b:Ljava/lang/String;

    goto :goto_0
.end method

.method private static declared-synchronized a()Lcom/google/android/location/copresence/b/a;
    .locals 2

    .prologue
    .line 129
    const-class v1, Lcom/google/android/location/copresence/b/a;

    monitor-enter v1

    :try_start_0
    const-string v0, ""

    invoke-static {v0}, Lcom/google/android/location/copresence/b/a;->a(Ljava/lang/String;)Lcom/google/android/location/copresence/b/a;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private static declared-synchronized a(Ljava/lang/String;)Lcom/google/android/location/copresence/b/a;
    .locals 4

    .prologue
    .line 115
    const-class v2, Lcom/google/android/location/copresence/b/a;

    monitor-enter v2

    :try_start_0
    const-string v0, ""

    .line 116
    if-eqz p0, :cond_1

    move-object v1, p0

    .line 120
    :goto_0
    sget-object v0, Lcom/google/android/location/copresence/b/a;->c:Ljava/util/Map;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/copresence/b/a;

    .line 121
    if-nez v0, :cond_0

    .line 122
    new-instance v0, Lcom/google/android/location/copresence/b/a;

    invoke-direct {v0, p0}, Lcom/google/android/location/copresence/b/a;-><init>(Ljava/lang/String;)V

    .line 123
    sget-object v3, Lcom/google/android/location/copresence/b/a;->c:Ljava/util/Map;

    invoke-interface {v3, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 125
    :cond_0
    monitor-exit v2

    return-object v0

    .line 115
    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0

    :cond_1
    move-object v1, v0

    goto :goto_0
.end method

.method public static a(I)V
    .locals 2

    .prologue
    .line 210
    new-instance v0, Lcom/google/ac/b/b/a/d;

    invoke-direct {v0}, Lcom/google/ac/b/b/a/d;-><init>()V

    .line 211
    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, v0, Lcom/google/ac/b/b/a/d;->b:Ljava/lang/Integer;

    .line 212
    const-string v1, "CopresenceLogger: "

    invoke-static {v1, v0}, Lcom/google/android/location/copresence/b/a;->a(Ljava/lang/String;Lcom/google/ac/b/b/a/d;)V

    .line 213
    return-void
.end method

.method private declared-synchronized a(Lcom/google/protobuf/nano/j;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 685
    monitor-enter p0

    :goto_0
    :try_start_0
    iget-object v0, p0, Lcom/google/android/location/copresence/b/a;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    invoke-static {}, Lcom/google/android/location/copresence/f/b;->b()Lcom/google/ac/b/c/g;

    move-result-object v1

    iget-object v1, v1, Lcom/google/ac/b/c/g;->m:Lcom/google/ac/b/c/k;

    iget-object v1, v1, Lcom/google/ac/b/c/k;->c:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-le v0, v1, :cond_0

    .line 686
    iget-object v0, p0, Lcom/google/android/location/copresence/b/a;->d:Ljava/util/List;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protobuf/nano/j;

    .line 687
    iget-object v1, p0, Lcom/google/android/location/copresence/b/a;->e:Ljava/util/List;

    const/4 v2, 0x0

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 688
    const/4 v2, 0x0

    const/16 v3, 0x190

    invoke-direct {p0, v1, v0, v2, v3}, Lcom/google/android/location/copresence/b/a;->a(Ljava/lang/String;Lcom/google/protobuf/nano/j;Lcom/google/protobuf/nano/j;I)Lcom/google/ac/b/b/a/b;

    move-result-object v0

    .line 690
    iget-object v1, p0, Lcom/google/android/location/copresence/b/a;->a:Ljava/lang/String;

    const-string v2, "ClearCache"

    invoke-direct {p0, v1, v2, v0}, Lcom/google/android/location/copresence/b/a;->a(Ljava/lang/String;Ljava/lang/String;Lcom/google/ac/b/b/a/b;)V

    .line 691
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/location/copresence/b/a;->d(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 685
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 693
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/google/android/location/copresence/b/a;->d:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 694
    iget-object v0, p0, Lcom/google/android/location/copresence/b/a;->e:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 695
    iget-object v0, p0, Lcom/google/android/location/copresence/b/a;->f:Ljava/util/List;

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 696
    monitor-exit p0

    return-void
.end method

.method public static a(Ljava/lang/String;I)V
    .locals 3

    .prologue
    .line 230
    new-instance v0, Lcom/google/ac/b/b/a/d;

    invoke-direct {v0}, Lcom/google/ac/b/b/a/d;-><init>()V

    .line 231
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, v0, Lcom/google/ac/b/b/a/d;->d:Ljava/lang/Integer;

    .line 232
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "CopresenceLogger: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/google/android/location/copresence/b/a;->a(Ljava/lang/String;Lcom/google/ac/b/b/a/d;)V

    .line 233
    return-void
.end method

.method private static a(Ljava/lang/String;Lcom/google/ac/b/b/a/d;)V
    .locals 3

    .prologue
    .line 236
    invoke-static {}, Lcom/google/android/location/copresence/b/a;->a()Lcom/google/android/location/copresence/b/a;

    move-result-object v0

    .line 237
    new-instance v1, Lcom/google/ac/b/b/a/b;

    invoke-direct {v1}, Lcom/google/ac/b/b/a/b;-><init>()V

    .line 238
    iput-object p1, v1, Lcom/google/ac/b/b/a/b;->b:Lcom/google/ac/b/b/a/d;

    .line 239
    const-string v2, ""

    invoke-direct {v0, v2}, Lcom/google/android/location/copresence/b/a;->b(Ljava/lang/String;)Lcom/google/android/gms/playlog/a;

    move-result-object v0

    .line 240
    invoke-static {v1}, Lcom/google/protobuf/nano/j;->toByteArray(Lcom/google/protobuf/nano/j;)[B

    move-result-object v1

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/String;

    invoke-virtual {v0, p0, v1, v2}, Lcom/google/android/gms/playlog/a;->a(Ljava/lang/String;[B[Ljava/lang/String;)V

    .line 241
    invoke-virtual {v0}, Lcom/google/android/gms/playlog/a;->a()V

    .line 242
    return-void
.end method

.method private declared-synchronized a(Ljava/lang/String;Ljava/lang/String;Lcom/google/ac/b/b/a/b;)V
    .locals 8

    .prologue
    const/4 v0, 0x0

    .line 313
    monitor-enter p0

    if-nez p3, :cond_0

    .line 334
    :goto_0
    monitor-exit p0

    return-void

    .line 316
    :cond_0
    :try_start_0
    invoke-direct {p0, p1}, Lcom/google/android/location/copresence/b/a;->b(Ljava/lang/String;)Lcom/google/android/gms/playlog/a;

    move-result-object v3

    .line 317
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "CopresenceLogger: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {p3}, Lcom/google/protobuf/nano/j;->toByteArray(Lcom/google/protobuf/nano/j;)[B

    move-result-object v2

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/String;

    invoke-virtual {v3, v1, v2, v4}, Lcom/google/android/gms/playlog/a;->a(Ljava/lang/String;[B[Ljava/lang/String;)V

    .line 320
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v4

    invoke-static {}, Lcom/google/android/location/copresence/f/b;->b()Lcom/google/ac/b/c/g;

    move-result-object v1

    iget-object v1, v1, Lcom/google/ac/b/c/g;->m:Lcom/google/ac/b/c/k;

    iget-object v1, v1, Lcom/google/ac/b/c/k;->a:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    sub-long/2addr v4, v6

    move v2, v0

    .line 322
    :goto_1
    iget-object v0, p0, Lcom/google/android/location/copresence/b/a;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_3

    .line 323
    iget-object v0, p0, Lcom/google/android/location/copresence/b/a;->d:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protobuf/nano/j;

    .line 324
    iget-object v1, p0, Lcom/google/android/location/copresence/b/a;->f:Ljava/util/List;

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    cmp-long v1, v6, v4

    if-gez v1, :cond_2

    .line 325
    iget-object v1, p0, Lcom/google/android/location/copresence/b/a;->e:Ljava/util/List;

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    const/4 v6, 0x0

    const/16 v7, 0x190

    invoke-direct {p0, v1, v0, v6, v7}, Lcom/google/android/location/copresence/b/a;->a(Ljava/lang/String;Lcom/google/protobuf/nano/j;Lcom/google/protobuf/nano/j;I)Lcom/google/ac/b/b/a/b;

    move-result-object v1

    .line 327
    if-eqz v1, :cond_1

    .line 328
    const-string v6, "CopresenceLogger: NoResponse"

    invoke-static {v1}, Lcom/google/protobuf/nano/j;->toByteArray(Lcom/google/protobuf/nano/j;)[B

    move-result-object v1

    const/4 v7, 0x0

    new-array v7, v7, [Ljava/lang/String;

    invoke-virtual {v3, v6, v1, v7}, Lcom/google/android/gms/playlog/a;->a(Ljava/lang/String;[B[Ljava/lang/String;)V

    .line 330
    :cond_1
    invoke-direct {p0, v0}, Lcom/google/android/location/copresence/b/a;->b(Lcom/google/protobuf/nano/j;)V

    .line 322
    :cond_2
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    .line 333
    :cond_3
    invoke-virtual {v3}, Lcom/google/android/gms/playlog/a;->a()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 313
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;Lcom/google/ac/b/c/cz;)V
    .locals 1

    .prologue
    .line 152
    invoke-static {p0}, Lcom/google/android/location/copresence/b/a;->a(Ljava/lang/String;)Lcom/google/android/location/copresence/b/a;

    move-result-object v0

    .line 153
    invoke-direct {v0, p2, p1}, Lcom/google/android/location/copresence/b/a;->a(Lcom/google/protobuf/nano/j;Ljava/lang/String;)V

    .line 154
    return-void
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;Lcom/google/ac/b/c/cz;Lcom/google/ac/b/c/da;)V
    .locals 3

    .prologue
    .line 160
    invoke-static {p0}, Lcom/google/android/location/copresence/b/a;->a(Ljava/lang/String;)Lcom/google/android/location/copresence/b/a;

    move-result-object v0

    .line 161
    const/16 v1, 0xc8

    invoke-direct {v0, p1, p2, p3, v1}, Lcom/google/android/location/copresence/b/a;->a(Ljava/lang/String;Lcom/google/protobuf/nano/j;Lcom/google/protobuf/nano/j;I)Lcom/google/ac/b/b/a/b;

    move-result-object v1

    .line 163
    const-string v2, "Report"

    invoke-direct {v0, p0, v2, v1}, Lcom/google/android/location/copresence/b/a;->a(Ljava/lang/String;Ljava/lang/String;Lcom/google/ac/b/b/a/b;)V

    .line 164
    invoke-direct {v0, p2}, Lcom/google/android/location/copresence/b/a;->b(Lcom/google/protobuf/nano/j;)V

    .line 165
    return-void
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;Lcom/google/protobuf/nano/j;I)V
    .locals 3

    .prologue
    .line 196
    invoke-static {p0}, Lcom/google/android/location/copresence/b/a;->a(Ljava/lang/String;)Lcom/google/android/location/copresence/b/a;

    move-result-object v0

    .line 197
    invoke-direct {v0, p2}, Lcom/google/android/location/copresence/b/a;->a(Lcom/google/protobuf/nano/j;)I

    move-result v1

    if-gez v1, :cond_0

    .line 203
    :goto_0
    return-void

    .line 200
    :cond_0
    const/4 v1, 0x0

    invoke-direct {v0, p1, p2, v1, p3}, Lcom/google/android/location/copresence/b/a;->a(Ljava/lang/String;Lcom/google/protobuf/nano/j;Lcom/google/protobuf/nano/j;I)Lcom/google/ac/b/b/a/b;

    move-result-object v1

    .line 201
    const-string v2, "NetworkError"

    invoke-direct {v0, p0, v2, v1}, Lcom/google/android/location/copresence/b/a;->a(Ljava/lang/String;Ljava/lang/String;Lcom/google/ac/b/b/a/b;)V

    .line 202
    invoke-direct {v0, p2}, Lcom/google/android/location/copresence/b/a;->b(Lcom/google/protobuf/nano/j;)V

    goto :goto_0
.end method

.method public static a(Ljava/lang/String;Z)V
    .locals 0

    .prologue
    .line 181
    invoke-static {p0, p1}, Lcom/google/android/location/copresence/b/a;->b(Ljava/lang/String;Z)V

    .line 182
    return-void
.end method

.method public static a(Z)V
    .locals 1

    .prologue
    .line 174
    const-string v0, "Bluetooth_Recovery_Attempt"

    invoke-static {v0, p0}, Lcom/google/android/location/copresence/b/a;->b(Ljava/lang/String;Z)V

    .line 175
    return-void
.end method

.method private b(Ljava/lang/String;)Lcom/google/android/gms/playlog/a;
    .locals 4

    .prologue
    .line 133
    new-instance v0, Lcom/google/android/gms/playlog/a;

    iget-object v1, p0, Lcom/google/android/location/copresence/b/a;->b:Landroid/content/Context;

    const/16 v2, 0x28

    const/4 v3, 0x0

    invoke-direct {v0, v1, v2, p1, v3}, Lcom/google/android/gms/playlog/a;-><init>(Landroid/content/Context;ILjava/lang/String;Z)V

    return-object v0
.end method

.method public static b(I)V
    .locals 2

    .prologue
    .line 220
    new-instance v0, Lcom/google/ac/b/b/a/d;

    invoke-direct {v0}, Lcom/google/ac/b/b/a/d;-><init>()V

    .line 221
    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, v0, Lcom/google/ac/b/b/a/d;->c:Ljava/lang/Integer;

    .line 222
    const-string v1, "CopresenceLogger: "

    invoke-static {v1, v0}, Lcom/google/android/location/copresence/b/a;->a(Ljava/lang/String;Lcom/google/ac/b/b/a/d;)V

    .line 223
    return-void
.end method

.method private declared-synchronized b(Lcom/google/protobuf/nano/j;)V
    .locals 1

    .prologue
    .line 673
    monitor-enter p0

    :try_start_0
    invoke-direct {p0, p1}, Lcom/google/android/location/copresence/b/a;->a(Lcom/google/protobuf/nano/j;)I

    move-result v0

    .line 674
    if-ltz v0, :cond_0

    .line 675
    invoke-direct {p0, v0}, Lcom/google/android/location/copresence/b/a;->d(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 677
    :cond_0
    monitor-exit p0

    return-void

    .line 673
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private static b(Ljava/lang/String;Z)V
    .locals 3

    .prologue
    .line 185
    invoke-static {}, Lcom/google/android/location/copresence/b/a;->a()Lcom/google/android/location/copresence/b/a;

    move-result-object v0

    .line 186
    invoke-direct {v0, p0, p1}, Lcom/google/android/location/copresence/b/a;->c(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 187
    new-instance v0, Lcom/google/ac/b/b/a/d;

    invoke-direct {v0}, Lcom/google/ac/b/b/a/d;-><init>()V

    .line 188
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, v0, Lcom/google/ac/b/b/a/d;->a:Ljava/lang/Boolean;

    .line 189
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "CopresenceLogger: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/google/android/location/copresence/b/a;->a(Ljava/lang/String;Lcom/google/ac/b/b/a/d;)V

    .line 191
    :cond_0
    return-void
.end method

.method private c(Ljava/lang/String;)Lcom/google/ac/b/b/a/i;
    .locals 4

    .prologue
    .line 617
    new-instance v0, Lcom/google/ac/b/b/a/i;

    invoke-direct {v0}, Lcom/google/ac/b/b/a/i;-><init>()V

    .line 618
    iput-object p1, v0, Lcom/google/ac/b/b/a/i;->a:Ljava/lang/String;

    .line 620
    :try_start_0
    iget-object v1, p0, Lcom/google/android/location/copresence/b/a;->b:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    const/16 v2, 0x40

    invoke-virtual {v1, p1, v2}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v1

    .line 622
    iget v2, v1, Landroid/content/pm/PackageInfo;->versionCode:I

    int-to-long v2, v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    iput-object v2, v0, Lcom/google/ac/b/b/a/i;->c:Ljava/lang/Long;

    .line 623
    iget-object v1, v1, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;

    iput-object v1, v0, Lcom/google/ac/b/b/a/i;->b:Ljava/lang/String;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 629
    :cond_0
    :goto_0
    return-object v0

    .line 625
    :catch_0
    move-exception v1

    const/4 v1, 0x6

    invoke-static {v1}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 626
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "CopresenceLogger: Failed to find package for "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/location/copresence/ag;->e(Ljava/lang/String;)I

    goto :goto_0
.end method

.method public static c(I)V
    .locals 4

    .prologue
    .line 249
    invoke-static {}, Lcom/google/android/location/copresence/b/a;->a()Lcom/google/android/location/copresence/b/a;

    move-result-object v0

    .line 250
    new-instance v1, Lcom/google/ac/b/b/a/b;

    invoke-direct {v1}, Lcom/google/ac/b/b/a/b;-><init>()V

    .line 251
    new-instance v2, Lcom/google/ac/b/b/a/c;

    invoke-direct {v2}, Lcom/google/ac/b/b/a/c;-><init>()V

    iput-object v2, v1, Lcom/google/ac/b/b/a/b;->a:Lcom/google/ac/b/b/a/c;

    .line 252
    iget-object v2, v1, Lcom/google/ac/b/b/a/b;->a:Lcom/google/ac/b/b/a/c;

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    iput-object v3, v2, Lcom/google/ac/b/b/a/c;->a:Ljava/lang/Integer;

    .line 253
    const-string v2, ""

    invoke-direct {v0, v2}, Lcom/google/android/location/copresence/b/a;->b(Ljava/lang/String;)Lcom/google/android/gms/playlog/a;

    move-result-object v0

    .line 254
    const-string v2, "CopresenceLogger: "

    invoke-static {v1}, Lcom/google/protobuf/nano/j;->toByteArray(Lcom/google/protobuf/nano/j;)[B

    move-result-object v1

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/String;

    invoke-virtual {v0, v2, v1, v3}, Lcom/google/android/gms/playlog/a;->a(Ljava/lang/String;[B[Ljava/lang/String;)V

    .line 255
    invoke-virtual {v0}, Lcom/google/android/gms/playlog/a;->a()V

    .line 256
    return-void
.end method

.method private declared-synchronized c(Ljava/lang/String;Z)Z
    .locals 8

    .prologue
    .line 295
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/location/copresence/b/a;->g:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    .line 296
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    .line 297
    iget-object v1, p0, Lcom/google/android/location/copresence/b/a;->h:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    .line 298
    if-eqz v0, :cond_0

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-static {}, Lcom/google/android/location/copresence/f/b;->b()Lcom/google/ac/b/c/g;

    move-result-object v0

    iget-object v0, v0, Lcom/google/ac/b/c/g;->m:Lcom/google/ac/b/c/k;

    iget-object v0, v0, Lcom/google/ac/b/c/k;->b:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    sub-long v6, v2, v6

    cmp-long v0, v4, v6

    if-ltz v0, :cond_0

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eq v0, p2, :cond_1

    .line 301
    :cond_0
    iget-object v0, p0, Lcom/google/android/location/copresence/b/a;->g:Ljava/util/Map;

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 302
    iget-object v0, p0, Lcom/google/android/location/copresence/b/a;->h:Ljava/util/Map;

    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 303
    const/4 v0, 0x1

    .line 305
    :goto_0
    monitor-exit p0

    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 295
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized d(I)V
    .locals 1

    .prologue
    .line 667
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/location/copresence/b/a;->d:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 668
    iget-object v0, p0, Lcom/google/android/location/copresence/b/a;->e:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 669
    iget-object v0, p0, Lcom/google/android/location/copresence/b/a;->f:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(I)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 670
    monitor-exit p0

    return-void

    .line 667
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
