.class public final Lcom/google/android/location/am;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/location/ao;


# static fields
.field private static final L:Lcom/google/android/location/e/i;


# instance fields
.field public A:J

.field public B:Lcom/google/android/location/e/bi;

.field C:[Lcom/google/android/location/e/bi;

.field D:Z

.field E:Lcom/google/android/location/j/g;

.field F:Lcom/google/android/location/g/w;

.field G:J

.field H:J

.field final I:Lcom/google/android/location/v;

.field final J:Lcom/google/android/location/e/g;

.field final K:Lcom/google/android/location/y;

.field private M:Lcom/google/android/location/os/aw;

.field private N:Z

.field private O:J

.field final a:Lcom/google/android/location/os/bi;

.field final b:Lcom/google/android/location/b/ak;

.field final c:Lcom/google/android/location/be;

.field public final d:Lcom/google/android/location/e/w;

.field final e:Lcom/google/android/location/b/ao;

.field final f:Lcom/google/android/location/activity/bb;

.field public g:Lcom/google/android/location/e/y;

.field final h:Lcom/google/android/location/g/f;

.field i:Lcom/google/android/location/e/ag;

.field j:Z

.field k:Z

.field l:Lcom/google/p/a/b/b/a;

.field public m:J

.field n:J

.field o:Z

.field p:J

.field q:J

.field r:Lcom/google/android/location/e/i;

.field public s:J

.field t:I

.field u:Z

.field v:Z

.field w:I

.field x:Z

.field y:Z

.field z:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 77
    new-instance v0, Lcom/google/android/location/e/i;

    invoke-direct {v0}, Lcom/google/android/location/e/i;-><init>()V

    sput-object v0, Lcom/google/android/location/am;->L:Lcom/google/android/location/e/i;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/location/os/bi;Lcom/google/android/location/b/ak;Lcom/google/android/location/b/ao;Lcom/google/android/location/b/ad;Lcom/google/android/location/v;Lcom/google/android/location/e/w;Lcom/google/android/location/be;Lcom/google/android/location/e/g;)V
    .locals 6

    .prologue
    .line 221
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 129
    new-instance v0, Lcom/google/android/location/e/ag;

    invoke-direct {v0}, Lcom/google/android/location/e/ag;-><init>()V

    iput-object v0, p0, Lcom/google/android/location/am;->i:Lcom/google/android/location/e/ag;

    .line 130
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/location/am;->j:Z

    .line 131
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/location/am;->k:Z

    .line 134
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/location/am;->N:Z

    .line 137
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/google/android/location/am;->O:J

    .line 141
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/google/android/location/am;->m:J

    .line 144
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/google/android/location/am;->n:J

    .line 146
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/location/am;->o:Z

    .line 149
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/google/android/location/am;->p:J

    .line 150
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/google/android/location/am;->q:J

    .line 155
    new-instance v0, Lcom/google/android/location/e/i;

    const-wide/16 v2, 0x7530

    invoke-direct {v0, v2, v3}, Lcom/google/android/location/e/i;-><init>(J)V

    iput-object v0, p0, Lcom/google/android/location/am;->r:Lcom/google/android/location/e/i;

    .line 156
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/google/android/location/am;->s:J

    .line 157
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/location/am;->t:I

    .line 158
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/location/am;->u:Z

    .line 159
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/location/am;->v:Z

    .line 160
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/location/am;->w:I

    .line 163
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/location/am;->x:Z

    .line 164
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/location/am;->y:Z

    .line 165
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/location/am;->z:I

    .line 166
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/google/android/location/am;->A:J

    .line 170
    new-instance v0, Lcom/google/android/location/an;

    invoke-direct {v0, p0}, Lcom/google/android/location/an;-><init>(Lcom/google/android/location/am;)V

    iput-object v0, p0, Lcom/google/android/location/am;->E:Lcom/google/android/location/j/g;

    .line 206
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/location/am;->G:J

    .line 207
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/location/am;->H:J

    .line 222
    iput-object p1, p0, Lcom/google/android/location/am;->a:Lcom/google/android/location/os/bi;

    .line 223
    iput-object p2, p0, Lcom/google/android/location/am;->b:Lcom/google/android/location/b/ak;

    .line 224
    iput-object p3, p0, Lcom/google/android/location/am;->e:Lcom/google/android/location/b/ao;

    .line 225
    new-instance v0, Lcom/google/android/location/g/w;

    invoke-interface {p1}, Lcom/google/android/location/os/bi;->B()Lcom/google/android/location/j/f;

    move-result-object v1

    invoke-interface {p1}, Lcom/google/android/location/os/bi;->c()Lcom/google/android/location/j/b;

    move-result-object v2

    invoke-direct {v0, v1, p4, v2, p0}, Lcom/google/android/location/g/w;-><init>(Lcom/google/android/location/j/f;Lcom/google/android/location/b/ad;Lcom/google/android/location/j/b;Lcom/google/android/location/ao;)V

    iput-object v0, p0, Lcom/google/android/location/am;->F:Lcom/google/android/location/g/w;

    .line 227
    new-instance v0, Lcom/google/android/location/g/f;

    iget-object v1, p2, Lcom/google/android/location/b/ak;->d:Lcom/google/android/location/b/ba;

    iget-object v2, p2, Lcom/google/android/location/b/ak;->e:Lcom/google/android/location/b/ba;

    invoke-virtual {p2}, Lcom/google/android/location/b/ak;->e()Lcom/google/android/location/b/ai;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/location/am;->F:Lcom/google/android/location/g/w;

    move-object v5, p1

    invoke-direct/range {v0 .. v5}, Lcom/google/android/location/g/f;-><init>(Lcom/google/android/location/b/ba;Lcom/google/android/location/b/ba;Lcom/google/android/location/b/ai;Lcom/google/android/location/g/w;Lcom/google/android/location/os/bi;)V

    iput-object v0, p0, Lcom/google/android/location/am;->h:Lcom/google/android/location/g/f;

    .line 229
    iput-object p6, p0, Lcom/google/android/location/am;->d:Lcom/google/android/location/e/w;

    .line 230
    iput-object p7, p0, Lcom/google/android/location/am;->c:Lcom/google/android/location/be;

    .line 231
    iput-object p5, p0, Lcom/google/android/location/am;->I:Lcom/google/android/location/v;

    .line 232
    iput-object p8, p0, Lcom/google/android/location/am;->J:Lcom/google/android/location/e/g;

    .line 233
    new-instance v0, Lcom/google/android/location/activity/bb;

    iget-object v1, p2, Lcom/google/android/location/b/ak;->e:Lcom/google/android/location/b/ba;

    invoke-interface {p1}, Lcom/google/android/location/os/bi;->c()Lcom/google/android/location/j/b;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/google/android/location/activity/bb;-><init>(Lcom/google/android/location/b/ba;Lcom/google/android/location/j/b;)V

    iput-object v0, p0, Lcom/google/android/location/am;->f:Lcom/google/android/location/activity/bb;

    .line 235
    new-instance v0, Lcom/google/android/location/e/y;

    const v1, 0x7fffffff

    const v2, 0x7fffffff

    const v3, 0x7fffffff

    const/4 v4, 0x0

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/location/e/y;-><init>(IIILcom/google/android/location/o/n;)V

    iput-object v0, p0, Lcom/google/android/location/am;->g:Lcom/google/android/location/e/y;

    .line 237
    new-instance v0, Lcom/google/android/location/y;

    invoke-direct {v0}, Lcom/google/android/location/y;-><init>()V

    iput-object v0, p0, Lcom/google/android/location/am;->K:Lcom/google/android/location/y;

    .line 246
    new-instance v0, Lcom/google/android/location/e/bi;

    const-wide/16 v2, -0x4e20

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    invoke-direct {v0, v2, v3, v1}, Lcom/google/android/location/e/bi;-><init>(JLjava/util/ArrayList;)V

    iput-object v0, p0, Lcom/google/android/location/am;->B:Lcom/google/android/location/e/bi;

    .line 251
    invoke-interface {p1}, Lcom/google/android/location/os/bi;->c()Lcom/google/android/location/j/b;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/location/j/b;->c()J

    move-result-wide v0

    invoke-static {}, Ljava/lang/Math;->random()D

    move-result-wide v2

    const-wide v4, 0x4144997000000000L    # 2700000.0

    mul-double/2addr v2, v4

    double-to-long v2, v2

    sub-long/2addr v0, v2

    iput-wide v0, p0, Lcom/google/android/location/am;->G:J

    .line 255
    return-void
.end method

.method private static a(Ljava/util/List;J)Lcom/google/p/a/b/b/a;
    .locals 9

    .prologue
    const/4 v7, 0x2

    const/4 v6, 0x1

    .line 288
    new-instance v2, Lcom/google/p/a/b/b/a;

    sget-object v0, Lcom/google/android/location/m/a;->Q:Lcom/google/p/a/b/b/c;

    invoke-direct {v2, v0}, Lcom/google/p/a/b/b/a;-><init>(Lcom/google/p/a/b/b/c;)V

    .line 293
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/e/ag;

    .line 294
    const/4 v1, 0x0

    .line 297
    iget-object v4, v0, Lcom/google/android/location/e/ag;->c:Lcom/google/android/location/e/f;

    if-eqz v4, :cond_1

    iget-object v4, v0, Lcom/google/android/location/e/ag;->c:Lcom/google/android/location/e/f;

    iget-object v4, v4, Lcom/google/android/location/e/f;->d:Lcom/google/android/location/e/ab;

    sget-object v5, Lcom/google/android/location/e/ab;->c:Lcom/google/android/location/e/ab;

    if-ne v4, v5, :cond_1

    .line 298
    iget-object v4, v0, Lcom/google/android/location/e/ag;->c:Lcom/google/android/location/e/f;

    iget-object v4, v4, Lcom/google/android/location/e/f;->a:Lcom/google/android/location/e/h;

    .line 300
    if-eqz v4, :cond_1

    .line 301
    new-instance v1, Lcom/google/p/a/b/b/a;

    sget-object v5, Lcom/google/android/location/m/a;->F:Lcom/google/p/a/b/b/c;

    invoke-direct {v1, v5}, Lcom/google/p/a/b/b/a;-><init>(Lcom/google/p/a/b/b/c;)V

    .line 302
    invoke-virtual {v4, v1, p1, p2}, Lcom/google/android/location/e/h;->a(Lcom/google/p/a/b/b/a;J)V

    .line 307
    :cond_1
    iget-object v4, v0, Lcom/google/android/location/e/ag;->b:Lcom/google/android/location/e/bf;

    if-eqz v4, :cond_3

    iget-object v4, v0, Lcom/google/android/location/e/ag;->b:Lcom/google/android/location/e/bf;

    iget-object v4, v4, Lcom/google/android/location/e/bf;->d:Lcom/google/android/location/e/ab;

    sget-object v5, Lcom/google/android/location/e/ab;->c:Lcom/google/android/location/e/ab;

    if-ne v4, v5, :cond_3

    .line 308
    iget-object v0, v0, Lcom/google/android/location/e/ag;->b:Lcom/google/android/location/e/bf;

    iget-object v0, v0, Lcom/google/android/location/e/bf;->b:Lcom/google/android/location/e/bi;

    .line 310
    if-eqz v0, :cond_3

    .line 311
    invoke-virtual {v0, p1, p2, v6}, Lcom/google/android/location/e/bi;->a(JZ)Lcom/google/p/a/b/b/a;

    move-result-object v0

    .line 312
    const/4 v4, 0x3

    invoke-virtual {v0, v4, v7}, Lcom/google/p/a/b/b/a;->f(II)Lcom/google/p/a/b/b/a;

    .line 315
    if-nez v1, :cond_2

    .line 316
    new-instance v1, Lcom/google/p/a/b/b/a;

    sget-object v4, Lcom/google/android/location/m/a;->F:Lcom/google/p/a/b/b/c;

    invoke-direct {v1, v4}, Lcom/google/p/a/b/b/a;-><init>(Lcom/google/p/a/b/b/c;)V

    .line 320
    :cond_2
    new-instance v4, Lcom/google/p/a/b/b/a;

    sget-object v5, Lcom/google/android/location/m/a;->I:Lcom/google/p/a/b/b/c;

    invoke-direct {v4, v5}, Lcom/google/p/a/b/b/a;-><init>(Lcom/google/p/a/b/b/c;)V

    .line 322
    invoke-virtual {v4, v6, v6}, Lcom/google/p/a/b/b/a;->f(II)Lcom/google/p/a/b/b/a;

    .line 325
    const/16 v5, 0xc

    invoke-virtual {v1, v5, v4}, Lcom/google/p/a/b/b/a;->a(ILcom/google/p/a/b/b/a;)V

    .line 327
    invoke-virtual {v1, v7, v0}, Lcom/google/p/a/b/b/a;->b(ILcom/google/p/a/b/b/a;)Lcom/google/p/a/b/b/a;

    .line 331
    :cond_3
    if-eqz v1, :cond_0

    .line 333
    const/16 v0, 0xa

    const/4 v4, 0x0

    invoke-virtual {v1, v0, v4}, Lcom/google/p/a/b/b/a;->f(II)Lcom/google/p/a/b/b/a;

    .line 336
    const/4 v0, 0x4

    invoke-virtual {v2, v0, v1}, Lcom/google/p/a/b/b/a;->a(ILcom/google/p/a/b/b/a;)V

    goto :goto_0

    .line 340
    :cond_4
    return-object v2
.end method

.method private a(I)V
    .locals 3

    .prologue
    .line 1036
    iget v0, p0, Lcom/google/android/location/am;->w:I

    if-eq v0, p1, :cond_0

    .line 1037
    iput p1, p0, Lcom/google/android/location/am;->w:I

    .line 1038
    iget-object v0, p0, Lcom/google/android/location/am;->a:Lcom/google/android/location/os/bi;

    iget v1, p0, Lcom/google/android/location/am;->z:I

    iget v2, p0, Lcom/google/android/location/am;->w:I

    invoke-interface {v0, v1, v2}, Lcom/google/android/location/os/bi;->a(II)V

    .line 1040
    :cond_0
    return-void
.end method

.method private a(J)V
    .locals 5

    .prologue
    .line 277
    iput-wide p1, p0, Lcom/google/android/location/am;->q:J

    .line 278
    iget-wide v0, p0, Lcom/google/android/location/am;->p:J

    const-wide/16 v2, 0x1

    sub-long v2, p1, v2

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/location/am;->p:J

    .line 279
    iget-object v0, p0, Lcom/google/android/location/am;->a:Lcom/google/android/location/os/bi;

    invoke-interface {v0}, Lcom/google/android/location/os/bi;->f()Lcom/google/android/location/j/j;

    move-result-object v0

    sget-object v1, Lcom/google/android/location/j/k;->a:Lcom/google/android/location/j/k;

    invoke-interface {v0, v1}, Lcom/google/android/location/j/j;->a(Lcom/google/android/location/j/k;)V

    .line 280
    return-void
.end method

.method private a(Lcom/google/android/location/j/b;ZZ)V
    .locals 6

    .prologue
    .line 894
    iget-object v2, p0, Lcom/google/android/location/am;->g:Lcom/google/android/location/e/y;

    invoke-interface {p1}, Lcom/google/android/location/j/b;->c()J

    move-result-wide v4

    iget-boolean v3, p0, Lcom/google/android/location/am;->u:Z

    iget-boolean v0, p0, Lcom/google/android/location/am;->x:Z

    if-nez v3, :cond_0

    if-eqz v0, :cond_4

    :cond_0
    const/4 v0, 0x1

    move v1, v0

    :goto_0
    if-nez p2, :cond_1

    if-eqz p3, :cond_5

    :cond_1
    const/4 v0, 0x1

    :goto_1
    if-eq v1, v0, :cond_2

    if-eqz v0, :cond_6

    invoke-virtual {v2}, Lcom/google/android/location/e/y;->c()Z

    move-result v0

    if-eqz v0, :cond_6

    const-wide/16 v0, 0x1

    sub-long v0, v4, v0

    iput-wide v0, v2, Lcom/google/android/location/e/y;->f:J

    :cond_2
    :goto_2
    if-eq v3, p2, :cond_3

    if-eqz p2, :cond_7

    invoke-virtual {v2}, Lcom/google/android/location/e/y;->d()Z

    move-result v0

    if-eqz v0, :cond_7

    const-wide/16 v0, 0x1

    sub-long v0, v4, v0

    iput-wide v0, v2, Lcom/google/android/location/e/y;->e:J

    .line 896
    :cond_3
    :goto_3
    iput-boolean p2, p0, Lcom/google/android/location/am;->u:Z

    .line 897
    iput-boolean p3, p0, Lcom/google/android/location/am;->x:Z

    .line 899
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/google/android/location/am;->a(Lcom/google/android/location/j/b;[Lcom/google/android/location/e/bi;Z)V

    .line 900
    return-void

    .line 894
    :cond_4
    const/4 v0, 0x0

    move v1, v0

    goto :goto_0

    :cond_5
    const/4 v0, 0x0

    goto :goto_1

    :cond_6
    const-wide/32 v0, 0x7fffffff

    add-long/2addr v0, v4

    iput-wide v0, v2, Lcom/google/android/location/e/y;->f:J

    goto :goto_2

    :cond_7
    const-wide/32 v0, 0x7fffffff

    add-long/2addr v0, v4

    iput-wide v0, v2, Lcom/google/android/location/e/y;->e:J

    goto :goto_3
.end method

.method private a(Lcom/google/android/location/j/b;[Lcom/google/android/location/e/bi;Z)V
    .locals 34

    .prologue
    .line 353
    invoke-interface/range {p1 .. p1}, Lcom/google/android/location/j/b;->c()J

    move-result-wide v12

    .line 359
    if-nez p2, :cond_18

    .line 360
    const/4 v2, 0x0

    .line 367
    :goto_0
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/location/am;->b:Lcom/google/android/location/b/ak;

    iget-object v3, v3, Lcom/google/android/location/b/ak;->c:Lcom/google/android/location/e/ah;

    invoke-virtual {v3}, Lcom/google/android/location/e/ah;->p()I

    move-result v3

    .line 368
    sget-boolean v4, Lcom/google/android/location/i/a;->b:Z

    if-eqz v4, :cond_0

    const-string v4, "NetworkLocator"

    const-string v5, "WifiOffset is %d"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/android/location/o/a/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 369
    :cond_0
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/location/am;->h:Lcom/google/android/location/g/f;

    iget-object v4, v4, Lcom/google/android/location/g/f;->a:Lcom/google/android/location/g/ae;

    new-instance v5, Lcom/google/android/location/g/ag;

    const-wide/high16 v6, 0x3ff0000000000000L    # 1.0

    int-to-double v8, v3

    invoke-direct {v5, v6, v7, v8, v9}, Lcom/google/android/location/g/ag;-><init>(DD)V

    iput-object v5, v4, Lcom/google/android/location/g/ae;->c:Lcom/google/android/location/g/ag;

    .line 372
    if-eqz v2, :cond_1

    .line 373
    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/location/am;->B:Lcom/google/android/location/e/bi;

    .line 374
    move-object/from16 v0, p2

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/location/am;->C:[Lcom/google/android/location/e/bi;

    .line 378
    :cond_1
    move-object/from16 v0, p0

    iget-wide v2, v0, Lcom/google/android/location/am;->O:J

    sub-long v2, v12, v2

    const-wide/32 v4, 0xdbba00

    cmp-long v2, v2, v4

    if-lez v2, :cond_3

    .line 380
    sget-object v2, Lcom/google/android/location/d/a;->f:Lcom/google/android/gms/common/a/d;

    invoke-static {v2}, Lcom/google/android/location/d/a;->a(Lcom/google/android/gms/common/a/d;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 382
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/location/am;->a:Lcom/google/android/location/os/bi;

    const-string v3, "nlp"

    const-string v4, "settings"

    const-string v5, "gps"

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/location/am;->a:Lcom/google/android/location/os/bi;

    invoke-interface {v6}, Lcom/google/android/location/os/bi;->h()Z

    move-result v6

    if-eqz v6, :cond_19

    const-wide/16 v6, 0x1

    :goto_1
    const/4 v8, 0x0

    invoke-interface/range {v2 .. v8}, Lcom/google/android/location/os/bi;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JZ)V

    .line 384
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/location/am;->a:Lcom/google/android/location/os/bi;

    const-string v3, "nlp"

    const-string v4, "settings"

    const-string v5, "cell"

    move-object/from16 v0, p0

    iget-boolean v6, v0, Lcom/google/android/location/am;->u:Z

    if-eqz v6, :cond_1a

    const-wide/16 v6, 0x1

    :goto_2
    const/4 v8, 0x0

    invoke-interface/range {v2 .. v8}, Lcom/google/android/location/os/bi;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JZ)V

    .line 386
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/location/am;->a:Lcom/google/android/location/os/bi;

    const-string v3, "nlp"

    const-string v4, "settings"

    const-string v5, "wifi"

    move-object/from16 v0, p0

    iget-boolean v6, v0, Lcom/google/android/location/am;->x:Z

    if-eqz v6, :cond_1b

    const-wide/16 v6, 0x1

    :goto_3
    const/4 v8, 0x0

    invoke-interface/range {v2 .. v8}, Lcom/google/android/location/os/bi;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JZ)V

    .line 389
    :cond_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/location/am;->a:Lcom/google/android/location/os/bi;

    invoke-interface {v2, v12, v13}, Lcom/google/android/location/os/bi;->a(J)V

    .line 390
    move-object/from16 v0, p0

    iput-wide v12, v0, Lcom/google/android/location/am;->O:J

    .line 395
    :cond_3
    move-object/from16 v0, p0

    iget-wide v2, v0, Lcom/google/android/location/am;->H:J

    sub-long v2, v12, v2

    const-wide/32 v4, 0x36ee80

    cmp-long v2, v2, v4

    if-lez v2, :cond_5

    .line 396
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/location/am;->b:Lcom/google/android/location/b/ak;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/location/am;->a:Lcom/google/android/location/os/bi;

    invoke-interface {v3}, Lcom/google/android/location/os/bi;->c()Lcom/google/android/location/j/b;

    move-result-object v3

    invoke-interface {v3}, Lcom/google/android/location/j/b;->b()J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Lcom/google/android/location/b/ak;->a(J)V

    .line 397
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/location/am;->F:Lcom/google/android/location/g/w;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/location/am;->a:Lcom/google/android/location/os/bi;

    invoke-interface {v3}, Lcom/google/android/location/os/bi;->c()Lcom/google/android/location/j/b;

    move-result-object v3

    invoke-interface {v3}, Lcom/google/android/location/j/b;->b()J

    move-result-wide v4

    iget-object v2, v2, Lcom/google/android/location/g/w;->a:Lcom/google/android/location/b/ad;

    invoke-virtual {v2, v4, v5}, Lcom/google/android/location/b/ad;->a(J)V

    .line 398
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/location/am;->e:Lcom/google/android/location/b/ao;

    if-eqz v2, :cond_4

    .line 399
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/location/am;->e:Lcom/google/android/location/b/ao;

    invoke-virtual {v2}, Lcom/google/android/location/b/ao;->c()V

    .line 401
    :cond_4
    move-object/from16 v0, p0

    iput-wide v12, v0, Lcom/google/android/location/am;->H:J

    .line 415
    :cond_5
    move-object/from16 v0, p0

    iget-wide v2, v0, Lcom/google/android/location/am;->p:J

    sub-long v30, v12, v2

    .line 416
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/location/am;->B:Lcom/google/android/location/e/bi;

    iget-wide v2, v2, Lcom/google/android/location/e/bi;->a:J

    sub-long v14, v12, v2

    .line 420
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/location/am;->r:Lcom/google/android/location/e/i;

    iget-object v0, v2, Lcom/google/android/location/e/i;->a:Lcom/google/android/location/e/h;

    move-object/from16 v29, v0

    .line 422
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/location/am;->g:Lcom/google/android/location/e/y;

    iget-wide v2, v2, Lcom/google/android/location/e/y;->f:J

    cmp-long v2, v12, v2

    if-ltz v2, :cond_1c

    const/4 v2, 0x1

    move v10, v2

    .line 423
    :goto_4
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/location/am;->g:Lcom/google/android/location/e/y;

    iget-wide v2, v2, Lcom/google/android/location/e/y;->e:J

    cmp-long v2, v12, v2

    if-ltz v2, :cond_1d

    const/4 v2, 0x1

    move/from16 v28, v2

    .line 424
    :goto_5
    if-nez v10, :cond_6

    if-eqz v28, :cond_1e

    :cond_6
    const/4 v2, 0x1

    move/from16 v27, v2

    .line 425
    :goto_6
    if-eqz v29, :cond_1f

    invoke-virtual/range {v29 .. v29}, Lcom/google/android/location/e/h;->i()Z

    move-result v2

    if-eqz v2, :cond_1f

    invoke-virtual/range {v29 .. v29}, Lcom/google/android/location/e/h;->f()J

    move-result-wide v2

    sub-long v2, v12, v2

    const-wide/16 v4, 0x4e20

    cmp-long v2, v2, v4

    if-gez v2, :cond_1f

    const/4 v2, 0x1

    move v3, v2

    .line 428
    :goto_7
    const-wide/16 v4, 0x4e20

    cmp-long v2, v14, v4

    if-gez v2, :cond_21

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/location/am;->B:Lcom/google/android/location/e/bi;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/location/am;->i:Lcom/google/android/location/e/ag;

    iget-object v5, v2, Lcom/google/android/location/e/ag;->b:Lcom/google/android/location/e/bf;

    if-nez v5, :cond_20

    const/4 v2, 0x0

    :goto_8
    invoke-virtual {v4, v2}, Lcom/google/android/location/e/bi;->b(Lcom/google/android/location/e/bi;)Z

    move-result v2

    if-nez v2, :cond_21

    const/4 v2, 0x1

    move/from16 v26, v2

    .line 434
    :goto_9
    if-eqz v3, :cond_23

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/location/am;->J:Lcom/google/android/location/e/g;

    invoke-virtual {v2}, Lcom/google/android/location/e/g;->a()Z

    move-result v2

    if-eqz v2, :cond_23

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/location/am;->i:Lcom/google/android/location/e/ag;

    iget-object v4, v2, Lcom/google/android/location/e/ag;->c:Lcom/google/android/location/e/f;

    if-nez v4, :cond_22

    const/4 v2, 0x0

    :goto_a
    move-object/from16 v0, v29

    if-eq v0, v2, :cond_23

    const/4 v2, 0x1

    move/from16 v25, v2

    .line 442
    :goto_b
    if-eqz v27, :cond_24

    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/google/android/location/am;->u:Z

    if-eqz v2, :cond_24

    const/4 v2, 0x1

    move/from16 v24, v2

    .line 443
    :goto_c
    if-eqz v10, :cond_25

    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/google/android/location/am;->x:Z

    if-eqz v2, :cond_25

    if-eqz v26, :cond_7

    const-wide/16 v4, 0x4e20

    sub-long/2addr v4, v14

    const-wide/16 v6, 0xc8

    cmp-long v2, v4, v6

    if-gez v2, :cond_25

    :cond_7
    const/4 v2, 0x1

    move/from16 v23, v2

    .line 448
    :goto_d
    if-nez v24, :cond_8

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/google/android/location/am;->s:J

    const-wide/16 v6, -0x1

    cmp-long v2, v4, v6

    if-eqz v2, :cond_26

    if-eqz v29, :cond_8

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/google/android/location/am;->s:J

    invoke-virtual/range {v29 .. v29}, Lcom/google/android/location/e/h;->f()J

    move-result-wide v6

    cmp-long v2, v4, v6

    if-lez v2, :cond_26

    :cond_8
    const/4 v2, 0x1

    move/from16 v22, v2

    .line 452
    :goto_e
    if-nez v23, :cond_9

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/google/android/location/am;->A:J

    const-wide/16 v6, -0x1

    cmp-long v2, v4, v6

    if-eqz v2, :cond_27

    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/google/android/location/am;->x:Z

    if-eqz v2, :cond_27

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/google/android/location/am;->A:J

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/location/am;->B:Lcom/google/android/location/e/bi;

    iget-wide v6, v2, Lcom/google/android/location/e/bi;->a:J

    cmp-long v2, v4, v6

    if-lez v2, :cond_27

    :cond_9
    const/4 v2, 0x1

    move/from16 v21, v2

    .line 456
    :goto_f
    if-eqz v22, :cond_28

    if-nez v24, :cond_a

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/google/android/location/am;->s:J

    sub-long v4, v12, v4

    const-wide/16 v6, 0x1388

    cmp-long v2, v4, v6

    if-gez v2, :cond_28

    :cond_a
    const/4 v2, 0x1

    move/from16 v20, v2

    .line 459
    :goto_10
    if-eqz v21, :cond_29

    if-nez v23, :cond_b

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/google/android/location/am;->A:J

    sub-long v4, v12, v4

    const-wide/16 v6, 0x1388

    cmp-long v2, v4, v6

    if-gez v2, :cond_29

    :cond_b
    const/4 v2, 0x1

    move/from16 v19, v2

    .line 462
    :goto_11
    if-nez v20, :cond_c

    if-eqz v19, :cond_2a

    :cond_c
    const/4 v2, 0x1

    .line 468
    :goto_12
    if-nez v25, :cond_d

    if-eqz v26, :cond_e

    :cond_d
    if-eqz v2, :cond_f

    :cond_e
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/google/android/location/am;->x:Z

    if-nez v2, :cond_2b

    if-eqz v3, :cond_2b

    if-nez v20, :cond_2b

    :cond_f
    const/4 v2, 0x1

    .line 470
    :goto_13
    if-nez p3, :cond_11

    if-nez v27, :cond_10

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/google/android/location/am;->j:Z

    if-eqz v3, :cond_2c

    :cond_10
    if-eqz v2, :cond_2c

    :cond_11
    const/4 v2, 0x1

    move/from16 v18, v2

    .line 472
    :goto_14
    const/4 v2, 0x0

    .line 476
    if-eqz v18, :cond_91

    .line 477
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/location/am;->M:Lcom/google/android/location/os/aw;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/location/am;->a:Lcom/google/android/location/os/bi;

    invoke-interface {v2}, Lcom/google/android/location/os/bi;->i()Lcom/google/android/location/os/aw;

    move-result-object v2

    const/4 v4, 0x0

    if-eqz v3, :cond_90

    invoke-interface {v3}, Lcom/google/android/location/os/aw;->f()J

    move-result-wide v6

    sub-long v6, v12, v6

    const-wide/32 v8, 0x15f90

    cmp-long v5, v6, v8

    if-gez v5, :cond_90

    :goto_15
    if-eqz v2, :cond_12

    invoke-interface {v2}, Lcom/google/android/location/os/aw;->f()J

    move-result-wide v4

    sub-long v4, v12, v4

    const-wide/32 v6, 0x15f90

    cmp-long v4, v4, v6

    if-gez v4, :cond_12

    invoke-interface {v2}, Lcom/google/android/location/os/aw;->a()F

    move-result v4

    if-eqz v3, :cond_2d

    invoke-interface {v3}, Lcom/google/android/location/os/aw;->a()F

    move-result v5

    float-to-double v6, v4

    float-to-double v4, v5

    const-wide v8, 0x3feccccccccccccdL    # 0.9

    mul-double/2addr v4, v8

    cmpg-double v4, v6, v4

    if-gez v4, :cond_8f

    :goto_16
    move-object v3, v2

    :cond_12
    :goto_17
    if-nez v3, :cond_2e

    const/4 v8, 0x0

    .line 479
    :goto_18
    if-eqz v28, :cond_13

    if-eqz v27, :cond_14

    :cond_13
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/google/android/location/am;->k:Z

    if-eqz v2, :cond_31

    :cond_14
    const/4 v9, 0x1

    .line 482
    :goto_19
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/android/location/am;->h:Lcom/google/android/location/g/f;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/location/am;->C:[Lcom/google/android/location/e/bi;

    if-nez v5, :cond_32

    const/4 v2, 0x0

    :cond_15
    :goto_1a
    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/location/am;->C:[Lcom/google/android/location/e/bi;

    .line 487
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/location/am;->h:Lcom/google/android/location/g/f;

    if-eqz v25, :cond_3b

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/location/am;->r:Lcom/google/android/location/e/i;

    :goto_1b
    const-wide/16 v4, 0x4e20

    cmp-long v2, v14, v4

    if-gez v2, :cond_3c

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/location/am;->C:[Lcom/google/android/location/e/bi;

    :goto_1c
    sget-boolean v2, Lcom/google/android/location/i/a;->b:Z

    if-eqz v2, :cond_16

    const-string v4, "LocatorManager"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v5, "batchMode is "

    invoke-direct {v2, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v5, v3, Lcom/google/android/location/g/f;->b:Z

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v5, ", scans is "

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    if-nez v7, :cond_3d

    const-string v2, "NONE"

    :goto_1d
    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v5, "cellStatus is "

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v5, ", now is "

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v12, v13}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v5, ", deadline is "

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-wide v14, v3, Lcom/google/android/location/g/f;->c:J

    invoke-virtual {v2, v14, v15}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v4, v2}, Lcom/google/android/location/o/a/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_16
    if-nez v7, :cond_3e

    const/4 v15, 0x0

    :goto_1e
    iget-boolean v2, v3, Lcom/google/android/location/g/f;->b:Z

    if-eqz v2, :cond_3f

    move-wide v4, v12

    invoke-virtual/range {v3 .. v9}, Lcom/google/android/location/g/f;->a(JLcom/google/android/location/e/i;[Lcom/google/android/location/e/bi;Lcom/google/android/location/e/al;Z)Ljava/util/List;

    move-result-object v2

    :goto_1f
    move-object v8, v2

    .line 491
    :goto_20
    if-nez v8, :cond_40

    const/4 v2, 0x0

    move v4, v2

    .line 493
    :goto_21
    if-lez v4, :cond_41

    .line 494
    add-int/lit8 v2, v4, -0x1

    invoke-interface {v8, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/location/e/ag;

    move-object v11, v2

    .line 499
    :goto_22
    const/4 v3, 0x0

    .line 500
    const/4 v2, 0x0

    .line 504
    if-lez v4, :cond_42

    .line 505
    invoke-interface {v8}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    move v4, v3

    move v3, v2

    :goto_23
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_43

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/location/e/ag;

    .line 506
    iget-object v6, v2, Lcom/google/android/location/e/ag;->b:Lcom/google/android/location/e/bf;

    if-eqz v6, :cond_17

    iget-object v6, v2, Lcom/google/android/location/e/ag;->b:Lcom/google/android/location/e/bf;

    iget-object v6, v6, Lcom/google/android/location/e/bf;->d:Lcom/google/android/location/e/ab;

    sget-object v7, Lcom/google/android/location/e/ab;->c:Lcom/google/android/location/e/ab;

    if-ne v6, v7, :cond_17

    .line 507
    const/4 v3, 0x1

    .line 510
    :cond_17
    iget-object v6, v2, Lcom/google/android/location/e/ag;->c:Lcom/google/android/location/e/f;

    if-eqz v6, :cond_8d

    iget-object v2, v2, Lcom/google/android/location/e/ag;->c:Lcom/google/android/location/e/f;

    iget-object v2, v2, Lcom/google/android/location/e/f;->d:Lcom/google/android/location/e/ab;

    sget-object v6, Lcom/google/android/location/e/ab;->c:Lcom/google/android/location/e/ab;

    if-ne v2, v6, :cond_8d

    .line 511
    const/4 v4, 0x1

    move v2, v4

    :goto_24
    move v4, v2

    .line 513
    goto :goto_23

    .line 362
    :cond_18
    const/4 v2, 0x0

    aget-object v2, p2, v2

    goto/16 :goto_0

    .line 382
    :cond_19
    const-wide/16 v6, 0x0

    goto/16 :goto_1

    .line 384
    :cond_1a
    const-wide/16 v6, 0x0

    goto/16 :goto_2

    .line 386
    :cond_1b
    const-wide/16 v6, 0x0

    goto/16 :goto_3

    .line 422
    :cond_1c
    const/4 v2, 0x0

    move v10, v2

    goto/16 :goto_4

    .line 423
    :cond_1d
    const/4 v2, 0x0

    move/from16 v28, v2

    goto/16 :goto_5

    .line 424
    :cond_1e
    const/4 v2, 0x0

    move/from16 v27, v2

    goto/16 :goto_6

    .line 425
    :cond_1f
    const/4 v2, 0x0

    move v3, v2

    goto/16 :goto_7

    .line 428
    :cond_20
    iget-object v2, v2, Lcom/google/android/location/e/ag;->b:Lcom/google/android/location/e/bf;

    iget-object v2, v2, Lcom/google/android/location/e/bf;->b:Lcom/google/android/location/e/bi;

    goto/16 :goto_8

    :cond_21
    const/4 v2, 0x0

    move/from16 v26, v2

    goto/16 :goto_9

    .line 434
    :cond_22
    iget-object v2, v2, Lcom/google/android/location/e/ag;->c:Lcom/google/android/location/e/f;

    iget-object v2, v2, Lcom/google/android/location/e/f;->a:Lcom/google/android/location/e/h;

    goto/16 :goto_a

    :cond_23
    const/4 v2, 0x0

    move/from16 v25, v2

    goto/16 :goto_b

    .line 442
    :cond_24
    const/4 v2, 0x0

    move/from16 v24, v2

    goto/16 :goto_c

    .line 443
    :cond_25
    const/4 v2, 0x0

    move/from16 v23, v2

    goto/16 :goto_d

    .line 448
    :cond_26
    const/4 v2, 0x0

    move/from16 v22, v2

    goto/16 :goto_e

    .line 452
    :cond_27
    const/4 v2, 0x0

    move/from16 v21, v2

    goto/16 :goto_f

    .line 456
    :cond_28
    const/4 v2, 0x0

    move/from16 v20, v2

    goto/16 :goto_10

    .line 459
    :cond_29
    const/4 v2, 0x0

    move/from16 v19, v2

    goto/16 :goto_11

    .line 462
    :cond_2a
    const/4 v2, 0x0

    goto/16 :goto_12

    .line 468
    :cond_2b
    const/4 v2, 0x0

    goto/16 :goto_13

    .line 470
    :cond_2c
    const/4 v2, 0x0

    move/from16 v18, v2

    goto/16 :goto_14

    :cond_2d
    move-object v3, v2

    .line 477
    goto/16 :goto_17

    :cond_2e
    invoke-interface {v3}, Lcom/google/android/location/os/aw;->a()F

    move-result v2

    float-to-int v2, v2

    const/16 v4, 0x1f4

    if-ge v2, v4, :cond_2f

    const/16 v2, 0xfa0

    :goto_25
    new-instance v8, Lcom/google/android/location/e/al;

    invoke-interface {v3}, Lcom/google/android/location/os/aw;->b()D

    move-result-wide v4

    const-wide v6, 0x416312d000000000L    # 1.0E7

    mul-double/2addr v4, v6

    double-to-int v4, v4

    invoke-interface {v3}, Lcom/google/android/location/os/aw;->c()D

    move-result-wide v6

    const-wide v16, 0x416312d000000000L    # 1.0E7

    mul-double v6, v6, v16

    double-to-int v3, v6

    mul-int/lit16 v2, v2, 0x3e8

    invoke-direct {v8, v4, v3, v2}, Lcom/google/android/location/e/al;-><init>(III)V

    goto/16 :goto_18

    :cond_2f
    const/16 v4, 0x2710

    if-ge v2, v4, :cond_30

    const v2, 0xc350

    goto :goto_25

    :cond_30
    const v2, 0x186a0

    goto :goto_25

    .line 479
    :cond_31
    const/4 v9, 0x0

    goto/16 :goto_19

    .line 482
    :cond_32
    const/4 v3, 0x0

    const/4 v2, 0x0

    move/from16 v33, v2

    move-object v2, v3

    move/from16 v3, v33

    :goto_26
    array-length v4, v5

    if-ge v3, v4, :cond_3a

    aget-object v6, v5, v3

    iget-object v0, v11, Lcom/google/android/location/g/f;->a:Lcom/google/android/location/g/ae;

    move-object/from16 v16, v0

    if-nez v6, :cond_35

    const/4 v4, 0x0

    :goto_27
    if-eq v4, v6, :cond_34

    if-nez v2, :cond_33

    array-length v2, v5

    invoke-static {v5, v2}, Ljava/util/Arrays;->copyOf([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [Lcom/google/android/location/e/bi;

    :cond_33
    aput-object v4, v2, v3

    :cond_34
    add-int/lit8 v3, v3, 0x1

    goto :goto_26

    :cond_35
    const/4 v7, 0x1

    const/4 v4, 0x0

    :goto_28
    invoke-virtual {v6}, Lcom/google/android/location/e/bi;->a()I

    move-result v17

    move/from16 v0, v17

    if-ge v4, v0, :cond_8e

    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/google/android/location/g/ae;->a:Lcom/google/android/location/d/k;

    move-object/from16 v17, v0

    invoke-virtual {v6, v4}, Lcom/google/android/location/e/bi;->a(I)Lcom/google/android/location/e/bc;

    move-result-object v17

    invoke-static/range {v17 .. v17}, Lcom/google/android/location/d/k;->a(Lcom/google/android/location/e/bc;)Z

    move-result v17

    if-nez v17, :cond_36

    const/4 v4, 0x0

    :goto_29
    if-eqz v4, :cond_37

    move-object v4, v6

    goto :goto_27

    :cond_36
    add-int/lit8 v4, v4, 0x1

    goto :goto_28

    :cond_37
    new-instance v7, Ljava/util/ArrayList;

    invoke-virtual {v6}, Lcom/google/android/location/e/bi;->a()I

    move-result v4

    invoke-direct {v7, v4}, Ljava/util/ArrayList;-><init>(I)V

    const/4 v4, 0x0

    :goto_2a
    invoke-virtual {v6}, Lcom/google/android/location/e/bi;->a()I

    move-result v17

    move/from16 v0, v17

    if-ge v4, v0, :cond_39

    invoke-virtual {v6, v4}, Lcom/google/android/location/e/bi;->a(I)Lcom/google/android/location/e/bc;

    move-result-object v17

    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/google/android/location/g/ae;->a:Lcom/google/android/location/d/k;

    move-object/from16 v32, v0

    invoke-static/range {v17 .. v17}, Lcom/google/android/location/d/k;->a(Lcom/google/android/location/e/bc;)Z

    move-result v32

    if-eqz v32, :cond_38

    move-object/from16 v0, v17

    invoke-virtual {v7, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_38
    add-int/lit8 v4, v4, 0x1

    goto :goto_2a

    :cond_39
    new-instance v4, Lcom/google/android/location/e/bi;

    iget-wide v0, v6, Lcom/google/android/location/e/bi;->a:J

    move-wide/from16 v16, v0

    move-wide/from16 v0, v16

    invoke-direct {v4, v0, v1, v7}, Lcom/google/android/location/e/bi;-><init>(JLjava/util/ArrayList;)V

    goto :goto_27

    :cond_3a
    if-nez v2, :cond_15

    move-object v2, v5

    goto/16 :goto_1a

    .line 487
    :cond_3b
    sget-object v6, Lcom/google/android/location/am;->L:Lcom/google/android/location/e/i;

    goto/16 :goto_1b

    :cond_3c
    const/4 v7, 0x0

    goto/16 :goto_1c

    :cond_3d
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v11, "length "

    invoke-direct {v2, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    array-length v11, v7

    invoke-virtual {v2, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_1d

    :cond_3e
    const/4 v2, 0x0

    aget-object v15, v7, v2

    goto/16 :goto_1e

    :cond_3f
    new-instance v11, Lcom/google/android/location/e/ap;

    move-object v14, v6

    move-object/from16 v16, v8

    move/from16 v17, v9

    invoke-direct/range {v11 .. v17}, Lcom/google/android/location/e/ap;-><init>(JLcom/google/android/location/e/i;Lcom/google/android/location/e/bi;Lcom/google/android/location/e/al;Z)V

    invoke-virtual {v3, v11}, Lcom/google/android/location/g/f;->a(Lcom/google/android/location/e/ap;)Lcom/google/android/location/e/ag;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    goto/16 :goto_1f

    .line 491
    :cond_40
    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v2

    move v4, v2

    goto/16 :goto_21

    .line 496
    :cond_41
    const/4 v2, 0x0

    move-object v11, v2

    goto/16 :goto_22

    :cond_42
    move v4, v3

    move v3, v2

    .line 516
    :cond_43
    if-eqz v11, :cond_5e

    iget-object v2, v11, Lcom/google/android/location/e/ag;->a:Lcom/google/android/location/e/z;

    if-eqz v2, :cond_5e

    const/4 v2, 0x1

    .line 519
    :goto_2b
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/location/am;->l:Lcom/google/p/a/b/b/a;

    if-nez v5, :cond_5f

    if-nez v27, :cond_44

    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/google/android/location/am;->o:Z

    if-eqz v5, :cond_5f

    :cond_44
    if-eqz v4, :cond_45

    if-nez v25, :cond_46

    :cond_45
    if-eqz v3, :cond_5f

    if-eqz v26, :cond_5f

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/location/am;->B:Lcom/google/android/location/e/bi;

    invoke-virtual {v3}, Lcom/google/android/location/e/bi;->a()I

    move-result v3

    if-lez v3, :cond_5f

    :cond_46
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/location/am;->K:Lcom/google/android/location/y;

    invoke-virtual {v3, v12, v13}, Lcom/google/android/location/y;->a(J)Z

    move-result v3

    if-eqz v3, :cond_5f

    const/4 v3, 0x1

    move v7, v3

    .line 523
    :goto_2c
    if-nez v7, :cond_47

    if-eqz v2, :cond_60

    :cond_47
    const/4 v3, 0x1

    move v6, v3

    .line 525
    :goto_2d
    if-eqz v2, :cond_61

    if-nez v7, :cond_61

    const/4 v2, 0x1

    .line 527
    :goto_2e
    invoke-direct/range {p0 .. p0}, Lcom/google/android/location/am;->b()Z

    move-result v5

    .line 528
    if-nez v5, :cond_62

    if-eqz v27, :cond_62

    const/4 v3, 0x1

    .line 532
    :goto_2f
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/location/am;->l:Lcom/google/p/a/b/b/a;

    if-eqz v4, :cond_48

    move-object/from16 v0, p0

    iget-wide v14, v0, Lcom/google/android/location/am;->m:J

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/google/android/location/am;->p:J

    move-wide/from16 v16, v0

    cmp-long v4, v14, v16

    if-gez v4, :cond_49

    :cond_48
    if-eqz v7, :cond_63

    :cond_49
    const/4 v4, 0x1

    .line 534
    :goto_30
    if-nez v22, :cond_4a

    if-nez v21, :cond_4a

    if-eqz v4, :cond_64

    :cond_4a
    const/4 v4, 0x1

    .line 535
    :goto_31
    if-eqz v5, :cond_65

    if-nez v27, :cond_65

    const-wide/16 v14, 0x1388

    cmp-long v9, v30, v14

    if-gez v9, :cond_4b

    if-nez v4, :cond_65

    :cond_4b
    const/4 v4, 0x1

    move v9, v4

    .line 539
    :goto_32
    if-eqz v5, :cond_66

    if-eqz v27, :cond_66

    if-nez v9, :cond_66

    const/4 v4, 0x1

    .line 576
    :goto_33
    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/google/android/location/am;->j:Z

    if-nez v5, :cond_4c

    if-eqz v27, :cond_67

    :cond_4c
    if-nez v18, :cond_67

    const/4 v5, 0x1

    :goto_34
    move-object/from16 v0, p0

    iput-boolean v5, v0, Lcom/google/android/location/am;->j:Z

    .line 577
    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/google/android/location/am;->k:Z

    if-nez v5, :cond_4d

    if-eqz v28, :cond_68

    :cond_4d
    if-nez v18, :cond_68

    const/4 v5, 0x1

    :goto_35
    move-object/from16 v0, p0

    iput-boolean v5, v0, Lcom/google/android/location/am;->k:Z

    .line 578
    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/google/android/location/am;->o:Z

    if-nez v5, :cond_4e

    if-eqz v27, :cond_69

    :cond_4e
    if-nez v6, :cond_69

    const/4 v5, 0x1

    :goto_36
    move-object/from16 v0, p0

    iput-boolean v5, v0, Lcom/google/android/location/am;->o:Z

    .line 580
    if-eqz v3, :cond_4f

    .line 581
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/location/am;->a:Lcom/google/android/location/os/bi;

    invoke-interface {v3}, Lcom/google/android/location/os/bi;->f()Lcom/google/android/location/j/j;

    move-result-object v3

    sget-object v5, Lcom/google/android/location/j/k;->a:Lcom/google/android/location/j/k;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/location/am;->g:Lcom/google/android/location/e/y;

    iget-object v6, v6, Lcom/google/android/location/e/y;->d:Lcom/google/android/location/o/n;

    invoke-interface {v3, v5, v6}, Lcom/google/android/location/j/j;->a(Lcom/google/android/location/j/k;Lcom/google/android/location/o/n;)V

    const-wide/16 v12, -0x1

    move-object/from16 v0, p0

    iput-wide v12, v0, Lcom/google/android/location/am;->q:J

    invoke-interface/range {p1 .. p1}, Lcom/google/android/location/j/b;->c()J

    move-result-wide v12

    move-object/from16 v0, p0

    iput-wide v12, v0, Lcom/google/android/location/am;->p:J

    move-object/from16 v0, p0

    iget-wide v12, v0, Lcom/google/android/location/am;->p:J

    .line 583
    :cond_4f
    if-eqz v4, :cond_50

    .line 584
    move-object/from16 v0, p0

    iput-wide v12, v0, Lcom/google/android/location/am;->p:J

    .line 586
    :cond_50
    if-eqz v10, :cond_51

    .line 587
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/location/am;->d:Lcom/google/android/location/e/w;

    sget-object v4, Lcom/google/android/location/e/x;->a:Lcom/google/android/location/e/x;

    invoke-virtual {v3, v4}, Lcom/google/android/location/e/w;->a(Lcom/google/android/location/e/x;)V

    .line 588
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/location/am;->g:Lcom/google/android/location/e/y;

    invoke-virtual {v3, v12, v13}, Lcom/google/android/location/e/y;->a(J)J

    move-result-wide v4

    iput-wide v4, v3, Lcom/google/android/location/e/y;->f:J

    .line 590
    :cond_51
    if-eqz v28, :cond_52

    .line 591
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/location/am;->d:Lcom/google/android/location/e/w;

    sget-object v4, Lcom/google/android/location/e/x;->a:Lcom/google/android/location/e/x;

    invoke-virtual {v3, v4}, Lcom/google/android/location/e/w;->a(Lcom/google/android/location/e/x;)V

    .line 592
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/location/am;->g:Lcom/google/android/location/e/y;

    invoke-virtual {v3, v12, v13}, Lcom/google/android/location/e/y;->b(J)J

    move-result-wide v4

    iput-wide v4, v3, Lcom/google/android/location/e/y;->e:J

    .line 594
    :cond_52
    if-eqz v24, :cond_53

    .line 595
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/location/am;->a:Lcom/google/android/location/os/bi;

    invoke-interface {v3}, Lcom/google/android/location/os/bi;->b()V

    .line 596
    move-object/from16 v0, p0

    iput-wide v12, v0, Lcom/google/android/location/am;->s:J

    .line 598
    :cond_53
    if-eqz v23, :cond_55

    .line 599
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/google/android/location/am;->D:Z

    if-nez v3, :cond_54

    .line 600
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/location/am;->a:Lcom/google/android/location/os/bi;

    invoke-interface {v3}, Lcom/google/android/location/os/bi;->g()Lcom/google/android/location/os/bn;

    move-result-object v3

    sget-object v4, Lcom/google/android/location/j/k;->a:Lcom/google/android/location/j/k;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/location/am;->g:Lcom/google/android/location/e/y;

    iget-object v5, v5, Lcom/google/android/location/e/y;->d:Lcom/google/android/location/o/n;

    invoke-interface {v3, v4, v5}, Lcom/google/android/location/os/bn;->a(Lcom/google/android/location/j/k;Lcom/google/android/location/o/n;)V

    .line 602
    :cond_54
    move-object/from16 v0, p0

    iput-wide v12, v0, Lcom/google/android/location/am;->A:J

    .line 604
    :cond_55
    if-eqz v7, :cond_56

    .line 605
    invoke-static {v8, v12, v13}, Lcom/google/android/location/am;->a(Ljava/util/List;J)Lcom/google/p/a/b/b/a;

    move-result-object v3

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/location/am;->l:Lcom/google/p/a/b/b/a;

    .line 606
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/location/am;->c:Lcom/google/android/location/be;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/location/am;->a:Lcom/google/android/location/os/bi;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/location/am;->l:Lcom/google/p/a/b/b/a;

    invoke-virtual {v3, v4, v5}, Lcom/google/android/location/be;->a(Lcom/google/android/location/os/bi;Lcom/google/p/a/b/b/a;)V

    .line 607
    move-object/from16 v0, p0

    iput-wide v12, v0, Lcom/google/android/location/am;->m:J

    .line 608
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/location/am;->a:Lcom/google/android/location/os/bi;

    invoke-interface {v3}, Lcom/google/android/location/os/bi;->B()Lcom/google/android/location/j/f;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/location/am;->l:Lcom/google/p/a/b/b/a;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/location/am;->g:Lcom/google/android/location/e/y;

    iget-object v5, v5, Lcom/google/android/location/e/y;->d:Lcom/google/android/location/o/n;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/location/am;->E:Lcom/google/android/location/j/g;

    invoke-interface {v3, v4, v5, v6}, Lcom/google/android/location/j/f;->a(Lcom/google/p/a/b/b/a;Lcom/google/android/location/o/n;Lcom/google/android/location/j/g;)V

    .line 611
    :cond_56
    if-eqz v18, :cond_57

    .line 613
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/location/am;->F:Lcom/google/android/location/g/w;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/location/am;->g:Lcom/google/android/location/e/y;

    iget-object v4, v4, Lcom/google/android/location/e/y;->d:Lcom/google/android/location/o/n;

    invoke-virtual {v3, v4}, Lcom/google/android/location/g/w;->a(Lcom/google/android/location/o/n;)V

    .line 615
    :cond_57
    if-eqz v2, :cond_6e

    .line 616
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/location/am;->f:Lcom/google/android/location/activity/bb;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/location/am;->B:Lcom/google/android/location/e/bi;

    invoke-virtual {v2, v11, v3}, Lcom/google/android/location/activity/bb;->a(Lcom/google/android/location/e/ag;Lcom/google/android/location/e/bi;)Lcom/google/android/location/activity/bd;

    move-result-object v4

    .line 618
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/location/am;->a:Lcom/google/android/location/os/bi;

    invoke-interface {v2, v4}, Lcom/google/android/location/os/bi;->a(Lcom/google/android/location/activity/bd;)V

    .line 619
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/location/am;->a:Lcom/google/android/location/os/bi;

    invoke-interface {v2}, Lcom/google/android/location/os/bi;->i()Lcom/google/android/location/os/aw;

    move-result-object v2

    .line 628
    if-eqz v2, :cond_58

    .line 629
    invoke-interface {v2}, Lcom/google/android/location/os/aw;->f()J

    move-result-wide v2

    .line 631
    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/google/android/location/am;->n:J

    cmp-long v5, v6, v2

    if-lez v5, :cond_6a

    .line 632
    sget-boolean v5, Lcom/google/android/location/i/a;->b:Z

    if-eqz v5, :cond_58

    const-string v5, "NetworkLocator"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "This location used GLS-fetched data: lastLocTime "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", glsQueryTime "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/google/android/location/am;->m:J

    invoke-virtual {v2, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v5, v2}, Lcom/google/android/location/o/a/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 641
    :cond_58
    :goto_37
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/location/am;->i:Lcom/google/android/location/e/ag;

    iget-object v2, v2, Lcom/google/android/location/e/ag;->a:Lcom/google/android/location/e/z;

    if-eqz v2, :cond_5c

    .line 642
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/location/am;->i:Lcom/google/android/location/e/ag;

    iget-object v2, v2, Lcom/google/android/location/e/ag;->a:Lcom/google/android/location/e/z;

    iget-object v3, v11, Lcom/google/android/location/e/ag;->a:Lcom/google/android/location/e/z;

    iget-object v5, v2, Lcom/google/android/location/e/z;->c:Lcom/google/android/location/e/al;

    iget-object v6, v3, Lcom/google/android/location/e/z;->c:Lcom/google/android/location/e/al;

    invoke-static {v5, v6}, Lcom/google/android/location/g/d;->b(Lcom/google/android/location/e/al;Lcom/google/android/location/e/al;)D

    move-result-wide v14

    const-wide v16, 0x408f400000000000L    # 1000.0

    mul-double v14, v14, v16

    iget v5, v5, Lcom/google/android/location/e/al;->f:I

    int-to-double v0, v5

    move-wide/from16 v16, v0

    sub-double v14, v14, v16

    iget v5, v6, Lcom/google/android/location/e/al;->f:I

    int-to-double v6, v5

    sub-double v6, v14, v6

    iget-wide v14, v2, Lcom/google/android/location/e/z;->e:J

    iget-wide v2, v3, Lcom/google/android/location/e/z;->e:J

    sub-long v2, v14, v2

    invoke-static {v2, v3}, Ljava/lang/Math;->abs(J)J

    move-result-wide v2

    const-wide/high16 v14, 0x3ff0000000000000L    # 1.0

    cmpg-double v5, v6, v14

    if-ltz v5, :cond_59

    const-wide/16 v14, 0x0

    cmp-long v5, v2, v14

    if-nez v5, :cond_6b

    :cond_59
    const-wide/16 v2, 0x0

    .line 644
    :goto_38
    sget-boolean v5, Lcom/google/android/location/i/a;->b:Z

    if-eqz v5, :cond_5a

    const-string v5, "NetworkLocator"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "Speed: "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/google/android/location/o/a/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 647
    :cond_5a
    const-wide v6, 0x407544a3d70a3d71L    # 340.29

    cmpl-double v5, v2, v6

    if-lez v5, :cond_5c

    .line 648
    sget-boolean v5, Lcom/google/android/location/i/a;->b:Z

    if-eqz v5, :cond_5b

    const-string v5, "NetworkLocator"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "Speed > Mach1: "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v5, v2}, Lcom/google/android/location/o/a/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 649
    :cond_5b
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/location/am;->d:Lcom/google/android/location/e/w;

    sget-object v3, Lcom/google/android/location/e/x;->d:Lcom/google/android/location/e/x;

    invoke-virtual {v2, v3}, Lcom/google/android/location/e/w;->a(Lcom/google/android/location/e/x;)V

    .line 653
    :cond_5c
    move-object/from16 v0, p0

    iput-object v11, v0, Lcom/google/android/location/am;->i:Lcom/google/android/location/e/ag;

    .line 654
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/location/am;->b:Lcom/google/android/location/b/ak;

    iget-object v3, v11, Lcom/google/android/location/e/ag;->a:Lcom/google/android/location/e/z;

    iput-object v3, v2, Lcom/google/android/location/b/ak;->b:Lcom/google/android/location/e/z;

    .line 655
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/location/am;->d:Lcom/google/android/location/e/w;

    sget-object v3, Lcom/google/android/location/e/x;->b:Lcom/google/android/location/e/x;

    invoke-virtual {v2, v3}, Lcom/google/android/location/e/w;->a(Lcom/google/android/location/e/x;)V

    .line 656
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/location/am;->h:Lcom/google/android/location/g/f;

    sget-boolean v3, Lcom/google/android/location/i/a;->b:Z

    if-eqz v3, :cond_5d

    const-string v3, "LocatorManager"

    const-string v5, "Clearing batch readings"

    invoke-static {v3, v5}, Lcom/google/android/location/o/a/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_5d
    iget-object v3, v2, Lcom/google/android/location/g/f;->e:Lcom/google/android/location/e/d;

    iget-object v3, v3, Lcom/google/android/location/e/d;->a:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->clear()V

    iget-wide v6, v2, Lcom/google/android/location/g/f;->c:J

    iget-wide v14, v2, Lcom/google/android/location/g/f;->d:J

    add-long/2addr v6, v14

    iput-wide v6, v2, Lcom/google/android/location/g/f;->c:J

    .line 658
    invoke-interface {v8}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_39
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_6d

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/location/e/ag;

    .line 659
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/location/am;->a:Lcom/google/android/location/os/bi;

    if-nez v4, :cond_6c

    const/4 v3, 0x0

    :goto_3a
    invoke-interface {v6, v2, v3}, Lcom/google/android/location/os/bi;->a(Lcom/google/android/location/e/ag;Lcom/google/android/location/e/ay;)V

    goto :goto_39

    .line 516
    :cond_5e
    const/4 v2, 0x0

    goto/16 :goto_2b

    .line 519
    :cond_5f
    const/4 v3, 0x0

    move v7, v3

    goto/16 :goto_2c

    .line 523
    :cond_60
    const/4 v3, 0x0

    move v6, v3

    goto/16 :goto_2d

    .line 525
    :cond_61
    const/4 v2, 0x0

    goto/16 :goto_2e

    .line 528
    :cond_62
    const/4 v3, 0x0

    goto/16 :goto_2f

    .line 532
    :cond_63
    const/4 v4, 0x0

    goto/16 :goto_30

    .line 534
    :cond_64
    const/4 v4, 0x0

    goto/16 :goto_31

    .line 535
    :cond_65
    const/4 v4, 0x0

    move v9, v4

    goto/16 :goto_32

    .line 539
    :cond_66
    const/4 v4, 0x0

    goto/16 :goto_33

    .line 576
    :cond_67
    const/4 v5, 0x0

    goto/16 :goto_34

    .line 577
    :cond_68
    const/4 v5, 0x0

    goto/16 :goto_35

    .line 578
    :cond_69
    const/4 v5, 0x0

    goto/16 :goto_36

    .line 635
    :cond_6a
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/location/am;->d:Lcom/google/android/location/e/w;

    sget-object v3, Lcom/google/android/location/e/x;->c:Lcom/google/android/location/e/x;

    invoke-virtual {v2, v3}, Lcom/google/android/location/e/w;->a(Lcom/google/android/location/e/x;)V

    goto/16 :goto_37

    .line 642
    :cond_6b
    long-to-double v2, v2

    div-double v2, v6, v2

    goto/16 :goto_38

    .line 659
    :cond_6c
    iget-object v3, v4, Lcom/google/android/location/activity/bd;->a:Lcom/google/android/location/e/ay;

    goto :goto_3a

    .line 664
    :cond_6d
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/location/am;->a:Lcom/google/android/location/os/bi;

    invoke-interface {v2, v12, v13}, Lcom/google/android/location/os/bi;->a(J)V

    .line 668
    :cond_6e
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/location/am;->g:Lcom/google/android/location/e/y;

    iget-wide v4, v2, Lcom/google/android/location/e/y;->f:J

    iget-wide v2, v2, Lcom/google/android/location/e/y;->e:J

    invoke-static {v4, v5, v2, v3}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v4

    .line 669
    const/4 v2, 0x0

    .line 670
    invoke-direct/range {p0 .. p0}, Lcom/google/android/location/am;->b()Z

    move-result v3

    if-eqz v3, :cond_6f

    if-nez v9, :cond_6f

    .line 671
    move-object/from16 v0, p0

    iget-wide v2, v0, Lcom/google/android/location/am;->p:J

    const-wide/16 v6, 0x1388

    add-long/2addr v2, v6

    invoke-static {v4, v5, v2, v3}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v4

    .line 672
    const/4 v2, 0x1

    .line 674
    :cond_6f
    const-wide v6, 0x7fffffffffffffffL

    cmp-long v3, v4, v6

    if-gez v3, :cond_70

    .line 678
    if-eqz v2, :cond_7c

    .line 682
    const-wide/16 v6, 0x0

    .line 687
    :goto_3b
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/location/am;->a:Lcom/google/android/location/os/bi;

    invoke-interface {v2}, Lcom/google/android/location/os/bi;->f()Lcom/google/android/location/j/j;

    move-result-object v2

    sget-object v3, Lcom/google/android/location/j/k;->a:Lcom/google/android/location/j/k;

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/location/am;->g:Lcom/google/android/location/e/y;

    iget-object v8, v8, Lcom/google/android/location/e/y;->d:Lcom/google/android/location/o/n;

    invoke-interface/range {v2 .. v8}, Lcom/google/android/location/j/j;->a(Lcom/google/android/location/j/k;JJLcom/google/android/location/o/n;)V

    .line 694
    :cond_70
    move-object/from16 v0, p0

    iget-wide v2, v0, Lcom/google/android/location/am;->G:J

    sub-long v2, v12, v2

    .line 695
    const-wide/32 v6, 0x5265c0

    cmp-long v6, v2, v6

    if-gtz v6, :cond_71

    const-wide/32 v6, 0x2932e0

    cmp-long v2, v2, v6

    if-lez v2, :cond_74

    sub-long v2, v4, v12

    const-wide/16 v4, 0x2710

    cmp-long v2, v2, v4

    if-lez v2, :cond_74

    if-nez v22, :cond_74

    if-nez v21, :cond_74

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/location/am;->l:Lcom/google/p/a/b/b/a;

    if-nez v2, :cond_74

    invoke-direct/range {p0 .. p0}, Lcom/google/android/location/am;->b()Z

    move-result v2

    if-eqz v2, :cond_74

    .line 699
    :cond_71
    sget-boolean v2, Lcom/google/android/location/i/a;->b:Z

    if-eqz v2, :cond_72

    const-string v2, "NetworkLocator"

    const-string v3, "NlpState checkpointing..."

    invoke-static {v2, v3}, Lcom/google/android/location/o/a/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 700
    :cond_72
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/location/am;->b:Lcom/google/android/location/b/ak;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/location/am;->a:Lcom/google/android/location/os/bi;

    invoke-interface {v3}, Lcom/google/android/location/os/bi;->e()Lcom/google/android/location/j/e;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/android/location/b/ak;->a(Lcom/google/android/location/j/e;)V

    .line 701
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/location/am;->F:Lcom/google/android/location/g/w;

    iget-object v2, v2, Lcom/google/android/location/g/w;->a:Lcom/google/android/location/b/ad;

    invoke-virtual {v2}, Lcom/google/android/location/b/ad;->b()V

    .line 702
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/location/am;->e:Lcom/google/android/location/b/ao;

    if-eqz v2, :cond_73

    .line 703
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/location/am;->e:Lcom/google/android/location/b/ao;

    invoke-virtual {v2}, Lcom/google/android/location/b/ao;->c()V

    :try_start_0
    iget-object v3, v2, Lcom/google/android/location/b/ao;->g:Lcom/google/android/location/e/au;

    invoke-virtual {v2}, Lcom/google/android/location/b/ao;->a()Lcom/google/p/a/b/b/a;

    move-result-object v2

    invoke-virtual {v3, v2}, Lcom/google/android/location/e/au;->b(Lcom/google/p/a/b/b/a;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 705
    :cond_73
    :goto_3c
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/location/am;->I:Lcom/google/android/location/v;

    invoke-virtual {v2}, Lcom/google/android/location/v;->a()V

    .line 706
    move-object/from16 v0, p0

    iput-wide v12, v0, Lcom/google/android/location/am;->G:J

    .line 709
    :cond_74
    if-eqz v9, :cond_75

    .line 710
    move-object/from16 v0, p0

    invoke-direct {v0, v12, v13}, Lcom/google/android/location/am;->a(J)V

    .line 715
    :cond_75
    if-eqz v11, :cond_7e

    iget-object v2, v11, Lcom/google/android/location/e/ag;->c:Lcom/google/android/location/e/f;

    if-eqz v2, :cond_7e

    const/4 v2, 0x1

    move v3, v2

    .line 716
    :goto_3d
    const/4 v2, 0x1

    .line 717
    if-eqz v22, :cond_7f

    if-nez v20, :cond_7f

    .line 718
    const/4 v2, 0x2

    .line 734
    :cond_76
    :goto_3e
    const/4 v3, 0x1

    if-eq v2, v3, :cond_77

    .line 735
    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/google/android/location/am;->a(I)V

    .line 739
    :cond_77
    if-eqz v11, :cond_86

    iget-object v2, v11, Lcom/google/android/location/e/ag;->b:Lcom/google/android/location/e/bf;

    if-eqz v2, :cond_86

    const/4 v2, 0x1

    move v3, v2

    .line 740
    :goto_3f
    const/4 v2, 0x1

    .line 741
    if-eqz v21, :cond_87

    if-nez v19, :cond_87

    .line 742
    const/4 v2, 0x2

    .line 756
    :cond_78
    :goto_40
    const/4 v3, 0x1

    if-eq v2, v3, :cond_79

    .line 757
    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/google/android/location/am;->b(I)V

    .line 761
    :cond_79
    if-eqz v11, :cond_7b

    .line 762
    iget-object v2, v11, Lcom/google/android/location/e/ag;->b:Lcom/google/android/location/e/bf;

    if-eqz v2, :cond_7a

    iget-object v2, v11, Lcom/google/android/location/e/ag;->b:Lcom/google/android/location/e/bf;

    iget-object v2, v2, Lcom/google/android/location/e/bf;->d:Lcom/google/android/location/e/ab;

    sget-object v3, Lcom/google/android/location/e/ab;->a:Lcom/google/android/location/e/ab;

    if-ne v2, v3, :cond_7a

    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/google/android/location/am;->x:Z

    if-eqz v2, :cond_7a

    .line 764
    const/4 v2, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/google/android/location/am;->b(I)V

    .line 767
    :cond_7a
    iget-object v2, v11, Lcom/google/android/location/e/ag;->c:Lcom/google/android/location/e/f;

    if-eqz v2, :cond_7b

    iget-object v2, v11, Lcom/google/android/location/e/ag;->c:Lcom/google/android/location/e/f;

    iget-object v2, v2, Lcom/google/android/location/e/f;->d:Lcom/google/android/location/e/ab;

    sget-object v3, Lcom/google/android/location/e/ab;->a:Lcom/google/android/location/e/ab;

    if-ne v2, v3, :cond_7b

    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/google/android/location/am;->u:Z

    if-eqz v2, :cond_7b

    .line 769
    const/4 v2, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/google/android/location/am;->a(I)V

    .line 772
    :cond_7b
    return-void

    .line 684
    :cond_7c
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/location/am;->g:Lcom/google/android/location/e/y;

    invoke-virtual {v2}, Lcom/google/android/location/e/y;->c()Z

    move-result v3

    if-nez v3, :cond_7d

    invoke-virtual {v2}, Lcom/google/android/location/e/y;->d()Z

    move-result v3

    if-nez v3, :cond_7d

    const-wide v6, 0x7fffffffffffffffL

    goto/16 :goto_3b

    :cond_7d
    iget v3, v2, Lcom/google/android/location/e/y;->a:I

    iget v2, v2, Lcom/google/android/location/e/y;->b:I

    invoke-static {v3, v2}, Ljava/lang/Math;->min(II)I

    move-result v2

    mul-int/lit16 v2, v2, 0x3e8

    div-int/lit8 v2, v2, 0x8

    int-to-long v2, v2

    const-wide/32 v6, 0x493e0

    invoke-static {v2, v3, v6, v7}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v6

    goto/16 :goto_3b

    .line 703
    :catch_0
    move-exception v2

    sget-boolean v2, Lcom/google/android/location/i/a;->b:Z

    if-eqz v2, :cond_73

    const-string v2, "SeenDevicesCache"

    const-string v3, "Unable to save data to disk."

    invoke-static {v2, v3}, Lcom/google/android/location/o/a/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_3c

    .line 715
    :cond_7e
    const/4 v2, 0x0

    move v3, v2

    goto/16 :goto_3d

    .line 719
    :cond_7f
    if-eqz v18, :cond_80

    if-eqz v25, :cond_80

    if-eqz v3, :cond_81

    iget-object v4, v11, Lcom/google/android/location/e/ag;->c:Lcom/google/android/location/e/f;

    iget-object v4, v4, Lcom/google/android/location/e/f;->d:Lcom/google/android/location/e/ab;

    sget-object v5, Lcom/google/android/location/e/ab;->b:Lcom/google/android/location/e/ab;

    if-eq v4, v5, :cond_81

    :cond_80
    if-eqz v29, :cond_82

    invoke-virtual/range {v29 .. v29}, Lcom/google/android/location/e/h;->f()J

    move-result-wide v4

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/google/android/location/am;->s:J

    cmp-long v4, v4, v6

    if-lez v4, :cond_82

    move-object/from16 v0, p0

    iget v4, v0, Lcom/google/android/location/am;->t:I

    const/4 v5, 0x1

    if-le v4, v5, :cond_82

    .line 723
    :cond_81
    const/4 v2, 0x4

    goto/16 :goto_3e

    .line 724
    :cond_82
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/google/android/location/am;->u:Z

    if-nez v4, :cond_83

    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/google/android/location/am;->v:Z

    if-eqz v4, :cond_83

    .line 725
    const/4 v2, 0x6

    goto/16 :goto_3e

    .line 726
    :cond_83
    if-eqz v18, :cond_85

    if-eqz p3, :cond_85

    if-eqz v3, :cond_84

    iget-object v4, v11, Lcom/google/android/location/e/ag;->c:Lcom/google/android/location/e/f;

    iget-object v4, v4, Lcom/google/android/location/e/f;->d:Lcom/google/android/location/e/ab;

    sget-object v5, Lcom/google/android/location/e/ab;->a:Lcom/google/android/location/e/ab;

    if-eq v4, v5, :cond_85

    .line 728
    :cond_84
    const/4 v2, 0x3

    goto/16 :goto_3e

    .line 729
    :cond_85
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/location/am;->K:Lcom/google/android/location/y;

    invoke-virtual {v4, v12, v13}, Lcom/google/android/location/y;->a(J)Z

    move-result v4

    if-nez v4, :cond_76

    if-eqz v3, :cond_76

    iget-object v3, v11, Lcom/google/android/location/e/ag;->c:Lcom/google/android/location/e/f;

    iget-object v3, v3, Lcom/google/android/location/e/f;->d:Lcom/google/android/location/e/ab;

    sget-object v4, Lcom/google/android/location/e/ab;->c:Lcom/google/android/location/e/ab;

    if-ne v3, v4, :cond_76

    .line 731
    const/4 v2, 0x5

    goto/16 :goto_3e

    .line 739
    :cond_86
    const/4 v2, 0x0

    move v3, v2

    goto/16 :goto_3f

    .line 743
    :cond_87
    if-eqz v18, :cond_89

    if-eqz v26, :cond_89

    if-eqz v3, :cond_88

    iget-object v4, v11, Lcom/google/android/location/e/ag;->b:Lcom/google/android/location/e/bf;

    iget-object v4, v4, Lcom/google/android/location/e/bf;->d:Lcom/google/android/location/e/ab;

    sget-object v5, Lcom/google/android/location/e/ab;->b:Lcom/google/android/location/e/ab;

    if-ne v4, v5, :cond_89

    .line 745
    :cond_88
    const/4 v2, 0x4

    goto/16 :goto_40

    .line 746
    :cond_89
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/google/android/location/am;->x:Z

    if-nez v4, :cond_8a

    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/google/android/location/am;->y:Z

    if-eqz v4, :cond_8a

    .line 747
    const/4 v2, 0x6

    goto/16 :goto_40

    .line 748
    :cond_8a
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/location/am;->K:Lcom/google/android/location/y;

    invoke-virtual {v4, v12, v13}, Lcom/google/android/location/y;->a(J)Z

    move-result v4

    if-nez v4, :cond_8b

    if-eqz v3, :cond_8b

    iget-object v4, v11, Lcom/google/android/location/e/ag;->b:Lcom/google/android/location/e/bf;

    iget-object v4, v4, Lcom/google/android/location/e/bf;->d:Lcom/google/android/location/e/ab;

    sget-object v5, Lcom/google/android/location/e/ab;->c:Lcom/google/android/location/e/ab;

    if-ne v4, v5, :cond_8b

    .line 750
    const/4 v2, 0x5

    goto/16 :goto_40

    .line 751
    :cond_8b
    if-eqz v18, :cond_78

    if-eqz p3, :cond_78

    if-eqz v3, :cond_8c

    iget-object v3, v11, Lcom/google/android/location/e/ag;->b:Lcom/google/android/location/e/bf;

    iget-object v3, v3, Lcom/google/android/location/e/bf;->d:Lcom/google/android/location/e/ab;

    sget-object v4, Lcom/google/android/location/e/ab;->a:Lcom/google/android/location/e/ab;

    if-eq v3, v4, :cond_78

    .line 753
    :cond_8c
    const/4 v2, 0x3

    goto/16 :goto_40

    :cond_8d
    move v2, v4

    goto/16 :goto_24

    :cond_8e
    move v4, v7

    goto/16 :goto_29

    :cond_8f
    move-object v2, v3

    goto/16 :goto_16

    :cond_90
    move-object v3, v4

    goto/16 :goto_15

    :cond_91
    move-object v8, v2

    goto/16 :goto_20
.end method

.method static synthetic a(Lcom/google/android/location/am;)Z
    .locals 1

    .prologue
    .line 61
    iget-boolean v0, p0, Lcom/google/android/location/am;->N:Z

    return v0
.end method

.method private b(I)V
    .locals 3

    .prologue
    .line 1052
    iget v0, p0, Lcom/google/android/location/am;->z:I

    if-eq v0, p1, :cond_0

    .line 1053
    iput p1, p0, Lcom/google/android/location/am;->z:I

    .line 1054
    iget-object v0, p0, Lcom/google/android/location/am;->a:Lcom/google/android/location/os/bi;

    iget v1, p0, Lcom/google/android/location/am;->z:I

    iget v2, p0, Lcom/google/android/location/am;->w:I

    invoke-interface {v0, v1, v2}, Lcom/google/android/location/os/bi;->a(II)V

    .line 1056
    :cond_0
    return-void
.end method

.method private b()Z
    .locals 4

    .prologue
    .line 258
    iget-wide v0, p0, Lcom/google/android/location/am;->p:J

    iget-wide v2, p0, Lcom/google/android/location/am;->q:J

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method final a()V
    .locals 2

    .prologue
    .line 1063
    iget-object v0, p0, Lcom/google/android/location/am;->a:Lcom/google/android/location/os/bi;

    invoke-interface {v0}, Lcom/google/android/location/os/bi;->f()Lcom/google/android/location/j/j;

    move-result-object v0

    sget-object v1, Lcom/google/android/location/j/k;->a:Lcom/google/android/location/j/k;

    invoke-interface {v0, v1}, Lcom/google/android/location/j/j;->b(Lcom/google/android/location/j/k;)V

    .line 1064
    invoke-direct {p0}, Lcom/google/android/location/am;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1065
    iget-object v0, p0, Lcom/google/android/location/am;->a:Lcom/google/android/location/os/bi;

    invoke-interface {v0}, Lcom/google/android/location/os/bi;->c()Lcom/google/android/location/j/b;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/location/j/b;->c()J

    move-result-wide v0

    .line 1066
    invoke-direct {p0, v0, v1}, Lcom/google/android/location/am;->a(J)V

    .line 1068
    :cond_0
    iget-object v0, p0, Lcom/google/android/location/am;->h:Lcom/google/android/location/g/f;

    iget-object v0, v0, Lcom/google/android/location/g/f;->a:Lcom/google/android/location/g/ae;

    iget-object v1, v0, Lcom/google/android/location/g/ae;->d:Lcom/google/t/b/b/a/d;

    if-eqz v1, :cond_1

    iget-object v1, v0, Lcom/google/android/location/g/ae;->d:Lcom/google/t/b/b/a/d;

    invoke-virtual {v1}, Lcom/google/t/b/b/a/d;->r()V

    :cond_1
    iget-object v0, v0, Lcom/google/android/location/g/ae;->b:Lcom/google/android/location/g/q;

    iget-object v0, v0, Lcom/google/android/location/g/q;->a:Lcom/google/android/location/g/s;

    invoke-interface {v0}, Lcom/google/android/location/g/s;->a()V

    .line 1069
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/location/am;->N:Z

    .line 1070
    return-void
.end method

.method public final a(Lcom/google/android/location/j/b;)V
    .locals 2

    .prologue
    .line 957
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/google/android/location/am;->a(Lcom/google/android/location/j/b;[Lcom/google/android/location/e/bi;Z)V

    .line 958
    return-void
.end method

.method public final a(Lcom/google/android/location/j/b;Lcom/google/android/location/e/h;)V
    .locals 9

    .prologue
    const/4 v1, 0x0

    const/4 v8, 0x0

    .line 968
    if-nez p2, :cond_1

    .line 969
    sget-boolean v0, Lcom/google/android/location/i/a;->b:Z

    if-eqz v0, :cond_0

    const-string v0, "NetworkLocator"

    const-string v2, "null cell state delivered"

    invoke-static {v0, v2}, Lcom/google/android/location/o/a/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 971
    :cond_0
    const-wide/16 v2, -0x1

    iput-wide v2, p0, Lcom/google/android/location/am;->s:J

    .line 980
    :goto_0
    invoke-direct {p0, p1, v1, v8}, Lcom/google/android/location/am;->a(Lcom/google/android/location/j/b;[Lcom/google/android/location/e/bi;Z)V

    .line 981
    return-void

    .line 973
    :cond_1
    iget-object v2, p0, Lcom/google/android/location/am;->r:Lcom/google/android/location/e/i;

    if-eqz p2, :cond_6

    iget-object v0, v2, Lcom/google/android/location/e/i;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_3

    invoke-virtual {p2}, Lcom/google/android/location/e/h;->f()J

    move-result-wide v4

    iget-wide v6, v2, Lcom/google/android/location/e/i;->c:J

    sub-long/2addr v4, v6

    iget-object v0, v2, Lcom/google/android/location/e/i;->b:Ljava/util/List;

    invoke-interface {v0, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/e/h;

    :goto_1
    if-eqz v0, :cond_3

    invoke-virtual {v0}, Lcom/google/android/location/e/h;->f()J

    move-result-wide v6

    cmp-long v0, v6, v4

    if-gez v0, :cond_3

    iget-object v0, v2, Lcom/google/android/location/e/i;->b:Ljava/util/List;

    invoke-interface {v0, v8}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    iget-object v0, v2, Lcom/google/android/location/e/i;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_2

    iget-object v0, v2, Lcom/google/android/location/e/i;->b:Ljava/util/List;

    invoke-interface {v0, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/e/h;

    goto :goto_1

    :cond_2
    move-object v0, v1

    goto :goto_1

    :cond_3
    iget-object v0, v2, Lcom/google/android/location/e/i;->a:Lcom/google/android/location/e/h;

    if-eqz v0, :cond_5

    iget-object v0, v2, Lcom/google/android/location/e/i;->a:Lcom/google/android/location/e/h;

    invoke-virtual {v0}, Lcom/google/android/location/e/h;->i()Z

    move-result v0

    if-eqz v0, :cond_5

    iget-object v0, v2, Lcom/google/android/location/e/i;->a:Lcom/google/android/location/e/h;

    invoke-virtual {v0, p2}, Lcom/google/android/location/e/h;->b(Lcom/google/android/location/e/h;)Z

    move-result v0

    if-nez v0, :cond_5

    iget-object v0, v2, Lcom/google/android/location/e/i;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/4 v3, 0x4

    if-lt v0, v3, :cond_4

    iget-object v0, v2, Lcom/google/android/location/e/i;->b:Ljava/util/List;

    invoke-interface {v0, v8}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    :cond_4
    iget-object v0, v2, Lcom/google/android/location/e/i;->b:Ljava/util/List;

    iget-object v3, v2, Lcom/google/android/location/e/i;->a:Lcom/google/android/location/e/h;

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_5
    iput-object p2, v2, Lcom/google/android/location/e/i;->a:Lcom/google/android/location/e/h;

    .line 974
    :cond_6
    invoke-virtual {p2}, Lcom/google/android/location/e/h;->i()Z

    move-result v0

    if-nez v0, :cond_7

    .line 975
    iget v0, p0, Lcom/google/android/location/am;->t:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/location/am;->t:I

    goto :goto_0

    .line 977
    :cond_7
    iput v8, p0, Lcom/google/android/location/am;->t:I

    goto :goto_0
.end method

.method public final a(Lcom/google/android/location/j/b;Lcom/google/android/location/e/y;Z)V
    .locals 12

    .prologue
    const-wide/16 v2, 0x0

    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 906
    invoke-interface {p1}, Lcom/google/android/location/j/b;->c()J

    move-result-wide v6

    .line 912
    iget-object v8, p0, Lcom/google/android/location/am;->g:Lcom/google/android/location/e/y;

    iget-boolean v9, p0, Lcom/google/android/location/am;->u:Z

    iget-boolean v0, p0, Lcom/google/android/location/am;->x:Z

    if-nez v9, :cond_0

    if-eqz v0, :cond_4

    :cond_0
    move v0, v4

    :goto_0
    if-eqz v0, :cond_6

    invoke-virtual {p2}, Lcom/google/android/location/e/y;->c()Z

    move-result v0

    if-eqz v0, :cond_6

    if-eqz p3, :cond_5

    move-wide v0, v2

    :goto_1
    iget-wide v10, v8, Lcom/google/android/location/e/y;->f:J

    invoke-static {v10, v11, v0, v1}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v0

    iput-wide v0, p2, Lcom/google/android/location/e/y;->f:J

    :goto_2
    if-eqz v9, :cond_8

    invoke-virtual {p2}, Lcom/google/android/location/e/y;->d()Z

    move-result v0

    if-eqz v0, :cond_8

    if-eqz p3, :cond_7

    :goto_3
    iget-wide v0, p2, Lcom/google/android/location/e/y;->e:J

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v0

    iput-wide v0, p2, Lcom/google/android/location/e/y;->e:J

    .line 916
    :goto_4
    iget-object v1, p0, Lcom/google/android/location/am;->g:Lcom/google/android/location/e/y;

    .line 917
    iput-object p2, p0, Lcom/google/android/location/am;->g:Lcom/google/android/location/e/y;

    .line 919
    iget-object v0, p0, Lcom/google/android/location/am;->F:Lcom/google/android/location/g/w;

    iget-object v2, p0, Lcom/google/android/location/am;->g:Lcom/google/android/location/e/y;

    invoke-virtual {v2}, Lcom/google/android/location/e/y;->a()Z

    move-result v2

    iput-boolean v2, v0, Lcom/google/android/location/g/w;->e:Z

    .line 920
    iget-object v0, p0, Lcom/google/android/location/am;->h:Lcom/google/android/location/g/f;

    iget-object v2, p0, Lcom/google/android/location/am;->g:Lcom/google/android/location/e/y;

    invoke-virtual {v2}, Lcom/google/android/location/e/y;->a()Z

    move-result v2

    iget-object v0, v0, Lcom/google/android/location/g/f;->a:Lcom/google/android/location/g/ae;

    iput-boolean v2, v0, Lcom/google/android/location/g/ae;->e:Z

    .line 923
    iget-object v0, p0, Lcom/google/android/location/am;->g:Lcom/google/android/location/e/y;

    iget v2, v0, Lcom/google/android/location/e/y;->c:I

    if-eqz v2, :cond_9

    iget v0, v0, Lcom/google/android/location/e/y;->c:I

    const v2, 0x7fffffff

    if-eq v0, v2, :cond_9

    move v0, v4

    .line 926
    :goto_5
    if-eqz v0, :cond_a

    iget-object v0, p0, Lcom/google/android/location/am;->a:Lcom/google/android/location/os/bi;

    invoke-interface {v0}, Lcom/google/android/location/os/bi;->g()Lcom/google/android/location/os/bn;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/location/os/bn;->a()Z

    move-result v0

    if-eqz v0, :cond_a

    move v0, v4

    :goto_6
    iput-boolean v0, p0, Lcom/google/android/location/am;->D:Z

    .line 927
    iget-object v0, p0, Lcom/google/android/location/am;->g:Lcom/google/android/location/e/y;

    invoke-virtual {v0}, Lcom/google/android/location/e/y;->b()J

    move-result-wide v2

    .line 928
    iget-object v0, p0, Lcom/google/android/location/am;->h:Lcom/google/android/location/g/f;

    iget-boolean v8, p0, Lcom/google/android/location/am;->D:Z

    add-long/2addr v6, v2

    sget-boolean v9, Lcom/google/android/location/i/a;->b:Z

    if-eqz v9, :cond_1

    const-string v9, "LocatorManager"

    new-instance v10, Ljava/lang/StringBuilder;

    const-string v11, "setBatchMode: newBatchMode is "

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10, v8}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, ", deadline is "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, ", batchPeriod is "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/google/android/location/o/a/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    iput-boolean v8, v0, Lcom/google/android/location/g/f;->b:Z

    iput-wide v6, v0, Lcom/google/android/location/g/f;->c:J

    iput-wide v2, v0, Lcom/google/android/location/g/f;->d:J

    .line 931
    iget-boolean v0, p0, Lcom/google/android/location/am;->D:Z

    if-eqz v0, :cond_3

    .line 932
    if-eqz v1, :cond_2

    .line 934
    iget-object v0, p0, Lcom/google/android/location/am;->a:Lcom/google/android/location/os/bi;

    invoke-interface {v0}, Lcom/google/android/location/os/bi;->g()Lcom/google/android/location/os/bn;

    move-result-object v0

    invoke-virtual {v1}, Lcom/google/android/location/e/y;->b()J

    move-result-wide v6

    iget-object v1, v1, Lcom/google/android/location/e/y;->d:Lcom/google/android/location/o/n;

    invoke-interface {v0, v5, v6, v7}, Lcom/google/android/location/os/bn;->a(ZJ)Z

    .line 939
    :cond_2
    iget-object v0, p0, Lcom/google/android/location/am;->a:Lcom/google/android/location/os/bi;

    invoke-interface {v0}, Lcom/google/android/location/os/bi;->g()Lcom/google/android/location/os/bn;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/location/am;->g:Lcom/google/android/location/e/y;

    iget-object v1, v1, Lcom/google/android/location/e/y;->d:Lcom/google/android/location/o/n;

    invoke-interface {v0, v4, v2, v3}, Lcom/google/android/location/os/bn;->a(ZJ)Z

    .line 948
    :cond_3
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0, v5}, Lcom/google/android/location/am;->a(Lcom/google/android/location/j/b;[Lcom/google/android/location/e/bi;Z)V

    .line 949
    return-void

    :cond_4
    move v0, v5

    .line 912
    goto/16 :goto_0

    :cond_5
    invoke-virtual {p2, v6, v7}, Lcom/google/android/location/e/y;->a(J)J

    move-result-wide v0

    goto/16 :goto_1

    :cond_6
    iget-wide v0, v8, Lcom/google/android/location/e/y;->f:J

    iput-wide v0, p2, Lcom/google/android/location/e/y;->f:J

    goto/16 :goto_2

    :cond_7
    invoke-virtual {p2, v6, v7}, Lcom/google/android/location/e/y;->b(J)J

    move-result-wide v2

    goto/16 :goto_3

    :cond_8
    iget-wide v0, v8, Lcom/google/android/location/e/y;->e:J

    iput-wide v0, p2, Lcom/google/android/location/e/y;->e:J

    goto/16 :goto_4

    :cond_9
    move v0, v5

    .line 923
    goto/16 :goto_5

    :cond_a
    move v0, v5

    .line 926
    goto/16 :goto_6
.end method

.method public final a(Lcom/google/android/location/j/b;Z)V
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 952
    iput-boolean v0, p0, Lcom/google/android/location/am;->v:Z

    .line 953
    if-nez p2, :cond_0

    :goto_0
    iget-boolean v1, p0, Lcom/google/android/location/am;->x:Z

    invoke-direct {p0, p1, v0, v1}, Lcom/google/android/location/am;->a(Lcom/google/android/location/j/b;ZZ)V

    .line 954
    return-void

    .line 953
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Lcom/google/android/location/j/b;[Lcom/google/android/location/e/bi;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1000
    if-eqz p2, :cond_0

    array-length v0, p2

    if-eqz v0, :cond_0

    aget-object v0, p2, v1

    if-nez v0, :cond_1

    .line 1015
    :cond_0
    :goto_0
    return-void

    .line 1014
    :cond_1
    invoke-direct {p0, p1, p2, v1}, Lcom/google/android/location/am;->a(Lcom/google/android/location/j/b;[Lcom/google/android/location/e/bi;Z)V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/location/os/aw;)V
    .locals 0

    .prologue
    .line 961
    iput-object p1, p0, Lcom/google/android/location/am;->M:Lcom/google/android/location/os/aw;

    .line 962
    return-void
.end method

.method public final a(ZZI)V
    .locals 2

    .prologue
    .line 1023
    iget-object v0, p0, Lcom/google/android/location/am;->K:Lcom/google/android/location/y;

    iget-boolean v1, v0, Lcom/google/android/location/y;->e:Z

    if-eqz v1, :cond_0

    iget v1, v0, Lcom/google/android/location/y;->f:I

    if-eq v1, p3, :cond_1

    :cond_0
    invoke-virtual {v0}, Lcom/google/android/location/y;->a()V

    :cond_1
    iput-boolean p1, v0, Lcom/google/android/location/y;->e:Z

    iput-boolean p2, v0, Lcom/google/android/location/y;->d:Z

    if-eqz p1, :cond_2

    iput p3, v0, Lcom/google/android/location/y;->f:I

    :cond_2
    sget-boolean v1, Lcom/google/android/location/i/a;->b:Z

    if-eqz v1, :cond_3

    const-string v1, "GlsFailureTracker"

    invoke-virtual {v0}, Lcom/google/android/location/y;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/google/android/location/o/a/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1024
    :cond_3
    return-void
.end method

.method public final b(Lcom/google/android/location/j/b;)V
    .locals 2

    .prologue
    .line 989
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-direct {p0, p1, v0, v1}, Lcom/google/android/location/am;->a(Lcom/google/android/location/j/b;[Lcom/google/android/location/e/bi;Z)V

    .line 990
    return-void
.end method

.method public final b(Lcom/google/android/location/j/b;Z)V
    .locals 1

    .prologue
    .line 1018
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/location/am;->y:Z

    .line 1019
    iget-boolean v0, p0, Lcom/google/android/location/am;->u:Z

    invoke-direct {p0, p1, v0, p2}, Lcom/google/android/location/am;->a(Lcom/google/android/location/j/b;ZZ)V

    .line 1020
    return-void
.end method
