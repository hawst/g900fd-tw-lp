.class public final Lcom/google/android/location/copresence/r/ah;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x10
.end annotation


# instance fields
.field final a:Landroid/net/wifi/p2p/WifiP2pManager;

.field b:Landroid/net/wifi/p2p/WifiP2pDevice;

.field c:Landroid/net/wifi/p2p/WifiP2pInfo;

.field d:Landroid/net/wifi/p2p/WifiP2pGroup;

.field e:Landroid/net/wifi/p2p/WifiP2pManager$Channel;

.field f:Lcom/google/android/location/copresence/r/ak;

.field private final g:Landroid/content/Context;

.field private final h:Landroid/content/BroadcastReceiver;

.field private final i:Landroid/net/wifi/p2p/WifiP2pManager$ChannelListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/net/wifi/p2p/WifiP2pManager;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 101
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 57
    new-instance v0, Lcom/google/android/location/copresence/r/ai;

    invoke-direct {v0, p0}, Lcom/google/android/location/copresence/r/ai;-><init>(Lcom/google/android/location/copresence/r/ah;)V

    iput-object v0, p0, Lcom/google/android/location/copresence/r/ah;->h:Landroid/content/BroadcastReceiver;

    .line 85
    new-instance v0, Lcom/google/android/location/copresence/r/aj;

    invoke-direct {v0, p0}, Lcom/google/android/location/copresence/r/aj;-><init>(Lcom/google/android/location/copresence/r/ah;)V

    iput-object v0, p0, Lcom/google/android/location/copresence/r/ah;->i:Landroid/net/wifi/p2p/WifiP2pManager$ChannelListener;

    .line 95
    new-instance v0, Landroid/net/wifi/p2p/WifiP2pInfo;

    invoke-direct {v0}, Landroid/net/wifi/p2p/WifiP2pInfo;-><init>()V

    iput-object v0, p0, Lcom/google/android/location/copresence/r/ah;->c:Landroid/net/wifi/p2p/WifiP2pInfo;

    .line 96
    iput-object v4, p0, Lcom/google/android/location/copresence/r/ah;->d:Landroid/net/wifi/p2p/WifiP2pGroup;

    .line 102
    iput-object p1, p0, Lcom/google/android/location/copresence/r/ah;->g:Landroid/content/Context;

    .line 103
    iput-object p2, p0, Lcom/google/android/location/copresence/r/ah;->a:Landroid/net/wifi/p2p/WifiP2pManager;

    .line 104
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 105
    const-string v1, "android.net.wifi.p2p.THIS_DEVICE_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 106
    const-string v1, "android.net.wifi.p2p.CONNECTION_STATE_CHANGE"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 107
    iget-object v1, p0, Lcom/google/android/location/copresence/r/ah;->g:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/location/copresence/r/ah;->h:Landroid/content/BroadcastReceiver;

    iget-object v3, p0, Lcom/google/android/location/copresence/r/ah;->g:Landroid/content/Context;

    invoke-static {v3}, Lcom/google/android/location/copresence/ap;->a(Landroid/content/Context;)Lcom/google/android/location/copresence/ap;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/location/copresence/ap;->d()Landroid/os/Handler;

    move-result-object v3

    invoke-virtual {v1, v2, v0, v4, v3}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;Ljava/lang/String;Landroid/os/Handler;)Landroid/content/Intent;

    .line 109
    return-void
.end method

.method private static b(Ljava/lang/String;)Landroid/net/wifi/p2p/WifiP2pGroup;
    .locals 6

    .prologue
    const/4 v5, 0x6

    .line 218
    new-instance v1, Landroid/net/wifi/p2p/WifiP2pGroup;

    invoke-direct {v1}, Landroid/net/wifi/p2p/WifiP2pGroup;-><init>()V

    .line 220
    :try_start_0
    const-class v0, Ljava/lang/Void;

    const-string v2, "setNetworkId"

    sget-object v3, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    const/4 v4, 0x0

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-static {v1, v0, v2, v3, v4}, Lcom/google/android/location/copresence/bd;->a(Ljava/lang/Object;Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Class;Ljava/lang/Object;)Ljava/lang/Object;

    .line 222
    const-class v0, Ljava/lang/Void;

    const-string v2, "setIsGroupOwner"

    sget-object v3, Ljava/lang/Boolean;->TYPE:Ljava/lang/Class;

    const/4 v4, 0x1

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-static {v1, v0, v2, v3, v4}, Lcom/google/android/location/copresence/bd;->a(Ljava/lang/Object;Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Class;Ljava/lang/Object;)Ljava/lang/Object;

    .line 223
    const-class v0, Ljava/lang/Void;

    const-string v2, "setInterface"

    const-class v3, Ljava/lang/String;

    invoke-static {v1, v0, v2, v3, p0}, Lcom/google/android/location/copresence/bd;->a(Ljava/lang/Object;Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Class;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Lcom/google/android/location/copresence/be; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_1

    .line 234
    :cond_0
    :goto_0
    return-object v1

    .line 224
    :catch_0
    move-exception v0

    .line 225
    invoke-static {v5}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 226
    const-string v2, "WifiP2pManager: Failed to invoke method: "

    invoke-static {v2, v0}, Lcom/google/android/location/copresence/ag;->a(Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 228
    :catch_1
    move-exception v0

    .line 229
    invoke-static {v5}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 230
    const-string v2, "WifiP2pManager: Failed to invoke method: "

    invoke-static {v2, v0}, Lcom/google/android/location/copresence/ag;->a(Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method


# virtual methods
.method final a()V
    .locals 4

    .prologue
    .line 112
    iget-object v0, p0, Lcom/google/android/location/copresence/r/ah;->e:Landroid/net/wifi/p2p/WifiP2pManager$Channel;

    if-nez v0, :cond_1

    .line 113
    const/4 v0, 0x2

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 114
    const-string v0, "WifiP2pManager: Initializing to create channel."

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->a(Ljava/lang/String;)I

    .line 116
    :cond_0
    iget-object v0, p0, Lcom/google/android/location/copresence/r/ah;->a:Landroid/net/wifi/p2p/WifiP2pManager;

    iget-object v1, p0, Lcom/google/android/location/copresence/r/ah;->g:Landroid/content/Context;

    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/location/copresence/r/ah;->i:Landroid/net/wifi/p2p/WifiP2pManager$ChannelListener;

    invoke-virtual {v0, v1, v2, v3}, Landroid/net/wifi/p2p/WifiP2pManager;->initialize(Landroid/content/Context;Landroid/os/Looper;Landroid/net/wifi/p2p/WifiP2pManager$ChannelListener;)Landroid/net/wifi/p2p/WifiP2pManager$Channel;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/copresence/r/ah;->e:Landroid/net/wifi/p2p/WifiP2pManager$Channel;

    .line 118
    :cond_1
    return-void
.end method

.method final a(Landroid/net/wifi/p2p/WifiP2pManager$GroupInfoListener;)V
    .locals 2

    .prologue
    .line 242
    invoke-virtual {p0}, Lcom/google/android/location/copresence/r/ah;->a()V

    .line 243
    const/4 v0, 0x2

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 244
    const-string v0, "WifiP2pManager: requestGroupInfo."

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->a(Ljava/lang/String;)I

    .line 246
    :cond_0
    iget-object v0, p0, Lcom/google/android/location/copresence/r/ah;->a:Landroid/net/wifi/p2p/WifiP2pManager;

    iget-object v1, p0, Lcom/google/android/location/copresence/r/ah;->e:Landroid/net/wifi/p2p/WifiP2pManager$Channel;

    invoke-virtual {v0, v1, p1}, Landroid/net/wifi/p2p/WifiP2pManager;->requestGroupInfo(Landroid/net/wifi/p2p/WifiP2pManager$Channel;Landroid/net/wifi/p2p/WifiP2pManager$GroupInfoListener;)V

    .line 247
    return-void
.end method

.method final a(Landroid/os/Message;)V
    .locals 4

    .prologue
    const/4 v3, 0x6

    .line 310
    invoke-virtual {p0}, Lcom/google/android/location/copresence/r/ah;->a()V

    .line 312
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x15

    if-lt v0, v1, :cond_1

    const-string v0, "getP2pStateMachineMessenger"

    move-object v1, v0

    .line 314
    :goto_0
    const/4 v0, 0x2

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 315
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "WifiP2pManager: Getting messenger via "

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "()"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->a(Ljava/lang/String;)I

    .line 318
    :cond_0
    :try_start_0
    iget-object v0, p0, Lcom/google/android/location/copresence/r/ah;->a:Landroid/net/wifi/p2p/WifiP2pManager;

    const-class v2, Landroid/os/Messenger;

    invoke-static {v0, v2, v1}, Lcom/google/android/location/copresence/bd;->a(Ljava/lang/Object;Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Messenger;

    .line 319
    invoke-virtual {v0, p1}, Landroid/os/Messenger;->send(Landroid/os/Message;)V
    :try_end_0
    .catch Lcom/google/android/location/copresence/be; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_1

    .line 330
    return-void

    .line 312
    :cond_1
    const-string v0, "getMessenger"

    move-object v1, v0

    goto :goto_0

    .line 320
    :catch_0
    move-exception v0

    .line 321
    invoke-static {v3}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 322
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "WifiP2pManager: Unable to access "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "()"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/location/copresence/ag;->e(Ljava/lang/String;)I

    .line 324
    :cond_2
    new-instance v1, Landroid/os/RemoteException;

    invoke-virtual {v0}, Lcom/google/android/location/copresence/be;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Landroid/os/RemoteException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 325
    :catch_1
    move-exception v0

    .line 326
    invoke-static {v3}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 327
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "WifiP2pManager: Unable to access "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "()"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/location/copresence/ag;->e(Ljava/lang/String;)I

    .line 329
    :cond_3
    new-instance v1, Landroid/os/RemoteException;

    invoke-virtual {v0}, Ljava/lang/SecurityException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Landroid/os/RemoteException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method final a(Ljava/lang/String;)Z
    .locals 2

    .prologue
    .line 199
    const/4 v0, 0x2

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 200
    const-string v0, "WifiP2pManager: Remove unconnectable group."

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->a(Ljava/lang/String;)I

    .line 202
    :cond_0
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v0

    .line 204
    const v1, 0x2401d

    iput v1, v0, Landroid/os/Message;->what:I

    .line 205
    invoke-static {p1}, Lcom/google/android/location/copresence/r/ah;->b(Ljava/lang/String;)Landroid/net/wifi/p2p/WifiP2pGroup;

    move-result-object v1

    iput-object v1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 207
    :try_start_0
    invoke-virtual {p0, v0}, Lcom/google/android/location/copresence/r/ah;->a(Landroid/os/Message;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 208
    const/4 v0, 0x1

    .line 213
    :goto_0
    return v0

    .line 209
    :catch_0
    move-exception v0

    .line 210
    const/4 v1, 0x6

    invoke-static {v1}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 211
    const-string v1, "WifiP2pManager: Failed to remove unconnectable group."

    invoke-static {v1, v0}, Lcom/google/android/location/copresence/ag;->a(Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 213
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method final b()Z
    .locals 2

    .prologue
    .line 174
    const/4 v0, 0x2

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 175
    const-string v0, "WifiP2pManager: Making group unconnectable."

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->a(Ljava/lang/String;)I

    .line 177
    :cond_0
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v0

    .line 178
    const v1, 0x2401e

    iput v1, v0, Landroid/os/Message;->what:I

    .line 180
    :try_start_0
    invoke-virtual {p0, v0}, Lcom/google/android/location/copresence/r/ah;->a(Landroid/os/Message;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 187
    const/4 v0, 0x1

    :goto_0
    return v0

    .line 181
    :catch_0
    move-exception v0

    .line 182
    const/4 v1, 0x6

    invoke-static {v1}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 183
    const-string v1, "WifiP2pManager: Unable to send message to WifiP2pService."

    invoke-static {v1, v0}, Lcom/google/android/location/copresence/ag;->a(Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 185
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method final c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 283
    iget-object v0, p0, Lcom/google/android/location/copresence/r/ah;->b:Landroid/net/wifi/p2p/WifiP2pDevice;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/location/copresence/r/ah;->b:Landroid/net/wifi/p2p/WifiP2pDevice;

    iget-object v0, v0, Landroid/net/wifi/p2p/WifiP2pDevice;->deviceName:Ljava/lang/String;

    goto :goto_0
.end method
