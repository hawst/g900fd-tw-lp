.class final Lcom/google/android/location/copresence/o/g;
.super Landroid/database/sqlite/SQLiteOpenHelper;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/location/copresence/o/d;

.field private final b:Ljava/io/File;


# direct methods
.method public constructor <init>(Lcom/google/android/location/copresence/o/d;Landroid/content/Context;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 68
    iput-object p1, p0, Lcom/google/android/location/copresence/o/g;->a:Lcom/google/android/location/copresence/o/d;

    .line 69
    const/4 v0, 0x0

    const/4 v1, 0x2

    invoke-direct {p0, p2, p3, v0, v1}, Landroid/database/sqlite/SQLiteOpenHelper;-><init>(Landroid/content/Context;Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase$CursorFactory;I)V

    .line 70
    invoke-virtual {p2, p3}, Landroid/content/Context;->getDatabasePath(Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/copresence/o/g;->b:Ljava/io/File;

    .line 71
    return-void
.end method

.method private static a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 95
    invoke-static {p1}, Lcom/google/android/location/copresence/q/af;->a(Ljava/lang/String;)Lcom/google/android/location/copresence/q/aj;

    move-result-object v0

    const-string v1, "_id"

    const-string v2, "INTEGER PRIMARY KEY AUTOINCREMENT"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/location/copresence/q/aj;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/location/copresence/q/aj;

    move-result-object v0

    const-string v1, "op_code"

    const-string v2, "INTEGER NOT NULL"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/location/copresence/q/aj;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/location/copresence/q/aj;

    move-result-object v0

    const-string v1, "write_time_millis"

    const-string v2, "INTEGER NOT NULL"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/location/copresence/q/aj;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/location/copresence/q/aj;

    move-result-object v0

    const-string v1, "package_id"

    sget-object v2, Lcom/google/android/location/copresence/o/t;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/location/copresence/q/aj;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/location/copresence/q/aj;

    move-result-object v0

    const-string v1, "account_id"

    sget-object v2, Lcom/google/android/location/copresence/o/t;->b:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/location/copresence/q/aj;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/location/copresence/q/aj;

    move-result-object v0

    const-string v1, "client_id"

    const-string v2, "TEXT NOT NULL"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/location/copresence/q/aj;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/location/copresence/q/aj;

    move-result-object v0

    const-string v1, "expiration_time"

    const-string v2, "INTEGER"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/location/copresence/q/aj;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/location/copresence/q/aj;

    move-result-object v0

    const-string v1, "network_status"

    const-string v2, "INTEGER NOT NULL"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/location/copresence/q/aj;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/location/copresence/q/aj;

    move-result-object v0

    const-string v1, "network_last_update_millis"

    const-string v2, "INTEGER NOT NULL"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/location/copresence/q/aj;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/location/copresence/q/aj;

    move-result-object v0

    const-string v1, "deletable"

    const-string v2, "INTEGER NOT NULL"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/location/copresence/q/aj;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/location/copresence/q/aj;

    move-result-object v0

    const-string v1, "proto"

    const-string v2, "BLOB"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/location/copresence/q/aj;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/location/copresence/q/aj;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/location/copresence/q/aj;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 110
    return-void
.end method


# virtual methods
.method public final getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;
    .locals 2

    .prologue
    const/4 v1, 0x6

    .line 191
    :try_start_0
    invoke-super {p0}, Landroid/database/sqlite/SQLiteOpenHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;
    :try_end_0
    .catch Lcom/google/android/location/copresence/o/e; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/google/android/location/copresence/o/f; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v0

    .line 208
    :goto_0
    return-object v0

    .line 192
    :catch_0
    move-exception v0

    .line 194
    invoke-static {v1}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 195
    const-string v1, "CopresenceStateDatabase: Failed to downgrade db."

    invoke-static {v1, v0}, Lcom/google/android/location/copresence/ag;->a(Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 205
    :cond_0
    :goto_1
    iget-object v0, p0, Lcom/google/android/location/copresence/o/g;->b:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    .line 208
    invoke-super {p0}, Landroid/database/sqlite/SQLiteOpenHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    goto :goto_0

    .line 197
    :catch_1
    move-exception v0

    .line 199
    invoke-static {v1}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 200
    const-string v1, "CopresenceStateDatabase: Failed to upgrade db."

    invoke-static {v1, v0}, Lcom/google/android/location/copresence/ag;->a(Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1
.end method

.method public final getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;
    .locals 2

    .prologue
    const/4 v1, 0x6

    .line 168
    :try_start_0
    invoke-super {p0}, Landroid/database/sqlite/SQLiteOpenHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;
    :try_end_0
    .catch Lcom/google/android/location/copresence/o/e; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/google/android/location/copresence/o/f; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v0

    .line 185
    :goto_0
    return-object v0

    .line 169
    :catch_0
    move-exception v0

    .line 171
    invoke-static {v1}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 172
    const-string v1, "CopresenceStateDatabase: Failed to downgrade db."

    invoke-static {v1, v0}, Lcom/google/android/location/copresence/ag;->a(Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 182
    :cond_0
    :goto_1
    iget-object v0, p0, Lcom/google/android/location/copresence/o/g;->b:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    .line 185
    invoke-super {p0}, Landroid/database/sqlite/SQLiteOpenHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    goto :goto_0

    .line 174
    :catch_1
    move-exception v0

    .line 176
    invoke-static {v1}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 177
    const-string v1, "CopresenceStateDatabase: Failed to upgrade db."

    invoke-static {v1, v0}, Lcom/google/android/location/copresence/ag;->a(Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1
.end method

.method public final onCreate(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 3

    .prologue
    .line 75
    const-string v0, "packages"

    invoke-static {v0}, Lcom/google/android/location/copresence/q/af;->a(Ljava/lang/String;)Lcom/google/android/location/copresence/q/aj;

    move-result-object v0

    const-string v1, "_id"

    const-string v2, "INTEGER PRIMARY KEY AUTOINCREMENT"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/location/copresence/q/aj;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/location/copresence/q/aj;

    move-result-object v0

    const-string v1, "name"

    const-string v2, "TEXT UNIQUE NOT NULL"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/location/copresence/q/aj;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/location/copresence/q/aj;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/location/copresence/q/aj;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v0, "accounts"

    invoke-static {v0}, Lcom/google/android/location/copresence/q/af;->a(Ljava/lang/String;)Lcom/google/android/location/copresence/q/aj;

    move-result-object v0

    const-string v1, "_id"

    const-string v2, "INTEGER PRIMARY KEY AUTOINCREMENT"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/location/copresence/q/aj;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/location/copresence/q/aj;

    move-result-object v0

    const-string v1, "name"

    const-string v2, "TEXT UNIQUE NOT NULL"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/location/copresence/q/aj;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/location/copresence/q/aj;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/location/copresence/q/aj;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v0, "operations"

    invoke-static {p1, v0}, Lcom/google/android/location/copresence/o/g;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)V

    .line 76
    const-string v0, "operations"

    const-string v1, "operations_id_index"

    invoke-static {v0, v1}, Lcom/google/android/location/copresence/q/af;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/location/copresence/q/ai;

    move-result-object v0

    const-string v1, "_id"

    invoke-virtual {v0, v1}, Lcom/google/android/location/copresence/q/ai;->a(Ljava/lang/String;)Lcom/google/android/location/copresence/q/ai;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/location/copresence/q/ai;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v0, "operations"

    const-string v1, "operations_package_id_index"

    invoke-static {v0, v1}, Lcom/google/android/location/copresence/q/af;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/location/copresence/q/ai;

    move-result-object v0

    const-string v1, "package_id"

    invoke-virtual {v0, v1}, Lcom/google/android/location/copresence/q/ai;->a(Ljava/lang/String;)Lcom/google/android/location/copresence/q/ai;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/location/copresence/q/ai;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v0, "operations"

    const-string v1, "operations_account_id_index"

    invoke-static {v0, v1}, Lcom/google/android/location/copresence/q/af;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/location/copresence/q/ai;

    move-result-object v0

    const-string v1, "account_id"

    invoke-virtual {v0, v1}, Lcom/google/android/location/copresence/q/ai;->a(Ljava/lang/String;)Lcom/google/android/location/copresence/q/ai;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/location/copresence/q/ai;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v0, "operations"

    const-string v1, "operations_op_code_index"

    invoke-static {v0, v1}, Lcom/google/android/location/copresence/q/af;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/location/copresence/q/ai;

    move-result-object v0

    const-string v1, "op_code"

    invoke-virtual {v0, v1}, Lcom/google/android/location/copresence/q/ai;->a(Ljava/lang/String;)Lcom/google/android/location/copresence/q/ai;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/location/copresence/q/ai;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v0, "operations"

    const-string v1, "operations_deletable_index"

    invoke-static {v0, v1}, Lcom/google/android/location/copresence/q/af;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/location/copresence/q/ai;

    move-result-object v0

    const-string v1, "deletable"

    invoke-virtual {v0, v1}, Lcom/google/android/location/copresence/q/ai;->a(Ljava/lang/String;)Lcom/google/android/location/copresence/q/ai;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/location/copresence/q/ai;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v0, "operations"

    const-string v1, "operations_network_status_index"

    invoke-static {v0, v1}, Lcom/google/android/location/copresence/q/af;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/location/copresence/q/ai;

    move-result-object v0

    const-string v1, "network_status"

    invoke-virtual {v0, v1}, Lcom/google/android/location/copresence/q/ai;->a(Ljava/lang/String;)Lcom/google/android/location/copresence/q/ai;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/location/copresence/q/ai;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 77
    return-void
.end method

.method public final onDowngrade(Landroid/database/sqlite/SQLiteDatabase;II)V
    .locals 3

    .prologue
    .line 151
    new-instance v0, Lcom/google/android/location/copresence/o/e;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Downgrade from "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " to "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/location/copresence/o/e;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final onOpen(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 2

    .prologue
    .line 158
    const/4 v0, 0x3

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 159
    const-string v0, "CopresenceStateDatabase: caching packages, accounts, and namepaces"

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->b(Ljava/lang/String;)I

    .line 161
    :cond_0
    iget-object v0, p0, Lcom/google/android/location/copresence/o/g;->a:Lcom/google/android/location/copresence/o/d;

    const-string v0, "packages"

    iget-object v1, p0, Lcom/google/android/location/copresence/o/g;->a:Lcom/google/android/location/copresence/o/d;

    iget-object v1, v1, Lcom/google/android/location/copresence/o/d;->c:Ljava/util/HashMap;

    invoke-static {p1, v0, v1}, Lcom/google/android/location/copresence/o/d;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/util/HashMap;)V

    .line 162
    iget-object v0, p0, Lcom/google/android/location/copresence/o/g;->a:Lcom/google/android/location/copresence/o/d;

    const-string v0, "accounts"

    iget-object v1, p0, Lcom/google/android/location/copresence/o/g;->a:Lcom/google/android/location/copresence/o/d;

    iget-object v1, v1, Lcom/google/android/location/copresence/o/d;->d:Ljava/util/HashMap;

    invoke-static {p1, v0, v1}, Lcom/google/android/location/copresence/o/d;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/util/HashMap;)V

    .line 163
    return-void
.end method

.method public final onUpgrade(Landroid/database/sqlite/SQLiteDatabase;II)V
    .locals 17

    .prologue
    .line 140
    const/4 v2, 0x1

    move/from16 v0, p2

    if-ne v0, v2, :cond_2

    const/4 v2, 0x2

    move/from16 v0, p3

    if-ne v0, v2, :cond_2

    .line 141
    :try_start_0
    const-string v2, "access_policies"

    invoke-static {v2}, Lcom/google/android/location/copresence/q/af;->c(Ljava/lang/String;)Lcom/google/android/location/copresence/q/ah;

    move-result-object v2

    iget-object v2, v2, Lcom/google/android/location/copresence/q/ah;->a:Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v2, "publications"

    invoke-static {v2}, Lcom/google/android/location/copresence/q/af;->c(Ljava/lang/String;)Lcom/google/android/location/copresence/q/ah;

    move-result-object v2

    iget-object v2, v2, Lcom/google/android/location/copresence/q/ah;->a:Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v2, "subscriptions"

    invoke-static {v2}, Lcom/google/android/location/copresence/q/af;->c(Ljava/lang/String;)Lcom/google/android/location/copresence/q/ah;

    move-result-object v2

    iget-object v2, v2, Lcom/google/android/location/copresence/q/ah;->a:Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v2, "temp_operations"

    move-object/from16 v0, p1

    invoke-static {v0, v2}, Lcom/google/android/location/copresence/o/g;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)V

    const-string v3, "operations"

    sget-object v4, Lcom/google/android/location/copresence/o/t;->e:[Ljava/lang/String;

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const-string v2, "_id"

    invoke-static {v2}, Lcom/google/android/location/copresence/q/af;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v9}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v3

    :goto_0
    :try_start_1
    invoke-interface {v3}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v2, 0x1

    invoke-interface {v3, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    const/4 v4, 0x2

    invoke-interface {v3, v4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    const/4 v6, 0x3

    invoke-interface {v3, v6}, Landroid/database/Cursor;->getInt(I)I

    move-result v6

    const/4 v7, 0x4

    invoke-interface {v3, v7}, Landroid/database/Cursor;->getInt(I)I

    move-result v7

    const/4 v8, 0x5

    invoke-interface {v3, v8}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    const/16 v9, 0x8

    invoke-interface {v3, v9}, Landroid/database/Cursor;->getInt(I)I

    move-result v9

    const/16 v10, 0x9

    invoke-interface {v3, v10}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v10

    const/16 v12, 0xa

    invoke-interface {v3, v12}, Landroid/database/Cursor;->getInt(I)I

    move-result v12

    const/16 v13, 0xb

    invoke-interface {v3, v13}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v13

    invoke-static {v2, v4, v5, v13}, Lcom/google/android/location/copresence/o/d;->a(IJ[B)Ljava/lang/Long;

    move-result-object v14

    new-instance v15, Landroid/content/ContentValues;

    invoke-direct {v15}, Landroid/content/ContentValues;-><init>()V

    const-string v16, "op_code"

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    move-object/from16 v0, v16

    invoke-virtual {v15, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v2, "write_time_millis"

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v15, v2, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v2, "package_id"

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v15, v2, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v2, "account_id"

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v15, v2, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v2, "client_id"

    invoke-virtual {v15, v2, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    if-eqz v14, :cond_0

    const-string v2, "expiration_time"

    invoke-virtual {v15, v2, v14}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    :cond_0
    const-string v2, "network_status"

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v15, v2, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v2, "network_last_update_millis"

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v15, v2, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v2, "deletable"

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v15, v2, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v2, "proto"

    invoke-virtual {v15, v2, v13}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    const-string v2, "temp_operations"

    const-string v4, "op_code"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v4, v15}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/16 :goto_0

    :catchall_0
    move-exception v2

    :try_start_2
    invoke-interface {v3}, Landroid/database/Cursor;->close()V

    throw v2
    :try_end_2
    .catch Ljava/lang/RuntimeException; {:try_start_2 .. :try_end_2} :catch_0

    .line 144
    :catch_0
    move-exception v2

    new-instance v2, Lcom/google/android/location/copresence/o/f;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Upgrade from "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move/from16 v0, p2

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " to "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move/from16 v0, p3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/google/android/location/copresence/o/f;-><init>(Ljava/lang/String;)V

    throw v2

    .line 141
    :cond_1
    :try_start_3
    invoke-interface {v3}, Landroid/database/Cursor;->close()V

    const-string v2, "operations"

    invoke-static {v2}, Lcom/google/android/location/copresence/q/af;->c(Ljava/lang/String;)Lcom/google/android/location/copresence/q/ah;

    move-result-object v2

    iget-object v2, v2, Lcom/google/android/location/copresence/q/ah;->a:Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v2, "temp_operations"

    new-instance v3, Lcom/google/android/location/copresence/q/ag;

    const/4 v4, 0x0

    invoke-direct {v3, v2, v4}, Lcom/google/android/location/copresence/q/ag;-><init>(Ljava/lang/String;B)V

    const-string v2, "operations"

    iget-object v4, v3, Lcom/google/android/location/copresence/q/ag;->a:Ljava/lang/StringBuilder;

    const-string v5, " RENAME TO "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, v3, Lcom/google/android/location/copresence/q/ag;->a:Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V
    :try_end_3
    .catch Ljava/lang/RuntimeException; {:try_start_3 .. :try_end_3} :catch_0

    .line 146
    :cond_2
    return-void
.end method
