.class final Lcom/google/android/location/fused/x;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/q/a/b/b/z;


# instance fields
.field final synthetic a:Lcom/google/android/location/fused/a;

.field final synthetic b:Lcom/google/android/location/fused/w;


# direct methods
.method constructor <init>(Lcom/google/android/location/fused/w;Lcom/google/android/location/fused/a;)V
    .locals 0

    .prologue
    .line 188
    iput-object p1, p0, Lcom/google/android/location/fused/x;->b:Lcom/google/android/location/fused/w;

    iput-object p2, p0, Lcom/google/android/location/fused/x;->a:Lcom/google/android/location/fused/a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(J)V
    .locals 9

    .prologue
    const-wide v2, 0x7fefffffffffffffL    # Double.MAX_VALUE

    .line 191
    iget-object v0, p0, Lcom/google/android/location/fused/x;->a:Lcom/google/android/location/fused/a;

    invoke-virtual {v0}, Lcom/google/android/location/fused/a;->e()D

    move-result-wide v0

    const-wide v4, 0x3ff226c3bcdbe7aeL    # 1.1344640137963142

    cmpl-double v0, v0, v4

    if-lez v0, :cond_0

    move-wide v0, v2

    .line 193
    :goto_0
    iget-object v4, p0, Lcom/google/android/location/fused/x;->a:Lcom/google/android/location/fused/a;

    invoke-virtual {v4}, Lcom/google/android/location/fused/a;->d()D

    move-result-wide v4

    .line 195
    iget-object v6, p0, Lcom/google/android/location/fused/x;->b:Lcom/google/android/location/fused/w;

    iget-object v6, v6, Lcom/google/android/location/fused/w;->b:Lcom/google/q/a/b/b/t;

    cmpl-double v7, v0, v2

    if-nez v7, :cond_1

    const v0, 0x7f7fffff    # Float.MAX_VALUE

    move v1, v0

    :goto_1
    cmpl-double v0, v4, v2

    if-nez v0, :cond_2

    const/4 v0, 0x0

    :goto_2
    invoke-interface {v6, p1, p2, v1, v0}, Lcom/google/q/a/b/b/t;->a(JFF)V

    .line 201
    return-void

    .line 191
    :cond_0
    iget-object v0, p0, Lcom/google/android/location/fused/x;->a:Lcom/google/android/location/fused/a;

    invoke-virtual {v0}, Lcom/google/android/location/fused/a;->c()D

    move-result-wide v0

    goto :goto_0

    .line 195
    :cond_1
    double-to-float v0, v0

    move v1, v0

    goto :goto_1

    :cond_2
    double-to-float v0, v4

    goto :goto_2
.end method
