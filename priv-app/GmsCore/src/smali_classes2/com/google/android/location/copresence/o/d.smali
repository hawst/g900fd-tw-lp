.class public final Lcom/google/android/location/copresence/o/d;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final a:Lcom/google/android/gms/common/util/p;

.field final b:Lcom/google/android/location/copresence/o/g;

.field final c:Ljava/util/HashMap;

.field final d:Ljava/util/HashMap;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 355
    const-string v0, "copresence_state.db"

    invoke-static {}, Lcom/google/android/gms/common/util/r;->c()Lcom/google/android/gms/common/util/p;

    move-result-object v1

    invoke-direct {p0, p1, v0, v1}, Lcom/google/android/location/copresence/o/d;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/gms/common/util/p;)V

    .line 356
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/gms/common/util/p;)V
    .locals 1

    .prologue
    .line 362
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 351
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/location/copresence/o/d;->c:Ljava/util/HashMap;

    .line 352
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/location/copresence/o/d;->d:Ljava/util/HashMap;

    .line 363
    iput-object p3, p0, Lcom/google/android/location/copresence/o/d;->a:Lcom/google/android/gms/common/util/p;

    .line 364
    new-instance v0, Lcom/google/android/location/copresence/o/g;

    invoke-direct {v0, p0, p1, p2}, Lcom/google/android/location/copresence/o/g;-><init>(Lcom/google/android/location/copresence/o/d;Landroid/content/Context;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/location/copresence/o/d;->b:Lcom/google/android/location/copresence/o/g;

    .line 365
    return-void
.end method

.method static a(JJ)J
    .locals 4

    .prologue
    const-wide/16 v0, -0x1

    .line 464
    cmp-long v2, p2, v0

    if-nez v2, :cond_0

    .line 467
    :goto_0
    return-wide v0

    :cond_0
    add-long v0, p0, p2

    goto :goto_0
.end method

.method private a(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)J
    .locals 6

    .prologue
    const-wide/16 v2, -0x1

    .line 1047
    invoke-virtual {p1}, Ljava/util/HashMap;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1050
    iget-object v0, p0, Lcom/google/android/location/copresence/o/d;->b:Lcom/google/android/location/copresence/o/g;

    invoke-virtual {v0}, Lcom/google/android/location/copresence/o/g;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    .line 1052
    :cond_0
    invoke-virtual {p1, p3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    .line 1053
    if-eqz v0, :cond_1

    .line 1054
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    .line 1065
    :goto_0
    return-wide v0

    .line 1056
    :cond_1
    iget-object v0, p0, Lcom/google/android/location/copresence/o/d;->b:Lcom/google/android/location/copresence/o/g;

    invoke-virtual {v0}, Lcom/google/android/location/copresence/o/g;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 1057
    const/4 v1, 0x0

    new-instance v4, Landroid/content/ContentValues;

    invoke-direct {v4}, Landroid/content/ContentValues;-><init>()V

    const-string v5, "name"

    invoke-virtual {v4, v5, p3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, p2, v1, v4}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    .line 1058
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    cmp-long v1, v4, v2

    if-nez v1, :cond_3

    .line 1059
    const/4 v0, 0x6

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1060
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "CopresenceStateDatabase: Could not insert name="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " to table "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->e(Ljava/lang/String;)I

    :cond_2
    move-wide v0, v2

    .line 1062
    goto :goto_0

    .line 1064
    :cond_3
    invoke-virtual {p1, p3, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1065
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    goto :goto_0
.end method

.method static a(IJ[B)Ljava/lang/Long;
    .locals 7

    .prologue
    const/4 v6, 0x6

    const-wide/16 v4, -0x1

    .line 281
    const-wide/32 v0, 0x493e0

    add-long v2, p1, v0

    .line 283
    packed-switch p0, :pswitch_data_0

    .line 333
    :pswitch_0
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 285
    :pswitch_1
    if-nez p3, :cond_0

    .line 286
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    goto :goto_0

    .line 290
    :cond_0
    :try_start_0
    new-instance v0, Lcom/google/ac/b/c/bf;

    invoke-direct {v0}, Lcom/google/ac/b/c/bf;-><init>()V

    invoke-static {v0, p3}, Lcom/google/protobuf/nano/j;->mergeFrom(Lcom/google/protobuf/nano/j;[B)Lcom/google/protobuf/nano/j;

    move-result-object v0

    check-cast v0, Lcom/google/ac/b/c/bf;
    :try_end_0
    .catch Lcom/google/protobuf/nano/i; {:try_start_0 .. :try_end_0} :catch_0

    .line 298
    iget-object v0, v0, Lcom/google/ac/b/c/bf;->b:Lcom/google/ac/b/c/t;

    .line 299
    if-nez v0, :cond_2

    .line 300
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    goto :goto_0

    .line 292
    :catch_0
    move-exception v0

    .line 293
    invoke-static {v6}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 294
    const-string v1, "CopresenceStateDatabase: "

    invoke-static {v1, v0}, Lcom/google/android/location/copresence/ag;->a(Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 296
    :cond_1
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    goto :goto_0

    .line 302
    :cond_2
    iget-object v0, v0, Lcom/google/ac/b/c/t;->a:Ljava/lang/Long;

    .line 303
    if-nez v0, :cond_3

    .line 304
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    goto :goto_0

    .line 306
    :cond_3
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    cmp-long v1, v2, v4

    if-nez v1, :cond_4

    .line 307
    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    goto :goto_0

    .line 309
    :cond_4
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    add-long/2addr v0, p1

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    goto :goto_0

    .line 312
    :pswitch_2
    if-nez p3, :cond_5

    .line 313
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    goto :goto_0

    .line 317
    :cond_5
    :try_start_1
    new-instance v0, Lcom/google/ac/b/c/bo;

    invoke-direct {v0}, Lcom/google/ac/b/c/bo;-><init>()V

    invoke-static {v0, p3}, Lcom/google/protobuf/nano/j;->mergeFrom(Lcom/google/protobuf/nano/j;[B)Lcom/google/protobuf/nano/j;

    move-result-object v0

    check-cast v0, Lcom/google/ac/b/c/bo;
    :try_end_1
    .catch Lcom/google/protobuf/nano/i; {:try_start_1 .. :try_end_1} :catch_1

    .line 324
    iget-object v1, v0, Lcom/google/ac/b/c/bo;->c:Ljava/lang/Long;

    if-nez v1, :cond_7

    .line 325
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    goto :goto_0

    .line 318
    :catch_1
    move-exception v0

    .line 319
    invoke-static {v6}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 320
    const-string v1, "CopresenceStateDatabase: "

    invoke-static {v1, v0}, Lcom/google/android/location/copresence/ag;->a(Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 322
    :cond_6
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    goto :goto_0

    .line 327
    :cond_7
    iget-object v1, v0, Lcom/google/ac/b/c/bo;->c:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    cmp-long v1, v2, v4

    if-nez v1, :cond_8

    .line 328
    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    goto/16 :goto_0

    .line 330
    :cond_8
    iget-object v0, v0, Lcom/google/ac/b/c/bo;->c:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    add-long/2addr v0, p1

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    goto/16 :goto_0

    .line 283
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method private static a(Ljava/util/HashMap;J)Ljava/lang/String;
    .locals 7

    .prologue
    .line 1012
    invoke-virtual {p0}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    .line 1013
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 1014
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    cmp-long v1, v4, p1

    if-nez v1, :cond_0

    .line 1015
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1018
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static a(Landroid/database/Cursor;Ljava/util/HashMap;)Ljava/util/Set;
    .locals 4

    .prologue
    .line 998
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    .line 999
    :goto_0
    invoke-interface {p0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1000
    const/4 v1, 0x0

    invoke-interface {p0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    .line 1001
    invoke-static {p1, v2, v3}, Lcom/google/android/location/copresence/o/d;->a(Ljava/util/HashMap;J)Ljava/lang/String;

    move-result-object v1

    .line 1002
    if-nez v1, :cond_1

    .line 1004
    const/4 v0, 0x0

    .line 1008
    :cond_0
    return-object v0

    .line 1006
    :cond_1
    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method static a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/util/HashMap;)V
    .locals 8

    .prologue
    const/4 v3, 0x0

    .line 378
    sget-object v2, Lcom/google/android/location/copresence/o/s;->a:[Ljava/lang/String;

    move-object v0, p0

    move-object v1, p1

    move-object v4, v3

    move-object v5, v3

    move-object v6, v3

    move-object v7, v3

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 381
    :goto_0
    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 382
    const/4 v0, 0x0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    .line 383
    const/4 v0, 0x1

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 384
    const/4 v4, 0x3

    invoke-static {v4}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 385
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "CopresenceStateDatabase: cache table="

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ": name="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " id:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/google/android/location/copresence/ag;->b(Ljava/lang/String;)I

    .line 387
    :cond_0
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {p2, v0, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 390
    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_1
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 391
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;Lcom/google/android/location/copresence/a/a;Lcom/google/android/gms/location/copresence/internal/SubscribeOperation;)Ljava/util/List;
    .locals 17

    .prologue
    .line 523
    move-object/from16 v0, p3

    iget-object v2, v0, Lcom/google/android/gms/location/copresence/internal/SubscribeOperation;->c:Lcom/google/android/gms/location/copresence/MessageFilter;

    invoke-virtual {v2}, Lcom/google/android/gms/location/copresence/MessageFilter;->b()Ljava/util/List;

    move-result-object v2

    .line 525
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/gms/location/copresence/MessageFilter$FilterPrimitive;

    .line 526
    iget-object v2, v2, Lcom/google/android/gms/location/copresence/MessageFilter$FilterPrimitive;->f:Ljava/lang/String;

    const-string v4, "Filter primitives must have id\'s before inserting a subscribe operation."

    invoke-static {v2, v4}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 530
    :cond_0
    invoke-virtual/range {p0 .. p1}, Lcom/google/android/location/copresence/o/d;->c(Ljava/lang/String;)J

    move-result-wide v4

    .line 531
    const-wide/16 v2, -0x1

    cmp-long v2, v4, v2

    if-nez v2, :cond_2

    .line 532
    const/4 v2, 0x6

    invoke-static {v2}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 533
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "CopresenceStateDatabase: Could not get package id for packageName="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p1

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " for "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p3

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/location/copresence/ag;->e(Ljava/lang/String;)I

    .line 536
    :cond_1
    const/4 v2, 0x0

    .line 586
    :goto_1
    return-object v2

    .line 539
    :cond_2
    move-object/from16 v0, p2

    iget-object v2, v0, Lcom/google/android/location/copresence/a/a;->a:Landroid/accounts/Account;

    iget-object v2, v2, Landroid/accounts/Account;->name:Ljava/lang/String;

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/google/android/location/copresence/o/d;->d(Ljava/lang/String;)J

    move-result-wide v6

    .line 540
    const-wide/16 v2, -0x1

    cmp-long v2, v6, v2

    if-nez v2, :cond_4

    .line 541
    const/4 v2, 0x6

    invoke-static {v2}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 542
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "CopresenceStateDatabase: Could not get account id for account="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static/range {p2 .. p2}, Lcom/google/android/location/copresence/ag;->a(Lcom/google/android/location/copresence/a/a;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " for "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p3

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/location/copresence/ag;->e(Ljava/lang/String;)I

    .line 545
    :cond_3
    const/4 v2, 0x0

    goto :goto_1

    .line 549
    :cond_4
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/location/copresence/o/d;->a:Lcom/google/android/gms/common/util/p;

    invoke-interface {v2}, Lcom/google/android/gms/common/util/p;->a()J

    move-result-wide v8

    .line 551
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/location/copresence/o/d;->b:Lcom/google/android/location/copresence/o/g;

    invoke-virtual {v2}, Lcom/google/android/location/copresence/o/g;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v10

    .line 552
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 553
    move-object/from16 v0, p3

    iget-object v2, v0, Lcom/google/android/gms/location/copresence/internal/SubscribeOperation;->c:Lcom/google/android/gms/location/copresence/MessageFilter;

    invoke-virtual {v2}, Lcom/google/android/gms/location/copresence/MessageFilter;->b()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v11

    :goto_2
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_8

    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/gms/location/copresence/MessageFilter$FilterPrimitive;

    .line 554
    iget-wide v12, v2, Lcom/google/android/gms/location/copresence/MessageFilter$FilterPrimitive;->d:J

    invoke-static {v8, v9, v12, v13}, Lcom/google/android/location/copresence/o/d;->a(JJ)J

    move-result-wide v12

    .line 557
    new-instance v14, Landroid/content/ContentValues;

    invoke-direct {v14}, Landroid/content/ContentValues;-><init>()V

    .line 558
    const-string v15, "op_code"

    const/16 v16, 0x2

    invoke-static/range {v16 .. v16}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v16

    invoke-virtual/range {v14 .. v16}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 559
    const-string v15, "write_time_millis"

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v16

    invoke-virtual/range {v14 .. v16}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 560
    const-string v15, "package_id"

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v16

    invoke-virtual/range {v14 .. v16}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 561
    const-string v15, "account_id"

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v16

    invoke-virtual/range {v14 .. v16}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 562
    const-string v15, "client_id"

    iget-object v0, v2, Lcom/google/android/gms/location/copresence/MessageFilter$FilterPrimitive;->f:Ljava/lang/String;

    move-object/from16 v16, v0

    invoke-virtual/range {v14 .. v16}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 563
    const-string v15, "expiration_time"

    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v12

    invoke-virtual {v14, v15, v12}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 564
    const-string v12, "network_status"

    const/4 v13, 0x0

    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    invoke-virtual {v14, v12, v13}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 565
    const-string v12, "network_last_update_millis"

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v13

    invoke-virtual {v14, v12, v13}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 566
    const-string v12, "deletable"

    const/4 v13, 0x0

    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    invoke-virtual {v14, v12, v13}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 567
    const-string v12, "proto"

    move-object/from16 v0, p3

    iget-object v13, v0, Lcom/google/android/gms/location/copresence/internal/SubscribeOperation;->b:Lcom/google/android/gms/location/copresence/internal/StrategyImpl;

    invoke-static {v13, v2}, Lcom/google/android/location/copresence/l/q;->a(Lcom/google/android/gms/location/copresence/internal/StrategyImpl;Lcom/google/android/gms/location/copresence/MessageFilter$FilterPrimitive;)Lcom/google/ac/b/c/bo;

    move-result-object v13

    invoke-static {v13}, Lcom/google/protobuf/nano/j;->toByteArray(Lcom/google/protobuf/nano/j;)[B

    move-result-object v13

    invoke-virtual {v14, v12, v13}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    .line 570
    const-string v12, "operations"

    const-string v13, "op_code"

    invoke-virtual {v10, v12, v13, v14}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v12

    .line 571
    const-wide/16 v14, -0x1

    cmp-long v14, v12, v14

    if-nez v14, :cond_7

    .line 572
    const/4 v4, 0x6

    invoke-static {v4}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 573
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "CopresenceStateDatabase: Could not add filter primitive "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, " for "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p3

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/location/copresence/ag;->e(Ljava/lang/String;)I

    .line 577
    :cond_5
    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lcom/google/android/location/copresence/o/d;->c(Ljava/util/List;)I

    move-result v2

    .line 578
    const/4 v3, 0x3

    invoke-static {v3}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v3

    if-eqz v3, :cond_6

    .line 579
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "CopresenceStateDatabase: Rolled back "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " operations."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/location/copresence/ag;->b(Ljava/lang/String;)I

    .line 581
    :cond_6
    const/4 v2, 0x0

    goto/16 :goto_1

    .line 583
    :cond_7
    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_2

    :cond_8
    move-object v2, v3

    .line 586
    goto/16 :goto_1
.end method

.method public final a(Ljava/lang/String;Lcom/google/android/location/copresence/a/a;Lcom/google/android/gms/location/copresence/internal/UnsubscribeOperation;)Ljava/util/List;
    .locals 16

    .prologue
    .line 601
    invoke-virtual/range {p3 .. p3}, Lcom/google/android/gms/location/copresence/internal/UnsubscribeOperation;->d()Ljava/util/List;

    move-result-object v2

    const-string v3, "unsubscribe operation must have subscription ids filled in"

    invoke-static {v2, v3}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 603
    invoke-virtual/range {p0 .. p1}, Lcom/google/android/location/copresence/o/d;->c(Ljava/lang/String;)J

    move-result-wide v4

    .line 604
    const-wide/16 v2, -0x1

    cmp-long v2, v4, v2

    if-nez v2, :cond_1

    .line 605
    const/4 v2, 0x6

    invoke-static {v2}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 606
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "CopresenceStateDatabase: Could not get package id for packageName="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p1

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " for "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p3

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/location/copresence/ag;->e(Ljava/lang/String;)I

    .line 609
    :cond_0
    const/4 v2, 0x0

    .line 650
    :goto_0
    return-object v2

    .line 612
    :cond_1
    move-object/from16 v0, p2

    iget-object v2, v0, Lcom/google/android/location/copresence/a/a;->a:Landroid/accounts/Account;

    iget-object v2, v2, Landroid/accounts/Account;->name:Ljava/lang/String;

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/google/android/location/copresence/o/d;->d(Ljava/lang/String;)J

    move-result-wide v6

    .line 613
    const-wide/16 v2, -0x1

    cmp-long v2, v6, v2

    if-nez v2, :cond_3

    .line 614
    const/4 v2, 0x6

    invoke-static {v2}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 615
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "CopresenceStateDatabase: Could not get account id for account="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static/range {p2 .. p2}, Lcom/google/android/location/copresence/ag;->a(Lcom/google/android/location/copresence/a/a;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " for "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p3

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/location/copresence/ag;->e(Ljava/lang/String;)I

    .line 618
    :cond_2
    const/4 v2, 0x0

    goto :goto_0

    .line 622
    :cond_3
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/location/copresence/o/d;->a:Lcom/google/android/gms/common/util/p;

    invoke-interface {v2}, Lcom/google/android/gms/common/util/p;->a()J

    move-result-wide v8

    .line 624
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/location/copresence/o/d;->b:Lcom/google/android/location/copresence/o/g;

    invoke-virtual {v2}, Lcom/google/android/location/copresence/o/g;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v10

    .line 625
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 626
    invoke-virtual/range {p3 .. p3}, Lcom/google/android/gms/location/copresence/internal/UnsubscribeOperation;->d()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v11

    :goto_1
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_6

    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 627
    new-instance v12, Landroid/content/ContentValues;

    invoke-direct {v12}, Landroid/content/ContentValues;-><init>()V

    .line 628
    const-string v13, "op_code"

    const/4 v14, 0x3

    invoke-static {v14}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v14

    invoke-virtual {v12, v13, v14}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 629
    const-string v13, "write_time_millis"

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v14

    invoke-virtual {v12, v13, v14}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 630
    const-string v13, "package_id"

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v14

    invoke-virtual {v12, v13, v14}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 631
    const-string v13, "account_id"

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v14

    invoke-virtual {v12, v13, v14}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 632
    const-string v13, "client_id"

    invoke-virtual {v12, v13, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 633
    const-string v13, "network_status"

    const/4 v14, 0x0

    invoke-static {v14}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v14

    invoke-virtual {v12, v13, v14}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 634
    const-string v13, "network_last_update_millis"

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v14

    invoke-virtual {v12, v13, v14}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 635
    const-string v13, "deletable"

    const/4 v14, 0x0

    invoke-static {v14}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v14

    invoke-virtual {v12, v13, v14}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 637
    const-string v13, "operations"

    const-string v14, "op_code"

    invoke-virtual {v10, v13, v14, v12}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v12

    .line 638
    const-wide/16 v14, -0x1

    cmp-long v14, v12, v14

    if-nez v14, :cond_5

    .line 639
    const/4 v4, 0x6

    invoke-static {v4}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 640
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "CopresenceStateDatabase: Could not insert unsubscribe for subscriptionId="

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/location/copresence/ag;->e(Ljava/lang/String;)I

    .line 644
    :cond_4
    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lcom/google/android/location/copresence/o/d;->c(Ljava/util/List;)I

    .line 645
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 648
    :cond_5
    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_1

    :cond_6
    move-object v2, v3

    .line 650
    goto/16 :goto_0
.end method

.method public final a(Ljava/lang/String;Lcom/google/android/location/copresence/a/a;[Ljava/lang/Integer;)Ljava/util/List;
    .locals 12

    .prologue
    .line 735
    if-eqz p3, :cond_0

    array-length v0, p3

    if-nez v0, :cond_1

    .line 736
    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 799
    :goto_0
    return-object v0

    .line 739
    :cond_1
    invoke-virtual {p0, p1}, Lcom/google/android/location/copresence/o/d;->c(Ljava/lang/String;)J

    move-result-wide v2

    .line 740
    const-wide/16 v0, -0x1

    cmp-long v0, v2, v0

    if-nez v0, :cond_3

    .line 741
    const/4 v0, 0x6

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 742
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "CopresenceStateDatabase: Could not get package id for packageName="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " for querying operation client ids."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->e(Ljava/lang/String;)I

    .line 745
    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    .line 748
    :cond_3
    iget-object v0, p2, Lcom/google/android/location/copresence/a/a;->a:Landroid/accounts/Account;

    iget-object v0, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/google/android/location/copresence/o/d;->d(Ljava/lang/String;)J

    move-result-wide v4

    .line 749
    const-wide/16 v0, -0x1

    cmp-long v0, v4, v0

    if-nez v0, :cond_5

    .line 750
    const/4 v0, 0x6

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 751
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "CopresenceStateDatabase: Could not get account id for account="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {p2}, Lcom/google/android/location/copresence/ag;->a(Lcom/google/android/location/copresence/a/a;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " for querying operation client ids."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->e(Ljava/lang/String;)I

    .line 755
    :cond_4
    const/4 v0, 0x0

    goto :goto_0

    .line 758
    :cond_5
    iget-object v0, p0, Lcom/google/android/location/copresence/o/d;->b:Lcom/google/android/location/copresence/o/g;

    invoke-virtual {v0}, Lcom/google/android/location/copresence/o/g;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 759
    invoke-static {}, Lcom/google/android/location/copresence/q/af;->a()Lcom/google/android/location/copresence/q/ak;

    move-result-object v1

    const-string v6, "package_id"

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v6, v2}, Lcom/google/android/location/copresence/q/ak;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/android/location/copresence/q/ak;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/location/copresence/q/ak;->a()Lcom/google/android/location/copresence/q/ak;

    move-result-object v1

    const-string v2, "account_id"

    invoke-static {v4, v5}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/google/android/location/copresence/q/ak;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/android/location/copresence/q/ak;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/location/copresence/q/ak;->a()Lcom/google/android/location/copresence/q/ak;

    move-result-object v1

    const-string v2, "op_code"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, p3, v3}, Lcom/google/android/location/copresence/q/ak;->a(Ljava/lang/String;[Ljava/lang/Object;Z)Lcom/google/android/location/copresence/q/ak;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/location/copresence/q/ak;->a()Lcom/google/android/location/copresence/q/ak;

    move-result-object v1

    const-string v2, "deletable"

    const/4 v3, 0x0

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/google/android/location/copresence/q/ak;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/android/location/copresence/q/ak;

    move-result-object v3

    .line 767
    const-string v1, "operations"

    const/4 v2, 0x5

    new-array v2, v2, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string v5, "_id"

    aput-object v5, v2, v4

    const/4 v4, 0x1

    const-string v5, "op_code"

    aput-object v5, v2, v4

    const/4 v4, 0x2

    const-string v5, "client_id"

    aput-object v5, v2, v4

    const/4 v4, 0x3

    const-string v5, "network_status"

    aput-object v5, v2, v4

    const/4 v4, 0x4

    const-string v5, "expiration_time"

    aput-object v5, v2, v4

    iget-object v3, v3, Lcom/google/android/location/copresence/q/ak;->a:Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const-string v7, "_id"

    invoke-static {v7}, Lcom/google/android/location/copresence/q/af;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v9

    .line 785
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 787
    :goto_1
    :try_start_0
    invoke-interface {v9}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-eqz v1, :cond_7

    .line 788
    const/4 v1, 0x0

    invoke-interface {v9, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    .line 789
    const/4 v1, 0x1

    invoke-interface {v9, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    .line 790
    const/4 v1, 0x2

    invoke-interface {v9, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 791
    const/4 v1, 0x3

    invoke-interface {v9, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v6

    .line 792
    const/4 v1, 0x4

    invoke-interface {v9, v1}, Landroid/database/Cursor;->isNull(I)Z

    move-result v1

    if-eqz v1, :cond_6

    const/4 v7, 0x0

    .line 793
    :goto_2
    new-instance v1, Lcom/google/android/location/copresence/o/i;

    const/4 v8, 0x0

    invoke-direct/range {v1 .. v8}, Lcom/google/android/location/copresence/o/i;-><init>(JILjava/lang/String;ILjava/lang/Long;B)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    .line 797
    :catchall_0
    move-exception v0

    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    throw v0

    .line 792
    :cond_6
    const/4 v1, 0x4

    :try_start_1
    invoke-interface {v9, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v10

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v7

    goto :goto_2

    .line 797
    :cond_7
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    goto/16 :goto_0
.end method

.method public final a(Ljava/util/List;)Ljava/util/List;
    .locals 8

    .prologue
    const/4 v7, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    const/4 v6, 0x0

    .line 683
    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 684
    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 708
    :goto_0
    return-object v0

    .line 686
    :cond_1
    iget-object v0, p0, Lcom/google/android/location/copresence/o/d;->b:Lcom/google/android/location/copresence/o/g;

    invoke-virtual {v0}, Lcom/google/android/location/copresence/o/g;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 687
    const-string v1, "operations"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/String;

    const-string v3, "_id"

    aput-object v3, v2, v6

    const-string v3, "proto"

    aput-object v3, v2, v5

    const-string v3, "client_id"

    aput-object v3, v2, v7

    invoke-static {}, Lcom/google/android/location/copresence/q/af;->a()Lcom/google/android/location/copresence/q/ak;

    move-result-object v3

    const-string v5, "_id"

    invoke-virtual {v3, v5, p1, v6}, Lcom/google/android/location/copresence/q/ak;->a(Ljava/lang/String;Ljava/util/List;Z)Lcom/google/android/location/copresence/q/ak;

    move-result-object v3

    iget-object v3, v3, Lcom/google/android/location/copresence/q/ak;->a:Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const-string v5, "_id"

    invoke-static {v5}, Lcom/google/android/location/copresence/q/af;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    move-object v5, v4

    move-object v6, v4

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 698
    :try_start_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 700
    :goto_1
    invoke-interface {v7}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 701
    const/4 v1, 0x0

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    .line 702
    const/4 v1, 0x1

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v5

    .line 703
    const/4 v1, 0x2

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 704
    new-instance v1, Lcom/google/android/location/copresence/o/j;

    const/4 v6, 0x0

    invoke-direct/range {v1 .. v6}, Lcom/google/android/location/copresence/o/j;-><init>(JLjava/lang/String;[BB)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    .line 708
    :catchall_0
    move-exception v0

    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_2
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    goto :goto_0
.end method

.method public final a()Ljava/util/Set;
    .locals 8

    .prologue
    const/4 v3, 0x0

    .line 908
    iget-object v0, p0, Lcom/google/android/location/copresence/o/d;->b:Lcom/google/android/location/copresence/o/g;

    invoke-virtual {v0}, Lcom/google/android/location/copresence/o/g;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 909
    const-string v1, "operations"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string v5, "DISTINCT account_id"

    aput-object v5, v2, v4

    move-object v4, v3

    move-object v5, v3

    move-object v6, v3

    move-object v7, v3

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 917
    :try_start_0
    iget-object v0, p0, Lcom/google/android/location/copresence/o/d;->d:Ljava/util/HashMap;

    invoke-static {v1, v0}, Lcom/google/android/location/copresence/o/d;->a(Landroid/database/Cursor;Ljava/util/HashMap;)Ljava/util/Set;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 919
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    return-object v0

    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0
.end method

.method public final a(Ljava/lang/String;)Ljava/util/Set;
    .locals 8

    .prologue
    const/4 v4, 0x0

    .line 930
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 931
    new-instance v4, Ljava/util/HashSet;

    invoke-direct {v4}, Ljava/util/HashSet;-><init>()V

    .line 955
    :cond_0
    :goto_0
    return-object v4

    .line 933
    :cond_1
    invoke-virtual {p0, p1}, Lcom/google/android/location/copresence/o/d;->c(Ljava/lang/String;)J

    move-result-wide v6

    .line 934
    const-wide/16 v0, -0x1

    cmp-long v0, v6, v0

    if-nez v0, :cond_2

    .line 935
    const/4 v0, 0x6

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 936
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "CopresenceStateDatabase: Could not get package id for packageName="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " for querying operation client ids."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->e(Ljava/lang/String;)I

    goto :goto_0

    .line 942
    :cond_2
    iget-object v0, p0, Lcom/google/android/location/copresence/o/d;->b:Lcom/google/android/location/copresence/o/g;

    invoke-virtual {v0}, Lcom/google/android/location/copresence/o/g;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 943
    const-string v1, "operations"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v5, "DISTINCT account_id"

    aput-object v5, v2, v3

    invoke-static {}, Lcom/google/android/location/copresence/q/af;->a()Lcom/google/android/location/copresence/q/ak;

    move-result-object v3

    const-string v5, "package_id"

    invoke-static {v6, v7}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v5, v6}, Lcom/google/android/location/copresence/q/ak;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/android/location/copresence/q/ak;

    move-result-object v3

    iget-object v3, v3, Lcom/google/android/location/copresence/q/ak;->a:Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object v5, v4

    move-object v6, v4

    move-object v7, v4

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 953
    :try_start_0
    iget-object v0, p0, Lcom/google/android/location/copresence/o/d;->d:Ljava/util/HashMap;

    invoke-static {v1, v0}, Lcom/google/android/location/copresence/o/d;->a(Landroid/database/Cursor;Ljava/util/HashMap;)Ljava/util/Set;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v4

    .line 955
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0
.end method

.method public final b(Ljava/util/List;)Ljava/util/List;
    .locals 9

    .prologue
    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    const/4 v6, 0x0

    .line 823
    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 824
    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 853
    :goto_0
    return-object v0

    .line 827
    :cond_1
    iget-object v0, p0, Lcom/google/android/location/copresence/o/d;->b:Lcom/google/android/location/copresence/o/g;

    invoke-virtual {v0}, Lcom/google/android/location/copresence/o/g;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 828
    const-string v1, "operations"

    const/4 v2, 0x4

    new-array v2, v2, [Ljava/lang/String;

    const-string v3, "_id"

    aput-object v3, v2, v6

    const-string v3, "op_code"

    aput-object v3, v2, v5

    const-string v3, "client_id"

    aput-object v3, v2, v7

    const-string v3, "network_status"

    aput-object v3, v2, v8

    invoke-static {}, Lcom/google/android/location/copresence/q/af;->a()Lcom/google/android/location/copresence/q/ak;

    move-result-object v3

    const-string v5, "_id"

    invoke-virtual {v3, v5, p1, v6}, Lcom/google/android/location/copresence/q/ak;->a(Ljava/lang/String;Ljava/util/List;Z)Lcom/google/android/location/copresence/q/ak;

    move-result-object v3

    iget-object v3, v3, Lcom/google/android/location/copresence/q/ak;->a:Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const-string v5, "_id"

    invoke-static {v5}, Lcom/google/android/location/copresence/q/af;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    move-object v5, v4

    move-object v6, v4

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 840
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 842
    :goto_1
    :try_start_0
    invoke-interface {v8}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 843
    const/4 v1, 0x0

    invoke-interface {v8, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    .line 844
    const/4 v1, 0x1

    invoke-interface {v8, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    .line 845
    const/4 v1, 0x2

    invoke-interface {v8, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 846
    const/4 v1, 0x3

    invoke-interface {v8, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v6

    .line 848
    new-instance v1, Lcom/google/android/location/copresence/o/h;

    const/4 v7, 0x0

    invoke-direct/range {v1 .. v7}, Lcom/google/android/location/copresence/o/h;-><init>(JILjava/lang/String;IB)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    .line 851
    :catchall_0
    move-exception v0

    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_2
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    goto :goto_0
.end method

.method public final b(Ljava/lang/String;)Ljava/util/Set;
    .locals 8

    .prologue
    const/4 v4, 0x0

    .line 966
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 967
    new-instance v4, Ljava/util/HashSet;

    invoke-direct {v4}, Ljava/util/HashSet;-><init>()V

    .line 993
    :cond_0
    :goto_0
    return-object v4

    .line 970
    :cond_1
    invoke-virtual {p0, p1}, Lcom/google/android/location/copresence/o/d;->d(Ljava/lang/String;)J

    move-result-wide v6

    .line 971
    const-wide/16 v0, -0x1

    cmp-long v0, v6, v0

    if-nez v0, :cond_2

    .line 972
    const/4 v0, 0x6

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 973
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "CopresenceStateDatabase: Could not get account id for account="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {p1}, Lcom/google/android/location/copresence/ag;->f(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " for querying operation client ids."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->e(Ljava/lang/String;)I

    goto :goto_0

    .line 980
    :cond_2
    iget-object v0, p0, Lcom/google/android/location/copresence/o/d;->b:Lcom/google/android/location/copresence/o/g;

    invoke-virtual {v0}, Lcom/google/android/location/copresence/o/g;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 981
    const-string v1, "operations"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v5, "DISTINCT package_id"

    aput-object v5, v2, v3

    invoke-static {}, Lcom/google/android/location/copresence/q/af;->a()Lcom/google/android/location/copresence/q/ak;

    move-result-object v3

    const-string v5, "account_id"

    invoke-static {v6, v7}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v5, v6}, Lcom/google/android/location/copresence/q/ak;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/android/location/copresence/q/ak;

    move-result-object v3

    iget-object v3, v3, Lcom/google/android/location/copresence/q/ak;->a:Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object v5, v4

    move-object v6, v4

    move-object v7, v4

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 991
    :try_start_0
    iget-object v0, p0, Lcom/google/android/location/copresence/o/d;->c:Ljava/util/HashMap;

    invoke-static {v1, v0}, Lcom/google/android/location/copresence/o/d;->a(Landroid/database/Cursor;Ljava/util/HashMap;)Ljava/util/Set;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v4

    .line 993
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0
.end method

.method public final c(Ljava/util/List;)I
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 1079
    const-string v1, "operations"

    const-string v2, "_id"

    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v3, p0, Lcom/google/android/location/copresence/o/d;->b:Lcom/google/android/location/copresence/o/g;

    invoke-virtual {v3}, Lcom/google/android/location/copresence/o/g;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v3

    invoke-static {}, Lcom/google/android/location/copresence/q/af;->a()Lcom/google/android/location/copresence/q/ak;

    move-result-object v4

    invoke-virtual {v4, v2, p1, v0}, Lcom/google/android/location/copresence/q/ak;->a(Ljava/lang/String;Ljava/util/List;Z)Lcom/google/android/location/copresence/q/ak;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/location/copresence/q/ak;->a:Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v2, 0x0

    invoke-virtual {v3, v1, v0, v2}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    goto :goto_0
.end method

.method final c(Ljava/lang/String;)J
    .locals 2

    .prologue
    .line 1029
    iget-object v0, p0, Lcom/google/android/location/copresence/o/d;->c:Ljava/util/HashMap;

    const-string v1, "packages"

    invoke-direct {p0, v0, v1, p1}, Lcom/google/android/location/copresence/o/d;->a(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)J

    move-result-wide v0

    return-wide v0
.end method

.method final d(Ljava/lang/String;)J
    .locals 2

    .prologue
    .line 1040
    iget-object v0, p0, Lcom/google/android/location/copresence/o/d;->d:Ljava/util/HashMap;

    const-string v1, "accounts"

    invoke-direct {p0, v0, v1, p1}, Lcom/google/android/location/copresence/o/d;->a(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)J

    move-result-wide v0

    return-wide v0
.end method
