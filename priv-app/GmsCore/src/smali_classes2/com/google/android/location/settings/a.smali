.class public abstract Lcom/google/android/location/settings/a;
.super Landroid/support/v7/app/d;
.source "SourceFile"

# interfaces
.implements Landroid/widget/CompoundButton$OnCheckedChangeListener;
.implements Lcom/google/android/gms/common/widget/h;


# instance fields
.field a:Lcom/google/android/gms/common/widget/SwitchBar;

.field b:Landroid/widget/CompoundButton;

.field c:Z

.field private d:Landroid/support/v7/app/a;

.field private e:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0}, Landroid/support/v7/app/d;-><init>()V

    return-void
.end method

.method private b(Z)V
    .locals 1

    .prologue
    .line 166
    iput-boolean p1, p0, Lcom/google/android/location/settings/a;->e:Z

    .line 167
    iget-boolean v0, p0, Lcom/google/android/location/settings/a;->c:Z

    if-eqz v0, :cond_0

    .line 168
    invoke-virtual {p0, p1}, Lcom/google/android/location/settings/a;->a(Z)V

    .line 170
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/common/widget/SwitchBar;Z)V
    .locals 0

    .prologue
    .line 177
    invoke-direct {p0, p2}, Lcom/google/android/location/settings/a;->b(Z)V

    .line 178
    return-void
.end method

.method protected abstract a(Z)V
.end method

.method public onCheckedChanged(Landroid/widget/CompoundButton;Z)V
    .locals 0

    .prologue
    .line 185
    invoke-direct {p0, p2}, Lcom/google/android/location/settings/a;->b(Z)V

    .line 186
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 40
    invoke-super {p0, p1}, Landroid/support/v7/app/d;->onCreate(Landroid/os/Bundle;)V

    .line 42
    invoke-super {p0}, Landroid/support/v7/app/d;->d()Landroid/support/v7/app/e;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v7/app/e;->b()Landroid/support/v7/app/a;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/settings/a;->d:Landroid/support/v7/app/a;

    .line 43
    iget-object v0, p0, Lcom/google/android/location/settings/a;->d:Landroid/support/v7/app/a;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/support/v7/app/a;->a(Z)V

    .line 46
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/location/settings/a;->c:Z

    .line 47
    return-void
.end method

.method public setContentView(I)V
    .locals 6

    .prologue
    const/4 v2, 0x0

    const/16 v4, 0x10

    const/16 v5, 0xe

    const/4 v1, 0x0

    .line 57
    invoke-super {p0, p1}, Landroid/support/v7/app/d;->setContentView(I)V

    .line 60
    iput-object v2, p0, Lcom/google/android/location/settings/a;->a:Lcom/google/android/gms/common/widget/SwitchBar;

    .line 61
    const/16 v0, 0x15

    invoke-static {v0}, Lcom/google/android/gms/common/util/ao;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 62
    sget v0, Lcom/google/android/gms/j;->sn:I

    invoke-virtual {p0, v0}, Lcom/google/android/location/settings/a;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/widget/SwitchBar;

    iput-object v0, p0, Lcom/google/android/location/settings/a;->a:Lcom/google/android/gms/common/widget/SwitchBar;

    .line 63
    iget-object v0, p0, Lcom/google/android/location/settings/a;->a:Lcom/google/android/gms/common/widget/SwitchBar;

    if-eqz v0, :cond_2

    .line 64
    iget-object v0, p0, Lcom/google/android/location/settings/a;->a:Lcom/google/android/gms/common/widget/SwitchBar;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/common/widget/SwitchBar;->a(Lcom/google/android/gms/common/widget/h;)V

    .line 65
    iput-object v2, p0, Lcom/google/android/location/settings/a;->b:Landroid/widget/CompoundButton;

    .line 72
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/google/android/location/settings/a;->a:Lcom/google/android/gms/common/widget/SwitchBar;

    if-nez v0, :cond_1

    .line 73
    invoke-static {v5}, Lcom/google/android/gms/common/util/ao;->a(I)Z

    move-result v0

    if-nez v0, :cond_3

    new-instance v0, Landroid/widget/CheckBox;

    invoke-direct {v0, p0}, Landroid/widget/CheckBox;-><init>(Landroid/content/Context;)V

    :goto_1
    iput-object v0, p0, Lcom/google/android/location/settings/a;->b:Landroid/widget/CompoundButton;

    .line 74
    iget-object v0, p0, Lcom/google/android/location/settings/a;->d:Landroid/support/v7/app/a;

    invoke-virtual {v0, v4, v4}, Landroid/support/v7/app/a;->a(II)V

    .line 76
    iget-object v2, p0, Lcom/google/android/location/settings/a;->d:Landroid/support/v7/app/a;

    iget-object v3, p0, Lcom/google/android/location/settings/a;->b:Landroid/widget/CompoundButton;

    new-instance v4, Landroid/support/v7/app/b;

    invoke-static {v5}, Lcom/google/android/gms/common/util/ao;->a(I)Z

    move-result v0

    if-eqz v0, :cond_4

    const v0, 0x800005

    :goto_2
    or-int/lit8 v0, v0, 0x10

    invoke-direct {v4, v0}, Landroid/support/v7/app/b;-><init>(I)V

    invoke-virtual {v2, v3, v4}, Landroid/support/v7/app/a;->a(Landroid/view/View;Landroid/support/v7/app/b;)V

    .line 79
    iget-object v0, p0, Lcom/google/android/location/settings/a;->b:Landroid/widget/CompoundButton;

    invoke-virtual {v0, p0}, Landroid/widget/CompoundButton;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 82
    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/location/settings/a;->c:Z

    .line 83
    return-void

    .line 67
    :cond_2
    const-string v0, "GCoreLocationSettings"

    const-string v2, "SwitchBar missing from layout"

    invoke-static {v0, v2}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 73
    :cond_3
    new-instance v0, Landroid/widget/Switch;

    invoke-direct {v0, p0}, Landroid/widget/Switch;-><init>(Landroid/content/Context;)V

    invoke-virtual {p0}, Lcom/google/android/location/settings/a;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/google/android/gms/g;->bd:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    invoke-virtual {v0, v1, v1, v2, v1}, Landroid/widget/Switch;->setPadding(IIII)V

    goto :goto_1

    :cond_4
    move v0, v1

    .line 76
    goto :goto_2
.end method
