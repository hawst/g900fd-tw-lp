.class final Lcom/google/android/location/places/ui/ah;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:[Lcom/google/android/gms/location/places/f;

.field final synthetic b:Ljava/lang/String;

.field final synthetic c:Lcom/google/android/location/places/ui/ac;


# direct methods
.method constructor <init>(Lcom/google/android/location/places/ui/ac;[Lcom/google/android/gms/location/places/f;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 532
    iput-object p1, p0, Lcom/google/android/location/places/ui/ah;->c:Lcom/google/android/location/places/ui/ac;

    iput-object p2, p0, Lcom/google/android/location/places/ui/ah;->a:[Lcom/google/android/gms/location/places/f;

    iput-object p3, p0, Lcom/google/android/location/places/ui/ah;->b:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 535
    iget-object v0, p0, Lcom/google/android/location/places/ui/ah;->c:Lcom/google/android/location/places/ui/ac;

    invoke-virtual {v0}, Lcom/google/android/location/places/ui/ac;->isVisible()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/location/places/ui/ah;->c:Lcom/google/android/location/places/ui/ac;

    invoke-static {v0}, Lcom/google/android/location/places/ui/ac;->j(Lcom/google/android/location/places/ui/ac;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 575
    :cond_0
    :goto_0
    return-void

    .line 538
    :cond_1
    iget-object v0, p0, Lcom/google/android/location/places/ui/ah;->c:Lcom/google/android/location/places/ui/ac;

    invoke-static {v0}, Lcom/google/android/location/places/ui/ac;->d(Lcom/google/android/location/places/ui/ac;)Lcom/google/android/location/places/ui/MarkerMapFragment;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/location/places/ui/ah;->a:[Lcom/google/android/gms/location/places/f;

    invoke-virtual {v0, v1}, Lcom/google/android/location/places/ui/MarkerMapFragment;->a([Lcom/google/android/gms/location/places/f;)V

    .line 539
    iget-object v0, p0, Lcom/google/android/location/places/ui/ah;->c:Lcom/google/android/location/places/ui/ac;

    invoke-static {v0}, Lcom/google/android/location/places/ui/ac;->e(Lcom/google/android/location/places/ui/ac;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 540
    iget-object v0, p0, Lcom/google/android/location/places/ui/ah;->c:Lcom/google/android/location/places/ui/ac;

    invoke-static {v0}, Lcom/google/android/location/places/ui/ac;->b(Lcom/google/android/location/places/ui/ac;)Lcom/google/android/location/places/ui/k;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/location/places/ui/ah;->c:Lcom/google/android/location/places/ui/ac;

    invoke-static {v1}, Lcom/google/android/location/places/ui/ac;->k(Lcom/google/android/location/places/ui/ac;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/location/places/ui/ah;->a:[Lcom/google/android/gms/location/places/f;

    iget-object v3, p0, Lcom/google/android/location/places/ui/ah;->b:Ljava/lang/String;

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/location/places/ui/k;->a(Ljava/lang/String;[Lcom/google/android/gms/location/places/f;Ljava/lang/String;)V

    .line 541
    iget-object v0, p0, Lcom/google/android/location/places/ui/ah;->a:[Lcom/google/android/gms/location/places/f;

    array-length v0, v0

    if-eqz v0, :cond_2

    .line 543
    iget-object v0, p0, Lcom/google/android/location/places/ui/ah;->c:Lcom/google/android/location/places/ui/ac;

    invoke-static {v0}, Lcom/google/android/location/places/ui/ac;->d(Lcom/google/android/location/places/ui/ac;)Lcom/google/android/location/places/ui/MarkerMapFragment;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/location/places/ui/ah;->a:[Lcom/google/android/gms/location/places/f;

    aget-object v1, v1, v4

    invoke-virtual {v0, v1}, Lcom/google/android/location/places/ui/MarkerMapFragment;->a(Lcom/google/android/gms/location/places/f;)V

    .line 544
    iget-object v0, p0, Lcom/google/android/location/places/ui/ah;->c:Lcom/google/android/location/places/ui/ac;

    invoke-static {v0}, Lcom/google/android/location/places/ui/ac;->d(Lcom/google/android/location/places/ui/ac;)Lcom/google/android/location/places/ui/MarkerMapFragment;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/location/places/ui/ah;->a:[Lcom/google/android/gms/location/places/f;

    aget-object v1, v1, v4

    invoke-virtual {v0, v1}, Lcom/google/android/location/places/ui/MarkerMapFragment;->b(Lcom/google/android/gms/location/places/f;)V

    .line 545
    iget-object v0, p0, Lcom/google/android/location/places/ui/ah;->c:Lcom/google/android/location/places/ui/ac;

    invoke-static {v0}, Lcom/google/android/location/places/ui/ac;->b(Lcom/google/android/location/places/ui/ac;)Lcom/google/android/location/places/ui/k;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/location/places/ui/ah;->a:[Lcom/google/android/gms/location/places/f;

    aget-object v1, v1, v4

    invoke-virtual {v0, v1}, Lcom/google/android/location/places/ui/k;->a(Lcom/google/android/gms/location/places/f;)V

    .line 565
    :goto_1
    iget-object v0, p0, Lcom/google/android/location/places/ui/ah;->c:Lcom/google/android/location/places/ui/ac;

    invoke-static {v0}, Lcom/google/android/location/places/ui/ac;->f(Lcom/google/android/location/places/ui/ac;)Lcom/google/android/location/places/ui/s;

    move-result-object v0

    iget-boolean v0, v0, Lcom/google/android/location/places/ui/s;->g:Z

    if-nez v0, :cond_0

    .line 566
    iget-object v0, p0, Lcom/google/android/location/places/ui/ah;->c:Lcom/google/android/location/places/ui/ac;

    invoke-static {v0}, Lcom/google/android/location/places/ui/ac;->b(Lcom/google/android/location/places/ui/ac;)Lcom/google/android/location/places/ui/k;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/location/places/ui/k;->d()V

    goto :goto_0

    .line 548
    :cond_2
    iget-object v0, p0, Lcom/google/android/location/places/ui/ah;->c:Lcom/google/android/location/places/ui/ac;

    invoke-static {v0}, Lcom/google/android/location/places/ui/ac;->f(Lcom/google/android/location/places/ui/ac;)Lcom/google/android/location/places/ui/s;

    move-result-object v0

    iget-boolean v0, v0, Lcom/google/android/location/places/ui/s;->g:Z

    if-eqz v0, :cond_3

    .line 549
    iget-object v0, p0, Lcom/google/android/location/places/ui/ah;->c:Lcom/google/android/location/places/ui/ac;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/google/android/location/places/ui/ac;->a(Lcom/google/android/location/places/ui/ac;Ljava/lang/Runnable;)V

    goto :goto_1

    .line 551
    :cond_3
    iget-object v0, p0, Lcom/google/android/location/places/ui/ah;->c:Lcom/google/android/location/places/ui/ac;

    new-instance v1, Lcom/google/android/location/places/ui/ai;

    invoke-direct {v1, p0}, Lcom/google/android/location/places/ui/ai;-><init>(Lcom/google/android/location/places/ui/ah;)V

    invoke-static {v0, v1}, Lcom/google/android/location/places/ui/ac;->a(Lcom/google/android/location/places/ui/ac;Ljava/lang/Runnable;)V

    goto :goto_1

    .line 569
    :cond_4
    iget-object v0, p0, Lcom/google/android/location/places/ui/ah;->c:Lcom/google/android/location/places/ui/ac;

    invoke-static {v0}, Lcom/google/android/location/places/ui/ac;->f(Lcom/google/android/location/places/ui/ac;)Lcom/google/android/location/places/ui/s;

    move-result-object v0

    iget-boolean v0, v0, Lcom/google/android/location/places/ui/s;->h:Z

    if-eqz v0, :cond_5

    .line 570
    iget-object v0, p0, Lcom/google/android/location/places/ui/ah;->c:Lcom/google/android/location/places/ui/ac;

    invoke-static {v0}, Lcom/google/android/location/places/ui/ac;->b(Lcom/google/android/location/places/ui/ac;)Lcom/google/android/location/places/ui/k;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/location/places/ui/ah;->c:Lcom/google/android/location/places/ui/ac;

    invoke-static {v1}, Lcom/google/android/location/places/ui/ac;->g(Lcom/google/android/location/places/ui/ac;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/location/places/ui/k;->a(Ljava/lang/String;)V

    .line 573
    :cond_5
    iget-object v0, p0, Lcom/google/android/location/places/ui/ah;->c:Lcom/google/android/location/places/ui/ac;

    invoke-static {v0}, Lcom/google/android/location/places/ui/ac;->b(Lcom/google/android/location/places/ui/ac;)Lcom/google/android/location/places/ui/k;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/location/places/ui/ah;->c:Lcom/google/android/location/places/ui/ac;

    invoke-static {v1}, Lcom/google/android/location/places/ui/ac;->h(Lcom/google/android/location/places/ui/ac;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/location/places/ui/ah;->a:[Lcom/google/android/gms/location/places/f;

    iget-object v3, p0, Lcom/google/android/location/places/ui/ah;->b:Ljava/lang/String;

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/location/places/ui/k;->a(Ljava/lang/String;[Lcom/google/android/gms/location/places/f;Ljava/lang/String;)V

    goto/16 :goto_0
.end method
