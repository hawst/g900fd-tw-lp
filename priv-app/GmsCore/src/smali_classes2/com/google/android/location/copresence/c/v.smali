.class final Lcom/google/android/location/copresence/c/v;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/audiomodem/bc;


# instance fields
.field final synthetic a:Lcom/google/android/location/copresence/c/q;


# direct methods
.method constructor <init>(Lcom/google/android/location/copresence/c/q;)V
    .locals 0

    .prologue
    .line 209
    iput-object p1, p0, Lcom/google/android/location/copresence/c/v;->a:Lcom/google/android/location/copresence/c/q;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 234
    const/4 v0, 0x3

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 235
    const-string v0, "Audio Stack: CarrierSenseManager: Broadcaster detected after pause broadcast"

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->b(Ljava/lang/String;)I

    .line 238
    :cond_0
    iget-object v0, p0, Lcom/google/android/location/copresence/c/v;->a:Lcom/google/android/location/copresence/c/q;

    iget-object v0, v0, Lcom/google/android/location/copresence/c/q;->b:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/location/copresence/c/x;

    invoke-direct {v1, p0}, Lcom/google/android/location/copresence/c/x;-><init>(Lcom/google/android/location/copresence/c/v;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 251
    return-void
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 212
    const/4 v0, 0x3

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 213
    const-string v0, "Audio Stack: CarrierSenseManager: No broadcaster detected after pause broadcast"

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->b(Ljava/lang/String;)I

    .line 216
    :cond_0
    iget-object v0, p0, Lcom/google/android/location/copresence/c/v;->a:Lcom/google/android/location/copresence/c/q;

    iget-object v0, v0, Lcom/google/android/location/copresence/c/q;->b:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/location/copresence/c/w;

    invoke-direct {v1, p0}, Lcom/google/android/location/copresence/c/w;-><init>(Lcom/google/android/location/copresence/c/v;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 230
    return-void
.end method
