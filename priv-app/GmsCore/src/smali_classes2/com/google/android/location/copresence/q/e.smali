.class public final Lcom/google/android/location/copresence/q/e;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final a:[I

.field private static final b:[I

.field private static final c:[I


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 25
    new-array v0, v1, [I

    aput v3, v0, v2

    sput-object v0, Lcom/google/android/location/copresence/q/e;->a:[I

    .line 26
    new-array v0, v1, [I

    aput v1, v0, v2

    sput-object v0, Lcom/google/android/location/copresence/q/e;->b:[I

    .line 27
    new-array v0, v3, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/google/android/location/copresence/q/e;->c:[I

    return-void

    nop

    :array_0
    .array-data 4
        0x2
        0x1
    .end array-data
.end method

.method public static a(Landroid/content/Context;[Lcom/google/ac/b/c/bf;[Lcom/google/ac/b/c/bo;)Lcom/google/ac/b/c/ac;
    .locals 11

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 33
    if-nez p0, :cond_0

    .line 34
    const/4 v0, 0x0

    .line 95
    :goto_0
    return-object v0

    .line 36
    :cond_0
    invoke-static {p0}, Lcom/google/android/location/copresence/c/a;->a(Landroid/content/Context;)Lcom/google/android/location/copresence/c/a;

    move-result-object v6

    .line 37
    invoke-static {p0}, Lcom/google/android/location/copresence/d/j;->a(Landroid/content/Context;)Lcom/google/android/location/copresence/d/j;

    move-result-object v7

    .line 38
    invoke-static {p0}, Lcom/google/android/location/copresence/r/ag;->a(Landroid/content/Context;)Lcom/google/android/location/copresence/r/ag;

    move-result-object v8

    .line 40
    new-instance v0, Lcom/google/android/location/copresence/q/f;

    invoke-direct {v0, v2}, Lcom/google/android/location/copresence/q/f;-><init>(B)V

    .line 47
    array-length v5, p1

    move v3, v2

    :goto_1
    if-ge v3, v5, :cond_1

    aget-object v4, p1, v3

    .line 48
    iget-object v4, v4, Lcom/google/ac/b/c/bf;->e:Lcom/google/ac/b/c/bs;

    invoke-static {v4, v0}, Lcom/google/android/location/copresence/q/e;->a(Lcom/google/ac/b/c/bs;Lcom/google/android/location/copresence/q/f;)Lcom/google/android/location/copresence/q/f;

    move-result-object v4

    .line 47
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    move-object v0, v4

    goto :goto_1

    .line 50
    :cond_1
    array-length v4, p2

    move-object v5, v0

    move v0, v2

    :goto_2
    if-ge v0, v4, :cond_2

    aget-object v3, p2, v0

    .line 51
    iget-object v3, v3, Lcom/google/ac/b/c/bo;->g:Lcom/google/ac/b/c/bs;

    invoke-static {v3, v5}, Lcom/google/android/location/copresence/q/e;->a(Lcom/google/ac/b/c/bs;Lcom/google/android/location/copresence/q/f;)Lcom/google/android/location/copresence/q/f;

    move-result-object v3

    .line 50
    add-int/lit8 v0, v0, 0x1

    move-object v5, v3

    goto :goto_2

    .line 54
    :cond_2
    new-instance v4, Lcom/google/ac/b/c/ac;

    invoke-direct {v4}, Lcom/google/ac/b/c/ac;-><init>()V

    .line 56
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    .line 59
    iget-boolean v0, v5, Lcom/google/android/location/copresence/q/f;->a:Z

    if-eqz v0, :cond_3

    iget-object v0, v6, Lcom/google/android/location/copresence/c/a;->c:Lcom/google/android/location/copresence/w;

    invoke-interface {v0}, Lcom/google/android/location/copresence/w;->a()Z

    move-result v0

    if-eqz v0, :cond_3

    move v0, v1

    :goto_3
    iget-boolean v3, v5, Lcom/google/android/location/copresence/q/f;->b:Z

    if-eqz v3, :cond_4

    iget-object v3, v6, Lcom/google/android/location/copresence/c/a;->d:Lcom/google/android/location/copresence/z;

    invoke-interface {v3}, Lcom/google/android/location/copresence/z;->b()Z

    move-result v3

    if-eqz v3, :cond_4

    move v3, v1

    :goto_4
    invoke-static {v1, v0, v3}, Lcom/google/android/location/copresence/q/e;->a(IZZ)Lcom/google/ac/b/c/bw;

    move-result-object v0

    .line 62
    invoke-interface {v9, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 64
    const/16 v10, 0x8

    iget-boolean v0, v5, Lcom/google/android/location/copresence/q/f;->c:Z

    if-eqz v0, :cond_5

    iget-boolean v0, v5, Lcom/google/android/location/copresence/q/f;->a:Z

    if-eqz v0, :cond_5

    iget-object v0, v6, Lcom/google/android/location/copresence/c/a;->a:Lcom/google/android/location/copresence/w;

    invoke-interface {v0}, Lcom/google/android/location/copresence/w;->a()Z

    move-result v0

    if-eqz v0, :cond_5

    move v0, v1

    :goto_5
    iget-boolean v3, v5, Lcom/google/android/location/copresence/q/f;->c:Z

    if-eqz v3, :cond_6

    iget-boolean v3, v5, Lcom/google/android/location/copresence/q/f;->b:Z

    if-eqz v3, :cond_6

    iget-object v3, v6, Lcom/google/android/location/copresence/c/a;->b:Lcom/google/android/location/copresence/z;

    invoke-interface {v3}, Lcom/google/android/location/copresence/z;->b()Z

    move-result v3

    if-eqz v3, :cond_6

    move v3, v1

    :goto_6
    invoke-static {v10, v0, v3}, Lcom/google/android/location/copresence/q/e;->a(IZZ)Lcom/google/ac/b/c/bw;

    move-result-object v0

    .line 71
    invoke-interface {v9, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 73
    const/4 v6, 0x2

    iget-boolean v0, v5, Lcom/google/android/location/copresence/q/f;->a:Z

    if-eqz v0, :cond_7

    invoke-virtual {v7}, Lcom/google/android/location/copresence/d/j;->a()Z

    move-result v0

    if-eqz v0, :cond_7

    move v0, v1

    :goto_7
    iget-boolean v3, v5, Lcom/google/android/location/copresence/q/f;->b:Z

    if-eqz v3, :cond_8

    invoke-virtual {v7}, Lcom/google/android/location/copresence/d/j;->c()Z

    move-result v3

    if-eqz v3, :cond_8

    move v3, v1

    :goto_8
    invoke-static {v6, v0, v3}, Lcom/google/android/location/copresence/q/e;->a(IZZ)Lcom/google/ac/b/c/bw;

    move-result-object v0

    .line 76
    invoke-interface {v9, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 78
    const/4 v6, 0x3

    iget-boolean v0, v5, Lcom/google/android/location/copresence/q/f;->a:Z

    if-eqz v0, :cond_9

    invoke-virtual {v7}, Lcom/google/android/location/copresence/d/j;->b()Z

    move-result v0

    if-eqz v0, :cond_9

    move v0, v1

    :goto_9
    iget-boolean v3, v5, Lcom/google/android/location/copresence/q/f;->b:Z

    if-eqz v3, :cond_a

    invoke-virtual {v7}, Lcom/google/android/location/copresence/d/j;->d()Z

    move-result v3

    if-eqz v3, :cond_a

    move v3, v1

    :goto_a
    invoke-static {v6, v0, v3}, Lcom/google/android/location/copresence/q/e;->a(IZZ)Lcom/google/ac/b/c/bw;

    move-result-object v0

    .line 81
    invoke-interface {v9, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 83
    const/4 v6, 0x7

    iget-boolean v0, v5, Lcom/google/android/location/copresence/q/f;->a:Z

    if-eqz v0, :cond_b

    iget-object v0, v8, Lcom/google/android/location/copresence/r/ag;->a:Lcom/google/android/location/copresence/w;

    invoke-interface {v0}, Lcom/google/android/location/copresence/w;->a()Z

    move-result v0

    if-eqz v0, :cond_b

    move v0, v1

    :goto_b
    iget-boolean v3, v5, Lcom/google/android/location/copresence/q/f;->b:Z

    if-eqz v3, :cond_c

    invoke-virtual {v8}, Lcom/google/android/location/copresence/r/ag;->a()Z

    move-result v3

    if-eqz v3, :cond_c

    move v3, v1

    :goto_c
    invoke-static {v6, v0, v3}, Lcom/google/android/location/copresence/q/e;->a(IZZ)Lcom/google/ac/b/c/bw;

    move-result-object v0

    .line 86
    invoke-interface {v9, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 88
    const/4 v3, 0x4

    iget-boolean v0, v5, Lcom/google/android/location/copresence/q/f;->a:Z

    if-eqz v0, :cond_d

    iget-object v0, v8, Lcom/google/android/location/copresence/r/ag;->b:Lcom/google/android/location/copresence/w;

    invoke-interface {v0}, Lcom/google/android/location/copresence/w;->a()Z

    move-result v0

    if-eqz v0, :cond_d

    move v0, v1

    :goto_d
    iget-boolean v5, v5, Lcom/google/android/location/copresence/q/f;->b:Z

    if-eqz v5, :cond_e

    invoke-virtual {v8}, Lcom/google/android/location/copresence/r/ag;->a()Z

    move-result v5

    if-eqz v5, :cond_e

    :goto_e
    invoke-static {v3, v0, v1}, Lcom/google/android/location/copresence/q/e;->a(IZZ)Lcom/google/ac/b/c/bw;

    move-result-object v0

    .line 91
    invoke-interface {v9, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 93
    invoke-interface {v9}, Ljava/util/List;->size()I

    move-result v0

    new-array v0, v0, [Lcom/google/ac/b/c/bw;

    invoke-interface {v9, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/ac/b/c/bw;

    iput-object v0, v4, Lcom/google/ac/b/c/ac;->b:[Lcom/google/ac/b/c/bw;

    move-object v0, v4

    .line 95
    goto/16 :goto_0

    :cond_3
    move v0, v2

    .line 59
    goto/16 :goto_3

    :cond_4
    move v3, v2

    goto/16 :goto_4

    :cond_5
    move v0, v2

    .line 64
    goto/16 :goto_5

    :cond_6
    move v3, v2

    goto/16 :goto_6

    :cond_7
    move v0, v2

    .line 73
    goto/16 :goto_7

    :cond_8
    move v3, v2

    goto/16 :goto_8

    :cond_9
    move v0, v2

    .line 78
    goto :goto_9

    :cond_a
    move v3, v2

    goto :goto_a

    :cond_b
    move v0, v2

    .line 83
    goto :goto_b

    :cond_c
    move v3, v2

    goto :goto_c

    :cond_d
    move v0, v2

    .line 88
    goto :goto_d

    :cond_e
    move v1, v2

    goto :goto_e
.end method

.method private static a(IZZ)Lcom/google/ac/b/c/bw;
    .locals 2

    .prologue
    .line 100
    new-instance v0, Lcom/google/ac/b/c/bw;

    invoke-direct {v0}, Lcom/google/ac/b/c/bw;-><init>()V

    .line 101
    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, v0, Lcom/google/ac/b/c/bw;->a:Ljava/lang/Integer;

    .line 102
    if-eqz p1, :cond_1

    if-eqz p2, :cond_1

    .line 103
    sget-object v1, Lcom/google/android/location/copresence/q/e;->c:[I

    iput-object v1, v0, Lcom/google/ac/b/c/bw;->b:[I

    .line 109
    :cond_0
    :goto_0
    return-object v0

    .line 104
    :cond_1
    if-eqz p1, :cond_2

    .line 105
    sget-object v1, Lcom/google/android/location/copresence/q/e;->b:[I

    iput-object v1, v0, Lcom/google/ac/b/c/bw;->b:[I

    goto :goto_0

    .line 106
    :cond_2
    if-eqz p2, :cond_0

    .line 107
    sget-object v1, Lcom/google/android/location/copresence/q/e;->a:[I

    iput-object v1, v0, Lcom/google/ac/b/c/bw;->b:[I

    goto :goto_0
.end method

.method private static a(Lcom/google/ac/b/c/bs;Lcom/google/android/location/copresence/q/f;)Lcom/google/android/location/copresence/q/f;
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 120
    if-eqz p0, :cond_2

    .line 121
    iget-object v0, p0, Lcom/google/ac/b/c/bs;->a:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/ac/b/c/bs;->a:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-ne v0, v1, :cond_0

    .line 123
    iput-boolean v1, p1, Lcom/google/android/location/copresence/q/f;->c:Z

    .line 125
    :cond_0
    iget-object v0, p0, Lcom/google/ac/b/c/bs;->b:Ljava/lang/Integer;

    if-eqz v0, :cond_1

    .line 126
    iget-object v0, p0, Lcom/google/ac/b/c/bs;->b:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 146
    :cond_1
    :goto_0
    return-object p1

    .line 128
    :pswitch_0
    iput-boolean v1, p1, Lcom/google/android/location/copresence/q/f;->a:Z

    .line 129
    iput-boolean v1, p1, Lcom/google/android/location/copresence/q/f;->b:Z

    goto :goto_0

    .line 132
    :pswitch_1
    iput-boolean v1, p1, Lcom/google/android/location/copresence/q/f;->a:Z

    goto :goto_0

    .line 135
    :pswitch_2
    iput-boolean v1, p1, Lcom/google/android/location/copresence/q/f;->b:Z

    goto :goto_0

    .line 143
    :cond_2
    iput-boolean v1, p1, Lcom/google/android/location/copresence/q/f;->a:Z

    .line 144
    iput-boolean v1, p1, Lcom/google/android/location/copresence/q/f;->b:Z

    goto :goto_0

    .line 126
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
