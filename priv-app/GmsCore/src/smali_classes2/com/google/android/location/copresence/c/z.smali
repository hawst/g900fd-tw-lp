.class public final Lcom/google/android/location/copresence/c/z;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field a:I

.field final b:Landroid/os/Handler;

.field final c:Lcom/google/android/gms/audiomodem/bm;

.field d:Lcom/google/android/gms/audiomodem/TokenReceiver$Params;

.field final e:Lcom/google/android/location/copresence/c/b;

.field f:Lcom/google/android/location/copresence/c/l;

.field final g:Lcom/google/android/gms/audiomodem/bc;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/location/copresence/c/b;Lcom/google/android/gms/audiomodem/bm;)V
    .locals 1

    .prologue
    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/location/copresence/c/z;->a:I

    .line 122
    new-instance v0, Lcom/google/android/location/copresence/c/aa;

    invoke-direct {v0, p0}, Lcom/google/android/location/copresence/c/aa;-><init>(Lcom/google/android/location/copresence/c/z;)V

    iput-object v0, p0, Lcom/google/android/location/copresence/c/z;->g:Lcom/google/android/gms/audiomodem/bc;

    .line 37
    invoke-static {p1}, Lcom/google/android/location/copresence/ap;->a(Landroid/content/Context;)Lcom/google/android/location/copresence/ap;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/location/copresence/ap;->d()Landroid/os/Handler;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/copresence/c/z;->b:Landroid/os/Handler;

    .line 38
    iput-object p3, p0, Lcom/google/android/location/copresence/c/z;->c:Lcom/google/android/gms/audiomodem/bm;

    .line 39
    iput-object p2, p0, Lcom/google/android/location/copresence/c/z;->e:Lcom/google/android/location/copresence/c/b;

    .line 40
    return-void
.end method


# virtual methods
.method final a()V
    .locals 2

    .prologue
    const/4 v1, 0x3

    .line 67
    iget v0, p0, Lcom/google/android/location/copresence/c/z;->a:I

    if-nez v0, :cond_1

    .line 68
    invoke-static {v1}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 69
    const-string v0, "Audio Stack: JustListenManager: already stopped"

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->b(Ljava/lang/String;)I

    .line 78
    :cond_0
    :goto_0
    return-void

    .line 73
    :cond_1
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/location/copresence/c/z;->a:I

    .line 74
    invoke-static {v1}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 75
    const-string v0, "Audio Stack: JustListenManager: stopped"

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->b(Ljava/lang/String;)I

    .line 77
    :cond_2
    iget-object v0, p0, Lcom/google/android/location/copresence/c/z;->e:Lcom/google/android/location/copresence/c/b;

    iget-object v1, p0, Lcom/google/android/location/copresence/c/z;->c:Lcom/google/android/gms/audiomodem/bm;

    invoke-virtual {v0, v1}, Lcom/google/android/location/copresence/c/b;->a(Lcom/google/android/gms/audiomodem/bm;)V

    goto :goto_0
.end method
