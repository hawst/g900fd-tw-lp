.class final Lcom/google/android/location/copresence/o/b;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final a:Lcom/google/android/gms/location/copresence/internal/i;

.field final b:Ljava/util/HashMap;

.field final synthetic c:Lcom/google/android/location/copresence/o/a;


# direct methods
.method public constructor <init>(Lcom/google/android/location/copresence/o/a;Lcom/google/android/gms/location/copresence/internal/i;)V
    .locals 1

    .prologue
    .line 61
    iput-object p1, p0, Lcom/google/android/location/copresence/o/b;->c:Lcom/google/android/location/copresence/o/a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 62
    invoke-static {p2}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/location/copresence/internal/i;

    iput-object v0, p0, Lcom/google/android/location/copresence/o/b;->a:Lcom/google/android/gms/location/copresence/internal/i;

    .line 63
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/location/copresence/o/b;->b:Ljava/util/HashMap;

    .line 64
    return-void
.end method


# virtual methods
.method public final a(JII)V
    .locals 11

    .prologue
    const/4 v1, 0x1

    const/4 v9, 0x6

    const/4 v8, 0x3

    const/4 v2, 0x0

    .line 78
    iget-object v0, p0, Lcom/google/android/location/copresence/o/b;->b:Ljava/util/HashMap;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/copresence/o/c;

    .line 79
    if-nez v0, :cond_0

    .line 80
    new-instance v0, Lcom/google/android/location/copresence/o/c;

    invoke-direct {v0, p0, p3, p4}, Lcom/google/android/location/copresence/o/c;-><init>(Lcom/google/android/location/copresence/o/b;II)V

    .line 81
    iget-object v3, p0, Lcom/google/android/location/copresence/o/b;->b:Ljava/util/HashMap;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v3, v4, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 83
    :cond_0
    iget v3, v0, Lcom/google/android/location/copresence/o/c;->a:I

    if-eq p3, v3, :cond_1

    .line 86
    invoke-static {v9}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 87
    const-string v3, "CopresenceCallbackCache: opCode doesn\'t match what is expected."

    invoke-static {v3}, Lcom/google/android/location/copresence/ag;->e(Ljava/lang/String;)I

    .line 91
    :cond_1
    iput p4, v0, Lcom/google/android/location/copresence/o/c;->b:I

    .line 93
    iget-object v0, p0, Lcom/google/android/location/copresence/o/b;->b:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move v3, v1

    :cond_2
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    iget-object v0, p0, Lcom/google/android/location/copresence/o/b;->b:Ljava/util/HashMap;

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v0, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/copresence/o/c;

    if-nez v0, :cond_5

    invoke-static {v9}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v0

    if-eqz v0, :cond_3

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "CopresenceCallbackCache: Null metadata for operation id: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->e(Ljava/lang/String;)I

    :cond_3
    iget-object v0, p0, Lcom/google/android/location/copresence/o/b;->c:Lcom/google/android/location/copresence/o/a;

    invoke-virtual {v0, p0}, Lcom/google/android/location/copresence/o/a;->a(Lcom/google/android/location/copresence/o/b;)V

    .line 94
    :cond_4
    :goto_1
    return-void

    .line 93
    :cond_5
    iget v5, v0, Lcom/google/android/location/copresence/o/c;->b:I

    if-eq v5, v8, :cond_6

    iget v5, v0, Lcom/google/android/location/copresence/o/c;->b:I

    const/4 v6, 0x4

    if-ne v5, v6, :cond_4

    :cond_6
    iget v5, v0, Lcom/google/android/location/copresence/o/c;->a:I

    packed-switch v5, :pswitch_data_0

    invoke-static {v9}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v5

    if-eqz v5, :cond_2

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "CopresenceCallbackCache: Unknown opCode: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v0, v0, Lcom/google/android/location/copresence/o/c;->a:I

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-instance v5, Ljava/lang/IllegalStateException;

    invoke-direct {v5}, Ljava/lang/IllegalStateException;-><init>()V

    invoke-static {v0, v5}, Lcom/google/android/location/copresence/ag;->a(Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    :pswitch_0
    iget v0, v0, Lcom/google/android/location/copresence/o/c;->b:I

    if-ne v0, v8, :cond_2

    move v3, v2

    goto :goto_0

    :pswitch_1
    iget v0, v0, Lcom/google/android/location/copresence/o/c;->b:I

    if-ne v0, v8, :cond_2

    move v1, v2

    goto :goto_0

    :cond_7
    if-eqz v3, :cond_8

    if-eqz v1, :cond_8

    :goto_2
    :try_start_0
    iget-object v0, p0, Lcom/google/android/location/copresence/o/b;->a:Lcom/google/android/gms/location/copresence/internal/i;

    new-instance v1, Lcom/google/android/gms/common/api/Status;

    invoke-direct {v1, v2}, Lcom/google/android/gms/common/api/Status;-><init>(I)V

    invoke-interface {v0, v1}, Lcom/google/android/gms/location/copresence/internal/i;->a(Lcom/google/android/gms/common/api/Status;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_3
    iget-object v0, p0, Lcom/google/android/location/copresence/o/b;->c:Lcom/google/android/location/copresence/o/a;

    invoke-virtual {v0, p0}, Lcom/google/android/location/copresence/o/a;->a(Lcom/google/android/location/copresence/o/b;)V

    goto :goto_1

    :cond_8
    const/16 v2, 0xd

    goto :goto_2

    :catch_0
    move-exception v0

    goto :goto_3

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method
