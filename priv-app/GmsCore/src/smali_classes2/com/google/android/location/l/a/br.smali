.class public final Lcom/google/android/location/l/a/br;
.super Lcom/google/protobuf/nano/d;
.source "SourceFile"


# static fields
.field private static volatile g:[Lcom/google/android/location/l/a/br;


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/lang/String;

.field public c:[Ljava/lang/String;

.field public d:[Lcom/google/android/location/l/a/bt;

.field public e:Lcom/google/android/location/l/a/co;

.field public f:Ljava/lang/Integer;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 52
    invoke-direct {p0}, Lcom/google/protobuf/nano/d;-><init>()V

    .line 53
    iput-object v1, p0, Lcom/google/android/location/l/a/br;->a:Ljava/lang/String;

    iput-object v1, p0, Lcom/google/android/location/l/a/br;->b:Ljava/lang/String;

    sget-object v0, Lcom/google/protobuf/nano/m;->f:[Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/location/l/a/br;->c:[Ljava/lang/String;

    invoke-static {}, Lcom/google/android/location/l/a/bt;->a()[Lcom/google/android/location/l/a/bt;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/l/a/br;->d:[Lcom/google/android/location/l/a/bt;

    iput-object v1, p0, Lcom/google/android/location/l/a/br;->e:Lcom/google/android/location/l/a/co;

    iput-object v1, p0, Lcom/google/android/location/l/a/br;->f:Ljava/lang/Integer;

    iput-object v1, p0, Lcom/google/android/location/l/a/br;->unknownFieldData:Lcom/google/protobuf/nano/f;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/location/l/a/br;->cachedSize:I

    .line 54
    return-void
.end method

.method public static a([B)Lcom/google/android/location/l/a/br;
    .locals 1

    .prologue
    .line 302
    new-instance v0, Lcom/google/android/location/l/a/br;

    invoke-direct {v0}, Lcom/google/android/location/l/a/br;-><init>()V

    invoke-static {v0, p0}, Lcom/google/protobuf/nano/j;->mergeFrom(Lcom/google/protobuf/nano/j;[B)Lcom/google/protobuf/nano/j;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/l/a/br;

    return-object v0
.end method

.method public static a()[Lcom/google/android/location/l/a/br;
    .locals 2

    .prologue
    .line 23
    sget-object v0, Lcom/google/android/location/l/a/br;->g:[Lcom/google/android/location/l/a/br;

    if-nez v0, :cond_1

    .line 24
    sget-object v1, Lcom/google/protobuf/nano/h;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 26
    :try_start_0
    sget-object v0, Lcom/google/android/location/l/a/br;->g:[Lcom/google/android/location/l/a/br;

    if-nez v0, :cond_0

    .line 27
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/android/location/l/a/br;

    sput-object v0, Lcom/google/android/location/l/a/br;->g:[Lcom/google/android/location/l/a/br;

    .line 29
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 31
    :cond_1
    sget-object v0, Lcom/google/android/location/l/a/br;->g:[Lcom/google/android/location/l/a/br;

    return-object v0

    .line 29
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 171
    invoke-super {p0}, Lcom/google/protobuf/nano/d;->computeSerializedSize()I

    move-result v0

    .line 172
    iget-object v1, p0, Lcom/google/android/location/l/a/br;->a:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 173
    const/4 v1, 0x1

    iget-object v3, p0, Lcom/google/android/location/l/a/br;->a:Ljava/lang/String;

    invoke-static {v1, v3}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 176
    :cond_0
    iget-object v1, p0, Lcom/google/android/location/l/a/br;->b:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 177
    const/4 v1, 0x2

    iget-object v3, p0, Lcom/google/android/location/l/a/br;->b:Ljava/lang/String;

    invoke-static {v1, v3}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 180
    :cond_1
    iget-object v1, p0, Lcom/google/android/location/l/a/br;->c:[Ljava/lang/String;

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/google/android/location/l/a/br;->c:[Ljava/lang/String;

    array-length v1, v1

    if-lez v1, :cond_4

    move v1, v2

    move v3, v2

    move v4, v2

    .line 183
    :goto_0
    iget-object v5, p0, Lcom/google/android/location/l/a/br;->c:[Ljava/lang/String;

    array-length v5, v5

    if-ge v1, v5, :cond_3

    .line 184
    iget-object v5, p0, Lcom/google/android/location/l/a/br;->c:[Ljava/lang/String;

    aget-object v5, v5, v1

    .line 185
    if-eqz v5, :cond_2

    .line 186
    add-int/lit8 v4, v4, 0x1

    .line 187
    invoke-static {v5}, Lcom/google/protobuf/nano/b;->a(Ljava/lang/String;)I

    move-result v5

    add-int/2addr v3, v5

    .line 183
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 191
    :cond_3
    add-int/2addr v0, v3

    .line 192
    mul-int/lit8 v1, v4, 0x1

    add-int/2addr v0, v1

    .line 194
    :cond_4
    iget-object v1, p0, Lcom/google/android/location/l/a/br;->d:[Lcom/google/android/location/l/a/bt;

    if-eqz v1, :cond_6

    iget-object v1, p0, Lcom/google/android/location/l/a/br;->d:[Lcom/google/android/location/l/a/bt;

    array-length v1, v1

    if-lez v1, :cond_6

    .line 195
    :goto_1
    iget-object v1, p0, Lcom/google/android/location/l/a/br;->d:[Lcom/google/android/location/l/a/bt;

    array-length v1, v1

    if-ge v2, v1, :cond_6

    .line 196
    iget-object v1, p0, Lcom/google/android/location/l/a/br;->d:[Lcom/google/android/location/l/a/bt;

    aget-object v1, v1, v2

    .line 197
    if-eqz v1, :cond_5

    .line 198
    const/4 v3, 0x4

    invoke-static {v3, v1}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 195
    :cond_5
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 203
    :cond_6
    iget-object v1, p0, Lcom/google/android/location/l/a/br;->e:Lcom/google/android/location/l/a/co;

    if-eqz v1, :cond_7

    .line 204
    const/4 v1, 0x5

    iget-object v2, p0, Lcom/google/android/location/l/a/br;->e:Lcom/google/android/location/l/a/co;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 207
    :cond_7
    iget-object v1, p0, Lcom/google/android/location/l/a/br;->f:Ljava/lang/Integer;

    if-eqz v1, :cond_8

    .line 208
    const/4 v1, 0x6

    iget-object v2, p0, Lcom/google/android/location/l/a/br;->f:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 211
    :cond_8
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 70
    if-ne p1, p0, :cond_1

    .line 71
    const/4 v0, 0x1

    .line 114
    :cond_0
    :goto_0
    return v0

    .line 73
    :cond_1
    instance-of v1, p1, Lcom/google/android/location/l/a/br;

    if-eqz v1, :cond_0

    .line 76
    check-cast p1, Lcom/google/android/location/l/a/br;

    .line 77
    iget-object v1, p0, Lcom/google/android/location/l/a/br;->a:Ljava/lang/String;

    if-nez v1, :cond_6

    .line 78
    iget-object v1, p1, Lcom/google/android/location/l/a/br;->a:Ljava/lang/String;

    if-nez v1, :cond_0

    .line 84
    :cond_2
    iget-object v1, p0, Lcom/google/android/location/l/a/br;->b:Ljava/lang/String;

    if-nez v1, :cond_7

    .line 85
    iget-object v1, p1, Lcom/google/android/location/l/a/br;->b:Ljava/lang/String;

    if-nez v1, :cond_0

    .line 91
    :cond_3
    iget-object v1, p0, Lcom/google/android/location/l/a/br;->c:[Ljava/lang/String;

    iget-object v2, p1, Lcom/google/android/location/l/a/br;->c:[Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 95
    iget-object v1, p0, Lcom/google/android/location/l/a/br;->d:[Lcom/google/android/location/l/a/bt;

    iget-object v2, p1, Lcom/google/android/location/l/a/br;->d:[Lcom/google/android/location/l/a/bt;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 99
    iget-object v1, p0, Lcom/google/android/location/l/a/br;->e:Lcom/google/android/location/l/a/co;

    if-nez v1, :cond_8

    .line 100
    iget-object v1, p1, Lcom/google/android/location/l/a/br;->e:Lcom/google/android/location/l/a/co;

    if-nez v1, :cond_0

    .line 108
    :cond_4
    iget-object v1, p0, Lcom/google/android/location/l/a/br;->f:Ljava/lang/Integer;

    if-nez v1, :cond_9

    .line 109
    iget-object v1, p1, Lcom/google/android/location/l/a/br;->f:Ljava/lang/Integer;

    if-nez v1, :cond_0

    .line 114
    :cond_5
    invoke-virtual {p0, p1}, Lcom/google/android/location/l/a/br;->unknownFieldDataEquals(Lcom/google/protobuf/nano/d;)Z

    move-result v0

    goto :goto_0

    .line 81
    :cond_6
    iget-object v1, p0, Lcom/google/android/location/l/a/br;->a:Ljava/lang/String;

    iget-object v2, p1, Lcom/google/android/location/l/a/br;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    goto :goto_0

    .line 88
    :cond_7
    iget-object v1, p0, Lcom/google/android/location/l/a/br;->b:Ljava/lang/String;

    iget-object v2, p1, Lcom/google/android/location/l/a/br;->b:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    goto :goto_0

    .line 104
    :cond_8
    iget-object v1, p0, Lcom/google/android/location/l/a/br;->e:Lcom/google/android/location/l/a/co;

    iget-object v2, p1, Lcom/google/android/location/l/a/br;->e:Lcom/google/android/location/l/a/co;

    invoke-virtual {v1, v2}, Lcom/google/android/location/l/a/co;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    goto :goto_0

    .line 112
    :cond_9
    iget-object v1, p0, Lcom/google/android/location/l/a/br;->f:Ljava/lang/Integer;

    iget-object v2, p1, Lcom/google/android/location/l/a/br;->f:Ljava/lang/Integer;

    invoke-virtual {v1, v2}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_5

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 119
    iget-object v0, p0, Lcom/google/android/location/l/a/br;->a:Ljava/lang/String;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    .line 122
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/location/l/a/br;->b:Ljava/lang/String;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    .line 124
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/android/location/l/a/br;->c:[Ljava/lang/String;

    invoke-static {v2}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;)I

    move-result v2

    add-int/2addr v0, v2

    .line 126
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/android/location/l/a/br;->d:[Lcom/google/android/location/l/a/bt;

    invoke-static {v2}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;)I

    move-result v2

    add-int/2addr v0, v2

    .line 128
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/location/l/a/br;->e:Lcom/google/android/location/l/a/co;

    if-nez v0, :cond_2

    move v0, v1

    :goto_2
    add-int/2addr v0, v2

    .line 130
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/android/location/l/a/br;->f:Ljava/lang/Integer;

    if-nez v2, :cond_3

    :goto_3
    add-int/2addr v0, v1

    .line 131
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/google/android/location/l/a/br;->unknownFieldDataHashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 132
    return v0

    .line 119
    :cond_0
    iget-object v0, p0, Lcom/google/android/location/l/a/br;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_0

    .line 122
    :cond_1
    iget-object v0, p0, Lcom/google/android/location/l/a/br;->b:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_1

    .line 128
    :cond_2
    iget-object v0, p0, Lcom/google/android/location/l/a/br;->e:Lcom/google/android/location/l/a/co;

    invoke-virtual {v0}, Lcom/google/android/location/l/a/co;->hashCode()I

    move-result v0

    goto :goto_2

    .line 130
    :cond_3
    iget-object v1, p0, Lcom/google/android/location/l/a/br;->f:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    goto :goto_3
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 5
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/android/location/l/a/br;->storeUnknownField(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/l/a/br;->a:Ljava/lang/String;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/l/a/br;->b:Ljava/lang/String;

    goto :goto_0

    :sswitch_3
    const/16 v0, 0x1a

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->b(Lcom/google/protobuf/nano/a;I)I

    move-result v2

    iget-object v0, p0, Lcom/google/android/location/l/a/br;->c:[Ljava/lang/String;

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v3, p0, Lcom/google/android/location/l/a/br;->c:[Ljava/lang/String;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_1
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_3

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    iget-object v0, p0, Lcom/google/android/location/l/a/br;->c:[Ljava/lang/String;

    array-length v0, v0

    goto :goto_1

    :cond_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    iput-object v2, p0, Lcom/google/android/location/l/a/br;->c:[Ljava/lang/String;

    goto :goto_0

    :sswitch_4
    const/16 v0, 0x22

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->b(Lcom/google/protobuf/nano/a;I)I

    move-result v2

    iget-object v0, p0, Lcom/google/android/location/l/a/br;->d:[Lcom/google/android/location/l/a/bt;

    if-nez v0, :cond_5

    move v0, v1

    :goto_3
    add-int/2addr v2, v0

    new-array v2, v2, [Lcom/google/android/location/l/a/bt;

    if-eqz v0, :cond_4

    iget-object v3, p0, Lcom/google/android/location/l/a/br;->d:[Lcom/google/android/location/l/a/bt;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_4
    :goto_4
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_6

    new-instance v3, Lcom/google/android/location/l/a/bt;

    invoke-direct {v3}, Lcom/google/android/location/l/a/bt;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_5
    iget-object v0, p0, Lcom/google/android/location/l/a/br;->d:[Lcom/google/android/location/l/a/bt;

    array-length v0, v0

    goto :goto_3

    :cond_6
    new-instance v3, Lcom/google/android/location/l/a/bt;

    invoke-direct {v3}, Lcom/google/android/location/l/a/bt;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    iput-object v2, p0, Lcom/google/android/location/l/a/br;->d:[Lcom/google/android/location/l/a/bt;

    goto/16 :goto_0

    :sswitch_5
    iget-object v0, p0, Lcom/google/android/location/l/a/br;->e:Lcom/google/android/location/l/a/co;

    if-nez v0, :cond_7

    new-instance v0, Lcom/google/android/location/l/a/co;

    invoke-direct {v0}, Lcom/google/android/location/l/a/co;-><init>()V

    iput-object v0, p0, Lcom/google/android/location/l/a/br;->e:Lcom/google/android/location/l/a/co;

    :cond_7
    iget-object v0, p0, Lcom/google/android/location/l/a/br;->e:Lcom/google/android/location/l/a/co;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto/16 :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto/16 :goto_0

    :pswitch_0
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/l/a/br;->f:Ljava/lang/Integer;

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x30 -> :sswitch_6
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 138
    iget-object v0, p0, Lcom/google/android/location/l/a/br;->a:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 139
    const/4 v0, 0x1

    iget-object v2, p0, Lcom/google/android/location/l/a/br;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v2}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 141
    :cond_0
    iget-object v0, p0, Lcom/google/android/location/l/a/br;->b:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 142
    const/4 v0, 0x2

    iget-object v2, p0, Lcom/google/android/location/l/a/br;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v2}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 144
    :cond_1
    iget-object v0, p0, Lcom/google/android/location/l/a/br;->c:[Ljava/lang/String;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/location/l/a/br;->c:[Ljava/lang/String;

    array-length v0, v0

    if-lez v0, :cond_3

    move v0, v1

    .line 145
    :goto_0
    iget-object v2, p0, Lcom/google/android/location/l/a/br;->c:[Ljava/lang/String;

    array-length v2, v2

    if-ge v0, v2, :cond_3

    .line 146
    iget-object v2, p0, Lcom/google/android/location/l/a/br;->c:[Ljava/lang/String;

    aget-object v2, v2, v0

    .line 147
    if-eqz v2, :cond_2

    .line 148
    const/4 v3, 0x3

    invoke-virtual {p1, v3, v2}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 145
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 152
    :cond_3
    iget-object v0, p0, Lcom/google/android/location/l/a/br;->d:[Lcom/google/android/location/l/a/bt;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/google/android/location/l/a/br;->d:[Lcom/google/android/location/l/a/bt;

    array-length v0, v0

    if-lez v0, :cond_5

    .line 153
    :goto_1
    iget-object v0, p0, Lcom/google/android/location/l/a/br;->d:[Lcom/google/android/location/l/a/bt;

    array-length v0, v0

    if-ge v1, v0, :cond_5

    .line 154
    iget-object v0, p0, Lcom/google/android/location/l/a/br;->d:[Lcom/google/android/location/l/a/bt;

    aget-object v0, v0, v1

    .line 155
    if-eqz v0, :cond_4

    .line 156
    const/4 v2, 0x4

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 153
    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 160
    :cond_5
    iget-object v0, p0, Lcom/google/android/location/l/a/br;->e:Lcom/google/android/location/l/a/co;

    if-eqz v0, :cond_6

    .line 161
    const/4 v0, 0x5

    iget-object v1, p0, Lcom/google/android/location/l/a/br;->e:Lcom/google/android/location/l/a/co;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 163
    :cond_6
    iget-object v0, p0, Lcom/google/android/location/l/a/br;->f:Ljava/lang/Integer;

    if-eqz v0, :cond_7

    .line 164
    const/4 v0, 0x6

    iget-object v1, p0, Lcom/google/android/location/l/a/br;->f:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 166
    :cond_7
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/d;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 167
    return-void
.end method
