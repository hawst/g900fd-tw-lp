.class public abstract Lcom/google/android/location/copresence/l;
.super Lcom/google/android/gms/common/a/d;
.source "SourceFile"


# static fields
.field static d:Landroid/content/SharedPreferences;


# instance fields
.field protected e:Lcom/google/android/gms/common/a/d;


# direct methods
.method protected constructor <init>(Ljava/lang/String;Lcom/google/android/gms/common/a/d;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 50
    invoke-direct {p0, p1, p3}, Lcom/google/android/gms/common/a/d;-><init>(Ljava/lang/String;Ljava/lang/Object;)V

    .line 51
    iput-object p2, p0, Lcom/google/android/location/copresence/l;->e:Lcom/google/android/gms/common/a/d;

    .line 52
    return-void
.end method

.method public static a(Ljava/lang/String;I)Lcom/google/android/location/copresence/l;
    .locals 3

    .prologue
    .line 197
    new-instance v0, Lcom/google/android/location/copresence/o;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {p0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/Integer;)Lcom/google/android/gms/common/a/d;

    move-result-object v1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-direct {v0, p0, v1, v2, p1}, Lcom/google/android/location/copresence/o;-><init>(Ljava/lang/String;Lcom/google/android/gms/common/a/d;Ljava/lang/Integer;I)V

    return-object v0
.end method

.method public static a(Ljava/lang/String;J)Lcom/google/android/location/copresence/l;
    .locals 7

    .prologue
    .line 175
    new-instance v0, Lcom/google/android/location/copresence/n;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {p0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/Long;)Lcom/google/android/gms/common/a/d;

    move-result-object v2

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    move-object v1, p0

    move-wide v4, p1

    invoke-direct/range {v0 .. v5}, Lcom/google/android/location/copresence/n;-><init>(Ljava/lang/String;Lcom/google/android/gms/common/a/d;Ljava/lang/Long;J)V

    return-object v0
.end method

.method public static b(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/location/copresence/l;
    .locals 2

    .prologue
    .line 241
    new-instance v0, Lcom/google/android/location/copresence/q;

    invoke-static {p0, p1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/a/d;

    move-result-object v1

    invoke-direct {v0, p0, v1, p1, p1}, Lcom/google/android/location/copresence/q;-><init>(Ljava/lang/String;Lcom/google/android/gms/common/a/d;Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public static b(Ljava/lang/String;Z)Lcom/google/android/location/copresence/l;
    .locals 3

    .prologue
    .line 154
    new-instance v0, Lcom/google/android/location/copresence/m;

    invoke-static {p0, p1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Z)Lcom/google/android/gms/common/a/d;

    move-result-object v1

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-direct {v0, p0, v1, v2, p1}, Lcom/google/android/location/copresence/m;-><init>(Ljava/lang/String;Lcom/google/android/gms/common/a/d;Ljava/lang/Boolean;Z)V

    return-object v0
.end method

.method public static declared-synchronized b(Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 27
    const-class v1, Lcom/google/android/location/copresence/l;

    monitor-enter v1

    :try_start_0
    invoke-static {p0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 28
    sget-object v0, Lcom/google/android/location/copresence/l;->d:Landroid/content/SharedPreferences;

    if-nez v0, :cond_0

    .line 29
    const-string v0, "copresence_preferences"

    const/4 v2, 0x0

    invoke-virtual {p0, v0, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    sput-object v0, Lcom/google/android/location/copresence/l;->d:Landroid/content/SharedPreferences;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 32
    :cond_0
    monitor-exit v1

    return-void

    .line 27
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static c(Ljava/lang/String;)Lcom/google/android/location/copresence/l;
    .locals 4

    .prologue
    const/high16 v3, 0x40400000    # 3.0f

    .line 219
    new-instance v0, Lcom/google/android/location/copresence/p;

    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    invoke-static {p0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/Float;)Lcom/google/android/gms/common/a/d;

    move-result-object v1

    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    invoke-direct {v0, p0, v1, v2, v3}, Lcom/google/android/location/copresence/p;-><init>(Ljava/lang/String;Lcom/google/android/gms/common/a/d;Ljava/lang/Float;F)V

    return-object v0
.end method


# virtual methods
.method protected final a(Ljava/lang/String;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 60
    sget-object v0, Lcom/google/android/location/copresence/l;->d:Landroid/content/SharedPreferences;

    if-nez v0, :cond_1

    .line 61
    iget-object v0, p0, Lcom/google/android/location/copresence/l;->e:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    .line 69
    :cond_0
    :goto_0
    return-object v0

    .line 63
    :cond_1
    sget-object v0, Lcom/google/android/location/copresence/l;->d:Landroid/content/SharedPreferences;

    invoke-interface {v0, p1}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 64
    invoke-virtual {p0, p1}, Lcom/google/android/location/copresence/l;->b(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 65
    if-nez v0, :cond_0

    .line 69
    :cond_2
    iget-object v0, p0, Lcom/google/android/location/copresence/l;->e:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    goto :goto_0
.end method

.method protected abstract b(Ljava/lang/String;)Ljava/lang/Object;
.end method
