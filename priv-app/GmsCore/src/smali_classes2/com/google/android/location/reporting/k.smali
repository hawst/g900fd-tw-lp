.class public final Lcom/google/android/location/reporting/k;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Lcom/google/android/location/reporting/config/ConfigGetter;

.field private final c:Lcom/google/android/location/reporting/x;

.field private final d:Lcom/google/android/location/reporting/s;

.field private final e:Lcom/google/android/location/reporting/LocationRecordStore;

.field private final f:Lcom/google/android/location/reporting/DetectedActivityStore;

.field private final g:Lcom/google/android/location/reporting/ApiMetadataStore;

.field private final h:Lcom/google/android/location/fused/g;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/location/reporting/config/ConfigGetter;Lcom/google/android/location/reporting/s;Lcom/google/android/location/reporting/LocationRecordStore;Lcom/google/android/location/reporting/DetectedActivityStore;Lcom/google/android/location/reporting/ApiMetadataStore;Lcom/google/android/location/fused/g;)V
    .locals 1

    .prologue
    .line 70
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 71
    iput-object p1, p0, Lcom/google/android/location/reporting/k;->a:Landroid/content/Context;

    .line 72
    iput-object p2, p0, Lcom/google/android/location/reporting/k;->b:Lcom/google/android/location/reporting/config/ConfigGetter;

    .line 73
    invoke-static {p1}, Lcom/google/android/location/reporting/v;->a(Landroid/content/Context;)Lcom/google/android/location/reporting/v;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/reporting/k;->c:Lcom/google/android/location/reporting/x;

    .line 74
    iput-object p3, p0, Lcom/google/android/location/reporting/k;->d:Lcom/google/android/location/reporting/s;

    .line 75
    iput-object p4, p0, Lcom/google/android/location/reporting/k;->e:Lcom/google/android/location/reporting/LocationRecordStore;

    .line 76
    iput-object p5, p0, Lcom/google/android/location/reporting/k;->f:Lcom/google/android/location/reporting/DetectedActivityStore;

    .line 77
    iput-object p6, p0, Lcom/google/android/location/reporting/k;->g:Lcom/google/android/location/reporting/ApiMetadataStore;

    .line 78
    iput-object p7, p0, Lcom/google/android/location/reporting/k;->h:Lcom/google/android/location/fused/g;

    .line 79
    return-void
.end method

.method private static a(Lcom/google/android/location/reporting/e;Landroid/accounts/Account;)Ljava/util/ArrayList;
    .locals 5

    .prologue
    .line 281
    :try_start_0
    invoke-virtual {p0, p1}, Lcom/google/android/location/reporting/e;->retrieveEntities(Landroid/accounts/Account;)Ljava/util/ArrayList;
    :try_end_0
    .catch Lcom/google/android/location/reporting/f; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 288
    :goto_0
    return-object v0

    .line 282
    :catch_0
    move-exception v0

    .line 283
    const-string v1, "GCoreUlr"

    const/4 v2, 0x5

    invoke-static {v1, v2}, Lcom/google/android/location/reporting/b/d;->a(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 284
    invoke-static {p1}, Lcom/google/android/gms/location/reporting/a/d;->a(Landroid/accounts/Account;)Ljava/lang/String;

    move-result-object v1

    .line 285
    const-string v2, "GCoreUlr"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "DB error retrieving entities for account "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1, v0}, Lcom/google/android/location/reporting/b/d;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 287
    :cond_0
    invoke-static {v0}, Lcom/google/android/location/reporting/b/l;->g(Ljava/lang/Exception;)V

    .line 288
    new-instance v0, Ljava/util/ArrayList;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    goto :goto_0
.end method

.method private static a(Landroid/accounts/Account;Ljava/lang/Exception;)V
    .locals 3

    .prologue
    .line 138
    invoke-static {p1}, Lcom/google/android/location/reporting/b/l;->e(Ljava/lang/Exception;)V

    .line 139
    const-string v0, "GCoreUlr"

    const/4 v1, 0x4

    invoke-static {v0, v1}, Lcom/google/android/location/reporting/b/d;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 140
    const-string v0, "GCoreUlr"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Batch Location Update failed for account "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {p0}, Lcom/google/android/gms/location/reporting/a/d;->a(Landroid/accounts/Account;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ": "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/location/reporting/b/d;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 143
    :cond_0
    return-void
.end method

.method private static a(Lcom/google/android/location/reporting/e;Landroid/accounts/Account;J)V
    .locals 4

    .prologue
    .line 296
    :try_start_0
    invoke-virtual {p0, p1, p2, p3}, Lcom/google/android/location/reporting/e;->a(Landroid/accounts/Account;J)V
    :try_end_0
    .catch Lcom/google/android/location/reporting/f; {:try_start_0 .. :try_end_0} :catch_0

    .line 303
    :goto_0
    return-void

    .line 297
    :catch_0
    move-exception v0

    .line 298
    const-string v1, "GCoreUlr"

    const/4 v2, 0x5

    invoke-static {v1, v2}, Lcom/google/android/location/reporting/b/d;->a(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 299
    const-string v1, "GCoreUlr"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "DB error deleting entities for "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lcom/google/android/location/reporting/b/d;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 301
    :cond_0
    invoke-static {v0}, Lcom/google/android/location/reporting/b/l;->f(Ljava/lang/Exception;)V

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 22

    .prologue
    .line 82
    const-string v2, "GCoreUlr"

    const/4 v3, 0x3

    invoke-static {v2, v3}, Lcom/google/android/location/reporting/b/d;->a(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 83
    const-string v2, "GCoreUlr"

    const-string v3, "LocationReporter has started."

    invoke-static {v2, v3}, Lcom/google/android/location/reporting/b/d;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 86
    :cond_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/location/reporting/k;->b:Lcom/google/android/location/reporting/config/ConfigGetter;

    invoke-interface {v2}, Lcom/google/android/location/reporting/config/ConfigGetter;->a()Lcom/google/android/location/reporting/config/ReportingConfig;

    move-result-object v4

    .line 87
    invoke-virtual {v4}, Lcom/google/android/location/reporting/config/ReportingConfig;->getActiveAccounts()Ljava/util/List;

    move-result-object v3

    .line 88
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/location/reporting/k;->e:Lcom/google/android/location/reporting/LocationRecordStore;

    invoke-static {v2, v3}, Lcom/google/android/location/reporting/b/n;->a(Lcom/google/android/location/reporting/e;Ljava/util/List;)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/location/reporting/k;->f:Lcom/google/android/location/reporting/DetectedActivityStore;

    invoke-static {v2, v3}, Lcom/google/android/location/reporting/b/n;->a(Lcom/google/android/location/reporting/e;Ljava/util/List;)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/location/reporting/k;->g:Lcom/google/android/location/reporting/ApiMetadataStore;

    invoke-static {v2, v3}, Lcom/google/android/location/reporting/b/n;->a(Lcom/google/android/location/reporting/e;Ljava/util/List;)V

    .line 89
    const-string v2, "attempt"

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v5

    int-to-long v6, v5

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-static {v2, v5}, Lcom/google/android/location/reporting/b/l;->a(Ljava/lang/String;Ljava/lang/Long;)V

    .line 91
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/location/reporting/k;->a:Landroid/content/Context;

    const-string v5, "connectivity"

    invoke-virtual {v2, v5}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/net/ConnectivityManager;

    invoke-virtual {v2}, Landroid/net/ConnectivityManager;->getBackgroundDataSetting()Z

    move-result v2

    if-eqz v2, :cond_3

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/location/reporting/k;->a:Landroid/content/Context;

    invoke-static {v2}, Lcom/google/android/gms/common/util/ak;->a(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_3

    const/4 v2, 0x1

    :goto_0
    if-nez v2, :cond_4

    .line 92
    const-string v2, "GCoreUlr"

    const/4 v4, 0x4

    invoke-static {v2, v4}, Lcom/google/android/location/reporting/b/d;->a(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 93
    const-string v2, "GCoreUlr"

    const-string v4, "Batch Location Update aborted because not connected"

    invoke-static {v2, v4}, Lcom/google/android/location/reporting/b/d;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 95
    :cond_1
    const-string v2, "no_connection"

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    int-to-long v4, v3

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/location/reporting/b/l;->a(Ljava/lang/String;Ljava/lang/Long;)V

    .line 131
    :cond_2
    :goto_1
    return-void

    .line 91
    :cond_3
    const/4 v2, 0x0

    goto :goto_0

    .line 100
    :cond_4
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v12

    .line 101
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/location/reporting/k;->d:Lcom/google/android/location/reporting/s;

    invoke-virtual {v2}, Lcom/google/android/location/reporting/s;->c()Ljava/util/List;

    move-result-object v14

    .line 104
    const/4 v3, 0x0

    .line 105
    const/4 v2, 0x0

    .line 106
    invoke-virtual {v4}, Lcom/google/android/location/reporting/config/ReportingConfig;->getActiveAccountConfigs()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v15

    move v10, v2

    move v11, v3

    :goto_2
    invoke-interface {v15}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_e

    invoke-interface {v15}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    move-object v3, v2

    check-cast v3, Lcom/google/android/location/reporting/config/AccountConfig;

    .line 107
    invoke-virtual {v3}, Lcom/google/android/location/reporting/config/AccountConfig;->b()Landroid/accounts/Account;

    move-result-object v16

    .line 109
    :try_start_0
    invoke-virtual {v3}, Lcom/google/android/location/reporting/config/AccountConfig;->b()Landroid/accounts/Account;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/gms/location/reporting/a/d;->a(Landroid/accounts/Account;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3}, Lcom/google/android/location/reporting/config/AccountConfig;->m()Z

    move-result v5

    if-eqz v5, :cond_6

    const-string v3, "GCoreUlr"

    const/4 v5, 0x4

    invoke-static {v3, v5}, Lcom/google/android/location/reporting/b/d;->a(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_5

    const-string v3, "GCoreUlr"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Settings dirty, skipping upload for "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/android/location/reporting/b/d;->c(Ljava/lang/String;Ljava/lang/String;)V

    :cond_5
    const-string v3, "LocationReporter"

    invoke-static {v2, v3}, Lcom/google/android/location/reporting/service/ReportingSyncService;->a(Landroid/accounts/Account;Ljava/lang/String;)V

    const-string v2, "dirty"

    const-wide/16 v4, 0x1

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/location/reporting/b/l;->a(Ljava/lang/String;Ljava/lang/Long;)V

    const/4 v2, 0x0

    :goto_3
    or-int/2addr v2, v11

    move v11, v2

    .line 121
    goto :goto_2

    .line 109
    :cond_6
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/location/reporting/k;->e:Lcom/google/android/location/reporting/LocationRecordStore;

    invoke-static {v5, v2}, Lcom/google/android/location/reporting/k;->a(Lcom/google/android/location/reporting/e;Landroid/accounts/Account;)Ljava/util/ArrayList;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/location/reporting/k;->f:Lcom/google/android/location/reporting/DetectedActivityStore;

    invoke-static {v6, v2}, Lcom/google/android/location/reporting/k;->a(Lcom/google/android/location/reporting/e;Landroid/accounts/Account;)Ljava/util/ArrayList;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/location/reporting/k;->g:Lcom/google/android/location/reporting/ApiMetadataStore;

    invoke-static {v7, v2}, Lcom/google/android/location/reporting/k;->a(Lcom/google/android/location/reporting/e;Landroid/accounts/Account;)Ljava/util/ArrayList;

    move-result-object v7

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/location/reporting/k;->h:Lcom/google/android/location/fused/g;

    invoke-virtual {v2}, Lcom/google/android/location/fused/g;->d()Lcom/google/android/gms/location/LocationStatus;

    move-result-object v2

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    invoke-static {v2, v8, v9}, Lcom/google/android/location/reporting/c;->a(Lcom/google/android/gms/location/LocationStatus;J)Lcom/google/android/ulr/ApiMetadata;

    move-result-object v2

    invoke-virtual {v7, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-interface {v5}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_8

    const-string v2, "GCoreUlr"

    const/4 v3, 0x3

    invoke-static {v2, v3}, Lcom/google/android/location/reporting/b/d;->a(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_7

    const-string v2, "GCoreUlr"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v5, "Skipping account with no locations: "

    invoke-direct {v3, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/location/reporting/b/d;->b(Ljava/lang/String;Ljava/lang/String;)V

    :cond_7
    const-string v2, "empty"

    const-wide/16 v4, 0x1

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/location/reporting/b/l;->a(Ljava/lang/String;Ljava/lang/Long;)V

    const/4 v2, 0x0

    goto :goto_3

    :cond_8
    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-interface {v5, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/location/reporting/a/f;

    iget-wide v0, v2, Lcom/google/android/location/reporting/a/f;->f:J

    move-wide/from16 v18, v0

    const-string v2, "GCoreUlr"

    const/4 v8, 0x3

    invoke-static {v2, v8}, Lcom/google/android/location/reporting/b/d;->a(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_9

    const-string v2, "GCoreUlr"

    sget-object v8, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v9, "LocationReporter sending %d locations, %d activities, and %d metadatas for account %s; requests: %s"

    const/16 v17, 0x5

    move/from16 v0, v17

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v17, v0

    const/16 v20, 0x0

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v21

    invoke-static/range {v21 .. v21}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v21

    aput-object v21, v17, v20

    const/16 v20, 0x1

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v21

    invoke-static/range {v21 .. v21}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v21

    aput-object v21, v17, v20

    const/16 v20, 0x2

    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v21

    invoke-static/range {v21 .. v21}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v21

    aput-object v21, v17, v20

    const/16 v20, 0x3

    aput-object v4, v17, v20

    const/4 v4, 0x4

    aput-object v14, v17, v4

    move-object/from16 v0, v17

    invoke-static {v8, v9, v0}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Lcom/google/android/location/reporting/b/d;->b(Ljava/lang/String;Ljava/lang/String;)V

    :cond_9
    const-string v2, "GCoreUlr"

    const/4 v4, 0x2

    invoke-static {v2, v4}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_a

    const-string v2, "GCoreUlr"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v8, "Locations: "

    invoke-direct {v4, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v5}, Lcom/google/android/location/reporting/b/j;->wrap(Ljava/lang/Iterable;)Ljava/lang/Iterable;

    move-result-object v8

    invoke-virtual {v4, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    const-string v2, "GCoreUlr"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v8, "Activities: "

    invoke-direct {v4, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    const-string v2, "GCoreUlr"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v8, "Metadatas: "

    invoke-direct {v4, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_a
    invoke-virtual {v3}, Lcom/google/android/location/reporting/config/AccountConfig;->b()Landroid/accounts/Account;

    move-result-object v4

    invoke-static {v4}, Lcom/google/android/gms/location/reporting/a/d;->a(Landroid/accounts/Account;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3}, Lcom/google/android/location/reporting/config/AccountConfig;->j()J

    move-result-wide v8

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/location/reporting/k;->c:Lcom/google/android/location/reporting/x;

    invoke-interface/range {v3 .. v9}, Lcom/google/android/location/reporting/x;->a(Landroid/accounts/Account;Ljava/util/List;Ljava/util/List;Ljava/util/ArrayList;J)Lcom/google/android/location/reporting/a;

    move-result-object v3

    if-eqz v3, :cond_c

    const-string v2, "GCoreUlr"

    const/16 v17, 0x4

    move/from16 v0, v17

    invoke-static {v2, v0}, Lcom/google/android/location/reporting/b/d;->a(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_b

    const-string v2, "GCoreUlr"

    new-instance v17, Ljava/lang/StringBuilder;

    const-string v18, "Server reports setting change occurred after "

    invoke-direct/range {v17 .. v18}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v17

    invoke-virtual {v0, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ", requesting sync: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v2, v8}, Lcom/google/android/location/reporting/b/d;->c(Ljava/lang/String;Ljava/lang/String;)V

    :cond_b
    const-string v2, "LocationReporter"

    invoke-static {v4, v2}, Lcom/google/android/location/reporting/service/ReportingSyncService;->a(Landroid/accounts/Account;Ljava/lang/String;)V

    invoke-static {v3, v5, v6, v7}, Lcom/google/android/location/reporting/b/l;->a(Lcom/google/android/location/reporting/a;Ljava/util/List;Ljava/util/List;Ljava/util/ArrayList;)V

    :goto_4
    const/4 v2, 0x1

    goto/16 :goto_3

    :cond_c
    const-string v3, "GCoreUlr"

    const/4 v8, 0x4

    invoke-static {v3, v8}, Lcom/google/android/location/reporting/b/d;->a(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_d

    const-string v3, "GCoreUlr"

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "Batch Location Update succeeded for account "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v2}, Lcom/google/android/location/reporting/b/d;->c(Ljava/lang/String;Ljava/lang/String;)V

    :cond_d
    invoke-static {v5, v6, v7}, Lcom/google/android/location/reporting/b/l;->a(Ljava/util/List;Ljava/util/List;Ljava/util/List;)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/location/reporting/k;->e:Lcom/google/android/location/reporting/LocationRecordStore;

    move-wide/from16 v0, v18

    invoke-static {v2, v4, v0, v1}, Lcom/google/android/location/reporting/k;->a(Lcom/google/android/location/reporting/e;Landroid/accounts/Account;J)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/location/reporting/k;->f:Lcom/google/android/location/reporting/DetectedActivityStore;

    move-wide/from16 v0, v18

    invoke-static {v2, v4, v0, v1}, Lcom/google/android/location/reporting/k;->a(Lcom/google/android/location/reporting/e;Landroid/accounts/Account;J)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/location/reporting/k;->g:Lcom/google/android/location/reporting/ApiMetadataStore;

    move-wide/from16 v0, v18

    invoke-static {v2, v4, v0, v1}, Lcom/google/android/location/reporting/k;->a(Lcom/google/android/location/reporting/e;Landroid/accounts/Account;J)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/google/android/gms/auth/q; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_2

    goto :goto_4

    .line 110
    :catch_0
    move-exception v2

    move-object v3, v2

    .line 111
    const/4 v2, 0x1

    .line 112
    move-object/from16 v0, v16

    invoke-static {v0, v3}, Lcom/google/android/location/reporting/k;->a(Landroid/accounts/Account;Ljava/lang/Exception;)V

    move v10, v2

    .line 121
    goto/16 :goto_2

    .line 113
    :catch_1
    move-exception v2

    move-object v3, v2

    .line 114
    const/4 v2, 0x1

    .line 115
    move-object/from16 v0, v16

    invoke-static {v0, v3}, Lcom/google/android/location/reporting/k;->a(Landroid/accounts/Account;Ljava/lang/Exception;)V

    move v10, v2

    .line 121
    goto/16 :goto_2

    .line 116
    :catch_2
    move-exception v2

    move-object v3, v2

    .line 117
    const/4 v2, 0x1

    .line 118
    invoke-static {v3}, Lcom/google/android/location/reporting/b/l;->a(Ljava/lang/RuntimeException;)V

    .line 119
    const-string v4, "GCoreUlr"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Batch Location Update failed for account "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static/range {v16 .. v16}, Lcom/google/android/gms/location/reporting/a/d;->a(Landroid/accounts/Account;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5, v3}, Lcom/google/android/location/reporting/b/d;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    move v10, v2

    .line 122
    goto/16 :goto_2

    .line 128
    :cond_e
    if-nez v11, :cond_f

    if-eqz v10, :cond_2

    .line 129
    :cond_f
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/location/reporting/k;->d:Lcom/google/android/location/reporting/s;

    invoke-virtual {v2, v12, v13}, Lcom/google/android/location/reporting/s;->c(J)Z

    goto/16 :goto_1
.end method
