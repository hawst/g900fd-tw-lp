.class public final Lcom/google/android/location/places/c/l;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 70
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 71
    iput-object p1, p0, Lcom/google/android/location/places/c/l;->a:Landroid/content/Context;

    .line 72
    return-void
.end method

.method private static a(D)I
    .locals 2

    .prologue
    .line 355
    const-wide v0, 0x416312d000000000L    # 1.0E7

    mul-double/2addr v0, p0

    invoke-static {v0, v1}, Ljava/lang/Math;->round(D)J

    move-result-wide v0

    long-to-int v0, v0

    return v0
.end method

.method private static a(I)Lcom/google/android/gms/location/places/UserDataType;
    .locals 3

    .prologue
    .line 346
    sget-object v0, Lcom/google/android/gms/location/places/UserDataType;->d:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/location/places/UserDataType;

    .line 347
    invoke-virtual {v0}, Lcom/google/android/gms/location/places/UserDataType;->b()I

    move-result v2

    if-ne v2, p0, :cond_0

    .line 351
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static a(Lcom/google/android/location/places/d/a/m;)Lcom/google/android/gms/location/places/personalized/HereContent;
    .locals 5

    .prologue
    .line 277
    iget-object v0, p0, Lcom/google/android/location/places/d/a/m;->a:[Lcom/google/android/location/places/d/a/l;

    array-length v1, v0

    .line 278
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 279
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    .line 280
    iget-object v3, p0, Lcom/google/android/location/places/d/a/m;->a:[Lcom/google/android/location/places/d/a/l;

    aget-object v3, v3, v0

    iget-object v3, v3, Lcom/google/android/location/places/d/a/l;->a:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/location/places/d/a/m;->a:[Lcom/google/android/location/places/d/a/l;

    aget-object v4, v4, v0

    iget-object v4, v4, Lcom/google/android/location/places/d/a/l;->b:Ljava/lang/String;

    invoke-static {v3, v4}, Lcom/google/android/gms/location/places/personalized/HereContent$Action;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/location/places/personalized/HereContent$Action;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 279
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 283
    :cond_0
    iget-object v0, p0, Lcom/google/android/location/places/d/a/m;->b:Ljava/lang/String;

    invoke-static {v0, v2}, Lcom/google/android/gms/location/places/personalized/HereContent;->a(Ljava/lang/String;Ljava/util/List;)Lcom/google/android/gms/location/places/personalized/HereContent;

    move-result-object v0

    return-object v0
.end method

.method private static a(Lcom/google/android/location/places/d/a/r;)Lcom/google/android/gms/maps/model/LatLng;
    .locals 8

    .prologue
    const-wide v6, 0x416312d000000000L    # 1.0E7

    .line 254
    new-instance v0, Lcom/google/android/gms/maps/model/LatLng;

    iget-object v1, p0, Lcom/google/android/location/places/d/a/r;->a:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    int-to-double v2, v1

    div-double/2addr v2, v6

    iget-object v1, p0, Lcom/google/android/location/places/d/a/r;->b:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    int-to-double v4, v1

    div-double/2addr v4, v6

    invoke-direct {v0, v2, v3, v4, v5}, Lcom/google/android/gms/maps/model/LatLng;-><init>(DD)V

    return-object v0
.end method

.method public static a(Lcom/google/android/location/places/d/a/i;)Lcom/google/android/location/places/c/i;
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 99
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    .line 100
    iget-object v3, p0, Lcom/google/android/location/places/d/a/i;->c:Lcom/google/android/location/places/d/a/s;

    .line 101
    iget-object v0, v3, Lcom/google/android/location/places/d/a/s;->b:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v4

    .line 102
    sget-object v0, Lcom/google/android/gms/location/places/UserDataType;->d:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/location/places/UserDataType;

    .line 103
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-interface {v2, v0, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 105
    :cond_0
    iget-object v3, v3, Lcom/google/android/location/places/d/a/s;->c:[Lcom/google/android/location/places/d/a/t;

    array-length v4, v3

    move v0, v1

    :goto_1
    if-ge v0, v4, :cond_2

    aget-object v5, v3, v0

    .line 106
    iget-object v6, v5, Lcom/google/android/location/places/d/a/t;->a:Ljava/lang/Integer;

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v6

    invoke-static {v6}, Lcom/google/android/location/places/c/l;->a(I)Lcom/google/android/gms/location/places/UserDataType;

    move-result-object v6

    .line 107
    if-eqz v6, :cond_1

    .line 108
    iget-object v5, v5, Lcom/google/android/location/places/d/a/t;->b:Ljava/lang/Integer;

    invoke-interface {v2, v6, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 105
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 112
    :cond_2
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 113
    iget-object v4, p0, Lcom/google/android/location/places/d/a/i;->a:[Lcom/google/android/location/places/d/a/q;

    array-length v5, v4

    move v0, v1

    :goto_2
    if-ge v0, v5, :cond_3

    aget-object v1, v4, v0

    .line 114
    invoke-static {v1}, Lcom/google/android/location/places/c/l;->a(Lcom/google/android/location/places/d/a/q;)Lcom/google/android/location/places/d/a;

    move-result-object v1

    invoke-interface {v3, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 113
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 117
    :cond_3
    new-instance v0, Lcom/google/android/location/places/b/j;

    iget-object v1, p0, Lcom/google/android/location/places/d/a/i;->c:Lcom/google/android/location/places/d/a/s;

    iget-object v1, v1, Lcom/google/android/location/places/d/a/s;->a:Lcom/google/android/location/places/d/a/c;

    iget-object v1, v1, Lcom/google/android/location/places/d/a/c;->a:Lcom/google/android/location/places/d/a/r;

    invoke-static {v1}, Lcom/google/android/location/places/c/l;->a(Lcom/google/android/location/places/d/a/r;)Lcom/google/android/gms/maps/model/LatLng;

    move-result-object v1

    iget-object v4, p0, Lcom/google/android/location/places/d/a/i;->c:Lcom/google/android/location/places/d/a/s;

    iget-object v4, v4, Lcom/google/android/location/places/d/a/s;->a:Lcom/google/android/location/places/d/a/c;

    iget-object v4, v4, Lcom/google/android/location/places/d/a/c;->b:Ljava/lang/Float;

    invoke-virtual {v4}, Ljava/lang/Float;->floatValue()F

    move-result v4

    invoke-direct {v0, v1, v4}, Lcom/google/android/location/places/b/j;-><init>(Lcom/google/android/gms/maps/model/LatLng;F)V

    .line 120
    new-instance v1, Lcom/google/android/location/places/c/i;

    invoke-direct {v1, v3, v0, v2}, Lcom/google/android/location/places/c/i;-><init>(Ljava/util/List;Lcom/google/android/location/places/b/j;Ljava/util/Map;)V

    return-object v1
.end method

.method static a(Lcom/google/android/gms/maps/model/LatLng;)Lcom/google/android/location/places/d/a/r;
    .locals 4

    .prologue
    .line 247
    new-instance v0, Lcom/google/android/location/places/d/a/r;

    invoke-direct {v0}, Lcom/google/android/location/places/d/a/r;-><init>()V

    .line 248
    iget-wide v2, p0, Lcom/google/android/gms/maps/model/LatLng;->a:D

    invoke-static {v2, v3}, Lcom/google/android/location/places/c/l;->a(D)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, v0, Lcom/google/android/location/places/d/a/r;->a:Ljava/lang/Integer;

    .line 249
    iget-wide v2, p0, Lcom/google/android/gms/maps/model/LatLng;->b:D

    invoke-static {v2, v3}, Lcom/google/android/location/places/c/l;->a(D)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, v0, Lcom/google/android/location/places/d/a/r;->b:Ljava/lang/Integer;

    .line 250
    return-object v0
.end method

.method static a(Lcom/google/android/location/places/d/a/q;)Lcom/google/android/location/places/d/a;
    .locals 12

    .prologue
    .line 306
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 307
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 308
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 310
    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    .line 311
    iget-object v6, p0, Lcom/google/android/location/places/d/a/q;->c:[Lcom/google/android/location/places/d/a/w;

    array-length v7, v6

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v7, :cond_2

    aget-object v0, v6, v1

    .line 312
    iget-object v8, v0, Lcom/google/android/location/places/d/a/w;->c:Ljava/lang/Integer;

    if-eqz v8, :cond_0

    .line 313
    sget-object v8, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    iget-object v9, v0, Lcom/google/android/location/places/d/a/w;->c:Ljava/lang/Integer;

    invoke-virtual {v9}, Ljava/lang/Integer;->intValue()I

    move-result v9

    int-to-long v10, v9

    invoke-virtual {v8, v10, v11}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v8

    long-to-int v8, v8

    .line 315
    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v2, v8}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 317
    :cond_0
    iget-object v8, v0, Lcom/google/android/location/places/d/a/w;->a:Ljava/lang/Integer;

    invoke-virtual {v8}, Ljava/lang/Integer;->intValue()I

    move-result v8

    sparse-switch v8, :sswitch_data_0

    .line 311
    :cond_1
    :goto_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 319
    :sswitch_0
    iget-object v0, v0, Lcom/google/android/location/places/d/a/w;->d:Lcom/google/android/location/places/d/a/u;

    iget-object v0, v0, Lcom/google/android/location/places/d/a/u;->a:Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/gms/location/places/personalized/internal/TestDataImpl;->a(Ljava/lang/String;)Lcom/google/android/gms/location/places/personalized/internal/TestDataImpl;

    move-result-object v0

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 322
    :sswitch_1
    iget-object v0, v0, Lcom/google/android/location/places/d/a/w;->f:Lcom/google/android/location/places/d/a/n;

    iget-object v8, v0, Lcom/google/android/location/places/d/a/n;->a:Ljava/lang/Integer;

    invoke-virtual {v8}, Ljava/lang/Integer;->intValue()I

    move-result v8

    packed-switch v8, :pswitch_data_0

    const/4 v0, 0x0

    .line 323
    :goto_2
    if-eqz v0, :cond_1

    .line 324
    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 322
    :pswitch_0
    invoke-static {}, Lcom/google/android/gms/location/places/personalized/PlaceAlias;->a()Lcom/google/android/gms/location/places/personalized/PlaceAlias;

    move-result-object v0

    goto :goto_2

    :pswitch_1
    invoke-static {}, Lcom/google/android/gms/location/places/personalized/PlaceAlias;->b()Lcom/google/android/gms/location/places/personalized/PlaceAlias;

    move-result-object v0

    goto :goto_2

    :pswitch_2
    iget-object v0, v0, Lcom/google/android/location/places/d/a/n;->b:Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/gms/location/places/personalized/PlaceAlias;->a(Ljava/lang/String;)Lcom/google/android/gms/location/places/personalized/PlaceAlias;

    move-result-object v0

    goto :goto_2

    .line 328
    :sswitch_2
    iget-object v0, v0, Lcom/google/android/location/places/d/a/w;->g:Lcom/google/android/location/places/d/a/m;

    invoke-static {v0}, Lcom/google/android/location/places/c/l;->a(Lcom/google/android/location/places/d/a/m;)Lcom/google/android/gms/location/places/personalized/HereContent;

    move-result-object v0

    .line 329
    if-eqz v0, :cond_1

    .line 330
    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 337
    :cond_2
    new-instance v0, Lcom/google/android/location/places/d/a;

    iget-object v1, p0, Lcom/google/android/location/places/d/a/q;->a:Ljava/lang/String;

    iget-object v6, p0, Lcom/google/android/location/places/d/a/q;->b:Lcom/google/android/location/places/d/a/o;

    invoke-static {v6, v2}, Lcom/google/android/location/places/c/l;->a(Lcom/google/android/location/places/d/a/o;Ljava/util/Collection;)Ljava/util/List;

    move-result-object v2

    invoke-direct/range {v0 .. v5}, Lcom/google/android/location/places/d/a;-><init>(Ljava/lang/String;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;)V

    return-object v0

    .line 317
    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x6 -> :sswitch_1
        0x7 -> :sswitch_2
    .end sparse-switch

    .line 322
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private static a(Lcom/google/android/location/places/d/a/o;Ljava/util/Collection;)Ljava/util/List;
    .locals 6

    .prologue
    const/4 v3, 0x0

    .line 289
    if-nez p0, :cond_0

    .line 290
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    .line 302
    :goto_0
    return-object v0

    .line 292
    :cond_0
    new-instance v1, Ljava/util/ArrayList;

    invoke-interface {p1}, Ljava/util/Collection;->size()I

    move-result v0

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 293
    iget-object v0, p0, Lcom/google/android/location/places/d/a/o;->a:Lcom/google/android/location/places/d/a/f;

    iget-object v0, v0, Lcom/google/android/location/places/d/a/f;->a:[Lcom/google/android/location/places/d/a/c;

    aget-object v0, v0, v3

    iget-object v0, v0, Lcom/google/android/location/places/d/a/c;->a:Lcom/google/android/location/places/d/a/r;

    invoke-static {v0}, Lcom/google/android/location/places/c/l;->a(Lcom/google/android/location/places/d/a/r;)Lcom/google/android/gms/maps/model/LatLng;

    move-result-object v2

    .line 294
    iget-object v0, p0, Lcom/google/android/location/places/d/a/o;->a:Lcom/google/android/location/places/d/a/f;

    iget-object v0, v0, Lcom/google/android/location/places/d/a/f;->a:[Lcom/google/android/location/places/d/a/c;

    aget-object v0, v0, v3

    iget-object v0, v0, Lcom/google/android/location/places/d/a/c;->b:Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v3

    .line 295
    invoke-interface {p1}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 296
    new-instance v0, Lcom/google/android/location/places/b/j;

    invoke-direct {v0, v2, v3}, Lcom/google/android/location/places/b/j;-><init>(Lcom/google/android/gms/maps/model/LatLng;F)V

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_1
    move-object v0, v1

    .line 302
    goto :goto_0

    .line 298
    :cond_2
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 299
    new-instance v5, Lcom/google/android/location/places/b/j;

    invoke-direct {v5, v2, v3, v0}, Lcom/google/android/location/places/b/j;-><init>(Lcom/google/android/gms/maps/model/LatLng;FI)V

    invoke-interface {v1, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1
.end method

.method static a([Lcom/google/android/location/l/a/bt;)Ljava/util/List;
    .locals 5

    .prologue
    .line 216
    new-instance v1, Ljava/util/ArrayList;

    array-length v0, p0

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 218
    array-length v2, p0

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, p0, v0

    .line 219
    iget-object v4, v3, Lcom/google/android/location/l/a/bt;->a:Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    iget-object v3, v3, Lcom/google/android/location/l/a/bt;->b:Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v4, v3}, Lcom/google/android/gms/location/places/AutocompletePrediction$Substring;->a(II)Lcom/google/android/gms/location/places/AutocompletePrediction$Substring;

    move-result-object v3

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 218
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 224
    :cond_0
    return-object v1
.end method


# virtual methods
.method final a()Lcom/google/android/location/places/d/a/d;
    .locals 2

    .prologue
    .line 232
    new-instance v0, Lcom/google/android/location/places/d/a/d;

    invoke-direct {v0}, Lcom/google/android/location/places/d/a/d;-><init>()V

    .line 233
    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, v0, Lcom/google/android/location/places/d/a/d;->a:Ljava/lang/Integer;

    .line 234
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, v0, Lcom/google/android/location/places/d/a/d;->b:Ljava/lang/Integer;

    .line 235
    iget-object v1, p0, Lcom/google/android/location/places/c/l;->a:Landroid/content/Context;

    invoke-static {v1}, Lcom/google/android/gms/common/util/e;->f(Landroid/content/Context;)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, v0, Lcom/google/android/location/places/d/a/d;->c:Ljava/lang/Integer;

    .line 236
    return-object v0
.end method
