.class public final Lcom/google/android/location/g/f;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public final a:Lcom/google/android/location/g/ae;

.field public b:Z

.field public c:J

.field public d:J

.field public final e:Lcom/google/android/location/e/d;

.field private final f:Lcom/google/android/location/g/a;

.field private final g:Lcom/google/android/location/os/bi;


# direct methods
.method public constructor <init>(Lcom/google/android/location/b/ba;Lcom/google/android/location/b/ba;Lcom/google/android/location/b/ai;Lcom/google/android/location/g/w;Lcom/google/android/location/os/bi;)V
    .locals 2

    .prologue
    .line 50
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 46
    new-instance v0, Lcom/google/android/location/e/d;

    invoke-direct {v0}, Lcom/google/android/location/e/d;-><init>()V

    iput-object v0, p0, Lcom/google/android/location/g/f;->e:Lcom/google/android/location/e/d;

    .line 51
    invoke-static {p4, p2, p3, p5}, Lcom/google/android/location/g/ae;->a(Lcom/google/android/location/g/w;Lcom/google/android/location/b/ba;Lcom/google/android/location/b/ai;Lcom/google/android/location/os/bi;)Lcom/google/android/location/g/ae;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/g/f;->a:Lcom/google/android/location/g/ae;

    .line 52
    new-instance v0, Lcom/google/android/location/g/a;

    invoke-interface {p5}, Lcom/google/android/location/os/bi;->c()Lcom/google/android/location/j/b;

    move-result-object v1

    invoke-direct {v0, p1, v1}, Lcom/google/android/location/g/a;-><init>(Lcom/google/android/location/b/ba;Lcom/google/android/location/j/b;)V

    iput-object v0, p0, Lcom/google/android/location/g/f;->f:Lcom/google/android/location/g/a;

    .line 53
    iput-object p5, p0, Lcom/google/android/location/g/f;->g:Lcom/google/android/location/os/bi;

    .line 54
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/location/g/f;->b:Z

    .line 55
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/location/e/ap;)Lcom/google/android/location/e/ag;
    .locals 18

    .prologue
    .line 146
    move-object/from16 v0, p1

    iget-wide v6, v0, Lcom/google/android/location/e/ap;->a:J

    .line 147
    move-object/from16 v0, p1

    iget-object v4, v0, Lcom/google/android/location/e/ap;->b:Lcom/google/android/location/e/i;

    .line 148
    move-object/from16 v0, p1

    iget-object v11, v0, Lcom/google/android/location/e/ap;->c:Lcom/google/android/location/e/bi;

    .line 149
    move-object/from16 v0, p1

    iget-object v10, v0, Lcom/google/android/location/e/ap;->d:Lcom/google/android/location/e/al;

    .line 150
    move-object/from16 v0, p1

    iget-boolean v0, v0, Lcom/google/android/location/e/ap;->e:Z

    move/from16 v17, v0

    .line 152
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/location/g/f;->f:Lcom/google/android/location/g/a;

    iget-object v2, v5, Lcom/google/android/location/g/a;->a:Lcom/google/android/location/j/b;

    invoke-interface {v2}, Lcom/google/android/location/j/b;->b()J

    move-result-wide v12

    const/4 v8, 0x0

    const/4 v2, 0x0

    if-eqz v4, :cond_49

    iget-object v8, v4, Lcom/google/android/location/e/i;->a:Lcom/google/android/location/e/h;

    iget-object v2, v4, Lcom/google/android/location/e/i;->b:Ljava/util/List;

    move-object v3, v2

    :goto_0
    if-eqz v8, :cond_0

    invoke-virtual {v8}, Lcom/google/android/location/e/h;->i()Z

    move-result v2

    if-nez v2, :cond_16

    :cond_0
    new-instance v3, Lcom/google/android/location/e/f;

    const/4 v4, 0x0

    sget-object v5, Lcom/google/android/location/e/ab;->b:Lcom/google/android/location/e/ab;

    const-wide/16 v6, 0x0

    invoke-direct/range {v3 .. v8}, Lcom/google/android/location/e/f;-><init>(Lcom/google/android/location/e/al;Lcom/google/android/location/e/ab;JLcom/google/android/location/e/h;)V

    move-object v9, v3

    .line 153
    :goto_1
    iget-object v2, v9, Lcom/google/android/location/e/f;->d:Lcom/google/android/location/e/ab;

    sget-object v3, Lcom/google/android/location/e/ab;->a:Lcom/google/android/location/e/ab;

    if-ne v2, v3, :cond_22

    const/4 v2, 0x1

    move/from16 v16, v2

    .line 168
    :goto_2
    if-nez v10, :cond_48

    .line 169
    iget-object v2, v9, Lcom/google/android/location/e/f;->d:Lcom/google/android/location/e/ab;

    sget-object v3, Lcom/google/android/location/e/ab;->a:Lcom/google/android/location/e/ab;

    if-ne v2, v3, :cond_48

    .line 170
    iget-object v3, v9, Lcom/google/android/location/e/f;->c:Lcom/google/android/location/e/al;

    .line 171
    new-instance v2, Lcom/google/android/location/e/al;

    iget v4, v3, Lcom/google/android/location/e/al;->d:I

    iget v3, v3, Lcom/google/android/location/e/al;->e:I

    const v5, 0x2faf080

    invoke-direct {v2, v4, v3, v5}, Lcom/google/android/location/e/al;-><init>(III)V

    .line 173
    sget-boolean v3, Lcom/google/android/location/i/a;->b:Z

    if-eqz v3, :cond_1

    const-string v3, "LocatorManager"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Generating cell-based aperture: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/android/location/o/a/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 178
    :cond_1
    :goto_3
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/location/g/f;->a:Lcom/google/android/location/g/ae;

    invoke-virtual {v3, v11, v2}, Lcom/google/android/location/g/ae;->a(Lcom/google/android/location/e/bi;Lcom/google/android/location/e/al;)Lcom/google/android/location/e/bf;

    move-result-object v12

    .line 180
    if-eqz v12, :cond_23

    iget-object v2, v12, Lcom/google/android/location/e/bf;->d:Lcom/google/android/location/e/ab;

    sget-object v3, Lcom/google/android/location/e/ab;->a:Lcom/google/android/location/e/ab;

    if-ne v2, v3, :cond_23

    const/4 v2, 0x1

    move v5, v2

    .line 181
    :goto_4
    if-eqz v5, :cond_24

    iget-object v2, v12, Lcom/google/android/location/e/bf;->c:Lcom/google/android/location/e/al;

    if-eqz v2, :cond_24

    iget-object v2, v12, Lcom/google/android/location/e/bf;->c:Lcom/google/android/location/e/al;

    instance-of v2, v2, Lcom/google/android/location/e/n;

    if-eqz v2, :cond_24

    iget-object v2, v12, Lcom/google/android/location/e/bf;->c:Lcom/google/android/location/e/al;

    check-cast v2, Lcom/google/android/location/e/n;

    iget-object v2, v2, Lcom/google/android/location/e/n;->b:Ljava/lang/String;

    if-eqz v2, :cond_24

    const/4 v2, 0x1

    move v10, v2

    .line 186
    :goto_5
    iget-object v2, v12, Lcom/google/android/location/e/z;->d:Lcom/google/android/location/e/ab;

    sget-object v3, Lcom/google/android/location/e/ab;->a:Lcom/google/android/location/e/ab;

    if-ne v2, v3, :cond_25

    const/4 v2, 0x1

    :goto_6
    iget-object v3, v9, Lcom/google/android/location/e/z;->d:Lcom/google/android/location/e/ab;

    sget-object v4, Lcom/google/android/location/e/ab;->a:Lcom/google/android/location/e/ab;

    if-ne v3, v4, :cond_26

    const/4 v3, 0x1

    :goto_7
    if-nez v2, :cond_27

    if-nez v3, :cond_27

    const/4 v2, 0x0

    move-object v11, v2

    .line 192
    :goto_8
    sget-object v2, Lcom/google/android/location/d/a;->d:Lcom/google/android/gms/common/a/d;

    invoke-static {v2}, Lcom/google/android/location/d/a;->a(Lcom/google/android/gms/common/a/d;)Z

    move-result v2

    if-eqz v2, :cond_15

    .line 193
    const/4 v4, 0x0

    .line 194
    const/4 v3, 0x0

    .line 195
    const/4 v2, 0x0

    .line 197
    if-eqz v11, :cond_47

    .line 198
    if-ne v11, v9, :cond_2e

    .line 199
    const/4 v4, 0x1

    move v13, v2

    move v14, v3

    move v15, v4

    .line 208
    :goto_9
    if-eqz v5, :cond_2

    .line 212
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/location/g/f;->g:Lcom/google/android/location/os/bi;

    const-string v3, "nlp"

    const-string v4, "locType"

    const-string v5, "has_wifi"

    const-wide/16 v6, 0x1

    const/4 v8, 0x1

    invoke-interface/range {v2 .. v8}, Lcom/google/android/location/os/bi;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JZ)V

    .line 215
    :cond_2
    if-eqz v16, :cond_3

    .line 216
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/location/g/f;->g:Lcom/google/android/location/os/bi;

    const-string v3, "nlp"

    const-string v4, "locType"

    const-string v5, "has_cell"

    const-wide/16 v6, 0x1

    const/4 v8, 0x1

    invoke-interface/range {v2 .. v8}, Lcom/google/android/location/os/bi;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JZ)V

    .line 219
    :cond_3
    if-eqz v10, :cond_4

    .line 220
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/location/g/f;->g:Lcom/google/android/location/os/bi;

    const-string v3, "nlp"

    const-string v4, "locType"

    const-string v5, "has_indoor"

    const-wide/16 v6, 0x1

    const/4 v8, 0x1

    invoke-interface/range {v2 .. v8}, Lcom/google/android/location/os/bi;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JZ)V

    .line 223
    :cond_4
    if-eqz v15, :cond_30

    .line 224
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/location/g/f;->g:Lcom/google/android/location/os/bi;

    const-string v3, "nlp"

    const-string v4, "locType"

    const-string v5, "uses_cell"

    const-wide/16 v6, 0x1

    const/4 v8, 0x1

    invoke-interface/range {v2 .. v8}, Lcom/google/android/location/os/bi;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JZ)V

    .line 235
    :goto_a
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/location/g/f;->a:Lcom/google/android/location/g/ae;

    iget-object v3, v2, Lcom/google/android/location/g/ae;->d:Lcom/google/t/b/b/a/d;

    if-nez v3, :cond_33

    const/4 v2, -0x1

    move v6, v2

    .line 239
    :goto_b
    if-lez v6, :cond_5

    .line 240
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/location/g/f;->g:Lcom/google/android/location/os/bi;

    const-string v3, "nlp"

    const-string v4, "gpwle"

    const-string v5, "sm_m_cs"

    int-to-long v6, v6

    const/4 v8, 0x0

    invoke-interface/range {v2 .. v8}, Lcom/google/android/location/os/bi;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JZ)V

    .line 245
    :cond_5
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/location/g/f;->a:Lcom/google/android/location/g/ae;

    iget-object v3, v2, Lcom/google/android/location/g/ae;->d:Lcom/google/t/b/b/a/d;

    if-nez v3, :cond_34

    const/4 v2, -0x1

    move v6, v2

    .line 247
    :goto_c
    if-lez v6, :cond_6

    .line 248
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/location/g/f;->g:Lcom/google/android/location/os/bi;

    const-string v3, "nlp"

    const-string v4, "gpwle"

    const-string v5, "sm_dc_lcs"

    int-to-long v6, v6

    const/4 v8, 0x0

    invoke-interface/range {v2 .. v8}, Lcom/google/android/location/os/bi;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JZ)V

    .line 253
    :cond_6
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/location/g/f;->a:Lcom/google/android/location/g/ae;

    iget-object v3, v2, Lcom/google/android/location/g/ae;->d:Lcom/google/t/b/b/a/d;

    if-nez v3, :cond_35

    const/4 v2, -0x1

    move v6, v2

    .line 255
    :goto_d
    if-lez v6, :cond_7

    .line 256
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/location/g/f;->g:Lcom/google/android/location/os/bi;

    const-string v3, "nlp"

    const-string v4, "gpwle"

    const-string v5, "sm_dc_ts"

    int-to-long v6, v6

    const/4 v8, 0x1

    invoke-interface/range {v2 .. v8}, Lcom/google/android/location/os/bi;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JZ)V

    .line 261
    :cond_7
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/location/g/f;->a:Lcom/google/android/location/g/ae;

    iget-object v3, v2, Lcom/google/android/location/g/ae;->d:Lcom/google/t/b/b/a/d;

    if-nez v3, :cond_36

    const/4 v2, -0x1

    move v6, v2

    .line 263
    :goto_e
    if-lez v6, :cond_8

    .line 264
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/location/g/f;->g:Lcom/google/android/location/os/bi;

    const-string v3, "nlp"

    const-string v4, "gpwle"

    const-string v5, "sm_dc_tm"

    int-to-long v6, v6

    const/4 v8, 0x1

    invoke-interface/range {v2 .. v8}, Lcom/google/android/location/os/bi;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JZ)V

    .line 269
    :cond_8
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/location/g/f;->a:Lcom/google/android/location/g/ae;

    iget-object v3, v2, Lcom/google/android/location/g/ae;->d:Lcom/google/t/b/b/a/d;

    if-nez v3, :cond_37

    const/4 v2, -0x1

    move v6, v2

    .line 272
    :goto_f
    if-lez v6, :cond_9

    .line 273
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/location/g/f;->g:Lcom/google/android/location/os/bi;

    const-string v3, "nlp"

    const-string v4, "gpwle"

    const-string v5, "sm_tiaed"

    int-to-long v6, v6

    const/4 v8, 0x1

    invoke-interface/range {v2 .. v8}, Lcom/google/android/location/os/bi;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JZ)V

    .line 279
    :cond_9
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/location/g/f;->a:Lcom/google/android/location/g/ae;

    iget-object v3, v2, Lcom/google/android/location/g/ae;->d:Lcom/google/t/b/b/a/d;

    if-nez v3, :cond_38

    const/4 v2, -0x1

    move v6, v2

    .line 282
    :goto_10
    if-lez v6, :cond_a

    .line 283
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/location/g/f;->g:Lcom/google/android/location/os/bi;

    const-string v3, "nlp"

    const-string v4, "gpwle"

    const-string v5, "sm_tiobed"

    int-to-long v6, v6

    const/4 v8, 0x1

    invoke-interface/range {v2 .. v8}, Lcom/google/android/location/os/bi;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JZ)V

    .line 289
    :cond_a
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/location/g/f;->a:Lcom/google/android/location/g/ae;

    iget-object v3, v2, Lcom/google/android/location/g/ae;->d:Lcom/google/t/b/b/a/d;

    if-nez v3, :cond_39

    const/4 v2, -0x1

    move v6, v2

    .line 291
    :goto_11
    if-ltz v6, :cond_b

    .line 292
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/location/g/f;->g:Lcom/google/android/location/os/bi;

    const-string v3, "nlp"

    const-string v4, "gpwle"

    const-string v5, "sm_dc_av"

    int-to-long v6, v6

    const/4 v8, 0x0

    invoke-interface/range {v2 .. v8}, Lcom/google/android/location/os/bi;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JZ)V

    .line 297
    :cond_b
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/location/g/f;->a:Lcom/google/android/location/g/ae;

    iget-object v3, v2, Lcom/google/android/location/g/ae;->d:Lcom/google/t/b/b/a/d;

    if-nez v3, :cond_3b

    const/4 v2, -0x1

    move v6, v2

    .line 299
    :goto_12
    if-lez v6, :cond_c

    .line 300
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/location/g/f;->g:Lcom/google/android/location/os/bi;

    const-string v3, "nlp"

    const-string v4, "gpwle"

    const-string v5, "pq_m_cs"

    int-to-long v6, v6

    const/4 v8, 0x0

    invoke-interface/range {v2 .. v8}, Lcom/google/android/location/os/bi;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JZ)V

    .line 305
    :cond_c
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/location/g/f;->a:Lcom/google/android/location/g/ae;

    iget-object v3, v2, Lcom/google/android/location/g/ae;->d:Lcom/google/t/b/b/a/d;

    if-nez v3, :cond_3c

    const/4 v2, -0x1

    move v6, v2

    .line 307
    :goto_13
    if-lez v6, :cond_d

    .line 308
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/location/g/f;->g:Lcom/google/android/location/os/bi;

    const-string v3, "nlp"

    const-string v4, "gpwle"

    const-string v5, "pq_dc_lcs"

    int-to-long v6, v6

    const/4 v8, 0x0

    invoke-interface/range {v2 .. v8}, Lcom/google/android/location/os/bi;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JZ)V

    .line 313
    :cond_d
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/location/g/f;->a:Lcom/google/android/location/g/ae;

    iget-object v3, v2, Lcom/google/android/location/g/ae;->d:Lcom/google/t/b/b/a/d;

    if-nez v3, :cond_3d

    const/4 v2, -0x1

    move v6, v2

    .line 315
    :goto_14
    if-lez v6, :cond_e

    .line 316
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/location/g/f;->g:Lcom/google/android/location/os/bi;

    const-string v3, "nlp"

    const-string v4, "gpwle"

    const-string v5, "pq_dc_ts"

    int-to-long v6, v6

    const/4 v8, 0x1

    invoke-interface/range {v2 .. v8}, Lcom/google/android/location/os/bi;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JZ)V

    .line 321
    :cond_e
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/location/g/f;->a:Lcom/google/android/location/g/ae;

    iget-object v3, v2, Lcom/google/android/location/g/ae;->d:Lcom/google/t/b/b/a/d;

    if-nez v3, :cond_3e

    const/4 v2, -0x1

    move v6, v2

    .line 323
    :goto_15
    if-lez v6, :cond_f

    .line 324
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/location/g/f;->g:Lcom/google/android/location/os/bi;

    const-string v3, "nlp"

    const-string v4, "gpwle"

    const-string v5, "pq_dc_tm"

    int-to-long v6, v6

    const/4 v8, 0x1

    invoke-interface/range {v2 .. v8}, Lcom/google/android/location/os/bi;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JZ)V

    .line 329
    :cond_f
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/location/g/f;->a:Lcom/google/android/location/g/ae;

    iget-object v3, v2, Lcom/google/android/location/g/ae;->d:Lcom/google/t/b/b/a/d;

    if-nez v3, :cond_3f

    const/4 v2, -0x1

    move v6, v2

    .line 332
    :goto_16
    if-lez v6, :cond_10

    .line 333
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/location/g/f;->g:Lcom/google/android/location/os/bi;

    const-string v3, "nlp"

    const-string v4, "gpwle"

    const-string v5, "pq_tiaed"

    int-to-long v6, v6

    const/4 v8, 0x1

    invoke-interface/range {v2 .. v8}, Lcom/google/android/location/os/bi;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JZ)V

    .line 339
    :cond_10
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/location/g/f;->a:Lcom/google/android/location/g/ae;

    iget-object v3, v2, Lcom/google/android/location/g/ae;->d:Lcom/google/t/b/b/a/d;

    if-nez v3, :cond_40

    const/4 v2, -0x1

    move v6, v2

    .line 342
    :goto_17
    if-lez v6, :cond_11

    .line 343
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/location/g/f;->g:Lcom/google/android/location/os/bi;

    const-string v3, "nlp"

    const-string v4, "gpwle"

    const-string v5, "pq_tiobed"

    int-to-long v6, v6

    const/4 v8, 0x1

    invoke-interface/range {v2 .. v8}, Lcom/google/android/location/os/bi;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JZ)V

    .line 349
    :cond_11
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/location/g/f;->a:Lcom/google/android/location/g/ae;

    iget-object v3, v2, Lcom/google/android/location/g/ae;->d:Lcom/google/t/b/b/a/d;

    if-nez v3, :cond_41

    const/4 v2, -0x1

    move v6, v2

    .line 351
    :goto_18
    if-ltz v6, :cond_12

    .line 352
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/location/g/f;->g:Lcom/google/android/location/os/bi;

    const-string v3, "nlp"

    const-string v4, "gpwle"

    const-string v5, "pq_dc_av"

    int-to-long v6, v6

    const/4 v8, 0x0

    invoke-interface/range {v2 .. v8}, Lcom/google/android/location/os/bi;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JZ)V

    .line 357
    :cond_12
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/location/g/f;->a:Lcom/google/android/location/g/ae;

    iget-object v3, v2, Lcom/google/android/location/g/ae;->d:Lcom/google/t/b/b/a/d;

    if-nez v3, :cond_43

    const/4 v2, -0x1

    move v6, v2

    .line 358
    :goto_19
    if-ltz v6, :cond_13

    .line 359
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/location/g/f;->g:Lcom/google/android/location/os/bi;

    const-string v3, "nlp"

    const-string v4, "gpwle"

    const-string v5, "lrl_ms"

    int-to-long v6, v6

    const/4 v8, 0x0

    invoke-interface/range {v2 .. v8}, Lcom/google/android/location/os/bi;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JZ)V

    .line 363
    :cond_13
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/location/g/f;->a:Lcom/google/android/location/g/ae;

    iget-object v3, v2, Lcom/google/android/location/g/ae;->d:Lcom/google/t/b/b/a/d;

    if-nez v3, :cond_44

    const/4 v2, -0x1

    move v6, v2

    .line 365
    :goto_1a
    if-ltz v6, :cond_14

    .line 366
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/location/g/f;->g:Lcom/google/android/location/os/bi;

    const-string v3, "nlp"

    const-string v4, "gpwle"

    const-string v5, "lsrl_ms"

    int-to-long v6, v6

    const/4 v8, 0x0

    invoke-interface/range {v2 .. v8}, Lcom/google/android/location/os/bi;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JZ)V

    .line 371
    :cond_14
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/location/g/f;->a:Lcom/google/android/location/g/ae;

    iget-object v3, v2, Lcom/google/android/location/g/ae;->d:Lcom/google/t/b/b/a/d;

    if-nez v3, :cond_45

    const/4 v2, -0x1

    move v6, v2

    .line 373
    :goto_1b
    if-ltz v6, :cond_15

    .line 374
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/location/g/f;->g:Lcom/google/android/location/os/bi;

    const-string v3, "nlp"

    const-string v4, "gpwle"

    const-string v5, "lurl_ms"

    int-to-long v6, v6

    const/4 v8, 0x0

    invoke-interface/range {v2 .. v8}, Lcom/google/android/location/os/bi;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JZ)V

    .line 380
    :cond_15
    if-eqz v17, :cond_46

    if-ne v11, v9, :cond_46

    const/4 v2, 0x1

    .line 381
    :goto_1c
    new-instance v3, Lcom/google/android/location/e/ag;

    invoke-direct {v3, v11, v12, v9, v2}, Lcom/google/android/location/e/ag;-><init>(Lcom/google/android/location/e/z;Lcom/google/android/location/e/bf;Lcom/google/android/location/e/f;Z)V

    return-object v3

    .line 152
    :cond_16
    const/4 v2, 0x0

    invoke-virtual {v5, v8, v2, v12, v13}, Lcom/google/android/location/g/a;->a(Lcom/google/android/location/e/h;Ljava/util/Map;J)Lcom/google/android/location/b/a;

    move-result-object v2

    if-nez v2, :cond_18

    sget-boolean v2, Lcom/google/android/location/i/a;->b:Z

    if-eqz v2, :cond_17

    const-string v2, "CellLocator"

    const-string v3, "Primary cell miss in cache. Need server request."

    invoke-static {v2, v3}, Lcom/google/android/location/o/a/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_17
    new-instance v3, Lcom/google/android/location/e/f;

    const/4 v4, 0x0

    sget-object v5, Lcom/google/android/location/e/ab;->c:Lcom/google/android/location/e/ab;

    const-wide/16 v6, 0x0

    invoke-direct/range {v3 .. v8}, Lcom/google/android/location/e/f;-><init>(Lcom/google/android/location/e/al;Lcom/google/android/location/e/ab;JLcom/google/android/location/e/h;)V

    move-object v9, v3

    goto/16 :goto_1

    :cond_18
    iget-object v2, v2, Lcom/google/android/location/b/a;->d:Ljava/lang/Object;

    check-cast v2, Lcom/google/android/location/e/al;

    invoke-virtual {v2}, Lcom/google/android/location/e/al;->b()Z

    move-result v2

    if-nez v2, :cond_1a

    sget-boolean v2, Lcom/google/android/location/i/a;->b:Z

    if-eqz v2, :cond_19

    const-string v2, "CellLocator"

    const-string v3, "Primary cell is in cache with no location."

    invoke-static {v2, v3}, Lcom/google/android/location/o/a/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_19
    new-instance v3, Lcom/google/android/location/e/f;

    const/4 v4, 0x0

    sget-object v5, Lcom/google/android/location/e/ab;->b:Lcom/google/android/location/e/ab;

    const-wide/16 v6, 0x0

    invoke-direct/range {v3 .. v8}, Lcom/google/android/location/e/f;-><init>(Lcom/google/android/location/e/al;Lcom/google/android/location/e/ab;JLcom/google/android/location/e/h;)V

    move-object v9, v3

    goto/16 :goto_1

    :cond_1a
    if-nez v3, :cond_1d

    const/4 v2, 0x0

    :goto_1d
    new-instance v3, Lcom/google/android/location/g/e;

    add-int/lit8 v2, v2, 0x1

    invoke-direct {v3, v2}, Lcom/google/android/location/g/e;-><init>(I)V

    new-instance v9, Lcom/google/android/location/g/b;

    invoke-direct {v9, v5, v12, v13, v3}, Lcom/google/android/location/g/b;-><init>(Lcom/google/android/location/g/a;JLcom/google/android/location/g/e;)V

    const-wide/16 v12, 0x7530

    sub-long v12, v6, v12

    iget-object v2, v4, Lcom/google/android/location/e/i;->a:Lcom/google/android/location/e/h;

    if-eqz v2, :cond_1b

    iget-object v2, v4, Lcom/google/android/location/e/i;->a:Lcom/google/android/location/e/h;

    invoke-virtual {v9, v2}, Lcom/google/android/location/e/j;->a(Lcom/google/android/location/e/h;)V

    :cond_1b
    iget-object v2, v4, Lcom/google/android/location/e/i;->b:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_1c
    :goto_1e
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1e

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/location/e/h;

    invoke-virtual {v2}, Lcom/google/android/location/e/h;->f()J

    move-result-wide v14

    cmp-long v5, v14, v12

    if-lez v5, :cond_1c

    invoke-virtual {v9, v2}, Lcom/google/android/location/e/j;->a(Lcom/google/android/location/e/h;)V

    goto :goto_1e

    :cond_1d
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v2

    goto :goto_1d

    :cond_1e
    new-instance v4, Lcom/google/android/location/e/al;

    invoke-virtual {v3}, Lcom/google/android/location/g/e;->a()D

    move-result-wide v12

    const-wide v14, 0x416312d000000000L    # 1.0E7

    mul-double/2addr v12, v14

    double-to-int v2, v12

    invoke-virtual {v3}, Lcom/google/android/location/g/e;->b()D

    move-result-wide v12

    const-wide v14, 0x416312d000000000L    # 1.0E7

    mul-double/2addr v12, v14

    double-to-int v5, v12

    invoke-virtual {v3}, Lcom/google/android/location/g/e;->c()I

    move-result v9

    invoke-static {v9}, Lcom/google/android/location/g/d;->a(I)I

    move-result v9

    iget v3, v3, Lcom/google/android/location/g/e;->d:I

    invoke-direct {v4, v2, v5, v9, v3}, Lcom/google/android/location/e/al;-><init>(IIII)V

    invoke-static {v4}, Lcom/google/android/location/g/d;->c(Lcom/google/android/location/e/al;)Z

    move-result v2

    if-eqz v2, :cond_20

    sget-boolean v2, Lcom/google/android/location/i/a;->b:Z

    if-eqz v2, :cond_1f

    const-string v2, "CellLocator"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v5, "Found cell location: "

    invoke-direct {v3, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/location/o/a/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1f
    new-instance v3, Lcom/google/android/location/e/f;

    sget-object v5, Lcom/google/android/location/e/ab;->a:Lcom/google/android/location/e/ab;

    invoke-direct/range {v3 .. v8}, Lcom/google/android/location/e/f;-><init>(Lcom/google/android/location/e/al;Lcom/google/android/location/e/ab;JLcom/google/android/location/e/h;)V

    move-object v9, v3

    goto/16 :goto_1

    :cond_20
    sget-boolean v2, Lcom/google/android/location/i/a;->d:Z

    if-eqz v2, :cond_21

    const-string v2, "CellLocator"

    const-string v3, "Cell location had non-sane values"

    invoke-static {v2, v3}, Lcom/google/android/location/o/a/a;->c(Ljava/lang/String;Ljava/lang/String;)V

    :cond_21
    new-instance v3, Lcom/google/android/location/e/f;

    const/4 v4, 0x0

    sget-object v5, Lcom/google/android/location/e/ab;->b:Lcom/google/android/location/e/ab;

    const-wide/16 v6, 0x0

    invoke-direct/range {v3 .. v8}, Lcom/google/android/location/e/f;-><init>(Lcom/google/android/location/e/al;Lcom/google/android/location/e/ab;JLcom/google/android/location/e/h;)V

    move-object v9, v3

    goto/16 :goto_1

    .line 153
    :cond_22
    const/4 v2, 0x0

    move/from16 v16, v2

    goto/16 :goto_2

    .line 180
    :cond_23
    const/4 v2, 0x0

    move v5, v2

    goto/16 :goto_4

    .line 181
    :cond_24
    const/4 v2, 0x0

    move v10, v2

    goto/16 :goto_5

    .line 186
    :cond_25
    const/4 v2, 0x0

    goto/16 :goto_6

    :cond_26
    const/4 v3, 0x0

    goto/16 :goto_7

    :cond_27
    if-nez v2, :cond_28

    move-object v11, v9

    goto/16 :goto_8

    :cond_28
    if-eqz v3, :cond_2d

    iget-object v3, v12, Lcom/google/android/location/e/z;->c:Lcom/google/android/location/e/al;

    iget-object v4, v9, Lcom/google/android/location/e/z;->c:Lcom/google/android/location/e/al;

    invoke-static {v3, v4}, Lcom/google/android/location/g/d;->a(Lcom/google/android/location/e/al;Lcom/google/android/location/e/al;)I

    move-result v2

    iget v6, v3, Lcom/google/android/location/e/al;->f:I

    iget v7, v4, Lcom/google/android/location/e/al;->f:I

    add-int/2addr v6, v7

    const v7, 0x3567e0

    invoke-static {v6, v7}, Ljava/lang/Math;->max(II)I

    move-result v6

    div-int/lit16 v6, v6, 0x3e8

    if-gt v2, v6, :cond_29

    const/4 v2, 0x1

    :goto_1f
    if-eqz v2, :cond_2b

    iget v2, v3, Lcom/google/android/location/e/al;->f:I

    iget v3, v4, Lcom/google/android/location/e/al;->f:I

    if-le v2, v3, :cond_2a

    const/4 v2, 0x1

    :goto_20
    if-eqz v2, :cond_2d

    move-object v11, v9

    goto/16 :goto_8

    :cond_29
    const/4 v2, 0x0

    goto :goto_1f

    :cond_2a
    const/4 v2, 0x0

    goto :goto_20

    :cond_2b
    iget v2, v3, Lcom/google/android/location/e/al;->g:I

    iget v3, v4, Lcom/google/android/location/e/al;->g:I

    if-ge v2, v3, :cond_2c

    const/4 v2, 0x1

    goto :goto_20

    :cond_2c
    const/4 v2, 0x0

    goto :goto_20

    :cond_2d
    move-object v11, v12

    goto/16 :goto_8

    .line 204
    :cond_2e
    if-nez v10, :cond_2f

    const/4 v2, 0x1

    :goto_21
    move v13, v10

    move v14, v2

    move v15, v4

    goto/16 :goto_9

    :cond_2f
    const/4 v2, 0x0

    goto :goto_21

    .line 225
    :cond_30
    if-eqz v14, :cond_31

    .line 226
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/location/g/f;->g:Lcom/google/android/location/os/bi;

    const-string v3, "nlp"

    const-string v4, "locType"

    const-string v5, "uses_wifi"

    const-wide/16 v6, 0x1

    const/4 v8, 0x1

    invoke-interface/range {v2 .. v8}, Lcom/google/android/location/os/bi;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JZ)V

    goto/16 :goto_a

    .line 227
    :cond_31
    if-eqz v13, :cond_32

    .line 228
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/location/g/f;->g:Lcom/google/android/location/os/bi;

    const-string v3, "nlp"

    const-string v4, "locType"

    const-string v5, "uses_indoor"

    const-wide/16 v6, 0x1

    const/4 v8, 0x1

    invoke-interface/range {v2 .. v8}, Lcom/google/android/location/os/bi;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JZ)V

    goto/16 :goto_a

    .line 231
    :cond_32
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/location/g/f;->g:Lcom/google/android/location/os/bi;

    const-string v3, "nlp"

    const-string v4, "locType"

    const-string v5, "no_location"

    const-wide/16 v6, 0x1

    const/4 v8, 0x1

    invoke-interface/range {v2 .. v8}, Lcom/google/android/location/os/bi;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JZ)V

    goto/16 :goto_a

    .line 235
    :cond_33
    iget-object v2, v2, Lcom/google/android/location/g/ae;->d:Lcom/google/t/b/b/a/d;

    invoke-virtual {v2}, Lcom/google/t/b/b/a/d;->a()I

    move-result v2

    move v6, v2

    goto/16 :goto_b

    .line 245
    :cond_34
    iget-object v2, v2, Lcom/google/android/location/g/ae;->d:Lcom/google/t/b/b/a/d;

    invoke-virtual {v2}, Lcom/google/t/b/b/a/d;->b()I

    move-result v2

    move v6, v2

    goto/16 :goto_c

    .line 253
    :cond_35
    iget-object v2, v2, Lcom/google/android/location/g/ae;->d:Lcom/google/t/b/b/a/d;

    invoke-virtual {v2}, Lcom/google/t/b/b/a/d;->c()I

    move-result v2

    move v6, v2

    goto/16 :goto_d

    .line 261
    :cond_36
    iget-object v2, v2, Lcom/google/android/location/g/ae;->d:Lcom/google/t/b/b/a/d;

    invoke-virtual {v2}, Lcom/google/t/b/b/a/d;->d()I

    move-result v2

    move v6, v2

    goto/16 :goto_e

    .line 269
    :cond_37
    iget-object v2, v2, Lcom/google/android/location/g/ae;->d:Lcom/google/t/b/b/a/d;

    invoke-virtual {v2}, Lcom/google/t/b/b/a/d;->f()I

    move-result v2

    move v6, v2

    goto/16 :goto_f

    .line 279
    :cond_38
    iget-object v2, v2, Lcom/google/android/location/g/ae;->d:Lcom/google/t/b/b/a/d;

    invoke-virtual {v2}, Lcom/google/t/b/b/a/d;->g()I

    move-result v2

    move v6, v2

    goto/16 :goto_10

    .line 289
    :cond_39
    iget-object v2, v2, Lcom/google/android/location/g/ae;->d:Lcom/google/t/b/b/a/d;

    invoke-virtual {v2}, Lcom/google/t/b/b/a/d;->e()Z

    move-result v2

    if-eqz v2, :cond_3a

    const v2, 0xf4240

    move v6, v2

    goto/16 :goto_11

    :cond_3a
    const/4 v2, 0x0

    move v6, v2

    goto/16 :goto_11

    .line 297
    :cond_3b
    iget-object v2, v2, Lcom/google/android/location/g/ae;->d:Lcom/google/t/b/b/a/d;

    invoke-virtual {v2}, Lcom/google/t/b/b/a/d;->h()I

    move-result v2

    move v6, v2

    goto/16 :goto_12

    .line 305
    :cond_3c
    iget-object v2, v2, Lcom/google/android/location/g/ae;->d:Lcom/google/t/b/b/a/d;

    invoke-virtual {v2}, Lcom/google/t/b/b/a/d;->i()I

    move-result v2

    move v6, v2

    goto/16 :goto_13

    .line 313
    :cond_3d
    iget-object v2, v2, Lcom/google/android/location/g/ae;->d:Lcom/google/t/b/b/a/d;

    invoke-virtual {v2}, Lcom/google/t/b/b/a/d;->j()I

    move-result v2

    move v6, v2

    goto/16 :goto_14

    .line 321
    :cond_3e
    iget-object v2, v2, Lcom/google/android/location/g/ae;->d:Lcom/google/t/b/b/a/d;

    invoke-virtual {v2}, Lcom/google/t/b/b/a/d;->k()I

    move-result v2

    move v6, v2

    goto/16 :goto_15

    .line 329
    :cond_3f
    iget-object v2, v2, Lcom/google/android/location/g/ae;->d:Lcom/google/t/b/b/a/d;

    invoke-virtual {v2}, Lcom/google/t/b/b/a/d;->m()I

    move-result v2

    move v6, v2

    goto/16 :goto_16

    .line 339
    :cond_40
    iget-object v2, v2, Lcom/google/android/location/g/ae;->d:Lcom/google/t/b/b/a/d;

    invoke-virtual {v2}, Lcom/google/t/b/b/a/d;->n()I

    move-result v2

    move v6, v2

    goto/16 :goto_17

    .line 349
    :cond_41
    iget-object v2, v2, Lcom/google/android/location/g/ae;->d:Lcom/google/t/b/b/a/d;

    invoke-virtual {v2}, Lcom/google/t/b/b/a/d;->l()Z

    move-result v2

    if-eqz v2, :cond_42

    const v2, 0xf4240

    move v6, v2

    goto/16 :goto_18

    :cond_42
    const/4 v2, 0x0

    move v6, v2

    goto/16 :goto_18

    .line 357
    :cond_43
    iget-object v2, v2, Lcom/google/android/location/g/ae;->d:Lcom/google/t/b/b/a/d;

    invoke-virtual {v2}, Lcom/google/t/b/b/a/d;->o()I

    move-result v2

    move v6, v2

    goto/16 :goto_19

    .line 363
    :cond_44
    iget-object v2, v2, Lcom/google/android/location/g/ae;->d:Lcom/google/t/b/b/a/d;

    invoke-virtual {v2}, Lcom/google/t/b/b/a/d;->p()I

    move-result v2

    move v6, v2

    goto/16 :goto_1a

    .line 371
    :cond_45
    iget-object v2, v2, Lcom/google/android/location/g/ae;->d:Lcom/google/t/b/b/a/d;

    invoke-virtual {v2}, Lcom/google/t/b/b/a/d;->q()I

    move-result v2

    move v6, v2

    goto/16 :goto_1b

    .line 380
    :cond_46
    const/4 v2, 0x0

    goto/16 :goto_1c

    :cond_47
    move v13, v2

    move v14, v3

    move v15, v4

    goto/16 :goto_9

    :cond_48
    move-object v2, v10

    goto/16 :goto_3

    :cond_49
    move-object v3, v2

    goto/16 :goto_0
.end method

.method public final a(JLcom/google/android/location/e/i;[Lcom/google/android/location/e/bi;Lcom/google/android/location/e/al;Z)Ljava/util/List;
    .locals 9

    .prologue
    .line 101
    new-instance v8, Ljava/util/ArrayList;

    if-nez p4, :cond_5

    const/4 v0, 0x0

    :goto_0
    invoke-direct {v8, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 103
    iget-object v1, p0, Lcom/google/android/location/g/f;->e:Lcom/google/android/location/e/d;

    move-wide v2, p1

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move v7, p6

    invoke-virtual/range {v1 .. v7}, Lcom/google/android/location/e/d;->a(JLcom/google/android/location/e/i;[Lcom/google/android/location/e/bi;Lcom/google/android/location/e/al;Z)V

    .line 105
    iget-wide v0, p0, Lcom/google/android/location/g/f;->c:J

    cmp-long v0, p1, v0

    if-ltz v0, :cond_b

    .line 106
    sget-boolean v0, Lcom/google/android/location/i/a;->b:Z

    if-eqz v0, :cond_0

    const-string v0, "LocatorManager"

    const-string v1, "Batch deadline expired."

    invoke-static {v0, v1}, Lcom/google/android/location/o/a/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 108
    :cond_0
    iget-object v0, p0, Lcom/google/android/location/g/f;->e:Lcom/google/android/location/e/d;

    iget-object v0, v0, Lcom/google/android/location/e/d;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_b

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/e/ap;

    .line 109
    sget-boolean v1, Lcom/google/android/location/i/a;->b:Z

    if-eqz v1, :cond_1

    const-string v1, "LocatorManager"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "BATCH: Computing location for timestamp "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v4, v0, Lcom/google/android/location/e/ap;->a:J

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", data is "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Lcom/google/android/location/o/a/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 111
    :cond_1
    invoke-virtual {p0, v0}, Lcom/google/android/location/g/f;->a(Lcom/google/android/location/e/ap;)Lcom/google/android/location/e/ag;

    move-result-object v3

    .line 113
    iget-object v1, v3, Lcom/google/android/location/e/ag;->a:Lcom/google/android/location/e/z;

    if-nez v1, :cond_6

    .line 114
    sget-boolean v1, Lcom/google/android/location/i/a;->b:Z

    if-eqz v1, :cond_2

    const-string v1, "LocatorManager"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Location timestamp : "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v6, v0, Lcom/google/android/location/e/ap;->a:J

    invoke-virtual {v4, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, ", bestResult is NULL"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/google/android/location/o/a/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 127
    :cond_2
    :goto_2
    sget-boolean v0, Lcom/google/android/location/i/a;->b:Z

    if-eqz v0, :cond_3

    const-string v1, "LocatorManager"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v0, "wifi status: "

    invoke-direct {v4, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v0, v3, Lcom/google/android/location/e/ag;->b:Lcom/google/android/location/e/bf;

    if-nez v0, :cond_9

    const-string v0, "null"

    :goto_3
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/google/android/location/o/a/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 129
    :cond_3
    sget-boolean v0, Lcom/google/android/location/i/a;->b:Z

    if-eqz v0, :cond_4

    const-string v1, "LocatorManager"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v0, "cell status: "

    invoke-direct {v4, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v0, v3, Lcom/google/android/location/e/ag;->c:Lcom/google/android/location/e/f;

    if-nez v0, :cond_a

    const-string v0, "null"

    :goto_4
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/google/android/location/o/a/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 132
    :cond_4
    invoke-virtual {v8, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_1

    .line 101
    :cond_5
    array-length v0, p4

    goto/16 :goto_0

    .line 116
    :cond_6
    const-string v1, "unknown"

    .line 118
    iget-object v4, v3, Lcom/google/android/location/e/ag;->a:Lcom/google/android/location/e/z;

    iget-object v5, v3, Lcom/google/android/location/e/ag;->b:Lcom/google/android/location/e/bf;

    if-ne v4, v5, :cond_8

    .line 119
    const-string v1, "wifi"

    .line 124
    :cond_7
    :goto_5
    sget-boolean v4, Lcom/google/android/location/i/a;->b:Z

    if-eqz v4, :cond_2

    const-string v4, "LocatorManager"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Location timestamp : "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v6, v0, Lcom/google/android/location/e/ap;->a:J

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v5, ", bestResult is "

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", with status "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, v3, Lcom/google/android/location/e/ag;->a:Lcom/google/android/location/e/z;

    iget-object v1, v1, Lcom/google/android/location/e/z;->d:Lcom/google/android/location/e/ab;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v4, v0}, Lcom/google/android/location/o/a/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    .line 120
    :cond_8
    iget-object v4, v3, Lcom/google/android/location/e/ag;->a:Lcom/google/android/location/e/z;

    iget-object v5, v3, Lcom/google/android/location/e/ag;->c:Lcom/google/android/location/e/f;

    if-ne v4, v5, :cond_7

    .line 121
    const-string v1, "cell"

    goto :goto_5

    .line 127
    :cond_9
    iget-object v0, v3, Lcom/google/android/location/e/ag;->b:Lcom/google/android/location/e/bf;

    iget-object v0, v0, Lcom/google/android/location/e/bf;->d:Lcom/google/android/location/e/ab;

    goto :goto_3

    .line 129
    :cond_a
    iget-object v0, v3, Lcom/google/android/location/e/ag;->c:Lcom/google/android/location/e/f;

    iget-object v0, v0, Lcom/google/android/location/e/f;->d:Lcom/google/android/location/e/ab;

    goto :goto_4

    .line 136
    :cond_b
    return-object v8
.end method
