.class public Lcom/google/android/location/settings/LocationHistorySettingsActivity;
.super Lcom/google/android/location/settings/b;
.source "SourceFile"


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0xe
.end annotation


# instance fields
.field private h:Landroid/widget/Button;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 38
    invoke-direct {p0}, Lcom/google/android/location/settings/b;-><init>()V

    return-void
.end method

.method private a(I)Landroid/app/AlertDialog;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 108
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 109
    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 110
    invoke-virtual {v0, p1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    .line 111
    const v1, 0x104000a

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 112
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/location/settings/LocationHistorySettingsActivity;)V
    .locals 2

    .prologue
    .line 38
    iget-object v0, p0, Lcom/google/android/location/settings/LocationHistorySettingsActivity;->h:Landroid/widget/Button;

    sget v1, Lcom/google/android/gms/p;->pS:I

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(I)V

    iget-object v0, p0, Lcom/google/android/location/settings/LocationHistorySettingsActivity;->h:Landroid/widget/Button;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    new-instance v0, Lcom/google/android/location/settings/i;

    invoke-direct {v0, p0, p0}, Lcom/google/android/location/settings/i;-><init>(Lcom/google/android/location/settings/LocationHistorySettingsActivity;Lcom/google/android/location/settings/LocationHistorySettingsActivity;)V

    new-instance v1, Landroid/os/Messenger;

    invoke-direct {v1, v0}, Landroid/os/Messenger;-><init>(Landroid/os/Handler;)V

    iget-object v0, p0, Lcom/google/android/location/settings/LocationHistorySettingsActivity;->g:Landroid/accounts/Account;

    invoke-static {p0, v0, v1}, Lcom/google/android/location/reporting/service/DeleteHistoryService;->a(Landroid/content/Context;Landroid/accounts/Account;Landroid/os/Messenger;)V

    return-void
.end method

.method static synthetic b(Lcom/google/android/location/settings/LocationHistorySettingsActivity;)V
    .locals 0

    .prologue
    .line 38
    invoke-direct {p0}, Lcom/google/android/location/settings/LocationHistorySettingsActivity;->e()V

    return-void
.end method

.method private e()V
    .locals 2

    .prologue
    .line 124
    iget-object v0, p0, Lcom/google/android/location/settings/LocationHistorySettingsActivity;->h:Landroid/widget/Button;

    sget v1, Lcom/google/android/gms/p;->pR:I

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(I)V

    .line 125
    iget-object v0, p0, Lcom/google/android/location/settings/LocationHistorySettingsActivity;->h:Landroid/widget/Button;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 126
    return-void
.end method


# virtual methods
.method protected final a(Lcom/google/android/location/reporting/config/AccountConfig;)I
    .locals 1

    .prologue
    .line 59
    invoke-virtual {p1}, Lcom/google/android/location/reporting/config/AccountConfig;->s()I

    move-result v0

    return v0
.end method

.method protected final b(Z)V
    .locals 2

    .prologue
    .line 65
    iget-object v0, p0, Lcom/google/android/location/settings/LocationHistorySettingsActivity;->e:Lcom/google/android/location/reporting/service/l;

    iget-object v1, p0, Lcom/google/android/location/settings/LocationHistorySettingsActivity;->g:Landroid/accounts/Account;

    invoke-interface {v0, v1, p1}, Lcom/google/android/location/reporting/service/l;->b(Landroid/accounts/Account;Z)V

    .line 66
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 49
    invoke-super {p0, p1}, Lcom/google/android/location/settings/b;->onCreate(Landroid/os/Bundle;)V

    .line 50
    sget v0, Lcom/google/android/gms/l;->cy:I

    invoke-virtual {p0, v0}, Lcom/google/android/location/settings/LocationHistorySettingsActivity;->setContentView(I)V

    .line 51
    sget v0, Lcom/google/android/gms/j;->lp:I

    invoke-virtual {p0, v0}, Lcom/google/android/location/settings/LocationHistorySettingsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 52
    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 53
    sget v0, Lcom/google/android/gms/j;->ef:I

    invoke-virtual {p0, v0}, Lcom/google/android/location/settings/LocationHistorySettingsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/google/android/location/settings/LocationHistorySettingsActivity;->h:Landroid/widget/Button;

    .line 54
    invoke-direct {p0}, Lcom/google/android/location/settings/LocationHistorySettingsActivity;->e()V

    .line 55
    return-void
.end method

.method protected onCreateDialog(I)Landroid/app/Dialog;
    .locals 3
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 71
    packed-switch p1, :pswitch_data_0

    .line 81
    invoke-super {p0, p1}, Lcom/google/android/location/settings/b;->onCreateDialog(I)Landroid/app/Dialog;

    move-result-object v0

    :goto_0
    return-object v0

    .line 73
    :pswitch_0
    sget v0, Lcom/google/android/gms/p;->pO:I

    sget v1, Lcom/google/android/gms/p;->pN:I

    new-instance v2, Lcom/google/android/location/settings/h;

    invoke-direct {v2, p0}, Lcom/google/android/location/settings/h;-><init>(Lcom/google/android/location/settings/LocationHistorySettingsActivity;)V

    invoke-static {p0, v0, v1, v2}, Lcom/google/android/gms/common/ui/d;->a(Landroid/app/Activity;IILandroid/content/DialogInterface$OnClickListener;)Landroid/app/Dialog;

    move-result-object v0

    goto :goto_0

    .line 75
    :pswitch_1
    sget v0, Lcom/google/android/gms/p;->pP:I

    invoke-direct {p0, v0}, Lcom/google/android/location/settings/LocationHistorySettingsActivity;->a(I)Landroid/app/AlertDialog;

    move-result-object v0

    goto :goto_0

    .line 77
    :pswitch_2
    sget v0, Lcom/google/android/gms/p;->pT:I

    invoke-direct {p0, v0}, Lcom/google/android/location/settings/LocationHistorySettingsActivity;->a(I)Landroid/app/AlertDialog;

    move-result-object v0

    goto :goto_0

    .line 79
    :pswitch_3
    sget v0, Lcom/google/android/gms/p;->pQ:I

    invoke-direct {p0, v0}, Lcom/google/android/location/settings/LocationHistorySettingsActivity;->a(I)Landroid/app/AlertDialog;

    move-result-object v0

    goto :goto_0

    .line 71
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public onDeleteLocationHistoryClicked(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 120
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lcom/google/android/location/settings/LocationHistorySettingsActivity;->showDialog(I)V

    .line 121
    return-void
.end method
