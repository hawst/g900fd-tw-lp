.class final Lcom/google/android/location/e/aa;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/location/e/ak;


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 83
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static b(Ljava/io/DataInput;)Lcom/google/android/location/e/z;
    .locals 6

    .prologue
    .line 87
    const/4 v0, 0x0

    .line 89
    :try_start_0
    invoke-static {}, Lcom/google/android/location/e/ab;->values()[Lcom/google/android/location/e/ab;

    move-result-object v1

    invoke-interface {p0}, Ljava/io/DataInput;->readInt()I

    move-result v2

    aget-object v1, v1, v2
    :try_end_0
    .catch Ljava/lang/ArrayIndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    .line 93
    sget-object v2, Lcom/google/android/location/e/ab;->a:Lcom/google/android/location/e/ab;

    if-ne v1, v2, :cond_0

    .line 94
    sget-object v0, Lcom/google/android/location/e/al;->h:Lcom/google/android/location/e/ak;

    invoke-interface {v0, p0}, Lcom/google/android/location/e/ak;->a(Ljava/io/DataInput;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/e/al;

    .line 96
    :cond_0
    new-instance v2, Lcom/google/android/location/e/z;

    invoke-interface {p0}, Ljava/io/DataInput;->readLong()J

    move-result-wide v4

    invoke-direct {v2, v0, v1, v4, v5}, Lcom/google/android/location/e/z;-><init>(Lcom/google/android/location/e/al;Lcom/google/android/location/e/ab;J)V

    return-object v2

    .line 91
    :catch_0
    move-exception v0

    new-instance v0, Ljava/io/IOException;

    const-string v1, "invalid status"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
.end method


# virtual methods
.method public final synthetic a(Ljava/io/DataInput;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 83
    invoke-static {p1}, Lcom/google/android/location/e/aa;->b(Ljava/io/DataInput;)Lcom/google/android/location/e/z;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic a(Ljava/lang/Object;Ljava/io/DataOutput;)V
    .locals 2

    .prologue
    .line 83
    check-cast p1, Lcom/google/android/location/e/z;

    iget-object v0, p1, Lcom/google/android/location/e/z;->d:Lcom/google/android/location/e/ab;

    invoke-virtual {v0}, Lcom/google/android/location/e/ab;->ordinal()I

    move-result v0

    invoke-interface {p2, v0}, Ljava/io/DataOutput;->writeInt(I)V

    iget-object v0, p1, Lcom/google/android/location/e/z;->d:Lcom/google/android/location/e/ab;

    sget-object v1, Lcom/google/android/location/e/ab;->a:Lcom/google/android/location/e/ab;

    if-ne v0, v1, :cond_0

    sget-object v0, Lcom/google/android/location/e/al;->h:Lcom/google/android/location/e/ak;

    iget-object v1, p1, Lcom/google/android/location/e/z;->c:Lcom/google/android/location/e/al;

    invoke-interface {v0, v1, p2}, Lcom/google/android/location/e/ak;->a(Ljava/lang/Object;Ljava/io/DataOutput;)V

    :cond_0
    iget-wide v0, p1, Lcom/google/android/location/e/z;->e:J

    invoke-interface {p2, v0, v1}, Ljava/io/DataOutput;->writeLong(J)V

    return-void
.end method
