.class public final Lcom/google/android/location/copresence/ap;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final a:J

.field private static h:Lcom/google/android/location/copresence/ap;

.field private static final i:Ljava/util/concurrent/locks/ReentrantLock;


# instance fields
.field private final b:Ljava/util/concurrent/CountDownLatch;

.field private final c:Lcom/google/android/location/copresence/ar;

.field private final d:Landroid/os/HandlerThread;

.field private final e:Landroid/content/Context;

.field private f:Lcom/google/android/location/copresence/c;

.field private volatile g:Lcom/google/android/location/copresence/j;

.field private final j:Ljava/lang/Runnable;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 45
    sget-object v0, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0x5

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    sput-wide v0, Lcom/google/android/location/copresence/ap;->a:J

    .line 55
    new-instance v0, Ljava/util/concurrent/locks/ReentrantLock;

    invoke-direct {v0}, Ljava/util/concurrent/locks/ReentrantLock;-><init>()V

    sput-object v0, Lcom/google/android/location/copresence/ap;->i:Ljava/util/concurrent/locks/ReentrantLock;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 74
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 47
    new-instance v0, Ljava/util/concurrent/CountDownLatch;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/location/copresence/ap;->b:Ljava/util/concurrent/CountDownLatch;

    .line 129
    new-instance v0, Lcom/google/android/location/copresence/aq;

    invoke-direct {v0, p0}, Lcom/google/android/location/copresence/aq;-><init>(Lcom/google/android/location/copresence/ap;)V

    iput-object v0, p0, Lcom/google/android/location/copresence/ap;->j:Ljava/lang/Runnable;

    .line 75
    iput-object p1, p0, Lcom/google/android/location/copresence/ap;->e:Landroid/content/Context;

    .line 77
    new-instance v0, Landroid/os/HandlerThread;

    const-string v1, "CopresenceEventLoop"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;I)V

    iput-object v0, p0, Lcom/google/android/location/copresence/ap;->d:Landroid/os/HandlerThread;

    .line 79
    iget-object v0, p0, Lcom/google/android/location/copresence/ap;->d:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    .line 80
    new-instance v1, Lcom/google/android/location/copresence/ar;

    iget-object v0, p0, Lcom/google/android/location/copresence/ap;->d:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v2

    const-string v0, "power"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    invoke-direct {v1, p0, v2, v0}, Lcom/google/android/location/copresence/ar;-><init>(Lcom/google/android/location/copresence/ap;Landroid/os/Looper;Landroid/os/PowerManager;)V

    iput-object v1, p0, Lcom/google/android/location/copresence/ap;->c:Lcom/google/android/location/copresence/ar;

    .line 83
    return-void
.end method

.method static synthetic a(Lcom/google/android/location/copresence/ap;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lcom/google/android/location/copresence/ap;->e:Landroid/content/Context;

    return-object v0
.end method

.method public static a(Landroid/content/Context;)Lcom/google/android/location/copresence/ap;
    .locals 3

    .prologue
    .line 58
    sget-object v0, Lcom/google/android/location/copresence/ap;->i:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    .line 59
    sget-object v0, Lcom/google/android/location/copresence/ap;->h:Lcom/google/android/location/copresence/ap;

    if-nez v0, :cond_0

    .line 60
    new-instance v0, Lcom/google/android/location/copresence/ap;

    invoke-direct {v0, p0}, Lcom/google/android/location/copresence/ap;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/google/android/location/copresence/ap;->h:Lcom/google/android/location/copresence/ap;

    .line 61
    sget-object v0, Lcom/google/android/location/copresence/ap;->i:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    .line 62
    sget-object v0, Lcom/google/android/location/copresence/ap;->h:Lcom/google/android/location/copresence/ap;

    iget-object v1, v0, Lcom/google/android/location/copresence/ap;->j:Ljava/lang/Runnable;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/google/android/location/copresence/ap;->a(Ljava/lang/Runnable;Landroid/os/WorkSource;)V

    :try_start_0
    iget-object v0, v0, Lcom/google/android/location/copresence/ap;->b:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->await()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 63
    :goto_0
    sget-object v0, Lcom/google/android/location/copresence/ap;->h:Lcom/google/android/location/copresence/ap;

    .line 66
    :goto_1
    return-object v0

    .line 62
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_0

    .line 65
    :cond_0
    sget-object v0, Lcom/google/android/location/copresence/ap;->i:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    .line 66
    sget-object v0, Lcom/google/android/location/copresence/ap;->h:Lcom/google/android/location/copresence/ap;

    goto :goto_1
.end method

.method static synthetic a(Lcom/google/android/location/copresence/ap;Lcom/google/android/location/copresence/c;)Lcom/google/android/location/copresence/c;
    .locals 0

    .prologue
    .line 43
    iput-object p1, p0, Lcom/google/android/location/copresence/ap;->f:Lcom/google/android/location/copresence/c;

    return-object p1
.end method

.method static synthetic a(Lcom/google/android/location/copresence/ap;Lcom/google/android/location/copresence/j;)Lcom/google/android/location/copresence/j;
    .locals 0

    .prologue
    .line 43
    iput-object p1, p0, Lcom/google/android/location/copresence/ap;->g:Lcom/google/android/location/copresence/j;

    return-object p1
.end method

.method static synthetic b(Lcom/google/android/location/copresence/ap;)Ljava/util/concurrent/CountDownLatch;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lcom/google/android/location/copresence/ap;->b:Ljava/util/concurrent/CountDownLatch;

    return-object v0
.end method

.method static synthetic c(Lcom/google/android/location/copresence/ap;)Lcom/google/android/location/copresence/j;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lcom/google/android/location/copresence/ap;->g:Lcom/google/android/location/copresence/j;

    return-object v0
.end method

.method static synthetic e()J
    .locals 2

    .prologue
    .line 43
    sget-wide v0, Lcom/google/android/location/copresence/ap;->a:J

    return-wide v0
.end method


# virtual methods
.method public final a()Lcom/google/android/location/copresence/j;
    .locals 1

    .prologue
    .line 107
    iget-object v0, p0, Lcom/google/android/location/copresence/ap;->g:Lcom/google/android/location/copresence/j;

    return-object v0
.end method

.method public final a(Ljava/lang/Runnable;Landroid/os/WorkSource;)V
    .locals 1

    .prologue
    .line 103
    iget-object v0, p0, Lcom/google/android/location/copresence/ap;->c:Lcom/google/android/location/copresence/ar;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/location/copresence/ar;->a(Ljava/lang/Runnable;Landroid/os/WorkSource;)V

    .line 104
    return-void
.end method

.method public final b()Lcom/google/android/location/copresence/c;
    .locals 1

    .prologue
    .line 114
    iget-object v0, p0, Lcom/google/android/location/copresence/ap;->f:Lcom/google/android/location/copresence/c;

    return-object v0
.end method

.method public final c()V
    .locals 2

    .prologue
    .line 121
    iget-object v0, p0, Lcom/google/android/location/copresence/ap;->c:Lcom/google/android/location/copresence/ar;

    invoke-virtual {v0}, Lcom/google/android/location/copresence/ar;->getLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "This method must run in the EventLoop thread."

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/bx;->a(ZLjava/lang/Object;)V

    .line 124
    return-void

    .line 121
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final d()Landroid/os/Handler;
    .locals 1

    .prologue
    .line 206
    iget-object v0, p0, Lcom/google/android/location/copresence/ap;->c:Lcom/google/android/location/copresence/ar;

    return-object v0
.end method
