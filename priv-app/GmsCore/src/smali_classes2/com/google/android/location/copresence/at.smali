.class public final Lcom/google/android/location/copresence/at;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final a:J

.field private static e:Lcom/google/android/location/copresence/at;


# instance fields
.field private final b:Ljava/util/HashSet;

.field private final c:Landroid/content/Context;

.field private final d:Lcom/google/android/location/copresence/c;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 23
    sget-object v0, Ljava/util/concurrent/TimeUnit;->MINUTES:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0x1

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    sput-wide v0, Lcom/google/android/location/copresence/at;->a:J

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/location/copresence/at;->b:Ljava/util/HashSet;

    .line 41
    iput-object p1, p0, Lcom/google/android/location/copresence/at;->c:Landroid/content/Context;

    .line 42
    invoke-static {p1}, Lcom/google/android/location/copresence/c;->a(Landroid/content/Context;)Lcom/google/android/location/copresence/c;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/copresence/at;->d:Lcom/google/android/location/copresence/c;

    .line 43
    return-void
.end method

.method static synthetic a(Lcom/google/android/location/copresence/at;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 18
    iget-object v0, p0, Lcom/google/android/location/copresence/at;->c:Landroid/content/Context;

    return-object v0
.end method

.method public static declared-synchronized a(Landroid/content/Context;)Lcom/google/android/location/copresence/at;
    .locals 2

    .prologue
    .line 33
    const-class v1, Lcom/google/android/location/copresence/at;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/google/android/location/copresence/at;->e:Lcom/google/android/location/copresence/at;

    if-nez v0, :cond_0

    .line 34
    new-instance v0, Lcom/google/android/location/copresence/at;

    invoke-direct {v0, p0}, Lcom/google/android/location/copresence/at;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/google/android/location/copresence/at;->e:Lcom/google/android/location/copresence/at;

    .line 36
    :cond_0
    sget-object v0, Lcom/google/android/location/copresence/at;->e:Lcom/google/android/location/copresence/at;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 33
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method static synthetic b(Lcom/google/android/location/copresence/at;)Ljava/util/HashSet;
    .locals 1

    .prologue
    .line 18
    iget-object v0, p0, Lcom/google/android/location/copresence/at;->b:Ljava/util/HashSet;

    return-object v0
.end method


# virtual methods
.method public final a(Landroid/accounts/Account;)V
    .locals 4

    .prologue
    .line 50
    iget-object v1, p0, Lcom/google/android/location/copresence/at;->b:Ljava/util/HashSet;

    monitor-enter v1

    .line 51
    :try_start_0
    iget-object v0, p0, Lcom/google/android/location/copresence/at;->b:Ljava/util/HashSet;

    invoke-virtual {v0, p1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 52
    monitor-exit v1

    .line 66
    :goto_0
    return-void

    .line 54
    :cond_0
    iget-object v0, p0, Lcom/google/android/location/copresence/at;->b:Ljava/util/HashSet;

    invoke-virtual {v0, p1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 55
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 56
    const/4 v0, 0x2

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 57
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "LocationReportUploadRequester: Starting async task: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p1, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->a(Ljava/lang/String;)I

    .line 60
    :cond_1
    new-instance v0, Lcom/google/android/location/copresence/au;

    invoke-direct {v0, p0, p1}, Lcom/google/android/location/copresence/au;-><init>(Lcom/google/android/location/copresence/at;Landroid/accounts/Account;)V

    invoke-virtual {v0}, Lcom/google/android/location/copresence/au;->run()V

    .line 64
    iget-object v0, p0, Lcom/google/android/location/copresence/at;->d:Lcom/google/android/location/copresence/c;

    new-instance v1, Lcom/google/android/location/copresence/au;

    invoke-direct {v1, p0, p1}, Lcom/google/android/location/copresence/au;-><init>(Lcom/google/android/location/copresence/at;Landroid/accounts/Account;)V

    sget-wide v2, Lcom/google/android/location/copresence/at;->a:J

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/location/copresence/c;->a(Ljava/lang/Runnable;J)V

    goto :goto_0

    .line 55
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method
