.class public final Lcom/google/android/location/places/an;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final a:Landroid/os/Handler;

.field final b:Lcom/google/android/location/places/f;

.field public final c:Ljava/lang/Object;

.field public final d:Lcom/google/android/location/places/bx;

.field final e:Lcom/google/android/location/places/x;

.field private final f:Lcom/google/android/location/fused/g;

.field private final g:Lcom/google/android/location/places/y;

.field private final h:Lcom/google/android/location/places/ak;

.field private final i:Landroid/content/pm/PackageManager;

.field private final j:Lcom/google/android/location/places/d/e;

.field private final k:Lcom/google/android/gms/common/util/p;

.field private final l:Lcom/google/android/location/n/ae;

.field private m:Ljava/util/Map;

.field private final n:Lcom/google/android/gms/location/k;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/os/Handler;Lcom/google/android/location/places/bx;Lcom/google/android/location/fused/g;Lcom/google/android/location/places/y;Lcom/google/android/location/places/ak;Lcom/google/android/location/places/d/e;Lcom/google/android/location/places/f;Lcom/google/android/gms/common/util/p;)V
    .locals 1

    .prologue
    .line 93
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 72
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/location/places/an;->m:Ljava/util/Map;

    .line 73
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/location/places/an;->c:Ljava/lang/Object;

    .line 405
    new-instance v0, Lcom/google/android/location/places/ao;

    invoke-direct {v0, p0}, Lcom/google/android/location/places/ao;-><init>(Lcom/google/android/location/places/an;)V

    iput-object v0, p0, Lcom/google/android/location/places/an;->n:Lcom/google/android/gms/location/k;

    .line 422
    new-instance v0, Lcom/google/android/location/places/ap;

    invoke-direct {v0, p0}, Lcom/google/android/location/places/ap;-><init>(Lcom/google/android/location/places/an;)V

    iput-object v0, p0, Lcom/google/android/location/places/an;->e:Lcom/google/android/location/places/x;

    .line 94
    iput-object p2, p0, Lcom/google/android/location/places/an;->a:Landroid/os/Handler;

    .line 95
    iput-object p3, p0, Lcom/google/android/location/places/an;->d:Lcom/google/android/location/places/bx;

    .line 96
    iput-object p4, p0, Lcom/google/android/location/places/an;->f:Lcom/google/android/location/fused/g;

    .line 97
    iput-object p5, p0, Lcom/google/android/location/places/an;->g:Lcom/google/android/location/places/y;

    .line 98
    iput-object p6, p0, Lcom/google/android/location/places/an;->h:Lcom/google/android/location/places/ak;

    .line 99
    iput-object p7, p0, Lcom/google/android/location/places/an;->j:Lcom/google/android/location/places/d/e;

    .line 100
    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/places/an;->i:Landroid/content/pm/PackageManager;

    .line 101
    iput-object p8, p0, Lcom/google/android/location/places/an;->b:Lcom/google/android/location/places/f;

    .line 102
    iput-object p9, p0, Lcom/google/android/location/places/an;->k:Lcom/google/android/gms/common/util/p;

    .line 103
    invoke-static {p1}, Lcom/google/android/location/n/ae;->a(Landroid/content/Context;)Lcom/google/android/location/n/ae;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/places/an;->l:Lcom/google/android/location/n/ae;

    .line 104
    return-void
.end method

.method private a()V
    .locals 5

    .prologue
    .line 178
    iget-object v1, p0, Lcom/google/android/location/places/an;->c:Ljava/lang/Object;

    monitor-enter v1

    .line 179
    :try_start_0
    iget-object v0, p0, Lcom/google/android/location/places/an;->d:Lcom/google/android/location/places/bx;

    invoke-virtual {v0}, Lcom/google/android/location/places/bx;->a()Ljava/util/List;

    move-result-object v0

    .line 180
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 181
    iget-object v0, p0, Lcom/google/android/location/places/an;->f:Lcom/google/android/location/fused/g;

    iget-object v2, p0, Lcom/google/android/location/places/an;->n:Lcom/google/android/gms/location/k;

    invoke-virtual {v0, v2}, Lcom/google/android/location/fused/g;->a(Lcom/google/android/gms/location/j;)V

    .line 182
    iget-object v0, p0, Lcom/google/android/location/places/an;->g:Lcom/google/android/location/places/y;

    invoke-static {}, Lcom/google/android/location/places/p;->a()Lcom/google/android/location/o/a/c;

    move-result-object v2

    const-string v3, "PlaceInferenceEngine stop"

    invoke-virtual {v2, v3}, Lcom/google/android/location/o/a/c;->a(Ljava/lang/String;)V

    iget-object v2, v0, Lcom/google/android/location/places/y;->b:Ljava/util/Set;

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 186
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 182
    :cond_0
    :try_start_1
    iget-object v0, v0, Lcom/google/android/location/places/y;->b:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    .line 186
    :cond_1
    monitor-exit v1

    return-void

    .line 184
    :cond_2
    iget-object v2, p0, Lcom/google/android/location/places/an;->g:Lcom/google/android/location/places/y;

    iget-object v0, p0, Lcom/google/android/location/places/an;->e:Lcom/google/android/location/places/x;

    invoke-static {}, Lcom/google/android/location/places/p;->a()Lcom/google/android/location/o/a/c;

    move-result-object v3

    const-string v4, "PlaceInferenceEngine start"

    invoke-virtual {v3, v4}, Lcom/google/android/location/o/a/c;->a(Ljava/lang/String;)V

    invoke-static {v0}, Lcom/google/k/a/ah;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/places/x;

    iput-object v0, v2, Lcom/google/android/location/places/y;->h:Lcom/google/android/location/places/x;

    iget-object v0, v2, Lcom/google/android/location/places/y;->b:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    iget-object v0, v2, Lcom/google/android/location/places/y;->c:Lcom/google/android/location/places/bv;

    if-eqz v0, :cond_3

    iget-object v0, v2, Lcom/google/android/location/places/y;->b:Ljava/util/Set;

    iget-object v3, v2, Lcom/google/android/location/places/y;->c:Lcom/google/android/location/places/bv;

    invoke-interface {v0, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    invoke-static {}, Lcom/google/android/location/places/p;->a()Lcom/google/android/location/o/a/c;

    move-result-object v0

    const-string v3, "Adding reverse geocoding module"

    invoke-virtual {v0, v3}, Lcom/google/android/location/o/a/c;->b(Ljava/lang/String;)V

    :cond_3
    iget-object v0, v2, Lcom/google/android/location/places/y;->d:Lcom/google/android/location/places/f/a;

    if-eqz v0, :cond_4

    iget-object v0, v2, Lcom/google/android/location/places/y;->b:Ljava/util/Set;

    iget-object v3, v2, Lcom/google/android/location/places/y;->d:Lcom/google/android/location/places/f/a;

    invoke-interface {v0, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    invoke-static {}, Lcom/google/android/location/places/p;->a()Lcom/google/android/location/o/a/c;

    move-result-object v0

    const-string v3, "Adding WiFi decision tree module"

    invoke-virtual {v0, v3}, Lcom/google/android/location/o/a/c;->b(Ljava/lang/String;)V

    :cond_4
    iget-object v0, v2, Lcom/google/android/location/places/y;->e:Lcom/google/android/location/places/a/a;

    if-eqz v0, :cond_5

    iget-object v0, v2, Lcom/google/android/location/places/y;->b:Ljava/util/Set;

    iget-object v3, v2, Lcom/google/android/location/places/y;->e:Lcom/google/android/location/places/a/a;

    invoke-interface {v0, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    invoke-static {}, Lcom/google/android/location/places/p;->a()Lcom/google/android/location/o/a/c;

    move-result-object v0

    const-string v3, "Adding BLE proximity module"

    invoke-virtual {v0, v3}, Lcom/google/android/location/o/a/c;->b(Ljava/lang/String;)V

    :cond_5
    iget-object v0, v2, Lcom/google/android/location/places/y;->b:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/places/ab;

    iget-object v4, v2, Lcom/google/android/location/places/y;->i:Lcom/google/android/location/places/ac;

    invoke-virtual {v0, v4}, Lcom/google/android/location/places/ab;->a(Lcom/google/android/location/places/ac;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1
.end method

.method static synthetic a(Lcom/google/android/location/places/an;Ljava/util/List;)V
    .locals 12

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    const/4 v1, 0x3

    .line 60
    iget-object v0, p0, Lcom/google/android/location/places/an;->l:Lcom/google/android/location/n/ae;

    invoke-virtual {v0}, Lcom/google/android/location/n/ae;->a()Z

    move-result v0

    if-nez v0, :cond_1

    const-string v0, "Places"

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "Places"

    const-string v1, "User is not in the foreground, dropping place estimate"

    invoke-static {v0, v1}, Lcom/google/android/location/n/aa;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v5, p0, Lcom/google/android/location/places/an;->c:Ljava/lang/Object;

    monitor-enter v5

    :try_start_0
    iget-object v0, p0, Lcom/google/android/location/places/an;->d:Lcom/google/android/location/places/bx;

    invoke-virtual {v0}, Lcom/google/android/location/places/bx;->a()Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_2
    :goto_1
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_d

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/places/PlaceSubscription;

    iget-object v1, p0, Lcom/google/android/location/places/an;->i:Landroid/content/pm/PackageManager;

    invoke-virtual {v0, v1}, Lcom/google/android/location/places/PlaceSubscription;->a(Landroid/content/pm/PackageManager;)Z

    move-result v1

    if-nez v1, :cond_4

    invoke-virtual {p0, v0}, Lcom/google/android/location/places/an;->b(Lcom/google/android/location/places/PlaceSubscription;)V

    const-string v0, "Places"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_3

    const-string v0, "Places"

    const-string v1, "Place subscription is not permitted - removed"

    invoke-static {v0, v1}, Lcom/google/android/location/n/aa;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_3
    monitor-exit v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v5

    throw v0

    :cond_4
    :try_start_1
    iget-object v1, p0, Lcom/google/android/location/places/an;->m:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/location/places/ar;

    if-eqz v1, :cond_2

    iget-object v2, v1, Lcom/google/android/location/places/ar;->a:Lcom/google/android/location/places/PlaceSubscription;

    iget-object v2, v2, Lcom/google/android/location/places/PlaceSubscription;->a:Lcom/google/android/gms/location/places/PlaceRequest;

    invoke-virtual {v2}, Lcom/google/android/gms/location/places/PlaceRequest;->a()Lcom/google/android/gms/location/places/PlaceFilter;

    move-result-object v7

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :cond_5
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_8

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/gms/location/places/PlaceLikelihood;

    invoke-virtual {v2}, Lcom/google/android/gms/location/places/PlaceLikelihood;->b()Lcom/google/android/gms/location/places/f;

    move-result-object v2

    invoke-virtual {v7, v2}, Lcom/google/android/gms/location/places/PlaceFilter;->a(Lcom/google/android/gms/location/places/f;)Z

    move-result v2

    if-eqz v2, :cond_5

    move v2, v4

    :goto_2
    if-nez v2, :cond_9

    const-string v2, "Places"

    const/4 v7, 0x3

    invoke-static {v2, v7}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_6

    const-string v2, "Places"

    const-string v7, "Place subscription not interested in the place estimate"

    invoke-static {v2, v7}, Lcom/google/android/location/n/aa;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_6
    move v2, v3

    :goto_3
    if-eqz v2, :cond_2

    :try_start_2
    iget-object v2, p0, Lcom/google/android/location/places/an;->h:Lcom/google/android/location/places/ak;

    const/16 v7, 0x69

    invoke-virtual {v2, v7, p1, v0}, Lcom/google/android/location/places/ak;->a(ILjava/util/List;Lcom/google/android/location/places/Subscription;)V

    iget v2, v1, Lcom/google/android/location/places/ar;->d:I

    add-int/lit8 v2, v2, 0x1

    iput v2, v1, Lcom/google/android/location/places/ar;->d:I

    iput-object p1, v1, Lcom/google/android/location/places/ar;->e:Ljava/util/List;

    iget-object v2, v1, Lcom/google/android/location/places/ar;->b:Lcom/google/android/gms/common/util/p;

    invoke-interface {v2}, Lcom/google/android/gms/common/util/p;->b()J

    move-result-wide v8

    iput-wide v8, v1, Lcom/google/android/location/places/ar;->c:J

    const-string v1, "Places"

    const/4 v2, 0x3

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_2

    const-string v1, "Places"

    const-string v2, "Place estimate delivered"

    invoke-static {v1, v2}, Lcom/google/android/location/n/aa;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catch Landroid/app/PendingIntent$CanceledException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_1

    :catch_0
    move-exception v1

    :try_start_3
    const-string v1, "Places"

    const/4 v2, 0x3

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_7

    const-string v1, "Places"

    const-string v2, "pending intent cancelled by client"

    invoke-static {v1, v2}, Lcom/google/android/location/n/aa;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_7
    invoke-virtual {p0, v0}, Lcom/google/android/location/places/an;->b(Lcom/google/android/location/places/PlaceSubscription;)V

    goto/16 :goto_1

    :cond_8
    move v2, v3

    goto :goto_2

    :cond_9
    iget-object v2, v1, Lcom/google/android/location/places/ar;->b:Lcom/google/android/gms/common/util/p;

    invoke-interface {v2}, Lcom/google/android/gms/common/util/p;->b()J

    move-result-wide v8

    iget-wide v10, v1, Lcom/google/android/location/places/ar;->c:J

    sub-long/2addr v8, v10

    iget v2, v1, Lcom/google/android/location/places/ar;->d:I

    if-eqz v2, :cond_c

    iget-object v2, v1, Lcom/google/android/location/places/ar;->a:Lcom/google/android/location/places/PlaceSubscription;

    iget-object v2, v2, Lcom/google/android/location/places/PlaceSubscription;->a:Lcom/google/android/gms/location/places/PlaceRequest;

    invoke-virtual {v2}, Lcom/google/android/gms/location/places/PlaceRequest;->b()J

    move-result-wide v10

    cmp-long v2, v8, v10

    if-gez v2, :cond_c

    iget-object v2, v1, Lcom/google/android/location/places/ar;->e:Ljava/util/List;

    invoke-static {p1, v2}, Lcom/google/android/gms/common/internal/bu;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_b

    move v2, v4

    :goto_4
    if-nez v2, :cond_c

    const-string v2, "Places"

    const/4 v7, 0x3

    invoke-static {v2, v7}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_a

    const-string v2, "Places"

    const-string v7, "Place subscription already received a similar update"

    invoke-static {v2, v7}, Lcom/google/android/location/n/aa;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_a
    move v2, v3

    goto :goto_3

    :cond_b
    move v2, v3

    goto :goto_4

    :cond_c
    move v2, v4

    goto :goto_3

    :cond_d
    monitor-exit v5
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto/16 :goto_0
.end method


# virtual methods
.method public final a(Landroid/content/Intent;)V
    .locals 3

    .prologue
    const/4 v1, 0x2

    .line 107
    iget-object v0, p0, Lcom/google/android/location/places/an;->d:Lcom/google/android/location/places/bx;

    invoke-virtual {v0, p1}, Lcom/google/android/location/places/bx;->b(Landroid/content/Intent;)Z

    move-result v0

    invoke-static {v0}, Lcom/google/android/gms/common/internal/bx;->b(Z)V

    .line 108
    const-string v0, "Places"

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 109
    const-string v0, "Places"

    const-string v1, "Initializing PlaceSubscriptionManager\'s system cache."

    invoke-static {v0, v1}, Lcom/google/android/location/n/aa;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 112
    :cond_0
    iget-object v1, p0, Lcom/google/android/location/places/an;->c:Ljava/lang/Object;

    monitor-enter v1

    .line 113
    :try_start_0
    iget-object v0, p0, Lcom/google/android/location/places/an;->d:Lcom/google/android/location/places/bx;

    iget-boolean v0, v0, Lcom/google/android/location/places/bx;->a:Z

    if-eqz v0, :cond_2

    .line 114
    const-string v0, "Places"

    const/4 v2, 0x2

    invoke-static {v0, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 115
    const-string v0, "Places"

    const-string v2, "PlaceSubscriptionManager.initializeSystemCache called >1 times"

    invoke-static {v0, v2}, Lcom/google/android/location/n/aa;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 118
    :cond_1
    monitor-exit v1

    .line 127
    :goto_0
    return-void

    .line 120
    :cond_2
    iget-object v0, p0, Lcom/google/android/location/places/an;->d:Lcom/google/android/location/places/bx;

    invoke-virtual {v0, p1}, Lcom/google/android/location/places/bx;->a(Landroid/content/Intent;)V

    .line 124
    iget-object v0, p0, Lcom/google/android/location/places/an;->d:Lcom/google/android/location/places/bx;

    invoke-virtual {v0}, Lcom/google/android/location/places/bx;->a()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/places/PlaceSubscription;

    .line 125
    invoke-virtual {p0, v0}, Lcom/google/android/location/places/an;->a(Lcom/google/android/location/places/PlaceSubscription;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    .line 127
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    :cond_3
    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0
.end method

.method public final a(Lcom/google/android/location/places/PlaceSubscription;)V
    .locals 9

    .prologue
    const/16 v0, 0x69

    const/4 v8, 0x2

    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 193
    iget-object v1, p0, Lcom/google/android/location/places/an;->m:Ljava/util/Map;

    new-instance v2, Lcom/google/android/location/places/ar;

    iget-object v5, p0, Lcom/google/android/location/places/an;->k:Lcom/google/android/gms/common/util/p;

    invoke-direct {v2, p1, v5, v4}, Lcom/google/android/location/places/ar;-><init>(Lcom/google/android/location/places/PlaceSubscription;Lcom/google/android/gms/common/util/p;B)V

    invoke-interface {v1, p1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 195
    invoke-virtual {p1}, Lcom/google/android/location/places/PlaceSubscription;->f()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 196
    iget-object v1, p0, Lcom/google/android/location/places/an;->j:Lcom/google/android/location/places/d/e;

    invoke-virtual {v1, p1}, Lcom/google/android/location/places/d/e;->b(Lcom/google/android/location/places/Subscription;)V

    .line 199
    :cond_0
    iget-object v1, p1, Lcom/google/android/location/places/PlaceSubscription;->c:Landroid/app/PendingIntent;

    iget-object v2, p0, Lcom/google/android/location/places/an;->i:Landroid/content/pm/PackageManager;

    invoke-static {v1, v2}, Lcom/google/android/location/n/ag;->a(Landroid/app/PendingIntent;Landroid/content/pm/PackageManager;)I

    move-result v1

    .line 201
    if-ne v1, v8, :cond_1

    move v4, v3

    .line 204
    :cond_1
    new-instance v2, Lcom/google/android/gms/location/internal/ClientIdentity;

    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v1

    iget-object v5, p1, Lcom/google/android/location/places/PlaceSubscription;->c:Landroid/app/PendingIntent;

    invoke-virtual {v5}, Landroid/app/PendingIntent;->getTargetPackage()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v2, v1, v5}, Lcom/google/android/gms/location/internal/ClientIdentity;-><init>(ILjava/lang/String;)V

    .line 208
    iget-object v1, p1, Lcom/google/android/location/places/PlaceSubscription;->a:Lcom/google/android/gms/location/places/PlaceRequest;

    new-instance v5, Lcom/google/android/gms/location/LocationRequest;

    invoke-direct {v5}, Lcom/google/android/gms/location/LocationRequest;-><init>()V

    invoke-virtual {v1}, Lcom/google/android/gms/location/places/PlaceRequest;->c()I

    move-result v6

    packed-switch v6, :pswitch_data_0

    :goto_0
    :pswitch_0
    invoke-virtual {v5, v0}, Lcom/google/android/gms/location/LocationRequest;->a(I)Lcom/google/android/gms/location/LocationRequest;

    move-result-object v0

    invoke-virtual {v1}, Lcom/google/android/gms/location/places/PlaceRequest;->b()J

    move-result-wide v6

    invoke-virtual {v0, v6, v7}, Lcom/google/android/gms/location/LocationRequest;->a(J)Lcom/google/android/gms/location/LocationRequest;

    move-result-object v1

    .line 209
    const-string v0, "Places"

    const/4 v5, 0x3

    invoke-static {v0, v5}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 210
    const-string v0, "Places"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Location request priority: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Lcom/google/android/gms/location/LocationRequest;->b()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v0, v5}, Lcom/google/android/location/n/aa;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 211
    const-string v0, "Places"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Location request interval: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Lcom/google/android/gms/location/LocationRequest;->c()J

    move-result-wide v6

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v0, v5}, Lcom/google/android/location/n/aa;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 212
    const-string v0, "Places"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Location request FINE permission: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v0, v5}, Lcom/google/android/location/n/aa;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 214
    :cond_2
    invoke-static {v2}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v5

    const-string v0, "Places"

    invoke-static {v0, v8}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_3

    const-string v0, "Places"

    const-string v2, "Request location updates"

    invoke-static {v0, v2}, Lcom/google/android/location/n/aa;->d(Ljava/lang/String;Ljava/lang/String;)V

    :cond_3
    iget-object v0, p0, Lcom/google/android/location/places/an;->f:Lcom/google/android/location/fused/g;

    iget-object v2, p0, Lcom/google/android/location/places/an;->n:Lcom/google/android/gms/location/k;

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/location/fused/g;->a(Lcom/google/android/gms/location/LocationRequest;Lcom/google/android/gms/location/j;ZZLjava/util/Collection;)V

    .line 216
    invoke-direct {p0}, Lcom/google/android/location/places/an;->a()V

    .line 217
    return-void

    .line 208
    :pswitch_1
    const/16 v0, 0x68

    goto :goto_0

    :pswitch_2
    const/16 v0, 0x66

    goto :goto_0

    :pswitch_3
    const/16 v0, 0x64

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x64
        :pswitch_3
        :pswitch_0
        :pswitch_2
        :pswitch_0
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public final a(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 164
    iget-object v1, p0, Lcom/google/android/location/places/an;->c:Ljava/lang/Object;

    monitor-enter v1

    .line 165
    :try_start_0
    iget-object v0, p0, Lcom/google/android/location/places/an;->d:Lcom/google/android/location/places/bx;

    invoke-virtual {v0, p1}, Lcom/google/android/location/places/bx;->a(Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    .line 167
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/places/PlaceSubscription;

    .line 168
    invoke-virtual {p0, v0}, Lcom/google/android/location/places/an;->b(Lcom/google/android/location/places/PlaceSubscription;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 170
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    :cond_0
    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void
.end method

.method public final b(Lcom/google/android/location/places/PlaceSubscription;)V
    .locals 3

    .prologue
    .line 365
    iget-object v1, p0, Lcom/google/android/location/places/an;->c:Ljava/lang/Object;

    monitor-enter v1

    .line 366
    :try_start_0
    iget-object v0, p0, Lcom/google/android/location/places/an;->d:Lcom/google/android/location/places/bx;

    invoke-virtual {v0, p1}, Lcom/google/android/location/places/bx;->b(Lcom/google/android/location/places/Subscription;)Lcom/google/android/location/places/Subscription;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/places/PlaceSubscription;

    .line 367
    if-eqz v0, :cond_0

    .line 368
    iget-object v2, p0, Lcom/google/android/location/places/an;->m:Ljava/util/Map;

    invoke-interface {v2, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 369
    invoke-virtual {v0}, Lcom/google/android/location/places/PlaceSubscription;->f()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 370
    iget-object v2, p0, Lcom/google/android/location/places/an;->j:Lcom/google/android/location/places/d/e;

    invoke-virtual {v2, v0}, Lcom/google/android/location/places/d/e;->c(Lcom/google/android/location/places/Subscription;)V

    .line 373
    :cond_0
    iget-object v0, p0, Lcom/google/android/location/places/an;->d:Lcom/google/android/location/places/bx;

    iget-boolean v0, v0, Lcom/google/android/location/places/bx;->a:Z

    if-nez v0, :cond_1

    .line 374
    monitor-exit v1

    .line 377
    :goto_0
    return-void

    .line 376
    :cond_1
    invoke-direct {p0}, Lcom/google/android/location/places/an;->a()V

    .line 377
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method
