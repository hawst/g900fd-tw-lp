.class final Lcom/google/android/location/copresence/r/s;
.super Landroid/content/BroadcastReceiver;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/location/copresence/r/r;


# direct methods
.method constructor <init>(Lcom/google/android/location/copresence/r/r;)V
    .locals 0

    .prologue
    .line 140
    iput-object p1, p0, Lcom/google/android/location/copresence/r/s;->a:Lcom/google/android/location/copresence/r/r;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public final onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 4

    .prologue
    const/4 v3, 0x3

    .line 143
    if-nez p2, :cond_2

    const/4 v0, 0x0

    .line 144
    :goto_0
    const-string v1, "android.net.wifi.p2p.THIS_DEVICE_CHANGED"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 145
    const-string v0, "wifiP2pDevice"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/wifi/p2p/WifiP2pDevice;

    .line 147
    if-eqz v0, :cond_1

    .line 148
    iget-object v1, p0, Lcom/google/android/location/copresence/r/s;->a:Lcom/google/android/location/copresence/r/r;

    invoke-virtual {v1}, Lcom/google/android/location/copresence/r/r;->b()Z

    move-result v1

    if-nez v1, :cond_0

    .line 149
    iget-object v1, p0, Lcom/google/android/location/copresence/r/s;->a:Lcom/google/android/location/copresence/r/r;

    iget-object v1, v1, Lcom/google/android/location/copresence/r/r;->a:Lcom/google/android/location/copresence/r/p;

    iget-object v1, p0, Lcom/google/android/location/copresence/r/s;->a:Lcom/google/android/location/copresence/r/r;

    iget-object v1, v1, Lcom/google/android/location/copresence/r/r;->a:Lcom/google/android/location/copresence/r/p;

    invoke-static {v1}, Lcom/google/android/location/copresence/r/p;->e(Lcom/google/android/location/copresence/r/p;)Landroid/content/SharedPreferences;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v2, "deviceName"

    iget-object v0, v0, Landroid/net/wifi/p2p/WifiP2pDevice;->deviceName:Ljava/lang/String;

    invoke-interface {v1, v2, v0}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "save wifi direct name"

    invoke-static {v0, v1}, Lcom/google/android/location/copresence/r/p;->a(Landroid/content/SharedPreferences$Editor;Ljava/lang/String;)Z

    move-result v0

    .line 152
    if-eqz v0, :cond_3

    .line 153
    invoke-static {v3}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 154
    const-string v0, "WifiDirectStates2: Saved user name."

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->b(Ljava/lang/String;)I

    .line 163
    :cond_0
    :goto_1
    :try_start_0
    iget-object v0, p0, Lcom/google/android/location/copresence/r/s;->a:Lcom/google/android/location/copresence/r/r;

    iget-object v0, v0, Lcom/google/android/location/copresence/r/r;->a:Lcom/google/android/location/copresence/r/p;

    invoke-static {v0}, Lcom/google/android/location/copresence/r/p;->f(Lcom/google/android/location/copresence/r/p;)Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 167
    :goto_2
    iget-object v0, p0, Lcom/google/android/location/copresence/r/s;->a:Lcom/google/android/location/copresence/r/r;

    iget-object v0, v0, Lcom/google/android/location/copresence/r/r;->a:Lcom/google/android/location/copresence/r/p;

    invoke-static {v0}, Lcom/google/android/location/copresence/r/p;->g(Lcom/google/android/location/copresence/r/p;)Lcom/google/android/location/copresence/k/a;

    move-result-object v0

    const/4 v1, 0x0

    new-array v1, v1, [Lcom/google/android/location/copresence/k/f;

    invoke-virtual {v0, v1}, Lcom/google/android/location/copresence/k/a;->b([Lcom/google/android/location/copresence/k/f;)V

    .line 170
    :cond_1
    return-void

    .line 143
    :cond_2
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 157
    :cond_3
    invoke-static {v3}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 158
    const-string v0, "WifiDirectStates2: Failed to save user name."

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->b(Ljava/lang/String;)I

    goto :goto_1

    :catch_0
    move-exception v0

    goto :goto_2
.end method
