.class final Lcom/google/android/location/b/h;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lcom/google/android/location/e/ax;

.field final synthetic b:Ljava/lang/Object;

.field final synthetic c:Lcom/google/android/location/e/ax;

.field final synthetic d:Ljava/lang/Object;

.field final synthetic e:Lcom/google/android/location/b/f;


# direct methods
.method constructor <init>(Lcom/google/android/location/b/f;Lcom/google/android/location/e/ax;Ljava/lang/Object;Lcom/google/android/location/e/ax;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 418
    iput-object p1, p0, Lcom/google/android/location/b/h;->e:Lcom/google/android/location/b/f;

    iput-object p2, p0, Lcom/google/android/location/b/h;->a:Lcom/google/android/location/e/ax;

    iput-object p3, p0, Lcom/google/android/location/b/h;->b:Ljava/lang/Object;

    iput-object p4, p0, Lcom/google/android/location/b/h;->c:Lcom/google/android/location/e/ax;

    iput-object p5, p0, Lcom/google/android/location/b/h;->d:Ljava/lang/Object;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 4

    .prologue
    .line 423
    iget-object v0, p0, Lcom/google/android/location/b/h;->a:Lcom/google/android/location/e/ax;

    if-eqz v0, :cond_0

    .line 424
    new-instance v1, Ljava/io/File;

    iget-object v0, p0, Lcom/google/android/location/b/h;->e:Lcom/google/android/location/b/f;

    iget-object v2, v0, Lcom/google/android/location/b/f;->a:Ljava/io/File;

    iget-object v0, p0, Lcom/google/android/location/b/h;->a:Lcom/google/android/location/e/ax;

    iget-object v0, v0, Lcom/google/android/location/e/ax;->a:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    invoke-direct {v1, v2, v0}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 425
    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    .line 428
    :cond_0
    iget-object v0, p0, Lcom/google/android/location/b/h;->b:Ljava/lang/Object;

    if-nez v0, :cond_1

    .line 462
    :goto_0
    return-void

    .line 433
    :cond_1
    iget-object v0, p0, Lcom/google/android/location/b/h;->e:Lcom/google/android/location/b/f;

    iget-object v0, v0, Lcom/google/android/location/b/f;->a:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    if-nez v0, :cond_2

    .line 434
    iget-object v0, p0, Lcom/google/android/location/b/h;->e:Lcom/google/android/location/b/f;

    iget-object v0, v0, Lcom/google/android/location/b/f;->a:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    .line 440
    :cond_2
    new-instance v1, Ljava/io/File;

    iget-object v0, p0, Lcom/google/android/location/b/h;->e:Lcom/google/android/location/b/f;

    iget-object v2, v0, Lcom/google/android/location/b/f;->a:Ljava/io/File;

    iget-object v0, p0, Lcom/google/android/location/b/h;->c:Lcom/google/android/location/e/ax;

    iget-object v0, v0, Lcom/google/android/location/e/ax;->a:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    invoke-direct {v1, v2, v0}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 443
    :try_start_0
    invoke-virtual {v1}, Ljava/io/File;->createNewFile()Z

    .line 444
    iget-object v0, p0, Lcom/google/android/location/b/h;->e:Lcom/google/android/location/b/f;

    iget-object v0, v0, Lcom/google/android/location/b/f;->c:Lcom/google/android/location/j/e;

    invoke-interface {v0, v1}, Lcom/google/android/location/j/e;->a(Ljava/io/File;)V

    .line 445
    new-instance v0, Lcom/google/android/location/b/ar;

    iget-object v2, p0, Lcom/google/android/location/b/h;->e:Lcom/google/android/location/b/f;

    iget-short v2, v2, Lcom/google/android/location/b/f;->d:S

    iget-object v3, p0, Lcom/google/android/location/b/h;->e:Lcom/google/android/location/b/f;

    iget-object v3, v3, Lcom/google/android/location/b/f;->b:Lcom/google/android/location/e/a;

    invoke-direct {v0, v2, v1, v3}, Lcom/google/android/location/b/ar;-><init>(ILjava/io/File;Lcom/google/android/location/e/a;)V

    .line 446
    iget-object v2, p0, Lcom/google/android/location/b/h;->e:Lcom/google/android/location/b/f;

    iget-object v2, v2, Lcom/google/android/location/b/f;->g:Lcom/google/android/location/b/i;

    iget-object v3, p0, Lcom/google/android/location/b/h;->b:Ljava/lang/Object;

    invoke-interface {v2, v3}, Lcom/google/android/location/b/i;->a(Ljava/lang/Object;)[B

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/android/location/b/ar;->a([B)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    .line 450
    :catch_0
    move-exception v0

    monitor-enter p0

    .line 451
    :try_start_1
    iget-object v0, p0, Lcom/google/android/location/b/h;->e:Lcom/google/android/location/b/f;

    iget-object v0, v0, Lcom/google/android/location/b/f;->e:Lcom/google/android/location/b/bd;

    iget-object v1, p0, Lcom/google/android/location/b/h;->d:Ljava/lang/Object;

    invoke-virtual {v0, v1}, Lcom/google/android/location/b/bd;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 452
    iget-object v0, p0, Lcom/google/android/location/b/h;->e:Lcom/google/android/location/b/f;

    iget-object v0, v0, Lcom/google/android/location/b/f;->f:Lcom/google/android/location/b/j;

    iget-object v1, p0, Lcom/google/android/location/b/h;->d:Ljava/lang/Object;

    invoke-virtual {v0, v1}, Lcom/google/android/location/b/j;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 453
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 457
    :catch_1
    move-exception v0

    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    .line 458
    monitor-enter p0

    .line 459
    :try_start_2
    iget-object v0, p0, Lcom/google/android/location/b/h;->e:Lcom/google/android/location/b/f;

    iget-object v0, v0, Lcom/google/android/location/b/f;->e:Lcom/google/android/location/b/bd;

    iget-object v1, p0, Lcom/google/android/location/b/h;->d:Ljava/lang/Object;

    invoke-virtual {v0, v1}, Lcom/google/android/location/b/bd;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 460
    iget-object v0, p0, Lcom/google/android/location/b/h;->e:Lcom/google/android/location/b/f;

    iget-object v0, v0, Lcom/google/android/location/b/f;->f:Lcom/google/android/location/b/j;

    iget-object v1, p0, Lcom/google/android/location/b/h;->d:Ljava/lang/Object;

    invoke-virtual {v0, v1}, Lcom/google/android/location/b/j;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 461
    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    goto :goto_0

    :catchall_1
    move-exception v0

    monitor-exit p0

    throw v0
.end method
