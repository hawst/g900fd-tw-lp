.class public final Lcom/google/android/location/l/a/am;
.super Lcom/google/protobuf/nano/d;
.source "SourceFile"


# static fields
.field private static volatile w:[Lcom/google/android/location/l/a/am;


# instance fields
.field public a:Lcom/google/android/location/l/a/ag;

.field public b:Ljava/lang/String;

.field public c:Ljava/lang/Integer;

.field public d:Ljava/lang/Integer;

.field public e:[Lcom/google/android/location/l/a/t;

.field public f:Ljava/lang/Long;

.field public g:Ljava/lang/Boolean;

.field public h:Ljava/lang/Integer;

.field public i:Ljava/lang/String;

.field public j:Ljava/lang/Integer;

.field public k:Ljava/lang/Integer;

.field public l:Ljava/lang/Integer;

.field public m:Ljava/lang/Integer;

.field public n:Lcom/google/android/location/l/a/x;

.field public o:Ljava/lang/String;

.field public p:Ljava/lang/Float;

.field public q:Ljava/lang/Boolean;

.field public r:Ljava/lang/String;

.field public s:Lcom/google/android/location/l/a/ae;

.field public t:Ljava/lang/Integer;

.field public u:[Lcom/google/android/location/l/a/ay;

.field public v:Ljava/lang/Integer;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 106
    invoke-direct {p0}, Lcom/google/protobuf/nano/d;-><init>()V

    .line 107
    iput-object v1, p0, Lcom/google/android/location/l/a/am;->a:Lcom/google/android/location/l/a/ag;

    iput-object v1, p0, Lcom/google/android/location/l/a/am;->b:Ljava/lang/String;

    iput-object v1, p0, Lcom/google/android/location/l/a/am;->c:Ljava/lang/Integer;

    iput-object v1, p0, Lcom/google/android/location/l/a/am;->d:Ljava/lang/Integer;

    invoke-static {}, Lcom/google/android/location/l/a/t;->a()[Lcom/google/android/location/l/a/t;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/l/a/am;->e:[Lcom/google/android/location/l/a/t;

    iput-object v1, p0, Lcom/google/android/location/l/a/am;->f:Ljava/lang/Long;

    iput-object v1, p0, Lcom/google/android/location/l/a/am;->g:Ljava/lang/Boolean;

    iput-object v1, p0, Lcom/google/android/location/l/a/am;->h:Ljava/lang/Integer;

    iput-object v1, p0, Lcom/google/android/location/l/a/am;->i:Ljava/lang/String;

    iput-object v1, p0, Lcom/google/android/location/l/a/am;->j:Ljava/lang/Integer;

    iput-object v1, p0, Lcom/google/android/location/l/a/am;->k:Ljava/lang/Integer;

    iput-object v1, p0, Lcom/google/android/location/l/a/am;->l:Ljava/lang/Integer;

    iput-object v1, p0, Lcom/google/android/location/l/a/am;->m:Ljava/lang/Integer;

    iput-object v1, p0, Lcom/google/android/location/l/a/am;->n:Lcom/google/android/location/l/a/x;

    iput-object v1, p0, Lcom/google/android/location/l/a/am;->o:Ljava/lang/String;

    iput-object v1, p0, Lcom/google/android/location/l/a/am;->p:Ljava/lang/Float;

    iput-object v1, p0, Lcom/google/android/location/l/a/am;->q:Ljava/lang/Boolean;

    iput-object v1, p0, Lcom/google/android/location/l/a/am;->r:Ljava/lang/String;

    iput-object v1, p0, Lcom/google/android/location/l/a/am;->s:Lcom/google/android/location/l/a/ae;

    iput-object v1, p0, Lcom/google/android/location/l/a/am;->t:Ljava/lang/Integer;

    invoke-static {}, Lcom/google/android/location/l/a/ay;->a()[Lcom/google/android/location/l/a/ay;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/l/a/am;->u:[Lcom/google/android/location/l/a/ay;

    iput-object v1, p0, Lcom/google/android/location/l/a/am;->v:Ljava/lang/Integer;

    iput-object v1, p0, Lcom/google/android/location/l/a/am;->unknownFieldData:Lcom/google/protobuf/nano/f;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/location/l/a/am;->cachedSize:I

    .line 108
    return-void
.end method

.method public static a()[Lcom/google/android/location/l/a/am;
    .locals 2

    .prologue
    .line 29
    sget-object v0, Lcom/google/android/location/l/a/am;->w:[Lcom/google/android/location/l/a/am;

    if-nez v0, :cond_1

    .line 30
    sget-object v1, Lcom/google/protobuf/nano/h;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 32
    :try_start_0
    sget-object v0, Lcom/google/android/location/l/a/am;->w:[Lcom/google/android/location/l/a/am;

    if-nez v0, :cond_0

    .line 33
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/android/location/l/a/am;

    sput-object v0, Lcom/google/android/location/l/a/am;->w:[Lcom/google/android/location/l/a/am;

    .line 35
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 37
    :cond_1
    sget-object v0, Lcom/google/android/location/l/a/am;->w:[Lcom/google/android/location/l/a/am;

    return-object v0

    .line 35
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 437
    invoke-super {p0}, Lcom/google/protobuf/nano/d;->computeSerializedSize()I

    move-result v0

    .line 438
    iget-object v2, p0, Lcom/google/android/location/l/a/am;->a:Lcom/google/android/location/l/a/ag;

    if-eqz v2, :cond_0

    .line 439
    const/4 v2, 0x1

    iget-object v3, p0, Lcom/google/android/location/l/a/am;->a:Lcom/google/android/location/l/a/ag;

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v2

    add-int/2addr v0, v2

    .line 442
    :cond_0
    iget-object v2, p0, Lcom/google/android/location/l/a/am;->b:Ljava/lang/String;

    if-eqz v2, :cond_1

    .line 443
    const/4 v2, 0x2

    iget-object v3, p0, Lcom/google/android/location/l/a/am;->b:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 446
    :cond_1
    iget-object v2, p0, Lcom/google/android/location/l/a/am;->c:Ljava/lang/Integer;

    if-eqz v2, :cond_2

    .line 447
    const/4 v2, 0x3

    iget-object v3, p0, Lcom/google/android/location/l/a/am;->c:Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 450
    :cond_2
    iget-object v2, p0, Lcom/google/android/location/l/a/am;->d:Ljava/lang/Integer;

    if-eqz v2, :cond_3

    .line 451
    const/4 v2, 0x4

    iget-object v3, p0, Lcom/google/android/location/l/a/am;->d:Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 454
    :cond_3
    iget-object v2, p0, Lcom/google/android/location/l/a/am;->e:[Lcom/google/android/location/l/a/t;

    if-eqz v2, :cond_6

    iget-object v2, p0, Lcom/google/android/location/l/a/am;->e:[Lcom/google/android/location/l/a/t;

    array-length v2, v2

    if-lez v2, :cond_6

    move v2, v0

    move v0, v1

    .line 455
    :goto_0
    iget-object v3, p0, Lcom/google/android/location/l/a/am;->e:[Lcom/google/android/location/l/a/t;

    array-length v3, v3

    if-ge v0, v3, :cond_5

    .line 456
    iget-object v3, p0, Lcom/google/android/location/l/a/am;->e:[Lcom/google/android/location/l/a/t;

    aget-object v3, v3, v0

    .line 457
    if-eqz v3, :cond_4

    .line 458
    const/4 v4, 0x5

    invoke-static {v4, v3}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v3

    add-int/2addr v2, v3

    .line 455
    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_5
    move v0, v2

    .line 463
    :cond_6
    iget-object v2, p0, Lcom/google/android/location/l/a/am;->f:Ljava/lang/Long;

    if-eqz v2, :cond_7

    .line 464
    const/4 v2, 0x6

    iget-object v3, p0, Lcom/google/android/location/l/a/am;->f:Ljava/lang/Long;

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-static {v2, v4, v5}, Lcom/google/protobuf/nano/b;->f(IJ)I

    move-result v2

    add-int/2addr v0, v2

    .line 467
    :cond_7
    iget-object v2, p0, Lcom/google/android/location/l/a/am;->g:Ljava/lang/Boolean;

    if-eqz v2, :cond_8

    .line 468
    const/4 v2, 0x7

    iget-object v3, p0, Lcom/google/android/location/l/a/am;->g:Ljava/lang/Boolean;

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v2}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 471
    :cond_8
    iget-object v2, p0, Lcom/google/android/location/l/a/am;->h:Ljava/lang/Integer;

    if-eqz v2, :cond_9

    .line 472
    const/16 v2, 0x8

    iget-object v3, p0, Lcom/google/android/location/l/a/am;->h:Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 475
    :cond_9
    iget-object v2, p0, Lcom/google/android/location/l/a/am;->i:Ljava/lang/String;

    if-eqz v2, :cond_a

    .line 476
    const/16 v2, 0x9

    iget-object v3, p0, Lcom/google/android/location/l/a/am;->i:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 479
    :cond_a
    iget-object v2, p0, Lcom/google/android/location/l/a/am;->j:Ljava/lang/Integer;

    if-eqz v2, :cond_b

    .line 480
    const/16 v2, 0xa

    iget-object v3, p0, Lcom/google/android/location/l/a/am;->j:Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 483
    :cond_b
    iget-object v2, p0, Lcom/google/android/location/l/a/am;->k:Ljava/lang/Integer;

    if-eqz v2, :cond_c

    .line 484
    const/16 v2, 0xb

    iget-object v3, p0, Lcom/google/android/location/l/a/am;->k:Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 487
    :cond_c
    iget-object v2, p0, Lcom/google/android/location/l/a/am;->l:Ljava/lang/Integer;

    if-eqz v2, :cond_d

    .line 488
    const/16 v2, 0xc

    iget-object v3, p0, Lcom/google/android/location/l/a/am;->l:Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 491
    :cond_d
    iget-object v2, p0, Lcom/google/android/location/l/a/am;->m:Ljava/lang/Integer;

    if-eqz v2, :cond_e

    .line 492
    const/16 v2, 0xd

    iget-object v3, p0, Lcom/google/android/location/l/a/am;->m:Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 495
    :cond_e
    iget-object v2, p0, Lcom/google/android/location/l/a/am;->n:Lcom/google/android/location/l/a/x;

    if-eqz v2, :cond_f

    .line 496
    const/16 v2, 0xe

    iget-object v3, p0, Lcom/google/android/location/l/a/am;->n:Lcom/google/android/location/l/a/x;

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v2

    add-int/2addr v0, v2

    .line 499
    :cond_f
    iget-object v2, p0, Lcom/google/android/location/l/a/am;->o:Ljava/lang/String;

    if-eqz v2, :cond_10

    .line 500
    const/16 v2, 0xf

    iget-object v3, p0, Lcom/google/android/location/l/a/am;->o:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 503
    :cond_10
    iget-object v2, p0, Lcom/google/android/location/l/a/am;->p:Ljava/lang/Float;

    if-eqz v2, :cond_11

    .line 504
    const/16 v2, 0x10

    iget-object v3, p0, Lcom/google/android/location/l/a/am;->p:Ljava/lang/Float;

    invoke-virtual {v3}, Ljava/lang/Float;->floatValue()F

    invoke-static {v2}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x4

    add-int/2addr v0, v2

    .line 507
    :cond_11
    iget-object v2, p0, Lcom/google/android/location/l/a/am;->q:Ljava/lang/Boolean;

    if-eqz v2, :cond_12

    .line 508
    const/16 v2, 0x11

    iget-object v3, p0, Lcom/google/android/location/l/a/am;->q:Ljava/lang/Boolean;

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v2}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 511
    :cond_12
    iget-object v2, p0, Lcom/google/android/location/l/a/am;->r:Ljava/lang/String;

    if-eqz v2, :cond_13

    .line 512
    const/16 v2, 0x12

    iget-object v3, p0, Lcom/google/android/location/l/a/am;->r:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 515
    :cond_13
    iget-object v2, p0, Lcom/google/android/location/l/a/am;->s:Lcom/google/android/location/l/a/ae;

    if-eqz v2, :cond_14

    .line 516
    const/16 v2, 0x13

    iget-object v3, p0, Lcom/google/android/location/l/a/am;->s:Lcom/google/android/location/l/a/ae;

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v2

    add-int/2addr v0, v2

    .line 519
    :cond_14
    iget-object v2, p0, Lcom/google/android/location/l/a/am;->t:Ljava/lang/Integer;

    if-eqz v2, :cond_15

    .line 520
    const/16 v2, 0x14

    iget-object v3, p0, Lcom/google/android/location/l/a/am;->t:Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 523
    :cond_15
    iget-object v2, p0, Lcom/google/android/location/l/a/am;->u:[Lcom/google/android/location/l/a/ay;

    if-eqz v2, :cond_17

    iget-object v2, p0, Lcom/google/android/location/l/a/am;->u:[Lcom/google/android/location/l/a/ay;

    array-length v2, v2

    if-lez v2, :cond_17

    .line 524
    :goto_1
    iget-object v2, p0, Lcom/google/android/location/l/a/am;->u:[Lcom/google/android/location/l/a/ay;

    array-length v2, v2

    if-ge v1, v2, :cond_17

    .line 525
    iget-object v2, p0, Lcom/google/android/location/l/a/am;->u:[Lcom/google/android/location/l/a/ay;

    aget-object v2, v2, v1

    .line 526
    if-eqz v2, :cond_16

    .line 527
    const/16 v3, 0x15

    invoke-static {v3, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v2

    add-int/2addr v0, v2

    .line 524
    :cond_16
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 532
    :cond_17
    iget-object v1, p0, Lcom/google/android/location/l/a/am;->v:Ljava/lang/Integer;

    if-eqz v1, :cond_18

    .line 533
    const/16 v1, 0x16

    iget-object v2, p0, Lcom/google/android/location/l/a/am;->v:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 536
    :cond_18
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 140
    if-ne p1, p0, :cond_1

    .line 141
    const/4 v0, 0x1

    .line 300
    :cond_0
    :goto_0
    return v0

    .line 143
    :cond_1
    instance-of v1, p1, Lcom/google/android/location/l/a/am;

    if-eqz v1, :cond_0

    .line 146
    check-cast p1, Lcom/google/android/location/l/a/am;

    .line 147
    iget-object v1, p0, Lcom/google/android/location/l/a/am;->a:Lcom/google/android/location/l/a/ag;

    if-nez v1, :cond_16

    .line 148
    iget-object v1, p1, Lcom/google/android/location/l/a/am;->a:Lcom/google/android/location/l/a/ag;

    if-nez v1, :cond_0

    .line 156
    :cond_2
    iget-object v1, p0, Lcom/google/android/location/l/a/am;->b:Ljava/lang/String;

    if-nez v1, :cond_17

    .line 157
    iget-object v1, p1, Lcom/google/android/location/l/a/am;->b:Ljava/lang/String;

    if-nez v1, :cond_0

    .line 163
    :cond_3
    iget-object v1, p0, Lcom/google/android/location/l/a/am;->c:Ljava/lang/Integer;

    if-nez v1, :cond_18

    .line 164
    iget-object v1, p1, Lcom/google/android/location/l/a/am;->c:Ljava/lang/Integer;

    if-nez v1, :cond_0

    .line 170
    :cond_4
    iget-object v1, p0, Lcom/google/android/location/l/a/am;->d:Ljava/lang/Integer;

    if-nez v1, :cond_19

    .line 171
    iget-object v1, p1, Lcom/google/android/location/l/a/am;->d:Ljava/lang/Integer;

    if-nez v1, :cond_0

    .line 177
    :cond_5
    iget-object v1, p0, Lcom/google/android/location/l/a/am;->e:[Lcom/google/android/location/l/a/t;

    iget-object v2, p1, Lcom/google/android/location/l/a/am;->e:[Lcom/google/android/location/l/a/t;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 181
    iget-object v1, p0, Lcom/google/android/location/l/a/am;->f:Ljava/lang/Long;

    if-nez v1, :cond_1a

    .line 182
    iget-object v1, p1, Lcom/google/android/location/l/a/am;->f:Ljava/lang/Long;

    if-nez v1, :cond_0

    .line 188
    :cond_6
    iget-object v1, p0, Lcom/google/android/location/l/a/am;->g:Ljava/lang/Boolean;

    if-nez v1, :cond_1b

    .line 189
    iget-object v1, p1, Lcom/google/android/location/l/a/am;->g:Ljava/lang/Boolean;

    if-nez v1, :cond_0

    .line 195
    :cond_7
    iget-object v1, p0, Lcom/google/android/location/l/a/am;->h:Ljava/lang/Integer;

    if-nez v1, :cond_1c

    .line 196
    iget-object v1, p1, Lcom/google/android/location/l/a/am;->h:Ljava/lang/Integer;

    if-nez v1, :cond_0

    .line 201
    :cond_8
    iget-object v1, p0, Lcom/google/android/location/l/a/am;->i:Ljava/lang/String;

    if-nez v1, :cond_1d

    .line 202
    iget-object v1, p1, Lcom/google/android/location/l/a/am;->i:Ljava/lang/String;

    if-nez v1, :cond_0

    .line 208
    :cond_9
    iget-object v1, p0, Lcom/google/android/location/l/a/am;->j:Ljava/lang/Integer;

    if-nez v1, :cond_1e

    .line 209
    iget-object v1, p1, Lcom/google/android/location/l/a/am;->j:Ljava/lang/Integer;

    if-nez v1, :cond_0

    .line 215
    :cond_a
    iget-object v1, p0, Lcom/google/android/location/l/a/am;->k:Ljava/lang/Integer;

    if-nez v1, :cond_1f

    .line 216
    iget-object v1, p1, Lcom/google/android/location/l/a/am;->k:Ljava/lang/Integer;

    if-nez v1, :cond_0

    .line 222
    :cond_b
    iget-object v1, p0, Lcom/google/android/location/l/a/am;->l:Ljava/lang/Integer;

    if-nez v1, :cond_20

    .line 223
    iget-object v1, p1, Lcom/google/android/location/l/a/am;->l:Ljava/lang/Integer;

    if-nez v1, :cond_0

    .line 229
    :cond_c
    iget-object v1, p0, Lcom/google/android/location/l/a/am;->m:Ljava/lang/Integer;

    if-nez v1, :cond_21

    .line 230
    iget-object v1, p1, Lcom/google/android/location/l/a/am;->m:Ljava/lang/Integer;

    if-nez v1, :cond_0

    .line 236
    :cond_d
    iget-object v1, p0, Lcom/google/android/location/l/a/am;->n:Lcom/google/android/location/l/a/x;

    if-nez v1, :cond_22

    .line 237
    iget-object v1, p1, Lcom/google/android/location/l/a/am;->n:Lcom/google/android/location/l/a/x;

    if-nez v1, :cond_0

    .line 245
    :cond_e
    iget-object v1, p0, Lcom/google/android/location/l/a/am;->o:Ljava/lang/String;

    if-nez v1, :cond_23

    .line 246
    iget-object v1, p1, Lcom/google/android/location/l/a/am;->o:Ljava/lang/String;

    if-nez v1, :cond_0

    .line 252
    :cond_f
    iget-object v1, p0, Lcom/google/android/location/l/a/am;->p:Ljava/lang/Float;

    if-nez v1, :cond_24

    .line 253
    iget-object v1, p1, Lcom/google/android/location/l/a/am;->p:Ljava/lang/Float;

    if-nez v1, :cond_0

    .line 259
    :cond_10
    iget-object v1, p0, Lcom/google/android/location/l/a/am;->q:Ljava/lang/Boolean;

    if-nez v1, :cond_25

    .line 260
    iget-object v1, p1, Lcom/google/android/location/l/a/am;->q:Ljava/lang/Boolean;

    if-nez v1, :cond_0

    .line 266
    :cond_11
    iget-object v1, p0, Lcom/google/android/location/l/a/am;->r:Ljava/lang/String;

    if-nez v1, :cond_26

    .line 267
    iget-object v1, p1, Lcom/google/android/location/l/a/am;->r:Ljava/lang/String;

    if-nez v1, :cond_0

    .line 273
    :cond_12
    iget-object v1, p0, Lcom/google/android/location/l/a/am;->s:Lcom/google/android/location/l/a/ae;

    if-nez v1, :cond_27

    .line 274
    iget-object v1, p1, Lcom/google/android/location/l/a/am;->s:Lcom/google/android/location/l/a/ae;

    if-nez v1, :cond_0

    .line 282
    :cond_13
    iget-object v1, p0, Lcom/google/android/location/l/a/am;->t:Ljava/lang/Integer;

    if-nez v1, :cond_28

    .line 283
    iget-object v1, p1, Lcom/google/android/location/l/a/am;->t:Ljava/lang/Integer;

    if-nez v1, :cond_0

    .line 289
    :cond_14
    iget-object v1, p0, Lcom/google/android/location/l/a/am;->u:[Lcom/google/android/location/l/a/ay;

    iget-object v2, p1, Lcom/google/android/location/l/a/am;->u:[Lcom/google/android/location/l/a/ay;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 293
    iget-object v1, p0, Lcom/google/android/location/l/a/am;->v:Ljava/lang/Integer;

    if-nez v1, :cond_29

    .line 294
    iget-object v1, p1, Lcom/google/android/location/l/a/am;->v:Ljava/lang/Integer;

    if-nez v1, :cond_0

    .line 300
    :cond_15
    invoke-virtual {p0, p1}, Lcom/google/android/location/l/a/am;->unknownFieldDataEquals(Lcom/google/protobuf/nano/d;)Z

    move-result v0

    goto/16 :goto_0

    .line 152
    :cond_16
    iget-object v1, p0, Lcom/google/android/location/l/a/am;->a:Lcom/google/android/location/l/a/ag;

    iget-object v2, p1, Lcom/google/android/location/l/a/am;->a:Lcom/google/android/location/l/a/ag;

    invoke-virtual {v1, v2}, Lcom/google/android/location/l/a/ag;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    goto/16 :goto_0

    .line 160
    :cond_17
    iget-object v1, p0, Lcom/google/android/location/l/a/am;->b:Ljava/lang/String;

    iget-object v2, p1, Lcom/google/android/location/l/a/am;->b:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    goto/16 :goto_0

    .line 167
    :cond_18
    iget-object v1, p0, Lcom/google/android/location/l/a/am;->c:Ljava/lang/Integer;

    iget-object v2, p1, Lcom/google/android/location/l/a/am;->c:Ljava/lang/Integer;

    invoke-virtual {v1, v2}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    goto/16 :goto_0

    .line 174
    :cond_19
    iget-object v1, p0, Lcom/google/android/location/l/a/am;->d:Ljava/lang/Integer;

    iget-object v2, p1, Lcom/google/android/location/l/a/am;->d:Ljava/lang/Integer;

    invoke-virtual {v1, v2}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_5

    goto/16 :goto_0

    .line 185
    :cond_1a
    iget-object v1, p0, Lcom/google/android/location/l/a/am;->f:Ljava/lang/Long;

    iget-object v2, p1, Lcom/google/android/location/l/a/am;->f:Ljava/lang/Long;

    invoke-virtual {v1, v2}, Ljava/lang/Long;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_6

    goto/16 :goto_0

    .line 192
    :cond_1b
    iget-object v1, p0, Lcom/google/android/location/l/a/am;->g:Ljava/lang/Boolean;

    iget-object v2, p1, Lcom/google/android/location/l/a/am;->g:Ljava/lang/Boolean;

    invoke-virtual {v1, v2}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_7

    goto/16 :goto_0

    .line 199
    :cond_1c
    iget-object v1, p0, Lcom/google/android/location/l/a/am;->h:Ljava/lang/Integer;

    iget-object v2, p1, Lcom/google/android/location/l/a/am;->h:Ljava/lang/Integer;

    invoke-virtual {v1, v2}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_8

    goto/16 :goto_0

    .line 205
    :cond_1d
    iget-object v1, p0, Lcom/google/android/location/l/a/am;->i:Ljava/lang/String;

    iget-object v2, p1, Lcom/google/android/location/l/a/am;->i:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_9

    goto/16 :goto_0

    .line 212
    :cond_1e
    iget-object v1, p0, Lcom/google/android/location/l/a/am;->j:Ljava/lang/Integer;

    iget-object v2, p1, Lcom/google/android/location/l/a/am;->j:Ljava/lang/Integer;

    invoke-virtual {v1, v2}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_a

    goto/16 :goto_0

    .line 219
    :cond_1f
    iget-object v1, p0, Lcom/google/android/location/l/a/am;->k:Ljava/lang/Integer;

    iget-object v2, p1, Lcom/google/android/location/l/a/am;->k:Ljava/lang/Integer;

    invoke-virtual {v1, v2}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_b

    goto/16 :goto_0

    .line 226
    :cond_20
    iget-object v1, p0, Lcom/google/android/location/l/a/am;->l:Ljava/lang/Integer;

    iget-object v2, p1, Lcom/google/android/location/l/a/am;->l:Ljava/lang/Integer;

    invoke-virtual {v1, v2}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_c

    goto/16 :goto_0

    .line 233
    :cond_21
    iget-object v1, p0, Lcom/google/android/location/l/a/am;->m:Ljava/lang/Integer;

    iget-object v2, p1, Lcom/google/android/location/l/a/am;->m:Ljava/lang/Integer;

    invoke-virtual {v1, v2}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_d

    goto/16 :goto_0

    .line 241
    :cond_22
    iget-object v1, p0, Lcom/google/android/location/l/a/am;->n:Lcom/google/android/location/l/a/x;

    iget-object v2, p1, Lcom/google/android/location/l/a/am;->n:Lcom/google/android/location/l/a/x;

    invoke-virtual {v1, v2}, Lcom/google/android/location/l/a/x;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_e

    goto/16 :goto_0

    .line 249
    :cond_23
    iget-object v1, p0, Lcom/google/android/location/l/a/am;->o:Ljava/lang/String;

    iget-object v2, p1, Lcom/google/android/location/l/a/am;->o:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_f

    goto/16 :goto_0

    .line 256
    :cond_24
    iget-object v1, p0, Lcom/google/android/location/l/a/am;->p:Ljava/lang/Float;

    iget-object v2, p1, Lcom/google/android/location/l/a/am;->p:Ljava/lang/Float;

    invoke-virtual {v1, v2}, Ljava/lang/Float;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_10

    goto/16 :goto_0

    .line 263
    :cond_25
    iget-object v1, p0, Lcom/google/android/location/l/a/am;->q:Ljava/lang/Boolean;

    iget-object v2, p1, Lcom/google/android/location/l/a/am;->q:Ljava/lang/Boolean;

    invoke-virtual {v1, v2}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_11

    goto/16 :goto_0

    .line 270
    :cond_26
    iget-object v1, p0, Lcom/google/android/location/l/a/am;->r:Ljava/lang/String;

    iget-object v2, p1, Lcom/google/android/location/l/a/am;->r:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_12

    goto/16 :goto_0

    .line 278
    :cond_27
    iget-object v1, p0, Lcom/google/android/location/l/a/am;->s:Lcom/google/android/location/l/a/ae;

    iget-object v2, p1, Lcom/google/android/location/l/a/am;->s:Lcom/google/android/location/l/a/ae;

    invoke-virtual {v1, v2}, Lcom/google/android/location/l/a/ae;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_13

    goto/16 :goto_0

    .line 286
    :cond_28
    iget-object v1, p0, Lcom/google/android/location/l/a/am;->t:Ljava/lang/Integer;

    iget-object v2, p1, Lcom/google/android/location/l/a/am;->t:Ljava/lang/Integer;

    invoke-virtual {v1, v2}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_14

    goto/16 :goto_0

    .line 297
    :cond_29
    iget-object v1, p0, Lcom/google/android/location/l/a/am;->v:Ljava/lang/Integer;

    iget-object v2, p1, Lcom/google/android/location/l/a/am;->v:Ljava/lang/Integer;

    invoke-virtual {v1, v2}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_15

    goto/16 :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 305
    iget-object v0, p0, Lcom/google/android/location/l/a/am;->a:Lcom/google/android/location/l/a/ag;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    .line 308
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/location/l/a/am;->b:Ljava/lang/String;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    .line 310
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/location/l/a/am;->c:Ljava/lang/Integer;

    if-nez v0, :cond_2

    move v0, v1

    :goto_2
    add-int/2addr v0, v2

    .line 312
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/location/l/a/am;->d:Ljava/lang/Integer;

    if-nez v0, :cond_3

    move v0, v1

    :goto_3
    add-int/2addr v0, v2

    .line 314
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/android/location/l/a/am;->e:[Lcom/google/android/location/l/a/t;

    invoke-static {v2}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;)I

    move-result v2

    add-int/2addr v0, v2

    .line 316
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/location/l/a/am;->f:Ljava/lang/Long;

    if-nez v0, :cond_4

    move v0, v1

    :goto_4
    add-int/2addr v0, v2

    .line 318
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/location/l/a/am;->g:Ljava/lang/Boolean;

    if-nez v0, :cond_5

    move v0, v1

    :goto_5
    add-int/2addr v0, v2

    .line 320
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/location/l/a/am;->h:Ljava/lang/Integer;

    if-nez v0, :cond_6

    move v0, v1

    :goto_6
    add-int/2addr v0, v2

    .line 321
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/location/l/a/am;->i:Ljava/lang/String;

    if-nez v0, :cond_7

    move v0, v1

    :goto_7
    add-int/2addr v0, v2

    .line 323
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/location/l/a/am;->j:Ljava/lang/Integer;

    if-nez v0, :cond_8

    move v0, v1

    :goto_8
    add-int/2addr v0, v2

    .line 325
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/location/l/a/am;->k:Ljava/lang/Integer;

    if-nez v0, :cond_9

    move v0, v1

    :goto_9
    add-int/2addr v0, v2

    .line 327
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/location/l/a/am;->l:Ljava/lang/Integer;

    if-nez v0, :cond_a

    move v0, v1

    :goto_a
    add-int/2addr v0, v2

    .line 329
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/location/l/a/am;->m:Ljava/lang/Integer;

    if-nez v0, :cond_b

    move v0, v1

    :goto_b
    add-int/2addr v0, v2

    .line 331
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/location/l/a/am;->n:Lcom/google/android/location/l/a/x;

    if-nez v0, :cond_c

    move v0, v1

    :goto_c
    add-int/2addr v0, v2

    .line 333
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/location/l/a/am;->o:Ljava/lang/String;

    if-nez v0, :cond_d

    move v0, v1

    :goto_d
    add-int/2addr v0, v2

    .line 335
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/location/l/a/am;->p:Ljava/lang/Float;

    if-nez v0, :cond_e

    move v0, v1

    :goto_e
    add-int/2addr v0, v2

    .line 337
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/location/l/a/am;->q:Ljava/lang/Boolean;

    if-nez v0, :cond_f

    move v0, v1

    :goto_f
    add-int/2addr v0, v2

    .line 339
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/location/l/a/am;->r:Ljava/lang/String;

    if-nez v0, :cond_10

    move v0, v1

    :goto_10
    add-int/2addr v0, v2

    .line 341
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/location/l/a/am;->s:Lcom/google/android/location/l/a/ae;

    if-nez v0, :cond_11

    move v0, v1

    :goto_11
    add-int/2addr v0, v2

    .line 343
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/location/l/a/am;->t:Ljava/lang/Integer;

    if-nez v0, :cond_12

    move v0, v1

    :goto_12
    add-int/2addr v0, v2

    .line 345
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/android/location/l/a/am;->u:[Lcom/google/android/location/l/a/ay;

    invoke-static {v2}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;)I

    move-result v2

    add-int/2addr v0, v2

    .line 347
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/android/location/l/a/am;->v:Ljava/lang/Integer;

    if-nez v2, :cond_13

    :goto_13
    add-int/2addr v0, v1

    .line 349
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/google/android/location/l/a/am;->unknownFieldDataHashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 350
    return v0

    .line 305
    :cond_0
    iget-object v0, p0, Lcom/google/android/location/l/a/am;->a:Lcom/google/android/location/l/a/ag;

    invoke-virtual {v0}, Lcom/google/android/location/l/a/ag;->hashCode()I

    move-result v0

    goto/16 :goto_0

    .line 308
    :cond_1
    iget-object v0, p0, Lcom/google/android/location/l/a/am;->b:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto/16 :goto_1

    .line 310
    :cond_2
    iget-object v0, p0, Lcom/google/android/location/l/a/am;->c:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->hashCode()I

    move-result v0

    goto/16 :goto_2

    .line 312
    :cond_3
    iget-object v0, p0, Lcom/google/android/location/l/a/am;->d:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->hashCode()I

    move-result v0

    goto/16 :goto_3

    .line 316
    :cond_4
    iget-object v0, p0, Lcom/google/android/location/l/a/am;->f:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->hashCode()I

    move-result v0

    goto/16 :goto_4

    .line 318
    :cond_5
    iget-object v0, p0, Lcom/google/android/location/l/a/am;->g:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->hashCode()I

    move-result v0

    goto/16 :goto_5

    .line 320
    :cond_6
    iget-object v0, p0, Lcom/google/android/location/l/a/am;->h:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    goto/16 :goto_6

    .line 321
    :cond_7
    iget-object v0, p0, Lcom/google/android/location/l/a/am;->i:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto/16 :goto_7

    .line 323
    :cond_8
    iget-object v0, p0, Lcom/google/android/location/l/a/am;->j:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->hashCode()I

    move-result v0

    goto/16 :goto_8

    .line 325
    :cond_9
    iget-object v0, p0, Lcom/google/android/location/l/a/am;->k:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->hashCode()I

    move-result v0

    goto/16 :goto_9

    .line 327
    :cond_a
    iget-object v0, p0, Lcom/google/android/location/l/a/am;->l:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->hashCode()I

    move-result v0

    goto/16 :goto_a

    .line 329
    :cond_b
    iget-object v0, p0, Lcom/google/android/location/l/a/am;->m:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->hashCode()I

    move-result v0

    goto/16 :goto_b

    .line 331
    :cond_c
    iget-object v0, p0, Lcom/google/android/location/l/a/am;->n:Lcom/google/android/location/l/a/x;

    invoke-virtual {v0}, Lcom/google/android/location/l/a/x;->hashCode()I

    move-result v0

    goto/16 :goto_c

    .line 333
    :cond_d
    iget-object v0, p0, Lcom/google/android/location/l/a/am;->o:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto/16 :goto_d

    .line 335
    :cond_e
    iget-object v0, p0, Lcom/google/android/location/l/a/am;->p:Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->hashCode()I

    move-result v0

    goto/16 :goto_e

    .line 337
    :cond_f
    iget-object v0, p0, Lcom/google/android/location/l/a/am;->q:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->hashCode()I

    move-result v0

    goto/16 :goto_f

    .line 339
    :cond_10
    iget-object v0, p0, Lcom/google/android/location/l/a/am;->r:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto/16 :goto_10

    .line 341
    :cond_11
    iget-object v0, p0, Lcom/google/android/location/l/a/am;->s:Lcom/google/android/location/l/a/ae;

    invoke-virtual {v0}, Lcom/google/android/location/l/a/ae;->hashCode()I

    move-result v0

    goto/16 :goto_11

    .line 343
    :cond_12
    iget-object v0, p0, Lcom/google/android/location/l/a/am;->t:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->hashCode()I

    move-result v0

    goto/16 :goto_12

    .line 347
    :cond_13
    iget-object v1, p0, Lcom/google/android/location/l/a/am;->v:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->hashCode()I

    move-result v1

    goto/16 :goto_13
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 5
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/android/location/l/a/am;->storeUnknownField(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Lcom/google/android/location/l/a/am;->a:Lcom/google/android/location/l/a/ag;

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/android/location/l/a/ag;

    invoke-direct {v0}, Lcom/google/android/location/l/a/ag;-><init>()V

    iput-object v0, p0, Lcom/google/android/location/l/a/am;->a:Lcom/google/android/location/l/a/ag;

    :cond_1
    iget-object v0, p0, Lcom/google/android/location/l/a/am;->a:Lcom/google/android/location/l/a/ag;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/l/a/am;->b:Ljava/lang/String;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/l/a/am;->c:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/l/a/am;->d:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_5
    const/16 v0, 0x2a

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->b(Lcom/google/protobuf/nano/a;I)I

    move-result v2

    iget-object v0, p0, Lcom/google/android/location/l/a/am;->e:[Lcom/google/android/location/l/a/t;

    if-nez v0, :cond_3

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lcom/google/android/location/l/a/t;

    if-eqz v0, :cond_2

    iget-object v3, p0, Lcom/google/android/location/l/a/am;->e:[Lcom/google/android/location/l/a/t;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_2
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_4

    new-instance v3, Lcom/google/android/location/l/a/t;

    invoke-direct {v3}, Lcom/google/android/location/l/a/t;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_3
    iget-object v0, p0, Lcom/google/android/location/l/a/am;->e:[Lcom/google/android/location/l/a/t;

    array-length v0, v0

    goto :goto_1

    :cond_4
    new-instance v3, Lcom/google/android/location/l/a/t;

    invoke-direct {v3}, Lcom/google/android/location/l/a/t;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    iput-object v2, p0, Lcom/google/android/location/l/a/am;->e:[Lcom/google/android/location/l/a/t;

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->j()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/l/a/am;->f:Ljava/lang/Long;

    goto/16 :goto_0

    :sswitch_7
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/l/a/am;->g:Ljava/lang/Boolean;

    goto/16 :goto_0

    :sswitch_8
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    sparse-switch v0, :sswitch_data_1

    goto/16 :goto_0

    :sswitch_9
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/l/a/am;->h:Ljava/lang/Integer;

    goto/16 :goto_0

    :sswitch_a
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/l/a/am;->i:Ljava/lang/String;

    goto/16 :goto_0

    :sswitch_b
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/l/a/am;->j:Ljava/lang/Integer;

    goto/16 :goto_0

    :sswitch_c
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/l/a/am;->k:Ljava/lang/Integer;

    goto/16 :goto_0

    :sswitch_d
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/l/a/am;->l:Ljava/lang/Integer;

    goto/16 :goto_0

    :sswitch_e
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/l/a/am;->m:Ljava/lang/Integer;

    goto/16 :goto_0

    :sswitch_f
    iget-object v0, p0, Lcom/google/android/location/l/a/am;->n:Lcom/google/android/location/l/a/x;

    if-nez v0, :cond_5

    new-instance v0, Lcom/google/android/location/l/a/x;

    invoke-direct {v0}, Lcom/google/android/location/l/a/x;-><init>()V

    iput-object v0, p0, Lcom/google/android/location/l/a/am;->n:Lcom/google/android/location/l/a/x;

    :cond_5
    iget-object v0, p0, Lcom/google/android/location/l/a/am;->n:Lcom/google/android/location/l/a/x;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto/16 :goto_0

    :sswitch_10
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/l/a/am;->o:Ljava/lang/String;

    goto/16 :goto_0

    :sswitch_11
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->k()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->intBitsToFloat(I)F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/l/a/am;->p:Ljava/lang/Float;

    goto/16 :goto_0

    :sswitch_12
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/l/a/am;->q:Ljava/lang/Boolean;

    goto/16 :goto_0

    :sswitch_13
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/l/a/am;->r:Ljava/lang/String;

    goto/16 :goto_0

    :sswitch_14
    iget-object v0, p0, Lcom/google/android/location/l/a/am;->s:Lcom/google/android/location/l/a/ae;

    if-nez v0, :cond_6

    new-instance v0, Lcom/google/android/location/l/a/ae;

    invoke-direct {v0}, Lcom/google/android/location/l/a/ae;-><init>()V

    iput-object v0, p0, Lcom/google/android/location/l/a/am;->s:Lcom/google/android/location/l/a/ae;

    :cond_6
    iget-object v0, p0, Lcom/google/android/location/l/a/am;->s:Lcom/google/android/location/l/a/ae;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto/16 :goto_0

    :sswitch_15
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/l/a/am;->t:Ljava/lang/Integer;

    goto/16 :goto_0

    :sswitch_16
    const/16 v0, 0xaa

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->b(Lcom/google/protobuf/nano/a;I)I

    move-result v2

    iget-object v0, p0, Lcom/google/android/location/l/a/am;->u:[Lcom/google/android/location/l/a/ay;

    if-nez v0, :cond_8

    move v0, v1

    :goto_3
    add-int/2addr v2, v0

    new-array v2, v2, [Lcom/google/android/location/l/a/ay;

    if-eqz v0, :cond_7

    iget-object v3, p0, Lcom/google/android/location/l/a/am;->u:[Lcom/google/android/location/l/a/ay;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_7
    :goto_4
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_9

    new-instance v3, Lcom/google/android/location/l/a/ay;

    invoke-direct {v3}, Lcom/google/android/location/l/a/ay;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_8
    iget-object v0, p0, Lcom/google/android/location/l/a/am;->u:[Lcom/google/android/location/l/a/ay;

    array-length v0, v0

    goto :goto_3

    :cond_9
    new-instance v3, Lcom/google/android/location/l/a/ay;

    invoke-direct {v3}, Lcom/google/android/location/l/a/ay;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    iput-object v2, p0, Lcom/google/android/location/l/a/am;->u:[Lcom/google/android/location/l/a/ay;

    goto/16 :goto_0

    :sswitch_17
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/l/a/am;->v:Ljava/lang/Integer;

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x2a -> :sswitch_5
        0x30 -> :sswitch_6
        0x38 -> :sswitch_7
        0x40 -> :sswitch_8
        0x4a -> :sswitch_a
        0x50 -> :sswitch_b
        0x58 -> :sswitch_c
        0x60 -> :sswitch_d
        0x68 -> :sswitch_e
        0x72 -> :sswitch_f
        0x7a -> :sswitch_10
        0x85 -> :sswitch_11
        0x88 -> :sswitch_12
        0x92 -> :sswitch_13
        0x9a -> :sswitch_14
        0xa0 -> :sswitch_15
        0xaa -> :sswitch_16
        0xb0 -> :sswitch_17
    .end sparse-switch

    :sswitch_data_1
    .sparse-switch
        0x0 -> :sswitch_9
        0x1 -> :sswitch_9
        0x2 -> :sswitch_9
        0x3 -> :sswitch_9
        0x4 -> :sswitch_9
        0xf -> :sswitch_9
        0x10 -> :sswitch_9
        0x20 -> :sswitch_9
        0x40 -> :sswitch_9
        0x80 -> :sswitch_9
        0x100 -> :sswitch_9
        0x200 -> :sswitch_9
        0x400 -> :sswitch_9
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 356
    iget-object v0, p0, Lcom/google/android/location/l/a/am;->a:Lcom/google/android/location/l/a/ag;

    if-eqz v0, :cond_0

    .line 357
    const/4 v0, 0x1

    iget-object v2, p0, Lcom/google/android/location/l/a/am;->a:Lcom/google/android/location/l/a/ag;

    invoke-virtual {p1, v0, v2}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 359
    :cond_0
    iget-object v0, p0, Lcom/google/android/location/l/a/am;->b:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 360
    const/4 v0, 0x2

    iget-object v2, p0, Lcom/google/android/location/l/a/am;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v2}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 362
    :cond_1
    iget-object v0, p0, Lcom/google/android/location/l/a/am;->c:Ljava/lang/Integer;

    if-eqz v0, :cond_2

    .line 363
    const/4 v0, 0x3

    iget-object v2, p0, Lcom/google/android/location/l/a/am;->c:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {p1, v0, v2}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 365
    :cond_2
    iget-object v0, p0, Lcom/google/android/location/l/a/am;->d:Ljava/lang/Integer;

    if-eqz v0, :cond_3

    .line 366
    const/4 v0, 0x4

    iget-object v2, p0, Lcom/google/android/location/l/a/am;->d:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {p1, v0, v2}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 368
    :cond_3
    iget-object v0, p0, Lcom/google/android/location/l/a/am;->e:[Lcom/google/android/location/l/a/t;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/google/android/location/l/a/am;->e:[Lcom/google/android/location/l/a/t;

    array-length v0, v0

    if-lez v0, :cond_5

    move v0, v1

    .line 369
    :goto_0
    iget-object v2, p0, Lcom/google/android/location/l/a/am;->e:[Lcom/google/android/location/l/a/t;

    array-length v2, v2

    if-ge v0, v2, :cond_5

    .line 370
    iget-object v2, p0, Lcom/google/android/location/l/a/am;->e:[Lcom/google/android/location/l/a/t;

    aget-object v2, v2, v0

    .line 371
    if-eqz v2, :cond_4

    .line 372
    const/4 v3, 0x5

    invoke-virtual {p1, v3, v2}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 369
    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 376
    :cond_5
    iget-object v0, p0, Lcom/google/android/location/l/a/am;->f:Ljava/lang/Long;

    if-eqz v0, :cond_6

    .line 377
    const/4 v0, 0x6

    iget-object v2, p0, Lcom/google/android/location/l/a/am;->f:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/b;->b(IJ)V

    .line 379
    :cond_6
    iget-object v0, p0, Lcom/google/android/location/l/a/am;->g:Ljava/lang/Boolean;

    if-eqz v0, :cond_7

    .line 380
    const/4 v0, 0x7

    iget-object v2, p0, Lcom/google/android/location/l/a/am;->g:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-virtual {p1, v0, v2}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 382
    :cond_7
    iget-object v0, p0, Lcom/google/android/location/l/a/am;->h:Ljava/lang/Integer;

    if-eqz v0, :cond_8

    .line 383
    const/16 v0, 0x8

    iget-object v2, p0, Lcom/google/android/location/l/a/am;->h:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {p1, v0, v2}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 385
    :cond_8
    iget-object v0, p0, Lcom/google/android/location/l/a/am;->i:Ljava/lang/String;

    if-eqz v0, :cond_9

    .line 386
    const/16 v0, 0x9

    iget-object v2, p0, Lcom/google/android/location/l/a/am;->i:Ljava/lang/String;

    invoke-virtual {p1, v0, v2}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 388
    :cond_9
    iget-object v0, p0, Lcom/google/android/location/l/a/am;->j:Ljava/lang/Integer;

    if-eqz v0, :cond_a

    .line 389
    const/16 v0, 0xa

    iget-object v2, p0, Lcom/google/android/location/l/a/am;->j:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {p1, v0, v2}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 391
    :cond_a
    iget-object v0, p0, Lcom/google/android/location/l/a/am;->k:Ljava/lang/Integer;

    if-eqz v0, :cond_b

    .line 392
    const/16 v0, 0xb

    iget-object v2, p0, Lcom/google/android/location/l/a/am;->k:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {p1, v0, v2}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 394
    :cond_b
    iget-object v0, p0, Lcom/google/android/location/l/a/am;->l:Ljava/lang/Integer;

    if-eqz v0, :cond_c

    .line 395
    const/16 v0, 0xc

    iget-object v2, p0, Lcom/google/android/location/l/a/am;->l:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {p1, v0, v2}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 397
    :cond_c
    iget-object v0, p0, Lcom/google/android/location/l/a/am;->m:Ljava/lang/Integer;

    if-eqz v0, :cond_d

    .line 398
    const/16 v0, 0xd

    iget-object v2, p0, Lcom/google/android/location/l/a/am;->m:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {p1, v0, v2}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 400
    :cond_d
    iget-object v0, p0, Lcom/google/android/location/l/a/am;->n:Lcom/google/android/location/l/a/x;

    if-eqz v0, :cond_e

    .line 401
    const/16 v0, 0xe

    iget-object v2, p0, Lcom/google/android/location/l/a/am;->n:Lcom/google/android/location/l/a/x;

    invoke-virtual {p1, v0, v2}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 403
    :cond_e
    iget-object v0, p0, Lcom/google/android/location/l/a/am;->o:Ljava/lang/String;

    if-eqz v0, :cond_f

    .line 404
    const/16 v0, 0xf

    iget-object v2, p0, Lcom/google/android/location/l/a/am;->o:Ljava/lang/String;

    invoke-virtual {p1, v0, v2}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 406
    :cond_f
    iget-object v0, p0, Lcom/google/android/location/l/a/am;->p:Ljava/lang/Float;

    if-eqz v0, :cond_10

    .line 407
    const/16 v0, 0x10

    iget-object v2, p0, Lcom/google/android/location/l/a/am;->p:Ljava/lang/Float;

    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    move-result v2

    invoke-virtual {p1, v0, v2}, Lcom/google/protobuf/nano/b;->a(IF)V

    .line 409
    :cond_10
    iget-object v0, p0, Lcom/google/android/location/l/a/am;->q:Ljava/lang/Boolean;

    if-eqz v0, :cond_11

    .line 410
    const/16 v0, 0x11

    iget-object v2, p0, Lcom/google/android/location/l/a/am;->q:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-virtual {p1, v0, v2}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 412
    :cond_11
    iget-object v0, p0, Lcom/google/android/location/l/a/am;->r:Ljava/lang/String;

    if-eqz v0, :cond_12

    .line 413
    const/16 v0, 0x12

    iget-object v2, p0, Lcom/google/android/location/l/a/am;->r:Ljava/lang/String;

    invoke-virtual {p1, v0, v2}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 415
    :cond_12
    iget-object v0, p0, Lcom/google/android/location/l/a/am;->s:Lcom/google/android/location/l/a/ae;

    if-eqz v0, :cond_13

    .line 416
    const/16 v0, 0x13

    iget-object v2, p0, Lcom/google/android/location/l/a/am;->s:Lcom/google/android/location/l/a/ae;

    invoke-virtual {p1, v0, v2}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 418
    :cond_13
    iget-object v0, p0, Lcom/google/android/location/l/a/am;->t:Ljava/lang/Integer;

    if-eqz v0, :cond_14

    .line 419
    const/16 v0, 0x14

    iget-object v2, p0, Lcom/google/android/location/l/a/am;->t:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {p1, v0, v2}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 421
    :cond_14
    iget-object v0, p0, Lcom/google/android/location/l/a/am;->u:[Lcom/google/android/location/l/a/ay;

    if-eqz v0, :cond_16

    iget-object v0, p0, Lcom/google/android/location/l/a/am;->u:[Lcom/google/android/location/l/a/ay;

    array-length v0, v0

    if-lez v0, :cond_16

    .line 422
    :goto_1
    iget-object v0, p0, Lcom/google/android/location/l/a/am;->u:[Lcom/google/android/location/l/a/ay;

    array-length v0, v0

    if-ge v1, v0, :cond_16

    .line 423
    iget-object v0, p0, Lcom/google/android/location/l/a/am;->u:[Lcom/google/android/location/l/a/ay;

    aget-object v0, v0, v1

    .line 424
    if-eqz v0, :cond_15

    .line 425
    const/16 v2, 0x15

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 422
    :cond_15
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 429
    :cond_16
    iget-object v0, p0, Lcom/google/android/location/l/a/am;->v:Ljava/lang/Integer;

    if-eqz v0, :cond_17

    .line 430
    const/16 v0, 0x16

    iget-object v1, p0, Lcom/google/android/location/l/a/am;->v:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 432
    :cond_17
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/d;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 433
    return-void
.end method
