.class public Lcom/google/android/location/settings/GoogleLocationSettingsActivity;
.super Landroid/support/v7/app/d;
.source "SourceFile"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;
.implements Landroid/content/ServiceConnection;


# static fields
.field private static final a:Landroid/content/IntentFilter;


# instance fields
.field private b:Lcom/google/android/location/reporting/service/l;

.field private c:I

.field private d:Z

.field private e:Z

.field private f:Lcom/google/android/location/settings/g;

.field private g:Ljava/lang/String;

.field private h:Landroid/widget/CheckBox;

.field private i:Landroid/view/ViewGroup;

.field private final j:Ljava/util/List;

.field private k:Landroid/view/ViewGroup;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 98
    new-instance v0, Landroid/content/IntentFilter;

    const-string v1, "com.google.android.gms.location.reporting.SETTINGS_CHANGED"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    const-string v1, "com.google.android.gms.location.reporting.INITIALIZATION_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/location/settings/GoogleLocationSettingsActivity;->a:Landroid/content/IntentFilter;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 72
    invoke-direct {p0}, Landroid/support/v7/app/d;-><init>()V

    .line 140
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/location/settings/GoogleLocationSettingsActivity;->j:Ljava/util/List;

    .line 762
    return-void
.end method

.method static synthetic a(Lcom/google/android/location/settings/GoogleLocationSettingsActivity;)I
    .locals 1

    .prologue
    .line 72
    iget v0, p0, Lcom/google/android/location/settings/GoogleLocationSettingsActivity;->c:I

    return v0
.end method

.method static synthetic a(Lcom/google/android/location/settings/GoogleLocationSettingsActivity;I)I
    .locals 0

    .prologue
    .line 72
    iput p1, p0, Lcom/google/android/location/settings/GoogleLocationSettingsActivity;->c:I

    return p1
.end method

.method static synthetic a(Lcom/google/android/location/settings/GoogleLocationSettingsActivity;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 72
    iput-object p1, p0, Lcom/google/android/location/settings/GoogleLocationSettingsActivity;->g:Ljava/lang/String;

    return-object p1
.end method

.method private static a(Landroid/content/Intent;)V
    .locals 2

    .prologue
    .line 416
    const-string v0, "ambiguous_setting_notification"

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 418
    invoke-static {}, Lcom/google/android/location/reporting/b/l;->j()V

    .line 420
    :cond_0
    return-void
.end method

.method public static a(Landroid/content/Context;Lcom/google/android/location/reporting/config/ReportingConfig;)Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 170
    const/16 v2, 0x13

    invoke-static {v2}, Lcom/google/android/gms/common/util/ao;->a(I)Z

    move-result v2

    if-nez v2, :cond_0

    invoke-static {p0}, Lcom/google/android/gms/common/util/y;->b(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 182
    :cond_0
    :goto_0
    return v0

    .line 177
    :cond_1
    new-instance v2, Lcom/google/android/location/n/b;

    invoke-direct {v2, p0}, Lcom/google/android/location/n/b;-><init>(Landroid/content/Context;)V

    invoke-virtual {v2}, Lcom/google/android/location/n/b;->a()[Landroid/accounts/Account;

    move-result-object v3

    .line 178
    invoke-virtual {p1}, Lcom/google/android/location/reporting/config/ReportingConfig;->g()Z

    move-result v2

    if-nez v2, :cond_2

    invoke-virtual {p1}, Lcom/google/android/location/reporting/config/ReportingConfig;->d()Lcom/google/android/location/reporting/config/Conditions;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/location/reporting/config/Conditions;->isIneligibleDueToGeoOnly()Z

    move-result v2

    if-eqz v2, :cond_4

    :cond_2
    move v2, v0

    .line 180
    :goto_1
    if-eqz v2, :cond_3

    array-length v2, v3

    if-gtz v2, :cond_0

    :cond_3
    move v0, v1

    .line 182
    goto :goto_0

    :cond_4
    move v2, v1

    .line 178
    goto :goto_1
.end method

.method static synthetic b(Lcom/google/android/location/settings/GoogleLocationSettingsActivity;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lcom/google/android/location/settings/GoogleLocationSettingsActivity;->g:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic c(Lcom/google/android/location/settings/GoogleLocationSettingsActivity;)V
    .locals 0

    .prologue
    .line 72
    invoke-direct {p0}, Lcom/google/android/location/settings/GoogleLocationSettingsActivity;->f()V

    return-void
.end method

.method private e()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 353
    iget-boolean v1, p0, Lcom/google/android/location/settings/GoogleLocationSettingsActivity;->e:Z

    if-nez v1, :cond_0

    invoke-static {p0}, Lcom/google/android/gms/common/util/y;->c(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {p0}, Lcom/google/android/gms/common/util/y;->a(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 355
    new-instance v1, Landroid/content/Intent;

    const-string v2, "com.google.android.gsf.GOOGLE_LOCATION_SETTINGS"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 356
    const/16 v2, 0x89

    invoke-virtual {p0, v1, v2}, Lcom/google/android/location/settings/GoogleLocationSettingsActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 357
    iput-boolean v0, p0, Lcom/google/android/location/settings/GoogleLocationSettingsActivity;->e:Z

    .line 360
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private f()V
    .locals 11
    .annotation build Landroid/annotation/TargetApi;
        value = 0xe
    .end annotation

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x2

    const/4 v10, 0x1

    const/4 v9, 0x0

    .line 437
    iget-object v0, p0, Lcom/google/android/location/settings/GoogleLocationSettingsActivity;->b:Lcom/google/android/location/reporting/service/l;

    if-nez v0, :cond_1

    .line 439
    const-string v0, "GCoreLocationSettings"

    invoke-static {v0, v5}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 440
    const-string v0, "GCoreLocationSettings"

    const-string v1, "Service not connected, skipping UI refresh"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 493
    :cond_0
    :goto_0
    return-void

    .line 447
    :cond_1
    :try_start_0
    iget-object v0, p0, Lcom/google/android/location/settings/GoogleLocationSettingsActivity;->b:Lcom/google/android/location/reporting/service/l;

    invoke-interface {v0}, Lcom/google/android/location/reporting/service/l;->a()Lcom/google/android/location/reporting/config/ReportingConfig;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v7

    .line 454
    invoke-virtual {v7}, Lcom/google/android/location/reporting/config/ReportingConfig;->b()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 455
    iput v10, p0, Lcom/google/android/location/settings/GoogleLocationSettingsActivity;->c:I

    .line 458
    :cond_2
    invoke-virtual {v7}, Lcom/google/android/location/reporting/config/ReportingConfig;->d()Lcom/google/android/location/reporting/config/Conditions;

    move-result-object v0

    .line 459
    invoke-virtual {v0}, Lcom/google/android/location/reporting/config/Conditions;->f()Z

    move-result v1

    .line 460
    invoke-virtual {v7}, Lcom/google/android/location/reporting/config/ReportingConfig;->g()Z

    move-result v2

    .line 461
    invoke-virtual {v0}, Lcom/google/android/location/reporting/config/Conditions;->isIneligibleDueToGeoOnly()Z

    move-result v3

    .line 463
    iget-object v0, p0, Lcom/google/android/location/settings/GoogleLocationSettingsActivity;->h:Landroid/widget/CheckBox;

    if-eqz v0, :cond_3

    .line 464
    iget-object v0, p0, Lcom/google/android/location/settings/GoogleLocationSettingsActivity;->h:Landroid/widget/CheckBox;

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 467
    :cond_3
    iget-object v0, p0, Lcom/google/android/location/settings/GoogleLocationSettingsActivity;->f:Lcom/google/android/location/settings/g;

    sget-object v1, Lcom/google/android/location/settings/GoogleLocationSettingsActivity;->a:Landroid/content/IntentFilter;

    invoke-virtual {p0, v0, v1}, Lcom/google/android/location/settings/GoogleLocationSettingsActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    iget-boolean v0, p0, Lcom/google/android/location/settings/GoogleLocationSettingsActivity;->d:Z

    if-eqz v0, :cond_6

    invoke-static {p0, v7}, Lcom/google/android/location/reporting/service/InitializerService;->a(Landroid/content/Context;Lcom/google/android/location/reporting/config/ReportingConfig;)I

    move-result v0

    iput v0, p0, Lcom/google/android/location/settings/GoogleLocationSettingsActivity;->c:I

    const-string v0, "GCoreLocationSettings"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_4

    const-string v0, "GCoreLocationSettings"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v4, "refreshUlrSettings("

    invoke-direct {v1, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, "), initialization: "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v4, p0, Lcom/google/android/location/settings/GoogleLocationSettingsActivity;->c:I

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_4
    iget v0, p0, Lcom/google/android/location/settings/GoogleLocationSettingsActivity;->c:I

    if-eq v0, v5, :cond_5

    iget-boolean v0, p0, Lcom/google/android/location/settings/GoogleLocationSettingsActivity;->d:Z

    if-eqz v0, :cond_6

    invoke-virtual {v7}, Lcom/google/android/location/reporting/config/ReportingConfig;->g()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-virtual {v7}, Lcom/google/android/location/reporting/config/ReportingConfig;->c()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/reporting/config/AccountConfig;

    invoke-virtual {v0}, Lcom/google/android/location/reporting/config/AccountConfig;->b()Landroid/accounts/Account;

    move-result-object v0

    const-string v4, "GoogleLocationSettingsActivity"

    invoke-static {p0, v4, v0, v6, v6}, Lcom/google/android/location/reporting/service/v;->a(Landroid/content/Context;Ljava/lang/String;Landroid/accounts/Account;Ljava/lang/Boolean;Ljava/lang/Boolean;)Z

    goto :goto_1

    .line 448
    :catch_0
    move-exception v0

    .line 449
    invoke-static {v0}, Lcom/google/android/location/reporting/b/l;->a(Ljava/lang/Throwable;)V

    .line 450
    const-string v1, "GCoreLocationSettings"

    invoke-static {v1, v0}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/Throwable;)I

    goto/16 :goto_0

    .line 467
    :cond_5
    iput-boolean v9, p0, Lcom/google/android/location/settings/GoogleLocationSettingsActivity;->d:Z

    .line 471
    :cond_6
    iget-object v0, p0, Lcom/google/android/location/settings/GoogleLocationSettingsActivity;->j:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/settings/e;

    .line 472
    iget-object v4, p0, Lcom/google/android/location/settings/GoogleLocationSettingsActivity;->k:Landroid/view/ViewGroup;

    iget-object v0, v0, Lcom/google/android/location/settings/e;->b:Landroid/view/ViewGroup;

    invoke-virtual {v4, v0}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    goto :goto_2

    .line 474
    :cond_7
    iget-object v0, p0, Lcom/google/android/location/settings/GoogleLocationSettingsActivity;->j:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 483
    if-nez v2, :cond_8

    if-eqz v3, :cond_9

    .line 484
    :cond_8
    invoke-virtual {v7}, Lcom/google/android/location/reporting/config/ReportingConfig;->c()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_3
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_9

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/google/android/location/reporting/config/AccountConfig;

    .line 485
    new-instance v0, Lcom/google/android/location/settings/e;

    invoke-virtual {v6}, Lcom/google/android/location/reporting/config/AccountConfig;->b()Landroid/accounts/Account;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/google/android/location/settings/e;-><init>(Lcom/google/android/location/settings/GoogleLocationSettingsActivity;Landroid/accounts/Account;)V

    .line 487
    iget-object v1, p0, Lcom/google/android/location/settings/GoogleLocationSettingsActivity;->k:Landroid/view/ViewGroup;

    iget-object v2, v0, Lcom/google/android/location/settings/e;->b:Landroid/view/ViewGroup;

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 488
    invoke-virtual {v6}, Lcom/google/android/location/reporting/config/AccountConfig;->y()Z

    move-result v2

    invoke-virtual {v6}, Lcom/google/android/location/reporting/config/AccountConfig;->r()I

    move-result v1

    invoke-virtual {v0, v1, v6, v10}, Lcom/google/android/location/settings/e;->a(ILcom/google/android/location/reporting/config/AccountConfig;Z)Ljava/lang/String;

    move-result-object v4

    iget-object v1, v0, Lcom/google/android/location/settings/e;->c:Landroid/view/ViewGroup;

    sget v3, Lcom/google/android/gms/p;->qr:I

    const-class v5, Lcom/google/android/location/settings/LocationReportingSettingsActivity;

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/location/settings/e;->a(Landroid/view/ViewGroup;ZILjava/lang/String;Ljava/lang/Class;)Landroid/view/ViewGroup;

    invoke-virtual {v6}, Lcom/google/android/location/reporting/config/AccountConfig;->s()I

    move-result v1

    invoke-virtual {v0, v1, v6, v9}, Lcom/google/android/location/settings/e;->a(ILcom/google/android/location/reporting/config/AccountConfig;Z)Ljava/lang/String;

    move-result-object v4

    iget-object v1, v0, Lcom/google/android/location/settings/e;->d:Landroid/view/ViewGroup;

    sget v3, Lcom/google/android/gms/p;->qq:I

    const-class v5, Lcom/google/android/location/settings/LocationHistorySettingsActivity;

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/location/settings/e;->a(Landroid/view/ViewGroup;ZILjava/lang/String;Ljava/lang/Class;)Landroid/view/ViewGroup;

    .line 489
    iget-object v1, p0, Lcom/google/android/location/settings/GoogleLocationSettingsActivity;->j:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 492
    :cond_9
    iget-object v0, p0, Lcom/google/android/location/settings/GoogleLocationSettingsActivity;->i:Landroid/view/ViewGroup;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/location/settings/GoogleLocationSettingsActivity;->k:Landroid/view/ViewGroup;

    invoke-static {v0}, Lcom/google/android/location/settings/r;->a(Landroid/view/ViewGroup;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {v7}, Lcom/google/android/location/reporting/config/ReportingConfig;->c()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/location/settings/GoogleLocationSettingsActivity;->i:Landroid/view/ViewGroup;

    invoke-virtual {v0, v9}, Landroid/view/ViewGroup;->setVisibility(I)V

    new-instance v0, Lcom/google/android/location/settings/d;

    invoke-direct {v0, p0}, Lcom/google/android/location/settings/d;-><init>(Lcom/google/android/location/settings/GoogleLocationSettingsActivity;)V

    iget-object v1, p0, Lcom/google/android/location/settings/GoogleLocationSettingsActivity;->i:Landroid/view/ViewGroup;

    sget v2, Lcom/google/android/gms/j;->O:I

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto/16 :goto_0
.end method


# virtual methods
.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 3

    .prologue
    .line 295
    const-string v0, "GCoreLocationSettings"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Got result: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "; "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "; "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 296
    sparse-switch p1, :sswitch_data_0

    .line 303
    :goto_0
    :sswitch_0
    return-void

    .line 300
    :sswitch_1
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/location/settings/GoogleLocationSettingsActivity;->e:Z

    goto :goto_0

    .line 296
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x89 -> :sswitch_1
    .end sparse-switch
.end method

.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 748
    const/4 v0, -0x1

    if-ne p2, v0, :cond_2

    .line 751
    iget-object v0, p0, Lcom/google/android/location/settings/GoogleLocationSettingsActivity;->h:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    .line 752
    :goto_0
    new-instance v3, Landroid/content/Intent;

    const-string v4, "com.google.android.gsf.action.SET_USE_LOCATION_FOR_SERVICES"

    invoke-direct {v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const/high16 v4, 0x10000000

    invoke-virtual {v3, v4}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    const-string v4, "disable"

    if-nez v0, :cond_1

    :goto_1
    invoke-virtual {v3, v4, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    :try_start_0
    invoke-virtual {p0, v3}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 757
    :goto_2
    return-void

    :cond_0
    move v0, v2

    .line 751
    goto :goto_0

    :cond_1
    move v1, v2

    .line 752
    goto :goto_1

    :catch_0
    move-exception v0

    const-string v0, "GCoreLocationSettings"

    const-string v1, "Problem while starting GSF location activity"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 755
    :cond_2
    invoke-direct {p0}, Lcom/google/android/location/settings/GoogleLocationSettingsActivity;->f()V

    goto :goto_2
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 6
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NewApi"
        }
    .end annotation

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 210
    invoke-super {p0, p1}, Landroid/support/v7/app/d;->onCreate(Landroid/os/Bundle;)V

    .line 212
    invoke-static {p0}, Lcom/google/android/location/reporting/b/n;->a(Landroid/content/Context;)V

    .line 214
    if-eqz p1, :cond_0

    .line 215
    const-string v0, "showingLgaayl"

    invoke-virtual {p1, v0, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/location/settings/GoogleLocationSettingsActivity;->e:Z

    .line 221
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/location/settings/GoogleLocationSettingsActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    .line 222
    invoke-virtual {v1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 223
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_1

    const-string v4, "com.google.android.gms.location.settings.LOCATION_HISTORY"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_1
    move v0, v3

    .line 226
    :goto_0
    invoke-static {v1}, Lcom/google/android/location/settings/GoogleLocationSettingsActivity;->a(Landroid/content/Intent;)V

    .line 228
    const/16 v4, 0x13

    invoke-static {v4}, Lcom/google/android/gms/common/util/ao;->a(I)Z

    move-result v4

    if-eqz v4, :cond_4

    if-nez v0, :cond_4

    .line 231
    invoke-direct {p0}, Lcom/google/android/location/settings/GoogleLocationSettingsActivity;->e()Z

    move-result v2

    .line 232
    if-nez v2, :cond_8

    .line 234
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.settings.LOCATION_SOURCE_SETTINGS"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 235
    invoke-virtual {p0, v0}, Lcom/google/android/location/settings/GoogleLocationSettingsActivity;->startActivity(Landroid/content/Intent;)V

    .line 237
    :goto_1
    const-string v1, "GCoreLocationSettings"

    const/4 v3, 0x3

    invoke-static {v1, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 238
    const-string v1, "GCoreLocationSettings"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Redirecting: lgaayl: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "; intent: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 240
    :cond_2
    invoke-virtual {p0}, Lcom/google/android/location/settings/GoogleLocationSettingsActivity;->finish()V

    .line 261
    :goto_2
    return-void

    :cond_3
    move v0, v2

    .line 223
    goto :goto_0

    .line 244
    :cond_4
    invoke-static {p0}, Lcom/google/android/gms/common/util/a;->b(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 245
    const-string v0, "GCoreLocationSettings"

    const-string v1, "Can\'t run for restricted users."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 246
    invoke-virtual {p0}, Lcom/google/android/location/settings/GoogleLocationSettingsActivity;->finish()V

    goto :goto_2

    .line 250
    :cond_5
    sget v0, Lcom/google/android/gms/l;->cA:I

    invoke-virtual {p0, v0}, Lcom/google/android/location/settings/GoogleLocationSettingsActivity;->setContentView(I)V

    .line 251
    sget v0, Lcom/google/android/gms/j;->ru:I

    invoke-virtual {p0, v0}, Lcom/google/android/location/settings/GoogleLocationSettingsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/google/android/location/settings/GoogleLocationSettingsActivity;->k:Landroid/view/ViewGroup;

    .line 253
    sget v0, Lcom/google/android/gms/j;->le:I

    invoke-virtual {p0, v0}, Lcom/google/android/location/settings/GoogleLocationSettingsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    invoke-static {p0}, Lcom/google/android/gms/common/util/y;->b(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_6

    invoke-static {p0}, Lcom/google/android/gms/common/util/y;->c(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_6

    move v1, v3

    :goto_3
    if-eqz v1, :cond_7

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->setVisibility(I)V

    const v1, 0x1020001

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/CheckBox;

    iput-object v1, p0, Lcom/google/android/location/settings/GoogleLocationSettingsActivity;->h:Landroid/widget/CheckBox;

    iget-object v1, p0, Lcom/google/android/location/settings/GoogleLocationSettingsActivity;->h:Landroid/widget/CheckBox;

    invoke-virtual {v1, v2}, Landroid/widget/CheckBox;->setVisibility(I)V

    new-instance v4, Lcom/google/android/location/settings/c;

    invoke-direct {v4, p0}, Lcom/google/android/location/settings/c;-><init>(Lcom/google/android/location/settings/GoogleLocationSettingsActivity;)V

    sget v1, Lcom/google/android/gms/p;->pM:I

    sget v5, Lcom/google/android/gms/p;->pL:I

    invoke-static {v0, v1}, Lcom/google/android/location/settings/r;->a(Landroid/view/ViewGroup;I)V

    const v1, 0x1020010

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    invoke-virtual {v1, v5}, Landroid/widget/TextView;->setText(I)V

    invoke-virtual {v0, v4}, Landroid/view/ViewGroup;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :goto_4
    sget v0, Lcom/google/android/gms/j;->mA:I

    invoke-virtual {p0, v0}, Lcom/google/android/location/settings/GoogleLocationSettingsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/google/android/location/settings/GoogleLocationSettingsActivity;->i:Landroid/view/ViewGroup;

    .line 255
    invoke-super {p0}, Landroid/support/v7/app/d;->d()Landroid/support/v7/app/e;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v7/app/e;->b()Landroid/support/v7/app/a;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/support/v7/app/a;->a(Z)V

    .line 257
    iput v2, p0, Lcom/google/android/location/settings/GoogleLocationSettingsActivity;->c:I

    .line 260
    new-instance v0, Lcom/google/android/location/settings/g;

    invoke-direct {v0, p0, v2}, Lcom/google/android/location/settings/g;-><init>(Lcom/google/android/location/settings/GoogleLocationSettingsActivity;B)V

    iput-object v0, p0, Lcom/google/android/location/settings/GoogleLocationSettingsActivity;->f:Lcom/google/android/location/settings/g;

    goto/16 :goto_2

    :cond_6
    move v1, v2

    .line 253
    goto :goto_3

    :cond_7
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    goto :goto_4

    :cond_8
    move-object v0, v1

    goto/16 :goto_1
.end method

.method protected onCreateDialog(I)Landroid/app/Dialog;
    .locals 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 720
    packed-switch p1, :pswitch_data_0

    .line 724
    invoke-super {p0, p1}, Landroid/support/v7/app/d;->onCreateDialog(I)Landroid/app/Dialog;

    move-result-object v0

    :goto_0
    return-object v0

    .line 722
    :pswitch_0
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    sget v1, Lcom/google/android/gms/p;->qw:I

    invoke-virtual {p0, v1}, Lcom/google/android/location/settings/GoogleLocationSettingsActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    const v1, 0x104000a

    invoke-virtual {v0, v1, p0}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    const/high16 v1, 0x1040000

    invoke-virtual {v0, v1, p0}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    goto :goto_0

    .line 720
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 341
    sget v0, Lcom/google/android/gms/p;->eV:I

    invoke-interface {p1, v1, v2, v1, v0}, Landroid/view/Menu;->add(IIII)Landroid/view/MenuItem;

    .line 343
    return v2
.end method

.method protected onNewIntent(Landroid/content/Intent;)V
    .locals 0

    .prologue
    .line 405
    invoke-super {p0, p1}, Landroid/support/v7/app/d;->onNewIntent(Landroid/content/Intent;)V

    .line 406
    invoke-static {p1}, Lcom/google/android/location/settings/GoogleLocationSettingsActivity;->a(Landroid/content/Intent;)V

    .line 407
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 324
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    const v2, 0x102002c

    if-ne v1, v2, :cond_0

    .line 325
    invoke-virtual {p0}, Lcom/google/android/location/settings/GoogleLocationSettingsActivity;->finish()V

    .line 335
    :goto_0
    return v0

    .line 327
    :cond_0
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    if-ne v1, v0, :cond_1

    .line 328
    const-string v1, "ulr_googlelocation"

    invoke-static {p0, v1}, Lcom/google/android/gsf/i;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 329
    const-string v2, "android_location"

    invoke-static {v2}, Lcom/google/android/gms/googlehelp/GoogleHelp;->a(Ljava/lang/String;)Lcom/google/android/gms/googlehelp/GoogleHelp;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/google/android/gms/googlehelp/GoogleHelp;->a(Landroid/net/Uri;)Lcom/google/android/gms/googlehelp/GoogleHelp;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/googlehelp/GoogleHelp;->a()Landroid/content/Intent;

    move-result-object v1

    .line 332
    new-instance v2, Lcom/google/android/gms/googlehelp/b;

    invoke-direct {v2, p0}, Lcom/google/android/gms/googlehelp/b;-><init>(Landroid/app/Activity;)V

    invoke-virtual {v2, v1}, Lcom/google/android/gms/googlehelp/b;->a(Landroid/content/Intent;)V

    goto :goto_0

    .line 335
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected onPause()V
    .locals 4

    .prologue
    .line 555
    :try_start_0
    iget-object v0, p0, Lcom/google/android/location/settings/GoogleLocationSettingsActivity;->f:Lcom/google/android/location/settings/g;

    invoke-virtual {p0, v0}, Lcom/google/android/location/settings/GoogleLocationSettingsActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    .line 566
    :cond_0
    :goto_0
    invoke-super {p0}, Landroid/support/v7/app/d;->onPause()V

    .line 567
    return-void

    .line 556
    :catch_0
    move-exception v0

    .line 562
    const-string v1, "GCoreLocationSettings"

    const/4 v2, 0x2

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 563
    const-string v1, "GCoreLocationSettings"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Swallowing "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method protected onResume()V
    .locals 0

    .prologue
    .line 424
    invoke-super {p0}, Landroid/support/v7/app/d;->onResume()V

    .line 429
    invoke-direct {p0}, Lcom/google/android/location/settings/GoogleLocationSettingsActivity;->f()V

    .line 430
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 289
    invoke-super {p0, p1}, Landroid/support/v7/app/d;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 290
    const-string v0, "showingLgaayl"

    iget-boolean v1, p0, Lcom/google/android/location/settings/GoogleLocationSettingsActivity;->e:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 291
    return-void
.end method

.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 2

    .prologue
    .line 307
    const-string v0, "GCoreLocationSettings"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 308
    const-string v0, "GCoreLocationSettings"

    const-string v1, "GoogleLocationSettingsActivity.onServiceConnected()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 310
    :cond_0
    invoke-static {p2}, Lcom/google/android/location/reporting/service/m;->a(Landroid/os/IBinder;)Lcom/google/android/location/reporting/service/l;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/settings/GoogleLocationSettingsActivity;->b:Lcom/google/android/location/reporting/service/l;

    .line 311
    invoke-direct {p0}, Lcom/google/android/location/settings/GoogleLocationSettingsActivity;->f()V

    .line 312
    return-void
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 2

    .prologue
    .line 316
    const-string v0, "GCoreLocationSettings"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 317
    const-string v0, "GCoreLocationSettings"

    const-string v1, "GoogleLocationSettingsActivity.onServiceDisconnected()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 319
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/location/settings/GoogleLocationSettingsActivity;->b:Lcom/google/android/location/reporting/service/l;

    .line 320
    return-void
.end method

.method protected onStart()V
    .locals 1

    .prologue
    .line 265
    invoke-super {p0}, Landroid/support/v7/app/d;->onStart()V

    .line 270
    invoke-direct {p0}, Lcom/google/android/location/settings/GoogleLocationSettingsActivity;->e()Z

    .line 274
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/location/settings/GoogleLocationSettingsActivity;->d:Z

    .line 275
    invoke-static {p0, p0}, Lcom/google/android/location/reporting/service/PreferenceService;->a(Landroid/content/Context;Landroid/content/ServiceConnection;)V

    .line 276
    return-void
.end method

.method protected onStop()V
    .locals 1

    .prologue
    .line 280
    invoke-super {p0}, Landroid/support/v7/app/d;->onStop()V

    .line 281
    iget-object v0, p0, Lcom/google/android/location/settings/GoogleLocationSettingsActivity;->b:Lcom/google/android/location/reporting/service/l;

    if-eqz v0, :cond_0

    .line 282
    invoke-virtual {p0, p0}, Lcom/google/android/location/settings/GoogleLocationSettingsActivity;->unbindService(Landroid/content/ServiceConnection;)V

    .line 283
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/location/settings/GoogleLocationSettingsActivity;->b:Lcom/google/android/location/reporting/service/l;

    .line 285
    :cond_0
    return-void
.end method
