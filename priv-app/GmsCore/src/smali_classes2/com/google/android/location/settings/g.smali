.class final Lcom/google/android/location/settings/g;
.super Landroid/content/BroadcastReceiver;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/location/settings/GoogleLocationSettingsActivity;


# direct methods
.method private constructor <init>(Lcom/google/android/location/settings/GoogleLocationSettingsActivity;)V
    .locals 0

    .prologue
    .line 762
    iput-object p1, p0, Lcom/google/android/location/settings/g;->a:Lcom/google/android/location/settings/GoogleLocationSettingsActivity;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/location/settings/GoogleLocationSettingsActivity;B)V
    .locals 0

    .prologue
    .line 762
    invoke-direct {p0, p1}, Lcom/google/android/location/settings/g;-><init>(Lcom/google/android/location/settings/GoogleLocationSettingsActivity;)V

    return-void
.end method


# virtual methods
.method public final onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 766
    const-string v0, "com.google.android.gms.location.reporting.INITIALIZATION_CHANGED"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, "initialization"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    :goto_0
    if-eqz v0, :cond_3

    const-string v0, "initialization"

    invoke-virtual {p2, v0, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 767
    :goto_1
    if-eqz v0, :cond_0

    .line 768
    iget-object v1, p0, Lcom/google/android/location/settings/g;->a:Lcom/google/android/location/settings/GoogleLocationSettingsActivity;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v1, v0}, Lcom/google/android/location/settings/GoogleLocationSettingsActivity;->a(Lcom/google/android/location/settings/GoogleLocationSettingsActivity;I)I

    .line 770
    :cond_0
    const-string v0, "GCoreLocationSettings"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 771
    const-string v0, "GCoreLocationSettings"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "GoogleLocationSettingsActivity received "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " => mInitialization="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/location/settings/g;->a:Lcom/google/android/location/settings/GoogleLocationSettingsActivity;

    invoke-static {v2}, Lcom/google/android/location/settings/GoogleLocationSettingsActivity;->a(Lcom/google/android/location/settings/GoogleLocationSettingsActivity;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 774
    :cond_1
    iget-object v0, p0, Lcom/google/android/location/settings/g;->a:Lcom/google/android/location/settings/GoogleLocationSettingsActivity;

    invoke-static {v0}, Lcom/google/android/location/settings/GoogleLocationSettingsActivity;->c(Lcom/google/android/location/settings/GoogleLocationSettingsActivity;)V

    .line 775
    return-void

    :cond_2
    move v0, v1

    .line 766
    goto :goto_0

    :cond_3
    const/4 v0, 0x0

    goto :goto_1
.end method
