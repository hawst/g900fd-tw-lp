.class final Lcom/google/android/location/places/ui/aw;
.super Lcom/google/android/location/places/ui/az;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/location/places/ui/av;

.field private d:Lcom/google/android/gms/location/places/UserAddedPlace;

.field private e:Lcom/google/android/gms/common/api/am;

.field private f:Lcom/google/android/gms/location/places/w;

.field private g:Lcom/google/android/gms/location/places/f;


# direct methods
.method public constructor <init>(Lcom/google/android/location/places/ui/av;Lcom/google/android/gms/location/places/UserAddedPlace;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 500
    iput-object p1, p0, Lcom/google/android/location/places/ui/aw;->a:Lcom/google/android/location/places/ui/av;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/location/places/ui/az;-><init>(Lcom/google/android/location/places/ui/av;B)V

    .line 496
    iput-object v1, p0, Lcom/google/android/location/places/ui/aw;->e:Lcom/google/android/gms/common/api/am;

    .line 497
    iput-object v1, p0, Lcom/google/android/location/places/ui/aw;->f:Lcom/google/android/gms/location/places/w;

    .line 498
    iput-object v1, p0, Lcom/google/android/location/places/ui/aw;->g:Lcom/google/android/gms/location/places/f;

    .line 501
    iput-object p2, p0, Lcom/google/android/location/places/ui/aw;->d:Lcom/google/android/gms/location/places/UserAddedPlace;

    .line 502
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 552
    invoke-super {p0}, Lcom/google/android/location/places/ui/az;->a()V

    .line 553
    iget-object v0, p0, Lcom/google/android/location/places/ui/aw;->e:Lcom/google/android/gms/common/api/am;

    if-eqz v0, :cond_0

    .line 554
    iget-object v0, p0, Lcom/google/android/location/places/ui/aw;->e:Lcom/google/android/gms/common/api/am;

    invoke-interface {v0}, Lcom/google/android/gms/common/api/am;->b()V

    .line 556
    :cond_0
    return-void
.end method

.method public final run()V
    .locals 6

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x0

    .line 506
    :try_start_0
    sget-object v0, Lcom/google/android/gms/location/places/q;->c:Lcom/google/android/gms/location/places/u;

    iget-object v3, p0, Lcom/google/android/location/places/ui/aw;->a:Lcom/google/android/location/places/ui/av;

    invoke-static {v3}, Lcom/google/android/location/places/ui/av;->b(Lcom/google/android/location/places/ui/av;)Lcom/google/android/gms/common/api/v;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/location/places/ui/aw;->d:Lcom/google/android/gms/location/places/UserAddedPlace;

    invoke-interface {v0, v3, v4}, Lcom/google/android/gms/location/places/u;->a(Lcom/google/android/gms/common/api/v;Lcom/google/android/gms/location/places/UserAddedPlace;)Lcom/google/android/gms/common/api/am;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/places/ui/aw;->e:Lcom/google/android/gms/common/api/am;

    iget-object v3, p0, Lcom/google/android/location/places/ui/aw;->e:Lcom/google/android/gms/common/api/am;

    sget-object v0, Lcom/google/android/location/x;->U:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    sget-object v0, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v3, v4, v5, v0}, Lcom/google/android/gms/common/api/am;->a(JLjava/util/concurrent/TimeUnit;)Lcom/google/android/gms/common/api/ap;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/location/places/w;

    iput-object v0, p0, Lcom/google/android/location/places/ui/aw;->f:Lcom/google/android/gms/location/places/w;

    iget-object v0, p0, Lcom/google/android/location/places/ui/aw;->f:Lcom/google/android/gms/location/places/w;

    invoke-virtual {v0}, Lcom/google/android/gms/location/places/w;->C_()Lcom/google/android/gms/common/api/Status;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/common/api/Status;->f()Z

    move-result v0

    if-nez v0, :cond_1

    const-string v0, "Places"

    const/4 v3, 0x3

    invoke-static {v0, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "Places"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Failed add a place for "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/google/android/location/places/ui/aw;->d:Lcom/google/android/gms/location/places/UserAddedPlace;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Lcom/google/android/location/n/aa;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    move v0, v1

    .line 510
    :goto_0
    iput-object v2, p0, Lcom/google/android/location/places/ui/aw;->e:Lcom/google/android/gms/common/api/am;

    .line 512
    iget-boolean v1, p0, Lcom/google/android/location/places/ui/aw;->b:Z

    if-eqz v1, :cond_5

    .line 523
    :goto_1
    return-void

    .line 506
    :cond_1
    :try_start_1
    iget-object v0, p0, Lcom/google/android/location/places/ui/aw;->f:Lcom/google/android/gms/location/places/w;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/location/places/ui/aw;->f:Lcom/google/android/gms/location/places/w;

    invoke-virtual {v0}, Lcom/google/android/gms/location/places/w;->b()Lcom/google/android/gms/location/places/f;

    move-result-object v0

    :goto_2
    iput-object v0, p0, Lcom/google/android/location/places/ui/aw;->g:Lcom/google/android/gms/location/places/f;

    iget-object v0, p0, Lcom/google/android/location/places/ui/aw;->g:Lcom/google/android/gms/location/places/f;

    if-nez v0, :cond_4

    const-string v0, "Places"

    const/4 v3, 0x3

    invoke-static {v0, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, "Places"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Bad add a place result for "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/google/android/location/places/ui/aw;->d:Lcom/google/android/gms/location/places/UserAddedPlace;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ": "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/location/places/ui/aw;->g:Lcom/google/android/gms/location/places/f;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Lcom/google/android/location/n/aa;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_2
    move v0, v1

    goto :goto_0

    :cond_3
    move-object v0, v2

    goto :goto_2

    :cond_4
    const/4 v0, 0x1

    goto :goto_0

    .line 510
    :catchall_0
    move-exception v0

    iput-object v2, p0, Lcom/google/android/location/places/ui/aw;->e:Lcom/google/android/gms/common/api/am;

    throw v0

    .line 515
    :cond_5
    monitor-enter p0

    .line 516
    :try_start_2
    iget-object v1, p0, Lcom/google/android/location/places/ui/aw;->a:Lcom/google/android/location/places/ui/av;

    invoke-static {v1}, Lcom/google/android/location/places/ui/av;->a(Lcom/google/android/location/places/ui/av;)Lcom/google/android/location/places/ui/ay;

    move-result-object v1

    if-eqz v1, :cond_6

    .line 517
    if-eqz v0, :cond_7

    .line 518
    iget-object v0, p0, Lcom/google/android/location/places/ui/aw;->a:Lcom/google/android/location/places/ui/av;

    invoke-static {v0}, Lcom/google/android/location/places/ui/av;->a(Lcom/google/android/location/places/ui/av;)Lcom/google/android/location/places/ui/ay;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/location/places/ui/aw;->g:Lcom/google/android/gms/location/places/f;

    invoke-interface {v0, v1}, Lcom/google/android/location/places/ui/ay;->c(Lcom/google/android/gms/location/places/f;)V

    .line 523
    :cond_6
    :goto_3
    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    goto :goto_1

    :catchall_1
    move-exception v0

    monitor-exit p0

    throw v0

    .line 520
    :cond_7
    :try_start_3
    iget-object v0, p0, Lcom/google/android/location/places/ui/aw;->a:Lcom/google/android/location/places/ui/av;

    invoke-static {v0}, Lcom/google/android/location/places/ui/av;->a(Lcom/google/android/location/places/ui/av;)Lcom/google/android/location/places/ui/ay;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/location/places/ui/aw;->d:Lcom/google/android/gms/location/places/UserAddedPlace;

    invoke-interface {v0, v1}, Lcom/google/android/location/places/ui/ay;->a(Lcom/google/android/gms/location/places/UserAddedPlace;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    goto :goto_3
.end method
