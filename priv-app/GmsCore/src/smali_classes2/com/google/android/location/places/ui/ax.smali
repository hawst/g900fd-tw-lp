.class final Lcom/google/android/location/places/ui/ax;
.super Lcom/google/android/location/places/ui/az;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/location/places/ui/av;

.field private volatile d:Lcom/google/android/gms/common/api/am;

.field private e:Lcom/google/android/gms/location/places/i;

.field private f:[Lcom/google/android/gms/location/places/f;


# direct methods
.method private constructor <init>(Lcom/google/android/location/places/ui/av;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 433
    iput-object p1, p0, Lcom/google/android/location/places/ui/ax;->a:Lcom/google/android/location/places/ui/av;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/location/places/ui/az;-><init>(Lcom/google/android/location/places/ui/av;B)V

    .line 435
    iput-object v1, p0, Lcom/google/android/location/places/ui/ax;->d:Lcom/google/android/gms/common/api/am;

    .line 436
    iput-object v1, p0, Lcom/google/android/location/places/ui/ax;->e:Lcom/google/android/gms/location/places/i;

    .line 437
    iput-object v1, p0, Lcom/google/android/location/places/ui/ax;->f:[Lcom/google/android/gms/location/places/f;

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/location/places/ui/av;B)V
    .locals 0

    .prologue
    .line 433
    invoke-direct {p0, p1}, Lcom/google/android/location/places/ui/ax;-><init>(Lcom/google/android/location/places/ui/av;)V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 483
    invoke-super {p0}, Lcom/google/android/location/places/ui/az;->a()V

    .line 484
    iget-object v0, p0, Lcom/google/android/location/places/ui/ax;->d:Lcom/google/android/gms/common/api/am;

    if-eqz v0, :cond_0

    .line 485
    iget-object v0, p0, Lcom/google/android/location/places/ui/ax;->d:Lcom/google/android/gms/common/api/am;

    invoke-interface {v0}, Lcom/google/android/gms/common/api/am;->b()V

    .line 487
    :cond_0
    return-void
.end method

.method public final run()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 441
    :try_start_0
    sget-object v0, Lcom/google/android/gms/location/places/q;->c:Lcom/google/android/gms/location/places/u;

    iget-object v1, p0, Lcom/google/android/location/places/ui/ax;->a:Lcom/google/android/location/places/ui/av;

    invoke-static {v1}, Lcom/google/android/location/places/ui/av;->b(Lcom/google/android/location/places/ui/av;)Lcom/google/android/gms/common/api/v;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/location/places/ui/ax;->a:Lcom/google/android/location/places/ui/av;

    invoke-static {v2}, Lcom/google/android/location/places/ui/av;->c(Lcom/google/android/location/places/ui/av;)Lcom/google/android/gms/location/places/PlaceFilter;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/google/android/gms/location/places/u;->a(Lcom/google/android/gms/common/api/v;Lcom/google/android/gms/location/places/PlaceFilter;)Lcom/google/android/gms/common/api/am;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/places/ui/ax;->d:Lcom/google/android/gms/common/api/am;

    iget-object v1, p0, Lcom/google/android/location/places/ui/ax;->d:Lcom/google/android/gms/common/api/am;

    sget-object v0, Lcom/google/android/location/x;->U:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    sget-object v0, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v1, v2, v3, v0}, Lcom/google/android/gms/common/api/am;->a(JLjava/util/concurrent/TimeUnit;)Lcom/google/android/gms/common/api/ap;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/location/places/i;

    iput-object v0, p0, Lcom/google/android/location/places/ui/ax;->e:Lcom/google/android/gms/location/places/i;

    iget-object v0, p0, Lcom/google/android/location/places/ui/ax;->e:Lcom/google/android/gms/location/places/i;

    invoke-virtual {v0}, Lcom/google/android/gms/location/places/i;->C_()Lcom/google/android/gms/common/api/Status;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/common/api/Status;->f()Z

    move-result v0

    if-nez v0, :cond_2

    const-string v0, "Places"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "Places"

    const-string v1, "Failed getCurrentPlace query."

    invoke-static {v0, v1}, Lcom/google/android/location/n/aa;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    const/4 v0, 0x0

    .line 445
    :goto_0
    iput-object v4, p0, Lcom/google/android/location/places/ui/ax;->d:Lcom/google/android/gms/common/api/am;

    .line 446
    iget-object v1, p0, Lcom/google/android/location/places/ui/ax;->e:Lcom/google/android/gms/location/places/i;

    if-eqz v1, :cond_1

    .line 447
    iget-object v1, p0, Lcom/google/android/location/places/ui/ax;->e:Lcom/google/android/gms/location/places/i;

    invoke-virtual {v1}, Lcom/google/android/gms/location/places/i;->w_()V

    .line 450
    :cond_1
    iget-boolean v1, p0, Lcom/google/android/location/places/ui/ax;->b:Z

    if-eqz v1, :cond_4

    .line 461
    :goto_1
    return-void

    .line 441
    :cond_2
    :try_start_1
    iget-object v0, p0, Lcom/google/android/location/places/ui/ax;->e:Lcom/google/android/gms/location/places/i;

    invoke-static {v0}, Lcom/google/android/location/places/ui/av;->a(Lcom/google/android/gms/location/places/i;)[Lcom/google/android/gms/location/places/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/places/ui/ax;->f:[Lcom/google/android/gms/location/places/f;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    const/4 v0, 0x1

    goto :goto_0

    .line 445
    :catchall_0
    move-exception v0

    iput-object v4, p0, Lcom/google/android/location/places/ui/ax;->d:Lcom/google/android/gms/common/api/am;

    .line 446
    iget-object v1, p0, Lcom/google/android/location/places/ui/ax;->e:Lcom/google/android/gms/location/places/i;

    if-eqz v1, :cond_3

    .line 447
    iget-object v1, p0, Lcom/google/android/location/places/ui/ax;->e:Lcom/google/android/gms/location/places/i;

    invoke-virtual {v1}, Lcom/google/android/gms/location/places/i;->w_()V

    :cond_3
    throw v0

    .line 453
    :cond_4
    monitor-enter p0

    .line 454
    :try_start_2
    iget-object v1, p0, Lcom/google/android/location/places/ui/ax;->a:Lcom/google/android/location/places/ui/av;

    invoke-static {v1}, Lcom/google/android/location/places/ui/av;->a(Lcom/google/android/location/places/ui/av;)Lcom/google/android/location/places/ui/ay;

    move-result-object v1

    if-eqz v1, :cond_5

    .line 455
    if-eqz v0, :cond_6

    .line 456
    iget-object v0, p0, Lcom/google/android/location/places/ui/ax;->a:Lcom/google/android/location/places/ui/av;

    invoke-static {v0}, Lcom/google/android/location/places/ui/av;->a(Lcom/google/android/location/places/ui/av;)Lcom/google/android/location/places/ui/ay;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/location/places/ui/ax;->f:[Lcom/google/android/gms/location/places/f;

    invoke-interface {v0, v1}, Lcom/google/android/location/places/ui/ay;->a([Lcom/google/android/gms/location/places/f;)V

    .line 461
    :cond_5
    :goto_2
    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    goto :goto_1

    :catchall_1
    move-exception v0

    monitor-exit p0

    throw v0

    .line 458
    :cond_6
    :try_start_3
    iget-object v0, p0, Lcom/google/android/location/places/ui/ax;->a:Lcom/google/android/location/places/ui/av;

    invoke-static {v0}, Lcom/google/android/location/places/ui/av;->a(Lcom/google/android/location/places/ui/av;)Lcom/google/android/location/places/ui/ay;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/google/android/location/places/ui/ay;->a([Lcom/google/android/gms/location/places/f;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    goto :goto_2
.end method
