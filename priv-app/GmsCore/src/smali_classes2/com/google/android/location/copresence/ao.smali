.class public final Lcom/google/android/location/copresence/ao;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public final a:Lcom/google/ac/b/c/ah;

.field public final b:Ljava/util/Set;

.field public final c:Ljava/util/Set;

.field public final d:Z

.field public final e:I

.field f:J

.field final synthetic g:Lcom/google/android/location/copresence/an;


# direct methods
.method constructor <init>(Lcom/google/android/location/copresence/an;JLcom/google/ac/b/c/ah;Z)V
    .locals 2

    .prologue
    .line 326
    iput-object p1, p0, Lcom/google/android/location/copresence/ao;->g:Lcom/google/android/location/copresence/an;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 327
    iput-wide p2, p0, Lcom/google/android/location/copresence/ao;->f:J

    .line 328
    iput-object p4, p0, Lcom/google/android/location/copresence/ao;->a:Lcom/google/ac/b/c/ah;

    .line 329
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/location/copresence/ao;->b:Ljava/util/Set;

    .line 330
    iget-object v0, p4, Lcom/google/ac/b/c/ah;->e:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 331
    iget-object v0, p0, Lcom/google/android/location/copresence/ao;->b:Ljava/util/Set;

    iget-object v1, p4, Lcom/google/ac/b/c/ah;->e:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 333
    :cond_0
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/location/copresence/ao;->c:Ljava/util/Set;

    .line 334
    iget-object v0, p4, Lcom/google/ac/b/c/ah;->f:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 335
    iget-object v0, p0, Lcom/google/android/location/copresence/ao;->c:Ljava/util/Set;

    iget-object v1, p4, Lcom/google/ac/b/c/ah;->f:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 337
    :cond_1
    iput-boolean p5, p0, Lcom/google/android/location/copresence/ao;->d:Z

    .line 338
    invoke-static {}, Lcom/google/android/location/copresence/an;->d()I

    move-result v0

    iput v0, p0, Lcom/google/android/location/copresence/ao;->e:I

    .line 339
    return-void
.end method


# virtual methods
.method public final a()J
    .locals 4

    .prologue
    .line 346
    iget-wide v0, p0, Lcom/google/android/location/copresence/ao;->f:J

    iget-object v2, p0, Lcom/google/android/location/copresence/ao;->a:Lcom/google/ac/b/c/ah;

    iget-object v2, v2, Lcom/google/ac/b/c/ah;->c:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    add-long/2addr v0, v2

    return-wide v0
.end method

.method public final b()J
    .locals 4

    .prologue
    .line 350
    iget-object v0, p0, Lcom/google/android/location/copresence/ao;->a:Lcom/google/ac/b/c/ah;

    iget-object v0, v0, Lcom/google/ac/b/c/ah;->d:Ljava/lang/Long;

    if-nez v0, :cond_0

    .line 351
    const-wide v0, 0x7fffffffffffffffL

    .line 353
    :goto_0
    return-wide v0

    :cond_0
    iget-wide v0, p0, Lcom/google/android/location/copresence/ao;->f:J

    iget-object v2, p0, Lcom/google/android/location/copresence/ao;->a:Lcom/google/ac/b/c/ah;

    iget-object v2, v2, Lcom/google/ac/b/c/ah;->d:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    add-long/2addr v0, v2

    goto :goto_0
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 357
    iget-object v0, p0, Lcom/google/android/location/copresence/ao;->b:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/location/copresence/ao;->c:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 362
    iget-object v0, p0, Lcom/google/android/location/copresence/ao;->a:Lcom/google/ac/b/c/ah;

    invoke-virtual {v0}, Lcom/google/ac/b/c/ah;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
