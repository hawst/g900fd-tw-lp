.class public final Lcom/google/android/location/places/f/a;
.super Lcom/google/android/location/places/c;
.source "SourceFile"


# instance fields
.field final e:Lcom/google/android/location/g/q;

.field private final f:Lcom/google/android/location/places/af;


# direct methods
.method public constructor <init>(Lcom/google/android/location/j/b;Ljava/util/concurrent/Executor;Ljava/util/concurrent/Executor;Lcom/google/android/location/places/af;Lcom/google/android/location/g/s;)V
    .locals 1

    .prologue
    .line 39
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/location/places/c;-><init>(Lcom/google/android/location/j/b;Ljava/util/concurrent/Executor;Ljava/util/concurrent/Executor;)V

    .line 40
    iput-object p4, p0, Lcom/google/android/location/places/f/a;->f:Lcom/google/android/location/places/af;

    .line 41
    new-instance v0, Lcom/google/android/location/g/q;

    invoke-direct {v0, p5}, Lcom/google/android/location/g/q;-><init>(Lcom/google/android/location/g/s;)V

    iput-object v0, p0, Lcom/google/android/location/places/f/a;->e:Lcom/google/android/location/g/q;

    .line 42
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lcom/google/android/location/places/f/a;->f:Lcom/google/android/location/places/af;

    invoke-virtual {v0}, Lcom/google/android/location/places/af;->a()Lcom/google/android/location/places/ad;

    const/16 v0, 0x32

    return v0
.end method

.method public final a(Lcom/google/android/location/e/bi;)V
    .locals 2

    .prologue
    .line 46
    if-eqz p1, :cond_0

    .line 47
    new-instance v0, Lcom/google/android/location/places/f/b;

    invoke-direct {v0, p0, p1}, Lcom/google/android/location/places/f/b;-><init>(Lcom/google/android/location/places/f/a;Lcom/google/android/location/e/bi;)V

    iget-object v1, p0, Lcom/google/android/location/places/c;->c:Ljava/util/concurrent/Executor;

    invoke-interface {v1, v0}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 49
    :cond_0
    return-void
.end method
