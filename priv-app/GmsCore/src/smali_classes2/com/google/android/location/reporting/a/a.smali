.class public final Lcom/google/android/location/reporting/a/a;
.super Lcom/google/protobuf/nano/d;
.source "SourceFile"


# static fields
.field private static volatile q:[Lcom/google/android/location/reporting/a/a;


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/lang/String;

.field public c:Ljava/lang/Boolean;

.field public d:Ljava/lang/Long;

.field public e:Ljava/lang/Boolean;

.field public f:Ljava/lang/Boolean;

.field public g:Ljava/lang/Boolean;

.field public h:Ljava/lang/Boolean;

.field public i:Ljava/lang/Long;

.field public j:Ljava/lang/Integer;

.field public k:Ljava/lang/Boolean;

.field public l:Ljava/lang/Boolean;

.field public m:Lcom/google/android/location/reporting/a/c;

.field public n:Ljava/lang/Integer;

.field public o:Ljava/lang/Boolean;

.field public p:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 71
    invoke-direct {p0}, Lcom/google/protobuf/nano/d;-><init>()V

    .line 72
    iput-object v0, p0, Lcom/google/android/location/reporting/a/a;->a:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/location/reporting/a/a;->b:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/location/reporting/a/a;->c:Ljava/lang/Boolean;

    iput-object v0, p0, Lcom/google/android/location/reporting/a/a;->d:Ljava/lang/Long;

    iput-object v0, p0, Lcom/google/android/location/reporting/a/a;->e:Ljava/lang/Boolean;

    iput-object v0, p0, Lcom/google/android/location/reporting/a/a;->f:Ljava/lang/Boolean;

    iput-object v0, p0, Lcom/google/android/location/reporting/a/a;->g:Ljava/lang/Boolean;

    iput-object v0, p0, Lcom/google/android/location/reporting/a/a;->h:Ljava/lang/Boolean;

    iput-object v0, p0, Lcom/google/android/location/reporting/a/a;->i:Ljava/lang/Long;

    iput-object v0, p0, Lcom/google/android/location/reporting/a/a;->j:Ljava/lang/Integer;

    iput-object v0, p0, Lcom/google/android/location/reporting/a/a;->k:Ljava/lang/Boolean;

    iput-object v0, p0, Lcom/google/android/location/reporting/a/a;->l:Ljava/lang/Boolean;

    iput-object v0, p0, Lcom/google/android/location/reporting/a/a;->m:Lcom/google/android/location/reporting/a/c;

    iput-object v0, p0, Lcom/google/android/location/reporting/a/a;->n:Ljava/lang/Integer;

    iput-object v0, p0, Lcom/google/android/location/reporting/a/a;->o:Ljava/lang/Boolean;

    iput-object v0, p0, Lcom/google/android/location/reporting/a/a;->p:Ljava/lang/Boolean;

    iput-object v0, p0, Lcom/google/android/location/reporting/a/a;->unknownFieldData:Lcom/google/protobuf/nano/f;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/location/reporting/a/a;->cachedSize:I

    .line 73
    return-void
.end method

.method public static a()[Lcom/google/android/location/reporting/a/a;
    .locals 2

    .prologue
    .line 12
    sget-object v0, Lcom/google/android/location/reporting/a/a;->q:[Lcom/google/android/location/reporting/a/a;

    if-nez v0, :cond_1

    .line 13
    sget-object v1, Lcom/google/protobuf/nano/h;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 15
    :try_start_0
    sget-object v0, Lcom/google/android/location/reporting/a/a;->q:[Lcom/google/android/location/reporting/a/a;

    if-nez v0, :cond_0

    .line 16
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/android/location/reporting/a/a;

    sput-object v0, Lcom/google/android/location/reporting/a/a;->q:[Lcom/google/android/location/reporting/a/a;

    .line 18
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 20
    :cond_1
    sget-object v0, Lcom/google/android/location/reporting/a/a;->q:[Lcom/google/android/location/reporting/a/a;

    return-object v0

    .line 18
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 4

    .prologue
    .line 318
    invoke-super {p0}, Lcom/google/protobuf/nano/d;->computeSerializedSize()I

    move-result v0

    .line 319
    iget-object v1, p0, Lcom/google/android/location/reporting/a/a;->a:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 320
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/location/reporting/a/a;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 323
    :cond_0
    iget-object v1, p0, Lcom/google/android/location/reporting/a/a;->b:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 324
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/location/reporting/a/a;->b:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 327
    :cond_1
    iget-object v1, p0, Lcom/google/android/location/reporting/a/a;->c:Ljava/lang/Boolean;

    if-eqz v1, :cond_2

    .line 328
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/android/location/reporting/a/a;->c:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 331
    :cond_2
    iget-object v1, p0, Lcom/google/android/location/reporting/a/a;->d:Ljava/lang/Long;

    if-eqz v1, :cond_3

    .line 332
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/android/location/reporting/a/a;->d:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/b;->f(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 335
    :cond_3
    iget-object v1, p0, Lcom/google/android/location/reporting/a/a;->e:Ljava/lang/Boolean;

    if-eqz v1, :cond_4

    .line 336
    const/4 v1, 0x5

    iget-object v2, p0, Lcom/google/android/location/reporting/a/a;->e:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 339
    :cond_4
    iget-object v1, p0, Lcom/google/android/location/reporting/a/a;->f:Ljava/lang/Boolean;

    if-eqz v1, :cond_5

    .line 340
    const/4 v1, 0x6

    iget-object v2, p0, Lcom/google/android/location/reporting/a/a;->f:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 343
    :cond_5
    iget-object v1, p0, Lcom/google/android/location/reporting/a/a;->g:Ljava/lang/Boolean;

    if-eqz v1, :cond_6

    .line 344
    const/4 v1, 0x7

    iget-object v2, p0, Lcom/google/android/location/reporting/a/a;->g:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 347
    :cond_6
    iget-object v1, p0, Lcom/google/android/location/reporting/a/a;->h:Ljava/lang/Boolean;

    if-eqz v1, :cond_7

    .line 348
    const/16 v1, 0x8

    iget-object v2, p0, Lcom/google/android/location/reporting/a/a;->h:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 351
    :cond_7
    iget-object v1, p0, Lcom/google/android/location/reporting/a/a;->i:Ljava/lang/Long;

    if-eqz v1, :cond_8

    .line 352
    const/16 v1, 0x9

    iget-object v2, p0, Lcom/google/android/location/reporting/a/a;->i:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/b;->f(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 355
    :cond_8
    iget-object v1, p0, Lcom/google/android/location/reporting/a/a;->j:Ljava/lang/Integer;

    if-eqz v1, :cond_9

    .line 356
    const/16 v1, 0xa

    iget-object v2, p0, Lcom/google/android/location/reporting/a/a;->j:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 359
    :cond_9
    iget-object v1, p0, Lcom/google/android/location/reporting/a/a;->k:Ljava/lang/Boolean;

    if-eqz v1, :cond_a

    .line 360
    const/16 v1, 0xb

    iget-object v2, p0, Lcom/google/android/location/reporting/a/a;->k:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 363
    :cond_a
    iget-object v1, p0, Lcom/google/android/location/reporting/a/a;->l:Ljava/lang/Boolean;

    if-eqz v1, :cond_b

    .line 364
    const/16 v1, 0xc

    iget-object v2, p0, Lcom/google/android/location/reporting/a/a;->l:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 367
    :cond_b
    iget-object v1, p0, Lcom/google/android/location/reporting/a/a;->m:Lcom/google/android/location/reporting/a/c;

    if-eqz v1, :cond_c

    .line 368
    const/16 v1, 0xd

    iget-object v2, p0, Lcom/google/android/location/reporting/a/a;->m:Lcom/google/android/location/reporting/a/c;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 371
    :cond_c
    iget-object v1, p0, Lcom/google/android/location/reporting/a/a;->n:Ljava/lang/Integer;

    if-eqz v1, :cond_d

    .line 372
    const/16 v1, 0xe

    iget-object v2, p0, Lcom/google/android/location/reporting/a/a;->n:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 375
    :cond_d
    iget-object v1, p0, Lcom/google/android/location/reporting/a/a;->o:Ljava/lang/Boolean;

    if-eqz v1, :cond_e

    .line 376
    const/16 v1, 0xf

    iget-object v2, p0, Lcom/google/android/location/reporting/a/a;->o:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 379
    :cond_e
    iget-object v1, p0, Lcom/google/android/location/reporting/a/a;->p:Ljava/lang/Boolean;

    if-eqz v1, :cond_f

    .line 380
    const/16 v1, 0x10

    iget-object v2, p0, Lcom/google/android/location/reporting/a/a;->p:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 383
    :cond_f
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 99
    if-ne p1, p0, :cond_1

    .line 100
    const/4 v0, 0x1

    .line 220
    :cond_0
    :goto_0
    return v0

    .line 102
    :cond_1
    instance-of v1, p1, Lcom/google/android/location/reporting/a/a;

    if-eqz v1, :cond_0

    .line 105
    check-cast p1, Lcom/google/android/location/reporting/a/a;

    .line 106
    iget-object v1, p0, Lcom/google/android/location/reporting/a/a;->a:Ljava/lang/String;

    if-nez v1, :cond_12

    .line 107
    iget-object v1, p1, Lcom/google/android/location/reporting/a/a;->a:Ljava/lang/String;

    if-nez v1, :cond_0

    .line 113
    :cond_2
    iget-object v1, p0, Lcom/google/android/location/reporting/a/a;->b:Ljava/lang/String;

    if-nez v1, :cond_13

    .line 114
    iget-object v1, p1, Lcom/google/android/location/reporting/a/a;->b:Ljava/lang/String;

    if-nez v1, :cond_0

    .line 120
    :cond_3
    iget-object v1, p0, Lcom/google/android/location/reporting/a/a;->c:Ljava/lang/Boolean;

    if-nez v1, :cond_14

    .line 121
    iget-object v1, p1, Lcom/google/android/location/reporting/a/a;->c:Ljava/lang/Boolean;

    if-nez v1, :cond_0

    .line 127
    :cond_4
    iget-object v1, p0, Lcom/google/android/location/reporting/a/a;->d:Ljava/lang/Long;

    if-nez v1, :cond_15

    .line 128
    iget-object v1, p1, Lcom/google/android/location/reporting/a/a;->d:Ljava/lang/Long;

    if-nez v1, :cond_0

    .line 134
    :cond_5
    iget-object v1, p0, Lcom/google/android/location/reporting/a/a;->e:Ljava/lang/Boolean;

    if-nez v1, :cond_16

    .line 135
    iget-object v1, p1, Lcom/google/android/location/reporting/a/a;->e:Ljava/lang/Boolean;

    if-nez v1, :cond_0

    .line 141
    :cond_6
    iget-object v1, p0, Lcom/google/android/location/reporting/a/a;->f:Ljava/lang/Boolean;

    if-nez v1, :cond_17

    .line 142
    iget-object v1, p1, Lcom/google/android/location/reporting/a/a;->f:Ljava/lang/Boolean;

    if-nez v1, :cond_0

    .line 148
    :cond_7
    iget-object v1, p0, Lcom/google/android/location/reporting/a/a;->g:Ljava/lang/Boolean;

    if-nez v1, :cond_18

    .line 149
    iget-object v1, p1, Lcom/google/android/location/reporting/a/a;->g:Ljava/lang/Boolean;

    if-nez v1, :cond_0

    .line 155
    :cond_8
    iget-object v1, p0, Lcom/google/android/location/reporting/a/a;->h:Ljava/lang/Boolean;

    if-nez v1, :cond_19

    .line 156
    iget-object v1, p1, Lcom/google/android/location/reporting/a/a;->h:Ljava/lang/Boolean;

    if-nez v1, :cond_0

    .line 162
    :cond_9
    iget-object v1, p0, Lcom/google/android/location/reporting/a/a;->i:Ljava/lang/Long;

    if-nez v1, :cond_1a

    .line 163
    iget-object v1, p1, Lcom/google/android/location/reporting/a/a;->i:Ljava/lang/Long;

    if-nez v1, :cond_0

    .line 169
    :cond_a
    iget-object v1, p0, Lcom/google/android/location/reporting/a/a;->j:Ljava/lang/Integer;

    if-nez v1, :cond_1b

    .line 170
    iget-object v1, p1, Lcom/google/android/location/reporting/a/a;->j:Ljava/lang/Integer;

    if-nez v1, :cond_0

    .line 176
    :cond_b
    iget-object v1, p0, Lcom/google/android/location/reporting/a/a;->k:Ljava/lang/Boolean;

    if-nez v1, :cond_1c

    .line 177
    iget-object v1, p1, Lcom/google/android/location/reporting/a/a;->k:Ljava/lang/Boolean;

    if-nez v1, :cond_0

    .line 183
    :cond_c
    iget-object v1, p0, Lcom/google/android/location/reporting/a/a;->l:Ljava/lang/Boolean;

    if-nez v1, :cond_1d

    .line 184
    iget-object v1, p1, Lcom/google/android/location/reporting/a/a;->l:Ljava/lang/Boolean;

    if-nez v1, :cond_0

    .line 190
    :cond_d
    iget-object v1, p0, Lcom/google/android/location/reporting/a/a;->m:Lcom/google/android/location/reporting/a/c;

    if-nez v1, :cond_1e

    .line 191
    iget-object v1, p1, Lcom/google/android/location/reporting/a/a;->m:Lcom/google/android/location/reporting/a/c;

    if-nez v1, :cond_0

    .line 199
    :cond_e
    iget-object v1, p0, Lcom/google/android/location/reporting/a/a;->n:Ljava/lang/Integer;

    if-nez v1, :cond_1f

    .line 200
    iget-object v1, p1, Lcom/google/android/location/reporting/a/a;->n:Ljava/lang/Integer;

    if-nez v1, :cond_0

    .line 206
    :cond_f
    iget-object v1, p0, Lcom/google/android/location/reporting/a/a;->o:Ljava/lang/Boolean;

    if-nez v1, :cond_20

    .line 207
    iget-object v1, p1, Lcom/google/android/location/reporting/a/a;->o:Ljava/lang/Boolean;

    if-nez v1, :cond_0

    .line 213
    :cond_10
    iget-object v1, p0, Lcom/google/android/location/reporting/a/a;->p:Ljava/lang/Boolean;

    if-nez v1, :cond_21

    .line 214
    iget-object v1, p1, Lcom/google/android/location/reporting/a/a;->p:Ljava/lang/Boolean;

    if-nez v1, :cond_0

    .line 220
    :cond_11
    invoke-virtual {p0, p1}, Lcom/google/android/location/reporting/a/a;->unknownFieldDataEquals(Lcom/google/protobuf/nano/d;)Z

    move-result v0

    goto/16 :goto_0

    .line 110
    :cond_12
    iget-object v1, p0, Lcom/google/android/location/reporting/a/a;->a:Ljava/lang/String;

    iget-object v2, p1, Lcom/google/android/location/reporting/a/a;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    goto/16 :goto_0

    .line 117
    :cond_13
    iget-object v1, p0, Lcom/google/android/location/reporting/a/a;->b:Ljava/lang/String;

    iget-object v2, p1, Lcom/google/android/location/reporting/a/a;->b:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    goto/16 :goto_0

    .line 124
    :cond_14
    iget-object v1, p0, Lcom/google/android/location/reporting/a/a;->c:Ljava/lang/Boolean;

    iget-object v2, p1, Lcom/google/android/location/reporting/a/a;->c:Ljava/lang/Boolean;

    invoke-virtual {v1, v2}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    goto/16 :goto_0

    .line 131
    :cond_15
    iget-object v1, p0, Lcom/google/android/location/reporting/a/a;->d:Ljava/lang/Long;

    iget-object v2, p1, Lcom/google/android/location/reporting/a/a;->d:Ljava/lang/Long;

    invoke-virtual {v1, v2}, Ljava/lang/Long;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_5

    goto/16 :goto_0

    .line 138
    :cond_16
    iget-object v1, p0, Lcom/google/android/location/reporting/a/a;->e:Ljava/lang/Boolean;

    iget-object v2, p1, Lcom/google/android/location/reporting/a/a;->e:Ljava/lang/Boolean;

    invoke-virtual {v1, v2}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_6

    goto/16 :goto_0

    .line 145
    :cond_17
    iget-object v1, p0, Lcom/google/android/location/reporting/a/a;->f:Ljava/lang/Boolean;

    iget-object v2, p1, Lcom/google/android/location/reporting/a/a;->f:Ljava/lang/Boolean;

    invoke-virtual {v1, v2}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_7

    goto/16 :goto_0

    .line 152
    :cond_18
    iget-object v1, p0, Lcom/google/android/location/reporting/a/a;->g:Ljava/lang/Boolean;

    iget-object v2, p1, Lcom/google/android/location/reporting/a/a;->g:Ljava/lang/Boolean;

    invoke-virtual {v1, v2}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_8

    goto/16 :goto_0

    .line 159
    :cond_19
    iget-object v1, p0, Lcom/google/android/location/reporting/a/a;->h:Ljava/lang/Boolean;

    iget-object v2, p1, Lcom/google/android/location/reporting/a/a;->h:Ljava/lang/Boolean;

    invoke-virtual {v1, v2}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_9

    goto/16 :goto_0

    .line 166
    :cond_1a
    iget-object v1, p0, Lcom/google/android/location/reporting/a/a;->i:Ljava/lang/Long;

    iget-object v2, p1, Lcom/google/android/location/reporting/a/a;->i:Ljava/lang/Long;

    invoke-virtual {v1, v2}, Ljava/lang/Long;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_a

    goto/16 :goto_0

    .line 173
    :cond_1b
    iget-object v1, p0, Lcom/google/android/location/reporting/a/a;->j:Ljava/lang/Integer;

    iget-object v2, p1, Lcom/google/android/location/reporting/a/a;->j:Ljava/lang/Integer;

    invoke-virtual {v1, v2}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_b

    goto/16 :goto_0

    .line 180
    :cond_1c
    iget-object v1, p0, Lcom/google/android/location/reporting/a/a;->k:Ljava/lang/Boolean;

    iget-object v2, p1, Lcom/google/android/location/reporting/a/a;->k:Ljava/lang/Boolean;

    invoke-virtual {v1, v2}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_c

    goto/16 :goto_0

    .line 187
    :cond_1d
    iget-object v1, p0, Lcom/google/android/location/reporting/a/a;->l:Ljava/lang/Boolean;

    iget-object v2, p1, Lcom/google/android/location/reporting/a/a;->l:Ljava/lang/Boolean;

    invoke-virtual {v1, v2}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_d

    goto/16 :goto_0

    .line 195
    :cond_1e
    iget-object v1, p0, Lcom/google/android/location/reporting/a/a;->m:Lcom/google/android/location/reporting/a/c;

    iget-object v2, p1, Lcom/google/android/location/reporting/a/a;->m:Lcom/google/android/location/reporting/a/c;

    invoke-virtual {v1, v2}, Lcom/google/android/location/reporting/a/c;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_e

    goto/16 :goto_0

    .line 203
    :cond_1f
    iget-object v1, p0, Lcom/google/android/location/reporting/a/a;->n:Ljava/lang/Integer;

    iget-object v2, p1, Lcom/google/android/location/reporting/a/a;->n:Ljava/lang/Integer;

    invoke-virtual {v1, v2}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_f

    goto/16 :goto_0

    .line 210
    :cond_20
    iget-object v1, p0, Lcom/google/android/location/reporting/a/a;->o:Ljava/lang/Boolean;

    iget-object v2, p1, Lcom/google/android/location/reporting/a/a;->o:Ljava/lang/Boolean;

    invoke-virtual {v1, v2}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_10

    goto/16 :goto_0

    .line 217
    :cond_21
    iget-object v1, p0, Lcom/google/android/location/reporting/a/a;->p:Ljava/lang/Boolean;

    iget-object v2, p1, Lcom/google/android/location/reporting/a/a;->p:Ljava/lang/Boolean;

    invoke-virtual {v1, v2}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_11

    goto/16 :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 225
    iget-object v0, p0, Lcom/google/android/location/reporting/a/a;->a:Ljava/lang/String;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    .line 228
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/location/reporting/a/a;->b:Ljava/lang/String;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    .line 230
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/location/reporting/a/a;->c:Ljava/lang/Boolean;

    if-nez v0, :cond_2

    move v0, v1

    :goto_2
    add-int/2addr v0, v2

    .line 232
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/location/reporting/a/a;->d:Ljava/lang/Long;

    if-nez v0, :cond_3

    move v0, v1

    :goto_3
    add-int/2addr v0, v2

    .line 234
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/location/reporting/a/a;->e:Ljava/lang/Boolean;

    if-nez v0, :cond_4

    move v0, v1

    :goto_4
    add-int/2addr v0, v2

    .line 236
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/location/reporting/a/a;->f:Ljava/lang/Boolean;

    if-nez v0, :cond_5

    move v0, v1

    :goto_5
    add-int/2addr v0, v2

    .line 238
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/location/reporting/a/a;->g:Ljava/lang/Boolean;

    if-nez v0, :cond_6

    move v0, v1

    :goto_6
    add-int/2addr v0, v2

    .line 240
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/location/reporting/a/a;->h:Ljava/lang/Boolean;

    if-nez v0, :cond_7

    move v0, v1

    :goto_7
    add-int/2addr v0, v2

    .line 242
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/location/reporting/a/a;->i:Ljava/lang/Long;

    if-nez v0, :cond_8

    move v0, v1

    :goto_8
    add-int/2addr v0, v2

    .line 244
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/location/reporting/a/a;->j:Ljava/lang/Integer;

    if-nez v0, :cond_9

    move v0, v1

    :goto_9
    add-int/2addr v0, v2

    .line 246
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/location/reporting/a/a;->k:Ljava/lang/Boolean;

    if-nez v0, :cond_a

    move v0, v1

    :goto_a
    add-int/2addr v0, v2

    .line 248
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/location/reporting/a/a;->l:Ljava/lang/Boolean;

    if-nez v0, :cond_b

    move v0, v1

    :goto_b
    add-int/2addr v0, v2

    .line 250
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/location/reporting/a/a;->m:Lcom/google/android/location/reporting/a/c;

    if-nez v0, :cond_c

    move v0, v1

    :goto_c
    add-int/2addr v0, v2

    .line 252
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/location/reporting/a/a;->n:Ljava/lang/Integer;

    if-nez v0, :cond_d

    move v0, v1

    :goto_d
    add-int/2addr v0, v2

    .line 254
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/location/reporting/a/a;->o:Ljava/lang/Boolean;

    if-nez v0, :cond_e

    move v0, v1

    :goto_e
    add-int/2addr v0, v2

    .line 256
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/android/location/reporting/a/a;->p:Ljava/lang/Boolean;

    if-nez v2, :cond_f

    :goto_f
    add-int/2addr v0, v1

    .line 258
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/google/android/location/reporting/a/a;->unknownFieldDataHashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 259
    return v0

    .line 225
    :cond_0
    iget-object v0, p0, Lcom/google/android/location/reporting/a/a;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto/16 :goto_0

    .line 228
    :cond_1
    iget-object v0, p0, Lcom/google/android/location/reporting/a/a;->b:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto/16 :goto_1

    .line 230
    :cond_2
    iget-object v0, p0, Lcom/google/android/location/reporting/a/a;->c:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->hashCode()I

    move-result v0

    goto/16 :goto_2

    .line 232
    :cond_3
    iget-object v0, p0, Lcom/google/android/location/reporting/a/a;->d:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->hashCode()I

    move-result v0

    goto/16 :goto_3

    .line 234
    :cond_4
    iget-object v0, p0, Lcom/google/android/location/reporting/a/a;->e:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->hashCode()I

    move-result v0

    goto/16 :goto_4

    .line 236
    :cond_5
    iget-object v0, p0, Lcom/google/android/location/reporting/a/a;->f:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->hashCode()I

    move-result v0

    goto/16 :goto_5

    .line 238
    :cond_6
    iget-object v0, p0, Lcom/google/android/location/reporting/a/a;->g:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->hashCode()I

    move-result v0

    goto/16 :goto_6

    .line 240
    :cond_7
    iget-object v0, p0, Lcom/google/android/location/reporting/a/a;->h:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->hashCode()I

    move-result v0

    goto/16 :goto_7

    .line 242
    :cond_8
    iget-object v0, p0, Lcom/google/android/location/reporting/a/a;->i:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->hashCode()I

    move-result v0

    goto/16 :goto_8

    .line 244
    :cond_9
    iget-object v0, p0, Lcom/google/android/location/reporting/a/a;->j:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->hashCode()I

    move-result v0

    goto/16 :goto_9

    .line 246
    :cond_a
    iget-object v0, p0, Lcom/google/android/location/reporting/a/a;->k:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->hashCode()I

    move-result v0

    goto/16 :goto_a

    .line 248
    :cond_b
    iget-object v0, p0, Lcom/google/android/location/reporting/a/a;->l:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->hashCode()I

    move-result v0

    goto/16 :goto_b

    .line 250
    :cond_c
    iget-object v0, p0, Lcom/google/android/location/reporting/a/a;->m:Lcom/google/android/location/reporting/a/c;

    invoke-virtual {v0}, Lcom/google/android/location/reporting/a/c;->hashCode()I

    move-result v0

    goto/16 :goto_c

    .line 252
    :cond_d
    iget-object v0, p0, Lcom/google/android/location/reporting/a/a;->n:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->hashCode()I

    move-result v0

    goto/16 :goto_d

    .line 254
    :cond_e
    iget-object v0, p0, Lcom/google/android/location/reporting/a/a;->o:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->hashCode()I

    move-result v0

    goto/16 :goto_e

    .line 256
    :cond_f
    iget-object v1, p0, Lcom/google/android/location/reporting/a/a;->p:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto/16 :goto_f
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 2

    .prologue
    .line 5
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/android/location/reporting/a/a;->storeUnknownField(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/reporting/a/a;->a:Ljava/lang/String;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/reporting/a/a;->b:Ljava/lang/String;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/reporting/a/a;->c:Ljava/lang/Boolean;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->j()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/reporting/a/a;->d:Ljava/lang/Long;

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/reporting/a/a;->e:Ljava/lang/Boolean;

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/reporting/a/a;->f:Ljava/lang/Boolean;

    goto :goto_0

    :sswitch_7
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/reporting/a/a;->g:Ljava/lang/Boolean;

    goto :goto_0

    :sswitch_8
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/reporting/a/a;->h:Ljava/lang/Boolean;

    goto :goto_0

    :sswitch_9
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->j()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/reporting/a/a;->i:Ljava/lang/Long;

    goto :goto_0

    :sswitch_a
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/reporting/a/a;->j:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_b
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/reporting/a/a;->k:Ljava/lang/Boolean;

    goto :goto_0

    :sswitch_c
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/reporting/a/a;->l:Ljava/lang/Boolean;

    goto/16 :goto_0

    :sswitch_d
    iget-object v0, p0, Lcom/google/android/location/reporting/a/a;->m:Lcom/google/android/location/reporting/a/c;

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/android/location/reporting/a/c;

    invoke-direct {v0}, Lcom/google/android/location/reporting/a/c;-><init>()V

    iput-object v0, p0, Lcom/google/android/location/reporting/a/a;->m:Lcom/google/android/location/reporting/a/c;

    :cond_1
    iget-object v0, p0, Lcom/google/android/location/reporting/a/a;->m:Lcom/google/android/location/reporting/a/c;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto/16 :goto_0

    :sswitch_e
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/reporting/a/a;->n:Ljava/lang/Integer;

    goto/16 :goto_0

    :sswitch_f
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/reporting/a/a;->o:Ljava/lang/Boolean;

    goto/16 :goto_0

    :sswitch_10
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/reporting/a/a;->p:Ljava/lang/Boolean;

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
        0x30 -> :sswitch_6
        0x38 -> :sswitch_7
        0x40 -> :sswitch_8
        0x48 -> :sswitch_9
        0x50 -> :sswitch_a
        0x58 -> :sswitch_b
        0x60 -> :sswitch_c
        0x6a -> :sswitch_d
        0x70 -> :sswitch_e
        0x78 -> :sswitch_f
        0x80 -> :sswitch_10
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 4

    .prologue
    .line 265
    iget-object v0, p0, Lcom/google/android/location/reporting/a/a;->a:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 266
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/location/reporting/a/a;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 268
    :cond_0
    iget-object v0, p0, Lcom/google/android/location/reporting/a/a;->b:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 269
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/android/location/reporting/a/a;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 271
    :cond_1
    iget-object v0, p0, Lcom/google/android/location/reporting/a/a;->c:Ljava/lang/Boolean;

    if-eqz v0, :cond_2

    .line 272
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/android/location/reporting/a/a;->c:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 274
    :cond_2
    iget-object v0, p0, Lcom/google/android/location/reporting/a/a;->d:Ljava/lang/Long;

    if-eqz v0, :cond_3

    .line 275
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/android/location/reporting/a/a;->d:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/b;->b(IJ)V

    .line 277
    :cond_3
    iget-object v0, p0, Lcom/google/android/location/reporting/a/a;->e:Ljava/lang/Boolean;

    if-eqz v0, :cond_4

    .line 278
    const/4 v0, 0x5

    iget-object v1, p0, Lcom/google/android/location/reporting/a/a;->e:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 280
    :cond_4
    iget-object v0, p0, Lcom/google/android/location/reporting/a/a;->f:Ljava/lang/Boolean;

    if-eqz v0, :cond_5

    .line 281
    const/4 v0, 0x6

    iget-object v1, p0, Lcom/google/android/location/reporting/a/a;->f:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 283
    :cond_5
    iget-object v0, p0, Lcom/google/android/location/reporting/a/a;->g:Ljava/lang/Boolean;

    if-eqz v0, :cond_6

    .line 284
    const/4 v0, 0x7

    iget-object v1, p0, Lcom/google/android/location/reporting/a/a;->g:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 286
    :cond_6
    iget-object v0, p0, Lcom/google/android/location/reporting/a/a;->h:Ljava/lang/Boolean;

    if-eqz v0, :cond_7

    .line 287
    const/16 v0, 0x8

    iget-object v1, p0, Lcom/google/android/location/reporting/a/a;->h:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 289
    :cond_7
    iget-object v0, p0, Lcom/google/android/location/reporting/a/a;->i:Ljava/lang/Long;

    if-eqz v0, :cond_8

    .line 290
    const/16 v0, 0x9

    iget-object v1, p0, Lcom/google/android/location/reporting/a/a;->i:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/b;->b(IJ)V

    .line 292
    :cond_8
    iget-object v0, p0, Lcom/google/android/location/reporting/a/a;->j:Ljava/lang/Integer;

    if-eqz v0, :cond_9

    .line 293
    const/16 v0, 0xa

    iget-object v1, p0, Lcom/google/android/location/reporting/a/a;->j:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 295
    :cond_9
    iget-object v0, p0, Lcom/google/android/location/reporting/a/a;->k:Ljava/lang/Boolean;

    if-eqz v0, :cond_a

    .line 296
    const/16 v0, 0xb

    iget-object v1, p0, Lcom/google/android/location/reporting/a/a;->k:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 298
    :cond_a
    iget-object v0, p0, Lcom/google/android/location/reporting/a/a;->l:Ljava/lang/Boolean;

    if-eqz v0, :cond_b

    .line 299
    const/16 v0, 0xc

    iget-object v1, p0, Lcom/google/android/location/reporting/a/a;->l:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 301
    :cond_b
    iget-object v0, p0, Lcom/google/android/location/reporting/a/a;->m:Lcom/google/android/location/reporting/a/c;

    if-eqz v0, :cond_c

    .line 302
    const/16 v0, 0xd

    iget-object v1, p0, Lcom/google/android/location/reporting/a/a;->m:Lcom/google/android/location/reporting/a/c;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 304
    :cond_c
    iget-object v0, p0, Lcom/google/android/location/reporting/a/a;->n:Ljava/lang/Integer;

    if-eqz v0, :cond_d

    .line 305
    const/16 v0, 0xe

    iget-object v1, p0, Lcom/google/android/location/reporting/a/a;->n:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 307
    :cond_d
    iget-object v0, p0, Lcom/google/android/location/reporting/a/a;->o:Ljava/lang/Boolean;

    if-eqz v0, :cond_e

    .line 308
    const/16 v0, 0xf

    iget-object v1, p0, Lcom/google/android/location/reporting/a/a;->o:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 310
    :cond_e
    iget-object v0, p0, Lcom/google/android/location/reporting/a/a;->p:Ljava/lang/Boolean;

    if-eqz v0, :cond_f

    .line 311
    const/16 v0, 0x10

    iget-object v1, p0, Lcom/google/android/location/reporting/a/a;->p:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 313
    :cond_f
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/d;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 314
    return-void
.end method
