.class public final Lcom/google/android/location/fused/b/i;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/wearable/internal/bn;
.implements Lcom/google/android/location/fused/ap;


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x11
.end annotation


# instance fields
.field private final a:Lcom/google/android/location/wearable/b;

.field private final b:Lcom/google/android/location/fused/ap;

.field private final c:Lcom/google/android/location/fused/ap;

.field private final d:Lcom/google/android/gms/wearable/internal/bn;

.field private e:Lcom/google/android/location/fused/ap;

.field private f:Lcom/google/android/gms/wearable/s;

.field private g:Z


# direct methods
.method public constructor <init>(Lcom/google/android/location/wearable/b;Lcom/google/android/location/fused/ap;Lcom/google/android/location/fused/ap;Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 47
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/location/fused/b/i;->f:Lcom/google/android/gms/wearable/s;

    .line 41
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/location/fused/b/i;->g:Z

    .line 48
    iput-object p1, p0, Lcom/google/android/location/fused/b/i;->a:Lcom/google/android/location/wearable/b;

    .line 49
    iput-object p2, p0, Lcom/google/android/location/fused/b/i;->b:Lcom/google/android/location/fused/ap;

    .line 50
    iput-object p3, p0, Lcom/google/android/location/fused/b/i;->c:Lcom/google/android/location/fused/ap;

    .line 51
    iget-object v0, p0, Lcom/google/android/location/fused/b/i;->c:Lcom/google/android/location/fused/ap;

    iput-object v0, p0, Lcom/google/android/location/fused/b/i;->e:Lcom/google/android/location/fused/ap;

    .line 52
    invoke-static {}, Lcom/google/android/location/fused/ah;->a()Landroid/os/Looper;

    move-result-object v0

    invoke-static {p4}, Lcom/google/android/location/fused/ah;->a(Landroid/content/Context;)Landroid/os/PowerManager$WakeLock;

    move-result-object v1

    invoke-static {p0, v0, v1}, Lcom/google/android/location/wearable/a;->a(Lcom/google/android/gms/wearable/internal/bn;Landroid/os/Looper;Landroid/os/PowerManager$WakeLock;)Lcom/google/android/gms/wearable/internal/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/fused/b/i;->d:Lcom/google/android/gms/wearable/internal/bn;

    .line 54
    return-void
.end method

.method private f()V
    .locals 1

    .prologue
    .line 157
    iget-boolean v0, p0, Lcom/google/android/location/fused/b/i;->g:Z

    if-nez v0, :cond_0

    .line 158
    iget-object v0, p0, Lcom/google/android/location/fused/b/i;->b:Lcom/google/android/location/fused/ap;

    invoke-interface {v0}, Lcom/google/android/location/fused/ap;->b()V

    .line 159
    iget-object v0, p0, Lcom/google/android/location/fused/b/i;->c:Lcom/google/android/location/fused/ap;

    invoke-interface {v0}, Lcom/google/android/location/fused/ap;->b()V

    .line 171
    :goto_0
    return-void

    .line 161
    :cond_0
    iget-object v0, p0, Lcom/google/android/location/fused/b/i;->f:Lcom/google/android/gms/wearable/s;

    if-eqz v0, :cond_1

    .line 162
    iget-object v0, p0, Lcom/google/android/location/fused/b/i;->b:Lcom/google/android/location/fused/ap;

    invoke-interface {v0}, Lcom/google/android/location/fused/ap;->a()V

    .line 163
    iget-object v0, p0, Lcom/google/android/location/fused/b/i;->c:Lcom/google/android/location/fused/ap;

    invoke-interface {v0}, Lcom/google/android/location/fused/ap;->b()V

    .line 164
    iget-object v0, p0, Lcom/google/android/location/fused/b/i;->b:Lcom/google/android/location/fused/ap;

    iput-object v0, p0, Lcom/google/android/location/fused/b/i;->e:Lcom/google/android/location/fused/ap;

    goto :goto_0

    .line 166
    :cond_1
    iget-object v0, p0, Lcom/google/android/location/fused/b/i;->b:Lcom/google/android/location/fused/ap;

    invoke-interface {v0}, Lcom/google/android/location/fused/ap;->b()V

    .line 167
    iget-object v0, p0, Lcom/google/android/location/fused/b/i;->c:Lcom/google/android/location/fused/ap;

    invoke-interface {v0}, Lcom/google/android/location/fused/ap;->a()V

    .line 168
    iget-object v0, p0, Lcom/google/android/location/fused/b/i;->c:Lcom/google/android/location/fused/ap;

    iput-object v0, p0, Lcom/google/android/location/fused/b/i;->e:Lcom/google/android/location/fused/ap;

    goto :goto_0
.end method


# virtual methods
.method public final a(Z)Landroid/location/Location;
    .locals 6

    .prologue
    .line 106
    iget-object v0, p0, Lcom/google/android/location/fused/b/i;->b:Lcom/google/android/location/fused/ap;

    invoke-interface {v0, p1}, Lcom/google/android/location/fused/ap;->a(Z)Landroid/location/Location;

    move-result-object v1

    .line 107
    iget-object v0, p0, Lcom/google/android/location/fused/b/i;->c:Lcom/google/android/location/fused/ap;

    invoke-interface {v0, p1}, Lcom/google/android/location/fused/ap;->a(Z)Landroid/location/Location;

    move-result-object v0

    .line 108
    if-nez v1, :cond_1

    .line 117
    :cond_0
    :goto_0
    return-object v0

    .line 111
    :cond_1
    if-nez v0, :cond_2

    move-object v0, v1

    .line 112
    goto :goto_0

    .line 114
    :cond_2
    invoke-virtual {v1}, Landroid/location/Location;->getElapsedRealtimeNanos()J

    move-result-wide v2

    invoke-virtual {v0}, Landroid/location/Location;->getElapsedRealtimeNanos()J

    move-result-wide v4

    cmp-long v2, v2, v4

    if-lez v2, :cond_0

    move-object v0, v1

    .line 115
    goto :goto_0
.end method

.method public final a()V
    .locals 2

    .prologue
    .line 58
    iget-boolean v0, p0, Lcom/google/android/location/fused/b/i;->g:Z

    if-nez v0, :cond_0

    .line 59
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/location/fused/b/i;->g:Z

    .line 60
    iget-object v0, p0, Lcom/google/android/location/fused/b/i;->a:Lcom/google/android/location/wearable/b;

    iget-object v1, p0, Lcom/google/android/location/fused/b/i;->d:Lcom/google/android/gms/wearable/internal/bn;

    invoke-virtual {v0, v1}, Lcom/google/android/location/wearable/b;->a(Lcom/google/android/gms/wearable/internal/bn;)V

    .line 61
    invoke-direct {p0}, Lcom/google/android/location/fused/b/i;->f()V

    .line 63
    :cond_0
    return-void
.end method

.method public final a(Landroid/location/Location;I)V
    .locals 1

    .prologue
    .line 99
    iget-object v0, p0, Lcom/google/android/location/fused/b/i;->b:Lcom/google/android/location/fused/ap;

    invoke-interface {v0, p1, p2}, Lcom/google/android/location/fused/ap;->a(Landroid/location/Location;I)V

    .line 100
    iget-object v0, p0, Lcom/google/android/location/fused/b/i;->c:Lcom/google/android/location/fused/ap;

    invoke-interface {v0, p1, p2}, Lcom/google/android/location/fused/ap;->a(Landroid/location/Location;I)V

    .line 101
    return-void
.end method

.method public final a(Lcom/google/android/gms/wearable/i;)V
    .locals 0

    .prologue
    .line 154
    return-void
.end method

.method public final a(Lcom/google/android/gms/wearable/r;)V
    .locals 0

    .prologue
    .line 149
    return-void
.end method

.method public final a(Lcom/google/android/gms/wearable/s;)V
    .locals 2

    .prologue
    .line 133
    iget-object v0, p0, Lcom/google/android/location/fused/b/i;->f:Lcom/google/android/gms/wearable/s;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/location/fused/b/i;->f:Lcom/google/android/gms/wearable/s;

    invoke-virtual {v0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 134
    const-string v0, "GCoreFlp"

    const-string v1, "Clockwork device should only have one peer"

    invoke-static {v0, v1}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)I

    .line 136
    :cond_0
    iput-object p1, p0, Lcom/google/android/location/fused/b/i;->f:Lcom/google/android/gms/wearable/s;

    .line 137
    invoke-direct {p0}, Lcom/google/android/location/fused/b/i;->f()V

    .line 138
    return-void
.end method

.method public final a(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 122
    iget-object v0, p0, Lcom/google/android/location/fused/b/i;->b:Lcom/google/android/location/fused/ap;

    invoke-interface {v0, p1, p2, p3}, Lcom/google/android/location/fused/ap;->a(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V

    .line 123
    iget-object v0, p0, Lcom/google/android/location/fused/b/i;->c:Lcom/google/android/location/fused/ap;

    invoke-interface {v0, p1, p2, p3}, Lcom/google/android/location/fused/ap;->a(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V

    .line 124
    return-void
.end method

.method public final a(Ljava/util/Collection;Z)V
    .locals 1

    .prologue
    .line 78
    iget-object v0, p0, Lcom/google/android/location/fused/b/i;->b:Lcom/google/android/location/fused/ap;

    invoke-interface {v0, p1, p2}, Lcom/google/android/location/fused/ap;->a(Ljava/util/Collection;Z)V

    .line 79
    iget-object v0, p0, Lcom/google/android/location/fused/b/i;->c:Lcom/google/android/location/fused/ap;

    invoke-interface {v0, p1, p2}, Lcom/google/android/location/fused/ap;->a(Ljava/util/Collection;Z)V

    .line 80
    return-void
.end method

.method public final b()V
    .locals 3

    .prologue
    .line 67
    iget-boolean v0, p0, Lcom/google/android/location/fused/b/i;->g:Z

    if-eqz v0, :cond_0

    .line 68
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/location/fused/b/i;->g:Z

    .line 69
    iget-object v0, p0, Lcom/google/android/location/fused/b/i;->a:Lcom/google/android/location/wearable/b;

    iget-object v1, p0, Lcom/google/android/location/fused/b/i;->d:Lcom/google/android/gms/wearable/internal/bn;

    iget-object v2, v0, Lcom/google/android/location/wearable/b;->a:Ljava/util/Collection;

    monitor-enter v2

    :try_start_0
    iget-object v0, v0, Lcom/google/android/location/wearable/b;->a:Ljava/util/Collection;

    invoke-interface {v0, v1}, Ljava/util/Collection;->remove(Ljava/lang/Object;)Z

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 70
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/location/fused/b/i;->f:Lcom/google/android/gms/wearable/s;

    .line 71
    invoke-direct {p0}, Lcom/google/android/location/fused/b/i;->f()V

    .line 73
    :cond_0
    return-void

    .line 69
    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0
.end method

.method public final b(Lcom/google/android/gms/wearable/s;)V
    .locals 1

    .prologue
    .line 142
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/location/fused/b/i;->f:Lcom/google/android/gms/wearable/s;

    .line 143
    invoke-direct {p0}, Lcom/google/android/location/fused/b/i;->f()V

    .line 144
    return-void
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 85
    iget-object v0, p0, Lcom/google/android/location/fused/b/i;->b:Lcom/google/android/location/fused/ap;

    invoke-interface {v0}, Lcom/google/android/location/fused/ap;->c()V

    .line 86
    iget-object v0, p0, Lcom/google/android/location/fused/b/i;->c:Lcom/google/android/location/fused/ap;

    invoke-interface {v0}, Lcom/google/android/location/fused/ap;->c()V

    .line 87
    return-void
.end method

.method public final d()V
    .locals 1

    .prologue
    .line 92
    iget-object v0, p0, Lcom/google/android/location/fused/b/i;->b:Lcom/google/android/location/fused/ap;

    invoke-interface {v0}, Lcom/google/android/location/fused/ap;->d()V

    .line 93
    iget-object v0, p0, Lcom/google/android/location/fused/b/i;->c:Lcom/google/android/location/fused/ap;

    invoke-interface {v0}, Lcom/google/android/location/fused/ap;->d()V

    .line 94
    return-void
.end method

.method public final e()Lcom/google/android/location/n/k;
    .locals 1

    .prologue
    .line 128
    iget-object v0, p0, Lcom/google/android/location/fused/b/i;->e:Lcom/google/android/location/fused/ap;

    invoke-interface {v0}, Lcom/google/android/location/fused/ap;->e()Lcom/google/android/location/n/k;

    move-result-object v0

    return-object v0
.end method
