.class final Lcom/google/android/location/copresence/r/k;
.super Lcom/google/android/location/copresence/k/f;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/location/copresence/r/f;


# direct methods
.method varargs constructor <init>(Lcom/google/android/location/copresence/r/f;Ljava/lang/String;[Lcom/google/android/location/copresence/k/f;)V
    .locals 0

    .prologue
    .line 179
    iput-object p1, p0, Lcom/google/android/location/copresence/r/k;->a:Lcom/google/android/location/copresence/r/f;

    invoke-direct {p0, p2, p3}, Lcom/google/android/location/copresence/k/f;-><init>(Ljava/lang/String;[Lcom/google/android/location/copresence/k/f;)V

    return-void
.end method


# virtual methods
.method public final a()Z
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 203
    iget-object v2, p0, Lcom/google/android/location/copresence/r/k;->a:Lcom/google/android/location/copresence/r/f;

    invoke-static {v2}, Lcom/google/android/location/copresence/r/f;->b(Lcom/google/android/location/copresence/r/f;)Lcom/google/android/location/copresence/r/ae;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/location/copresence/r/ae;->b()Landroid/net/wifi/WifiConfiguration;

    move-result-object v2

    .line 204
    invoke-static {v5}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 205
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "WifiApStates2: Wifi config before revert: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/location/copresence/ag;->b(Ljava/lang/String;)I

    .line 207
    :cond_0
    iget-object v3, p0, Lcom/google/android/location/copresence/r/k;->a:Lcom/google/android/location/copresence/r/f;

    invoke-static {v3}, Lcom/google/android/location/copresence/r/f;->a(Lcom/google/android/location/copresence/r/f;)Landroid/content/SharedPreferences;

    move-result-object v3

    invoke-static {v3, v2}, Lcom/google/android/location/copresence/r/f;->a(Landroid/content/SharedPreferences;Landroid/net/wifi/WifiConfiguration;)V

    .line 208
    invoke-static {v5}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 209
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "WifiApStates2: Wifi config after revert: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/location/copresence/ag;->b(Ljava/lang/String;)I

    .line 211
    :cond_1
    iget-object v3, p0, Lcom/google/android/location/copresence/r/k;->a:Lcom/google/android/location/copresence/r/f;

    invoke-static {v3}, Lcom/google/android/location/copresence/r/f;->b(Lcom/google/android/location/copresence/r/f;)Lcom/google/android/location/copresence/r/ae;

    move-result-object v3

    invoke-virtual {v3, v2, v0}, Lcom/google/android/location/copresence/r/ae;->a(Landroid/net/wifi/WifiConfiguration;Z)Z

    move-result v3

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/google/android/location/copresence/r/k;->a:Lcom/google/android/location/copresence/r/f;

    invoke-static {v3}, Lcom/google/android/location/copresence/r/f;->b(Lcom/google/android/location/copresence/r/f;)Lcom/google/android/location/copresence/r/ae;

    move-result-object v3

    invoke-virtual {v3, v2}, Lcom/google/android/location/copresence/r/ae;->a(Landroid/net/wifi/WifiConfiguration;)Z

    move-result v2

    if-eqz v2, :cond_2

    move v0, v1

    .line 213
    :cond_2
    iget-object v2, p0, Lcom/google/android/location/copresence/r/k;->a:Lcom/google/android/location/copresence/r/f;

    invoke-static {v2}, Lcom/google/android/location/copresence/r/f;->a(Lcom/google/android/location/copresence/r/f;)Landroid/content/SharedPreferences;

    move-result-object v2

    const-string v3, "wifiEnabled"

    invoke-interface {v2, v3}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 214
    iget-object v2, p0, Lcom/google/android/location/copresence/r/k;->a:Lcom/google/android/location/copresence/r/f;

    invoke-static {v2}, Lcom/google/android/location/copresence/r/f;->b(Lcom/google/android/location/copresence/r/f;)Lcom/google/android/location/copresence/r/ae;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/location/copresence/r/k;->a:Lcom/google/android/location/copresence/r/f;

    invoke-static {v3}, Lcom/google/android/location/copresence/r/f;->a(Lcom/google/android/location/copresence/r/f;)Landroid/content/SharedPreferences;

    move-result-object v3

    const-string v4, "wifiEnabled"

    invoke-interface {v3, v4, v1}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    invoke-virtual {v2, v1}, Lcom/google/android/location/copresence/r/ae;->a(Z)Z

    move-result v1

    and-int/2addr v0, v1

    .line 217
    :cond_3
    return v0
.end method

.method public final b()Z
    .locals 7

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 183
    iget-object v0, p0, Lcom/google/android/location/copresence/r/k;->a:Lcom/google/android/location/copresence/r/f;

    invoke-static {v0}, Lcom/google/android/location/copresence/r/f;->b(Lcom/google/android/location/copresence/r/f;)Lcom/google/android/location/copresence/r/ae;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/location/copresence/r/ae;->b()Landroid/net/wifi/WifiConfiguration;

    move-result-object v0

    .line 184
    invoke-static {v0}, Lcom/google/android/location/copresence/r/f;->a(Landroid/net/wifi/WifiConfiguration;)Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 185
    iget-object v1, p0, Lcom/google/android/location/copresence/r/k;->a:Lcom/google/android/location/copresence/r/f;

    invoke-static {v1}, Lcom/google/android/location/copresence/r/f;->a(Lcom/google/android/location/copresence/r/f;)Landroid/content/SharedPreferences;

    move-result-object v5

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-interface {v5, v1}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 186
    iget-object v1, p0, Lcom/google/android/location/copresence/r/k;->a:Lcom/google/android/location/copresence/r/f;

    invoke-static {v1}, Lcom/google/android/location/copresence/r/f;->a(Lcom/google/android/location/copresence/r/f;)Landroid/content/SharedPreferences;

    move-result-object v5

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    const/4 v6, 0x0

    invoke-interface {v5, v1, v6}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 187
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    invoke-static {v1, v5}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 188
    const/4 v1, 0x3

    invoke-static {v1}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 189
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v1, "WifiApStates2: Wifi ap not reverted: "

    invoke-direct {v3, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "!="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->b(Ljava/lang/String;)I

    :cond_1
    move v0, v2

    .line 196
    :goto_0
    return v0

    :cond_2
    iget-object v0, p0, Lcom/google/android/location/copresence/r/k;->a:Lcom/google/android/location/copresence/r/f;

    invoke-static {v0}, Lcom/google/android/location/copresence/r/f;->a(Lcom/google/android/location/copresence/r/f;)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "wifiEnabled"

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/location/copresence/r/k;->a:Lcom/google/android/location/copresence/r/f;

    invoke-static {v0}, Lcom/google/android/location/copresence/r/f;->a(Lcom/google/android/location/copresence/r/f;)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "wifiEnabled"

    invoke-interface {v0, v1, v3}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iget-object v1, p0, Lcom/google/android/location/copresence/r/k;->a:Lcom/google/android/location/copresence/r/f;

    invoke-static {v1}, Lcom/google/android/location/copresence/r/f;->b(Lcom/google/android/location/copresence/r/f;)Lcom/google/android/location/copresence/r/ae;

    move-result-object v1

    iget-object v1, v1, Lcom/google/android/location/copresence/r/ae;->a:Landroid/net/wifi/WifiManager;

    invoke-virtual {v1}, Landroid/net/wifi/WifiManager;->isWifiEnabled()Z

    move-result v1

    if-ne v0, v1, :cond_4

    :cond_3
    move v0, v3

    goto :goto_0

    :cond_4
    move v0, v2

    goto :goto_0
.end method

.method public final d()J
    .locals 2

    .prologue
    .line 222
    const-wide/16 v0, 0x1388

    return-wide v0
.end method
