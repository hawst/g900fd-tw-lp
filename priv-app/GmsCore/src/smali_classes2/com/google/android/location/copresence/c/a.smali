.class public final Lcom/google/android/location/copresence/c/a;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static e:Lcom/google/android/location/copresence/c/a;


# instance fields
.field public final a:Lcom/google/android/location/copresence/w;

.field public final b:Lcom/google/android/location/copresence/z;

.field public final c:Lcom/google/android/location/copresence/w;

.field public final d:Lcom/google/android/location/copresence/z;


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .locals 5

    .prologue
    const/16 v4, 0x8

    const/4 v3, 0x1

    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    new-instance v0, Lcom/google/android/location/copresence/c/h;

    const/4 v1, 0x0

    const-string v2, "audible: "

    invoke-direct {v0, p1, v1, v2}, Lcom/google/android/location/copresence/c/h;-><init>(Landroid/content/Context;ZLjava/lang/String;)V

    .line 32
    new-instance v1, Lcom/google/android/location/copresence/c/h;

    const-string v2, "inaudible: "

    invoke-direct {v1, p1, v3, v2}, Lcom/google/android/location/copresence/c/h;-><init>(Landroid/content/Context;ZLjava/lang/String;)V

    .line 35
    new-instance v2, Lcom/google/android/location/copresence/c/m;

    invoke-direct {v2, v0, v4}, Lcom/google/android/location/copresence/c/m;-><init>(Lcom/google/android/location/copresence/c/h;I)V

    iput-object v2, p0, Lcom/google/android/location/copresence/c/a;->a:Lcom/google/android/location/copresence/w;

    .line 37
    new-instance v2, Lcom/google/android/location/copresence/c/n;

    invoke-direct {v2, p1, v0, v4}, Lcom/google/android/location/copresence/c/n;-><init>(Landroid/content/Context;Lcom/google/android/location/copresence/c/h;I)V

    iput-object v2, p0, Lcom/google/android/location/copresence/c/a;->b:Lcom/google/android/location/copresence/z;

    .line 40
    new-instance v0, Lcom/google/android/location/copresence/c/m;

    invoke-direct {v0, v1, v3}, Lcom/google/android/location/copresence/c/m;-><init>(Lcom/google/android/location/copresence/c/h;I)V

    iput-object v0, p0, Lcom/google/android/location/copresence/c/a;->c:Lcom/google/android/location/copresence/w;

    .line 42
    new-instance v0, Lcom/google/android/location/copresence/c/n;

    invoke-direct {v0, p1, v1, v3}, Lcom/google/android/location/copresence/c/n;-><init>(Landroid/content/Context;Lcom/google/android/location/copresence/c/h;I)V

    iput-object v0, p0, Lcom/google/android/location/copresence/c/a;->d:Lcom/google/android/location/copresence/z;

    .line 44
    return-void
.end method

.method public static declared-synchronized a(Landroid/content/Context;)Lcom/google/android/location/copresence/c/a;
    .locals 2

    .prologue
    .line 22
    const-class v1, Lcom/google/android/location/copresence/c/a;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/google/android/location/copresence/c/a;->e:Lcom/google/android/location/copresence/c/a;

    if-nez v0, :cond_0

    .line 23
    new-instance v0, Lcom/google/android/location/copresence/c/a;

    invoke-direct {v0, p0}, Lcom/google/android/location/copresence/c/a;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/google/android/location/copresence/c/a;->e:Lcom/google/android/location/copresence/c/a;

    .line 25
    :cond_0
    sget-object v0, Lcom/google/android/location/copresence/c/a;->e:Lcom/google/android/location/copresence/c/a;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 22
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method
