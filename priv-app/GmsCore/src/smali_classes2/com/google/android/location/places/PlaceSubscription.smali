.class public final Lcom/google/android/location/places/PlaceSubscription;
.super Lcom/google/android/location/places/Subscription;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:Lcom/google/android/location/places/am;

.field private static final d:Lcom/google/android/gms/location/places/PlaceRequest;


# instance fields
.field public final a:Lcom/google/android/gms/location/places/PlaceRequest;

.field public final b:Lcom/google/android/gms/location/places/internal/PlacesParams;

.field public final c:Landroid/app/PendingIntent;

.field private final e:I


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    .line 39
    new-instance v0, Lcom/google/android/location/places/am;

    invoke-direct {v0}, Lcom/google/android/location/places/am;-><init>()V

    sput-object v0, Lcom/google/android/location/places/PlaceSubscription;->CREATOR:Lcom/google/android/location/places/am;

    .line 41
    new-instance v4, Lcom/google/android/gms/location/places/m;

    invoke-direct {v4}, Lcom/google/android/gms/location/places/m;-><init>()V

    invoke-static {}, Lcom/google/android/gms/location/places/PlaceFilter;->h()Lcom/google/android/gms/location/places/PlaceFilter;

    move-result-object v0

    iput-object v0, v4, Lcom/google/android/gms/location/places/m;->a:Lcom/google/android/gms/location/places/PlaceFilter;

    iget-wide v0, v4, Lcom/google/android/gms/location/places/m;->b:J

    invoke-static {v0, v1}, Lcom/google/android/gms/location/places/PlaceRequest;->a(J)V

    iget v0, v4, Lcom/google/android/gms/location/places/m;->c:I

    invoke-static {v0}, Lcom/google/android/gms/location/places/PlaceRequest;->a(I)V

    new-instance v0, Lcom/google/android/gms/location/places/PlaceRequest;

    iget-object v1, v4, Lcom/google/android/gms/location/places/m;->a:Lcom/google/android/gms/location/places/PlaceFilter;

    iget-wide v2, v4, Lcom/google/android/gms/location/places/m;->b:J

    iget v4, v4, Lcom/google/android/gms/location/places/m;->c:I

    const/4 v5, 0x0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/location/places/PlaceRequest;-><init>(Lcom/google/android/gms/location/places/PlaceFilter;JIB)V

    sput-object v0, Lcom/google/android/location/places/PlaceSubscription;->d:Lcom/google/android/gms/location/places/PlaceRequest;

    return-void
.end method

.method public constructor <init>(ILcom/google/android/gms/location/places/PlaceRequest;Lcom/google/android/gms/location/places/internal/PlacesParams;Landroid/app/PendingIntent;)V
    .locals 1

    .prologue
    .line 107
    invoke-direct {p0}, Lcom/google/android/location/places/Subscription;-><init>()V

    .line 108
    iput p1, p0, Lcom/google/android/location/places/PlaceSubscription;->e:I

    .line 109
    invoke-static {p2}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/location/places/PlaceRequest;

    iput-object v0, p0, Lcom/google/android/location/places/PlaceSubscription;->a:Lcom/google/android/gms/location/places/PlaceRequest;

    .line 110
    iput-object p3, p0, Lcom/google/android/location/places/PlaceSubscription;->b:Lcom/google/android/gms/location/places/internal/PlacesParams;

    .line 111
    invoke-static {p4}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/PendingIntent;

    iput-object v0, p0, Lcom/google/android/location/places/PlaceSubscription;->c:Landroid/app/PendingIntent;

    .line 112
    return-void
.end method

.method private constructor <init>(Lcom/google/android/gms/location/places/PlaceRequest;Lcom/google/android/gms/location/places/internal/PlacesParams;Landroid/app/PendingIntent;)V
    .locals 1

    .prologue
    .line 94
    const/4 v0, 0x0

    invoke-direct {p0, v0, p1, p2, p3}, Lcom/google/android/location/places/PlaceSubscription;-><init>(ILcom/google/android/gms/location/places/PlaceRequest;Lcom/google/android/gms/location/places/internal/PlacesParams;Landroid/app/PendingIntent;)V

    .line 95
    return-void
.end method

.method public static a(Landroid/app/PendingIntent;)Lcom/google/android/location/places/PlaceSubscription;
    .locals 4

    .prologue
    .line 81
    new-instance v0, Lcom/google/android/location/places/PlaceSubscription;

    const/4 v1, 0x0

    sget-object v2, Lcom/google/android/location/places/PlaceSubscription;->d:Lcom/google/android/gms/location/places/PlaceRequest;

    sget-object v3, Lcom/google/android/gms/location/places/internal/PlacesParams;->a:Lcom/google/android/gms/location/places/internal/PlacesParams;

    invoke-direct {v0, v1, v2, v3, p0}, Lcom/google/android/location/places/PlaceSubscription;-><init>(ILcom/google/android/gms/location/places/PlaceRequest;Lcom/google/android/gms/location/places/internal/PlacesParams;Landroid/app/PendingIntent;)V

    return-object v0
.end method

.method public static a(Lcom/google/android/gms/location/places/PlaceRequest;Lcom/google/android/gms/location/places/internal/PlacesParams;Landroid/app/PendingIntent;)Lcom/google/android/location/places/PlaceSubscription;
    .locals 1

    .prologue
    .line 72
    new-instance v0, Lcom/google/android/location/places/PlaceSubscription;

    invoke-direct {v0, p0, p1, p2}, Lcom/google/android/location/places/PlaceSubscription;-><init>(Lcom/google/android/gms/location/places/PlaceRequest;Lcom/google/android/gms/location/places/internal/PlacesParams;Landroid/app/PendingIntent;)V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/android/gms/location/places/internal/PlacesParams;
    .locals 1

    .prologue
    .line 116
    iget-object v0, p0, Lcom/google/android/location/places/PlaceSubscription;->b:Lcom/google/android/gms/location/places/internal/PlacesParams;

    return-object v0
.end method

.method public final b()Lcom/google/android/gms/location/places/PlaceFilter;
    .locals 1

    .prologue
    .line 121
    iget-object v0, p0, Lcom/google/android/location/places/PlaceSubscription;->a:Lcom/google/android/gms/location/places/PlaceRequest;

    invoke-virtual {v0}, Lcom/google/android/gms/location/places/PlaceRequest;->a()Lcom/google/android/gms/location/places/PlaceFilter;

    move-result-object v0

    return-object v0
.end method

.method public final c()Landroid/app/PendingIntent;
    .locals 1

    .prologue
    .line 126
    iget-object v0, p0, Lcom/google/android/location/places/PlaceSubscription;->c:Landroid/app/PendingIntent;

    return-object v0
.end method

.method public final d()Z
    .locals 1

    .prologue
    .line 131
    const/4 v0, 0x0

    return v0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 174
    sget-object v0, Lcom/google/android/location/places/PlaceSubscription;->CREATOR:Lcom/google/android/location/places/am;

    const/4 v0, 0x0

    return v0
.end method

.method public final e()I
    .locals 1

    .prologue
    .line 167
    iget v0, p0, Lcom/google/android/location/places/PlaceSubscription;->e:I

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 151
    if-ne p0, p1, :cond_1

    .line 158
    :cond_0
    :goto_0
    return v0

    .line 154
    :cond_1
    instance-of v2, p1, Lcom/google/android/location/places/PlaceSubscription;

    if-nez v2, :cond_2

    move v0, v1

    .line 155
    goto :goto_0

    .line 157
    :cond_2
    check-cast p1, Lcom/google/android/location/places/PlaceSubscription;

    .line 158
    iget-object v2, p0, Lcom/google/android/location/places/PlaceSubscription;->a:Lcom/google/android/gms/location/places/PlaceRequest;

    iget-object v3, p1, Lcom/google/android/location/places/PlaceSubscription;->a:Lcom/google/android/gms/location/places/PlaceRequest;

    invoke-static {v2, v3}, Lcom/google/android/gms/common/internal/bu;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/google/android/location/places/PlaceSubscription;->b:Lcom/google/android/gms/location/places/internal/PlacesParams;

    iget-object v3, p1, Lcom/google/android/location/places/PlaceSubscription;->b:Lcom/google/android/gms/location/places/internal/PlacesParams;

    invoke-static {v2, v3}, Lcom/google/android/gms/common/internal/bu;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/google/android/location/places/PlaceSubscription;->c:Landroid/app/PendingIntent;

    iget-object v3, p1, Lcom/google/android/location/places/PlaceSubscription;->c:Landroid/app/PendingIntent;

    invoke-virtual {v2, v3}, Landroid/app/PendingIntent;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    .line 146
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/location/places/PlaceSubscription;->a:Lcom/google/android/gms/location/places/PlaceRequest;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/location/places/PlaceSubscription;->b:Lcom/google/android/gms/location/places/internal/PlacesParams;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/location/places/PlaceSubscription;->c:Landroid/app/PendingIntent;

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 3
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "DefaultLocale"
        }
    .end annotation

    .prologue
    .line 137
    invoke-static {p0}, Lcom/google/android/gms/common/internal/bu;->a(Ljava/lang/Object;)Lcom/google/android/gms/common/internal/bv;

    move-result-object v0

    const-string v1, "request"

    iget-object v2, p0, Lcom/google/android/location/places/PlaceSubscription;->a:Lcom/google/android/gms/location/places/PlaceRequest;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/common/internal/bv;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/android/gms/common/internal/bv;

    move-result-object v0

    const-string v1, "params"

    iget-object v2, p0, Lcom/google/android/location/places/PlaceSubscription;->b:Lcom/google/android/gms/location/places/internal/PlacesParams;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/common/internal/bv;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/android/gms/common/internal/bv;

    move-result-object v0

    const-string v1, "callbackIntent"

    iget-object v2, p0, Lcom/google/android/location/places/PlaceSubscription;->c:Landroid/app/PendingIntent;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/common/internal/bv;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/android/gms/common/internal/bv;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/common/internal/bv;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 181
    sget-object v0, Lcom/google/android/location/places/PlaceSubscription;->CREATOR:Lcom/google/android/location/places/am;

    invoke-static {p0, p1, p2}, Lcom/google/android/location/places/am;->a(Lcom/google/android/location/places/PlaceSubscription;Landroid/os/Parcel;I)V

    .line 182
    return-void
.end method
