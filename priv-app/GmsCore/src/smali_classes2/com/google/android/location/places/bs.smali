.class final Lcom/google/android/location/places/bs;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final a:Ljava/util/List;


# direct methods
.method private constructor <init>(Ljava/util/List;)V
    .locals 0

    .prologue
    .line 296
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 297
    iput-object p1, p0, Lcom/google/android/location/places/bs;->a:Ljava/util/List;

    .line 298
    return-void
.end method

.method public static a(Lcom/google/android/location/e/bi;)Lcom/google/android/location/places/bs;
    .locals 6

    .prologue
    .line 304
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 305
    new-instance v2, Ljava/util/ArrayList;

    invoke-virtual {p0}, Lcom/google/android/location/e/bi;->a()I

    move-result v0

    invoke-direct {v2, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 306
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0}, Lcom/google/android/location/e/bi;->a()I

    move-result v3

    if-ge v0, v3, :cond_0

    .line 307
    invoke-virtual {p0, v0}, Lcom/google/android/location/e/bi;->a(I)Lcom/google/android/location/e/bc;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 306
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 309
    :cond_0
    new-instance v0, Lcom/google/android/location/places/bt;

    invoke-direct {v0}, Lcom/google/android/location/places/bt;-><init>()V

    invoke-static {v2, v0}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 315
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/e/bc;

    .line 316
    iget-wide v4, v0, Lcom/google/android/location/e/bc;->b:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 318
    :cond_1
    new-instance v0, Lcom/google/android/location/places/bs;

    invoke-direct {v0, v1}, Lcom/google/android/location/places/bs;-><init>(Ljava/util/List;)V

    return-object v0
.end method


# virtual methods
.method public final equals(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 323
    if-ne p0, p1, :cond_0

    .line 324
    const/4 v0, 0x1

    .line 332
    :goto_0
    return v0

    .line 326
    :cond_0
    if-eqz p1, :cond_1

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    if-eq v0, v1, :cond_2

    .line 327
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 330
    :cond_2
    check-cast p1, Lcom/google/android/location/places/bs;

    .line 332
    iget-object v0, p0, Lcom/google/android/location/places/bs;->a:Ljava/util/List;

    iget-object v1, p1, Lcom/google/android/location/places/bs;->a:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 337
    iget-object v0, p0, Lcom/google/android/location/places/bs;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->hashCode()I

    move-result v0

    return v0
.end method
