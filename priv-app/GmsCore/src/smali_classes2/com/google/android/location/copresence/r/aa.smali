.class final Lcom/google/android/location/copresence/r/aa;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/net/wifi/p2p/WifiP2pManager$GroupInfoListener;


# instance fields
.field final synthetic a:Lcom/google/android/location/copresence/r/z;


# direct methods
.method constructor <init>(Lcom/google/android/location/copresence/r/z;)V
    .locals 0

    .prologue
    .line 197
    iput-object p1, p0, Lcom/google/android/location/copresence/r/aa;->a:Lcom/google/android/location/copresence/r/z;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onGroupInfoAvailable(Landroid/net/wifi/p2p/WifiP2pGroup;)V
    .locals 4

    .prologue
    const/4 v3, 0x3

    .line 200
    invoke-static {v3}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 201
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "WifiDirectStates2: TransientGroupCreatedState group info available: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->b(Ljava/lang/String;)I

    .line 203
    :cond_0
    iget-object v0, p0, Lcom/google/android/location/copresence/r/aa;->a:Lcom/google/android/location/copresence/r/z;

    iget-boolean v0, v0, Lcom/google/android/location/copresence/r/z;->b:Z

    if-nez v0, :cond_3

    if-eqz p1, :cond_3

    invoke-virtual {p1}, Landroid/net/wifi/p2p/WifiP2pGroup;->isGroupOwner()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 204
    invoke-static {v3}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 205
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "WifiDirectStates2: Saving interface info: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Landroid/net/wifi/p2p/WifiP2pGroup;->getInterface()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->b(Ljava/lang/String;)I

    .line 208
    :cond_1
    iget-object v0, p0, Lcom/google/android/location/copresence/r/aa;->a:Lcom/google/android/location/copresence/r/z;

    iget-object v0, v0, Lcom/google/android/location/copresence/r/z;->c:Lcom/google/android/location/copresence/r/p;

    iget-object v0, p0, Lcom/google/android/location/copresence/r/aa;->a:Lcom/google/android/location/copresence/r/z;

    iget-object v0, v0, Lcom/google/android/location/copresence/r/z;->c:Lcom/google/android/location/copresence/r/p;

    invoke-static {v0}, Lcom/google/android/location/copresence/r/p;->e(Lcom/google/android/location/copresence/r/p;)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "groupInterface"

    invoke-virtual {p1}, Landroid/net/wifi/p2p/WifiP2pGroup;->getInterface()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "save group interface"

    invoke-static {v0, v1}, Lcom/google/android/location/copresence/r/p;->a(Landroid/content/SharedPreferences$Editor;Ljava/lang/String;)Z

    move-result v0

    .line 211
    if-eqz v0, :cond_3

    .line 212
    invoke-static {v3}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 213
    const-string v0, "WifiDirectStates2: Interface info saved!"

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->b(Ljava/lang/String;)I

    .line 215
    :cond_2
    iget-object v0, p0, Lcom/google/android/location/copresence/r/aa;->a:Lcom/google/android/location/copresence/r/z;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/google/android/location/copresence/r/z;->b:Z

    .line 216
    iget-object v0, p0, Lcom/google/android/location/copresence/r/aa;->a:Lcom/google/android/location/copresence/r/z;

    iget-object v0, v0, Lcom/google/android/location/copresence/r/z;->c:Lcom/google/android/location/copresence/r/p;

    invoke-static {v0}, Lcom/google/android/location/copresence/r/p;->b(Lcom/google/android/location/copresence/r/p;)Lcom/google/android/location/copresence/r/ah;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/location/copresence/r/ah;->b()Z

    .line 217
    iget-object v0, p0, Lcom/google/android/location/copresence/r/aa;->a:Lcom/google/android/location/copresence/r/z;

    iget-object v0, v0, Lcom/google/android/location/copresence/r/z;->c:Lcom/google/android/location/copresence/r/p;

    invoke-static {v0}, Lcom/google/android/location/copresence/r/p;->b(Lcom/google/android/location/copresence/r/p;)Lcom/google/android/location/copresence/r/ah;

    move-result-object v0

    const/4 v1, 0x0

    iput-object v1, v0, Lcom/google/android/location/copresence/r/ah;->f:Lcom/google/android/location/copresence/r/ak;

    .line 218
    iget-object v0, p0, Lcom/google/android/location/copresence/r/aa;->a:Lcom/google/android/location/copresence/r/z;

    iget-object v0, v0, Lcom/google/android/location/copresence/r/z;->c:Lcom/google/android/location/copresence/r/p;

    invoke-static {v0}, Lcom/google/android/location/copresence/r/p;->g(Lcom/google/android/location/copresence/r/p;)Lcom/google/android/location/copresence/k/a;

    move-result-object v0

    const/4 v1, 0x0

    new-array v1, v1, [Lcom/google/android/location/copresence/k/f;

    invoke-virtual {v0, v1}, Lcom/google/android/location/copresence/k/a;->b([Lcom/google/android/location/copresence/k/f;)V

    .line 221
    :cond_3
    return-void
.end method
