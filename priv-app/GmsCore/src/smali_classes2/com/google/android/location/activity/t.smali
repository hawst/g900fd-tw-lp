.class abstract Lcom/google/android/location/activity/t;
.super Lcom/google/android/location/activity/av;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/location/activity/e;


# instance fields
.field protected a:Z


# direct methods
.method public constructor <init>(Lcom/google/android/location/activity/k;Lcom/google/android/location/os/bi;)V
    .locals 1

    .prologue
    .line 32
    invoke-direct {p0, p1, p2}, Lcom/google/android/location/activity/av;-><init>(Lcom/google/android/location/activity/k;Lcom/google/android/location/os/bi;)V

    .line 26
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/location/activity/t;->a:Z

    .line 33
    return-void
.end method


# virtual methods
.method public a(Lcom/google/android/gms/location/ActivityRecognitionResult;)V
    .locals 6

    .prologue
    .line 91
    iget-object v0, p0, Lcom/google/android/location/activity/t;->d:Lcom/google/android/location/activity/k;

    invoke-virtual {v0, p1}, Lcom/google/android/location/activity/k;->b(Lcom/google/android/gms/location/ActivityRecognitionResult;)V

    .line 92
    iget-boolean v0, p0, Lcom/google/android/location/activity/t;->a:Z

    if-eqz v0, :cond_1

    .line 102
    :cond_0
    :goto_0
    return-void

    .line 95
    :cond_1
    invoke-virtual {p1}, Lcom/google/android/gms/location/ActivityRecognitionResult;->a()Lcom/google/android/gms/location/DetectedActivity;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/location/DetectedActivity;->a()I

    move-result v0

    const/4 v1, 0x5

    if-ne v0, v1, :cond_0

    .line 97
    new-instance v0, Lcom/google/android/gms/location/ActivityRecognitionResult;

    invoke-virtual {p1}, Lcom/google/android/gms/location/ActivityRecognitionResult;->c()Ljava/util/List;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/android/gms/location/ActivityRecognitionResult;->d()J

    move-result-wide v2

    invoke-virtual {p1}, Lcom/google/android/gms/location/ActivityRecognitionResult;->e()J

    move-result-wide v4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/location/ActivityRecognitionResult;-><init>(Ljava/util/List;JJ)V

    .line 99
    sget-boolean v1, Lcom/google/android/location/i/a;->b:Z

    if-eqz v1, :cond_2

    const-string v1, "ActivityScheduler"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Reporting tilting: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/location/o/a/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 100
    :cond_2
    iget-object v1, p0, Lcom/google/android/location/activity/t;->d:Lcom/google/android/location/activity/k;

    invoke-virtual {v1, v0}, Lcom/google/android/location/activity/k;->a(Lcom/google/android/gms/location/ActivityRecognitionResult;)V

    goto :goto_0
.end method

.method public a(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 118
    iget-boolean v0, p0, Lcom/google/android/location/activity/t;->a:Z

    if-eqz v0, :cond_0

    .line 126
    :goto_0
    return-void

    .line 121
    :cond_0
    sget-boolean v0, Lcom/google/android/location/i/a;->d:Z

    if-eqz v0, :cond_1

    const-string v0, "ActivityScheduler"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "onFatalError: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/location/o/a/a;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 122
    :cond_1
    invoke-static {}, Lcom/google/android/location/activity/k;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 123
    iget-object v0, p0, Lcom/google/android/location/activity/t;->d:Lcom/google/android/location/activity/k;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/location/activity/k;->a(Z)V

    .line 125
    :cond_2
    new-instance v0, Lcom/google/android/location/activity/ao;

    iget-object v1, p0, Lcom/google/android/location/activity/t;->d:Lcom/google/android/location/activity/k;

    iget-object v2, p0, Lcom/google/android/location/activity/t;->e:Lcom/google/android/location/os/bi;

    invoke-direct {v0, v1, v2}, Lcom/google/android/location/activity/ao;-><init>(Lcom/google/android/location/activity/k;Lcom/google/android/location/os/bi;)V

    invoke-virtual {p0, v0}, Lcom/google/android/location/activity/t;->a(Lcom/google/android/location/activity/av;)V

    goto :goto_0
.end method

.method protected an_()V
    .locals 4

    .prologue
    .line 67
    iget-object v0, p0, Lcom/google/android/location/activity/t;->d:Lcom/google/android/location/activity/k;

    iget-object v1, p0, Lcom/google/android/location/activity/t;->e:Lcom/google/android/location/os/bi;

    invoke-interface {v1}, Lcom/google/android/location/os/bi;->c()Lcom/google/android/location/j/b;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/location/j/b;->c()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/google/android/location/activity/k;->a(J)V

    .line 68
    iget-object v0, p0, Lcom/google/android/location/activity/t;->d:Lcom/google/android/location/activity/k;

    iget-object v1, p0, Lcom/google/android/location/activity/t;->e:Lcom/google/android/location/os/bi;

    invoke-interface {v1}, Lcom/google/android/location/os/bi;->c()Lcom/google/android/location/j/b;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/location/j/b;->c()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/google/android/location/activity/k;->b(J)V

    .line 69
    iget-object v0, p0, Lcom/google/android/location/activity/t;->d:Lcom/google/android/location/activity/k;

    iget-object v1, p0, Lcom/google/android/location/activity/t;->e:Lcom/google/android/location/os/bi;

    invoke-interface {v1}, Lcom/google/android/location/os/bi;->c()Lcom/google/android/location/j/b;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/location/j/b;->c()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/google/android/location/activity/k;->c(J)V

    .line 70
    return-void
.end method

.method protected ao_()V
    .locals 11

    .prologue
    .line 73
    invoke-virtual {p0}, Lcom/google/android/location/activity/t;->d()Lcom/google/android/location/activity/a/o;

    move-result-object v8

    .line 74
    invoke-virtual {p0}, Lcom/google/android/location/activity/t;->f()Ljava/lang/String;

    move-result-object v6

    .line 75
    iget-object v0, p0, Lcom/google/android/location/activity/t;->e:Lcom/google/android/location/os/bi;

    invoke-interface {v0}, Lcom/google/android/location/os/bi;->v()Lcom/google/android/location/os/j;

    move-result-object v2

    iget-object v0, p0, Lcom/google/android/location/activity/t;->d:Lcom/google/android/location/activity/k;

    invoke-virtual {v0}, Lcom/google/android/location/activity/k;->t()I

    move-result v7

    new-instance v1, Lcom/google/android/location/os/y;

    sget-object v3, Lcom/google/android/location/os/au;->V:Lcom/google/android/location/os/au;

    iget-object v0, v2, Lcom/google/android/location/os/j;->a:Lcom/google/android/location/os/at;

    invoke-interface {v0}, Lcom/google/android/location/os/at;->a()J

    move-result-wide v4

    invoke-direct/range {v1 .. v7}, Lcom/google/android/location/os/y;-><init>(Lcom/google/android/location/os/j;Lcom/google/android/location/os/au;JLjava/lang/String;I)V

    invoke-virtual {v6}, Ljava/lang/String;->hashCode()I

    move-result v0

    invoke-virtual {v2, v1, v0, v7}, Lcom/google/android/location/os/j;->a(Lcom/google/android/location/os/as;II)V

    .line 76
    sget-boolean v0, Lcom/google/android/location/i/a;->b:Z

    if-eqz v0, :cond_0

    const-string v0, "ActivityScheduler"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Starting activity detection at: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/location/activity/t;->e:Lcom/google/android/location/os/bi;

    invoke-interface {v2}, Lcom/google/android/location/os/bi;->c()Lcom/google/android/location/j/b;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/location/j/b;->c()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " with detector: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " at delay: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/location/activity/t;->d:Lcom/google/android/location/activity/k;

    invoke-virtual {v2}, Lcom/google/android/location/activity/k;->t()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/location/o/a/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 78
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/location/activity/t;->b()Lcom/google/android/location/activity/d;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/location/activity/t;->d:Lcom/google/android/location/activity/k;

    invoke-virtual {v1}, Lcom/google/android/location/activity/k;->t()I

    move-result v1

    invoke-virtual {p0}, Lcom/google/android/location/activity/t;->e()J

    move-result-wide v2

    invoke-virtual {p0}, Lcom/google/android/location/activity/t;->l()I

    move-result v4

    invoke-virtual {p0}, Lcom/google/android/location/activity/t;->m()D

    move-result-wide v5

    iget-object v7, p0, Lcom/google/android/location/activity/t;->d:Lcom/google/android/location/activity/k;

    invoke-virtual {v7}, Lcom/google/android/location/activity/k;->s()Lcom/google/android/location/o/n;

    move-result-object v9

    iget-object v7, p0, Lcom/google/android/location/activity/t;->d:Lcom/google/android/location/activity/k;

    invoke-virtual {v7}, Lcom/google/android/location/activity/k;->c()Z

    move-result v10

    move-object v7, v8

    move-object v8, p0

    invoke-virtual/range {v0 .. v10}, Lcom/google/android/location/activity/d;->a(IJIDLcom/google/android/location/activity/a/o;Lcom/google/android/location/activity/e;Lcom/google/android/location/o/n;Z)V

    .line 87
    return-void
.end method

.method public final ap_()V
    .locals 3

    .prologue
    .line 130
    iget-boolean v0, p0, Lcom/google/android/location/activity/t;->a:Z

    if-eqz v0, :cond_0

    .line 138
    :goto_0
    return-void

    .line 133
    :cond_0
    sget-boolean v0, Lcom/google/android/location/i/a;->d:Z

    if-eqz v0, :cond_1

    const-string v0, "ActivityScheduler"

    const-string v1, "Insufficient samples"

    invoke-static {v0, v1}, Lcom/google/android/location/o/a/a;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 134
    :cond_1
    invoke-static {}, Lcom/google/android/location/activity/k;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 135
    iget-object v0, p0, Lcom/google/android/location/activity/t;->d:Lcom/google/android/location/activity/k;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/location/activity/k;->a(Z)V

    .line 137
    :cond_2
    new-instance v0, Lcom/google/android/location/activity/ao;

    iget-object v1, p0, Lcom/google/android/location/activity/t;->d:Lcom/google/android/location/activity/k;

    iget-object v2, p0, Lcom/google/android/location/activity/t;->e:Lcom/google/android/location/os/bi;

    invoke-direct {v0, v1, v2}, Lcom/google/android/location/activity/ao;-><init>(Lcom/google/android/location/activity/k;Lcom/google/android/location/os/bi;)V

    invoke-virtual {p0, v0}, Lcom/google/android/location/activity/t;->a(Lcom/google/android/location/activity/av;)V

    goto :goto_0
.end method

.method protected abstract b()Lcom/google/android/location/activity/d;
.end method

.method protected abstract d()Lcom/google/android/location/activity/a/o;
.end method

.method protected e()J
    .locals 2

    .prologue
    .line 41
    invoke-virtual {p0}, Lcom/google/android/location/activity/t;->d()Lcom/google/android/location/activity/a/o;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/location/activity/a/o;->a()J

    move-result-wide v0

    return-wide v0
.end method

.method protected abstract f()Ljava/lang/String;
.end method

.method protected g()V
    .locals 0

    .prologue
    .line 51
    invoke-virtual {p0}, Lcom/google/android/location/activity/t;->ao_()V

    .line 52
    return-void
.end method

.method protected final h()V
    .locals 1

    .prologue
    .line 56
    invoke-super {p0}, Lcom/google/android/location/activity/av;->h()V

    .line 57
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/location/activity/t;->a:Z

    .line 58
    invoke-virtual {p0}, Lcom/google/android/location/activity/t;->b()Lcom/google/android/location/activity/d;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/location/activity/d;->a()V

    .line 59
    invoke-virtual {p0}, Lcom/google/android/location/activity/t;->an_()V

    .line 60
    return-void
.end method

.method protected final k()V
    .locals 3

    .prologue
    .line 105
    iget-object v0, p0, Lcom/google/android/location/activity/t;->d:Lcom/google/android/location/activity/k;

    invoke-virtual {v0}, Lcom/google/android/location/activity/k;->A()Ljava/util/Map;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 106
    iget-object v0, p0, Lcom/google/android/location/activity/t;->d:Lcom/google/android/location/activity/k;

    invoke-virtual {v0}, Lcom/google/android/location/activity/k;->A()Ljava/util/Map;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/location/activity/t;->f()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 108
    if-nez v0, :cond_0

    .line 109
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 111
    :cond_0
    iget-object v1, p0, Lcom/google/android/location/activity/t;->d:Lcom/google/android/location/activity/k;

    invoke-virtual {v1}, Lcom/google/android/location/activity/k;->A()Ljava/util/Map;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/location/activity/t;->f()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 114
    :cond_1
    return-void
.end method

.method protected l()I
    .locals 1

    .prologue
    .line 145
    const/16 v0, 0xa

    return v0
.end method

.method protected m()D
    .locals 2

    .prologue
    .line 153
    const-wide v0, 0x4041800000000000L    # 35.0

    return-wide v0
.end method

.method protected final n()V
    .locals 3

    .prologue
    .line 157
    invoke-static {}, Lcom/google/android/location/activity/k;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/location/activity/t;->d:Lcom/google/android/location/activity/k;

    invoke-virtual {v0}, Lcom/google/android/location/activity/k;->k()Lcom/google/android/location/activity/at;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/location/activity/at;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 160
    new-instance v0, Lcom/google/android/location/activity/as;

    iget-object v1, p0, Lcom/google/android/location/activity/t;->d:Lcom/google/android/location/activity/k;

    iget-object v2, p0, Lcom/google/android/location/activity/t;->e:Lcom/google/android/location/os/bi;

    invoke-direct {v0, v1, v2}, Lcom/google/android/location/activity/as;-><init>(Lcom/google/android/location/activity/k;Lcom/google/android/location/os/bi;)V

    invoke-virtual {p0, v0}, Lcom/google/android/location/activity/t;->a(Lcom/google/android/location/activity/av;)V

    .line 164
    :goto_0
    return-void

    .line 162
    :cond_0
    new-instance v0, Lcom/google/android/location/activity/ao;

    iget-object v1, p0, Lcom/google/android/location/activity/t;->d:Lcom/google/android/location/activity/k;

    iget-object v2, p0, Lcom/google/android/location/activity/t;->e:Lcom/google/android/location/os/bi;

    invoke-direct {v0, v1, v2}, Lcom/google/android/location/activity/ao;-><init>(Lcom/google/android/location/activity/k;Lcom/google/android/location/os/bi;)V

    invoke-virtual {p0, v0}, Lcom/google/android/location/activity/t;->a(Lcom/google/android/location/activity/av;)V

    goto :goto_0
.end method

.method protected final o()V
    .locals 9

    .prologue
    .line 171
    iget-object v0, p0, Lcom/google/android/location/activity/t;->e:Lcom/google/android/location/os/bi;

    invoke-interface {v0}, Lcom/google/android/location/os/bi;->c()Lcom/google/android/location/j/b;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/location/j/b;->d()J

    move-result-wide v6

    .line 172
    iget-object v0, p0, Lcom/google/android/location/activity/t;->e:Lcom/google/android/location/os/bi;

    invoke-interface {v0}, Lcom/google/android/location/os/bi;->c()Lcom/google/android/location/j/b;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/location/j/b;->c()J

    move-result-wide v0

    const-wide v4, 0x7fffffffffffffffL

    const/4 v8, 0x1

    move-wide v2, v0

    invoke-static/range {v0 .. v8}, Lcom/google/android/location/activity/bp;->a(JJJJZ)J

    move-result-wide v0

    .line 174
    iget-object v2, p0, Lcom/google/android/location/activity/t;->d:Lcom/google/android/location/activity/k;

    new-instance v3, Lcom/google/android/location/e/b;

    const-wide/16 v4, 0x3e8

    sub-long/2addr v0, v4

    const-wide/16 v4, 0x7d0

    invoke-direct {v3, v0, v1, v4, v5}, Lcom/google/android/location/e/b;-><init>(JJ)V

    invoke-virtual {v2, v3}, Lcom/google/android/location/activity/k;->a(Lcom/google/android/location/e/b;)V

    .line 176
    return-void
.end method
