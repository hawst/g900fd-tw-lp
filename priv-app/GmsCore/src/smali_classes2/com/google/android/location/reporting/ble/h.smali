.class public final Lcom/google/android/location/reporting/ble/h;
.super Lcom/google/android/location/reporting/ble/e;
.source "SourceFile"


# static fields
.field private static final a:J

.field private static final h:Lcom/google/android/gms/blescanner/compat/ScanSettings;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 21
    sget-object v0, Ljava/util/concurrent/TimeUnit;->HOURS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0x1

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    sput-wide v0, Lcom/google/android/location/reporting/ble/h;->a:J

    .line 45
    new-instance v0, Lcom/google/android/gms/blescanner/compat/ab;

    invoke-direct {v0}, Lcom/google/android/gms/blescanner/compat/ab;-><init>()V

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/gms/blescanner/compat/ab;->a(I)Lcom/google/android/gms/blescanner/compat/ab;

    move-result-object v0

    sget-wide v2, Lcom/google/android/location/reporting/ble/h;->a:J

    invoke-virtual {v0, v2, v3}, Lcom/google/android/gms/blescanner/compat/ab;->a(J)Lcom/google/android/gms/blescanner/compat/ab;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/blescanner/compat/ab;->a()Lcom/google/android/gms/blescanner/compat/ScanSettings;

    move-result-object v0

    sput-object v0, Lcom/google/android/location/reporting/ble/h;->h:Lcom/google/android/gms/blescanner/compat/ScanSettings;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/android/gms/common/util/p;)V
    .locals 1

    .prologue
    .line 24
    invoke-static {p1}, Lcom/google/android/location/reporting/ble/h;->a(Landroid/content/Context;)Lcom/google/android/gms/blescanner/compat/a;

    move-result-object v0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/location/reporting/ble/h;-><init>(Landroid/content/Context;Lcom/google/android/gms/common/util/p;Lcom/google/android/gms/blescanner/compat/a;)V

    .line 25
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Lcom/google/android/gms/common/util/p;Lcom/google/android/gms/blescanner/compat/a;)V
    .locals 1

    .prologue
    .line 32
    sget-object v0, Lcom/google/android/location/reporting/ble/h;->h:Lcom/google/android/gms/blescanner/compat/ScanSettings;

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/google/android/location/reporting/ble/e;-><init>(Landroid/content/Context;Lcom/google/android/gms/common/util/p;Lcom/google/android/gms/blescanner/compat/a;Lcom/google/android/gms/blescanner/compat/ScanSettings;)V

    .line 33
    return-void
.end method


# virtual methods
.method protected final a()V
    .locals 0

    .prologue
    .line 37
    invoke-virtual {p0}, Lcom/google/android/location/reporting/ble/h;->c()Z

    .line 38
    return-void
.end method

.method protected final b()V
    .locals 0

    .prologue
    .line 42
    invoke-virtual {p0}, Lcom/google/android/location/reporting/ble/h;->d()V

    .line 43
    return-void
.end method
