.class final Lcom/google/android/location/activity/an;
.super Lcom/google/android/location/activity/av;
.source "SourceFile"


# direct methods
.method public constructor <init>(Lcom/google/android/location/activity/k;Lcom/google/android/location/os/bi;)V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0, p1, p2}, Lcom/google/android/location/activity/av;-><init>(Lcom/google/android/location/activity/k;Lcom/google/android/location/os/bi;)V

    .line 15
    return-void
.end method

.method private b(Z)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 23
    sget-boolean v0, Lcom/google/android/location/i/a;->b:Z

    if-eqz v0, :cond_0

    const-string v0, "ActivityScheduler"

    const-string v1, "maybeChangeState(%s)"

    new-array v2, v5, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/location/o/a/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 24
    :cond_0
    iget-object v0, p0, Lcom/google/android/location/activity/an;->e:Lcom/google/android/location/os/bi;

    invoke-interface {v0}, Lcom/google/android/location/os/bi;->s()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/location/activity/an;->d:Lcom/google/android/location/activity/k;

    invoke-virtual {v0}, Lcom/google/android/location/activity/k;->K()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/location/activity/an;->e:Lcom/google/android/location/os/bi;

    invoke-interface {v0}, Lcom/google/android/location/os/bi;->u()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 26
    if-eqz p1, :cond_3

    .line 28
    iget-object v0, p0, Lcom/google/android/location/activity/an;->e:Lcom/google/android/location/os/bi;

    invoke-interface {v0}, Lcom/google/android/location/os/bi;->y()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/location/activity/an;->d:Lcom/google/android/location/activity/k;

    invoke-virtual {v0}, Lcom/google/android/location/activity/k;->c()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 29
    const/4 v0, 0x3

    invoke-virtual {p0, v0}, Lcom/google/android/location/activity/an;->a(I)V

    .line 38
    :cond_1
    :goto_0
    return-void

    .line 31
    :cond_2
    invoke-virtual {p0, v5}, Lcom/google/android/location/activity/an;->a(I)V

    goto :goto_0

    .line 35
    :cond_3
    new-instance v0, Lcom/google/android/location/activity/ao;

    iget-object v1, p0, Lcom/google/android/location/activity/an;->d:Lcom/google/android/location/activity/k;

    iget-object v2, p0, Lcom/google/android/location/activity/an;->e:Lcom/google/android/location/os/bi;

    invoke-direct {v0, v1, v2}, Lcom/google/android/location/activity/ao;-><init>(Lcom/google/android/location/activity/k;Lcom/google/android/location/os/bi;)V

    invoke-virtual {p0, v0}, Lcom/google/android/location/activity/an;->a(Lcom/google/android/location/activity/av;)V

    goto :goto_0
.end method


# virtual methods
.method protected final a()V
    .locals 0

    .prologue
    .line 43
    return-void
.end method

.method protected final a(Z)V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0, p1}, Lcom/google/android/location/activity/an;->b(Z)V

    .line 20
    return-void
.end method

.method public final ar_()V
    .locals 1

    .prologue
    .line 52
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/location/activity/an;->b(Z)V

    .line 53
    return-void
.end method

.method public final as_()V
    .locals 0

    .prologue
    .line 48
    return-void
.end method

.method protected final g()V
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Lcom/google/android/location/activity/an;->d:Lcom/google/android/location/activity/k;

    invoke-virtual {v0}, Lcom/google/android/location/activity/k;->h()Lcom/google/android/location/activity/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/location/activity/a;->a()V

    .line 58
    return-void
.end method

.method protected final h()V
    .locals 2

    .prologue
    .line 62
    invoke-super {p0}, Lcom/google/android/location/activity/av;->h()V

    .line 63
    invoke-static {}, Lcom/google/android/location/activity/k;->b()Z

    move-result v0

    if-nez v0, :cond_0

    .line 64
    iget-object v0, p0, Lcom/google/android/location/activity/an;->d:Lcom/google/android/location/activity/k;

    invoke-virtual {v0}, Lcom/google/android/location/activity/k;->h()Lcom/google/android/location/activity/a;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/location/activity/an;->d:Lcom/google/android/location/activity/k;

    invoke-virtual {v1}, Lcom/google/android/location/activity/k;->t()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/location/activity/a;->a(I)Z

    .line 66
    :cond_0
    return-void
.end method

.method public final p()Ljava/lang/String;
    .locals 1

    .prologue
    .line 70
    const-string v0, "OffState"

    return-object v0
.end method
