.class public final Lcom/google/android/location/copresence/g/a;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method public static a(Ljava/lang/String;Lcom/google/android/location/copresence/a/a;Lcom/google/android/location/copresence/o/k;Lcom/google/android/location/copresence/o/m;Lcom/google/android/location/copresence/g/c;Ljava/util/HashSet;)Lcom/google/android/location/copresence/g/b;
    .locals 5

    .prologue
    .line 155
    invoke-static {p0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/String;)Ljava/lang/String;

    .line 156
    invoke-static {p1}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 158
    new-instance v2, Lcom/google/android/location/copresence/g/b;

    const/4 v0, 0x0

    invoke-direct {v2, p4, v0}, Lcom/google/android/location/copresence/g/b;-><init>(Lcom/google/android/location/copresence/g/c;B)V

    .line 159
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    iget-object v0, p3, Lcom/google/android/location/copresence/o/m;->a:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/location/copresence/o/n;

    iget v1, v1, Lcom/google/android/location/copresence/o/n;->b:I

    invoke-static {v1}, Lcom/google/android/location/copresence/o/u;->a(I)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/copresence/o/n;

    iget-wide v0, v0, Lcom/google/android/location/copresence/o/n;->c:J

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 160
    :cond_1
    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3

    .line 161
    iget-object v0, p2, Lcom/google/android/location/copresence/o/k;->b:Lcom/google/android/location/copresence/ap;

    invoke-virtual {v0}, Lcom/google/android/location/copresence/ap;->c()V

    iget-object v0, p2, Lcom/google/android/location/copresence/o/k;->a:Lcom/google/android/location/copresence/o/d;

    invoke-virtual {v0, v3}, Lcom/google/android/location/copresence/o/d;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    .line 163
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_2
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/copresence/o/j;

    .line 165
    :try_start_0
    invoke-interface {p4}, Lcom/google/android/location/copresence/g/c;->a()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/protobuf/nano/j;

    iget-object v4, v0, Lcom/google/android/location/copresence/o/j;->c:[B

    invoke-static {v1, v4}, Lcom/google/protobuf/nano/j;->mergeFrom(Lcom/google/protobuf/nano/j;[B)Lcom/google/protobuf/nano/j;

    move-result-object v1

    .line 168
    iget-object v4, v2, Lcom/google/android/location/copresence/g/b;->a:Ljava/util/ArrayList;

    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 169
    iget-object v0, v0, Lcom/google/android/location/copresence/o/j;->b:Ljava/lang/String;

    iget-object v1, v2, Lcom/google/android/location/copresence/g/b;->b:Ljava/util/HashSet;

    invoke-virtual {v1, v0}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Lcom/google/protobuf/nano/i; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 170
    :catch_0
    move-exception v0

    .line 171
    const/4 v1, 0x6

    invoke-static {v1}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 172
    const-string v1, "EventLogDatabaseUtils: "

    invoke-static {v1, v0}, Lcom/google/android/location/copresence/ag;->a(Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1

    .line 177
    :cond_3
    iget-object v0, p3, Lcom/google/android/location/copresence/o/m;->b:Ljava/util/ArrayList;

    invoke-virtual {p5, v0}, Ljava/util/HashSet;->addAll(Ljava/util/Collection;)Z

    .line 178
    return-object v2
.end method

.method public static a(Lcom/google/android/location/copresence/a/a;Ljava/lang/String;Lcom/google/android/location/copresence/o/k;Ljava/util/HashSet;)[Ljava/lang/String;
    .locals 2

    .prologue
    .line 192
    invoke-static {p0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 193
    invoke-static {p1}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/String;)Ljava/lang/String;

    .line 194
    iget-object v0, p2, Lcom/google/android/location/copresence/o/k;->b:Lcom/google/android/location/copresence/ap;

    invoke-virtual {v0}, Lcom/google/android/location/copresence/ap;->c()V

    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-virtual {p2, p1, p0, v0, v1}, Lcom/google/android/location/copresence/o/k;->a(Ljava/lang/String;Lcom/google/android/location/copresence/a/a;II)Lcom/google/android/location/copresence/o/m;

    move-result-object v0

    invoke-static {v0, p3}, Lcom/google/android/location/copresence/g/a;->a(Lcom/google/android/location/copresence/o/m;Ljava/util/HashSet;)[Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static a(Lcom/google/android/location/copresence/o/m;Ljava/util/HashSet;)[Ljava/lang/String;
    .locals 4

    .prologue
    .line 219
    iget-object v0, p0, Lcom/google/android/location/copresence/o/m;->b:Ljava/util/ArrayList;

    invoke-virtual {p1, v0}, Ljava/util/HashSet;->addAll(Ljava/util/Collection;)Z

    .line 220
    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    iget-object v0, p0, Lcom/google/android/location/copresence/o/m;->a:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/location/copresence/o/n;

    iget v1, v1, Lcom/google/android/location/copresence/o/n;->b:I

    invoke-static {v1}, Lcom/google/android/location/copresence/o/u;->a(I)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 221
    :cond_1
    invoke-interface {v2}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    .line 222
    invoke-interface {v2}, Ljava/util/Set;->size()I

    move-result v0

    new-array v0, v0, [Ljava/lang/String;

    invoke-interface {v2, v0}, Ljava/util/Set;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    .line 224
    :goto_1
    return-object v0

    :cond_2
    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/String;

    goto :goto_1
.end method

.method public static b(Lcom/google/android/location/copresence/a/a;Ljava/lang/String;Lcom/google/android/location/copresence/o/k;Ljava/util/HashSet;)[Ljava/lang/String;
    .locals 2

    .prologue
    .line 210
    invoke-static {p0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 211
    invoke-static {p1}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/String;)Ljava/lang/String;

    .line 212
    iget-object v0, p2, Lcom/google/android/location/copresence/o/k;->b:Lcom/google/android/location/copresence/ap;

    invoke-virtual {v0}, Lcom/google/android/location/copresence/ap;->c()V

    const/4 v0, 0x3

    const/4 v1, 0x2

    invoke-virtual {p2, p1, p0, v0, v1}, Lcom/google/android/location/copresence/o/k;->a(Ljava/lang/String;Lcom/google/android/location/copresence/a/a;II)Lcom/google/android/location/copresence/o/m;

    move-result-object v0

    invoke-static {v0, p3}, Lcom/google/android/location/copresence/g/a;->a(Lcom/google/android/location/copresence/o/m;Ljava/util/HashSet;)[Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
