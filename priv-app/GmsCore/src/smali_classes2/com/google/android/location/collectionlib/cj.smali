.class public final Lcom/google/android/location/collectionlib/cj;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/hardware/SensorEventListener;


# instance fields
.field final synthetic a:Lcom/google/android/location/collectionlib/ci;


# direct methods
.method public constructor <init>(Lcom/google/android/location/collectionlib/ci;)V
    .locals 0

    .prologue
    .line 40
    iput-object p1, p0, Lcom/google/android/location/collectionlib/cj;->a:Lcom/google/android/location/collectionlib/ci;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static a([FI)F
    .locals 1

    .prologue
    .line 181
    array-length v0, p0

    if-ge p1, v0, :cond_0

    if-gez p1, :cond_1

    .line 182
    :cond_0
    const v0, 0x7f7fffff    # Float.MAX_VALUE

    .line 184
    :goto_0
    return v0

    :cond_1
    aget v0, p0, p1

    goto :goto_0
.end method

.method private static a(I)Lcom/google/android/location/collectionlib/cg;
    .locals 2

    .prologue
    .line 188
    sget-object v0, Lcom/google/android/location/collectionlib/RealCollectorConfig;->c:Ljava/util/Map;

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/collectionlib/cg;

    .line 189
    if-nez v0, :cond_0

    .line 190
    sget-object v0, Lcom/google/android/location/collectionlib/cg;->r:Lcom/google/android/location/collectionlib/cg;

    .line 192
    :cond_0
    return-object v0
.end method

.method private a(Lcom/google/android/location/collectionlib/cg;)Z
    .locals 1

    .prologue
    .line 90
    iget-object v0, p0, Lcom/google/android/location/collectionlib/cj;->a:Lcom/google/android/location/collectionlib/ci;

    invoke-static {v0}, Lcom/google/android/location/collectionlib/ci;->b(Lcom/google/android/location/collectionlib/ci;)Lcom/google/android/location/collectionlib/SensorScannerConfig;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/location/collectionlib/cj;->a:Lcom/google/android/location/collectionlib/ci;

    invoke-static {v0}, Lcom/google/android/location/collectionlib/ci;->b(Lcom/google/android/location/collectionlib/ci;)Lcom/google/android/location/collectionlib/SensorScannerConfig;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/location/collectionlib/SensorScannerConfig;->a(Lcom/google/android/location/collectionlib/cg;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(IJJIFFFLandroid/hardware/SensorEvent;)V
    .locals 16

    .prologue
    .line 96
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/location/collectionlib/cj;->a:Lcom/google/android/location/collectionlib/ci;

    invoke-virtual {v4}, Lcom/google/android/location/collectionlib/ci;->e()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 114
    :goto_0
    return-void

    .line 100
    :cond_0
    invoke-static/range {p1 .. p1}, Lcom/google/android/location/collectionlib/cj;->a(I)Lcom/google/android/location/collectionlib/cg;

    move-result-object v14

    .line 101
    move-object/from16 v0, p0

    invoke-direct {v0, v14}, Lcom/google/android/location/collectionlib/cj;->a(Lcom/google/android/location/collectionlib/cg;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 105
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/location/collectionlib/cj;->a:Lcom/google/android/location/collectionlib/ci;

    iget-object v4, v4, Lcom/google/android/location/collectionlib/ce;->d:Lcom/google/android/location/collectionlib/ak;

    move/from16 v5, p1

    move/from16 v6, p7

    move/from16 v7, p8

    move/from16 v8, p9

    move/from16 v9, p6

    move-wide/from16 v10, p2

    move-wide/from16 v12, p4

    invoke-virtual/range {v4 .. v13}, Lcom/google/android/location/collectionlib/ak;->a(IFFFIJJ)V

    .line 113
    :cond_1
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/location/collectionlib/cj;->a:Lcom/google/android/location/collectionlib/ci;

    move-wide/from16 v0, p4

    move-object/from16 v2, p10

    invoke-virtual {v4, v14, v0, v1, v2}, Lcom/google/android/location/collectionlib/ci;->b(Lcom/google/android/location/collectionlib/cg;JLandroid/hardware/SensorEvent;)V

    goto :goto_0
.end method

.method public final a(I[J[J[F[F[F)V
    .locals 15

    .prologue
    .line 123
    iget-object v2, p0, Lcom/google/android/location/collectionlib/cj;->a:Lcom/google/android/location/collectionlib/ci;

    invoke-virtual {v2}, Lcom/google/android/location/collectionlib/ci;->e()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 144
    :cond_0
    return-void

    .line 127
    :cond_1
    invoke-static/range {p1 .. p1}, Lcom/google/android/location/collectionlib/cj;->a(I)Lcom/google/android/location/collectionlib/cg;

    move-result-object v13

    .line 128
    move-object/from16 v0, p4

    array-length v14, v0

    .line 129
    const/4 v2, 0x0

    move v12, v2

    :goto_0
    if-ge v12, v14, :cond_0

    .line 130
    invoke-direct {p0, v13}, Lcom/google/android/location/collectionlib/cj;->a(Lcom/google/android/location/collectionlib/cg;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 134
    iget-object v2, p0, Lcom/google/android/location/collectionlib/cj;->a:Lcom/google/android/location/collectionlib/ci;

    iget-object v2, v2, Lcom/google/android/location/collectionlib/ce;->d:Lcom/google/android/location/collectionlib/ak;

    aget v4, p4, v12

    aget v5, p5, v12

    aget v6, p6, v12

    const/4 v7, 0x0

    aget-wide v8, p2, v12

    aget-wide v10, p3, v12

    move/from16 v3, p1

    invoke-virtual/range {v2 .. v11}, Lcom/google/android/location/collectionlib/ak;->a(IFFFIJJ)V

    .line 142
    :cond_2
    iget-object v2, p0, Lcom/google/android/location/collectionlib/cj;->a:Lcom/google/android/location/collectionlib/ci;

    aget-wide v4, p3, v12

    const/4 v3, 0x0

    invoke-virtual {v2, v13, v4, v5, v3}, Lcom/google/android/location/collectionlib/ci;->b(Lcom/google/android/location/collectionlib/cg;JLandroid/hardware/SensorEvent;)V

    .line 129
    add-int/lit8 v2, v12, 0x1

    move v12, v2

    goto :goto_0
.end method

.method public final onAccuracyChanged(Landroid/hardware/Sensor;I)V
    .locals 0

    .prologue
    .line 174
    return-void
.end method

.method public final onSensorChanged(Landroid/hardware/SensorEvent;)V
    .locals 21

    .prologue
    .line 45
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v14

    .line 47
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/location/collectionlib/cj;->a:Lcom/google/android/location/collectionlib/ci;

    invoke-virtual {v2}, Lcom/google/android/location/collectionlib/ci;->f()V

    .line 49
    move-object/from16 v0, p1

    iget-object v2, v0, Landroid/hardware/SensorEvent;->sensor:Landroid/hardware/Sensor;

    invoke-virtual {v2}, Landroid/hardware/Sensor;->getType()I

    move-result v4

    .line 50
    move-object/from16 v0, p1

    iget-object v2, v0, Landroid/hardware/SensorEvent;->values:[F

    .line 55
    const/16 v3, 0x13

    if-eq v4, v3, :cond_0

    const/4 v3, 0x5

    if-ne v4, v3, :cond_2

    .line 58
    :cond_0
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/location/collectionlib/cj;->a:Lcom/google/android/location/collectionlib/ci;

    invoke-static {v3}, Lcom/google/android/location/collectionlib/ci;->a(Lcom/google/android/location/collectionlib/ci;)[Z

    move-result-object v3

    aget-boolean v3, v3, v4

    if-nez v3, :cond_2

    .line 59
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/location/collectionlib/cj;->a:Lcom/google/android/location/collectionlib/ci;

    invoke-static {v2}, Lcom/google/android/location/collectionlib/ci;->a(Lcom/google/android/location/collectionlib/ci;)[Z

    move-result-object v2

    const/4 v3, 0x1

    aput-boolean v3, v2, v4

    .line 60
    sget-boolean v2, Lcom/google/android/location/i/a;->d:Z

    if-eqz v2, :cond_1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/location/collectionlib/cj;->a:Lcom/google/android/location/collectionlib/ci;

    iget-object v2, v2, Lcom/google/android/location/collectionlib/ci;->c:Lcom/google/android/location/o/a/c;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v5, "Ignored first event from sensor "

    invoke-direct {v3, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/android/location/o/a/c;->c(Ljava/lang/String;)V

    .line 87
    :cond_1
    :goto_0
    return-void

    .line 65
    :cond_2
    const/16 v3, 0xe

    if-ne v4, v3, :cond_4

    .line 66
    move-object/from16 v0, p1

    iget-wide v12, v0, Landroid/hardware/SensorEvent;->timestamp:J

    move-object/from16 v0, p1

    iget v11, v0, Landroid/hardware/SensorEvent;->accuracy:I

    const/4 v3, 0x0

    aget v5, v2, v3

    const/4 v3, 0x1

    aget v6, v2, v3

    const/4 v3, 0x2

    aget v7, v2, v3

    const/4 v3, 0x3

    aget v8, v2, v3

    const/4 v3, 0x4

    aget v9, v2, v3

    const/4 v3, 0x5

    aget v10, v2, v3

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/location/collectionlib/cj;->a:Lcom/google/android/location/collectionlib/ci;

    invoke-virtual {v2}, Lcom/google/android/location/collectionlib/ci;->e()Z

    move-result v2

    if-nez v2, :cond_1

    invoke-static {v4}, Lcom/google/android/location/collectionlib/cj;->a(I)Lcom/google/android/location/collectionlib/cg;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/google/android/location/collectionlib/cj;->a(Lcom/google/android/location/collectionlib/cg;)Z

    move-result v3

    if-nez v3, :cond_3

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/location/collectionlib/cj;->a:Lcom/google/android/location/collectionlib/ci;

    iget-object v3, v3, Lcom/google/android/location/collectionlib/ce;->d:Lcom/google/android/location/collectionlib/ak;

    invoke-virtual/range {v3 .. v15}, Lcom/google/android/location/collectionlib/ak;->a(IFFFFFFIJJ)V

    :cond_3
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/location/collectionlib/cj;->a:Lcom/google/android/location/collectionlib/ci;

    move-object/from16 v0, p1

    invoke-virtual {v3, v2, v14, v15, v0}, Lcom/google/android/location/collectionlib/ci;->b(Lcom/google/android/location/collectionlib/cg;JLandroid/hardware/SensorEvent;)V

    goto :goto_0

    .line 78
    :cond_4
    move-object/from16 v0, p1

    iget-wide v12, v0, Landroid/hardware/SensorEvent;->timestamp:J

    move-object/from16 v0, p1

    iget v0, v0, Landroid/hardware/SensorEvent;->accuracy:I

    move/from16 v16, v0

    const/4 v3, 0x0

    invoke-static {v2, v3}, Lcom/google/android/location/collectionlib/cj;->a([FI)F

    move-result v17

    const/4 v3, 0x1

    invoke-static {v2, v3}, Lcom/google/android/location/collectionlib/cj;->a([FI)F

    move-result v18

    const/4 v3, 0x2

    invoke-static {v2, v3}, Lcom/google/android/location/collectionlib/cj;->a([FI)F

    move-result v19

    move-object/from16 v10, p0

    move v11, v4

    move-object/from16 v20, p1

    invoke-virtual/range {v10 .. v20}, Lcom/google/android/location/collectionlib/cj;->a(IJJIFFFLandroid/hardware/SensorEvent;)V

    goto :goto_0
.end method
