.class public final Lcom/google/android/location/geofencer/service/ae;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/location/activity/au;


# instance fields
.field final a:Lcom/google/android/location/activity/at;

.field final b:Lcom/google/android/location/h/c;

.field c:Landroid/location/Location;

.field d:Landroid/net/wifi/WifiInfo;

.field e:Z

.field private final f:Lcom/google/android/location/geofencer/service/k;

.field private final g:Landroid/net/wifi/WifiManager;

.field private final h:Lcom/google/android/location/fused/c;


# direct methods
.method public constructor <init>(Lcom/google/android/location/geofencer/service/k;Lcom/google/android/location/h/c;Lcom/google/android/location/activity/at;Landroid/net/wifi/WifiManager;)V
    .locals 1

    .prologue
    .line 46
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    new-instance v0, Lcom/google/android/location/fused/c;

    invoke-direct {v0}, Lcom/google/android/location/fused/c;-><init>()V

    iput-object v0, p0, Lcom/google/android/location/geofencer/service/ae;->h:Lcom/google/android/location/fused/c;

    .line 40
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/location/geofencer/service/ae;->e:Z

    .line 47
    iput-object p1, p0, Lcom/google/android/location/geofencer/service/ae;->f:Lcom/google/android/location/geofencer/service/k;

    .line 48
    iput-object p3, p0, Lcom/google/android/location/geofencer/service/ae;->a:Lcom/google/android/location/activity/at;

    .line 49
    iput-object p4, p0, Lcom/google/android/location/geofencer/service/ae;->g:Landroid/net/wifi/WifiManager;

    .line 50
    iput-object p2, p0, Lcom/google/android/location/geofencer/service/ae;->b:Lcom/google/android/location/h/c;

    .line 51
    return-void
.end method


# virtual methods
.method final a()Landroid/net/wifi/WifiInfo;
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 57
    iget-object v1, p0, Lcom/google/android/location/geofencer/service/ae;->g:Landroid/net/wifi/WifiManager;

    if-nez v1, :cond_1

    .line 71
    :cond_0
    :goto_0
    return-object v0

    .line 60
    :cond_1
    iget-object v1, p0, Lcom/google/android/location/geofencer/service/ae;->g:Landroid/net/wifi/WifiManager;

    invoke-virtual {v1}, Landroid/net/wifi/WifiManager;->getConnectionInfo()Landroid/net/wifi/WifiInfo;

    move-result-object v1

    .line 61
    if-eqz v1, :cond_0

    .line 71
    invoke-virtual {v1}, Landroid/net/wifi/WifiInfo;->getRssi()I

    move-result v2

    const/16 v3, -0x7f

    if-le v2, v3, :cond_0

    move-object v0, v1

    goto :goto_0
.end method

.method final a(Landroid/util/Pair;)Landroid/util/Pair;
    .locals 6

    .prologue
    const-wide/16 v4, 0x3e8

    .line 181
    iget-object v0, p0, Lcom/google/android/location/geofencer/service/ae;->c:Landroid/location/Location;

    if-nez v0, :cond_0

    .line 182
    const-string v0, "LocationFilter"

    const-string v1, "createFromStableLocation: mStableLocation is unknown."

    invoke-static {v0, v1}, Lcom/google/android/location/geofencer/a/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 191
    :goto_0
    return-object p1

    .line 185
    :cond_0
    new-instance v1, Landroid/location/Location;

    const-string v0, "geofencing"

    invoke-direct {v1, v0}, Landroid/location/Location;-><init>(Ljava/lang/String;)V

    .line 186
    iget-object v0, p0, Lcom/google/android/location/geofencer/service/ae;->c:Landroid/location/Location;

    invoke-virtual {v0}, Landroid/location/Location;->getLatitude()D

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Landroid/location/Location;->setLatitude(D)V

    .line 187
    iget-object v0, p0, Lcom/google/android/location/geofencer/service/ae;->c:Landroid/location/Location;

    invoke-virtual {v0}, Landroid/location/Location;->getLongitude()D

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Landroid/location/Location;->setLongitude(D)V

    .line 188
    iget-object v0, p0, Lcom/google/android/location/geofencer/service/ae;->c:Landroid/location/Location;

    invoke-virtual {v0}, Landroid/location/Location;->getAccuracy()F

    move-result v0

    invoke-virtual {v1, v0}, Landroid/location/Location;->setAccuracy(F)V

    .line 189
    iget-object v0, p1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Landroid/location/Location;

    invoke-virtual {v0}, Landroid/location/Location;->getTime()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Landroid/location/Location;->setTime(J)V

    .line 190
    iget-object v0, p0, Lcom/google/android/location/geofencer/service/ae;->h:Lcom/google/android/location/fused/c;

    iget-object v0, p1, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    mul-long/2addr v2, v4

    mul-long/2addr v2, v4

    invoke-static {v1, v2, v3}, Lcom/google/android/location/fused/c;->a(Landroid/location/Location;J)V

    .line 191
    iget-object v0, p1, Landroid/util/Pair;->first:Ljava/lang/Object;

    invoke-static {v0, v1}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object p1

    goto :goto_0
.end method

.method final b()V
    .locals 2

    .prologue
    .line 86
    iget-object v0, p0, Lcom/google/android/location/geofencer/service/ae;->c:Landroid/location/Location;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/location/geofencer/service/ae;->b:Lcom/google/android/location/h/c;

    invoke-static {}, Lcom/google/android/location/activity/k;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/location/geofencer/service/ae;->a:Lcom/google/android/location/activity/at;

    invoke-interface {v0}, Lcom/google/android/location/activity/at;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/location/geofencer/service/ae;->a:Lcom/google/android/location/activity/at;

    invoke-interface {v0}, Lcom/google/android/location/activity/at;->c()Z

    move-result v0

    if-nez v0, :cond_0

    .line 88
    iget-object v0, p0, Lcom/google/android/location/geofencer/service/ae;->a:Lcom/google/android/location/activity/at;

    invoke-interface {v0, p0}, Lcom/google/android/location/activity/at;->a(Lcom/google/android/location/activity/au;)Z

    .line 89
    const-string v0, "LocationFilter"

    const-string v1, "Significant motion detection enabled."

    invoke-static {v0, v1}, Lcom/google/android/location/geofencer/a/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 91
    :cond_0
    return-void
.end method

.method public final l()V
    .locals 1

    .prologue
    .line 196
    iget-object v0, p0, Lcom/google/android/location/geofencer/service/ae;->f:Lcom/google/android/location/geofencer/service/k;

    invoke-virtual {v0}, Lcom/google/android/location/geofencer/service/k;->g()V

    .line 197
    return-void
.end method
