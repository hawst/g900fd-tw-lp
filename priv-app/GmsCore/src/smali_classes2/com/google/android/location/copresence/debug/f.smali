.class public final Lcom/google/android/location/copresence/debug/f;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final a:Ljava/util/Map;


# instance fields
.field private final b:Landroid/content/Context;

.field private final c:Landroid/content/SharedPreferences;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 29
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/google/android/location/copresence/debug/f;->a:Ljava/util/Map;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    iput-object p1, p0, Lcom/google/android/location/copresence/debug/f;->b:Landroid/content/Context;

    .line 36
    iget-object v0, p0, Lcom/google/android/location/copresence/debug/f;->b:Landroid/content/Context;

    const-string v1, "copresence_debug_pref"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/copresence/debug/f;->c:Landroid/content/SharedPreferences;

    .line 37
    return-void
.end method

.method public static a(Landroid/content/Context;)Lcom/google/android/location/copresence/debug/f;
    .locals 2

    .prologue
    .line 40
    sget-object v0, Lcom/google/android/location/copresence/debug/f;->a:Ljava/util/Map;

    invoke-interface {v0, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/copresence/debug/f;

    .line 41
    if-nez v0, :cond_0

    .line 42
    new-instance v0, Lcom/google/android/location/copresence/debug/f;

    invoke-direct {v0, p0}, Lcom/google/android/location/copresence/debug/f;-><init>(Landroid/content/Context;)V

    .line 43
    sget-object v1, Lcom/google/android/location/copresence/debug/f;->a:Ljava/util/Map;

    invoke-interface {v1, p0, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 45
    :cond_0
    return-object v0
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 3

    .prologue
    .line 49
    iget-object v0, p0, Lcom/google/android/location/copresence/debug/f;->c:Landroid/content/SharedPreferences;

    const-string v1, "apiary_tracing_token"

    const-string v2, ""

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
