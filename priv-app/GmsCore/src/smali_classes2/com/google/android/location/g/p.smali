.class public final Lcom/google/android/location/g/p;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field private final a:Ljava/util/List;


# direct methods
.method private constructor <init>(Ljava/util/List;)V
    .locals 0

    .prologue
    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    iput-object p1, p0, Lcom/google/android/location/g/p;->a:Ljava/util/List;

    .line 39
    return-void
.end method

.method private static a(Lcom/google/p/a/b/b/a;)Lcom/google/android/location/g/p;
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 73
    invoke-virtual {p0, v4}, Lcom/google/p/a/b/b/a;->k(I)I

    move-result v1

    .line 74
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 75
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    .line 76
    invoke-virtual {p0, v4, v0}, Lcom/google/p/a/b/b/a;->d(II)Lcom/google/p/a/b/b/a;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/location/g/c;->a(Lcom/google/p/a/b/b/a;)Lcom/google/android/location/g/c;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 75
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 78
    :cond_0
    new-instance v0, Lcom/google/android/location/g/p;

    invoke-direct {v0, v2}, Lcom/google/android/location/g/p;-><init>(Ljava/util/List;)V

    return-object v0
.end method

.method static a(Ljava/io/InputStream;)Lcom/google/android/location/g/p;
    .locals 4

    .prologue
    .line 50
    new-instance v0, Lcom/google/p/a/b/b/c;

    const/4 v1, 0x3

    invoke-direct {v0, v1}, Lcom/google/p/a/b/b/c;-><init>(I)V

    .line 51
    new-instance v1, Lcom/google/p/a/b/b/a;

    invoke-direct {v1, v0}, Lcom/google/p/a/b/b/a;-><init>(Lcom/google/p/a/b/b/c;)V

    .line 53
    :try_start_0
    invoke-static {p0, v1}, Lcom/google/android/location/o/j;->a(Ljava/io/InputStream;Lcom/google/p/a/b/b/a;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 59
    :try_start_1
    invoke-virtual {p0}, Ljava/io/InputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    .line 64
    :cond_0
    :goto_0
    invoke-static {v1}, Lcom/google/android/location/g/p;->a(Lcom/google/p/a/b/b/a;)Lcom/google/android/location/g/p;

    move-result-object v0

    return-object v0

    .line 60
    :catch_0
    move-exception v0

    .line 61
    sget-boolean v2, Lcom/google/android/location/i/a;->e:Z

    if-eqz v2, :cond_0

    const-string v2, "MetricModel"

    const-string v3, "Unable to close metric model stream"

    invoke-static {v2, v3, v0}, Lcom/google/android/location/o/a/a;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 54
    :catch_1
    move-exception v0

    .line 55
    :try_start_2
    new-instance v1, Ljava/lang/RuntimeException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unable to parse stream to load metric model. "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 58
    :catchall_0
    move-exception v0

    .line 59
    :try_start_3
    invoke-virtual {p0}, Ljava/io/InputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2

    .line 62
    :cond_1
    :goto_1
    throw v0

    .line 60
    :catch_2
    move-exception v1

    .line 61
    sget-boolean v2, Lcom/google/android/location/i/a;->e:Z

    if-eqz v2, :cond_1

    const-string v2, "MetricModel"

    const-string v3, "Unable to close metric model stream"

    invoke-static {v2, v3, v1}, Lcom/google/android/location/o/a/a;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1
.end method


# virtual methods
.method final a()Ljava/util/List;
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Lcom/google/android/location/g/p;->a:Ljava/util/List;

    return-object v0
.end method
