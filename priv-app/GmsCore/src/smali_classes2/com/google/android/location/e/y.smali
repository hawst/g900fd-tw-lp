.class public final Lcom/google/android/location/e/y;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public final a:I

.field public final b:I

.field public final c:I

.field public final d:Lcom/google/android/location/o/n;

.field public e:J

.field public f:J


# direct methods
.method public constructor <init>(IIILcom/google/android/location/o/n;)V
    .locals 2

    .prologue
    const-wide/32 v0, 0x7fffffff

    .line 55
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 56
    iput-object p4, p0, Lcom/google/android/location/e/y;->d:Lcom/google/android/location/o/n;

    .line 57
    iput p1, p0, Lcom/google/android/location/e/y;->a:I

    .line 58
    iput p2, p0, Lcom/google/android/location/e/y;->b:I

    .line 59
    iput-wide v0, p0, Lcom/google/android/location/e/y;->f:J

    .line 60
    iput-wide v0, p0, Lcom/google/android/location/e/y;->e:J

    .line 61
    iput p3, p0, Lcom/google/android/location/e/y;->c:I

    .line 62
    return-void
.end method


# virtual methods
.method public final a(J)J
    .locals 5

    .prologue
    .line 73
    iget v0, p0, Lcom/google/android/location/e/y;->a:I

    int-to-long v0, v0

    const-wide/16 v2, 0x3e8

    mul-long/2addr v0, v2

    add-long/2addr v0, p1

    return-wide v0
.end method

.method public final a()Z
    .locals 4

    .prologue
    .line 81
    iget v0, p0, Lcom/google/android/location/e/y;->a:I

    int-to-long v0, v0

    const-wide/16 v2, 0x14

    cmp-long v0, v0, v2

    if-gtz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()J
    .locals 4

    .prologue
    .line 85
    iget v0, p0, Lcom/google/android/location/e/y;->c:I

    int-to-long v0, v0

    const-wide/16 v2, 0x3e8

    mul-long/2addr v0, v2

    return-wide v0
.end method

.method public final b(J)J
    .locals 5

    .prologue
    .line 77
    iget v0, p0, Lcom/google/android/location/e/y;->b:I

    int-to-long v0, v0

    const-wide/16 v2, 0x3e8

    mul-long/2addr v0, v2

    add-long/2addr v0, p1

    return-wide v0
.end method

.method public final c()Z
    .locals 2

    .prologue
    .line 108
    iget v0, p0, Lcom/google/android/location/e/y;->a:I

    const v1, 0x7fffffff

    if-ge v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final d()Z
    .locals 2

    .prologue
    .line 112
    iget v0, p0, Lcom/google/android/location/e/y;->b:I

    const v1, 0x7fffffff

    if-ge v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
