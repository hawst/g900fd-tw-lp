.class public final Lcom/google/android/location/geofencer/service/g;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public a:Lcom/google/android/location/geofencer/service/k;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    invoke-static {p1}, Lcom/google/android/location/geofencer/a/a;->a(Landroid/content/Context;)V

    .line 31
    invoke-static {p1}, Lcom/google/android/location/geofencer/service/k;->a(Landroid/content/Context;)Lcom/google/android/location/geofencer/service/k;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/geofencer/service/g;->a:Lcom/google/android/location/geofencer/service/k;

    .line 32
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/location/GeofencingRequest;Landroid/app/PendingIntent;Lcom/google/android/location/geofencer/service/f;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 45
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/google/android/gms/location/GeofencingRequest;->b()Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/google/android/gms/location/GeofencingRequest;->b()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "Invalid GeofencingRequest request."

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/bx;->b(ZLjava/lang/Object;)V

    .line 49
    const-string v0, "PendingIntent not specified."

    invoke-static {p2, v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 50
    const-string v0, "Package name not specified."

    invoke-static {p4, v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 51
    iget-object v0, p0, Lcom/google/android/location/geofencer/service/g;->a:Lcom/google/android/location/geofencer/service/k;

    invoke-virtual {v0, p1, p3, p2}, Lcom/google/android/location/geofencer/service/k;->a(Lcom/google/android/gms/location/GeofencingRequest;Lcom/google/android/location/geofencer/service/f;Landroid/app/PendingIntent;)V

    .line 52
    return-void

    .line 45
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Lcom/google/android/location/geofencer/service/f;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 55
    const-string v0, "Package name not specified."

    invoke-static {p2, v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 56
    invoke-static {p2, p1}, Lcom/google/android/location/geofencer/service/an;->a(Ljava/lang/String;Lcom/google/android/location/geofencer/service/f;)Lcom/google/android/location/geofencer/service/an;

    move-result-object v0

    .line 58
    iget-object v1, p0, Lcom/google/android/location/geofencer/service/g;->a:Lcom/google/android/location/geofencer/service/k;

    invoke-virtual {v1, v0}, Lcom/google/android/location/geofencer/service/k;->a(Lcom/google/android/location/geofencer/service/an;)V

    .line 59
    return-void
.end method

.method public final a([Ljava/lang/String;Lcom/google/android/location/geofencer/service/f;Ljava/lang/String;)V
    .locals 6

    .prologue
    .line 63
    if-eqz p1, :cond_0

    array-length v0, p1

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "geofenceRequestIds can not be null or empty."

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/bx;->b(ZLjava/lang/Object;)V

    .line 65
    const-string v0, "Package name not specified."

    invoke-static {p3, v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 66
    new-instance v0, Lcom/google/android/location/geofencer/service/an;

    const/4 v1, 0x2

    const/4 v2, 0x0

    move-object v3, p1

    move-object v4, p3

    move-object v5, p2

    invoke-direct/range {v0 .. v5}, Lcom/google/android/location/geofencer/service/an;-><init>(ILandroid/app/PendingIntent;[Ljava/lang/String;Ljava/lang/String;Lcom/google/android/location/geofencer/service/f;)V

    .line 68
    iget-object v1, p0, Lcom/google/android/location/geofencer/service/g;->a:Lcom/google/android/location/geofencer/service/k;

    invoke-virtual {v1, v0}, Lcom/google/android/location/geofencer/service/k;->a(Lcom/google/android/location/geofencer/service/an;)V

    .line 69
    return-void

    .line 63
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
