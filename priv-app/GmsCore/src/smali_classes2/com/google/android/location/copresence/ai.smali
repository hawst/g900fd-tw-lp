.class final Lcom/google/android/location/copresence/ai;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/location/copresence/l/ab;


# instance fields
.field final synthetic a:Lcom/google/android/location/copresence/ah;


# direct methods
.method constructor <init>(Lcom/google/android/location/copresence/ah;)V
    .locals 0

    .prologue
    .line 53
    iput-object p1, p0, Lcom/google/android/location/copresence/ai;->a:Lcom/google/android/location/copresence/ah;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/location/copresence/l/aa;I)V
    .locals 3

    .prologue
    .line 71
    invoke-interface {p1}, Lcom/google/android/location/copresence/l/aa;->c()Lcom/google/android/location/copresence/a/a;

    move-result-object v0

    .line 72
    const/4 v1, 0x3

    invoke-static {v1}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 73
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Copresence registration failed for account: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->a(Lcom/google/android/location/copresence/a/a;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->b(Ljava/lang/String;)I

    .line 76
    :cond_0
    iget-object v0, p0, Lcom/google/android/location/copresence/ai;->a:Lcom/google/android/location/copresence/ah;

    iget-object v0, v0, Lcom/google/android/location/copresence/ah;->f:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 77
    return-void
.end method

.method public final synthetic a(Lcom/google/android/location/copresence/l/aa;Ljava/lang/Object;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 53
    check-cast p2, Ljava/lang/String;

    const/4 v0, 0x3

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "Successfully registered device with copresence servers."

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->b(Ljava/lang/String;)I

    :cond_0
    iget-object v0, p0, Lcom/google/android/location/copresence/ai;->a:Lcom/google/android/location/copresence/ah;

    iget-object v0, v0, Lcom/google/android/location/copresence/ah;->b:Landroid/content/Context;

    const-string v1, "copresence_gcm_pref"

    invoke-virtual {v0, v1, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "copresence_uuid"

    invoke-interface {v0, v1, p2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "REGISTERED_"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {p1}, Lcom/google/android/location/copresence/l/aa;->c()Lcom/google/android/location/copresence/a/a;

    move-result-object v2

    iget-object v2, v2, Lcom/google/android/location/copresence/a/a;->a:Landroid/accounts/Account;

    iget-object v2, v2, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    iget-object v0, p0, Lcom/google/android/location/copresence/ai;->a:Lcom/google/android/location/copresence/ah;

    iget-object v0, v0, Lcom/google/android/location/copresence/ah;->f:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0, v3}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    iget-object v0, p0, Lcom/google/android/location/copresence/ai;->a:Lcom/google/android/location/copresence/ah;

    invoke-virtual {v0}, Lcom/google/android/location/copresence/ah;->c()V

    return-void
.end method
