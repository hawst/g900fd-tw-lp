.class final Lcom/google/android/location/fused/ae;
.super Lcom/google/android/location/fused/bg;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/location/fused/ad;


# direct methods
.method public constructor <init>(Lcom/google/android/location/fused/ad;Lcom/google/android/location/fused/a/s;Lcom/google/android/location/fused/a/r;)V
    .locals 2

    .prologue
    .line 448
    iput-object p1, p0, Lcom/google/android/location/fused/ae;->a:Lcom/google/android/location/fused/ad;

    .line 449
    new-instance v0, Lcom/google/android/location/fused/c;

    invoke-direct {v0}, Lcom/google/android/location/fused/c;-><init>()V

    invoke-static {p1}, Lcom/google/android/location/fused/ad;->a(Lcom/google/android/location/fused/ad;)Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {p0, p2, p3, v0, v1}, Lcom/google/android/location/fused/bg;-><init>(Lcom/google/android/location/fused/a/s;Lcom/google/android/location/fused/a/r;Lcom/google/android/location/fused/c;Landroid/os/Looper;)V

    .line 454
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 490
    iget-object v0, p0, Lcom/google/android/location/fused/ae;->a:Lcom/google/android/location/fused/ad;

    invoke-static {v0}, Lcom/google/android/location/fused/ad;->b(Lcom/google/android/location/fused/ad;)Lcom/google/android/location/fused/a/m;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/location/fused/a/m;->f()V

    .line 491
    iget-object v0, p0, Lcom/google/android/location/fused/ae;->a:Lcom/google/android/location/fused/ad;

    invoke-static {v0}, Lcom/google/android/location/fused/ad;->c(Lcom/google/android/location/fused/ad;)Lcom/google/android/location/fused/a/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/location/fused/a/a;->f()V

    .line 492
    iget-object v0, p0, Lcom/google/android/location/fused/ae;->a:Lcom/google/android/location/fused/ad;

    invoke-static {v0}, Lcom/google/android/location/fused/ad;->d(Lcom/google/android/location/fused/ad;)Lcom/google/android/location/fused/a/u;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/location/fused/a/u;->f()V

    .line 493
    return-void
.end method

.method public final a(Z)V
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 458
    iget-object v2, p0, Lcom/google/android/location/fused/bg;->b:Lcom/google/android/location/fused/bl;

    invoke-virtual {v2}, Lcom/google/android/location/fused/bl;->e()J

    move-result-wide v2

    const-wide v4, 0x7fffffffffffffffL

    cmp-long v2, v2, v4

    if-gez v2, :cond_5

    .line 459
    invoke-virtual {p0}, Lcom/google/android/location/fused/ae;->d()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 460
    iget-object v2, p0, Lcom/google/android/location/fused/ae;->a:Lcom/google/android/location/fused/ad;

    invoke-static {v2}, Lcom/google/android/location/fused/ad;->b(Lcom/google/android/location/fused/ad;)Lcom/google/android/location/fused/a/m;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/location/fused/bg;->b:Lcom/google/android/location/fused/bl;

    invoke-virtual {v3}, Lcom/google/android/location/fused/bl;->e()J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Lcom/google/android/location/fused/a/m;->a(J)V

    .line 461
    iget-object v2, p0, Lcom/google/android/location/fused/ae;->a:Lcom/google/android/location/fused/ad;

    invoke-static {v2}, Lcom/google/android/location/fused/ad;->b(Lcom/google/android/location/fused/ad;)Lcom/google/android/location/fused/a/m;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/location/fused/bg;->b:Lcom/google/android/location/fused/bl;

    invoke-virtual {v3}, Lcom/google/android/location/fused/bl;->f()Ljava/util/Collection;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/android/location/fused/a/m;->a(Ljava/util/Collection;)V

    .line 462
    iget-object v2, p0, Lcom/google/android/location/fused/ae;->a:Lcom/google/android/location/fused/ad;

    invoke-static {v2}, Lcom/google/android/location/fused/ad;->b(Lcom/google/android/location/fused/ad;)Lcom/google/android/location/fused/a/m;

    move-result-object v2

    iput-boolean p1, v2, Lcom/google/android/location/fused/a/m;->g:Z

    .line 463
    iget-object v2, p0, Lcom/google/android/location/fused/ae;->a:Lcom/google/android/location/fused/ad;

    invoke-static {v2}, Lcom/google/android/location/fused/ad;->b(Lcom/google/android/location/fused/ad;)Lcom/google/android/location/fused/a/m;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/location/fused/a/m;->e()V

    .line 468
    :goto_0
    iget v2, p0, Lcom/google/android/location/fused/bg;->c:I

    and-int/lit8 v2, v2, 0x10

    if-eqz v2, :cond_1

    move v2, v0

    :goto_1
    if-eqz v2, :cond_2

    .line 469
    iget-object v2, p0, Lcom/google/android/location/fused/ae;->a:Lcom/google/android/location/fused/ad;

    invoke-static {v2}, Lcom/google/android/location/fused/ad;->c(Lcom/google/android/location/fused/ad;)Lcom/google/android/location/fused/a/a;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/location/fused/bg;->b:Lcom/google/android/location/fused/bl;

    invoke-virtual {v3}, Lcom/google/android/location/fused/bl;->e()J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Lcom/google/android/location/fused/a/a;->a(J)V

    .line 470
    iget-object v2, p0, Lcom/google/android/location/fused/ae;->a:Lcom/google/android/location/fused/ad;

    invoke-static {v2}, Lcom/google/android/location/fused/ad;->c(Lcom/google/android/location/fused/ad;)Lcom/google/android/location/fused/a/a;

    move-result-object v2

    iput-boolean p1, v2, Lcom/google/android/location/fused/a/m;->g:Z

    .line 471
    iget-object v2, p0, Lcom/google/android/location/fused/ae;->a:Lcom/google/android/location/fused/ad;

    invoke-static {v2}, Lcom/google/android/location/fused/ad;->c(Lcom/google/android/location/fused/ad;)Lcom/google/android/location/fused/a/a;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/location/fused/bg;->b:Lcom/google/android/location/fused/bl;

    invoke-virtual {v3}, Lcom/google/android/location/fused/bl;->f()Ljava/util/Collection;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/android/location/fused/a/a;->a(Ljava/util/Collection;)V

    .line 472
    iget-object v2, p0, Lcom/google/android/location/fused/ae;->a:Lcom/google/android/location/fused/ad;

    invoke-static {v2}, Lcom/google/android/location/fused/ad;->c(Lcom/google/android/location/fused/ad;)Lcom/google/android/location/fused/a/a;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/location/fused/a/a;->e()V

    .line 477
    :goto_2
    iget v2, p0, Lcom/google/android/location/fused/bg;->c:I

    and-int/lit8 v2, v2, 0x8

    if-eqz v2, :cond_3

    :goto_3
    if-eqz v0, :cond_4

    .line 478
    iget-object v0, p0, Lcom/google/android/location/fused/ae;->a:Lcom/google/android/location/fused/ad;

    invoke-static {v0}, Lcom/google/android/location/fused/ad;->d(Lcom/google/android/location/fused/ad;)Lcom/google/android/location/fused/a/u;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/location/fused/bg;->b:Lcom/google/android/location/fused/bl;

    invoke-virtual {v1}, Lcom/google/android/location/fused/bl;->f()Ljava/util/Collection;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/location/fused/a/u;->a(Ljava/util/Collection;)V

    .line 479
    iget-object v0, p0, Lcom/google/android/location/fused/ae;->a:Lcom/google/android/location/fused/ad;

    invoke-static {v0}, Lcom/google/android/location/fused/ad;->d(Lcom/google/android/location/fused/ad;)Lcom/google/android/location/fused/a/u;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/location/fused/a/u;->e()V

    .line 486
    :goto_4
    return-void

    .line 465
    :cond_0
    iget-object v2, p0, Lcom/google/android/location/fused/ae;->a:Lcom/google/android/location/fused/ad;

    invoke-static {v2}, Lcom/google/android/location/fused/ad;->b(Lcom/google/android/location/fused/ad;)Lcom/google/android/location/fused/a/m;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/location/fused/a/m;->f()V

    goto :goto_0

    :cond_1
    move v2, v1

    .line 468
    goto :goto_1

    .line 474
    :cond_2
    iget-object v2, p0, Lcom/google/android/location/fused/ae;->a:Lcom/google/android/location/fused/ad;

    invoke-static {v2}, Lcom/google/android/location/fused/ad;->c(Lcom/google/android/location/fused/ad;)Lcom/google/android/location/fused/a/a;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/location/fused/a/a;->f()V

    goto :goto_2

    :cond_3
    move v0, v1

    .line 477
    goto :goto_3

    .line 481
    :cond_4
    iget-object v0, p0, Lcom/google/android/location/fused/ae;->a:Lcom/google/android/location/fused/ad;

    invoke-static {v0}, Lcom/google/android/location/fused/ad;->d(Lcom/google/android/location/fused/ad;)Lcom/google/android/location/fused/a/u;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/location/fused/a/u;->f()V

    goto :goto_4

    .line 484
    :cond_5
    invoke-virtual {p0}, Lcom/google/android/location/fused/ae;->a()V

    goto :goto_4
.end method
