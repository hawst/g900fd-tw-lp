.class public final Lcom/google/android/location/fused/a;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/hardware/SensorEventListener;


# instance fields
.field a:Z

.field private final b:Landroid/hardware/SensorManager;

.field private final c:Landroid/hardware/Sensor;

.field private final d:Landroid/content/Context;

.field private final e:Landroid/content/BroadcastReceiver;

.field private final f:Landroid/view/Display;

.field private final g:Landroid/os/PowerManager;

.field private final h:Landroid/os/Handler;

.field private i:D

.field private j:D

.field private k:D

.field private l:Z

.field private m:F

.field private n:F

.field private o:F

.field private final p:[F

.field private final q:[F

.field private r:D

.field private s:C


# direct methods
.method constructor <init>(Landroid/content/Context;Landroid/hardware/SensorManager;Landroid/os/Handler;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const-wide v0, 0x7fefffffffffffffL    # Double.MAX_VALUE

    .line 82
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 46
    iput-wide v0, p0, Lcom/google/android/location/fused/a;->i:D

    .line 48
    iput-wide v0, p0, Lcom/google/android/location/fused/a;->j:D

    .line 51
    iput-wide v0, p0, Lcom/google/android/location/fused/a;->k:D

    .line 57
    iput-boolean v2, p0, Lcom/google/android/location/fused/a;->a:Z

    .line 59
    const/16 v0, 0x9

    new-array v0, v0, [F

    iput-object v0, p0, Lcom/google/android/location/fused/a;->p:[F

    .line 61
    const/4 v0, 0x3

    new-array v0, v0, [F

    iput-object v0, p0, Lcom/google/android/location/fused/a;->q:[F

    .line 64
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/location/fused/a;->r:D

    .line 65
    iput-char v2, p0, Lcom/google/android/location/fused/a;->s:C

    .line 83
    iput-object p1, p0, Lcom/google/android/location/fused/a;->d:Landroid/content/Context;

    .line 84
    iput-object p3, p0, Lcom/google/android/location/fused/a;->h:Landroid/os/Handler;

    .line 85
    iput-object p2, p0, Lcom/google/android/location/fused/a;->b:Landroid/hardware/SensorManager;

    .line 86
    const-string v0, "power"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    iput-object v0, p0, Lcom/google/android/location/fused/a;->g:Landroid/os/PowerManager;

    .line 87
    iget-object v0, p0, Lcom/google/android/location/fused/a;->b:Landroid/hardware/SensorManager;

    const/16 v1, 0xb

    invoke-virtual {v0, v1}, Landroid/hardware/SensorManager;->getDefaultSensor(I)Landroid/hardware/Sensor;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/fused/a;->c:Landroid/hardware/Sensor;

    .line 91
    iget-object v0, p0, Lcom/google/android/location/fused/a;->d:Landroid/content/Context;

    const-string v1, "window"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    .line 93
    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/fused/a;->f:Landroid/view/Display;

    .line 95
    new-instance v0, Lcom/google/android/location/fused/b;

    invoke-direct {v0, p0}, Lcom/google/android/location/fused/b;-><init>(Lcom/google/android/location/fused/a;)V

    iput-object v0, p0, Lcom/google/android/location/fused/a;->e:Landroid/content/BroadcastReceiver;

    .line 106
    return-void
.end method

.method static synthetic a(Lcom/google/android/location/fused/a;)D
    .locals 2

    .prologue
    .line 27
    const-wide v0, 0x7fefffffffffffffL    # Double.MAX_VALUE

    iput-wide v0, p0, Lcom/google/android/location/fused/a;->i:D

    return-wide v0
.end method

.method static synthetic a(Lcom/google/android/location/fused/a;Z)Z
    .locals 0

    .prologue
    .line 27
    iput-boolean p1, p0, Lcom/google/android/location/fused/a;->l:Z

    return p1
.end method


# virtual methods
.method public final a()V
    .locals 4

    .prologue
    .line 112
    iget-object v0, p0, Lcom/google/android/location/fused/a;->c:Landroid/hardware/Sensor;

    if-eqz v0, :cond_0

    .line 113
    iget-object v0, p0, Lcom/google/android/location/fused/a;->g:Landroid/os/PowerManager;

    invoke-virtual {v0}, Landroid/os/PowerManager;->isScreenOn()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/location/fused/a;->l:Z

    .line 114
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 115
    const-string v1, "android.intent.action.SCREEN_OFF"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 116
    const-string v1, "android.intent.action.SCREEN_ON"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 117
    iget-object v1, p0, Lcom/google/android/location/fused/a;->d:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/location/fused/a;->e:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 119
    iget-object v0, p0, Lcom/google/android/location/fused/a;->b:Landroid/hardware/SensorManager;

    iget-object v1, p0, Lcom/google/android/location/fused/a;->c:Landroid/hardware/Sensor;

    const/4 v2, 0x3

    iget-object v3, p0, Lcom/google/android/location/fused/a;->h:Landroid/os/Handler;

    invoke-virtual {v0, p0, v1, v2, v3}, Landroid/hardware/SensorManager;->registerListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;ILandroid/os/Handler;)Z

    .line 125
    :cond_0
    return-void
.end method

.method public final b()V
    .locals 2

    .prologue
    const-wide v0, 0x7fefffffffffffffL    # Double.MAX_VALUE

    .line 131
    iput-wide v0, p0, Lcom/google/android/location/fused/a;->i:D

    .line 132
    iput-wide v0, p0, Lcom/google/android/location/fused/a;->j:D

    .line 133
    iput-wide v0, p0, Lcom/google/android/location/fused/a;->k:D

    .line 134
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/location/fused/a;->a:Z

    .line 135
    iget-object v0, p0, Lcom/google/android/location/fused/a;->c:Landroid/hardware/Sensor;

    if-eqz v0, :cond_0

    .line 136
    iget-object v0, p0, Lcom/google/android/location/fused/a;->d:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/location/fused/a;->e:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 138
    iget-object v0, p0, Lcom/google/android/location/fused/a;->b:Landroid/hardware/SensorManager;

    invoke-virtual {v0, p0}, Landroid/hardware/SensorManager;->unregisterListener(Landroid/hardware/SensorEventListener;)V

    .line 140
    :cond_0
    return-void
.end method

.method public final c()D
    .locals 2

    .prologue
    .line 151
    iget-wide v0, p0, Lcom/google/android/location/fused/a;->i:D

    return-wide v0
.end method

.method public final d()D
    .locals 2

    .prologue
    .line 162
    iget-wide v0, p0, Lcom/google/android/location/fused/a;->j:D

    return-wide v0
.end method

.method public final e()D
    .locals 2

    .prologue
    .line 171
    iget-wide v0, p0, Lcom/google/android/location/fused/a;->k:D

    return-wide v0
.end method

.method public final onAccuracyChanged(Landroid/hardware/Sensor;I)V
    .locals 0

    .prologue
    .line 312
    return-void
.end method

.method public final onSensorChanged(Landroid/hardware/SensorEvent;)V
    .locals 12

    .prologue
    .line 200
    iget-object v0, p1, Landroid/hardware/SensorEvent;->sensor:Landroid/hardware/Sensor;

    invoke-virtual {v0}, Landroid/hardware/Sensor;->getType()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 303
    :cond_0
    :goto_0
    return-void

    .line 202
    :pswitch_0
    iget-object v0, p1, Landroid/hardware/SensorEvent;->values:[F

    array-length v0, v0

    const/4 v1, 0x3

    if-lt v0, v1, :cond_0

    .line 207
    iget-object v0, p0, Lcom/google/android/location/fused/a;->q:[F

    const/4 v1, 0x0

    iget-object v2, p1, Landroid/hardware/SensorEvent;->values:[F

    const/4 v3, 0x0

    aget v2, v2, v3

    aput v2, v0, v1

    .line 208
    iget-object v0, p0, Lcom/google/android/location/fused/a;->q:[F

    const/4 v1, 0x1

    iget-object v2, p1, Landroid/hardware/SensorEvent;->values:[F

    const/4 v3, 0x1

    aget v2, v2, v3

    aput v2, v0, v1

    .line 209
    iget-object v0, p0, Lcom/google/android/location/fused/a;->q:[F

    const/4 v1, 0x2

    iget-object v2, p1, Landroid/hardware/SensorEvent;->values:[F

    const/4 v3, 0x2

    aget v2, v2, v3

    aput v2, v0, v1

    .line 210
    iget-object v0, p0, Lcom/google/android/location/fused/a;->b:Landroid/hardware/SensorManager;

    iget-object v0, p0, Lcom/google/android/location/fused/a;->p:[F

    iget-object v1, p0, Lcom/google/android/location/fused/a;->q:[F

    invoke-static {v0, v1}, Landroid/hardware/SensorManager;->getRotationMatrixFromVector([F[F)V

    .line 212
    iget-object v0, p0, Lcom/google/android/location/fused/a;->p:[F

    const/4 v1, 0x6

    aget v4, v0, v1

    .line 213
    iget-object v0, p0, Lcom/google/android/location/fused/a;->p:[F

    const/4 v1, 0x7

    aget v5, v0, v1

    .line 214
    iget-object v0, p0, Lcom/google/android/location/fused/a;->p:[F

    const/16 v1, 0x8

    aget v0, v0, v1

    .line 215
    neg-float v1, v4

    iput v1, p0, Lcom/google/android/location/fused/a;->m:F

    .line 216
    neg-float v1, v5

    iput v1, p0, Lcom/google/android/location/fused/a;->n:F

    .line 217
    neg-float v1, v0

    iput v1, p0, Lcom/google/android/location/fused/a;->o:F

    .line 218
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/location/fused/a;->a:Z

    .line 223
    float-to-double v2, v0

    mul-float v1, v4, v4

    mul-float v6, v5, v5

    add-float/2addr v1, v6

    mul-float/2addr v0, v0

    add-float/2addr v0, v1

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v0

    div-double v0, v2, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->acos(D)D

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/location/fused/a;->k:D

    .line 228
    iget-wide v0, p0, Lcom/google/android/location/fused/a;->k:D

    const-wide v2, 0x3ff657184ae74487L    # 1.3962634015954636

    cmpl-double v0, v0, v2

    if-lez v0, :cond_4

    iget-wide v0, p0, Lcom/google/android/location/fused/a;->k:D

    const-wide v2, 0x3ffbecde5da115a9L    # 1.7453292519943295

    cmpg-double v0, v0, v2

    if-gez v0, :cond_4

    iget-wide v0, p0, Lcom/google/android/location/fused/a;->j:D

    .line 239
    :goto_1
    iget-wide v2, p0, Lcom/google/android/location/fused/a;->k:D

    const-wide v6, 0x3fc657184ae74487L    # 0.17453292519943295

    cmpg-double v2, v2, v6

    if-ltz v2, :cond_1

    iget-wide v2, p0, Lcom/google/android/location/fused/a;->k:D

    const-wide v6, 0x4007bc89cf95b8d0L    # 2.9670597283903604

    cmpl-double v2, v2, v6

    if-lez v2, :cond_5

    :cond_1
    iget-wide v2, p0, Lcom/google/android/location/fused/a;->j:D

    .line 246
    :goto_2
    iget-wide v6, p0, Lcom/google/android/location/fused/a;->k:D

    const-wide v8, 0x3fe921fb54442d18L    # 0.7853981633974483

    cmpg-double v6, v6, v8

    if-ltz v6, :cond_2

    iget-wide v6, p0, Lcom/google/android/location/fused/a;->k:D

    const-wide v8, 0x4002d97c7f3321d2L    # 2.356194490192345

    cmpl-double v6, v6, v8

    if-lez v6, :cond_8

    .line 247
    :cond_2
    iget-char v6, p0, Lcom/google/android/location/fused/a;->s:C

    const/4 v7, 0x1

    if-ne v6, v7, :cond_3

    .line 248
    const-wide v6, 0x7fefffffffffffffL    # Double.MAX_VALUE

    cmpl-double v6, v2, v6

    if-eqz v6, :cond_6

    sub-double/2addr v2, v0

    :goto_3
    iput-wide v2, p0, Lcom/google/android/location/fused/a;->r:D

    .line 251
    :cond_3
    iget-wide v2, p0, Lcom/google/android/location/fused/a;->k:D

    const-wide v6, 0x3fc657184ae74487L    # 0.17453292519943295

    cmpl-double v2, v2, v6

    if-lez v2, :cond_7

    .line 263
    float-to-double v2, v4

    float-to-double v4, v5

    invoke-static {v2, v3, v4, v5}, Ljava/lang/Math;->atan2(DD)D

    move-result-wide v2

    .line 287
    :goto_4
    const/4 v4, 0x2

    iput-char v4, p0, Lcom/google/android/location/fused/a;->s:C

    .line 299
    :goto_5
    iget-boolean v4, p0, Lcom/google/android/location/fused/a;->l:Z

    if-nez v4, :cond_b

    const-wide v2, 0x7fefffffffffffffL    # Double.MAX_VALUE

    :goto_6
    iput-wide v2, p0, Lcom/google/android/location/fused/a;->i:D

    .line 302
    iget-wide v2, p0, Lcom/google/android/location/fused/a;->r:D

    add-double/2addr v0, v2

    iput-wide v0, p0, Lcom/google/android/location/fused/a;->j:D

    goto/16 :goto_0

    .line 228
    :cond_4
    iget-object v0, p0, Lcom/google/android/location/fused/a;->p:[F

    const/4 v1, 0x1

    aget v0, v0, v1

    float-to-double v0, v0

    iget-object v2, p0, Lcom/google/android/location/fused/a;->p:[F

    const/4 v3, 0x4

    aget v2, v2, v3

    float-to-double v2, v2

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->atan2(DD)D

    move-result-wide v0

    goto :goto_1

    .line 239
    :cond_5
    iget-object v2, p0, Lcom/google/android/location/fused/a;->p:[F

    const/4 v3, 0x2

    aget v2, v2, v3

    neg-float v2, v2

    float-to-double v2, v2

    iget-object v6, p0, Lcom/google/android/location/fused/a;->p:[F

    const/4 v7, 0x5

    aget v6, v6, v7

    neg-float v6, v6

    float-to-double v6, v6

    invoke-static {v2, v3, v6, v7}, Ljava/lang/Math;->atan2(DD)D

    move-result-wide v2

    goto :goto_2

    .line 248
    :cond_6
    const-wide/16 v2, 0x0

    goto :goto_3

    .line 269
    :cond_7
    iget-object v2, p0, Lcom/google/android/location/fused/a;->f:Landroid/view/Display;

    invoke-virtual {v2}, Landroid/view/Display;->getRotation()I

    move-result v2

    packed-switch v2, :pswitch_data_1

    .line 283
    const-wide v2, 0x7fefffffffffffffL    # Double.MAX_VALUE

    goto :goto_4

    .line 271
    :pswitch_1
    const-wide/16 v2, 0x0

    .line 272
    goto :goto_4

    .line 274
    :pswitch_2
    const-wide v2, 0x3ff921fb54442d18L    # 1.5707963267948966

    .line 275
    goto :goto_4

    .line 277
    :pswitch_3
    const-wide v2, 0x400921fb54442d18L    # Math.PI

    .line 278
    goto :goto_4

    .line 280
    :pswitch_4
    const-wide v2, -0x4006de04abbbd2e8L    # -1.5707963267948966

    .line 281
    goto :goto_4

    .line 290
    :cond_8
    iget-char v4, p0, Lcom/google/android/location/fused/a;->s:C

    const/4 v5, 0x2

    if-ne v4, v5, :cond_9

    .line 291
    const-wide v4, 0x7fefffffffffffffL    # Double.MAX_VALUE

    cmpl-double v4, v0, v4

    if-eqz v4, :cond_a

    sub-double/2addr v0, v2

    :goto_7
    iput-wide v0, p0, Lcom/google/android/location/fused/a;->r:D

    .line 294
    :cond_9
    const-wide/16 v0, 0x0

    .line 296
    const/4 v4, 0x1

    iput-char v4, p0, Lcom/google/android/location/fused/a;->s:C

    move-wide v10, v0

    move-wide v0, v2

    move-wide v2, v10

    goto :goto_5

    .line 291
    :cond_a
    const-wide/16 v0, 0x0

    goto :goto_7

    .line 299
    :cond_b
    add-double/2addr v2, v0

    goto :goto_6

    .line 200
    nop

    :pswitch_data_0
    .packed-switch 0xb
        :pswitch_0
    .end packed-switch

    .line 269
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method
