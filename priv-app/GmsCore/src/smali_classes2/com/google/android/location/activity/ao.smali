.class public final Lcom/google/android/location/activity/ao;
.super Lcom/google/android/location/activity/av;
.source "SourceFile"


# instance fields
.field a:Z

.field private b:Z

.field private c:I

.field private final g:Lcom/google/android/location/activity/o;

.field private final h:Lcom/google/android/location/activity/a;


# direct methods
.method public constructor <init>(Lcom/google/android/location/activity/k;Lcom/google/android/location/os/bi;)V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 41
    invoke-direct {p0, p1, p2}, Lcom/google/android/location/activity/av;-><init>(Lcom/google/android/location/activity/k;Lcom/google/android/location/os/bi;)V

    .line 29
    iput-boolean v0, p0, Lcom/google/android/location/activity/ao;->b:Z

    .line 35
    iput-boolean v0, p0, Lcom/google/android/location/activity/ao;->a:Z

    .line 36
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/location/activity/ao;->c:I

    .line 42
    invoke-virtual {p1}, Lcom/google/android/location/activity/k;->h()Lcom/google/android/location/activity/a;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/activity/ao;->h:Lcom/google/android/location/activity/a;

    .line 43
    invoke-interface {p2}, Lcom/google/android/location/os/bi;->y()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 44
    new-instance v0, Lcom/google/android/location/activity/bp;

    invoke-interface {p2}, Lcom/google/android/location/os/bi;->c()Lcom/google/android/location/j/b;

    move-result-object v1

    invoke-direct {v0, p1, v1}, Lcom/google/android/location/activity/bp;-><init>(Lcom/google/android/location/activity/k;Lcom/google/android/location/j/b;)V

    iput-object v0, p0, Lcom/google/android/location/activity/ao;->g:Lcom/google/android/location/activity/o;

    .line 48
    :goto_0
    return-void

    .line 46
    :cond_0
    new-instance v0, Lcom/google/android/location/activity/ag;

    invoke-interface {p2}, Lcom/google/android/location/os/bi;->c()Lcom/google/android/location/j/b;

    move-result-object v1

    invoke-direct {v0, p1, v1}, Lcom/google/android/location/activity/ag;-><init>(Lcom/google/android/location/activity/k;Lcom/google/android/location/j/b;)V

    iput-object v0, p0, Lcom/google/android/location/activity/ao;->g:Lcom/google/android/location/activity/o;

    goto :goto_0
.end method

.method private l()V
    .locals 12

    .prologue
    .line 76
    iget-object v0, p0, Lcom/google/android/location/activity/ao;->g:Lcom/google/android/location/activity/o;

    invoke-virtual {v0}, Lcom/google/android/location/activity/o;->d()Lcom/google/android/location/activity/p;

    move-result-object v3

    .line 77
    sget-boolean v0, Lcom/google/android/location/i/a;->b:Z

    if-eqz v0, :cond_0

    const-string v0, "ScheduledState"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "setNextTriggerTime, scheduleInfo.detectorType="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, v3, Lcom/google/android/location/activity/p;->b:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/location/o/a/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 79
    :cond_0
    iget v0, v3, Lcom/google/android/location/activity/p;->b:I

    iput v0, p0, Lcom/google/android/location/activity/ao;->c:I

    .line 81
    iget-boolean v0, p0, Lcom/google/android/location/activity/ao;->a:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/location/activity/ao;->h:Lcom/google/android/location/activity/a;

    iget-boolean v0, v0, Lcom/google/android/location/activity/a;->a:Z

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/google/android/location/activity/ao;->e:Lcom/google/android/location/os/bi;

    invoke-interface {v0}, Lcom/google/android/location/os/bi;->y()Z

    move-result v0

    if-eqz v0, :cond_5

    :cond_1
    iget v0, v3, Lcom/google/android/location/activity/p;->b:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_3

    const-wide/16 v0, 0x1c2

    :goto_0
    const-wide/16 v4, 0x5dc

    add-long/2addr v4, v0

    const-wide/16 v0, 0x0

    iget-object v2, v3, Lcom/google/android/location/activity/p;->a:Lcom/google/android/location/e/b;

    iget-wide v6, v2, Lcom/google/android/location/e/b;->a:J

    sub-long/2addr v6, v4

    invoke-static {v0, v1, v6, v7}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v6

    iget-object v0, p0, Lcom/google/android/location/activity/ao;->e:Lcom/google/android/location/os/bi;

    invoke-interface {v0}, Lcom/google/android/location/os/bi;->c()Lcom/google/android/location/j/b;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/location/j/b;->d()J

    move-result-wide v0

    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-static {v2}, Ljava/util/Calendar;->getInstance(Ljava/util/Locale;)Ljava/util/Calendar;

    move-result-object v2

    add-long v8, v6, v0

    invoke-virtual {v2, v8, v9}, Ljava/util/Calendar;->setTimeInMillis(J)V

    const/16 v8, 0xe

    const/4 v9, 0x0

    invoke-virtual {v2, v8, v9}, Ljava/util/Calendar;->set(II)V

    const/16 v8, 0xd

    const/4 v9, 0x0

    invoke-virtual {v2, v8, v9}, Ljava/util/Calendar;->set(II)V

    invoke-virtual {v2}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v8

    sub-long v0, v8, v0

    iget-object v2, p0, Lcom/google/android/location/activity/ao;->e:Lcom/google/android/location/os/bi;

    invoke-interface {v2}, Lcom/google/android/location/os/bi;->c()Lcom/google/android/location/j/b;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/location/j/b;->c()J

    move-result-wide v8

    sub-long v8, v0, v8

    const-wide/16 v10, 0x2710

    cmp-long v2, v8, v10

    if-lez v2, :cond_4

    const/4 v2, 0x1

    :goto_1
    if-eqz v2, :cond_5

    sget-boolean v2, Lcom/google/android/location/i/a;->b:Z

    if-eqz v2, :cond_2

    const-string v2, "ScheduledState"

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "Prefer to disable sensor batching: minDurationMillis="

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " timeToReenableBatching="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " timeToReenableBatchingRoundedDown="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Lcom/google/android/location/o/a/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 82
    :cond_2
    :goto_2
    const-wide/16 v4, -0x1

    cmp-long v2, v0, v4

    if-eqz v2, :cond_6

    .line 86
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/google/android/location/activity/ao;->a:Z

    .line 87
    iget-object v2, p0, Lcom/google/android/location/activity/ao;->d:Lcom/google/android/location/activity/k;

    new-instance v3, Lcom/google/android/location/e/b;

    const-wide/16 v4, 0x3e8

    sub-long/2addr v0, v4

    const-wide/16 v4, 0x7d0

    invoke-direct {v3, v0, v1, v4, v5}, Lcom/google/android/location/e/b;-><init>(JJ)V

    invoke-virtual {v2, v3}, Lcom/google/android/location/activity/k;->a(Lcom/google/android/location/e/b;)V

    .line 90
    iget-object v0, p0, Lcom/google/android/location/activity/ao;->h:Lcom/google/android/location/activity/a;

    invoke-virtual {v0}, Lcom/google/android/location/activity/a;->a()V

    .line 101
    :goto_3
    return-void

    .line 81
    :cond_3
    const-wide/32 v0, 0x8ca0

    goto/16 :goto_0

    :cond_4
    const/4 v2, 0x0

    goto :goto_1

    :cond_5
    const-wide/16 v0, -0x1

    goto :goto_2

    .line 92
    :cond_6
    iget-boolean v0, p0, Lcom/google/android/location/activity/ao;->a:Z

    if-eqz v0, :cond_8

    .line 95
    sget-boolean v0, Lcom/google/android/location/i/a;->b:Z

    if-eqz v0, :cond_7

    const-string v0, "ScheduledState"

    const-string v1, "Re-enabling sensor batching early because schedule changed."

    invoke-static {v0, v1}, Lcom/google/android/location/o/a/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 96
    :cond_7
    iget-object v0, p0, Lcom/google/android/location/activity/ao;->h:Lcom/google/android/location/activity/a;

    iget-object v1, p0, Lcom/google/android/location/activity/ao;->d:Lcom/google/android/location/activity/k;

    invoke-virtual {v1}, Lcom/google/android/location/activity/k;->t()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/location/activity/a;->a(I)Z

    .line 98
    :cond_8
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/location/activity/ao;->a:Z

    .line 99
    iget-object v0, p0, Lcom/google/android/location/activity/ao;->d:Lcom/google/android/location/activity/k;

    iget-object v1, v3, Lcom/google/android/location/activity/p;->a:Lcom/google/android/location/e/b;

    invoke-virtual {v0, v1}, Lcom/google/android/location/activity/k;->a(Lcom/google/android/location/e/b;)V

    goto :goto_3
.end method

.method private m()V
    .locals 2

    .prologue
    .line 249
    iget v0, p0, Lcom/google/android/location/activity/ao;->c:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 250
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/location/activity/ao;->c:I

    .line 252
    :cond_0
    iget v0, p0, Lcom/google/android/location/activity/ao;->c:I

    invoke-virtual {p0, v0}, Lcom/google/android/location/activity/ao;->a(I)V

    .line 253
    return-void
.end method


# virtual methods
.method protected final a(Lcom/google/android/location/activity/bd;Lcom/google/android/location/activity/bd;)V
    .locals 7

    .prologue
    const/4 v1, 0x4

    const/4 v6, 0x3

    const/4 v2, 0x0

    const/4 v3, 0x1

    .line 237
    if-eqz p1, :cond_3

    iget-object v0, p1, Lcom/google/android/location/activity/bd;->a:Lcom/google/android/location/e/ay;

    sget-object v4, Lcom/google/android/location/e/ay;->c:Lcom/google/android/location/e/ay;

    if-eq v0, v4, :cond_3

    iget-object v0, p2, Lcom/google/android/location/activity/bd;->a:Lcom/google/android/location/e/ay;

    sget-object v4, Lcom/google/android/location/e/ay;->c:Lcom/google/android/location/e/ay;

    if-eq v0, v4, :cond_3

    iget-object v0, p1, Lcom/google/android/location/activity/bd;->a:Lcom/google/android/location/e/ay;

    iget-object v4, p2, Lcom/google/android/location/activity/bd;->a:Lcom/google/android/location/e/ay;

    if-eq v0, v4, :cond_3

    move v0, v3

    :goto_0
    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/location/activity/ao;->d:Lcom/google/android/location/activity/k;

    invoke-virtual {v0}, Lcom/google/android/location/activity/k;->i()Lcom/google/android/gms/location/ActivityRecognitionResult;

    move-result-object v0

    if-eqz p2, :cond_0

    iget-object v4, p2, Lcom/google/android/location/activity/bd;->a:Lcom/google/android/location/e/ay;

    sget-object v5, Lcom/google/android/location/e/ay;->c:Lcom/google/android/location/e/ay;

    if-ne v4, v5, :cond_4

    :cond_0
    :goto_1
    if-eqz v2, :cond_2

    .line 240
    sget-boolean v0, Lcom/google/android/location/i/a;->b:Z

    if-eqz v0, :cond_1

    const-string v0, "ScheduledState"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Travel mode change and inconsistent with last activity detection. Running activity detection early: last="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " cur="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/location/o/a/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 244
    :cond_1
    invoke-virtual {p0, v3}, Lcom/google/android/location/activity/ao;->a(I)V

    .line 246
    :cond_2
    return-void

    :cond_3
    move v0, v2

    .line 237
    goto :goto_0

    :cond_4
    if-nez v0, :cond_6

    move v0, v1

    :goto_2
    iget-object v4, p2, Lcom/google/android/location/activity/bd;->a:Lcom/google/android/location/e/ay;

    if-eq v0, v1, :cond_5

    const/4 v1, 0x5

    if-eq v0, v1, :cond_5

    if-ne v0, v6, :cond_7

    :cond_5
    move v1, v3

    :goto_3
    sget-object v5, Lcom/google/android/location/e/ay;->b:Lcom/google/android/location/e/ay;

    if-ne v4, v5, :cond_8

    if-eqz v1, :cond_8

    move v2, v3

    goto :goto_1

    :cond_6
    invoke-virtual {v0}, Lcom/google/android/gms/location/ActivityRecognitionResult;->a()Lcom/google/android/gms/location/DetectedActivity;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/location/DetectedActivity;->a()I

    move-result v0

    goto :goto_2

    :cond_7
    move v1, v2

    goto :goto_3

    :cond_8
    sget-object v1, Lcom/google/android/location/e/ay;->a:Lcom/google/android/location/e/ay;

    if-ne v4, v1, :cond_9

    move v1, v3

    :goto_4
    if-ne v0, v6, :cond_a

    move v0, v3

    :goto_5
    if-eq v1, v0, :cond_0

    move v2, v3

    goto :goto_1

    :cond_9
    move v1, v2

    goto :goto_4

    :cond_a
    move v0, v2

    goto :goto_5
.end method

.method protected final a(Lcom/google/android/location/j/k;)V
    .locals 1

    .prologue
    .line 155
    sget-object v0, Lcom/google/android/location/j/k;->a:Lcom/google/android/location/j/k;

    if-eq p1, v0, :cond_0

    .line 160
    :goto_0
    return-void

    .line 159
    :cond_0
    invoke-direct {p0}, Lcom/google/android/location/activity/ao;->l()V

    goto :goto_0
.end method

.method protected final a(Z)V
    .locals 5

    .prologue
    .line 257
    sget-boolean v0, Lcom/google/android/location/i/a;->b:Z

    if-eqz v0, :cond_0

    const-string v0, "ScheduledState"

    const-string v1, "newClientAdded(%s)"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/location/o/a/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 258
    :cond_0
    invoke-direct {p0}, Lcom/google/android/location/activity/ao;->l()V

    .line 259
    if-eqz p1, :cond_1

    .line 261
    invoke-direct {p0}, Lcom/google/android/location/activity/ao;->m()V

    .line 263
    :cond_1
    return-void
.end method

.method protected final aq_()V
    .locals 4

    .prologue
    .line 173
    iget-object v0, p0, Lcom/google/android/location/activity/ao;->d:Lcom/google/android/location/activity/k;

    invoke-virtual {v0}, Lcom/google/android/location/activity/k;->D()J

    move-result-wide v0

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-nez v0, :cond_2

    .line 174
    iget-object v0, p0, Lcom/google/android/location/activity/ao;->d:Lcom/google/android/location/activity/k;

    iget-object v1, p0, Lcom/google/android/location/activity/ao;->e:Lcom/google/android/location/os/bi;

    invoke-interface {v1}, Lcom/google/android/location/os/bi;->c()Lcom/google/android/location/j/b;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/location/j/b;->c()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/google/android/location/activity/k;->d(J)V

    .line 177
    iget-object v0, p0, Lcom/google/android/location/activity/ao;->d:Lcom/google/android/location/activity/k;

    invoke-virtual {v0}, Lcom/google/android/location/activity/k;->C()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/location/activity/ao;->e:Lcom/google/android/location/os/bi;

    invoke-interface {v0}, Lcom/google/android/location/os/bi;->y()Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/location/activity/ao;->d:Lcom/google/android/location/activity/k;

    invoke-virtual {v0}, Lcom/google/android/location/activity/k;->c()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 179
    new-instance v0, Lcom/google/android/location/activity/y;

    iget-object v1, p0, Lcom/google/android/location/activity/ao;->d:Lcom/google/android/location/activity/k;

    iget-object v2, p0, Lcom/google/android/location/activity/ao;->e:Lcom/google/android/location/os/bi;

    invoke-direct {v0, v1, v2}, Lcom/google/android/location/activity/y;-><init>(Lcom/google/android/location/activity/k;Lcom/google/android/location/os/bi;)V

    .line 180
    invoke-virtual {p0, v0}, Lcom/google/android/location/activity/ao;->a(Lcom/google/android/location/activity/s;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 181
    sget-boolean v1, Lcom/google/android/location/i/a;->b:Z

    if-eqz v1, :cond_0

    const-string v1, "ScheduledState"

    const-string v2, "Screen state changed. Run activity detection now."

    invoke-static {v1, v2}, Lcom/google/android/location/o/a/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 182
    :cond_0
    invoke-virtual {p0, v0}, Lcom/google/android/location/activity/ao;->a(Lcom/google/android/location/activity/av;)V

    .line 189
    :cond_1
    :goto_0
    return-void

    .line 187
    :cond_2
    invoke-direct {p0}, Lcom/google/android/location/activity/ao;->l()V

    .line 188
    sget-boolean v0, Lcom/google/android/location/i/a;->b:Z

    if-eqz v0, :cond_1

    const-string v0, "ScheduledState"

    const-string v1, "Screen state changed. Schedule activity detection soon."

    invoke-static {v0, v1}, Lcom/google/android/location/o/a/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final at_()V
    .locals 2

    .prologue
    .line 166
    sget-boolean v0, Lcom/google/android/location/i/a;->b:Z

    if-eqz v0, :cond_0

    const-string v0, "ScheduledState"

    const-string v1, "networkProviderQuit: rescheduling alarms."

    invoke-static {v0, v1}, Lcom/google/android/location/o/a/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 167
    :cond_0
    iget-object v0, p0, Lcom/google/android/location/activity/ao;->d:Lcom/google/android/location/activity/k;

    invoke-virtual {v0}, Lcom/google/android/location/activity/k;->v()V

    .line 168
    invoke-direct {p0}, Lcom/google/android/location/activity/ao;->l()V

    .line 169
    return-void
.end method

.method public final au_()V
    .locals 0

    .prologue
    .line 280
    invoke-direct {p0}, Lcom/google/android/location/activity/ao;->l()V

    .line 281
    return-void
.end method

.method protected final c()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 142
    iget-boolean v0, p0, Lcom/google/android/location/activity/ao;->a:Z

    if-eqz v0, :cond_1

    .line 143
    sget-boolean v0, Lcom/google/android/location/i/a;->b:Z

    if-eqz v0, :cond_0

    const-string v0, "ScheduledState"

    const-string v1, "alarmRing: re-enabling sensor batching."

    invoke-static {v0, v1}, Lcom/google/android/location/o/a/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 144
    :cond_0
    iget-object v0, p0, Lcom/google/android/location/activity/ao;->h:Lcom/google/android/location/activity/a;

    iget-object v1, p0, Lcom/google/android/location/activity/ao;->d:Lcom/google/android/location/activity/k;

    invoke-virtual {v1}, Lcom/google/android/location/activity/k;->t()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/location/activity/a;->a(I)Z

    .line 145
    iput-boolean v4, p0, Lcom/google/android/location/activity/ao;->a:Z

    .line 146
    invoke-direct {p0}, Lcom/google/android/location/activity/ao;->l()V

    .line 151
    :goto_0
    return-void

    .line 148
    :cond_1
    sget-boolean v0, Lcom/google/android/location/i/a;->b:Z

    if-eqz v0, :cond_2

    const-string v0, "ScheduledState"

    const-string v1, "moveToDetectingState(%s)"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    iget v3, p0, Lcom/google/android/location/activity/ao;->c:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/location/o/a/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 149
    :cond_2
    iget v0, p0, Lcom/google/android/location/activity/ao;->c:I

    invoke-virtual {p0, v0}, Lcom/google/android/location/activity/ao;->a(I)V

    goto :goto_0
.end method

.method protected final g()V
    .locals 6

    .prologue
    const-wide/16 v4, -0x1

    .line 52
    iget-object v0, p0, Lcom/google/android/location/activity/ao;->h:Lcom/google/android/location/activity/a;

    iget-boolean v0, v0, Lcom/google/android/location/activity/a;->a:Z

    iput-boolean v0, p0, Lcom/google/android/location/activity/ao;->b:Z

    .line 53
    iget-object v0, p0, Lcom/google/android/location/activity/ao;->d:Lcom/google/android/location/activity/k;

    invoke-virtual {v0}, Lcom/google/android/location/activity/k;->x()J

    move-result-wide v0

    cmp-long v0, v0, v4

    if-nez v0, :cond_0

    .line 54
    iget-object v0, p0, Lcom/google/android/location/activity/ao;->d:Lcom/google/android/location/activity/k;

    iget-object v1, p0, Lcom/google/android/location/activity/ao;->e:Lcom/google/android/location/os/bi;

    invoke-interface {v1}, Lcom/google/android/location/os/bi;->c()Lcom/google/android/location/j/b;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/location/j/b;->c()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/google/android/location/activity/k;->a(J)V

    .line 56
    :cond_0
    iget-object v0, p0, Lcom/google/android/location/activity/ao;->d:Lcom/google/android/location/activity/k;

    invoke-virtual {v0}, Lcom/google/android/location/activity/k;->y()J

    move-result-wide v0

    cmp-long v0, v0, v4

    if-nez v0, :cond_1

    .line 57
    iget-object v0, p0, Lcom/google/android/location/activity/ao;->d:Lcom/google/android/location/activity/k;

    iget-object v1, p0, Lcom/google/android/location/activity/ao;->e:Lcom/google/android/location/os/bi;

    invoke-interface {v1}, Lcom/google/android/location/os/bi;->c()Lcom/google/android/location/j/b;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/location/j/b;->c()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/google/android/location/activity/k;->b(J)V

    .line 59
    :cond_1
    iget-object v0, p0, Lcom/google/android/location/activity/ao;->d:Lcom/google/android/location/activity/k;

    invoke-virtual {v0}, Lcom/google/android/location/activity/k;->z()J

    move-result-wide v0

    cmp-long v0, v0, v4

    if-nez v0, :cond_2

    .line 60
    iget-object v0, p0, Lcom/google/android/location/activity/ao;->d:Lcom/google/android/location/activity/k;

    iget-object v1, p0, Lcom/google/android/location/activity/ao;->e:Lcom/google/android/location/os/bi;

    invoke-interface {v1}, Lcom/google/android/location/os/bi;->c()Lcom/google/android/location/j/b;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/location/j/b;->c()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/google/android/location/activity/k;->c(J)V

    .line 62
    :cond_2
    invoke-direct {p0}, Lcom/google/android/location/activity/ao;->l()V

    .line 63
    return-void
.end method

.method protected final h()V
    .locals 2

    .prologue
    .line 67
    invoke-super {p0}, Lcom/google/android/location/activity/av;->h()V

    .line 68
    iget-object v0, p0, Lcom/google/android/location/activity/ao;->h:Lcom/google/android/location/activity/a;

    iget-boolean v0, v0, Lcom/google/android/location/activity/a;->a:Z

    iget-boolean v1, p0, Lcom/google/android/location/activity/ao;->b:Z

    if-eq v0, v1, :cond_1

    .line 69
    sget-boolean v0, Lcom/google/android/location/i/a;->b:Z

    if-eqz v0, :cond_0

    const-string v0, "ScheduledState"

    const-string v1, "Re-enabling sensor batching because the enable state is different. Probably caused by early detection because a new client is added or tile is detected."

    invoke-static {v0, v1}, Lcom/google/android/location/o/a/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 71
    :cond_0
    iget-object v0, p0, Lcom/google/android/location/activity/ao;->h:Lcom/google/android/location/activity/a;

    iget-object v1, p0, Lcom/google/android/location/activity/ao;->d:Lcom/google/android/location/activity/k;

    invoke-virtual {v1}, Lcom/google/android/location/activity/k;->t()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/location/activity/a;->a(I)Z

    .line 73
    :cond_1
    return-void
.end method

.method protected final i()V
    .locals 2

    .prologue
    .line 267
    sget-boolean v0, Lcom/google/android/location/i/a;->b:Z

    if-eqz v0, :cond_0

    const-string v0, "ScheduledState"

    const-string v1, "clientRemoved."

    invoke-static {v0, v1}, Lcom/google/android/location/o/a/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 268
    :cond_0
    invoke-direct {p0}, Lcom/google/android/location/activity/ao;->l()V

    .line 269
    return-void
.end method

.method public final j()V
    .locals 1

    .prologue
    .line 273
    iget-object v0, p0, Lcom/google/android/location/activity/ao;->d:Lcom/google/android/location/activity/k;

    invoke-virtual {v0}, Lcom/google/android/location/activity/k;->E()V

    .line 274
    invoke-direct {p0}, Lcom/google/android/location/activity/ao;->m()V

    .line 276
    return-void
.end method

.method public final p()Ljava/lang/String;
    .locals 1

    .prologue
    .line 285
    const-string v0, "ScheduledState"

    return-object v0
.end method
