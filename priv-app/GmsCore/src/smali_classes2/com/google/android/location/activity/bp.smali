.class public final Lcom/google/android/location/activity/bp;
.super Lcom/google/android/location/activity/o;
.source "SourceFile"


# direct methods
.method public constructor <init>(Lcom/google/android/location/activity/k;Lcom/google/android/location/j/b;)V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0, p1, p2}, Lcom/google/android/location/activity/o;-><init>(Lcom/google/android/location/activity/k;Lcom/google/android/location/j/b;)V

    .line 28
    return-void
.end method

.method static a(JJJJZ)J
    .locals 8

    .prologue
    .line 94
    sget-boolean v0, Lcom/google/android/location/i/a;->b:Z

    if-eqz v0, :cond_0

    const-string v0, "ActivityScheduler"

    const-string v1, "toNearestMinuteSinceBoot: desiredAlarmSinceBoot = %s,lastTriggerTimeSinceBoot = %s, period = %d, bootTime = %d"

    const/4 v2, 0x4

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p0, p1, p6, p7}, Lcom/google/android/location/activity/bp;->b(JJ)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    invoke-static {p2, p3, p6, p7}, Lcom/google/android/location/activity/bp;->b(JJ)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x2

    invoke-static {p4, p5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x3

    invoke-static {p6, p7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/location/o/a/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 99
    :cond_0
    const-wide v0, 0x7fffffffffffffffL

    cmp-long v0, p0, v0

    if-nez v0, :cond_2

    .line 100
    const-wide v0, 0x7fffffffffffffffL

    .line 126
    :cond_1
    :goto_0
    return-wide v0

    .line 102
    :cond_2
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v2

    .line 103
    add-long v0, p0, p6

    invoke-virtual {v2, v0, v1}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 104
    const/16 v0, 0xe

    const/4 v1, 0x0

    invoke-virtual {v2, v0, v1}, Ljava/util/Calendar;->set(II)V

    .line 105
    const/16 v0, 0xd

    invoke-virtual {v2, v0}, Ljava/util/Calendar;->get(I)I

    move-result v0

    .line 107
    const/16 v1, 0x1e

    if-ge v0, v1, :cond_3

    if-nez p8, :cond_3

    .line 109
    const/16 v0, 0xd

    const/4 v1, 0x0

    invoke-virtual {v2, v0, v1}, Ljava/util/Calendar;->set(II)V

    .line 110
    invoke-virtual {v2}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v0

    sub-long/2addr v0, p6

    .line 115
    sub-long v4, v0, p2

    .line 116
    const-wide/16 v6, 0x2

    div-long v6, p4, v6

    cmp-long v3, v4, v6

    if-lez v3, :cond_3

    .line 117
    sget-boolean v3, Lcom/google/android/location/i/a;->b:Z

    if-eqz v3, :cond_1

    const-string v3, "ActivityScheduler"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "  Prefer beginning of minute: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v2, p6, p7}, Lcom/google/android/location/activity/bp;->a(Ljava/util/Calendar;J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v2}, Lcom/google/android/location/o/a/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 121
    :cond_3
    const/16 v0, 0xd

    const/4 v1, 0x0

    invoke-virtual {v2, v0, v1}, Ljava/util/Calendar;->set(II)V

    .line 122
    const/16 v0, 0xc

    const/4 v1, 0x1

    invoke-virtual {v2, v0, v1}, Ljava/util/Calendar;->add(II)V

    .line 123
    invoke-virtual {v2}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v0

    sub-long/2addr v0, p6

    .line 124
    sget-boolean v3, Lcom/google/android/location/i/a;->b:Z

    if-eqz v3, :cond_1

    const-string v3, "ActivityScheduler"

    const-string v4, "Prefer beginning of next minute: %s, outputTime = %s"

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    invoke-static {v2, p6, p7}, Lcom/google/android/location/activity/bp;->a(Ljava/util/Calendar;J)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v5, v6

    const/4 v2, 0x1

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    aput-object v6, v5, v2

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v2}, Lcom/google/android/location/o/a/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private static a(Ljava/util/Calendar;J)Ljava/lang/String;
    .locals 7

    .prologue
    .line 37
    invoke-virtual {p0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v0

    sub-long/2addr v0, p1

    .line 38
    new-instance v2, Ljava/text/SimpleDateFormat;

    const-string v3, "MM-dd-yyyy HH:mm:ss.SSS"

    invoke-direct {v2, v3}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 39
    invoke-virtual {p0}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v2

    .line 40
    const-string v3, "sinceBootTime=%d, %s"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    aput-object v0, v4, v5

    const/4 v0, 0x1

    aput-object v2, v4, v0

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static b(JJ)Ljava/lang/String;
    .locals 4

    .prologue
    .line 31
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 32
    add-long v2, p0, p2

    invoke-virtual {v0, v2, v3}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 33
    invoke-static {v0, p2, p3}, Lcom/google/android/location/activity/bp;->a(Ljava/util/Calendar;J)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final d()Lcom/google/android/location/activity/p;
    .locals 13

    .prologue
    const/4 v12, 0x2

    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 51
    iget-object v0, p0, Lcom/google/android/location/activity/bp;->b:Lcom/google/android/location/j/b;

    invoke-interface {v0}, Lcom/google/android/location/j/b;->d()J

    move-result-wide v6

    .line 52
    invoke-virtual {p0}, Lcom/google/android/location/activity/bp;->a()J

    move-result-wide v0

    iget-object v2, p0, Lcom/google/android/location/activity/bp;->a:Lcom/google/android/location/activity/k;

    invoke-virtual {v2}, Lcom/google/android/location/activity/k;->x()J

    move-result-wide v2

    iget-object v4, p0, Lcom/google/android/location/activity/bp;->a:Lcom/google/android/location/activity/k;

    invoke-virtual {v4}, Lcom/google/android/location/activity/k;->q()J

    move-result-wide v4

    invoke-static/range {v0 .. v8}, Lcom/google/android/location/activity/bp;->a(JJJJZ)J

    move-result-wide v10

    .line 54
    invoke-virtual {p0}, Lcom/google/android/location/activity/bp;->c()J

    move-result-wide v0

    iget-object v2, p0, Lcom/google/android/location/activity/bp;->a:Lcom/google/android/location/activity/k;

    invoke-virtual {v2}, Lcom/google/android/location/activity/k;->y()J

    move-result-wide v2

    iget-object v4, p0, Lcom/google/android/location/activity/bp;->a:Lcom/google/android/location/activity/k;

    invoke-virtual {v4}, Lcom/google/android/location/activity/k;->r()J

    move-result-wide v4

    invoke-static/range {v0 .. v8}, Lcom/google/android/location/activity/bp;->a(JJJJZ)J

    move-result-wide v2

    .line 57
    sget-boolean v0, Lcom/google/android/location/i/a;->b:Z

    if-eqz v0, :cond_0

    const-string v0, "ActivityScheduler"

    const-string v1, "WatchAlarmSynchronizer.getAlarmScheduleInfo: nextFullTrigger=%d, nextTiltOnlyTrigger=%d"

    new-array v4, v12, [Ljava/lang/Object;

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v4, v8

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v4, v9

    invoke-static {v1, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/location/o/a/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 69
    :cond_0
    iget-object v0, p0, Lcom/google/android/location/activity/bp;->a:Lcom/google/android/location/activity/k;

    invoke-virtual {v0}, Lcom/google/android/location/activity/k;->c()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 70
    const/4 v0, 0x3

    .line 73
    :goto_0
    invoke-virtual {p0, v2, v3, v10, v11}, Lcom/google/android/location/activity/bp;->a(JJ)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 75
    sget-boolean v0, Lcom/google/android/location/i/a;->b:Z

    if-eqz v0, :cond_1

    const-string v0, "ActivityScheduler"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v4, "Running tilt only detector next: FullTrigger: "

    invoke-direct {v1, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, " TiltOnlyTrigger: "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/location/o/a/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    move-wide v0, v2

    move v2, v12

    .line 80
    :goto_1
    new-instance v3, Lcom/google/android/location/activity/p;

    new-instance v4, Lcom/google/android/location/e/b;

    const-wide/16 v6, 0x3e8

    sub-long/2addr v0, v6

    const-wide/16 v6, 0x7d0

    invoke-direct {v4, v0, v1, v6, v7}, Lcom/google/android/location/e/b;-><init>(JJ)V

    invoke-direct {v3, p0, v4, v2}, Lcom/google/android/location/activity/p;-><init>(Lcom/google/android/location/activity/o;Lcom/google/android/location/e/b;I)V

    return-object v3

    :cond_2
    move v2, v0

    move-wide v0, v10

    goto :goto_1

    :cond_3
    move v0, v9

    goto :goto_0
.end method
