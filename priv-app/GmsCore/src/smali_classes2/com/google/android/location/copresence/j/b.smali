.class public final Lcom/google/android/location/copresence/j/b;
.super Lcom/google/protobuf/nano/d;
.source "SourceFile"


# static fields
.field private static volatile e:[Lcom/google/android/location/copresence/j/b;


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/lang/Boolean;

.field public c:[Lcom/google/ac/b/c/aw;

.field public d:[Lcom/google/ac/b/c/am;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 37
    invoke-direct {p0}, Lcom/google/protobuf/nano/d;-><init>()V

    .line 38
    iput-object v1, p0, Lcom/google/android/location/copresence/j/b;->a:Ljava/lang/String;

    iput-object v1, p0, Lcom/google/android/location/copresence/j/b;->b:Ljava/lang/Boolean;

    invoke-static {}, Lcom/google/ac/b/c/aw;->a()[Lcom/google/ac/b/c/aw;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/copresence/j/b;->c:[Lcom/google/ac/b/c/aw;

    invoke-static {}, Lcom/google/ac/b/c/am;->a()[Lcom/google/ac/b/c/am;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/copresence/j/b;->d:[Lcom/google/ac/b/c/am;

    iput-object v1, p0, Lcom/google/android/location/copresence/j/b;->unknownFieldData:Lcom/google/protobuf/nano/f;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/location/copresence/j/b;->cachedSize:I

    .line 39
    return-void
.end method

.method public static a()[Lcom/google/android/location/copresence/j/b;
    .locals 2

    .prologue
    .line 14
    sget-object v0, Lcom/google/android/location/copresence/j/b;->e:[Lcom/google/android/location/copresence/j/b;

    if-nez v0, :cond_1

    .line 15
    sget-object v1, Lcom/google/protobuf/nano/h;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 17
    :try_start_0
    sget-object v0, Lcom/google/android/location/copresence/j/b;->e:[Lcom/google/android/location/copresence/j/b;

    if-nez v0, :cond_0

    .line 18
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/android/location/copresence/j/b;

    sput-object v0, Lcom/google/android/location/copresence/j/b;->e:[Lcom/google/android/location/copresence/j/b;

    .line 20
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 22
    :cond_1
    sget-object v0, Lcom/google/android/location/copresence/j/b;->e:[Lcom/google/android/location/copresence/j/b;

    return-object v0

    .line 20
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 130
    invoke-super {p0}, Lcom/google/protobuf/nano/d;->computeSerializedSize()I

    move-result v0

    .line 131
    iget-object v2, p0, Lcom/google/android/location/copresence/j/b;->a:Ljava/lang/String;

    if-eqz v2, :cond_0

    .line 132
    const/4 v2, 0x1

    iget-object v3, p0, Lcom/google/android/location/copresence/j/b;->a:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 135
    :cond_0
    iget-object v2, p0, Lcom/google/android/location/copresence/j/b;->b:Ljava/lang/Boolean;

    if-eqz v2, :cond_1

    .line 136
    const/4 v2, 0x2

    iget-object v3, p0, Lcom/google/android/location/copresence/j/b;->b:Ljava/lang/Boolean;

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v2}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 139
    :cond_1
    iget-object v2, p0, Lcom/google/android/location/copresence/j/b;->c:[Lcom/google/ac/b/c/aw;

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/google/android/location/copresence/j/b;->c:[Lcom/google/ac/b/c/aw;

    array-length v2, v2

    if-lez v2, :cond_4

    move v2, v0

    move v0, v1

    .line 140
    :goto_0
    iget-object v3, p0, Lcom/google/android/location/copresence/j/b;->c:[Lcom/google/ac/b/c/aw;

    array-length v3, v3

    if-ge v0, v3, :cond_3

    .line 141
    iget-object v3, p0, Lcom/google/android/location/copresence/j/b;->c:[Lcom/google/ac/b/c/aw;

    aget-object v3, v3, v0

    .line 142
    if-eqz v3, :cond_2

    .line 143
    const/4 v4, 0x6

    invoke-static {v4, v3}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v3

    add-int/2addr v2, v3

    .line 140
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_3
    move v0, v2

    .line 148
    :cond_4
    iget-object v2, p0, Lcom/google/android/location/copresence/j/b;->d:[Lcom/google/ac/b/c/am;

    if-eqz v2, :cond_6

    iget-object v2, p0, Lcom/google/android/location/copresence/j/b;->d:[Lcom/google/ac/b/c/am;

    array-length v2, v2

    if-lez v2, :cond_6

    .line 149
    :goto_1
    iget-object v2, p0, Lcom/google/android/location/copresence/j/b;->d:[Lcom/google/ac/b/c/am;

    array-length v2, v2

    if-ge v1, v2, :cond_6

    .line 150
    iget-object v2, p0, Lcom/google/android/location/copresence/j/b;->d:[Lcom/google/ac/b/c/am;

    aget-object v2, v2, v1

    .line 151
    if-eqz v2, :cond_5

    .line 152
    const/4 v3, 0x7

    invoke-static {v3, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v2

    add-int/2addr v0, v2

    .line 149
    :cond_5
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 157
    :cond_6
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 53
    if-ne p1, p0, :cond_1

    .line 54
    const/4 v0, 0x1

    .line 82
    :cond_0
    :goto_0
    return v0

    .line 56
    :cond_1
    instance-of v1, p1, Lcom/google/android/location/copresence/j/b;

    if-eqz v1, :cond_0

    .line 59
    check-cast p1, Lcom/google/android/location/copresence/j/b;

    .line 60
    iget-object v1, p0, Lcom/google/android/location/copresence/j/b;->a:Ljava/lang/String;

    if-nez v1, :cond_4

    .line 61
    iget-object v1, p1, Lcom/google/android/location/copresence/j/b;->a:Ljava/lang/String;

    if-nez v1, :cond_0

    .line 67
    :cond_2
    iget-object v1, p0, Lcom/google/android/location/copresence/j/b;->b:Ljava/lang/Boolean;

    if-nez v1, :cond_5

    .line 68
    iget-object v1, p1, Lcom/google/android/location/copresence/j/b;->b:Ljava/lang/Boolean;

    if-nez v1, :cond_0

    .line 74
    :cond_3
    iget-object v1, p0, Lcom/google/android/location/copresence/j/b;->c:[Lcom/google/ac/b/c/aw;

    iget-object v2, p1, Lcom/google/android/location/copresence/j/b;->c:[Lcom/google/ac/b/c/aw;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 78
    iget-object v1, p0, Lcom/google/android/location/copresence/j/b;->d:[Lcom/google/ac/b/c/am;

    iget-object v2, p1, Lcom/google/android/location/copresence/j/b;->d:[Lcom/google/ac/b/c/am;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 82
    invoke-virtual {p0, p1}, Lcom/google/android/location/copresence/j/b;->unknownFieldDataEquals(Lcom/google/protobuf/nano/d;)Z

    move-result v0

    goto :goto_0

    .line 64
    :cond_4
    iget-object v1, p0, Lcom/google/android/location/copresence/j/b;->a:Ljava/lang/String;

    iget-object v2, p1, Lcom/google/android/location/copresence/j/b;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    goto :goto_0

    .line 71
    :cond_5
    iget-object v1, p0, Lcom/google/android/location/copresence/j/b;->b:Ljava/lang/Boolean;

    iget-object v2, p1, Lcom/google/android/location/copresence/j/b;->b:Ljava/lang/Boolean;

    invoke-virtual {v1, v2}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 87
    iget-object v0, p0, Lcom/google/android/location/copresence/j/b;->a:Ljava/lang/String;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    .line 90
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/android/location/copresence/j/b;->b:Ljava/lang/Boolean;

    if-nez v2, :cond_1

    :goto_1
    add-int/2addr v0, v1

    .line 92
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/google/android/location/copresence/j/b;->c:[Lcom/google/ac/b/c/aw;

    invoke-static {v1}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 94
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/google/android/location/copresence/j/b;->d:[Lcom/google/ac/b/c/am;

    invoke-static {v1}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 96
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/google/android/location/copresence/j/b;->unknownFieldDataHashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 97
    return v0

    .line 87
    :cond_0
    iget-object v0, p0, Lcom/google/android/location/copresence/j/b;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_0

    .line 90
    :cond_1
    iget-object v1, p0, Lcom/google/android/location/copresence/j/b;->b:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_1
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 8
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/android/location/copresence/j/b;->storeUnknownField(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/copresence/j/b;->a:Ljava/lang/String;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/copresence/j/b;->b:Ljava/lang/Boolean;

    goto :goto_0

    :sswitch_3
    const/16 v0, 0x32

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->b(Lcom/google/protobuf/nano/a;I)I

    move-result v2

    iget-object v0, p0, Lcom/google/android/location/copresence/j/b;->c:[Lcom/google/ac/b/c/aw;

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lcom/google/ac/b/c/aw;

    if-eqz v0, :cond_1

    iget-object v3, p0, Lcom/google/android/location/copresence/j/b;->c:[Lcom/google/ac/b/c/aw;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_1
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_3

    new-instance v3, Lcom/google/ac/b/c/aw;

    invoke-direct {v3}, Lcom/google/ac/b/c/aw;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    iget-object v0, p0, Lcom/google/android/location/copresence/j/b;->c:[Lcom/google/ac/b/c/aw;

    array-length v0, v0

    goto :goto_1

    :cond_3
    new-instance v3, Lcom/google/ac/b/c/aw;

    invoke-direct {v3}, Lcom/google/ac/b/c/aw;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    iput-object v2, p0, Lcom/google/android/location/copresence/j/b;->c:[Lcom/google/ac/b/c/aw;

    goto :goto_0

    :sswitch_4
    const/16 v0, 0x3a

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->b(Lcom/google/protobuf/nano/a;I)I

    move-result v2

    iget-object v0, p0, Lcom/google/android/location/copresence/j/b;->d:[Lcom/google/ac/b/c/am;

    if-nez v0, :cond_5

    move v0, v1

    :goto_3
    add-int/2addr v2, v0

    new-array v2, v2, [Lcom/google/ac/b/c/am;

    if-eqz v0, :cond_4

    iget-object v3, p0, Lcom/google/android/location/copresence/j/b;->d:[Lcom/google/ac/b/c/am;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_4
    :goto_4
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_6

    new-instance v3, Lcom/google/ac/b/c/am;

    invoke-direct {v3}, Lcom/google/ac/b/c/am;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_5
    iget-object v0, p0, Lcom/google/android/location/copresence/j/b;->d:[Lcom/google/ac/b/c/am;

    array-length v0, v0

    goto :goto_3

    :cond_6
    new-instance v3, Lcom/google/ac/b/c/am;

    invoke-direct {v3}, Lcom/google/ac/b/c/am;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    iput-object v2, p0, Lcom/google/android/location/copresence/j/b;->d:[Lcom/google/ac/b/c/am;

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x32 -> :sswitch_3
        0x3a -> :sswitch_4
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 103
    iget-object v0, p0, Lcom/google/android/location/copresence/j/b;->a:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 104
    const/4 v0, 0x1

    iget-object v2, p0, Lcom/google/android/location/copresence/j/b;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v2}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 106
    :cond_0
    iget-object v0, p0, Lcom/google/android/location/copresence/j/b;->b:Ljava/lang/Boolean;

    if-eqz v0, :cond_1

    .line 107
    const/4 v0, 0x2

    iget-object v2, p0, Lcom/google/android/location/copresence/j/b;->b:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-virtual {p1, v0, v2}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 109
    :cond_1
    iget-object v0, p0, Lcom/google/android/location/copresence/j/b;->c:[Lcom/google/ac/b/c/aw;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/location/copresence/j/b;->c:[Lcom/google/ac/b/c/aw;

    array-length v0, v0

    if-lez v0, :cond_3

    move v0, v1

    .line 110
    :goto_0
    iget-object v2, p0, Lcom/google/android/location/copresence/j/b;->c:[Lcom/google/ac/b/c/aw;

    array-length v2, v2

    if-ge v0, v2, :cond_3

    .line 111
    iget-object v2, p0, Lcom/google/android/location/copresence/j/b;->c:[Lcom/google/ac/b/c/aw;

    aget-object v2, v2, v0

    .line 112
    if-eqz v2, :cond_2

    .line 113
    const/4 v3, 0x6

    invoke-virtual {p1, v3, v2}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 110
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 117
    :cond_3
    iget-object v0, p0, Lcom/google/android/location/copresence/j/b;->d:[Lcom/google/ac/b/c/am;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/google/android/location/copresence/j/b;->d:[Lcom/google/ac/b/c/am;

    array-length v0, v0

    if-lez v0, :cond_5

    .line 118
    :goto_1
    iget-object v0, p0, Lcom/google/android/location/copresence/j/b;->d:[Lcom/google/ac/b/c/am;

    array-length v0, v0

    if-ge v1, v0, :cond_5

    .line 119
    iget-object v0, p0, Lcom/google/android/location/copresence/j/b;->d:[Lcom/google/ac/b/c/am;

    aget-object v0, v0, v1

    .line 120
    if-eqz v0, :cond_4

    .line 121
    const/4 v2, 0x7

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 118
    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 125
    :cond_5
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/d;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 126
    return-void
.end method
