.class final Lcom/google/android/location/copresence/d/a/i;
.super Landroid/bluetooth/le/ScanCallback;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/location/copresence/d/a/h;


# direct methods
.method constructor <init>(Lcom/google/android/location/copresence/d/a/h;)V
    .locals 0

    .prologue
    .line 39
    iput-object p1, p0, Lcom/google/android/location/copresence/d/a/i;->a:Lcom/google/android/location/copresence/d/a/h;

    invoke-direct {p0}, Landroid/bluetooth/le/ScanCallback;-><init>()V

    return-void
.end method


# virtual methods
.method public final onScanFailed(I)V
    .locals 2

    .prologue
    .line 95
    const/4 v0, 0x6

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 96
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "BleScanImplL: Scanning failed with error="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->e(Ljava/lang/String;)I

    .line 98
    :cond_0
    return-void
.end method

.method public final onScanResult(ILandroid/bluetooth/le/ScanResult;)V
    .locals 2

    .prologue
    .line 43
    iget-object v0, p0, Lcom/google/android/location/copresence/d/a/i;->a:Lcom/google/android/location/copresence/d/a/h;

    iget-object v0, v0, Lcom/google/android/location/copresence/d/a/h;->a:Lcom/google/android/location/copresence/d/a/o;

    if-eqz v0, :cond_0

    .line 44
    new-instance v0, Lcom/google/android/location/copresence/d/a/j;

    invoke-direct {v0, p0, p2}, Lcom/google/android/location/copresence/d/a/j;-><init>(Lcom/google/android/location/copresence/d/a/i;Landroid/bluetooth/le/ScanResult;)V

    .line 89
    iget-object v1, p0, Lcom/google/android/location/copresence/d/a/i;->a:Lcom/google/android/location/copresence/d/a/h;

    iget-object v1, v1, Lcom/google/android/location/copresence/d/a/h;->a:Lcom/google/android/location/copresence/d/a/o;

    invoke-interface {v1, v0}, Lcom/google/android/location/copresence/d/a/o;->a(Lcom/google/android/location/copresence/d/a/k;)V

    .line 91
    :cond_0
    return-void
.end method
