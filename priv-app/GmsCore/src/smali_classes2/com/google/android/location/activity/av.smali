.class abstract Lcom/google/android/location/activity/av;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field static final synthetic f:Z


# instance fields
.field protected final d:Lcom/google/android/location/activity/k;

.field protected final e:Lcom/google/android/location/os/bi;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 14
    const-class v0, Lcom/google/android/location/activity/av;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/google/android/location/activity/av;->f:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lcom/google/android/location/activity/k;Lcom/google/android/location/os/bi;)V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    iput-object p1, p0, Lcom/google/android/location/activity/av;->d:Lcom/google/android/location/activity/k;

    .line 21
    iput-object p2, p0, Lcom/google/android/location/activity/av;->e:Lcom/google/android/location/os/bi;

    .line 22
    return-void
.end method

.method private a(Lcom/google/android/location/activity/r;Lcom/google/android/location/activity/s;)V
    .locals 2

    .prologue
    .line 144
    sget-boolean v0, Lcom/google/android/location/activity/av;->f:Z

    if-nez v0, :cond_0

    invoke-virtual {p2}, Lcom/google/android/location/activity/s;->d()Lcom/google/android/location/activity/a/o;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/android/location/activity/r;->d()Lcom/google/android/location/activity/a/o;

    move-result-object v1

    if-eq v0, v1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 146
    :cond_0
    invoke-virtual {p0, p2}, Lcom/google/android/location/activity/av;->a(Lcom/google/android/location/activity/s;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 147
    invoke-virtual {p0, p2}, Lcom/google/android/location/activity/av;->a(Lcom/google/android/location/activity/av;)V

    .line 151
    :goto_0
    return-void

    .line 149
    :cond_1
    invoke-virtual {p0, p1}, Lcom/google/android/location/activity/av;->a(Lcom/google/android/location/activity/av;)V

    goto :goto_0
.end method


# virtual methods
.method protected a()V
    .locals 3

    .prologue
    .line 43
    sget-boolean v0, Lcom/google/android/location/i/a;->b:Z

    if-eqz v0, :cond_0

    const-string v0, "ActivityScheduler"

    const-string v1, "activityDetectionDisabled."

    invoke-static {v0, v1}, Lcom/google/android/location/o/a/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 44
    :cond_0
    new-instance v0, Lcom/google/android/location/activity/an;

    iget-object v1, p0, Lcom/google/android/location/activity/av;->d:Lcom/google/android/location/activity/k;

    iget-object v2, p0, Lcom/google/android/location/activity/av;->e:Lcom/google/android/location/os/bi;

    invoke-direct {v0, v1, v2}, Lcom/google/android/location/activity/an;-><init>(Lcom/google/android/location/activity/k;Lcom/google/android/location/os/bi;)V

    invoke-virtual {p0, v0}, Lcom/google/android/location/activity/av;->a(Lcom/google/android/location/activity/av;)V

    .line 45
    return-void
.end method

.method protected final a(I)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 188
    sget-boolean v0, Lcom/google/android/location/i/a;->b:Z

    if-eqz v0, :cond_0

    const-string v0, "ActivityScheduler"

    const-string v1, "movetoDetectingState(%s)"

    new-array v2, v5, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/location/o/a/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 189
    :cond_0
    const/4 v0, 0x2

    if-ne p1, v0, :cond_2

    .line 190
    new-instance v0, Lcom/google/android/location/activity/aw;

    iget-object v1, p0, Lcom/google/android/location/activity/av;->d:Lcom/google/android/location/activity/k;

    iget-object v2, p0, Lcom/google/android/location/activity/av;->e:Lcom/google/android/location/os/bi;

    invoke-direct {v0, v1, v2}, Lcom/google/android/location/activity/aw;-><init>(Lcom/google/android/location/activity/k;Lcom/google/android/location/os/bi;)V

    new-instance v1, Lcom/google/android/location/activity/ax;

    iget-object v2, p0, Lcom/google/android/location/activity/av;->d:Lcom/google/android/location/activity/k;

    iget-object v3, p0, Lcom/google/android/location/activity/av;->e:Lcom/google/android/location/os/bi;

    invoke-direct {v1, v2, v3}, Lcom/google/android/location/activity/ax;-><init>(Lcom/google/android/location/activity/k;Lcom/google/android/location/os/bi;)V

    invoke-direct {p0, v0, v1}, Lcom/google/android/location/activity/av;->a(Lcom/google/android/location/activity/r;Lcom/google/android/location/activity/s;)V

    .line 205
    :cond_1
    :goto_0
    return-void

    .line 192
    :cond_2
    const/4 v0, 0x3

    if-ne p1, v0, :cond_3

    .line 193
    new-instance v0, Lcom/google/android/location/activity/v;

    iget-object v1, p0, Lcom/google/android/location/activity/av;->d:Lcom/google/android/location/activity/k;

    iget-object v2, p0, Lcom/google/android/location/activity/av;->e:Lcom/google/android/location/os/bi;

    invoke-direct {v0, v1, v2}, Lcom/google/android/location/activity/v;-><init>(Lcom/google/android/location/activity/k;Lcom/google/android/location/os/bi;)V

    new-instance v1, Lcom/google/android/location/activity/w;

    iget-object v2, p0, Lcom/google/android/location/activity/av;->d:Lcom/google/android/location/activity/k;

    iget-object v3, p0, Lcom/google/android/location/activity/av;->e:Lcom/google/android/location/os/bi;

    invoke-direct {v1, v2, v3}, Lcom/google/android/location/activity/w;-><init>(Lcom/google/android/location/activity/k;Lcom/google/android/location/os/bi;)V

    invoke-direct {p0, v0, v1}, Lcom/google/android/location/activity/av;->a(Lcom/google/android/location/activity/r;Lcom/google/android/location/activity/s;)V

    goto :goto_0

    .line 195
    :cond_3
    invoke-static {}, Lcom/google/android/location/activity/k;->b()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 196
    iget-object v0, p0, Lcom/google/android/location/activity/av;->d:Lcom/google/android/location/activity/k;

    invoke-virtual {v0}, Lcom/google/android/location/activity/k;->k()Lcom/google/android/location/activity/at;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/location/activity/at;->b()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 197
    new-instance v0, Lcom/google/android/location/activity/as;

    iget-object v1, p0, Lcom/google/android/location/activity/av;->d:Lcom/google/android/location/activity/k;

    iget-object v2, p0, Lcom/google/android/location/activity/av;->e:Lcom/google/android/location/os/bi;

    invoke-direct {v0, v1, v2}, Lcom/google/android/location/activity/as;-><init>(Lcom/google/android/location/activity/k;Lcom/google/android/location/os/bi;)V

    invoke-virtual {p0, v0}, Lcom/google/android/location/activity/av;->a(Lcom/google/android/location/activity/av;)V

    goto :goto_0

    .line 199
    :cond_4
    new-instance v0, Lcom/google/android/location/activity/ai;

    iget-object v1, p0, Lcom/google/android/location/activity/av;->d:Lcom/google/android/location/activity/k;

    iget-object v2, p0, Lcom/google/android/location/activity/av;->e:Lcom/google/android/location/os/bi;

    invoke-direct {v0, v1, v2}, Lcom/google/android/location/activity/ai;-><init>(Lcom/google/android/location/activity/k;Lcom/google/android/location/os/bi;)V

    invoke-virtual {p0, v0}, Lcom/google/android/location/activity/av;->a(Lcom/google/android/location/activity/av;)V

    goto :goto_0

    .line 201
    :cond_5
    if-ne p1, v5, :cond_1

    .line 202
    new-instance v0, Lcom/google/android/location/activity/x;

    iget-object v1, p0, Lcom/google/android/location/activity/av;->d:Lcom/google/android/location/activity/k;

    iget-object v2, p0, Lcom/google/android/location/activity/av;->e:Lcom/google/android/location/os/bi;

    invoke-direct {v0, v1, v2}, Lcom/google/android/location/activity/x;-><init>(Lcom/google/android/location/activity/k;Lcom/google/android/location/os/bi;)V

    new-instance v1, Lcom/google/android/location/activity/y;

    iget-object v2, p0, Lcom/google/android/location/activity/av;->d:Lcom/google/android/location/activity/k;

    iget-object v3, p0, Lcom/google/android/location/activity/av;->e:Lcom/google/android/location/os/bi;

    invoke-direct {v1, v2, v3}, Lcom/google/android/location/activity/y;-><init>(Lcom/google/android/location/activity/k;Lcom/google/android/location/os/bi;)V

    invoke-direct {p0, v0, v1}, Lcom/google/android/location/activity/av;->a(Lcom/google/android/location/activity/r;Lcom/google/android/location/activity/s;)V

    goto :goto_0
.end method

.method protected final a(Lcom/google/android/location/activity/av;)V
    .locals 4

    .prologue
    .line 126
    sget-boolean v0, Lcom/google/android/location/i/a;->b:Z

    if-eqz v0, :cond_0

    const-string v0, "ActivityScheduler"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Leaving state: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/location/activity/av;->d:Lcom/google/android/location/activity/k;

    invoke-virtual {v2}, Lcom/google/android/location/activity/k;->g()Lcom/google/android/location/activity/av;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/location/activity/av;->p()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " at: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/location/activity/av;->e:Lcom/google/android/location/os/bi;

    invoke-interface {v2}, Lcom/google/android/location/os/bi;->c()Lcom/google/android/location/j/b;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/location/j/b;->c()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/location/o/a/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 128
    :cond_0
    iget-object v0, p0, Lcom/google/android/location/activity/av;->d:Lcom/google/android/location/activity/k;

    invoke-virtual {v0}, Lcom/google/android/location/activity/k;->g()Lcom/google/android/location/activity/av;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/location/activity/av;->h()V

    .line 129
    iget-object v0, p0, Lcom/google/android/location/activity/av;->d:Lcom/google/android/location/activity/k;

    invoke-virtual {v0, p1}, Lcom/google/android/location/activity/k;->a(Lcom/google/android/location/activity/av;)V

    .line 130
    sget-boolean v0, Lcom/google/android/location/i/a;->b:Z

    if-eqz v0, :cond_1

    const-string v0, "ActivityScheduler"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Entering state: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/location/activity/av;->d:Lcom/google/android/location/activity/k;

    invoke-virtual {v2}, Lcom/google/android/location/activity/k;->g()Lcom/google/android/location/activity/av;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/location/activity/av;->p()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " at: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/location/activity/av;->e:Lcom/google/android/location/os/bi;

    invoke-interface {v2}, Lcom/google/android/location/os/bi;->c()Lcom/google/android/location/j/b;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/location/j/b;->c()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/location/o/a/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 132
    :cond_1
    invoke-virtual {p1}, Lcom/google/android/location/activity/av;->g()V

    .line 133
    return-void
.end method

.method protected a(Lcom/google/android/location/activity/bd;Lcom/google/android/location/activity/bd;)V
    .locals 0

    .prologue
    .line 86
    return-void
.end method

.method protected a(Lcom/google/android/location/j/k;)V
    .locals 0

    .prologue
    .line 116
    return-void
.end method

.method protected a(Z)V
    .locals 0

    .prologue
    .line 56
    return-void
.end method

.method protected final a(Lcom/google/android/location/activity/s;)Z
    .locals 8

    .prologue
    const-wide/32 v6, 0xf4240

    .line 155
    iget-object v0, p0, Lcom/google/android/location/activity/av;->d:Lcom/google/android/location/activity/k;

    invoke-virtual {v0}, Lcom/google/android/location/activity/k;->h()Lcom/google/android/location/activity/a;

    move-result-object v0

    iget-boolean v0, v0, Lcom/google/android/location/activity/a;->a:Z

    if-eqz v0, :cond_1

    .line 157
    iget-object v0, p0, Lcom/google/android/location/activity/av;->d:Lcom/google/android/location/activity/k;

    invoke-virtual {v0}, Lcom/google/android/location/activity/k;->h()Lcom/google/android/location/activity/a;

    move-result-object v0

    iget-boolean v1, v0, Lcom/google/android/location/activity/a;->a:Z

    if-nez v1, :cond_0

    new-instance v0, Ljava/lang/RuntimeException;

    invoke-direct {v0}, Ljava/lang/RuntimeException;-><init>()V

    throw v0

    :cond_0
    iget-object v1, v0, Lcom/google/android/location/activity/a;->b:Lcom/google/android/location/os/bi;

    invoke-interface {v1}, Lcom/google/android/location/os/bi;->c()Lcom/google/android/location/j/b;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/location/j/b;->c()J

    move-result-wide v2

    iget-wide v0, v0, Lcom/google/android/location/activity/a;->c:J

    sub-long v0, v2, v0

    invoke-virtual {p1}, Lcom/google/android/location/activity/s;->av_()J

    move-result-wide v2

    div-long/2addr v2, v6

    const-wide/16 v4, 0x5dc

    add-long/2addr v2, v4

    cmp-long v0, v0, v2

    if-lez v0, :cond_1

    .line 162
    iget-object v0, p0, Lcom/google/android/location/activity/av;->e:Lcom/google/android/location/os/bi;

    invoke-interface {v0}, Lcom/google/android/location/os/bi;->c()Lcom/google/android/location/j/b;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/location/j/b;->c()J

    move-result-wide v0

    iget-object v2, p0, Lcom/google/android/location/activity/av;->d:Lcom/google/android/location/activity/k;

    invoke-virtual {v2}, Lcom/google/android/location/activity/k;->x()J

    move-result-wide v2

    sub-long/2addr v0, v2

    invoke-virtual {p1}, Lcom/google/android/location/activity/s;->d()Lcom/google/android/location/activity/a/o;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/location/activity/a/o;->a()J

    move-result-wide v2

    div-long/2addr v2, v6

    cmp-long v0, v0, v2

    if-lez v0, :cond_1

    .line 164
    const/4 v0, 0x1

    .line 168
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected aq_()V
    .locals 0

    .prologue
    .line 72
    return-void
.end method

.method protected ar_()V
    .locals 0

    .prologue
    .line 93
    return-void
.end method

.method protected as_()V
    .locals 3

    .prologue
    .line 101
    new-instance v0, Lcom/google/android/location/activity/an;

    iget-object v1, p0, Lcom/google/android/location/activity/av;->d:Lcom/google/android/location/activity/k;

    iget-object v2, p0, Lcom/google/android/location/activity/av;->e:Lcom/google/android/location/os/bi;

    invoke-direct {v0, v1, v2}, Lcom/google/android/location/activity/an;-><init>(Lcom/google/android/location/activity/k;Lcom/google/android/location/os/bi;)V

    invoke-virtual {p0, v0}, Lcom/google/android/location/activity/av;->a(Lcom/google/android/location/activity/av;)V

    .line 102
    return-void
.end method

.method public at_()V
    .locals 0

    .prologue
    .line 123
    return-void
.end method

.method public au_()V
    .locals 0

    .prologue
    .line 217
    return-void
.end method

.method protected c()V
    .locals 0

    .prologue
    .line 78
    return-void
.end method

.method g()V
    .locals 0

    .prologue
    .line 28
    return-void
.end method

.method h()V
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lcom/google/android/location/activity/av;->d:Lcom/google/android/location/activity/k;

    invoke-virtual {v0}, Lcom/google/android/location/activity/k;->E()V

    .line 35
    return-void
.end method

.method protected i()V
    .locals 2

    .prologue
    .line 65
    sget-boolean v0, Lcom/google/android/location/i/a;->b:Z

    if-eqz v0, :cond_0

    const-string v0, "ActivityScheduler"

    const-string v1, "clientRemoved."

    invoke-static {v0, v1}, Lcom/google/android/location/o/a/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 66
    :cond_0
    return-void
.end method

.method public j()V
    .locals 0

    .prologue
    .line 211
    return-void
.end method

.method public abstract p()Ljava/lang/String;
.end method
