.class final Lcom/google/android/location/os/h;
.super Ljava/io/FilterWriter;
.source "SourceFile"


# instance fields
.field private final a:I

.field private b:I


# direct methods
.method public constructor <init>(Ljava/io/Writer;)V
    .locals 1

    .prologue
    .line 409
    invoke-direct {p0, p1}, Ljava/io/FilterWriter;-><init>(Ljava/io/Writer;)V

    .line 406
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/location/os/h;->b:I

    .line 410
    const/16 v0, 0x400

    iput v0, p0, Lcom/google/android/location/os/h;->a:I

    .line 414
    return-void
.end method


# virtual methods
.method public final write(I)V
    .locals 2

    .prologue
    .line 418
    iget v0, p0, Lcom/google/android/location/os/h;->b:I

    iget v1, p0, Lcom/google/android/location/os/h;->a:I

    if-ne v0, v1, :cond_0

    .line 419
    iget-object v0, p0, Lcom/google/android/location/os/h;->out:Ljava/io/Writer;

    const/16 v1, 0xa

    invoke-virtual {v0, v1}, Ljava/io/Writer;->write(I)V

    .line 420
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/location/os/h;->b:I

    .line 422
    :cond_0
    iget-object v0, p0, Lcom/google/android/location/os/h;->out:Ljava/io/Writer;

    invoke-virtual {v0, p1}, Ljava/io/Writer;->write(I)V

    .line 423
    iget v0, p0, Lcom/google/android/location/os/h;->b:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/location/os/h;->b:I

    .line 424
    return-void
.end method

.method public final write(Ljava/lang/String;II)V
    .locals 3

    .prologue
    .line 442
    :goto_0
    iget v0, p0, Lcom/google/android/location/os/h;->b:I

    add-int/2addr v0, p3

    iget v1, p0, Lcom/google/android/location/os/h;->a:I

    if-le v0, v1, :cond_0

    .line 443
    iget v0, p0, Lcom/google/android/location/os/h;->a:I

    iget v1, p0, Lcom/google/android/location/os/h;->b:I

    sub-int/2addr v0, v1

    .line 444
    iget-object v1, p0, Lcom/google/android/location/os/h;->out:Ljava/io/Writer;

    invoke-virtual {v1, p1, p2, v0}, Ljava/io/Writer;->write(Ljava/lang/String;II)V

    .line 445
    iget-object v1, p0, Lcom/google/android/location/os/h;->out:Ljava/io/Writer;

    const/16 v2, 0xa

    invoke-virtual {v1, v2}, Ljava/io/Writer;->write(I)V

    .line 446
    add-int/2addr p2, v0

    .line 447
    sub-int/2addr p3, v0

    .line 448
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/location/os/h;->b:I

    goto :goto_0

    .line 450
    :cond_0
    iget-object v0, p0, Lcom/google/android/location/os/h;->out:Ljava/io/Writer;

    invoke-virtual {v0, p1, p2, p3}, Ljava/io/Writer;->write(Ljava/lang/String;II)V

    .line 451
    iget v0, p0, Lcom/google/android/location/os/h;->b:I

    add-int/2addr v0, p3

    iput v0, p0, Lcom/google/android/location/os/h;->b:I

    .line 452
    return-void
.end method

.method public final write([CII)V
    .locals 3

    .prologue
    .line 428
    :goto_0
    iget v0, p0, Lcom/google/android/location/os/h;->b:I

    add-int/2addr v0, p3

    iget v1, p0, Lcom/google/android/location/os/h;->a:I

    if-le v0, v1, :cond_0

    .line 429
    iget v0, p0, Lcom/google/android/location/os/h;->a:I

    iget v1, p0, Lcom/google/android/location/os/h;->b:I

    sub-int/2addr v0, v1

    .line 430
    iget-object v1, p0, Lcom/google/android/location/os/h;->out:Ljava/io/Writer;

    invoke-virtual {v1, p1, p2, v0}, Ljava/io/Writer;->write([CII)V

    .line 431
    iget-object v1, p0, Lcom/google/android/location/os/h;->out:Ljava/io/Writer;

    const/16 v2, 0xa

    invoke-virtual {v1, v2}, Ljava/io/Writer;->write(I)V

    .line 432
    add-int/2addr p2, v0

    .line 433
    sub-int/2addr p3, v0

    .line 434
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/location/os/h;->b:I

    goto :goto_0

    .line 436
    :cond_0
    iget-object v0, p0, Lcom/google/android/location/os/h;->out:Ljava/io/Writer;

    invoke-virtual {v0, p1, p2, p3}, Ljava/io/Writer;->write([CII)V

    .line 437
    iget v0, p0, Lcom/google/android/location/os/h;->b:I

    add-int/2addr v0, p3

    iput v0, p0, Lcom/google/android/location/os/h;->b:I

    .line 438
    return-void
.end method
