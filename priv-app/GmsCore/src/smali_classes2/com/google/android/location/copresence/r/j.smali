.class final Lcom/google/android/location/copresence/r/j;
.super Lcom/google/android/location/copresence/k/f;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/location/copresence/r/f;


# direct methods
.method varargs constructor <init>(Lcom/google/android/location/copresence/r/f;Ljava/lang/String;[Lcom/google/android/location/copresence/k/f;)V
    .locals 0

    .prologue
    .line 152
    iput-object p1, p0, Lcom/google/android/location/copresence/r/j;->a:Lcom/google/android/location/copresence/r/f;

    invoke-direct {p0, p2, p3}, Lcom/google/android/location/copresence/k/f;-><init>(Ljava/lang/String;[Lcom/google/android/location/copresence/k/f;)V

    return-void
.end method


# virtual methods
.method public final a()Z
    .locals 3

    .prologue
    .line 170
    iget-object v0, p0, Lcom/google/android/location/copresence/r/j;->a:Lcom/google/android/location/copresence/r/f;

    invoke-static {v0}, Lcom/google/android/location/copresence/r/f;->a(Lcom/google/android/location/copresence/r/f;)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 171
    iget-object v1, p0, Lcom/google/android/location/copresence/r/j;->a:Lcom/google/android/location/copresence/r/f;

    invoke-static {v1}, Lcom/google/android/location/copresence/r/f;->b(Lcom/google/android/location/copresence/r/f;)Lcom/google/android/location/copresence/r/ae;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/location/copresence/r/ae;->b()Landroid/net/wifi/WifiConfiguration;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/location/copresence/r/f;->a(Landroid/content/SharedPreferences$Editor;Landroid/net/wifi/WifiConfiguration;)Landroid/content/SharedPreferences$Editor;

    .line 172
    const-string v1, "wifiEnabled"

    iget-object v2, p0, Lcom/google/android/location/copresence/r/j;->a:Lcom/google/android/location/copresence/r/f;

    invoke-static {v2}, Lcom/google/android/location/copresence/r/f;->b(Lcom/google/android/location/copresence/r/f;)Lcom/google/android/location/copresence/r/ae;

    move-result-object v2

    iget-object v2, v2, Lcom/google/android/location/copresence/r/ae;->a:Landroid/net/wifi/WifiManager;

    invoke-virtual {v2}, Landroid/net/wifi/WifiManager;->isWifiEnabled()Z

    move-result v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 173
    iget-object v1, p0, Lcom/google/android/location/copresence/r/j;->a:Lcom/google/android/location/copresence/r/f;

    const-string v1, "save user wifi ap config"

    invoke-static {v0, v1}, Lcom/google/android/location/copresence/r/f;->a(Landroid/content/SharedPreferences$Editor;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 157
    invoke-static {}, Lcom/google/android/location/copresence/r/f;->a()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 158
    iget-object v3, p0, Lcom/google/android/location/copresence/r/j;->a:Lcom/google/android/location/copresence/r/f;

    invoke-static {v3}, Lcom/google/android/location/copresence/r/f;->a(Lcom/google/android/location/copresence/r/f;)Landroid/content/SharedPreferences;

    move-result-object v3

    invoke-interface {v3, v0}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    .line 165
    :goto_0
    return v0

    .line 162
    :cond_1
    iget-object v0, p0, Lcom/google/android/location/copresence/r/j;->a:Lcom/google/android/location/copresence/r/f;

    invoke-static {v0}, Lcom/google/android/location/copresence/r/f;->a(Lcom/google/android/location/copresence/r/f;)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v2, "wifiEnabled"

    invoke-interface {v0, v2}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v1

    .line 163
    goto :goto_0

    .line 165
    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method
