.class public final Lcom/google/android/location/places/v;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method static a(Lcom/google/android/location/l/a/as;)Landroid/os/Bundle;
    .locals 4

    .prologue
    .line 379
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 381
    const/4 v0, 0x0

    :goto_0
    iget-object v2, p0, Lcom/google/android/location/l/a/as;->d:[Lcom/google/android/location/l/a/au;

    array-length v2, v2

    if-ge v0, v2, :cond_0

    .line 382
    iget-object v2, p0, Lcom/google/android/location/l/a/as;->d:[Lcom/google/android/location/l/a/au;

    aget-object v2, v2, v0

    iget-object v2, v2, Lcom/google/android/location/l/a/au;->a:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/location/l/a/as;->d:[Lcom/google/android/location/l/a/au;

    aget-object v3, v3, v0

    iget-object v3, v3, Lcom/google/android/location/l/a/au;->b:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 381
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 386
    :cond_0
    return-object v1
.end method

.method public static a(Lcom/google/android/location/l/a/br;)Lcom/google/android/gms/location/places/AutocompletePrediction;
    .locals 10

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 41
    if-nez p0, :cond_0

    move-object v0, v1

    .line 81
    :goto_0
    return-object v0

    .line 45
    :cond_0
    iget-object v0, p0, Lcom/google/android/location/l/a/br;->a:Ljava/lang/String;

    if-nez v0, :cond_1

    move-object v0, v1

    .line 46
    goto :goto_0

    .line 49
    :cond_1
    iget-object v3, p0, Lcom/google/android/location/l/a/br;->a:Ljava/lang/String;

    .line 50
    iget-object v4, p0, Lcom/google/android/location/l/a/br;->b:Ljava/lang/String;

    .line 52
    iget-object v0, p0, Lcom/google/android/location/l/a/br;->c:[Ljava/lang/String;

    array-length v5, v0

    .line 53
    iget-object v0, p0, Lcom/google/android/location/l/a/br;->d:[Lcom/google/android/location/l/a/bt;

    array-length v6, v0

    .line 55
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 56
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    move v0, v2

    .line 59
    :goto_1
    if-ge v0, v5, :cond_2

    .line 60
    iget-object v9, p0, Lcom/google/android/location/l/a/br;->c:[Ljava/lang/String;

    aget-object v9, v9, v0

    invoke-interface {v7, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 59
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 63
    :cond_2
    iget-object v0, p0, Lcom/google/android/location/l/a/br;->f:Ljava/lang/Integer;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/google/android/location/l/a/br;->f:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 67
    :goto_2
    if-ge v2, v6, :cond_6

    .line 68
    iget-object v5, p0, Lcom/google/android/location/l/a/br;->d:[Lcom/google/android/location/l/a/bt;

    aget-object v5, v5, v2

    .line 71
    iget-object v9, v5, Lcom/google/android/location/l/a/bt;->a:Ljava/lang/Integer;

    if-eqz v9, :cond_3

    iget-object v9, v5, Lcom/google/android/location/l/a/bt;->b:Ljava/lang/Integer;

    if-nez v9, :cond_5

    :cond_3
    move-object v0, v1

    .line 73
    goto :goto_0

    .line 63
    :cond_4
    const/4 v0, 0x6

    goto :goto_2

    .line 76
    :cond_5
    iget-object v9, v5, Lcom/google/android/location/l/a/bt;->a:Ljava/lang/Integer;

    invoke-virtual {v9}, Ljava/lang/Integer;->intValue()I

    move-result v9

    .line 77
    iget-object v5, v5, Lcom/google/android/location/l/a/bt;->b:Ljava/lang/Integer;

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    .line 78
    invoke-static {v9, v5}, Lcom/google/android/gms/location/places/AutocompletePrediction$Substring;->a(II)Lcom/google/android/gms/location/places/AutocompletePrediction$Substring;

    move-result-object v5

    invoke-interface {v8, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 67
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 81
    :cond_6
    invoke-static {v3, v4, v7, v8, v0}, Lcom/google/android/gms/location/places/AutocompletePrediction;->a(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/util/List;I)Lcom/google/android/gms/location/places/AutocompletePrediction;

    move-result-object v0

    goto :goto_0
.end method

.method static a(Lcom/google/android/location/l/a/ag;)Lcom/google/android/gms/maps/model/LatLng;
    .locals 8

    .prologue
    const-wide v6, 0x416312d000000000L    # 1.0E7

    .line 396
    new-instance v0, Lcom/google/android/gms/maps/model/LatLng;

    iget-object v1, p0, Lcom/google/android/location/l/a/ag;->a:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    int-to-double v2, v1

    div-double/2addr v2, v6

    iget-object v1, p0, Lcom/google/android/location/l/a/ag;->b:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    int-to-double v4, v1

    div-double/2addr v4, v6

    invoke-direct {v0, v2, v3, v4, v5}, Lcom/google/android/gms/maps/model/LatLng;-><init>(DD)V

    return-object v0
.end method
