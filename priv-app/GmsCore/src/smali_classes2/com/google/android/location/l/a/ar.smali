.class public final Lcom/google/android/location/l/a/ar;
.super Lcom/google/protobuf/nano/d;
.source "SourceFile"


# static fields
.field private static volatile k:[Lcom/google/android/location/l/a/ar;


# instance fields
.field public a:Ljava/lang/Float;

.field public b:Ljava/lang/Float;

.field public c:Ljava/lang/Float;

.field public d:Ljava/lang/Integer;

.field public e:Ljava/lang/Integer;

.field public f:Ljava/lang/Integer;

.field public g:Ljava/lang/Integer;

.field public h:Ljava/lang/Integer;

.field public i:Ljava/lang/Long;

.field public j:Ljava/lang/Long;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 53
    invoke-direct {p0}, Lcom/google/protobuf/nano/d;-><init>()V

    .line 54
    iput-object v0, p0, Lcom/google/android/location/l/a/ar;->a:Ljava/lang/Float;

    iput-object v0, p0, Lcom/google/android/location/l/a/ar;->b:Ljava/lang/Float;

    iput-object v0, p0, Lcom/google/android/location/l/a/ar;->c:Ljava/lang/Float;

    iput-object v0, p0, Lcom/google/android/location/l/a/ar;->d:Ljava/lang/Integer;

    iput-object v0, p0, Lcom/google/android/location/l/a/ar;->e:Ljava/lang/Integer;

    iput-object v0, p0, Lcom/google/android/location/l/a/ar;->f:Ljava/lang/Integer;

    iput-object v0, p0, Lcom/google/android/location/l/a/ar;->g:Ljava/lang/Integer;

    iput-object v0, p0, Lcom/google/android/location/l/a/ar;->h:Ljava/lang/Integer;

    iput-object v0, p0, Lcom/google/android/location/l/a/ar;->i:Ljava/lang/Long;

    iput-object v0, p0, Lcom/google/android/location/l/a/ar;->j:Ljava/lang/Long;

    iput-object v0, p0, Lcom/google/android/location/l/a/ar;->unknownFieldData:Lcom/google/protobuf/nano/f;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/location/l/a/ar;->cachedSize:I

    .line 55
    return-void
.end method

.method public static a()[Lcom/google/android/location/l/a/ar;
    .locals 2

    .prologue
    .line 12
    sget-object v0, Lcom/google/android/location/l/a/ar;->k:[Lcom/google/android/location/l/a/ar;

    if-nez v0, :cond_1

    .line 13
    sget-object v1, Lcom/google/protobuf/nano/h;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 15
    :try_start_0
    sget-object v0, Lcom/google/android/location/l/a/ar;->k:[Lcom/google/android/location/l/a/ar;

    if-nez v0, :cond_0

    .line 16
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/android/location/l/a/ar;

    sput-object v0, Lcom/google/android/location/l/a/ar;->k:[Lcom/google/android/location/l/a/ar;

    .line 18
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 20
    :cond_1
    sget-object v0, Lcom/google/android/location/l/a/ar;->k:[Lcom/google/android/location/l/a/ar;

    return-object v0

    .line 18
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 4

    .prologue
    .line 220
    invoke-super {p0}, Lcom/google/protobuf/nano/d;->computeSerializedSize()I

    move-result v0

    .line 221
    iget-object v1, p0, Lcom/google/android/location/l/a/ar;->a:Ljava/lang/Float;

    if-eqz v1, :cond_0

    .line 222
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/location/l/a/ar;->a:Ljava/lang/Float;

    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    .line 225
    :cond_0
    iget-object v1, p0, Lcom/google/android/location/l/a/ar;->b:Ljava/lang/Float;

    if-eqz v1, :cond_1

    .line 226
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/location/l/a/ar;->b:Ljava/lang/Float;

    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    .line 229
    :cond_1
    iget-object v1, p0, Lcom/google/android/location/l/a/ar;->c:Ljava/lang/Float;

    if-eqz v1, :cond_2

    .line 230
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/android/location/l/a/ar;->c:Ljava/lang/Float;

    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    .line 233
    :cond_2
    iget-object v1, p0, Lcom/google/android/location/l/a/ar;->d:Ljava/lang/Integer;

    if-eqz v1, :cond_3

    .line 234
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/android/location/l/a/ar;->d:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 237
    :cond_3
    iget-object v1, p0, Lcom/google/android/location/l/a/ar;->e:Ljava/lang/Integer;

    if-eqz v1, :cond_4

    .line 238
    const/4 v1, 0x5

    iget-object v2, p0, Lcom/google/android/location/l/a/ar;->e:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 241
    :cond_4
    iget-object v1, p0, Lcom/google/android/location/l/a/ar;->f:Ljava/lang/Integer;

    if-eqz v1, :cond_5

    .line 242
    const/4 v1, 0x6

    iget-object v2, p0, Lcom/google/android/location/l/a/ar;->f:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 245
    :cond_5
    iget-object v1, p0, Lcom/google/android/location/l/a/ar;->g:Ljava/lang/Integer;

    if-eqz v1, :cond_6

    .line 246
    const/4 v1, 0x7

    iget-object v2, p0, Lcom/google/android/location/l/a/ar;->g:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 249
    :cond_6
    iget-object v1, p0, Lcom/google/android/location/l/a/ar;->h:Ljava/lang/Integer;

    if-eqz v1, :cond_7

    .line 250
    const/16 v1, 0x8

    iget-object v2, p0, Lcom/google/android/location/l/a/ar;->h:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 253
    :cond_7
    iget-object v1, p0, Lcom/google/android/location/l/a/ar;->i:Ljava/lang/Long;

    if-eqz v1, :cond_8

    .line 254
    const/16 v1, 0x9

    iget-object v2, p0, Lcom/google/android/location/l/a/ar;->i:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/b;->f(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 257
    :cond_8
    iget-object v1, p0, Lcom/google/android/location/l/a/ar;->j:Ljava/lang/Long;

    if-eqz v1, :cond_9

    .line 258
    const/16 v1, 0xa

    iget-object v2, p0, Lcom/google/android/location/l/a/ar;->j:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/b;->f(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 261
    :cond_9
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 75
    if-ne p1, p0, :cond_1

    .line 76
    const/4 v0, 0x1

    .line 152
    :cond_0
    :goto_0
    return v0

    .line 78
    :cond_1
    instance-of v1, p1, Lcom/google/android/location/l/a/ar;

    if-eqz v1, :cond_0

    .line 81
    check-cast p1, Lcom/google/android/location/l/a/ar;

    .line 82
    iget-object v1, p0, Lcom/google/android/location/l/a/ar;->a:Ljava/lang/Float;

    if-nez v1, :cond_c

    .line 83
    iget-object v1, p1, Lcom/google/android/location/l/a/ar;->a:Ljava/lang/Float;

    if-nez v1, :cond_0

    .line 89
    :cond_2
    iget-object v1, p0, Lcom/google/android/location/l/a/ar;->b:Ljava/lang/Float;

    if-nez v1, :cond_d

    .line 90
    iget-object v1, p1, Lcom/google/android/location/l/a/ar;->b:Ljava/lang/Float;

    if-nez v1, :cond_0

    .line 96
    :cond_3
    iget-object v1, p0, Lcom/google/android/location/l/a/ar;->c:Ljava/lang/Float;

    if-nez v1, :cond_e

    .line 97
    iget-object v1, p1, Lcom/google/android/location/l/a/ar;->c:Ljava/lang/Float;

    if-nez v1, :cond_0

    .line 103
    :cond_4
    iget-object v1, p0, Lcom/google/android/location/l/a/ar;->d:Ljava/lang/Integer;

    if-nez v1, :cond_f

    .line 104
    iget-object v1, p1, Lcom/google/android/location/l/a/ar;->d:Ljava/lang/Integer;

    if-nez v1, :cond_0

    .line 110
    :cond_5
    iget-object v1, p0, Lcom/google/android/location/l/a/ar;->e:Ljava/lang/Integer;

    if-nez v1, :cond_10

    .line 111
    iget-object v1, p1, Lcom/google/android/location/l/a/ar;->e:Ljava/lang/Integer;

    if-nez v1, :cond_0

    .line 117
    :cond_6
    iget-object v1, p0, Lcom/google/android/location/l/a/ar;->f:Ljava/lang/Integer;

    if-nez v1, :cond_11

    .line 118
    iget-object v1, p1, Lcom/google/android/location/l/a/ar;->f:Ljava/lang/Integer;

    if-nez v1, :cond_0

    .line 124
    :cond_7
    iget-object v1, p0, Lcom/google/android/location/l/a/ar;->g:Ljava/lang/Integer;

    if-nez v1, :cond_12

    .line 125
    iget-object v1, p1, Lcom/google/android/location/l/a/ar;->g:Ljava/lang/Integer;

    if-nez v1, :cond_0

    .line 131
    :cond_8
    iget-object v1, p0, Lcom/google/android/location/l/a/ar;->h:Ljava/lang/Integer;

    if-nez v1, :cond_13

    .line 132
    iget-object v1, p1, Lcom/google/android/location/l/a/ar;->h:Ljava/lang/Integer;

    if-nez v1, :cond_0

    .line 138
    :cond_9
    iget-object v1, p0, Lcom/google/android/location/l/a/ar;->i:Ljava/lang/Long;

    if-nez v1, :cond_14

    .line 139
    iget-object v1, p1, Lcom/google/android/location/l/a/ar;->i:Ljava/lang/Long;

    if-nez v1, :cond_0

    .line 145
    :cond_a
    iget-object v1, p0, Lcom/google/android/location/l/a/ar;->j:Ljava/lang/Long;

    if-nez v1, :cond_15

    .line 146
    iget-object v1, p1, Lcom/google/android/location/l/a/ar;->j:Ljava/lang/Long;

    if-nez v1, :cond_0

    .line 152
    :cond_b
    invoke-virtual {p0, p1}, Lcom/google/android/location/l/a/ar;->unknownFieldDataEquals(Lcom/google/protobuf/nano/d;)Z

    move-result v0

    goto :goto_0

    .line 86
    :cond_c
    iget-object v1, p0, Lcom/google/android/location/l/a/ar;->a:Ljava/lang/Float;

    iget-object v2, p1, Lcom/google/android/location/l/a/ar;->a:Ljava/lang/Float;

    invoke-virtual {v1, v2}, Ljava/lang/Float;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    goto :goto_0

    .line 93
    :cond_d
    iget-object v1, p0, Lcom/google/android/location/l/a/ar;->b:Ljava/lang/Float;

    iget-object v2, p1, Lcom/google/android/location/l/a/ar;->b:Ljava/lang/Float;

    invoke-virtual {v1, v2}, Ljava/lang/Float;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    goto :goto_0

    .line 100
    :cond_e
    iget-object v1, p0, Lcom/google/android/location/l/a/ar;->c:Ljava/lang/Float;

    iget-object v2, p1, Lcom/google/android/location/l/a/ar;->c:Ljava/lang/Float;

    invoke-virtual {v1, v2}, Ljava/lang/Float;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    goto :goto_0

    .line 107
    :cond_f
    iget-object v1, p0, Lcom/google/android/location/l/a/ar;->d:Ljava/lang/Integer;

    iget-object v2, p1, Lcom/google/android/location/l/a/ar;->d:Ljava/lang/Integer;

    invoke-virtual {v1, v2}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_5

    goto/16 :goto_0

    .line 114
    :cond_10
    iget-object v1, p0, Lcom/google/android/location/l/a/ar;->e:Ljava/lang/Integer;

    iget-object v2, p1, Lcom/google/android/location/l/a/ar;->e:Ljava/lang/Integer;

    invoke-virtual {v1, v2}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_6

    goto/16 :goto_0

    .line 121
    :cond_11
    iget-object v1, p0, Lcom/google/android/location/l/a/ar;->f:Ljava/lang/Integer;

    iget-object v2, p1, Lcom/google/android/location/l/a/ar;->f:Ljava/lang/Integer;

    invoke-virtual {v1, v2}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_7

    goto/16 :goto_0

    .line 128
    :cond_12
    iget-object v1, p0, Lcom/google/android/location/l/a/ar;->g:Ljava/lang/Integer;

    iget-object v2, p1, Lcom/google/android/location/l/a/ar;->g:Ljava/lang/Integer;

    invoke-virtual {v1, v2}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_8

    goto/16 :goto_0

    .line 135
    :cond_13
    iget-object v1, p0, Lcom/google/android/location/l/a/ar;->h:Ljava/lang/Integer;

    iget-object v2, p1, Lcom/google/android/location/l/a/ar;->h:Ljava/lang/Integer;

    invoke-virtual {v1, v2}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_9

    goto/16 :goto_0

    .line 142
    :cond_14
    iget-object v1, p0, Lcom/google/android/location/l/a/ar;->i:Ljava/lang/Long;

    iget-object v2, p1, Lcom/google/android/location/l/a/ar;->i:Ljava/lang/Long;

    invoke-virtual {v1, v2}, Ljava/lang/Long;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_a

    goto/16 :goto_0

    .line 149
    :cond_15
    iget-object v1, p0, Lcom/google/android/location/l/a/ar;->j:Ljava/lang/Long;

    iget-object v2, p1, Lcom/google/android/location/l/a/ar;->j:Ljava/lang/Long;

    invoke-virtual {v1, v2}, Ljava/lang/Long;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_b

    goto/16 :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 157
    iget-object v0, p0, Lcom/google/android/location/l/a/ar;->a:Ljava/lang/Float;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    .line 160
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/location/l/a/ar;->b:Ljava/lang/Float;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    .line 162
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/location/l/a/ar;->c:Ljava/lang/Float;

    if-nez v0, :cond_2

    move v0, v1

    :goto_2
    add-int/2addr v0, v2

    .line 164
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/location/l/a/ar;->d:Ljava/lang/Integer;

    if-nez v0, :cond_3

    move v0, v1

    :goto_3
    add-int/2addr v0, v2

    .line 166
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/location/l/a/ar;->e:Ljava/lang/Integer;

    if-nez v0, :cond_4

    move v0, v1

    :goto_4
    add-int/2addr v0, v2

    .line 168
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/location/l/a/ar;->f:Ljava/lang/Integer;

    if-nez v0, :cond_5

    move v0, v1

    :goto_5
    add-int/2addr v0, v2

    .line 170
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/location/l/a/ar;->g:Ljava/lang/Integer;

    if-nez v0, :cond_6

    move v0, v1

    :goto_6
    add-int/2addr v0, v2

    .line 172
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/location/l/a/ar;->h:Ljava/lang/Integer;

    if-nez v0, :cond_7

    move v0, v1

    :goto_7
    add-int/2addr v0, v2

    .line 174
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/location/l/a/ar;->i:Ljava/lang/Long;

    if-nez v0, :cond_8

    move v0, v1

    :goto_8
    add-int/2addr v0, v2

    .line 176
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/android/location/l/a/ar;->j:Ljava/lang/Long;

    if-nez v2, :cond_9

    :goto_9
    add-int/2addr v0, v1

    .line 178
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/google/android/location/l/a/ar;->unknownFieldDataHashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 179
    return v0

    .line 157
    :cond_0
    iget-object v0, p0, Lcom/google/android/location/l/a/ar;->a:Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->hashCode()I

    move-result v0

    goto :goto_0

    .line 160
    :cond_1
    iget-object v0, p0, Lcom/google/android/location/l/a/ar;->b:Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->hashCode()I

    move-result v0

    goto :goto_1

    .line 162
    :cond_2
    iget-object v0, p0, Lcom/google/android/location/l/a/ar;->c:Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->hashCode()I

    move-result v0

    goto :goto_2

    .line 164
    :cond_3
    iget-object v0, p0, Lcom/google/android/location/l/a/ar;->d:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->hashCode()I

    move-result v0

    goto :goto_3

    .line 166
    :cond_4
    iget-object v0, p0, Lcom/google/android/location/l/a/ar;->e:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->hashCode()I

    move-result v0

    goto :goto_4

    .line 168
    :cond_5
    iget-object v0, p0, Lcom/google/android/location/l/a/ar;->f:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->hashCode()I

    move-result v0

    goto :goto_5

    .line 170
    :cond_6
    iget-object v0, p0, Lcom/google/android/location/l/a/ar;->g:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->hashCode()I

    move-result v0

    goto :goto_6

    .line 172
    :cond_7
    iget-object v0, p0, Lcom/google/android/location/l/a/ar;->h:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->hashCode()I

    move-result v0

    goto :goto_7

    .line 174
    :cond_8
    iget-object v0, p0, Lcom/google/android/location/l/a/ar;->i:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->hashCode()I

    move-result v0

    goto :goto_8

    .line 176
    :cond_9
    iget-object v1, p0, Lcom/google/android/location/l/a/ar;->j:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->hashCode()I

    move-result v1

    goto :goto_9
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 2

    .prologue
    .line 5
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/android/location/l/a/ar;->storeUnknownField(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->k()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->intBitsToFloat(I)F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/l/a/ar;->a:Ljava/lang/Float;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->k()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->intBitsToFloat(I)F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/l/a/ar;->b:Ljava/lang/Float;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->k()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->intBitsToFloat(I)F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/l/a/ar;->c:Ljava/lang/Float;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/l/a/ar;->d:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/l/a/ar;->e:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/l/a/ar;->f:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_7
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/l/a/ar;->g:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_8
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/l/a/ar;->h:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_9
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->j()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/l/a/ar;->i:Ljava/lang/Long;

    goto :goto_0

    :sswitch_a
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->j()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/l/a/ar;->j:Ljava/lang/Long;

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xd -> :sswitch_1
        0x15 -> :sswitch_2
        0x1d -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
        0x30 -> :sswitch_6
        0x38 -> :sswitch_7
        0x40 -> :sswitch_8
        0x48 -> :sswitch_9
        0x50 -> :sswitch_a
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 4

    .prologue
    .line 185
    iget-object v0, p0, Lcom/google/android/location/l/a/ar;->a:Ljava/lang/Float;

    if-eqz v0, :cond_0

    .line 186
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/location/l/a/ar;->a:Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IF)V

    .line 188
    :cond_0
    iget-object v0, p0, Lcom/google/android/location/l/a/ar;->b:Ljava/lang/Float;

    if-eqz v0, :cond_1

    .line 189
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/android/location/l/a/ar;->b:Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IF)V

    .line 191
    :cond_1
    iget-object v0, p0, Lcom/google/android/location/l/a/ar;->c:Ljava/lang/Float;

    if-eqz v0, :cond_2

    .line 192
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/android/location/l/a/ar;->c:Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IF)V

    .line 194
    :cond_2
    iget-object v0, p0, Lcom/google/android/location/l/a/ar;->d:Ljava/lang/Integer;

    if-eqz v0, :cond_3

    .line 195
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/android/location/l/a/ar;->d:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 197
    :cond_3
    iget-object v0, p0, Lcom/google/android/location/l/a/ar;->e:Ljava/lang/Integer;

    if-eqz v0, :cond_4

    .line 198
    const/4 v0, 0x5

    iget-object v1, p0, Lcom/google/android/location/l/a/ar;->e:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 200
    :cond_4
    iget-object v0, p0, Lcom/google/android/location/l/a/ar;->f:Ljava/lang/Integer;

    if-eqz v0, :cond_5

    .line 201
    const/4 v0, 0x6

    iget-object v1, p0, Lcom/google/android/location/l/a/ar;->f:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 203
    :cond_5
    iget-object v0, p0, Lcom/google/android/location/l/a/ar;->g:Ljava/lang/Integer;

    if-eqz v0, :cond_6

    .line 204
    const/4 v0, 0x7

    iget-object v1, p0, Lcom/google/android/location/l/a/ar;->g:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 206
    :cond_6
    iget-object v0, p0, Lcom/google/android/location/l/a/ar;->h:Ljava/lang/Integer;

    if-eqz v0, :cond_7

    .line 207
    const/16 v0, 0x8

    iget-object v1, p0, Lcom/google/android/location/l/a/ar;->h:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 209
    :cond_7
    iget-object v0, p0, Lcom/google/android/location/l/a/ar;->i:Ljava/lang/Long;

    if-eqz v0, :cond_8

    .line 210
    const/16 v0, 0x9

    iget-object v1, p0, Lcom/google/android/location/l/a/ar;->i:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/b;->b(IJ)V

    .line 212
    :cond_8
    iget-object v0, p0, Lcom/google/android/location/l/a/ar;->j:Ljava/lang/Long;

    if-eqz v0, :cond_9

    .line 213
    const/16 v0, 0xa

    iget-object v1, p0, Lcom/google/android/location/l/a/ar;->j:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/b;->b(IJ)V

    .line 215
    :cond_9
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/d;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 216
    return-void
.end method
