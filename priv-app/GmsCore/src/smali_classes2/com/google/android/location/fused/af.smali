.class final Lcom/google/android/location/fused/af;
.super Lcom/google/android/location/fused/bg;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/location/fused/ad;


# direct methods
.method public constructor <init>(Lcom/google/android/location/fused/ad;Lcom/google/android/location/fused/a/s;Lcom/google/android/location/fused/a/r;)V
    .locals 2

    .prologue
    .line 502
    iput-object p1, p0, Lcom/google/android/location/fused/af;->a:Lcom/google/android/location/fused/ad;

    .line 503
    new-instance v0, Lcom/google/android/location/fused/c;

    invoke-direct {v0}, Lcom/google/android/location/fused/c;-><init>()V

    invoke-static {p1}, Lcom/google/android/location/fused/ad;->a(Lcom/google/android/location/fused/ad;)Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {p0, p2, p3, v0, v1}, Lcom/google/android/location/fused/bg;-><init>(Lcom/google/android/location/fused/a/s;Lcom/google/android/location/fused/a/r;Lcom/google/android/location/fused/c;Landroid/os/Looper;)V

    .line 508
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 539
    iget-object v0, p0, Lcom/google/android/location/fused/af;->a:Lcom/google/android/location/fused/ad;

    invoke-static {v0}, Lcom/google/android/location/fused/ad;->f(Lcom/google/android/location/fused/ad;)Lcom/google/android/location/fused/a/o;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/location/fused/a/o;->f()V

    .line 540
    iget-object v0, p0, Lcom/google/android/location/fused/af;->a:Lcom/google/android/location/fused/ad;

    invoke-static {v0}, Lcom/google/android/location/fused/ad;->g(Lcom/google/android/location/fused/ad;)Lcom/google/android/location/fused/a/g;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/location/fused/a/g;->f()V

    .line 541
    return-void
.end method

.method public final a(Z)V
    .locals 7

    .prologue
    const/4 v4, 0x1

    const/4 v5, 0x0

    const-wide/32 v2, 0x5265c00

    .line 513
    iget v0, p0, Lcom/google/android/location/fused/bg;->c:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_1

    move v0, v4

    :goto_0
    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/location/fused/af;->e()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/location/fused/af;->a:Lcom/google/android/location/fused/ad;

    invoke-static {v0}, Lcom/google/android/location/fused/ad;->e(Lcom/google/android/location/fused/ad;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 514
    :cond_0
    iget-object v0, p0, Lcom/google/android/location/fused/bg;->b:Lcom/google/android/location/fused/bl;

    invoke-virtual {v0}, Lcom/google/android/location/fused/bl;->e()J

    move-result-wide v0

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v0

    .line 518
    :goto_1
    iget-object v6, p0, Lcom/google/android/location/fused/af;->a:Lcom/google/android/location/fused/ad;

    invoke-static {v6}, Lcom/google/android/location/fused/ad;->f(Lcom/google/android/location/fused/ad;)Lcom/google/android/location/fused/a/o;

    move-result-object v6

    invoke-virtual {v6, v0, v1}, Lcom/google/android/location/fused/a/o;->a(J)V

    .line 521
    iget-object v6, p0, Lcom/google/android/location/fused/af;->a:Lcom/google/android/location/fused/ad;

    invoke-static {v6}, Lcom/google/android/location/fused/ad;->f(Lcom/google/android/location/fused/ad;)Lcom/google/android/location/fused/a/o;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/android/location/fused/a/o;->b()V

    .line 522
    iget-object v6, p0, Lcom/google/android/location/fused/af;->a:Lcom/google/android/location/fused/ad;

    invoke-static {v6}, Lcom/google/android/location/fused/ad;->f(Lcom/google/android/location/fused/ad;)Lcom/google/android/location/fused/a/o;

    move-result-object v6

    if-eqz p1, :cond_3

    cmp-long v0, v0, v2

    if-gez v0, :cond_3

    move v0, v4

    :goto_2
    iput-boolean v0, v6, Lcom/google/android/location/fused/a/m;->g:Z

    .line 524
    iget-object v0, p0, Lcom/google/android/location/fused/af;->a:Lcom/google/android/location/fused/ad;

    invoke-static {v0}, Lcom/google/android/location/fused/ad;->f(Lcom/google/android/location/fused/ad;)Lcom/google/android/location/fused/a/o;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/location/fused/bg;->b:Lcom/google/android/location/fused/bl;

    invoke-virtual {v1}, Lcom/google/android/location/fused/bl;->f()Ljava/util/Collection;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/location/fused/a/o;->a(Ljava/util/Collection;)V

    .line 525
    iget-object v0, p0, Lcom/google/android/location/fused/af;->a:Lcom/google/android/location/fused/ad;

    invoke-static {v0}, Lcom/google/android/location/fused/ad;->f(Lcom/google/android/location/fused/ad;)Lcom/google/android/location/fused/a/o;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/location/fused/a/o;->e()V

    .line 527
    invoke-virtual {p0}, Lcom/google/android/location/fused/af;->d()Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/google/android/location/fused/bg;->b:Lcom/google/android/location/fused/bl;

    invoke-virtual {v0}, Lcom/google/android/location/fused/bl;->e()J

    move-result-wide v0

    const-wide v2, 0x7fffffffffffffffL

    cmp-long v0, v0, v2

    if-gez v0, :cond_4

    .line 528
    iget-object v0, p0, Lcom/google/android/location/fused/af;->a:Lcom/google/android/location/fused/ad;

    invoke-static {v0}, Lcom/google/android/location/fused/ad;->g(Lcom/google/android/location/fused/ad;)Lcom/google/android/location/fused/a/g;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/location/fused/bg;->b:Lcom/google/android/location/fused/bl;

    invoke-virtual {v1}, Lcom/google/android/location/fused/bl;->f()Ljava/util/Collection;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/location/fused/a/g;->a(Ljava/util/Collection;)V

    .line 529
    iget-object v0, p0, Lcom/google/android/location/fused/af;->a:Lcom/google/android/location/fused/ad;

    invoke-static {v0}, Lcom/google/android/location/fused/ad;->g(Lcom/google/android/location/fused/ad;)Lcom/google/android/location/fused/a/g;

    move-result-object v0

    const-wide/16 v2, 0x2

    iget-object v1, p0, Lcom/google/android/location/fused/bg;->b:Lcom/google/android/location/fused/bl;

    invoke-virtual {v1}, Lcom/google/android/location/fused/bl;->e()J

    move-result-wide v4

    mul-long/2addr v2, v4

    invoke-virtual {v0, v2, v3}, Lcom/google/android/location/fused/a/g;->a(J)V

    .line 531
    iget-object v0, p0, Lcom/google/android/location/fused/af;->a:Lcom/google/android/location/fused/ad;

    invoke-static {v0}, Lcom/google/android/location/fused/ad;->g(Lcom/google/android/location/fused/ad;)Lcom/google/android/location/fused/a/g;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/location/fused/a/g;->e()V

    .line 535
    :goto_3
    return-void

    :cond_1
    move v0, v5

    .line 513
    goto/16 :goto_0

    :cond_2
    move-wide v0, v2

    .line 516
    goto :goto_1

    :cond_3
    move v0, v5

    .line 522
    goto :goto_2

    .line 533
    :cond_4
    iget-object v0, p0, Lcom/google/android/location/fused/af;->a:Lcom/google/android/location/fused/ad;

    invoke-static {v0}, Lcom/google/android/location/fused/ad;->g(Lcom/google/android/location/fused/ad;)Lcom/google/android/location/fused/a/g;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/location/fused/a/g;->f()V

    goto :goto_3
.end method
