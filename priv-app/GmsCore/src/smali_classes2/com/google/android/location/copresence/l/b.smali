.class public Lcom/google/android/location/copresence/l/b;
.super Lcom/google/android/gms/common/server/q;
.source "SourceFile"


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 8

    .prologue
    .line 30
    sget-object v0, Lcom/google/android/location/copresence/k;->a:Lcom/google/android/location/copresence/l;

    invoke-virtual {v0}, Lcom/google/android/location/copresence/l;->c()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    sget-object v0, Lcom/google/android/location/copresence/k;->b:Lcom/google/android/location/copresence/l;

    invoke-virtual {v0}, Lcom/google/android/location/copresence/l;->c()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-static {}, Lcom/google/android/location/copresence/f/b;->b()Lcom/google/ac/b/c/g;

    move-result-object v0

    iget-object v0, v0, Lcom/google/ac/b/c/g;->e:Lcom/google/ac/b/c/m;

    iget-object v0, v0, Lcom/google/ac/b/c/m;->y:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    invoke-static {}, Lcom/google/android/location/copresence/f/b;->b()Lcom/google/ac/b/c/g;

    move-result-object v0

    iget-object v0, v0, Lcom/google/ac/b/c/g;->e:Lcom/google/ac/b/c/m;

    iget-object v0, v0, Lcom/google/ac/b/c/m;->z:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v5

    invoke-static {p1}, Lcom/google/android/location/copresence/debug/f;->a(Landroid/content/Context;)Lcom/google/android/location/copresence/debug/f;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/location/copresence/debug/f;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v6, "token:"

    invoke-direct {v1, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    :goto_0
    invoke-static {}, Lcom/google/android/location/copresence/f/b;->b()Lcom/google/ac/b/c/g;

    move-result-object v0

    iget-object v0, v0, Lcom/google/ac/b/c/g;->d:Lcom/google/ac/b/c/h;

    iget-object v7, v0, Lcom/google/ac/b/c/h;->c:Ljava/lang/String;

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v7}, Lcom/google/android/gms/common/server/q;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;ZZLjava/lang/String;Ljava/lang/String;)V

    .line 38
    return-void

    .line 30
    :cond_0
    sget-object v0, Lcom/google/android/location/copresence/k;->c:Lcom/google/android/location/copresence/l;

    invoke-virtual {v0}, Lcom/google/android/location/copresence/l;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    move-object v6, v0

    goto :goto_0
.end method

.method static synthetic a(Lcom/google/android/location/copresence/l/b;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 22
    iget-object v0, p0, Lcom/google/android/location/copresence/l/b;->a:Landroid/content/Context;

    return-object v0
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/common/server/ClientContext;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 42
    invoke-super {p0, p1}, Lcom/google/android/gms/common/server/q;->a(Lcom/google/android/gms/common/server/ClientContext;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 46
    invoke-virtual {p0}, Lcom/google/android/location/copresence/l/b;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0, p1}, Lcom/google/android/location/copresence/l/b;->b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected final a(Lcom/android/volley/p;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 63
    invoke-virtual {p0, p2}, Lcom/google/android/location/copresence/l/b;->b(Ljava/lang/String;)Lcom/android/volley/z;

    move-result-object v0

    .line 64
    invoke-virtual {p1, v0}, Lcom/android/volley/p;->a(Lcom/android/volley/z;)Lcom/android/volley/p;

    .line 65
    return-void
.end method

.method protected b(Ljava/lang/String;)Lcom/android/volley/z;
    .locals 2

    .prologue
    .line 71
    new-instance v0, Lcom/google/android/location/copresence/l/c;

    const/4 v1, 0x3

    invoke-direct {v0, p0, p1, v1}, Lcom/google/android/location/copresence/l/c;-><init>(Lcom/google/android/location/copresence/l/b;Ljava/lang/String;I)V

    return-object v0
.end method

.method public final e(Lcom/google/android/gms/common/server/ClientContext;)Ljava/util/HashMap;
    .locals 2

    .prologue
    .line 55
    iget-object v0, p0, Lcom/google/android/location/copresence/l/b;->a:Landroid/content/Context;

    const-string v1, "_snip"

    invoke-virtual {p0, v0, p1, v1}, Lcom/google/android/location/copresence/l/b;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)Ljava/util/HashMap;

    move-result-object v0

    return-object v0
.end method
