.class final Lcom/google/android/location/activity/ac;
.super Landroid/hardware/TriggerEventListener;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field private final a:Ljava/lang/ref/WeakReference;

.field private final b:Ljava/lang/ref/WeakReference;


# direct methods
.method private constructor <init>(Lcom/google/android/location/os/bi;Lcom/google/android/location/activity/ab;)V
    .locals 1

    .prologue
    .line 124
    invoke-direct {p0}, Landroid/hardware/TriggerEventListener;-><init>()V

    .line 125
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/google/android/location/activity/ac;->a:Ljava/lang/ref/WeakReference;

    .line 126
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p2}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/google/android/location/activity/ac;->b:Ljava/lang/ref/WeakReference;

    .line 127
    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/location/os/bi;Lcom/google/android/location/activity/ab;B)V
    .locals 0

    .prologue
    .line 120
    invoke-direct {p0, p1, p2}, Lcom/google/android/location/activity/ac;-><init>(Lcom/google/android/location/os/bi;Lcom/google/android/location/activity/ab;)V

    return-void
.end method

.method private static a(Lcom/google/android/location/activity/ab;)V
    .locals 2

    .prologue
    .line 151
    if-eqz p0, :cond_1

    .line 152
    iget-object v1, p0, Lcom/google/android/location/activity/ab;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 153
    :try_start_0
    iget-object v0, p0, Lcom/google/android/location/activity/ab;->b:Lcom/google/android/location/activity/au;

    if-eqz v0, :cond_0

    .line 154
    iget-object v0, p0, Lcom/google/android/location/activity/ab;->b:Lcom/google/android/location/activity/au;

    invoke-interface {v0}, Lcom/google/android/location/activity/au;->l()V

    .line 155
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/location/activity/ab;->b:Lcom/google/android/location/activity/au;

    .line 157
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 159
    :cond_1
    return-void

    .line 157
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final onTrigger(Landroid/hardware/TriggerEvent;)V
    .locals 2

    .prologue
    .line 131
    iget-object v0, p0, Lcom/google/android/location/activity/ac;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/os/bi;

    .line 132
    iget-object v1, p0, Lcom/google/android/location/activity/ac;->b:Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/location/activity/ab;

    .line 133
    if-nez v0, :cond_1

    .line 134
    invoke-static {v1}, Lcom/google/android/location/activity/ac;->a(Lcom/google/android/location/activity/ab;)V

    .line 142
    :cond_0
    :goto_0
    return-void

    .line 137
    :cond_1
    invoke-interface {v0}, Lcom/google/android/location/os/bi;->f()Lcom/google/android/location/j/j;

    move-result-object v0

    invoke-interface {v0, p0}, Lcom/google/android/location/j/j;->a(Ljava/lang/Runnable;)Z

    move-result v0

    .line 138
    if-nez v0, :cond_0

    .line 139
    sget-boolean v0, Lcom/google/android/location/i/a;->d:Z

    if-eqz v0, :cond_0

    const-string v0, "HardSigMotion"

    const-string v1, "onSignificantMotion not called because os is quitting or quit."

    invoke-static {v0, v1}, Lcom/google/android/location/o/a/a;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final run()V
    .locals 1

    .prologue
    .line 147
    iget-object v0, p0, Lcom/google/android/location/activity/ac;->b:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/activity/ab;

    invoke-static {v0}, Lcom/google/android/location/activity/ac;->a(Lcom/google/android/location/activity/ab;)V

    .line 148
    return-void
.end method
