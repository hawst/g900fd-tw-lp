.class public Lcom/google/android/location/settings/ActivityRecognitionPermissionActivity;
.super Landroid/support/v4/app/q;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lcom/google/android/location/settings/v;


# instance fields
.field private a:Ljava/lang/String;

.field private b:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 53
    invoke-direct {p0}, Landroid/support/v4/app/q;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 147
    invoke-static {p1, p2}, Lcom/google/android/gms/auth/login/ay;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/auth/login/ay;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/location/settings/ActivityRecognitionPermissionActivity;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/auth/login/ay;->a(Landroid/support/v4/app/v;Ljava/lang/String;)V

    .line 149
    return-void
.end method

.method public onBackPressed()V
    .locals 2

    .prologue
    .line 140
    const/4 v0, 0x0

    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    invoke-virtual {p0, v0, v1}, Lcom/google/android/location/settings/ActivityRecognitionPermissionActivity;->setResult(ILandroid/content/Intent;)V

    .line 141
    invoke-virtual {p0}, Lcom/google/android/location/settings/ActivityRecognitionPermissionActivity;->finish()V

    .line 142
    invoke-super {p0}, Landroid/support/v4/app/q;->onBackPressed()V

    .line 143
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 3

    .prologue
    .line 118
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    .line 119
    sget v1, Lcom/google/android/gms/j;->cs:I

    if-ne v0, v1, :cond_1

    .line 120
    const/4 v0, 0x0

    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    invoke-virtual {p0, v0, v1}, Lcom/google/android/location/settings/ActivityRecognitionPermissionActivity;->setResult(ILandroid/content/Intent;)V

    .line 121
    invoke-virtual {p0}, Lcom/google/android/location/settings/ActivityRecognitionPermissionActivity;->finish()V

    .line 127
    :cond_0
    :goto_0
    return-void

    .line 122
    :cond_1
    sget v1, Lcom/google/android/gms/j;->d:I

    if-ne v0, v1, :cond_0

    .line 123
    const-string v0, "activity_recognition_permission_whitelist"

    invoke-static {}, Lcom/google/android/location/internal/a;->a()I

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/location/settings/ActivityRecognitionPermissionActivity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/location/settings/ActivityRecognitionPermissionActivity;->b:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 124
    const/4 v0, -0x1

    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    invoke-virtual {p0, v0, v1}, Lcom/google/android/location/settings/ActivityRecognitionPermissionActivity;->setResult(ILandroid/content/Intent;)V

    .line 125
    invoke-virtual {p0}, Lcom/google/android/location/settings/ActivityRecognitionPermissionActivity;->finish()V

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 65
    invoke-super {p0, p1}, Landroid/support/v4/app/q;->onCreate(Landroid/os/Bundle;)V

    .line 67
    invoke-virtual {p0, v7}, Lcom/google/android/location/settings/ActivityRecognitionPermissionActivity;->requestWindowFeature(I)Z

    .line 68
    sget v0, Lcom/google/android/gms/l;->e:I

    invoke-virtual {p0, v0}, Lcom/google/android/location/settings/ActivityRecognitionPermissionActivity;->setContentView(I)V

    .line 70
    invoke-virtual {p0}, Lcom/google/android/location/settings/ActivityRecognitionPermissionActivity;->getCallingPackage()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/settings/ActivityRecognitionPermissionActivity;->b:Ljava/lang/String;

    .line 71
    iget-object v0, p0, Lcom/google/android/location/settings/ActivityRecognitionPermissionActivity;->b:Ljava/lang/String;

    if-nez v0, :cond_1

    .line 74
    invoke-virtual {p0, v6}, Lcom/google/android/location/settings/ActivityRecognitionPermissionActivity;->setResult(I)V

    .line 75
    invoke-virtual {p0}, Lcom/google/android/location/settings/ActivityRecognitionPermissionActivity;->finish()V

    .line 114
    :cond_0
    :goto_0
    return-void

    .line 78
    :cond_1
    sget v0, Lcom/google/android/gms/j;->d:I

    invoke-virtual {p0, v0}, Lcom/google/android/location/settings/ActivityRecognitionPermissionActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 79
    sget v0, Lcom/google/android/gms/j;->cs:I

    invoke-virtual {p0, v0}, Lcom/google/android/location/settings/ActivityRecognitionPermissionActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 80
    sget v0, Lcom/google/android/gms/j;->gm:I

    invoke-virtual {p0, v0}, Lcom/google/android/location/settings/ActivityRecognitionPermissionActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 81
    if-eqz v0, :cond_2

    .line 82
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 85
    :cond_2
    new-instance v0, Lcom/google/android/gms/auth/a/c;

    invoke-direct {v0, p0}, Lcom/google/android/gms/auth/a/c;-><init>(Landroid/content/Context;)V

    .line 86
    iget-object v1, p0, Lcom/google/android/location/settings/ActivityRecognitionPermissionActivity;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/auth/a/c;->d(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 87
    iget-object v2, p0, Lcom/google/android/location/settings/ActivityRecognitionPermissionActivity;->b:Ljava/lang/String;

    invoke-virtual {v0, v2}, Lcom/google/android/gms/auth/a/c;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/settings/ActivityRecognitionPermissionActivity;->a:Ljava/lang/String;

    .line 88
    if-eqz v1, :cond_3

    iget-object v0, p0, Lcom/google/android/location/settings/ActivityRecognitionPermissionActivity;->a:Ljava/lang/String;

    if-nez v0, :cond_5

    .line 89
    :cond_3
    const-string v0, "ActivityRecogPermisAc"

    const/4 v1, 0x5

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 90
    const-string v0, "ActivityRecogPermisAc"

    const-string v1, "Failed to get ApplicationInfo for package: %s"

    new-array v2, v7, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/google/android/location/settings/ActivityRecognitionPermissionActivity;->b:Ljava/lang/String;

    aput-object v3, v2, v6

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 93
    :cond_4
    invoke-virtual {p0, v6}, Lcom/google/android/location/settings/ActivityRecognitionPermissionActivity;->setResult(I)V

    .line 94
    invoke-virtual {p0}, Lcom/google/android/location/settings/ActivityRecognitionPermissionActivity;->finish()V

    goto :goto_0

    .line 97
    :cond_5
    sget v0, Lcom/google/android/gms/j;->aK:I

    invoke-virtual {p0, v0}, Lcom/google/android/location/settings/ActivityRecognitionPermissionActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/google/android/location/settings/ActivityRecognitionPermissionActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/google/android/gms/p;->z:I

    new-array v4, v7, [Ljava/lang/Object;

    iget-object v5, p0, Lcom/google/android/location/settings/ActivityRecognitionPermissionActivity;->a:Ljava/lang/String;

    aput-object v5, v4, v6

    invoke-virtual {v2, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 99
    sget v0, Lcom/google/android/gms/j;->aG:I

    invoke-virtual {p0, v0}, Lcom/google/android/location/settings/ActivityRecognitionPermissionActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 100
    sget v0, Lcom/google/android/gms/j;->gm:I

    invoke-virtual {p0, v0}, Lcom/google/android/location/settings/ActivityRecognitionPermissionActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/google/android/location/settings/ActivityRecognitionPermissionActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/google/android/gms/p;->w:I

    new-array v3, v7, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/google/android/location/settings/ActivityRecognitionPermissionActivity;->a:Ljava/lang/String;

    aput-object v4, v3, v6

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 103
    new-instance v0, Lcom/google/android/gms/common/acl/b;

    sget v1, Lcom/google/android/gms/p;->e:I

    invoke-virtual {p0, v1}, Lcom/google/android/location/settings/ActivityRecognitionPermissionActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    sget v2, Lcom/google/android/gms/p;->d:I

    invoke-virtual {p0, v2}, Lcom/google/android/location/settings/ActivityRecognitionPermissionActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/common/acl/b;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/google/android/gms/common/acl/b;->a()Lcom/google/android/gms/common/acl/ScopeData;

    move-result-object v0

    .line 107
    invoke-virtual {p0}, Lcom/google/android/location/settings/ActivityRecognitionPermissionActivity;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v1

    .line 108
    const-string v2, "activity_permission"

    invoke-virtual {v1, v2}, Landroid/support/v4/app/v;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v2

    if-nez v2, :cond_0

    .line 109
    invoke-virtual {v1}, Landroid/support/v4/app/v;->a()Landroid/support/v4/app/aj;

    move-result-object v1

    .line 110
    sget v2, Lcom/google/android/gms/j;->qI:I

    invoke-static {v0}, Lcom/google/android/location/settings/t;->a(Lcom/google/android/gms/common/acl/ScopeData;)Lcom/google/android/location/settings/t;

    move-result-object v0

    const-string v3, "activity_permission"

    invoke-virtual {v1, v2, v0, v3}, Landroid/support/v4/app/aj;->a(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/aj;

    .line 112
    invoke-virtual {v1}, Landroid/support/v4/app/aj;->a()I

    goto/16 :goto_0
.end method
