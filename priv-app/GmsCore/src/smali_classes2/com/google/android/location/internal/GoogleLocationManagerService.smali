.class public Lcom/google/android/location/internal/GoogleLocationManagerService;
.super Landroid/app/Service;
.source "SourceFile"


# instance fields
.field private a:Lcom/google/android/location/internal/c;

.field private b:Lcom/google/android/location/internal/e;

.field private c:Landroid/content/pm/PackageManager;

.field private d:Landroid/os/Handler;

.field private e:Landroid/content/BroadcastReceiver;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 47
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 203
    return-void
.end method

.method static synthetic a(Lcom/google/android/location/internal/GoogleLocationManagerService;)Lcom/google/android/location/internal/e;
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lcom/google/android/location/internal/GoogleLocationManagerService;->b:Lcom/google/android/location/internal/e;

    return-object v0
.end method

.method private a(Lcom/google/android/gms/common/internal/bg;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 250
    :try_start_0
    const-string v0, "GLMS"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 251
    const-string v0, "GLMS"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Generating binder for: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 253
    :cond_0
    new-instance v0, Lcom/google/android/location/internal/f;

    iget-object v1, p0, Lcom/google/android/location/internal/GoogleLocationManagerService;->b:Lcom/google/android/location/internal/e;

    invoke-direct {v0, v1, p2}, Lcom/google/android/location/internal/f;-><init>(Lcom/google/android/location/internal/e;Ljava/lang/String;)V

    .line 256
    const/4 v1, 0x0

    invoke-virtual {v0}, Lcom/google/android/location/internal/f;->asBinder()Landroid/os/IBinder;

    move-result-object v0

    const/4 v2, 0x0

    invoke-interface {p1, v1, v0, v2}, Lcom/google/android/gms/common/internal/bg;->a(ILandroid/os/IBinder;Landroid/os/Bundle;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 262
    :cond_1
    :goto_0
    return-void

    .line 258
    :catch_0
    move-exception v0

    const-string v0, "GLMS"

    const/4 v1, 0x5

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 259
    const-string v0, "GLMS"

    const-string v1, "client died while brokering service"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method static synthetic a(Lcom/google/android/location/internal/GoogleLocationManagerService;Lcom/google/android/gms/common/internal/bg;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 47
    invoke-direct {p0, p1, p2}, Lcom/google/android/location/internal/GoogleLocationManagerService;->a(Lcom/google/android/gms/common/internal/bg;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic a(Lcom/google/android/location/internal/GoogleLocationManagerService;Lcom/google/android/gms/common/internal/bg;Ljava/lang/String;II)V
    .locals 4

    .prologue
    const/4 v3, 0x3

    const/4 v1, 0x0

    .line 47
    invoke-static {}, Lcom/google/android/gms/common/util/ay;->e()I

    move-result v0

    const/16 v2, 0x8

    if-eq v0, v2, :cond_0

    invoke-static {}, Lcom/google/android/gms/common/util/ay;->e()I

    move-result v0

    const/16 v2, 0xa

    if-ne v0, v2, :cond_2

    :cond_0
    move v0, v1

    :goto_0
    if-eqz v0, :cond_7

    new-instance v0, Landroid/content/Intent;

    const-string v2, "com.google.android.location.settings.ACTIVITY_RECOGNITION_PERMISSION"

    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v2, "com.google.android.gms"

    const-string v3, "com.google.android.location.settings.ActivityRecognitionPermissionActivity"

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const/high16 v2, 0x8000000

    invoke-static {p0, v1, v0, v2}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    const-string v2, "pendingIntent"

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    const/4 v0, 0x6

    const/4 v2, 0x0

    :try_start_0
    invoke-interface {p1, v0, v2, v1}, Lcom/google/android/gms/common/internal/bg;->a(ILandroid/os/IBinder;Landroid/os/Bundle;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_1
    :goto_1
    return-void

    :cond_2
    invoke-virtual {p0}, Lcom/google/android/location/internal/GoogleLocationManagerService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, p3, p4}, Lcom/google/android/location/internal/e;->a(Landroid/content/Context;II)Z

    move-result v0

    if-nez v0, :cond_6

    const-string v0, "GLMS"

    invoke-static {v0, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_3

    const-string v0, "GLMS"

    const-string v2, "App was not granted the activity recognition permission"

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_3
    iget-object v0, p0, Lcom/google/android/location/internal/GoogleLocationManagerService;->c:Landroid/content/pm/PackageManager;

    invoke-static {p2, v0}, Lcom/google/android/location/internal/e;->a(Ljava/lang/String;Landroid/content/pm/PackageManager;)Z

    move-result v0

    if-eqz v0, :cond_5

    const-string v0, "GLMS"

    invoke-static {v0, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_4

    const-string v0, "GLMS"

    const-string v2, "App should show the activity recognition permission dialog."

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_4
    const/4 v0, 0x1

    goto :goto_0

    :cond_5
    const-string v0, "GLMS"

    invoke-static {v0, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_6

    const-string v0, "GLMS"

    const-string v2, "App did not request activity recognition in its manifest"

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_6
    move v0, v1

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v0, "GLMS"

    const/4 v1, 0x5

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "GLMS"

    const-string v1, "client died while brokering service"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    :cond_7
    invoke-direct {p0, p1, p2}, Lcom/google/android/location/internal/GoogleLocationManagerService;->a(Lcom/google/android/gms/common/internal/bg;Ljava/lang/String;)V

    goto :goto_1
.end method

.method static synthetic a(Landroid/os/Bundle;)Z
    .locals 2

    .prologue
    .line 47
    const-string v0, "client_name"

    invoke-virtual {p0, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "activity_recognition"

    const-string v1, "client_name"

    invoke-virtual {p0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic b(Lcom/google/android/location/internal/GoogleLocationManagerService;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lcom/google/android/location/internal/GoogleLocationManagerService;->d:Landroid/os/Handler;

    return-object v0
.end method


# virtual methods
.method protected dump(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    .locals 6

    .prologue
    .line 195
    invoke-super {p0, p1, p2, p3}, Landroid/app/Service;->dump(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V

    .line 196
    iget-object v2, p0, Lcom/google/android/location/internal/GoogleLocationManagerService;->b:Lcom/google/android/location/internal/e;

    const-string v0, "\nGeofencer State:"

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    iget-object v0, v2, Lcom/google/android/location/internal/e;->c:Lcom/google/android/location/geofencer/service/g;

    iget-object v0, v0, Lcom/google/android/location/geofencer/service/g;->a:Lcom/google/android/location/geofencer/service/k;

    invoke-virtual {v0, p2}, Lcom/google/android/location/geofencer/service/k;->b(Ljava/io/PrintWriter;)V

    const-string v0, "\nFused Location Provider State:"

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    iget-object v0, v2, Lcom/google/android/location/internal/e;->b:Lcom/google/android/location/fused/g;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/location/fused/g;->a(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V

    const-string v0, "\nPlaces State:"

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    iget-object v0, v2, Lcom/google/android/location/internal/e;->d:Lcom/google/android/location/places/as;

    if-eqz v0, :cond_6

    iget-object v1, v2, Lcom/google/android/location/internal/e;->d:Lcom/google/android/location/places/as;

    iget-object v0, v1, Lcom/google/android/location/places/as;->h:Lcom/google/android/location/places/an;

    iget-object v3, v0, Lcom/google/android/location/places/an;->c:Ljava/lang/Object;

    monitor-enter v3

    :try_start_0
    iget-object v4, v0, Lcom/google/android/location/places/an;->d:Lcom/google/android/location/places/bx;

    iget-boolean v4, v4, Lcom/google/android/location/places/bx;->a:Z

    if-nez v4, :cond_0

    const-string v0, "PlaceSubscriptionManager not yet initialized from cache"

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_0
    iget-object v0, v1, Lcom/google/android/location/places/as;->i:Lcom/google/android/location/places/r;

    iget-object v3, v0, Lcom/google/android/location/places/r;->c:Ljava/lang/Object;

    monitor-enter v3

    :try_start_1
    iget-object v4, v0, Lcom/google/android/location/places/r;->d:Lcom/google/android/location/places/bx;

    iget-boolean v4, v4, Lcom/google/android/location/places/bx;->a:Z

    if-nez v4, :cond_2

    const-string v0, "NearbyAlertSubscriptionManager not yet initialized from cache"

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    :goto_1
    iget-object v3, v1, Lcom/google/android/location/places/as;->j:Lcom/google/android/location/places/b/a;

    const-string v0, "\nNearbyPlaceFenceManager:"

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    const-string v0, "  Geofences (geofenceId -> placeId):"

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    iget-object v0, v3, Lcom/google/android/location/places/b/a;->c:Lcom/google/k/c/dn;

    invoke-interface {v0}, Lcom/google/k/c/dn;->h()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    const-string v1, "   "

    invoke-virtual {p2, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v5, " -> "

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    goto :goto_2

    :cond_0
    :try_start_2
    const-string v4, "\nActive PlaceSubscriptions:"

    invoke-virtual {p2, v4}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    iget-object v0, v0, Lcom/google/android/location/places/an;->d:Lcom/google/android/location/places/bx;

    invoke-virtual {v0}, Lcom/google/android/location/places/bx;->a()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_3
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/places/PlaceSubscription;

    const-string v5, "  "

    invoke-virtual {p2, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_3

    :catchall_0
    move-exception v0

    monitor-exit v3

    throw v0

    :cond_1
    :try_start_3
    monitor-exit v3
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto/16 :goto_0

    :cond_2
    :try_start_4
    const-string v4, "\nActive NearbyAlertSubscriptions:"

    invoke-virtual {p2, v4}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    iget-object v0, v0, Lcom/google/android/location/places/r;->d:Lcom/google/android/location/places/bx;

    invoke-virtual {v0}, Lcom/google/android/location/places/bx;->a()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_4
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/places/NearbyAlertSubscription;

    const-string v5, "  "

    invoke-virtual {p2, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    goto :goto_4

    :catchall_1
    move-exception v0

    monitor-exit v3

    throw v0

    :cond_3
    :try_start_5
    monitor-exit v3
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    goto/16 :goto_1

    :cond_4
    const-string v0, "  Refresh Geofences (geofenceId):"

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    iget-object v0, v3, Lcom/google/android/location/places/b/a;->b:Lcom/google/k/c/dn;

    invoke-interface {v0}, Lcom/google/k/c/dn;->m()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_5
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const-string v4, "   "

    invoke-virtual {p2, v4}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    goto :goto_5

    :cond_5
    iget-object v0, v3, Lcom/google/android/location/places/b/a;->a:Lcom/google/android/location/places/b/k;

    invoke-virtual {v0, p2}, Lcom/google/android/location/places/b/k;->a(Ljava/io/PrintWriter;)V

    :cond_6
    const-string v0, "\nCopresence State:"

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    iget-object v0, v2, Lcom/google/android/location/internal/e;->e:Lcom/google/android/location/copresence/m/b;

    if-eqz v0, :cond_7

    iget-object v0, v2, Lcom/google/android/location/internal/e;->e:Lcom/google/android/location/copresence/m/b;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/location/copresence/m/b;->dump(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V

    :cond_7
    const-string v0, "\nAudio Modem State:"

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    iget-object v0, v2, Lcom/google/android/location/internal/e;->f:Lcom/google/android/gms/audiomodem/b/a;

    if-eqz v0, :cond_8

    iget-object v0, v2, Lcom/google/android/location/internal/e;->f:Lcom/google/android/gms/audiomodem/b/a;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/gms/audiomodem/b/a;->dump(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V

    .line 197
    :cond_8
    return-void
.end method

.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 3

    .prologue
    const/4 v2, 0x3

    .line 152
    const-string v0, "com.google.android.location.internal.GoogleLocationManagerService.START"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 153
    const-string v0, "GLMS"

    invoke-static {v0, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 154
    const-string v0, "GLMS"

    const-string v1, "Location ServiceBroker created."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 156
    :cond_0
    iget-object v0, p0, Lcom/google/android/location/internal/GoogleLocationManagerService;->b:Lcom/google/android/location/internal/e;

    invoke-virtual {v0, p1}, Lcom/google/android/location/internal/e;->a(Landroid/content/Intent;)V

    .line 157
    iget-object v0, p0, Lcom/google/android/location/internal/GoogleLocationManagerService;->a:Lcom/google/android/location/internal/c;

    invoke-virtual {v0}, Lcom/google/android/location/internal/c;->asBinder()Landroid/os/IBinder;

    move-result-object v0

    .line 166
    :goto_0
    return-object v0

    .line 158
    :cond_1
    const-string v0, "com.google.android.gms.location.places.PlacesApi"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 160
    const-string v0, "GLMS"

    invoke-static {v0, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 161
    const-string v0, "GLMS"

    const-string v1, "Places ServiceBroker created."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 163
    :cond_2
    iget-object v0, p0, Lcom/google/android/location/internal/GoogleLocationManagerService;->b:Lcom/google/android/location/internal/e;

    invoke-virtual {v0, p1}, Lcom/google/android/location/internal/e;->a(Landroid/content/Intent;)V

    .line 164
    iget-object v0, p0, Lcom/google/android/location/internal/GoogleLocationManagerService;->a:Lcom/google/android/location/internal/c;

    invoke-virtual {v0}, Lcom/google/android/location/internal/c;->asBinder()Landroid/os/IBinder;

    move-result-object v0

    goto :goto_0

    .line 166
    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onCreate()V
    .locals 3

    .prologue
    .line 99
    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    .line 100
    invoke-virtual {p0}, Lcom/google/android/location/internal/GoogleLocationManagerService;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/internal/GoogleLocationManagerService;->c:Landroid/content/pm/PackageManager;

    .line 102
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/location/internal/GoogleLocationManagerService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 103
    invoke-virtual {p0, v0}, Lcom/google/android/location/internal/GoogleLocationManagerService;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 104
    new-instance v0, Lcom/google/android/location/internal/c;

    const/4 v1, 0x0

    invoke-direct {v0, p0, p0, v1}, Lcom/google/android/location/internal/c;-><init>(Lcom/google/android/location/internal/GoogleLocationManagerService;Landroid/content/Context;B)V

    iput-object v0, p0, Lcom/google/android/location/internal/GoogleLocationManagerService;->a:Lcom/google/android/location/internal/c;

    .line 105
    new-instance v0, Lcom/google/android/location/internal/e;

    invoke-virtual {p0}, Lcom/google/android/location/internal/GoogleLocationManagerService;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/location/internal/e;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/location/internal/GoogleLocationManagerService;->b:Lcom/google/android/location/internal/e;

    .line 107
    new-instance v0, Landroid/os/HandlerThread;

    const-string v1, "LocationServiceBroker"

    invoke-direct {v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    .line 108
    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    .line 109
    invoke-virtual {v0}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v0

    .line 110
    if-eqz v0, :cond_0

    .line 111
    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1, v0}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v1, p0, Lcom/google/android/location/internal/GoogleLocationManagerService;->d:Landroid/os/Handler;

    .line 126
    :goto_0
    new-instance v0, Lcom/google/android/location/internal/b;

    invoke-direct {v0, p0}, Lcom/google/android/location/internal/b;-><init>(Lcom/google/android/location/internal/GoogleLocationManagerService;)V

    iput-object v0, p0, Lcom/google/android/location/internal/GoogleLocationManagerService;->e:Landroid/content/BroadcastReceiver;

    .line 139
    iget-object v0, p0, Lcom/google/android/location/internal/GoogleLocationManagerService;->e:Landroid/content/BroadcastReceiver;

    new-instance v1, Landroid/content/IntentFilter;

    const-string v2, "android.intent.action.LOCALE_CHANGED"

    invoke-direct {v1, v2}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0, v1}, Lcom/google/android/location/internal/GoogleLocationManagerService;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 140
    return-void

    .line 117
    :cond_0
    const-string v0, "GLMS"

    const/4 v1, 0x5

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 118
    const-string v0, "GLMS"

    const-string v1, "Unable to create looper. Running handler on main thread."

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 120
    :cond_1
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/google/android/location/internal/GoogleLocationManagerService;->d:Landroid/os/Handler;

    goto :goto_0
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 145
    iget-object v0, p0, Lcom/google/android/location/internal/GoogleLocationManagerService;->e:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/google/android/location/internal/GoogleLocationManagerService;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 146
    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    .line 147
    return-void
.end method

.method public onRebind(Landroid/content/Intent;)V
    .locals 2

    .prologue
    .line 172
    const-string v0, "com.google.android.location.internal.GoogleLocationManagerService.START"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 173
    iget-object v0, p0, Lcom/google/android/location/internal/GoogleLocationManagerService;->b:Lcom/google/android/location/internal/e;

    invoke-virtual {v0, p1}, Lcom/google/android/location/internal/e;->a(Landroid/content/Intent;)V

    .line 179
    :cond_0
    :goto_0
    return-void

    .line 176
    :cond_1
    const-string v0, "com.google.android.gms.location.places.PlacesApi"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 177
    iget-object v0, p0, Lcom/google/android/location/internal/GoogleLocationManagerService;->b:Lcom/google/android/location/internal/e;

    invoke-virtual {v0, p1}, Lcom/google/android/location/internal/e;->a(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 75
    if-eqz p1, :cond_2

    .line 76
    invoke-static {p1}, Lcom/google/android/location/b/at;->a(Landroid/content/Intent;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 77
    iget-object v0, p0, Lcom/google/android/location/internal/GoogleLocationManagerService;->b:Lcom/google/android/location/internal/e;

    invoke-static {p1}, Lcom/google/android/location/b/at;->b(Landroid/content/Intent;)I

    move-result v3

    packed-switch v3, :pswitch_data_0

    :pswitch_0
    const-string v0, "GLMSImpl"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Unknown cache type: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 87
    :cond_0
    :goto_0
    const-string v0, "fromDeviceBoot"

    invoke-virtual {p1, v0, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 88
    iget-object v0, p0, Lcom/google/android/location/internal/GoogleLocationManagerService;->b:Lcom/google/android/location/internal/e;

    invoke-virtual {v0}, Lcom/google/android/location/internal/e;->b()Lcom/google/android/location/copresence/m/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/location/copresence/m/b;->a()V

    .line 90
    :cond_1
    const-string v0, "fromGmsCoreInit"

    invoke-virtual {p1, v0, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 91
    iget-object v0, p0, Lcom/google/android/location/internal/GoogleLocationManagerService;->b:Lcom/google/android/location/internal/e;

    const-string v3, "GLMSImpl"

    const-string v4, "onGmsCoreInit"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, v0, Lcom/google/android/location/internal/e;->a:Landroid/content/Context;

    new-instance v3, Landroid/content/Intent;

    const-class v4, Lcom/google/android/location/internal/GoogleLocationManagerService;

    invoke-direct {v3, v0, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    const/high16 v4, 0x8000000

    invoke-static {v0, v2, v3, v4}, Landroid/app/PendingIntent;->getService(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/google/android/location/h/a;->a(Landroid/content/Context;Landroid/app/PendingIntent;)V

    .line 94
    :cond_2
    return v1

    .line 77
    :pswitch_1
    iget-object v0, v0, Lcom/google/android/location/internal/e;->b:Lcom/google/android/location/fused/g;

    invoke-virtual {v0, p1}, Lcom/google/android/location/fused/g;->a(Landroid/content/Intent;)V

    goto :goto_0

    :pswitch_2
    iget-object v3, v0, Lcom/google/android/location/internal/e;->c:Lcom/google/android/location/geofencer/service/g;

    invoke-static {p1}, Lcom/google/android/location/b/at;->a(Landroid/content/Intent;)Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-static {p1}, Lcom/google/android/location/b/at;->b(Landroid/content/Intent;)I

    move-result v0

    const/4 v4, 0x2

    if-ne v0, v4, :cond_3

    move v0, v1

    :goto_1
    invoke-static {v0}, Lcom/google/android/gms/common/internal/bx;->b(Z)V

    const-string v0, "GeofencerHelper"

    const-string v4, "Initializing geofence\'s system cache."

    invoke-static {v0, v4}, Lcom/google/android/location/geofencer/a/a;->c(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, v3, Lcom/google/android/location/geofencer/service/g;->a:Lcom/google/android/location/geofencer/service/k;

    new-instance v3, Landroid/content/Intent;

    invoke-direct {v3, p1}, Landroid/content/Intent;-><init>(Landroid/content/Intent;)V

    invoke-virtual {v0, v3}, Lcom/google/android/location/geofencer/service/k;->a(Landroid/content/Intent;)V

    goto :goto_0

    :cond_3
    move v0, v2

    goto :goto_1

    :pswitch_3
    invoke-virtual {v0}, Lcom/google/android/location/internal/e;->a()Lcom/google/android/location/places/as;

    move-result-object v0

    packed-switch v3, :pswitch_data_1

    :pswitch_4
    const-string v0, "Places"

    const/4 v4, 0x5

    invoke-static {v0, v4}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "Places"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Unknown cahce type: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Lcom/google/android/location/n/aa;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :pswitch_5
    iget-object v0, v0, Lcom/google/android/location/places/as;->i:Lcom/google/android/location/places/r;

    invoke-virtual {v0, p1}, Lcom/google/android/location/places/r;->a(Landroid/content/Intent;)V

    goto/16 :goto_0

    :pswitch_6
    iget-object v0, v0, Lcom/google/android/location/places/as;->h:Lcom/google/android/location/places/an;

    invoke-virtual {v0, p1}, Lcom/google/android/location/places/an;->a(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 78
    :cond_4
    invoke-static {p1}, Lcom/google/android/location/copresence/GcmBroadcastReceiver;->b(Landroid/content/Intent;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 80
    :try_start_0
    iget-object v0, p0, Lcom/google/android/location/internal/GoogleLocationManagerService;->b:Lcom/google/android/location/internal/e;

    invoke-virtual {v0}, Lcom/google/android/location/internal/e;->b()Lcom/google/android/location/copresence/m/b;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/location/copresence/m/b;->a(Landroid/content/Intent;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 83
    invoke-static {p1}, Lcom/google/android/location/copresence/m/b;->b(Landroid/content/Intent;)V

    goto/16 :goto_0

    :catchall_0
    move-exception v0

    invoke-static {p1}, Lcom/google/android/location/copresence/m/b;->b(Landroid/content/Intent;)V

    throw v0

    .line 77
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_3
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x3
        :pswitch_6
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public onUnbind(Landroid/content/Intent;)Z
    .locals 2

    .prologue
    .line 183
    const-string v0, "com.google.android.location.internal.GoogleLocationManagerService.START"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 184
    iget-object v0, p0, Lcom/google/android/location/internal/GoogleLocationManagerService;->b:Lcom/google/android/location/internal/e;

    invoke-virtual {v0, p1}, Lcom/google/android/location/internal/e;->b(Landroid/content/Intent;)V

    .line 190
    :cond_0
    :goto_0
    const/4 v0, 0x1

    return v0

    .line 187
    :cond_1
    const-string v0, "com.google.android.gms.location.places.PlacesApi"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 188
    iget-object v0, p0, Lcom/google/android/location/internal/GoogleLocationManagerService;->b:Lcom/google/android/location/internal/e;

    invoke-virtual {v0, p1}, Lcom/google/android/location/internal/e;->b(Landroid/content/Intent;)V

    goto :goto_0
.end method
