.class public final Lcom/google/android/location/activity/a/a/c;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method public static a([D)D
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 115
    array-length v0, p0

    if-nez v0, :cond_0

    .line 116
    const-wide/16 v0, 0x0

    .line 128
    :goto_0
    return-wide v0

    .line 118
    :cond_0
    aget-wide v4, p0, v1

    .line 119
    aget-wide v2, p0, v1

    .line 120
    const/4 v0, 0x1

    :goto_1
    array-length v1, p0

    if-ge v0, v1, :cond_3

    .line 121
    aget-wide v6, p0, v0

    cmpl-double v1, v6, v4

    if-lez v1, :cond_1

    .line 122
    aget-wide v4, p0, v0

    .line 124
    :cond_1
    aget-wide v6, p0, v0

    cmpg-double v1, v6, v2

    if-gez v1, :cond_2

    .line 125
    aget-wide v2, p0, v0

    .line 120
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 128
    :cond_3
    sub-double v0, v4, v2

    goto :goto_0
.end method

.method public static a([DD)D
    .locals 7

    .prologue
    .line 137
    const-wide/16 v2, 0x0

    .line 138
    array-length v1, p0

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    aget-wide v4, p0, v0

    .line 139
    sub-double/2addr v4, p1

    invoke-static {v4, v5}, Ljava/lang/Math;->abs(D)D

    move-result-wide v4

    add-double/2addr v2, v4

    .line 138
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 141
    :cond_0
    array-length v0, p0

    int-to-double v0, v0

    div-double v0, v2, v0

    return-wide v0
.end method

.method public static b([D)F
    .locals 10

    .prologue
    const/4 v1, 0x1

    const/4 v3, 0x0

    .line 149
    array-length v0, p0

    const/4 v2, 0x2

    if-ge v0, v2, :cond_0

    .line 150
    const/4 v0, 0x0

    .line 158
    :goto_0
    return v0

    :cond_0
    move v0, v1

    move v2, v3

    .line 153
    :goto_1
    array-length v4, p0

    add-int/lit8 v4, v4, -0x1

    if-ge v0, v4, :cond_4

    .line 154
    add-int/lit8 v4, v0, -0x1

    aget-wide v4, p0, v4

    aget-wide v6, p0, v0

    cmpg-double v4, v4, v6

    if-gez v4, :cond_2

    move v4, v1

    :goto_2
    aget-wide v6, p0, v0

    add-int/lit8 v5, v0, 0x1

    aget-wide v8, p0, v5

    cmpg-double v5, v6, v8

    if-gez v5, :cond_3

    move v5, v1

    :goto_3
    if-eq v4, v5, :cond_1

    .line 155
    add-int/lit8 v2, v2, 0x1

    .line 153
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_2
    move v4, v3

    .line 154
    goto :goto_2

    :cond_3
    move v5, v3

    goto :goto_3

    .line 158
    :cond_4
    int-to-float v0, v2

    array-length v1, p0

    add-int/lit8 v1, v1, -0x2

    int-to-float v1, v1

    div-float/2addr v0, v1

    goto :goto_0
.end method

.method public static b([DD)F
    .locals 9

    .prologue
    const/4 v4, 0x1

    const/4 v1, 0x0

    .line 168
    array-length v0, p0

    if-nez v0, :cond_0

    .line 169
    const/4 v0, 0x0

    .line 177
    :goto_0
    return v0

    :cond_0
    move v0, v1

    move v2, v1

    .line 172
    :goto_1
    array-length v3, p0

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_4

    .line 173
    aget-wide v6, p0, v0

    cmpl-double v3, v6, p1

    if-ltz v3, :cond_2

    move v3, v4

    :goto_2
    add-int/lit8 v5, v0, 0x1

    aget-wide v6, p0, v5

    cmpl-double v5, v6, p1

    if-ltz v5, :cond_3

    move v5, v4

    :goto_3
    if-eq v3, v5, :cond_1

    .line 174
    add-int/lit8 v2, v2, 0x1

    .line 172
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_2
    move v3, v1

    .line 173
    goto :goto_2

    :cond_3
    move v5, v1

    goto :goto_3

    .line 177
    :cond_4
    int-to-float v0, v2

    array-length v1, p0

    add-int/lit8 v1, v1, -0x1

    int-to-float v1, v1

    div-float/2addr v0, v1

    goto :goto_0
.end method
