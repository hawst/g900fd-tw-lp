.class public interface abstract Lcom/google/android/location/copresence/o/t;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static final a:Ljava/lang/String;

.field public static final b:Ljava/lang/String;

.field public static final c:[Ljava/lang/Integer;

.field public static final d:[Ljava/lang/String;

.field public static final e:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 81
    const-string v0, "packages"

    const-string v1, "_id"

    invoke-static {v0, v1}, Lcom/google/android/location/copresence/o/r;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/location/copresence/o/t;->a:Ljava/lang/String;

    .line 84
    const-string v0, "accounts"

    const-string v1, "_id"

    invoke-static {v0, v1}, Lcom/google/android/location/copresence/o/r;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/location/copresence/o/t;->b:Ljava/lang/String;

    .line 121
    new-array v0, v6, [Ljava/lang/Integer;

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v3

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v4

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v5

    sput-object v0, Lcom/google/android/location/copresence/o/t;->c:[Ljava/lang/Integer;

    .line 142
    const/16 v0, 0xb

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v3

    const-string v1, "op_code"

    aput-object v1, v0, v4

    const-string v1, "write_time_millis"

    aput-object v1, v0, v5

    const-string v1, "package_id"

    aput-object v1, v0, v6

    const-string v1, "account_id"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "client_id"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "expiration_time"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "network_status"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "network_last_update_millis"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "deletable"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "proto"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/location/copresence/o/t;->d:[Ljava/lang/String;

    .line 175
    const/16 v0, 0xc

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v3

    const-string v1, "op_code"

    aput-object v1, v0, v4

    const-string v1, "write_time_millis"

    aput-object v1, v0, v5

    const-string v1, "package_id"

    aput-object v1, v0, v6

    const-string v1, "account_id"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "client_id"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "publication_id"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "subscription_id"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "network_status"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "network_last_update_millis"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "deletable"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "proto"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/location/copresence/o/t;->e:[Ljava/lang/String;

    return-void
.end method
