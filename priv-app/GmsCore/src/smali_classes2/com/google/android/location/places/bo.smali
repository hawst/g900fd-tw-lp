.class public final Lcom/google/android/location/places/bo;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method public static a(Lcom/google/android/gms/maps/model/LatLng;)Lcom/google/android/location/l/a/ag;
    .locals 6

    .prologue
    const-wide v4, 0x416312d000000000L    # 1.0E7

    .line 322
    new-instance v0, Lcom/google/android/location/l/a/ag;

    invoke-direct {v0}, Lcom/google/android/location/l/a/ag;-><init>()V

    .line 323
    iget-wide v2, p0, Lcom/google/android/gms/maps/model/LatLng;->a:D

    mul-double/2addr v2, v4

    double-to-int v1, v2

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, v0, Lcom/google/android/location/l/a/ag;->a:Ljava/lang/Integer;

    .line 324
    iget-wide v2, p0, Lcom/google/android/gms/maps/model/LatLng;->b:D

    mul-double/2addr v2, v4

    double-to-int v1, v2

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, v0, Lcom/google/android/location/l/a/ag;->b:Ljava/lang/Integer;

    .line 325
    return-object v0
.end method

.method public static a(Lcom/google/android/gms/location/places/internal/PlacesParams;Lcom/google/android/location/l/a/bi;)Lcom/google/android/location/l/a/ak;
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 249
    new-instance v0, Lcom/google/android/location/l/a/ak;

    invoke-direct {v0}, Lcom/google/android/location/l/a/ak;-><init>()V

    new-instance v1, Lcom/google/android/location/l/a/bu;

    invoke-direct {v1}, Lcom/google/android/location/l/a/bu;-><init>()V

    iput-object v1, v0, Lcom/google/android/location/l/a/ak;->a:Lcom/google/android/location/l/a/bu;

    iget-object v1, p0, Lcom/google/android/gms/location/places/internal/PlacesParams;->c:Ljava/lang/String;

    if-eqz v1, :cond_0

    new-instance v1, Lcom/google/android/location/l/a/d;

    invoke-direct {v1}, Lcom/google/android/location/l/a/d;-><init>()V

    iget-object v2, p0, Lcom/google/android/gms/location/places/internal/PlacesParams;->c:Ljava/lang/String;

    iput-object v2, v1, Lcom/google/android/location/l/a/d;->a:Ljava/lang/String;

    new-array v2, v4, [Lcom/google/android/location/l/a/d;

    aput-object v1, v2, v3

    iput-object v2, v0, Lcom/google/android/location/l/a/ak;->b:[Lcom/google/android/location/l/a/d;

    .line 251
    :cond_0
    new-instance v1, Lcom/google/android/location/l/a/bm;

    invoke-direct {v1}, Lcom/google/android/location/l/a/bm;-><init>()V

    .line 252
    new-array v2, v4, [Lcom/google/android/location/l/a/bi;

    aput-object p1, v2, v3

    iput-object v2, v1, Lcom/google/android/location/l/a/bm;->a:[Lcom/google/android/location/l/a/bi;

    .line 254
    new-instance v2, Lcom/google/android/location/l/a/al;

    invoke-direct {v2}, Lcom/google/android/location/l/a/al;-><init>()V

    .line 255
    iput-object v1, v2, Lcom/google/android/location/l/a/al;->n:Lcom/google/android/location/l/a/bm;

    .line 257
    new-array v1, v4, [Lcom/google/android/location/l/a/al;

    aput-object v2, v1, v3

    iput-object v1, v0, Lcom/google/android/location/l/a/ak;->d:[Lcom/google/android/location/l/a/al;

    .line 259
    return-object v0
.end method

.method public static a(Landroid/content/Context;Lcom/google/android/gms/location/places/PlaceFilter;Lcom/google/android/gms/location/places/internal/PlacesParams;)Lcom/google/android/location/l/a/bi;
    .locals 9

    .prologue
    const/4 v4, 0x1

    const/4 v2, 0x0

    .line 306
    new-instance v6, Lcom/google/android/location/l/a/bi;

    invoke-direct {v6}, Lcom/google/android/location/l/a/bi;-><init>()V

    .line 307
    iget-object v0, p2, Lcom/google/android/gms/location/places/internal/PlacesParams;->d:Ljava/lang/String;

    iput-object v0, v6, Lcom/google/android/location/l/a/bi;->a:Ljava/lang/String;

    .line 309
    if-nez p1, :cond_2

    const/4 v0, 0x0

    .line 310
    :goto_0
    if-eqz v0, :cond_0

    .line 311
    iput-object v0, v6, Lcom/google/android/location/l/a/bi;->b:Lcom/google/android/location/l/a/az;

    .line 314
    :cond_0
    sget-object v0, Lcom/google/android/location/x;->m:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 315
    iget-object v0, p2, Lcom/google/android/gms/location/places/internal/PlacesParams;->c:Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    iget-object v1, p2, Lcom/google/android/gms/location/places/internal/PlacesParams;->c:Ljava/lang/String;

    const/16 v3, 0x80

    invoke-virtual {v0, v1, v3}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object v0

    iget-object v0, v0, Landroid/content/pm/ApplicationInfo;->metaData:Landroid/os/Bundle;

    const-string v1, "com.google.android.geo.API_KEY"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iget-object v0, p2, Lcom/google/android/gms/location/places/internal/PlacesParams;->c:Ljava/lang/String;

    invoke-static {p0, v0}, Lcom/google/android/gms/common/util/e;->d(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v3

    if-eqz v1, :cond_8

    move v0, v4

    :goto_1
    const-string v5, "API key not found.  Check that <meta-data android:name=\"%s\" android:value=\"your API key\"/> is in the <application> element of AndroidManifest.xml"

    new-array v7, v4, [Ljava/lang/Object;

    const-string v8, "com.google.android.geo.API_KEY"

    aput-object v8, v7, v2

    invoke-static {v0, v5, v7}, Lcom/google/android/gms/common/internal/bx;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    if-eqz v3, :cond_9

    :goto_2
    const-string v0, "Package is not signed."

    invoke-static {v4, v0}, Lcom/google/android/gms/common/internal/bx;->a(ZLjava/lang/Object;)V

    new-instance v0, Lcom/google/android/location/l/a/d;

    invoke-direct {v0}, Lcom/google/android/location/l/a/d;-><init>()V

    iget-object v2, p2, Lcom/google/android/gms/location/places/internal/PlacesParams;->c:Ljava/lang/String;

    iput-object v2, v0, Lcom/google/android/location/l/a/d;->a:Ljava/lang/String;

    iput-object v1, v0, Lcom/google/android/location/l/a/d;->b:Ljava/lang/String;

    iput-object v3, v0, Lcom/google/android/location/l/a/d;->g:Ljava/lang/String;

    iput-object v0, v6, Lcom/google/android/location/l/a/bi;->l:Lcom/google/android/location/l/a/d;

    .line 318
    :cond_1
    return-object v6

    .line 309
    :cond_2
    new-instance v5, Lcom/google/android/location/l/a/az;

    invoke-direct {v5}, Lcom/google/android/location/l/a/az;-><init>()V

    invoke-virtual {p1}, Lcom/google/android/gms/location/places/PlaceFilter;->c()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3

    invoke-virtual {p1}, Lcom/google/android/gms/location/places/PlaceFilter;->c()Ljava/util/Set;

    move-result-object v0

    sget-object v1, Lcom/google/android/gms/location/places/p;->b:Ljava/util/Set;

    invoke-interface {v0, v1}, Ljava/util/Set;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    new-array v0, v4, [Ljava/lang/String;

    const-string v1, "coarse"

    aput-object v1, v0, v2

    iput-object v0, v5, Lcom/google/android/location/l/a/az;->a:[Ljava/lang/String;

    :cond_3
    invoke-virtual {p1}, Lcom/google/android/gms/location/places/PlaceFilter;->d()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_4

    invoke-virtual {p1}, Lcom/google/android/gms/location/places/PlaceFilter;->d()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v5, Lcom/google/android/location/l/a/az;->b:Ljava/lang/String;

    :cond_4
    invoke-virtual {p1}, Lcom/google/android/gms/location/places/PlaceFilter;->e()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, v5, Lcom/google/android/location/l/a/az;->c:Ljava/lang/Boolean;

    :cond_5
    move-object v0, v5

    goto/16 :goto_0

    :cond_6
    invoke-virtual {p1}, Lcom/google/android/gms/location/places/PlaceFilter;->c()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v0

    new-array v0, v0, [Ljava/lang/String;

    iput-object v0, v5, Lcom/google/android/location/l/a/az;->a:[Ljava/lang/String;

    invoke-virtual {p1}, Lcom/google/android/gms/location/places/PlaceFilter;->c()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v7

    move v1, v2

    :goto_3
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/location/places/PlaceType;

    iget-object v8, v5, Lcom/google/android/location/l/a/az;->a:[Ljava/lang/String;

    add-int/lit8 v3, v1, 0x1

    invoke-virtual {v0}, Lcom/google/android/gms/location/places/PlaceType;->a()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v8, v1

    move v1, v3

    goto :goto_3

    .line 315
    :catch_0
    move-exception v0

    const-string v1, "Places"

    const/4 v2, 0x6

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_7

    const-string v1, "Places"

    const-string v2, "Package name not found"

    invoke-static {v1, v2, v0}, Lcom/google/android/location/n/aa;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    :cond_7
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "Package name not found"

    invoke-direct {v1, v2, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    :cond_8
    move v0, v2

    goto/16 :goto_1

    :cond_9
    move v4, v2

    goto/16 :goto_2
.end method

.method public static a(Landroid/content/Context;Lcom/google/android/gms/maps/model/LatLngBounds;Ljava/lang/String;Lcom/google/android/gms/location/places/AutocompleteFilter;Lcom/google/android/gms/location/places/internal/PlacesParams;)Lcom/google/android/location/l/a/bi;
    .locals 4

    .prologue
    .line 97
    const/4 v0, 0x0

    invoke-static {p0, v0, p4}, Lcom/google/android/location/places/bo;->a(Landroid/content/Context;Lcom/google/android/gms/location/places/PlaceFilter;Lcom/google/android/gms/location/places/internal/PlacesParams;)Lcom/google/android/location/l/a/bi;

    move-result-object v0

    .line 100
    new-instance v1, Lcom/google/android/location/l/a/bs;

    invoke-direct {v1}, Lcom/google/android/location/l/a/bs;-><init>()V

    .line 102
    if-eqz p1, :cond_0

    .line 103
    new-instance v2, Lcom/google/android/location/l/a/ca;

    invoke-direct {v2}, Lcom/google/android/location/l/a/ca;-><init>()V

    .line 104
    iget-object v3, p1, Lcom/google/android/gms/maps/model/LatLngBounds;->a:Lcom/google/android/gms/maps/model/LatLng;

    invoke-static {v3}, Lcom/google/android/location/places/bo;->a(Lcom/google/android/gms/maps/model/LatLng;)Lcom/google/android/location/l/a/ag;

    move-result-object v3

    iput-object v3, v2, Lcom/google/android/location/l/a/ca;->a:Lcom/google/android/location/l/a/ag;

    .line 105
    iget-object v3, p1, Lcom/google/android/gms/maps/model/LatLngBounds;->b:Lcom/google/android/gms/maps/model/LatLng;

    invoke-static {v3}, Lcom/google/android/location/places/bo;->a(Lcom/google/android/gms/maps/model/LatLng;)Lcom/google/android/location/l/a/ag;

    move-result-object v3

    iput-object v3, v2, Lcom/google/android/location/l/a/ca;->b:Lcom/google/android/location/l/a/ag;

    .line 106
    iput-object v2, v1, Lcom/google/android/location/l/a/bs;->b:Lcom/google/android/location/l/a/ca;

    .line 109
    :cond_0
    invoke-static {p3}, Lcom/google/android/location/places/a;->a(Lcom/google/android/gms/location/places/AutocompleteFilter;)Lcom/google/android/location/l/a/av;

    move-result-object v2

    iput-object v2, v1, Lcom/google/android/location/l/a/bs;->c:Lcom/google/android/location/l/a/av;

    .line 111
    iput-object p2, v1, Lcom/google/android/location/l/a/bs;->a:Ljava/lang/String;

    .line 113
    iput-object v1, v0, Lcom/google/android/location/l/a/bi;->j:Lcom/google/android/location/l/a/bs;

    .line 115
    return-object v0
.end method
