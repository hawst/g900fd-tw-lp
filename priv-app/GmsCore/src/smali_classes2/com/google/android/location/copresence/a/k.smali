.class final Lcom/google/android/location/copresence/a/k;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/android/volley/w;
.implements Lcom/android/volley/x;


# instance fields
.field final synthetic a:Lcom/google/android/location/copresence/a/i;

.field private final b:Ljava/util/concurrent/CountDownLatch;

.field private final c:Lcom/google/android/location/copresence/a/j;

.field private final d:Lcom/android/volley/s;

.field private e:Ljava/lang/String;


# direct methods
.method private constructor <init>(Lcom/google/android/location/copresence/a/i;Landroid/accounts/Account;)V
    .locals 2

    .prologue
    .line 213
    iput-object p1, p0, Lcom/google/android/location/copresence/a/k;->a:Lcom/google/android/location/copresence/a/i;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 206
    new-instance v0, Ljava/util/concurrent/CountDownLatch;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/location/copresence/a/k;->b:Ljava/util/concurrent/CountDownLatch;

    .line 214
    new-instance v0, Lcom/google/android/location/copresence/a/j;

    invoke-direct {v0, p1, p2, p0, p0}, Lcom/google/android/location/copresence/a/j;-><init>(Lcom/google/android/location/copresence/a/i;Landroid/accounts/Account;Lcom/android/volley/x;Lcom/android/volley/w;)V

    iput-object v0, p0, Lcom/google/android/location/copresence/a/k;->c:Lcom/google/android/location/copresence/a/j;

    .line 215
    invoke-static {}, Lcom/google/android/gms/common/app/GmsApplication;->b()Lcom/google/android/gms/common/app/GmsApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/common/app/GmsApplication;->c()Lcom/android/volley/s;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/copresence/a/k;->d:Lcom/android/volley/s;

    .line 216
    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/location/copresence/a/i;Landroid/accounts/Account;B)V
    .locals 0

    .prologue
    .line 204
    invoke-direct {p0, p1, p2}, Lcom/google/android/location/copresence/a/k;-><init>(Lcom/google/android/location/copresence/a/i;Landroid/accounts/Account;)V

    return-void
.end method

.method private static a(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 263
    :try_start_0
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0, p0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 264
    const-string v1, "id"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 270
    :goto_0
    return-object v0

    .line 266
    :catch_0
    move-exception v0

    .line 267
    const/4 v1, 0x6

    invoke-static {v1}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 268
    const-string v1, "ObfuscatedGaiaIdLookup: An error occurred parsing the UserInfo."

    invoke-static {v1, v0}, Lcom/google/android/location/copresence/ag;->a(Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 270
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 2

    .prologue
    .line 247
    iget-object v0, p0, Lcom/google/android/location/copresence/a/k;->d:Lcom/android/volley/s;

    iget-object v1, p0, Lcom/google/android/location/copresence/a/k;->c:Lcom/google/android/location/copresence/a/j;

    invoke-virtual {v0, v1}, Lcom/android/volley/s;->a(Lcom/android/volley/p;)Lcom/android/volley/p;

    .line 250
    :try_start_0
    iget-object v0, p0, Lcom/google/android/location/copresence/a/k;->b:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->await()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 258
    iget-object v0, p0, Lcom/google/android/location/copresence/a/k;->e:Ljava/lang/String;

    :goto_0
    return-object v0

    .line 251
    :catch_0
    move-exception v0

    .line 252
    const/4 v1, 0x6

    invoke-static {v1}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 253
    const-string v1, "ObfuscatedGaiaIdLookup: Await for StatusInfo was interrupted."

    invoke-static {v1, v0}, Lcom/google/android/location/copresence/ag;->a(Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 255
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Lcom/android/volley/ac;)V
    .locals 3

    .prologue
    .line 226
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 227
    const-string v1, "An error occurred fetching an ObfuscatedGaiaId. "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 229
    iget-object v1, p1, Lcom/android/volley/ac;->a:Lcom/android/volley/m;

    .line 230
    if-eqz v1, :cond_0

    .line 231
    const-string v2, "StatusCode: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 232
    iget v1, v1, Lcom/android/volley/m;->a:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 233
    const-string v1, ". "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 236
    :cond_0
    invoke-virtual {p1}, Lcom/android/volley/ac;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 237
    const/4 v1, 0x6

    invoke-static {v1}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 238
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "ObfuscatedGaiaIdLookup: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->e(Ljava/lang/String;)I

    .line 241
    :cond_1
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/location/copresence/a/k;->e:Ljava/lang/String;

    .line 242
    iget-object v0, p0, Lcom/google/android/location/copresence/a/k;->b:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    .line 243
    return-void
.end method

.method public final synthetic a(Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 204
    check-cast p1, Ljava/lang/String;

    invoke-static {p1}, Lcom/google/android/location/copresence/a/k;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/copresence/a/k;->e:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/location/copresence/a/k;->b:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    return-void
.end method
