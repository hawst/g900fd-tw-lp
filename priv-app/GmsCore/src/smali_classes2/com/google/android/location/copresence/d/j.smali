.class public final Lcom/google/android/location/copresence/d/j;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field static final a:Ljava/util/UUID;

.field private static c:Lcom/google/android/location/copresence/d/j;


# instance fields
.field final b:Landroid/content/BroadcastReceiver;

.field private final d:Lcom/google/android/location/copresence/d/e;

.field private final e:Lcom/google/android/location/copresence/d/n;

.field private final f:Landroid/content/Context;

.field private final g:Landroid/os/Handler;

.field private final h:Lcom/google/android/location/copresence/w;

.field private final i:Lcom/google/android/location/copresence/w;

.field private final j:Lcom/google/android/location/copresence/z;

.field private final k:Lcom/google/android/location/copresence/z;

.field private final l:Lcom/google/android/location/copresence/k/a;

.field private m:Z

.field private n:Z

.field private final o:Lcom/google/android/location/copresence/q/ae;

.field private final p:Lcom/google/android/location/copresence/k/c;

.field private q:Z

.field private r:Lcom/google/android/location/copresence/d/ac;

.field private s:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 52
    const-string v0, "0000FEF3-0000-1000-8000-00805F9B34FB"

    invoke-static {v0}, Ljava/util/UUID;->fromString(Ljava/lang/String;)Ljava/util/UUID;

    move-result-object v0

    sput-object v0, Lcom/google/android/location/copresence/d/j;->a:Ljava/util/UUID;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 165
    invoke-static {p1}, Lcom/google/android/location/copresence/ap;->a(Landroid/content/Context;)Lcom/google/android/location/copresence/ap;

    move-result-object v0

    new-instance v1, Lcom/google/android/location/copresence/d/e;

    invoke-direct {v1, p1}, Lcom/google/android/location/copresence/d/e;-><init>(Landroid/content/Context;)V

    invoke-direct {p0, p1, v0, v1}, Lcom/google/android/location/copresence/d/j;-><init>(Landroid/content/Context;Lcom/google/android/location/copresence/ap;Lcom/google/android/location/copresence/d/e;)V

    .line 166
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Lcom/google/android/location/copresence/ap;Lcom/google/android/location/copresence/d/e;)V
    .locals 10

    .prologue
    const/4 v8, 0x1

    const/4 v9, 0x0

    .line 169
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 69
    new-instance v0, Lcom/google/android/location/copresence/d/k;

    invoke-direct {v0, p0}, Lcom/google/android/location/copresence/d/k;-><init>(Lcom/google/android/location/copresence/d/j;)V

    iput-object v0, p0, Lcom/google/android/location/copresence/d/j;->p:Lcom/google/android/location/copresence/k/c;

    .line 135
    new-instance v0, Lcom/google/android/location/copresence/d/i;

    new-instance v1, Lcom/google/android/location/copresence/d/l;

    invoke-direct {v1, p0}, Lcom/google/android/location/copresence/d/l;-><init>(Lcom/google/android/location/copresence/d/j;)V

    invoke-direct {v0, v1}, Lcom/google/android/location/copresence/d/i;-><init>(Landroid/content/BroadcastReceiver;)V

    iput-object v0, p0, Lcom/google/android/location/copresence/d/j;->b:Landroid/content/BroadcastReceiver;

    .line 170
    const/4 v0, 0x3

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 171
    const-string v0, "BluetoothMedium: created!"

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->b(Ljava/lang/String;)I

    .line 173
    :cond_0
    iput-object p3, p0, Lcom/google/android/location/copresence/d/j;->d:Lcom/google/android/location/copresence/d/e;

    .line 174
    iput-object p1, p0, Lcom/google/android/location/copresence/d/j;->f:Landroid/content/Context;

    .line 175
    invoke-virtual {p2}, Lcom/google/android/location/copresence/ap;->d()Landroid/os/Handler;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/copresence/d/j;->g:Landroid/os/Handler;

    .line 176
    new-instance v0, Lcom/google/android/location/copresence/d/n;

    iget-object v1, p0, Lcom/google/android/location/copresence/d/j;->d:Lcom/google/android/location/copresence/d/e;

    const-string v2, "copresence_user_bluetooth_adapter_state"

    invoke-virtual {p1, v2, v9}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/google/android/location/copresence/d/n;-><init>(Lcom/google/android/location/copresence/d/e;Landroid/content/SharedPreferences;)V

    iput-object v0, p0, Lcom/google/android/location/copresence/d/j;->e:Lcom/google/android/location/copresence/d/n;

    .line 178
    iput-boolean v9, p0, Lcom/google/android/location/copresence/d/j;->q:Z

    .line 179
    new-instance v3, Lcom/google/android/location/copresence/k/e;

    iget-object v0, p0, Lcom/google/android/location/copresence/d/j;->f:Landroid/content/Context;

    const-string v1, "bluetooth"

    iget-object v2, p0, Lcom/google/android/location/copresence/d/j;->p:Lcom/google/android/location/copresence/k/c;

    invoke-direct {v3, v0, v1, v2}, Lcom/google/android/location/copresence/k/e;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/location/copresence/k/c;)V

    .line 181
    iput-boolean v9, p0, Lcom/google/android/location/copresence/d/j;->m:Z

    .line 182
    iput-boolean v9, p0, Lcom/google/android/location/copresence/d/j;->n:Z

    .line 183
    new-instance v1, Lcom/google/android/location/copresence/k/a;

    iget-object v2, p0, Lcom/google/android/location/copresence/d/j;->g:Landroid/os/Handler;

    invoke-static {}, Lcom/google/android/location/copresence/f/b;->b()Lcom/google/ac/b/c/g;

    move-result-object v0

    iget-object v0, v0, Lcom/google/ac/b/c/g;->h:Lcom/google/ac/b/c/e;

    iget-object v0, v0, Lcom/google/ac/b/c/e;->l:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-static {}, Lcom/google/android/location/copresence/f/b;->b()Lcom/google/ac/b/c/g;

    move-result-object v0

    iget-object v0, v0, Lcom/google/ac/b/c/g;->h:Lcom/google/ac/b/c/e;

    iget-object v0, v0, Lcom/google/ac/b/c/e;->k:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    invoke-direct/range {v1 .. v7}, Lcom/google/android/location/copresence/k/a;-><init>(Landroid/os/Handler;Lcom/google/android/location/copresence/k/c;JJ)V

    iput-object v1, p0, Lcom/google/android/location/copresence/d/j;->l:Lcom/google/android/location/copresence/k/a;

    .line 187
    new-instance v0, Lcom/google/android/location/copresence/q/ae;

    invoke-direct {v0}, Lcom/google/android/location/copresence/q/ae;-><init>()V

    iput-object v0, p0, Lcom/google/android/location/copresence/d/j;->o:Lcom/google/android/location/copresence/q/ae;

    .line 188
    iget-object v0, p3, Lcom/google/android/location/copresence/d/e;->a:Landroid/bluetooth/BluetoothAdapter;

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    const-string v1, "android.hardware.bluetooth"

    invoke-virtual {v0, v1}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    move v0, v8

    :goto_0
    if-eqz v0, :cond_2

    .line 189
    new-instance v0, Lcom/google/android/location/copresence/d/m;

    invoke-direct {v0, p0}, Lcom/google/android/location/copresence/d/m;-><init>(Lcom/google/android/location/copresence/d/j;)V

    iput-object v0, p0, Lcom/google/android/location/copresence/d/j;->h:Lcom/google/android/location/copresence/w;

    .line 190
    new-instance v0, Lcom/google/android/location/copresence/d/g;

    invoke-direct {v0, p0}, Lcom/google/android/location/copresence/d/g;-><init>(Lcom/google/android/location/copresence/d/j;)V

    iput-object v0, p0, Lcom/google/android/location/copresence/d/j;->j:Lcom/google/android/location/copresence/z;

    .line 195
    :goto_1
    iget-object v0, p3, Lcom/google/android/location/copresence/d/e;->b:Lcom/google/android/location/copresence/d/a/l;

    iget v0, v0, Lcom/google/android/location/copresence/d/a/l;->d:I

    if-eqz v0, :cond_3

    move v0, v8

    :goto_2
    if-eqz v0, :cond_4

    .line 196
    new-instance v0, Lcom/google/android/location/copresence/d/b;

    invoke-direct {v0, p0}, Lcom/google/android/location/copresence/d/b;-><init>(Lcom/google/android/location/copresence/d/j;)V

    iput-object v0, p0, Lcom/google/android/location/copresence/d/j;->k:Lcom/google/android/location/copresence/z;

    .line 200
    :goto_3
    iget-object v0, p3, Lcom/google/android/location/copresence/d/e;->b:Lcom/google/android/location/copresence/d/a/l;

    iget v0, v0, Lcom/google/android/location/copresence/d/a/l;->c:I

    if-eqz v0, :cond_5

    move v0, v8

    :goto_4
    if-eqz v0, :cond_6

    .line 201
    new-instance v0, Lcom/google/android/location/copresence/d/a;

    invoke-direct {v0, p0}, Lcom/google/android/location/copresence/d/a;-><init>(Lcom/google/android/location/copresence/d/j;)V

    iput-object v0, p0, Lcom/google/android/location/copresence/d/j;->i:Lcom/google/android/location/copresence/w;

    .line 206
    :goto_5
    const/4 v0, 0x0

    new-array v1, v8, [Lcom/google/android/location/copresence/k/f;

    iget-object v2, p0, Lcom/google/android/location/copresence/d/j;->e:Lcom/google/android/location/copresence/d/n;

    iget-object v2, v2, Lcom/google/android/location/copresence/d/n;->o:Lcom/google/android/location/copresence/k/f;

    aput-object v2, v1, v9

    invoke-direct {p0, v0, v1}, Lcom/google/android/location/copresence/d/j;->a(Lcom/google/android/location/copresence/q/al;[Lcom/google/android/location/copresence/k/f;)V

    .line 207
    return-void

    :cond_1
    move v0, v9

    .line 188
    goto :goto_0

    .line 192
    :cond_2
    sget-object v0, Lcom/google/android/location/copresence/w;->a:Lcom/google/android/location/copresence/w;

    iput-object v0, p0, Lcom/google/android/location/copresence/d/j;->h:Lcom/google/android/location/copresence/w;

    .line 193
    sget-object v0, Lcom/google/android/location/copresence/z;->a:Lcom/google/android/location/copresence/z;

    iput-object v0, p0, Lcom/google/android/location/copresence/d/j;->j:Lcom/google/android/location/copresence/z;

    goto :goto_1

    :cond_3
    move v0, v9

    .line 195
    goto :goto_2

    .line 198
    :cond_4
    sget-object v0, Lcom/google/android/location/copresence/z;->a:Lcom/google/android/location/copresence/z;

    iput-object v0, p0, Lcom/google/android/location/copresence/d/j;->k:Lcom/google/android/location/copresence/z;

    goto :goto_3

    :cond_5
    move v0, v9

    .line 200
    goto :goto_4

    .line 203
    :cond_6
    sget-object v0, Lcom/google/android/location/copresence/w;->a:Lcom/google/android/location/copresence/w;

    iput-object v0, p0, Lcom/google/android/location/copresence/d/j;->i:Lcom/google/android/location/copresence/w;

    goto :goto_5
.end method

.method public static declared-synchronized a(Landroid/content/Context;)Lcom/google/android/location/copresence/d/j;
    .locals 2

    .prologue
    .line 158
    const-class v1, Lcom/google/android/location/copresence/d/j;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/google/android/location/copresence/d/j;->c:Lcom/google/android/location/copresence/d/j;

    if-nez v0, :cond_0

    .line 159
    new-instance v0, Lcom/google/android/location/copresence/d/j;

    invoke-direct {v0, p0}, Lcom/google/android/location/copresence/d/j;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/google/android/location/copresence/d/j;->c:Lcom/google/android/location/copresence/d/j;

    .line 161
    :cond_0
    sget-object v0, Lcom/google/android/location/copresence/d/j;->c:Lcom/google/android/location/copresence/d/j;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 158
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method static synthetic a(Lcom/google/android/location/copresence/d/j;)Lcom/google/android/location/copresence/d/n;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lcom/google/android/location/copresence/d/j;->e:Lcom/google/android/location/copresence/d/n;

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/location/copresence/d/j;Lcom/google/android/location/copresence/k/f;)V
    .locals 0

    .prologue
    .line 43
    invoke-direct {p0, p1}, Lcom/google/android/location/copresence/d/j;->b(Lcom/google/android/location/copresence/k/f;)V

    return-void
.end method

.method private a(Lcom/google/android/location/copresence/k/f;)V
    .locals 3

    .prologue
    .line 459
    iget-object v0, p0, Lcom/google/android/location/copresence/d/j;->l:Lcom/google/android/location/copresence/k/a;

    const/4 v1, 0x1

    new-array v1, v1, [Lcom/google/android/location/copresence/k/f;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    invoke-virtual {v0, v1}, Lcom/google/android/location/copresence/k/a;->c([Lcom/google/android/location/copresence/k/f;)V

    .line 460
    invoke-direct {p0, p1}, Lcom/google/android/location/copresence/d/j;->b(Lcom/google/android/location/copresence/k/f;)V

    .line 461
    return-void
.end method

.method private varargs a(Lcom/google/android/location/copresence/q/al;[Lcom/google/android/location/copresence/k/f;)V
    .locals 6

    .prologue
    const/4 v5, 0x3

    .line 306
    iget-boolean v0, p0, Lcom/google/android/location/copresence/d/j;->q:Z

    if-nez v0, :cond_2

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/location/copresence/d/j;->q:Z

    invoke-static {v5}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "BluetoothMedium: startListeningToBluetoothBroadcasts"

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->b(Ljava/lang/String;)I

    :cond_0
    new-instance v1, Landroid/content/IntentFilter;

    invoke-direct {v1}, Landroid/content/IntentFilter;-><init>()V

    sget-object v0, Lcom/google/android/location/copresence/d/i;->a:Lcom/google/k/c/cd;

    invoke-virtual {v0}, Lcom/google/k/c/cd;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/google/android/location/copresence/d/j;->f:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/location/copresence/d/j;->b:Landroid/content/BroadcastReceiver;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/android/location/copresence/d/j;->g:Landroid/os/Handler;

    invoke-virtual {v0, v2, v1, v3, v4}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;Ljava/lang/String;Landroid/os/Handler;)Landroid/content/Intent;

    .line 307
    :cond_2
    if-eqz p2, :cond_6

    .line 308
    array-length v1, p2

    const/4 v0, 0x0

    :goto_1
    if-ge v0, v1, :cond_5

    aget-object v2, p2, v0

    .line 309
    invoke-static {v5}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 310
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "BluetoothMedium: Requesting state transition: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, v2, Lcom/google/android/location/copresence/k/f;->e:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/location/copresence/ag;->b(Ljava/lang/String;)I

    .line 312
    :cond_3
    if-eqz p1, :cond_4

    .line 313
    iget-object v3, p0, Lcom/google/android/location/copresence/d/j;->o:Lcom/google/android/location/copresence/q/ae;

    invoke-virtual {v3, v2, p1}, Lcom/google/android/location/copresence/q/ae;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 308
    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 316
    :cond_5
    iget-object v0, p0, Lcom/google/android/location/copresence/d/j;->l:Lcom/google/android/location/copresence/k/a;

    invoke-virtual {v0, p2}, Lcom/google/android/location/copresence/k/a;->c([Lcom/google/android/location/copresence/k/f;)V

    .line 317
    iget-object v0, p0, Lcom/google/android/location/copresence/d/j;->l:Lcom/google/android/location/copresence/k/a;

    invoke-virtual {v0, p2}, Lcom/google/android/location/copresence/k/a;->b([Lcom/google/android/location/copresence/k/f;)V

    .line 319
    :cond_6
    return-void
.end method

.method static synthetic b(Lcom/google/android/location/copresence/d/j;)V
    .locals 8

    .prologue
    const/4 v7, 0x6

    const/4 v6, 0x5

    const/4 v5, 0x1

    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 43
    invoke-static {v6}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "BluetoothMedium: BluetoothMedium state transition has failed!"

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->d(Ljava/lang/String;)I

    :cond_0
    iget-object v0, p0, Lcom/google/android/location/copresence/d/j;->l:Lcom/google/android/location/copresence/k/a;

    invoke-virtual {v0}, Lcom/google/android/location/copresence/k/a;->b()V

    iget-object v0, p0, Lcom/google/android/location/copresence/d/j;->o:Lcom/google/android/location/copresence/q/ae;

    invoke-virtual {v0}, Lcom/google/android/location/copresence/q/ae;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/copresence/q/al;

    invoke-interface {v0}, Lcom/google/android/location/copresence/q/al;->b()V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/google/android/location/copresence/d/j;->o:Lcom/google/android/location/copresence/q/ae;

    invoke-virtual {v0}, Lcom/google/android/location/copresence/q/ae;->clear()V

    iget-boolean v0, p0, Lcom/google/android/location/copresence/d/j;->n:Z

    if-eqz v0, :cond_7

    invoke-static {v7}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v0

    if-eqz v0, :cond_3

    const-string v0, "BluetoothMedium: Bluetooth stack is in an unrecoverable state!"

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->e(Ljava/lang/String;)I

    :cond_3
    const/4 v0, 0x3

    invoke-static {v0}, Lcom/google/android/location/copresence/b/a;->a(I)V

    invoke-direct {p0}, Lcom/google/android/location/copresence/d/j;->n()V

    iput-boolean v3, p0, Lcom/google/android/location/copresence/d/j;->m:Z

    iput-boolean v3, p0, Lcom/google/android/location/copresence/d/j;->n:Z

    invoke-static {}, Lcom/google/android/location/copresence/f/b;->b()Lcom/google/ac/b/c/g;

    move-result-object v0

    iget-object v0, v0, Lcom/google/ac/b/c/g;->h:Lcom/google/ac/b/c/e;

    iget-object v0, v0, Lcom/google/ac/b/c/e;->g:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/google/android/location/copresence/d/j;->f:Landroid/content/Context;

    const-string v1, "com.android.bluetooth"

    :try_start_0
    const-string v2, "activity"

    invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    invoke-virtual {v0, v1}, Landroid/app/ActivityManager;->killBackgroundProcesses(Ljava/lang/String;)V

    const-class v2, Ljava/lang/Void;

    const-string v3, "forceStopPackage"

    const-class v4, Ljava/lang/String;

    invoke-static {v0, v2, v3, v4, v1}, Lcom/google/android/location/copresence/bd;->a(Ljava/lang/Object;Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Class;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {v0}, Landroid/app/ActivityManager;->getRunningAppProcesses()Ljava/util/List;

    move-result-object v0

    if-nez v0, :cond_5

    :cond_4
    :goto_1
    return-void

    :cond_5
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_6
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager$RunningAppProcessInfo;

    if-eqz v0, :cond_6

    iget-object v3, v0, Landroid/app/ActivityManager$RunningAppProcessInfo;->processName:Ljava/lang/String;

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_6

    iget v0, v0, Landroid/app/ActivityManager$RunningAppProcessInfo;->pid:I

    invoke-static {v0}, Landroid/os/Process;->killProcess(I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v0

    invoke-static {v7}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v1

    if-eqz v1, :cond_4

    const-string v1, "ProcessUtil: Could not killProcess."

    invoke-static {v1, v0}, Lcom/google/android/location/copresence/ag;->a(Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1

    :cond_7
    invoke-static {v6}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v0

    if-eqz v0, :cond_8

    const-string v0, "BluetoothMedium: Attempting to revert."

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->d(Ljava/lang/String;)I

    :cond_8
    const/4 v0, 0x4

    invoke-static {v0}, Lcom/google/android/location/copresence/b/a;->a(I)V

    iput-boolean v5, p0, Lcom/google/android/location/copresence/d/j;->n:Z

    iget-object v0, p0, Lcom/google/android/location/copresence/d/j;->r:Lcom/google/android/location/copresence/d/ac;

    if-eqz v0, :cond_9

    new-array v0, v5, [Lcom/google/android/location/copresence/k/f;

    iget-object v1, p0, Lcom/google/android/location/copresence/d/j;->r:Lcom/google/android/location/copresence/d/ac;

    iget-object v1, v1, Lcom/google/android/location/copresence/d/ac;->b:Lcom/google/android/location/copresence/k/f;

    aput-object v1, v0, v3

    invoke-direct {p0, v4, v0}, Lcom/google/android/location/copresence/d/j;->a(Lcom/google/android/location/copresence/q/al;[Lcom/google/android/location/copresence/k/f;)V

    iput-object v4, p0, Lcom/google/android/location/copresence/d/j;->r:Lcom/google/android/location/copresence/d/ac;

    :cond_9
    iput-boolean v3, p0, Lcom/google/android/location/copresence/d/j;->s:Z

    new-array v0, v5, [Lcom/google/android/location/copresence/k/f;

    iget-object v1, p0, Lcom/google/android/location/copresence/d/j;->e:Lcom/google/android/location/copresence/d/n;

    iget-object v1, v1, Lcom/google/android/location/copresence/d/n;->i:Lcom/google/android/location/copresence/k/f;

    aput-object v1, v0, v3

    invoke-direct {p0, v4, v0}, Lcom/google/android/location/copresence/d/j;->a(Lcom/google/android/location/copresence/q/al;[Lcom/google/android/location/copresence/k/f;)V

    new-array v0, v5, [Lcom/google/android/location/copresence/k/f;

    iget-object v1, p0, Lcom/google/android/location/copresence/d/j;->e:Lcom/google/android/location/copresence/d/n;

    iget-object v1, v1, Lcom/google/android/location/copresence/d/n;->l:Lcom/google/android/location/copresence/k/f;

    aput-object v1, v0, v3

    invoke-direct {p0, v4, v0}, Lcom/google/android/location/copresence/d/j;->a(Lcom/google/android/location/copresence/q/al;[Lcom/google/android/location/copresence/k/f;)V

    invoke-virtual {p0, v4}, Lcom/google/android/location/copresence/d/j;->c(Lcom/google/android/location/copresence/q/al;)Z

    goto :goto_1
.end method

.method static synthetic b(Lcom/google/android/location/copresence/d/j;Lcom/google/android/location/copresence/k/f;)V
    .locals 2

    .prologue
    .line 43
    iget-object v0, p0, Lcom/google/android/location/copresence/d/j;->o:Lcom/google/android/location/copresence/q/ae;

    invoke-virtual {v0, p1}, Lcom/google/android/location/copresence/q/ae;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    if-eqz v0, :cond_0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/copresence/q/al;

    invoke-interface {v0}, Lcom/google/android/location/copresence/q/al;->a()V

    goto :goto_0

    :cond_0
    return-void
.end method

.method private b(Lcom/google/android/location/copresence/k/f;)V
    .locals 2

    .prologue
    .line 468
    iget-object v0, p0, Lcom/google/android/location/copresence/d/j;->o:Lcom/google/android/location/copresence/q/ae;

    invoke-virtual {v0, p1}, Lcom/google/android/location/copresence/q/ae;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    .line 469
    if-eqz v0, :cond_0

    .line 470
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/copresence/q/al;

    .line 471
    invoke-interface {v0}, Lcom/google/android/location/copresence/q/al;->b()V

    goto :goto_0

    .line 474
    :cond_0
    return-void
.end method

.method static synthetic c(Lcom/google/android/location/copresence/d/j;)Lcom/google/android/location/copresence/d/e;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lcom/google/android/location/copresence/d/j;->d:Lcom/google/android/location/copresence/d/e;

    return-object v0
.end method

.method static synthetic d(Lcom/google/android/location/copresence/d/j;)Z
    .locals 1

    .prologue
    .line 43
    iget-boolean v0, p0, Lcom/google/android/location/copresence/d/j;->m:Z

    return v0
.end method

.method static synthetic e(Lcom/google/android/location/copresence/d/j;)Z
    .locals 1

    .prologue
    .line 43
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/location/copresence/d/j;->m:Z

    return v0
.end method

.method static synthetic f(Lcom/google/android/location/copresence/d/j;)Lcom/google/android/location/copresence/k/a;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lcom/google/android/location/copresence/d/j;->l:Lcom/google/android/location/copresence/k/a;

    return-object v0
.end method

.method static synthetic g(Lcom/google/android/location/copresence/d/j;)Z
    .locals 1

    .prologue
    .line 43
    iget-boolean v0, p0, Lcom/google/android/location/copresence/d/j;->n:Z

    return v0
.end method

.method static synthetic h(Lcom/google/android/location/copresence/d/j;)Z
    .locals 1

    .prologue
    .line 43
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/location/copresence/d/j;->n:Z

    return v0
.end method

.method static synthetic i(Lcom/google/android/location/copresence/d/j;)Lcom/google/android/location/copresence/d/ac;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lcom/google/android/location/copresence/d/j;->r:Lcom/google/android/location/copresence/d/ac;

    return-object v0
.end method

.method private n()V
    .locals 2

    .prologue
    .line 290
    iget-boolean v0, p0, Lcom/google/android/location/copresence/d/j;->q:Z

    if-eqz v0, :cond_2

    .line 291
    const/4 v0, 0x3

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 292
    const-string v0, "BluetoothMedium: stopListeningToBluetoothBroadcasts"

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->b(Ljava/lang/String;)I

    .line 295
    :cond_0
    :try_start_0
    iget-object v0, p0, Lcom/google/android/location/copresence/d/j;->f:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/location/copresence/d/j;->b:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 301
    :cond_1
    :goto_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/location/copresence/d/j;->q:Z

    .line 303
    :cond_2
    return-void

    .line 297
    :catch_0
    move-exception v0

    const/4 v0, 0x5

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 298
    const-string v0, "Unregistered bluetooth broadcast receiver when it wasn\'t registered."

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->d(Ljava/lang/String;)I

    goto :goto_0
.end method


# virtual methods
.method final a(Lcom/google/android/gms/location/copresence/x;)V
    .locals 5

    .prologue
    .line 340
    const/4 v0, 0x3

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 341
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "BluetoothMedium: startBleAdvertiseUuidAndToken: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->b(Ljava/lang/String;)I

    .line 343
    :cond_0
    const/4 v0, 0x0

    const/4 v1, 0x1

    new-array v1, v1, [Lcom/google/android/location/copresence/k/f;

    const/4 v2, 0x0

    new-instance v3, Lcom/google/android/location/copresence/d/ab;

    iget-object v4, p0, Lcom/google/android/location/copresence/d/j;->e:Lcom/google/android/location/copresence/d/n;

    invoke-virtual {v4}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    invoke-direct {v3, v4, p1}, Lcom/google/android/location/copresence/d/ab;-><init>(Lcom/google/android/location/copresence/d/n;Lcom/google/android/gms/location/copresence/x;)V

    aput-object v3, v1, v2

    invoke-direct {p0, v0, v1}, Lcom/google/android/location/copresence/d/j;->a(Lcom/google/android/location/copresence/q/al;[Lcom/google/android/location/copresence/k/f;)V

    .line 345
    return-void
.end method

.method final a(Lcom/google/android/location/copresence/d/f;Lcom/google/android/location/copresence/q/al;)V
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 381
    invoke-static {p1}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 382
    iget-object v0, p0, Lcom/google/android/location/copresence/d/j;->r:Lcom/google/android/location/copresence/d/ac;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/location/copresence/d/j;->r:Lcom/google/android/location/copresence/d/ac;

    iget-object v0, v0, Lcom/google/android/location/copresence/d/ac;->a:Lcom/google/android/location/copresence/d/f;

    if-ne v0, p1, :cond_2

    :cond_0
    move v0, v2

    :goto_0
    const-string v3, "BluetoothMedium only supports one ble callback for now."

    invoke-static {v0, v3}, Lcom/google/android/gms/common/internal/bx;->a(ZLjava/lang/Object;)V

    .line 385
    iget-object v0, p0, Lcom/google/android/location/copresence/d/j;->r:Lcom/google/android/location/copresence/d/ac;

    if-nez v0, :cond_1

    .line 386
    new-instance v0, Lcom/google/android/location/copresence/d/ac;

    iget-object v3, p0, Lcom/google/android/location/copresence/d/j;->e:Lcom/google/android/location/copresence/d/n;

    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    invoke-direct {v0, v3, p1}, Lcom/google/android/location/copresence/d/ac;-><init>(Lcom/google/android/location/copresence/d/n;Lcom/google/android/location/copresence/d/f;)V

    iput-object v0, p0, Lcom/google/android/location/copresence/d/j;->r:Lcom/google/android/location/copresence/d/ac;

    .line 387
    new-array v0, v2, [Lcom/google/android/location/copresence/k/f;

    iget-object v2, p0, Lcom/google/android/location/copresence/d/j;->r:Lcom/google/android/location/copresence/d/ac;

    aput-object v2, v0, v1

    invoke-direct {p0, p2, v0}, Lcom/google/android/location/copresence/d/j;->a(Lcom/google/android/location/copresence/q/al;[Lcom/google/android/location/copresence/k/f;)V

    .line 389
    :cond_1
    return-void

    :cond_2
    move v0, v1

    .line 382
    goto :goto_0
.end method

.method final a(Lcom/google/android/location/copresence/q/al;)V
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 360
    const/4 v0, 0x3

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 361
    const-string v0, "BluetoothMedium: startDiscovery"

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->b(Ljava/lang/String;)I

    .line 363
    :cond_0
    iput-boolean v1, p0, Lcom/google/android/location/copresence/d/j;->s:Z

    .line 364
    iget-object v0, p0, Lcom/google/android/location/copresence/d/j;->e:Lcom/google/android/location/copresence/d/n;

    iget-object v0, v0, Lcom/google/android/location/copresence/d/n;->i:Lcom/google/android/location/copresence/k/f;

    invoke-direct {p0, v0}, Lcom/google/android/location/copresence/d/j;->a(Lcom/google/android/location/copresence/k/f;)V

    .line 365
    new-array v0, v1, [Lcom/google/android/location/copresence/k/f;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/location/copresence/d/j;->e:Lcom/google/android/location/copresence/d/n;

    iget-object v2, v2, Lcom/google/android/location/copresence/d/n;->h:Lcom/google/android/location/copresence/k/f;

    aput-object v2, v0, v1

    invoke-direct {p0, p1, v0}, Lcom/google/android/location/copresence/d/j;->a(Lcom/google/android/location/copresence/q/al;[Lcom/google/android/location/copresence/k/f;)V

    .line 366
    return-void
.end method

.method final a(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 323
    const/4 v0, 0x3

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 324
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "BluetoothMedium: setNameAndMakeDiscoverable: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->b(Ljava/lang/String;)I

    .line 326
    :cond_0
    new-instance v0, Lcom/google/android/location/copresence/d/ae;

    iget-object v1, p0, Lcom/google/android/location/copresence/d/j;->e:Lcom/google/android/location/copresence/d/n;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    invoke-direct {v0, v1, p1}, Lcom/google/android/location/copresence/d/ae;-><init>(Lcom/google/android/location/copresence/d/n;Ljava/lang/String;)V

    .line 327
    const/4 v1, 0x0

    const/4 v2, 0x2

    new-array v2, v2, [Lcom/google/android/location/copresence/k/f;

    const/4 v3, 0x0

    aput-object v0, v2, v3

    const/4 v0, 0x1

    iget-object v3, p0, Lcom/google/android/location/copresence/d/j;->e:Lcom/google/android/location/copresence/d/n;

    iget-object v3, v3, Lcom/google/android/location/copresence/d/n;->j:Lcom/google/android/location/copresence/k/f;

    aput-object v3, v2, v0

    invoke-direct {p0, v1, v2}, Lcom/google/android/location/copresence/d/j;->a(Lcom/google/android/location/copresence/q/al;[Lcom/google/android/location/copresence/k/f;)V

    .line 328
    return-void
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 210
    iget-object v0, p0, Lcom/google/android/location/copresence/d/j;->h:Lcom/google/android/location/copresence/w;

    invoke-interface {v0}, Lcom/google/android/location/copresence/w;->a()Z

    move-result v0

    return v0
.end method

.method final b(Lcom/google/android/location/copresence/d/f;Lcom/google/android/location/copresence/q/al;)V
    .locals 3

    .prologue
    .line 395
    const/4 v0, 0x3

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 396
    const-string v0, "BluetoothMedium: stopLeScan"

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->b(Ljava/lang/String;)I

    .line 398
    :cond_0
    iget-object v0, p0, Lcom/google/android/location/copresence/d/j;->r:Lcom/google/android/location/copresence/d/ac;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/location/copresence/d/j;->r:Lcom/google/android/location/copresence/d/ac;

    iget-object v0, v0, Lcom/google/android/location/copresence/d/ac;->a:Lcom/google/android/location/copresence/d/f;

    if-ne v0, p1, :cond_1

    .line 399
    const/4 v0, 0x1

    new-array v0, v0, [Lcom/google/android/location/copresence/k/f;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/location/copresence/d/j;->r:Lcom/google/android/location/copresence/d/ac;

    iget-object v2, v2, Lcom/google/android/location/copresence/d/ac;->b:Lcom/google/android/location/copresence/k/f;

    aput-object v2, v0, v1

    invoke-direct {p0, p2, v0}, Lcom/google/android/location/copresence/d/j;->a(Lcom/google/android/location/copresence/q/al;[Lcom/google/android/location/copresence/k/f;)V

    .line 400
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/location/copresence/d/j;->r:Lcom/google/android/location/copresence/d/ac;

    .line 402
    :cond_1
    return-void
.end method

.method final b(Lcom/google/android/location/copresence/q/al;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 370
    const/4 v0, 0x3

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 371
    const-string v0, "BluetoothMedium: cancelDiscovery"

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->b(Ljava/lang/String;)I

    .line 373
    :cond_0
    iput-boolean v2, p0, Lcom/google/android/location/copresence/d/j;->s:Z

    .line 374
    iget-object v0, p0, Lcom/google/android/location/copresence/d/j;->e:Lcom/google/android/location/copresence/d/n;

    iget-object v0, v0, Lcom/google/android/location/copresence/d/n;->h:Lcom/google/android/location/copresence/k/f;

    invoke-direct {p0, v0}, Lcom/google/android/location/copresence/d/j;->a(Lcom/google/android/location/copresence/k/f;)V

    .line 375
    const/4 v0, 0x1

    new-array v0, v0, [Lcom/google/android/location/copresence/k/f;

    iget-object v1, p0, Lcom/google/android/location/copresence/d/j;->e:Lcom/google/android/location/copresence/d/n;

    iget-object v1, v1, Lcom/google/android/location/copresence/d/n;->i:Lcom/google/android/location/copresence/k/f;

    aput-object v1, v0, v2

    invoke-direct {p0, p1, v0}, Lcom/google/android/location/copresence/d/j;->a(Lcom/google/android/location/copresence/q/al;[Lcom/google/android/location/copresence/k/f;)V

    .line 376
    return-void
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 214
    iget-object v0, p0, Lcom/google/android/location/copresence/d/j;->i:Lcom/google/android/location/copresence/w;

    invoke-interface {v0}, Lcom/google/android/location/copresence/w;->a()Z

    move-result v0

    return v0
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 218
    iget-object v0, p0, Lcom/google/android/location/copresence/d/j;->j:Lcom/google/android/location/copresence/z;

    invoke-interface {v0}, Lcom/google/android/location/copresence/z;->b()Z

    move-result v0

    return v0
.end method

.method public final c(Lcom/google/android/location/copresence/q/al;)Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 422
    iget-object v2, p0, Lcom/google/android/location/copresence/d/j;->e:Lcom/google/android/location/copresence/d/n;

    iget-object v2, v2, Lcom/google/android/location/copresence/d/n;->o:Lcom/google/android/location/copresence/k/f;

    invoke-virtual {v2}, Lcom/google/android/location/copresence/k/f;->b()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 424
    iput-boolean v0, p0, Lcom/google/android/location/copresence/d/j;->m:Z

    .line 425
    iput-boolean v0, p0, Lcom/google/android/location/copresence/d/j;->n:Z

    .line 426
    invoke-direct {p0}, Lcom/google/android/location/copresence/d/j;->n()V

    .line 430
    :goto_0
    return v0

    .line 429
    :cond_0
    new-array v2, v1, [Lcom/google/android/location/copresence/k/f;

    iget-object v3, p0, Lcom/google/android/location/copresence/d/j;->e:Lcom/google/android/location/copresence/d/n;

    iget-object v3, v3, Lcom/google/android/location/copresence/d/n;->o:Lcom/google/android/location/copresence/k/f;

    aput-object v3, v2, v0

    invoke-direct {p0, p1, v2}, Lcom/google/android/location/copresence/d/j;->a(Lcom/google/android/location/copresence/q/al;[Lcom/google/android/location/copresence/k/f;)V

    move v0, v1

    .line 430
    goto :goto_0
.end method

.method public final d()Z
    .locals 1

    .prologue
    .line 222
    iget-object v0, p0, Lcom/google/android/location/copresence/d/j;->k:Lcom/google/android/location/copresence/z;

    invoke-interface {v0}, Lcom/google/android/location/copresence/z;->b()Z

    move-result v0

    return v0
.end method

.method final e()Landroid/content/Context;
    .locals 1

    .prologue
    .line 268
    iget-object v0, p0, Lcom/google/android/location/copresence/d/j;->f:Landroid/content/Context;

    return-object v0
.end method

.method final f()Landroid/os/Handler;
    .locals 1

    .prologue
    .line 272
    iget-object v0, p0, Lcom/google/android/location/copresence/d/j;->g:Landroid/os/Handler;

    return-object v0
.end method

.method final g()V
    .locals 4

    .prologue
    .line 332
    const/4 v0, 0x3

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 333
    const-string v0, "BluetoothMedium: revertNameAndDiscoverability"

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->b(Ljava/lang/String;)I

    .line 335
    :cond_0
    const/4 v0, 0x0

    const/4 v1, 0x2

    new-array v1, v1, [Lcom/google/android/location/copresence/k/f;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/android/location/copresence/d/j;->e:Lcom/google/android/location/copresence/d/n;

    iget-object v3, v3, Lcom/google/android/location/copresence/d/n;->k:Lcom/google/android/location/copresence/k/f;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/google/android/location/copresence/d/j;->e:Lcom/google/android/location/copresence/d/n;

    iget-object v3, v3, Lcom/google/android/location/copresence/d/n;->m:Lcom/google/android/location/copresence/k/f;

    aput-object v3, v1, v2

    invoke-direct {p0, v0, v1}, Lcom/google/android/location/copresence/d/j;->a(Lcom/google/android/location/copresence/q/al;[Lcom/google/android/location/copresence/k/f;)V

    .line 337
    return-void
.end method

.method final h()V
    .locals 4

    .prologue
    .line 348
    const/4 v0, 0x3

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 349
    const-string v0, "BluetoothMedium: stopBleAdvertise"

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->b(Ljava/lang/String;)I

    .line 351
    :cond_0
    const/4 v0, 0x0

    const/4 v1, 0x1

    new-array v1, v1, [Lcom/google/android/location/copresence/k/f;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/android/location/copresence/d/j;->e:Lcom/google/android/location/copresence/d/n;

    iget-object v3, v3, Lcom/google/android/location/copresence/d/n;->l:Lcom/google/android/location/copresence/k/f;

    aput-object v3, v1, v2

    invoke-direct {p0, v0, v1}, Lcom/google/android/location/copresence/d/j;->a(Lcom/google/android/location/copresence/q/al;[Lcom/google/android/location/copresence/k/f;)V

    .line 352
    return-void
.end method

.method final i()V
    .locals 1

    .prologue
    .line 406
    iget-boolean v0, p0, Lcom/google/android/location/copresence/d/j;->s:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/location/copresence/d/j;->r:Lcom/google/android/location/copresence/d/ac;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/location/copresence/d/j;->e:Lcom/google/android/location/copresence/d/n;

    iget-object v0, v0, Lcom/google/android/location/copresence/d/n;->m:Lcom/google/android/location/copresence/k/f;

    invoke-virtual {v0}, Lcom/google/android/location/copresence/k/f;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/location/copresence/d/j;->e:Lcom/google/android/location/copresence/d/n;

    iget-object v0, v0, Lcom/google/android/location/copresence/d/n;->k:Lcom/google/android/location/copresence/k/f;

    invoke-virtual {v0}, Lcom/google/android/location/copresence/k/f;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/location/copresence/d/j;->e:Lcom/google/android/location/copresence/d/n;

    iget-object v0, v0, Lcom/google/android/location/copresence/d/n;->l:Lcom/google/android/location/copresence/k/f;

    invoke-virtual {v0}, Lcom/google/android/location/copresence/k/f;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 411
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/location/copresence/d/j;->c(Lcom/google/android/location/copresence/q/al;)Z

    .line 413
    :cond_0
    return-void
.end method

.method public final j()Lcom/google/android/location/copresence/w;
    .locals 1

    .prologue
    .line 436
    iget-object v0, p0, Lcom/google/android/location/copresence/d/j;->h:Lcom/google/android/location/copresence/w;

    return-object v0
.end method

.method public final k()Lcom/google/android/location/copresence/z;
    .locals 1

    .prologue
    .line 441
    iget-object v0, p0, Lcom/google/android/location/copresence/d/j;->j:Lcom/google/android/location/copresence/z;

    return-object v0
.end method

.method public final l()Lcom/google/android/location/copresence/w;
    .locals 1

    .prologue
    .line 445
    iget-object v0, p0, Lcom/google/android/location/copresence/d/j;->i:Lcom/google/android/location/copresence/w;

    return-object v0
.end method

.method public final m()Lcom/google/android/location/copresence/z;
    .locals 1

    .prologue
    .line 450
    iget-object v0, p0, Lcom/google/android/location/copresence/d/j;->k:Lcom/google/android/location/copresence/z;

    return-object v0
.end method
