.class final Lcom/google/android/location/copresence/a/j;
.super Lcom/android/volley/toolbox/ab;
.source "SourceFile"


# instance fields
.field final synthetic f:Lcom/google/android/location/copresence/a/i;

.field private final g:Landroid/accounts/Account;


# direct methods
.method public constructor <init>(Lcom/google/android/location/copresence/a/i;Landroid/accounts/Account;Lcom/android/volley/x;Lcom/android/volley/w;)V
    .locals 2

    .prologue
    .line 176
    iput-object p1, p0, Lcom/google/android/location/copresence/a/j;->f:Lcom/google/android/location/copresence/a/i;

    .line 177
    const-string v0, "https://www.googleapis.com/oauth2/v1/userinfo"

    const/4 v1, 0x0

    invoke-direct {p0, v0, p3, p4, v1}, Lcom/android/volley/toolbox/ab;-><init>(Ljava/lang/String;Lcom/android/volley/x;Lcom/android/volley/w;B)V

    .line 178
    iput-object p2, p0, Lcom/google/android/location/copresence/a/j;->g:Landroid/accounts/Account;

    .line 179
    return-void
.end method

.method private t()Ljava/lang/String;
    .locals 3

    .prologue
    .line 191
    :try_start_0
    iget-object v0, p0, Lcom/google/android/location/copresence/a/j;->f:Lcom/google/android/location/copresence/a/i;

    iget-object v0, v0, Lcom/google/android/location/copresence/a/i;->b:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/location/copresence/a/j;->g:Landroid/accounts/Account;

    iget-object v1, v1, Landroid/accounts/Account;->name:Ljava/lang/String;

    const-string v2, "oauth2:https://www.googleapis.com/auth/userinfo.profile"

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/auth/r;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/google/android/gms/auth/q; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v0

    .line 195
    return-object v0

    .line 196
    :catch_0
    move-exception v0

    .line 197
    new-instance v1, Lcom/android/volley/a;

    const-string v2, "Exception fetching AuthToken"

    invoke-direct {v1, v2, v0}, Lcom/android/volley/a;-><init>(Ljava/lang/String;Ljava/lang/Exception;)V

    throw v1

    .line 198
    :catch_1
    move-exception v0

    .line 199
    new-instance v1, Lcom/android/volley/a;

    const-string v2, "Exception fetching AuthToken"

    invoke-direct {v1, v2, v0}, Lcom/android/volley/a;-><init>(Ljava/lang/String;Ljava/lang/Exception;)V

    throw v1
.end method


# virtual methods
.method public final i()Ljava/util/Map;
    .locals 5

    .prologue
    .line 183
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 184
    invoke-direct {p0}, Lcom/google/android/location/copresence/a/j;->t()Ljava/lang/String;

    move-result-object v1

    .line 185
    const-string v2, "Authorization"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Bearer "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 186
    return-object v0
.end method
