.class final Lcom/google/android/location/places/at;
.super Landroid/content/BroadcastReceiver;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/location/places/as;


# direct methods
.method constructor <init>(Lcom/google/android/location/places/as;)V
    .locals 0

    .prologue
    .line 728
    iput-object p1, p0, Lcom/google/android/location/places/at;->a:Lcom/google/android/location/places/as;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public final onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 5

    .prologue
    const/4 v1, 0x0

    const/4 v4, 0x3

    .line 731
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    .line 732
    const/4 v0, -0x1

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v3

    sparse-switch v3, :sswitch_data_0

    :cond_0
    :goto_0
    packed-switch v0, :pswitch_data_0

    .line 759
    const-string v0, "Places"

    invoke-static {v0, v4}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 760
    const-string v0, "Places"

    const-string v1, "Unknown action, ignoring"

    invoke-static {v0, v1}, Lcom/google/android/location/n/aa;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 764
    :cond_1
    :goto_1
    return-void

    .line 732
    :sswitch_0
    const-string v3, "android.intent.action.PACKAGE_REMOVED"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    move v0, v1

    goto :goto_0

    :sswitch_1
    const-string v3, "android.intent.action.PACKAGE_DATA_CLEARED"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    .line 734
    :pswitch_0
    const-string v0, "android.intent.extra.DATA_REMOVED"

    invoke-virtual {p2, v0, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    .line 735
    if-nez v0, :cond_2

    .line 736
    const-string v0, "Places"

    invoke-static {v0, v4}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 737
    const-string v0, "Places"

    const-string v1, "Keeping package data."

    invoke-static {v0, v1}, Lcom/google/android/location/n/aa;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 743
    :cond_2
    :pswitch_1
    invoke-virtual {p2}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    .line 744
    if-eqz v0, :cond_1

    .line 747
    invoke-virtual {v0}, Landroid/net/Uri;->getSchemeSpecificPart()Ljava/lang/String;

    move-result-object v0

    .line 748
    if-eqz v0, :cond_1

    .line 751
    const-string v1, "Places"

    invoke-static {v1, v4}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 752
    const-string v1, "Places"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "App data cleared. Removing subscriptions for: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/location/n/aa;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 755
    :cond_3
    iget-object v1, p0, Lcom/google/android/location/places/at;->a:Lcom/google/android/location/places/as;

    iget-object v1, v1, Lcom/google/android/location/places/as;->h:Lcom/google/android/location/places/an;

    invoke-virtual {v1, v0}, Lcom/google/android/location/places/an;->a(Ljava/lang/String;)V

    .line 756
    iget-object v1, p0, Lcom/google/android/location/places/at;->a:Lcom/google/android/location/places/as;

    iget-object v1, v1, Lcom/google/android/location/places/as;->i:Lcom/google/android/location/places/r;

    invoke-virtual {v1, v0}, Lcom/google/android/location/places/r;->a(Ljava/lang/String;)V

    goto :goto_1

    .line 732
    :sswitch_data_0
    .sparse-switch
        0xff13fb5 -> :sswitch_1
        0x1f50b9c2 -> :sswitch_0
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
