.class final Lcom/google/android/location/copresence/r/g;
.super Lcom/google/android/location/copresence/k/f;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/location/copresence/r/f;


# direct methods
.method varargs constructor <init>(Lcom/google/android/location/copresence/r/f;Ljava/lang/String;[Lcom/google/android/location/copresence/k/f;)V
    .locals 0

    .prologue
    .line 73
    iput-object p1, p0, Lcom/google/android/location/copresence/r/g;->a:Lcom/google/android/location/copresence/r/f;

    invoke-direct {p0, p2, p3}, Lcom/google/android/location/copresence/k/f;-><init>(Ljava/lang/String;[Lcom/google/android/location/copresence/k/f;)V

    return-void
.end method


# virtual methods
.method public final a()Z
    .locals 4

    .prologue
    .line 82
    iget-object v1, p0, Lcom/google/android/location/copresence/r/g;->a:Lcom/google/android/location/copresence/r/f;

    const-string v2, "tether_supported"

    const-string v0, "ro.tether.denied"

    const-string v3, ""

    invoke-static {v0, v3}, Lcom/google/android/gms/common/util/av;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v3, "true"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-static {v1, v2, v0}, Lcom/google/android/location/copresence/r/f;->a(Lcom/google/android/location/copresence/r/f;Ljava/lang/String;I)I

    move-result v0

    .line 84
    iget-object v1, p0, Lcom/google/android/location/copresence/r/g;->a:Lcom/google/android/location/copresence/r/f;

    iget-object v1, p0, Lcom/google/android/location/copresence/r/g;->a:Lcom/google/android/location/copresence/r/f;

    invoke-static {v1}, Lcom/google/android/location/copresence/r/f;->a(Lcom/google/android/location/copresence/r/f;)Landroid/content/SharedPreferences;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v2, "wifiTethering"

    invoke-interface {v1, v2, v0}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "save tethering setting"

    invoke-static {v0, v1}, Lcom/google/android/location/copresence/r/f;->a(Landroid/content/SharedPreferences$Editor;Ljava/lang/String;)Z

    move-result v0

    return v0

    .line 82
    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final b()Z
    .locals 2

    .prologue
    .line 77
    iget-object v0, p0, Lcom/google/android/location/copresence/r/g;->a:Lcom/google/android/location/copresence/r/f;

    invoke-static {v0}, Lcom/google/android/location/copresence/r/f;->a(Lcom/google/android/location/copresence/r/f;)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "wifiTethering"

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method
