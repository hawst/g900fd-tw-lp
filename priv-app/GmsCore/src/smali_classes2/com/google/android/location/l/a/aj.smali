.class public final Lcom/google/android/location/l/a/aj;
.super Lcom/google/protobuf/nano/d;
.source "SourceFile"


# static fields
.field private static volatile k:[Lcom/google/android/location/l/a/aj;


# instance fields
.field public a:Ljava/lang/Integer;

.field public b:Lcom/google/android/location/l/a/am;

.field public c:[Lcom/google/android/location/l/a/r;

.field public d:Ljava/lang/Integer;

.field public e:Lcom/google/android/location/l/a/cj;

.field public f:Lcom/google/android/location/l/a/cl;

.field public g:Lcom/google/android/location/l/a/bk;

.field public h:Lcom/google/android/location/l/a/am;

.field public i:Lcom/google/android/location/l/a/dc;

.field public j:Lcom/google/android/location/l/a/by;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 53
    invoke-direct {p0}, Lcom/google/protobuf/nano/d;-><init>()V

    .line 54
    iput-object v1, p0, Lcom/google/android/location/l/a/aj;->a:Ljava/lang/Integer;

    iput-object v1, p0, Lcom/google/android/location/l/a/aj;->b:Lcom/google/android/location/l/a/am;

    invoke-static {}, Lcom/google/android/location/l/a/r;->a()[Lcom/google/android/location/l/a/r;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/l/a/aj;->c:[Lcom/google/android/location/l/a/r;

    iput-object v1, p0, Lcom/google/android/location/l/a/aj;->d:Ljava/lang/Integer;

    iput-object v1, p0, Lcom/google/android/location/l/a/aj;->e:Lcom/google/android/location/l/a/cj;

    iput-object v1, p0, Lcom/google/android/location/l/a/aj;->f:Lcom/google/android/location/l/a/cl;

    iput-object v1, p0, Lcom/google/android/location/l/a/aj;->g:Lcom/google/android/location/l/a/bk;

    iput-object v1, p0, Lcom/google/android/location/l/a/aj;->h:Lcom/google/android/location/l/a/am;

    iput-object v1, p0, Lcom/google/android/location/l/a/aj;->i:Lcom/google/android/location/l/a/dc;

    iput-object v1, p0, Lcom/google/android/location/l/a/aj;->j:Lcom/google/android/location/l/a/by;

    iput-object v1, p0, Lcom/google/android/location/l/a/aj;->unknownFieldData:Lcom/google/protobuf/nano/f;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/location/l/a/aj;->cachedSize:I

    .line 55
    return-void
.end method

.method public static a()[Lcom/google/android/location/l/a/aj;
    .locals 2

    .prologue
    .line 12
    sget-object v0, Lcom/google/android/location/l/a/aj;->k:[Lcom/google/android/location/l/a/aj;

    if-nez v0, :cond_1

    .line 13
    sget-object v1, Lcom/google/protobuf/nano/h;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 15
    :try_start_0
    sget-object v0, Lcom/google/android/location/l/a/aj;->k:[Lcom/google/android/location/l/a/aj;

    if-nez v0, :cond_0

    .line 16
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/android/location/l/a/aj;

    sput-object v0, Lcom/google/android/location/l/a/aj;->k:[Lcom/google/android/location/l/a/aj;

    .line 18
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 20
    :cond_1
    sget-object v0, Lcom/google/android/location/l/a/aj;->k:[Lcom/google/android/location/l/a/aj;

    return-object v0

    .line 18
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 5

    .prologue
    .line 232
    invoke-super {p0}, Lcom/google/protobuf/nano/d;->computeSerializedSize()I

    move-result v0

    .line 233
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/location/l/a/aj;->a:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 235
    iget-object v1, p0, Lcom/google/android/location/l/a/aj;->b:Lcom/google/android/location/l/a/am;

    if-eqz v1, :cond_0

    .line 236
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/location/l/a/aj;->b:Lcom/google/android/location/l/a/am;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 239
    :cond_0
    iget-object v1, p0, Lcom/google/android/location/l/a/aj;->c:[Lcom/google/android/location/l/a/r;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/google/android/location/l/a/aj;->c:[Lcom/google/android/location/l/a/r;

    array-length v1, v1

    if-lez v1, :cond_3

    .line 240
    const/4 v1, 0x0

    move v4, v1

    move v1, v0

    move v0, v4

    :goto_0
    iget-object v2, p0, Lcom/google/android/location/l/a/aj;->c:[Lcom/google/android/location/l/a/r;

    array-length v2, v2

    if-ge v0, v2, :cond_2

    .line 241
    iget-object v2, p0, Lcom/google/android/location/l/a/aj;->c:[Lcom/google/android/location/l/a/r;

    aget-object v2, v2, v0

    .line 242
    if-eqz v2, :cond_1

    .line 243
    const/4 v3, 0x3

    invoke-static {v3, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v2

    add-int/2addr v1, v2

    .line 240
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    move v0, v1

    .line 248
    :cond_3
    iget-object v1, p0, Lcom/google/android/location/l/a/aj;->d:Ljava/lang/Integer;

    if-eqz v1, :cond_4

    .line 249
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/android/location/l/a/aj;->d:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 252
    :cond_4
    iget-object v1, p0, Lcom/google/android/location/l/a/aj;->e:Lcom/google/android/location/l/a/cj;

    if-eqz v1, :cond_5

    .line 253
    const/4 v1, 0x6

    iget-object v2, p0, Lcom/google/android/location/l/a/aj;->e:Lcom/google/android/location/l/a/cj;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 256
    :cond_5
    iget-object v1, p0, Lcom/google/android/location/l/a/aj;->f:Lcom/google/android/location/l/a/cl;

    if-eqz v1, :cond_6

    .line 257
    const/4 v1, 0x7

    iget-object v2, p0, Lcom/google/android/location/l/a/aj;->f:Lcom/google/android/location/l/a/cl;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 260
    :cond_6
    iget-object v1, p0, Lcom/google/android/location/l/a/aj;->h:Lcom/google/android/location/l/a/am;

    if-eqz v1, :cond_7

    .line 261
    const/16 v1, 0x8

    iget-object v2, p0, Lcom/google/android/location/l/a/aj;->h:Lcom/google/android/location/l/a/am;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 264
    :cond_7
    iget-object v1, p0, Lcom/google/android/location/l/a/aj;->g:Lcom/google/android/location/l/a/bk;

    if-eqz v1, :cond_8

    .line 265
    const/16 v1, 0x9

    iget-object v2, p0, Lcom/google/android/location/l/a/aj;->g:Lcom/google/android/location/l/a/bk;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 268
    :cond_8
    iget-object v1, p0, Lcom/google/android/location/l/a/aj;->i:Lcom/google/android/location/l/a/dc;

    if-eqz v1, :cond_9

    .line 269
    const/16 v1, 0xa

    iget-object v2, p0, Lcom/google/android/location/l/a/aj;->i:Lcom/google/android/location/l/a/dc;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 272
    :cond_9
    iget-object v1, p0, Lcom/google/android/location/l/a/aj;->j:Lcom/google/android/location/l/a/by;

    if-eqz v1, :cond_a

    .line 273
    const/16 v1, 0xb

    iget-object v2, p0, Lcom/google/android/location/l/a/aj;->j:Lcom/google/android/location/l/a/by;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 276
    :cond_a
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 75
    if-ne p1, p0, :cond_1

    .line 76
    const/4 v0, 0x1

    .line 162
    :cond_0
    :goto_0
    return v0

    .line 78
    :cond_1
    instance-of v1, p1, Lcom/google/android/location/l/a/aj;

    if-eqz v1, :cond_0

    .line 81
    check-cast p1, Lcom/google/android/location/l/a/aj;

    .line 82
    iget-object v1, p0, Lcom/google/android/location/l/a/aj;->a:Ljava/lang/Integer;

    if-nez v1, :cond_b

    .line 83
    iget-object v1, p1, Lcom/google/android/location/l/a/aj;->a:Ljava/lang/Integer;

    if-nez v1, :cond_0

    .line 88
    :cond_2
    iget-object v1, p0, Lcom/google/android/location/l/a/aj;->b:Lcom/google/android/location/l/a/am;

    if-nez v1, :cond_c

    .line 89
    iget-object v1, p1, Lcom/google/android/location/l/a/aj;->b:Lcom/google/android/location/l/a/am;

    if-nez v1, :cond_0

    .line 97
    :cond_3
    iget-object v1, p0, Lcom/google/android/location/l/a/aj;->c:[Lcom/google/android/location/l/a/r;

    iget-object v2, p1, Lcom/google/android/location/l/a/aj;->c:[Lcom/google/android/location/l/a/r;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 101
    iget-object v1, p0, Lcom/google/android/location/l/a/aj;->d:Ljava/lang/Integer;

    if-nez v1, :cond_d

    .line 102
    iget-object v1, p1, Lcom/google/android/location/l/a/aj;->d:Ljava/lang/Integer;

    if-nez v1, :cond_0

    .line 108
    :cond_4
    iget-object v1, p0, Lcom/google/android/location/l/a/aj;->e:Lcom/google/android/location/l/a/cj;

    if-nez v1, :cond_e

    .line 109
    iget-object v1, p1, Lcom/google/android/location/l/a/aj;->e:Lcom/google/android/location/l/a/cj;

    if-nez v1, :cond_0

    .line 117
    :cond_5
    iget-object v1, p0, Lcom/google/android/location/l/a/aj;->f:Lcom/google/android/location/l/a/cl;

    if-nez v1, :cond_f

    .line 118
    iget-object v1, p1, Lcom/google/android/location/l/a/aj;->f:Lcom/google/android/location/l/a/cl;

    if-nez v1, :cond_0

    .line 126
    :cond_6
    iget-object v1, p0, Lcom/google/android/location/l/a/aj;->g:Lcom/google/android/location/l/a/bk;

    if-nez v1, :cond_10

    .line 127
    iget-object v1, p1, Lcom/google/android/location/l/a/aj;->g:Lcom/google/android/location/l/a/bk;

    if-nez v1, :cond_0

    .line 135
    :cond_7
    iget-object v1, p0, Lcom/google/android/location/l/a/aj;->h:Lcom/google/android/location/l/a/am;

    if-nez v1, :cond_11

    .line 136
    iget-object v1, p1, Lcom/google/android/location/l/a/aj;->h:Lcom/google/android/location/l/a/am;

    if-nez v1, :cond_0

    .line 144
    :cond_8
    iget-object v1, p0, Lcom/google/android/location/l/a/aj;->i:Lcom/google/android/location/l/a/dc;

    if-nez v1, :cond_12

    .line 145
    iget-object v1, p1, Lcom/google/android/location/l/a/aj;->i:Lcom/google/android/location/l/a/dc;

    if-nez v1, :cond_0

    .line 153
    :cond_9
    iget-object v1, p0, Lcom/google/android/location/l/a/aj;->j:Lcom/google/android/location/l/a/by;

    if-nez v1, :cond_13

    .line 154
    iget-object v1, p1, Lcom/google/android/location/l/a/aj;->j:Lcom/google/android/location/l/a/by;

    if-nez v1, :cond_0

    .line 162
    :cond_a
    invoke-virtual {p0, p1}, Lcom/google/android/location/l/a/aj;->unknownFieldDataEquals(Lcom/google/protobuf/nano/d;)Z

    move-result v0

    goto :goto_0

    .line 86
    :cond_b
    iget-object v1, p0, Lcom/google/android/location/l/a/aj;->a:Ljava/lang/Integer;

    iget-object v2, p1, Lcom/google/android/location/l/a/aj;->a:Ljava/lang/Integer;

    invoke-virtual {v1, v2}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    goto :goto_0

    .line 93
    :cond_c
    iget-object v1, p0, Lcom/google/android/location/l/a/aj;->b:Lcom/google/android/location/l/a/am;

    iget-object v2, p1, Lcom/google/android/location/l/a/aj;->b:Lcom/google/android/location/l/a/am;

    invoke-virtual {v1, v2}, Lcom/google/android/location/l/a/am;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    goto :goto_0

    .line 105
    :cond_d
    iget-object v1, p0, Lcom/google/android/location/l/a/aj;->d:Ljava/lang/Integer;

    iget-object v2, p1, Lcom/google/android/location/l/a/aj;->d:Ljava/lang/Integer;

    invoke-virtual {v1, v2}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    goto :goto_0

    .line 113
    :cond_e
    iget-object v1, p0, Lcom/google/android/location/l/a/aj;->e:Lcom/google/android/location/l/a/cj;

    iget-object v2, p1, Lcom/google/android/location/l/a/aj;->e:Lcom/google/android/location/l/a/cj;

    invoke-virtual {v1, v2}, Lcom/google/android/location/l/a/cj;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_5

    goto/16 :goto_0

    .line 122
    :cond_f
    iget-object v1, p0, Lcom/google/android/location/l/a/aj;->f:Lcom/google/android/location/l/a/cl;

    iget-object v2, p1, Lcom/google/android/location/l/a/aj;->f:Lcom/google/android/location/l/a/cl;

    invoke-virtual {v1, v2}, Lcom/google/android/location/l/a/cl;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_6

    goto/16 :goto_0

    .line 131
    :cond_10
    iget-object v1, p0, Lcom/google/android/location/l/a/aj;->g:Lcom/google/android/location/l/a/bk;

    iget-object v2, p1, Lcom/google/android/location/l/a/aj;->g:Lcom/google/android/location/l/a/bk;

    invoke-virtual {v1, v2}, Lcom/google/android/location/l/a/bk;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_7

    goto/16 :goto_0

    .line 140
    :cond_11
    iget-object v1, p0, Lcom/google/android/location/l/a/aj;->h:Lcom/google/android/location/l/a/am;

    iget-object v2, p1, Lcom/google/android/location/l/a/aj;->h:Lcom/google/android/location/l/a/am;

    invoke-virtual {v1, v2}, Lcom/google/android/location/l/a/am;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_8

    goto/16 :goto_0

    .line 149
    :cond_12
    iget-object v1, p0, Lcom/google/android/location/l/a/aj;->i:Lcom/google/android/location/l/a/dc;

    iget-object v2, p1, Lcom/google/android/location/l/a/aj;->i:Lcom/google/android/location/l/a/dc;

    invoke-virtual {v1, v2}, Lcom/google/android/location/l/a/dc;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_9

    goto/16 :goto_0

    .line 158
    :cond_13
    iget-object v1, p0, Lcom/google/android/location/l/a/aj;->j:Lcom/google/android/location/l/a/by;

    iget-object v2, p1, Lcom/google/android/location/l/a/aj;->j:Lcom/google/android/location/l/a/by;

    invoke-virtual {v1, v2}, Lcom/google/android/location/l/a/by;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_a

    goto/16 :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 167
    iget-object v0, p0, Lcom/google/android/location/l/a/aj;->a:Ljava/lang/Integer;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    .line 169
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/location/l/a/aj;->b:Lcom/google/android/location/l/a/am;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    .line 171
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/android/location/l/a/aj;->c:[Lcom/google/android/location/l/a/r;

    invoke-static {v2}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;)I

    move-result v2

    add-int/2addr v0, v2

    .line 173
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/location/l/a/aj;->d:Ljava/lang/Integer;

    if-nez v0, :cond_2

    move v0, v1

    :goto_2
    add-int/2addr v0, v2

    .line 175
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/location/l/a/aj;->e:Lcom/google/android/location/l/a/cj;

    if-nez v0, :cond_3

    move v0, v1

    :goto_3
    add-int/2addr v0, v2

    .line 177
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/location/l/a/aj;->f:Lcom/google/android/location/l/a/cl;

    if-nez v0, :cond_4

    move v0, v1

    :goto_4
    add-int/2addr v0, v2

    .line 179
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/location/l/a/aj;->g:Lcom/google/android/location/l/a/bk;

    if-nez v0, :cond_5

    move v0, v1

    :goto_5
    add-int/2addr v0, v2

    .line 181
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/location/l/a/aj;->h:Lcom/google/android/location/l/a/am;

    if-nez v0, :cond_6

    move v0, v1

    :goto_6
    add-int/2addr v0, v2

    .line 183
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/location/l/a/aj;->i:Lcom/google/android/location/l/a/dc;

    if-nez v0, :cond_7

    move v0, v1

    :goto_7
    add-int/2addr v0, v2

    .line 185
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/android/location/l/a/aj;->j:Lcom/google/android/location/l/a/by;

    if-nez v2, :cond_8

    :goto_8
    add-int/2addr v0, v1

    .line 187
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/google/android/location/l/a/aj;->unknownFieldDataHashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 188
    return v0

    .line 167
    :cond_0
    iget-object v0, p0, Lcom/google/android/location/l/a/aj;->a:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    goto :goto_0

    .line 169
    :cond_1
    iget-object v0, p0, Lcom/google/android/location/l/a/aj;->b:Lcom/google/android/location/l/a/am;

    invoke-virtual {v0}, Lcom/google/android/location/l/a/am;->hashCode()I

    move-result v0

    goto :goto_1

    .line 173
    :cond_2
    iget-object v0, p0, Lcom/google/android/location/l/a/aj;->d:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->hashCode()I

    move-result v0

    goto :goto_2

    .line 175
    :cond_3
    iget-object v0, p0, Lcom/google/android/location/l/a/aj;->e:Lcom/google/android/location/l/a/cj;

    invoke-virtual {v0}, Lcom/google/android/location/l/a/cj;->hashCode()I

    move-result v0

    goto :goto_3

    .line 177
    :cond_4
    iget-object v0, p0, Lcom/google/android/location/l/a/aj;->f:Lcom/google/android/location/l/a/cl;

    invoke-virtual {v0}, Lcom/google/android/location/l/a/cl;->hashCode()I

    move-result v0

    goto :goto_4

    .line 179
    :cond_5
    iget-object v0, p0, Lcom/google/android/location/l/a/aj;->g:Lcom/google/android/location/l/a/bk;

    invoke-virtual {v0}, Lcom/google/android/location/l/a/bk;->hashCode()I

    move-result v0

    goto :goto_5

    .line 181
    :cond_6
    iget-object v0, p0, Lcom/google/android/location/l/a/aj;->h:Lcom/google/android/location/l/a/am;

    invoke-virtual {v0}, Lcom/google/android/location/l/a/am;->hashCode()I

    move-result v0

    goto :goto_6

    .line 183
    :cond_7
    iget-object v0, p0, Lcom/google/android/location/l/a/aj;->i:Lcom/google/android/location/l/a/dc;

    invoke-virtual {v0}, Lcom/google/android/location/l/a/dc;->hashCode()I

    move-result v0

    goto :goto_7

    .line 185
    :cond_8
    iget-object v1, p0, Lcom/google/android/location/l/a/aj;->j:Lcom/google/android/location/l/a/by;

    invoke-virtual {v1}, Lcom/google/android/location/l/a/by;->hashCode()I

    move-result v1

    goto :goto_8
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 5
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/android/location/l/a/aj;->storeUnknownField(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    sparse-switch v0, :sswitch_data_1

    goto :goto_0

    :sswitch_2
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/l/a/aj;->a:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Lcom/google/android/location/l/a/aj;->b:Lcom/google/android/location/l/a/am;

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/android/location/l/a/am;

    invoke-direct {v0}, Lcom/google/android/location/l/a/am;-><init>()V

    iput-object v0, p0, Lcom/google/android/location/l/a/aj;->b:Lcom/google/android/location/l/a/am;

    :cond_1
    iget-object v0, p0, Lcom/google/android/location/l/a/aj;->b:Lcom/google/android/location/l/a/am;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_4
    const/16 v0, 0x1a

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->b(Lcom/google/protobuf/nano/a;I)I

    move-result v2

    iget-object v0, p0, Lcom/google/android/location/l/a/aj;->c:[Lcom/google/android/location/l/a/r;

    if-nez v0, :cond_3

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lcom/google/android/location/l/a/r;

    if-eqz v0, :cond_2

    iget-object v3, p0, Lcom/google/android/location/l/a/aj;->c:[Lcom/google/android/location/l/a/r;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_2
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_4

    new-instance v3, Lcom/google/android/location/l/a/r;

    invoke-direct {v3}, Lcom/google/android/location/l/a/r;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_3
    iget-object v0, p0, Lcom/google/android/location/l/a/aj;->c:[Lcom/google/android/location/l/a/r;

    array-length v0, v0

    goto :goto_1

    :cond_4
    new-instance v3, Lcom/google/android/location/l/a/r;

    invoke-direct {v3}, Lcom/google/android/location/l/a/r;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    iput-object v2, p0, Lcom/google/android/location/l/a/aj;->c:[Lcom/google/android/location/l/a/r;

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/l/a/aj;->d:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_6
    iget-object v0, p0, Lcom/google/android/location/l/a/aj;->e:Lcom/google/android/location/l/a/cj;

    if-nez v0, :cond_5

    new-instance v0, Lcom/google/android/location/l/a/cj;

    invoke-direct {v0}, Lcom/google/android/location/l/a/cj;-><init>()V

    iput-object v0, p0, Lcom/google/android/location/l/a/aj;->e:Lcom/google/android/location/l/a/cj;

    :cond_5
    iget-object v0, p0, Lcom/google/android/location/l/a/aj;->e:Lcom/google/android/location/l/a/cj;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto/16 :goto_0

    :sswitch_7
    iget-object v0, p0, Lcom/google/android/location/l/a/aj;->f:Lcom/google/android/location/l/a/cl;

    if-nez v0, :cond_6

    new-instance v0, Lcom/google/android/location/l/a/cl;

    invoke-direct {v0}, Lcom/google/android/location/l/a/cl;-><init>()V

    iput-object v0, p0, Lcom/google/android/location/l/a/aj;->f:Lcom/google/android/location/l/a/cl;

    :cond_6
    iget-object v0, p0, Lcom/google/android/location/l/a/aj;->f:Lcom/google/android/location/l/a/cl;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto/16 :goto_0

    :sswitch_8
    iget-object v0, p0, Lcom/google/android/location/l/a/aj;->h:Lcom/google/android/location/l/a/am;

    if-nez v0, :cond_7

    new-instance v0, Lcom/google/android/location/l/a/am;

    invoke-direct {v0}, Lcom/google/android/location/l/a/am;-><init>()V

    iput-object v0, p0, Lcom/google/android/location/l/a/aj;->h:Lcom/google/android/location/l/a/am;

    :cond_7
    iget-object v0, p0, Lcom/google/android/location/l/a/aj;->h:Lcom/google/android/location/l/a/am;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto/16 :goto_0

    :sswitch_9
    iget-object v0, p0, Lcom/google/android/location/l/a/aj;->g:Lcom/google/android/location/l/a/bk;

    if-nez v0, :cond_8

    new-instance v0, Lcom/google/android/location/l/a/bk;

    invoke-direct {v0}, Lcom/google/android/location/l/a/bk;-><init>()V

    iput-object v0, p0, Lcom/google/android/location/l/a/aj;->g:Lcom/google/android/location/l/a/bk;

    :cond_8
    iget-object v0, p0, Lcom/google/android/location/l/a/aj;->g:Lcom/google/android/location/l/a/bk;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto/16 :goto_0

    :sswitch_a
    iget-object v0, p0, Lcom/google/android/location/l/a/aj;->i:Lcom/google/android/location/l/a/dc;

    if-nez v0, :cond_9

    new-instance v0, Lcom/google/android/location/l/a/dc;

    invoke-direct {v0}, Lcom/google/android/location/l/a/dc;-><init>()V

    iput-object v0, p0, Lcom/google/android/location/l/a/aj;->i:Lcom/google/android/location/l/a/dc;

    :cond_9
    iget-object v0, p0, Lcom/google/android/location/l/a/aj;->i:Lcom/google/android/location/l/a/dc;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto/16 :goto_0

    :sswitch_b
    iget-object v0, p0, Lcom/google/android/location/l/a/aj;->j:Lcom/google/android/location/l/a/by;

    if-nez v0, :cond_a

    new-instance v0, Lcom/google/android/location/l/a/by;

    invoke-direct {v0}, Lcom/google/android/location/l/a/by;-><init>()V

    iput-object v0, p0, Lcom/google/android/location/l/a/aj;->j:Lcom/google/android/location/l/a/by;

    :cond_a
    iget-object v0, p0, Lcom/google/android/location/l/a/aj;->j:Lcom/google/android/location/l/a/by;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_3
        0x1a -> :sswitch_4
        0x20 -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x42 -> :sswitch_8
        0x4a -> :sswitch_9
        0x52 -> :sswitch_a
        0x5a -> :sswitch_b
    .end sparse-switch

    :sswitch_data_1
    .sparse-switch
        0x0 -> :sswitch_2
        0x1 -> :sswitch_2
        0x2 -> :sswitch_2
        0x3 -> :sswitch_2
        0x4 -> :sswitch_2
        0x5 -> :sswitch_2
        0x6 -> :sswitch_2
        0x7 -> :sswitch_2
        0x8 -> :sswitch_2
        0x9 -> :sswitch_2
        0xa -> :sswitch_2
        0xb -> :sswitch_2
        0xc -> :sswitch_2
        0xd -> :sswitch_2
        0xe -> :sswitch_2
        0xf -> :sswitch_2
        0x10 -> :sswitch_2
        0x11 -> :sswitch_2
        0x12 -> :sswitch_2
        0x13 -> :sswitch_2
        0x14 -> :sswitch_2
        0x15 -> :sswitch_2
        0x16 -> :sswitch_2
        0x17 -> :sswitch_2
        0x18 -> :sswitch_2
        0x19 -> :sswitch_2
        0x64 -> :sswitch_2
        0x65 -> :sswitch_2
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 3

    .prologue
    .line 194
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/location/l/a/aj;->a:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 195
    iget-object v0, p0, Lcom/google/android/location/l/a/aj;->b:Lcom/google/android/location/l/a/am;

    if-eqz v0, :cond_0

    .line 196
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/android/location/l/a/aj;->b:Lcom/google/android/location/l/a/am;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 198
    :cond_0
    iget-object v0, p0, Lcom/google/android/location/l/a/aj;->c:[Lcom/google/android/location/l/a/r;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/location/l/a/aj;->c:[Lcom/google/android/location/l/a/r;

    array-length v0, v0

    if-lez v0, :cond_2

    .line 199
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/google/android/location/l/a/aj;->c:[Lcom/google/android/location/l/a/r;

    array-length v1, v1

    if-ge v0, v1, :cond_2

    .line 200
    iget-object v1, p0, Lcom/google/android/location/l/a/aj;->c:[Lcom/google/android/location/l/a/r;

    aget-object v1, v1, v0

    .line 201
    if-eqz v1, :cond_1

    .line 202
    const/4 v2, 0x3

    invoke-virtual {p1, v2, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 199
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 206
    :cond_2
    iget-object v0, p0, Lcom/google/android/location/l/a/aj;->d:Ljava/lang/Integer;

    if-eqz v0, :cond_3

    .line 207
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/android/location/l/a/aj;->d:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 209
    :cond_3
    iget-object v0, p0, Lcom/google/android/location/l/a/aj;->e:Lcom/google/android/location/l/a/cj;

    if-eqz v0, :cond_4

    .line 210
    const/4 v0, 0x6

    iget-object v1, p0, Lcom/google/android/location/l/a/aj;->e:Lcom/google/android/location/l/a/cj;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 212
    :cond_4
    iget-object v0, p0, Lcom/google/android/location/l/a/aj;->f:Lcom/google/android/location/l/a/cl;

    if-eqz v0, :cond_5

    .line 213
    const/4 v0, 0x7

    iget-object v1, p0, Lcom/google/android/location/l/a/aj;->f:Lcom/google/android/location/l/a/cl;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 215
    :cond_5
    iget-object v0, p0, Lcom/google/android/location/l/a/aj;->h:Lcom/google/android/location/l/a/am;

    if-eqz v0, :cond_6

    .line 216
    const/16 v0, 0x8

    iget-object v1, p0, Lcom/google/android/location/l/a/aj;->h:Lcom/google/android/location/l/a/am;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 218
    :cond_6
    iget-object v0, p0, Lcom/google/android/location/l/a/aj;->g:Lcom/google/android/location/l/a/bk;

    if-eqz v0, :cond_7

    .line 219
    const/16 v0, 0x9

    iget-object v1, p0, Lcom/google/android/location/l/a/aj;->g:Lcom/google/android/location/l/a/bk;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 221
    :cond_7
    iget-object v0, p0, Lcom/google/android/location/l/a/aj;->i:Lcom/google/android/location/l/a/dc;

    if-eqz v0, :cond_8

    .line 222
    const/16 v0, 0xa

    iget-object v1, p0, Lcom/google/android/location/l/a/aj;->i:Lcom/google/android/location/l/a/dc;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 224
    :cond_8
    iget-object v0, p0, Lcom/google/android/location/l/a/aj;->j:Lcom/google/android/location/l/a/by;

    if-eqz v0, :cond_9

    .line 225
    const/16 v0, 0xb

    iget-object v1, p0, Lcom/google/android/location/l/a/aj;->j:Lcom/google/android/location/l/a/by;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 227
    :cond_9
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/d;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 228
    return-void
.end method
