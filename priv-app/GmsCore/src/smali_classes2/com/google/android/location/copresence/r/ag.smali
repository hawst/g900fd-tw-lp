.class public final Lcom/google/android/location/copresence/r/ag;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static f:Lcom/google/android/location/copresence/r/ag;


# instance fields
.field public final a:Lcom/google/android/location/copresence/w;

.field public final b:Lcom/google/android/location/copresence/w;

.field public final c:Lcom/google/android/location/copresence/z;

.field public final d:Lcom/google/android/location/copresence/z;

.field public final e:Lcom/google/android/location/copresence/r/al;


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 52
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 53
    invoke-static {p1}, Lcom/google/android/location/copresence/r/ag;->b(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 54
    new-instance v3, Lcom/google/android/location/copresence/r/ae;

    const-string v0, "wifi"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/wifi/WifiManager;

    invoke-direct {v3, v0}, Lcom/google/android/location/copresence/r/ae;-><init>(Landroid/net/wifi/WifiManager;)V

    .line 56
    new-instance v0, Lcom/google/android/location/copresence/r/al;

    invoke-direct {v0, p1, v3}, Lcom/google/android/location/copresence/r/al;-><init>(Landroid/content/Context;Lcom/google/android/location/copresence/r/ae;)V

    iput-object v0, p0, Lcom/google/android/location/copresence/r/ag;->e:Lcom/google/android/location/copresence/r/al;

    .line 57
    new-instance v0, Lcom/google/android/location/copresence/r/an;

    invoke-static {p1}, Lcom/google/android/location/copresence/ap;->a(Landroid/content/Context;)Lcom/google/android/location/copresence/ap;

    move-result-object v4

    invoke-direct {v0, p1, v3, v4, v1}, Lcom/google/android/location/copresence/r/an;-><init>(Landroid/content/Context;Lcom/google/android/location/copresence/r/ae;Lcom/google/android/location/copresence/ap;Z)V

    iput-object v0, p0, Lcom/google/android/location/copresence/r/ag;->c:Lcom/google/android/location/copresence/z;

    .line 59
    new-instance v0, Lcom/google/android/location/copresence/r/an;

    invoke-static {p1}, Lcom/google/android/location/copresence/ap;->a(Landroid/content/Context;)Lcom/google/android/location/copresence/ap;

    move-result-object v4

    invoke-direct {v0, p1, v3, v4, v2}, Lcom/google/android/location/copresence/r/an;-><init>(Landroid/content/Context;Lcom/google/android/location/copresence/r/ae;Lcom/google/android/location/copresence/ap;Z)V

    iput-object v0, p0, Lcom/google/android/location/copresence/r/ag;->d:Lcom/google/android/location/copresence/z;

    .line 61
    invoke-static {}, Lcom/google/android/location/copresence/f/b;->b()Lcom/google/ac/b/c/g;

    move-result-object v0

    iget-object v0, v0, Lcom/google/ac/b/c/g;->e:Lcom/google/ac/b/c/m;

    iget-object v0, v0, Lcom/google/ac/b/c/m;->j:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 62
    invoke-static {v5}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 63
    const-string v0, "WifiMedium: Using WifiApBeacon2 with notification blocking"

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->b(Ljava/lang/String;)I

    .line 65
    :cond_0
    new-instance v0, Lcom/google/android/location/copresence/r/c;

    invoke-direct {v0, p1, v3}, Lcom/google/android/location/copresence/r/c;-><init>(Landroid/content/Context;Lcom/google/android/location/copresence/r/ae;)V

    iput-object v0, p0, Lcom/google/android/location/copresence/r/ag;->a:Lcom/google/android/location/copresence/w;

    .line 72
    :goto_0
    invoke-static {p1}, Lcom/google/android/location/copresence/r/ag;->b(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_3

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v4, 0x10

    if-lt v0, v4, :cond_3

    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    const-string v4, "android.hardware.wifi.direct"

    invoke-virtual {v0, v4}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    move v0, v1

    :goto_1
    if-eqz v0, :cond_4

    .line 73
    new-instance v1, Lcom/google/android/location/copresence/r/ah;

    const-string v0, "wifip2p"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/wifi/p2p/WifiP2pManager;

    invoke-direct {v1, p1, v0}, Lcom/google/android/location/copresence/r/ah;-><init>(Landroid/content/Context;Landroid/net/wifi/p2p/WifiP2pManager;)V

    .line 75
    new-instance v0, Lcom/google/android/location/copresence/r/n;

    invoke-direct {v0, p1, v3, v1}, Lcom/google/android/location/copresence/r/n;-><init>(Landroid/content/Context;Lcom/google/android/location/copresence/r/ae;Lcom/google/android/location/copresence/r/ah;)V

    iput-object v0, p0, Lcom/google/android/location/copresence/r/ag;->b:Lcom/google/android/location/copresence/w;

    .line 89
    :goto_2
    return-void

    .line 67
    :cond_1
    invoke-static {v5}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 68
    const-string v0, "WifiMedium: Using WifiApBeacon"

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->b(Ljava/lang/String;)I

    .line 70
    :cond_2
    new-instance v0, Lcom/google/android/location/copresence/r/a;

    invoke-direct {v0, p1, v3}, Lcom/google/android/location/copresence/r/a;-><init>(Landroid/content/Context;Lcom/google/android/location/copresence/r/ae;)V

    iput-object v0, p0, Lcom/google/android/location/copresence/r/ag;->a:Lcom/google/android/location/copresence/w;

    goto :goto_0

    :cond_3
    move v0, v2

    .line 72
    goto :goto_1

    .line 77
    :cond_4
    sget-object v0, Lcom/google/android/location/copresence/w;->a:Lcom/google/android/location/copresence/w;

    iput-object v0, p0, Lcom/google/android/location/copresence/r/ag;->b:Lcom/google/android/location/copresence/w;

    goto :goto_2

    .line 80
    :cond_5
    const/4 v0, 0x6

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 81
    const-string v0, "WifiMedium: Wifi is not supported!!"

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->e(Ljava/lang/String;)I

    .line 83
    :cond_6
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/location/copresence/r/ag;->e:Lcom/google/android/location/copresence/r/al;

    .line 84
    sget-object v0, Lcom/google/android/location/copresence/w;->a:Lcom/google/android/location/copresence/w;

    iput-object v0, p0, Lcom/google/android/location/copresence/r/ag;->a:Lcom/google/android/location/copresence/w;

    .line 85
    sget-object v0, Lcom/google/android/location/copresence/w;->a:Lcom/google/android/location/copresence/w;

    iput-object v0, p0, Lcom/google/android/location/copresence/r/ag;->b:Lcom/google/android/location/copresence/w;

    .line 86
    sget-object v0, Lcom/google/android/location/copresence/z;->a:Lcom/google/android/location/copresence/z;

    iput-object v0, p0, Lcom/google/android/location/copresence/r/ag;->c:Lcom/google/android/location/copresence/z;

    .line 87
    sget-object v0, Lcom/google/android/location/copresence/z;->a:Lcom/google/android/location/copresence/z;

    iput-object v0, p0, Lcom/google/android/location/copresence/r/ag;->d:Lcom/google/android/location/copresence/z;

    goto :goto_2
.end method

.method public static declared-synchronized a(Landroid/content/Context;)Lcom/google/android/location/copresence/r/ag;
    .locals 2

    .prologue
    .line 35
    const-class v1, Lcom/google/android/location/copresence/r/ag;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/google/android/location/copresence/r/ag;->f:Lcom/google/android/location/copresence/r/ag;

    if-nez v0, :cond_0

    .line 36
    new-instance v0, Lcom/google/android/location/copresence/r/ag;

    invoke-direct {v0, p0}, Lcom/google/android/location/copresence/r/ag;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/google/android/location/copresence/r/ag;->f:Lcom/google/android/location/copresence/r/ag;

    .line 38
    :cond_0
    sget-object v0, Lcom/google/android/location/copresence/r/ag;->f:Lcom/google/android/location/copresence/r/ag;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 35
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private static b(Landroid/content/Context;)Z
    .locals 2

    .prologue
    .line 152
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    const-string v1, "android.hardware.wifi"

    invoke-virtual {v0, v1}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method


# virtual methods
.method public final a()Z
    .locals 1

    .prologue
    .line 100
    iget-object v0, p0, Lcom/google/android/location/copresence/r/ag;->c:Lcom/google/android/location/copresence/z;

    invoke-interface {v0}, Lcom/google/android/location/copresence/z;->b()Z

    move-result v0

    return v0
.end method
