.class public final Lcom/google/android/location/copresence/l/z;
.super Lcom/google/android/location/copresence/l/ac;
.source "SourceFile"


# instance fields
.field private final n:Ljava/lang/String;

.field private final o:[Lcom/google/ac/b/c/bf;

.field private final p:[Ljava/lang/String;

.field private final q:Z

.field private final r:[Lcom/google/ac/b/c/bo;

.field private final s:[Ljava/lang/String;

.field private final t:Z

.field private final u:[Lcom/google/ac/b/c/ah;

.field private final v:[Lcom/google/ac/b/c/bu;

.field private final w:Lcom/google/ac/b/c/as;

.field private final x:Lcom/google/ac/b/c/ac;

.field private final y:Lcom/google/ac/b/c/bz;

.field private final z:Ljava/lang/String;


# direct methods
.method constructor <init>(Landroid/content/Context;Lcom/google/android/location/copresence/ap;Lcom/google/android/location/copresence/a/a;Landroid/os/WorkSource;Ljava/lang/String;[Lcom/google/ac/b/c/bf;[Ljava/lang/String;Z[Lcom/google/ac/b/c/bo;[Ljava/lang/String;Z[Lcom/google/ac/b/c/ah;[Lcom/google/ac/b/c/bu;Lcom/google/ac/b/c/as;Lcom/google/ac/b/c/ac;Lcom/google/ac/b/c/bz;Ljava/lang/String;Lcom/google/android/location/copresence/l/ab;)V
    .locals 9

    .prologue
    .line 71
    new-instance v4, Lcom/google/ac/b/c/da;

    invoke-direct {v4}, Lcom/google/ac/b/c/da;-><init>()V

    const-string v5, "Report"

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v6, p3

    move-object v7, p4

    move-object/from16 v8, p18

    invoke-direct/range {v1 .. v8}, Lcom/google/android/location/copresence/l/ac;-><init>(Landroid/content/Context;Lcom/google/android/location/copresence/ap;Lcom/google/protobuf/nano/j;Ljava/lang/String;Lcom/google/android/location/copresence/a/a;Landroid/os/WorkSource;Lcom/google/android/location/copresence/l/ab;)V

    .line 74
    iput-object p5, p0, Lcom/google/android/location/copresence/l/z;->n:Ljava/lang/String;

    .line 75
    iput-object p6, p0, Lcom/google/android/location/copresence/l/z;->o:[Lcom/google/ac/b/c/bf;

    .line 76
    move-object/from16 v0, p7

    iput-object v0, p0, Lcom/google/android/location/copresence/l/z;->p:[Ljava/lang/String;

    .line 77
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/google/android/location/copresence/l/z;->q:Z

    .line 78
    move-object/from16 v0, p9

    iput-object v0, p0, Lcom/google/android/location/copresence/l/z;->r:[Lcom/google/ac/b/c/bo;

    .line 79
    move-object/from16 v0, p10

    iput-object v0, p0, Lcom/google/android/location/copresence/l/z;->s:[Ljava/lang/String;

    .line 80
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/google/android/location/copresence/l/z;->t:Z

    .line 81
    move-object/from16 v0, p12

    iput-object v0, p0, Lcom/google/android/location/copresence/l/z;->u:[Lcom/google/ac/b/c/ah;

    .line 82
    move-object/from16 v0, p13

    iput-object v0, p0, Lcom/google/android/location/copresence/l/z;->v:[Lcom/google/ac/b/c/bu;

    .line 83
    move-object/from16 v0, p14

    iput-object v0, p0, Lcom/google/android/location/copresence/l/z;->w:Lcom/google/ac/b/c/as;

    .line 84
    move-object/from16 v0, p15

    iput-object v0, p0, Lcom/google/android/location/copresence/l/z;->x:Lcom/google/ac/b/c/ac;

    .line 85
    move-object/from16 v0, p16

    iput-object v0, p0, Lcom/google/android/location/copresence/l/z;->y:Lcom/google/ac/b/c/bz;

    .line 86
    move-object/from16 v0, p17

    iput-object v0, p0, Lcom/google/android/location/copresence/l/z;->z:Ljava/lang/String;

    .line 88
    invoke-static {}, Lcom/google/android/location/copresence/f/a;->a()Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_8

    .line 89
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 90
    if-eqz p6, :cond_0

    array-length v2, p6

    if-gtz v2, :cond_2

    :cond_0
    iget-object v2, p0, Lcom/google/android/location/copresence/l/z;->p:[Ljava/lang/String;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/google/android/location/copresence/l/z;->p:[Ljava/lang/String;

    array-length v2, v2

    if-gtz v2, :cond_2

    :cond_1
    iget-boolean v2, p0, Lcom/google/android/location/copresence/l/z;->q:Z

    if-eqz v2, :cond_3

    .line 93
    :cond_2
    const-string v2, "Publish/Unpublish "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 95
    :cond_3
    if-eqz p9, :cond_4

    move-object/from16 v0, p9

    array-length v2, v0

    if-gtz v2, :cond_6

    :cond_4
    iget-object v2, p0, Lcom/google/android/location/copresence/l/z;->s:[Ljava/lang/String;

    if-eqz v2, :cond_5

    iget-object v2, p0, Lcom/google/android/location/copresence/l/z;->s:[Ljava/lang/String;

    array-length v2, v2

    if-gtz v2, :cond_6

    :cond_5
    iget-boolean v2, p0, Lcom/google/android/location/copresence/l/z;->t:Z

    if-eqz v2, :cond_7

    .line 98
    :cond_6
    const-string v2, "Subscribe/Unsubscribe "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 100
    :cond_7
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/location/copresence/l/z;->l:Ljava/lang/String;

    .line 102
    :cond_8
    return-void
.end method


# virtual methods
.method protected final bridge synthetic a(Lcom/google/protobuf/nano/j;)Lcom/google/ac/b/c/dc;
    .locals 1

    .prologue
    .line 36
    check-cast p1, Lcom/google/ac/b/c/da;

    if-eqz p1, :cond_0

    iget-object v0, p1, Lcom/google/ac/b/c/da;->a:Lcom/google/ac/b/c/dc;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected final synthetic a(Lcom/google/protobuf/nano/j;Lcom/google/protobuf/nano/j;)Ljava/lang/Object;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 36
    check-cast p1, Lcom/google/ac/b/c/cz;

    check-cast p2, Lcom/google/ac/b/c/da;

    invoke-super {p0}, Lcom/google/android/location/copresence/l/ac;->c()Lcom/google/android/location/copresence/a/a;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/location/copresence/a/a;->a:Landroid/accounts/Account;

    iget-object v0, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/location/copresence/l/z;->n:Ljava/lang/String;

    invoke-static {v0, v2, p1, p2}, Lcom/google/android/location/copresence/b/a;->a(Ljava/lang/String;Ljava/lang/String;Lcom/google/ac/b/c/cz;Lcom/google/ac/b/c/da;)V

    if-eqz p2, :cond_5

    iget-object v0, p2, Lcom/google/ac/b/c/da;->b:Lcom/google/ac/b/c/cu;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/location/copresence/l/z;->o:[Lcom/google/ac/b/c/bf;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/location/copresence/l/z;->o:[Lcom/google/ac/b/c/bf;

    array-length v0, v0

    if-lez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/location/copresence/l/z;->o:[Lcom/google/ac/b/c/bf;

    array-length v0, v0

    new-array v2, v0, [Ljava/lang/String;

    move v0, v1

    :goto_0
    iget-object v3, p0, Lcom/google/android/location/copresence/l/z;->o:[Lcom/google/ac/b/c/bf;

    array-length v3, v3

    if-ge v0, v3, :cond_0

    iget-object v3, p0, Lcom/google/android/location/copresence/l/z;->o:[Lcom/google/ac/b/c/bf;

    aget-object v3, v3, v0

    iget-object v3, v3, Lcom/google/ac/b/c/bf;->a:Ljava/lang/String;

    aput-object v3, v2, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/google/android/location/copresence/l/z;->b:Landroid/content/Context;

    iget-object v3, p0, Lcom/google/android/location/copresence/l/z;->o:[Lcom/google/ac/b/c/bf;

    invoke-static {v0, v3, v2}, Lcom/google/android/location/copresence/debug/CopresDatabaseHelper;->a(Landroid/content/Context;[Lcom/google/ac/b/c/bf;[Ljava/lang/String;)V

    :cond_1
    iget-object v0, p2, Lcom/google/ac/b/c/da;->c:Lcom/google/ac/b/c/cw;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/location/copresence/l/z;->r:[Lcom/google/ac/b/c/bo;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/location/copresence/l/z;->r:[Lcom/google/ac/b/c/bo;

    array-length v0, v0

    if-lez v0, :cond_3

    iget-object v0, p0, Lcom/google/android/location/copresence/l/z;->r:[Lcom/google/ac/b/c/bo;

    array-length v0, v0

    new-array v0, v0, [Ljava/lang/String;

    :goto_1
    iget-object v2, p0, Lcom/google/android/location/copresence/l/z;->r:[Lcom/google/ac/b/c/bo;

    array-length v2, v2

    if-ge v1, v2, :cond_2

    iget-object v2, p0, Lcom/google/android/location/copresence/l/z;->r:[Lcom/google/ac/b/c/bo;

    aget-object v2, v2, v1

    iget-object v2, v2, Lcom/google/ac/b/c/bo;->a:Ljava/lang/String;

    aput-object v2, v0, v1

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_2
    iget-object v1, p0, Lcom/google/android/location/copresence/l/z;->b:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/location/copresence/l/z;->r:[Lcom/google/ac/b/c/bo;

    invoke-static {v1, v2, v0}, Lcom/google/android/location/copresence/debug/CopresDatabaseHelper;->a(Landroid/content/Context;[Lcom/google/ac/b/c/bo;[Ljava/lang/String;)V

    :cond_3
    iget-object v0, p2, Lcom/google/ac/b/c/da;->d:Lcom/google/ac/b/c/dg;

    if-eqz v0, :cond_4

    iget-object v0, p2, Lcom/google/ac/b/c/da;->d:Lcom/google/ac/b/c/dg;

    iget-object v0, v0, Lcom/google/ac/b/c/dg;->c:[Lcom/google/ac/b/c/bm;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/google/android/location/copresence/l/z;->b:Landroid/content/Context;

    iget-object v1, p2, Lcom/google/ac/b/c/da;->d:Lcom/google/ac/b/c/dg;

    iget-object v1, v1, Lcom/google/ac/b/c/dg;->c:[Lcom/google/ac/b/c/bm;

    invoke-static {v0, v1}, Lcom/google/android/location/copresence/debug/CopresDatabaseHelper;->a(Landroid/content/Context;[Lcom/google/ac/b/c/bm;)V

    :cond_4
    iget-object v0, p0, Lcom/google/android/location/copresence/l/z;->p:[Ljava/lang/String;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/google/android/location/copresence/l/z;->b:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/location/copresence/l/z;->p:[Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/google/android/location/copresence/debug/CopresDatabaseHelper;->a(Landroid/content/Context;[Ljava/lang/String;)V

    :cond_5
    return-object p2
.end method

.method protected final synthetic b(Lcom/google/protobuf/nano/j;)I
    .locals 1

    .prologue
    .line 36
    check-cast p1, Lcom/google/ac/b/c/da;

    if-eqz p1, :cond_0

    iget-object v0, p1, Lcom/google/ac/b/c/da;->a:Lcom/google/ac/b/c/dc;

    if-eqz v0, :cond_0

    iget-object v0, p1, Lcom/google/ac/b/c/da;->a:Lcom/google/ac/b/c/dc;

    iget-object v0, v0, Lcom/google/ac/b/c/dc;->c:Lcom/google/ac/b/c/bl;

    if-eqz v0, :cond_0

    iget-object v0, p1, Lcom/google/ac/b/c/da;->a:Lcom/google/ac/b/c/dc;

    iget-object v0, v0, Lcom/google/ac/b/c/dc;->c:Lcom/google/ac/b/c/bl;

    iget-object v0, v0, Lcom/google/ac/b/c/bl;->a:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    iget-object v0, p1, Lcom/google/ac/b/c/da;->a:Lcom/google/ac/b/c/dc;

    iget-object v0, v0, Lcom/google/ac/b/c/dc;->c:Lcom/google/ac/b/c/bl;

    iget-object v0, v0, Lcom/google/ac/b/c/bl;->a:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method protected final synthetic b()Lcom/google/protobuf/nano/j;
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 36
    new-instance v5, Lcom/google/ac/b/c/cz;

    invoke-direct {v5}, Lcom/google/ac/b/c/cz;-><init>()V

    iget-object v0, p0, Lcom/google/android/location/copresence/l/z;->n:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/google/android/location/copresence/l/z;->a(Ljava/lang/String;)Lcom/google/ac/b/c/db;

    move-result-object v0

    iput-object v0, v5, Lcom/google/ac/b/c/cz;->a:Lcom/google/ac/b/c/db;

    iget-object v0, p0, Lcom/google/android/location/copresence/l/z;->o:[Lcom/google/ac/b/c/bf;

    if-eqz v0, :cond_8

    iget-object v0, p0, Lcom/google/android/location/copresence/l/z;->o:[Lcom/google/ac/b/c/bf;

    array-length v0, v0

    if-lez v0, :cond_8

    move v0, v1

    :goto_0
    iget-object v3, p0, Lcom/google/android/location/copresence/l/z;->p:[Ljava/lang/String;

    if-eqz v3, :cond_9

    iget-object v3, p0, Lcom/google/android/location/copresence/l/z;->p:[Ljava/lang/String;

    array-length v3, v3

    if-lez v3, :cond_9

    move v3, v1

    :goto_1
    iget-object v4, p0, Lcom/google/android/location/copresence/l/z;->r:[Lcom/google/ac/b/c/bo;

    if-eqz v4, :cond_a

    iget-object v4, p0, Lcom/google/android/location/copresence/l/z;->r:[Lcom/google/ac/b/c/bo;

    array-length v4, v4

    if-lez v4, :cond_a

    move v4, v1

    :goto_2
    iget-object v6, p0, Lcom/google/android/location/copresence/l/z;->s:[Ljava/lang/String;

    if-eqz v6, :cond_b

    iget-object v6, p0, Lcom/google/android/location/copresence/l/z;->s:[Ljava/lang/String;

    array-length v6, v6

    if-lez v6, :cond_b

    :goto_3
    if-nez v0, :cond_0

    if-nez v3, :cond_0

    iget-boolean v2, p0, Lcom/google/android/location/copresence/l/z;->q:Z

    if-eqz v2, :cond_3

    :cond_0
    new-instance v2, Lcom/google/ac/b/c/ct;

    invoke-direct {v2}, Lcom/google/ac/b/c/ct;-><init>()V

    iput-object v2, v5, Lcom/google/ac/b/c/cz;->b:Lcom/google/ac/b/c/ct;

    if-eqz v0, :cond_1

    iget-object v0, v5, Lcom/google/ac/b/c/cz;->b:Lcom/google/ac/b/c/ct;

    iget-object v2, p0, Lcom/google/android/location/copresence/l/z;->o:[Lcom/google/ac/b/c/bf;

    iput-object v2, v0, Lcom/google/ac/b/c/ct;->a:[Lcom/google/ac/b/c/bf;

    :cond_1
    if-eqz v3, :cond_2

    iget-object v0, v5, Lcom/google/ac/b/c/cz;->b:Lcom/google/ac/b/c/ct;

    iget-object v2, p0, Lcom/google/android/location/copresence/l/z;->p:[Ljava/lang/String;

    iput-object v2, v0, Lcom/google/ac/b/c/ct;->b:[Ljava/lang/String;

    :cond_2
    iget-boolean v0, p0, Lcom/google/android/location/copresence/l/z;->q:Z

    if-eqz v0, :cond_3

    iget-object v0, v5, Lcom/google/ac/b/c/cz;->b:Lcom/google/ac/b/c/ct;

    new-instance v2, Lcom/google/ac/b/c/ab;

    invoke-direct {v2}, Lcom/google/ac/b/c/ab;-><init>()V

    iput-object v2, v0, Lcom/google/ac/b/c/ct;->c:Lcom/google/ac/b/c/ab;

    iget-object v0, v5, Lcom/google/ac/b/c/cz;->b:Lcom/google/ac/b/c/ct;

    iget-object v0, v0, Lcom/google/ac/b/c/ct;->c:Lcom/google/ac/b/c/ab;

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iput-object v2, v0, Lcom/google/ac/b/c/ab;->a:Ljava/lang/Integer;

    :cond_3
    if-nez v4, :cond_4

    if-nez v1, :cond_4

    iget-boolean v0, p0, Lcom/google/android/location/copresence/l/z;->t:Z

    if-eqz v0, :cond_7

    :cond_4
    new-instance v0, Lcom/google/ac/b/c/cv;

    invoke-direct {v0}, Lcom/google/ac/b/c/cv;-><init>()V

    iput-object v0, v5, Lcom/google/ac/b/c/cz;->c:Lcom/google/ac/b/c/cv;

    if-eqz v4, :cond_5

    iget-object v0, v5, Lcom/google/ac/b/c/cz;->c:Lcom/google/ac/b/c/cv;

    iget-object v2, p0, Lcom/google/android/location/copresence/l/z;->r:[Lcom/google/ac/b/c/bo;

    iput-object v2, v0, Lcom/google/ac/b/c/cv;->a:[Lcom/google/ac/b/c/bo;

    :cond_5
    if-eqz v1, :cond_6

    iget-object v0, v5, Lcom/google/ac/b/c/cz;->c:Lcom/google/ac/b/c/cv;

    iget-object v1, p0, Lcom/google/android/location/copresence/l/z;->s:[Ljava/lang/String;

    iput-object v1, v0, Lcom/google/ac/b/c/cv;->b:[Ljava/lang/String;

    :cond_6
    iget-boolean v0, p0, Lcom/google/android/location/copresence/l/z;->q:Z

    if-eqz v0, :cond_7

    iget-object v0, v5, Lcom/google/ac/b/c/cz;->c:Lcom/google/ac/b/c/cv;

    new-instance v1, Lcom/google/ac/b/c/ab;

    invoke-direct {v1}, Lcom/google/ac/b/c/ab;-><init>()V

    iput-object v1, v0, Lcom/google/ac/b/c/cv;->c:Lcom/google/ac/b/c/ab;

    iget-object v0, v5, Lcom/google/ac/b/c/cz;->c:Lcom/google/ac/b/c/cv;

    iget-object v0, v0, Lcom/google/ac/b/c/cv;->c:Lcom/google/ac/b/c/ab;

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, v0, Lcom/google/ac/b/c/ab;->a:Ljava/lang/Integer;

    :cond_7
    new-instance v0, Lcom/google/ac/b/c/df;

    invoke-direct {v0}, Lcom/google/ac/b/c/df;-><init>()V

    iput-object v0, v5, Lcom/google/ac/b/c/cz;->d:Lcom/google/ac/b/c/df;

    iget-object v0, v5, Lcom/google/ac/b/c/cz;->d:Lcom/google/ac/b/c/df;

    new-instance v1, Lcom/google/ac/b/c/ag;

    invoke-direct {v1}, Lcom/google/ac/b/c/ag;-><init>()V

    iput-object v1, v0, Lcom/google/ac/b/c/df;->c:Lcom/google/ac/b/c/ag;

    iget-object v0, v5, Lcom/google/ac/b/c/cz;->d:Lcom/google/ac/b/c/df;

    iget-object v0, v0, Lcom/google/ac/b/c/df;->c:Lcom/google/ac/b/c/ag;

    iget-object v1, p0, Lcom/google/android/location/copresence/l/z;->x:Lcom/google/ac/b/c/ac;

    iput-object v1, v0, Lcom/google/ac/b/c/ag;->a:Lcom/google/ac/b/c/ac;

    iget-object v0, v5, Lcom/google/ac/b/c/cz;->d:Lcom/google/ac/b/c/df;

    iget-object v0, v0, Lcom/google/ac/b/c/df;->c:Lcom/google/ac/b/c/ag;

    iget-object v1, p0, Lcom/google/android/location/copresence/l/z;->u:[Lcom/google/ac/b/c/ah;

    iput-object v1, v0, Lcom/google/ac/b/c/ag;->b:[Lcom/google/ac/b/c/ah;

    iget-object v0, v5, Lcom/google/ac/b/c/cz;->d:Lcom/google/ac/b/c/df;

    iget-object v1, p0, Lcom/google/android/location/copresence/l/z;->v:[Lcom/google/ac/b/c/bu;

    iput-object v1, v0, Lcom/google/ac/b/c/df;->a:[Lcom/google/ac/b/c/bu;

    iget-object v0, v5, Lcom/google/ac/b/c/cz;->d:Lcom/google/ac/b/c/df;

    iget-object v1, p0, Lcom/google/android/location/copresence/l/z;->w:Lcom/google/ac/b/c/as;

    iput-object v1, v0, Lcom/google/ac/b/c/df;->b:Lcom/google/ac/b/c/as;

    iget-object v0, v5, Lcom/google/ac/b/c/cz;->d:Lcom/google/ac/b/c/df;

    new-instance v1, Lcom/google/ac/b/c/ak;

    invoke-direct {v1}, Lcom/google/ac/b/c/ak;-><init>()V

    iput-object v1, v0, Lcom/google/ac/b/c/df;->d:Lcom/google/ac/b/c/ak;

    iget-object v0, v5, Lcom/google/ac/b/c/cz;->d:Lcom/google/ac/b/c/df;

    iget-object v0, v0, Lcom/google/ac/b/c/df;->d:Lcom/google/ac/b/c/ak;

    iget-object v1, p0, Lcom/google/android/location/copresence/l/z;->y:Lcom/google/ac/b/c/bz;

    iput-object v1, v0, Lcom/google/ac/b/c/ak;->b:Lcom/google/ac/b/c/bz;

    iget-object v0, v5, Lcom/google/ac/b/c/cz;->d:Lcom/google/ac/b/c/df;

    iget-object v1, p0, Lcom/google/android/location/copresence/l/z;->z:Ljava/lang/String;

    iput-object v1, v0, Lcom/google/ac/b/c/df;->e:Ljava/lang/String;

    invoke-super {p0}, Lcom/google/android/location/copresence/l/ac;->c()Lcom/google/android/location/copresence/a/a;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/location/copresence/a/a;->a:Landroid/accounts/Account;

    iget-object v0, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/location/copresence/l/z;->n:Ljava/lang/String;

    invoke-static {v0, v1, v5}, Lcom/google/android/location/copresence/b/a;->a(Ljava/lang/String;Ljava/lang/String;Lcom/google/ac/b/c/cz;)V

    return-object v5

    :cond_8
    move v0, v2

    goto/16 :goto_0

    :cond_9
    move v3, v2

    goto/16 :goto_1

    :cond_a
    move v4, v2

    goto/16 :goto_2

    :cond_b
    move v1, v2

    goto/16 :goto_3
.end method

.method public final bridge synthetic c()Lcom/google/android/location/copresence/a/a;
    .locals 1

    .prologue
    .line 36
    invoke-super {p0}, Lcom/google/android/location/copresence/l/ac;->c()Lcom/google/android/location/copresence/a/a;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic d()V
    .locals 0

    .prologue
    .line 36
    invoke-super {p0}, Lcom/google/android/location/copresence/l/ac;->d()V

    return-void
.end method
