.class public Lcom/google/android/location/os/real/SdkSpecific19;
.super Lcom/google/android/location/os/real/SdkSpecific18;
.source "SourceFile"


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x13
.end annotation


# static fields
.field private static a:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 30
    const/4 v0, 0x0

    sput-boolean v0, Lcom/google/android/location/os/real/SdkSpecific19;->a:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0}, Lcom/google/android/location/os/real/SdkSpecific18;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/app/AlarmManager;IJJLandroid/app/PendingIntent;Lcom/google/android/location/o/n;)V
    .locals 11

    .prologue
    .line 83
    if-eqz p8, :cond_0

    .line 85
    const/4 v1, 0x2

    const-wide/16 v6, 0x0

    :try_start_0
    invoke-virtual/range {p8 .. p8}, Lcom/google/android/location/o/n;->b()Landroid/os/Parcelable;

    move-result-object v9

    check-cast v9, Landroid/os/WorkSource;

    move-object v0, p1

    move-wide v2, p3

    move-wide/from16 v4, p5

    move-object/from16 v8, p7

    invoke-virtual/range {v0 .. v9}, Landroid/app/AlarmManager;->set(IJJJLandroid/app/PendingIntent;Landroid/os/WorkSource;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 97
    :goto_0
    return-void

    .line 88
    :catch_0
    move-exception v0

    .line 91
    sget-boolean v1, Lcom/google/android/location/i/a;->e:Z

    if-eqz v1, :cond_0

    const-string v1, "SdkSpecific19"

    const-string v2, "Unable to assign WorkSource blame to alarm in setAlarmWindow."

    invoke-static {v1, v2, v0}, Lcom/google/android/location/o/a/a;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 96
    :cond_0
    const/4 v1, 0x2

    move-object v0, p1

    move-wide v2, p3

    move-wide/from16 v4, p5

    move-object/from16 v6, p7

    invoke-virtual/range {v0 .. v6}, Landroid/app/AlarmManager;->setWindow(IJJLandroid/app/PendingIntent;)V

    goto :goto_0
.end method

.method public final a(Landroid/app/AlarmManager;IJLandroid/app/PendingIntent;Lcom/google/android/location/o/n;)V
    .locals 11

    .prologue
    .line 56
    if-eqz p6, :cond_0

    .line 58
    const-wide/16 v4, -0x1

    const-wide/16 v6, 0x0

    :try_start_0
    invoke-virtual/range {p6 .. p6}, Lcom/google/android/location/o/n;->b()Landroid/os/Parcelable;

    move-result-object v9

    check-cast v9, Landroid/os/WorkSource;

    move-object v0, p1

    move v1, p2

    move-wide v2, p3

    move-object/from16 v8, p5

    invoke-virtual/range {v0 .. v9}, Landroid/app/AlarmManager;->set(IJJJLandroid/app/PendingIntent;Landroid/os/WorkSource;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 69
    :goto_0
    return-void

    .line 61
    :catch_0
    move-exception v0

    .line 64
    sget-boolean v1, Lcom/google/android/location/i/a;->e:Z

    if-eqz v1, :cond_0

    const-string v1, "SdkSpecific19"

    const-string v2, "Unable to assign WorkSource blame to alarm."

    invoke-static {v1, v2, v0}, Lcom/google/android/location/o/a/a;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 68
    :cond_0
    invoke-virtual/range {p1 .. p5}, Landroid/app/AlarmManager;->set(IJLandroid/app/PendingIntent;)V

    goto :goto_0
.end method

.method public a(Landroid/content/Context;Lcom/google/android/location/os/real/be;Lcom/google/android/location/o/n;)V
    .locals 3

    .prologue
    .line 36
    const-string v0, "wifi"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/wifi/WifiManager;

    .line 39
    if-eqz p3, :cond_0

    .line 41
    :try_start_0
    invoke-virtual {p3}, Lcom/google/android/location/o/n;->b()Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Landroid/os/WorkSource;

    invoke-virtual {v0, v1}, Landroid/net/wifi/WifiManager;->startScan(Landroid/os/WorkSource;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 51
    :goto_0
    return-void

    .line 46
    :catch_0
    move-exception v1

    sget-boolean v1, Lcom/google/android/location/i/a;->e:Z

    if-eqz v1, :cond_0

    const-string v1, "SdkSpecific19"

    const-string v2, "Unable to assign WorkSource blame to wifi scan."

    invoke-static {v1, v2}, Lcom/google/android/location/o/a/a;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 50
    :cond_0
    invoke-virtual {v0}, Landroid/net/wifi/WifiManager;->startScan()Z

    goto :goto_0
.end method

.method public final a(Landroid/content/Context;Ljava/lang/String;Landroid/location/LocationListener;Landroid/os/Looper;)V
    .locals 8

    .prologue
    const-wide/16 v2, 0x0

    const/4 v1, 0x1

    const/4 v5, 0x0

    const/4 v4, 0x0

    .line 102
    const-string v0, "location"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/location/LocationManager;

    .line 105
    sget-boolean v6, Lcom/google/android/location/os/real/SdkSpecific19;->a:Z

    if-nez v6, :cond_0

    .line 107
    const-string v6, "android.permission.UPDATE_APP_OPS_STATS"

    invoke-virtual {p1, v6}, Landroid/content/Context;->checkCallingOrSelfPermission(Ljava/lang/String;)I

    move-result v6

    if-nez v6, :cond_1

    :goto_0
    sput-boolean v1, Lcom/google/android/location/os/real/SdkSpecific19;->a:Z

    .line 111
    :cond_0
    sget-boolean v1, Lcom/google/android/location/os/real/SdkSpecific19;->a:Z

    if-eqz v1, :cond_2

    .line 113
    const-wide/16 v6, 0x0

    const/4 v1, 0x0

    const/4 v5, 0x0

    :try_start_0
    invoke-static {p2, v6, v7, v1, v5}, Landroid/location/LocationRequest;->createFromDeprecatedProvider(Ljava/lang/String;JFZ)Landroid/location/LocationRequest;

    move-result-object v1

    .line 116
    const/4 v5, 0x1

    invoke-virtual {v1, v5}, Landroid/location/LocationRequest;->setHideFromAppOps(Z)V

    .line 117
    invoke-virtual {v0, v1, p3, p4}, Landroid/location/LocationManager;->requestLocationUpdates(Landroid/location/LocationRequest;Landroid/location/LocationListener;Landroid/os/Looper;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 128
    :goto_1
    return-void

    :cond_1
    move v1, v5

    .line 107
    goto :goto_0

    .line 122
    :catch_0
    move-exception v1

    sget-boolean v1, Lcom/google/android/location/i/a;->e:Z

    if-eqz v1, :cond_2

    const-string v1, "SdkSpecific19"

    const-string v5, "Unable to hide location request from AppOps."

    invoke-static {v1, v5}, Lcom/google/android/location/o/a/a;->d(Ljava/lang/String;Ljava/lang/String;)V

    :cond_2
    move-object v1, p2

    move-object v5, p3

    .line 127
    invoke-virtual/range {v0 .. v5}, Landroid/location/LocationManager;->requestLocationUpdates(Ljava/lang/String;JFLandroid/location/LocationListener;)V

    goto :goto_1
.end method

.method public final a(Landroid/hardware/SensorManager;I)Z
    .locals 1

    .prologue
    .line 132
    invoke-virtual {p1, p2}, Landroid/hardware/SensorManager;->getDefaultSensor(I)Landroid/hardware/Sensor;

    move-result-object v0

    .line 133
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/hardware/Sensor;->getFifoMaxEventCount()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
