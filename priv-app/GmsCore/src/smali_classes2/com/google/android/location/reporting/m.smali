.class public final Lcom/google/android/location/reporting/m;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Lcom/google/android/location/reporting/service/q;

.field private final c:Lcom/google/android/location/reporting/s;

.field private final d:Lcom/google/android/location/reporting/e;

.field private final e:Lcom/google/android/gms/common/util/p;

.field private final f:Lcom/google/android/location/reporting/b/h;

.field private final g:Lcom/google/android/location/reporting/b/c;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/location/reporting/service/q;Lcom/google/android/location/reporting/s;Lcom/google/android/location/reporting/e;)V
    .locals 8

    .prologue
    .line 76
    new-instance v5, Lcom/google/android/gms/common/util/r;

    invoke-direct {v5}, Lcom/google/android/gms/common/util/r;-><init>()V

    new-instance v6, Lcom/google/android/location/reporting/b/i;

    invoke-direct {v6}, Lcom/google/android/location/reporting/b/i;-><init>()V

    new-instance v7, Lcom/google/android/location/reporting/b/c;

    invoke-direct {v7, p1}, Lcom/google/android/location/reporting/b/c;-><init>(Landroid/content/Context;)V

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v7}, Lcom/google/android/location/reporting/m;-><init>(Landroid/content/Context;Lcom/google/android/location/reporting/service/q;Lcom/google/android/location/reporting/s;Lcom/google/android/location/reporting/e;Lcom/google/android/gms/common/util/p;Lcom/google/android/location/reporting/b/h;Lcom/google/android/location/reporting/b/c;)V

    .line 78
    invoke-static {p1}, Lcom/google/android/location/reporting/b/n;->a(Landroid/content/Context;)V

    .line 79
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Lcom/google/android/location/reporting/service/q;Lcom/google/android/location/reporting/s;Lcom/google/android/location/reporting/e;Lcom/google/android/gms/common/util/p;Lcom/google/android/location/reporting/b/h;Lcom/google/android/location/reporting/b/c;)V
    .locals 0

    .prologue
    .line 60
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 61
    iput-object p1, p0, Lcom/google/android/location/reporting/m;->a:Landroid/content/Context;

    .line 62
    iput-object p2, p0, Lcom/google/android/location/reporting/m;->b:Lcom/google/android/location/reporting/service/q;

    .line 63
    iput-object p3, p0, Lcom/google/android/location/reporting/m;->c:Lcom/google/android/location/reporting/s;

    .line 64
    iput-object p4, p0, Lcom/google/android/location/reporting/m;->d:Lcom/google/android/location/reporting/e;

    .line 65
    iput-object p5, p0, Lcom/google/android/location/reporting/m;->e:Lcom/google/android/gms/common/util/p;

    .line 66
    iput-object p6, p0, Lcom/google/android/location/reporting/m;->f:Lcom/google/android/location/reporting/b/h;

    .line 67
    iput-object p7, p0, Lcom/google/android/location/reporting/m;->g:Lcom/google/android/location/reporting/b/c;

    .line 68
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/location/reporting/service/q;Lcom/google/android/location/reporting/config/ReportingConfig;Landroid/location/Location;)Z
    .locals 20

    .prologue
    .line 92
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/location/reporting/service/q;->h()Landroid/location/Location;

    move-result-object v2

    .line 93
    invoke-static {}, Lcom/google/android/location/activity/k;->a()Z

    move-result v4

    .line 94
    move-object/from16 v0, p3

    invoke-static {v0, v2}, Lcom/google/android/location/reporting/t;->a(Landroid/location/Location;Landroid/location/Location;)F

    move-result v3

    const/4 v5, 0x0

    cmpg-float v5, v3, v5

    if-gtz v5, :cond_3

    new-instance v3, Lcom/google/android/location/reporting/u;

    if-nez v4, :cond_2

    const/4 v2, 0x1

    :goto_0
    const/4 v4, 0x1

    invoke-direct {v3, v2, v4}, Lcom/google/android/location/reporting/u;-><init>(ZI)V

    move-object v11, v3

    .line 96
    :goto_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/location/reporting/m;->b:Lcom/google/android/location/reporting/service/q;

    invoke-virtual {v2}, Lcom/google/android/location/reporting/service/q;->b()J

    move-result-wide v2

    const-wide/16 v4, 0x1

    cmp-long v2, v2, v4

    if-gtz v2, :cond_8

    const-string v2, "GCoreUlr"

    const/4 v3, 0x4

    invoke-static {v2, v3}, Lcom/google/android/location/reporting/b/d;->a(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_0

    const-string v2, "GCoreUlr"

    const-string v3, "Not uploading first location since start (in case we\'re in restart loop)"

    invoke-static {v2, v3}, Lcom/google/android/location/reporting/b/d;->c(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    const/4 v13, 0x0

    :cond_1
    :goto_2
    return v13

    .line 94
    :cond_2
    const/4 v2, 0x0

    goto :goto_0

    :cond_3
    move-object/from16 v0, p3

    invoke-virtual {v2, v0}, Landroid/location/Location;->distanceTo(Landroid/location/Location;)F

    move-result v5

    sget-object v2, Lcom/google/android/location/reporting/service/ab;->E:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v2}, Lcom/google/android/gms/common/a/d;->c()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Float;

    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    move-result v2

    cmpl-float v6, v5, v3

    if-lez v6, :cond_4

    cmpl-float v6, v5, v2

    if-lez v6, :cond_4

    new-instance v2, Lcom/google/android/location/reporting/u;

    const/4 v3, 0x1

    const/4 v4, 0x2

    invoke-direct {v2, v3, v4}, Lcom/google/android/location/reporting/u;-><init>(ZI)V

    move-object v11, v2

    goto :goto_1

    :cond_4
    cmpl-float v3, v3, v2

    if-lez v3, :cond_6

    new-instance v3, Lcom/google/android/location/reporting/u;

    if-nez v4, :cond_5

    const/4 v2, 0x1

    :goto_3
    const/4 v4, 0x3

    invoke-direct {v3, v2, v4}, Lcom/google/android/location/reporting/u;-><init>(ZI)V

    move-object v11, v3

    goto :goto_1

    :cond_5
    const/4 v2, 0x0

    goto :goto_3

    :cond_6
    new-instance v3, Lcom/google/android/location/reporting/u;

    cmpl-float v2, v5, v2

    if-lez v2, :cond_7

    const/4 v2, 0x1

    :goto_4
    const/4 v4, 0x4

    invoke-direct {v3, v2, v4}, Lcom/google/android/location/reporting/u;-><init>(ZI)V

    move-object v11, v3

    goto :goto_1

    :cond_7
    const/4 v2, 0x0

    goto :goto_4

    .line 96
    :cond_8
    iget-boolean v4, v11, Lcom/google/android/location/reporting/u;->a:Z

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/location/reporting/m;->e:Lcom/google/android/gms/common/util/p;

    invoke-interface {v2}, Lcom/google/android/gms/common/util/p;->b()J

    move-result-wide v14

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/location/reporting/m;->b:Lcom/google/android/location/reporting/service/q;

    invoke-virtual {v2}, Lcom/google/android/location/reporting/service/q;->i()J

    move-result-wide v2

    sub-long v6, v14, v2

    const-wide/16 v8, 0x0

    cmp-long v2, v2, v8

    if-gtz v2, :cond_b

    sget-object v3, Lcom/google/android/location/reporting/n;->a:Lcom/google/android/location/reporting/n;

    move-object v12, v3

    :goto_5
    invoke-interface {v12, v4}, Lcom/google/android/location/reporting/i;->a(Z)J

    move-result-wide v16

    cmp-long v2, v6, v16

    if-ltz v2, :cond_11

    const/4 v2, 0x1

    move v13, v2

    :goto_6
    invoke-interface {v12, v4}, Lcom/google/android/location/reporting/i;->b(Z)Ljava/lang/String;

    move-result-object v9

    invoke-interface {v12}, Lcom/google/android/location/reporting/i;->b()Ljava/lang/String;

    move-result-object v10

    if-eqz v13, :cond_12

    const-string v2, "GCoreUlr"

    const/4 v3, 0x3

    invoke-static {v2, v3}, Lcom/google/android/location/reporting/b/d;->a(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_9

    const-string v2, "GCoreUlr"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v5, "Sending an intent to LocationReporter, hasMoved: "

    invoke-direct {v3, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", elapsed millis: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", request: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-wide/from16 v0, v16

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ")"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/location/reporting/b/d;->b(Ljava/lang/String;Ljava/lang/String;)V

    :cond_9
    new-instance v2, Lcom/google/android/ulr/ApiRate;

    invoke-interface {v12}, Lcom/google/android/location/reporting/i;->a()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/location/reporting/m;->e:Lcom/google/android/gms/common/util/p;

    invoke-interface {v7}, Lcom/google/android/gms/common/util/p;->a()J

    move-result-wide v18

    invoke-static/range {v18 .. v19}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-static/range {v16 .. v17}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    invoke-direct/range {v2 .. v10}, Lcom/google/android/ulr/ApiRate;-><init>(Ljava/lang/String;Ljava/lang/Long;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/location/reporting/m;->b:Lcom/google/android/location/reporting/service/q;

    move-object/from16 v0, p2

    move-object/from16 v1, p3

    invoke-static {v3, v0, v1, v11, v12}, Lcom/google/android/location/reporting/b/l;->a(Lcom/google/android/location/reporting/service/q;Lcom/google/android/location/reporting/config/ReportingConfig;Landroid/location/Location;Lcom/google/android/location/reporting/u;Lcom/google/android/location/reporting/i;)V

    invoke-static {v2}, Lcom/google/android/location/reporting/c;->a(Lcom/google/android/ulr/ApiRate;)Lcom/google/android/ulr/ApiMetadata;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/location/reporting/m;->d:Lcom/google/android/location/reporting/e;

    invoke-virtual {v2}, Lcom/google/android/ulr/ApiRate;->toString()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p2

    invoke-static {v4, v0, v3, v2}, Lcom/google/android/location/reporting/c;->a(Lcom/google/android/location/reporting/e;Lcom/google/android/location/reporting/config/ReportingConfig;Lcom/google/android/ulr/ApiMetadata;Ljava/lang/String;)V

    if-eqz p3, :cond_a

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/location/reporting/m;->b:Lcom/google/android/location/reporting/service/q;

    move-object/from16 v0, p3

    invoke-virtual {v2, v0}, Lcom/google/android/location/reporting/service/q;->a(Landroid/location/Location;)V

    :cond_a
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/location/reporting/m;->b:Lcom/google/android/location/reporting/service/q;

    invoke-virtual {v2, v14, v15}, Lcom/google/android/location/reporting/service/q;->b(J)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/location/reporting/m;->f:Lcom/google/android/location/reporting/b/h;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/location/reporting/m;->a:Landroid/content/Context;

    invoke-interface {v2, v3}, Lcom/google/android/location/reporting/b/h;->a(Landroid/content/Context;)V

    goto/16 :goto_2

    :cond_b
    if-eqz v4, :cond_d

    sget-object v2, Lcom/google/android/location/reporting/n;->c:Lcom/google/android/location/reporting/n;

    :goto_7
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/location/reporting/m;->c:Lcom/google/android/location/reporting/s;

    invoke-virtual {v3}, Lcom/google/android/location/reporting/s;->c()Ljava/util/List;

    move-result-object v3

    invoke-interface {v5, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/location/reporting/m;->b:Lcom/google/android/location/reporting/service/q;

    invoke-virtual {v3}, Lcom/google/android/location/reporting/service/q;->f()Lcom/google/android/gms/location/ActivityRecognitionResult;

    move-result-object v3

    if-eqz v3, :cond_e

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/location/reporting/m;->b:Lcom/google/android/location/reporting/service/q;

    invoke-virtual {v3}, Lcom/google/android/location/reporting/service/q;->f()Lcom/google/android/gms/location/ActivityRecognitionResult;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/gms/location/ActivityRecognitionResult;->a()Lcom/google/android/gms/location/DetectedActivity;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/gms/location/DetectedActivity;->a()I

    move-result v3

    if-nez v3, :cond_e

    const/4 v3, 0x1

    :goto_8
    if-eqz v3, :cond_f

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/location/reporting/m;->g:Lcom/google/android/location/reporting/b/c;

    invoke-virtual {v3}, Lcom/google/android/location/reporting/b/c;->a()Z

    move-result v3

    if-eqz v3, :cond_f

    const/4 v3, 0x1

    :goto_9
    if-eqz v3, :cond_c

    sget-object v3, Lcom/google/android/location/reporting/n;->b:Lcom/google/android/location/reporting/n;

    invoke-interface {v5, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_c
    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    move-object v3, v2

    :goto_a
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_10

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/location/reporting/i;

    invoke-interface {v2, v4}, Lcom/google/android/location/reporting/i;->a(Z)J

    move-result-wide v8

    invoke-interface {v3, v4}, Lcom/google/android/location/reporting/i;->a(Z)J

    move-result-wide v12

    cmp-long v8, v8, v12

    if-gez v8, :cond_13

    :goto_b
    move-object v3, v2

    goto :goto_a

    :cond_d
    sget-object v2, Lcom/google/android/location/reporting/n;->d:Lcom/google/android/location/reporting/n;

    goto :goto_7

    :cond_e
    const/4 v3, 0x0

    goto :goto_8

    :cond_f
    const/4 v3, 0x0

    goto :goto_9

    :cond_10
    move-object v12, v3

    goto/16 :goto_5

    :cond_11
    const/4 v2, 0x0

    move v13, v2

    goto/16 :goto_6

    :cond_12
    const-string v2, "GCoreUlr"

    const/4 v3, 0x4

    invoke-static {v2, v3}, Lcom/google/android/location/reporting/b/d;->a(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_1

    const-string v2, "GCoreUlr"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v5, "Not calling LocationReporter, hasMoved: "

    invoke-direct {v3, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", elapsed millis: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", request: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-wide/from16 v0, v16

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ")"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/location/reporting/b/d;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2

    :cond_13
    move-object v2, v3

    goto :goto_b
.end method
