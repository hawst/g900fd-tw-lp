.class public final Lcom/google/android/location/copresence/o/k;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static g:Lcom/google/android/location/copresence/o/k;


# instance fields
.field public final a:Lcom/google/android/location/copresence/o/d;

.field public final b:Lcom/google/android/location/copresence/ap;

.field private final c:Lcom/google/android/location/copresence/o/a;

.field private final d:Lcom/google/android/location/copresence/e/d;

.field private final e:Ljava/util/HashSet;

.field private final f:Ljava/lang/Runnable;


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .locals 4

    .prologue
    .line 100
    invoke-static {p1}, Lcom/google/android/location/copresence/ap;->a(Landroid/content/Context;)Lcom/google/android/location/copresence/ap;

    move-result-object v0

    new-instance v1, Lcom/google/android/location/copresence/o/d;

    invoke-direct {v1, p1}, Lcom/google/android/location/copresence/o/d;-><init>(Landroid/content/Context;)V

    new-instance v2, Lcom/google/android/location/copresence/o/a;

    invoke-direct {v2}, Lcom/google/android/location/copresence/o/a;-><init>()V

    invoke-static {p1}, Lcom/google/android/location/copresence/e/d;->a(Landroid/content/Context;)Lcom/google/android/location/copresence/e/d;

    move-result-object v3

    invoke-direct {p0, v0, v1, v2, v3}, Lcom/google/android/location/copresence/o/k;-><init>(Lcom/google/android/location/copresence/ap;Lcom/google/android/location/copresence/o/d;Lcom/google/android/location/copresence/o/a;Lcom/google/android/location/copresence/e/d;)V

    .line 105
    return-void
.end method

.method private constructor <init>(Lcom/google/android/location/copresence/ap;Lcom/google/android/location/copresence/o/d;Lcom/google/android/location/copresence/o/a;Lcom/google/android/location/copresence/e/d;)V
    .locals 1

    .prologue
    .line 112
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 56
    new-instance v0, Lcom/google/android/location/copresence/o/l;

    invoke-direct {v0, p0}, Lcom/google/android/location/copresence/o/l;-><init>(Lcom/google/android/location/copresence/o/k;)V

    iput-object v0, p0, Lcom/google/android/location/copresence/o/k;->f:Ljava/lang/Runnable;

    .line 113
    iput-object p1, p0, Lcom/google/android/location/copresence/o/k;->b:Lcom/google/android/location/copresence/ap;

    .line 114
    iput-object p2, p0, Lcom/google/android/location/copresence/o/k;->a:Lcom/google/android/location/copresence/o/d;

    .line 115
    iput-object p3, p0, Lcom/google/android/location/copresence/o/k;->c:Lcom/google/android/location/copresence/o/a;

    .line 116
    iput-object p4, p0, Lcom/google/android/location/copresence/o/k;->d:Lcom/google/android/location/copresence/e/d;

    .line 117
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/location/copresence/o/k;->e:Ljava/util/HashSet;

    .line 120
    iget-object v0, p0, Lcom/google/android/location/copresence/o/k;->c:Lcom/google/android/location/copresence/o/a;

    invoke-virtual {p0, v0}, Lcom/google/android/location/copresence/o/k;->a(Lcom/google/android/location/copresence/o/o;)V

    .line 121
    return-void
.end method

.method public static declared-synchronized a(Landroid/content/Context;)Lcom/google/android/location/copresence/o/k;
    .locals 2

    .prologue
    .line 93
    const-class v1, Lcom/google/android/location/copresence/o/k;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/google/android/location/copresence/o/k;->g:Lcom/google/android/location/copresence/o/k;

    if-nez v0, :cond_0

    .line 94
    new-instance v0, Lcom/google/android/location/copresence/o/k;

    invoke-direct {v0, p0}, Lcom/google/android/location/copresence/o/k;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/google/android/location/copresence/o/k;->g:Lcom/google/android/location/copresence/o/k;

    .line 96
    :cond_0
    sget-object v0, Lcom/google/android/location/copresence/o/k;->g:Lcom/google/android/location/copresence/o/k;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 93
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private a(JII)V
    .locals 3

    .prologue
    .line 140
    iget-object v0, p0, Lcom/google/android/location/copresence/o/k;->e:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/copresence/o/o;

    .line 141
    invoke-interface {v0, p1, p2, p3, p4}, Lcom/google/android/location/copresence/o/o;->a(JII)V

    goto :goto_0

    .line 144
    :cond_0
    return-void
.end method

.method private static a(Lcom/google/android/gms/location/copresence/internal/i;I)V
    .locals 1

    .prologue
    .line 260
    if-nez p0, :cond_0

    .line 269
    :goto_0
    return-void

    .line 265
    :cond_0
    :try_start_0
    new-instance v0, Lcom/google/android/gms/common/api/Status;

    invoke-direct {v0, p1}, Lcom/google/android/gms/common/api/Status;-><init>(I)V

    invoke-interface {p0, v0}, Lcom/google/android/gms/location/copresence/internal/i;->a(Lcom/google/android/gms/common/api/Status;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 269
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method private a(Lcom/google/android/gms/location/copresence/internal/i;Lcom/google/android/location/copresence/o/p;)V
    .locals 8

    .prologue
    .line 838
    if-nez p1, :cond_1

    .line 845
    :cond_0
    return-void

    .line 842
    :cond_1
    iget-object v0, p2, Lcom/google/android/location/copresence/o/p;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/copresence/o/q;

    .line 843
    iget-object v3, p0, Lcom/google/android/location/copresence/o/k;->c:Lcom/google/android/location/copresence/o/a;

    iget-wide v4, v0, Lcom/google/android/location/copresence/o/q;->a:J

    iget v6, v0, Lcom/google/android/location/copresence/o/q;->b:I

    iget v7, v0, Lcom/google/android/location/copresence/o/q;->d:I

    invoke-static {p1}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, v3, Lcom/google/android/location/copresence/o/a;->a:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/copresence/o/b;

    if-nez v0, :cond_3

    new-instance v0, Lcom/google/android/location/copresence/o/b;

    invoke-direct {v0, v3, p1}, Lcom/google/android/location/copresence/o/b;-><init>(Lcom/google/android/location/copresence/o/a;Lcom/google/android/gms/location/copresence/internal/i;)V

    iget-object v1, v3, Lcom/google/android/location/copresence/o/a;->a:Ljava/util/HashMap;

    invoke-virtual {v1, p1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-object v1, v0

    :goto_1
    invoke-virtual {v1, v4, v5, v6, v7}, Lcom/google/android/location/copresence/o/b;->a(JII)V

    iget-object v0, v3, Lcom/google/android/location/copresence/o/a;->b:Ljava/util/HashMap;

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v0, v6}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/copresence/o/b;

    if-nez v0, :cond_2

    iget-object v0, v3, Lcom/google/android/location/copresence/o/a;->b:Ljava/util/HashMap;

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v0, v3, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_2
    iget-object v0, v0, Lcom/google/android/location/copresence/o/b;->a:Lcom/google/android/gms/location/copresence/internal/i;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "A different callback object is associated with operation id="

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/e;->a(ZLjava/lang/Object;)V

    goto :goto_0

    :cond_3
    move-object v1, v0

    goto :goto_1
.end method

.method private a(Lcom/google/android/location/copresence/o/p;)V
    .locals 8

    .prologue
    .line 895
    iget-object v0, p1, Lcom/google/android/location/copresence/o/p;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :cond_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/copresence/o/q;

    .line 896
    iget-wide v2, v0, Lcom/google/android/location/copresence/o/q;->a:J

    iget v4, v0, Lcom/google/android/location/copresence/o/q;->b:I

    iget-object v5, v0, Lcom/google/android/location/copresence/o/q;->c:Ljava/lang/String;

    iget v6, v0, Lcom/google/android/location/copresence/o/q;->d:I

    iget-object v0, p0, Lcom/google/android/location/copresence/o/k;->e:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/location/copresence/o/o;

    invoke-interface/range {v1 .. v6}, Lcom/google/android/location/copresence/o/o;->a(JILjava/lang/String;I)V

    goto :goto_0

    .line 902
    :cond_1
    return-void
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/location/copresence/o/p;)V
    .locals 7

    .prologue
    .line 849
    iget-object v0, p3, Lcom/google/android/location/copresence/o/p;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_0
    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/copresence/o/q;

    .line 850
    iget v1, v0, Lcom/google/android/location/copresence/o/q;->b:I

    packed-switch v1, :pswitch_data_0

    goto :goto_0

    .line 853
    :pswitch_0
    iget-object v1, v0, Lcom/google/android/location/copresence/o/q;->e:Lcom/google/android/gms/location/copresence/internal/SubscribeOperation;

    if-eqz v1, :cond_0

    iget v0, v1, Lcom/google/android/gms/location/copresence/internal/SubscribeOperation;->a:I

    packed-switch v0, :pswitch_data_1

    const/4 v0, 0x6

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "CopresenceStateManager: Unknown subscribe operation."

    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1}, Ljava/lang/IllegalStateException;-><init>()V

    invoke-static {v0, v1}, Lcom/google/android/location/copresence/ag;->a(Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    :pswitch_1
    iget-object v0, p0, Lcom/google/android/location/copresence/o/k;->d:Lcom/google/android/location/copresence/e/d;

    iget-object v3, v1, Lcom/google/android/gms/location/copresence/internal/SubscribeOperation;->d:Lcom/google/android/gms/location/copresence/internal/o;

    iget-object v4, v1, Lcom/google/android/gms/location/copresence/internal/SubscribeOperation;->c:Lcom/google/android/gms/location/copresence/MessageFilter;

    iget-object v5, v1, Lcom/google/android/gms/location/copresence/internal/SubscribeOperation;->b:Lcom/google/android/gms/location/copresence/internal/StrategyImpl;

    move-object v1, p1

    move-object v2, p2

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/location/copresence/e/d;->a(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/location/copresence/internal/o;Lcom/google/android/gms/location/copresence/MessageFilter;Lcom/google/android/gms/location/copresence/u;)Lcom/google/android/location/copresence/e/f;

    goto :goto_0

    :pswitch_2
    iget-object v0, p0, Lcom/google/android/location/copresence/o/k;->d:Lcom/google/android/location/copresence/e/d;

    iget-object v3, v1, Lcom/google/android/gms/location/copresence/internal/SubscribeOperation;->e:Landroid/app/PendingIntent;

    iget-object v4, v1, Lcom/google/android/gms/location/copresence/internal/SubscribeOperation;->c:Lcom/google/android/gms/location/copresence/MessageFilter;

    iget-object v5, v1, Lcom/google/android/gms/location/copresence/internal/SubscribeOperation;->b:Lcom/google/android/gms/location/copresence/internal/StrategyImpl;

    move-object v1, p1

    move-object v2, p2

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/location/copresence/e/d;->a(Ljava/lang/String;Ljava/lang/String;Landroid/app/PendingIntent;Lcom/google/android/gms/location/copresence/MessageFilter;Lcom/google/android/gms/location/copresence/u;)Lcom/google/android/location/copresence/e/f;

    goto :goto_0

    .line 857
    :pswitch_3
    iget-object v0, v0, Lcom/google/android/location/copresence/o/q;->f:Lcom/google/android/gms/location/copresence/internal/UnsubscribeOperation;

    iget-object v1, p0, Lcom/google/android/location/copresence/o/k;->d:Lcom/google/android/location/copresence/e/d;

    invoke-virtual {v1, p1, p2, v0}, Lcom/google/android/location/copresence/e/d;->b(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/location/copresence/internal/UnsubscribeOperation;)V

    goto :goto_0

    .line 864
    :cond_1
    return-void

    .line 850
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_3
    .end packed-switch

    .line 853
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private a(Ljava/util/List;)V
    .locals 6

    .prologue
    .line 794
    iget-object v0, p0, Lcom/google/android/location/copresence/o/k;->a:Lcom/google/android/location/copresence/o/d;

    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 795
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/google/android/location/copresence/o/k;->b:Lcom/google/android/location/copresence/ap;

    invoke-virtual {v0}, Lcom/google/android/location/copresence/ap;->b()Lcom/google/android/location/copresence/c;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/location/copresence/o/k;->f:Ljava/lang/Runnable;

    iget-object v2, v0, Lcom/google/android/location/copresence/c;->d:Lcom/google/android/location/copresence/ap;

    invoke-virtual {v2}, Lcom/google/android/location/copresence/ap;->c()V

    iget-object v0, v0, Lcom/google/android/location/copresence/c;->b:Ljava/util/Map;

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/location/copresence/o/k;->b:Lcom/google/android/location/copresence/ap;

    invoke-virtual {v0}, Lcom/google/android/location/copresence/ap;->b()Lcom/google/android/location/copresence/c;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/location/copresence/o/k;->f:Ljava/lang/Runnable;

    invoke-static {}, Lcom/google/android/location/copresence/f/a;->d()Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/location/copresence/c;->a(Ljava/lang/Runnable;J)V

    .line 796
    :cond_1
    return-void

    .line 794
    :cond_2
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    const-string v2, "deletable"

    const/4 v3, 0x1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    iget-object v0, v0, Lcom/google/android/location/copresence/o/d;->b:Lcom/google/android/location/copresence/o/g;

    invoke-virtual {v0}, Lcom/google/android/location/copresence/o/g;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    const-string v2, "operations"

    invoke-static {}, Lcom/google/android/location/copresence/q/af;->a()Lcom/google/android/location/copresence/q/ak;

    move-result-object v3

    const-string v4, "_id"

    const/4 v5, 0x0

    invoke-virtual {v3, v4, p1, v5}, Lcom/google/android/location/copresence/q/ak;->a(Ljava/lang/String;Ljava/util/List;Z)Lcom/google/android/location/copresence/q/ak;

    move-result-object v3

    iget-object v3, v3, Lcom/google/android/location/copresence/q/ak;->a:Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v0, v2, v1, v3, v4}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    goto :goto_0
.end method

.method private a(Ljava/lang/String;Lcom/google/android/location/copresence/a/a;Lcom/google/android/gms/location/copresence/internal/SubscribeOperation;Lcom/google/android/location/copresence/o/p;)Z
    .locals 11

    .prologue
    const/4 v1, 0x0

    .line 368
    iget-object v0, p0, Lcom/google/android/location/copresence/o/k;->b:Lcom/google/android/location/copresence/ap;

    invoke-virtual {v0}, Lcom/google/android/location/copresence/ap;->c()V

    .line 369
    const/4 v0, 0x3

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 370
    const-string v0, "CopresenceStateManager: add subscribe operation"

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->b(Ljava/lang/String;)I

    .line 374
    :cond_0
    iget-object v0, p3, Lcom/google/android/gms/location/copresence/internal/SubscribeOperation;->c:Lcom/google/android/gms/location/copresence/MessageFilter;

    invoke-virtual {v0}, Lcom/google/android/gms/location/copresence/MessageFilter;->b()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/location/copresence/MessageFilter$FilterPrimitive;

    iget-object v3, v0, Lcom/google/android/gms/location/copresence/MessageFilter$FilterPrimitive;->f:Ljava/lang/String;

    if-nez v3, :cond_1

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "s#"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    iput-object v3, v0, Lcom/google/android/gms/location/copresence/MessageFilter$FilterPrimitive;->f:Ljava/lang/String;

    goto :goto_0

    .line 376
    :cond_2
    iget-object v0, p0, Lcom/google/android/location/copresence/o/k;->a:Lcom/google/android/location/copresence/o/d;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/location/copresence/o/d;->a(Ljava/lang/String;Lcom/google/android/location/copresence/a/a;Lcom/google/android/gms/location/copresence/internal/SubscribeOperation;)Ljava/util/List;

    move-result-object v9

    .line 378
    if-nez v9, :cond_3

    .line 391
    :goto_1
    return v1

    .line 383
    :cond_3
    iget-object v0, p3, Lcom/google/android/gms/location/copresence/internal/SubscribeOperation;->c:Lcom/google/android/gms/location/copresence/MessageFilter;

    invoke-virtual {v0}, Lcom/google/android/gms/location/copresence/MessageFilter;->c()Ljava/util/List;

    move-result-object v10

    move v8, v1

    .line 384
    :goto_2
    invoke-interface {v10}, Ljava/util/List;->size()I

    move-result v0

    if-ge v8, v0, :cond_4

    .line 385
    invoke-interface {v9, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    .line 386
    invoke-interface {v10, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    .line 388
    const/4 v4, 0x2

    const/4 v7, 0x0

    move-object v1, p4

    move-object v6, p3

    invoke-virtual/range {v1 .. v7}, Lcom/google/android/location/copresence/o/p;->a(JILjava/lang/String;Lcom/google/android/gms/location/copresence/internal/SubscribeOperation;Lcom/google/android/gms/location/copresence/internal/UnsubscribeOperation;)V

    .line 384
    add-int/lit8 v0, v8, 0x1

    move v8, v0

    goto :goto_2

    .line 391
    :cond_4
    const/4 v1, 0x1

    goto :goto_1
.end method

.method private a(Ljava/lang/String;Lcom/google/android/location/copresence/a/a;Lcom/google/android/gms/location/copresence/internal/UnpublishOperation;Lcom/google/android/location/copresence/o/p;)Z
    .locals 16

    .prologue
    .line 308
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/location/copresence/o/k;->b:Lcom/google/android/location/copresence/ap;

    invoke-virtual {v2}, Lcom/google/android/location/copresence/ap;->c()V

    .line 310
    const/4 v2, 0x3

    invoke-static {v2}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 311
    const-string v2, "CopresenceStateManager: add unpublish operation"

    invoke-static {v2}, Lcom/google/android/location/copresence/ag;->b(Ljava/lang/String;)I

    .line 314
    :cond_0
    move-object/from16 v0, p3

    iget v2, v0, Lcom/google/android/gms/location/copresence/internal/UnpublishOperation;->a:I

    packed-switch v2, :pswitch_data_0

    const/4 v2, 0x6

    invoke-static {v2}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v2

    if-eqz v2, :cond_1

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "CopresenceStateManager: Unknown unpublish operation type: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p3

    iget v3, v0, Lcom/google/android/gms/location/copresence/internal/UnpublishOperation;->a:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/location/copresence/ag;->e(Ljava/lang/String;)I

    :cond_1
    const/4 v2, 0x0

    .line 316
    :goto_0
    if-nez v2, :cond_2

    .line 317
    const/4 v2, 0x0

    .line 329
    :goto_1
    return v2

    .line 314
    :pswitch_0
    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    move-object/from16 v0, p3

    iget-object v3, v0, Lcom/google/android/gms/location/copresence/internal/UnpublishOperation;->b:Ljava/lang/String;

    invoke-interface {v2, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :pswitch_1
    invoke-virtual/range {p0 .. p2}, Lcom/google/android/location/copresence/o/k;->a(Ljava/lang/String;Lcom/google/android/location/copresence/a/a;)Lcom/google/android/location/copresence/o/m;

    move-result-object v2

    if-eqz v2, :cond_1

    iget-object v2, v2, Lcom/google/android/location/copresence/o/m;->a:Ljava/util/HashMap;

    invoke-virtual {v2}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v2

    goto :goto_0

    .line 320
    :cond_2
    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :goto_2
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_9

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    .line 321
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/location/copresence/o/k;->a:Lcom/google/android/location/copresence/o/d;

    invoke-static {v7}, Lcom/google/android/gms/location/copresence/internal/UnpublishOperation;->a(Ljava/lang/String;)Lcom/google/android/gms/location/copresence/internal/UnpublishOperation;

    move-result-object v4

    iget v2, v4, Lcom/google/android/gms/location/copresence/internal/UnpublishOperation;->a:I

    const/4 v5, 0x1

    if-ne v2, v5, :cond_4

    const/4 v2, 0x1

    :goto_3
    const-string v5, "insert only supports unpublish operations of type TYPE_UNPUBLISH_ID"

    invoke-static {v2, v5}, Lcom/google/android/gms/common/internal/bx;->b(ZLjava/lang/Object;)V

    move-object/from16 v0, p1

    invoke-virtual {v3, v0}, Lcom/google/android/location/copresence/o/d;->c(Ljava/lang/String;)J

    move-result-wide v8

    const-wide/16 v12, -0x1

    cmp-long v2, v8, v12

    if-nez v2, :cond_5

    const/4 v2, 0x6

    invoke-static {v2}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v2

    if-eqz v2, :cond_3

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "CopresenceStateDatabase: Could not get package id for packageName="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p1

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " for "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/location/copresence/ag;->e(Ljava/lang/String;)I

    :cond_3
    const-wide/16 v4, -0x1

    .line 323
    :goto_4
    const-wide/16 v2, -0x1

    cmp-long v2, v4, v2

    if-nez v2, :cond_8

    .line 324
    const/4 v2, 0x0

    goto :goto_1

    .line 321
    :cond_4
    const/4 v2, 0x0

    goto :goto_3

    :cond_5
    move-object/from16 v0, p2

    iget-object v2, v0, Lcom/google/android/location/copresence/a/a;->a:Landroid/accounts/Account;

    iget-object v2, v2, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v3, v2}, Lcom/google/android/location/copresence/o/d;->d(Ljava/lang/String;)J

    move-result-wide v12

    const-wide/16 v14, -0x1

    cmp-long v2, v12, v14

    if-nez v2, :cond_7

    const/4 v2, 0x6

    invoke-static {v2}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v2

    if-eqz v2, :cond_6

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "CopresenceStateDatabase: Could not get account id for account="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static/range {p2 .. p2}, Lcom/google/android/location/copresence/ag;->a(Lcom/google/android/location/copresence/a/a;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " for "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/location/copresence/ag;->e(Ljava/lang/String;)I

    :cond_6
    const-wide/16 v4, -0x1

    goto :goto_4

    :cond_7
    iget-object v2, v3, Lcom/google/android/location/copresence/o/d;->a:Lcom/google/android/gms/common/util/p;

    invoke-interface {v2}, Lcom/google/android/gms/common/util/p;->a()J

    move-result-wide v14

    iget-object v2, v3, Lcom/google/android/location/copresence/o/d;->b:Lcom/google/android/location/copresence/o/g;

    invoke-virtual {v2}, Lcom/google/android/location/copresence/o/g;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    new-instance v3, Landroid/content/ContentValues;

    invoke-direct {v3}, Landroid/content/ContentValues;-><init>()V

    const-string v5, "op_code"

    const/4 v6, 0x1

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v3, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v5, "write_time_millis"

    invoke-static {v14, v15}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v3, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v5, "package_id"

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v3, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v5, "account_id"

    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v3, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v5, "client_id"

    iget-object v4, v4, Lcom/google/android/gms/location/copresence/internal/UnpublishOperation;->b:Ljava/lang/String;

    invoke-virtual {v3, v5, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v4, "network_status"

    const/4 v5, 0x0

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v4, "network_last_update_millis"

    invoke-static {v14, v15}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v4, "deletable"

    const/4 v5, 0x0

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v4, "operations"

    const-string v5, "op_code"

    invoke-virtual {v2, v4, v5, v3}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v4

    goto/16 :goto_4

    .line 326
    :cond_8
    const/4 v6, 0x1

    const/4 v8, 0x0

    const/4 v9, 0x0

    move-object/from16 v3, p4

    invoke-virtual/range {v3 .. v9}, Lcom/google/android/location/copresence/o/p;->a(JILjava/lang/String;Lcom/google/android/gms/location/copresence/internal/SubscribeOperation;Lcom/google/android/gms/location/copresence/internal/UnsubscribeOperation;)V

    goto/16 :goto_2

    .line 329
    :cond_9
    const/4 v2, 0x1

    goto/16 :goto_1

    .line 314
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private a(Ljava/lang/String;Lcom/google/android/location/copresence/a/a;Lcom/google/android/gms/location/copresence/internal/UnsubscribeOperation;Lcom/google/android/location/copresence/o/p;)Z
    .locals 11

    .prologue
    .line 413
    iget-object v0, p0, Lcom/google/android/location/copresence/o/k;->b:Lcom/google/android/location/copresence/ap;

    invoke-virtual {v0}, Lcom/google/android/location/copresence/ap;->c()V

    .line 414
    const/4 v0, 0x3

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 415
    const-string v0, "CopresenceStateManager: add unsubscribe operation"

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->b(Ljava/lang/String;)I

    .line 418
    :cond_0
    iget v0, p3, Lcom/google/android/gms/location/copresence/internal/UnsubscribeOperation;->a:I

    packed-switch v0, :pswitch_data_0

    const/4 v0, 0x6

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "CopresenceStateManager: Unknown unsubscribe type="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v1, p3, Lcom/google/android/gms/location/copresence/internal/UnsubscribeOperation;->a:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->e(Ljava/lang/String;)I

    :cond_1
    const/4 v0, 0x0

    move-object v9, v0

    .line 420
    :goto_0
    if-nez v9, :cond_3

    .line 421
    const/4 v0, 0x0

    .line 444
    :goto_1
    return v0

    .line 418
    :pswitch_0
    iget-object v0, p0, Lcom/google/android/location/copresence/o/k;->d:Lcom/google/android/location/copresence/e/d;

    iget-object v1, p2, Lcom/google/android/location/copresence/a/a;->a:Landroid/accounts/Account;

    iget-object v1, v1, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v0, p1, v1, p3}, Lcom/google/android/location/copresence/e/d;->a(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/location/copresence/internal/UnsubscribeOperation;)Lcom/google/android/location/copresence/e/f;

    move-result-object v0

    if-nez v0, :cond_2

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    move-object v9, v0

    goto :goto_0

    :cond_2
    iget-object v0, v0, Lcom/google/android/location/copresence/e/f;->b:Lcom/google/android/gms/location/copresence/MessageFilter;

    invoke-virtual {v0}, Lcom/google/android/gms/location/copresence/MessageFilter;->c()Ljava/util/List;

    move-result-object v0

    move-object v9, v0

    goto :goto_0

    :pswitch_1
    invoke-virtual {p0, p1, p2}, Lcom/google/android/location/copresence/o/k;->b(Ljava/lang/String;Lcom/google/android/location/copresence/a/a;)Lcom/google/android/location/copresence/o/m;

    move-result-object v1

    if-eqz v1, :cond_1

    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, v1, Lcom/google/android/location/copresence/o/m;->a:Ljava/util/HashMap;

    invoke-virtual {v1}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    move-object v9, v0

    goto :goto_0

    .line 423
    :cond_3
    invoke-interface {v9}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 425
    const/4 v0, 0x1

    goto :goto_1

    .line 428
    :cond_4
    invoke-virtual {p3, v9}, Lcom/google/android/gms/location/copresence/internal/UnsubscribeOperation;->a(Ljava/util/List;)V

    .line 429
    iget-object v0, p0, Lcom/google/android/location/copresence/o/k;->a:Lcom/google/android/location/copresence/o/d;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/location/copresence/o/d;->a(Ljava/lang/String;Lcom/google/android/location/copresence/a/a;Lcom/google/android/gms/location/copresence/internal/UnsubscribeOperation;)Ljava/util/List;

    move-result-object v10

    .line 431
    if-nez v10, :cond_5

    .line 433
    const/4 v0, 0x0

    goto :goto_1

    .line 437
    :cond_5
    const/4 v0, 0x0

    move v8, v0

    :goto_2
    invoke-interface {v9}, Ljava/util/List;->size()I

    move-result v0

    if-ge v8, v0, :cond_6

    .line 438
    invoke-interface {v10, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    .line 439
    invoke-interface {v9, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    .line 441
    const/4 v4, 0x3

    const/4 v6, 0x0

    move-object v1, p4

    move-object v7, p3

    invoke-virtual/range {v1 .. v7}, Lcom/google/android/location/copresence/o/p;->a(JILjava/lang/String;Lcom/google/android/gms/location/copresence/internal/SubscribeOperation;Lcom/google/android/gms/location/copresence/internal/UnsubscribeOperation;)V

    .line 437
    add-int/lit8 v0, v8, 0x1

    move v8, v0

    goto :goto_2

    .line 444
    :cond_6
    const/4 v0, 0x1

    goto :goto_1

    .line 418
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method public final a(Ljava/lang/String;Lcom/google/android/location/copresence/a/a;)Lcom/google/android/location/copresence/o/m;
    .locals 2

    .prologue
    .line 711
    iget-object v0, p0, Lcom/google/android/location/copresence/o/k;->b:Lcom/google/android/location/copresence/ap;

    invoke-virtual {v0}, Lcom/google/android/location/copresence/ap;->c()V

    .line 712
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {p0, p1, p2, v0, v1}, Lcom/google/android/location/copresence/o/k;->a(Ljava/lang/String;Lcom/google/android/location/copresence/a/a;II)Lcom/google/android/location/copresence/o/m;

    move-result-object v0

    .line 714
    iget-object v1, v0, Lcom/google/android/location/copresence/o/m;->c:Ljava/util/ArrayList;

    invoke-direct {p0, v1}, Lcom/google/android/location/copresence/o/k;->a(Ljava/util/List;)V

    .line 715
    return-object v0
.end method

.method public final a(Ljava/lang/String;Lcom/google/android/location/copresence/a/a;II)Lcom/google/android/location/copresence/o/m;
    .locals 16

    .prologue
    .line 774
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/location/copresence/o/k;->a:Lcom/google/android/location/copresence/o/d;

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Integer;

    const/4 v4, 0x0

    invoke-static/range {p3 .. p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    invoke-static/range {p4 .. p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    invoke-virtual {v2, v0, v1, v3}, Lcom/google/android/location/copresence/o/d;->a(Ljava/lang/String;Lcom/google/android/location/copresence/a/a;[Ljava/lang/Integer;)Ljava/util/List;

    move-result-object v2

    .line 776
    if-nez v2, :cond_1

    .line 778
    const/4 v4, 0x0

    .line 790
    :cond_0
    return-object v4

    .line 781
    :cond_1
    new-instance v4, Lcom/google/android/location/copresence/o/m;

    invoke-direct {v4}, Lcom/google/android/location/copresence/o/m;-><init>()V

    .line 782
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v12

    :cond_2
    :goto_0
    invoke-interface {v12}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v12}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/location/copresence/o/i;

    .line 783
    iget v3, v2, Lcom/google/android/location/copresence/o/i;->b:I

    move/from16 v0, p3

    if-ne v3, v0, :cond_5

    .line 784
    iget-object v5, v2, Lcom/google/android/location/copresence/o/i;->c:Ljava/lang/String;

    iget-wide v6, v2, Lcom/google/android/location/copresence/o/i;->a:J

    iget v8, v2, Lcom/google/android/location/copresence/o/i;->d:I

    iget-object v9, v2, Lcom/google/android/location/copresence/o/i;->e:Ljava/lang/Long;

    iget-object v2, v4, Lcom/google/android/location/copresence/o/m;->b:Ljava/util/ArrayList;

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    if-eqz v9, :cond_3

    invoke-virtual {v9}, Ljava/lang/Long;->longValue()J

    move-result-wide v10

    const-wide/16 v14, -0x1

    cmp-long v10, v10, v14

    if-eqz v10, :cond_3

    invoke-virtual {v9}, Ljava/lang/Long;->longValue()J

    move-result-wide v10

    cmp-long v2, v2, v10

    if-lez v2, :cond_3

    iget-object v2, v4, Lcom/google/android/location/copresence/o/m;->c:Ljava/util/ArrayList;

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_3
    iget-object v2, v4, Lcom/google/android/location/copresence/o/m;->a:Ljava/util/HashMap;

    invoke-virtual {v2, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/location/copresence/o/n;

    if-eqz v2, :cond_4

    const/4 v3, 0x1

    move v11, v3

    :goto_1
    packed-switch v8, :pswitch_data_0

    const/4 v2, 0x6

    invoke-static {v2}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v2

    if-eqz v2, :cond_2

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "CopresenceStateManager: Unknown network status = "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/location/copresence/ag;->e(Ljava/lang/String;)I

    goto :goto_0

    :cond_4
    const/4 v3, 0x0

    move v11, v3

    goto :goto_1

    :pswitch_0
    iget-object v13, v4, Lcom/google/android/location/copresence/o/m;->a:Ljava/util/HashMap;

    new-instance v3, Lcom/google/android/location/copresence/o/n;

    const/4 v10, 0x0

    invoke-direct/range {v3 .. v10}, Lcom/google/android/location/copresence/o/n;-><init>(Lcom/google/android/location/copresence/o/m;Ljava/lang/String;JILjava/lang/Long;B)V

    invoke-virtual {v13, v5, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    if-eqz v11, :cond_2

    iget-object v3, v4, Lcom/google/android/location/copresence/o/m;->c:Ljava/util/ArrayList;

    iget-wide v6, v2, Lcom/google/android/location/copresence/o/n;->c:J

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    :pswitch_1
    iget-object v2, v4, Lcom/google/android/location/copresence/o/m;->c:Ljava/util/ArrayList;

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 786
    :cond_5
    iget v3, v2, Lcom/google/android/location/copresence/o/i;->b:I

    move/from16 v0, p4

    if-ne v3, v0, :cond_2

    .line 787
    iget-object v5, v2, Lcom/google/android/location/copresence/o/i;->c:Ljava/lang/String;

    iget-wide v6, v2, Lcom/google/android/location/copresence/o/i;->a:J

    iget v8, v2, Lcom/google/android/location/copresence/o/i;->d:I

    iget-object v2, v4, Lcom/google/android/location/copresence/o/m;->b:Ljava/util/ArrayList;

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v2, v4, Lcom/google/android/location/copresence/o/m;->a:Ljava/util/HashMap;

    invoke-virtual {v2, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/location/copresence/o/n;

    if-eqz v2, :cond_6

    const/4 v3, 0x1

    :goto_2
    packed-switch v8, :pswitch_data_1

    const/4 v2, 0x6

    invoke-static {v2}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v2

    if-eqz v2, :cond_2

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "CopresenceStateManager: Unknown network status = "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/location/copresence/ag;->e(Ljava/lang/String;)I

    goto/16 :goto_0

    :cond_6
    const/4 v3, 0x0

    goto :goto_2

    :pswitch_2
    if-eqz v3, :cond_2

    iget-object v3, v4, Lcom/google/android/location/copresence/o/m;->a:Ljava/util/HashMap;

    invoke-virtual {v3, v5}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v3, v4, Lcom/google/android/location/copresence/o/m;->c:Ljava/util/ArrayList;

    iget-wide v6, v2, Lcom/google/android/location/copresence/o/n;->c:J

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    :pswitch_3
    iget-object v2, v4, Lcom/google/android/location/copresence/o/m;->c:Ljava/util/ArrayList;

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    :pswitch_4
    iget-object v8, v4, Lcom/google/android/location/copresence/o/m;->c:Ljava/util/ArrayList;

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v8, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    if-eqz v3, :cond_2

    iget-object v3, v4, Lcom/google/android/location/copresence/o/m;->a:Ljava/util/HashMap;

    invoke-virtual {v3, v5}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v3, v4, Lcom/google/android/location/copresence/o/m;->c:Ljava/util/ArrayList;

    iget-wide v6, v2, Lcom/google/android/location/copresence/o/n;->c:J

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 784
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_0
    .end packed-switch

    .line 787
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public final a(Ljava/lang/String;)Ljava/util/Set;
    .locals 1

    .prologue
    .line 833
    iget-object v0, p0, Lcom/google/android/location/copresence/o/k;->b:Lcom/google/android/location/copresence/ap;

    invoke-virtual {v0}, Lcom/google/android/location/copresence/ap;->c()V

    .line 834
    iget-object v0, p0, Lcom/google/android/location/copresence/o/k;->a:Lcom/google/android/location/copresence/o/d;

    invoke-virtual {v0, p1}, Lcom/google/android/location/copresence/o/d;->b(Ljava/lang/String;)Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/android/location/copresence/o/o;)V
    .locals 1

    .prologue
    .line 124
    iget-object v0, p0, Lcom/google/android/location/copresence/o/k;->e:Ljava/util/HashSet;

    invoke-virtual {v0, p1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 125
    return-void
.end method

.method public final a(Ljava/util/List;I)V
    .locals 7

    .prologue
    .line 910
    iget-object v0, p0, Lcom/google/android/location/copresence/o/k;->b:Lcom/google/android/location/copresence/ap;

    invoke-virtual {v0}, Lcom/google/android/location/copresence/ap;->c()V

    .line 913
    iget-object v0, p0, Lcom/google/android/location/copresence/o/k;->a:Lcom/google/android/location/copresence/o/d;

    invoke-virtual {v0, p1}, Lcom/google/android/location/copresence/o/d;->b(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    .line 916
    iget-object v1, p0, Lcom/google/android/location/copresence/o/k;->a:Lcom/google/android/location/copresence/o/d;

    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 919
    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/copresence/o/h;

    .line 920
    iget-wide v2, v0, Lcom/google/android/location/copresence/o/h;->a:J

    iget v4, v0, Lcom/google/android/location/copresence/o/h;->b:I

    iget-object v5, v0, Lcom/google/android/location/copresence/o/h;->c:Ljava/lang/String;

    iget v0, v0, Lcom/google/android/location/copresence/o/h;->d:I

    invoke-direct {p0, v2, v3, v4, p2}, Lcom/google/android/location/copresence/o/k;->a(JII)V

    goto :goto_1

    .line 916
    :cond_1
    iget-object v2, v1, Lcom/google/android/location/copresence/o/d;->a:Lcom/google/android/gms/common/util/p;

    invoke-interface {v2}, Lcom/google/android/gms/common/util/p;->a()J

    move-result-wide v2

    new-instance v4, Landroid/content/ContentValues;

    invoke-direct {v4}, Landroid/content/ContentValues;-><init>()V

    const-string v5, "network_status"

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v5, "network_last_update_millis"

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v4, v5, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    iget-object v1, v1, Lcom/google/android/location/copresence/o/d;->b:Lcom/google/android/location/copresence/o/g;

    invoke-virtual {v1}, Lcom/google/android/location/copresence/o/g;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    const-string v2, "operations"

    invoke-static {}, Lcom/google/android/location/copresence/q/af;->a()Lcom/google/android/location/copresence/q/ak;

    move-result-object v3

    const-string v5, "_id"

    const/4 v6, 0x0

    invoke-virtual {v3, v5, p1, v6}, Lcom/google/android/location/copresence/q/ak;->a(Ljava/lang/String;Ljava/util/List;Z)Lcom/google/android/location/copresence/q/ak;

    move-result-object v3

    iget-object v3, v3, Lcom/google/android/location/copresence/q/ak;->a:Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v5, 0x0

    invoke-virtual {v1, v2, v4, v3, v5}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    goto :goto_0

    .line 923
    :cond_2
    return-void
.end method

.method public final a(Ljava/lang/String;Lcom/google/android/location/copresence/a/a;Ljava/util/List;Lcom/google/android/gms/location/copresence/internal/i;)Z
    .locals 20

    .prologue
    .line 208
    new-instance v5, Lcom/google/android/location/copresence/o/p;

    move-object/from16 v0, p0

    invoke-direct {v5, v0}, Lcom/google/android/location/copresence/o/p;-><init>(Lcom/google/android/location/copresence/o/k;)V

    .line 209
    const/4 v4, 0x1

    .line 210
    invoke-interface/range {p3 .. p3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v12

    move v6, v4

    :goto_0
    invoke-interface {v12}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_13

    invoke-interface {v12}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/gms/location/copresence/internal/Operation;

    .line 211
    iget v7, v4, Lcom/google/android/gms/location/copresence/internal/Operation;->a:I

    packed-switch v7, :pswitch_data_0

    .line 226
    const/4 v7, 0x6

    invoke-static {v7}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 227
    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "CopresenceStateManager: Unknown opCode: "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v4, v4, Lcom/google/android/gms/location/copresence/internal/Operation;->a:I

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/google/android/location/copresence/ag;->e(Ljava/lang/String;)I

    :cond_0
    move v4, v6

    .line 231
    :goto_1
    if-nez v4, :cond_12

    .line 233
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/location/copresence/o/k;->a:Lcom/google/android/location/copresence/o/d;

    invoke-virtual {v5}, Lcom/google/android/location/copresence/o/p;->a()Ljava/util/ArrayList;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/google/android/location/copresence/o/d;->c(Ljava/util/List;)I

    .line 234
    const/16 v4, 0xd

    move-object/from16 v0, p4

    invoke-static {v0, v4}, Lcom/google/android/location/copresence/o/k;->a(Lcom/google/android/gms/location/copresence/internal/i;I)V

    .line 235
    const/4 v4, 0x0

    .line 256
    :goto_2
    return v4

    .line 213
    :pswitch_0
    iget-object v9, v4, Lcom/google/android/gms/location/copresence/internal/Operation;->b:Lcom/google/android/gms/location/copresence/internal/PublishOperation;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/location/copresence/o/k;->b:Lcom/google/android/location/copresence/ap;

    invoke-virtual {v4}, Lcom/google/android/location/copresence/ap;->c()V

    invoke-virtual {v9}, Lcom/google/android/gms/location/copresence/internal/PublishOperation;->e()Ljava/lang/String;

    move-result-object v4

    if-nez v4, :cond_2

    const/4 v4, 0x6

    invoke-static {v4}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v4

    if-eqz v4, :cond_1

    const-string v4, "CopresenceStateManager: You look to be using an old version.  Please update to the latest Gcore client jar."

    invoke-static {v4}, Lcom/google/android/location/copresence/ag;->e(Ljava/lang/String;)I

    :cond_1
    const/4 v4, 0x0

    goto :goto_1

    :cond_2
    const/4 v4, 0x3

    invoke-static {v4}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v4

    if-eqz v4, :cond_3

    const-string v4, "CopresenceStateManager: add publish operation"

    invoke-static {v4}, Lcom/google/android/location/copresence/ag;->b(Ljava/lang/String;)I

    :cond_3
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/android/location/copresence/o/k;->a:Lcom/google/android/location/copresence/o/d;

    invoke-virtual {v9}, Lcom/google/android/gms/location/copresence/internal/PublishOperation;->e()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-object/from16 v0, p1

    invoke-virtual {v10, v0}, Lcom/google/android/location/copresence/o/d;->c(Ljava/lang/String;)J

    move-result-wide v6

    const-wide/16 v14, -0x1

    cmp-long v4, v6, v14

    if-nez v4, :cond_5

    const/4 v4, 0x6

    invoke-static {v4}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v4

    if-eqz v4, :cond_4

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v6, "CopresenceStateDatabase: Could not get package id for packageName="

    invoke-direct {v4, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p1

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, " for "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/google/android/location/copresence/ag;->e(Ljava/lang/String;)I

    :cond_4
    const-wide/16 v6, -0x1

    :goto_3
    const-wide/16 v10, -0x1

    cmp-long v4, v6, v10

    if-nez v4, :cond_11

    const/4 v4, 0x0

    goto/16 :goto_1

    :cond_5
    move-object/from16 v0, p2

    iget-object v4, v0, Lcom/google/android/location/copresence/a/a;->a:Landroid/accounts/Account;

    iget-object v4, v4, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v10, v4}, Lcom/google/android/location/copresence/o/d;->d(Ljava/lang/String;)J

    move-result-wide v14

    const-wide/16 v16, -0x1

    cmp-long v4, v14, v16

    if-nez v4, :cond_7

    const/4 v4, 0x6

    invoke-static {v4}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v4

    if-eqz v4, :cond_6

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v6, "CopresenceStateDatabase: Could not get account id for account="

    invoke-direct {v4, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static/range {p2 .. p2}, Lcom/google/android/location/copresence/ag;->a(Lcom/google/android/location/copresence/a/a;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, " for "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/google/android/location/copresence/ag;->e(Ljava/lang/String;)I

    :cond_6
    const-wide/16 v6, -0x1

    goto :goto_3

    :cond_7
    iget-object v4, v10, Lcom/google/android/location/copresence/o/d;->a:Lcom/google/android/gms/common/util/p;

    invoke-interface {v4}, Lcom/google/android/gms/common/util/p;->a()J

    move-result-wide v16

    invoke-virtual {v9}, Lcom/google/android/gms/location/copresence/internal/PublishOperation;->f()Lcom/google/android/gms/location/copresence/AccessPolicy;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/gms/location/copresence/AccessPolicy;->f()J

    move-result-wide v18

    invoke-static/range {v16 .. v19}, Lcom/google/android/location/copresence/o/d;->a(JJ)J

    move-result-wide v18

    new-instance v11, Landroid/content/ContentValues;

    invoke-direct {v11}, Landroid/content/ContentValues;-><init>()V

    const-string v4, "op_code"

    const/4 v8, 0x0

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v11, v4, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v4, "write_time_millis"

    invoke-static/range {v16 .. v17}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    invoke-virtual {v11, v4, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v4, "package_id"

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v11, v4, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v4, "account_id"

    invoke-static {v14, v15}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v11, v4, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v4, "client_id"

    invoke-virtual {v9}, Lcom/google/android/gms/location/copresence/internal/PublishOperation;->e()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v11, v4, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v4, "expiration_time"

    invoke-static/range {v18 .. v19}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v11, v4, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v4, "network_status"

    const/4 v6, 0x0

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v11, v4, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v4, "network_last_update_millis"

    invoke-static/range {v16 .. v17}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v11, v4, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v4, "deletable"

    const/4 v6, 0x0

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v11, v4, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v13, "proto"

    if-nez v9, :cond_8

    const/4 v4, 0x0

    :goto_4
    invoke-static {v4}, Lcom/google/protobuf/nano/j;->toByteArray(Lcom/google/protobuf/nano/j;)[B

    move-result-object v4

    invoke-virtual {v11, v13, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    iget-object v4, v10, Lcom/google/android/location/copresence/o/d;->b:Lcom/google/android/location/copresence/o/g;

    invoke-virtual {v4}, Lcom/google/android/location/copresence/o/g;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v4

    const-string v6, "operations"

    const-string v7, "op_code"

    invoke-virtual {v4, v6, v7, v11}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v6

    goto/16 :goto_3

    :cond_8
    invoke-virtual {v9}, Lcom/google/android/gms/location/copresence/internal/PublishOperation;->c()Lcom/google/android/gms/location/copresence/u;

    move-result-object v4

    check-cast v4, Lcom/google/android/gms/location/copresence/internal/StrategyImpl;

    new-instance v8, Lcom/google/ac/b/c/bf;

    invoke-direct {v8}, Lcom/google/ac/b/c/bf;-><init>()V

    invoke-virtual {v9}, Lcom/google/android/gms/location/copresence/internal/PublishOperation;->e()Ljava/lang/String;

    move-result-object v6

    iput-object v6, v8, Lcom/google/ac/b/c/bf;->a:Ljava/lang/String;

    invoke-static {v4}, Lcom/google/android/location/copresence/l/q;->c(Lcom/google/android/gms/location/copresence/internal/StrategyImpl;)Ljava/lang/Integer;

    move-result-object v6

    iput-object v6, v8, Lcom/google/ac/b/c/bf;->d:Ljava/lang/Integer;

    invoke-virtual {v9}, Lcom/google/android/gms/location/copresence/internal/PublishOperation;->d()Lcom/google/android/gms/location/copresence/Message;

    move-result-object v7

    if-nez v7, :cond_9

    const/4 v6, 0x0

    :goto_5
    iput-object v6, v8, Lcom/google/ac/b/c/bf;->c:Lcom/google/ac/b/c/at;

    invoke-virtual {v9}, Lcom/google/android/gms/location/copresence/internal/PublishOperation;->f()Lcom/google/android/gms/location/copresence/AccessPolicy;

    move-result-object v14

    if-nez v14, :cond_a

    const/4 v6, 0x0

    :goto_6
    iput-object v6, v8, Lcom/google/ac/b/c/bf;->b:Lcom/google/ac/b/c/t;

    invoke-static {v4}, Lcom/google/android/location/copresence/l/q;->a(Lcom/google/android/gms/location/copresence/internal/StrategyImpl;)Lcom/google/ac/b/c/bs;

    move-result-object v6

    iput-object v6, v8, Lcom/google/ac/b/c/bf;->e:Lcom/google/ac/b/c/bs;

    invoke-static {v4}, Lcom/google/android/location/copresence/l/q;->b(Lcom/google/android/gms/location/copresence/internal/StrategyImpl;)Lcom/google/ac/b/c/bb;

    move-result-object v4

    iput-object v4, v8, Lcom/google/ac/b/c/bf;->f:Lcom/google/ac/b/c/bb;

    move-object v4, v8

    goto :goto_4

    :cond_9
    new-instance v6, Lcom/google/ac/b/c/at;

    invoke-direct {v6}, Lcom/google/ac/b/c/at;-><init>()V

    invoke-virtual {v7}, Lcom/google/android/gms/location/copresence/Message;->b()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v7}, Lcom/google/android/gms/location/copresence/Message;->c()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Lcom/google/android/location/copresence/l/q;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/ac/b/c/av;

    move-result-object v14

    iput-object v14, v6, Lcom/google/ac/b/c/at;->a:Lcom/google/ac/b/c/av;

    invoke-virtual {v7}, Lcom/google/android/gms/location/copresence/Message;->d()[B

    move-result-object v7

    iput-object v7, v6, Lcom/google/ac/b/c/at;->b:[B

    goto :goto_5

    :cond_a
    new-instance v7, Lcom/google/ac/b/c/t;

    invoke-direct {v7}, Lcom/google/ac/b/c/t;-><init>()V

    invoke-virtual {v14}, Lcom/google/android/gms/location/copresence/AccessPolicy;->g()Z

    move-result v6

    if-eqz v6, :cond_b

    invoke-virtual {v14}, Lcom/google/android/gms/location/copresence/AccessPolicy;->h()Lcom/google/android/gms/location/copresence/AccessLock;

    move-result-object v15

    if-nez v15, :cond_10

    const/4 v6, 0x0

    :goto_7
    iput-object v6, v7, Lcom/google/ac/b/c/t;->c:Lcom/google/ac/b/c/r;

    :cond_b
    invoke-virtual {v14}, Lcom/google/android/gms/location/copresence/AccessPolicy;->i()Z

    move-result v6

    if-eqz v6, :cond_c

    invoke-virtual {v14}, Lcom/google/android/gms/location/copresence/AccessPolicy;->j()Lcom/google/android/gms/common/people/data/Audience;

    move-result-object v6

    invoke-static {v6, v4}, Lcom/google/android/location/copresence/l/q;->a(Lcom/google/android/gms/common/people/data/Audience;Lcom/google/android/gms/location/copresence/internal/StrategyImpl;)Lcom/google/ac/b/c/u;

    move-result-object v6

    iput-object v6, v7, Lcom/google/ac/b/c/t;->b:Lcom/google/ac/b/c/u;

    :cond_c
    invoke-virtual {v14}, Lcom/google/android/gms/location/copresence/AccessPolicy;->e()Z

    move-result v6

    if-eqz v6, :cond_d

    invoke-virtual {v14}, Lcom/google/android/gms/location/copresence/AccessPolicy;->f()J

    move-result-wide v16

    const-wide/16 v18, -0x1

    cmp-long v6, v16, v18

    if-eqz v6, :cond_d

    invoke-virtual {v14}, Lcom/google/android/gms/location/copresence/AccessPolicy;->f()J

    move-result-wide v16

    invoke-static/range {v16 .. v17}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    iput-object v6, v7, Lcom/google/ac/b/c/t;->a:Ljava/lang/Long;

    :cond_d
    invoke-virtual {v14}, Lcom/google/android/gms/location/copresence/AccessPolicy;->c()Z

    move-result v6

    if-eqz v6, :cond_e

    new-instance v6, Lcom/google/ac/b/c/u;

    invoke-direct {v6}, Lcom/google/ac/b/c/u;-><init>()V

    iput-object v6, v7, Lcom/google/ac/b/c/t;->b:Lcom/google/ac/b/c/u;

    iget-object v6, v7, Lcom/google/ac/b/c/t;->b:Lcom/google/ac/b/c/u;

    const/4 v15, 0x4

    invoke-static {v15}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v15

    iput-object v15, v6, Lcom/google/ac/b/c/u;->a:Ljava/lang/Integer;

    iget-object v6, v7, Lcom/google/ac/b/c/t;->b:Lcom/google/ac/b/c/u;

    invoke-virtual {v14}, Lcom/google/android/gms/location/copresence/AccessPolicy;->d()Ljava/lang/String;

    move-result-object v15

    iput-object v15, v6, Lcom/google/ac/b/c/u;->b:Ljava/lang/String;

    :cond_e
    invoke-virtual {v14}, Lcom/google/android/gms/location/copresence/AccessPolicy;->k()I

    move-result v6

    packed-switch v6, :pswitch_data_1

    :goto_8
    invoke-virtual {v14}, Lcom/google/android/gms/location/copresence/AccessPolicy;->l()I

    move-result v6

    packed-switch v6, :pswitch_data_2

    :goto_9
    invoke-virtual {v14}, Lcom/google/android/gms/location/copresence/AccessPolicy;->m()Z

    move-result v6

    if-eqz v6, :cond_f

    invoke-virtual {v14}, Lcom/google/android/gms/location/copresence/AccessPolicy;->n()Lcom/google/android/gms/location/copresence/AclResourceId;

    move-result-object v6

    new-instance v14, Lcom/google/ac/b/c/u;

    invoke-direct {v14}, Lcom/google/ac/b/c/u;-><init>()V

    const/4 v15, 0x6

    invoke-static {v15}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v15

    iput-object v15, v14, Lcom/google/ac/b/c/u;->a:Ljava/lang/Integer;

    new-instance v15, Lcom/google/ac/b/c/bh;

    invoke-direct {v15}, Lcom/google/ac/b/c/bh;-><init>()V

    iput-object v15, v14, Lcom/google/ac/b/c/u;->d:Lcom/google/ac/b/c/bh;

    iget-object v15, v14, Lcom/google/ac/b/c/u;->d:Lcom/google/ac/b/c/bh;

    invoke-virtual {v6}, Lcom/google/android/gms/location/copresence/AclResourceId;->b()Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, v16

    iput-object v0, v15, Lcom/google/ac/b/c/bh;->a:Ljava/lang/String;

    iget-object v15, v14, Lcom/google/ac/b/c/u;->d:Lcom/google/ac/b/c/bh;

    invoke-virtual {v6}, Lcom/google/android/gms/location/copresence/AclResourceId;->c()Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, v16

    iput-object v0, v15, Lcom/google/ac/b/c/bh;->b:Ljava/lang/String;

    iget-object v15, v14, Lcom/google/ac/b/c/u;->d:Lcom/google/ac/b/c/bh;

    invoke-virtual {v6}, Lcom/google/android/gms/location/copresence/AclResourceId;->d()Ljava/lang/String;

    move-result-object v6

    iput-object v6, v15, Lcom/google/ac/b/c/bh;->c:Ljava/lang/String;

    iput-object v14, v7, Lcom/google/ac/b/c/t;->b:Lcom/google/ac/b/c/u;

    :cond_f
    move-object v6, v7

    goto/16 :goto_6

    :cond_10
    new-instance v6, Lcom/google/ac/b/c/r;

    invoke-direct {v6}, Lcom/google/ac/b/c/r;-><init>()V

    invoke-virtual {v15}, Lcom/google/android/gms/location/copresence/AccessLock;->b()Ljava/lang/String;

    move-result-object v15

    iput-object v15, v6, Lcom/google/ac/b/c/r;->a:Ljava/lang/String;

    goto/16 :goto_7

    :pswitch_1
    new-instance v6, Lcom/google/ac/b/c/ai;

    invoke-direct {v6}, Lcom/google/ac/b/c/ai;-><init>()V

    iput-object v6, v7, Lcom/google/ac/b/c/t;->d:Lcom/google/ac/b/c/ai;

    iget-object v6, v7, Lcom/google/ac/b/c/t;->d:Lcom/google/ac/b/c/ai;

    const/4 v15, 0x1

    invoke-static {v15}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v15

    iput-object v15, v6, Lcom/google/ac/b/c/ai;->a:Ljava/lang/Integer;

    goto :goto_8

    :pswitch_2
    new-instance v6, Lcom/google/ac/b/c/bx;

    invoke-direct {v6}, Lcom/google/ac/b/c/bx;-><init>()V

    iput-object v6, v7, Lcom/google/ac/b/c/t;->f:Lcom/google/ac/b/c/bx;

    iget-object v6, v7, Lcom/google/ac/b/c/t;->f:Lcom/google/ac/b/c/bx;

    const/4 v15, 0x1

    invoke-static {v15}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v15

    iput-object v15, v6, Lcom/google/ac/b/c/bx;->a:Ljava/lang/Integer;

    goto :goto_9

    :cond_11
    const/4 v8, 0x0

    invoke-virtual {v9}, Lcom/google/android/gms/location/copresence/internal/PublishOperation;->e()Ljava/lang/String;

    move-result-object v9

    const/4 v10, 0x0

    const/4 v11, 0x0

    invoke-virtual/range {v5 .. v11}, Lcom/google/android/location/copresence/o/p;->a(JILjava/lang/String;Lcom/google/android/gms/location/copresence/internal/SubscribeOperation;Lcom/google/android/gms/location/copresence/internal/UnsubscribeOperation;)V

    const/4 v4, 0x1

    goto/16 :goto_1

    .line 216
    :pswitch_3
    iget-object v4, v4, Lcom/google/android/gms/location/copresence/internal/Operation;->c:Lcom/google/android/gms/location/copresence/internal/UnpublishOperation;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    invoke-direct {v0, v1, v2, v4, v5}, Lcom/google/android/location/copresence/o/k;->a(Ljava/lang/String;Lcom/google/android/location/copresence/a/a;Lcom/google/android/gms/location/copresence/internal/UnpublishOperation;Lcom/google/android/location/copresence/o/p;)Z

    move-result v4

    goto/16 :goto_1

    .line 219
    :pswitch_4
    iget-object v4, v4, Lcom/google/android/gms/location/copresence/internal/Operation;->d:Lcom/google/android/gms/location/copresence/internal/SubscribeOperation;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    invoke-direct {v0, v1, v2, v4, v5}, Lcom/google/android/location/copresence/o/k;->a(Ljava/lang/String;Lcom/google/android/location/copresence/a/a;Lcom/google/android/gms/location/copresence/internal/SubscribeOperation;Lcom/google/android/location/copresence/o/p;)Z

    move-result v4

    goto/16 :goto_1

    .line 222
    :pswitch_5
    iget-object v4, v4, Lcom/google/android/gms/location/copresence/internal/Operation;->e:Lcom/google/android/gms/location/copresence/internal/UnsubscribeOperation;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    invoke-direct {v0, v1, v2, v4, v5}, Lcom/google/android/location/copresence/o/k;->a(Ljava/lang/String;Lcom/google/android/location/copresence/a/a;Lcom/google/android/gms/location/copresence/internal/UnsubscribeOperation;Lcom/google/android/location/copresence/o/p;)Z

    move-result v4

    goto/16 :goto_1

    :cond_12
    move v6, v4

    .line 237
    goto/16 :goto_0

    .line 240
    :cond_13
    move-object/from16 v0, p0

    move-object/from16 v1, p4

    invoke-direct {v0, v1, v5}, Lcom/google/android/location/copresence/o/k;->a(Lcom/google/android/gms/location/copresence/internal/i;Lcom/google/android/location/copresence/o/p;)V

    .line 241
    move-object/from16 v0, p2

    iget-object v4, v0, Lcom/google/android/location/copresence/a/a;->a:Landroid/accounts/Account;

    iget-object v4, v4, Landroid/accounts/Account;->name:Ljava/lang/String;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v4, v5}, Lcom/google/android/location/copresence/o/k;->a(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/location/copresence/o/p;)V

    .line 244
    move-object/from16 v0, p0

    invoke-direct {v0, v5}, Lcom/google/android/location/copresence/o/k;->a(Lcom/google/android/location/copresence/o/p;)V

    .line 247
    move-object/from16 v0, p2

    iget-object v4, v0, Lcom/google/android/location/copresence/a/a;->a:Landroid/accounts/Account;

    iget-object v4, v4, Landroid/accounts/Account;->name:Ljava/lang/String;

    const/4 v6, 0x1

    invoke-static {v4, v6}, Lcom/google/android/location/copresence/a/h;->a(Ljava/lang/String;Z)V

    .line 253
    iget-object v4, v5, Lcom/google/android/location/copresence/o/p;->a:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_14

    .line 254
    const/4 v4, 0x0

    move-object/from16 v0, p4

    invoke-static {v0, v4}, Lcom/google/android/location/copresence/o/k;->a(Lcom/google/android/gms/location/copresence/internal/i;I)V

    .line 256
    :cond_14
    const/4 v4, 0x1

    goto/16 :goto_2

    .line 211
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch

    .line 213
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_1
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x1
        :pswitch_2
    .end packed-switch
.end method

.method public final b(Ljava/lang/String;Lcom/google/android/location/copresence/a/a;)Lcom/google/android/location/copresence/o/m;
    .locals 2

    .prologue
    .line 743
    iget-object v0, p0, Lcom/google/android/location/copresence/o/k;->b:Lcom/google/android/location/copresence/ap;

    invoke-virtual {v0}, Lcom/google/android/location/copresence/ap;->c()V

    .line 744
    const/4 v0, 0x2

    const/4 v1, 0x3

    invoke-virtual {p0, p1, p2, v0, v1}, Lcom/google/android/location/copresence/o/k;->a(Ljava/lang/String;Lcom/google/android/location/copresence/a/a;II)Lcom/google/android/location/copresence/o/m;

    move-result-object v0

    .line 746
    iget-object v1, v0, Lcom/google/android/location/copresence/o/m;->c:Ljava/util/ArrayList;

    invoke-direct {p0, v1}, Lcom/google/android/location/copresence/o/k;->a(Ljava/util/List;)V

    .line 747
    return-object v0
.end method
