.class public final Lcom/google/android/location/o/i;
.super Lcom/google/android/location/o/h;
.source "SourceFile"


# instance fields
.field private final e:Landroid/net/wifi/WifiManager$WifiLock;


# direct methods
.method public constructor <init>(Landroid/os/PowerManager;Landroid/net/wifi/WifiManager;Ljava/lang/String;)V
    .locals 6

    .prologue
    const/4 v3, 0x0

    .line 211
    const/4 v2, 0x1

    sget-object v5, Lcom/google/android/location/o/i;->a:[S

    move-object v0, p0

    move-object v1, p1

    move-object v4, p3

    invoke-direct/range {v0 .. v5}, Lcom/google/android/location/o/h;-><init>(Landroid/os/PowerManager;IZLjava/lang/String;[S)V

    .line 214
    const/4 v0, 0x2

    const-string v1, "NlpWifiLock"

    invoke-virtual {p2, v0, v1}, Landroid/net/wifi/WifiManager;->createWifiLock(ILjava/lang/String;)Landroid/net/wifi/WifiManager$WifiLock;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/o/i;->e:Landroid/net/wifi/WifiManager$WifiLock;

    .line 215
    iget-object v0, p0, Lcom/google/android/location/o/i;->e:Landroid/net/wifi/WifiManager$WifiLock;

    invoke-virtual {v0, v3}, Landroid/net/wifi/WifiManager$WifiLock;->setReferenceCounted(Z)V

    .line 218
    return-void
.end method


# virtual methods
.method public final declared-synchronized a(Lcom/google/android/location/o/n;)V
    .locals 1

    .prologue
    .line 227
    monitor-enter p0

    if-eqz p1, :cond_0

    .line 228
    :try_start_0
    iget-object v0, p0, Lcom/google/android/location/o/i;->c:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {p1, v0}, Lcom/google/android/location/o/n;->a(Landroid/os/PowerManager$WakeLock;)V

    .line 229
    iget-object v0, p0, Lcom/google/android/location/o/i;->e:Landroid/net/wifi/WifiManager$WifiLock;

    invoke-virtual {p1, v0}, Lcom/google/android/location/o/n;->a(Landroid/net/wifi/WifiManager$WifiLock;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 231
    :cond_0
    monitor-exit p0

    return-void

    .line 227
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected final e()V
    .locals 1

    .prologue
    .line 235
    iget-object v0, p0, Lcom/google/android/location/o/i;->e:Landroid/net/wifi/WifiManager$WifiLock;

    invoke-virtual {v0}, Landroid/net/wifi/WifiManager$WifiLock;->acquire()V

    .line 236
    return-void
.end method

.method protected final f()V
    .locals 1

    .prologue
    .line 240
    iget-object v0, p0, Lcom/google/android/location/o/i;->e:Landroid/net/wifi/WifiManager$WifiLock;

    invoke-virtual {v0}, Landroid/net/wifi/WifiManager$WifiLock;->release()V

    .line 241
    return-void
.end method
