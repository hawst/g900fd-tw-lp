.class public final Lcom/google/android/location/copresence/n/p;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final a:Ljava/util/Map;

.field b:Lcom/google/android/location/os/bl;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 44
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 45
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/location/copresence/n/p;->a:Ljava/util/Map;

    .line 46
    return-void
.end method

.method static a(Lcom/google/android/gms/location/copresence/CopresenceSettings;)Landroid/util/Pair;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 143
    invoke-virtual {p0}, Lcom/google/android/gms/location/copresence/CopresenceSettings;->b()Z

    move-result v0

    if-nez v0, :cond_1

    .line 144
    const/4 v0, 0x5

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 145
    const-string v0, "Unable to set settings: Copresence enable/disable state not specified."

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->d(Ljava/lang/String;)I

    .line 147
    :cond_0
    const/16 v0, 0xa

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v0, v1}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v0

    .line 159
    :goto_0
    return-object v0

    .line 149
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/gms/location/copresence/CopresenceSettings;->a()Z

    move-result v2

    .line 150
    invoke-virtual {p0}, Lcom/google/android/gms/location/copresence/CopresenceSettings;->c()[Lcom/google/android/gms/location/copresence/internal/CopresenceNamedAcl;

    move-result-object v0

    .line 151
    if-nez v0, :cond_2

    .line 153
    new-array v0, v3, [Lcom/google/android/gms/location/copresence/internal/CopresenceNamedAcl;

    .line 155
    :cond_2
    invoke-virtual {p0}, Lcom/google/android/gms/location/copresence/CopresenceSettings;->d()[Lcom/google/android/gms/location/copresence/internal/CopresenceFeatureOptIn;

    move-result-object v1

    .line 156
    if-nez v1, :cond_3

    .line 157
    new-array v1, v3, [Lcom/google/android/gms/location/copresence/internal/CopresenceFeatureOptIn;

    .line 159
    :cond_3
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-static {v2, v0, v1}, Lcom/google/android/gms/location/copresence/CopresenceSettings;->a(Z[Lcom/google/android/gms/location/copresence/internal/CopresenceNamedAcl;[Lcom/google/android/gms/location/copresence/internal/CopresenceFeatureOptIn;)Lcom/google/android/gms/location/copresence/CopresenceSettings;

    move-result-object v0

    invoke-static {v3, v0}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v0

    goto :goto_0
.end method

.method private b()Lcom/google/android/location/copresence/j/e;
    .locals 7

    .prologue
    .line 184
    new-instance v3, Lcom/google/android/location/copresence/j/e;

    invoke-direct {v3}, Lcom/google/android/location/copresence/j/e;-><init>()V

    .line 186
    iget-object v0, p0, Lcom/google/android/location/copresence/n/p;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v0

    new-array v4, v0, [Lcom/google/android/location/copresence/j/b;

    .line 188
    const/4 v0, 0x0

    .line 190
    iget-object v1, p0, Lcom/google/android/location/copresence/n/p;->a:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    move v2, v0

    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 191
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/location/copresence/CopresenceSettings;

    .line 192
    new-instance v6, Lcom/google/android/location/copresence/j/b;

    invoke-direct {v6}, Lcom/google/android/location/copresence/j/b;-><init>()V

    .line 194
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, v6, Lcom/google/android/location/copresence/j/b;->a:Ljava/lang/String;

    .line 195
    invoke-virtual {v1}, Lcom/google/android/gms/location/copresence/CopresenceSettings;->a()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, v6, Lcom/google/android/location/copresence/j/b;->b:Ljava/lang/Boolean;

    .line 197
    invoke-virtual {v1}, Lcom/google/android/gms/location/copresence/CopresenceSettings;->c()[Lcom/google/android/gms/location/copresence/internal/CopresenceNamedAcl;

    move-result-object v0

    .line 198
    if-eqz v0, :cond_0

    .line 200
    :try_start_0
    invoke-static {v0}, Lcom/google/android/location/copresence/l/l;->a([Lcom/google/android/gms/location/copresence/internal/CopresenceNamedAcl;)[Lcom/google/ac/b/c/aw;

    move-result-object v0

    iput-object v0, v6, Lcom/google/android/location/copresence/j/b;->c:[Lcom/google/ac/b/c/aw;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 208
    :cond_0
    :goto_1
    add-int/lit8 v0, v2, 0x1

    aput-object v6, v4, v2

    move v2, v0

    .line 209
    goto :goto_0

    .line 201
    :catch_0
    move-exception v0

    .line 202
    const/4 v1, 0x6

    invoke-static {v1}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 203
    const-string v1, "An error has occurred reading the account\'s named ACLs."

    invoke-static {v1, v0}, Lcom/google/android/location/copresence/ag;->a(Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1

    .line 210
    :cond_1
    iput-object v4, v3, Lcom/google/android/location/copresence/j/e;->a:[Lcom/google/android/location/copresence/j/b;

    .line 211
    return-object v3
.end method

.method private c()V
    .locals 4

    .prologue
    .line 254
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 255
    iget-object v0, p0, Lcom/google/android/location/copresence/n/p;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 256
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/location/copresence/CopresenceSettings;

    invoke-static {v1, v0}, Lcom/google/android/location/copresence/ag;->a(Ljava/lang/String;Lcom/google/android/gms/location/copresence/CopresenceSettings;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " ---- "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 260
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Lcom/google/android/gms/location/copresence/CopresenceSettings;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 71
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    move-object v0, v1

    .line 75
    :goto_0
    return-object v0

    .line 74
    :cond_0
    iget-object v0, p0, Lcom/google/android/location/copresence/n/p;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/location/copresence/CopresenceSettings;

    .line 75
    if-nez v0, :cond_1

    move-object v0, v1

    goto :goto_0

    :cond_1
    invoke-virtual {v0}, Lcom/google/android/gms/location/copresence/CopresenceSettings;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/location/copresence/CopresenceSettings;

    goto :goto_0
.end method

.method final a(Lcom/google/android/location/copresence/j/e;)V
    .locals 10

    .prologue
    const/4 v1, 0x0

    const/4 v9, 0x6

    .line 218
    iget-object v0, p0, Lcom/google/android/location/copresence/n/p;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 220
    iget-object v4, p1, Lcom/google/android/location/copresence/j/e;->a:[Lcom/google/android/location/copresence/j/b;

    array-length v5, v4

    const/4 v0, 0x0

    move v3, v0

    :goto_0
    if-ge v3, v5, :cond_4

    aget-object v2, v4, v3

    .line 221
    iget-object v6, v2, Lcom/google/android/location/copresence/j/b;->a:Ljava/lang/String;

    .line 222
    iget-object v7, v2, Lcom/google/android/location/copresence/j/b;->b:Ljava/lang/Boolean;

    .line 223
    if-eqz v6, :cond_0

    if-nez v7, :cond_2

    .line 224
    :cond_0
    invoke-static {v9}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 225
    const-string v0, "Incomplete settings object."

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->e(Ljava/lang/String;)I

    .line 220
    :cond_1
    :goto_1
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_0

    .line 231
    :cond_2
    iget-object v0, v2, Lcom/google/android/location/copresence/j/b;->c:[Lcom/google/ac/b/c/aw;

    .line 232
    if-eqz v0, :cond_3

    array-length v8, v0

    if-lez v8, :cond_3

    .line 234
    :try_start_0
    invoke-static {v0}, Lcom/google/android/location/copresence/l/l;->a([Lcom/google/ac/b/c/aw;)[Lcom/google/android/gms/location/copresence/internal/CopresenceNamedAcl;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 243
    :goto_2
    iget-object v2, v2, Lcom/google/android/location/copresence/j/b;->d:[Lcom/google/ac/b/c/am;

    .line 244
    if-eqz v2, :cond_5

    array-length v8, v2

    if-lez v8, :cond_5

    .line 245
    invoke-static {v2}, Lcom/google/android/location/copresence/l/g;->a([Lcom/google/ac/b/c/am;)[Lcom/google/android/gms/location/copresence/internal/CopresenceFeatureOptIn;

    move-result-object v2

    .line 248
    :goto_3
    iget-object v8, p0, Lcom/google/android/location/copresence/n/p;->a:Ljava/util/Map;

    invoke-virtual {v7}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v7

    invoke-static {v7, v0, v2}, Lcom/google/android/gms/location/copresence/CopresenceSettings;->a(Z[Lcom/google/android/gms/location/copresence/internal/CopresenceNamedAcl;[Lcom/google/android/gms/location/copresence/internal/CopresenceFeatureOptIn;)Lcom/google/android/gms/location/copresence/CopresenceSettings;

    move-result-object v0

    invoke-interface {v8, v6, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 235
    :catch_0
    move-exception v0

    .line 236
    invoke-static {v9}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v8

    if-eqz v8, :cond_3

    .line 237
    const-string v8, "An error occurred populating the account\'s named ACLs."

    invoke-static {v8, v0}, Lcom/google/android/location/copresence/ag;->a(Ljava/lang/String;Ljava/lang/Throwable;)I

    :cond_3
    move-object v0, v1

    goto :goto_2

    .line 250
    :cond_4
    invoke-direct {p0}, Lcom/google/android/location/copresence/n/p;->c()V

    .line 251
    return-void

    :cond_5
    move-object v2, v1

    goto :goto_3
.end method

.method final a()Z
    .locals 3

    .prologue
    .line 170
    invoke-direct {p0}, Lcom/google/android/location/copresence/n/p;->c()V

    .line 171
    invoke-direct {p0}, Lcom/google/android/location/copresence/n/p;->b()Lcom/google/android/location/copresence/j/e;

    move-result-object v0

    .line 173
    :try_start_0
    iget-object v1, p0, Lcom/google/android/location/copresence/n/p;->b:Lcom/google/android/location/os/bl;

    invoke-virtual {v1, v0}, Lcom/google/android/location/os/bl;->b(Lcom/google/protobuf/nano/j;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 180
    const/4 v0, 0x1

    :goto_0
    return v0

    .line 174
    :catch_0
    move-exception v0

    .line 175
    const/4 v1, 0x6

    invoke-static {v1}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 176
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unable to write settings: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->e(Ljava/lang/String;)I

    .line 178
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
