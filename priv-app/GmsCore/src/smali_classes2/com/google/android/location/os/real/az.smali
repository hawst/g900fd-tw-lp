.class final Lcom/google/android/location/os/real/az;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/location/j/j;


# instance fields
.field private final b:Landroid/content/Context;

.field private final c:Lcom/google/android/location/os/j;

.field private final d:Landroid/app/AlarmManager;

.field private final e:[Landroid/app/PendingIntent;

.field private final f:Lcom/google/android/location/os/real/x;

.field private final g:[Lcom/google/android/location/o/i;

.field private final h:[J

.field private final i:[J


# direct methods
.method constructor <init>(Landroid/content/Context;Lcom/google/android/location/os/real/x;Lcom/google/android/location/os/j;)V
    .locals 8

    .prologue
    const-wide/16 v4, -0x1

    const/4 v2, 0x0

    .line 47
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 48
    iput-object p1, p0, Lcom/google/android/location/os/real/az;->b:Landroid/content/Context;

    .line 49
    iput-object p3, p0, Lcom/google/android/location/os/real/az;->c:Lcom/google/android/location/os/j;

    .line 50
    iput-object p2, p0, Lcom/google/android/location/os/real/az;->f:Lcom/google/android/location/os/real/x;

    .line 51
    sget v0, Lcom/google/android/location/os/real/az;->a:I

    new-array v0, v0, [Lcom/google/android/location/o/i;

    iput-object v0, p0, Lcom/google/android/location/os/real/az;->g:[Lcom/google/android/location/o/i;

    .line 52
    sget v0, Lcom/google/android/location/os/real/az;->a:I

    new-array v0, v0, [J

    iput-object v0, p0, Lcom/google/android/location/os/real/az;->h:[J

    .line 53
    sget v0, Lcom/google/android/location/os/real/az;->a:I

    new-array v0, v0, [J

    iput-object v0, p0, Lcom/google/android/location/os/real/az;->i:[J

    .line 54
    iget-object v0, p0, Lcom/google/android/location/os/real/az;->h:[J

    invoke-static {v0, v4, v5}, Ljava/util/Arrays;->fill([JJ)V

    .line 55
    iget-object v0, p0, Lcom/google/android/location/os/real/az;->i:[J

    invoke-static {v0, v4, v5}, Ljava/util/Arrays;->fill([JJ)V

    .line 56
    const-string v0, "alarm"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    iput-object v0, p0, Lcom/google/android/location/os/real/az;->d:Landroid/app/AlarmManager;

    .line 57
    sget v0, Lcom/google/android/location/os/real/az;->a:I

    new-array v0, v0, [Landroid/app/PendingIntent;

    iput-object v0, p0, Lcom/google/android/location/os/real/az;->e:[Landroid/app/PendingIntent;

    .line 58
    iget-object v0, p0, Lcom/google/android/location/os/real/az;->e:[Landroid/app/PendingIntent;

    sget-object v1, Lcom/google/android/location/j/k;->a:Lcom/google/android/location/j/k;

    invoke-virtual {v1}, Lcom/google/android/location/j/k;->ordinal()I

    move-result v1

    new-instance v3, Landroid/content/Intent;

    iget-object v4, p2, Lcom/google/android/location/os/real/x;->a:Ljava/lang/String;

    invoke-direct {v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-static {p1, v2, v3, v2}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v3

    aput-object v3, v0, v1

    .line 60
    iget-object v0, p0, Lcom/google/android/location/os/real/az;->e:[Landroid/app/PendingIntent;

    sget-object v1, Lcom/google/android/location/j/k;->b:Lcom/google/android/location/j/k;

    invoke-virtual {v1}, Lcom/google/android/location/j/k;->ordinal()I

    move-result v1

    new-instance v3, Landroid/content/Intent;

    iget-object v4, p2, Lcom/google/android/location/os/real/x;->b:Ljava/lang/String;

    invoke-direct {v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-static {p1, v2, v3, v2}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v3

    aput-object v3, v0, v1

    .line 62
    iget-object v0, p0, Lcom/google/android/location/os/real/az;->e:[Landroid/app/PendingIntent;

    sget-object v1, Lcom/google/android/location/j/k;->c:Lcom/google/android/location/j/k;

    invoke-virtual {v1}, Lcom/google/android/location/j/k;->ordinal()I

    move-result v1

    new-instance v3, Landroid/content/Intent;

    iget-object v4, p2, Lcom/google/android/location/os/real/x;->c:Ljava/lang/String;

    invoke-direct {v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-static {p1, v2, v3, v2}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v3

    aput-object v3, v0, v1

    .line 64
    iget-object v0, p0, Lcom/google/android/location/os/real/az;->e:[Landroid/app/PendingIntent;

    sget-object v1, Lcom/google/android/location/j/k;->d:Lcom/google/android/location/j/k;

    invoke-virtual {v1}, Lcom/google/android/location/j/k;->ordinal()I

    move-result v1

    new-instance v3, Landroid/content/Intent;

    iget-object v4, p2, Lcom/google/android/location/os/real/x;->d:Ljava/lang/String;

    invoke-direct {v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-static {p1, v2, v3, v2}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v3

    aput-object v3, v0, v1

    .line 66
    iget-object v0, p0, Lcom/google/android/location/os/real/az;->e:[Landroid/app/PendingIntent;

    sget-object v1, Lcom/google/android/location/j/k;->e:Lcom/google/android/location/j/k;

    invoke-virtual {v1}, Lcom/google/android/location/j/k;->ordinal()I

    move-result v1

    new-instance v3, Landroid/content/Intent;

    iget-object v4, p2, Lcom/google/android/location/os/real/x;->e:Ljava/lang/String;

    invoke-direct {v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-static {p1, v2, v3, v2}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v3

    aput-object v3, v0, v1

    .line 68
    iget-object v0, p0, Lcom/google/android/location/os/real/az;->e:[Landroid/app/PendingIntent;

    sget-object v1, Lcom/google/android/location/j/k;->f:Lcom/google/android/location/j/k;

    invoke-virtual {v1}, Lcom/google/android/location/j/k;->ordinal()I

    move-result v1

    new-instance v3, Landroid/content/Intent;

    iget-object v4, p2, Lcom/google/android/location/os/real/x;->f:Ljava/lang/String;

    invoke-direct {v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-static {p1, v2, v3, v2}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v3

    aput-object v3, v0, v1

    .line 70
    iget-object v0, p0, Lcom/google/android/location/os/real/az;->e:[Landroid/app/PendingIntent;

    sget-object v1, Lcom/google/android/location/j/k;->g:Lcom/google/android/location/j/k;

    invoke-virtual {v1}, Lcom/google/android/location/j/k;->ordinal()I

    move-result v1

    new-instance v3, Landroid/content/Intent;

    iget-object v4, p2, Lcom/google/android/location/os/real/x;->g:Ljava/lang/String;

    invoke-direct {v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-static {p1, v2, v3, v2}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v3

    aput-object v3, v0, v1

    .line 72
    iget-object v0, p0, Lcom/google/android/location/os/real/az;->e:[Landroid/app/PendingIntent;

    sget-object v1, Lcom/google/android/location/j/k;->h:Lcom/google/android/location/j/k;

    invoke-virtual {v1}, Lcom/google/android/location/j/k;->ordinal()I

    move-result v1

    new-instance v3, Landroid/content/Intent;

    iget-object v4, p2, Lcom/google/android/location/os/real/x;->h:Ljava/lang/String;

    invoke-direct {v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-static {p1, v2, v3, v2}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v3

    aput-object v3, v0, v1

    .line 74
    iget-object v0, p0, Lcom/google/android/location/os/real/az;->e:[Landroid/app/PendingIntent;

    sget-object v1, Lcom/google/android/location/j/k;->i:Lcom/google/android/location/j/k;

    invoke-virtual {v1}, Lcom/google/android/location/j/k;->ordinal()I

    move-result v1

    new-instance v3, Landroid/content/Intent;

    iget-object v4, p2, Lcom/google/android/location/os/real/x;->i:Ljava/lang/String;

    invoke-direct {v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-static {p1, v2, v3, v2}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v3

    aput-object v3, v0, v1

    .line 76
    iget-object v0, p0, Lcom/google/android/location/os/real/az;->e:[Landroid/app/PendingIntent;

    sget-object v1, Lcom/google/android/location/j/k;->j:Lcom/google/android/location/j/k;

    invoke-virtual {v1}, Lcom/google/android/location/j/k;->ordinal()I

    move-result v1

    new-instance v3, Landroid/content/Intent;

    iget-object v4, p2, Lcom/google/android/location/os/real/x;->j:Ljava/lang/String;

    invoke-direct {v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-static {p1, v2, v3, v2}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v3

    aput-object v3, v0, v1

    .line 78
    iget-object v0, p0, Lcom/google/android/location/os/real/az;->e:[Landroid/app/PendingIntent;

    sget-object v1, Lcom/google/android/location/j/k;->k:Lcom/google/android/location/j/k;

    invoke-virtual {v1}, Lcom/google/android/location/j/k;->ordinal()I

    move-result v1

    new-instance v3, Landroid/content/Intent;

    iget-object v4, p2, Lcom/google/android/location/os/real/x;->k:Ljava/lang/String;

    invoke-direct {v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-static {p1, v2, v3, v2}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v3

    aput-object v3, v0, v1

    .line 80
    iget-object v0, p0, Lcom/google/android/location/os/real/az;->e:[Landroid/app/PendingIntent;

    sget-object v1, Lcom/google/android/location/j/k;->l:Lcom/google/android/location/j/k;

    invoke-virtual {v1}, Lcom/google/android/location/j/k;->ordinal()I

    move-result v1

    new-instance v3, Landroid/content/Intent;

    iget-object v4, p2, Lcom/google/android/location/os/real/x;->l:Ljava/lang/String;

    invoke-direct {v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-static {p1, v2, v3, v2}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v3

    aput-object v3, v0, v1

    .line 83
    const-string v0, "wifi"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/wifi/WifiManager;

    .line 84
    const-string v1, "power"

    invoke-virtual {p1, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/os/PowerManager;

    .line 86
    invoke-static {}, Lcom/google/android/location/j/k;->values()[Lcom/google/android/location/j/k;

    move-result-object v3

    array-length v4, v3

    :goto_0
    if-ge v2, v4, :cond_0

    aget-object v5, v3, v2

    .line 91
    new-instance v6, Lcom/google/android/location/o/i;

    iget-object v7, v5, Lcom/google/android/location/j/k;->m:Ljava/lang/String;

    invoke-direct {v6, v1, v0, v7}, Lcom/google/android/location/o/i;-><init>(Landroid/os/PowerManager;Landroid/net/wifi/WifiManager;Ljava/lang/String;)V

    .line 98
    iget-object v7, p0, Lcom/google/android/location/os/real/az;->g:[Lcom/google/android/location/o/i;

    invoke-virtual {v5}, Lcom/google/android/location/j/k;->ordinal()I

    move-result v5

    aput-object v6, v7, v5

    .line 86
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 100
    :cond_0
    return-void
.end method


# virtual methods
.method final a(I)V
    .locals 4

    .prologue
    const-wide/16 v2, -0x1

    .line 171
    iget-object v0, p0, Lcom/google/android/location/os/real/az;->h:[J

    aput-wide v2, v0, p1

    .line 172
    iget-object v0, p0, Lcom/google/android/location/os/real/az;->i:[J

    aput-wide v2, v0, p1

    .line 173
    return-void
.end method

.method public final a(Lcom/google/android/location/j/k;)V
    .locals 7

    .prologue
    .line 189
    invoke-virtual {p1}, Lcom/google/android/location/j/k;->ordinal()I

    move-result v6

    .line 190
    iget-object v2, p0, Lcom/google/android/location/os/real/az;->c:Lcom/google/android/location/os/j;

    new-instance v1, Lcom/google/android/location/os/x;

    sget-object v3, Lcom/google/android/location/os/au;->S:Lcom/google/android/location/os/au;

    iget-object v0, v2, Lcom/google/android/location/os/j;->a:Lcom/google/android/location/os/at;

    invoke-interface {v0}, Lcom/google/android/location/os/at;->a()J

    move-result-wide v4

    invoke-direct/range {v1 .. v6}, Lcom/google/android/location/os/x;-><init>(Lcom/google/android/location/os/j;Lcom/google/android/location/os/au;JI)V

    invoke-virtual {v2, v1, v6}, Lcom/google/android/location/os/j;->a(Lcom/google/android/location/os/as;I)V

    .line 191
    iget-object v0, p0, Lcom/google/android/location/os/real/az;->g:[Lcom/google/android/location/o/i;

    aget-object v0, v0, v6

    .line 192
    invoke-virtual {v0}, Lcom/google/android/location/o/h;->b()V

    .line 193
    return-void
.end method

.method public final a(Lcom/google/android/location/j/k;JJLcom/google/android/location/o/n;)V
    .locals 10

    .prologue
    .line 127
    invoke-virtual {p1}, Lcom/google/android/location/j/k;->ordinal()I

    move-result v5

    .line 131
    iget-object v0, p0, Lcom/google/android/location/os/real/az;->h:[J

    aget-wide v0, v0, v5

    cmp-long v0, v0, p2

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/location/os/real/az;->i:[J

    aget-wide v0, v0, v5

    cmp-long v0, v0, p4

    if-eqz v0, :cond_2

    .line 133
    :cond_0
    iget-object v0, p0, Lcom/google/android/location/os/real/az;->h:[J

    aput-wide p2, v0, v5

    .line 134
    iget-object v0, p0, Lcom/google/android/location/os/real/az;->i:[J

    aput-wide p4, v0, v5

    .line 135
    iget-object v1, p0, Lcom/google/android/location/os/real/az;->c:Lcom/google/android/location/os/j;

    new-instance v0, Lcom/google/android/location/os/q;

    sget-object v2, Lcom/google/android/location/os/au;->y:Lcom/google/android/location/os/au;

    iget-object v3, v1, Lcom/google/android/location/os/j;->a:Lcom/google/android/location/os/at;

    invoke-interface {v3}, Lcom/google/android/location/os/at;->a()J

    move-result-wide v3

    move-wide v6, p2

    move-wide v8, p4

    invoke-direct/range {v0 .. v9}, Lcom/google/android/location/os/q;-><init>(Lcom/google/android/location/os/j;Lcom/google/android/location/os/au;JIJJ)V

    long-to-int v2, p2

    long-to-int v3, p4

    invoke-virtual {v1, v0, v5, v2, v3}, Lcom/google/android/location/os/j;->a(Lcom/google/android/location/os/as;III)V

    .line 137
    invoke-static {}, Lcom/google/android/location/os/real/bf;->a()Lcom/google/android/location/os/real/bf;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/location/os/real/az;->d:Landroid/app/AlarmManager;

    const/4 v3, 0x2

    iget-object v0, p0, Lcom/google/android/location/os/real/az;->e:[Landroid/app/PendingIntent;

    aget-object v8, v0, v5

    move-wide v4, p2

    move-wide v6, p4

    move-object/from16 v9, p6

    invoke-virtual/range {v1 .. v9}, Lcom/google/android/location/os/real/bf;->a(Landroid/app/AlarmManager;IJJLandroid/app/PendingIntent;Lcom/google/android/location/o/n;)V

    .line 153
    iget-object v0, p0, Lcom/google/android/location/os/real/az;->f:Lcom/google/android/location/os/real/x;

    move-object v1, p1

    move-wide v2, p2

    move-wide v4, p4

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/location/os/real/x;->a(Lcom/google/android/location/j/k;JJ)V

    .line 158
    :cond_1
    :goto_0
    return-void

    .line 155
    :cond_2
    sget-boolean v0, Lcom/google/android/location/i/a;->b:Z

    if-eqz v0, :cond_1

    const-string v0, "RealTimers"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Redundant alarmWindowReset of "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p1, Lcom/google/android/location/j/k;->m:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " with deadline "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/location/o/a/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/location/j/k;JLcom/google/android/location/o/n;)V
    .locals 10

    .prologue
    const-wide/16 v8, -0x1

    .line 106
    invoke-virtual {p1}, Lcom/google/android/location/j/k;->ordinal()I

    move-result v5

    .line 110
    iget-object v0, p0, Lcom/google/android/location/os/real/az;->h:[J

    aget-wide v0, v0, v5

    cmp-long v0, v0, p2

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/location/os/real/az;->i:[J

    aget-wide v0, v0, v5

    cmp-long v0, v0, v8

    if-eqz v0, :cond_2

    .line 112
    :cond_0
    iget-object v0, p0, Lcom/google/android/location/os/real/az;->h:[J

    aput-wide p2, v0, v5

    .line 113
    iget-object v0, p0, Lcom/google/android/location/os/real/az;->i:[J

    aput-wide v8, v0, v5

    .line 114
    iget-object v1, p0, Lcom/google/android/location/os/real/az;->c:Lcom/google/android/location/os/j;

    new-instance v0, Lcom/google/android/location/os/p;

    sget-object v2, Lcom/google/android/location/os/au;->x:Lcom/google/android/location/os/au;

    iget-object v3, v1, Lcom/google/android/location/os/j;->a:Lcom/google/android/location/os/at;

    invoke-interface {v3}, Lcom/google/android/location/os/at;->a()J

    move-result-wide v3

    move-wide v6, p2

    invoke-direct/range {v0 .. v7}, Lcom/google/android/location/os/p;-><init>(Lcom/google/android/location/os/j;Lcom/google/android/location/os/au;JIJ)V

    long-to-int v2, p2

    invoke-virtual {v1, v0, v5, v2}, Lcom/google/android/location/os/j;->a(Lcom/google/android/location/os/as;II)V

    .line 115
    invoke-static {}, Lcom/google/android/location/os/real/bf;->a()Lcom/google/android/location/os/real/bf;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/location/os/real/az;->d:Landroid/app/AlarmManager;

    const/4 v3, 0x2

    iget-object v0, p0, Lcom/google/android/location/os/real/az;->e:[Landroid/app/PendingIntent;

    aget-object v6, v0, v5

    move-wide v4, p2

    move-object v7, p4

    invoke-virtual/range {v1 .. v7}, Lcom/google/android/location/os/real/bf;->a(Landroid/app/AlarmManager;IJLandroid/app/PendingIntent;Lcom/google/android/location/o/n;)V

    .line 117
    iget-object v0, p0, Lcom/google/android/location/os/real/az;->f:Lcom/google/android/location/os/real/x;

    move-object v1, p1

    move-wide v2, p2

    move-wide v4, v8

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/location/os/real/x;->a(Lcom/google/android/location/j/k;JJ)V

    .line 122
    :cond_1
    :goto_0
    return-void

    .line 119
    :cond_2
    sget-boolean v0, Lcom/google/android/location/i/a;->b:Z

    if-eqz v0, :cond_1

    const-string v0, "RealTimers"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Redundant alarmReset of "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p1, Lcom/google/android/location/j/k;->m:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " with deadline "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/location/o/a/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/location/j/k;Lcom/google/android/location/o/n;)V
    .locals 7

    .prologue
    .line 177
    invoke-virtual {p1}, Lcom/google/android/location/j/k;->ordinal()I

    move-result v6

    .line 178
    iget-object v2, p0, Lcom/google/android/location/os/real/az;->c:Lcom/google/android/location/os/j;

    new-instance v1, Lcom/google/android/location/os/w;

    sget-object v3, Lcom/google/android/location/os/au;->R:Lcom/google/android/location/os/au;

    iget-object v0, v2, Lcom/google/android/location/os/j;->a:Lcom/google/android/location/os/at;

    invoke-interface {v0}, Lcom/google/android/location/os/at;->a()J

    move-result-wide v4

    invoke-direct/range {v1 .. v6}, Lcom/google/android/location/os/w;-><init>(Lcom/google/android/location/os/j;Lcom/google/android/location/os/au;JI)V

    invoke-virtual {v2, v1, v6}, Lcom/google/android/location/os/j;->a(Lcom/google/android/location/os/as;I)V

    .line 179
    iget-object v0, p0, Lcom/google/android/location/os/real/az;->g:[Lcom/google/android/location/o/i;

    aget-object v0, v0, v6

    .line 180
    iget-object v1, p0, Lcom/google/android/location/os/real/az;->b:Landroid/content/Context;

    const-string v2, "android.permission.UPDATE_DEVICE_STATS"

    invoke-virtual {v1, v2}, Landroid/content/Context;->checkCallingOrSelfPermission(Ljava/lang/String;)I

    move-result v1

    if-nez v1, :cond_0

    .line 182
    invoke-virtual {v0, p2}, Lcom/google/android/location/o/h;->a(Lcom/google/android/location/o/n;)V

    .line 184
    :cond_0
    invoke-virtual {v0}, Lcom/google/android/location/o/h;->a()V

    .line 185
    return-void
.end method

.method public final a(Z)V
    .locals 7

    .prologue
    .line 197
    invoke-static {}, Lcom/google/android/location/j/k;->values()[Lcom/google/android/location/j/k;

    move-result-object v1

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_4

    aget-object v3, v1, v0

    .line 198
    invoke-virtual {v3}, Lcom/google/android/location/j/k;->ordinal()I

    move-result v4

    .line 199
    if-eqz p1, :cond_0

    iget-boolean v5, v3, Lcom/google/android/location/j/k;->n:Z

    if-eqz v5, :cond_3

    .line 200
    :cond_0
    iget-object v5, p0, Lcom/google/android/location/os/real/az;->g:[Lcom/google/android/location/o/i;

    aget-object v4, v5, v4

    invoke-virtual {v4}, Lcom/google/android/location/o/i;->c()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 201
    sget-boolean v4, Lcom/google/android/location/i/a;->b:Z

    if-eqz v4, :cond_1

    const-string v4, "RealTimers"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Client "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v6, v3, Lcom/google/android/location/j/k;->m:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " leaked wakelock."

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/android/location/o/a/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 202
    :cond_1
    invoke-virtual {p0, v3}, Lcom/google/android/location/os/real/az;->a(Lcom/google/android/location/j/k;)V

    .line 204
    :cond_2
    invoke-virtual {p0, v3}, Lcom/google/android/location/os/real/az;->b(Lcom/google/android/location/j/k;)V

    .line 197
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 207
    :cond_4
    return-void
.end method

.method public final a(Ljava/lang/Runnable;)Z
    .locals 1

    .prologue
    .line 211
    iget-object v0, p0, Lcom/google/android/location/os/real/az;->f:Lcom/google/android/location/os/real/x;

    invoke-virtual {v0, p1}, Lcom/google/android/location/os/real/x;->a(Ljava/lang/Runnable;)Z

    move-result v0

    return v0
.end method

.method public final b(Lcom/google/android/location/j/k;)V
    .locals 7

    .prologue
    .line 162
    invoke-virtual {p1}, Lcom/google/android/location/j/k;->ordinal()I

    move-result v6

    .line 165
    invoke-virtual {p0, v6}, Lcom/google/android/location/os/real/az;->a(I)V

    .line 166
    iget-object v2, p0, Lcom/google/android/location/os/real/az;->c:Lcom/google/android/location/os/j;

    new-instance v1, Lcom/google/android/location/os/r;

    sget-object v3, Lcom/google/android/location/os/au;->z:Lcom/google/android/location/os/au;

    iget-object v0, v2, Lcom/google/android/location/os/j;->a:Lcom/google/android/location/os/at;

    invoke-interface {v0}, Lcom/google/android/location/os/at;->a()J

    move-result-wide v4

    invoke-direct/range {v1 .. v6}, Lcom/google/android/location/os/r;-><init>(Lcom/google/android/location/os/j;Lcom/google/android/location/os/au;JI)V

    invoke-virtual {v2, v1, v6}, Lcom/google/android/location/os/j;->a(Lcom/google/android/location/os/as;I)V

    .line 167
    iget-object v0, p0, Lcom/google/android/location/os/real/az;->d:Landroid/app/AlarmManager;

    iget-object v1, p0, Lcom/google/android/location/os/real/az;->e:[Landroid/app/PendingIntent;

    aget-object v1, v1, v6

    invoke-virtual {v0, v1}, Landroid/app/AlarmManager;->cancel(Landroid/app/PendingIntent;)V

    .line 168
    return-void
.end method
