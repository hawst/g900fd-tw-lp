.class public final Lcom/google/android/location/reporting/service/p;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method public static a(I)I
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 50
    if-eq p0, v0, :cond_0

    :goto_0
    const-string v1, "Caller must handle DEFINED itself"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/bx;->b(ZLjava/lang/Object;)V

    .line 51
    packed-switch p0, :pswitch_data_0

    .line 61
    sget v0, Lcom/google/android/gms/p;->qG:I

    :goto_1
    return v0

    .line 50
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 53
    :pswitch_0
    sget v0, Lcom/google/android/gms/p;->qB:I

    goto :goto_1

    .line 55
    :pswitch_1
    sget v0, Lcom/google/android/gms/p;->qD:I

    goto :goto_1

    .line 57
    :pswitch_2
    sget v0, Lcom/google/android/gms/p;->qz:I

    goto :goto_1

    .line 59
    :pswitch_3
    sget v0, Lcom/google/android/gms/p;->qy:I

    goto :goto_1

    .line 51
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
