.class public final Lcom/google/android/location/copresence/l/m;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static d:Lcom/google/android/location/copresence/l/m;


# instance fields
.field final a:Lcom/google/android/location/copresence/q/r;

.field public b:Lcom/google/android/location/copresence/l/y;

.field public c:Lcom/google/android/location/copresence/l/x;

.field private final e:Lcom/google/android/location/copresence/l/t;

.field private final f:Lcom/google/android/location/copresence/o/k;

.field private final g:Lcom/google/android/location/copresence/a/b;


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .locals 4

    .prologue
    .line 93
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 94
    new-instance v0, Lcom/google/android/location/copresence/q/r;

    invoke-static {p1}, Lcom/google/android/location/copresence/ap;->a(Landroid/content/Context;)Lcom/google/android/location/copresence/ap;

    move-result-object v1

    invoke-static {}, Lcom/google/android/location/copresence/f/b;->b()Lcom/google/ac/b/c/g;

    move-result-object v2

    iget-object v2, v2, Lcom/google/ac/b/c/g;->e:Lcom/google/ac/b/c/m;

    iget-object v2, v2, Lcom/google/ac/b/c/m;->p:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/location/copresence/q/r;-><init>(Lcom/google/android/location/copresence/ap;J)V

    iput-object v0, p0, Lcom/google/android/location/copresence/l/m;->a:Lcom/google/android/location/copresence/q/r;

    .line 96
    iget-object v0, p0, Lcom/google/android/location/copresence/l/m;->a:Lcom/google/android/location/copresence/q/r;

    new-instance v1, Lcom/google/android/location/copresence/l/n;

    invoke-direct {v1, p0}, Lcom/google/android/location/copresence/l/n;-><init>(Lcom/google/android/location/copresence/l/m;)V

    invoke-virtual {v0, v1}, Lcom/google/android/location/copresence/q/r;->a(Lcom/google/android/location/copresence/q/v;)V

    .line 102
    invoke-static {p1}, Lcom/google/android/location/copresence/l/t;->a(Landroid/content/Context;)Lcom/google/android/location/copresence/l/t;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/copresence/l/m;->e:Lcom/google/android/location/copresence/l/t;

    .line 103
    invoke-static {p1}, Lcom/google/android/location/copresence/o/k;->a(Landroid/content/Context;)Lcom/google/android/location/copresence/o/k;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/copresence/l/m;->f:Lcom/google/android/location/copresence/o/k;

    .line 104
    invoke-static {p1}, Lcom/google/android/location/copresence/a/b;->a(Landroid/content/Context;)Lcom/google/android/location/copresence/a/b;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/copresence/l/m;->g:Lcom/google/android/location/copresence/a/b;

    .line 105
    return-void
.end method

.method public static declared-synchronized a(Landroid/content/Context;)Lcom/google/android/location/copresence/l/m;
    .locals 2

    .prologue
    .line 87
    const-class v1, Lcom/google/android/location/copresence/l/m;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/google/android/location/copresence/l/m;->d:Lcom/google/android/location/copresence/l/m;

    if-nez v0, :cond_0

    .line 88
    new-instance v0, Lcom/google/android/location/copresence/l/m;

    invoke-direct {v0, p0}, Lcom/google/android/location/copresence/l/m;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/google/android/location/copresence/l/m;->d:Lcom/google/android/location/copresence/l/m;

    .line 90
    :cond_0
    sget-object v0, Lcom/google/android/location/copresence/l/m;->d:Lcom/google/android/location/copresence/l/m;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 87
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final declared-synchronized a()V
    .locals 3

    .prologue
    .line 116
    monitor-enter p0

    const-wide/16 v0, 0x0

    const/4 v2, 0x0

    :try_start_0
    invoke-virtual {p0, v0, v1, v2}, Lcom/google/android/location/copresence/l/m;->a(JLcom/google/android/location/copresence/l/w;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 117
    monitor-exit p0

    return-void

    .line 116
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(JLcom/google/android/location/copresence/l/w;)V
    .locals 5

    .prologue
    .line 128
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/location/copresence/l/m;->f:Lcom/google/android/location/copresence/o/k;

    iget-object v1, v0, Lcom/google/android/location/copresence/o/k;->b:Lcom/google/android/location/copresence/ap;

    invoke-virtual {v1}, Lcom/google/android/location/copresence/ap;->c()V

    iget-object v0, v0, Lcom/google/android/location/copresence/o/k;->a:Lcom/google/android/location/copresence/o/d;

    invoke-virtual {v0}, Lcom/google/android/location/copresence/o/d;->a()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 129
    iget-object v2, p0, Lcom/google/android/location/copresence/l/m;->g:Lcom/google/android/location/copresence/a/b;

    invoke-virtual {v2, v0}, Lcom/google/android/location/copresence/a/b;->a(Ljava/lang/String;)Lcom/google/android/location/copresence/a/a;

    move-result-object v0

    .line 130
    if-eqz v0, :cond_0

    .line 131
    iget-object v2, p0, Lcom/google/android/location/copresence/l/m;->a:Lcom/google/android/location/copresence/q/r;

    new-instance v3, Lcom/google/android/location/copresence/l/o;

    invoke-direct {v3, v0, p3}, Lcom/google/android/location/copresence/l/o;-><init>(Lcom/google/android/location/copresence/a/a;Lcom/google/android/location/copresence/l/w;)V

    invoke-virtual {v2, v3, p1, p2}, Lcom/google/android/location/copresence/q/r;->a(Ljava/lang/Object;J)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 128
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 136
    :cond_1
    monitor-exit p0

    return-void
.end method

.method final declared-synchronized a(Lcom/google/android/location/copresence/l/o;)V
    .locals 23

    .prologue
    .line 146
    monitor-enter p0

    :try_start_0
    move-object/from16 v0, p1

    iget-object v4, v0, Lcom/google/android/location/copresence/l/o;->a:Lcom/google/android/location/copresence/a/a;

    .line 147
    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/google/android/location/copresence/l/o;->b:Lcom/google/android/location/copresence/l/w;

    move-object/from16 v17, v0

    .line 149
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/location/copresence/l/m;->f:Lcom/google/android/location/copresence/o/k;

    iget-object v5, v4, Lcom/google/android/location/copresence/a/a;->a:Landroid/accounts/Account;

    iget-object v5, v5, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v3, v5}, Lcom/google/android/location/copresence/o/k;->a(Ljava/lang/String;)Ljava/util/Set;

    move-result-object v3

    .line 152
    const/4 v9, 0x0

    .line 154
    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v22

    :goto_0
    invoke-interface/range {v22 .. v22}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_4

    invoke-interface/range {v22 .. v22}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 155
    new-instance v8, Ljava/util/HashSet;

    invoke-direct {v8}, Ljava/util/HashSet;-><init>()V

    .line 157
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/location/copresence/l/m;->f:Lcom/google/android/location/copresence/o/k;

    invoke-virtual {v5, v3, v4}, Lcom/google/android/location/copresence/o/k;->a(Ljava/lang/String;Lcom/google/android/location/copresence/a/a;)Lcom/google/android/location/copresence/o/m;

    move-result-object v6

    sget-object v7, Lcom/google/android/location/copresence/g/d;->a:Lcom/google/android/location/copresence/g/d;

    if-nez v7, :cond_0

    new-instance v7, Lcom/google/android/location/copresence/g/d;

    invoke-direct {v7}, Lcom/google/android/location/copresence/g/d;-><init>()V

    sput-object v7, Lcom/google/android/location/copresence/g/d;->a:Lcom/google/android/location/copresence/g/d;

    :cond_0
    sget-object v7, Lcom/google/android/location/copresence/g/d;->a:Lcom/google/android/location/copresence/g/d;

    invoke-static/range {v3 .. v8}, Lcom/google/android/location/copresence/g/a;->a(Ljava/lang/String;Lcom/google/android/location/copresence/a/a;Lcom/google/android/location/copresence/o/k;Lcom/google/android/location/copresence/o/m;Lcom/google/android/location/copresence/g/c;Ljava/util/HashSet;)Lcom/google/android/location/copresence/g/b;

    move-result-object v10

    .line 160
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/location/copresence/l/m;->f:Lcom/google/android/location/copresence/o/k;

    invoke-static {v4, v3, v5, v8}, Lcom/google/android/location/copresence/g/a;->a(Lcom/google/android/location/copresence/a/a;Ljava/lang/String;Lcom/google/android/location/copresence/o/k;Ljava/util/HashSet;)[Ljava/lang/String;

    move-result-object v13

    .line 163
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/location/copresence/l/m;->f:Lcom/google/android/location/copresence/o/k;

    invoke-virtual {v5, v3, v4}, Lcom/google/android/location/copresence/o/k;->b(Ljava/lang/String;Lcom/google/android/location/copresence/a/a;)Lcom/google/android/location/copresence/o/m;

    move-result-object v6

    sget-object v7, Lcom/google/android/location/copresence/g/e;->a:Lcom/google/android/location/copresence/g/e;

    if-nez v7, :cond_1

    new-instance v7, Lcom/google/android/location/copresence/g/e;

    invoke-direct {v7}, Lcom/google/android/location/copresence/g/e;-><init>()V

    sput-object v7, Lcom/google/android/location/copresence/g/e;->a:Lcom/google/android/location/copresence/g/e;

    :cond_1
    sget-object v7, Lcom/google/android/location/copresence/g/e;->a:Lcom/google/android/location/copresence/g/e;

    invoke-static/range {v3 .. v8}, Lcom/google/android/location/copresence/g/a;->a(Ljava/lang/String;Lcom/google/android/location/copresence/a/a;Lcom/google/android/location/copresence/o/k;Lcom/google/android/location/copresence/o/m;Lcom/google/android/location/copresence/g/c;Ljava/util/HashSet;)Lcom/google/android/location/copresence/g/b;

    move-result-object v6

    .line 166
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/location/copresence/l/m;->f:Lcom/google/android/location/copresence/o/k;

    invoke-static {v4, v3, v5, v8}, Lcom/google/android/location/copresence/g/a;->b(Lcom/google/android/location/copresence/a/a;Ljava/lang/String;Lcom/google/android/location/copresence/o/k;Ljava/util/HashSet;)[Ljava/lang/String;

    move-result-object v15

    .line 170
    invoke-virtual {v10}, Lcom/google/android/location/copresence/g/b;->a()[Lcom/google/protobuf/nano/j;

    move-result-object v5

    check-cast v5, [Lcom/google/ac/b/c/bf;

    array-length v5, v5

    if-gtz v5, :cond_2

    array-length v5, v13

    if-gtz v5, :cond_2

    invoke-virtual {v6}, Lcom/google/android/location/copresence/g/b;->a()[Lcom/google/protobuf/nano/j;

    move-result-object v5

    check-cast v5, [Lcom/google/ac/b/c/bo;

    array-length v5, v5

    if-gtz v5, :cond_2

    array-length v5, v15

    if-lez v5, :cond_7

    .line 174
    :cond_2
    const/4 v5, 0x2

    invoke-static {v5}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 175
    new-instance v5, Ljava/lang/StringBuilder;

    const-string v7, "NetworkPollManager: Copresence operation on behalf of "

    invoke-direct {v5, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/google/android/location/copresence/ag;->a(Ljava/lang/String;)I

    .line 177
    :cond_3
    const/4 v5, 0x1

    .line 178
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/location/copresence/l/m;->e:Lcom/google/android/location/copresence/l/t;

    invoke-virtual {v10}, Lcom/google/android/location/copresence/g/b;->a()[Lcom/google/protobuf/nano/j;

    move-result-object v12

    check-cast v12, [Lcom/google/ac/b/c/bf;

    invoke-virtual {v6}, Lcom/google/android/location/copresence/g/b;->a()[Lcom/google/protobuf/nano/j;

    move-result-object v14

    check-cast v14, [Lcom/google/ac/b/c/bo;

    iget-object v7, v10, Lcom/google/android/location/copresence/g/b;->b:Ljava/util/HashSet;

    iget-object v6, v6, Lcom/google/android/location/copresence/g/b;->b:Ljava/util/HashSet;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/location/copresence/l/m;->b:Lcom/google/android/location/copresence/l/y;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/location/copresence/l/m;->c:Lcom/google/android/location/copresence/l/x;

    move-object/from16 v19, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/location/copresence/l/m;->f:Lcom/google/android/location/copresence/o/k;

    move-object/from16 v20, v0

    new-instance v21, Lcom/google/android/location/copresence/l/p;

    move-object/from16 v0, v21

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    invoke-direct {v0, v1, v2}, Lcom/google/android/location/copresence/l/p;-><init>(Lcom/google/android/location/copresence/l/m;Lcom/google/android/location/copresence/l/o;)V

    move-object v10, v4

    move-object v11, v3

    move-object/from16 v16, v8

    invoke-virtual/range {v9 .. v21}, Lcom/google/android/location/copresence/l/t;->a(Lcom/google/android/location/copresence/a/a;Ljava/lang/String;[Lcom/google/ac/b/c/bf;[Ljava/lang/String;[Lcom/google/ac/b/c/bo;[Ljava/lang/String;Ljava/util/HashSet;Lcom/google/android/location/copresence/l/w;Lcom/google/android/location/copresence/l/y;Lcom/google/android/location/copresence/l/x;Lcom/google/android/location/copresence/o/k;Lcom/google/android/location/copresence/l/p;)V

    move v3, v5

    :goto_1
    move v9, v3

    .line 194
    goto/16 :goto_0

    .line 196
    :cond_4
    if-nez v9, :cond_6

    iget-object v3, v4, Lcom/google/android/location/copresence/a/a;->a:Landroid/accounts/Account;

    iget-object v3, v3, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-static {v3}, Lcom/google/android/location/copresence/a/h;->a(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_6

    .line 198
    const/4 v3, 0x2

    invoke-static {v3}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 199
    const-string v3, "NetworkPollManager: Heartbeating"

    invoke-static {v3}, Lcom/google/android/location/copresence/ag;->a(Ljava/lang/String;)I

    .line 202
    :cond_5
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/location/copresence/l/m;->e:Lcom/google/android/location/copresence/l/t;

    const/4 v5, 0x0

    const/4 v6, 0x0

    new-array v6, v6, [Lcom/google/ac/b/c/bf;

    const/4 v7, 0x0

    new-array v7, v7, [Ljava/lang/String;

    const/4 v8, 0x0

    new-array v8, v8, [Lcom/google/ac/b/c/bo;

    const/4 v9, 0x0

    new-array v9, v9, [Ljava/lang/String;

    new-instance v10, Ljava/util/HashSet;

    invoke-direct {v10}, Ljava/util/HashSet;-><init>()V

    new-instance v11, Ljava/util/HashSet;

    invoke-direct {v11}, Ljava/util/HashSet;-><init>()V

    new-instance v11, Ljava/util/HashSet;

    invoke-direct {v11}, Ljava/util/HashSet;-><init>()V

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/location/copresence/l/m;->b:Lcom/google/android/location/copresence/l/y;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/location/copresence/l/m;->c:Lcom/google/android/location/copresence/l/x;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/location/copresence/l/m;->f:Lcom/google/android/location/copresence/o/k;

    new-instance v15, Lcom/google/android/location/copresence/l/p;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v15, v0, v1}, Lcom/google/android/location/copresence/l/p;-><init>(Lcom/google/android/location/copresence/l/m;Lcom/google/android/location/copresence/l/o;)V

    move-object/from16 v11, v17

    invoke-virtual/range {v3 .. v15}, Lcom/google/android/location/copresence/l/t;->a(Lcom/google/android/location/copresence/a/a;Ljava/lang/String;[Lcom/google/ac/b/c/bf;[Ljava/lang/String;[Lcom/google/ac/b/c/bo;[Ljava/lang/String;Ljava/util/HashSet;Lcom/google/android/location/copresence/l/w;Lcom/google/android/location/copresence/l/y;Lcom/google/android/location/copresence/l/x;Lcom/google/android/location/copresence/o/k;Lcom/google/android/location/copresence/l/p;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 218
    :cond_6
    monitor-exit p0

    return-void

    .line 146
    :catchall_0
    move-exception v3

    monitor-exit p0

    throw v3

    :cond_7
    move v3, v9

    goto :goto_1
.end method
