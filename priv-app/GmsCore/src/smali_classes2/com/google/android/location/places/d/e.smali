.class public final Lcom/google/android/location/places/d/e;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/location/places/b/c;


# static fields
.field static final a:J

.field static final b:J

.field static final c:J


# instance fields
.field private final d:Landroid/content/Context;

.field private final e:Lcom/google/android/location/places/c/j;

.field private final f:Lcom/google/android/location/places/bz;

.field private final g:Lcom/google/android/location/places/c/d;

.field private final h:Lcom/google/android/location/places/d/i;

.field private final i:Lcom/google/android/location/places/b/a;

.field private final j:Lcom/google/android/location/fused/g;

.field private final k:Lcom/google/android/gms/common/util/p;

.field private final l:Lcom/google/android/location/places/d/b;

.field private final m:Lcom/google/android/location/places/d/d;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x1

    .line 52
    sget-object v0, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    sput-wide v0, Lcom/google/android/location/places/d/e;->a:J

    .line 54
    sget-object v0, Ljava/util/concurrent/TimeUnit;->HOURS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    sput-wide v0, Lcom/google/android/location/places/d/e;->b:J

    .line 58
    sget-object v0, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0xa

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    sput-wide v0, Lcom/google/android/location/places/d/e;->c:J

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/android/location/places/c/j;Lcom/google/android/location/places/bz;Lcom/google/android/location/places/c/d;Lcom/google/android/location/places/d/i;Lcom/google/android/location/fused/g;Lcom/google/android/location/places/b/a;Landroid/app/AlarmManager;Lcom/google/android/location/geofencer/service/ah;Landroid/support/v4/a/m;Lcom/google/android/gms/common/util/p;)V
    .locals 8

    .prologue
    .line 89
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 70
    new-instance v1, Lcom/google/android/location/places/d/f;

    invoke-direct {v1, p0}, Lcom/google/android/location/places/d/f;-><init>(Lcom/google/android/location/places/d/e;)V

    iput-object v1, p0, Lcom/google/android/location/places/d/e;->m:Lcom/google/android/location/places/d/d;

    .line 90
    iput-object p1, p0, Lcom/google/android/location/places/d/e;->d:Landroid/content/Context;

    .line 91
    iput-object p2, p0, Lcom/google/android/location/places/d/e;->e:Lcom/google/android/location/places/c/j;

    .line 92
    iput-object p3, p0, Lcom/google/android/location/places/d/e;->f:Lcom/google/android/location/places/bz;

    .line 93
    iput-object p4, p0, Lcom/google/android/location/places/d/e;->g:Lcom/google/android/location/places/c/d;

    .line 94
    iput-object p5, p0, Lcom/google/android/location/places/d/e;->h:Lcom/google/android/location/places/d/i;

    .line 95
    iput-object p6, p0, Lcom/google/android/location/places/d/e;->j:Lcom/google/android/location/fused/g;

    .line 96
    iput-object p7, p0, Lcom/google/android/location/places/d/e;->i:Lcom/google/android/location/places/b/a;

    .line 97
    move-object/from16 v0, p11

    iput-object v0, p0, Lcom/google/android/location/places/d/e;->k:Lcom/google/android/gms/common/util/p;

    .line 98
    new-instance v1, Lcom/google/android/location/places/d/b;

    iget-object v7, p0, Lcom/google/android/location/places/d/e;->m:Lcom/google/android/location/places/d/d;

    move-object v2, p1

    move-object/from16 v3, p11

    move-object/from16 v4, p8

    move-object/from16 v5, p9

    move-object/from16 v6, p10

    invoke-direct/range {v1 .. v7}, Lcom/google/android/location/places/d/b;-><init>(Landroid/content/Context;Lcom/google/android/gms/common/util/p;Landroid/app/AlarmManager;Lcom/google/android/location/geofencer/service/ah;Landroid/support/v4/a/m;Lcom/google/android/location/places/d/d;)V

    iput-object v1, p0, Lcom/google/android/location/places/d/e;->l:Lcom/google/android/location/places/d/b;

    .line 101
    return-void
.end method

.method static synthetic a(Lcom/google/android/location/places/d/e;)Lcom/google/android/location/fused/g;
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lcom/google/android/location/places/d/e;->j:Lcom/google/android/location/fused/g;

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/location/places/d/e;Lcom/google/android/location/places/Subscription;)V
    .locals 0

    .prologue
    .line 46
    invoke-direct {p0, p1}, Lcom/google/android/location/places/d/e;->d(Lcom/google/android/location/places/Subscription;)V

    return-void
.end method

.method static synthetic b(Lcom/google/android/location/places/d/e;)Lcom/google/android/location/places/bz;
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lcom/google/android/location/places/d/e;->f:Lcom/google/android/location/places/bz;

    return-object v0
.end method

.method static synthetic c(Lcom/google/android/location/places/d/e;)Lcom/google/android/location/places/c/j;
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lcom/google/android/location/places/d/e;->e:Lcom/google/android/location/places/c/j;

    return-object v0
.end method

.method static synthetic d(Lcom/google/android/location/places/d/e;)Lcom/google/android/location/places/d/b;
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lcom/google/android/location/places/d/e;->l:Lcom/google/android/location/places/d/b;

    return-object v0
.end method

.method private d(Lcom/google/android/location/places/Subscription;)V
    .locals 1

    .prologue
    .line 156
    new-instance v0, Lcom/google/android/location/places/d/g;

    invoke-direct {v0, p0, p1}, Lcom/google/android/location/places/d/g;-><init>(Lcom/google/android/location/places/d/e;Lcom/google/android/location/places/Subscription;)V

    invoke-virtual {v0}, Lcom/google/android/location/places/d/g;->run()V

    .line 157
    return-void
.end method

.method static synthetic e(Lcom/google/android/location/places/d/e;)Lcom/google/android/location/places/c/d;
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lcom/google/android/location/places/d/e;->g:Lcom/google/android/location/places/c/d;

    return-object v0
.end method

.method static synthetic f(Lcom/google/android/location/places/d/e;)Lcom/google/android/location/places/d/i;
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lcom/google/android/location/places/d/e;->h:Lcom/google/android/location/places/d/i;

    return-object v0
.end method

.method static synthetic g(Lcom/google/android/location/places/d/e;)Lcom/google/android/location/places/b/a;
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lcom/google/android/location/places/d/e;->i:Lcom/google/android/location/places/b/a;

    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;Lcom/google/android/location/places/Subscription;)Lcom/google/android/gms/location/places/personalized/PlaceUserData;
    .locals 2

    .prologue
    .line 132
    invoke-virtual {p2}, Lcom/google/android/location/places/Subscription;->a()Lcom/google/android/gms/location/places/internal/PlacesParams;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/gms/location/places/internal/PlacesParams;->e:Ljava/lang/String;

    .line 133
    invoke-virtual {p2}, Lcom/google/android/location/places/Subscription;->b()Lcom/google/android/gms/location/places/PlaceFilter;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/location/places/PlaceFilter;->f()Ljava/util/Set;

    move-result-object v1

    invoke-virtual {p0, v0, p1, v1}, Lcom/google/android/location/places/d/e;->a(Ljava/lang/String;Ljava/lang/String;Ljava/util/Set;)Lcom/google/android/gms/location/places/personalized/PlaceUserData;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Ljava/util/Set;)Lcom/google/android/gms/location/places/personalized/PlaceUserData;
    .locals 4

    .prologue
    .line 144
    iget-object v0, p0, Lcom/google/android/location/places/d/e;->h:Lcom/google/android/location/places/d/i;

    invoke-static {p3}, Ljava/util/Collections;->unmodifiableSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v1

    iget-object v0, v0, Lcom/google/android/location/places/d/i;->a:Lcom/google/k/c/dn;

    invoke-interface {v0, p2}, Lcom/google/k/c/dn;->c(Ljava/lang/Object;)Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/location/places/personalized/PlaceUserData;

    invoke-virtual {v0}, Lcom/google/android/gms/location/places/personalized/PlaceUserData;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {v0, v1}, Lcom/google/android/gms/location/places/personalized/PlaceUserData;->a(Ljava/util/Set;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 146
    :goto_0
    return-object v0

    .line 144
    :cond_1
    const/4 v0, 0x0

    .line 146
    goto :goto_0
.end method

.method public final a(Lcom/google/android/location/places/Subscription;)V
    .locals 0

    .prologue
    .line 151
    invoke-direct {p0, p1}, Lcom/google/android/location/places/d/e;->d(Lcom/google/android/location/places/Subscription;)V

    .line 152
    return-void
.end method

.method public final b(Lcom/google/android/location/places/Subscription;)V
    .locals 2

    .prologue
    .line 108
    invoke-virtual {p1}, Lcom/google/android/location/places/Subscription;->f()Z

    move-result v0

    if-nez v0, :cond_0

    .line 109
    const-string v0, "Places"

    const/4 v1, 0x5

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 110
    const-string v0, "Places"

    const-string v1, "Adding subscription that doesn\'t require user data - seems wrong"

    invoke-static {v0, v1}, Lcom/google/android/location/n/aa;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 113
    :cond_0
    invoke-direct {p0, p1}, Lcom/google/android/location/places/d/e;->d(Lcom/google/android/location/places/Subscription;)V

    .line 114
    return-void
.end method

.method public final c(Lcom/google/android/location/places/Subscription;)V
    .locals 3

    .prologue
    .line 123
    iget-object v1, p0, Lcom/google/android/location/places/d/e;->l:Lcom/google/android/location/places/d/b;

    iget-object v0, v1, Lcom/google/android/location/places/d/b;->g:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, v1, Lcom/google/android/location/places/d/b;->g:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iget-object v2, v1, Lcom/google/android/location/places/d/b;->g:Ljava/util/Map;

    invoke-interface {v2, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v1, v1, Lcom/google/android/location/places/d/b;->h:Ljava/util/Map;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 124
    :cond_0
    iget-object v0, p0, Lcom/google/android/location/places/d/e;->i:Lcom/google/android/location/places/b/a;

    invoke-virtual {v0, p1}, Lcom/google/android/location/places/b/a;->a(Lcom/google/android/location/places/Subscription;)V

    .line 125
    return-void
.end method
