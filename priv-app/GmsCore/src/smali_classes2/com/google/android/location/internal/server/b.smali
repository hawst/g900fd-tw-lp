.class public final Lcom/google/android/location/internal/server/b;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final a:Ljava/util/HashSet;

.field final b:I

.field final c:Ljava/util/Map;

.field final d:Ljava/util/Map;

.field e:Lcom/google/android/location/o/n;

.field f:I

.field g:Z

.field h:Lcom/google/android/location/b/at;

.field final i:Lcom/google/android/location/o/a;

.field j:Lcom/google/android/location/os/j;

.field k:Ljava/util/Set;

.field l:Ljava/util/HashSet;


# direct methods
.method public constructor <init>(ILcom/google/android/location/b/at;Lcom/google/android/location/o/a;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/16 v2, 0x9

    .line 65
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 45
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/location/internal/server/b;->a:Ljava/util/HashSet;

    .line 47
    new-instance v0, Ljava/util/HashMap;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/HashMap;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/location/internal/server/b;->c:Ljava/util/Map;

    .line 49
    new-instance v0, Ljava/util/HashMap;

    const/16 v1, 0x28

    invoke-direct {v0, v1}, Ljava/util/HashMap;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/location/internal/server/b;->d:Ljava/util/Map;

    .line 53
    iput-object v3, p0, Lcom/google/android/location/internal/server/b;->e:Lcom/google/android/location/o/n;

    .line 54
    const v0, 0x7fffffff

    iput v0, p0, Lcom/google/android/location/internal/server/b;->f:I

    .line 55
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/location/internal/server/b;->g:Z

    .line 59
    iput-object v3, p0, Lcom/google/android/location/internal/server/b;->j:Lcom/google/android/location/os/j;

    .line 61
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/location/internal/server/b;->l:Ljava/util/HashSet;

    .line 66
    iput p1, p0, Lcom/google/android/location/internal/server/b;->b:I

    .line 67
    iput-object p2, p0, Lcom/google/android/location/internal/server/b;->h:Lcom/google/android/location/b/at;

    .line 68
    iput-object p3, p0, Lcom/google/android/location/internal/server/b;->i:Lcom/google/android/location/o/a;

    .line 69
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/location/internal/server/b;->k:Ljava/util/Set;

    .line 70
    iget-object v0, p0, Lcom/google/android/location/internal/server/b;->k:Ljava/util/Set;

    const/4 v1, 0x6

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 71
    iget-object v0, p0, Lcom/google/android/location/internal/server/b;->k:Ljava/util/Set;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 72
    iget-object v0, p0, Lcom/google/android/location/internal/server/b;->a:Ljava/util/HashSet;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 73
    return-void
.end method

.method static a(Ljava/io/PrintWriter;JLcom/google/android/location/internal/server/p;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 173
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "ActivityClient: duration="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {p1, p2}, Lcom/google/android/location/internal/server/i;->a(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " period: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p3, Lcom/google/android/location/internal/server/p;->first:Ljava/lang/Object;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " package: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 175
    const-string v0, "com.google.android.gms"

    invoke-virtual {v0, p4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 176
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v0, " tag: "

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v0, p3, Lcom/google/android/location/internal/server/p;->second:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 178
    :cond_0
    invoke-virtual {p0}, Ljava/io/PrintWriter;->println()V

    .line 179
    return-void
.end method


# virtual methods
.method final a()V
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 217
    const v0, 0x7fffffff

    iput v0, p0, Lcom/google/android/location/internal/server/b;->f:I

    .line 218
    iput-boolean v2, p0, Lcom/google/android/location/internal/server/b;->g:Z

    .line 219
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/location/internal/server/b;->l:Ljava/util/HashSet;

    .line 220
    iget-object v0, p0, Lcom/google/android/location/internal/server/b;->c:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/internal/server/c;

    .line 221
    sget-boolean v1, Lcom/google/android/location/i/a;->b:Z

    if-eqz v1, :cond_1

    const-string v1, "ActivityRecognitionListener"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Still have activity intent receiver "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v4}, Lcom/google/android/location/o/a/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 222
    :cond_1
    iget v1, p0, Lcom/google/android/location/internal/server/b;->f:I

    iget v4, v0, Lcom/google/android/location/internal/server/c;->d:I

    invoke-static {v1, v4}, Ljava/lang/Math;->min(II)I

    move-result v1

    iput v1, p0, Lcom/google/android/location/internal/server/b;->f:I

    .line 223
    iget-boolean v1, p0, Lcom/google/android/location/internal/server/b;->g:Z

    if-nez v1, :cond_2

    iget-boolean v1, v0, Lcom/google/android/location/internal/server/c;->i:Z

    if-eqz v1, :cond_3

    :cond_2
    const/4 v1, 0x1

    :goto_0
    iput-boolean v1, p0, Lcom/google/android/location/internal/server/b;->g:Z

    .line 224
    invoke-virtual {v0}, Lcom/google/android/location/internal/server/c;->a()[I

    move-result-object v1

    .line 225
    if-eqz v1, :cond_0

    .line 226
    array-length v4, v1

    move v0, v2

    :goto_1
    if-ge v0, v4, :cond_0

    aget v5, v1, v0

    .line 227
    iget-object v6, p0, Lcom/google/android/location/internal/server/b;->l:Ljava/util/HashSet;

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v6, v5}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 226
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_3
    move v1, v2

    .line 223
    goto :goto_0

    .line 232
    :cond_4
    invoke-static {}, Lcom/google/android/location/o/n;->a()Lcom/google/android/location/o/n;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/internal/server/b;->e:Lcom/google/android/location/o/n;

    .line 233
    iget v0, p0, Lcom/google/android/location/internal/server/b;->f:I

    add-int/lit8 v0, v0, 0x1

    mul-int/lit8 v0, v0, 0x3

    div-int/lit8 v0, v0, 0x2

    int-to-long v2, v0

    .line 234
    iget-object v0, p0, Lcom/google/android/location/internal/server/b;->c:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_5
    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/internal/server/c;

    .line 235
    iget v4, v0, Lcom/google/android/location/internal/server/c;->d:I

    int-to-long v4, v4

    cmp-long v4, v4, v2

    if-gez v4, :cond_5

    iget-object v4, v0, Lcom/google/android/location/internal/server/c;->j:Lcom/google/android/location/o/n;

    if-eqz v4, :cond_5

    .line 236
    iget-object v4, p0, Lcom/google/android/location/internal/server/b;->e:Lcom/google/android/location/o/n;

    iget-object v0, v0, Lcom/google/android/location/internal/server/c;->j:Lcom/google/android/location/o/n;

    invoke-virtual {v4, v0}, Lcom/google/android/location/o/n;->a(Lcom/google/android/location/o/n;)V

    goto :goto_2

    .line 239
    :cond_6
    return-void
.end method
