.class public final Lcom/google/android/location/b/aa;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public final a:J

.field public final b:J

.field public final c:J

.field public final d:J

.field public final e:J

.field public final f:J

.field public final g:J

.field public final h:J

.field public final i:J

.field public final j:J

.field public final k:J


# direct methods
.method public constructor <init>(JJJJJJJJJJJ)V
    .locals 5

    .prologue
    .line 176
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 177
    const-wide/16 v2, 0x0

    cmp-long v2, p1, v2

    if-ltz v2, :cond_0

    const/4 v2, 0x1

    :goto_0
    invoke-static {v2}, Lcom/google/android/location/o/j;->a(Z)V

    .line 178
    const-wide/16 v2, 0x0

    cmp-long v2, p3, v2

    if-ltz v2, :cond_1

    const/4 v2, 0x1

    :goto_1
    invoke-static {v2}, Lcom/google/android/location/o/j;->a(Z)V

    .line 179
    const-wide/16 v2, 0x0

    cmp-long v2, p5, v2

    if-ltz v2, :cond_2

    const/4 v2, 0x1

    :goto_2
    invoke-static {v2}, Lcom/google/android/location/o/j;->a(Z)V

    .line 180
    const-wide/16 v2, 0x0

    cmp-long v2, p7, v2

    if-ltz v2, :cond_3

    const/4 v2, 0x1

    :goto_3
    invoke-static {v2}, Lcom/google/android/location/o/j;->a(Z)V

    .line 181
    const-wide/16 v2, 0x0

    cmp-long v2, p9, v2

    if-ltz v2, :cond_4

    const/4 v2, 0x1

    :goto_4
    invoke-static {v2}, Lcom/google/android/location/o/j;->a(Z)V

    .line 182
    const-wide/16 v2, 0x0

    cmp-long v2, p11, v2

    if-ltz v2, :cond_5

    const/4 v2, 0x1

    :goto_5
    invoke-static {v2}, Lcom/google/android/location/o/j;->a(Z)V

    .line 183
    const-wide/16 v2, 0x0

    cmp-long v2, p13, v2

    if-ltz v2, :cond_6

    const/4 v2, 0x1

    :goto_6
    invoke-static {v2}, Lcom/google/android/location/o/j;->a(Z)V

    .line 184
    const-wide/16 v2, 0x0

    cmp-long v2, p15, v2

    if-ltz v2, :cond_7

    const/4 v2, 0x1

    :goto_7
    invoke-static {v2}, Lcom/google/android/location/o/j;->a(Z)V

    .line 185
    const-wide/16 v2, 0x0

    cmp-long v2, p17, v2

    if-ltz v2, :cond_8

    const/4 v2, 0x1

    :goto_8
    invoke-static {v2}, Lcom/google/android/location/o/j;->a(Z)V

    .line 186
    const-wide/16 v2, 0x0

    cmp-long v2, p19, v2

    if-ltz v2, :cond_9

    const/4 v2, 0x1

    :goto_9
    invoke-static {v2}, Lcom/google/android/location/o/j;->a(Z)V

    .line 187
    const-wide/16 v2, 0x0

    cmp-long v2, p21, v2

    if-ltz v2, :cond_a

    const/4 v2, 0x1

    :goto_a
    invoke-static {v2}, Lcom/google/android/location/o/j;->a(Z)V

    .line 188
    iput-wide p1, p0, Lcom/google/android/location/b/aa;->a:J

    .line 189
    iput-wide p3, p0, Lcom/google/android/location/b/aa;->b:J

    .line 190
    iput-wide p5, p0, Lcom/google/android/location/b/aa;->c:J

    .line 191
    iput-wide p7, p0, Lcom/google/android/location/b/aa;->d:J

    .line 192
    iput-wide p9, p0, Lcom/google/android/location/b/aa;->e:J

    .line 193
    move-wide/from16 v0, p11

    iput-wide v0, p0, Lcom/google/android/location/b/aa;->f:J

    .line 194
    move-wide/from16 v0, p13

    iput-wide v0, p0, Lcom/google/android/location/b/aa;->g:J

    .line 195
    move-wide/from16 v0, p15

    iput-wide v0, p0, Lcom/google/android/location/b/aa;->h:J

    .line 196
    move-wide/from16 v0, p17

    iput-wide v0, p0, Lcom/google/android/location/b/aa;->i:J

    .line 197
    move-wide/from16 v0, p19

    iput-wide v0, p0, Lcom/google/android/location/b/aa;->j:J

    .line 198
    move-wide/from16 v0, p21

    iput-wide v0, p0, Lcom/google/android/location/b/aa;->k:J

    .line 199
    return-void

    .line 177
    :cond_0
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 178
    :cond_1
    const/4 v2, 0x0

    goto/16 :goto_1

    .line 179
    :cond_2
    const/4 v2, 0x0

    goto :goto_2

    .line 180
    :cond_3
    const/4 v2, 0x0

    goto :goto_3

    .line 181
    :cond_4
    const/4 v2, 0x0

    goto :goto_4

    .line 182
    :cond_5
    const/4 v2, 0x0

    goto :goto_5

    .line 183
    :cond_6
    const/4 v2, 0x0

    goto :goto_6

    .line 184
    :cond_7
    const/4 v2, 0x0

    goto :goto_7

    .line 185
    :cond_8
    const/4 v2, 0x0

    goto :goto_8

    .line 186
    :cond_9
    const/4 v2, 0x0

    goto :goto_9

    .line 187
    :cond_a
    const/4 v2, 0x0

    goto :goto_a
.end method
