.class public final Lcom/google/android/location/copresence/l/e;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static d:Lcom/google/android/location/copresence/l/e;


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Lcom/google/android/location/copresence/q/a;

.field private final c:Lcom/google/android/location/copresence/q/d;


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 43
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    new-instance v0, Lcom/google/android/location/copresence/l/f;

    invoke-direct {v0, p0}, Lcom/google/android/location/copresence/l/f;-><init>(Lcom/google/android/location/copresence/l/e;)V

    iput-object v0, p0, Lcom/google/android/location/copresence/l/e;->c:Lcom/google/android/location/copresence/q/d;

    .line 44
    iput-object p1, p0, Lcom/google/android/location/copresence/l/e;->a:Landroid/content/Context;

    .line 45
    invoke-static {p1}, Lcom/google/android/location/copresence/q/a;->a(Landroid/content/Context;)Lcom/google/android/location/copresence/q/a;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/copresence/l/e;->b:Lcom/google/android/location/copresence/q/a;

    .line 46
    return-void
.end method

.method public static declared-synchronized a(Landroid/content/Context;)Lcom/google/android/location/copresence/l/e;
    .locals 2

    .prologue
    .line 37
    const-class v1, Lcom/google/android/location/copresence/l/e;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/google/android/location/copresence/l/e;->d:Lcom/google/android/location/copresence/l/e;

    if-nez v0, :cond_0

    .line 38
    new-instance v0, Lcom/google/android/location/copresence/l/e;

    invoke-direct {v0, p0}, Lcom/google/android/location/copresence/l/e;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/google/android/location/copresence/l/e;->d:Lcom/google/android/location/copresence/l/e;

    .line 40
    :cond_0
    sget-object v0, Lcom/google/android/location/copresence/l/e;->d:Lcom/google/android/location/copresence/l/e;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 37
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final a()Lcom/google/ac/b/c/as;
    .locals 10

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    const-wide v8, 0x416312d000000000L    # 1.0E7

    const/4 v5, 0x0

    .line 56
    iget-object v0, p0, Lcom/google/android/location/copresence/l/e;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/location/fused/g;->a(Landroid/content/Context;)Lcom/google/android/location/fused/g;

    move-result-object v0

    .line 59
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v1

    invoke-virtual {v0, v1, v5, v4, v3}, Lcom/google/android/location/fused/g;->a(ILjava/lang/String;ZZ)Landroid/location/Location;

    move-result-object v1

    .line 66
    if-nez v1, :cond_1

    move-object v6, v5

    .line 68
    :goto_0
    iget-object v0, p0, Lcom/google/android/location/copresence/l/e;->b:Lcom/google/android/location/copresence/q/a;

    if-eqz v0, :cond_0

    .line 69
    invoke-static {v1}, Lcom/google/android/location/copresence/q/a;->a(Landroid/location/Location;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 71
    iget-object v2, p0, Lcom/google/android/location/copresence/l/e;->b:Lcom/google/android/location/copresence/q/a;

    iget-object v0, p0, Lcom/google/android/location/copresence/l/e;->c:Lcom/google/android/location/copresence/q/d;

    iget-object v1, v2, Lcom/google/android/location/copresence/q/a;->b:Ljava/util/Set;

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    new-instance v0, Lcom/google/android/gms/location/LocationRequest;

    invoke-direct {v0}, Lcom/google/android/gms/location/LocationRequest;-><init>()V

    const/16 v1, 0x64

    invoke-virtual {v0, v1}, Lcom/google/android/gms/location/LocationRequest;->a(I)Lcom/google/android/gms/location/LocationRequest;

    move-result-object v0

    invoke-static {}, Lcom/google/android/location/copresence/f/b;->b()Lcom/google/ac/b/c/g;

    move-result-object v1

    iget-object v1, v1, Lcom/google/ac/b/c/g;->l:Lcom/google/ac/b/c/j;

    iget-object v1, v1, Lcom/google/ac/b/c/j;->e:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    invoke-virtual {v0, v8, v9}, Lcom/google/android/gms/location/LocationRequest;->a(J)Lcom/google/android/gms/location/LocationRequest;

    move-result-object v0

    invoke-static {}, Lcom/google/android/location/copresence/f/b;->b()Lcom/google/ac/b/c/g;

    move-result-object v1

    iget-object v1, v1, Lcom/google/ac/b/c/g;->l:Lcom/google/ac/b/c/j;

    iget-object v1, v1, Lcom/google/ac/b/c/j;->d:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    invoke-virtual {v0, v8, v9}, Lcom/google/android/gms/location/LocationRequest;->c(J)Lcom/google/android/gms/location/LocationRequest;

    move-result-object v1

    iget-object v0, v2, Lcom/google/android/location/copresence/q/a;->a:Lcom/google/android/location/fused/g;

    iget-object v2, v2, Lcom/google/android/location/copresence/q/a;->c:Lcom/google/android/gms/location/j;

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/location/fused/g;->a(Lcom/google/android/gms/location/LocationRequest;Lcom/google/android/gms/location/j;ZZLjava/util/Collection;)V

    .line 77
    :cond_0
    :goto_1
    return-object v6

    .line 66
    :cond_1
    new-instance v0, Lcom/google/ac/b/c/as;

    invoke-direct {v0}, Lcom/google/ac/b/c/as;-><init>()V

    invoke-virtual {v1}, Landroid/location/Location;->getLatitude()D

    move-result-wide v6

    mul-double/2addr v6, v8

    double-to-int v2, v6

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iput-object v2, v0, Lcom/google/ac/b/c/as;->a:Ljava/lang/Integer;

    invoke-virtual {v1}, Landroid/location/Location;->getLongitude()D

    move-result-wide v6

    mul-double/2addr v6, v8

    double-to-int v2, v6

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iput-object v2, v0, Lcom/google/ac/b/c/as;->b:Ljava/lang/Integer;

    invoke-virtual {v1}, Landroid/location/Location;->getAccuracy()F

    move-result v2

    float-to-double v6, v2

    invoke-static {v6, v7}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    iput-object v2, v0, Lcom/google/ac/b/c/as;->c:Ljava/lang/Double;

    invoke-virtual {v1}, Landroid/location/Location;->hasSpeed()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-virtual {v1}, Landroid/location/Location;->getSpeed()F

    move-result v2

    float-to-double v6, v2

    invoke-static {v6, v7}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    iput-object v2, v0, Lcom/google/ac/b/c/as;->d:Ljava/lang/Double;

    :cond_2
    invoke-virtual {v1}, Landroid/location/Location;->hasAltitude()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-virtual {v1}, Landroid/location/Location;->getAltitude()D

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    iput-object v2, v0, Lcom/google/ac/b/c/as;->e:Ljava/lang/Double;

    :cond_3
    invoke-virtual {v1}, Landroid/location/Location;->getTime()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    iput-object v2, v0, Lcom/google/ac/b/c/as;->g:Ljava/lang/Long;

    move-object v6, v0

    goto/16 :goto_0

    .line 73
    :cond_4
    iget-object v0, p0, Lcom/google/android/location/copresence/l/e;->b:Lcom/google/android/location/copresence/q/a;

    iget-object v1, p0, Lcom/google/android/location/copresence/l/e;->c:Lcom/google/android/location/copresence/q/d;

    iget-object v2, v0, Lcom/google/android/location/copresence/q/a;->b:Ljava/util/Set;

    invoke-interface {v2, v1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    iget-object v1, v0, Lcom/google/android/location/copresence/q/a;->b:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->size()I

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, v0, Lcom/google/android/location/copresence/q/a;->a:Lcom/google/android/location/fused/g;

    iget-object v0, v0, Lcom/google/android/location/copresence/q/a;->c:Lcom/google/android/gms/location/j;

    invoke-virtual {v1, v0}, Lcom/google/android/location/fused/g;->a(Lcom/google/android/gms/location/j;)V

    goto :goto_1
.end method
