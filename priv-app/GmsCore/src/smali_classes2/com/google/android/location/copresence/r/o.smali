.class final Lcom/google/android/location/copresence/r/o;
.super Lcom/google/android/location/copresence/k/d;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/location/copresence/r/n;


# direct methods
.method constructor <init>(Lcom/google/android/location/copresence/r/n;)V
    .locals 0

    .prologue
    .line 52
    iput-object p1, p0, Lcom/google/android/location/copresence/r/o;->a:Lcom/google/android/location/copresence/r/n;

    invoke-direct {p0}, Lcom/google/android/location/copresence/k/d;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/location/copresence/k/f;)V
    .locals 2

    .prologue
    const/4 v1, 0x6

    .line 70
    iget-object v0, p0, Lcom/google/android/location/copresence/r/o;->a:Lcom/google/android/location/copresence/r/n;

    invoke-static {v0}, Lcom/google/android/location/copresence/r/n;->b(Lcom/google/android/location/copresence/r/n;)Lcom/google/android/location/copresence/k/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/location/copresence/k/a;->b()V

    .line 71
    iget-object v0, p0, Lcom/google/android/location/copresence/r/o;->a:Lcom/google/android/location/copresence/r/n;

    invoke-static {v0}, Lcom/google/android/location/copresence/r/n;->c(Lcom/google/android/location/copresence/r/n;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 72
    invoke-static {v1}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 73
    const-string v0, "WifiDirectBeacon2: Failed to revert."

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->e(Ljava/lang/String;)I

    .line 75
    :cond_0
    invoke-static {v1}, Lcom/google/android/location/copresence/b/a;->b(I)V

    .line 76
    iget-object v0, p0, Lcom/google/android/location/copresence/r/o;->a:Lcom/google/android/location/copresence/r/n;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/google/android/location/copresence/r/n;->a(Lcom/google/android/location/copresence/r/n;Z)Z

    .line 85
    :goto_0
    return-void

    .line 78
    :cond_1
    invoke-static {v1}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 79
    const-string v0, "WifiDirectBeacon2: State machine failed!"

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->e(Ljava/lang/String;)I

    .line 81
    :cond_2
    const/4 v0, 0x7

    invoke-static {v0}, Lcom/google/android/location/copresence/b/a;->b(I)V

    .line 82
    iget-object v0, p0, Lcom/google/android/location/copresence/r/o;->a:Lcom/google/android/location/copresence/r/n;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/google/android/location/copresence/r/n;->a(Lcom/google/android/location/copresence/r/n;Z)Z

    .line 83
    iget-object v0, p0, Lcom/google/android/location/copresence/r/o;->a:Lcom/google/android/location/copresence/r/n;

    invoke-virtual {v0}, Lcom/google/android/location/copresence/r/n;->b()V

    goto :goto_0
.end method

.method public final b(Lcom/google/android/location/copresence/k/f;)V
    .locals 2

    .prologue
    .line 56
    instance-of v0, p1, Lcom/google/android/location/copresence/r/z;

    if-eqz v0, :cond_1

    .line 57
    const/4 v0, 0x3

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 58
    const-string v0, "E2E Advertise: step 2d) Advertise on Wifi Direct started."

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->b(Ljava/lang/String;)I

    .line 64
    :cond_0
    :goto_0
    return-void

    .line 62
    :cond_1
    iget-object v0, p0, Lcom/google/android/location/copresence/r/o;->a:Lcom/google/android/location/copresence/r/n;

    invoke-static {v0}, Lcom/google/android/location/copresence/r/n;->a(Lcom/google/android/location/copresence/r/n;)Lcom/google/android/location/copresence/r/p;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/location/copresence/r/p;->c:Lcom/google/android/location/copresence/r/ad;

    if-ne p1, v0, :cond_0

    .line 63
    iget-object v0, p0, Lcom/google/android/location/copresence/r/o;->a:Lcom/google/android/location/copresence/r/n;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/google/android/location/copresence/r/n;->a(Lcom/google/android/location/copresence/r/n;Z)Z

    goto :goto_0
.end method
