.class public final Lcom/google/android/location/fused/ak;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/location/fused/ap;


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Lcom/google/android/location/fused/ap;

.field private final c:Lcom/google/android/location/fused/ap;

.field private d:Lcom/google/android/location/fused/ap;

.field private e:Z

.field private f:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/location/fused/ap;Lcom/google/android/location/fused/ap;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    iput-boolean v0, p0, Lcom/google/android/location/fused/ak;->e:Z

    .line 30
    iput-boolean v0, p0, Lcom/google/android/location/fused/ak;->f:Z

    .line 36
    iput-object p1, p0, Lcom/google/android/location/fused/ak;->a:Landroid/content/Context;

    .line 37
    iput-object p2, p0, Lcom/google/android/location/fused/ak;->b:Lcom/google/android/location/fused/ap;

    .line 38
    iput-object p3, p0, Lcom/google/android/location/fused/ak;->c:Lcom/google/android/location/fused/ap;

    .line 39
    iget-object v0, p0, Lcom/google/android/location/fused/ak;->b:Lcom/google/android/location/fused/ap;

    iput-object v0, p0, Lcom/google/android/location/fused/ak;->d:Lcom/google/android/location/fused/ap;

    .line 40
    return-void
.end method


# virtual methods
.method public final a(Z)Landroid/location/Location;
    .locals 1

    .prologue
    .line 153
    iget-object v0, p0, Lcom/google/android/location/fused/ak;->d:Lcom/google/android/location/fused/ap;

    invoke-interface {v0, p1}, Lcom/google/android/location/fused/ap;->a(Z)Landroid/location/Location;

    move-result-object v0

    return-object v0
.end method

.method public final a()V
    .locals 7

    .prologue
    const/4 v3, 0x0

    const/4 v1, 0x1

    .line 44
    iget-object v0, p0, Lcom/google/android/location/fused/ak;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/gms/common/a/d;->a(Landroid/content/Context;)V

    .line 47
    sget-object v0, Lcom/google/android/location/x;->D:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v2

    .line 48
    if-nez v2, :cond_7

    sget-object v0, Lcom/google/android/location/x;->C:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_7

    move v0, v1

    .line 52
    :goto_0
    iget-object v2, p0, Lcom/google/android/location/fused/ak;->b:Lcom/google/android/location/fused/ap;

    iput-object v2, p0, Lcom/google/android/location/fused/ak;->d:Lcom/google/android/location/fused/ap;

    .line 60
    packed-switch v0, :pswitch_data_0

    move v0, v3

    move v2, v3

    move v4, v3

    move v5, v1

    .line 86
    :goto_1
    const/16 v6, 0x15

    invoke-static {v6}, Lcom/google/android/gms/common/util/ao;->a(I)Z

    move-result v6

    if-nez v6, :cond_0

    if-eqz v0, :cond_3

    const/16 v0, 0x13

    invoke-static {v0}, Lcom/google/android/gms/common/util/ao;->a(I)Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_0
    move v0, v1

    .line 88
    :goto_2
    if-eqz v0, :cond_4

    if-eqz v2, :cond_4

    :goto_3
    invoke-static {v1}, Lcom/google/android/location/fused/service/a;->a(Z)V

    .line 89
    iget-boolean v0, p0, Lcom/google/android/location/fused/ak;->e:Z

    if-eq v5, v0, :cond_1

    .line 90
    iput-boolean v5, p0, Lcom/google/android/location/fused/ak;->e:Z

    .line 91
    if-eqz v5, :cond_5

    .line 92
    iget-object v0, p0, Lcom/google/android/location/fused/ak;->b:Lcom/google/android/location/fused/ap;

    invoke-interface {v0}, Lcom/google/android/location/fused/ap;->a()V

    .line 97
    :cond_1
    :goto_4
    iget-boolean v0, p0, Lcom/google/android/location/fused/ak;->f:Z

    if-eq v4, v0, :cond_2

    .line 98
    iput-boolean v4, p0, Lcom/google/android/location/fused/ak;->f:Z

    .line 99
    if-eqz v4, :cond_6

    .line 100
    iget-object v0, p0, Lcom/google/android/location/fused/ak;->c:Lcom/google/android/location/fused/ap;

    invoke-interface {v0}, Lcom/google/android/location/fused/ap;->a()V

    .line 105
    :cond_2
    :goto_5
    return-void

    :pswitch_0
    move v0, v3

    move v2, v3

    move v4, v3

    move v5, v1

    .line 64
    goto :goto_1

    .line 70
    :pswitch_1
    iget-object v0, p0, Lcom/google/android/location/fused/ak;->c:Lcom/google/android/location/fused/ap;

    iput-object v0, p0, Lcom/google/android/location/fused/ak;->d:Lcom/google/android/location/fused/ap;

    move v0, v1

    move v2, v1

    move v4, v1

    move v5, v3

    .line 71
    goto :goto_1

    :pswitch_2
    move v0, v1

    move v2, v1

    move v4, v1

    move v5, v1

    .line 77
    goto :goto_1

    :pswitch_3
    move v0, v3

    move v2, v1

    move v4, v3

    move v5, v1

    .line 83
    goto :goto_1

    :cond_3
    move v0, v3

    .line 86
    goto :goto_2

    :cond_4
    move v1, v3

    .line 88
    goto :goto_3

    .line 94
    :cond_5
    iget-object v0, p0, Lcom/google/android/location/fused/ak;->b:Lcom/google/android/location/fused/ap;

    invoke-interface {v0}, Lcom/google/android/location/fused/ap;->b()V

    goto :goto_4

    .line 102
    :cond_6
    iget-object v0, p0, Lcom/google/android/location/fused/ak;->c:Lcom/google/android/location/fused/ap;

    invoke-interface {v0}, Lcom/google/android/location/fused/ap;->b()V

    goto :goto_5

    :cond_7
    move v0, v2

    goto :goto_0

    .line 60
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_3
        :pswitch_3
    .end packed-switch
.end method

.method public final a(Landroid/location/Location;I)V
    .locals 1

    .prologue
    .line 143
    iget-boolean v0, p0, Lcom/google/android/location/fused/ak;->f:Z

    if-eqz v0, :cond_0

    .line 144
    iget-object v0, p0, Lcom/google/android/location/fused/ak;->c:Lcom/google/android/location/fused/ap;

    invoke-interface {v0, p1, p2}, Lcom/google/android/location/fused/ap;->a(Landroid/location/Location;I)V

    .line 146
    :cond_0
    iget-boolean v0, p0, Lcom/google/android/location/fused/ak;->e:Z

    if-eqz v0, :cond_1

    .line 147
    iget-object v0, p0, Lcom/google/android/location/fused/ak;->b:Lcom/google/android/location/fused/ap;

    invoke-interface {v0, p1, p2}, Lcom/google/android/location/fused/ap;->a(Landroid/location/Location;I)V

    .line 149
    :cond_1
    return-void
.end method

.method public final a(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 158
    iget-object v0, p0, Lcom/google/android/location/fused/ak;->b:Lcom/google/android/location/fused/ap;

    invoke-interface {v0, p1, p2, p3}, Lcom/google/android/location/fused/ap;->a(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V

    .line 159
    invoke-virtual {p2}, Ljava/io/PrintWriter;->println()V

    .line 160
    iget-object v0, p0, Lcom/google/android/location/fused/ak;->c:Lcom/google/android/location/fused/ap;

    invoke-interface {v0, p1, p2, p3}, Lcom/google/android/location/fused/ap;->a(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V

    .line 161
    return-void
.end method

.method public final a(Ljava/util/Collection;Z)V
    .locals 1

    .prologue
    .line 123
    iget-object v0, p0, Lcom/google/android/location/fused/ak;->b:Lcom/google/android/location/fused/ap;

    invoke-interface {v0, p1, p2}, Lcom/google/android/location/fused/ap;->a(Ljava/util/Collection;Z)V

    .line 124
    iget-object v0, p0, Lcom/google/android/location/fused/ak;->c:Lcom/google/android/location/fused/ap;

    invoke-interface {v0, p1, p2}, Lcom/google/android/location/fused/ap;->a(Ljava/util/Collection;Z)V

    .line 125
    return-void
.end method

.method public final b()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 109
    iget-boolean v0, p0, Lcom/google/android/location/fused/ak;->e:Z

    if-eqz v0, :cond_0

    .line 110
    iput-boolean v1, p0, Lcom/google/android/location/fused/ak;->e:Z

    .line 111
    iget-object v0, p0, Lcom/google/android/location/fused/ak;->b:Lcom/google/android/location/fused/ap;

    invoke-interface {v0}, Lcom/google/android/location/fused/ap;->b()V

    .line 114
    :cond_0
    iget-boolean v0, p0, Lcom/google/android/location/fused/ak;->f:Z

    if-eqz v0, :cond_1

    .line 115
    iput-boolean v1, p0, Lcom/google/android/location/fused/ak;->f:Z

    .line 116
    iget-object v0, p0, Lcom/google/android/location/fused/ak;->c:Lcom/google/android/location/fused/ap;

    invoke-interface {v0}, Lcom/google/android/location/fused/ap;->b()V

    .line 118
    :cond_1
    return-void
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 130
    iget-object v0, p0, Lcom/google/android/location/fused/ak;->b:Lcom/google/android/location/fused/ap;

    invoke-interface {v0}, Lcom/google/android/location/fused/ap;->c()V

    .line 131
    iget-object v0, p0, Lcom/google/android/location/fused/ak;->c:Lcom/google/android/location/fused/ap;

    invoke-interface {v0}, Lcom/google/android/location/fused/ap;->c()V

    .line 132
    return-void
.end method

.method public final d()V
    .locals 1

    .prologue
    .line 137
    iget-object v0, p0, Lcom/google/android/location/fused/ak;->b:Lcom/google/android/location/fused/ap;

    invoke-interface {v0}, Lcom/google/android/location/fused/ap;->d()V

    .line 138
    iget-object v0, p0, Lcom/google/android/location/fused/ak;->c:Lcom/google/android/location/fused/ap;

    invoke-interface {v0}, Lcom/google/android/location/fused/ap;->d()V

    .line 139
    return-void
.end method

.method public final e()Lcom/google/android/location/n/k;
    .locals 1

    .prologue
    .line 165
    iget-object v0, p0, Lcom/google/android/location/fused/ak;->d:Lcom/google/android/location/fused/ap;

    invoke-interface {v0}, Lcom/google/android/location/fused/ap;->e()Lcom/google/android/location/n/k;

    move-result-object v0

    return-object v0
.end method
