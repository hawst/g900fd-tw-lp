.class public final Lcom/google/android/location/copresence/e/g;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final a:Ljava/lang/String;

.field final b:Landroid/app/PendingIntent;

.field final synthetic c:Lcom/google/android/location/copresence/e/d;

.field private final d:Ljava/lang/String;

.field private final e:Lcom/google/android/gms/location/copresence/internal/o;


# direct methods
.method public constructor <init>(Lcom/google/android/location/copresence/e/d;Ljava/lang/String;Ljava/lang/String;Landroid/app/PendingIntent;)V
    .locals 1

    .prologue
    .line 76
    iput-object p1, p0, Lcom/google/android/location/copresence/e/g;->c:Lcom/google/android/location/copresence/e/d;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 77
    invoke-static {p2}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/location/copresence/e/g;->a:Ljava/lang/String;

    .line 78
    invoke-static {p3}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/location/copresence/e/g;->d:Ljava/lang/String;

    .line 79
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/location/copresence/e/g;->e:Lcom/google/android/gms/location/copresence/internal/o;

    .line 80
    invoke-static {p4}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/PendingIntent;

    iput-object v0, p0, Lcom/google/android/location/copresence/e/g;->b:Landroid/app/PendingIntent;

    .line 81
    return-void
.end method

.method public constructor <init>(Lcom/google/android/location/copresence/e/d;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/location/copresence/internal/o;)V
    .locals 1

    .prologue
    .line 69
    iput-object p1, p0, Lcom/google/android/location/copresence/e/g;->c:Lcom/google/android/location/copresence/e/d;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 70
    invoke-static {p2}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/location/copresence/e/g;->a:Ljava/lang/String;

    .line 71
    invoke-static {p3}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/location/copresence/e/g;->d:Ljava/lang/String;

    .line 72
    invoke-static {p4}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/location/copresence/internal/o;

    iput-object v0, p0, Lcom/google/android/location/copresence/e/g;->e:Lcom/google/android/gms/location/copresence/internal/o;

    .line 73
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/location/copresence/e/g;->b:Landroid/app/PendingIntent;

    .line 74
    return-void
.end method


# virtual methods
.method final a(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 97
    if-eqz p1, :cond_1

    iget-object v1, p0, Lcom/google/android/location/copresence/e/g;->a:Ljava/lang/String;

    invoke-static {v1, p1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 101
    :cond_0
    :goto_0
    return v0

    :cond_1
    if-eqz p2, :cond_2

    iget-object v1, p0, Lcom/google/android/location/copresence/e/g;->d:Ljava/lang/String;

    invoke-static {v1, p2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final a(Ljava/util/ArrayList;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 131
    iget-object v2, p0, Lcom/google/android/location/copresence/e/g;->e:Lcom/google/android/gms/location/copresence/internal/o;

    if-eqz v2, :cond_1

    .line 133
    :try_start_0
    iget-object v2, p0, Lcom/google/android/location/copresence/e/g;->e:Lcom/google/android/gms/location/copresence/internal/o;

    invoke-interface {v2, p1}, Lcom/google/android/gms/location/copresence/internal/o;->a(Ljava/util/List;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 155
    :goto_0
    return v0

    .line 137
    :catch_0
    move-exception v0

    .line 140
    const-string v2, "MessageFilterCache: Client code has thrown an exception in onMessageReceived()"

    invoke-static {v2, v0}, Lcom/google/android/location/copresence/ag;->b(Ljava/lang/String;Ljava/lang/Throwable;)I

    :cond_0
    :goto_1
    move v0, v1

    .line 155
    goto :goto_0

    .line 143
    :cond_1
    iget-object v2, p0, Lcom/google/android/location/copresence/e/g;->c:Lcom/google/android/location/copresence/e/d;

    iget-object v2, v2, Lcom/google/android/location/copresence/e/d;->a:Landroid/content/Context;

    if-eqz v2, :cond_0

    .line 145
    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    .line 146
    const-string v3, "com.google.android.gms.location.copresence.SUBSCRIBED_MESSAGES"

    invoke-virtual {v2, v3, p1}, Landroid/content/Intent;->putParcelableArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    .line 147
    const-string v3, "com.google.android.gms.location.copresence.ACCOUNT_NAME"

    iget-object v4, p0, Lcom/google/android/location/copresence/e/g;->d:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 149
    :try_start_1
    iget-object v3, p0, Lcom/google/android/location/copresence/e/g;->b:Landroid/app/PendingIntent;

    iget-object v4, p0, Lcom/google/android/location/copresence/e/g;->c:Lcom/google/android/location/copresence/e/d;

    iget-object v4, v4, Lcom/google/android/location/copresence/e/d;->a:Landroid/content/Context;

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5, v2}, Landroid/app/PendingIntent;->send(Landroid/content/Context;ILandroid/content/Intent;)V
    :try_end_1
    .catch Landroid/app/PendingIntent$CanceledException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    :catch_1
    move-exception v0

    goto :goto_1

    .line 142
    :catch_2
    move-exception v0

    goto :goto_1
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 169
    if-ne p0, p1, :cond_1

    .line 179
    :cond_0
    :goto_0
    return v0

    .line 172
    :cond_1
    if-nez p1, :cond_2

    move v0, v1

    .line 173
    goto :goto_0

    .line 175
    :cond_2
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    move v0, v1

    .line 176
    goto :goto_0

    .line 178
    :cond_3
    check-cast p1, Lcom/google/android/location/copresence/e/g;

    .line 179
    iget-object v2, p0, Lcom/google/android/location/copresence/e/g;->a:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/android/location/copresence/e/g;->a:Ljava/lang/String;

    invoke-static {v2, v3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/google/android/location/copresence/e/g;->d:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/android/location/copresence/e/g;->d:Ljava/lang/String;

    invoke-static {v2, v3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/google/android/location/copresence/e/g;->e:Lcom/google/android/gms/location/copresence/internal/o;

    iget-object v3, p1, Lcom/google/android/location/copresence/e/g;->e:Lcom/google/android/gms/location/copresence/internal/o;

    if-ne v2, v3, :cond_5

    move v2, v0

    :goto_1
    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/google/android/location/copresence/e/g;->b:Landroid/app/PendingIntent;

    iget-object v3, p1, Lcom/google/android/location/copresence/e/g;->b:Landroid/app/PendingIntent;

    invoke-static {v2, v3}, Lcom/google/android/gms/common/internal/bu;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_4
    move v0, v1

    goto :goto_0

    :cond_5
    if-eqz v2, :cond_7

    if-eqz v3, :cond_7

    invoke-interface {v2}, Lcom/google/android/gms/location/copresence/internal/o;->asBinder()Landroid/os/IBinder;

    move-result-object v2

    invoke-interface {v3}, Lcom/google/android/gms/location/copresence/internal/o;->asBinder()Landroid/os/IBinder;

    move-result-object v3

    if-ne v2, v3, :cond_6

    move v2, v0

    goto :goto_1

    :cond_6
    move v2, v1

    goto :goto_1

    :cond_7
    move v2, v1

    goto :goto_1
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    .line 160
    const/4 v0, 0x4

    new-array v1, v0, [Ljava/lang/Object;

    const/4 v0, 0x0

    iget-object v2, p0, Lcom/google/android/location/copresence/e/g;->a:Ljava/lang/String;

    aput-object v2, v1, v0

    const/4 v0, 0x1

    iget-object v2, p0, Lcom/google/android/location/copresence/e/g;->d:Ljava/lang/String;

    aput-object v2, v1, v0

    const/4 v2, 0x2

    iget-object v0, p0, Lcom/google/android/location/copresence/e/g;->e:Lcom/google/android/gms/location/copresence/internal/o;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    aput-object v0, v1, v2

    const/4 v0, 0x3

    iget-object v2, p0, Lcom/google/android/location/copresence/e/g;->b:Landroid/app/PendingIntent;

    aput-object v2, v1, v0

    invoke-static {v1}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/location/copresence/e/g;->e:Lcom/google/android/gms/location/copresence/internal/o;

    invoke-interface {v0}, Lcom/google/android/gms/location/copresence/internal/o;->asBinder()Landroid/os/IBinder;

    move-result-object v0

    goto :goto_0
.end method

.method public final toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 196
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "("

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/location/copresence/e/g;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/location/copresence/e/g;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v0, p0, Lcom/google/android/location/copresence/e/g;->e:Lcom/google/android/gms/location/copresence/internal/o;

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "l:"

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/location/copresence/e/g;->e:Lcom/google/android/gms/location/copresence/internal/o;

    invoke-interface {v2}, Lcom/google/android/gms/location/copresence/internal/o;->asBinder()Landroid/os/IBinder;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "p:"

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/location/copresence/e/g;->b:Landroid/app/PendingIntent;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
