.class public final Lcom/google/android/location/places/ui/bd;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method public static a(Landroid/app/Activity;Lcom/google/android/gms/location/places/f;IILcom/google/android/location/places/ui/s;Lcom/google/android/gms/maps/model/LatLngBounds;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 36
    sget-object v0, Lcom/google/android/location/x;->f:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_0

    .line 86
    :goto_0
    return-void

    .line 43
    :cond_0
    const/4 v0, 0x4

    iget-object v1, p4, Lcom/google/android/location/places/ui/s;->a:Ljava/lang/String;

    iget v2, p4, Lcom/google/android/location/places/ui/s;->c:I

    invoke-static {v0, v1, v2}, Lcom/google/android/location/places/bf;->a(ILjava/lang/String;I)Lcom/google/k/f/c/n;

    move-result-object v0

    .line 69
    new-instance v1, Lcom/google/k/f/c/l;

    invoke-direct {v1}, Lcom/google/k/f/c/l;-><init>()V

    .line 70
    iput-object v1, v0, Lcom/google/k/f/c/n;->j:Lcom/google/k/f/c/l;

    .line 71
    if-eqz p1, :cond_1

    .line 72
    invoke-virtual {p1}, Lcom/google/android/gms/location/places/f;->a()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/google/k/f/c/l;->a:Ljava/lang/String;

    .line 74
    :cond_1
    iget-object v2, p4, Lcom/google/android/location/places/ui/s;->b:Ljava/lang/String;

    iput-object v2, v1, Lcom/google/k/f/c/l;->b:Ljava/lang/String;

    .line 75
    iget-object v2, p4, Lcom/google/android/location/places/ui/s;->d:Lcom/google/android/gms/location/places/PlaceFilter;

    const-string v3, ""

    invoke-static {v2, v3}, Lcom/google/android/location/places/bf;->a(Lcom/google/android/gms/location/places/PlaceFilter;Ljava/lang/String;)Lcom/google/k/f/c/k;

    move-result-object v2

    iput-object v2, v1, Lcom/google/k/f/c/l;->c:Lcom/google/k/f/c/k;

    .line 76
    invoke-static {p5}, Lcom/google/android/location/places/bf;->a(Lcom/google/android/gms/maps/model/LatLngBounds;)Lcom/google/k/f/c/h;

    move-result-object v2

    iput-object v2, v1, Lcom/google/k/f/c/l;->d:Lcom/google/k/f/c/h;

    .line 77
    iget v2, p4, Lcom/google/android/location/places/ui/s;->e:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iput-object v2, v1, Lcom/google/k/f/c/l;->e:Ljava/lang/Integer;

    .line 78
    const/4 v2, 0x3

    if-ne p2, v2, :cond_2

    .line 79
    iput-object p6, v1, Lcom/google/k/f/c/l;->f:Ljava/lang/String;

    .line 81
    :cond_2
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iput-object v2, v1, Lcom/google/k/f/c/l;->g:Ljava/lang/Integer;

    .line 82
    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iput-object v2, v1, Lcom/google/k/f/c/l;->h:Ljava/lang/Integer;

    .line 85
    invoke-static {p0, v0}, Lcom/google/android/location/places/PlaylogService;->a(Landroid/content/Context;Lcom/google/k/f/c/n;)V

    goto :goto_0
.end method
