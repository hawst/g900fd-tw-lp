.class final Lcom/google/android/location/copresence/r/t;
.super Lcom/google/android/location/copresence/r/ad;
.source "SourceFile"


# instance fields
.field final a:Landroid/net/wifi/p2p/WifiP2pManager$GroupInfoListener;

.field final synthetic b:Lcom/google/android/location/copresence/r/p;


# direct methods
.method varargs constructor <init>(Lcom/google/android/location/copresence/r/p;Ljava/lang/String;[Lcom/google/android/location/copresence/k/f;)V
    .locals 1

    .prologue
    .line 298
    iput-object p1, p0, Lcom/google/android/location/copresence/r/t;->b:Lcom/google/android/location/copresence/r/p;

    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/location/copresence/r/ad;-><init>(Lcom/google/android/location/copresence/r/p;Ljava/lang/String;[Lcom/google/android/location/copresence/k/f;)V

    .line 301
    new-instance v0, Lcom/google/android/location/copresence/r/u;

    invoke-direct {v0, p0}, Lcom/google/android/location/copresence/r/u;-><init>(Lcom/google/android/location/copresence/r/t;)V

    iput-object v0, p0, Lcom/google/android/location/copresence/r/t;->a:Landroid/net/wifi/p2p/WifiP2pManager$GroupInfoListener;

    return-void
.end method


# virtual methods
.method public final a()Z
    .locals 2

    .prologue
    .line 340
    iget-object v0, p0, Lcom/google/android/location/copresence/r/t;->b:Lcom/google/android/location/copresence/r/p;

    invoke-static {v0}, Lcom/google/android/location/copresence/r/p;->b(Lcom/google/android/location/copresence/r/p;)Lcom/google/android/location/copresence/r/ah;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/location/copresence/r/t;->a:Landroid/net/wifi/p2p/WifiP2pManager$GroupInfoListener;

    invoke-virtual {v0, v1}, Lcom/google/android/location/copresence/r/ah;->a(Landroid/net/wifi/p2p/WifiP2pManager$GroupInfoListener;)V

    .line 341
    const/4 v0, 0x1

    return v0
.end method

.method public final b()Z
    .locals 4

    .prologue
    .line 332
    iget-object v0, p0, Lcom/google/android/location/copresence/r/t;->b:Lcom/google/android/location/copresence/r/p;

    invoke-static {v0}, Lcom/google/android/location/copresence/r/p;->b(Lcom/google/android/location/copresence/r/p;)Lcom/google/android/location/copresence/r/ah;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/location/copresence/r/ah;->d:Landroid/net/wifi/p2p/WifiP2pGroup;

    .line 333
    iget-object v1, p0, Lcom/google/android/location/copresence/r/t;->b:Lcom/google/android/location/copresence/r/p;

    invoke-static {v1}, Lcom/google/android/location/copresence/r/p;->e(Lcom/google/android/location/copresence/r/p;)Landroid/content/SharedPreferences;

    move-result-object v1

    const-string v2, "groupInterface"

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 335
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/net/wifi/p2p/WifiP2pGroup;->getOwner()Landroid/net/wifi/p2p/WifiP2pDevice;

    move-result-object v0

    if-nez v0, :cond_1

    :cond_0
    if-nez v1, :cond_1

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final d()J
    .locals 2

    .prologue
    .line 346
    const-wide/16 v0, 0x2710

    return-wide v0
.end method
