.class public final Lcom/google/android/location/reporting/service/b;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/content/ServiceConnection;


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Landroid/content/Intent;

.field private final c:Ljava/lang/Object;

.field private final d:Landroid/os/ConditionVariable;

.field private e:Landroid/os/IBinder;

.field private f:Ljava/lang/Exception;

.field private g:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 1

    .prologue
    .line 59
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 60
    iput-object p1, p0, Lcom/google/android/location/reporting/service/b;->a:Landroid/content/Context;

    .line 61
    iput-object p2, p0, Lcom/google/android/location/reporting/service/b;->b:Landroid/content/Intent;

    .line 62
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/location/reporting/service/b;->c:Ljava/lang/Object;

    .line 63
    new-instance v0, Landroid/os/ConditionVariable;

    invoke-direct {v0}, Landroid/os/ConditionVariable;-><init>()V

    iput-object v0, p0, Lcom/google/android/location/reporting/service/b;->d:Landroid/os/ConditionVariable;

    .line 64
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/location/reporting/service/b;->g:Z

    .line 65
    return-void
.end method


# virtual methods
.method public final a(JLjava/util/concurrent/TimeUnit;)Landroid/os/IBinder;
    .locals 5

    .prologue
    const/4 v3, 0x1

    .line 77
    invoke-static {}, Lcom/google/android/location/reporting/b/a;->a()V

    .line 79
    invoke-static {}, Lcom/google/android/gms/common/stats/b;->a()Lcom/google/android/gms/common/stats/b;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/location/reporting/service/b;->a:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/location/reporting/service/b;->b:Landroid/content/Intent;

    invoke-virtual {v0, v1, v2, p0, v3}, Lcom/google/android/gms/common/stats/b;->a(Landroid/content/Context;Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    move-result v0

    .line 81
    if-eqz v0, :cond_0

    .line 82
    sget-object v0, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, p1, p2, p3}, Ljava/util/concurrent/TimeUnit;->convert(JLjava/util/concurrent/TimeUnit;)J

    move-result-wide v0

    .line 83
    iget-object v2, p0, Lcom/google/android/location/reporting/service/b;->d:Landroid/os/ConditionVariable;

    invoke-virtual {v2, v0, v1}, Landroid/os/ConditionVariable;->block(J)Z

    .line 88
    iget-object v1, p0, Lcom/google/android/location/reporting/service/b;->c:Ljava/lang/Object;

    monitor-enter v1

    .line 89
    :try_start_0
    iget-object v0, p0, Lcom/google/android/location/reporting/service/b;->f:Ljava/lang/Exception;

    if-eqz v0, :cond_1

    .line 90
    new-instance v0, Ljava/util/concurrent/ExecutionException;

    iget-object v2, p0, Lcom/google/android/location/reporting/service/b;->f:Ljava/lang/Exception;

    invoke-direct {v0, v2}, Ljava/util/concurrent/ExecutionException;-><init>(Ljava/lang/Throwable;)V

    throw v0

    .line 97
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 85
    :cond_0
    new-instance v0, Ljava/util/concurrent/ExecutionException;

    new-instance v1, Ljava/io/IOException;

    const-string v2, "Error binding to service"

    invoke-direct {v1, v2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    invoke-direct {v0, v1}, Ljava/util/concurrent/ExecutionException;-><init>(Ljava/lang/Throwable;)V

    throw v0

    .line 91
    :cond_1
    :try_start_1
    iget-object v0, p0, Lcom/google/android/location/reporting/service/b;->e:Landroid/os/IBinder;

    if-eqz v0, :cond_2

    .line 92
    iget-object v0, p0, Lcom/google/android/location/reporting/service/b;->e:Landroid/os/IBinder;

    monitor-exit v1

    return-object v0

    .line 95
    :cond_2
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/location/reporting/service/b;->g:Z

    .line 96
    new-instance v0, Ljava/util/concurrent/TimeoutException;

    invoke-direct {v0}, Ljava/util/concurrent/TimeoutException;-><init>()V

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0
.end method

.method public final a()V
    .locals 3

    .prologue
    .line 104
    iget-object v0, p0, Lcom/google/android/location/reporting/service/b;->e:Landroid/os/IBinder;

    if-eqz v0, :cond_0

    .line 106
    :try_start_0
    invoke-static {}, Lcom/google/android/gms/common/stats/b;->a()Lcom/google/android/gms/common/stats/b;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/location/reporting/service/b;->a:Landroid/content/Context;

    invoke-virtual {v0, v1, p0}, Lcom/google/android/gms/common/stats/b;->a(Landroid/content/Context;Landroid/content/ServiceConnection;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 111
    :cond_0
    :goto_0
    return-void

    .line 107
    :catch_0
    move-exception v0

    .line 108
    const-string v1, "GCoreUlr"

    const-string v2, "Best-effort unbind failed"

    invoke-static {v1, v2, v0}, Lcom/google/android/location/reporting/b/d;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public final onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 2

    .prologue
    .line 115
    iget-object v1, p0, Lcom/google/android/location/reporting/service/b;->c:Ljava/lang/Object;

    monitor-enter v1

    .line 116
    :try_start_0
    iput-object p2, p0, Lcom/google/android/location/reporting/service/b;->e:Landroid/os/IBinder;

    .line 117
    iget-object v0, p0, Lcom/google/android/location/reporting/service/b;->d:Landroid/os/ConditionVariable;

    invoke-virtual {v0}, Landroid/os/ConditionVariable;->open()V

    .line 118
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 4

    .prologue
    .line 123
    iget-object v1, p0, Lcom/google/android/location/reporting/service/b;->c:Ljava/lang/Object;

    monitor-enter v1

    .line 125
    :try_start_0
    new-instance v0, Ljava/io/IOException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Disconnected from "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/location/reporting/service/b;->f:Ljava/lang/Exception;

    .line 126
    iget-object v0, p0, Lcom/google/android/location/reporting/service/b;->d:Landroid/os/ConditionVariable;

    invoke-virtual {v0}, Landroid/os/ConditionVariable;->open()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 128
    :try_start_1
    iget-boolean v0, p0, Lcom/google/android/location/reporting/service/b;->g:Z

    if-eqz v0, :cond_0

    .line 131
    invoke-virtual {p0}, Lcom/google/android/location/reporting/service/b;->a()V

    .line 134
    :cond_0
    monitor-exit v1

    return-void

    .line 128
    :catchall_0
    move-exception v0

    iget-boolean v2, p0, Lcom/google/android/location/reporting/service/b;->g:Z

    if-eqz v2, :cond_1

    .line 131
    invoke-virtual {p0}, Lcom/google/android/location/reporting/service/b;->a()V

    :cond_1
    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 134
    :catchall_1
    move-exception v0

    monitor-exit v1

    throw v0
.end method
