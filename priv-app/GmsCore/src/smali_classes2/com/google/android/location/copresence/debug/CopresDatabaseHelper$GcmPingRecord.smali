.class public Lcom/google/android/location/copresence/debug/CopresDatabaseHelper$GcmPingRecord;
.super Lcom/google/android/location/copresence/debug/a;
.source "SourceFile"


# instance fields
.field public accountName:Ljava/lang/String;
    .annotation runtime Lcom/google/android/location/copresence/debug/b;
        a = true
        c = "(null)"
    .end annotation
.end field

.field public debugInfoTokenIds:Ljava/lang/String;
    .annotation runtime Lcom/google/android/location/copresence/debug/b;
        a = false
    .end annotation
.end field

.field public numDirectives:Ljava/lang/Integer;
    .annotation runtime Lcom/google/android/location/copresence/debug/b;
        a = true
        c = "0"
    .end annotation
.end field

.field public numMessages:Ljava/lang/Integer;
    .annotation runtime Lcom/google/android/location/copresence/debug/b;
        a = true
        c = "0"
    .end annotation
.end field

.field public pushMessageProtoTextFormat:Ljava/lang/String;
    .annotation runtime Lcom/google/android/location/copresence/debug/b;
        a = true
        c = ""
    .end annotation
.end field

.field public timestamp:Ljava/lang/Long;
    .annotation runtime Lcom/google/android/location/copresence/debug/b;
        a = true
        d = 0x2
    .end annotation
.end field

.field public type:Ljava/lang/Integer;
    .annotation runtime Lcom/google/android/location/copresence/debug/b;
        a = true
        c = "0"
    .end annotation
.end field


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 225
    invoke-direct {p0}, Lcom/google/android/location/copresence/debug/a;-><init>()V

    .line 226
    return-void
.end method

.method public constructor <init>(Lcom/google/android/location/copresence/a/a;Lcom/google/ac/b/c/co;)V
    .locals 2

    .prologue
    .line 229
    invoke-direct {p0}, Lcom/google/android/location/copresence/debug/CopresDatabaseHelper$GcmPingRecord;-><init>()V

    .line 230
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/copresence/debug/CopresDatabaseHelper$GcmPingRecord;->timestamp:Ljava/lang/Long;

    .line 231
    iget-object v0, p1, Lcom/google/android/location/copresence/a/a;->a:Landroid/accounts/Account;

    iget-object v0, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/location/copresence/debug/CopresDatabaseHelper$GcmPingRecord;->accountName:Ljava/lang/String;

    .line 232
    invoke-virtual {p2}, Lcom/google/ac/b/c/co;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/copresence/debug/CopresDatabaseHelper$GcmPingRecord;->pushMessageProtoTextFormat:Ljava/lang/String;

    .line 233
    iget-object v0, p2, Lcom/google/ac/b/c/co;->a:Ljava/lang/Integer;

    iput-object v0, p0, Lcom/google/android/location/copresence/debug/CopresDatabaseHelper$GcmPingRecord;->type:Ljava/lang/Integer;

    .line 234
    iget-object v0, p2, Lcom/google/ac/b/c/co;->c:Lcom/google/ac/b/c/aa;

    if-eqz v0, :cond_0

    iget-object v0, p2, Lcom/google/ac/b/c/co;->c:Lcom/google/ac/b/c/aa;

    iget-object v0, v0, Lcom/google/ac/b/c/aa;->c:[Ljava/lang/String;

    array-length v0, v0

    if-lez v0, :cond_0

    .line 235
    const-string v0, ","

    iget-object v1, p2, Lcom/google/ac/b/c/co;->c:Lcom/google/ac/b/c/aa;

    iget-object v1, v1, Lcom/google/ac/b/c/aa;->c:[Ljava/lang/String;

    invoke-static {v0, v1}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/copresence/debug/CopresDatabaseHelper$GcmPingRecord;->debugInfoTokenIds:Ljava/lang/String;

    .line 237
    :cond_0
    iget-object v0, p2, Lcom/google/ac/b/c/co;->i:Lcom/google/ac/b/c/cp;

    .line 238
    if-eqz v0, :cond_2

    .line 239
    iget-object v1, v0, Lcom/google/ac/b/c/cp;->a:[Lcom/google/ac/b/c/ah;

    if-eqz v1, :cond_1

    .line 240
    iget-object v1, v0, Lcom/google/ac/b/c/cp;->a:[Lcom/google/ac/b/c/ah;

    array-length v1, v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/location/copresence/debug/CopresDatabaseHelper$GcmPingRecord;->numDirectives:Ljava/lang/Integer;

    .line 242
    :cond_1
    iget-object v1, v0, Lcom/google/ac/b/c/cp;->b:[Lcom/google/ac/b/c/bm;

    if-eqz v1, :cond_2

    .line 243
    iget-object v0, v0, Lcom/google/ac/b/c/cp;->b:[Lcom/google/ac/b/c/bm;

    array-length v0, v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/copresence/debug/CopresDatabaseHelper$GcmPingRecord;->numMessages:Ljava/lang/Integer;

    .line 246
    :cond_2
    return-void
.end method
