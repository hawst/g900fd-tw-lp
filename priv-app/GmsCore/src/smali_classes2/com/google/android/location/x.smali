.class public final Lcom/google/android/location/x;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static final A:Lcom/google/android/gms/common/a/d;

.field public static final B:Lcom/google/android/gms/common/a/d;

.field public static final C:Lcom/google/android/gms/common/a/d;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final D:Lcom/google/android/gms/common/a/d;

.field public static final E:Lcom/google/android/gms/common/a/d;

.field public static final F:Lcom/google/android/gms/common/a/d;

.field public static final G:Lcom/google/android/gms/common/a/d;

.field public static final H:Lcom/google/android/gms/common/a/d;

.field public static final I:J

.field public static final J:Lcom/google/android/gms/common/a/d;

.field public static final K:Lcom/google/android/gms/common/a/d;

.field public static final L:Lcom/google/android/gms/common/a/d;

.field public static final M:Lcom/google/android/gms/common/a/d;

.field public static final N:Lcom/google/android/gms/common/a/d;

.field public static final O:Lcom/google/android/gms/common/a/d;

.field public static final P:Lcom/google/android/gms/common/a/d;

.field public static final Q:Lcom/google/android/gms/common/a/d;

.field public static final R:Lcom/google/android/gms/common/a/d;

.field public static final S:Lcom/google/android/gms/common/a/d;

.field public static final T:Lcom/google/android/gms/common/a/d;

.field public static final U:Lcom/google/android/gms/common/a/d;

.field public static V:Lcom/google/android/gms/common/a/d;

.field public static final W:Lcom/google/android/gms/common/a/d;

.field public static final X:Lcom/google/android/gms/common/a/d;

.field public static final Y:Lcom/google/android/gms/common/a/d;

.field public static final Z:Lcom/google/android/gms/common/a/d;

.field public static final a:Lcom/google/android/gms/common/a/d;

.field public static final aa:Lcom/google/android/gms/common/a/d;

.field private static final ab:I

.field private static final ac:Ljava/lang/String;

.field public static final b:Lcom/google/android/gms/common/a/d;

.field public static final c:Lcom/google/android/gms/common/a/d;

.field public static final d:Lcom/google/android/gms/common/a/d;

.field public static final e:Lcom/google/android/gms/common/a/d;

.field public static final f:Lcom/google/android/gms/common/a/d;

.field public static final g:Lcom/google/android/gms/common/a/d;

.field public static final h:Lcom/google/android/gms/common/a/d;

.field public static final i:Lcom/google/android/gms/common/a/d;

.field public static final j:Lcom/google/android/gms/common/a/d;

.field public static final k:Lcom/google/android/gms/common/a/d;

.field public static final l:Lcom/google/android/gms/common/a/d;

.field public static final m:Lcom/google/android/gms/common/a/d;

.field public static final n:Lcom/google/android/gms/common/a/d;

.field public static final o:Lcom/google/android/gms/common/a/d;

.field public static final p:Lcom/google/android/gms/common/a/d;

.field public static final q:Lcom/google/android/gms/common/a/d;

.field public static final r:Lcom/google/android/gms/common/a/d;

.field public static final s:Lcom/google/android/gms/common/a/d;

.field public static final t:Lcom/google/android/gms/common/a/d;

.field public static final u:Lcom/google/android/gms/common/a/d;

.field public static v:Lcom/google/android/gms/common/a/d;

.field public static w:Lcom/google/android/gms/common/a/d;

.field public static x:Lcom/google/android/gms/common/a/d;

.field public static y:Lcom/google/android/gms/common/a/d;

.field public static final z:Lcom/google/android/gms/common/a/d;


# direct methods
.method static constructor <clinit>()V
    .locals 10

    .prologue
    const/high16 v9, 0x42c80000    # 100.0f

    const/high16 v8, 0x3f800000    # 1.0f

    const-wide/16 v6, 0xa

    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 23
    const-string v0, "location:analytics_enabled"

    invoke-static {v0, v5}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Z)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/location/x;->a:Lcom/google/android/gms/common/a/d;

    .line 33
    const-string v0, "location:user_domain"

    const-string v1, "unknown"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/location/x;->b:Lcom/google/android/gms/common/a/d;

    .line 40
    const-string v0, "location:places_log_to_playlog"

    invoke-static {v0, v4}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Z)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/location/x;->c:Lcom/google/android/gms/common/a/d;

    .line 44
    const-string v0, "location:places_log_api_calls"

    invoke-static {v0, v4}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Z)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/location/x;->d:Lcom/google/android/gms/common/a/d;

    .line 48
    const-string v0, "location:places_log_method_calls"

    invoke-static {v0, v4}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Z)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/location/x;->e:Lcom/google/android/gms/common/a/d;

    .line 52
    const-string v0, "location:places_log_place_picker"

    invoke-static {v0, v4}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Z)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/location/x;->f:Lcom/google/android/gms/common/a/d;

    .line 56
    const-string v0, "location:places_sensors_logging_interval_msec"

    sget-object v1, Ljava/util/concurrent/TimeUnit;->MINUTES:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0x2

    invoke-virtual {v1, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/Long;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/location/x;->g:Lcom/google/android/gms/common/a/d;

    .line 60
    const-string v0, "location:places_enable_implicit_logging_location"

    invoke-static {v0, v4}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Z)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/location/x;->h:Lcom/google/android/gms/common/a/d;

    .line 64
    const-string v0, "location:places_enable_implicit_logging_wifi"

    invoke-static {v0, v4}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Z)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/location/x;->i:Lcom/google/android/gms/common/a/d;

    .line 68
    const-string v0, "location:places_get_location_deadline_msec"

    sget-object v1, Ljava/util/concurrent/TimeUnit;->MINUTES:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0x1

    invoke-virtual {v1, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/Long;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/location/x;->j:Lcom/google/android/gms/common/a/d;

    .line 73
    const-string v0, "location:places_get_location_retry_interval_msec"

    sget-object v1, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v1, v6, v7}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/Long;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/location/x;->k:Lcom/google/android/gms/common/a/d;

    .line 78
    const-string v0, "location:places_whitelisted_partners"

    const-string v1, ""

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/location/x;->l:Lcom/google/android/gms/common/a/d;

    .line 82
    const-string v0, "location:places_add_apiary_info"

    invoke-static {v0, v4}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Z)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/location/x;->m:Lcom/google/android/gms/common/a/d;

    .line 89
    const-string v0, "location:places_latency_collection_fraction"

    invoke-static {v8}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/Float;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/location/x;->n:Lcom/google/android/gms/common/a/d;

    .line 96
    const-string v0, "location:places_place_picker_user_behavior_collection_fraction"

    invoke-static {v8}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/Float;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/location/x;->o:Lcom/google/android/gms/common/a/d;

    .line 104
    const-string v0, "location:location_analytics_tracking_id"

    const-string v1, "UA-44492294-1"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/location/x;->p:Lcom/google/android/gms/common/a/d;

    .line 111
    const-string v0, "location:places_analytics_tracking_id"

    const-string v1, "UA-53776808-1"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/location/x;->q:Lcom/google/android/gms/common/a/d;

    .line 118
    const-string v0, "location:places_analytics_sampling_rate_percent"

    invoke-static {v9}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/Float;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/location/x;->r:Lcom/google/android/gms/common/a/d;

    .line 122
    const-string v0, "location:places:use_wifi_place_cache"

    invoke-static {v0, v5}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Z)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/location/x;->s:Lcom/google/android/gms/common/a/d;

    .line 126
    const-string v0, "location:places_default_nearby_alert_place_radius_meters"

    invoke-static {v9}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/Float;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/location/x;->t:Lcom/google/android/gms/common/a/d;

    .line 131
    const-string v0, "location:places_default_nearby_alert_refresh_radius_meters"

    const/high16 v1, 0x447a0000    # 1000.0f

    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/Float;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/location/x;->u:Lcom/google/android/gms/common/a/d;

    .line 139
    const-string v0, "url:location:personalized_places_server_url"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/location/x;->v:Lcom/google/android/gms/common/a/d;

    .line 143
    const-string v0, "location:personalized_places_server_enabled"

    invoke-static {v0, v4}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Z)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/location/x;->w:Lcom/google/android/gms/common/a/d;

    .line 147
    const-string v0, "location:personalized_places_api_path"

    const-string v1, "/locationmessaging/v2beta/locationPlatform/"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/location/x;->x:Lcom/google/android/gms/common/a/d;

    .line 151
    const-string v0, "location:personalized_places_auth_scope"

    const-string v1, "https://www.googleapis.com/auth/offers.lmp"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/location/x;->y:Lcom/google/android/gms/common/a/d;

    .line 155
    sget-object v0, Lcom/google/android/gms/location/places/UserDataType;->c:Lcom/google/android/gms/location/places/UserDataType;

    invoke-virtual {v0}, Lcom/google/android/gms/location/places/UserDataType;->b()I

    move-result v0

    sput v0, Lcom/google/android/location/x;->ab:I

    .line 160
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "com.google.android.apps.maps:"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget v1, Lcom/google/android/location/x;->ab:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ",com.google.android.apps.gmm:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget v1, Lcom/google/android/location/x;->ab:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ",com.google.android.apps.gmm.fishfood:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget v1, Lcom/google/android/location/x;->ab:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ",com.google.android.apps.gmm.dev:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget v1, Lcom/google/android/location/x;->ab:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/location/x;->ac:Ljava/lang/String;

    .line 173
    const-string v0, "location:personalized_places_user_data_white_list"

    sget-object v1, Lcom/google/android/location/x;->ac:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/location/x;->z:Lcom/google/android/gms/common/a/d;

    .line 181
    const-string v0, "location:flp_battery_threshold_full_power"

    const v1, 0x3f666666    # 0.9f

    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/Float;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/location/x;->A:Lcom/google/android/gms/common/a/d;

    .line 185
    const-string v0, "location:flp_battery_threshold_hysteresis"

    const v1, 0x3ca3d70a    # 0.02f

    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/Float;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/location/x;->B:Lcom/google/android/gms/common/a/d;

    .line 189
    const-string v0, "location:flp_enable_hal_debug"

    invoke-static {v0, v5}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Z)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/location/x;->C:Lcom/google/android/gms/common/a/d;

    .line 206
    const-string v0, "location:flp_hal_mode"

    const/4 v1, 0x3

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/Integer;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/location/x;->D:Lcom/google/android/gms/common/a/d;

    .line 210
    const-string v0, "location:flp_low_power_gps_min_interval_ms"

    sget-object v1, Ljava/util/concurrent/TimeUnit;->MINUTES:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v1, v6, v7}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/Long;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/location/x;->E:Lcom/google/android/gms/common/a/d;

    .line 213
    const-string v0, "location:flp_low_power_gps_pulse_ms"

    sget-object v1, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0xf

    invoke-virtual {v1, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/Long;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/location/x;->F:Lcom/google/android/gms/common/a/d;

    .line 216
    const-string v0, "location:flp_smd_interval_threshold_ms"

    sget-object v1, Ljava/util/concurrent/TimeUnit;->MINUTES:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0x6

    invoke-virtual {v1, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/Long;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/location/x;->G:Lcom/google/android/gms/common/a/d;

    .line 222
    const-string v0, "location:disable_gps_when_injected"

    invoke-static {v0, v4}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Z)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/location/x;->H:Lcom/google/android/gms/common/a/d;

    .line 228
    sget-object v0, Ljava/util/concurrent/TimeUnit;->MINUTES:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0x5

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    sput-wide v0, Lcom/google/android/location/x;->I:J

    .line 236
    const-string v0, "location:flp_low_power_mode_min_interval_ms"

    sget-wide v2, Lcom/google/android/location/x;->I:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/Long;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/location/x;->J:Lcom/google/android/gms/common/a/d;

    .line 241
    const-string v0, "location:flp_low_power_mode_min_priority"

    const/16 v1, 0x66

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/Integer;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/location/x;->K:Lcom/google/android/gms/common/a/d;

    .line 248
    const-string v0, "location:flp_smallestDisplacementMode"

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/Integer;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/location/x;->L:Lcom/google/android/gms/common/a/d;

    .line 263
    const-string v0, "location:enable_hardware_geofencing_olivet"

    invoke-static {v0, v4}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Z)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/location/x;->M:Lcom/google/android/gms/common/a/d;

    .line 270
    const-string v0, "location:flp_geofence_enable_hal_debug"

    invoke-static {v0, v5}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Z)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/location/x;->N:Lcom/google/android/gms/common/a/d;

    .line 278
    const-string v0, "location:force_hardware_geofence_when_available"

    invoke-static {v0, v5}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Z)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/location/x;->O:Lcom/google/android/gms/common/a/d;

    .line 286
    const-string v0, "location:use_hardware_geofence_when_unavailable"

    invoke-static {v0, v5}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Z)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/location/x;->P:Lcom/google/android/gms/common/a/d;

    .line 293
    const-string v0, "location:place_picker_enable_query_suggestions"

    invoke-static {v0, v4}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Z)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/location/x;->Q:Lcom/google/android/gms/common/a/d;

    .line 297
    const-string v0, "location:place_picker_my_location_deadline"

    sget-object v1, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v1, v6, v7}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/Long;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/location/x;->R:Lcom/google/android/gms/common/a/d;

    .line 302
    const-string v0, "location:place_picker_reverse_geocoding_deadline"

    const/16 v1, 0x3e8

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/Integer;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/location/x;->S:Lcom/google/android/gms/common/a/d;

    .line 306
    const-string v0, "location:place_picker_max_results"

    const/16 v1, 0x14

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/Integer;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/location/x;->T:Lcom/google/android/gms/common/a/d;

    .line 310
    const-string v0, "location:place_picker_places_server_deadline"

    sget-object v1, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v1, v6, v7}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/Long;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/location/x;->U:Lcom/google/android/gms/common/a/d;

    .line 315
    const-string v0, "location:place_inference_module_list"

    const-string v1, "ReverseGeocoding,WifiDecisionTree"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/location/x;->V:Lcom/google/android/gms/common/a/d;

    .line 323
    const-string v0, "location:place_cache_memory_capacity"

    const/16 v1, 0x64

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/Integer;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/location/x;->W:Lcom/google/android/gms/common/a/d;

    .line 327
    const-string v0, "location:place_cache_num_chunks"

    const/16 v1, 0x64

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/Integer;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/location/x;->X:Lcom/google/android/gms/common/a/d;

    .line 331
    const-string v0, "location:place_cache_num_entries_per_chunk"

    const/16 v1, 0xa

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/Integer;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/location/x;->Y:Lcom/google/android/gms/common/a/d;

    .line 335
    const-string v0, "location:place_cache_cron_job_interval_millis"

    sget-object v1, Ljava/util/concurrent/TimeUnit;->DAYS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0x1

    invoke-virtual {v1, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/Long;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/location/x;->Z:Lcom/google/android/gms/common/a/d;

    .line 340
    const-string v0, "location:place_cache_expiration_millis"

    sget-object v1, Ljava/util/concurrent/TimeUnit;->DAYS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0x7

    invoke-virtual {v1, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/Long;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/location/x;->aa:Lcom/google/android/gms/common/a/d;

    return-void
.end method
