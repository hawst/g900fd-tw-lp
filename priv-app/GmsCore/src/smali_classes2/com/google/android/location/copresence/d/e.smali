.class public final Lcom/google/android/location/copresence/d/e;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/location/copresence/d/a/o;


# instance fields
.field public final a:Landroid/bluetooth/BluetoothAdapter;

.field final b:Lcom/google/android/location/copresence/d/a/l;

.field final c:Ljava/util/HashSet;

.field d:Z

.field private final e:Landroid/content/Context;


# direct methods
.method constructor <init>(Landroid/content/Context;)V
    .locals 1
    .annotation build Landroid/annotation/TargetApi;
        value = 0x13
    .end annotation

    .prologue
    .line 57
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 58
    iput-object p1, p0, Lcom/google/android/location/copresence/d/e;->e:Landroid/content/Context;

    .line 59
    invoke-static {}, Landroid/bluetooth/BluetoothAdapter;->getDefaultAdapter()Landroid/bluetooth/BluetoothAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/copresence/d/e;->a:Landroid/bluetooth/BluetoothAdapter;

    .line 60
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/location/copresence/d/e;->d:Z

    .line 61
    new-instance v0, Lcom/google/android/location/copresence/d/a/l;

    invoke-direct {v0, p1, p0, p0}, Lcom/google/android/location/copresence/d/a/l;-><init>(Landroid/content/Context;Lcom/google/android/location/copresence/d/e;Lcom/google/android/location/copresence/d/a/o;)V

    iput-object v0, p0, Lcom/google/android/location/copresence/d/e;->b:Lcom/google/android/location/copresence/d/a/l;

    .line 62
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/location/copresence/d/e;->c:Ljava/util/HashSet;

    .line 63
    return-void
.end method

.method private a(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Class;Ljava/lang/Object;Ljava/lang/Class;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 7

    .prologue
    .line 355
    iget-object v0, p0, Lcom/google/android/location/copresence/d/e;->a:Landroid/bluetooth/BluetoothAdapter;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object v6, p6

    invoke-static/range {v0 .. v6}, Lcom/google/android/location/copresence/bd;->a(Ljava/lang/Object;Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Class;Ljava/lang/Object;Ljava/lang/Class;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(Lcom/google/android/location/copresence/d/a/k;)V
    .locals 2

    .prologue
    .line 396
    iget-object v0, p0, Lcom/google/android/location/copresence/d/e;->c:Ljava/util/HashSet;

    if-eqz v0, :cond_0

    .line 397
    iget-object v0, p0, Lcom/google/android/location/copresence/d/e;->c:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/copresence/d/f;

    .line 398
    invoke-interface {v0, p1}, Lcom/google/android/location/copresence/d/f;->a(Lcom/google/android/location/copresence/d/a/k;)V

    goto :goto_0

    .line 401
    :cond_0
    return-void
.end method

.method public final a(Lcom/google/android/location/copresence/d/f;)V
    .locals 2

    .prologue
    .line 165
    const/4 v0, 0x2

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 166
    const-string v0, "BluetoothAdapterWrapper: stopBleScan"

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->a(Ljava/lang/String;)I

    .line 168
    :cond_0
    const-string v0, "stopBleScan"

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/google/android/location/copresence/d/e;->a(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 169
    iget-object v0, p0, Lcom/google/android/location/copresence/d/e;->c:Ljava/util/HashSet;

    invoke-virtual {v0, p1}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    .line 170
    iget-object v0, p0, Lcom/google/android/location/copresence/d/e;->c:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 171
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/location/copresence/d/e;->d:Z

    .line 172
    iget-object v0, p0, Lcom/google/android/location/copresence/d/e;->b:Lcom/google/android/location/copresence/d/a/l;

    iget-object v1, v0, Lcom/google/android/location/copresence/d/a/l;->b:Lcom/google/android/location/copresence/d/a/n;

    if-eqz v1, :cond_1

    iget-object v0, v0, Lcom/google/android/location/copresence/d/a/l;->b:Lcom/google/android/location/copresence/d/a/n;

    invoke-interface {v0}, Lcom/google/android/location/copresence/d/a/n;->b()Z

    .line 173
    :cond_1
    const-string v0, "stopBleScan"

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/location/copresence/d/e;->a(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 176
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Boolean;)V
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 364
    iget-object v0, p0, Lcom/google/android/location/copresence/d/e;->a:Landroid/bluetooth/BluetoothAdapter;

    if-nez v0, :cond_1

    .line 365
    const/4 v0, 0x6

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 366
    const-string v0, "BluetoothAdapterWrapper: Bluetooth unsupported, unable to log transition."

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->e(Ljava/lang/String;)I

    .line 382
    :cond_0
    :goto_0
    return-void

    .line 370
    :cond_1
    new-instance v3, Lcom/google/android/location/copresence/debug/CopresDatabaseHelper$BluetoothAdapterMethodCallRecord;

    invoke-direct {v3}, Lcom/google/android/location/copresence/debug/CopresDatabaseHelper$BluetoothAdapterMethodCallRecord;-><init>()V

    .line 371
    iput-object p1, v3, Lcom/google/android/location/copresence/debug/CopresDatabaseHelper$BluetoothAdapterMethodCallRecord;->methodName:Ljava/lang/String;

    .line 372
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, v3, Lcom/google/android/location/copresence/debug/CopresDatabaseHelper$BluetoothAdapterMethodCallRecord;->callTimestamp:Ljava/lang/Long;

    .line 373
    if-nez p2, :cond_2

    const/4 v0, -0x1

    :goto_1
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, v3, Lcom/google/android/location/copresence/debug/CopresDatabaseHelper$BluetoothAdapterMethodCallRecord;->startedSuccessfully:Ljava/lang/Integer;

    .line 374
    iget-object v0, p0, Lcom/google/android/location/copresence/d/e;->a:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothAdapter;->getState()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, v3, Lcom/google/android/location/copresence/debug/CopresDatabaseHelper$BluetoothAdapterMethodCallRecord;->bluetoothState:Ljava/lang/Integer;

    .line 375
    iget-object v0, p0, Lcom/google/android/location/copresence/d/e;->a:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothAdapter;->getName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v3, Lcom/google/android/location/copresence/debug/CopresDatabaseHelper$BluetoothAdapterMethodCallRecord;->bluetoothName:Ljava/lang/String;

    .line 376
    iget-object v0, p0, Lcom/google/android/location/copresence/d/e;->a:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothAdapter;->getScanMode()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, v3, Lcom/google/android/location/copresence/debug/CopresDatabaseHelper$BluetoothAdapterMethodCallRecord;->bluetoothDiscoverabilityState:Ljava/lang/Integer;

    .line 377
    invoke-virtual {p0}, Lcom/google/android/location/copresence/d/e;->c()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, v3, Lcom/google/android/location/copresence/debug/CopresDatabaseHelper$BluetoothAdapterMethodCallRecord;->bluetoothDiscoverabilityTimeout:Ljava/lang/Integer;

    .line 378
    invoke-virtual {p0}, Lcom/google/android/location/copresence/d/e;->d()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, v3, Lcom/google/android/location/copresence/debug/CopresDatabaseHelper$BluetoothAdapterMethodCallRecord;->bluetoothConnection:Ljava/lang/Integer;

    .line 379
    iget-object v0, p0, Lcom/google/android/location/copresence/d/e;->a:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothAdapter;->isDiscovering()Z

    move-result v0

    if-eqz v0, :cond_4

    move v0, v1

    :goto_2
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, v3, Lcom/google/android/location/copresence/debug/CopresDatabaseHelper$BluetoothAdapterMethodCallRecord;->bluetoothDiscovering:Ljava/lang/Integer;

    .line 380
    iget-boolean v0, p0, Lcom/google/android/location/copresence/d/e;->d:Z

    if-eqz v0, :cond_5

    :goto_3
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, v3, Lcom/google/android/location/copresence/debug/CopresDatabaseHelper$BluetoothAdapterMethodCallRecord;->bleDiscovering:Ljava/lang/Integer;

    .line 381
    iget-object v0, p0, Lcom/google/android/location/copresence/d/e;->e:Landroid/content/Context;

    invoke-static {v0, v3}, Lcom/google/android/location/copresence/debug/CopresDatabaseHelper;->a(Landroid/content/Context;Lcom/google/android/location/copresence/debug/a;)V

    goto :goto_0

    .line 373
    :cond_2
    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_3

    move v0, v1

    goto :goto_1

    :cond_3
    move v0, v2

    goto :goto_1

    :cond_4
    move v0, v2

    .line 379
    goto :goto_2

    :cond_5
    move v1, v2

    .line 380
    goto :goto_3
.end method

.method public final a()Z
    .locals 3

    .prologue
    .line 202
    const/4 v0, 0x2

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 203
    const-string v0, "BluetoothAdapterWrapper: enable"

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->a(Ljava/lang/String;)I

    .line 205
    :cond_0
    const-string v0, "enable"

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/google/android/location/copresence/d/e;->a(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 206
    iget-object v0, p0, Lcom/google/android/location/copresence/d/e;->a:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothAdapter;->enable()Z

    move-result v0

    .line 207
    const-string v1, "enable"

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {p0, v1, v2}, Lcom/google/android/location/copresence/d/e;->a(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 208
    return v0
.end method

.method final a(II)Z
    .locals 8

    .prologue
    const/4 v0, 0x2

    const/4 v7, 0x0

    .line 294
    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 295
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "BluetoothAdapterWrapper: Starting classic bluetooth discovery mode: scanMode ="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->a(Ljava/lang/String;)I

    .line 297
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "setScanMode to "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " with duration "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/google/android/location/copresence/d/e;->a(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 300
    :try_start_0
    const-class v1, Ljava/lang/Boolean;

    const-string v2, "setScanMode"

    sget-object v3, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    sget-object v5, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, Lcom/google/android/location/copresence/d/e;->a(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Class;Ljava/lang/Object;Ljava/lang/Class;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    const-string v1, "setScanMode"

    invoke-static {v0, v1}, Lcom/google/android/location/copresence/bd;->a(Ljava/lang/Boolean;Ljava/lang/String;)Z

    move-result v0

    .line 302
    const/4 v1, 0x2

    invoke-static {v1}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 303
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "BluetoothAdapterWrapper: Scan mode result: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/location/copresence/ag;->a(Ljava/lang/String;)I

    .line 305
    :cond_1
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "setScanMode to "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " with duration "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {p0, v1, v2}, Lcom/google/android/location/copresence/d/e;->a(Ljava/lang/String;Ljava/lang/Boolean;)V
    :try_end_0
    .catch Lcom/google/android/location/copresence/be; {:try_start_0 .. :try_end_0} :catch_0

    .line 310
    :goto_0
    return v0

    .line 309
    :catch_0
    move-exception v0

    const-string v0, "setScanMode"

    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/location/copresence/d/e;->a(Ljava/lang/String;Ljava/lang/Boolean;)V

    move v0, v7

    .line 310
    goto :goto_0
.end method

.method public final a(Ljava/lang/String;)Z
    .locals 3

    .prologue
    .line 243
    const/4 v0, 0x2

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 244
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "BluetoothAdapterWrapper: setName \'"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->a(Ljava/lang/String;)I

    .line 246
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "setName to "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/google/android/location/copresence/d/e;->a(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 247
    iget-object v0, p0, Lcom/google/android/location/copresence/d/e;->a:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v0, p1}, Landroid/bluetooth/BluetoothAdapter;->setName(Ljava/lang/String;)Z

    move-result v0

    .line 248
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "setName to "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {p0, v1, v2}, Lcom/google/android/location/copresence/d/e;->a(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 249
    return v0
.end method

.method public final b()Z
    .locals 3

    .prologue
    .line 215
    const/4 v0, 0x2

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 216
    const-string v0, "BluetoothAdapterWrapper: disable"

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->a(Ljava/lang/String;)I

    .line 218
    :cond_0
    const-string v0, "disable"

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/google/android/location/copresence/d/e;->a(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 219
    iget-object v0, p0, Lcom/google/android/location/copresence/d/e;->a:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothAdapter;->disable()Z

    move-result v0

    .line 220
    const-string v1, "disable"

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {p0, v1, v2}, Lcom/google/android/location/copresence/d/e;->a(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 221
    return v0
.end method

.method final c()I
    .locals 8

    .prologue
    const/16 v7, 0x78

    .line 320
    .line 322
    :try_start_0
    const-class v1, Ljava/lang/Integer;

    const-string v2, "getDiscoverableTimeout"

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, Lcom/google/android/location/copresence/d/e;->a(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Class;Ljava/lang/Object;Ljava/lang/Class;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    const-string v1, "getDiscoverableTimeout"

    invoke-static {v0, v1}, Lcom/google/android/location/copresence/bd;->a(Ljava/lang/Integer;Ljava/lang/String;)I
    :try_end_0
    .catch Lcom/google/android/location/copresence/be; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 324
    const/4 v1, 0x3

    :try_start_1
    invoke-static {v1}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 325
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "BluetoothAdapterWrapper: getDiscoverableTimeout is "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/location/copresence/ag;->b(Ljava/lang/String;)I
    :try_end_1
    .catch Lcom/google/android/location/copresence/be; {:try_start_1 .. :try_end_1} :catch_1

    .line 330
    :cond_0
    :goto_0
    const/4 v1, -0x1

    if-ne v0, v1, :cond_1

    move v0, v7

    .line 336
    :cond_1
    return v0

    :catch_0
    move-exception v0

    move v0, v7

    goto :goto_0

    :catch_1
    move-exception v1

    goto :goto_0
.end method

.method final d()I
    .locals 7

    .prologue
    .line 345
    :try_start_0
    const-class v1, Ljava/lang/Integer;

    const-string v2, "getConnectionState"

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, Lcom/google/android/location/copresence/d/e;->a(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Class;Ljava/lang/Object;Ljava/lang/Class;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    const-string v1, "getConnectionState"

    invoke-static {v0, v1}, Lcom/google/android/location/copresence/bd;->a(Ljava/lang/Integer;Ljava/lang/String;)I
    :try_end_0
    .catch Lcom/google/android/location/copresence/be; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 348
    :goto_0
    return v0

    :catch_0
    move-exception v0

    const/high16 v0, -0x80000000

    goto :goto_0
.end method
