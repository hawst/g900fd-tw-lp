.class final Lcom/google/android/location/internal/server/c;
.super Lcom/google/android/location/internal/server/g;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/location/internal/server/b;

.field private final k:[I


# direct methods
.method constructor <init>(Lcom/google/android/location/internal/server/b;Landroid/app/PendingIntent;ILcom/google/android/location/o/h;Lcom/google/android/location/o/n;ZLjava/lang/String;[I)V
    .locals 9

    .prologue
    .line 345
    iput-object p1, p0, Lcom/google/android/location/internal/server/c;->a:Lcom/google/android/location/internal/server/b;

    .line 346
    const/4 v4, 0x0

    move-object v1, p0

    move-object v2, p2

    move v3, p3

    move-object v5, p4

    move-object v6, p5

    move v7, p6

    move-object/from16 v8, p7

    invoke-direct/range {v1 .. v8}, Lcom/google/android/location/internal/server/g;-><init>(Landroid/app/PendingIntent;IILcom/google/android/location/o/h;Lcom/google/android/location/o/n;ZLjava/lang/String;)V

    .line 349
    if-nez p8, :cond_0

    invoke-virtual {p2}, Landroid/app/PendingIntent;->getTargetPackage()Ljava/lang/String;

    move-result-object v1

    const-string v2, "com.google.android.wearable.app"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    new-array v0, v1, [I

    move-object/from16 p8, v0

    const/4 v1, 0x0

    const/16 v2, 0x9

    aput v2, p8, v1

    :cond_0
    move-object/from16 v0, p8

    iput-object v0, p0, Lcom/google/android/location/internal/server/c;->k:[I

    .line 352
    return-void
.end method

.method private a(I)Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 359
    iget-object v0, p0, Lcom/google/android/location/internal/server/c;->k:[I

    if-nez v0, :cond_1

    .line 367
    :cond_0
    :goto_0
    return v1

    :cond_1
    move v0, v1

    .line 362
    :goto_1
    iget-object v2, p0, Lcom/google/android/location/internal/server/c;->k:[I

    array-length v2, v2

    if-ge v0, v2, :cond_0

    .line 363
    iget-object v2, p0, Lcom/google/android/location/internal/server/c;->k:[I

    aget v2, v2, v0

    if-ne p1, v2, :cond_2

    .line 364
    const/4 v1, 0x1

    goto :goto_0

    .line 362
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method


# virtual methods
.method final a(Landroid/content/Context;Landroid/content/Intent;)Z
    .locals 10

    .prologue
    const/4 v6, 0x1

    const/4 v3, 0x0

    .line 372
    iget-object v0, p0, Lcom/google/android/location/internal/server/c;->a:Lcom/google/android/location/internal/server/b;

    iget-object v0, v0, Lcom/google/android/location/internal/server/b;->i:Lcom/google/android/location/o/a;

    iget-object v1, p0, Lcom/google/android/location/internal/server/c;->b:Landroid/app/PendingIntent;

    invoke-virtual {v1}, Landroid/app/PendingIntent;->getTargetPackage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/location/o/a;->a(Ljava/lang/String;)I

    move-result v7

    .line 374
    const-string v0, "com.google.android.location.internal.EXTRA_ACTIVITY_RESULT"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 375
    const-string v0, "com.google.android.location.internal.EXTRA_ACTIVITY_RESULT"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/google/android/gms/location/ActivityRecognitionResult;

    .line 378
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 380
    invoke-virtual {v4}, Lcom/google/android/gms/location/ActivityRecognitionResult;->c()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_0
    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/location/DetectedActivity;

    .line 381
    invoke-virtual {v0}, Lcom/google/android/gms/location/DetectedActivity;->a()I

    move-result v2

    const v8, 0x432380

    if-ge v7, v8, :cond_1

    const/4 v8, 0x6

    if-le v2, v8, :cond_2

    move v2, v3

    :goto_1
    if-eqz v2, :cond_0

    .line 382
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 381
    :cond_1
    const v8, 0x4dd1e0

    if-ge v7, v8, :cond_2

    const/16 v8, 0x8

    if-le v2, v8, :cond_2

    move v2, v3

    goto :goto_1

    :cond_2
    iget-boolean v8, p0, Lcom/google/android/location/internal/server/c;->i:Z

    if-nez v8, :cond_3

    iget-object v8, p0, Lcom/google/android/location/internal/server/c;->a:Lcom/google/android/location/internal/server/b;

    iget-object v8, v8, Lcom/google/android/location/internal/server/b;->k:Ljava/util/Set;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-interface {v8, v9}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_3

    move v2, v3

    goto :goto_1

    :cond_3
    const/4 v8, 0x5

    if-ne v2, v8, :cond_4

    invoke-static {p1}, Lcom/google/android/location/os/real/aw;->a(Landroid/content/Context;)Z

    move-result v8

    if-eqz v8, :cond_4

    move v2, v3

    goto :goto_1

    :cond_4
    iget-object v8, p0, Lcom/google/android/location/internal/server/c;->a:Lcom/google/android/location/internal/server/b;

    iget-object v8, v8, Lcom/google/android/location/internal/server/b;->a:Ljava/util/HashSet;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_5

    invoke-direct {p0, v2}, Lcom/google/android/location/internal/server/c;->a(I)Z

    move-result v2

    if-nez v2, :cond_5

    move v2, v3

    goto :goto_1

    :cond_5
    move v2, v6

    goto :goto_1

    .line 385
    :cond_6
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_8

    .line 386
    new-instance v0, Lcom/google/android/gms/location/ActivityRecognitionResult;

    invoke-virtual {v4}, Lcom/google/android/gms/location/ActivityRecognitionResult;->d()J

    move-result-wide v2

    invoke-virtual {v4}, Lcom/google/android/gms/location/ActivityRecognitionResult;->e()J

    move-result-wide v4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/location/ActivityRecognitionResult;-><init>(Ljava/util/List;JJ)V

    .line 390
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1, p2}, Landroid/content/Intent;-><init>(Landroid/content/Intent;)V

    const-string v2, "com.google.android.location.internal.EXTRA_ACTIVITY_RESULT"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v1

    .line 401
    const v0, 0x7fffffff

    if-lt v7, v0, :cond_a

    .line 412
    sget-boolean v0, Lcom/google/android/location/i/a;->b:Z

    if-eqz v0, :cond_7

    const-string v0, "ActivityRecognitionListener"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "ActivityIntentReceiver.deliverIntent: Send byte array to new client. clientVersion="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/google/android/location/o/a/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 415
    :cond_7
    const-string v0, "com.google.android.location.internal.EXTRA_ACTIVITY_RESULT"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/location/ActivityRecognitionResult;

    .line 418
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v2

    .line 419
    invoke-virtual {v0, v2, v6}, Lcom/google/android/gms/location/ActivityRecognitionResult;->writeToParcel(Landroid/os/Parcel;I)V

    .line 421
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Intent;)V

    const-string v1, "com.google.android.location.internal.EXTRA_ACTIVITY_RESULT"

    invoke-virtual {v2}, Landroid/os/Parcel;->marshall()[B

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[B)Landroid/content/Intent;

    move-result-object v0

    .line 423
    :goto_2
    invoke-super {p0, p1, v0}, Lcom/google/android/location/internal/server/g;->a(Landroid/content/Context;Landroid/content/Intent;)Z

    move-result v0

    .line 425
    :goto_3
    return v0

    :cond_8
    move v0, v6

    .line 393
    goto :goto_3

    :cond_9
    move v0, v6

    .line 425
    goto :goto_3

    :cond_a
    move-object v0, v1

    goto :goto_2
.end method

.method public final a()[I
    .locals 1

    .prologue
    .line 355
    iget-object v0, p0, Lcom/google/android/location/internal/server/c;->k:[I

    return-object v0
.end method

.method final b()V
    .locals 6

    .prologue
    .line 472
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    iget-object v0, p0, Lcom/google/android/location/internal/server/c;->b:Landroid/app/PendingIntent;

    invoke-virtual {v0}, Landroid/app/PendingIntent;->getTargetPackage()Ljava/lang/String;

    move-result-object v1

    iget-object v4, p0, Lcom/google/android/location/internal/server/c;->a:Lcom/google/android/location/internal/server/b;

    iget-object v0, v4, Lcom/google/android/location/internal/server/b;->d:Ljava/util/Map;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/internal/server/d;

    if-nez v0, :cond_0

    iget-object v0, v4, Lcom/google/android/location/internal/server/b;->d:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v0

    const/16 v5, 0x28

    if-le v0, v5, :cond_2

    const/4 v0, 0x0

    :cond_0
    :goto_0
    if-eqz v0, :cond_1

    iget-object v0, v0, Lcom/google/android/location/internal/server/d;->a:Lcom/google/android/location/internal/server/o;

    invoke-virtual {v0, p0, v2, v3}, Lcom/google/android/location/internal/server/o;->a(Lcom/google/android/location/internal/server/g;J)V

    .line 473
    :cond_1
    return-void

    .line 472
    :cond_2
    new-instance v0, Lcom/google/android/location/internal/server/d;

    const/4 v5, 0x0

    invoke-direct {v0, v4, v5}, Lcom/google/android/location/internal/server/d;-><init>(Lcom/google/android/location/internal/server/b;B)V

    iget-object v4, v4, Lcom/google/android/location/internal/server/b;->d:Ljava/util/Map;

    invoke-interface {v4, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method
