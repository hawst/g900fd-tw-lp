.class final Lcom/google/android/location/copresence/g;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/location/copresence/q/at;


# instance fields
.field final synthetic a:Lcom/google/android/location/copresence/f;


# direct methods
.method constructor <init>(Lcom/google/android/location/copresence/f;)V
    .locals 0

    .prologue
    .line 73
    iput-object p1, p0, Lcom/google/android/location/copresence/g;->a:Lcom/google/android/location/copresence/f;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 76
    const/4 v0, 0x5

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 77
    const-string v0, "BluetoothCleanup: Bluetooth revert attempt timed out."

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->d(Ljava/lang/String;)I

    .line 79
    :cond_0
    const/4 v0, 0x1

    invoke-static {v0}, Lcom/google/android/location/copresence/b/a;->a(Z)V

    .line 81
    return-void
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 85
    const/4 v0, 0x5

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 86
    const-string v0, "BluetoothCleanup: Bluetooth revert attempt succeeded."

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->d(Ljava/lang/String;)I

    .line 88
    :cond_0
    const/4 v0, 0x0

    invoke-static {v0}, Lcom/google/android/location/copresence/b/a;->a(Z)V

    .line 90
    return-void
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 94
    const/4 v0, 0x5

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 95
    const-string v0, "BluetoothCleanup: Bluetooth revert attempt failed."

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->d(Ljava/lang/String;)I

    .line 97
    :cond_0
    const/4 v0, 0x1

    invoke-static {v0}, Lcom/google/android/location/copresence/b/a;->a(Z)V

    .line 99
    return-void
.end method
