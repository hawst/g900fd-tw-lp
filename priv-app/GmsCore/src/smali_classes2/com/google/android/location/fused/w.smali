.class public final Lcom/google/android/location/fused/w;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/location/fused/ap;
.implements Lcom/google/android/location/fused/bb;
.implements Lcom/google/android/location/fused/service/b;


# instance fields
.field final a:Lcom/google/android/location/fused/c;

.field b:Lcom/google/q/a/b/b/t;

.field final c:Lcom/google/android/location/fused/bf;

.field final d:Lcom/google/android/location/fused/ad;

.field private final e:Lcom/google/android/gms/common/util/p;

.field private final f:Ljava/lang/String;

.field private final g:Landroid/os/Handler;

.field private final h:Landroid/content/Context;

.field private i:Lcom/google/android/location/fused/aq;

.field private j:Landroid/location/Location;

.field private final k:Lcom/google/android/location/fused/ao;

.field private final l:Lcom/google/android/location/fused/ao;

.field private m:J

.field private n:J

.field private o:Z

.field private p:Z

.field private q:Z

.field private r:Lcom/google/android/location/fused/al;

.field private s:I

.field private t:Lcom/google/android/location/fused/al;

.field private final u:Lcom/google/android/location/fused/ai;

.field private final v:Lcom/google/android/location/fused/ac;

.field private final w:[Landroid/location/Location;


# direct methods
.method private constructor <init>(Landroid/content/Context;Landroid/os/Looper;Landroid/location/LocationManager;Lcom/google/android/location/fused/ar;Landroid/hardware/SensorManager;Lcom/google/android/location/fused/c;Lcom/google/android/location/fused/az;Lcom/google/android/gms/common/util/p;Lcom/google/android/location/fused/aq;)V
    .locals 20

    .prologue
    .line 165
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    .line 120
    const/4 v4, 0x1

    move-object/from16 v0, p0

    iput-boolean v4, v0, Lcom/google/android/location/fused/w;->p:Z

    .line 121
    const/4 v4, 0x1

    move-object/from16 v0, p0

    iput-boolean v4, v0, Lcom/google/android/location/fused/w;->q:Z

    .line 135
    new-instance v4, Lcom/google/android/location/fused/ac;

    invoke-direct {v4}, Lcom/google/android/location/fused/ac;-><init>()V

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/google/android/location/fused/w;->v:Lcom/google/android/location/fused/ac;

    .line 140
    const/4 v4, 0x1

    new-array v4, v4, [Landroid/location/Location;

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/google/android/location/fused/w;->w:[Landroid/location/Location;

    .line 166
    move-object/from16 v0, p8

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/location/fused/w;->e:Lcom/google/android/gms/common/util/p;

    .line 167
    move-object/from16 v0, p1

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/location/fused/w;->h:Landroid/content/Context;

    .line 168
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/google/android/location/fused/w;->f:Ljava/lang/String;

    .line 169
    new-instance v4, Landroid/os/Handler;

    move-object/from16 v0, p2

    invoke-direct {v4, v0}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/google/android/location/fused/w;->g:Landroid/os/Handler;

    .line 170
    move-object/from16 v0, p6

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/location/fused/w;->a:Lcom/google/android/location/fused/c;

    .line 171
    new-instance v4, Lcom/google/android/location/fused/bf;

    move-object/from16 v0, p6

    invoke-direct {v4, v0}, Lcom/google/android/location/fused/bf;-><init>(Lcom/google/android/location/fused/c;)V

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/google/android/location/fused/w;->c:Lcom/google/android/location/fused/bf;

    .line 173
    move-object/from16 v0, p9

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/location/fused/w;->i:Lcom/google/android/location/fused/aq;

    .line 175
    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/google/android/location/fused/w;->r:Lcom/google/android/location/fused/al;

    .line 176
    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/google/android/location/fused/w;->t:Lcom/google/android/location/fused/al;

    .line 177
    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput v4, v0, Lcom/google/android/location/fused/w;->s:I

    .line 179
    new-instance v4, Lcom/google/q/a/b/b/h;

    new-instance v5, Lcom/google/q/a/b/b/ah;

    invoke-direct {v5}, Lcom/google/q/a/b/b/ah;-><init>()V

    invoke-direct {v4, v5}, Lcom/google/q/a/b/b/h;-><init>(Lcom/google/q/a/b/b/t;)V

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/google/android/location/fused/w;->b:Lcom/google/q/a/b/b/t;

    .line 181
    new-instance v4, Lcom/google/android/location/fused/ao;

    move-object/from16 v0, p6

    invoke-direct {v4, v0}, Lcom/google/android/location/fused/ao;-><init>(Lcom/google/android/location/fused/c;)V

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/google/android/location/fused/w;->k:Lcom/google/android/location/fused/ao;

    .line 182
    new-instance v4, Lcom/google/android/location/fused/ao;

    move-object/from16 v0, p6

    invoke-direct {v4, v0}, Lcom/google/android/location/fused/ao;-><init>(Lcom/google/android/location/fused/c;)V

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/google/android/location/fused/w;->l:Lcom/google/android/location/fused/ao;

    .line 184
    new-instance v4, Lcom/google/android/location/fused/ai;

    move-object/from16 v0, p3

    invoke-direct {v4, v0}, Lcom/google/android/location/fused/ai;-><init>(Landroid/location/LocationManager;)V

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/google/android/location/fused/w;->u:Lcom/google/android/location/fused/ai;

    .line 185
    new-instance v12, Lcom/google/android/location/fused/bd;

    move-object/from16 v0, p1

    invoke-direct {v12, v0}, Lcom/google/android/location/fused/bd;-><init>(Landroid/content/Context;)V

    .line 186
    new-instance v13, Lcom/google/android/location/fused/a;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/location/fused/w;->g:Landroid/os/Handler;

    move-object/from16 v0, p1

    move-object/from16 v1, p5

    invoke-direct {v13, v0, v1, v4}, Lcom/google/android/location/fused/a;-><init>(Landroid/content/Context;Landroid/hardware/SensorManager;Landroid/os/Handler;)V

    .line 188
    new-instance v14, Lcom/google/android/location/fused/x;

    move-object/from16 v0, p0

    invoke-direct {v14, v0, v13}, Lcom/google/android/location/fused/x;-><init>(Lcom/google/android/location/fused/w;Lcom/google/android/location/fused/a;)V

    .line 203
    const-string v4, "alarm"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/app/AlarmManager;

    invoke-static {v4}, Lcom/google/android/location/n/c;->a(Landroid/app/AlarmManager;)Lcom/google/android/location/n/c;

    move-result-object v15

    .line 205
    invoke-static/range {p2 .. p2}, Lcom/google/android/location/fused/service/a;->a(Landroid/os/Looper;)Lcom/google/android/location/fused/service/a;

    move-result-object v11

    .line 206
    move-object/from16 v0, p0

    invoke-virtual {v11, v0}, Lcom/google/android/location/fused/service/a;->a(Lcom/google/android/location/fused/service/b;)V

    .line 207
    new-instance v16, Lcom/google/android/location/fused/ad;

    new-instance v17, Lcom/google/android/location/fused/a/c;

    new-instance v4, Lcom/google/android/location/fused/a/f;

    new-instance v6, Lcom/google/android/location/fused/z;

    move-object/from16 v0, p0

    invoke-direct {v6, v0}, Lcom/google/android/location/fused/z;-><init>(Lcom/google/android/location/fused/w;)V

    new-instance v7, Lcom/google/android/location/fused/aa;

    move-object/from16 v0, p0

    move-object/from16 v1, p3

    invoke-direct {v7, v0, v1}, Lcom/google/android/location/fused/aa;-><init>(Lcom/google/android/location/fused/w;Landroid/location/LocationManager;)V

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/location/fused/w;->u:Lcom/google/android/location/fused/ai;

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/location/fused/w;->v:Lcom/google/android/location/fused/ac;

    move-object/from16 v5, p4

    move-object/from16 v10, p2

    invoke-direct/range {v4 .. v10}, Lcom/google/android/location/fused/a/f;-><init>(Lcom/google/android/location/fused/ar;Landroid/location/LocationListener;Landroid/location/GpsStatus$Listener;Lcom/google/android/location/fused/ai;Lcom/google/android/location/fused/ac;Landroid/os/Looper;)V

    new-instance v5, Lcom/google/android/location/fused/a/d;

    const/4 v7, 0x3

    move-object v6, v11

    move-object/from16 v8, p1

    move-object v9, v15

    move-object/from16 v10, p8

    move-object/from16 v11, p2

    invoke-direct/range {v5 .. v11}, Lcom/google/android/location/fused/a/d;-><init>(Lcom/google/android/location/fused/service/a;ILandroid/content/Context;Lcom/google/android/location/n/c;Lcom/google/android/gms/common/util/p;Landroid/os/Looper;)V

    move-object/from16 v0, v17

    invoke-direct {v0, v4, v5}, Lcom/google/android/location/fused/a/c;-><init>(Lcom/google/android/location/fused/a/f;Lcom/google/android/location/fused/a/d;)V

    new-instance v18, Lcom/google/android/location/fused/a/p;

    new-instance v4, Lcom/google/android/location/fused/ab;

    move-object/from16 v0, p0

    invoke-direct {v4, v0}, Lcom/google/android/location/fused/ab;-><init>(Lcom/google/android/location/fused/w;)V

    move-object/from16 v0, v18

    move-object/from16 v1, p4

    move-object/from16 v2, p2

    invoke-direct {v0, v1, v4, v2}, Lcom/google/android/location/fused/a/p;-><init>(Lcom/google/android/location/fused/ar;Landroid/location/LocationListener;Landroid/os/Looper;)V

    new-instance v19, Lcom/google/android/location/fused/a/u;

    new-instance v4, Lcom/google/android/location/fused/bp;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/location/fused/w;->g:Landroid/os/Handler;

    move-object/from16 v0, p5

    move-object/from16 v1, p8

    invoke-direct {v4, v0, v14, v5, v1}, Lcom/google/android/location/fused/bp;-><init>(Landroid/hardware/SensorManager;Lcom/google/q/a/b/b/z;Landroid/os/Handler;Lcom/google/android/gms/common/util/p;)V

    move-object/from16 v0, v19

    invoke-direct {v0, v4, v13}, Lcom/google/android/location/fused/a/u;-><init>(Lcom/google/android/location/fused/bp;Lcom/google/android/location/fused/a;)V

    new-instance v13, Lcom/google/android/location/fused/a/o;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/location/fused/w;->v:Lcom/google/android/location/fused/ac;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/location/fused/w;->c:Lcom/google/android/location/fused/bf;

    invoke-direct {v13, v12, v4, v5}, Lcom/google/android/location/fused/a/o;-><init>(Lcom/google/android/location/fused/bd;Lcom/google/android/location/fused/ac;Lcom/google/android/location/fused/bf;)V

    new-instance v10, Lcom/google/android/location/fused/a/n;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/location/fused/w;->v:Lcom/google/android/location/fused/ac;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/location/fused/w;->c:Lcom/google/android/location/fused/bf;

    invoke-direct {v10, v12, v4, v5}, Lcom/google/android/location/fused/a/n;-><init>(Lcom/google/android/location/fused/bd;Lcom/google/android/location/fused/ac;Lcom/google/android/location/fused/bf;)V

    new-instance v11, Lcom/google/android/location/fused/a/a;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/location/fused/w;->v:Lcom/google/android/location/fused/ac;

    invoke-direct {v11, v12, v4}, Lcom/google/android/location/fused/a/a;-><init>(Lcom/google/android/location/fused/bd;Lcom/google/android/location/fused/ac;)V

    new-instance v4, Lcom/google/android/location/fused/a/g;

    invoke-static {}, Lcom/google/android/gms/common/util/r;->c()Lcom/google/android/gms/common/util/p;

    move-result-object v9

    move-object/from16 v5, p4

    move-object v6, v15

    move-object/from16 v7, p1

    move-object/from16 v8, p2

    invoke-direct/range {v4 .. v9}, Lcom/google/android/location/fused/a/g;-><init>(Lcom/google/android/location/fused/ar;Lcom/google/android/location/n/c;Landroid/content/Context;Landroid/os/Looper;Lcom/google/android/gms/common/util/p;)V

    move-object/from16 v5, v16

    move-object/from16 v6, v17

    move-object/from16 v7, v18

    move-object/from16 v8, v19

    move-object v9, v13

    move-object v12, v4

    move-object/from16 v13, p5

    move-object/from16 v14, p1

    move-object/from16 v15, p2

    invoke-direct/range {v5 .. v15}, Lcom/google/android/location/fused/ad;-><init>(Lcom/google/android/location/fused/a/m;Lcom/google/android/location/fused/a/p;Lcom/google/android/location/fused/a/u;Lcom/google/android/location/fused/a/o;Lcom/google/android/location/fused/a/n;Lcom/google/android/location/fused/a/a;Lcom/google/android/location/fused/a/g;Landroid/hardware/SensorManager;Landroid/content/Context;Landroid/os/Looper;)V

    move-object/from16 v0, v16

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/location/fused/w;->d:Lcom/google/android/location/fused/ad;

    .line 240
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/location/fused/w;->e:Lcom/google/android/gms/common/util/p;

    invoke-interface {v4}, Lcom/google/android/gms/common/util/p;->b()J

    move-result-wide v4

    const-wide/32 v6, 0xf4240

    mul-long/2addr v4, v6

    move-object/from16 v0, p0

    invoke-virtual {v0, v4, v5}, Lcom/google/android/location/fused/w;->a(J)V

    .line 244
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    .line 246
    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lcom/google/android/location/fused/w;->a(Landroid/content/ContentResolver;)V

    .line 247
    const-string v5, "location_providers_allowed"

    invoke-static {v5}, Landroid/provider/Settings$Secure;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    const/4 v6, 0x1

    new-instance v7, Lcom/google/android/location/fused/y;

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/location/fused/w;->g:Landroid/os/Handler;

    move-object/from16 v0, p0

    invoke-direct {v7, v0, v8, v4}, Lcom/google/android/location/fused/y;-><init>(Lcom/google/android/location/fused/w;Landroid/os/Handler;Landroid/content/ContentResolver;)V

    invoke-virtual {v4, v5, v6, v7}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 256
    move-object/from16 v0, p7

    move-object/from16 v1, p0

    invoke-virtual {v0, v1}, Lcom/google/android/location/fused/az;->a(Lcom/google/android/location/fused/bb;)V

    .line 257
    return-void
.end method

.method constructor <init>(Landroid/content/Context;Landroid/os/Looper;Lcom/google/android/location/fused/aq;)V
    .locals 10

    .prologue
    .line 143
    const-string v0, "location"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/location/LocationManager;

    new-instance v4, Lcom/google/android/location/fused/ar;

    invoke-direct {v4, p1}, Lcom/google/android/location/fused/ar;-><init>(Landroid/content/Context;)V

    const-string v0, "sensor"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/hardware/SensorManager;

    new-instance v6, Lcom/google/android/location/fused/c;

    invoke-direct {v6}, Lcom/google/android/location/fused/c;-><init>()V

    new-instance v7, Lcom/google/android/location/fused/az;

    invoke-direct {v7, p1}, Lcom/google/android/location/fused/az;-><init>(Landroid/content/Context;)V

    invoke-static {}, Lcom/google/android/gms/common/util/r;->c()Lcom/google/android/gms/common/util/p;

    move-result-object v8

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v9, p3

    invoke-direct/range {v0 .. v9}, Lcom/google/android/location/fused/w;-><init>(Landroid/content/Context;Landroid/os/Looper;Landroid/location/LocationManager;Lcom/google/android/location/fused/ar;Landroid/hardware/SensorManager;Lcom/google/android/location/fused/c;Lcom/google/android/location/fused/az;Lcom/google/android/gms/common/util/p;Lcom/google/android/location/fused/aq;)V

    .line 153
    return-void
.end method

.method private a(Landroid/location/Location;Landroid/location/Location;)V
    .locals 4

    .prologue
    .line 388
    if-eqz p1, :cond_0

    .line 389
    new-instance v0, Landroid/location/Location;

    invoke-direct {v0, p1}, Landroid/location/Location;-><init>(Landroid/location/Location;)V

    .line 390
    const-string v1, "fused"

    invoke-virtual {v0, v1}, Landroid/location/Location;->setProvider(Ljava/lang/String;)V

    .line 391
    iget-object v1, p0, Lcom/google/android/location/fused/w;->l:Lcom/google/android/location/fused/ao;

    iget-object v1, v1, Lcom/google/android/location/fused/ao;->b:Landroid/location/Location;

    invoke-static {v1, v0}, Lcom/google/android/location/fused/w;->b(Landroid/location/Location;Landroid/location/Location;)V

    move-object p1, v0

    .line 394
    :cond_0
    if-eqz p2, :cond_1

    .line 395
    new-instance v0, Landroid/location/Location;

    invoke-direct {v0, p2}, Landroid/location/Location;-><init>(Landroid/location/Location;)V

    .line 396
    const-string v1, "fused"

    invoke-virtual {v0, v1}, Landroid/location/Location;->setProvider(Ljava/lang/String;)V

    .line 397
    iget-object v1, p0, Lcom/google/android/location/fused/w;->l:Lcom/google/android/location/fused/ao;

    invoke-virtual {v1}, Lcom/google/android/location/fused/ao;->a()Landroid/location/Location;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/google/android/location/fused/w;->b(Landroid/location/Location;Landroid/location/Location;)V

    move-object p2, v0

    .line 402
    :cond_1
    iget-object v0, p0, Lcom/google/android/location/fused/w;->k:Lcom/google/android/location/fused/ao;

    iput-object p1, v0, Lcom/google/android/location/fused/ao;->b:Landroid/location/Location;

    iput-object p2, v0, Lcom/google/android/location/fused/ao;->c:Landroid/location/Location;

    iget-object v1, v0, Lcom/google/android/location/fused/ao;->a:Lcom/google/android/location/fused/c;

    invoke-static {}, Lcom/google/android/location/fused/c;->a()J

    move-result-wide v2

    iput-wide v2, v0, Lcom/google/android/location/fused/ao;->d:J

    .line 404
    iget-object v0, p0, Lcom/google/android/location/fused/w;->b:Lcom/google/q/a/b/b/t;

    invoke-interface {v0}, Lcom/google/q/a/b/b/t;->c()V

    .line 405
    return-void
.end method

.method private static b(Landroid/location/Location;Landroid/location/Location;)V
    .locals 2

    .prologue
    .line 703
    if-eqz p0, :cond_0

    .line 704
    new-instance v0, Landroid/location/Location;

    invoke-direct {v0, p0}, Landroid/location/Location;-><init>(Landroid/location/Location;)V

    .line 705
    const-string v1, "fused"

    invoke-virtual {v0, v1}, Landroid/location/Location;->setProvider(Ljava/lang/String;)V

    .line 706
    const-string v1, "noGPSLocation"

    invoke-static {p1, v1, v0}, Lcom/google/android/location/n/z;->a(Landroid/location/Location;Ljava/lang/String;Landroid/location/Location;)V

    .line 711
    :cond_0
    return-void
.end method

.method private g()V
    .locals 2

    .prologue
    .line 369
    iget-object v0, p0, Lcom/google/android/location/fused/w;->j:Landroid/location/Location;

    if-eqz v0, :cond_0

    .line 370
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/location/fused/w;->j:Landroid/location/Location;

    .line 371
    iget-object v0, p0, Lcom/google/android/location/fused/w;->l:Lcom/google/android/location/fused/ao;

    iget-object v0, v0, Lcom/google/android/location/fused/ao;->b:Landroid/location/Location;

    iget-object v1, p0, Lcom/google/android/location/fused/w;->l:Lcom/google/android/location/fused/ao;

    invoke-virtual {v1}, Lcom/google/android/location/fused/ao;->a()Landroid/location/Location;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/google/android/location/fused/w;->a(Landroid/location/Location;Landroid/location/Location;)V

    .line 375
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(Z)Landroid/location/Location;
    .locals 3

    .prologue
    .line 312
    const-string v0, "GCoreFlp"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 313
    const-string v0, "getLastLocation() request in engine"

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/google/android/location/fused/ax;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 317
    :cond_0
    if-eqz p1, :cond_2

    .line 318
    iget-object v0, p0, Lcom/google/android/location/fused/w;->k:Lcom/google/android/location/fused/ao;

    invoke-virtual {v0}, Lcom/google/android/location/fused/ao;->a()Landroid/location/Location;

    move-result-object v0

    .line 323
    :goto_0
    if-nez v0, :cond_1

    .line 324
    const-string v1, "GCoreFlp"

    const-string v2, "No location to return for getLastLocation()"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 327
    :cond_1
    return-object v0

    .line 320
    :cond_2
    iget-object v0, p0, Lcom/google/android/location/fused/w;->k:Lcom/google/android/location/fused/ao;

    iget-object v0, v0, Lcom/google/android/location/fused/ao;->b:Landroid/location/Location;

    goto :goto_0
.end method

.method public final a()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 262
    iget-boolean v0, p0, Lcom/google/android/location/fused/w;->o:Z

    if-eqz v0, :cond_1

    .line 273
    :cond_0
    :goto_0
    return-void

    .line 266
    :cond_1
    iget-object v0, p0, Lcom/google/android/location/fused/w;->h:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/gms/common/a/d;->a(Landroid/content/Context;)V

    .line 267
    iput-boolean v3, p0, Lcom/google/android/location/fused/w;->o:Z

    .line 268
    invoke-static {p0}, Lcom/google/android/location/fused/NlpLocationReceiverService;->a(Lcom/google/android/location/fused/w;)V

    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/google/android/location/fused/w;->h:Landroid/content/Context;

    const-class v2, Lcom/google/android/location/fused/NlpLocationReceiverService;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    iget-object v1, p0, Lcom/google/android/location/fused/w;->h:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    move-result-object v0

    if-nez v0, :cond_2

    const-string v0, "GCoreFlp"

    const-string v1, "Unable to start the NLPLocationReceiverService"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 269
    :cond_2
    iget-object v0, p0, Lcom/google/android/location/fused/w;->d:Lcom/google/android/location/fused/ad;

    invoke-virtual {v0}, Lcom/google/android/location/fused/ad;->b()V

    .line 270
    const-string v0, "GCoreFlp"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 271
    const-string v0, "Engine enabled (%s)"

    new-array v1, v3, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/android/location/fused/w;->f:Ljava/lang/String;

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcom/google/android/location/fused/ax;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method final a(J)V
    .locals 3

    .prologue
    .line 603
    iget-object v0, p0, Lcom/google/android/location/fused/w;->d:Lcom/google/android/location/fused/ad;

    iget-object v1, p0, Lcom/google/android/location/fused/w;->b:Lcom/google/q/a/b/b/t;

    invoke-interface {v1, p1, p2}, Lcom/google/q/a/b/b/t;->a(J)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/location/fused/ad;->a(I)V

    .line 604
    return-void
.end method

.method final a(Landroid/content/ContentResolver;)V
    .locals 5

    .prologue
    const/4 v0, 0x1

    const/4 v4, 0x0

    .line 797
    const-string v1, "gps"

    invoke-static {p1, v1}, Landroid/provider/Settings$Secure;->isLocationProviderEnabled(Landroid/content/ContentResolver;Ljava/lang/String;)Z

    move-result v1

    const-string v2, "network"

    invoke-static {p1, v2}, Landroid/provider/Settings$Secure;->isLocationProviderEnabled(Landroid/content/ContentResolver;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3

    const-string v2, "network_location_opt_in"

    const/4 v3, -0x1

    invoke-static {p1, v2, v3}, Lcom/google/android/gsf/e;->b(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v2

    if-ne v0, v2, :cond_3

    :goto_0
    iput-boolean v1, p0, Lcom/google/android/location/fused/w;->q:Z

    iput-boolean v0, p0, Lcom/google/android/location/fused/w;->p:Z

    iget-object v0, p0, Lcom/google/android/location/fused/w;->d:Lcom/google/android/location/fused/ad;

    iget-boolean v1, p0, Lcom/google/android/location/fused/w;->q:Z

    iget-boolean v2, p0, Lcom/google/android/location/fused/w;->p:Z

    invoke-virtual {v0, v1, v2}, Lcom/google/android/location/fused/ad;->b(ZZ)V

    iget-boolean v0, p0, Lcom/google/android/location/fused/w;->q:Z

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/location/fused/w;->g()V

    :cond_0
    iget-boolean v0, p0, Lcom/google/android/location/fused/w;->p:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/location/fused/w;->l:Lcom/google/android/location/fused/ao;

    iget-object v0, v0, Lcom/google/android/location/fused/ao;->b:Landroid/location/Location;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/location/fused/w;->l:Lcom/google/android/location/fused/ao;

    iput-object v4, v0, Lcom/google/android/location/fused/ao;->b:Landroid/location/Location;

    iput-object v4, v0, Lcom/google/android/location/fused/ao;->c:Landroid/location/Location;

    iget-object v0, p0, Lcom/google/android/location/fused/w;->j:Landroid/location/Location;

    invoke-direct {p0, v0, v4}, Lcom/google/android/location/fused/w;->a(Landroid/location/Location;Landroid/location/Location;)V

    :cond_1
    iget-object v0, p0, Lcom/google/android/location/fused/w;->c:Lcom/google/android/location/fused/bf;

    iget-boolean v1, p0, Lcom/google/android/location/fused/w;->p:Z

    invoke-virtual {v0, v1}, Lcom/google/android/location/fused/bf;->a(Z)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lcom/google/android/location/fused/w;->f()V

    .line 801
    :cond_2
    return-void

    .line 797
    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Landroid/location/Location;I)V
    .locals 3

    .prologue
    .line 609
    packed-switch p2, :pswitch_data_0

    .line 615
    const-string v0, "GCoreFlp"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown injection type: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)I

    .line 617
    :goto_0
    return-void

    .line 611
    :pswitch_0
    const-string v0, "gps"

    invoke-virtual {p1, v0}, Landroid/location/Location;->setProvider(Ljava/lang/String;)V

    .line 612
    sget-object v0, Lcom/google/q/a/b/a/e;->d:Lcom/google/q/a/b/a/e;

    invoke-virtual {p0, p1, v0}, Lcom/google/android/location/fused/w;->a(Landroid/location/Location;Lcom/google/q/a/b/a/e;)V

    goto :goto_0

    .line 609
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method final a(Landroid/location/Location;Lcom/google/q/a/b/a/e;)V
    .locals 12

    .prologue
    .line 461
    iget-boolean v0, p0, Lcom/google/android/location/fused/w;->o:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/location/fused/w;->i:Lcom/google/android/location/fused/aq;

    if-nez v0, :cond_1

    .line 578
    :cond_0
    :goto_0
    return-void

    .line 467
    :cond_1
    invoke-virtual {p1}, Landroid/location/Location;->getProvider()Ljava/lang/String;

    move-result-object v0

    .line 468
    invoke-virtual {p1}, Landroid/location/Location;->getTime()J

    move-result-wide v2

    .line 470
    iget-object v1, p0, Lcom/google/android/location/fused/w;->a:Lcom/google/android/location/fused/c;

    invoke-static {p1}, Lcom/google/android/location/fused/c;->a(Landroid/location/Location;)J

    move-result-wide v4

    .line 471
    const-string v1, "network"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_e

    .line 472
    iget-object v0, p0, Lcom/google/android/location/fused/w;->l:Lcom/google/android/location/fused/ao;

    iget-object v0, v0, Lcom/google/android/location/fused/ao;->b:Landroid/location/Location;

    .line 473
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Landroid/location/Location;->getTime()J

    move-result-wide v0

    invoke-virtual {p1}, Landroid/location/Location;->getTime()J

    move-result-wide v6

    cmp-long v0, v0, v6

    if-nez v0, :cond_2

    .line 476
    const-string v0, "GCoreFlp"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 477
    const-string v0, "Dropping duplicate NLP location"

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/google/android/location/fused/ax;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 481
    :cond_2
    const-string v0, "cell"

    invoke-static {p1}, Lcom/google/android/location/n/z;->c(Landroid/location/Location;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 483
    iget-object v0, p0, Lcom/google/android/location/fused/w;->v:Lcom/google/android/location/fused/ac;

    invoke-virtual {v0}, Lcom/google/android/location/fused/ac;->c()V

    .line 488
    :goto_1
    iget-boolean v0, p0, Lcom/google/android/location/fused/w;->p:Z

    if-nez v0, :cond_4

    .line 489
    const-string v0, "GCoreFlp"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 490
    const-string v0, "Dropping NLP location because NLP provider disabled"

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/google/android/location/fused/ax;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 485
    :cond_3
    iget-object v0, p0, Lcom/google/android/location/fused/w;->v:Lcom/google/android/location/fused/ac;

    invoke-virtual {v0}, Lcom/google/android/location/fused/ac;->b()V

    goto :goto_1

    .line 495
    :cond_4
    iget-object v0, p0, Lcom/google/android/location/fused/w;->l:Lcom/google/android/location/fused/ao;

    new-instance v1, Landroid/location/Location;

    invoke-direct {v1, p1}, Landroid/location/Location;-><init>(Landroid/location/Location;)V

    invoke-virtual {v0, v1}, Lcom/google/android/location/fused/ao;->a(Landroid/location/Location;)V

    .line 496
    iput-wide v4, p0, Lcom/google/android/location/fused/w;->m:J

    .line 497
    iget-object v0, p0, Lcom/google/android/location/fused/w;->a:Lcom/google/android/location/fused/c;

    invoke-static {p1}, Lcom/google/android/location/fused/c;->a(Landroid/location/Location;)J

    move-result-wide v6

    invoke-virtual {p1}, Landroid/location/Location;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    if-eqz v0, :cond_c

    const-string v1, "levelId"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v8, "levelNumberE3"

    const v9, 0x7fffffff

    invoke-virtual {v0, v8, v9}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v8

    if-eqz v1, :cond_c

    const v0, 0x7fffffff

    if-eq v8, v0, :cond_b

    new-instance v0, Lcom/google/android/location/fused/al;

    invoke-direct {v0, v1, v8, v6, v7}, Lcom/google/android/location/fused/al;-><init>(Ljava/lang/String;IJ)V

    :goto_2
    iget-object v1, p0, Lcom/google/android/location/fused/w;->r:Lcom/google/android/location/fused/al;

    invoke-static {v0, v1}, Lcom/google/k/a/ad;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_d

    iget v1, p0, Lcom/google/android/location/fused/w;->s:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/google/android/location/fused/w;->s:I

    :goto_3
    iput-object v0, p0, Lcom/google/android/location/fused/w;->r:Lcom/google/android/location/fused/al;

    iget-object v0, p0, Lcom/google/android/location/fused/w;->t:Lcom/google/android/location/fused/al;

    if-eqz v0, :cond_5

    iget v0, p0, Lcom/google/android/location/fused/w;->s:I

    const/4 v1, 0x2

    if-ge v0, v1, :cond_5

    iget-object v0, p0, Lcom/google/android/location/fused/w;->t:Lcom/google/android/location/fused/al;

    iget-wide v0, v0, Lcom/google/android/location/fused/al;->d:J

    sub-long v0, v6, v0

    const-wide v6, 0x4a817c800L

    cmp-long v0, v0, v6

    if-lez v0, :cond_6

    :cond_5
    iget-object v0, p0, Lcom/google/android/location/fused/w;->r:Lcom/google/android/location/fused/al;

    iput-object v0, p0, Lcom/google/android/location/fused/w;->t:Lcom/google/android/location/fused/al;

    .line 500
    :cond_6
    iget-object v0, p0, Lcom/google/android/location/fused/w;->c:Lcom/google/android/location/fused/bf;

    iget-object v1, v0, Lcom/google/android/location/fused/bf;->a:Ljava/lang/Object;

    monitor-enter v1

    const/4 v6, 0x0

    :try_start_0
    iput-boolean v6, v0, Lcom/google/android/location/fused/bf;->b:Z

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-wide v0, v2

    .line 538
    :goto_4
    iget-object v2, p0, Lcom/google/android/location/fused/w;->t:Lcom/google/android/location/fused/al;

    if-eqz v2, :cond_7

    iget-object v2, p0, Lcom/google/android/location/fused/w;->t:Lcom/google/android/location/fused/al;

    iget-wide v2, v2, Lcom/google/android/location/fused/al;->d:J

    sub-long v2, v4, v2

    const-wide v6, 0x9502f9000L

    cmp-long v2, v2, v6

    if-lez v2, :cond_7

    const/4 v2, 0x0

    iput-object v2, p0, Lcom/google/android/location/fused/w;->t:Lcom/google/android/location/fused/al;

    .line 540
    :cond_7
    if-nez p1, :cond_13

    const/4 v2, 0x0

    .line 541
    :goto_5
    iget-object v3, p0, Lcom/google/android/location/fused/w;->b:Lcom/google/q/a/b/b/t;

    invoke-interface {v3, v4, v5, v2}, Lcom/google/q/a/b/b/t;->a(JLcom/google/q/a/b/a/b;)V

    .line 542
    invoke-virtual {p0, v4, v5}, Lcom/google/android/location/fused/w;->a(J)V

    .line 543
    iget-object v2, p0, Lcom/google/android/location/fused/w;->b:Lcom/google/q/a/b/b/t;

    invoke-interface {v2}, Lcom/google/q/a/b/b/t;->a()Lcom/google/q/a/b/a/b;

    move-result-object v6

    .line 544
    const-string v2, "GCoreFlp"

    const/4 v3, 0x3

    invoke-static {v2, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_8

    .line 545
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Filtered position: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v2, v3}, Lcom/google/android/location/fused/ax;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 550
    :cond_8
    if-eqz v6, :cond_1a

    iget-object v2, v6, Lcom/google/q/a/b/a/b;->a:Lcom/google/q/a/b/a/e;

    sget-object v3, Lcom/google/q/a/b/a/e;->b:Lcom/google/q/a/b/a/e;

    if-ne v2, v3, :cond_1a

    iget-object v2, p0, Lcom/google/android/location/fused/w;->t:Lcom/google/android/location/fused/al;

    .line 553
    :goto_6
    const-string v7, "fused"

    if-nez v6, :cond_1b

    const/4 v0, 0x0

    move-object v1, v0

    .line 560
    :goto_7
    if-eqz v1, :cond_a

    .line 563
    iget-object v0, p0, Lcom/google/android/location/fused/w;->l:Lcom/google/android/location/fused/ao;

    iget-object v0, v0, Lcom/google/android/location/fused/ao;->b:Landroid/location/Location;

    invoke-static {v0, v1}, Lcom/google/android/location/fused/w;->b(Landroid/location/Location;Landroid/location/Location;)V

    .line 566
    iget-wide v2, p0, Lcom/google/android/location/fused/w;->m:J

    sub-long v2, v4, v2

    .line 567
    iget-object v0, p0, Lcom/google/android/location/fused/w;->l:Lcom/google/android/location/fused/ao;

    if-eqz v0, :cond_9

    invoke-static {v2, v3}, Ljava/lang/Math;->abs(J)J

    move-result-wide v2

    sget-object v0, Lcom/google/android/location/reporting/service/ab;->n:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    cmp-long v0, v2, v6

    if-gtz v0, :cond_9

    .line 569
    iget-object v0, p0, Lcom/google/android/location/fused/w;->l:Lcom/google/android/location/fused/ao;

    iget-object v0, v0, Lcom/google/android/location/fused/ao;->b:Landroid/location/Location;

    if-eqz v0, :cond_9

    const-string v2, "dbgProtoBuf"

    invoke-static {v0, v2}, Lcom/google/android/location/n/z;->c(Landroid/location/Location;Ljava/lang/String;)[B

    move-result-object v0

    const-string v2, "dbgProtoBuf"

    invoke-static {v1, v2, v0}, Lcom/google/android/location/n/z;->a(Landroid/location/Location;Ljava/lang/String;[B)V

    .line 573
    :cond_9
    invoke-static {p1}, Lcom/google/android/location/n/z;->e(Landroid/location/Location;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 574
    invoke-static {v1}, Lcom/google/android/location/n/z;->f(Landroid/location/Location;)V

    .line 577
    :cond_a
    if-eqz v1, :cond_0

    iget-object v0, p0, Lcom/google/android/location/fused/w;->w:[Landroid/location/Location;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    iget-object v0, p0, Lcom/google/android/location/fused/w;->i:Lcom/google/android/location/fused/aq;

    iget-object v2, p0, Lcom/google/android/location/fused/w;->w:[Landroid/location/Location;

    invoke-interface {v0, v2}, Lcom/google/android/location/fused/aq;->a([Landroid/location/Location;)V

    iget-object v0, p0, Lcom/google/android/location/fused/w;->d:Lcom/google/android/location/fused/ad;

    invoke-virtual {v0, v1}, Lcom/google/android/location/fused/ad;->a(Landroid/location/Location;)V

    iget-object v0, p0, Lcom/google/android/location/fused/w;->k:Lcom/google/android/location/fused/ao;

    invoke-virtual {v0, v1}, Lcom/google/android/location/fused/ao;->a(Landroid/location/Location;)V

    iput-wide v4, p0, Lcom/google/android/location/fused/w;->n:J

    goto/16 :goto_0

    .line 497
    :cond_b
    new-instance v0, Lcom/google/android/location/fused/al;

    invoke-direct {v0, v1, v6, v7}, Lcom/google/android/location/fused/al;-><init>(Ljava/lang/String;J)V

    goto/16 :goto_2

    :cond_c
    const/4 v0, 0x0

    goto/16 :goto_2

    :cond_d
    const/4 v1, 0x1

    iput v1, p0, Lcom/google/android/location/fused/w;->s:I

    goto/16 :goto_3

    .line 500
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 501
    :cond_e
    const-string v1, "gps"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_11

    .line 502
    iget-object v0, p0, Lcom/google/android/location/fused/w;->j:Landroid/location/Location;

    if-eqz v0, :cond_f

    iget-object v0, p0, Lcom/google/android/location/fused/w;->j:Landroid/location/Location;

    invoke-virtual {v0}, Landroid/location/Location;->getTime()J

    move-result-wide v0

    invoke-virtual {p1}, Landroid/location/Location;->getTime()J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-nez v0, :cond_f

    .line 505
    const-string v0, "GCoreFlp"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 506
    const-string v0, "Dropping duplicate GPS location"

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/google/android/location/fused/ax;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 510
    :cond_f
    iget-object v0, p0, Lcom/google/android/location/fused/w;->v:Lcom/google/android/location/fused/ac;

    invoke-virtual {v0}, Lcom/google/android/location/fused/ac;->a()V

    .line 511
    iget-boolean v0, p0, Lcom/google/android/location/fused/w;->q:Z

    if-nez v0, :cond_10

    .line 512
    const-string v0, "GCoreFlp"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 513
    const-string v0, "Dropping GPS location because GPS provider disabled"

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/google/android/location/fused/ax;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 518
    :cond_10
    iget-object v0, p0, Lcom/google/android/location/fused/w;->e:Lcom/google/android/gms/common/util/p;

    invoke-interface {v0}, Lcom/google/android/gms/common/util/p;->a()J

    move-result-wide v0

    .line 519
    iget-object v2, p0, Lcom/google/android/location/fused/w;->u:Lcom/google/android/location/fused/ai;

    invoke-virtual {v2, p1}, Lcom/google/android/location/fused/ai;->a(Landroid/location/Location;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 522
    new-instance v2, Landroid/location/Location;

    invoke-direct {v2, p1}, Landroid/location/Location;-><init>(Landroid/location/Location;)V

    iput-object v2, p0, Lcom/google/android/location/fused/w;->j:Landroid/location/Location;

    goto/16 :goto_4

    .line 524
    :cond_11
    const-string v1, "fused"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_12

    .line 526
    iget-object v0, p0, Lcom/google/android/location/fused/w;->v:Lcom/google/android/location/fused/ac;

    invoke-virtual {v0}, Lcom/google/android/location/fused/ac;->d()V

    .line 530
    :cond_12
    const-string v0, "GCoreFlp"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 531
    const-string v0, "Dropping location from unknown provider"

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/google/android/location/fused/ax;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 540
    :cond_13
    if-nez p2, :cond_14

    invoke-virtual {p1}, Landroid/location/Location;->getProvider()Ljava/lang/String;

    move-result-object v2

    const-string v3, "gps"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_18

    sget-object p2, Lcom/google/q/a/b/a/e;->a:Lcom/google/q/a/b/a/e;

    :cond_14
    :goto_8
    invoke-static {}, Lcom/google/q/a/b/a/b;->e()Lcom/google/q/a/b/a/d;

    move-result-object v2

    invoke-virtual {p1}, Landroid/location/Location;->getLatitude()D

    move-result-wide v6

    invoke-virtual {p1}, Landroid/location/Location;->getLongitude()D

    move-result-wide v8

    invoke-virtual {p1}, Landroid/location/Location;->getAccuracy()F

    move-result v3

    const/high16 v10, 0x447a0000    # 1000.0f

    mul-float/2addr v3, v10

    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    move-result v3

    const-wide v10, 0x416312d000000000L    # 1.0E7

    mul-double/2addr v6, v10

    double-to-int v6, v6

    const-wide v10, 0x416312d000000000L    # 1.0E7

    mul-double/2addr v8, v10

    double-to-int v7, v8

    invoke-virtual {v2, v6, v7, v3}, Lcom/google/q/a/b/a/d;->a(III)Lcom/google/q/a/b/a/d;

    invoke-virtual {v2, p2}, Lcom/google/q/a/b/a/d;->a(Lcom/google/q/a/b/a/e;)Lcom/google/q/a/b/a/d;

    move-result-object v2

    invoke-virtual {p1}, Landroid/location/Location;->hasBearing()Z

    move-result v3

    if-eqz v3, :cond_15

    invoke-virtual {p1}, Landroid/location/Location;->getBearing()F

    move-result v3

    float-to-int v3, v3

    invoke-virtual {v2, v3}, Lcom/google/q/a/b/a/d;->a(I)Lcom/google/q/a/b/a/d;

    :cond_15
    invoke-virtual {p1}, Landroid/location/Location;->hasAltitude()Z

    move-result v3

    if-eqz v3, :cond_16

    invoke-virtual {p1}, Landroid/location/Location;->getAltitude()D

    move-result-wide v6

    invoke-virtual {v2, v6, v7}, Lcom/google/q/a/b/a/d;->a(D)Lcom/google/q/a/b/a/d;

    :cond_16
    invoke-virtual {p1}, Landroid/location/Location;->hasSpeed()Z

    move-result v3

    if-eqz v3, :cond_17

    invoke-virtual {p1}, Landroid/location/Location;->getSpeed()F

    move-result v3

    iput v3, v2, Lcom/google/q/a/b/a/d;->e:F

    iget-byte v3, v2, Lcom/google/q/a/b/a/d;->j:B

    or-int/lit8 v3, v3, 0x4

    int-to-byte v3, v3

    iput-byte v3, v2, Lcom/google/q/a/b/a/d;->j:B

    :cond_17
    invoke-virtual {v2}, Lcom/google/q/a/b/a/d;->a()Lcom/google/q/a/b/a/b;

    move-result-object v2

    goto/16 :goto_5

    :cond_18
    const-string v3, "network"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_19

    sget-object p2, Lcom/google/q/a/b/a/e;->b:Lcom/google/q/a/b/a/e;

    const-string v2, "cell"

    invoke-static {p1}, Lcom/google/android/location/n/z;->c(Landroid/location/Location;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_14

    sget-object p2, Lcom/google/q/a/b/a/e;->c:Lcom/google/q/a/b/a/e;

    goto :goto_8

    :cond_19
    sget-object p2, Lcom/google/q/a/b/a/e;->e:Lcom/google/q/a/b/a/e;

    goto :goto_8

    .line 550
    :cond_1a
    const/4 v2, 0x0

    goto/16 :goto_6

    .line 553
    :cond_1b
    new-instance v3, Landroid/location/Location;

    invoke-direct {v3, v7}, Landroid/location/Location;-><init>(Ljava/lang/String;)V

    iget v7, v6, Lcom/google/q/a/b/a/b;->d:I

    int-to-float v7, v7

    const/high16 v8, 0x447a0000    # 1000.0f

    div-float/2addr v7, v8

    invoke-virtual {v3, v7}, Landroid/location/Location;->setAccuracy(F)V

    invoke-virtual {v6}, Lcom/google/q/a/b/a/b;->c()Z

    move-result v7

    if-eqz v7, :cond_1c

    iget v7, v6, Lcom/google/q/a/b/a/b;->i:I

    int-to-float v7, v7

    invoke-virtual {v3, v7}, Landroid/location/Location;->setBearing(F)V

    :cond_1c
    invoke-virtual {v6}, Lcom/google/q/a/b/a/b;->a()Z

    move-result v7

    if-eqz v7, :cond_1d

    iget v7, v6, Lcom/google/q/a/b/a/b;->e:F

    invoke-virtual {v3, v7}, Landroid/location/Location;->setSpeed(F)V

    :cond_1d
    invoke-virtual {v6}, Lcom/google/q/a/b/a/b;->b()Z

    move-result v7

    if-eqz v7, :cond_1e

    iget-wide v8, v6, Lcom/google/q/a/b/a/b;->f:D

    invoke-virtual {v3, v8, v9}, Landroid/location/Location;->setAltitude(D)V

    :cond_1e
    iget v7, v6, Lcom/google/q/a/b/a/b;->b:I

    int-to-double v8, v7

    const-wide v10, 0x416312d000000000L    # 1.0E7

    div-double/2addr v8, v10

    invoke-virtual {v3, v8, v9}, Landroid/location/Location;->setLatitude(D)V

    iget v7, v6, Lcom/google/q/a/b/a/b;->c:I

    int-to-double v8, v7

    const-wide v10, 0x416312d000000000L    # 1.0E7

    div-double/2addr v8, v10

    invoke-virtual {v3, v8, v9}, Landroid/location/Location;->setLongitude(D)V

    invoke-virtual {v3, v0, v1}, Landroid/location/Location;->setTime(J)V

    const/16 v0, 0x11

    invoke-static {v0}, Lcom/google/android/gms/common/util/ao;->a(I)Z

    move-result v0

    if-eqz v0, :cond_1f

    invoke-virtual {v3, v4, v5}, Landroid/location/Location;->setElapsedRealtimeNanos(J)V

    :cond_1f
    if-eqz v2, :cond_21

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v1, "levelId"

    iget-object v7, v2, Lcom/google/android/location/fused/al;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, v7}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    iget-boolean v1, v2, Lcom/google/android/location/fused/al;->c:Z

    if-eqz v1, :cond_20

    const-string v1, "levelNumberE3"

    iget v2, v2, Lcom/google/android/location/fused/al;->b:I

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    :cond_20
    invoke-virtual {v3, v0}, Landroid/location/Location;->setExtras(Landroid/os/Bundle;)V

    :cond_21
    const/4 v0, 0x0

    sget-object v1, Lcom/google/android/location/fused/an;->a:[I

    iget-object v2, v6, Lcom/google/q/a/b/a/b;->a:Lcom/google/q/a/b/a/e;

    invoke-virtual {v2}, Lcom/google/q/a/b/a/e;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    :goto_9
    invoke-static {v3, v0}, Lcom/google/android/location/n/z;->e(Landroid/location/Location;Ljava/lang/String;)V

    move-object v1, v3

    goto/16 :goto_7

    :pswitch_0
    const-string v0, "gps"

    goto :goto_9

    :pswitch_1
    const-string v0, "wifi"

    goto :goto_9

    :pswitch_2
    const-string v0, "cell"

    goto :goto_9

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final a(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    .locals 7

    .prologue
    .line 666
    iget-object v0, p0, Lcom/google/android/location/fused/w;->k:Lcom/google/android/location/fused/ao;

    iget-object v0, v0, Lcom/google/android/location/fused/ao;->b:Landroid/location/Location;

    .line 667
    iget-object v1, p0, Lcom/google/android/location/fused/w;->k:Lcom/google/android/location/fused/ao;

    invoke-virtual {v1}, Lcom/google/android/location/fused/ao;->a()Landroid/location/Location;

    move-result-object v1

    .line 668
    iget-object v2, p0, Lcom/google/android/location/fused/w;->l:Lcom/google/android/location/fused/ao;

    iget-object v2, v2, Lcom/google/android/location/fused/ao;->b:Landroid/location/Location;

    .line 669
    iget-object v3, p0, Lcom/google/android/location/fused/w;->j:Landroid/location/Location;

    .line 670
    iget-object v4, p0, Lcom/google/android/location/fused/w;->l:Lcom/google/android/location/fused/ao;

    invoke-virtual {v4}, Lcom/google/android/location/fused/ao;->a()Landroid/location/Location;

    move-result-object v4

    .line 672
    iget-object v5, p0, Lcom/google/android/location/fused/w;->d:Lcom/google/android/location/fused/ad;

    invoke-virtual {v5, p2}, Lcom/google/android/location/fused/ad;->a(Ljava/io/PrintWriter;)V

    .line 673
    invoke-virtual {p2}, Ljava/io/PrintWriter;->println()V

    .line 675
    iget-boolean v5, p0, Lcom/google/android/location/fused/w;->o:Z

    if-eqz v5, :cond_0

    .line 676
    const-string v5, "Fused Location Provider Is Enabled"

    invoke-virtual {p2, v5}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 681
    :goto_0
    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Fused "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 682
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v5, "Gps "

    invoke-direct {v0, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 683
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v3, "Network "

    invoke-direct {v0, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 684
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "Fused Coarse Interval "

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 685
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Network Coarse Interval "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 686
    return-void

    .line 678
    :cond_0
    const-string v5, "Fused Location Provider Is Disabled"

    invoke-virtual {p2, v5}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final a(Ljava/util/Collection;Z)V
    .locals 3

    .prologue
    .line 295
    const-string v0, "GCoreFlp"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 296
    const-string v0, "Location requests set in engine: %s"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    invoke-static {v0, v1}, Lcom/google/android/location/fused/ax;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 299
    :cond_0
    iget-object v0, p0, Lcom/google/android/location/fused/w;->d:Lcom/google/android/location/fused/ad;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/location/fused/ad;->a(Ljava/lang/Iterable;Z)V

    .line 300
    return-void
.end method

.method public final a([Landroid/location/Location;)V
    .locals 6

    .prologue
    .line 839
    array-length v0, p1

    if-gtz v0, :cond_0

    .line 856
    :goto_0
    return-void

    .line 842
    :cond_0
    const/4 v0, 0x0

    aget-object v1, p1, v0

    .line 843
    array-length v0, p1

    const/4 v2, 0x1

    if-le v0, v2, :cond_1

    .line 844
    const-string v0, "GCoreFlp"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Expected one location from FLP HAL but got "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    array-length v3, p1

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 845
    array-length v0, p1

    add-int/lit8 v0, v0, -0x1

    aget-object v0, p1, v0

    .line 846
    invoke-virtual {v0}, Landroid/location/Location;->getTime()J

    move-result-wide v2

    invoke-virtual {v1}, Landroid/location/Location;->getTime()J

    move-result-wide v4

    cmp-long v2, v2, v4

    if-lez v2, :cond_1

    .line 851
    :goto_1
    iget-object v1, p0, Lcom/google/android/location/fused/w;->a:Lcom/google/android/location/fused/c;

    iget-object v1, p0, Lcom/google/android/location/fused/w;->a:Lcom/google/android/location/fused/c;

    invoke-static {}, Lcom/google/android/location/fused/c;->a()J

    move-result-wide v2

    invoke-static {v0, v2, v3}, Lcom/google/android/location/fused/c;->a(Landroid/location/Location;J)V

    .line 854
    const-string v1, "gps"

    invoke-virtual {v0, v1}, Landroid/location/Location;->setProvider(Ljava/lang/String;)V

    .line 855
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/google/android/location/fused/w;->a(Landroid/location/Location;Lcom/google/q/a/b/a/e;)V

    goto :goto_0

    :cond_1
    move-object v0, v1

    goto :goto_1
.end method

.method public final b()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 278
    iget-boolean v0, p0, Lcom/google/android/location/fused/w;->o:Z

    if-nez v0, :cond_1

    .line 288
    :cond_0
    :goto_0
    return-void

    .line 282
    :cond_1
    iput-boolean v3, p0, Lcom/google/android/location/fused/w;->o:Z

    .line 283
    const/4 v0, 0x0

    invoke-static {v0}, Lcom/google/android/location/fused/NlpLocationReceiverService;->a(Lcom/google/android/location/fused/w;)V

    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/google/android/location/fused/w;->h:Landroid/content/Context;

    const-class v2, Lcom/google/android/location/fused/NlpLocationReceiverService;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    iget-object v1, p0, Lcom/google/android/location/fused/w;->h:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->stopService(Landroid/content/Intent;)Z

    .line 284
    iget-object v0, p0, Lcom/google/android/location/fused/w;->d:Lcom/google/android/location/fused/ad;

    invoke-virtual {v0}, Lcom/google/android/location/fused/ad;->c()V

    .line 285
    const-string v0, "GCoreFlp"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 286
    const-string v0, "Engine disabled (%s)"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/google/android/location/fused/w;->f:Ljava/lang/String;

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/google/android/location/fused/ax;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public final b(Z)V
    .locals 0

    .prologue
    .line 832
    if-nez p1, :cond_0

    .line 833
    invoke-direct {p0}, Lcom/google/android/location/fused/w;->g()V

    .line 835
    :cond_0
    return-void
.end method

.method public final c()V
    .locals 6

    .prologue
    const/4 v1, 0x1

    .line 337
    iget-object v0, p0, Lcom/google/android/location/fused/w;->e:Lcom/google/android/gms/common/util/p;

    invoke-interface {v0}, Lcom/google/android/gms/common/util/p;->b()J

    move-result-wide v2

    const-wide/32 v4, 0xf4240

    mul-long/2addr v2, v4

    iget-wide v4, p0, Lcom/google/android/location/fused/w;->n:J

    sub-long/2addr v2, v4

    .line 341
    iget-object v0, p0, Lcom/google/android/location/fused/w;->k:Lcom/google/android/location/fused/ao;

    if-eqz v0, :cond_0

    const-wide v4, 0x22ecb25c00L

    cmp-long v0, v2, v4

    if-lez v0, :cond_1

    :cond_0
    move v0, v1

    .line 343
    :goto_0
    iget-object v2, p0, Lcom/google/android/location/fused/w;->d:Lcom/google/android/location/fused/ad;

    invoke-virtual {v2, v1, v0}, Lcom/google/android/location/fused/ad;->a(ZZ)V

    .line 345
    return-void

    .line 341
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final d()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 354
    iget-object v0, p0, Lcom/google/android/location/fused/w;->d:Lcom/google/android/location/fused/ad;

    invoke-virtual {v0, v1, v1}, Lcom/google/android/location/fused/ad;->a(ZZ)V

    .line 357
    return-void
.end method

.method public final e()Lcom/google/android/location/n/k;
    .locals 1

    .prologue
    .line 763
    iget-object v0, p0, Lcom/google/android/location/fused/w;->v:Lcom/google/android/location/fused/ac;

    return-object v0
.end method

.method final f()V
    .locals 2

    .prologue
    .line 822
    iget-object v0, p0, Lcom/google/android/location/fused/w;->i:Lcom/google/android/location/fused/aq;

    iget-object v1, p0, Lcom/google/android/location/fused/w;->c:Lcom/google/android/location/fused/bf;

    invoke-virtual {v1}, Lcom/google/android/location/fused/bf;->b()Lcom/google/android/gms/location/LocationStatus;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/location/fused/aq;->a(Lcom/google/android/gms/location/LocationStatus;)V

    .line 823
    return-void
.end method
