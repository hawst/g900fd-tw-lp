.class final Lcom/google/android/location/copresence/n/l;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/location/copresence/l/ab;


# instance fields
.field final synthetic a:Lcom/google/android/location/copresence/n/f;

.field private final b:Lcom/google/android/gms/location/copresence/internal/i;

.field private final c:Z


# direct methods
.method public constructor <init>(Lcom/google/android/location/copresence/n/f;Lcom/google/android/gms/location/copresence/internal/i;Z)V
    .locals 0

    .prologue
    .line 219
    iput-object p1, p0, Lcom/google/android/location/copresence/n/l;->a:Lcom/google/android/location/copresence/n/f;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 220
    iput-object p2, p0, Lcom/google/android/location/copresence/n/l;->b:Lcom/google/android/gms/location/copresence/internal/i;

    .line 221
    iput-boolean p3, p0, Lcom/google/android/location/copresence/n/l;->c:Z

    .line 222
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/location/copresence/l/aa;I)V
    .locals 3

    .prologue
    .line 248
    :try_start_0
    iget-object v0, p0, Lcom/google/android/location/copresence/n/l;->b:Lcom/google/android/gms/location/copresence/internal/i;

    const/4 v1, 0x7

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/google/android/gms/location/copresence/internal/i;->a(ILcom/google/android/gms/location/copresence/CopresenceSettings;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 252
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public final synthetic a(Lcom/google/android/location/copresence/l/aa;Ljava/lang/Object;)V
    .locals 9

    .prologue
    const/4 v8, 0x2

    const/4 v7, 0x6

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 213
    check-cast p2, Lcom/google/android/gms/location/copresence/CopresenceSettings;

    invoke-interface {p1}, Lcom/google/android/location/copresence/l/aa;->c()Lcom/google/android/location/copresence/a/a;

    move-result-object v3

    iget-boolean v0, p0, Lcom/google/android/location/copresence/n/l;->c:Z

    if-eqz v0, :cond_1

    invoke-virtual {p2}, Lcom/google/android/gms/location/copresence/CopresenceSettings;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p2}, Lcom/google/android/gms/location/copresence/CopresenceSettings;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    move v0, v1

    :goto_0
    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/location/copresence/n/l;->a:Lcom/google/android/location/copresence/n/f;

    invoke-static {v0}, Lcom/google/android/location/copresence/n/f;->a(Lcom/google/android/location/copresence/n/f;)Lcom/google/android/location/copresence/n/m;

    move-result-object v0

    invoke-interface {p1}, Lcom/google/android/location/copresence/l/aa;->c()Lcom/google/android/location/copresence/a/a;

    move-result-object v4

    iget-object v4, v4, Lcom/google/android/location/copresence/a/a;->a:Landroid/accounts/Account;

    invoke-static {v4}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {v0, v4}, Lcom/google/android/location/copresence/n/m;->a(Landroid/accounts/Account;)Lcom/google/android/gms/location/reporting/ReportingState;

    move-result-object v5

    if-nez v5, :cond_2

    invoke-static {v7}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "An error has occurred fetching ReportingState for account \'%s\'"

    new-array v1, v1, [Ljava/lang/Object;

    iget-object v4, v4, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-static {v4}, Lcom/google/android/location/copresence/ag;->f(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v4, "CopresenceUlrClient: "

    invoke-direct {v1, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->e(Ljava/lang/String;)I

    :cond_0
    :goto_1
    iget-object v0, p0, Lcom/google/android/location/copresence/n/l;->a:Lcom/google/android/location/copresence/n/f;

    invoke-static {v0, v3, p2}, Lcom/google/android/location/copresence/n/f;->a(Lcom/google/android/location/copresence/n/f;Lcom/google/android/location/copresence/a/a;Lcom/google/android/gms/location/copresence/CopresenceSettings;)Lcom/google/android/gms/location/copresence/CopresenceSettings;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/location/copresence/n/l;->b:Lcom/google/android/gms/location/copresence/internal/i;

    invoke-static {v1, v2, v0}, Lcom/google/android/location/copresence/r;->a(Lcom/google/android/gms/location/copresence/internal/i;ILcom/google/android/gms/location/copresence/CopresenceSettings;)V

    return-void

    :cond_1
    move v0, v2

    goto :goto_0

    :cond_2
    invoke-virtual {v5}, Lcom/google/android/gms/location/reporting/ReportingState;->h()Z

    move-result v6

    if-nez v6, :cond_4

    invoke-static {v7}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v0

    if-eqz v0, :cond_3

    const-string v0, "Cannot attempt to enable ULR for account \'%s\'. ReportingState: %s"

    new-array v6, v8, [Ljava/lang/Object;

    iget-object v4, v4, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-static {v4}, Lcom/google/android/location/copresence/ag;->f(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v6, v2

    aput-object v5, v6, v1

    invoke-static {v0, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v4, "CopresenceUlrClient: "

    invoke-direct {v1, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->e(Ljava/lang/String;)I

    :cond_3
    invoke-virtual {v5}, Lcom/google/android/gms/location/reporting/ReportingState;->g()I

    goto :goto_1

    :cond_4
    iget-object v0, v0, Lcom/google/android/location/copresence/n/m;->a:Lcom/google/android/gms/location/reporting/i;

    iget-object v0, v0, Lcom/google/android/gms/location/reporting/i;->a:Lcom/google/android/gms/location/reporting/a/f;

    invoke-virtual {v0, v4}, Lcom/google/android/gms/location/reporting/a/f;->c(Landroid/accounts/Account;)I

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {v7}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v5

    if-eqz v5, :cond_0

    const-string v5, "An error occurred trying to opt-in \'%s\' into ULR: %s"

    new-array v6, v8, [Ljava/lang/Object;

    iget-object v4, v4, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-static {v4}, Lcom/google/android/location/copresence/ag;->f(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v6, v2

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v6, v1

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v4, "CopresenceUlrClient: "

    invoke-direct {v1, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->e(Ljava/lang/String;)I

    goto :goto_1
.end method
