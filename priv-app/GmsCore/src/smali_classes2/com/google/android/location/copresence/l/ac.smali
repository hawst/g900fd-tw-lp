.class public abstract Lcom/google/android/location/copresence/l/ac;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/location/copresence/l/aa;


# instance fields
.field final a:Ljava/lang/Object;

.field protected final b:Landroid/content/Context;

.field final c:Lcom/google/protobuf/nano/j;

.field final d:Lcom/google/android/location/copresence/ap;

.field final e:Lcom/google/android/location/copresence/l/ab;

.field final f:Landroid/net/ConnectivityManager;

.field final g:Lcom/google/android/location/copresence/a/a;

.field final h:Landroid/os/WorkSource;

.field final i:Lcom/google/android/location/copresence/f/b;

.field j:Lcom/google/android/location/copresence/l/ai;

.field k:I

.field protected l:Ljava/lang/String;

.field m:Lcom/google/protobuf/nano/j;

.field private final n:Lcom/google/android/location/copresence/l/ad;

.field private final o:Lcom/google/android/location/copresence/l/af;

.field private final p:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/location/copresence/ap;Lcom/google/protobuf/nano/j;Ljava/lang/String;Lcom/google/android/location/copresence/a/a;Landroid/os/WorkSource;Lcom/google/android/location/copresence/l/ab;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 101
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 70
    new-instance v0, Lcom/google/android/location/copresence/l/ad;

    invoke-direct {v0, p0, v1}, Lcom/google/android/location/copresence/l/ad;-><init>(Lcom/google/android/location/copresence/l/ac;B)V

    iput-object v0, p0, Lcom/google/android/location/copresence/l/ac;->n:Lcom/google/android/location/copresence/l/ad;

    .line 71
    new-instance v0, Lcom/google/android/location/copresence/l/af;

    invoke-direct {v0, p0, v1}, Lcom/google/android/location/copresence/l/af;-><init>(Lcom/google/android/location/copresence/l/ac;B)V

    iput-object v0, p0, Lcom/google/android/location/copresence/l/ac;->o:Lcom/google/android/location/copresence/l/af;

    .line 72
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/location/copresence/l/ac;->a:Ljava/lang/Object;

    .line 102
    iput-object p1, p0, Lcom/google/android/location/copresence/l/ac;->b:Landroid/content/Context;

    .line 103
    iput-object p2, p0, Lcom/google/android/location/copresence/l/ac;->d:Lcom/google/android/location/copresence/ap;

    .line 104
    iput-object p3, p0, Lcom/google/android/location/copresence/l/ac;->c:Lcom/google/protobuf/nano/j;

    .line 105
    iput-object p7, p0, Lcom/google/android/location/copresence/l/ac;->e:Lcom/google/android/location/copresence/l/ab;

    .line 106
    iput-object p5, p0, Lcom/google/android/location/copresence/l/ac;->g:Lcom/google/android/location/copresence/a/a;

    .line 107
    iput-object p6, p0, Lcom/google/android/location/copresence/l/ac;->h:Landroid/os/WorkSource;

    .line 108
    invoke-virtual {p4}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/copresence/l/ac;->p:Ljava/lang/String;

    .line 109
    const-string v0, "connectivity"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    iput-object v0, p0, Lcom/google/android/location/copresence/l/ac;->f:Landroid/net/ConnectivityManager;

    .line 112
    invoke-static {}, Lcom/google/android/location/copresence/f/b;->b()Lcom/google/ac/b/c/g;

    move-result-object v0

    iget-object v0, v0, Lcom/google/ac/b/c/g;->e:Lcom/google/ac/b/c/m;

    iget-object v0, v0, Lcom/google/ac/b/c/m;->r:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p0, Lcom/google/android/location/copresence/l/ac;->k:I

    .line 113
    iget-object v0, p0, Lcom/google/android/location/copresence/l/ac;->g:Lcom/google/android/location/copresence/a/a;

    .line 114
    invoke-static {}, Lcom/google/android/location/copresence/f/b;->a()Lcom/google/android/location/copresence/f/b;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/copresence/l/ac;->i:Lcom/google/android/location/copresence/f/b;

    .line 115
    return-void
.end method

.method static a(I)I
    .locals 1

    .prologue
    .line 381
    const/16 v0, 0x1f4

    if-lt p0, v0, :cond_0

    .line 382
    const/4 v0, 0x7

    .line 384
    :goto_0
    return v0

    :cond_0
    const/16 v0, 0xd

    goto :goto_0
.end method

.method private static a(Landroid/content/pm/PackageInfo;)Ljava/lang/String;
    .locals 4

    .prologue
    const/4 v3, 0x6

    const/4 v0, 0x0

    .line 256
    iget-object v1, p0, Landroid/content/pm/PackageInfo;->signatures:[Landroid/content/pm/Signature;

    if-eqz v1, :cond_0

    iget-object v1, p0, Landroid/content/pm/PackageInfo;->signatures:[Landroid/content/pm/Signature;

    array-length v1, v1

    if-nez v1, :cond_1

    .line 273
    :cond_0
    :goto_0
    return-object v0

    .line 259
    :cond_1
    iget-object v1, p0, Landroid/content/pm/PackageInfo;->signatures:[Landroid/content/pm/Signature;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    .line 261
    :try_start_0
    const-string v2, "SHA1"

    invoke-static {v2}, Ljava/security/MessageDigest;->getInstance(Ljava/lang/String;)Ljava/security/MessageDigest;

    move-result-object v2

    invoke-virtual {v1}, Landroid/content/pm/Signature;->toByteArray()[B

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/security/MessageDigest;->digest([B)[B

    move-result-object v1

    .line 262
    if-nez v1, :cond_2

    .line 263
    const/4 v1, 0x6

    invoke-static {v1}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 264
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "ServerTask: Unable to compute digest for  "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/location/copresence/ag;->e(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 269
    :catch_0
    move-exception v1

    .line 270
    invoke-static {v3}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 271
    const-string v2, "ServerTask: No such algorithm "

    invoke-static {v2, v1}, Lcom/google/android/location/copresence/ag;->a(Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 268
    :cond_2
    :try_start_1
    invoke-static {v1}, Lcom/google/android/location/copresence/l/ac;->a([B)Ljava/lang/String;
    :try_end_1
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_1 .. :try_end_1} :catch_0

    move-result-object v0

    goto :goto_0
.end method

.method private static a([B)Ljava/lang/String;
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 278
    new-instance v2, Ljava/lang/StringBuilder;

    array-length v0, p0

    mul-int/lit8 v0, v0, 0x2

    invoke-direct {v2, v0}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 279
    array-length v3, p0

    move v0, v1

    :goto_0
    if-ge v0, v3, :cond_0

    aget-byte v4, p0, v0

    .line 280
    const-string v5, "%02x"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    invoke-static {v4}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v4

    aput-object v4, v6, v1

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 279
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 282
    :cond_0
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private b(Ljava/lang/String;)Lcom/google/ac/b/c/y;
    .locals 4

    .prologue
    .line 235
    new-instance v0, Lcom/google/ac/b/c/y;

    invoke-direct {v0}, Lcom/google/ac/b/c/y;-><init>()V

    .line 236
    iput-object p1, v0, Lcom/google/ac/b/c/y;->a:Ljava/lang/String;

    .line 238
    :try_start_0
    iget-object v1, p0, Lcom/google/android/location/copresence/l/ac;->b:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    const/16 v2, 0x40

    invoke-virtual {v1, p1, v2}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v1

    .line 240
    iget v2, v1, Landroid/content/pm/PackageInfo;->versionCode:I

    int-to-long v2, v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    iput-object v2, v0, Lcom/google/ac/b/c/y;->c:Ljava/lang/Long;

    .line 241
    iget-object v2, v1, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;

    iput-object v2, v0, Lcom/google/ac/b/c/y;->b:Ljava/lang/String;

    .line 242
    invoke-static {v1}, Lcom/google/android/location/copresence/l/ac;->a(Landroid/content/pm/PackageInfo;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/google/ac/b/c/y;->d:Ljava/lang/String;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 248
    :cond_0
    :goto_0
    return-object v0

    .line 244
    :catch_0
    move-exception v1

    const/4 v1, 0x6

    invoke-static {v1}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 245
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "ServerTask: Failed to find package for "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/location/copresence/ag;->e(Ljava/lang/String;)I

    goto :goto_0
.end method


# virtual methods
.method protected final a(Ljava/lang/String;)Lcom/google/ac/b/c/db;
    .locals 10

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 202
    new-instance v4, Lcom/google/ac/b/c/db;

    invoke-direct {v4}, Lcom/google/ac/b/c/db;-><init>()V

    .line 204
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    iput-object v3, v4, Lcom/google/ac/b/c/db;->c:Ljava/lang/Long;

    .line 206
    iget-object v3, p0, Lcom/google/android/location/copresence/l/ac;->b:Landroid/content/Context;

    const-string v5, "copresence_gcm_pref"

    invoke-virtual {v3, v5, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v3

    const-string v5, "copresence_uuid"

    invoke-interface {v3, v5, v0}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v4, Lcom/google/ac/b/c/db;->d:Ljava/lang/String;

    .line 210
    new-instance v3, Lcom/google/ac/b/c/y;

    invoke-direct {v3}, Lcom/google/ac/b/c/y;-><init>()V

    iput-object v3, v4, Lcom/google/ac/b/c/db;->b:Lcom/google/ac/b/c/y;

    .line 211
    iget-object v3, v4, Lcom/google/ac/b/c/db;->b:Lcom/google/ac/b/c/y;

    const-string v5, "com.google.android.gms"

    iput-object v5, v3, Lcom/google/ac/b/c/y;->a:Ljava/lang/String;

    .line 212
    iget-object v3, v4, Lcom/google/ac/b/c/db;->b:Lcom/google/ac/b/c/y;

    invoke-static {}, Lcom/google/android/gms/common/util/ay;->b()I

    move-result v5

    int-to-long v6, v5

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    iput-object v5, v3, Lcom/google/ac/b/c/y;->c:Ljava/lang/Long;

    .line 213
    iget-object v3, v4, Lcom/google/ac/b/c/db;->b:Lcom/google/ac/b/c/y;

    invoke-static {}, Lcom/google/android/gms/common/util/ay;->a()Ljava/lang/String;

    move-result-object v5

    iput-object v5, v3, Lcom/google/ac/b/c/y;->b:Ljava/lang/String;

    .line 215
    if-eqz p1, :cond_0

    .line 216
    invoke-direct {p0, p1}, Lcom/google/android/location/copresence/l/ac;->b(Ljava/lang/String;)Lcom/google/ac/b/c/y;

    move-result-object v3

    iput-object v3, v4, Lcom/google/ac/b/c/db;->a:Lcom/google/ac/b/c/y;

    .line 219
    :cond_0
    new-instance v5, Ljava/util/HashSet;

    invoke-direct {v5}, Ljava/util/HashSet;-><init>()V

    invoke-static {}, Lcom/google/android/location/copresence/f/b;->b()Lcom/google/ac/b/c/g;

    move-result-object v3

    iget-object v3, v3, Lcom/google/ac/b/c/g;->e:Lcom/google/ac/b/c/m;

    iget-object v3, v3, Lcom/google/ac/b/c/m;->w:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_2

    const-string v6, ","

    invoke-virtual {v3, v6}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v6

    array-length v7, v6

    move v3, v2

    :goto_0
    if-ge v3, v7, :cond_2

    aget-object v8, v6, v3

    invoke-virtual {v8}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v9

    if-nez v9, :cond_1

    invoke-interface {v5, v8}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    :cond_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_2
    invoke-interface {v5}, Ljava/util/Set;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_5

    :goto_1
    iput-object v0, v4, Lcom/google/ac/b/c/db;->e:[Ljava/lang/String;

    .line 221
    new-instance v0, Lcom/google/ac/b/c/ad;

    invoke-direct {v0}, Lcom/google/ac/b/c/ad;-><init>()V

    iput-object v0, v4, Lcom/google/ac/b/c/db;->f:Lcom/google/ac/b/c/ad;

    .line 222
    iget-object v0, v4, Lcom/google/ac/b/c/db;->f:Lcom/google/ac/b/c/ad;

    const/4 v3, 0x6

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    iput-object v3, v0, Lcom/google/ac/b/c/ad;->c:Ljava/lang/Integer;

    .line 223
    iget-object v0, v4, Lcom/google/ac/b/c/db;->f:Lcom/google/ac/b/c/ad;

    sget-object v3, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    iput-object v3, v0, Lcom/google/ac/b/c/ad;->a:Ljava/lang/String;

    .line 224
    iget-object v0, v4, Lcom/google/ac/b/c/db;->f:Lcom/google/ac/b/c/ad;

    sget-object v3, Landroid/os/Build;->MODEL:Ljava/lang/String;

    iput-object v3, v0, Lcom/google/ac/b/c/ad;->b:Ljava/lang/String;

    .line 225
    iget-object v0, v4, Lcom/google/ac/b/c/db;->f:Lcom/google/ac/b/c/ad;

    sget-object v3, Landroid/os/Build$VERSION;->RELEASE:Ljava/lang/String;

    iput-object v3, v0, Lcom/google/ac/b/c/ad;->d:Ljava/lang/String;

    .line 226
    iget-object v0, v4, Lcom/google/ac/b/c/db;->f:Lcom/google/ac/b/c/ad;

    sget v3, Landroid/os/Build$VERSION;->SDK_INT:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    iput-object v3, v0, Lcom/google/ac/b/c/ad;->f:Ljava/lang/Integer;

    .line 227
    iget-object v3, v4, Lcom/google/ac/b/c/db;->f:Lcom/google/ac/b/c/ad;

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v5, 0x10

    if-lt v0, v5, :cond_6

    iget-object v0, p0, Lcom/google/android/location/copresence/l/ac;->b:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    const-string v5, "android.hardware.type.television"

    invoke-virtual {v0, v5}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    move v0, v1

    :goto_2
    if-eqz v0, :cond_3

    new-array v0, v1, [I

    aput v1, v0, v2

    iput-object v0, v3, Lcom/google/ac/b/c/ad;->e:[I

    :cond_3
    const/4 v0, 0x3

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v0

    if-eqz v0, :cond_4

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "ServerTask: Feature value: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, v3, Lcom/google/ac/b/c/ad;->e:[I

    invoke-static {v1}, Ljava/util/Arrays;->toString([I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->b(Ljava/lang/String;)I

    .line 229
    :cond_4
    iget-object v0, p0, Lcom/google/android/location/copresence/l/ac;->i:Lcom/google/android/location/copresence/f/b;

    invoke-virtual {v0}, Lcom/google/android/location/copresence/f/b;->c()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v4, Lcom/google/ac/b/c/db;->g:Ljava/lang/String;

    .line 231
    return-object v4

    .line 219
    :cond_5
    invoke-interface {v5}, Ljava/util/Set;->size()I

    move-result v0

    new-array v0, v0, [Ljava/lang/String;

    invoke-interface {v5, v0}, Ljava/util/Set;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    goto/16 :goto_1

    :cond_6
    move v0, v2

    .line 227
    goto :goto_2
.end method

.method protected abstract a(Lcom/google/protobuf/nano/j;)Lcom/google/ac/b/c/dc;
.end method

.method protected a()Lcom/google/android/location/copresence/l/b;
    .locals 2

    .prologue
    .line 194
    new-instance v0, Lcom/google/android/location/copresence/l/b;

    iget-object v1, p0, Lcom/google/android/location/copresence/l/ac;->b:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/google/android/location/copresence/l/b;-><init>(Landroid/content/Context;)V

    return-object v0
.end method

.method protected abstract a(Lcom/google/protobuf/nano/j;Lcom/google/protobuf/nano/j;)Ljava/lang/Object;
.end method

.method final a(Ljava/lang/Exception;I)V
    .locals 3

    .prologue
    .line 341
    const/4 v0, 0x6

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 342
    if-eqz p1, :cond_0

    .line 343
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "ServerTask: Server task Exception:"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/google/android/location/copresence/ag;->a(Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 346
    :cond_0
    iget-object v0, p0, Lcom/google/android/location/copresence/l/ac;->n:Lcom/google/android/location/copresence/l/ad;

    invoke-virtual {v0, p2}, Lcom/google/android/location/copresence/l/ad;->a(I)V

    .line 347
    iget-object v0, p0, Lcom/google/android/location/copresence/l/ac;->d:Lcom/google/android/location/copresence/ap;

    iget-object v1, p0, Lcom/google/android/location/copresence/l/ac;->n:Lcom/google/android/location/copresence/l/ad;

    iget-object v2, p0, Lcom/google/android/location/copresence/l/ac;->h:Landroid/os/WorkSource;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/location/copresence/ap;->a(Ljava/lang/Runnable;Landroid/os/WorkSource;)V

    .line 348
    return-void
.end method

.method protected abstract b(Lcom/google/protobuf/nano/j;)I
.end method

.method protected abstract b()Lcom/google/protobuf/nano/j;
.end method

.method public c()Lcom/google/android/location/copresence/a/a;
    .locals 1

    .prologue
    .line 183
    iget-object v0, p0, Lcom/google/android/location/copresence/l/ac;->g:Lcom/google/android/location/copresence/a/a;

    return-object v0
.end method

.method public d()V
    .locals 14

    .prologue
    const/16 v3, 0x190

    .line 118
    iget-object v0, p0, Lcom/google/android/location/copresence/l/ac;->d:Lcom/google/android/location/copresence/ap;

    invoke-virtual {v0}, Lcom/google/android/location/copresence/ap;->c()V

    .line 119
    new-instance v0, Lcom/google/android/location/copresence/l/ai;

    iget-object v1, p0, Lcom/google/android/location/copresence/l/ac;->b:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/location/copresence/l/ac;->p:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Lcom/google/android/location/copresence/l/ai;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/location/copresence/l/ac;->j:Lcom/google/android/location/copresence/l/ai;

    .line 120
    const/4 v0, 0x2

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 121
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "ServerTask: Running task "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->a(Ljava/lang/String;)I

    .line 124
    :cond_0
    const/4 v4, 0x0

    .line 126
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/location/copresence/l/ac;->b()Lcom/google/protobuf/nano/j;

    move-result-object v4

    .line 127
    iget-object v0, p0, Lcom/google/android/location/copresence/l/ac;->f:Landroid/net/ConnectivityManager;

    invoke-virtual {v0}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v0

    if-nez v0, :cond_4

    :cond_1
    new-instance v0, Ljava/io/IOException;

    const-string v1, "Not connected"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 128
    :catch_0
    move-exception v0

    .line 131
    const/4 v1, 0x6

    invoke-static {v1}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 132
    const-string v1, "ServerTask: Failed due to no internet connectivity"

    invoke-static {v1}, Lcom/google/android/location/copresence/ag;->e(Ljava/lang/String;)I

    .line 134
    :cond_2
    if-eqz v4, :cond_3

    .line 135
    iget-object v1, p0, Lcom/google/android/location/copresence/l/ac;->g:Lcom/google/android/location/copresence/a/a;

    iget-object v1, v1, Lcom/google/android/location/copresence/a/a;->a:Landroid/accounts/Account;

    iget-object v1, v1, Landroid/accounts/Account;->name:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/location/copresence/l/ac;->b:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v4, v3}, Lcom/google/android/location/copresence/b/a;->a(Ljava/lang/String;Ljava/lang/String;Lcom/google/protobuf/nano/j;I)V

    .line 138
    :cond_3
    invoke-static {v3}, Lcom/google/android/location/copresence/l/ac;->a(I)I

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/location/copresence/l/ac;->a(Ljava/lang/Exception;I)V

    .line 178
    :goto_0
    return-void

    .line 142
    :cond_4
    iget-object v0, p0, Lcom/google/android/location/copresence/l/ac;->g:Lcom/google/android/location/copresence/a/a;

    iget-object v0, v0, Lcom/google/android/location/copresence/a/a;->a:Landroid/accounts/Account;

    iget-object v0, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    .line 143
    new-instance v12, Lcom/google/android/gms/common/server/ClientContext;

    invoke-static {}, Landroid/os/Process;->myUid()I

    move-result v1

    iget-object v2, p0, Lcom/google/android/location/copresence/l/ac;->b:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v12, v1, v0, v0, v2}, Lcom/google/android/gms/common/server/ClientContext;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 148
    const/4 v0, 0x1

    new-array v1, v0, [Ljava/lang/String;

    const/4 v2, 0x0

    sget-object v0, Lcom/google/android/location/copresence/k;->d:Lcom/google/android/location/copresence/l;

    invoke-virtual {v0}, Lcom/google/android/location/copresence/l;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    aput-object v0, v1, v2

    invoke-virtual {v12, v1}, Lcom/google/android/gms/common/server/ClientContext;->a([Ljava/lang/String;)V

    .line 151
    invoke-virtual {p0}, Lcom/google/android/location/copresence/l/ac;->a()Lcom/google/android/location/copresence/l/b;

    move-result-object v13

    .line 157
    invoke-virtual {v13, v12}, Lcom/google/android/location/copresence/l/b;->a(Lcom/google/android/gms/common/server/ClientContext;)Ljava/lang/String;

    move-result-object v6

    .line 158
    iget-object v0, p0, Lcom/google/android/location/copresence/l/ac;->j:Lcom/google/android/location/copresence/l/ai;

    iget-object v1, p0, Lcom/google/android/location/copresence/l/ac;->l:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/location/copresence/l/ac;->p:Ljava/lang/String;

    invoke-virtual {v13, v2}, Lcom/google/android/location/copresence/l/b;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/location/copresence/l/ac;->g:Lcom/google/android/location/copresence/a/a;

    invoke-virtual {v13, v12}, Lcom/google/android/location/copresence/l/b;->e(Lcom/google/android/gms/common/server/ClientContext;)Ljava/util/HashMap;

    move-result-object v5

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/location/copresence/l/ai;->a(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/location/copresence/a/a;Lcom/google/protobuf/nano/j;Ljava/util/Map;Ljava/lang/String;)V

    .line 166
    iget-object v7, p0, Lcom/google/android/location/copresence/l/ac;->p:Ljava/lang/String;

    invoke-static {v4}, Lcom/google/protobuf/nano/j;->toByteArray(Lcom/google/protobuf/nano/j;)[B

    move-result-object v8

    iget-object v9, p0, Lcom/google/android/location/copresence/l/ac;->c:Lcom/google/protobuf/nano/j;

    iget-object v10, p0, Lcom/google/android/location/copresence/l/ac;->o:Lcom/google/android/location/copresence/l/af;

    iget-object v11, p0, Lcom/google/android/location/copresence/l/ac;->o:Lcom/google/android/location/copresence/l/af;

    move-object v5, v13

    move-object v6, v12

    invoke-virtual/range {v5 .. v11}, Lcom/google/android/location/copresence/l/b;->a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;[BLjava/lang/Object;Lcom/android/volley/x;Lcom/android/volley/w;)V

    .line 176
    iget-object v1, p0, Lcom/google/android/location/copresence/l/ac;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 177
    :try_start_1
    iput-object v4, p0, Lcom/google/android/location/copresence/l/ac;->m:Lcom/google/protobuf/nano/j;

    .line 178
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method protected final e()Lcom/google/ac/b/c/db;
    .locals 1

    .prologue
    .line 198
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/location/copresence/l/ac;->a(Ljava/lang/String;)Lcom/google/ac/b/c/db;

    move-result-object v0

    return-object v0
.end method
