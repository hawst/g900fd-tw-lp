.class public final Lcom/google/android/location/places/ui/r;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final a:Landroid/support/v4/g/s;

.field final b:Landroid/support/v4/g/s;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 14

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x0

    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 24
    sget v3, Lcom/google/android/gms/c;->f:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v4

    .line 25
    sget v3, Lcom/google/android/gms/h;->dJ:I

    invoke-static {v2, v3}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v5

    .line 26
    sget v3, Lcom/google/android/gms/h;->db:I

    invoke-static {v2, v3}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v6

    .line 27
    new-instance v2, Landroid/support/v4/g/s;

    array-length v3, v4

    invoke-direct {v2, v3}, Landroid/support/v4/g/s;-><init>(I)V

    iput-object v2, p0, Lcom/google/android/location/places/ui/r;->a:Landroid/support/v4/g/s;

    .line 28
    new-instance v2, Landroid/support/v4/g/s;

    array-length v3, v4

    invoke-direct {v2, v3}, Landroid/support/v4/g/s;-><init>(I)V

    iput-object v2, p0, Lcom/google/android/location/places/ui/r;->b:Landroid/support/v4/g/s;

    .line 34
    invoke-virtual {v5}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    int-to-float v2, v2

    array-length v3, v4

    int-to-float v3, v3

    div-float v7, v2, v3

    .line 35
    invoke-virtual {v6}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    int-to-float v2, v2

    array-length v3, v4

    int-to-float v3, v3

    div-float v8, v2, v3

    .line 36
    invoke-virtual {v5}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    int-to-float v9, v2

    .line 37
    invoke-virtual {v6}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    int-to-float v10, v2

    move v2, v0

    move v3, v0

    move v0, v1

    .line 40
    :goto_0
    array-length v11, v4

    if-ge v0, v11, :cond_0

    .line 42
    float-to-int v11, v3

    float-to-int v12, v7

    float-to-int v13, v9

    invoke-static {v5, v11, v1, v12, v13}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIII)Landroid/graphics/Bitmap;

    move-result-object v11

    .line 45
    iget-object v12, p0, Lcom/google/android/location/places/ui/r;->a:Landroid/support/v4/g/s;

    aget-object v13, v4, v0

    invoke-virtual {v12, v13, v11}, Landroid/support/v4/g/s;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 46
    add-float/2addr v3, v7

    .line 48
    float-to-int v11, v2

    float-to-int v12, v8

    float-to-int v13, v10

    invoke-static {v6, v11, v1, v12, v13}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIII)Landroid/graphics/Bitmap;

    move-result-object v11

    .line 50
    iget-object v12, p0, Lcom/google/android/location/places/ui/r;->b:Landroid/support/v4/g/s;

    aget-object v13, v4, v0

    invoke-virtual {v12, v13, v11}, Landroid/support/v4/g/s;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 51
    add-float/2addr v2, v8

    .line 40
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 53
    :cond_0
    return-void
.end method
