.class public final Lcom/google/android/location/l/a/cw;
.super Lcom/google/protobuf/nano/d;
.source "SourceFile"


# instance fields
.field public a:Ljava/lang/Integer;

.field public b:Ljava/lang/Integer;

.field public c:Ljava/lang/Integer;

.field public d:Ljava/lang/Integer;

.field public e:Ljava/lang/Integer;

.field public f:Ljava/lang/Integer;

.field public g:Ljava/lang/Integer;

.field public h:Ljava/lang/Integer;

.field public i:Ljava/lang/Float;

.field public j:Ljava/lang/Float;

.field public k:Ljava/lang/Float;

.field public l:Ljava/lang/Integer;

.field public m:Ljava/lang/Integer;

.field public n:Ljava/lang/Integer;

.field public o:Ljava/lang/Boolean;

.field public p:Ljava/lang/Integer;

.field public q:Ljava/lang/Float;

.field public r:Ljava/lang/Integer;

.field public s:Ljava/lang/Integer;

.field public t:Ljava/lang/Float;

.field public u:Ljava/lang/Float;

.field public v:Ljava/lang/Float;

.field public w:Ljava/lang/Integer;

.field public x:Ljava/lang/Integer;

.field public y:Ljava/lang/Integer;

.field public z:Ljava/lang/Integer;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 101
    invoke-direct {p0}, Lcom/google/protobuf/nano/d;-><init>()V

    .line 102
    iput-object v0, p0, Lcom/google/android/location/l/a/cw;->a:Ljava/lang/Integer;

    iput-object v0, p0, Lcom/google/android/location/l/a/cw;->b:Ljava/lang/Integer;

    iput-object v0, p0, Lcom/google/android/location/l/a/cw;->c:Ljava/lang/Integer;

    iput-object v0, p0, Lcom/google/android/location/l/a/cw;->d:Ljava/lang/Integer;

    iput-object v0, p0, Lcom/google/android/location/l/a/cw;->e:Ljava/lang/Integer;

    iput-object v0, p0, Lcom/google/android/location/l/a/cw;->f:Ljava/lang/Integer;

    iput-object v0, p0, Lcom/google/android/location/l/a/cw;->g:Ljava/lang/Integer;

    iput-object v0, p0, Lcom/google/android/location/l/a/cw;->h:Ljava/lang/Integer;

    iput-object v0, p0, Lcom/google/android/location/l/a/cw;->i:Ljava/lang/Float;

    iput-object v0, p0, Lcom/google/android/location/l/a/cw;->j:Ljava/lang/Float;

    iput-object v0, p0, Lcom/google/android/location/l/a/cw;->k:Ljava/lang/Float;

    iput-object v0, p0, Lcom/google/android/location/l/a/cw;->l:Ljava/lang/Integer;

    iput-object v0, p0, Lcom/google/android/location/l/a/cw;->m:Ljava/lang/Integer;

    iput-object v0, p0, Lcom/google/android/location/l/a/cw;->n:Ljava/lang/Integer;

    iput-object v0, p0, Lcom/google/android/location/l/a/cw;->o:Ljava/lang/Boolean;

    iput-object v0, p0, Lcom/google/android/location/l/a/cw;->p:Ljava/lang/Integer;

    iput-object v0, p0, Lcom/google/android/location/l/a/cw;->q:Ljava/lang/Float;

    iput-object v0, p0, Lcom/google/android/location/l/a/cw;->r:Ljava/lang/Integer;

    iput-object v0, p0, Lcom/google/android/location/l/a/cw;->s:Ljava/lang/Integer;

    iput-object v0, p0, Lcom/google/android/location/l/a/cw;->t:Ljava/lang/Float;

    iput-object v0, p0, Lcom/google/android/location/l/a/cw;->u:Ljava/lang/Float;

    iput-object v0, p0, Lcom/google/android/location/l/a/cw;->v:Ljava/lang/Float;

    iput-object v0, p0, Lcom/google/android/location/l/a/cw;->w:Ljava/lang/Integer;

    iput-object v0, p0, Lcom/google/android/location/l/a/cw;->x:Ljava/lang/Integer;

    iput-object v0, p0, Lcom/google/android/location/l/a/cw;->y:Ljava/lang/Integer;

    iput-object v0, p0, Lcom/google/android/location/l/a/cw;->z:Ljava/lang/Integer;

    iput-object v0, p0, Lcom/google/android/location/l/a/cw;->unknownFieldData:Lcom/google/protobuf/nano/f;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/location/l/a/cw;->cachedSize:I

    .line 103
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 476
    invoke-super {p0}, Lcom/google/protobuf/nano/d;->computeSerializedSize()I

    move-result v0

    .line 477
    iget-object v1, p0, Lcom/google/android/location/l/a/cw;->a:Ljava/lang/Integer;

    if-eqz v1, :cond_0

    .line 478
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/location/l/a/cw;->a:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 481
    :cond_0
    iget-object v1, p0, Lcom/google/android/location/l/a/cw;->b:Ljava/lang/Integer;

    if-eqz v1, :cond_1

    .line 482
    const/16 v1, 0x32

    iget-object v2, p0, Lcom/google/android/location/l/a/cw;->b:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 485
    :cond_1
    iget-object v1, p0, Lcom/google/android/location/l/a/cw;->c:Ljava/lang/Integer;

    if-eqz v1, :cond_2

    .line 486
    const/16 v1, 0x33

    iget-object v2, p0, Lcom/google/android/location/l/a/cw;->c:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 489
    :cond_2
    iget-object v1, p0, Lcom/google/android/location/l/a/cw;->d:Ljava/lang/Integer;

    if-eqz v1, :cond_3

    .line 490
    const/16 v1, 0x34

    iget-object v2, p0, Lcom/google/android/location/l/a/cw;->d:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 493
    :cond_3
    iget-object v1, p0, Lcom/google/android/location/l/a/cw;->e:Ljava/lang/Integer;

    if-eqz v1, :cond_4

    .line 494
    const/16 v1, 0x35

    iget-object v2, p0, Lcom/google/android/location/l/a/cw;->e:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 497
    :cond_4
    iget-object v1, p0, Lcom/google/android/location/l/a/cw;->f:Ljava/lang/Integer;

    if-eqz v1, :cond_5

    .line 498
    const/16 v1, 0x36

    iget-object v2, p0, Lcom/google/android/location/l/a/cw;->f:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 501
    :cond_5
    iget-object v1, p0, Lcom/google/android/location/l/a/cw;->g:Ljava/lang/Integer;

    if-eqz v1, :cond_6

    .line 502
    const/16 v1, 0x37

    iget-object v2, p0, Lcom/google/android/location/l/a/cw;->g:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 505
    :cond_6
    iget-object v1, p0, Lcom/google/android/location/l/a/cw;->h:Ljava/lang/Integer;

    if-eqz v1, :cond_7

    .line 506
    const/16 v1, 0x38

    iget-object v2, p0, Lcom/google/android/location/l/a/cw;->h:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 509
    :cond_7
    iget-object v1, p0, Lcom/google/android/location/l/a/cw;->i:Ljava/lang/Float;

    if-eqz v1, :cond_8

    .line 510
    const/16 v1, 0x39

    iget-object v2, p0, Lcom/google/android/location/l/a/cw;->i:Ljava/lang/Float;

    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    .line 513
    :cond_8
    iget-object v1, p0, Lcom/google/android/location/l/a/cw;->j:Ljava/lang/Float;

    if-eqz v1, :cond_9

    .line 514
    const/16 v1, 0x3a

    iget-object v2, p0, Lcom/google/android/location/l/a/cw;->j:Ljava/lang/Float;

    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    .line 517
    :cond_9
    iget-object v1, p0, Lcom/google/android/location/l/a/cw;->k:Ljava/lang/Float;

    if-eqz v1, :cond_a

    .line 518
    const/16 v1, 0x3b

    iget-object v2, p0, Lcom/google/android/location/l/a/cw;->k:Ljava/lang/Float;

    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    .line 521
    :cond_a
    iget-object v1, p0, Lcom/google/android/location/l/a/cw;->l:Ljava/lang/Integer;

    if-eqz v1, :cond_b

    .line 522
    const/16 v1, 0x3c

    iget-object v2, p0, Lcom/google/android/location/l/a/cw;->l:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 525
    :cond_b
    iget-object v1, p0, Lcom/google/android/location/l/a/cw;->m:Ljava/lang/Integer;

    if-eqz v1, :cond_c

    .line 526
    const/16 v1, 0x3d

    iget-object v2, p0, Lcom/google/android/location/l/a/cw;->m:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 529
    :cond_c
    iget-object v1, p0, Lcom/google/android/location/l/a/cw;->n:Ljava/lang/Integer;

    if-eqz v1, :cond_d

    .line 530
    const/16 v1, 0x3e

    iget-object v2, p0, Lcom/google/android/location/l/a/cw;->n:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 533
    :cond_d
    iget-object v1, p0, Lcom/google/android/location/l/a/cw;->o:Ljava/lang/Boolean;

    if-eqz v1, :cond_e

    .line 534
    const/16 v1, 0x3f

    iget-object v2, p0, Lcom/google/android/location/l/a/cw;->o:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 537
    :cond_e
    iget-object v1, p0, Lcom/google/android/location/l/a/cw;->p:Ljava/lang/Integer;

    if-eqz v1, :cond_f

    .line 538
    const/16 v1, 0x40

    iget-object v2, p0, Lcom/google/android/location/l/a/cw;->p:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 541
    :cond_f
    iget-object v1, p0, Lcom/google/android/location/l/a/cw;->q:Ljava/lang/Float;

    if-eqz v1, :cond_10

    .line 542
    const/16 v1, 0x41

    iget-object v2, p0, Lcom/google/android/location/l/a/cw;->q:Ljava/lang/Float;

    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    .line 545
    :cond_10
    iget-object v1, p0, Lcom/google/android/location/l/a/cw;->r:Ljava/lang/Integer;

    if-eqz v1, :cond_11

    .line 546
    const/16 v1, 0x42

    iget-object v2, p0, Lcom/google/android/location/l/a/cw;->r:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 549
    :cond_11
    iget-object v1, p0, Lcom/google/android/location/l/a/cw;->s:Ljava/lang/Integer;

    if-eqz v1, :cond_12

    .line 550
    const/16 v1, 0x43

    iget-object v2, p0, Lcom/google/android/location/l/a/cw;->s:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 553
    :cond_12
    iget-object v1, p0, Lcom/google/android/location/l/a/cw;->t:Ljava/lang/Float;

    if-eqz v1, :cond_13

    .line 554
    const/16 v1, 0x44

    iget-object v2, p0, Lcom/google/android/location/l/a/cw;->t:Ljava/lang/Float;

    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    .line 557
    :cond_13
    iget-object v1, p0, Lcom/google/android/location/l/a/cw;->u:Ljava/lang/Float;

    if-eqz v1, :cond_14

    .line 558
    const/16 v1, 0x45

    iget-object v2, p0, Lcom/google/android/location/l/a/cw;->u:Ljava/lang/Float;

    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    .line 561
    :cond_14
    iget-object v1, p0, Lcom/google/android/location/l/a/cw;->v:Ljava/lang/Float;

    if-eqz v1, :cond_15

    .line 562
    const/16 v1, 0x46

    iget-object v2, p0, Lcom/google/android/location/l/a/cw;->v:Ljava/lang/Float;

    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    .line 565
    :cond_15
    iget-object v1, p0, Lcom/google/android/location/l/a/cw;->w:Ljava/lang/Integer;

    if-eqz v1, :cond_16

    .line 566
    const/16 v1, 0x47

    iget-object v2, p0, Lcom/google/android/location/l/a/cw;->w:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 569
    :cond_16
    iget-object v1, p0, Lcom/google/android/location/l/a/cw;->x:Ljava/lang/Integer;

    if-eqz v1, :cond_17

    .line 570
    const/16 v1, 0x48

    iget-object v2, p0, Lcom/google/android/location/l/a/cw;->x:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 573
    :cond_17
    iget-object v1, p0, Lcom/google/android/location/l/a/cw;->y:Ljava/lang/Integer;

    if-eqz v1, :cond_18

    .line 574
    const/16 v1, 0x49

    iget-object v2, p0, Lcom/google/android/location/l/a/cw;->y:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 577
    :cond_18
    iget-object v1, p0, Lcom/google/android/location/l/a/cw;->z:Ljava/lang/Integer;

    if-eqz v1, :cond_19

    .line 578
    const/16 v1, 0x4a

    iget-object v2, p0, Lcom/google/android/location/l/a/cw;->z:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 581
    :cond_19
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 139
    if-ne p1, p0, :cond_1

    .line 140
    const/4 v0, 0x1

    .line 328
    :cond_0
    :goto_0
    return v0

    .line 142
    :cond_1
    instance-of v1, p1, Lcom/google/android/location/l/a/cw;

    if-eqz v1, :cond_0

    .line 145
    check-cast p1, Lcom/google/android/location/l/a/cw;

    .line 146
    iget-object v1, p0, Lcom/google/android/location/l/a/cw;->a:Ljava/lang/Integer;

    if-nez v1, :cond_1c

    .line 147
    iget-object v1, p1, Lcom/google/android/location/l/a/cw;->a:Ljava/lang/Integer;

    if-nez v1, :cond_0

    .line 153
    :cond_2
    iget-object v1, p0, Lcom/google/android/location/l/a/cw;->b:Ljava/lang/Integer;

    if-nez v1, :cond_1d

    .line 154
    iget-object v1, p1, Lcom/google/android/location/l/a/cw;->b:Ljava/lang/Integer;

    if-nez v1, :cond_0

    .line 160
    :cond_3
    iget-object v1, p0, Lcom/google/android/location/l/a/cw;->c:Ljava/lang/Integer;

    if-nez v1, :cond_1e

    .line 161
    iget-object v1, p1, Lcom/google/android/location/l/a/cw;->c:Ljava/lang/Integer;

    if-nez v1, :cond_0

    .line 167
    :cond_4
    iget-object v1, p0, Lcom/google/android/location/l/a/cw;->d:Ljava/lang/Integer;

    if-nez v1, :cond_1f

    .line 168
    iget-object v1, p1, Lcom/google/android/location/l/a/cw;->d:Ljava/lang/Integer;

    if-nez v1, :cond_0

    .line 174
    :cond_5
    iget-object v1, p0, Lcom/google/android/location/l/a/cw;->e:Ljava/lang/Integer;

    if-nez v1, :cond_20

    .line 175
    iget-object v1, p1, Lcom/google/android/location/l/a/cw;->e:Ljava/lang/Integer;

    if-nez v1, :cond_0

    .line 181
    :cond_6
    iget-object v1, p0, Lcom/google/android/location/l/a/cw;->f:Ljava/lang/Integer;

    if-nez v1, :cond_21

    .line 182
    iget-object v1, p1, Lcom/google/android/location/l/a/cw;->f:Ljava/lang/Integer;

    if-nez v1, :cond_0

    .line 188
    :cond_7
    iget-object v1, p0, Lcom/google/android/location/l/a/cw;->g:Ljava/lang/Integer;

    if-nez v1, :cond_22

    .line 189
    iget-object v1, p1, Lcom/google/android/location/l/a/cw;->g:Ljava/lang/Integer;

    if-nez v1, :cond_0

    .line 195
    :cond_8
    iget-object v1, p0, Lcom/google/android/location/l/a/cw;->h:Ljava/lang/Integer;

    if-nez v1, :cond_23

    .line 196
    iget-object v1, p1, Lcom/google/android/location/l/a/cw;->h:Ljava/lang/Integer;

    if-nez v1, :cond_0

    .line 202
    :cond_9
    iget-object v1, p0, Lcom/google/android/location/l/a/cw;->i:Ljava/lang/Float;

    if-nez v1, :cond_24

    .line 203
    iget-object v1, p1, Lcom/google/android/location/l/a/cw;->i:Ljava/lang/Float;

    if-nez v1, :cond_0

    .line 209
    :cond_a
    iget-object v1, p0, Lcom/google/android/location/l/a/cw;->j:Ljava/lang/Float;

    if-nez v1, :cond_25

    .line 210
    iget-object v1, p1, Lcom/google/android/location/l/a/cw;->j:Ljava/lang/Float;

    if-nez v1, :cond_0

    .line 216
    :cond_b
    iget-object v1, p0, Lcom/google/android/location/l/a/cw;->k:Ljava/lang/Float;

    if-nez v1, :cond_26

    .line 217
    iget-object v1, p1, Lcom/google/android/location/l/a/cw;->k:Ljava/lang/Float;

    if-nez v1, :cond_0

    .line 223
    :cond_c
    iget-object v1, p0, Lcom/google/android/location/l/a/cw;->l:Ljava/lang/Integer;

    if-nez v1, :cond_27

    .line 224
    iget-object v1, p1, Lcom/google/android/location/l/a/cw;->l:Ljava/lang/Integer;

    if-nez v1, :cond_0

    .line 230
    :cond_d
    iget-object v1, p0, Lcom/google/android/location/l/a/cw;->m:Ljava/lang/Integer;

    if-nez v1, :cond_28

    .line 231
    iget-object v1, p1, Lcom/google/android/location/l/a/cw;->m:Ljava/lang/Integer;

    if-nez v1, :cond_0

    .line 237
    :cond_e
    iget-object v1, p0, Lcom/google/android/location/l/a/cw;->n:Ljava/lang/Integer;

    if-nez v1, :cond_29

    .line 238
    iget-object v1, p1, Lcom/google/android/location/l/a/cw;->n:Ljava/lang/Integer;

    if-nez v1, :cond_0

    .line 244
    :cond_f
    iget-object v1, p0, Lcom/google/android/location/l/a/cw;->o:Ljava/lang/Boolean;

    if-nez v1, :cond_2a

    .line 245
    iget-object v1, p1, Lcom/google/android/location/l/a/cw;->o:Ljava/lang/Boolean;

    if-nez v1, :cond_0

    .line 251
    :cond_10
    iget-object v1, p0, Lcom/google/android/location/l/a/cw;->p:Ljava/lang/Integer;

    if-nez v1, :cond_2b

    .line 252
    iget-object v1, p1, Lcom/google/android/location/l/a/cw;->p:Ljava/lang/Integer;

    if-nez v1, :cond_0

    .line 258
    :cond_11
    iget-object v1, p0, Lcom/google/android/location/l/a/cw;->q:Ljava/lang/Float;

    if-nez v1, :cond_2c

    .line 259
    iget-object v1, p1, Lcom/google/android/location/l/a/cw;->q:Ljava/lang/Float;

    if-nez v1, :cond_0

    .line 265
    :cond_12
    iget-object v1, p0, Lcom/google/android/location/l/a/cw;->r:Ljava/lang/Integer;

    if-nez v1, :cond_2d

    .line 266
    iget-object v1, p1, Lcom/google/android/location/l/a/cw;->r:Ljava/lang/Integer;

    if-nez v1, :cond_0

    .line 272
    :cond_13
    iget-object v1, p0, Lcom/google/android/location/l/a/cw;->s:Ljava/lang/Integer;

    if-nez v1, :cond_2e

    .line 273
    iget-object v1, p1, Lcom/google/android/location/l/a/cw;->s:Ljava/lang/Integer;

    if-nez v1, :cond_0

    .line 279
    :cond_14
    iget-object v1, p0, Lcom/google/android/location/l/a/cw;->t:Ljava/lang/Float;

    if-nez v1, :cond_2f

    .line 280
    iget-object v1, p1, Lcom/google/android/location/l/a/cw;->t:Ljava/lang/Float;

    if-nez v1, :cond_0

    .line 286
    :cond_15
    iget-object v1, p0, Lcom/google/android/location/l/a/cw;->u:Ljava/lang/Float;

    if-nez v1, :cond_30

    .line 287
    iget-object v1, p1, Lcom/google/android/location/l/a/cw;->u:Ljava/lang/Float;

    if-nez v1, :cond_0

    .line 293
    :cond_16
    iget-object v1, p0, Lcom/google/android/location/l/a/cw;->v:Ljava/lang/Float;

    if-nez v1, :cond_31

    .line 294
    iget-object v1, p1, Lcom/google/android/location/l/a/cw;->v:Ljava/lang/Float;

    if-nez v1, :cond_0

    .line 300
    :cond_17
    iget-object v1, p0, Lcom/google/android/location/l/a/cw;->w:Ljava/lang/Integer;

    if-nez v1, :cond_32

    .line 301
    iget-object v1, p1, Lcom/google/android/location/l/a/cw;->w:Ljava/lang/Integer;

    if-nez v1, :cond_0

    .line 307
    :cond_18
    iget-object v1, p0, Lcom/google/android/location/l/a/cw;->x:Ljava/lang/Integer;

    if-nez v1, :cond_33

    .line 308
    iget-object v1, p1, Lcom/google/android/location/l/a/cw;->x:Ljava/lang/Integer;

    if-nez v1, :cond_0

    .line 314
    :cond_19
    iget-object v1, p0, Lcom/google/android/location/l/a/cw;->y:Ljava/lang/Integer;

    if-nez v1, :cond_34

    .line 315
    iget-object v1, p1, Lcom/google/android/location/l/a/cw;->y:Ljava/lang/Integer;

    if-nez v1, :cond_0

    .line 321
    :cond_1a
    iget-object v1, p0, Lcom/google/android/location/l/a/cw;->z:Ljava/lang/Integer;

    if-nez v1, :cond_35

    .line 322
    iget-object v1, p1, Lcom/google/android/location/l/a/cw;->z:Ljava/lang/Integer;

    if-nez v1, :cond_0

    .line 328
    :cond_1b
    invoke-virtual {p0, p1}, Lcom/google/android/location/l/a/cw;->unknownFieldDataEquals(Lcom/google/protobuf/nano/d;)Z

    move-result v0

    goto/16 :goto_0

    .line 150
    :cond_1c
    iget-object v1, p0, Lcom/google/android/location/l/a/cw;->a:Ljava/lang/Integer;

    iget-object v2, p1, Lcom/google/android/location/l/a/cw;->a:Ljava/lang/Integer;

    invoke-virtual {v1, v2}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    goto/16 :goto_0

    .line 157
    :cond_1d
    iget-object v1, p0, Lcom/google/android/location/l/a/cw;->b:Ljava/lang/Integer;

    iget-object v2, p1, Lcom/google/android/location/l/a/cw;->b:Ljava/lang/Integer;

    invoke-virtual {v1, v2}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    goto/16 :goto_0

    .line 164
    :cond_1e
    iget-object v1, p0, Lcom/google/android/location/l/a/cw;->c:Ljava/lang/Integer;

    iget-object v2, p1, Lcom/google/android/location/l/a/cw;->c:Ljava/lang/Integer;

    invoke-virtual {v1, v2}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    goto/16 :goto_0

    .line 171
    :cond_1f
    iget-object v1, p0, Lcom/google/android/location/l/a/cw;->d:Ljava/lang/Integer;

    iget-object v2, p1, Lcom/google/android/location/l/a/cw;->d:Ljava/lang/Integer;

    invoke-virtual {v1, v2}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_5

    goto/16 :goto_0

    .line 178
    :cond_20
    iget-object v1, p0, Lcom/google/android/location/l/a/cw;->e:Ljava/lang/Integer;

    iget-object v2, p1, Lcom/google/android/location/l/a/cw;->e:Ljava/lang/Integer;

    invoke-virtual {v1, v2}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_6

    goto/16 :goto_0

    .line 185
    :cond_21
    iget-object v1, p0, Lcom/google/android/location/l/a/cw;->f:Ljava/lang/Integer;

    iget-object v2, p1, Lcom/google/android/location/l/a/cw;->f:Ljava/lang/Integer;

    invoke-virtual {v1, v2}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_7

    goto/16 :goto_0

    .line 192
    :cond_22
    iget-object v1, p0, Lcom/google/android/location/l/a/cw;->g:Ljava/lang/Integer;

    iget-object v2, p1, Lcom/google/android/location/l/a/cw;->g:Ljava/lang/Integer;

    invoke-virtual {v1, v2}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_8

    goto/16 :goto_0

    .line 199
    :cond_23
    iget-object v1, p0, Lcom/google/android/location/l/a/cw;->h:Ljava/lang/Integer;

    iget-object v2, p1, Lcom/google/android/location/l/a/cw;->h:Ljava/lang/Integer;

    invoke-virtual {v1, v2}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_9

    goto/16 :goto_0

    .line 206
    :cond_24
    iget-object v1, p0, Lcom/google/android/location/l/a/cw;->i:Ljava/lang/Float;

    iget-object v2, p1, Lcom/google/android/location/l/a/cw;->i:Ljava/lang/Float;

    invoke-virtual {v1, v2}, Ljava/lang/Float;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_a

    goto/16 :goto_0

    .line 213
    :cond_25
    iget-object v1, p0, Lcom/google/android/location/l/a/cw;->j:Ljava/lang/Float;

    iget-object v2, p1, Lcom/google/android/location/l/a/cw;->j:Ljava/lang/Float;

    invoke-virtual {v1, v2}, Ljava/lang/Float;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_b

    goto/16 :goto_0

    .line 220
    :cond_26
    iget-object v1, p0, Lcom/google/android/location/l/a/cw;->k:Ljava/lang/Float;

    iget-object v2, p1, Lcom/google/android/location/l/a/cw;->k:Ljava/lang/Float;

    invoke-virtual {v1, v2}, Ljava/lang/Float;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_c

    goto/16 :goto_0

    .line 227
    :cond_27
    iget-object v1, p0, Lcom/google/android/location/l/a/cw;->l:Ljava/lang/Integer;

    iget-object v2, p1, Lcom/google/android/location/l/a/cw;->l:Ljava/lang/Integer;

    invoke-virtual {v1, v2}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_d

    goto/16 :goto_0

    .line 234
    :cond_28
    iget-object v1, p0, Lcom/google/android/location/l/a/cw;->m:Ljava/lang/Integer;

    iget-object v2, p1, Lcom/google/android/location/l/a/cw;->m:Ljava/lang/Integer;

    invoke-virtual {v1, v2}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_e

    goto/16 :goto_0

    .line 241
    :cond_29
    iget-object v1, p0, Lcom/google/android/location/l/a/cw;->n:Ljava/lang/Integer;

    iget-object v2, p1, Lcom/google/android/location/l/a/cw;->n:Ljava/lang/Integer;

    invoke-virtual {v1, v2}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_f

    goto/16 :goto_0

    .line 248
    :cond_2a
    iget-object v1, p0, Lcom/google/android/location/l/a/cw;->o:Ljava/lang/Boolean;

    iget-object v2, p1, Lcom/google/android/location/l/a/cw;->o:Ljava/lang/Boolean;

    invoke-virtual {v1, v2}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_10

    goto/16 :goto_0

    .line 255
    :cond_2b
    iget-object v1, p0, Lcom/google/android/location/l/a/cw;->p:Ljava/lang/Integer;

    iget-object v2, p1, Lcom/google/android/location/l/a/cw;->p:Ljava/lang/Integer;

    invoke-virtual {v1, v2}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_11

    goto/16 :goto_0

    .line 262
    :cond_2c
    iget-object v1, p0, Lcom/google/android/location/l/a/cw;->q:Ljava/lang/Float;

    iget-object v2, p1, Lcom/google/android/location/l/a/cw;->q:Ljava/lang/Float;

    invoke-virtual {v1, v2}, Ljava/lang/Float;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_12

    goto/16 :goto_0

    .line 269
    :cond_2d
    iget-object v1, p0, Lcom/google/android/location/l/a/cw;->r:Ljava/lang/Integer;

    iget-object v2, p1, Lcom/google/android/location/l/a/cw;->r:Ljava/lang/Integer;

    invoke-virtual {v1, v2}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_13

    goto/16 :goto_0

    .line 276
    :cond_2e
    iget-object v1, p0, Lcom/google/android/location/l/a/cw;->s:Ljava/lang/Integer;

    iget-object v2, p1, Lcom/google/android/location/l/a/cw;->s:Ljava/lang/Integer;

    invoke-virtual {v1, v2}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_14

    goto/16 :goto_0

    .line 283
    :cond_2f
    iget-object v1, p0, Lcom/google/android/location/l/a/cw;->t:Ljava/lang/Float;

    iget-object v2, p1, Lcom/google/android/location/l/a/cw;->t:Ljava/lang/Float;

    invoke-virtual {v1, v2}, Ljava/lang/Float;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_15

    goto/16 :goto_0

    .line 290
    :cond_30
    iget-object v1, p0, Lcom/google/android/location/l/a/cw;->u:Ljava/lang/Float;

    iget-object v2, p1, Lcom/google/android/location/l/a/cw;->u:Ljava/lang/Float;

    invoke-virtual {v1, v2}, Ljava/lang/Float;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_16

    goto/16 :goto_0

    .line 297
    :cond_31
    iget-object v1, p0, Lcom/google/android/location/l/a/cw;->v:Ljava/lang/Float;

    iget-object v2, p1, Lcom/google/android/location/l/a/cw;->v:Ljava/lang/Float;

    invoke-virtual {v1, v2}, Ljava/lang/Float;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_17

    goto/16 :goto_0

    .line 304
    :cond_32
    iget-object v1, p0, Lcom/google/android/location/l/a/cw;->w:Ljava/lang/Integer;

    iget-object v2, p1, Lcom/google/android/location/l/a/cw;->w:Ljava/lang/Integer;

    invoke-virtual {v1, v2}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_18

    goto/16 :goto_0

    .line 311
    :cond_33
    iget-object v1, p0, Lcom/google/android/location/l/a/cw;->x:Ljava/lang/Integer;

    iget-object v2, p1, Lcom/google/android/location/l/a/cw;->x:Ljava/lang/Integer;

    invoke-virtual {v1, v2}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_19

    goto/16 :goto_0

    .line 318
    :cond_34
    iget-object v1, p0, Lcom/google/android/location/l/a/cw;->y:Ljava/lang/Integer;

    iget-object v2, p1, Lcom/google/android/location/l/a/cw;->y:Ljava/lang/Integer;

    invoke-virtual {v1, v2}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1a

    goto/16 :goto_0

    .line 325
    :cond_35
    iget-object v1, p0, Lcom/google/android/location/l/a/cw;->z:Ljava/lang/Integer;

    iget-object v2, p1, Lcom/google/android/location/l/a/cw;->z:Ljava/lang/Integer;

    invoke-virtual {v1, v2}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1b

    goto/16 :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 333
    iget-object v0, p0, Lcom/google/android/location/l/a/cw;->a:Ljava/lang/Integer;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    .line 336
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/location/l/a/cw;->b:Ljava/lang/Integer;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    .line 338
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/location/l/a/cw;->c:Ljava/lang/Integer;

    if-nez v0, :cond_2

    move v0, v1

    :goto_2
    add-int/2addr v0, v2

    .line 340
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/location/l/a/cw;->d:Ljava/lang/Integer;

    if-nez v0, :cond_3

    move v0, v1

    :goto_3
    add-int/2addr v0, v2

    .line 342
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/location/l/a/cw;->e:Ljava/lang/Integer;

    if-nez v0, :cond_4

    move v0, v1

    :goto_4
    add-int/2addr v0, v2

    .line 344
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/location/l/a/cw;->f:Ljava/lang/Integer;

    if-nez v0, :cond_5

    move v0, v1

    :goto_5
    add-int/2addr v0, v2

    .line 346
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/location/l/a/cw;->g:Ljava/lang/Integer;

    if-nez v0, :cond_6

    move v0, v1

    :goto_6
    add-int/2addr v0, v2

    .line 348
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/location/l/a/cw;->h:Ljava/lang/Integer;

    if-nez v0, :cond_7

    move v0, v1

    :goto_7
    add-int/2addr v0, v2

    .line 350
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/location/l/a/cw;->i:Ljava/lang/Float;

    if-nez v0, :cond_8

    move v0, v1

    :goto_8
    add-int/2addr v0, v2

    .line 352
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/location/l/a/cw;->j:Ljava/lang/Float;

    if-nez v0, :cond_9

    move v0, v1

    :goto_9
    add-int/2addr v0, v2

    .line 354
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/location/l/a/cw;->k:Ljava/lang/Float;

    if-nez v0, :cond_a

    move v0, v1

    :goto_a
    add-int/2addr v0, v2

    .line 356
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/location/l/a/cw;->l:Ljava/lang/Integer;

    if-nez v0, :cond_b

    move v0, v1

    :goto_b
    add-int/2addr v0, v2

    .line 358
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/location/l/a/cw;->m:Ljava/lang/Integer;

    if-nez v0, :cond_c

    move v0, v1

    :goto_c
    add-int/2addr v0, v2

    .line 360
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/location/l/a/cw;->n:Ljava/lang/Integer;

    if-nez v0, :cond_d

    move v0, v1

    :goto_d
    add-int/2addr v0, v2

    .line 362
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/location/l/a/cw;->o:Ljava/lang/Boolean;

    if-nez v0, :cond_e

    move v0, v1

    :goto_e
    add-int/2addr v0, v2

    .line 364
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/location/l/a/cw;->p:Ljava/lang/Integer;

    if-nez v0, :cond_f

    move v0, v1

    :goto_f
    add-int/2addr v0, v2

    .line 366
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/location/l/a/cw;->q:Ljava/lang/Float;

    if-nez v0, :cond_10

    move v0, v1

    :goto_10
    add-int/2addr v0, v2

    .line 368
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/location/l/a/cw;->r:Ljava/lang/Integer;

    if-nez v0, :cond_11

    move v0, v1

    :goto_11
    add-int/2addr v0, v2

    .line 370
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/location/l/a/cw;->s:Ljava/lang/Integer;

    if-nez v0, :cond_12

    move v0, v1

    :goto_12
    add-int/2addr v0, v2

    .line 372
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/location/l/a/cw;->t:Ljava/lang/Float;

    if-nez v0, :cond_13

    move v0, v1

    :goto_13
    add-int/2addr v0, v2

    .line 374
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/location/l/a/cw;->u:Ljava/lang/Float;

    if-nez v0, :cond_14

    move v0, v1

    :goto_14
    add-int/2addr v0, v2

    .line 376
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/location/l/a/cw;->v:Ljava/lang/Float;

    if-nez v0, :cond_15

    move v0, v1

    :goto_15
    add-int/2addr v0, v2

    .line 378
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/location/l/a/cw;->w:Ljava/lang/Integer;

    if-nez v0, :cond_16

    move v0, v1

    :goto_16
    add-int/2addr v0, v2

    .line 380
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/location/l/a/cw;->x:Ljava/lang/Integer;

    if-nez v0, :cond_17

    move v0, v1

    :goto_17
    add-int/2addr v0, v2

    .line 382
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/location/l/a/cw;->y:Ljava/lang/Integer;

    if-nez v0, :cond_18

    move v0, v1

    :goto_18
    add-int/2addr v0, v2

    .line 384
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/android/location/l/a/cw;->z:Ljava/lang/Integer;

    if-nez v2, :cond_19

    :goto_19
    add-int/2addr v0, v1

    .line 386
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/google/android/location/l/a/cw;->unknownFieldDataHashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 387
    return v0

    .line 333
    :cond_0
    iget-object v0, p0, Lcom/google/android/location/l/a/cw;->a:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->hashCode()I

    move-result v0

    goto/16 :goto_0

    .line 336
    :cond_1
    iget-object v0, p0, Lcom/google/android/location/l/a/cw;->b:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->hashCode()I

    move-result v0

    goto/16 :goto_1

    .line 338
    :cond_2
    iget-object v0, p0, Lcom/google/android/location/l/a/cw;->c:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->hashCode()I

    move-result v0

    goto/16 :goto_2

    .line 340
    :cond_3
    iget-object v0, p0, Lcom/google/android/location/l/a/cw;->d:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->hashCode()I

    move-result v0

    goto/16 :goto_3

    .line 342
    :cond_4
    iget-object v0, p0, Lcom/google/android/location/l/a/cw;->e:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->hashCode()I

    move-result v0

    goto/16 :goto_4

    .line 344
    :cond_5
    iget-object v0, p0, Lcom/google/android/location/l/a/cw;->f:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->hashCode()I

    move-result v0

    goto/16 :goto_5

    .line 346
    :cond_6
    iget-object v0, p0, Lcom/google/android/location/l/a/cw;->g:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->hashCode()I

    move-result v0

    goto/16 :goto_6

    .line 348
    :cond_7
    iget-object v0, p0, Lcom/google/android/location/l/a/cw;->h:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->hashCode()I

    move-result v0

    goto/16 :goto_7

    .line 350
    :cond_8
    iget-object v0, p0, Lcom/google/android/location/l/a/cw;->i:Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->hashCode()I

    move-result v0

    goto/16 :goto_8

    .line 352
    :cond_9
    iget-object v0, p0, Lcom/google/android/location/l/a/cw;->j:Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->hashCode()I

    move-result v0

    goto/16 :goto_9

    .line 354
    :cond_a
    iget-object v0, p0, Lcom/google/android/location/l/a/cw;->k:Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->hashCode()I

    move-result v0

    goto/16 :goto_a

    .line 356
    :cond_b
    iget-object v0, p0, Lcom/google/android/location/l/a/cw;->l:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->hashCode()I

    move-result v0

    goto/16 :goto_b

    .line 358
    :cond_c
    iget-object v0, p0, Lcom/google/android/location/l/a/cw;->m:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->hashCode()I

    move-result v0

    goto/16 :goto_c

    .line 360
    :cond_d
    iget-object v0, p0, Lcom/google/android/location/l/a/cw;->n:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->hashCode()I

    move-result v0

    goto/16 :goto_d

    .line 362
    :cond_e
    iget-object v0, p0, Lcom/google/android/location/l/a/cw;->o:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->hashCode()I

    move-result v0

    goto/16 :goto_e

    .line 364
    :cond_f
    iget-object v0, p0, Lcom/google/android/location/l/a/cw;->p:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->hashCode()I

    move-result v0

    goto/16 :goto_f

    .line 366
    :cond_10
    iget-object v0, p0, Lcom/google/android/location/l/a/cw;->q:Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->hashCode()I

    move-result v0

    goto/16 :goto_10

    .line 368
    :cond_11
    iget-object v0, p0, Lcom/google/android/location/l/a/cw;->r:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->hashCode()I

    move-result v0

    goto/16 :goto_11

    .line 370
    :cond_12
    iget-object v0, p0, Lcom/google/android/location/l/a/cw;->s:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->hashCode()I

    move-result v0

    goto/16 :goto_12

    .line 372
    :cond_13
    iget-object v0, p0, Lcom/google/android/location/l/a/cw;->t:Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->hashCode()I

    move-result v0

    goto/16 :goto_13

    .line 374
    :cond_14
    iget-object v0, p0, Lcom/google/android/location/l/a/cw;->u:Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->hashCode()I

    move-result v0

    goto/16 :goto_14

    .line 376
    :cond_15
    iget-object v0, p0, Lcom/google/android/location/l/a/cw;->v:Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->hashCode()I

    move-result v0

    goto/16 :goto_15

    .line 378
    :cond_16
    iget-object v0, p0, Lcom/google/android/location/l/a/cw;->w:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->hashCode()I

    move-result v0

    goto/16 :goto_16

    .line 380
    :cond_17
    iget-object v0, p0, Lcom/google/android/location/l/a/cw;->x:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->hashCode()I

    move-result v0

    goto/16 :goto_17

    .line 382
    :cond_18
    iget-object v0, p0, Lcom/google/android/location/l/a/cw;->y:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->hashCode()I

    move-result v0

    goto/16 :goto_18

    .line 384
    :cond_19
    iget-object v1, p0, Lcom/google/android/location/l/a/cw;->z:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->hashCode()I

    move-result v1

    goto/16 :goto_19
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 5
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/android/location/l/a/cw;->storeUnknownField(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/l/a/cw;->a:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/l/a/cw;->b:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/l/a/cw;->c:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/l/a/cw;->d:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/l/a/cw;->e:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/l/a/cw;->f:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_7
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/l/a/cw;->g:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_8
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/l/a/cw;->h:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_9
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->k()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->intBitsToFloat(I)F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/l/a/cw;->i:Ljava/lang/Float;

    goto :goto_0

    :sswitch_a
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->k()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->intBitsToFloat(I)F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/l/a/cw;->j:Ljava/lang/Float;

    goto/16 :goto_0

    :sswitch_b
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->k()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->intBitsToFloat(I)F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/l/a/cw;->k:Ljava/lang/Float;

    goto/16 :goto_0

    :sswitch_c
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/l/a/cw;->l:Ljava/lang/Integer;

    goto/16 :goto_0

    :sswitch_d
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/l/a/cw;->m:Ljava/lang/Integer;

    goto/16 :goto_0

    :sswitch_e
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/l/a/cw;->n:Ljava/lang/Integer;

    goto/16 :goto_0

    :sswitch_f
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/l/a/cw;->o:Ljava/lang/Boolean;

    goto/16 :goto_0

    :sswitch_10
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/l/a/cw;->p:Ljava/lang/Integer;

    goto/16 :goto_0

    :sswitch_11
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->k()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->intBitsToFloat(I)F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/l/a/cw;->q:Ljava/lang/Float;

    goto/16 :goto_0

    :sswitch_12
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/l/a/cw;->r:Ljava/lang/Integer;

    goto/16 :goto_0

    :sswitch_13
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/l/a/cw;->s:Ljava/lang/Integer;

    goto/16 :goto_0

    :sswitch_14
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->k()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->intBitsToFloat(I)F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/l/a/cw;->t:Ljava/lang/Float;

    goto/16 :goto_0

    :sswitch_15
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->k()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->intBitsToFloat(I)F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/l/a/cw;->u:Ljava/lang/Float;

    goto/16 :goto_0

    :sswitch_16
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->k()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->intBitsToFloat(I)F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/l/a/cw;->v:Ljava/lang/Float;

    goto/16 :goto_0

    :sswitch_17
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/l/a/cw;->w:Ljava/lang/Integer;

    goto/16 :goto_0

    :sswitch_18
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/l/a/cw;->x:Ljava/lang/Integer;

    goto/16 :goto_0

    :sswitch_19
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/l/a/cw;->y:Ljava/lang/Integer;

    goto/16 :goto_0

    :sswitch_1a
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/l/a/cw;->z:Ljava/lang/Integer;

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x190 -> :sswitch_2
        0x198 -> :sswitch_3
        0x1a0 -> :sswitch_4
        0x1a8 -> :sswitch_5
        0x1b0 -> :sswitch_6
        0x1b8 -> :sswitch_7
        0x1c0 -> :sswitch_8
        0x1cd -> :sswitch_9
        0x1d5 -> :sswitch_a
        0x1dd -> :sswitch_b
        0x1e0 -> :sswitch_c
        0x1e8 -> :sswitch_d
        0x1f0 -> :sswitch_e
        0x1f8 -> :sswitch_f
        0x200 -> :sswitch_10
        0x20d -> :sswitch_11
        0x210 -> :sswitch_12
        0x218 -> :sswitch_13
        0x225 -> :sswitch_14
        0x22d -> :sswitch_15
        0x235 -> :sswitch_16
        0x238 -> :sswitch_17
        0x240 -> :sswitch_18
        0x248 -> :sswitch_19
        0x250 -> :sswitch_1a
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 393
    iget-object v0, p0, Lcom/google/android/location/l/a/cw;->a:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 394
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/location/l/a/cw;->a:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 396
    :cond_0
    iget-object v0, p0, Lcom/google/android/location/l/a/cw;->b:Ljava/lang/Integer;

    if-eqz v0, :cond_1

    .line 397
    const/16 v0, 0x32

    iget-object v1, p0, Lcom/google/android/location/l/a/cw;->b:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 399
    :cond_1
    iget-object v0, p0, Lcom/google/android/location/l/a/cw;->c:Ljava/lang/Integer;

    if-eqz v0, :cond_2

    .line 400
    const/16 v0, 0x33

    iget-object v1, p0, Lcom/google/android/location/l/a/cw;->c:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 402
    :cond_2
    iget-object v0, p0, Lcom/google/android/location/l/a/cw;->d:Ljava/lang/Integer;

    if-eqz v0, :cond_3

    .line 403
    const/16 v0, 0x34

    iget-object v1, p0, Lcom/google/android/location/l/a/cw;->d:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 405
    :cond_3
    iget-object v0, p0, Lcom/google/android/location/l/a/cw;->e:Ljava/lang/Integer;

    if-eqz v0, :cond_4

    .line 406
    const/16 v0, 0x35

    iget-object v1, p0, Lcom/google/android/location/l/a/cw;->e:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 408
    :cond_4
    iget-object v0, p0, Lcom/google/android/location/l/a/cw;->f:Ljava/lang/Integer;

    if-eqz v0, :cond_5

    .line 409
    const/16 v0, 0x36

    iget-object v1, p0, Lcom/google/android/location/l/a/cw;->f:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 411
    :cond_5
    iget-object v0, p0, Lcom/google/android/location/l/a/cw;->g:Ljava/lang/Integer;

    if-eqz v0, :cond_6

    .line 412
    const/16 v0, 0x37

    iget-object v1, p0, Lcom/google/android/location/l/a/cw;->g:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 414
    :cond_6
    iget-object v0, p0, Lcom/google/android/location/l/a/cw;->h:Ljava/lang/Integer;

    if-eqz v0, :cond_7

    .line 415
    const/16 v0, 0x38

    iget-object v1, p0, Lcom/google/android/location/l/a/cw;->h:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 417
    :cond_7
    iget-object v0, p0, Lcom/google/android/location/l/a/cw;->i:Ljava/lang/Float;

    if-eqz v0, :cond_8

    .line 418
    const/16 v0, 0x39

    iget-object v1, p0, Lcom/google/android/location/l/a/cw;->i:Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IF)V

    .line 420
    :cond_8
    iget-object v0, p0, Lcom/google/android/location/l/a/cw;->j:Ljava/lang/Float;

    if-eqz v0, :cond_9

    .line 421
    const/16 v0, 0x3a

    iget-object v1, p0, Lcom/google/android/location/l/a/cw;->j:Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IF)V

    .line 423
    :cond_9
    iget-object v0, p0, Lcom/google/android/location/l/a/cw;->k:Ljava/lang/Float;

    if-eqz v0, :cond_a

    .line 424
    const/16 v0, 0x3b

    iget-object v1, p0, Lcom/google/android/location/l/a/cw;->k:Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IF)V

    .line 426
    :cond_a
    iget-object v0, p0, Lcom/google/android/location/l/a/cw;->l:Ljava/lang/Integer;

    if-eqz v0, :cond_b

    .line 427
    const/16 v0, 0x3c

    iget-object v1, p0, Lcom/google/android/location/l/a/cw;->l:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 429
    :cond_b
    iget-object v0, p0, Lcom/google/android/location/l/a/cw;->m:Ljava/lang/Integer;

    if-eqz v0, :cond_c

    .line 430
    const/16 v0, 0x3d

    iget-object v1, p0, Lcom/google/android/location/l/a/cw;->m:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 432
    :cond_c
    iget-object v0, p0, Lcom/google/android/location/l/a/cw;->n:Ljava/lang/Integer;

    if-eqz v0, :cond_d

    .line 433
    const/16 v0, 0x3e

    iget-object v1, p0, Lcom/google/android/location/l/a/cw;->n:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 435
    :cond_d
    iget-object v0, p0, Lcom/google/android/location/l/a/cw;->o:Ljava/lang/Boolean;

    if-eqz v0, :cond_e

    .line 436
    const/16 v0, 0x3f

    iget-object v1, p0, Lcom/google/android/location/l/a/cw;->o:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 438
    :cond_e
    iget-object v0, p0, Lcom/google/android/location/l/a/cw;->p:Ljava/lang/Integer;

    if-eqz v0, :cond_f

    .line 439
    const/16 v0, 0x40

    iget-object v1, p0, Lcom/google/android/location/l/a/cw;->p:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 441
    :cond_f
    iget-object v0, p0, Lcom/google/android/location/l/a/cw;->q:Ljava/lang/Float;

    if-eqz v0, :cond_10

    .line 442
    const/16 v0, 0x41

    iget-object v1, p0, Lcom/google/android/location/l/a/cw;->q:Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IF)V

    .line 444
    :cond_10
    iget-object v0, p0, Lcom/google/android/location/l/a/cw;->r:Ljava/lang/Integer;

    if-eqz v0, :cond_11

    .line 445
    const/16 v0, 0x42

    iget-object v1, p0, Lcom/google/android/location/l/a/cw;->r:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 447
    :cond_11
    iget-object v0, p0, Lcom/google/android/location/l/a/cw;->s:Ljava/lang/Integer;

    if-eqz v0, :cond_12

    .line 448
    const/16 v0, 0x43

    iget-object v1, p0, Lcom/google/android/location/l/a/cw;->s:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 450
    :cond_12
    iget-object v0, p0, Lcom/google/android/location/l/a/cw;->t:Ljava/lang/Float;

    if-eqz v0, :cond_13

    .line 451
    const/16 v0, 0x44

    iget-object v1, p0, Lcom/google/android/location/l/a/cw;->t:Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IF)V

    .line 453
    :cond_13
    iget-object v0, p0, Lcom/google/android/location/l/a/cw;->u:Ljava/lang/Float;

    if-eqz v0, :cond_14

    .line 454
    const/16 v0, 0x45

    iget-object v1, p0, Lcom/google/android/location/l/a/cw;->u:Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IF)V

    .line 456
    :cond_14
    iget-object v0, p0, Lcom/google/android/location/l/a/cw;->v:Ljava/lang/Float;

    if-eqz v0, :cond_15

    .line 457
    const/16 v0, 0x46

    iget-object v1, p0, Lcom/google/android/location/l/a/cw;->v:Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IF)V

    .line 459
    :cond_15
    iget-object v0, p0, Lcom/google/android/location/l/a/cw;->w:Ljava/lang/Integer;

    if-eqz v0, :cond_16

    .line 460
    const/16 v0, 0x47

    iget-object v1, p0, Lcom/google/android/location/l/a/cw;->w:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 462
    :cond_16
    iget-object v0, p0, Lcom/google/android/location/l/a/cw;->x:Ljava/lang/Integer;

    if-eqz v0, :cond_17

    .line 463
    const/16 v0, 0x48

    iget-object v1, p0, Lcom/google/android/location/l/a/cw;->x:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 465
    :cond_17
    iget-object v0, p0, Lcom/google/android/location/l/a/cw;->y:Ljava/lang/Integer;

    if-eqz v0, :cond_18

    .line 466
    const/16 v0, 0x49

    iget-object v1, p0, Lcom/google/android/location/l/a/cw;->y:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 468
    :cond_18
    iget-object v0, p0, Lcom/google/android/location/l/a/cw;->z:Ljava/lang/Integer;

    if-eqz v0, :cond_19

    .line 469
    const/16 v0, 0x4a

    iget-object v1, p0, Lcom/google/android/location/l/a/cw;->z:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 471
    :cond_19
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/d;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 472
    return-void
.end method
