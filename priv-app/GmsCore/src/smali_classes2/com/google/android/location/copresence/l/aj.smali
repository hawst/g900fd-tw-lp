.class public final Lcom/google/android/location/copresence/l/aj;
.super Lcom/google/android/location/copresence/l/ac;
.source "SourceFile"


# instance fields
.field private final n:Lcom/google/android/gms/location/copresence/CopresenceSettings;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/location/copresence/ap;Lcom/google/android/location/copresence/a/a;Lcom/google/android/gms/location/copresence/CopresenceSettings;Lcom/google/android/location/copresence/l/ab;)V
    .locals 8

    .prologue
    .line 36
    new-instance v3, Lcom/google/ac/b/c/de;

    invoke-direct {v3}, Lcom/google/ac/b/c/de;-><init>()V

    const-string v4, "UpdateSettings"

    const/4 v6, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v5, p3

    move-object v7, p5

    invoke-direct/range {v0 .. v7}, Lcom/google/android/location/copresence/l/ac;-><init>(Landroid/content/Context;Lcom/google/android/location/copresence/ap;Lcom/google/protobuf/nano/j;Ljava/lang/String;Lcom/google/android/location/copresence/a/a;Landroid/os/WorkSource;Lcom/google/android/location/copresence/l/ab;)V

    .line 45
    iput-object p4, p0, Lcom/google/android/location/copresence/l/aj;->n:Lcom/google/android/gms/location/copresence/CopresenceSettings;

    .line 46
    return-void
.end method


# virtual methods
.method protected final bridge synthetic a(Lcom/google/protobuf/nano/j;)Lcom/google/ac/b/c/dc;
    .locals 1

    .prologue
    .line 25
    check-cast p1, Lcom/google/ac/b/c/de;

    if-eqz p1, :cond_0

    iget-object v0, p1, Lcom/google/ac/b/c/de;->a:Lcom/google/ac/b/c/dc;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected final bridge synthetic a(Lcom/google/protobuf/nano/j;Lcom/google/protobuf/nano/j;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 25
    check-cast p2, Lcom/google/ac/b/c/de;

    iget-object v0, p2, Lcom/google/ac/b/c/de;->b:Lcom/google/ac/b/c/bi;

    invoke-static {v0}, Lcom/google/android/location/copresence/l/i;->a(Lcom/google/ac/b/c/bi;)Lcom/google/android/gms/location/copresence/CopresenceSettings;

    move-result-object v0

    return-object v0
.end method

.method protected final synthetic b(Lcom/google/protobuf/nano/j;)I
    .locals 1

    .prologue
    .line 25
    check-cast p1, Lcom/google/ac/b/c/de;

    if-eqz p1, :cond_0

    iget-object v0, p1, Lcom/google/ac/b/c/de;->a:Lcom/google/ac/b/c/dc;

    if-eqz v0, :cond_0

    iget-object v0, p1, Lcom/google/ac/b/c/de;->a:Lcom/google/ac/b/c/dc;

    iget-object v0, v0, Lcom/google/ac/b/c/dc;->c:Lcom/google/ac/b/c/bl;

    if-eqz v0, :cond_0

    iget-object v0, p1, Lcom/google/ac/b/c/de;->a:Lcom/google/ac/b/c/dc;

    iget-object v0, v0, Lcom/google/ac/b/c/dc;->c:Lcom/google/ac/b/c/bl;

    iget-object v0, v0, Lcom/google/ac/b/c/bl;->a:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    iget-object v0, p1, Lcom/google/ac/b/c/de;->a:Lcom/google/ac/b/c/dc;

    iget-object v0, v0, Lcom/google/ac/b/c/dc;->c:Lcom/google/ac/b/c/bl;

    iget-object v0, v0, Lcom/google/ac/b/c/bl;->a:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method protected final synthetic b()Lcom/google/protobuf/nano/j;
    .locals 10

    .prologue
    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 25
    new-instance v4, Lcom/google/ac/b/c/dd;

    invoke-direct {v4}, Lcom/google/ac/b/c/dd;-><init>()V

    invoke-virtual {p0}, Lcom/google/android/location/copresence/l/aj;->e()Lcom/google/ac/b/c/db;

    move-result-object v0

    iput-object v0, v4, Lcom/google/ac/b/c/dd;->a:Lcom/google/ac/b/c/db;

    invoke-static {}, Lcom/google/android/location/copresence/l/i;->aw_()Lcom/google/ac/b/c/bj;

    move-result-object v0

    iput-object v0, v4, Lcom/google/ac/b/c/dd;->c:Lcom/google/ac/b/c/bj;

    new-instance v0, Lcom/google/ac/b/c/bk;

    invoke-direct {v0}, Lcom/google/ac/b/c/bk;-><init>()V

    iput-object v0, v4, Lcom/google/ac/b/c/dd;->d:Lcom/google/ac/b/c/bk;

    iget-object v0, p0, Lcom/google/android/location/copresence/l/aj;->n:Lcom/google/android/gms/location/copresence/CopresenceSettings;

    invoke-virtual {v0}, Lcom/google/android/gms/location/copresence/CopresenceSettings;->c()[Lcom/google/android/gms/location/copresence/internal/CopresenceNamedAcl;

    move-result-object v2

    if-eqz v2, :cond_1

    iget-object v0, v4, Lcom/google/ac/b/c/dd;->c:Lcom/google/ac/b/c/bj;

    iget-object v5, v0, Lcom/google/ac/b/c/bj;->c:Lcom/google/ac/b/c/ay;

    invoke-static {v2}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    array-length v6, v2

    new-array v7, v6, [Lcom/google/ac/b/c/ax;

    move v0, v1

    :goto_0
    if-ge v0, v6, :cond_0

    new-instance v8, Lcom/google/ac/b/c/ax;

    invoke-direct {v8}, Lcom/google/ac/b/c/ax;-><init>()V

    aget-object v9, v2, v0

    invoke-virtual {v9}, Lcom/google/android/gms/location/copresence/internal/CopresenceNamedAcl;->a()Ljava/lang/String;

    move-result-object v9

    iput-object v9, v8, Lcom/google/ac/b/c/ax;->a:Ljava/lang/String;

    aput-object v8, v7, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    iput-object v7, v5, Lcom/google/ac/b/c/ay;->a:[Lcom/google/ac/b/c/ax;

    invoke-static {v2}, Lcom/google/android/location/copresence/l/l;->a([Lcom/google/android/gms/location/copresence/internal/CopresenceNamedAcl;)[Lcom/google/ac/b/c/aw;

    move-result-object v2

    iget-object v0, v4, Lcom/google/ac/b/c/dd;->d:Lcom/google/ac/b/c/bk;

    array-length v5, v2

    new-array v5, v5, [Lcom/google/ac/b/c/az;

    iput-object v5, v0, Lcom/google/ac/b/c/bk;->b:[Lcom/google/ac/b/c/az;

    move v0, v1

    :goto_1
    array-length v5, v2

    if-ge v0, v5, :cond_1

    new-instance v5, Lcom/google/ac/b/c/az;

    invoke-direct {v5}, Lcom/google/ac/b/c/az;-><init>()V

    aget-object v6, v2, v0

    iget-object v6, v6, Lcom/google/ac/b/c/aw;->a:Lcom/google/ac/b/c/ax;

    iput-object v6, v5, Lcom/google/ac/b/c/az;->a:Lcom/google/ac/b/c/ax;

    aget-object v6, v2, v0

    iget-object v6, v6, Lcom/google/ac/b/c/aw;->b:Lcom/google/ac/b/c/dm;

    iput-object v6, v5, Lcom/google/ac/b/c/az;->b:Lcom/google/ac/b/c/dm;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    iput-object v6, v5, Lcom/google/ac/b/c/az;->c:Ljava/lang/Integer;

    iget-object v6, v4, Lcom/google/ac/b/c/dd;->d:Lcom/google/ac/b/c/bk;

    iget-object v6, v6, Lcom/google/ac/b/c/bk;->b:[Lcom/google/ac/b/c/az;

    aput-object v5, v6, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_1
    iget-object v0, p0, Lcom/google/android/location/copresence/l/aj;->n:Lcom/google/android/gms/location/copresence/CopresenceSettings;

    invoke-virtual {v0}, Lcom/google/android/gms/location/copresence/CopresenceSettings;->b()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, v4, Lcom/google/ac/b/c/dd;->d:Lcom/google/ac/b/c/bk;

    new-instance v2, Lcom/google/ac/b/c/z;

    invoke-direct {v2}, Lcom/google/ac/b/c/z;-><init>()V

    iput-object v2, v0, Lcom/google/ac/b/c/bk;->a:Lcom/google/ac/b/c/z;

    iget-object v0, v4, Lcom/google/ac/b/c/dd;->d:Lcom/google/ac/b/c/bk;

    iget-object v0, v0, Lcom/google/ac/b/c/bk;->a:Lcom/google/ac/b/c/z;

    iget-object v2, p0, Lcom/google/android/location/copresence/l/aj;->n:Lcom/google/android/gms/location/copresence/CopresenceSettings;

    invoke-virtual {v2}, Lcom/google/android/gms/location/copresence/CopresenceSettings;->a()Z

    move-result v2

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    iput-object v2, v0, Lcom/google/ac/b/c/z;->a:Ljava/lang/Boolean;

    iget-object v0, v4, Lcom/google/ac/b/c/dd;->d:Lcom/google/ac/b/c/bk;

    iget-object v0, v0, Lcom/google/ac/b/c/bk;->a:Lcom/google/ac/b/c/z;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iput-object v2, v0, Lcom/google/ac/b/c/z;->b:Ljava/lang/Integer;

    :cond_2
    iget-object v0, p0, Lcom/google/android/location/copresence/l/aj;->n:Lcom/google/android/gms/location/copresence/CopresenceSettings;

    invoke-virtual {v0}, Lcom/google/android/gms/location/copresence/CopresenceSettings;->d()[Lcom/google/android/gms/location/copresence/internal/CopresenceFeatureOptIn;

    move-result-object v5

    if-eqz v5, :cond_5

    array-length v0, v5

    new-array v6, v0, [Lcom/google/ac/b/c/am;

    move v0, v1

    :goto_2
    array-length v2, v5

    if-ge v0, v2, :cond_4

    new-instance v2, Lcom/google/ac/b/c/am;

    invoke-direct {v2}, Lcom/google/ac/b/c/am;-><init>()V

    aput-object v2, v6, v0

    aget-object v2, v6, v0

    new-instance v7, Lcom/google/ac/b/c/al;

    invoke-direct {v7}, Lcom/google/ac/b/c/al;-><init>()V

    iput-object v7, v2, Lcom/google/ac/b/c/am;->a:Lcom/google/ac/b/c/al;

    aget-object v2, v6, v0

    iget-object v2, v2, Lcom/google/ac/b/c/am;->a:Lcom/google/ac/b/c/al;

    aget-object v7, v5, v0

    invoke-virtual {v7}, Lcom/google/android/gms/location/copresence/internal/CopresenceFeatureOptIn;->a()Ljava/lang/String;

    move-result-object v7

    iput-object v7, v2, Lcom/google/ac/b/c/al;->a:Ljava/lang/String;

    aget-object v7, v6, v0

    aget-object v2, v5, v0

    invoke-virtual {v2}, Lcom/google/android/gms/location/copresence/internal/CopresenceFeatureOptIn;->b()Z

    move-result v2

    if-eqz v2, :cond_3

    move v2, v3

    :goto_3
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iput-object v2, v7, Lcom/google/ac/b/c/am;->b:Ljava/lang/Integer;

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_3
    const/4 v2, 0x2

    goto :goto_3

    :cond_4
    iget-object v0, v4, Lcom/google/ac/b/c/dd;->d:Lcom/google/ac/b/c/bk;

    array-length v2, v6

    new-array v2, v2, [Lcom/google/ac/b/c/ao;

    iput-object v2, v0, Lcom/google/ac/b/c/bk;->c:[Lcom/google/ac/b/c/ao;

    :goto_4
    array-length v0, v6

    if-ge v1, v0, :cond_5

    new-instance v0, Lcom/google/ac/b/c/ao;

    invoke-direct {v0}, Lcom/google/ac/b/c/ao;-><init>()V

    aget-object v2, v6, v1

    iget-object v2, v2, Lcom/google/ac/b/c/am;->a:Lcom/google/ac/b/c/al;

    iput-object v2, v0, Lcom/google/ac/b/c/ao;->a:Lcom/google/ac/b/c/al;

    aget-object v2, v6, v1

    iget-object v2, v2, Lcom/google/ac/b/c/am;->b:Ljava/lang/Integer;

    iput-object v2, v0, Lcom/google/ac/b/c/ao;->b:Ljava/lang/Integer;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iput-object v2, v0, Lcom/google/ac/b/c/ao;->c:Ljava/lang/Integer;

    iget-object v2, v4, Lcom/google/ac/b/c/dd;->d:Lcom/google/ac/b/c/bk;

    iget-object v2, v2, Lcom/google/ac/b/c/bk;->c:[Lcom/google/ac/b/c/ao;

    aput-object v0, v2, v1

    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    :cond_5
    return-object v4
.end method

.method public final bridge synthetic c()Lcom/google/android/location/copresence/a/a;
    .locals 1

    .prologue
    .line 25
    invoke-super {p0}, Lcom/google/android/location/copresence/l/ac;->c()Lcom/google/android/location/copresence/a/a;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic d()V
    .locals 0

    .prologue
    .line 25
    invoke-super {p0}, Lcom/google/android/location/copresence/l/ac;->d()V

    return-void
.end method
