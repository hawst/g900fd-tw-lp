.class public Lcom/google/android/location/internal/AnalyticsSamplerService;
.super Landroid/app/IntentService;
.source "SourceFile"


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 37
    const-string v0, "AnalyticsSamplerService"

    invoke-direct {p0, v0}, Landroid/app/IntentService;-><init>(Ljava/lang/String;)V

    .line 38
    return-void
.end method


# virtual methods
.method public onHandleIntent(Landroid/content/Intent;)V
    .locals 12

    .prologue
    const/4 v0, 0x1

    const-wide/16 v8, 0x1

    const-wide/16 v4, 0x0

    .line 46
    .line 48
    invoke-static {p0}, Lcom/google/android/location/a/b;->a(Landroid/content/Context;)V

    .line 51
    invoke-static {p0}, Lcom/google/android/location/clientlib/c;->a(Landroid/content/Context;)I

    move-result v1

    if-ne v0, v1, :cond_2

    move v1, v0

    .line 52
    :goto_0
    sget-object v0, Lcom/google/android/location/d/a;->l:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    .line 61
    const v2, 0x2b8cbccc    # 1.0E-12f

    cmpg-float v2, v0, v2

    if-gez v2, :cond_3

    move-wide v2, v4

    .line 67
    :goto_1
    const-string v0, "uc"

    const-string v10, "settings"

    const-string v11, "nlp"

    if-eqz v1, :cond_4

    move-wide v6, v8

    :goto_2
    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-static {v0, v10, v11, v6}, Lcom/google/android/location/a/b;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;)V

    .line 86
    cmp-long v0, v2, v4

    if-eqz v0, :cond_0

    .line 87
    const-string v0, "uc"

    const-string v6, "settings"

    const-string v7, "nlp_p"

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-static {v0, v6, v7, v2}, Lcom/google/android/location/a/b;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;)V

    .line 94
    :cond_0
    const-string v0, "location"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/location/LocationManager;

    .line 96
    :try_start_0
    const-string v2, "gps"

    invoke-virtual {v0, v2}, Landroid/location/LocationManager;->isProviderEnabled(Ljava/lang/String;)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 101
    :goto_3
    if-eqz v0, :cond_1

    .line 102
    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    .line 104
    const-string v6, "uc"

    const-string v7, "settings"

    const-string v10, "gps"

    if-eqz v0, :cond_5

    move-wide v2, v8

    :goto_4
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-static {v6, v7, v10, v2}, Lcom/google/android/location/a/b;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;)V

    .line 109
    const-string v6, "uc"

    const-string v7, "settings"

    const-string v10, "ngon"

    if-eqz v0, :cond_6

    if-eqz v1, :cond_6

    move-wide v2, v8

    :goto_5
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-static {v6, v7, v10, v2}, Lcom/google/android/location/a/b;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;)V

    .line 115
    const-string v2, "uc"

    const-string v3, "settings"

    const-string v6, "ngoff"

    if-nez v0, :cond_7

    if-nez v1, :cond_7

    move-wide v0, v8

    :goto_6
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-static {v2, v3, v6, v0}, Lcom/google/android/location/a/b;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;)V

    .line 124
    :cond_1
    const-string v0, "uc"

    const-string v1, "settings"

    const-string v2, "lga"

    invoke-static {p0}, Lcom/google/android/gms/common/util/y;->a(Landroid/content/Context;)Z

    move-result v3

    if-eqz v3, :cond_8

    :goto_7
    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/location/a/b;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;)V

    .line 131
    return-void

    .line 51
    :cond_2
    const/4 v0, 0x0

    move v1, v0

    goto/16 :goto_0

    .line 64
    :cond_3
    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    float-to-double v6, v0

    div-double/2addr v2, v6

    double-to-long v2, v2

    goto/16 :goto_1

    :cond_4
    move-wide v6, v4

    .line 67
    goto/16 :goto_2

    .line 98
    :catch_0
    move-exception v0

    const/4 v0, 0x0

    goto :goto_3

    :cond_5
    move-wide v2, v4

    .line 104
    goto :goto_4

    :cond_6
    move-wide v2, v4

    .line 109
    goto :goto_5

    :cond_7
    move-wide v0, v4

    .line 115
    goto :goto_6

    :cond_8
    move-wide v8, v4

    .line 124
    goto :goto_7
.end method
