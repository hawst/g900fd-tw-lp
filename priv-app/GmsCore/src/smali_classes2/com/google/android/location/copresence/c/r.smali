.class final Lcom/google/android/location/copresence/c/r;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lcom/google/android/location/copresence/c/q;


# direct methods
.method constructor <init>(Lcom/google/android/location/copresence/c/q;)V
    .locals 0

    .prologue
    .line 107
    iput-object p1, p0, Lcom/google/android/location/copresence/c/r;->a:Lcom/google/android/location/copresence/c/q;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 4

    .prologue
    const/4 v2, 0x3

    .line 110
    invoke-static {v2}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 111
    const-string v0, "Audio Stack: CarrierSenseManager: mStopBroadcasting called"

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->b(Ljava/lang/String;)I

    .line 113
    :cond_0
    iget-object v0, p0, Lcom/google/android/location/copresence/c/r;->a:Lcom/google/android/location/copresence/c/q;

    iget v0, v0, Lcom/google/android/location/copresence/c/q;->a:I

    const/4 v1, 0x2

    if-eq v0, v1, :cond_2

    .line 114
    invoke-static {v2}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 115
    const-string v0, "Audio Stack: CarrierSenseManager: Not in pause broadcaster state; returning"

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->b(Ljava/lang/String;)I

    .line 123
    :cond_1
    :goto_0
    return-void

    .line 120
    :cond_2
    iget-object v0, p0, Lcom/google/android/location/copresence/c/r;->a:Lcom/google/android/location/copresence/c/q;

    iget-object v0, v0, Lcom/google/android/location/copresence/c/q;->g:Lcom/google/android/location/copresence/c/b;

    iget-object v1, p0, Lcom/google/android/location/copresence/c/r;->a:Lcom/google/android/location/copresence/c/q;

    iget-object v1, v1, Lcom/google/android/location/copresence/c/q;->d:Lcom/google/android/gms/audiomodem/bf;

    invoke-virtual {v0, v1}, Lcom/google/android/location/copresence/c/b;->a(Lcom/google/android/gms/audiomodem/bf;)V

    .line 121
    iget-object v0, p0, Lcom/google/android/location/copresence/c/r;->a:Lcom/google/android/location/copresence/c/q;

    iget-object v0, v0, Lcom/google/android/location/copresence/c/q;->b:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/location/copresence/c/r;->a:Lcom/google/android/location/copresence/c/q;

    iget-object v1, v1, Lcom/google/android/location/copresence/c/q;->i:Ljava/lang/Runnable;

    invoke-static {}, Lcom/google/android/location/copresence/f/b;->b()Lcom/google/ac/b/c/g;

    move-result-object v2

    iget-object v2, v2, Lcom/google/ac/b/c/g;->f:Lcom/google/ac/b/c/f;

    iget-object v2, v2, Lcom/google/ac/b/c/f;->i:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0
.end method
