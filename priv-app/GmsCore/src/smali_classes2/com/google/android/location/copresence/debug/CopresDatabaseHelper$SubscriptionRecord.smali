.class public Lcom/google/android/location/copresence/debug/CopresDatabaseHelper$SubscriptionRecord;
.super Lcom/google/android/location/copresence/debug/a;
.source "SourceFile"


# instance fields
.field public accessKey:Ljava/lang/String;
    .annotation runtime Lcom/google/android/location/copresence/debug/b;
        a = false
    .end annotation
.end field

.field public isActive:Ljava/lang/Boolean;
    .annotation runtime Lcom/google/android/location/copresence/debug/b;
        a = true
        c = "1"
    .end annotation
.end field

.field public namespace:Ljava/lang/String;
    .annotation runtime Lcom/google/android/location/copresence/debug/b;
        a = true
    .end annotation
.end field

.field public sentTimestamp:Ljava/lang/Long;
    .annotation runtime Lcom/google/android/location/copresence/debug/b;
        a = false
        d = 0x2
    .end annotation
.end field

.field public strategy:Ljava/lang/Integer;
    .annotation runtime Lcom/google/android/location/copresence/debug/b;
        a = false
    .end annotation
.end field

.field public subscriptionId:Ljava/lang/String;
    .annotation runtime Lcom/google/android/location/copresence/debug/b;
        a = false
    .end annotation
.end field

.field public ttlMillis:Ljava/lang/Long;
    .annotation runtime Lcom/google/android/location/copresence/debug/b;
        a = false
    .end annotation
.end field

.field public type:Ljava/lang/String;
    .annotation runtime Lcom/google/android/location/copresence/debug/b;
        a = true
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 263
    invoke-direct {p0}, Lcom/google/android/location/copresence/debug/a;-><init>()V

    return-void
.end method
