.class public final Lcom/google/android/location/copresence/ah;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/location/copresence/i;


# static fields
.field private static j:Lcom/google/android/location/copresence/ah;


# instance fields
.field final a:Lcom/google/android/gms/gcm/ag;

.field final b:Landroid/content/Context;

.field final c:Lcom/google/android/location/copresence/ap;

.field final d:Lcom/google/android/location/copresence/l/d;

.field final e:Lcom/google/android/location/reporting/config/h;

.field final f:Ljava/util/concurrent/atomic/AtomicBoolean;

.field final g:Lcom/google/android/location/copresence/l/ab;

.field h:Ljava/lang/String;

.field private final i:Lcom/google/android/location/copresence/n/f;


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .locals 5

    .prologue
    const/4 v4, 0x3

    .line 102
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 52
    new-instance v0, Lcom/google/android/location/copresence/ai;

    invoke-direct {v0, p0}, Lcom/google/android/location/copresence/ai;-><init>(Lcom/google/android/location/copresence/ah;)V

    iput-object v0, p0, Lcom/google/android/location/copresence/ah;->g:Lcom/google/android/location/copresence/l/ab;

    .line 103
    iput-object p1, p0, Lcom/google/android/location/copresence/ah;->b:Landroid/content/Context;

    .line 104
    invoke-static {p1}, Lcom/google/android/location/copresence/ap;->a(Landroid/content/Context;)Lcom/google/android/location/copresence/ap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/copresence/ah;->c:Lcom/google/android/location/copresence/ap;

    .line 105
    invoke-static {p1}, Lcom/google/android/location/copresence/l/d;->a(Landroid/content/Context;)Lcom/google/android/location/copresence/l/d;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/copresence/ah;->d:Lcom/google/android/location/copresence/l/d;

    .line 106
    invoke-static {p1}, Lcom/google/android/gms/gcm/ag;->a(Landroid/content/Context;)Lcom/google/android/gms/gcm/ag;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/copresence/ah;->a:Lcom/google/android/gms/gcm/ag;

    .line 107
    const-string v0, "copresence_gcm_pref"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    const-string v0, "registration_id"

    const-string v2, ""

    invoke-interface {v1, v0, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-nez v2, :cond_2

    invoke-static {v4}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "DeviceRegistrationHelper: Registration not found."

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->b(Ljava/lang/String;)I

    :cond_0
    const-string v0, ""

    :cond_1
    :goto_0
    iput-object v0, p0, Lcom/google/android/location/copresence/ah;->h:Ljava/lang/String;

    .line 108
    invoke-static {p1}, Lcom/google/android/location/reporting/config/h;->a(Landroid/content/Context;)Lcom/google/android/location/reporting/config/h;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/copresence/ah;->e:Lcom/google/android/location/reporting/config/h;

    .line 109
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>()V

    iput-object v0, p0, Lcom/google/android/location/copresence/ah;->f:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 110
    invoke-static {p1}, Lcom/google/android/location/copresence/n/f;->a(Landroid/content/Context;)Lcom/google/android/location/copresence/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/copresence/ah;->i:Lcom/google/android/location/copresence/n/f;

    .line 111
    return-void

    .line 107
    :cond_2
    const-string v2, "appVersion"

    const/high16 v3, -0x80000000

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v1

    invoke-static {p1}, Lcom/google/android/location/copresence/ah;->b(Landroid/content/Context;)I

    move-result v2

    if-eq v1, v2, :cond_1

    invoke-static {v4}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v0

    if-eqz v0, :cond_3

    const-string v0, "DeviceRegistrationHelper: App version changed."

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->b(Ljava/lang/String;)I

    :cond_3
    const-string v0, ""

    goto :goto_0
.end method

.method public static declared-synchronized a(Landroid/content/Context;)Lcom/google/android/location/copresence/ah;
    .locals 2

    .prologue
    .line 96
    const-class v1, Lcom/google/android/location/copresence/ah;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/google/android/location/copresence/ah;->j:Lcom/google/android/location/copresence/ah;

    if-nez v0, :cond_0

    .line 97
    new-instance v0, Lcom/google/android/location/copresence/ah;

    invoke-direct {v0, p0}, Lcom/google/android/location/copresence/ah;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/google/android/location/copresence/ah;->j:Lcom/google/android/location/copresence/ah;

    .line 99
    :cond_0
    sget-object v0, Lcom/google/android/location/copresence/ah;->j:Lcom/google/android/location/copresence/ah;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 96
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method static b(Landroid/content/Context;)I
    .locals 4

    .prologue
    .line 256
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v0

    .line 258
    iget v0, v0, Landroid/content/pm/PackageInfo;->versionCode:I
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    return v0

    .line 259
    :catch_0
    move-exception v0

    .line 261
    new-instance v1, Ljava/lang/RuntimeException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Could not get package name: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1
.end method


# virtual methods
.method public final a()V
    .locals 0

    .prologue
    .line 324
    invoke-virtual {p0}, Lcom/google/android/location/copresence/ah;->c()V

    .line 325
    return-void
.end method

.method public final a(Lcom/google/android/location/copresence/a/a;Z)V
    .locals 2

    .prologue
    .line 182
    sget-object v0, Lcom/google/android/location/copresence/k;->H:Lcom/google/android/location/copresence/l;

    invoke-virtual {v0}, Lcom/google/android/location/copresence/l;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_1

    .line 183
    const/4 v0, 0x3

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 184
    const-string v0, "DeviceRegistrationHelper: Skipping register device since the flag is disabled"

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->b(Ljava/lang/String;)I

    .line 211
    :cond_0
    :goto_0
    return-void

    .line 189
    :cond_1
    new-instance v0, Lcom/google/android/location/copresence/ak;

    invoke-direct {v0, p0, p2, p1}, Lcom/google/android/location/copresence/ak;-><init>(Lcom/google/android/location/copresence/ah;ZLcom/google/android/location/copresence/a/a;)V

    .line 206
    iget-object v1, p0, Lcom/google/android/location/copresence/ah;->h:Ljava/lang/String;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/google/android/location/copresence/ah;->h:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 207
    :cond_2
    new-instance v1, Lcom/google/android/location/copresence/al;

    invoke-direct {v1, p0, v0}, Lcom/google/android/location/copresence/al;-><init>(Lcom/google/android/location/copresence/ah;Ljava/lang/Runnable;)V

    invoke-virtual {v1}, Lcom/google/android/location/copresence/al;->start()V

    goto :goto_0

    .line 209
    :cond_3
    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    goto :goto_0
.end method

.method public final b()V
    .locals 0

    .prologue
    .line 330
    return-void
.end method

.method public final declared-synchronized c()V
    .locals 7

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 129
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/location/copresence/ah;->f:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v0, :cond_1

    .line 159
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 132
    :cond_1
    :try_start_1
    iget-object v0, p0, Lcom/google/android/location/copresence/ah;->i:Lcom/google/android/location/copresence/n/f;

    invoke-virtual {v0}, Lcom/google/android/location/copresence/n/f;->c()Ljava/util/Set;

    move-result-object v0

    .line 134
    if-eqz v0, :cond_0

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_0

    .line 137
    iget-object v3, p0, Lcom/google/android/location/copresence/ah;->f:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 139
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/copresence/a/a;

    .line 140
    iget-object v4, p0, Lcom/google/android/location/copresence/ah;->b:Landroid/content/Context;

    const-string v5, "copresence_gcm_pref"

    const/4 v6, 0x0

    invoke-virtual {v4, v5, v6}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "REGISTERED_"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v6, v0, Lcom/google/android/location/copresence/a/a;->a:Landroid/accounts/Account;

    iget-object v6, v6, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    invoke-interface {v4, v5, v6}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v4

    .line 142
    if-nez v4, :cond_2

    .line 143
    iget-object v2, p0, Lcom/google/android/location/copresence/ah;->c:Lcom/google/android/location/copresence/ap;

    new-instance v3, Lcom/google/android/location/copresence/aj;

    invoke-direct {v3, p0, v0}, Lcom/google/android/location/copresence/aj;-><init>(Lcom/google/android/location/copresence/ah;Lcom/google/android/location/copresence/a/a;)V

    const/4 v0, 0x0

    invoke-virtual {v2, v3, v0}, Lcom/google/android/location/copresence/ap;->a(Ljava/lang/Runnable;Landroid/os/WorkSource;)V

    move v0, v1

    .line 156
    :goto_1
    if-nez v0, :cond_0

    .line 157
    iget-object v0, p0, Lcom/google/android/location/copresence/ah;->f:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 129
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_3
    move v0, v2

    goto :goto_1
.end method
