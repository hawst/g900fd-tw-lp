.class public final Lcom/google/android/location/copresence/q/ar;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/location/copresence/q/al;


# instance fields
.field final a:Lcom/google/android/location/copresence/q/at;

.field b:Z

.field private final c:Lcom/google/android/location/copresence/q/am;

.field private final d:Lcom/google/android/location/copresence/q/aq;


# direct methods
.method public constructor <init>(JLcom/google/android/location/copresence/q/at;)V
    .locals 1

    .prologue
    .line 30
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/google/android/location/copresence/q/ar;-><init>(JLcom/google/android/location/copresence/q/at;Landroid/os/Handler;)V

    .line 31
    return-void
.end method

.method private constructor <init>(JLcom/google/android/location/copresence/q/at;Landroid/os/Handler;)V
    .locals 3

    .prologue
    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    invoke-static {p3}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/copresence/q/at;

    iput-object v0, p0, Lcom/google/android/location/copresence/q/ar;->a:Lcom/google/android/location/copresence/q/at;

    .line 38
    new-instance v0, Lcom/google/android/location/copresence/q/as;

    invoke-direct {v0, p0}, Lcom/google/android/location/copresence/q/as;-><init>(Lcom/google/android/location/copresence/q/ar;)V

    iput-object v0, p0, Lcom/google/android/location/copresence/q/ar;->d:Lcom/google/android/location/copresence/q/aq;

    .line 53
    new-instance v0, Lcom/google/android/location/copresence/q/am;

    iget-object v1, p0, Lcom/google/android/location/copresence/q/ar;->d:Lcom/google/android/location/copresence/q/aq;

    invoke-direct {v0, v1, p4}, Lcom/google/android/location/copresence/q/am;-><init>(Lcom/google/android/location/copresence/q/aq;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/google/android/location/copresence/q/ar;->c:Lcom/google/android/location/copresence/q/am;

    .line 54
    iget-object v0, p0, Lcom/google/android/location/copresence/q/ar;->c:Lcom/google/android/location/copresence/q/am;

    iget-object v1, v0, Lcom/google/android/location/copresence/q/am;->d:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-boolean v2, v0, Lcom/google/android/location/copresence/q/am;->e:Z

    if-eqz v2, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v2, "Cannot start more than once."

    invoke-direct {v0, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    :cond_0
    const/4 v2, 0x1

    :try_start_1
    iput-boolean v2, v0, Lcom/google/android/location/copresence/q/am;->e:Z

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    iget-object v1, v0, Lcom/google/android/location/copresence/q/am;->a:Landroid/os/Handler;

    iget-object v0, v0, Lcom/google/android/location/copresence/q/am;->c:Ljava/lang/Runnable;

    invoke-virtual {v1, v0, p1, p2}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 55
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 59
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/location/copresence/q/ar;->b:Z

    .line 60
    iget-object v0, p0, Lcom/google/android/location/copresence/q/ar;->c:Lcom/google/android/location/copresence/q/am;

    invoke-virtual {v0}, Lcom/google/android/location/copresence/q/am;->a()V

    .line 61
    return-void
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 65
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/location/copresence/q/ar;->b:Z

    .line 66
    iget-object v0, p0, Lcom/google/android/location/copresence/q/ar;->c:Lcom/google/android/location/copresence/q/am;

    invoke-virtual {v0}, Lcom/google/android/location/copresence/q/am;->a()V

    .line 67
    return-void
.end method
