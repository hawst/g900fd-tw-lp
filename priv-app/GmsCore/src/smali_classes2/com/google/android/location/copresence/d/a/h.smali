.class public final Lcom/google/android/location/copresence/d/a/h;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/location/copresence/d/a/n;


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x15
.end annotation


# instance fields
.field final a:Lcom/google/android/location/copresence/d/a/o;

.field private final b:Lcom/google/android/location/copresence/d/e;

.field private final c:Landroid/bluetooth/le/ScanCallback;


# direct methods
.method public constructor <init>(Lcom/google/android/location/copresence/d/e;Lcom/google/android/location/copresence/d/a/o;)V
    .locals 1

    .prologue
    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    iput-object p1, p0, Lcom/google/android/location/copresence/d/a/h;->b:Lcom/google/android/location/copresence/d/e;

    .line 38
    iput-object p2, p0, Lcom/google/android/location/copresence/d/a/h;->a:Lcom/google/android/location/copresence/d/a/o;

    .line 39
    new-instance v0, Lcom/google/android/location/copresence/d/a/i;

    invoke-direct {v0, p0}, Lcom/google/android/location/copresence/d/a/i;-><init>(Lcom/google/android/location/copresence/d/a/h;)V

    iput-object v0, p0, Lcom/google/android/location/copresence/d/a/h;->c:Landroid/bluetooth/le/ScanCallback;

    .line 100
    return-void
.end method

.method static a(Ljava/lang/String;)[B
    .locals 2

    .prologue
    .line 103
    if-nez p0, :cond_0

    .line 104
    const/4 v0, 0x0

    .line 116
    :goto_0
    return-object v0

    .line 106
    :cond_0
    :try_start_0
    const-string v0, "UTF-8"

    invoke-virtual {p0, v0}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_0

    .line 109
    :catch_0
    move-exception v0

    .line 110
    const/4 v1, 0x6

    invoke-static {v1}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 111
    const-string v1, "BleScanImplL: Could not convert string to byte using UTF-8."

    invoke-static {v1, v0}, Lcom/google/android/location/copresence/ag;->a(Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 114
    :cond_1
    invoke-virtual {p0}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    goto :goto_0
.end method

.method private c()Landroid/bluetooth/le/BluetoothLeScanner;
    .locals 3

    .prologue
    .line 152
    iget-object v0, p0, Lcom/google/android/location/copresence/d/a/h;->b:Lcom/google/android/location/copresence/d/e;

    iget-object v0, v0, Lcom/google/android/location/copresence/d/e;->a:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothAdapter;->getBluetoothLeScanner()Landroid/bluetooth/le/BluetoothLeScanner;

    move-result-object v0

    .line 154
    if-nez v0, :cond_0

    .line 155
    const/4 v1, 0x5

    invoke-static {v1}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 156
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "BleScanImplL: Failed to get scanner. Bluetooth must be enabled. Enabled state: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/location/copresence/d/a/h;->b:Lcom/google/android/location/copresence/d/e;

    iget-object v2, v2, Lcom/google/android/location/copresence/d/e;->a:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v2}, Landroid/bluetooth/BluetoothAdapter;->isEnabled()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/location/copresence/ag;->d(Ljava/lang/String;)I

    .line 160
    :cond_0
    return-object v0
.end method


# virtual methods
.method public final a()Z
    .locals 6

    .prologue
    const/4 v3, 0x2

    const/4 v0, 0x1

    .line 121
    invoke-static {v3}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 122
    const-string v1, "BleScanImplL: startScan"

    invoke-static {v1}, Lcom/google/android/location/copresence/ag;->a(Ljava/lang/String;)I

    .line 124
    :cond_0
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 125
    new-instance v2, Landroid/bluetooth/le/ScanSettings$Builder;

    invoke-direct {v2}, Landroid/bluetooth/le/ScanSettings$Builder;-><init>()V

    invoke-virtual {v2, v0}, Landroid/bluetooth/le/ScanSettings$Builder;->setCallbackType(I)Landroid/bluetooth/le/ScanSettings$Builder;

    move-result-object v2

    const-wide/16 v4, 0x0

    invoke-virtual {v2, v4, v5}, Landroid/bluetooth/le/ScanSettings$Builder;->setReportDelay(J)Landroid/bluetooth/le/ScanSettings$Builder;

    move-result-object v2

    invoke-virtual {v2, v3}, Landroid/bluetooth/le/ScanSettings$Builder;->setScanMode(I)Landroid/bluetooth/le/ScanSettings$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/bluetooth/le/ScanSettings$Builder;->build()Landroid/bluetooth/le/ScanSettings;

    move-result-object v2

    .line 130
    invoke-direct {p0}, Lcom/google/android/location/copresence/d/a/h;->c()Landroid/bluetooth/le/BluetoothLeScanner;

    move-result-object v3

    .line 131
    if-eqz v3, :cond_1

    .line 132
    iget-object v4, p0, Lcom/google/android/location/copresence/d/a/h;->c:Landroid/bluetooth/le/ScanCallback;

    invoke-virtual {v3, v1, v2, v4}, Landroid/bluetooth/le/BluetoothLeScanner;->startScan(Ljava/util/List;Landroid/bluetooth/le/ScanSettings;Landroid/bluetooth/le/ScanCallback;)V

    .line 135
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()Z
    .locals 2

    .prologue
    .line 140
    const/4 v0, 0x2

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 141
    const-string v0, "BleScanImplL: stopScan"

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->a(Ljava/lang/String;)I

    .line 143
    :cond_0
    invoke-direct {p0}, Lcom/google/android/location/copresence/d/a/h;->c()Landroid/bluetooth/le/BluetoothLeScanner;

    move-result-object v0

    .line 144
    if-eqz v0, :cond_1

    .line 145
    iget-object v1, p0, Lcom/google/android/location/copresence/d/a/h;->c:Landroid/bluetooth/le/ScanCallback;

    invoke-virtual {v0, v1}, Landroid/bluetooth/le/BluetoothLeScanner;->stopScan(Landroid/bluetooth/le/ScanCallback;)V

    .line 146
    const/4 v0, 0x1

    .line 148
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
