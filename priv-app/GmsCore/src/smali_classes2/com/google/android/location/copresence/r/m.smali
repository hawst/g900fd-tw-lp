.class final Lcom/google/android/location/copresence/r/m;
.super Lcom/google/android/location/copresence/k/f;
.source "SourceFile"


# instance fields
.field public final a:Ljava/lang/String;

.field final synthetic b:Lcom/google/android/location/copresence/r/f;


# direct methods
.method public constructor <init>(Lcom/google/android/location/copresence/r/f;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 251
    iput-object p1, p0, Lcom/google/android/location/copresence/r/m;->b:Lcom/google/android/location/copresence/r/f;

    .line 252
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "WifiApEnabled="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x2

    new-array v1, v1, [Lcom/google/android/location/copresence/k/f;

    const/4 v2, 0x0

    invoke-static {p1}, Lcom/google/android/location/copresence/r/f;->c(Lcom/google/android/location/copresence/r/f;)Lcom/google/android/location/copresence/k/f;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    invoke-static {p1}, Lcom/google/android/location/copresence/r/f;->d(Lcom/google/android/location/copresence/r/f;)Lcom/google/android/location/copresence/k/f;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-direct {p0, v0, v1}, Lcom/google/android/location/copresence/k/f;-><init>(Ljava/lang/String;[Lcom/google/android/location/copresence/k/f;)V

    .line 253
    iput-object p2, p0, Lcom/google/android/location/copresence/r/m;->a:Ljava/lang/String;

    .line 254
    return-void
.end method


# virtual methods
.method public final a()Z
    .locals 5

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 263
    iget-object v2, p0, Lcom/google/android/location/copresence/r/m;->b:Lcom/google/android/location/copresence/r/f;

    invoke-static {v2}, Lcom/google/android/location/copresence/r/f;->b(Lcom/google/android/location/copresence/r/f;)Lcom/google/android/location/copresence/r/ae;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/location/copresence/r/ae;->b()Landroid/net/wifi/WifiConfiguration;

    move-result-object v2

    .line 264
    const/16 v3, 0xa

    new-array v3, v3, [B

    iget-object v4, p0, Lcom/google/android/location/copresence/r/m;->b:Lcom/google/android/location/copresence/r/f;

    invoke-static {v4}, Lcom/google/android/location/copresence/r/f;->e(Lcom/google/android/location/copresence/r/f;)Ljava/util/Random;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/util/Random;->nextBytes([B)V

    const/16 v4, 0xb

    invoke-static {v3, v4}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Landroid/net/wifi/WifiConfiguration;->preSharedKey:Ljava/lang/String;

    iget-object v3, v2, Landroid/net/wifi/WifiConfiguration;->allowedAuthAlgorithms:Ljava/util/BitSet;

    invoke-virtual {v3}, Ljava/util/BitSet;->clear()V

    iget-object v3, v2, Landroid/net/wifi/WifiConfiguration;->allowedAuthAlgorithms:Ljava/util/BitSet;

    invoke-virtual {v3, v1}, Ljava/util/BitSet;->set(I)V

    iget-object v3, v2, Landroid/net/wifi/WifiConfiguration;->allowedKeyManagement:Ljava/util/BitSet;

    invoke-virtual {v3}, Ljava/util/BitSet;->clear()V

    iget-object v3, v2, Landroid/net/wifi/WifiConfiguration;->allowedKeyManagement:Ljava/util/BitSet;

    const/4 v4, 0x4

    invoke-virtual {v3, v4}, Ljava/util/BitSet;->set(I)V

    .line 265
    iget-object v3, p0, Lcom/google/android/location/copresence/r/m;->a:Ljava/lang/String;

    iput-object v3, v2, Landroid/net/wifi/WifiConfiguration;->SSID:Ljava/lang/String;

    .line 266
    iget-object v3, p0, Lcom/google/android/location/copresence/r/m;->b:Lcom/google/android/location/copresence/r/f;

    invoke-static {v3}, Lcom/google/android/location/copresence/r/f;->b(Lcom/google/android/location/copresence/r/f;)Lcom/google/android/location/copresence/r/ae;

    move-result-object v3

    invoke-virtual {v3, v1}, Lcom/google/android/location/copresence/r/ae;->a(Z)Z

    move-result v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/google/android/location/copresence/r/m;->b:Lcom/google/android/location/copresence/r/f;

    invoke-static {v3}, Lcom/google/android/location/copresence/r/f;->b(Lcom/google/android/location/copresence/r/f;)Lcom/google/android/location/copresence/r/ae;

    move-result-object v3

    invoke-virtual {v3, v2, v0}, Lcom/google/android/location/copresence/r/ae;->a(Landroid/net/wifi/WifiConfiguration;Z)Z

    move-result v2

    if-eqz v2, :cond_0

    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method public final b()Z
    .locals 2

    .prologue
    .line 258
    iget-object v0, p0, Lcom/google/android/location/copresence/r/m;->a:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/location/copresence/r/m;->b:Lcom/google/android/location/copresence/r/f;

    invoke-static {v1}, Lcom/google/android/location/copresence/r/f;->b(Lcom/google/android/location/copresence/r/f;)Lcom/google/android/location/copresence/r/ae;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/location/copresence/r/ae;->b()Landroid/net/wifi/WifiConfiguration;

    move-result-object v1

    iget-object v1, v1, Landroid/net/wifi/WifiConfiguration;->SSID:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/location/copresence/r/m;->b:Lcom/google/android/location/copresence/r/f;

    invoke-static {v0}, Lcom/google/android/location/copresence/r/f;->b(Lcom/google/android/location/copresence/r/f;)Lcom/google/android/location/copresence/r/ae;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/location/copresence/r/ae;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final d()J
    .locals 2

    .prologue
    .line 272
    const-wide/16 v0, 0x1388

    return-wide v0
.end method
