.class public final Lcom/google/android/location/ap;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/location/o/e;
.implements Lcom/google/android/location/os/a;


# instance fields
.field final a:Lcom/google/android/location/os/bi;

.field final b:Lcom/google/android/location/b/ak;

.field final c:Lcom/google/android/location/b/ao;

.field final d:Lcom/google/android/location/b/c;

.field final e:Lcom/google/android/location/k/a;

.field final f:Lcom/google/android/location/am;

.field final g:Lcom/google/android/location/a;

.field final h:Lcom/google/android/location/be;

.field final i:Lcom/google/android/location/b/ad;

.field final j:Lcom/google/android/location/b/ai;

.field final k:Lcom/google/android/location/av;

.field final l:Lcom/google/android/location/v;

.field final m:Lcom/google/android/location/ba;

.field final n:Lcom/google/android/location/e/g;

.field private o:Lcom/google/android/location/e/bi;


# direct methods
.method public constructor <init>(Lcom/google/android/location/os/bi;Lcom/google/android/location/e/ah;Lcom/google/android/location/activity/k;Z)V
    .locals 18

    .prologue
    .line 77
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    .line 78
    invoke-interface/range {p1 .. p1}, Lcom/google/android/location/os/bi;->d()Lcom/google/android/location/j/d;

    move-result-object v9

    .line 79
    move-object/from16 v0, p1

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/location/ap;->a:Lcom/google/android/location/os/bi;

    .line 80
    invoke-interface/range {p1 .. p1}, Lcom/google/android/location/os/bi;->e()Lcom/google/android/location/j/e;

    move-result-object v2

    .line 81
    new-instance v17, Lcom/google/android/location/k/a;

    invoke-interface/range {p1 .. p1}, Lcom/google/android/location/os/bi;->c()Lcom/google/android/location/j/b;

    move-result-object v3

    move-object/from16 v0, v17

    move-object/from16 v1, p2

    invoke-direct {v0, v3, v2, v1}, Lcom/google/android/location/k/a;-><init>(Lcom/google/android/location/j/b;Lcom/google/android/location/j/e;Lcom/google/android/location/e/ah;)V

    .line 82
    invoke-interface {v2}, Lcom/google/android/location/j/e;->d()Lcom/google/android/location/j/i;

    move-result-object v3

    invoke-interface {v2}, Lcom/google/android/location/j/e;->b()Ljava/io/File;

    move-result-object v4

    invoke-interface {v9}, Lcom/google/android/location/j/d;->c()[B

    move-result-object v5

    invoke-static {v3, v4, v5, v2}, Lcom/google/android/location/b/ad;->a(Lcom/google/android/location/j/i;Ljava/io/File;[BLcom/google/android/location/j/e;)Lcom/google/android/location/b/ad;

    move-result-object v3

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/location/ap;->i:Lcom/google/android/location/b/ad;

    .line 84
    invoke-interface {v2}, Lcom/google/android/location/j/e;->b()Ljava/io/File;

    move-result-object v3

    .line 85
    if-eqz v3, :cond_0

    invoke-virtual {v3}, Ljava/io/File;->isDirectory()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 86
    new-instance v4, Lcom/google/android/location/b/an;

    invoke-interface {v9}, Lcom/google/android/location/j/d;->c()[B

    move-result-object v5

    invoke-direct {v4, v2, v3, v5}, Lcom/google/android/location/b/an;-><init>(Lcom/google/android/location/j/e;Ljava/io/File;[B)V

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/google/android/location/ap;->j:Lcom/google/android/location/b/ai;

    .line 91
    :goto_0
    new-instance v2, Lcom/google/android/location/b/ak;

    invoke-interface/range {p1 .. p1}, Lcom/google/android/location/os/bi;->c()Lcom/google/android/location/j/b;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/location/ap;->j:Lcom/google/android/location/b/ai;

    move-object/from16 v0, p2

    invoke-direct {v2, v9, v3, v0, v4}, Lcom/google/android/location/b/ak;-><init>(Lcom/google/android/location/j/d;Lcom/google/android/location/j/b;Lcom/google/android/location/e/ah;Lcom/google/android/location/b/ai;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/location/ap;->b:Lcom/google/android/location/b/ak;

    .line 92
    new-instance v2, Lcom/google/android/location/b/ao;

    invoke-interface/range {p1 .. p1}, Lcom/google/android/location/os/bi;->c()Lcom/google/android/location/j/b;

    move-result-object v3

    invoke-interface/range {p1 .. p1}, Lcom/google/android/location/os/bi;->e()Lcom/google/android/location/j/e;

    move-result-object v4

    move-object/from16 v0, p2

    invoke-direct {v2, v3, v4, v9, v0}, Lcom/google/android/location/b/ao;-><init>(Lcom/google/android/location/j/b;Lcom/google/android/location/j/e;Lcom/google/android/location/j/d;Lcom/google/android/location/e/ah;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/location/ap;->c:Lcom/google/android/location/b/ao;

    .line 94
    new-instance v2, Lcom/google/android/location/b/c;

    invoke-interface/range {p1 .. p1}, Lcom/google/android/location/os/bi;->B()Lcom/google/android/location/j/f;

    move-result-object v3

    invoke-interface/range {p1 .. p1}, Lcom/google/android/location/os/bi;->c()Lcom/google/android/location/j/b;

    move-result-object v4

    invoke-interface/range {p1 .. p1}, Lcom/google/android/location/os/bi;->f()Lcom/google/android/location/j/j;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/location/ap;->b:Lcom/google/android/location/b/ak;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/location/ap;->c:Lcom/google/android/location/b/ao;

    move-object/from16 v8, p2

    invoke-direct/range {v2 .. v8}, Lcom/google/android/location/b/c;-><init>(Lcom/google/android/location/j/f;Lcom/google/android/location/j/b;Lcom/google/android/location/j/j;Lcom/google/android/location/b/ak;Lcom/google/android/location/b/ao;Lcom/google/android/location/e/ah;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/location/ap;->d:Lcom/google/android/location/b/c;

    .line 96
    invoke-static/range {p1 .. p1}, Lcom/google/android/location/av;->a(Lcom/google/android/location/os/bi;)Lcom/google/android/location/av;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/location/ap;->k:Lcom/google/android/location/av;

    .line 97
    new-instance v7, Lcom/google/android/location/e/w;

    invoke-direct {v7}, Lcom/google/android/location/e/w;-><init>()V

    .line 99
    new-instance v2, Lcom/google/android/location/v;

    invoke-interface/range {p1 .. p1}, Lcom/google/android/location/os/bi;->e()Lcom/google/android/location/j/e;

    move-result-object v3

    invoke-interface {v9}, Lcom/google/android/location/j/d;->a()Ljavax/crypto/SecretKey;

    move-result-object v4

    invoke-interface {v9}, Lcom/google/android/location/j/d;->c()[B

    move-result-object v5

    invoke-direct {v2, v3, v4, v5}, Lcom/google/android/location/v;-><init>(Lcom/google/android/location/j/e;Ljavax/crypto/SecretKey;[B)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/location/ap;->l:Lcom/google/android/location/v;

    .line 102
    new-instance v2, Lcom/google/android/location/be;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/location/ap;->b:Lcom/google/android/location/b/ak;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/location/ap;->c:Lcom/google/android/location/b/ao;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/location/ap;->l:Lcom/google/android/location/v;

    move-object/from16 v3, p1

    move-object/from16 v8, v17

    invoke-direct/range {v2 .. v8}, Lcom/google/android/location/be;-><init>(Lcom/google/android/location/os/bi;Lcom/google/android/location/b/ak;Lcom/google/android/location/b/ao;Lcom/google/android/location/v;Lcom/google/android/location/e/w;Lcom/google/android/location/k/a;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/location/ap;->h:Lcom/google/android/location/be;

    .line 103
    new-instance v2, Lcom/google/android/location/e/g;

    invoke-direct {v2}, Lcom/google/android/location/e/g;-><init>()V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/location/ap;->n:Lcom/google/android/location/e/g;

    .line 108
    new-instance v8, Lcom/google/android/location/am;

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/android/location/ap;->b:Lcom/google/android/location/b/ak;

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/android/location/ap;->c:Lcom/google/android/location/b/ao;

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/location/ap;->i:Lcom/google/android/location/b/ad;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/location/ap;->l:Lcom/google/android/location/v;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/location/ap;->h:Lcom/google/android/location/be;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/location/ap;->n:Lcom/google/android/location/e/g;

    move-object/from16 v16, v0

    move-object/from16 v9, p1

    move-object v14, v7

    invoke-direct/range {v8 .. v16}, Lcom/google/android/location/am;-><init>(Lcom/google/android/location/os/bi;Lcom/google/android/location/b/ak;Lcom/google/android/location/b/ao;Lcom/google/android/location/b/ad;Lcom/google/android/location/v;Lcom/google/android/location/e/w;Lcom/google/android/location/be;Lcom/google/android/location/e/g;)V

    move-object/from16 v0, p0

    iput-object v8, v0, Lcom/google/android/location/ap;->f:Lcom/google/android/location/am;

    .line 110
    if-eqz p4, :cond_2

    .line 111
    new-instance v2, Lcom/google/android/location/al;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/location/ap;->b:Lcom/google/android/location/b/ak;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/location/ap;->c:Lcom/google/android/location/b/ao;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/location/ap;->l:Lcom/google/android/location/v;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/location/ap;->h:Lcom/google/android/location/be;

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/location/ap;->k:Lcom/google/android/location/av;

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/android/location/ap;->n:Lcom/google/android/location/e/g;

    move-object/from16 v3, p1

    move-object/from16 v9, v17

    move-object/from16 v11, p3

    invoke-direct/range {v2 .. v11}, Lcom/google/android/location/al;-><init>(Lcom/google/android/location/os/bi;Lcom/google/android/location/b/ak;Lcom/google/android/location/b/ao;Lcom/google/android/location/v;Lcom/google/android/location/be;Lcom/google/android/location/av;Lcom/google/android/location/k/a;Lcom/google/android/location/e/g;Lcom/google/android/location/activity/k;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/location/ap;->g:Lcom/google/android/location/a;

    .line 117
    :goto_1
    move-object/from16 v0, v17

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/location/ap;->e:Lcom/google/android/location/k/a;

    .line 118
    new-instance v2, Lcom/google/android/location/ba;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/location/ap;->l:Lcom/google/android/location/v;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/location/ap;->k:Lcom/google/android/location/av;

    move-object/from16 v0, p1

    invoke-direct {v2, v0, v3, v4}, Lcom/google/android/location/ba;-><init>(Lcom/google/android/location/os/bi;Lcom/google/android/location/v;Lcom/google/android/location/av;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/location/ap;->m:Lcom/google/android/location/ba;

    .line 119
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/location/ap;->k:Lcom/google/android/location/av;

    move-object/from16 v0, p0

    invoke-virtual {v2, v0}, Lcom/google/android/location/av;->a(Lcom/google/android/location/o/e;)V

    .line 120
    return-void

    .line 88
    :cond_0
    sget-boolean v2, Lcom/google/android/location/i/a;->d:Z

    if-eqz v2, :cond_1

    const-string v2, "NetworkProvider"

    const-string v3, "No persistence dir, unable to record NlpStats"

    invoke-static {v2, v3}, Lcom/google/android/location/o/a/a;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 89
    :cond_1
    new-instance v2, Lcom/google/android/location/b/aj;

    invoke-direct {v2}, Lcom/google/android/location/b/aj;-><init>()V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/location/ap;->j:Lcom/google/android/location/b/ai;

    goto/16 :goto_0

    .line 114
    :cond_2
    new-instance v2, Lcom/google/android/location/ar;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/location/ap;->b:Lcom/google/android/location/b/ak;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/location/ap;->c:Lcom/google/android/location/b/ao;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/location/ap;->h:Lcom/google/android/location/be;

    move-object/from16 v3, p1

    move-object/from16 v5, v17

    invoke-direct/range {v2 .. v7}, Lcom/google/android/location/ar;-><init>(Lcom/google/android/location/os/bi;Lcom/google/android/location/b/ak;Lcom/google/android/location/k/a;Lcom/google/android/location/b/ao;Lcom/google/android/location/be;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/location/ap;->g:Lcom/google/android/location/a;

    goto :goto_1
.end method


# virtual methods
.method public final a()V
    .locals 0

    .prologue
    .line 399
    return-void
.end method

.method public final a(IIIZLcom/google/android/location/o/n;)V
    .locals 3

    .prologue
    .line 197
    iget-object v0, p0, Lcom/google/android/location/ap;->f:Lcom/google/android/location/am;

    iget-object v1, p0, Lcom/google/android/location/ap;->a:Lcom/google/android/location/os/bi;

    invoke-interface {v1}, Lcom/google/android/location/os/bi;->c()Lcom/google/android/location/j/b;

    move-result-object v1

    new-instance v2, Lcom/google/android/location/e/y;

    invoke-direct {v2, p1, p2, p3, p5}, Lcom/google/android/location/e/y;-><init>(IIILcom/google/android/location/o/n;)V

    invoke-virtual {v0, v1, v2, p4}, Lcom/google/android/location/am;->a(Lcom/google/android/location/j/b;Lcom/google/android/location/e/y;Z)V

    .line 199
    iget-object v0, p0, Lcom/google/android/location/ap;->d:Lcom/google/android/location/b/c;

    invoke-virtual {v0, p5}, Lcom/google/android/location/b/c;->a(Lcom/google/android/location/o/n;)V

    .line 200
    return-void
.end method

.method public final a(IIZ)V
    .locals 1

    .prologue
    .line 323
    iget-object v0, p0, Lcom/google/android/location/ap;->k:Lcom/google/android/location/av;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/location/av;->a(IIZ)V

    .line 324
    iget-object v0, p0, Lcom/google/android/location/ap;->g:Lcom/google/android/location/a;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/location/a;->a(IIZ)V

    .line 325
    return-void
.end method

.method public final a(IIZZLcom/google/android/location/o/n;Ljava/util/Set;)V
    .locals 0

    .prologue
    .line 385
    return-void
.end method

.method public final a(Lcom/google/android/gms/location/ActivityRecognitionResult;)V
    .locals 1

    .prologue
    .line 378
    iget-object v0, p0, Lcom/google/android/location/ap;->g:Lcom/google/android/location/a;

    invoke-virtual {v0, p1}, Lcom/google/android/location/a;->a(Lcom/google/android/gms/location/ActivityRecognitionResult;)V

    .line 379
    return-void
.end method

.method public final a(Lcom/google/android/location/activity/bd;)V
    .locals 0

    .prologue
    .line 374
    return-void
.end method

.method public final a(Lcom/google/android/location/e/ah;)V
    .locals 5

    .prologue
    .line 361
    iget-object v0, p0, Lcom/google/android/location/ap;->e:Lcom/google/android/location/k/a;

    iget-object v1, v0, Lcom/google/android/location/k/a;->f:Lcom/google/android/location/j/b;

    invoke-interface {v1}, Lcom/google/android/location/j/b;->c()J

    move-result-wide v2

    iget-object v1, v0, Lcom/google/android/location/k/a;->b:Lcom/google/android/location/k/d;

    if-eqz v1, :cond_0

    iget-object v1, v0, Lcom/google/android/location/k/a;->b:Lcom/google/android/location/k/d;

    invoke-virtual {p1}, Lcom/google/android/location/e/ah;->r()Lcom/google/android/location/e/ai;

    move-result-object v4

    invoke-virtual {v1, v4, v2, v3}, Lcom/google/android/location/k/d;->a(Lcom/google/android/location/e/ai;J)V

    :cond_0
    iget-object v1, v0, Lcom/google/android/location/k/a;->c:Lcom/google/android/location/k/d;

    if-eqz v1, :cond_1

    iget-object v1, v0, Lcom/google/android/location/k/a;->c:Lcom/google/android/location/k/d;

    invoke-virtual {p1}, Lcom/google/android/location/e/ah;->s()Lcom/google/android/location/e/ai;

    move-result-object v4

    invoke-virtual {v1, v4, v2, v3}, Lcom/google/android/location/k/d;->a(Lcom/google/android/location/e/ai;J)V

    :cond_1
    iget-object v1, v0, Lcom/google/android/location/k/a;->d:Lcom/google/android/location/k/d;

    if-eqz v1, :cond_2

    iget-object v1, v0, Lcom/google/android/location/k/a;->d:Lcom/google/android/location/k/d;

    invoke-virtual {p1}, Lcom/google/android/location/e/ah;->t()Lcom/google/android/location/e/ai;

    move-result-object v4

    invoke-virtual {v1, v4, v2, v3}, Lcom/google/android/location/k/d;->a(Lcom/google/android/location/e/ai;J)V

    :cond_2
    iget-object v1, v0, Lcom/google/android/location/k/a;->e:Lcom/google/android/location/k/d;

    if-eqz v1, :cond_3

    iget-object v0, v0, Lcom/google/android/location/k/a;->e:Lcom/google/android/location/k/d;

    invoke-virtual {p1}, Lcom/google/android/location/e/ah;->u()Lcom/google/android/location/e/ai;

    move-result-object v1

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/location/k/d;->a(Lcom/google/android/location/e/ai;J)V

    .line 362
    :cond_3
    iget-object v0, p0, Lcom/google/android/location/ap;->k:Lcom/google/android/location/av;

    invoke-virtual {v0, p1}, Lcom/google/android/location/av;->a(Lcom/google/android/location/e/ah;)V

    .line 363
    iget-object v0, p0, Lcom/google/android/location/ap;->g:Lcom/google/android/location/a;

    invoke-virtual {v0, p1}, Lcom/google/android/location/a;->a(Lcom/google/android/location/e/ah;)V

    .line 364
    return-void
.end method

.method public final a(Lcom/google/android/location/e/bh;)V
    .locals 1

    .prologue
    .line 411
    iget-object v0, p0, Lcom/google/android/location/ap;->g:Lcom/google/android/location/a;

    invoke-virtual {v0, p1}, Lcom/google/android/location/a;->a(Lcom/google/android/location/e/bh;)V

    .line 412
    return-void
.end method

.method public final a(Lcom/google/android/location/e/h;)V
    .locals 3

    .prologue
    .line 240
    iget-object v0, p0, Lcom/google/android/location/ap;->a:Lcom/google/android/location/os/bi;

    invoke-interface {v0}, Lcom/google/android/location/os/bi;->c()Lcom/google/android/location/j/b;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/location/j/b;->c()J

    move-result-wide v0

    .line 241
    iget-object v2, p0, Lcom/google/android/location/ap;->k:Lcom/google/android/location/av;

    invoke-virtual {v2, p1}, Lcom/google/android/location/av;->a(Lcom/google/android/location/e/h;)V

    .line 242
    iget-object v2, p0, Lcom/google/android/location/ap;->n:Lcom/google/android/location/e/g;

    invoke-virtual {v2, v0, v1, p1}, Lcom/google/android/location/e/g;->a(JLcom/google/android/location/e/h;)V

    .line 243
    iget-object v0, p0, Lcom/google/android/location/ap;->f:Lcom/google/android/location/am;

    iget-object v1, p0, Lcom/google/android/location/ap;->a:Lcom/google/android/location/os/bi;

    invoke-interface {v1}, Lcom/google/android/location/os/bi;->c()Lcom/google/android/location/j/b;

    move-result-object v1

    invoke-virtual {v0, v1, p1}, Lcom/google/android/location/am;->a(Lcom/google/android/location/j/b;Lcom/google/android/location/e/h;)V

    .line 244
    iget-object v0, p0, Lcom/google/android/location/ap;->g:Lcom/google/android/location/a;

    invoke-virtual {v0, p1}, Lcom/google/android/location/a;->a(Lcom/google/android/location/e/h;)V

    .line 245
    return-void
.end method

.method public final a(Lcom/google/android/location/j/k;)V
    .locals 4

    .prologue
    .line 204
    sget-object v0, Lcom/google/android/location/aq;->a:[I

    invoke-virtual {p1}, Lcom/google/android/location/j/k;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 230
    :cond_0
    :goto_0
    return-void

    .line 206
    :pswitch_0
    iget-object v0, p0, Lcom/google/android/location/ap;->f:Lcom/google/android/location/am;

    iget-object v1, p0, Lcom/google/android/location/ap;->a:Lcom/google/android/location/os/bi;

    invoke-interface {v1}, Lcom/google/android/location/os/bi;->c()Lcom/google/android/location/j/b;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/location/am;->a(Lcom/google/android/location/j/b;)V

    goto :goto_0

    .line 221
    :pswitch_1
    iget-object v0, p0, Lcom/google/android/location/ap;->g:Lcom/google/android/location/a;

    invoke-virtual {v0, p1}, Lcom/google/android/location/a;->a(Lcom/google/android/location/j/k;)V

    goto :goto_0

    .line 224
    :pswitch_2
    iget-object v0, p0, Lcom/google/android/location/ap;->d:Lcom/google/android/location/b/c;

    if-eqz v0, :cond_0

    .line 225
    iget-object v0, p0, Lcom/google/android/location/ap;->d:Lcom/google/android/location/b/c;

    invoke-virtual {v0, p1}, Lcom/google/android/location/b/c;->a(Lcom/google/android/location/j/k;)V

    goto :goto_0

    .line 229
    :pswitch_3
    iget-object v0, p0, Lcom/google/android/location/ap;->m:Lcom/google/android/location/ba;

    sget-object v1, Lcom/google/android/location/j/k;->h:Lcom/google/android/location/j/k;

    if-ne p1, v1, :cond_0

    sget-boolean v1, Lcom/google/android/location/i/a;->b:Z

    if-eqz v1, :cond_1

    const-string v1, "SensorUploader"

    const-string v2, "alarmRing"

    invoke-static {v1, v2}, Lcom/google/android/location/o/a/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    const-wide/16 v2, -0x1

    iput-wide v2, v0, Lcom/google/android/location/ba;->b:J

    invoke-virtual {v0}, Lcom/google/android/location/ba;->a()V

    goto :goto_0

    .line 204
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public final a(Lcom/google/android/location/j/k;Lcom/google/android/location/e/b;)V
    .locals 0

    .prologue
    .line 407
    return-void
.end method

.method public final a(Lcom/google/android/location/os/aw;)V
    .locals 2

    .prologue
    .line 254
    invoke-interface {p1}, Lcom/google/android/location/os/aw;->h()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {p1}, Lcom/google/android/location/os/aw;->d()I

    move-result v0

    const/4 v1, 0x3

    if-ge v0, v1, :cond_1

    .line 255
    sget-boolean v0, Lcom/google/android/location/i/a;->c:Z

    if-eqz v0, :cond_0

    const-string v0, "NetworkProvider"

    const-string v1, "ignoring GPS location lacking satellites for 2d fix"

    invoke-static {v0, v1}, Lcom/google/android/location/o/a/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 260
    :cond_0
    :goto_0
    return-void

    .line 258
    :cond_1
    iget-object v0, p0, Lcom/google/android/location/ap;->f:Lcom/google/android/location/am;

    invoke-virtual {v0, p1}, Lcom/google/android/location/am;->a(Lcom/google/android/location/os/aw;)V

    .line 259
    iget-object v0, p0, Lcom/google/android/location/ap;->g:Lcom/google/android/location/a;

    invoke-virtual {v0, p1}, Lcom/google/android/location/a;->a(Lcom/google/android/location/os/aw;)V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/location/os/bk;Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 394
    iget-object v0, p0, Lcom/google/android/location/ap;->g:Lcom/google/android/location/a;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/location/a;->a(Lcom/google/android/location/os/bk;Ljava/lang/Object;)V

    .line 395
    return-void
.end method

.method public final a(Lcom/google/p/a/b/b/a;)V
    .locals 1

    .prologue
    .line 347
    iget-object v0, p0, Lcom/google/android/location/ap;->d:Lcom/google/android/location/b/c;

    if-eqz v0, :cond_0

    .line 348
    iget-object v0, p0, Lcom/google/android/location/ap;->d:Lcom/google/android/location/b/c;

    invoke-virtual {v0, p1}, Lcom/google/android/location/b/c;->a(Lcom/google/p/a/b/b/a;)V

    .line 350
    :cond_0
    return-void
.end method

.method public final synthetic a(Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lcom/google/android/location/ap;->g:Lcom/google/android/location/a;

    invoke-virtual {v0}, Lcom/google/android/location/a;->g()V

    iget-object v0, p0, Lcom/google/android/location/ap;->m:Lcom/google/android/location/ba;

    invoke-virtual {v0}, Lcom/google/android/location/ba;->a()V

    return-void
.end method

.method public final a(Z)V
    .locals 4

    .prologue
    .line 329
    iget-object v0, p0, Lcom/google/android/location/ap;->n:Lcom/google/android/location/e/g;

    iget-object v1, p0, Lcom/google/android/location/ap;->a:Lcom/google/android/location/os/bi;

    invoke-interface {v1}, Lcom/google/android/location/os/bi;->c()Lcom/google/android/location/j/b;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/location/j/b;->c()J

    move-result-wide v2

    invoke-virtual {v0, p1, v2, v3}, Lcom/google/android/location/e/g;->a(ZJ)V

    .line 330
    iget-object v0, p0, Lcom/google/android/location/ap;->k:Lcom/google/android/location/av;

    invoke-virtual {v0, p1}, Lcom/google/android/location/av;->b(Z)V

    .line 331
    iget-object v0, p0, Lcom/google/android/location/ap;->g:Lcom/google/android/location/a;

    invoke-virtual {v0, p1}, Lcom/google/android/location/a;->b(Z)V

    .line 332
    return-void
.end method

.method public final a(ZLjava/lang/String;)V
    .locals 1

    .prologue
    .line 389
    iget-object v0, p0, Lcom/google/android/location/ap;->k:Lcom/google/android/location/av;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/location/av;->a(ZLjava/lang/String;)V

    .line 390
    return-void
.end method

.method public final a(ZZ)V
    .locals 2

    .prologue
    .line 316
    iget-object v0, p0, Lcom/google/android/location/ap;->f:Lcom/google/android/location/am;

    iget-object v1, p0, Lcom/google/android/location/ap;->a:Lcom/google/android/location/os/bi;

    invoke-interface {v1}, Lcom/google/android/location/os/bi;->c()Lcom/google/android/location/j/b;

    move-result-object v1

    invoke-virtual {v0, v1, p1}, Lcom/google/android/location/am;->b(Lcom/google/android/location/j/b;Z)V

    .line 317
    iget-object v0, p0, Lcom/google/android/location/ap;->g:Lcom/google/android/location/a;

    invoke-virtual {v0, p1}, Lcom/google/android/location/a;->a(Z)V

    .line 318
    iget-object v0, p0, Lcom/google/android/location/ap;->m:Lcom/google/android/location/ba;

    iput-boolean p2, v0, Lcom/google/android/location/ba;->c:Z

    invoke-virtual {v0}, Lcom/google/android/location/ba;->a()V

    .line 319
    return-void
.end method

.method public final a(ZZI)V
    .locals 4

    .prologue
    .line 354
    iget-object v0, p0, Lcom/google/android/location/ap;->e:Lcom/google/android/location/k/a;

    iput-boolean p2, v0, Lcom/google/android/location/k/a;->g:Z

    .line 355
    iget-object v0, p0, Lcom/google/android/location/ap;->m:Lcom/google/android/location/ba;

    iput-boolean p1, v0, Lcom/google/android/location/ba;->d:Z

    iget-object v1, v0, Lcom/google/android/location/ba;->a:Lcom/google/android/location/os/bi;

    invoke-interface {v1}, Lcom/google/android/location/os/bi;->c()Lcom/google/android/location/j/b;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/location/j/b;->c()J

    move-result-wide v2

    iput-wide v2, v0, Lcom/google/android/location/ba;->e:J

    invoke-virtual {v0}, Lcom/google/android/location/ba;->a()V

    .line 356
    iget-object v0, p0, Lcom/google/android/location/ap;->f:Lcom/google/android/location/am;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/location/am;->a(ZZI)V

    .line 357
    return-void
.end method

.method public final a([Lcom/google/android/location/e/bi;Z)V
    .locals 12

    .prologue
    const/4 v6, 0x0

    .line 268
    move v0, v6

    :goto_0
    array-length v1, p1

    if-ge v0, v1, :cond_2

    .line 269
    aget-object v2, p1, v0

    .line 270
    invoke-virtual {v2}, Lcom/google/android/location/e/bi;->a()I

    move-result v3

    .line 271
    iget-wide v4, v2, Lcom/google/android/location/e/bi;->a:J

    move v1, v6

    .line 273
    :goto_1
    if-ge v1, v3, :cond_1

    .line 274
    invoke-virtual {v2, v1}, Lcom/google/android/location/e/bi;->a(I)Lcom/google/android/location/e/bc;

    move-result-object v7

    .line 276
    iget-wide v8, v7, Lcom/google/android/location/e/l;->a:J

    sub-long v8, v4, v8

    invoke-static {v8, v9}, Ljava/lang/Math;->abs(J)J

    move-result-wide v8

    const-wide/32 v10, 0xea60

    cmp-long v8, v8, v10

    if-ltz v8, :cond_0

    .line 277
    sget-boolean v8, Lcom/google/android/location/i/a;->b:Z

    if-eqz v8, :cond_0

    const-string v8, "NetworkProvider"

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "Stale scan: scanTimestamp is "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " and device timestamp is "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget-wide v10, v7, Lcom/google/android/location/e/l;->a:J

    invoke-virtual {v9, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " and diff is "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget-wide v10, v7, Lcom/google/android/location/e/l;->a:J

    sub-long v10, v4, v10

    invoke-virtual {v9, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v8, v7}, Lcom/google/android/location/o/a/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 273
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 268
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 289
    :cond_2
    if-nez p2, :cond_3

    array-length v0, p1

    const/4 v1, 0x1

    if-ne v0, v1, :cond_3

    .line 290
    aget-object v0, p1, v6

    if-eqz v0, :cond_3

    .line 291
    aget-object v0, p1, v6

    .line 293
    iget-object v1, p0, Lcom/google/android/location/ap;->o:Lcom/google/android/location/e/bi;

    invoke-virtual {v0, v1}, Lcom/google/android/location/e/bi;->a(Lcom/google/android/location/e/bi;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 295
    new-instance v1, Lcom/google/android/location/e/bi;

    iget-wide v2, v0, Lcom/google/android/location/e/bi;->a:J

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    invoke-direct {v1, v2, v3, v0}, Lcom/google/android/location/e/bi;-><init>(JLjava/util/ArrayList;)V

    aput-object v1, p1, v6

    .line 303
    :cond_3
    :goto_2
    iget-object v0, p0, Lcom/google/android/location/ap;->f:Lcom/google/android/location/am;

    iget-object v1, p0, Lcom/google/android/location/ap;->a:Lcom/google/android/location/os/bi;

    invoke-interface {v1}, Lcom/google/android/location/os/bi;->c()Lcom/google/android/location/j/b;

    move-result-object v1

    invoke-virtual {v0, v1, p1}, Lcom/google/android/location/am;->a(Lcom/google/android/location/j/b;[Lcom/google/android/location/e/bi;)V

    .line 304
    iget-object v0, p0, Lcom/google/android/location/ap;->g:Lcom/google/android/location/a;

    aget-object v1, p1, v6

    invoke-virtual {v0, v1}, Lcom/google/android/location/a;->a(Lcom/google/android/location/e/bi;)V

    .line 307
    if-eqz p2, :cond_4

    if-eqz p1, :cond_4

    sget-object v0, Lcom/google/android/location/d/a;->e:Lcom/google/android/gms/common/a/d;

    invoke-static {v0}, Lcom/google/android/location/d/a;->a(Lcom/google/android/gms/common/a/d;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 309
    iget-object v0, p0, Lcom/google/android/location/ap;->a:Lcom/google/android/location/os/bi;

    const-string v1, "nlp"

    const-string v2, "wifi_batch"

    const-string v3, "count"

    array-length v4, p1

    int-to-long v4, v4

    invoke-interface/range {v0 .. v6}, Lcom/google/android/location/os/bi;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JZ)V

    .line 312
    :cond_4
    return-void

    .line 298
    :cond_5
    iput-object v0, p0, Lcom/google/android/location/ap;->o:Lcom/google/android/location/e/bi;

    goto :goto_2
.end method

.method public final b()V
    .locals 0

    .prologue
    .line 403
    return-void
.end method

.method public final b(Lcom/google/p/a/b/b/a;)V
    .locals 1

    .prologue
    .line 249
    iget-object v0, p0, Lcom/google/android/location/ap;->g:Lcom/google/android/location/a;

    invoke-virtual {v0, p1}, Lcom/google/android/location/a;->a(Lcom/google/p/a/b/b/a;)V

    .line 250
    return-void
.end method

.method public final b(Z)V
    .locals 0

    .prologue
    .line 421
    return-void
.end method

.method public final c()V
    .locals 0

    .prologue
    .line 168
    return-void
.end method

.method public final c(Z)V
    .locals 2

    .prologue
    .line 172
    iget-object v0, p0, Lcom/google/android/location/ap;->k:Lcom/google/android/location/av;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/location/av;->c(Z)V

    .line 173
    iget-object v0, p0, Lcom/google/android/location/ap;->g:Lcom/google/android/location/a;

    invoke-virtual {v0}, Lcom/google/android/location/a;->f()V

    .line 174
    iget-object v0, p0, Lcom/google/android/location/ap;->f:Lcom/google/android/location/am;

    invoke-virtual {v0}, Lcom/google/android/location/am;->a()V

    .line 175
    iget-object v0, p0, Lcom/google/android/location/ap;->d:Lcom/google/android/location/b/c;

    invoke-virtual {v0}, Lcom/google/android/location/b/c;->b()V

    .line 176
    iget-object v0, p0, Lcom/google/android/location/ap;->k:Lcom/google/android/location/av;

    invoke-virtual {v0, p0}, Lcom/google/android/location/av;->b(Lcom/google/android/location/o/e;)V

    .line 177
    if-eqz p1, :cond_1

    .line 178
    iget-object v0, p0, Lcom/google/android/location/ap;->b:Lcom/google/android/location/b/ak;

    iget-object v1, p0, Lcom/google/android/location/ap;->a:Lcom/google/android/location/os/bi;

    invoke-interface {v1}, Lcom/google/android/location/os/bi;->e()Lcom/google/android/location/j/e;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/location/b/ak;->a(Lcom/google/android/location/j/e;)V

    .line 179
    iget-object v0, p0, Lcom/google/android/location/ap;->i:Lcom/google/android/location/b/ad;

    if-eqz v0, :cond_0

    .line 180
    iget-object v0, p0, Lcom/google/android/location/ap;->i:Lcom/google/android/location/b/ad;

    invoke-virtual {v0}, Lcom/google/android/location/b/ad;->b()V

    .line 191
    :cond_0
    :goto_0
    invoke-static {}, Lcom/google/android/location/av;->a()V

    .line 192
    return-void

    .line 183
    :cond_1
    iget-object v0, p0, Lcom/google/android/location/ap;->i:Lcom/google/android/location/b/ad;

    if-eqz v0, :cond_2

    .line 184
    iget-object v0, p0, Lcom/google/android/location/ap;->i:Lcom/google/android/location/b/ad;

    iget-object v1, v0, Lcom/google/android/location/b/ad;->a:Lcom/google/android/location/b/m;

    iget-object v1, v1, Lcom/google/android/location/b/m;->c:Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    iget-object v1, v0, Lcom/google/android/location/b/ad;->b:Lcom/google/android/location/b/f;

    invoke-virtual {v1}, Lcom/google/android/location/b/f;->c()V

    iget-object v0, v0, Lcom/google/android/location/b/ad;->c:Lcom/google/android/location/b/f;

    invoke-virtual {v0}, Lcom/google/android/location/b/f;->c()V

    .line 186
    :cond_2
    iget-object v0, p0, Lcom/google/android/location/ap;->j:Lcom/google/android/location/b/ai;

    if-eqz v0, :cond_3

    .line 187
    iget-object v0, p0, Lcom/google/android/location/ap;->j:Lcom/google/android/location/b/ai;

    invoke-interface {v0}, Lcom/google/android/location/b/ai;->c()V

    .line 189
    :cond_3
    iget-object v0, p0, Lcom/google/android/location/ap;->a:Lcom/google/android/location/os/bi;

    invoke-interface {v0}, Lcom/google/android/location/os/bi;->e()Lcom/google/android/location/j/e;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/location/v;->a(Lcom/google/android/location/j/e;)Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    goto :goto_0
.end method

.method public final d()V
    .locals 8

    .prologue
    const/4 v3, 0x1

    .line 148
    iget-object v0, p0, Lcom/google/android/location/ap;->b:Lcom/google/android/location/b/ak;

    iget-object v1, p0, Lcom/google/android/location/ap;->a:Lcom/google/android/location/os/bi;

    invoke-interface {v1}, Lcom/google/android/location/os/bi;->e()Lcom/google/android/location/j/e;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/location/ap;->a:Lcom/google/android/location/os/bi;

    invoke-interface {v2}, Lcom/google/android/location/os/bi;->c()Lcom/google/android/location/j/b;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/location/b/ak;->a(Lcom/google/android/location/j/e;Lcom/google/android/location/j/b;)V

    .line 149
    iget-object v0, p0, Lcom/google/android/location/ap;->k:Lcom/google/android/location/av;

    invoke-virtual {v0, v3}, Lcom/google/android/location/av;->c(Z)V

    .line 150
    iget-object v0, p0, Lcom/google/android/location/ap;->c:Lcom/google/android/location/b/ao;

    if-eqz v0, :cond_1

    .line 151
    iget-object v1, p0, Lcom/google/android/location/ap;->c:Lcom/google/android/location/b/ao;

    :try_start_0
    iget-object v0, v1, Lcom/google/android/location/b/ao;->g:Lcom/google/android/location/e/au;

    invoke-virtual {v0}, Lcom/google/android/location/e/au;->a()Lcom/google/p/a/b/b/a;

    move-result-object v0

    sget-boolean v2, Lcom/google/android/location/i/a;->b:Z

    if-eqz v2, :cond_0

    const-string v2, "SeenDevicesCache"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Actual file version: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, v1, Lcom/google/android/location/b/ao;->g:Lcom/google/android/location/e/au;

    invoke-virtual {v4}, Lcom/google/android/location/e/au;->b()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/location/o/a/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v1, v0}, Lcom/google/android/location/b/ao;->b(Lcom/google/p/a/b/b/a;)V

    invoke-virtual {v1}, Lcom/google/android/location/b/ao;->c()V

    sget-boolean v0, Lcom/google/android/location/i/a;->b:Z

    if-eqz v0, :cond_1

    const-string v0, "SeenDevicesCache"

    const-string v2, "Loaded %d entries, last refresh: %d"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, v1, Lcom/google/android/location/b/ao;->b:Lcom/google/android/location/b/ap;

    invoke-virtual {v5}, Lcom/google/android/location/b/ap;->size()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    iget-wide v6, v1, Lcom/google/android/location/b/ao;->e:J

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/google/android/location/o/a/a;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 153
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/google/android/location/ap;->d:Lcom/google/android/location/b/c;

    if-eqz v0, :cond_2

    .line 155
    iget-object v0, p0, Lcom/google/android/location/ap;->d:Lcom/google/android/location/b/c;

    invoke-virtual {v0}, Lcom/google/android/location/b/c;->a()V

    .line 158
    :cond_2
    iget-object v0, p0, Lcom/google/android/location/ap;->h:Lcom/google/android/location/be;

    iget-object v1, p0, Lcom/google/android/location/ap;->a:Lcom/google/android/location/os/bi;

    invoke-interface {v1}, Lcom/google/android/location/os/bi;->c()Lcom/google/android/location/j/b;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/location/j/b;->c()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/google/android/location/be;->a(J)V

    .line 159
    iget-object v0, p0, Lcom/google/android/location/ap;->i:Lcom/google/android/location/b/ad;

    if-eqz v0, :cond_3

    .line 160
    iget-object v0, p0, Lcom/google/android/location/ap;->i:Lcom/google/android/location/b/ad;

    invoke-virtual {v0}, Lcom/google/android/location/b/ad;->a()V

    .line 163
    :cond_3
    iget-object v1, p0, Lcom/google/android/location/ap;->l:Lcom/google/android/location/v;

    iget-object v2, v1, Lcom/google/android/location/v;->a:Ljava/lang/Object;

    monitor-enter v2

    :try_start_1
    iget-object v0, v1, Lcom/google/android/location/v;->b:Lcom/google/android/location/e/au;

    invoke-virtual {v0}, Lcom/google/android/location/e/au;->a()Lcom/google/p/a/b/b/a;

    move-result-object v0

    iput-object v0, v1, Lcom/google/android/location/v;->c:Lcom/google/p/a/b/b/a;

    sget-boolean v0, Lcom/google/android/location/i/a;->b:Z

    if-eqz v0, :cond_4

    const-string v0, "CollectorState"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Actual file version: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, v1, Lcom/google/android/location/v;->b:Lcom/google/android/location/e/au;

    invoke-virtual {v4}, Lcom/google/android/location/e/au;->b()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Lcom/google/android/location/o/a/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_4
    invoke-virtual {v1}, Lcom/google/android/location/v;->h()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_5
    :goto_1
    :try_start_2
    sget-boolean v0, Lcom/google/android/location/i/a;->b:Z

    if-eqz v0, :cond_6

    const-string v0, "CollectorState"

    const-string v3, "Loaded: %s"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-virtual {v1}, Lcom/google/android/location/v;->toString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/location/o/a/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_6
    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    return-void

    .line 151
    :catch_0
    move-exception v0

    invoke-virtual {v1}, Lcom/google/android/location/b/ao;->b()V

    sget-boolean v1, Lcom/google/android/location/i/a;->b:Z

    if-eqz v1, :cond_1

    const-string v1, "SeenDevicesCache"

    invoke-virtual {v0}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/google/android/location/o/a/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 163
    :catch_1
    move-exception v0

    :try_start_3
    sget-boolean v3, Lcom/google/android/location/i/a;->b:Z

    if-eqz v3, :cond_5

    const-string v3, "CollectorState"

    invoke-virtual {v0}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, Lcom/google/android/location/o/a/a;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0
.end method

.method public final d(Z)V
    .locals 2

    .prologue
    .line 336
    iget-object v0, p0, Lcom/google/android/location/ap;->f:Lcom/google/android/location/am;

    iget-object v1, p0, Lcom/google/android/location/ap;->a:Lcom/google/android/location/os/bi;

    invoke-interface {v1}, Lcom/google/android/location/os/bi;->c()Lcom/google/android/location/j/b;

    move-result-object v1

    invoke-virtual {v0, v1, p1}, Lcom/google/android/location/am;->a(Lcom/google/android/location/j/b;Z)V

    .line 337
    return-void
.end method

.method public final e()V
    .locals 1

    .prologue
    .line 416
    iget-object v0, p0, Lcom/google/android/location/ap;->g:Lcom/google/android/location/a;

    invoke-virtual {v0}, Lcom/google/android/location/a;->h()V

    .line 417
    return-void
.end method

.method public final e(Z)V
    .locals 3

    .prologue
    .line 341
    sget-boolean v0, Lcom/google/android/location/i/a;->b:Z

    if-eqz v0, :cond_0

    const-string v0, "NetworkProvider"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Full collection mode changed: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/location/o/a/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 342
    :cond_0
    iget-object v0, p0, Lcom/google/android/location/ap;->g:Lcom/google/android/location/a;

    invoke-virtual {v0, p1}, Lcom/google/android/location/a;->c(Z)V

    .line 343
    return-void
.end method
