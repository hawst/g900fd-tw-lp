.class public final Lcom/google/android/location/copresence/ag;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final a:Ljava/util/LinkedList;

.field private static final b:J


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 49
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    sput-object v0, Lcom/google/android/location/copresence/ag;->a:Ljava/util/LinkedList;

    .line 55
    sget-object v0, Ljava/util/concurrent/TimeUnit;->HOURS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0x1

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    sput-wide v0, Lcom/google/android/location/copresence/ag;->b:J

    return-void
.end method

.method public static a(Ljava/lang/String;)I
    .locals 2

    .prologue
    .line 101
    invoke-static {p0}, Lcom/google/android/location/copresence/ag;->g(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 102
    const/4 v1, 0x2

    invoke-static {v1, v0}, Lcom/google/android/location/copresence/ag;->a(ILjava/lang/String;)V

    .line 103
    const-string v1, "copresGcore"

    invoke-static {v1, v0}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public static a(Ljava/lang/String;Ljava/lang/Throwable;)I
    .locals 2

    .prologue
    .line 139
    invoke-static {p0}, Lcom/google/android/location/copresence/ag;->g(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 140
    const/4 v1, 0x6

    invoke-static {v1, v0}, Lcom/google/android/location/copresence/ag;->a(ILjava/lang/String;)V

    .line 141
    const-string v1, "copresGcore"

    invoke-static {v1, v0, p1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    move-result v0

    return v0
.end method

.method public static varargs a(Ljava/lang/String;[Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 107
    invoke-static {p0, p1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->b(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public static a(Lcom/google/android/location/copresence/a/a;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 164
    if-nez p0, :cond_0

    .line 165
    const/4 v0, 0x0

    .line 167
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/location/copresence/a/a;->a:Landroid/accounts/Account;

    iget-object v0, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->f(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(Ljava/lang/String;Lcom/google/android/gms/location/copresence/CopresenceSettings;)Ljava/lang/String;
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 206
    if-nez p1, :cond_0

    .line 230
    :goto_0
    return-object v1

    .line 210
    :cond_0
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v0, "CopresenceSettings [mAccount="

    invoke-direct {v3, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 211
    invoke-static {p0}, Lcom/google/android/location/copresence/ag;->f(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 212
    const-string v0, ", hasEnabled="

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 213
    invoke-virtual {p1}, Lcom/google/android/gms/location/copresence/CopresenceSettings;->b()Z

    move-result v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 214
    const-string v0, ", isEnabled="

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 215
    invoke-virtual {p1}, Lcom/google/android/gms/location/copresence/CopresenceSettings;->a()Z

    move-result v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 217
    const-string v0, ", namedAcls={"

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 218
    invoke-virtual {p1}, Lcom/google/android/gms/location/copresence/CopresenceSettings;->c()[Lcom/google/android/gms/location/copresence/internal/CopresenceNamedAcl;

    move-result-object v4

    .line 219
    if-eqz v4, :cond_4

    .line 220
    array-length v5, v4

    const/4 v0, 0x0

    move v2, v0

    :goto_1
    if-ge v2, v5, :cond_4

    aget-object v0, v4, v2

    .line 221
    const-string v6, "<"

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 222
    invoke-virtual {v0}, Lcom/google/android/gms/location/copresence/internal/CopresenceNamedAcl;->a()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 223
    const-string v6, ", "

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 224
    invoke-virtual {v0}, Lcom/google/android/gms/location/copresence/internal/CopresenceNamedAcl;->b()Lcom/google/android/gms/common/people/data/Audience;

    move-result-object v0

    if-nez v0, :cond_1

    move-object v0, v1

    :goto_2
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 225
    const-string v0, ">, "

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 220
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    .line 224
    :cond_1
    invoke-virtual {v0}, Lcom/google/android/gms/common/people/data/Audience;->a()Ljava/util/List;

    move-result-object v0

    if-nez v0, :cond_2

    move-object v0, v1

    goto :goto_2

    :cond_2
    const-string v6, "copresGcore"

    const/4 v7, 0x2

    invoke-static {v6, v7}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v6

    if-nez v6, :cond_3

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "acl: size="

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    :cond_3
    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    .line 228
    :cond_4
    const-string v0, "}]"

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 230
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_0
.end method

.method private static a(ILjava/lang/String;)V
    .locals 3

    .prologue
    .line 60
    sget-object v0, Lcom/google/android/location/copresence/k;->I:Lcom/google/android/location/copresence/l;

    invoke-virtual {v0}, Lcom/google/android/location/copresence/l;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_0

    .line 68
    :goto_0
    return-void

    .line 63
    :cond_0
    sget-object v1, Lcom/google/android/location/copresence/ag;->a:Ljava/util/LinkedList;

    monitor-enter v1

    .line 64
    :try_start_0
    sget-object v0, Lcom/google/android/location/copresence/ag;->a:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->size()I

    move-result v0

    const/16 v2, 0x64

    if-lt v0, v2, :cond_1

    .line 65
    sget-object v0, Lcom/google/android/location/copresence/ag;->a:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->removeFirst()Ljava/lang/Object;

    .line 67
    :cond_1
    sget-object v0, Lcom/google/android/location/copresence/ag;->a:Ljava/util/LinkedList;

    new-instance v2, Lcom/google/android/location/copresence/aw;

    invoke-direct {v2, p0, p1}, Lcom/google/android/location/copresence/aw;-><init>(ILjava/lang/String;)V

    invoke-virtual {v0, v2}, Ljava/util/LinkedList;->addLast(Ljava/lang/Object;)V

    .line 68
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static a(Ljava/io/PrintWriter;)V
    .locals 10

    .prologue
    .line 75
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    .line 76
    const-string v0, "--Begin Copresence Log Messages--"

    invoke-virtual {p0, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 77
    sget-object v1, Lcom/google/android/location/copresence/ag;->a:Ljava/util/LinkedList;

    monitor-enter v1

    .line 78
    :try_start_0
    sget-object v0, Lcom/google/android/location/copresence/ag;->a:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/copresence/aw;

    .line 79
    iget-wide v6, v0, Lcom/google/android/location/copresence/aw;->b:J

    sub-long v6, v2, v6

    sget-wide v8, Lcom/google/android/location/copresence/ag;->b:J

    cmp-long v5, v6, v8

    if-gez v5, :cond_0

    .line 80
    invoke-virtual {v0}, Lcom/google/android/location/copresence/aw;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 83
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    :cond_1
    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 84
    const-string v0, "--End Copresence Log Messages--"

    invoke-virtual {p0, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 85
    return-void
.end method

.method public static a(I)Z
    .locals 1

    .prologue
    .line 88
    const-string v0, "copresGcore"

    invoke-static {v0, p0}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    return v0
.end method

.method public static b(Ljava/lang/String;)I
    .locals 2

    .prologue
    .line 111
    invoke-static {p0}, Lcom/google/android/location/copresence/ag;->g(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 112
    const/4 v1, 0x3

    invoke-static {v1, v0}, Lcom/google/android/location/copresence/ag;->a(ILjava/lang/String;)V

    .line 113
    const-string v1, "copresGcore"

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public static b(Ljava/lang/String;Ljava/lang/Throwable;)I
    .locals 2

    .prologue
    .line 150
    invoke-static {p0}, Lcom/google/android/location/copresence/ag;->g(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 151
    const/4 v1, 0x6

    invoke-static {v1, v0}, Lcom/google/android/location/copresence/ag;->a(ILjava/lang/String;)V

    .line 152
    const-string v1, "copresGcore"

    invoke-static {v1, v0, p1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    move-result v0

    .line 153
    invoke-static {}, Lcom/google/android/location/copresence/f/b;->b()Lcom/google/ac/b/c/g;

    move-result-object v1

    iget-object v1, v1, Lcom/google/ac/b/c/g;->d:Lcom/google/ac/b/c/h;

    iget-object v1, v1, Lcom/google/ac/b/c/h;->b:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 154
    new-instance v0, Ljava/lang/RuntimeException;

    invoke-direct {v0, p1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v0

    .line 156
    :cond_0
    return v0
.end method

.method public static b(I)Ljava/lang/String;
    .locals 1

    .prologue
    .line 248
    packed-switch p0, :pswitch_data_0

    .line 259
    :pswitch_0
    const-string v0, "unknown"

    :goto_0
    return-object v0

    .line 250
    :pswitch_1
    const-string v0, "a"

    goto :goto_0

    .line 252
    :pswitch_2
    const-string v0, "b"

    goto :goto_0

    .line 254
    :pswitch_3
    const-string v0, "c"

    goto :goto_0

    .line 257
    :pswitch_4
    const-string v0, "d"

    goto :goto_0

    .line 248
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_0
        :pswitch_0
        :pswitch_4
    .end packed-switch
.end method

.method public static c(Ljava/lang/String;)I
    .locals 2

    .prologue
    .line 121
    invoke-static {p0}, Lcom/google/android/location/copresence/ag;->g(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 122
    const/4 v1, 0x4

    invoke-static {v1, v0}, Lcom/google/android/location/copresence/ag;->a(ILjava/lang/String;)V

    .line 123
    const-string v1, "copresGcore"

    invoke-static {v1, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public static c(I)Ljava/lang/String;
    .locals 1

    .prologue
    .line 264
    packed-switch p0, :pswitch_data_0

    .line 275
    :pswitch_0
    const-string v0, "unknown"

    :goto_0
    return-object v0

    .line 266
    :pswitch_1
    const-string v0, "audio"

    goto :goto_0

    .line 268
    :pswitch_2
    const-string v0, "bluetooth"

    goto :goto_0

    .line 270
    :pswitch_3
    const-string v0, "ble"

    goto :goto_0

    .line 273
    :pswitch_4
    const-string v0, "wifi"

    goto :goto_0

    .line 264
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_0
        :pswitch_0
        :pswitch_4
    .end packed-switch
.end method

.method public static d(Ljava/lang/String;)I
    .locals 2

    .prologue
    .line 127
    invoke-static {p0}, Lcom/google/android/location/copresence/ag;->g(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 128
    const/4 v1, 0x5

    invoke-static {v1, v0}, Lcom/google/android/location/copresence/ag;->a(ILjava/lang/String;)V

    .line 129
    const-string v1, "copresGcore"

    invoke-static {v1, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public static d(I)Ljava/lang/String;
    .locals 2

    .prologue
    .line 295
    packed-switch p0, :pswitch_data_0

    .line 305
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "UNKNOWN: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    .line 297
    :pswitch_0
    const-string v0, "OFF"

    goto :goto_0

    .line 299
    :pswitch_1
    const-string v0, "ON"

    goto :goto_0

    .line 301
    :pswitch_2
    const-string v0, "TURNING ON"

    goto :goto_0

    .line 303
    :pswitch_3
    const-string v0, "TURNING OFF"

    goto :goto_0

    .line 295
    nop

    :pswitch_data_0
    .packed-switch 0xa
        :pswitch_0
        :pswitch_2
        :pswitch_1
        :pswitch_3
    .end packed-switch
.end method

.method public static e(Ljava/lang/String;)I
    .locals 2

    .prologue
    .line 133
    invoke-static {p0}, Lcom/google/android/location/copresence/ag;->g(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 134
    const/4 v1, 0x6

    invoke-static {v1, v0}, Lcom/google/android/location/copresence/ag;->a(ILjava/lang/String;)V

    .line 135
    const-string v1, "copresGcore"

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public static f(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 175
    if-nez p0, :cond_1

    .line 176
    const/4 p0, 0x0

    .line 178
    :cond_0
    :goto_0
    return-object p0

    :cond_1
    const-string v0, "copresGcore"

    const/4 v1, 0x2

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "account#"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Ljava/lang/String;->hashCode()I

    move-result v1

    rem-int/lit8 v1, v1, 0x14

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "#"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    goto :goto_0
.end method

.method private static g(Ljava/lang/String;)Ljava/lang/String;
    .locals 4

    .prologue
    const/16 v3, 0x190

    .line 92
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    if-le v0, v3, :cond_0

    .line 93
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    add-int/lit16 v0, v0, -0x190

    .line 94
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v2, 0x0

    invoke-virtual {p0, v2, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "...[TRUNCATED "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " chars]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    .line 97
    :cond_0
    const-string v0, "\n"

    invoke-virtual {p0, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "<<< "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ">>>"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    :cond_1
    return-object p0
.end method
