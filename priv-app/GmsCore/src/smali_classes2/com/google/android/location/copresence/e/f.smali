.class public final Lcom/google/android/location/copresence/e/f;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public final a:Lcom/google/android/location/copresence/e/g;

.field public final b:Lcom/google/android/gms/location/copresence/MessageFilter;

.field public final c:Lcom/google/android/gms/location/copresence/u;

.field final d:J

.field final synthetic e:Lcom/google/android/location/copresence/e/d;


# direct methods
.method public constructor <init>(Lcom/google/android/location/copresence/e/d;Lcom/google/android/location/copresence/e/g;Lcom/google/android/gms/location/copresence/MessageFilter;Lcom/google/android/gms/location/copresence/u;)V
    .locals 2

    .prologue
    .line 214
    iput-object p1, p0, Lcom/google/android/location/copresence/e/f;->e:Lcom/google/android/location/copresence/e/d;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 215
    invoke-static {p3}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/location/copresence/MessageFilter;

    iput-object v0, p0, Lcom/google/android/location/copresence/e/f;->b:Lcom/google/android/gms/location/copresence/MessageFilter;

    .line 216
    invoke-static {p2}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/copresence/e/g;

    iput-object v0, p0, Lcom/google/android/location/copresence/e/f;->a:Lcom/google/android/location/copresence/e/g;

    .line 217
    invoke-static {p4}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/location/copresence/u;

    iput-object v0, p0, Lcom/google/android/location/copresence/e/f;->c:Lcom/google/android/gms/location/copresence/u;

    .line 218
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/location/copresence/e/f;->d:J

    .line 219
    return-void
.end method


# virtual methods
.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 241
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Entry[listener="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/location/copresence/e/f;->a:Lcom/google/android/location/copresence/e/g;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
