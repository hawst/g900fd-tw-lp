.class public final Lcom/google/android/location/places/d/a/w;
.super Lcom/google/protobuf/nano/d;
.source "SourceFile"


# static fields
.field private static volatile h:[Lcom/google/android/location/places/d/a/w;


# instance fields
.field public a:Ljava/lang/Integer;

.field public b:Ljava/lang/String;

.field public c:Ljava/lang/Integer;

.field public d:Lcom/google/android/location/places/d/a/u;

.field public e:Lcom/google/android/location/places/d/a/v;

.field public f:Lcom/google/android/location/places/d/a/n;

.field public g:Lcom/google/android/location/places/d/a/m;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 53
    invoke-direct {p0}, Lcom/google/protobuf/nano/d;-><init>()V

    .line 54
    iput-object v0, p0, Lcom/google/android/location/places/d/a/w;->a:Ljava/lang/Integer;

    iput-object v0, p0, Lcom/google/android/location/places/d/a/w;->b:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/location/places/d/a/w;->c:Ljava/lang/Integer;

    iput-object v0, p0, Lcom/google/android/location/places/d/a/w;->d:Lcom/google/android/location/places/d/a/u;

    iput-object v0, p0, Lcom/google/android/location/places/d/a/w;->e:Lcom/google/android/location/places/d/a/v;

    iput-object v0, p0, Lcom/google/android/location/places/d/a/w;->f:Lcom/google/android/location/places/d/a/n;

    iput-object v0, p0, Lcom/google/android/location/places/d/a/w;->g:Lcom/google/android/location/places/d/a/m;

    iput-object v0, p0, Lcom/google/android/location/places/d/a/w;->unknownFieldData:Lcom/google/protobuf/nano/f;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/location/places/d/a/w;->cachedSize:I

    .line 55
    return-void
.end method

.method public static a()[Lcom/google/android/location/places/d/a/w;
    .locals 2

    .prologue
    .line 21
    sget-object v0, Lcom/google/android/location/places/d/a/w;->h:[Lcom/google/android/location/places/d/a/w;

    if-nez v0, :cond_1

    .line 22
    sget-object v1, Lcom/google/protobuf/nano/h;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 24
    :try_start_0
    sget-object v0, Lcom/google/android/location/places/d/a/w;->h:[Lcom/google/android/location/places/d/a/w;

    if-nez v0, :cond_0

    .line 25
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/android/location/places/d/a/w;

    sput-object v0, Lcom/google/android/location/places/d/a/w;->h:[Lcom/google/android/location/places/d/a/w;

    .line 27
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 29
    :cond_1
    sget-object v0, Lcom/google/android/location/places/d/a/w;->h:[Lcom/google/android/location/places/d/a/w;

    return-object v0

    .line 27
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 187
    invoke-super {p0}, Lcom/google/protobuf/nano/d;->computeSerializedSize()I

    move-result v0

    .line 188
    iget-object v1, p0, Lcom/google/android/location/places/d/a/w;->a:Ljava/lang/Integer;

    if-eqz v1, :cond_0

    .line 189
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/location/places/d/a/w;->a:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 192
    :cond_0
    iget-object v1, p0, Lcom/google/android/location/places/d/a/w;->b:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 193
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/location/places/d/a/w;->b:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 196
    :cond_1
    iget-object v1, p0, Lcom/google/android/location/places/d/a/w;->c:Ljava/lang/Integer;

    if-eqz v1, :cond_2

    .line 197
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/android/location/places/d/a/w;->c:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 200
    :cond_2
    iget-object v1, p0, Lcom/google/android/location/places/d/a/w;->d:Lcom/google/android/location/places/d/a/u;

    if-eqz v1, :cond_3

    .line 201
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/android/location/places/d/a/w;->d:Lcom/google/android/location/places/d/a/u;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 204
    :cond_3
    iget-object v1, p0, Lcom/google/android/location/places/d/a/w;->e:Lcom/google/android/location/places/d/a/v;

    if-eqz v1, :cond_4

    .line 205
    const/4 v1, 0x5

    iget-object v2, p0, Lcom/google/android/location/places/d/a/w;->e:Lcom/google/android/location/places/d/a/v;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 208
    :cond_4
    iget-object v1, p0, Lcom/google/android/location/places/d/a/w;->f:Lcom/google/android/location/places/d/a/n;

    if-eqz v1, :cond_5

    .line 209
    const/16 v1, 0x9

    iget-object v2, p0, Lcom/google/android/location/places/d/a/w;->f:Lcom/google/android/location/places/d/a/n;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 212
    :cond_5
    iget-object v1, p0, Lcom/google/android/location/places/d/a/w;->g:Lcom/google/android/location/places/d/a/m;

    if-eqz v1, :cond_6

    .line 213
    const/16 v1, 0xa

    iget-object v2, p0, Lcom/google/android/location/places/d/a/w;->g:Lcom/google/android/location/places/d/a/m;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 216
    :cond_6
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 72
    if-ne p1, p0, :cond_1

    .line 73
    const/4 v0, 0x1

    .line 135
    :cond_0
    :goto_0
    return v0

    .line 75
    :cond_1
    instance-of v1, p1, Lcom/google/android/location/places/d/a/w;

    if-eqz v1, :cond_0

    .line 78
    check-cast p1, Lcom/google/android/location/places/d/a/w;

    .line 79
    iget-object v1, p0, Lcom/google/android/location/places/d/a/w;->a:Ljava/lang/Integer;

    if-nez v1, :cond_9

    .line 80
    iget-object v1, p1, Lcom/google/android/location/places/d/a/w;->a:Ljava/lang/Integer;

    if-nez v1, :cond_0

    .line 85
    :cond_2
    iget-object v1, p0, Lcom/google/android/location/places/d/a/w;->b:Ljava/lang/String;

    if-nez v1, :cond_a

    .line 86
    iget-object v1, p1, Lcom/google/android/location/places/d/a/w;->b:Ljava/lang/String;

    if-nez v1, :cond_0

    .line 92
    :cond_3
    iget-object v1, p0, Lcom/google/android/location/places/d/a/w;->c:Ljava/lang/Integer;

    if-nez v1, :cond_b

    .line 93
    iget-object v1, p1, Lcom/google/android/location/places/d/a/w;->c:Ljava/lang/Integer;

    if-nez v1, :cond_0

    .line 99
    :cond_4
    iget-object v1, p0, Lcom/google/android/location/places/d/a/w;->d:Lcom/google/android/location/places/d/a/u;

    if-nez v1, :cond_c

    .line 100
    iget-object v1, p1, Lcom/google/android/location/places/d/a/w;->d:Lcom/google/android/location/places/d/a/u;

    if-nez v1, :cond_0

    .line 108
    :cond_5
    iget-object v1, p0, Lcom/google/android/location/places/d/a/w;->e:Lcom/google/android/location/places/d/a/v;

    if-nez v1, :cond_d

    .line 109
    iget-object v1, p1, Lcom/google/android/location/places/d/a/w;->e:Lcom/google/android/location/places/d/a/v;

    if-nez v1, :cond_0

    .line 117
    :cond_6
    iget-object v1, p0, Lcom/google/android/location/places/d/a/w;->f:Lcom/google/android/location/places/d/a/n;

    if-nez v1, :cond_e

    .line 118
    iget-object v1, p1, Lcom/google/android/location/places/d/a/w;->f:Lcom/google/android/location/places/d/a/n;

    if-nez v1, :cond_0

    .line 126
    :cond_7
    iget-object v1, p0, Lcom/google/android/location/places/d/a/w;->g:Lcom/google/android/location/places/d/a/m;

    if-nez v1, :cond_f

    .line 127
    iget-object v1, p1, Lcom/google/android/location/places/d/a/w;->g:Lcom/google/android/location/places/d/a/m;

    if-nez v1, :cond_0

    .line 135
    :cond_8
    invoke-virtual {p0, p1}, Lcom/google/android/location/places/d/a/w;->unknownFieldDataEquals(Lcom/google/protobuf/nano/d;)Z

    move-result v0

    goto :goto_0

    .line 83
    :cond_9
    iget-object v1, p0, Lcom/google/android/location/places/d/a/w;->a:Ljava/lang/Integer;

    iget-object v2, p1, Lcom/google/android/location/places/d/a/w;->a:Ljava/lang/Integer;

    invoke-virtual {v1, v2}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    goto :goto_0

    .line 89
    :cond_a
    iget-object v1, p0, Lcom/google/android/location/places/d/a/w;->b:Ljava/lang/String;

    iget-object v2, p1, Lcom/google/android/location/places/d/a/w;->b:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    goto :goto_0

    .line 96
    :cond_b
    iget-object v1, p0, Lcom/google/android/location/places/d/a/w;->c:Ljava/lang/Integer;

    iget-object v2, p1, Lcom/google/android/location/places/d/a/w;->c:Ljava/lang/Integer;

    invoke-virtual {v1, v2}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    goto :goto_0

    .line 104
    :cond_c
    iget-object v1, p0, Lcom/google/android/location/places/d/a/w;->d:Lcom/google/android/location/places/d/a/u;

    iget-object v2, p1, Lcom/google/android/location/places/d/a/w;->d:Lcom/google/android/location/places/d/a/u;

    invoke-virtual {v1, v2}, Lcom/google/android/location/places/d/a/u;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_5

    goto :goto_0

    .line 113
    :cond_d
    iget-object v1, p0, Lcom/google/android/location/places/d/a/w;->e:Lcom/google/android/location/places/d/a/v;

    iget-object v2, p1, Lcom/google/android/location/places/d/a/w;->e:Lcom/google/android/location/places/d/a/v;

    invoke-virtual {v1, v2}, Lcom/google/android/location/places/d/a/v;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_6

    goto :goto_0

    .line 122
    :cond_e
    iget-object v1, p0, Lcom/google/android/location/places/d/a/w;->f:Lcom/google/android/location/places/d/a/n;

    iget-object v2, p1, Lcom/google/android/location/places/d/a/w;->f:Lcom/google/android/location/places/d/a/n;

    invoke-virtual {v1, v2}, Lcom/google/android/location/places/d/a/n;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_7

    goto/16 :goto_0

    .line 131
    :cond_f
    iget-object v1, p0, Lcom/google/android/location/places/d/a/w;->g:Lcom/google/android/location/places/d/a/m;

    iget-object v2, p1, Lcom/google/android/location/places/d/a/w;->g:Lcom/google/android/location/places/d/a/m;

    invoke-virtual {v1, v2}, Lcom/google/android/location/places/d/a/m;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_8

    goto/16 :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 140
    iget-object v0, p0, Lcom/google/android/location/places/d/a/w;->a:Ljava/lang/Integer;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    .line 142
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/location/places/d/a/w;->b:Ljava/lang/String;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    .line 144
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/location/places/d/a/w;->c:Ljava/lang/Integer;

    if-nez v0, :cond_2

    move v0, v1

    :goto_2
    add-int/2addr v0, v2

    .line 146
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/location/places/d/a/w;->d:Lcom/google/android/location/places/d/a/u;

    if-nez v0, :cond_3

    move v0, v1

    :goto_3
    add-int/2addr v0, v2

    .line 148
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/location/places/d/a/w;->e:Lcom/google/android/location/places/d/a/v;

    if-nez v0, :cond_4

    move v0, v1

    :goto_4
    add-int/2addr v0, v2

    .line 150
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/location/places/d/a/w;->f:Lcom/google/android/location/places/d/a/n;

    if-nez v0, :cond_5

    move v0, v1

    :goto_5
    add-int/2addr v0, v2

    .line 152
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/android/location/places/d/a/w;->g:Lcom/google/android/location/places/d/a/m;

    if-nez v2, :cond_6

    :goto_6
    add-int/2addr v0, v1

    .line 154
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/google/android/location/places/d/a/w;->unknownFieldDataHashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 155
    return v0

    .line 140
    :cond_0
    iget-object v0, p0, Lcom/google/android/location/places/d/a/w;->a:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    goto :goto_0

    .line 142
    :cond_1
    iget-object v0, p0, Lcom/google/android/location/places/d/a/w;->b:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_1

    .line 144
    :cond_2
    iget-object v0, p0, Lcom/google/android/location/places/d/a/w;->c:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->hashCode()I

    move-result v0

    goto :goto_2

    .line 146
    :cond_3
    iget-object v0, p0, Lcom/google/android/location/places/d/a/w;->d:Lcom/google/android/location/places/d/a/u;

    invoke-virtual {v0}, Lcom/google/android/location/places/d/a/u;->hashCode()I

    move-result v0

    goto :goto_3

    .line 148
    :cond_4
    iget-object v0, p0, Lcom/google/android/location/places/d/a/w;->e:Lcom/google/android/location/places/d/a/v;

    invoke-virtual {v0}, Lcom/google/android/location/places/d/a/v;->hashCode()I

    move-result v0

    goto :goto_4

    .line 150
    :cond_5
    iget-object v0, p0, Lcom/google/android/location/places/d/a/w;->f:Lcom/google/android/location/places/d/a/n;

    invoke-virtual {v0}, Lcom/google/android/location/places/d/a/n;->hashCode()I

    move-result v0

    goto :goto_5

    .line 152
    :cond_6
    iget-object v1, p0, Lcom/google/android/location/places/d/a/w;->g:Lcom/google/android/location/places/d/a/m;

    invoke-virtual {v1}, Lcom/google/android/location/places/d/a/m;->hashCode()I

    move-result v1

    goto :goto_6
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 5
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/android/location/places/d/a/w;->storeUnknownField(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :pswitch_0
    goto :goto_0

    :pswitch_1
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/places/d/a/w;->a:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/places/d/a/w;->b:Ljava/lang/String;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/places/d/a/w;->c:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_4
    iget-object v0, p0, Lcom/google/android/location/places/d/a/w;->d:Lcom/google/android/location/places/d/a/u;

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/android/location/places/d/a/u;

    invoke-direct {v0}, Lcom/google/android/location/places/d/a/u;-><init>()V

    iput-object v0, p0, Lcom/google/android/location/places/d/a/w;->d:Lcom/google/android/location/places/d/a/u;

    :cond_1
    iget-object v0, p0, Lcom/google/android/location/places/d/a/w;->d:Lcom/google/android/location/places/d/a/u;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_5
    iget-object v0, p0, Lcom/google/android/location/places/d/a/w;->e:Lcom/google/android/location/places/d/a/v;

    if-nez v0, :cond_2

    new-instance v0, Lcom/google/android/location/places/d/a/v;

    invoke-direct {v0}, Lcom/google/android/location/places/d/a/v;-><init>()V

    iput-object v0, p0, Lcom/google/android/location/places/d/a/w;->e:Lcom/google/android/location/places/d/a/v;

    :cond_2
    iget-object v0, p0, Lcom/google/android/location/places/d/a/w;->e:Lcom/google/android/location/places/d/a/v;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_6
    iget-object v0, p0, Lcom/google/android/location/places/d/a/w;->f:Lcom/google/android/location/places/d/a/n;

    if-nez v0, :cond_3

    new-instance v0, Lcom/google/android/location/places/d/a/n;

    invoke-direct {v0}, Lcom/google/android/location/places/d/a/n;-><init>()V

    iput-object v0, p0, Lcom/google/android/location/places/d/a/w;->f:Lcom/google/android/location/places/d/a/n;

    :cond_3
    iget-object v0, p0, Lcom/google/android/location/places/d/a/w;->f:Lcom/google/android/location/places/d/a/n;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_7
    iget-object v0, p0, Lcom/google/android/location/places/d/a/w;->g:Lcom/google/android/location/places/d/a/m;

    if-nez v0, :cond_4

    new-instance v0, Lcom/google/android/location/places/d/a/m;

    invoke-direct {v0}, Lcom/google/android/location/places/d/a/m;-><init>()V

    iput-object v0, p0, Lcom/google/android/location/places/d/a/w;->g:Lcom/google/android/location/places/d/a/m;

    :cond_4
    iget-object v0, p0, Lcom/google/android/location/places/d/a/w;->g:Lcom/google/android/location/places/d/a/m;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x4a -> :sswitch_6
        0x52 -> :sswitch_7
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 161
    iget-object v0, p0, Lcom/google/android/location/places/d/a/w;->a:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 162
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/location/places/d/a/w;->a:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 164
    :cond_0
    iget-object v0, p0, Lcom/google/android/location/places/d/a/w;->b:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 165
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/android/location/places/d/a/w;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 167
    :cond_1
    iget-object v0, p0, Lcom/google/android/location/places/d/a/w;->c:Ljava/lang/Integer;

    if-eqz v0, :cond_2

    .line 168
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/android/location/places/d/a/w;->c:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 170
    :cond_2
    iget-object v0, p0, Lcom/google/android/location/places/d/a/w;->d:Lcom/google/android/location/places/d/a/u;

    if-eqz v0, :cond_3

    .line 171
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/android/location/places/d/a/w;->d:Lcom/google/android/location/places/d/a/u;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 173
    :cond_3
    iget-object v0, p0, Lcom/google/android/location/places/d/a/w;->e:Lcom/google/android/location/places/d/a/v;

    if-eqz v0, :cond_4

    .line 174
    const/4 v0, 0x5

    iget-object v1, p0, Lcom/google/android/location/places/d/a/w;->e:Lcom/google/android/location/places/d/a/v;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 176
    :cond_4
    iget-object v0, p0, Lcom/google/android/location/places/d/a/w;->f:Lcom/google/android/location/places/d/a/n;

    if-eqz v0, :cond_5

    .line 177
    const/16 v0, 0x9

    iget-object v1, p0, Lcom/google/android/location/places/d/a/w;->f:Lcom/google/android/location/places/d/a/n;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 179
    :cond_5
    iget-object v0, p0, Lcom/google/android/location/places/d/a/w;->g:Lcom/google/android/location/places/d/a/m;

    if-eqz v0, :cond_6

    .line 180
    const/16 v0, 0xa

    iget-object v1, p0, Lcom/google/android/location/places/d/a/w;->g:Lcom/google/android/location/places/d/a/m;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 182
    :cond_6
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/d;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 183
    return-void
.end method
