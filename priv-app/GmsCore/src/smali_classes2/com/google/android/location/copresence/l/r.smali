.class public final Lcom/google/android/location/copresence/l/r;
.super Lcom/google/android/location/copresence/l/ac;
.source "SourceFile"


# instance fields
.field private final n:Ljava/lang/String;

.field private final o:Ljava/lang/String;

.field private final p:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/location/copresence/ap;Lcom/google/android/location/copresence/a/a;Ljava/lang/String;Ljava/lang/String;ILcom/google/android/location/copresence/l/ab;)V
    .locals 8

    .prologue
    .line 40
    new-instance v3, Lcom/google/ac/b/c/cy;

    invoke-direct {v3}, Lcom/google/ac/b/c/cy;-><init>()V

    const-string v4, "RegisterDevice"

    const/4 v6, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v5, p3

    move-object v7, p7

    invoke-direct/range {v0 .. v7}, Lcom/google/android/location/copresence/l/ac;-><init>(Landroid/content/Context;Lcom/google/android/location/copresence/ap;Lcom/google/protobuf/nano/j;Ljava/lang/String;Lcom/google/android/location/copresence/a/a;Landroid/os/WorkSource;Lcom/google/android/location/copresence/l/ab;)V

    .line 49
    iput-object p4, p0, Lcom/google/android/location/copresence/l/r;->n:Ljava/lang/String;

    .line 50
    iput-object p5, p0, Lcom/google/android/location/copresence/l/r;->o:Ljava/lang/String;

    .line 51
    iput p6, p0, Lcom/google/android/location/copresence/l/r;->p:I

    .line 53
    iget-object v0, p0, Lcom/google/android/location/copresence/l/r;->n:Ljava/lang/String;

    if-nez v0, :cond_0

    const-string v0, "unregister"

    :goto_0
    iput-object v0, p0, Lcom/google/android/location/copresence/l/r;->l:Ljava/lang/String;

    .line 54
    return-void

    .line 53
    :cond_0
    iget-object v0, p0, Lcom/google/android/location/copresence/l/r;->n:Ljava/lang/String;

    goto :goto_0
.end method


# virtual methods
.method protected final bridge synthetic a(Lcom/google/protobuf/nano/j;)Lcom/google/ac/b/c/dc;
    .locals 1

    .prologue
    .line 23
    check-cast p1, Lcom/google/ac/b/c/cy;

    if-eqz p1, :cond_0

    iget-object v0, p1, Lcom/google/ac/b/c/cy;->a:Lcom/google/ac/b/c/dc;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected final a()Lcom/google/android/location/copresence/l/b;
    .locals 2

    .prologue
    .line 83
    new-instance v0, Lcom/google/android/location/copresence/l/s;

    iget-object v1, p0, Lcom/google/android/location/copresence/l/r;->b:Landroid/content/Context;

    invoke-direct {v0, p0, v1}, Lcom/google/android/location/copresence/l/s;-><init>(Lcom/google/android/location/copresence/l/r;Landroid/content/Context;)V

    return-object v0
.end method

.method protected final bridge synthetic a(Lcom/google/protobuf/nano/j;Lcom/google/protobuf/nano/j;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 23
    check-cast p2, Lcom/google/ac/b/c/cy;

    iget-object v0, p2, Lcom/google/ac/b/c/cy;->b:Ljava/lang/String;

    return-object v0
.end method

.method protected final synthetic b(Lcom/google/protobuf/nano/j;)I
    .locals 1

    .prologue
    .line 23
    check-cast p1, Lcom/google/ac/b/c/cy;

    if-eqz p1, :cond_0

    iget-object v0, p1, Lcom/google/ac/b/c/cy;->a:Lcom/google/ac/b/c/dc;

    if-eqz v0, :cond_0

    iget-object v0, p1, Lcom/google/ac/b/c/cy;->a:Lcom/google/ac/b/c/dc;

    iget-object v0, v0, Lcom/google/ac/b/c/dc;->c:Lcom/google/ac/b/c/bl;

    if-eqz v0, :cond_0

    iget-object v0, p1, Lcom/google/ac/b/c/cy;->a:Lcom/google/ac/b/c/dc;

    iget-object v0, v0, Lcom/google/ac/b/c/dc;->c:Lcom/google/ac/b/c/bl;

    iget-object v0, v0, Lcom/google/ac/b/c/bl;->a:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    iget-object v0, p1, Lcom/google/ac/b/c/cy;->a:Lcom/google/ac/b/c/dc;

    iget-object v0, v0, Lcom/google/ac/b/c/dc;->c:Lcom/google/ac/b/c/bl;

    iget-object v0, v0, Lcom/google/ac/b/c/bl;->a:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method protected final synthetic b()Lcom/google/protobuf/nano/j;
    .locals 3

    .prologue
    .line 23
    new-instance v0, Lcom/google/ac/b/c/cx;

    invoke-direct {v0}, Lcom/google/ac/b/c/cx;-><init>()V

    invoke-virtual {p0}, Lcom/google/android/location/copresence/l/r;->e()Lcom/google/ac/b/c/db;

    move-result-object v1

    iput-object v1, v0, Lcom/google/ac/b/c/cx;->a:Lcom/google/ac/b/c/db;

    new-instance v1, Lcom/google/ac/b/c/bg;

    invoke-direct {v1}, Lcom/google/ac/b/c/bg;-><init>()V

    iput-object v1, v0, Lcom/google/ac/b/c/cx;->d:Lcom/google/ac/b/c/bg;

    iget-object v1, p0, Lcom/google/android/location/copresence/l/r;->n:Ljava/lang/String;

    if-eqz v1, :cond_0

    iget-object v1, v0, Lcom/google/ac/b/c/cx;->d:Lcom/google/ac/b/c/bg;

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iput-object v2, v1, Lcom/google/ac/b/c/bg;->a:Ljava/lang/Integer;

    iget-object v1, v0, Lcom/google/ac/b/c/cx;->d:Lcom/google/ac/b/c/bg;

    new-instance v2, Lcom/google/ac/b/c/ap;

    invoke-direct {v2}, Lcom/google/ac/b/c/ap;-><init>()V

    iput-object v2, v1, Lcom/google/ac/b/c/bg;->b:Lcom/google/ac/b/c/ap;

    iget-object v1, v0, Lcom/google/ac/b/c/cx;->d:Lcom/google/ac/b/c/bg;

    iget-object v1, v1, Lcom/google/ac/b/c/bg;->b:Lcom/google/ac/b/c/ap;

    iget-object v2, p0, Lcom/google/android/location/copresence/l/r;->n:Ljava/lang/String;

    iput-object v2, v1, Lcom/google/ac/b/c/ap;->a:Ljava/lang/String;

    :goto_0
    new-instance v1, Lcom/google/ac/b/c/ae;

    invoke-direct {v1}, Lcom/google/ac/b/c/ae;-><init>()V

    iput-object v1, v0, Lcom/google/ac/b/c/cx;->e:Lcom/google/ac/b/c/ae;

    iget-object v1, v0, Lcom/google/ac/b/c/cx;->e:Lcom/google/ac/b/c/ae;

    iget v2, p0, Lcom/google/android/location/copresence/l/r;->p:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iput-object v2, v1, Lcom/google/ac/b/c/ae;->a:Ljava/lang/Integer;

    return-object v0

    :cond_0
    iget-object v1, v0, Lcom/google/ac/b/c/cx;->d:Lcom/google/ac/b/c/bg;

    const/4 v2, 0x3

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iput-object v2, v1, Lcom/google/ac/b/c/bg;->a:Ljava/lang/Integer;

    goto :goto_0
.end method

.method public final bridge synthetic c()Lcom/google/android/location/copresence/a/a;
    .locals 1

    .prologue
    .line 23
    invoke-super {p0}, Lcom/google/android/location/copresence/l/ac;->c()Lcom/google/android/location/copresence/a/a;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic d()V
    .locals 0

    .prologue
    .line 23
    invoke-super {p0}, Lcom/google/android/location/copresence/l/ac;->d()V

    return-void
.end method
