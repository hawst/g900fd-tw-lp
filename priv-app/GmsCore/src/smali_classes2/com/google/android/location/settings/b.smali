.class public abstract Lcom/google/android/location/settings/b;
.super Lcom/google/android/location/settings/a;
.source "SourceFile"

# interfaces
.implements Landroid/content/ServiceConnection;


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0xe
.end annotation


# instance fields
.field protected d:Lcom/google/android/location/n/a;

.field protected e:Lcom/google/android/location/reporting/service/l;

.field protected f:Ljava/lang/String;

.field protected g:Landroid/accounts/Account;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0}, Lcom/google/android/location/settings/a;-><init>()V

    return-void
.end method

.method private e()V
    .locals 9

    .prologue
    const/4 v8, 0x2

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 110
    iget-object v0, p0, Lcom/google/android/location/settings/b;->g:Landroid/accounts/Account;

    if-nez v0, :cond_1

    .line 111
    iget-object v0, p0, Lcom/google/android/location/settings/b;->d:Lcom/google/android/location/n/a;

    invoke-virtual {v0}, Lcom/google/android/location/n/a;->a()[Landroid/accounts/Account;

    move-result-object v4

    array-length v5, v4

    move v3, v2

    :goto_0
    if-ge v3, v5, :cond_8

    aget-object v0, v4, v3

    iget-object v6, p0, Lcom/google/android/location/settings/b;->f:Ljava/lang/String;

    iget-object v7, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_7

    const-string v3, "GCoreLocationSettings"

    invoke-static {v3, v8}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_0

    const-string v3, "GCoreLocationSettings"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Found account: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Lcom/google/android/gms/location/reporting/a/d;->a(Landroid/accounts/Account;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :goto_1
    iput-object v0, p0, Lcom/google/android/location/settings/b;->g:Landroid/accounts/Account;

    .line 113
    :cond_1
    iget-object v0, p0, Lcom/google/android/location/settings/b;->e:Lcom/google/android/location/reporting/service/l;

    if-eqz v0, :cond_d

    invoke-virtual {p0}, Lcom/google/android/location/settings/b;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_d

    iget-object v0, p0, Lcom/google/android/location/settings/b;->g:Landroid/accounts/Account;

    if-eqz v0, :cond_d

    .line 115
    :try_start_0
    iget-object v0, p0, Lcom/google/android/location/settings/b;->e:Lcom/google/android/location/reporting/service/l;

    iget-object v3, p0, Lcom/google/android/location/settings/b;->g:Landroid/accounts/Account;

    invoke-interface {v0, v3}, Lcom/google/android/location/reporting/service/l;->a(Landroid/accounts/Account;)Lcom/google/android/location/reporting/config/AccountConfig;

    move-result-object v4

    .line 116
    const-string v0, "GCoreLocationSettings"

    const/4 v3, 0x3

    invoke-static {v0, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 117
    const-string v0, "GCoreLocationSettings"

    invoke-virtual {v4}, Lcom/google/android/location/reporting/config/AccountConfig;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 119
    :cond_2
    invoke-virtual {p0, v4}, Lcom/google/android/location/settings/b;->a(Lcom/google/android/location/reporting/config/AccountConfig;)I

    move-result v5

    .line 120
    if-lez v5, :cond_9

    move v3, v1

    .line 123
    :goto_2
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/location/settings/a;->c:Z

    iget-object v0, p0, Lcom/google/android/location/settings/a;->a:Lcom/google/android/gms/common/widget/SwitchBar;

    if-eqz v0, :cond_a

    iget-object v0, p0, Lcom/google/android/location/settings/a;->a:Lcom/google/android/gms/common/widget/SwitchBar;

    invoke-virtual {v0, v3}, Lcom/google/android/gms/common/widget/SwitchBar;->setChecked(Z)V

    :cond_3
    :goto_3
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/location/settings/a;->c:Z

    .line 124
    invoke-virtual {v4}, Lcom/google/android/location/reporting/config/AccountConfig;->x()Z

    move-result v6

    iget-object v0, p0, Lcom/google/android/location/settings/a;->a:Lcom/google/android/gms/common/widget/SwitchBar;

    if-eqz v0, :cond_c

    iget-object v7, p0, Lcom/google/android/location/settings/a;->a:Lcom/google/android/gms/common/widget/SwitchBar;

    if-nez v6, :cond_b

    move v0, v1

    :goto_4
    invoke-virtual {v7, v0}, Lcom/google/android/gms/common/widget/SwitchBar;->setEnabled(Z)V

    iget-object v0, p0, Lcom/google/android/location/settings/a;->a:Lcom/google/android/gms/common/widget/SwitchBar;

    invoke-virtual {v0, v6}, Lcom/google/android/gms/common/widget/SwitchBar;->setEnabled(Z)V

    .line 129
    :cond_4
    :goto_5
    invoke-virtual {v4}, Lcom/google/android/location/reporting/config/AccountConfig;->x()Z

    move-result v0

    if-eqz v0, :cond_6

    if-eqz v5, :cond_5

    const/4 v0, -0x2

    if-ne v5, v0, :cond_6

    .line 135
    :cond_5
    invoke-virtual {p0, v3}, Lcom/google/android/location/settings/b;->b(Z)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 144
    :cond_6
    :goto_6
    return-void

    .line 111
    :cond_7
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto/16 :goto_0

    :cond_8
    invoke-virtual {p0}, Lcom/google/android/location/settings/b;->finish()V

    const/4 v0, 0x0

    goto :goto_1

    :cond_9
    move v3, v2

    .line 120
    goto :goto_2

    .line 123
    :cond_a
    :try_start_1
    iget-object v0, p0, Lcom/google/android/location/settings/a;->b:Landroid/widget/CompoundButton;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/location/settings/a;->b:Landroid/widget/CompoundButton;

    invoke-virtual {v0, v3}, Landroid/widget/CompoundButton;->setChecked(Z)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_3

    .line 137
    :catch_0
    move-exception v0

    .line 138
    const-string v1, "GCoreLocationSettings"

    const-string v2, ""

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_6

    :cond_b
    move v0, v2

    .line 124
    goto :goto_4

    :cond_c
    :try_start_2
    iget-object v0, p0, Lcom/google/android/location/settings/a;->b:Landroid/widget/CompoundButton;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/google/android/location/settings/a;->b:Landroid/widget/CompoundButton;

    invoke-virtual {v0, v6}, Landroid/widget/CompoundButton;->setEnabled(Z)V
    :try_end_2
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_5

    .line 140
    :cond_d
    const-string v0, "GCoreLocationSettings"

    invoke-static {v0, v8}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 141
    const-string v0, "GCoreLocationSettings"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "BaseUserLocationSettingsActivity skipping UI update, svc="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/location/settings/b;->e:Lcom/google/android/location/reporting/service/l;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", finishing: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/location/settings/b;->isFinishing()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", account="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/location/settings/b;->g:Landroid/accounts/Account;

    invoke-static {v2}, Lcom/google/android/gms/location/reporting/a/d;->a(Landroid/accounts/Account;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_6
.end method


# virtual methods
.method protected abstract a(Lcom/google/android/location/reporting/config/AccountConfig;)I
.end method

.method protected final a(Z)V
    .locals 3

    .prologue
    .line 182
    iget-object v0, p0, Lcom/google/android/location/settings/b;->e:Lcom/google/android/location/reporting/service/l;

    if-eqz v0, :cond_0

    .line 184
    :try_start_0
    invoke-virtual {p0, p1}, Lcom/google/android/location/settings/b;->b(Z)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 193
    :goto_0
    return-void

    .line 185
    :catch_0
    move-exception v0

    .line 186
    const-string v1, "GCoreLocationSettings"

    const-string v2, ""

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 189
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "No service, setting change ignored"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    .line 190
    invoke-static {v0}, Lcom/google/android/location/reporting/b/l;->a(Ljava/lang/Throwable;)V

    .line 191
    const-string v1, "GCoreLocationSettings"

    invoke-static {v1, v0}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method protected abstract b(Z)V
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 47
    invoke-super {p0, p1}, Lcom/google/android/location/settings/a;->onCreate(Landroid/os/Bundle;)V

    .line 49
    invoke-virtual {p0}, Lcom/google/android/location/settings/b;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "com.google.android.location.settings.extra.account"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/settings/b;->f:Ljava/lang/String;

    .line 50
    iget-object v0, p0, Lcom/google/android/location/settings/b;->f:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 51
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Finishing activity: no account name found in the intent. This shouldn\'t happen."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    .line 53
    const-string v1, "GCoreLocationSettings"

    invoke-static {v1, v0}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 54
    invoke-virtual {p0}, Lcom/google/android/location/settings/b;->finish()V

    .line 58
    :goto_0
    return-void

    .line 57
    :cond_0
    new-instance v0, Lcom/google/android/location/n/b;

    invoke-direct {v0, p0}, Lcom/google/android/location/n/b;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/location/settings/b;->d:Lcom/google/android/location/n/a;

    goto :goto_0
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2

    .prologue
    .line 170
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const v1, 0x102002c

    if-ne v0, v1, :cond_0

    .line 171
    invoke-virtual {p0}, Lcom/google/android/location/settings/b;->finish()V

    .line 172
    const/4 v0, 0x1

    .line 174
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected onResume()V
    .locals 0
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NewApi"
        }
    .end annotation

    .prologue
    .line 105
    invoke-super {p0}, Lcom/google/android/location/settings/a;->onResume()V

    .line 106
    invoke-direct {p0}, Lcom/google/android/location/settings/b;->e()V

    .line 107
    return-void
.end method

.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 2

    .prologue
    .line 77
    const-string v0, "GCoreLocationSettings"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 78
    const-string v0, "GCoreLocationSettings"

    const-string v1, "BaseUserLocationSettingsActivity.onServiceConnected()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 80
    :cond_0
    invoke-static {p2}, Lcom/google/android/location/reporting/service/m;->a(Landroid/os/IBinder;)Lcom/google/android/location/reporting/service/l;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/settings/b;->e:Lcom/google/android/location/reporting/service/l;

    .line 81
    invoke-direct {p0}, Lcom/google/android/location/settings/b;->e()V

    .line 82
    return-void
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 2

    .prologue
    .line 86
    const-string v0, "GCoreLocationSettings"

    const/4 v1, 0x4

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 87
    const-string v0, "GCoreLocationSettings"

    const-string v1, "BaseUserLocationSettingsActivity.onServiceDisconnected()"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 89
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/location/settings/b;->e:Lcom/google/android/location/reporting/service/l;

    .line 90
    return-void
.end method

.method protected onStart()V
    .locals 0

    .prologue
    .line 62
    invoke-super {p0}, Lcom/google/android/location/settings/a;->onStart()V

    .line 63
    invoke-static {p0, p0}, Lcom/google/android/location/reporting/service/PreferenceService;->a(Landroid/content/Context;Landroid/content/ServiceConnection;)V

    .line 64
    return-void
.end method

.method protected onStop()V
    .locals 1

    .prologue
    .line 68
    invoke-super {p0}, Lcom/google/android/location/settings/a;->onStop()V

    .line 69
    iget-object v0, p0, Lcom/google/android/location/settings/b;->e:Lcom/google/android/location/reporting/service/l;

    if-eqz v0, :cond_0

    .line 70
    invoke-virtual {p0, p0}, Lcom/google/android/location/settings/b;->unbindService(Landroid/content/ServiceConnection;)V

    .line 71
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/location/settings/b;->e:Lcom/google/android/location/reporting/service/l;

    .line 73
    :cond_0
    return-void
.end method
