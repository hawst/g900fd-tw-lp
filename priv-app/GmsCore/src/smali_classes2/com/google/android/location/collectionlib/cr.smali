.class final Lcom/google/android/location/collectionlib/cr;
.super Ljava/lang/Thread;
.source "SourceFile"


# instance fields
.field private volatile a:Lcom/google/android/location/collectionlib/ar;

.field private volatile b:Landroid/content/Context;

.field private volatile c:Lcom/google/android/location/collectionlib/ai;

.field private volatile d:Lcom/google/android/location/collectionlib/bp;

.field private e:Lcom/google/android/location/collectionlib/bo;

.field private volatile f:Lcom/google/p/a/b/b/a;

.field private g:Lcom/google/android/location/collectionlib/bf;

.field private h:Lcom/google/android/location/collectionlib/bq;

.field private final i:Ljava/lang/Integer;

.field private final j:Lcom/google/android/location/o/a/c;

.field private final k:Lcom/google/android/location/d/b;

.field private final l:Lcom/google/android/location/os/bn;

.field private final m:Lcom/google/p/a/b/b/a;

.field private final n:Ljava/lang/Object;

.field private volatile o:Z

.field private final p:Ljava/util/concurrent/CountDownLatch;

.field private q:Lcom/google/android/location/collectionlib/ar;


# direct methods
.method constructor <init>(Landroid/content/Context;Lcom/google/android/location/collectionlib/ai;Ljava/util/concurrent/CountDownLatch;Lcom/google/android/location/d/b;Lcom/google/android/location/os/bn;Lcom/google/p/a/b/b/a;Ljava/lang/Integer;Lcom/google/android/location/collectionlib/ar;Lcom/google/android/location/o/a/c;)V
    .locals 1

    .prologue
    .line 176
    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    .line 166
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/location/collectionlib/cr;->n:Ljava/lang/Object;

    .line 167
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/location/collectionlib/cr;->o:Z

    .line 300
    new-instance v0, Lcom/google/android/location/collectionlib/cs;

    invoke-direct {v0, p0}, Lcom/google/android/location/collectionlib/cs;-><init>(Lcom/google/android/location/collectionlib/cr;)V

    iput-object v0, p0, Lcom/google/android/location/collectionlib/cr;->q:Lcom/google/android/location/collectionlib/ar;

    .line 177
    const-string v0, "SignalCollector.ScannerThread"

    invoke-virtual {p0, v0}, Lcom/google/android/location/collectionlib/cr;->setName(Ljava/lang/String;)V

    .line 178
    const/16 v0, 0xa

    invoke-virtual {p0, v0}, Lcom/google/android/location/collectionlib/cr;->setPriority(I)V

    .line 179
    iput-object p2, p0, Lcom/google/android/location/collectionlib/cr;->c:Lcom/google/android/location/collectionlib/ai;

    .line 180
    iput-object p1, p0, Lcom/google/android/location/collectionlib/cr;->b:Landroid/content/Context;

    .line 181
    iput-object p8, p0, Lcom/google/android/location/collectionlib/cr;->a:Lcom/google/android/location/collectionlib/ar;

    .line 182
    iput-object p7, p0, Lcom/google/android/location/collectionlib/cr;->i:Ljava/lang/Integer;

    .line 183
    iput-object p9, p0, Lcom/google/android/location/collectionlib/cr;->j:Lcom/google/android/location/o/a/c;

    .line 184
    iput-object p3, p0, Lcom/google/android/location/collectionlib/cr;->p:Ljava/util/concurrent/CountDownLatch;

    .line 185
    iput-object p6, p0, Lcom/google/android/location/collectionlib/cr;->m:Lcom/google/p/a/b/b/a;

    .line 186
    iput-object p4, p0, Lcom/google/android/location/collectionlib/cr;->k:Lcom/google/android/location/d/b;

    .line 187
    iput-object p5, p0, Lcom/google/android/location/collectionlib/cr;->l:Lcom/google/android/location/os/bn;

    .line 188
    return-void
.end method

.method static synthetic a(Lcom/google/android/location/collectionlib/cr;Lcom/google/android/location/collectionlib/bq;)Lcom/google/android/location/collectionlib/bq;
    .locals 0

    .prologue
    .line 139
    iput-object p1, p0, Lcom/google/android/location/collectionlib/cr;->h:Lcom/google/android/location/collectionlib/bq;

    return-object p1
.end method

.method static synthetic a(Lcom/google/android/location/collectionlib/cr;Lcom/google/p/a/b/b/a;)Lcom/google/p/a/b/b/a;
    .locals 0

    .prologue
    .line 139
    iput-object p1, p0, Lcom/google/android/location/collectionlib/cr;->f:Lcom/google/p/a/b/b/a;

    return-object p1
.end method

.method static synthetic a(Lcom/google/android/location/collectionlib/cr;)V
    .locals 3

    .prologue
    .line 139
    iget-object v1, p0, Lcom/google/android/location/collectionlib/cr;->n:Ljava/lang/Object;

    monitor-enter v1

    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Lcom/google/android/location/collectionlib/cr;->o:Z

    iget-object v0, p0, Lcom/google/android/location/collectionlib/cr;->e:Lcom/google/android/location/collectionlib/bo;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/location/collectionlib/cr;->isAlive()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/location/collectionlib/cr;->e:Lcom/google/android/location/collectionlib/bo;

    iget-object v2, v0, Lcom/google/android/location/collectionlib/bo;->b:Lcom/google/android/location/collectionlib/as;

    if-eqz v2, :cond_0

    iget-object v2, v0, Lcom/google/android/location/collectionlib/bo;->c:Lcom/google/android/location/collectionlib/ak;

    if-eqz v2, :cond_0

    iget-object v2, v0, Lcom/google/android/location/collectionlib/bo;->c:Lcom/google/android/location/collectionlib/ak;

    iget-object v0, v0, Lcom/google/android/location/collectionlib/bo;->b:Lcom/google/android/location/collectionlib/as;

    invoke-virtual {v2, v0}, Lcom/google/android/location/collectionlib/ak;->a(Lcom/google/android/location/collectionlib/ce;)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/location/collectionlib/cr;->h:Lcom/google/android/location/collectionlib/bq;

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/google/android/location/collectionlib/cr;->isAlive()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/location/collectionlib/cr;->h:Lcom/google/android/location/collectionlib/bq;

    invoke-virtual {v0}, Lcom/google/android/location/collectionlib/bq;->a()V

    :cond_1
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method static synthetic b(Lcom/google/android/location/collectionlib/cr;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 139
    iget-object v0, p0, Lcom/google/android/location/collectionlib/cr;->n:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic c(Lcom/google/android/location/collectionlib/cr;)Lcom/google/android/location/o/a/c;
    .locals 1

    .prologue
    .line 139
    iget-object v0, p0, Lcom/google/android/location/collectionlib/cr;->j:Lcom/google/android/location/o/a/c;

    return-object v0
.end method

.method static synthetic d(Lcom/google/android/location/collectionlib/cr;)Lcom/google/p/a/b/b/a;
    .locals 1

    .prologue
    .line 139
    iget-object v0, p0, Lcom/google/android/location/collectionlib/cr;->f:Lcom/google/p/a/b/b/a;

    return-object v0
.end method

.method static synthetic e(Lcom/google/android/location/collectionlib/cr;)Lcom/google/android/location/collectionlib/bf;
    .locals 1

    .prologue
    .line 139
    iget-object v0, p0, Lcom/google/android/location/collectionlib/cr;->g:Lcom/google/android/location/collectionlib/bf;

    return-object v0
.end method

.method static synthetic f(Lcom/google/android/location/collectionlib/cr;)Z
    .locals 1

    .prologue
    .line 139
    iget-boolean v0, p0, Lcom/google/android/location/collectionlib/cr;->o:Z

    return v0
.end method

.method static synthetic g(Lcom/google/android/location/collectionlib/cr;)Lcom/google/android/location/collectionlib/ar;
    .locals 1

    .prologue
    .line 139
    iget-object v0, p0, Lcom/google/android/location/collectionlib/cr;->a:Lcom/google/android/location/collectionlib/ar;

    return-object v0
.end method

.method static synthetic h(Lcom/google/android/location/collectionlib/cr;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 139
    iget-object v0, p0, Lcom/google/android/location/collectionlib/cr;->b:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic i(Lcom/google/android/location/collectionlib/cr;)Lcom/google/android/location/collectionlib/ai;
    .locals 1

    .prologue
    .line 139
    iget-object v0, p0, Lcom/google/android/location/collectionlib/cr;->c:Lcom/google/android/location/collectionlib/ai;

    return-object v0
.end method

.method static synthetic j(Lcom/google/android/location/collectionlib/cr;)Lcom/google/android/location/d/b;
    .locals 1

    .prologue
    .line 139
    iget-object v0, p0, Lcom/google/android/location/collectionlib/cr;->k:Lcom/google/android/location/d/b;

    return-object v0
.end method

.method static synthetic k(Lcom/google/android/location/collectionlib/cr;)Lcom/google/android/location/os/bn;
    .locals 1

    .prologue
    .line 139
    iget-object v0, p0, Lcom/google/android/location/collectionlib/cr;->l:Lcom/google/android/location/os/bn;

    return-object v0
.end method

.method static synthetic l(Lcom/google/android/location/collectionlib/cr;)Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 139
    iget-object v0, p0, Lcom/google/android/location/collectionlib/cr;->i:Ljava/lang/Integer;

    return-object v0
.end method

.method static synthetic m(Lcom/google/android/location/collectionlib/cr;)Lcom/google/p/a/b/b/a;
    .locals 1

    .prologue
    .line 139
    iget-object v0, p0, Lcom/google/android/location/collectionlib/cr;->m:Lcom/google/p/a/b/b/a;

    return-object v0
.end method

.method static synthetic n(Lcom/google/android/location/collectionlib/cr;)Lcom/google/android/location/collectionlib/bq;
    .locals 1

    .prologue
    .line 139
    iget-object v0, p0, Lcom/google/android/location/collectionlib/cr;->h:Lcom/google/android/location/collectionlib/bq;

    return-object v0
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    .line 426
    iget-object v1, p0, Lcom/google/android/location/collectionlib/cr;->n:Ljava/lang/Object;

    monitor-enter v1

    .line 427
    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Lcom/google/android/location/collectionlib/cr;->o:Z

    .line 428
    iget-object v0, p0, Lcom/google/android/location/collectionlib/cr;->h:Lcom/google/android/location/collectionlib/bq;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/location/collectionlib/cr;->isAlive()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 429
    iget-object v0, p0, Lcom/google/android/location/collectionlib/cr;->h:Lcom/google/android/location/collectionlib/bq;

    invoke-virtual {v0}, Lcom/google/android/location/collectionlib/bq;->a()V

    iget-object v2, v0, Lcom/google/android/location/collectionlib/bq;->b:Lcom/google/android/location/collectionlib/ak;

    if-eqz v2, :cond_0

    iget-object v0, v0, Lcom/google/android/location/collectionlib/bq;->b:Lcom/google/android/location/collectionlib/ak;

    invoke-virtual {v0}, Lcom/google/android/location/collectionlib/ak;->a()V

    .line 431
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final a(Lcom/google/p/a/b/b/a;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 442
    iget-object v1, p0, Lcom/google/android/location/collectionlib/cr;->h:Lcom/google/android/location/collectionlib/bq;

    if-eqz v1, :cond_0

    .line 443
    iget-object v1, p0, Lcom/google/android/location/collectionlib/cr;->h:Lcom/google/android/location/collectionlib/bq;

    iget-object v2, v1, Lcom/google/android/location/collectionlib/bq;->a:Lcom/google/android/location/collectionlib/ca;

    if-nez v2, :cond_1

    .line 446
    :cond_0
    :goto_0
    return v0

    .line 443
    :cond_1
    iget-object v0, v1, Lcom/google/android/location/collectionlib/bq;->a:Lcom/google/android/location/collectionlib/ca;

    invoke-interface {v0, p1}, Lcom/google/android/location/collectionlib/ca;->a(Lcom/google/p/a/b/b/a;)Z

    move-result v0

    goto :goto_0
.end method

.method public final run()V
    .locals 29

    .prologue
    .line 197
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/location/collectionlib/cr;->b:Landroid/content/Context;

    const-string v3, "power"

    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/os/PowerManager;

    .line 198
    new-instance v2, Lcom/google/android/location/o/h;

    const/4 v4, 0x1

    const/4 v5, 0x0

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/location/collectionlib/cr;->getName()Ljava/lang/String;

    move-result-object v6

    sget-object v7, Lcom/google/android/location/o/h;->a:[S

    invoke-direct/range {v2 .. v7}, Lcom/google/android/location/o/h;-><init>(Landroid/os/PowerManager;IZLjava/lang/String;[S)V

    .line 201
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/location/collectionlib/cr;->c:Lcom/google/android/location/collectionlib/ai;

    invoke-interface {v3}, Lcom/google/android/location/collectionlib/ai;->a()Lcom/google/android/location/o/n;

    move-result-object v3

    .line 202
    invoke-virtual {v2, v3}, Lcom/google/android/location/o/h;->a(Lcom/google/android/location/o/n;)V

    .line 203
    invoke-virtual {v2}, Lcom/google/android/location/o/h;->a()V

    .line 205
    :try_start_0
    invoke-static {}, Landroid/os/Looper;->prepare()V

    .line 206
    new-instance v4, Lcom/google/android/location/collectionlib/ct;

    move-object/from16 v0, p0

    invoke-direct {v4, v0}, Lcom/google/android/location/collectionlib/ct;-><init>(Lcom/google/android/location/collectionlib/cr;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 207
    const/16 v23, 0x0

    .line 208
    const/16 v22, 0x0

    .line 210
    :try_start_1
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/location/collectionlib/cr;->c:Lcom/google/android/location/collectionlib/ai;

    invoke-interface {v5}, Lcom/google/android/location/collectionlib/ai;->m()Lcom/google/android/location/collectionlib/ai;

    move-result-object v5

    move-object/from16 v0, p0

    iput-object v5, v0, Lcom/google/android/location/collectionlib/cr;->c:Lcom/google/android/location/collectionlib/ai;

    .line 211
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/location/collectionlib/cr;->d:Lcom/google/android/location/collectionlib/bp;

    if-eqz v5, :cond_7

    .line 213
    new-instance v5, Lcom/google/android/location/collectionlib/bo;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/location/collectionlib/cr;->j:Lcom/google/android/location/o/a/c;

    invoke-direct {v5, v4, v6, v3}, Lcom/google/android/location/collectionlib/bo;-><init>(Landroid/os/Handler;Lcom/google/android/location/o/a/c;Lcom/google/android/location/o/n;)V

    move-object/from16 v0, p0

    iput-object v5, v0, Lcom/google/android/location/collectionlib/cr;->e:Lcom/google/android/location/collectionlib/bo;

    .line 214
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/location/collectionlib/cr;->e:Lcom/google/android/location/collectionlib/bo;

    move-object/from16 v25, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/location/collectionlib/cr;->b:Landroid/content/Context;

    move-object/from16 v26, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/location/collectionlib/cr;->d:Lcom/google/android/location/collectionlib/bp;

    move-object/from16 v27, v0

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/location/collectionlib/cr;->k:Lcom/google/android/location/d/b;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/location/collectionlib/cr;->l:Lcom/google/android/location/os/bn;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/location/collectionlib/cr;->q:Lcom/google/android/location/collectionlib/ar;

    move-object/from16 v19, v0

    new-instance v4, Lcom/google/android/location/collectionlib/bm;

    move-object/from16 v0, v25

    iget-object v3, v0, Lcom/google/android/location/collectionlib/bo;->d:Lcom/google/android/location/o/a/c;

    move-object/from16 v0, v19

    invoke-direct {v4, v0, v3}, Lcom/google/android/location/collectionlib/bm;-><init>(Lcom/google/android/location/collectionlib/ar;Lcom/google/android/location/o/a/c;)V

    const/16 v24, 0x0

    new-instance v3, Lcom/google/android/location/collectionlib/ay;

    invoke-virtual/range {v26 .. v26}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    move-object/from16 v0, v25

    iget-object v9, v0, Lcom/google/android/location/collectionlib/bo;->d:Lcom/google/android/location/o/a/c;

    invoke-direct/range {v3 .. v9}, Lcom/google/android/location/collectionlib/ay;-><init>(Lcom/google/android/location/collectionlib/cc;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Lcom/google/p/a/b/b/a;Lcom/google/android/location/o/a/c;)V

    new-instance v4, Lcom/google/android/location/collectionlib/ak;

    move-object/from16 v0, v25

    iget-object v5, v0, Lcom/google/android/location/collectionlib/bo;->a:Landroid/os/Handler;

    const/4 v6, 0x1

    move-object/from16 v0, v25

    iget-object v7, v0, Lcom/google/android/location/collectionlib/bo;->d:Lcom/google/android/location/o/a/c;

    invoke-direct {v4, v3, v5, v6, v7}, Lcom/google/android/location/collectionlib/ak;-><init>(Lcom/google/android/location/collectionlib/ca;Landroid/os/Handler;ILcom/google/android/location/o/a/c;)V

    move-object/from16 v0, v25

    iput-object v4, v0, Lcom/google/android/location/collectionlib/bo;->c:Lcom/google/android/location/collectionlib/ak;

    new-instance v4, Lcom/google/android/location/collectionlib/as;

    invoke-interface/range {v27 .. v27}, Lcom/google/android/location/collectionlib/bp;->b()Ljava/util/Set;

    move-result-object v6

    invoke-interface/range {v27 .. v27}, Lcom/google/android/location/collectionlib/bp;->d()Ljava/util/Map;

    move-result-object v7

    const/4 v8, 0x0

    const-wide/16 v9, 0x0

    const/4 v11, 0x0

    const-wide/16 v14, 0x0

    invoke-interface/range {v27 .. v27}, Lcom/google/android/location/collectionlib/bp;->c()Z

    move-result v16

    move-object/from16 v0, v25

    iget-object v0, v0, Lcom/google/android/location/collectionlib/bo;->c:Lcom/google/android/location/collectionlib/ak;

    move-object/from16 v17, v0

    const/16 v18, 0x1

    move-object/from16 v0, v25

    iget-object v0, v0, Lcom/google/android/location/collectionlib/bo;->d:Lcom/google/android/location/o/a/c;

    move-object/from16 v20, v0

    move-object/from16 v0, v25

    iget-object v0, v0, Lcom/google/android/location/collectionlib/bo;->e:Lcom/google/android/location/o/n;

    move-object/from16 v21, v0

    move-object/from16 v5, v26

    invoke-direct/range {v4 .. v21}, Lcom/google/android/location/collectionlib/as;-><init>(Landroid/content/Context;Ljava/util/Set;Ljava/util/Map;ZJLcom/google/android/location/collectionlib/SensorScannerConfig;Lcom/google/android/location/d/b;Lcom/google/android/location/os/bn;JZLcom/google/android/location/collectionlib/ak;ZLcom/google/android/location/collectionlib/ar;Lcom/google/android/location/o/a/c;Lcom/google/android/location/o/n;)V

    move-object/from16 v0, v25

    iput-object v4, v0, Lcom/google/android/location/collectionlib/bo;->b:Lcom/google/android/location/collectionlib/as;

    sget-boolean v3, Lcom/google/android/location/i/a;->b:Z

    if-eqz v3, :cond_0

    move-object/from16 v0, v25

    iget-object v3, v0, Lcom/google/android/location/collectionlib/bo;->d:Lcom/google/android/location/o/a/c;

    const-string v4, "PreScan started with config: %s"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object v27, v5, v6

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/google/android/location/o/a/c;->a(Ljava/lang/String;)V

    :cond_0
    move-object/from16 v0, v25

    iget-object v3, v0, Lcom/google/android/location/collectionlib/bo;->b:Lcom/google/android/location/collectionlib/as;

    invoke-virtual {v3}, Lcom/google/android/location/collectionlib/as;->c()I

    move-result v3

    if-lez v3, :cond_b

    move-object/from16 v0, v25

    iget-object v3, v0, Lcom/google/android/location/collectionlib/bo;->b:Lcom/google/android/location/collectionlib/as;

    new-instance v4, Lcom/google/android/location/collectionlib/dd;

    move-object/from16 v0, v25

    iget-object v5, v0, Lcom/google/android/location/collectionlib/bo;->b:Lcom/google/android/location/collectionlib/as;

    move-object/from16 v0, v25

    iget-object v6, v0, Lcom/google/android/location/collectionlib/bo;->c:Lcom/google/android/location/collectionlib/ak;

    invoke-interface/range {v27 .. v27}, Lcom/google/android/location/collectionlib/bp;->a()J

    move-result-wide v8

    invoke-direct {v4, v5, v6, v8, v9}, Lcom/google/android/location/collectionlib/dd;-><init>(Lcom/google/android/location/collectionlib/ce;Lcom/google/android/location/collectionlib/ak;J)V

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Lcom/google/android/location/collectionlib/as;->a(Lcom/google/android/location/collectionlib/g;Lcom/google/android/location/collectionlib/cf;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    const/4 v3, 0x1

    .line 216
    :goto_0
    if-eqz v3, :cond_6

    .line 217
    :try_start_2
    sget-boolean v4, Lcom/google/android/location/i/a;->b:Z

    if-eqz v4, :cond_a

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/location/collectionlib/cr;->j:Lcom/google/android/location/o/a/c;

    const-string v5, "PreScanner started."

    invoke-virtual {v4, v5}, Lcom/google/android/location/o/a/c;->a(Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move v4, v3

    .line 235
    :cond_1
    :goto_1
    if-nez v4, :cond_3

    .line 236
    :try_start_3
    sget-boolean v3, Lcom/google/android/location/i/a;->b:Z

    if-eqz v3, :cond_2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/location/collectionlib/cr;->j:Lcom/google/android/location/o/a/c;

    move-object/from16 v0, v22

    invoke-virtual {v3, v0}, Lcom/google/android/location/o/a/c;->a(Ljava/lang/String;)V

    .line 237
    :cond_2
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/location/collectionlib/cr;->a:Lcom/google/android/location/collectionlib/ar;

    if-eqz v3, :cond_3

    .line 238
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/location/collectionlib/cr;->a:Lcom/google/android/location/collectionlib/ar;

    move-object/from16 v0, v22

    invoke-interface {v3, v0}, Lcom/google/android/location/collectionlib/ar;->a(Ljava/lang/String;)V

    .line 244
    :cond_3
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/location/collectionlib/cr;->p:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v3}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    .line 245
    if-eqz v4, :cond_4

    .line 246
    invoke-static {}, Landroid/os/Looper;->loop()V

    .line 248
    :cond_4
    sget-boolean v3, Lcom/google/android/location/i/a;->b:Z

    if-eqz v3, :cond_5

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/location/collectionlib/cr;->j:Lcom/google/android/location/o/a/c;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/location/collectionlib/cr;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " is quiting."

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/google/android/location/o/a/c;->a(Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 250
    :cond_5
    invoke-virtual {v2}, Lcom/google/android/location/o/h;->b()V

    .line 251
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/location/collectionlib/cr;->b:Landroid/content/Context;

    .line 252
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/location/collectionlib/cr;->c:Lcom/google/android/location/collectionlib/ai;

    .line 253
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/location/collectionlib/cr;->h:Lcom/google/android/location/collectionlib/bq;

    .line 254
    return-void

    .line 219
    :cond_6
    :try_start_4
    const-string v22, "PreScanner: Nothing to scan."
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    move v4, v3

    goto :goto_1

    .line 222
    :cond_7
    :try_start_5
    new-instance v3, Lcom/google/android/location/collectionlib/bq;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/location/collectionlib/cr;->j:Lcom/google/android/location/o/a/c;

    invoke-direct {v3, v4, v5}, Lcom/google/android/location/collectionlib/bq;-><init>(Landroid/os/Handler;Lcom/google/android/location/o/a/c;)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/location/collectionlib/cr;->h:Lcom/google/android/location/collectionlib/bq;

    .line 223
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/location/collectionlib/cr;->h:Lcom/google/android/location/collectionlib/bq;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/location/collectionlib/cr;->b:Landroid/content/Context;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/location/collectionlib/cr;->c:Lcom/google/android/location/collectionlib/ai;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/location/collectionlib/cr;->k:Lcom/google/android/location/d/b;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/location/collectionlib/cr;->l:Lcom/google/android/location/os/bn;

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/location/collectionlib/cr;->i:Ljava/lang/Integer;

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/location/collectionlib/cr;->m:Lcom/google/p/a/b/b/a;

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/android/location/collectionlib/cr;->a:Lcom/google/android/location/collectionlib/ar;

    invoke-virtual/range {v3 .. v10}, Lcom/google/android/location/collectionlib/bq;->a(Landroid/content/Context;Lcom/google/android/location/collectionlib/ai;Lcom/google/android/location/d/b;Lcom/google/android/location/os/bn;Ljava/lang/Integer;Lcom/google/p/a/b/b/a;Lcom/google/android/location/collectionlib/ar;)Z
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_1
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    move-result v4

    .line 225
    if-eqz v4, :cond_8

    .line 226
    :try_start_6
    sget-boolean v3, Lcom/google/android/location/i/a;->b:Z

    if-eqz v3, :cond_9

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/location/collectionlib/cr;->j:Lcom/google/android/location/o/a/c;

    const-string v5, "RealCollector started."

    invoke-virtual {v3, v5}, Lcom/google/android/location/o/a/c;->a(Ljava/lang/String;)V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_0
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    goto/16 :goto_1

    .line 231
    :catch_0
    move-exception v3

    .line 232
    :goto_2
    :try_start_7
    const-string v5, "Failed normalize configuration: %s"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    invoke-virtual {v3}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v6, v7

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v22

    .line 233
    sget-boolean v3, Lcom/google/android/location/i/a;->b:Z

    if-eqz v3, :cond_1

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/location/collectionlib/cr;->j:Lcom/google/android/location/o/a/c;

    move-object/from16 v0, v22

    invoke-virtual {v3, v0}, Lcom/google/android/location/o/a/c;->a(Ljava/lang/String;)V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    goto/16 :goto_1

    .line 250
    :catchall_0
    move-exception v3

    invoke-virtual {v2}, Lcom/google/android/location/o/h;->b()V

    .line 251
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/location/collectionlib/cr;->b:Landroid/content/Context;

    .line 252
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/location/collectionlib/cr;->c:Lcom/google/android/location/collectionlib/ai;

    .line 253
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/location/collectionlib/cr;->h:Lcom/google/android/location/collectionlib/bq;

    throw v3

    .line 228
    :cond_8
    :try_start_8
    const-string v3, "RealCollector: Nothing to scan."
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_0
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    :goto_3
    move-object/from16 v22, v3

    .line 234
    goto/16 :goto_1

    .line 231
    :catch_1
    move-exception v3

    move/from16 v4, v23

    goto :goto_2

    :catch_2
    move-exception v4

    move-object/from16 v28, v4

    move v4, v3

    move-object/from16 v3, v28

    goto :goto_2

    :cond_9
    move-object/from16 v3, v22

    goto :goto_3

    :cond_a
    move v4, v3

    move-object/from16 v3, v22

    goto :goto_3

    :cond_b
    move/from16 v3, v24

    goto/16 :goto_0
.end method
