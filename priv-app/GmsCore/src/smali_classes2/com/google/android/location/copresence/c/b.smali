.class public final Lcom/google/android/location/copresence/c/b;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final a:Ljava/lang/String;

.field final b:Lcom/google/android/gms/common/api/v;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    iput-object p2, p0, Lcom/google/android/location/copresence/c/b;->a:Ljava/lang/String;

    .line 29
    new-instance v0, Lcom/google/android/gms/common/api/w;

    invoke-direct {v0, p1}, Lcom/google/android/gms/common/api/w;-><init>(Landroid/content/Context;)V

    sget-object v1, Lcom/google/android/gms/audiomodem/d;->b:Lcom/google/android/gms/common/api/c;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/api/w;->a(Lcom/google/android/gms/common/api/c;)Lcom/google/android/gms/common/api/w;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/common/api/w;->a()Lcom/google/android/gms/common/api/v;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/copresence/c/b;->b:Lcom/google/android/gms/common/api/v;

    .line 30
    return-void
.end method

.method private static a(ZZ)I
    .locals 1

    .prologue
    .line 163
    if-eqz p0, :cond_0

    if-eqz p1, :cond_0

    .line 164
    const/4 v0, 0x3

    .line 170
    :goto_0
    return v0

    .line 165
    :cond_0
    if-eqz p0, :cond_1

    .line 166
    const/4 v0, 0x1

    goto :goto_0

    .line 167
    :cond_1
    if-eqz p1, :cond_2

    .line 168
    const/4 v0, 0x2

    goto :goto_0

    .line 170
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(I)Lcom/google/android/gms/audiomodem/Encoding;
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 133
    new-instance v0, Lcom/google/android/gms/audiomodem/am;

    invoke-direct {v0}, Lcom/google/android/gms/audiomodem/am;-><init>()V

    invoke-virtual {v0, p0}, Lcom/google/android/gms/audiomodem/am;->a(I)Lcom/google/android/gms/audiomodem/am;

    move-result-object v3

    invoke-static {}, Lcom/google/android/location/copresence/f/b;->b()Lcom/google/ac/b/c/g;

    move-result-object v0

    iget-object v0, v0, Lcom/google/ac/b/c/g;->g:Lcom/google/ac/b/c/b;

    iget-object v0, v0, Lcom/google/ac/b/c/b;->c:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, v3, Lcom/google/android/gms/audiomodem/am;->a:Z

    invoke-static {}, Lcom/google/android/location/copresence/f/b;->b()Lcom/google/ac/b/c/g;

    move-result-object v0

    iget-object v0, v0, Lcom/google/ac/b/c/g;->g:Lcom/google/ac/b/c/b;

    iget-object v0, v0, Lcom/google/ac/b/c/b;->d:Ljava/lang/Double;

    invoke-virtual {v0}, Ljava/lang/Double;->floatValue()F

    move-result v4

    cmpl-float v0, v4, v6

    if-lez v0, :cond_1

    move v0, v1

    :goto_0
    const-string v5, "desiredCarrierFrequency must be greater than zero"

    invoke-static {v0, v5}, Lcom/google/android/gms/common/internal/bx;->b(ZLjava/lang/Object;)V

    iput v4, v3, Lcom/google/android/gms/audiomodem/am;->f:F

    invoke-static {}, Lcom/google/android/location/copresence/f/b;->b()Lcom/google/ac/b/c/g;

    move-result-object v0

    iget-object v0, v0, Lcom/google/ac/b/c/g;->g:Lcom/google/ac/b/c/b;

    iget-object v0, v0, Lcom/google/ac/b/c/b;->b:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-static {}, Lcom/google/android/location/copresence/f/b;->b()Lcom/google/ac/b/c/g;

    move-result-object v4

    iget-object v4, v4, Lcom/google/ac/b/c/g;->g:Lcom/google/ac/b/c/b;

    iget-object v4, v4, Lcom/google/ac/b/c/b;->e:Ljava/lang/Boolean;

    invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    invoke-static {v0, v4}, Lcom/google/android/location/copresence/c/b;->a(ZZ)I

    move-result v0

    invoke-virtual {v3, v0}, Lcom/google/android/gms/audiomodem/am;->b(I)Lcom/google/android/gms/audiomodem/am;

    move-result-object v3

    invoke-static {}, Lcom/google/android/location/copresence/f/b;->b()Lcom/google/ac/b/c/g;

    move-result-object v0

    iget-object v0, v0, Lcom/google/ac/b/c/g;->g:Lcom/google/ac/b/c/b;

    iget-object v0, v0, Lcom/google/ac/b/c/b;->f:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v4

    const/4 v0, 0x5

    if-gt v0, v4, :cond_2

    const/16 v0, 0xb

    if-gt v4, v0, :cond_2

    move v0, v1

    :goto_1
    const-string v5, "numberOfTapsLfsr must be in the range [5, 11]"

    invoke-static {v0, v5}, Lcom/google/android/gms/common/internal/bx;->b(ZLjava/lang/Object;)V

    iput v4, v3, Lcom/google/android/gms/audiomodem/am;->b:I

    invoke-static {}, Lcom/google/android/location/copresence/f/b;->b()Lcom/google/ac/b/c/g;

    move-result-object v0

    iget-object v0, v0, Lcom/google/ac/b/c/g;->g:Lcom/google/ac/b/c/b;

    iget-object v0, v0, Lcom/google/ac/b/c/b;->g:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, v3, Lcom/google/android/gms/audiomodem/am;->c:I

    invoke-static {}, Lcom/google/android/location/copresence/f/b;->b()Lcom/google/ac/b/c/g;

    move-result-object v0

    iget-object v0, v0, Lcom/google/ac/b/c/g;->g:Lcom/google/ac/b/c/b;

    iget-object v0, v0, Lcom/google/ac/b/c/b;->h:Ljava/lang/Double;

    invoke-virtual {v0}, Ljava/lang/Double;->floatValue()F

    move-result v4

    cmpl-float v0, v4, v6

    if-lez v0, :cond_3

    move v0, v1

    :goto_2
    const-string v5, "coderSampleRate must be greater than zero"

    invoke-static {v0, v5}, Lcom/google/android/gms/common/internal/bx;->b(ZLjava/lang/Object;)V

    iput v4, v3, Lcom/google/android/gms/audiomodem/am;->d:F

    invoke-static {}, Lcom/google/android/location/copresence/f/b;->b()Lcom/google/ac/b/c/g;

    move-result-object v0

    iget-object v0, v0, Lcom/google/ac/b/c/g;->g:Lcom/google/ac/b/c/b;

    iget-object v0, v0, Lcom/google/ac/b/c/b;->i:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v4

    if-eq v4, v1, :cond_0

    const/4 v0, 0x2

    if-eq v4, v0, :cond_0

    const/4 v0, 0x4

    if-eq v4, v0, :cond_0

    const/16 v0, 0x8

    if-ne v4, v0, :cond_4

    :cond_0
    move v0, v1

    :goto_3
    const-string v5, "bitsPerSymbol must be 1, 2, 4, or 8"

    invoke-static {v0, v5}, Lcom/google/android/gms/common/internal/bx;->b(ZLjava/lang/Object;)V

    iput v4, v3, Lcom/google/android/gms/audiomodem/am;->g:I

    invoke-static {}, Lcom/google/android/location/copresence/f/b;->b()Lcom/google/ac/b/c/g;

    move-result-object v0

    iget-object v0, v0, Lcom/google/ac/b/c/g;->g:Lcom/google/ac/b/c/b;

    iget-object v0, v0, Lcom/google/ac/b/c/b;->j:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v4

    if-lez v4, :cond_5

    move v0, v1

    :goto_4
    const-string v5, "minCyclesPerFrame must be greater than zero"

    invoke-static {v0, v5}, Lcom/google/android/gms/common/internal/bx;->b(ZLjava/lang/Object;)V

    iput v4, v3, Lcom/google/android/gms/audiomodem/am;->h:I

    invoke-static {}, Lcom/google/android/location/copresence/f/b;->b()Lcom/google/ac/b/c/g;

    move-result-object v0

    iget-object v0, v0, Lcom/google/ac/b/c/g;->g:Lcom/google/ac/b/c/b;

    iget-object v0, v0, Lcom/google/ac/b/c/b;->k:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v4

    if-lez v4, :cond_6

    move v0, v1

    :goto_5
    const-string v5, "basebandDecimationFactor must be greater than zero"

    invoke-static {v0, v5}, Lcom/google/android/gms/common/internal/bx;->b(ZLjava/lang/Object;)V

    iput v4, v3, Lcom/google/android/gms/audiomodem/am;->i:I

    invoke-static {}, Lcom/google/android/location/copresence/f/b;->b()Lcom/google/ac/b/c/g;

    move-result-object v0

    iget-object v0, v0, Lcom/google/ac/b/c/g;->g:Lcom/google/ac/b/c/b;

    iget-object v0, v0, Lcom/google/ac/b/c/b;->l:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-lez v0, :cond_7

    :goto_6
    const-string v2, "upsamplingFactor must be greater than zero"

    invoke-static {v1, v2}, Lcom/google/android/gms/common/internal/bx;->b(ZLjava/lang/Object;)V

    iput v0, v3, Lcom/google/android/gms/audiomodem/am;->e:I

    invoke-virtual {v3}, Lcom/google/android/gms/audiomodem/am;->a()Lcom/google/android/gms/audiomodem/Encoding;

    move-result-object v0

    return-object v0

    :cond_1
    move v0, v2

    goto/16 :goto_0

    :cond_2
    move v0, v2

    goto/16 :goto_1

    :cond_3
    move v0, v2

    goto :goto_2

    :cond_4
    move v0, v2

    goto :goto_3

    :cond_5
    move v0, v2

    goto :goto_4

    :cond_6
    move v0, v2

    goto :goto_5

    :cond_7
    move v1, v2

    goto :goto_6
.end method

.method public static b(I)Lcom/google/android/gms/audiomodem/Encoding;
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 150
    new-instance v0, Lcom/google/android/gms/audiomodem/an;

    invoke-direct {v0}, Lcom/google/android/gms/audiomodem/an;-><init>()V

    invoke-virtual {v0, p0}, Lcom/google/android/gms/audiomodem/an;->a(I)Lcom/google/android/gms/audiomodem/an;

    move-result-object v0

    invoke-static {}, Lcom/google/android/location/copresence/f/b;->b()Lcom/google/ac/b/c/g;

    move-result-object v3

    iget-object v3, v3, Lcom/google/ac/b/c/g;->n:Lcom/google/ac/b/c/c;

    iget-object v3, v3, Lcom/google/ac/b/c/c;->a:Ljava/lang/Boolean;

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    invoke-static {}, Lcom/google/android/location/copresence/f/b;->b()Lcom/google/ac/b/c/g;

    move-result-object v4

    iget-object v4, v4, Lcom/google/ac/b/c/g;->n:Lcom/google/ac/b/c/c;

    iget-object v4, v4, Lcom/google/ac/b/c/c;->b:Ljava/lang/Boolean;

    invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    invoke-static {v3, v4}, Lcom/google/android/location/copresence/c/b;->a(ZZ)I

    move-result v3

    invoke-virtual {v0, v3}, Lcom/google/android/gms/audiomodem/an;->b(I)Lcom/google/android/gms/audiomodem/an;

    move-result-object v3

    invoke-static {}, Lcom/google/android/location/copresence/f/b;->b()Lcom/google/ac/b/c/g;

    move-result-object v0

    iget-object v0, v0, Lcom/google/ac/b/c/g;->n:Lcom/google/ac/b/c/c;

    iget-object v0, v0, Lcom/google/ac/b/c/c;->c:Ljava/lang/Double;

    invoke-virtual {v0}, Ljava/lang/Double;->floatValue()F

    move-result v4

    const/4 v0, 0x0

    cmpl-float v0, v4, v0

    if-lez v0, :cond_0

    move v0, v1

    :goto_0
    const-string v5, "coderSampleRate must be greater than zero"

    invoke-static {v0, v5}, Lcom/google/android/gms/common/internal/bx;->b(ZLjava/lang/Object;)V

    iput v4, v3, Lcom/google/android/gms/audiomodem/an;->a:F

    invoke-static {}, Lcom/google/android/location/copresence/f/b;->b()Lcom/google/ac/b/c/g;

    move-result-object v0

    iget-object v0, v0, Lcom/google/ac/b/c/g;->n:Lcom/google/ac/b/c/c;

    iget-object v0, v0, Lcom/google/ac/b/c/c;->d:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v4

    if-lez v4, :cond_1

    move v0, v1

    :goto_1
    const-string v5, "basebandDecimationFactor must be greater than zero"

    invoke-static {v0, v5}, Lcom/google/android/gms/common/internal/bx;->b(ZLjava/lang/Object;)V

    iput v4, v3, Lcom/google/android/gms/audiomodem/an;->b:I

    invoke-static {}, Lcom/google/android/location/copresence/f/b;->b()Lcom/google/ac/b/c/g;

    move-result-object v0

    iget-object v0, v0, Lcom/google/ac/b/c/g;->n:Lcom/google/ac/b/c/c;

    iget-object v0, v0, Lcom/google/ac/b/c/c;->e:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v4

    if-lez v4, :cond_2

    move v0, v1

    :goto_2
    const-string v5, "frequenciesPerSymbol must be greater than zero"

    invoke-static {v0, v5}, Lcom/google/android/gms/common/internal/bx;->b(ZLjava/lang/Object;)V

    iput v4, v3, Lcom/google/android/gms/audiomodem/an;->d:I

    invoke-static {}, Lcom/google/android/location/copresence/f/b;->b()Lcom/google/ac/b/c/g;

    move-result-object v0

    iget-object v0, v0, Lcom/google/ac/b/c/g;->n:Lcom/google/ac/b/c/c;

    iget-object v0, v0, Lcom/google/ac/b/c/c;->f:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-lez v0, :cond_3

    :goto_3
    const-string v2, "windowDurationMillis must be greater than zero"

    invoke-static {v1, v2}, Lcom/google/android/gms/common/internal/bx;->b(ZLjava/lang/Object;)V

    iput v0, v3, Lcom/google/android/gms/audiomodem/an;->c:I

    invoke-virtual {v3}, Lcom/google/android/gms/audiomodem/an;->a()Lcom/google/android/gms/audiomodem/Encoding;

    move-result-object v0

    return-object v0

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    move v0, v2

    goto :goto_1

    :cond_2
    move v0, v2

    goto :goto_2

    :cond_3
    move v1, v2

    goto :goto_3
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 33
    const/4 v0, 0x3

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 34
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Audio Stack: AudioModemHelper: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/location/copresence/c/b;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "connecting client"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->b(Ljava/lang/String;)I

    .line 36
    :cond_0
    iget-object v0, p0, Lcom/google/android/location/copresence/c/b;->b:Lcom/google/android/gms/common/api/v;

    invoke-interface {v0}, Lcom/google/android/gms/common/api/v;->b()V

    .line 37
    return-void
.end method

.method public final a(Lcom/google/android/gms/audiomodem/bf;)V
    .locals 2

    .prologue
    .line 120
    iget-object v0, p0, Lcom/google/android/location/copresence/c/b;->b:Lcom/google/android/gms/common/api/v;

    invoke-static {v0, p1}, Lcom/google/android/gms/audiomodem/d;->a(Lcom/google/android/gms/common/api/v;Lcom/google/android/gms/audiomodem/bf;)Lcom/google/android/gms/common/api/am;

    move-result-object v0

    new-instance v1, Lcom/google/android/location/copresence/c/g;

    invoke-direct {v1, p0}, Lcom/google/android/location/copresence/c/g;-><init>(Lcom/google/android/location/copresence/c/b;)V

    invoke-interface {v0, v1}, Lcom/google/android/gms/common/api/am;->a(Lcom/google/android/gms/common/api/aq;)V

    .line 130
    return-void
.end method

.method public final a(Lcom/google/android/gms/audiomodem/bf;Lcom/google/android/gms/audiomodem/TokenBroadcaster$Params;)V
    .locals 2

    .prologue
    .line 107
    iget-object v0, p0, Lcom/google/android/location/copresence/c/b;->b:Lcom/google/android/gms/common/api/v;

    invoke-static {v0, p1, p2}, Lcom/google/android/gms/audiomodem/d;->a(Lcom/google/android/gms/common/api/v;Lcom/google/android/gms/audiomodem/bf;Lcom/google/android/gms/audiomodem/TokenBroadcaster$Params;)Lcom/google/android/gms/common/api/am;

    move-result-object v0

    new-instance v1, Lcom/google/android/location/copresence/c/f;

    invoke-direct {v1, p0}, Lcom/google/android/location/copresence/c/f;-><init>(Lcom/google/android/location/copresence/c/b;)V

    invoke-interface {v0, v1}, Lcom/google/android/gms/common/api/am;->a(Lcom/google/android/gms/common/api/aq;)V

    .line 117
    return-void
.end method

.method public final a(Lcom/google/android/gms/audiomodem/bm;)V
    .locals 2

    .prologue
    .line 93
    iget-object v0, p0, Lcom/google/android/location/copresence/c/b;->b:Lcom/google/android/gms/common/api/v;

    invoke-static {v0, p1}, Lcom/google/android/gms/audiomodem/d;->a(Lcom/google/android/gms/common/api/v;Lcom/google/android/gms/audiomodem/bm;)Lcom/google/android/gms/common/api/am;

    move-result-object v0

    new-instance v1, Lcom/google/android/location/copresence/c/e;

    invoke-direct {v1, p0}, Lcom/google/android/location/copresence/c/e;-><init>(Lcom/google/android/location/copresence/c/b;)V

    invoke-interface {v0, v1}, Lcom/google/android/gms/common/api/am;->a(Lcom/google/android/gms/common/api/aq;)V

    .line 103
    return-void
.end method

.method public final a(Lcom/google/android/gms/audiomodem/bm;Lcom/google/android/gms/audiomodem/TokenReceiver$Params;ZJLcom/google/android/gms/audiomodem/bc;)V
    .locals 8

    .prologue
    const/4 v2, 0x1

    .line 59
    iget-object v0, p0, Lcom/google/android/location/copresence/c/b;->b:Lcom/google/android/gms/common/api/v;

    invoke-static {v0, p1, p2}, Lcom/google/android/gms/audiomodem/d;->a(Lcom/google/android/gms/common/api/v;Lcom/google/android/gms/audiomodem/bm;Lcom/google/android/gms/audiomodem/TokenReceiver$Params;)Lcom/google/android/gms/common/api/am;

    move-result-object v0

    new-instance v1, Lcom/google/android/location/copresence/c/c;

    invoke-direct {v1, p0}, Lcom/google/android/location/copresence/c/c;-><init>(Lcom/google/android/location/copresence/c/b;)V

    invoke-interface {v0, v1}, Lcom/google/android/gms/common/api/am;->a(Lcom/google/android/gms/common/api/aq;)V

    .line 70
    if-eqz p6, :cond_2

    .line 71
    new-instance v6, Lcom/google/android/gms/audiomodem/bd;

    invoke-virtual {p2}, Lcom/google/android/gms/audiomodem/TokenReceiver$Params;->c()[Lcom/google/android/gms/audiomodem/Encoding;

    move-result-object v0

    invoke-direct {v6, v0}, Lcom/google/android/gms/audiomodem/bd;-><init>([Lcom/google/android/gms/audiomodem/Encoding;)V

    .line 73
    if-eqz p3, :cond_0

    .line 74
    iput-boolean v2, v6, Lcom/google/android/gms/audiomodem/bd;->b:Z

    .line 76
    :cond_0
    const-wide/16 v0, -0x1

    cmp-long v0, p4, v0

    if-eqz v0, :cond_1

    .line 77
    const-wide/16 v0, 0x0

    cmp-long v0, p4, v0

    if-lez v0, :cond_3

    move v0, v2

    :goto_0
    invoke-static {v0}, Lcom/google/android/gms/common/internal/bx;->b(Z)V

    iput-boolean v2, v6, Lcom/google/android/gms/audiomodem/bd;->c:Z

    iput-wide p4, v6, Lcom/google/android/gms/audiomodem/bd;->d:J

    .line 79
    :cond_1
    iget-object v0, p0, Lcom/google/android/location/copresence/c/b;->b:Lcom/google/android/gms/common/api/v;

    new-instance v1, Lcom/google/android/gms/audiomodem/Snoop$Params;

    iget-object v3, v6, Lcom/google/android/gms/audiomodem/bd;->a:[Lcom/google/android/gms/audiomodem/Encoding;

    iget-boolean v4, v6, Lcom/google/android/gms/audiomodem/bd;->b:Z

    iget-boolean v5, v6, Lcom/google/android/gms/audiomodem/bd;->c:Z

    iget-wide v6, v6, Lcom/google/android/gms/audiomodem/bd;->d:J

    invoke-direct/range {v1 .. v7}, Lcom/google/android/gms/audiomodem/Snoop$Params;-><init>(I[Lcom/google/android/gms/audiomodem/Encoding;ZZJ)V

    invoke-static {v0, v1, p6}, Lcom/google/android/gms/audiomodem/d;->a(Lcom/google/android/gms/common/api/v;Lcom/google/android/gms/audiomodem/Snoop$Params;Lcom/google/android/gms/audiomodem/bc;)Lcom/google/android/gms/common/api/am;

    move-result-object v0

    new-instance v1, Lcom/google/android/location/copresence/c/d;

    invoke-direct {v1, p0}, Lcom/google/android/location/copresence/c/d;-><init>(Lcom/google/android/location/copresence/c/b;)V

    invoke-interface {v0, v1}, Lcom/google/android/gms/common/api/am;->a(Lcom/google/android/gms/common/api/aq;)V

    .line 90
    :cond_2
    return-void

    .line 77
    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method
