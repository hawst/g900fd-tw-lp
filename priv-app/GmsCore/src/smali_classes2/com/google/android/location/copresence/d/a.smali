.class public final Lcom/google/android/location/copresence/d/a;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/location/copresence/w;


# static fields
.field private static final b:Ljava/util/Set;


# instance fields
.field private final c:Lcom/google/android/location/copresence/d/j;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 17
    const/4 v0, 0x3

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Collections;->singleton(Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v0

    sput-object v0, Lcom/google/android/location/copresence/d/a;->b:Ljava/util/Set;

    return-void
.end method

.method constructor <init>(Lcom/google/android/location/copresence/d/j;)V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    iput-object p1, p0, Lcom/google/android/location/copresence/d/a;->c:Lcom/google/android/location/copresence/d/j;

    .line 24
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/location/copresence/b;)V
    .locals 2

    .prologue
    .line 39
    invoke-virtual {p0}, Lcom/google/android/location/copresence/d/a;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 40
    new-instance v0, Lcom/google/android/location/copresence/y;

    invoke-direct {v0}, Lcom/google/android/location/copresence/y;-><init>()V

    throw v0

    .line 42
    :cond_0
    iget-object v0, p0, Lcom/google/android/location/copresence/d/a;->c:Lcom/google/android/location/copresence/d/j;

    iget-object v1, p1, Lcom/google/android/location/copresence/b;->a:Lcom/google/android/gms/location/copresence/x;

    invoke-virtual {v0, v1}, Lcom/google/android/location/copresence/d/j;->a(Lcom/google/android/gms/location/copresence/x;)V

    .line 43
    return-void
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 33
    invoke-static {}, Lcom/google/android/location/copresence/f/b;->b()Lcom/google/ac/b/c/g;

    move-result-object v0

    iget-object v0, v0, Lcom/google/ac/b/c/g;->e:Lcom/google/ac/b/c/m;

    iget-object v0, v0, Lcom/google/ac/b/c/m;->g:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lcom/google/android/location/copresence/d/a;->c:Lcom/google/android/location/copresence/d/j;

    invoke-virtual {v0}, Lcom/google/android/location/copresence/d/j;->h()V

    .line 48
    return-void
.end method
