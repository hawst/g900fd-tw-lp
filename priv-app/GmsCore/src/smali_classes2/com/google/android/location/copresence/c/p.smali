.class final Lcom/google/android/location/copresence/c/p;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/location/copresence/c/l;


# instance fields
.field final synthetic a:Lcom/google/android/location/copresence/c/n;


# direct methods
.method constructor <init>(Lcom/google/android/location/copresence/c/n;)V
    .locals 0

    .prologue
    .line 71
    iput-object p1, p0, Lcom/google/android/location/copresence/c/p;->a:Lcom/google/android/location/copresence/c/n;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 102
    iget-object v0, p0, Lcom/google/android/location/copresence/c/p;->a:Lcom/google/android/location/copresence/c/n;

    invoke-static {v0}, Lcom/google/android/location/copresence/c/n;->f(Lcom/google/android/location/copresence/c/n;)V

    .line 103
    iget-object v0, p0, Lcom/google/android/location/copresence/c/p;->a:Lcom/google/android/location/copresence/c/n;

    invoke-static {v0}, Lcom/google/android/location/copresence/c/n;->g(Lcom/google/android/location/copresence/c/n;)Z

    .line 104
    iget-object v0, p0, Lcom/google/android/location/copresence/c/p;->a:Lcom/google/android/location/copresence/c/n;

    invoke-static {v0}, Lcom/google/android/location/copresence/c/n;->e(Lcom/google/android/location/copresence/c/n;)Lcom/google/android/location/copresence/ab;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 105
    iget-object v0, p0, Lcom/google/android/location/copresence/c/p;->a:Lcom/google/android/location/copresence/c/n;

    invoke-static {v0}, Lcom/google/android/location/copresence/c/n;->e(Lcom/google/android/location/copresence/c/n;)Lcom/google/android/location/copresence/ab;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/location/copresence/c/p;->a:Lcom/google/android/location/copresence/c/n;

    invoke-static {v1}, Lcom/google/android/location/copresence/c/n;->d(Lcom/google/android/location/copresence/c/n;)I

    move-result v1

    invoke-interface {v0, v1}, Lcom/google/android/location/copresence/ab;->a(I)V

    .line 107
    :cond_0
    return-void
.end method

.method public final a(Ljava/util/List;)V
    .locals 8

    .prologue
    .line 74
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 75
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 76
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/audiomodem/DecodedToken;

    .line 77
    new-instance v4, Lcom/google/android/gms/location/copresence/x;

    invoke-virtual {v0}, Lcom/google/android/gms/audiomodem/DecodedToken;->b()[B

    move-result-object v0

    invoke-direct {v4, v0}, Lcom/google/android/gms/location/copresence/x;-><init>([B)V

    .line 79
    const/4 v0, 0x3

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 80
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v5, "E2E Listen: AudioTokenListener: Received tokenId from audio: "

    invoke-direct {v0, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->b(Ljava/lang/String;)I

    .line 83
    :cond_0
    invoke-virtual {v4}, Lcom/google/android/gms/location/copresence/x;->a()Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/location/copresence/c/p;->a:Lcom/google/android/location/copresence/c/n;

    invoke-static {v0}, Lcom/google/android/location/copresence/c/n;->d(Lcom/google/android/location/copresence/c/n;)I

    invoke-static {}, Lcom/google/android/location/copresence/debug/d;->b()V

    .line 85
    new-instance v0, Lcom/google/ac/b/c/bu;

    invoke-direct {v0}, Lcom/google/ac/b/c/bu;-><init>()V

    .line 86
    invoke-virtual {v4}, Lcom/google/android/gms/location/copresence/x;->a()Ljava/lang/String;

    move-result-object v5

    iput-object v5, v0, Lcom/google/ac/b/c/bu;->a:Ljava/lang/String;

    .line 87
    const/4 v5, 0x1

    new-array v5, v5, [Lcom/google/ac/b/c/bv;

    iput-object v5, v0, Lcom/google/ac/b/c/bu;->b:[Lcom/google/ac/b/c/bv;

    .line 88
    new-instance v5, Lcom/google/ac/b/c/bv;

    invoke-direct {v5}, Lcom/google/ac/b/c/bv;-><init>()V

    .line 89
    iget-object v6, p0, Lcom/google/android/location/copresence/c/p;->a:Lcom/google/android/location/copresence/c/n;

    invoke-static {v6}, Lcom/google/android/location/copresence/c/n;->d(Lcom/google/android/location/copresence/c/n;)I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    iput-object v6, v5, Lcom/google/ac/b/c/bv;->a:Ljava/lang/Integer;

    .line 90
    iget-object v6, v0, Lcom/google/ac/b/c/bu;->b:[Lcom/google/ac/b/c/bv;

    const/4 v7, 0x0

    aput-object v5, v6, v7

    .line 92
    invoke-interface {v1, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 93
    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 96
    :cond_1
    iget-object v0, p0, Lcom/google/android/location/copresence/c/p;->a:Lcom/google/android/location/copresence/c/n;

    invoke-static {v0}, Lcom/google/android/location/copresence/c/n;->e(Lcom/google/android/location/copresence/c/n;)Lcom/google/android/location/copresence/ab;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 97
    iget-object v0, p0, Lcom/google/android/location/copresence/c/p;->a:Lcom/google/android/location/copresence/c/n;

    invoke-static {v0}, Lcom/google/android/location/copresence/c/n;->e(Lcom/google/android/location/copresence/c/n;)Lcom/google/android/location/copresence/ab;

    move-result-object v0

    invoke-interface {v0, v1, v2}, Lcom/google/android/location/copresence/ab;->a(Ljava/util/List;Ljava/util/List;)V

    .line 99
    :cond_2
    return-void
.end method
