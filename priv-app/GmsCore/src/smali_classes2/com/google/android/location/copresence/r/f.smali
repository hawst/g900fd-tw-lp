.class final Lcom/google/android/location/copresence/r/f;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final c:Ljava/util/Set;


# instance fields
.field a:Lcom/google/android/location/copresence/k/f;

.field b:Lcom/google/android/location/copresence/k/f;

.field private final d:Landroid/content/Context;

.field private final e:Lcom/google/android/location/copresence/r/ae;

.field private final f:Landroid/content/SharedPreferences;

.field private final g:Ljava/util/Random;

.field private final h:Lcom/google/android/location/copresence/k/f;

.field private final i:Lcom/google/android/location/copresence/k/f;

.field private final j:Lcom/google/android/location/copresence/k/f;

.field private final k:Lcom/google/android/location/copresence/k/f;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 46
    new-instance v0, Ljava/util/HashSet;

    const/4 v1, 0x4

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "ssid"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string v3, "shared_key"

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-string v3, "allowedAuthAlgorithms"

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const-string v3, "allowedKeyManagement"

    aput-object v3, v1, v2

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    sput-object v0, Lcom/google/android/location/copresence/r/f;->c:Ljava/util/Set;

    return-void
.end method

.method constructor <init>(Landroid/content/Context;Lcom/google/android/location/copresence/r/ae;Landroid/content/SharedPreferences;)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 65
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 72
    new-instance v0, Lcom/google/android/location/copresence/r/g;

    const-string v1, "TetheringSettingSaved"

    new-array v2, v4, [Lcom/google/android/location/copresence/k/f;

    invoke-direct {v0, p0, v1, v2}, Lcom/google/android/location/copresence/r/g;-><init>(Lcom/google/android/location/copresence/r/f;Ljava/lang/String;[Lcom/google/android/location/copresence/k/f;)V

    iput-object v0, p0, Lcom/google/android/location/copresence/r/f;->h:Lcom/google/android/location/copresence/k/f;

    .line 98
    new-instance v0, Lcom/google/android/location/copresence/r/h;

    const-string v1, "TetheringDisabled"

    new-array v2, v5, [Lcom/google/android/location/copresence/k/f;

    iget-object v3, p0, Lcom/google/android/location/copresence/r/f;->h:Lcom/google/android/location/copresence/k/f;

    aput-object v3, v2, v4

    invoke-direct {v0, p0, v1, v2}, Lcom/google/android/location/copresence/r/h;-><init>(Lcom/google/android/location/copresence/r/f;Ljava/lang/String;[Lcom/google/android/location/copresence/k/f;)V

    iput-object v0, p0, Lcom/google/android/location/copresence/r/f;->i:Lcom/google/android/location/copresence/k/f;

    .line 120
    new-instance v0, Lcom/google/android/location/copresence/r/i;

    const-string v1, "TetheringReverted"

    new-array v2, v4, [Lcom/google/android/location/copresence/k/f;

    invoke-direct {v0, p0, v1, v2}, Lcom/google/android/location/copresence/r/i;-><init>(Lcom/google/android/location/copresence/r/f;Ljava/lang/String;[Lcom/google/android/location/copresence/k/f;)V

    iput-object v0, p0, Lcom/google/android/location/copresence/r/f;->a:Lcom/google/android/location/copresence/k/f;

    .line 151
    new-instance v0, Lcom/google/android/location/copresence/r/j;

    const-string v1, "SavedWifiApConfig"

    new-array v2, v4, [Lcom/google/android/location/copresence/k/f;

    invoke-direct {v0, p0, v1, v2}, Lcom/google/android/location/copresence/r/j;-><init>(Lcom/google/android/location/copresence/r/f;Ljava/lang/String;[Lcom/google/android/location/copresence/k/f;)V

    iput-object v0, p0, Lcom/google/android/location/copresence/r/f;->j:Lcom/google/android/location/copresence/k/f;

    .line 178
    new-instance v0, Lcom/google/android/location/copresence/r/k;

    const-string v1, "RevertedWifiApConfig"

    new-array v2, v4, [Lcom/google/android/location/copresence/k/f;

    invoke-direct {v0, p0, v1, v2}, Lcom/google/android/location/copresence/r/k;-><init>(Lcom/google/android/location/copresence/r/f;Ljava/lang/String;[Lcom/google/android/location/copresence/k/f;)V

    iput-object v0, p0, Lcom/google/android/location/copresence/r/f;->k:Lcom/google/android/location/copresence/k/f;

    .line 229
    new-instance v0, Lcom/google/android/location/copresence/r/l;

    const-string v1, "Unmodified"

    const/4 v2, 0x2

    new-array v2, v2, [Lcom/google/android/location/copresence/k/f;

    iget-object v3, p0, Lcom/google/android/location/copresence/r/f;->k:Lcom/google/android/location/copresence/k/f;

    aput-object v3, v2, v4

    iget-object v3, p0, Lcom/google/android/location/copresence/r/f;->a:Lcom/google/android/location/copresence/k/f;

    aput-object v3, v2, v5

    invoke-direct {v0, p0, v1, v2}, Lcom/google/android/location/copresence/r/l;-><init>(Lcom/google/android/location/copresence/r/f;Ljava/lang/String;[Lcom/google/android/location/copresence/k/f;)V

    iput-object v0, p0, Lcom/google/android/location/copresence/r/f;->b:Lcom/google/android/location/copresence/k/f;

    .line 66
    iput-object p1, p0, Lcom/google/android/location/copresence/r/f;->d:Landroid/content/Context;

    .line 67
    iput-object p2, p0, Lcom/google/android/location/copresence/r/f;->e:Lcom/google/android/location/copresence/r/ae;

    .line 68
    iput-object p3, p0, Lcom/google/android/location/copresence/r/f;->f:Landroid/content/SharedPreferences;

    .line 69
    new-instance v0, Ljava/util/Random;

    invoke-direct {v0}, Ljava/util/Random;-><init>()V

    iput-object v0, p0, Lcom/google/android/location/copresence/r/f;->g:Ljava/util/Random;

    .line 70
    return-void
.end method

.method static synthetic a(Lcom/google/android/location/copresence/r/f;Ljava/lang/String;)I
    .locals 3

    .prologue
    .line 35
    iget-object v0, p0, Lcom/google/android/location/copresence/r/f;->d:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x11

    if-ge v1, v2, :cond_0

    invoke-static {v0, p1}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;)I

    move-result v0

    :goto_0
    return v0

    :cond_0
    invoke-static {v0, p1}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;)I

    move-result v0

    goto :goto_0
.end method

.method static synthetic a(Lcom/google/android/location/copresence/r/f;Ljava/lang/String;I)I
    .locals 3

    .prologue
    .line 35
    iget-object v0, p0, Lcom/google/android/location/copresence/r/f;->d:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x11

    if-ge v1, v2, :cond_0

    invoke-static {v0, p1, p2}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    :goto_0
    return v0

    :cond_0
    invoke-static {v0, p1, p2}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    goto :goto_0
.end method

.method static synthetic a(Landroid/content/SharedPreferences$Editor;Landroid/net/wifi/WifiConfiguration;)Landroid/content/SharedPreferences$Editor;
    .locals 3

    .prologue
    .line 35
    invoke-static {p1}, Lcom/google/android/location/copresence/r/f;->b(Landroid/net/wifi/WifiConfiguration;)Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-interface {p0, v1, v0}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    goto :goto_0

    :cond_0
    return-object p0
.end method

.method static synthetic a(Lcom/google/android/location/copresence/r/f;)Landroid/content/SharedPreferences;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lcom/google/android/location/copresence/r/f;->f:Landroid/content/SharedPreferences;

    return-object v0
.end method

.method private static a(Ljava/util/BitSet;)Ljava/lang/String;
    .locals 4

    .prologue
    .line 349
    if-nez p0, :cond_0

    .line 350
    const/4 v0, 0x0

    .line 359
    :goto_0
    return-object v0

    .line 352
    :cond_0
    const/4 v1, -0x1

    .line 353
    const/4 v0, 0x0

    .line 354
    invoke-virtual {p0}, Ljava/util/BitSet;->cardinality()I

    move-result v2

    new-array v2, v2, [Ljava/lang/Integer;

    .line 355
    :goto_1
    add-int/lit8 v1, v1, 0x1

    invoke-virtual {p0, v1}, Ljava/util/BitSet;->nextSetBit(I)I

    move-result v1

    if-ltz v1, :cond_1

    .line 356
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v0

    .line 357
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 359
    :cond_1
    const-string v0, ","

    invoke-static {v0, v2}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private static a(Ljava/lang/String;)Ljava/util/BitSet;
    .locals 5

    .prologue
    .line 363
    if-eqz p0, :cond_0

    const-string v0, "null"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 364
    :cond_0
    const/4 v0, 0x0

    .line 371
    :cond_1
    return-object v0

    .line 366
    :cond_2
    new-instance v0, Ljava/util/BitSet;

    invoke-direct {v0}, Ljava/util/BitSet;-><init>()V

    .line 367
    const-string v1, ","

    invoke-static {p0, v1}, Landroid/text/TextUtils;->split(Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 368
    array-length v3, v2

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    .line 369
    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    invoke-virtual {v0, v4}, Ljava/util/BitSet;->set(I)V

    .line 368
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method static synthetic a(Landroid/net/wifi/WifiConfiguration;)Ljava/util/Map;
    .locals 1

    .prologue
    .line 35
    invoke-static {p0}, Lcom/google/android/location/copresence/r/f;->b(Landroid/net/wifi/WifiConfiguration;)Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a()Ljava/util/Set;
    .locals 1

    .prologue
    .line 35
    sget-object v0, Lcom/google/android/location/copresence/r/f;->c:Ljava/util/Set;

    return-object v0
.end method

.method static synthetic a(Landroid/content/SharedPreferences;Landroid/net/wifi/WifiConfiguration;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 35
    const-string v0, "ssid"

    invoke-interface {p0, v0, v1}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p1, Landroid/net/wifi/WifiConfiguration;->SSID:Ljava/lang/String;

    const-string v0, "shared_key"

    invoke-interface {p0, v0, v1}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p1, Landroid/net/wifi/WifiConfiguration;->preSharedKey:Ljava/lang/String;

    const-string v0, "allowedAuthAlgorithms"

    invoke-interface {p0, v0, v1}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/location/copresence/r/f;->a(Ljava/lang/String;)Ljava/util/BitSet;

    move-result-object v0

    iput-object v0, p1, Landroid/net/wifi/WifiConfiguration;->allowedAuthAlgorithms:Ljava/util/BitSet;

    const-string v0, "allowedKeyManagement"

    invoke-interface {p0, v0, v1}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/location/copresence/r/f;->a(Ljava/lang/String;)Ljava/util/BitSet;

    move-result-object v0

    iput-object v0, p1, Landroid/net/wifi/WifiConfiguration;->allowedKeyManagement:Ljava/util/BitSet;

    return-void
.end method

.method static synthetic a(Landroid/content/SharedPreferences$Editor;Ljava/lang/String;)Z
    .locals 4

    .prologue
    .line 35
    const-string v0, "updated"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-interface {p0, v0, v2, v3}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v1, 0x3

    invoke-static {v1}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v1

    if-eqz v1, :cond_0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "WifiApStates2: Completed store to disk and "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/location/copresence/ag;->b(Ljava/lang/String;)I

    :cond_0
    :goto_0
    return v0

    :cond_1
    const/4 v1, 0x6

    invoke-static {v1}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v1

    if-eqz v1, :cond_0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "WifiApStates2: Failed to store to disk and "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/location/copresence/ag;->e(Ljava/lang/String;)I

    goto :goto_0
.end method

.method static synthetic b(Lcom/google/android/location/copresence/r/f;)Lcom/google/android/location/copresence/r/ae;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lcom/google/android/location/copresence/r/f;->e:Lcom/google/android/location/copresence/r/ae;

    return-object v0
.end method

.method private static b(Landroid/net/wifi/WifiConfiguration;)Ljava/util/Map;
    .locals 3

    .prologue
    .line 322
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 323
    const-string v1, "ssid"

    iget-object v2, p0, Landroid/net/wifi/WifiConfiguration;->SSID:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 324
    const-string v1, "shared_key"

    iget-object v2, p0, Landroid/net/wifi/WifiConfiguration;->preSharedKey:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 325
    const-string v1, "allowedAuthAlgorithms"

    iget-object v2, p0, Landroid/net/wifi/WifiConfiguration;->allowedAuthAlgorithms:Ljava/util/BitSet;

    invoke-static {v2}, Lcom/google/android/location/copresence/r/f;->a(Ljava/util/BitSet;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 326
    const-string v1, "allowedKeyManagement"

    iget-object v2, p0, Landroid/net/wifi/WifiConfiguration;->allowedKeyManagement:Ljava/util/BitSet;

    invoke-static {v2}, Lcom/google/android/location/copresence/r/f;->a(Ljava/util/BitSet;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 327
    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v1

    sget-object v2, Lcom/google/android/location/copresence/r/f;->c:Ljava/util/Set;

    invoke-interface {v1, v2}, Ljava/util/Set;->equals(Ljava/lang/Object;)Z

    move-result v1

    invoke-static {v1}, Lcom/google/k/a/ah;->a(Z)V

    .line 328
    return-object v0
.end method

.method static synthetic b(Lcom/google/android/location/copresence/r/f;Ljava/lang/String;I)Z
    .locals 3

    .prologue
    .line 35
    iget-object v0, p0, Lcom/google/android/location/copresence/r/f;->d:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x11

    if-ge v1, v2, :cond_0

    invoke-static {v0, p1, p2}, Landroid/provider/Settings$Secure;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    invoke-static {v0, p1, p2}, Landroid/provider/Settings$Global;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    move-result v0

    goto :goto_0
.end method

.method static synthetic c(Lcom/google/android/location/copresence/r/f;)Lcom/google/android/location/copresence/k/f;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lcom/google/android/location/copresence/r/f;->j:Lcom/google/android/location/copresence/k/f;

    return-object v0
.end method

.method static synthetic d(Lcom/google/android/location/copresence/r/f;)Lcom/google/android/location/copresence/k/f;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lcom/google/android/location/copresence/r/f;->i:Lcom/google/android/location/copresence/k/f;

    return-object v0
.end method

.method static synthetic e(Lcom/google/android/location/copresence/r/f;)Ljava/util/Random;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lcom/google/android/location/copresence/r/f;->g:Ljava/util/Random;

    return-object v0
.end method
