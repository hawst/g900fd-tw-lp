.class final Lcom/google/android/location/copresence/d/z;
.super Lcom/google/android/location/copresence/k/f;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/location/copresence/d/n;


# direct methods
.method varargs constructor <init>(Lcom/google/android/location/copresence/d/n;Ljava/lang/String;[Lcom/google/android/location/copresence/k/f;)V
    .locals 0

    .prologue
    .line 332
    iput-object p1, p0, Lcom/google/android/location/copresence/d/z;->a:Lcom/google/android/location/copresence/d/n;

    invoke-direct {p0, p2, p3}, Lcom/google/android/location/copresence/k/f;-><init>(Ljava/lang/String;[Lcom/google/android/location/copresence/k/f;)V

    return-void
.end method

.method private e()I
    .locals 3

    .prologue
    const/4 v2, -0x1

    .line 374
    iget-object v0, p0, Lcom/google/android/location/copresence/d/z;->a:Lcom/google/android/location/copresence/d/n;

    iget-object v0, v0, Lcom/google/android/location/copresence/d/n;->c:Landroid/content/SharedPreferences;

    const-string v1, "discoverableTimeout"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 376
    if-ne v0, v2, :cond_0

    const/4 v1, 0x6

    invoke-static {v1}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 377
    const-string v1, "BluetoothStates: User discoverable timeout not stored."

    invoke-static {v1}, Lcom/google/android/location/copresence/ag;->e(Ljava/lang/String;)I

    .line 379
    :cond_0
    return v0
.end method

.method private f()I
    .locals 3

    .prologue
    const/4 v2, -0x1

    .line 383
    iget-object v0, p0, Lcom/google/android/location/copresence/d/z;->a:Lcom/google/android/location/copresence/d/n;

    iget-object v0, v0, Lcom/google/android/location/copresence/d/n;->c:Landroid/content/SharedPreferences;

    const-string v1, "scanMode"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 384
    if-ne v0, v2, :cond_0

    const/4 v1, 0x6

    invoke-static {v1}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 385
    const-string v1, "BluetoothStates: Scan mode not stored."

    invoke-static {v1}, Lcom/google/android/location/copresence/ag;->e(Ljava/lang/String;)I

    .line 387
    :cond_0
    return v0
.end method


# virtual methods
.method public final a()Z
    .locals 3

    .prologue
    .line 365
    iget-object v0, p0, Lcom/google/android/location/copresence/d/z;->a:Lcom/google/android/location/copresence/d/n;

    iget-object v0, v0, Lcom/google/android/location/copresence/d/n;->g:Lcom/google/android/location/copresence/k/f;

    invoke-virtual {v0}, Lcom/google/android/location/copresence/k/f;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 366
    iget-object v0, p0, Lcom/google/android/location/copresence/d/z;->a:Lcom/google/android/location/copresence/d/n;

    iget-object v0, v0, Lcom/google/android/location/copresence/d/n;->b:Lcom/google/android/location/copresence/d/e;

    invoke-direct {p0}, Lcom/google/android/location/copresence/d/z;->f()I

    move-result v1

    invoke-direct {p0}, Lcom/google/android/location/copresence/d/z;->e()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/location/copresence/d/e;->a(II)Z

    move-result v0

    .line 370
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final b()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 353
    iget-object v1, p0, Lcom/google/android/location/copresence/d/z;->a:Lcom/google/android/location/copresence/d/n;

    iget-object v1, v1, Lcom/google/android/location/copresence/d/n;->g:Lcom/google/android/location/copresence/k/f;

    invoke-virtual {v1}, Lcom/google/android/location/copresence/k/f;->b()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 354
    iget-object v1, p0, Lcom/google/android/location/copresence/d/z;->a:Lcom/google/android/location/copresence/d/n;

    iget-object v1, v1, Lcom/google/android/location/copresence/d/n;->b:Lcom/google/android/location/copresence/d/e;

    iget-object v1, v1, Lcom/google/android/location/copresence/d/e;->a:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v1}, Landroid/bluetooth/BluetoothAdapter;->isEnabled()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-direct {p0}, Lcom/google/android/location/copresence/d/z;->e()I

    move-result v1

    iget-object v2, p0, Lcom/google/android/location/copresence/d/z;->a:Lcom/google/android/location/copresence/d/n;

    iget-object v2, v2, Lcom/google/android/location/copresence/d/n;->b:Lcom/google/android/location/copresence/d/e;

    invoke-virtual {v2}, Lcom/google/android/location/copresence/d/e;->c()I

    move-result v2

    if-ne v1, v2, :cond_1

    invoke-direct {p0}, Lcom/google/android/location/copresence/d/z;->f()I

    move-result v1

    iget-object v2, p0, Lcom/google/android/location/copresence/d/z;->a:Lcom/google/android/location/copresence/d/n;

    iget-object v2, v2, Lcom/google/android/location/copresence/d/n;->b:Lcom/google/android/location/copresence/d/e;

    iget-object v2, v2, Lcom/google/android/location/copresence/d/e;->a:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v2}, Landroid/bluetooth/BluetoothAdapter;->getScanMode()I

    move-result v2

    if-ne v1, v2, :cond_1

    .line 360
    :cond_0
    :goto_0
    return v0

    .line 354
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c()Lcom/google/android/location/copresence/k/f;
    .locals 1

    .prologue
    .line 336
    iget-object v0, p0, Lcom/google/android/location/copresence/d/z;->a:Lcom/google/android/location/copresence/d/n;

    iget-object v0, v0, Lcom/google/android/location/copresence/d/n;->g:Lcom/google/android/location/copresence/k/f;

    invoke-virtual {v0}, Lcom/google/android/location/copresence/k/f;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/location/copresence/d/z;->a:Lcom/google/android/location/copresence/d/n;

    iget-object v0, v0, Lcom/google/android/location/copresence/d/n;->f:Lcom/google/android/location/copresence/k/f;

    invoke-virtual {v0}, Lcom/google/android/location/copresence/k/f;->b()Z

    move-result v0

    if-nez v0, :cond_0

    .line 337
    iget-object v0, p0, Lcom/google/android/location/copresence/d/z;->a:Lcom/google/android/location/copresence/d/n;

    iget-object v0, v0, Lcom/google/android/location/copresence/d/n;->f:Lcom/google/android/location/copresence/k/f;

    .line 348
    :goto_0
    return-object v0

    .line 345
    :cond_0
    iget-object v0, p0, Lcom/google/android/location/copresence/d/z;->a:Lcom/google/android/location/copresence/d/n;

    iget-object v0, v0, Lcom/google/android/location/copresence/d/n;->g:Lcom/google/android/location/copresence/k/f;

    invoke-virtual {v0}, Lcom/google/android/location/copresence/k/f;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/location/copresence/d/z;->a:Lcom/google/android/location/copresence/d/n;

    iget-object v0, v0, Lcom/google/android/location/copresence/d/n;->l:Lcom/google/android/location/copresence/k/f;

    invoke-virtual {v0}, Lcom/google/android/location/copresence/k/f;->b()Z

    move-result v0

    if-nez v0, :cond_1

    .line 346
    iget-object v0, p0, Lcom/google/android/location/copresence/d/z;->a:Lcom/google/android/location/copresence/d/n;

    iget-object v0, v0, Lcom/google/android/location/copresence/d/n;->l:Lcom/google/android/location/copresence/k/f;

    goto :goto_0

    .line 348
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
