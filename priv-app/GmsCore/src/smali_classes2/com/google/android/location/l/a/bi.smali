.class public final Lcom/google/android/location/l/a/bi;
.super Lcom/google/protobuf/nano/d;
.source "SourceFile"


# static fields
.field private static volatile m:[Lcom/google/android/location/l/a/bi;


# instance fields
.field public a:Ljava/lang/String;

.field public b:Lcom/google/android/location/l/a/az;

.field public c:Lcom/google/android/location/l/a/bn;

.field public d:Lcom/google/android/location/l/a/bo;

.field public e:Lcom/google/android/location/l/a/bq;

.field public f:Lcom/google/android/location/l/a/bb;

.field public g:Lcom/google/android/location/l/a/bh;

.field public h:Lcom/google/android/location/l/a/bl;

.field public i:Lcom/google/android/location/l/a/at;

.field public j:Lcom/google/android/location/l/a/bs;

.field public k:Lcom/google/android/location/l/a/ax;

.field public l:Lcom/google/android/location/l/a/d;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 59
    invoke-direct {p0}, Lcom/google/protobuf/nano/d;-><init>()V

    .line 60
    iput-object v0, p0, Lcom/google/android/location/l/a/bi;->a:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/location/l/a/bi;->b:Lcom/google/android/location/l/a/az;

    iput-object v0, p0, Lcom/google/android/location/l/a/bi;->c:Lcom/google/android/location/l/a/bn;

    iput-object v0, p0, Lcom/google/android/location/l/a/bi;->d:Lcom/google/android/location/l/a/bo;

    iput-object v0, p0, Lcom/google/android/location/l/a/bi;->e:Lcom/google/android/location/l/a/bq;

    iput-object v0, p0, Lcom/google/android/location/l/a/bi;->f:Lcom/google/android/location/l/a/bb;

    iput-object v0, p0, Lcom/google/android/location/l/a/bi;->g:Lcom/google/android/location/l/a/bh;

    iput-object v0, p0, Lcom/google/android/location/l/a/bi;->h:Lcom/google/android/location/l/a/bl;

    iput-object v0, p0, Lcom/google/android/location/l/a/bi;->i:Lcom/google/android/location/l/a/at;

    iput-object v0, p0, Lcom/google/android/location/l/a/bi;->j:Lcom/google/android/location/l/a/bs;

    iput-object v0, p0, Lcom/google/android/location/l/a/bi;->k:Lcom/google/android/location/l/a/ax;

    iput-object v0, p0, Lcom/google/android/location/l/a/bi;->l:Lcom/google/android/location/l/a/d;

    iput-object v0, p0, Lcom/google/android/location/l/a/bi;->unknownFieldData:Lcom/google/protobuf/nano/f;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/location/l/a/bi;->cachedSize:I

    .line 61
    return-void
.end method

.method public static a()[Lcom/google/android/location/l/a/bi;
    .locals 2

    .prologue
    .line 12
    sget-object v0, Lcom/google/android/location/l/a/bi;->m:[Lcom/google/android/location/l/a/bi;

    if-nez v0, :cond_1

    .line 13
    sget-object v1, Lcom/google/protobuf/nano/h;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 15
    :try_start_0
    sget-object v0, Lcom/google/android/location/l/a/bi;->m:[Lcom/google/android/location/l/a/bi;

    if-nez v0, :cond_0

    .line 16
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/android/location/l/a/bi;

    sput-object v0, Lcom/google/android/location/l/a/bi;->m:[Lcom/google/android/location/l/a/bi;

    .line 18
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 20
    :cond_1
    sget-object v0, Lcom/google/android/location/l/a/bi;->m:[Lcom/google/android/location/l/a/bi;

    return-object v0

    .line 18
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 274
    invoke-super {p0}, Lcom/google/protobuf/nano/d;->computeSerializedSize()I

    move-result v0

    .line 275
    iget-object v1, p0, Lcom/google/android/location/l/a/bi;->a:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 276
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/location/l/a/bi;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 279
    :cond_0
    iget-object v1, p0, Lcom/google/android/location/l/a/bi;->b:Lcom/google/android/location/l/a/az;

    if-eqz v1, :cond_1

    .line 280
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/location/l/a/bi;->b:Lcom/google/android/location/l/a/az;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 283
    :cond_1
    iget-object v1, p0, Lcom/google/android/location/l/a/bi;->c:Lcom/google/android/location/l/a/bn;

    if-eqz v1, :cond_2

    .line 284
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/android/location/l/a/bi;->c:Lcom/google/android/location/l/a/bn;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 287
    :cond_2
    iget-object v1, p0, Lcom/google/android/location/l/a/bi;->d:Lcom/google/android/location/l/a/bo;

    if-eqz v1, :cond_3

    .line 288
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/android/location/l/a/bi;->d:Lcom/google/android/location/l/a/bo;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 291
    :cond_3
    iget-object v1, p0, Lcom/google/android/location/l/a/bi;->e:Lcom/google/android/location/l/a/bq;

    if-eqz v1, :cond_4

    .line 292
    const/4 v1, 0x5

    iget-object v2, p0, Lcom/google/android/location/l/a/bi;->e:Lcom/google/android/location/l/a/bq;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 295
    :cond_4
    iget-object v1, p0, Lcom/google/android/location/l/a/bi;->f:Lcom/google/android/location/l/a/bb;

    if-eqz v1, :cond_5

    .line 296
    const/4 v1, 0x6

    iget-object v2, p0, Lcom/google/android/location/l/a/bi;->f:Lcom/google/android/location/l/a/bb;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 299
    :cond_5
    iget-object v1, p0, Lcom/google/android/location/l/a/bi;->g:Lcom/google/android/location/l/a/bh;

    if-eqz v1, :cond_6

    .line 300
    const/4 v1, 0x7

    iget-object v2, p0, Lcom/google/android/location/l/a/bi;->g:Lcom/google/android/location/l/a/bh;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 303
    :cond_6
    iget-object v1, p0, Lcom/google/android/location/l/a/bi;->h:Lcom/google/android/location/l/a/bl;

    if-eqz v1, :cond_7

    .line 304
    const/16 v1, 0x8

    iget-object v2, p0, Lcom/google/android/location/l/a/bi;->h:Lcom/google/android/location/l/a/bl;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 307
    :cond_7
    iget-object v1, p0, Lcom/google/android/location/l/a/bi;->l:Lcom/google/android/location/l/a/d;

    if-eqz v1, :cond_8

    .line 308
    const/16 v1, 0x9

    iget-object v2, p0, Lcom/google/android/location/l/a/bi;->l:Lcom/google/android/location/l/a/d;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 311
    :cond_8
    iget-object v1, p0, Lcom/google/android/location/l/a/bi;->i:Lcom/google/android/location/l/a/at;

    if-eqz v1, :cond_9

    .line 312
    const/16 v1, 0xa

    iget-object v2, p0, Lcom/google/android/location/l/a/bi;->i:Lcom/google/android/location/l/a/at;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 315
    :cond_9
    iget-object v1, p0, Lcom/google/android/location/l/a/bi;->j:Lcom/google/android/location/l/a/bs;

    if-eqz v1, :cond_a

    .line 316
    const/16 v1, 0xb

    iget-object v2, p0, Lcom/google/android/location/l/a/bi;->j:Lcom/google/android/location/l/a/bs;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 319
    :cond_a
    iget-object v1, p0, Lcom/google/android/location/l/a/bi;->k:Lcom/google/android/location/l/a/ax;

    if-eqz v1, :cond_b

    .line 320
    const/16 v1, 0xc

    iget-object v2, p0, Lcom/google/android/location/l/a/bi;->k:Lcom/google/android/location/l/a/ax;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 323
    :cond_b
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 83
    if-ne p1, p0, :cond_1

    .line 84
    const/4 v0, 0x1

    .line 196
    :cond_0
    :goto_0
    return v0

    .line 86
    :cond_1
    instance-of v1, p1, Lcom/google/android/location/l/a/bi;

    if-eqz v1, :cond_0

    .line 89
    check-cast p1, Lcom/google/android/location/l/a/bi;

    .line 90
    iget-object v1, p0, Lcom/google/android/location/l/a/bi;->a:Ljava/lang/String;

    if-nez v1, :cond_e

    .line 91
    iget-object v1, p1, Lcom/google/android/location/l/a/bi;->a:Ljava/lang/String;

    if-nez v1, :cond_0

    .line 97
    :cond_2
    iget-object v1, p0, Lcom/google/android/location/l/a/bi;->b:Lcom/google/android/location/l/a/az;

    if-nez v1, :cond_f

    .line 98
    iget-object v1, p1, Lcom/google/android/location/l/a/bi;->b:Lcom/google/android/location/l/a/az;

    if-nez v1, :cond_0

    .line 106
    :cond_3
    iget-object v1, p0, Lcom/google/android/location/l/a/bi;->c:Lcom/google/android/location/l/a/bn;

    if-nez v1, :cond_10

    .line 107
    iget-object v1, p1, Lcom/google/android/location/l/a/bi;->c:Lcom/google/android/location/l/a/bn;

    if-nez v1, :cond_0

    .line 115
    :cond_4
    iget-object v1, p0, Lcom/google/android/location/l/a/bi;->d:Lcom/google/android/location/l/a/bo;

    if-nez v1, :cond_11

    .line 116
    iget-object v1, p1, Lcom/google/android/location/l/a/bi;->d:Lcom/google/android/location/l/a/bo;

    if-nez v1, :cond_0

    .line 124
    :cond_5
    iget-object v1, p0, Lcom/google/android/location/l/a/bi;->e:Lcom/google/android/location/l/a/bq;

    if-nez v1, :cond_12

    .line 125
    iget-object v1, p1, Lcom/google/android/location/l/a/bi;->e:Lcom/google/android/location/l/a/bq;

    if-nez v1, :cond_0

    .line 133
    :cond_6
    iget-object v1, p0, Lcom/google/android/location/l/a/bi;->f:Lcom/google/android/location/l/a/bb;

    if-nez v1, :cond_13

    .line 134
    iget-object v1, p1, Lcom/google/android/location/l/a/bi;->f:Lcom/google/android/location/l/a/bb;

    if-nez v1, :cond_0

    .line 142
    :cond_7
    iget-object v1, p0, Lcom/google/android/location/l/a/bi;->g:Lcom/google/android/location/l/a/bh;

    if-nez v1, :cond_14

    .line 143
    iget-object v1, p1, Lcom/google/android/location/l/a/bi;->g:Lcom/google/android/location/l/a/bh;

    if-nez v1, :cond_0

    .line 151
    :cond_8
    iget-object v1, p0, Lcom/google/android/location/l/a/bi;->h:Lcom/google/android/location/l/a/bl;

    if-nez v1, :cond_15

    .line 152
    iget-object v1, p1, Lcom/google/android/location/l/a/bi;->h:Lcom/google/android/location/l/a/bl;

    if-nez v1, :cond_0

    .line 160
    :cond_9
    iget-object v1, p0, Lcom/google/android/location/l/a/bi;->i:Lcom/google/android/location/l/a/at;

    if-nez v1, :cond_16

    .line 161
    iget-object v1, p1, Lcom/google/android/location/l/a/bi;->i:Lcom/google/android/location/l/a/at;

    if-nez v1, :cond_0

    .line 169
    :cond_a
    iget-object v1, p0, Lcom/google/android/location/l/a/bi;->j:Lcom/google/android/location/l/a/bs;

    if-nez v1, :cond_17

    .line 170
    iget-object v1, p1, Lcom/google/android/location/l/a/bi;->j:Lcom/google/android/location/l/a/bs;

    if-nez v1, :cond_0

    .line 178
    :cond_b
    iget-object v1, p0, Lcom/google/android/location/l/a/bi;->k:Lcom/google/android/location/l/a/ax;

    if-nez v1, :cond_18

    .line 179
    iget-object v1, p1, Lcom/google/android/location/l/a/bi;->k:Lcom/google/android/location/l/a/ax;

    if-nez v1, :cond_0

    .line 187
    :cond_c
    iget-object v1, p0, Lcom/google/android/location/l/a/bi;->l:Lcom/google/android/location/l/a/d;

    if-nez v1, :cond_19

    .line 188
    iget-object v1, p1, Lcom/google/android/location/l/a/bi;->l:Lcom/google/android/location/l/a/d;

    if-nez v1, :cond_0

    .line 196
    :cond_d
    invoke-virtual {p0, p1}, Lcom/google/android/location/l/a/bi;->unknownFieldDataEquals(Lcom/google/protobuf/nano/d;)Z

    move-result v0

    goto :goto_0

    .line 94
    :cond_e
    iget-object v1, p0, Lcom/google/android/location/l/a/bi;->a:Ljava/lang/String;

    iget-object v2, p1, Lcom/google/android/location/l/a/bi;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    goto :goto_0

    .line 102
    :cond_f
    iget-object v1, p0, Lcom/google/android/location/l/a/bi;->b:Lcom/google/android/location/l/a/az;

    iget-object v2, p1, Lcom/google/android/location/l/a/bi;->b:Lcom/google/android/location/l/a/az;

    invoke-virtual {v1, v2}, Lcom/google/android/location/l/a/az;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    goto/16 :goto_0

    .line 111
    :cond_10
    iget-object v1, p0, Lcom/google/android/location/l/a/bi;->c:Lcom/google/android/location/l/a/bn;

    iget-object v2, p1, Lcom/google/android/location/l/a/bi;->c:Lcom/google/android/location/l/a/bn;

    invoke-virtual {v1, v2}, Lcom/google/android/location/l/a/bn;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    goto/16 :goto_0

    .line 120
    :cond_11
    iget-object v1, p0, Lcom/google/android/location/l/a/bi;->d:Lcom/google/android/location/l/a/bo;

    iget-object v2, p1, Lcom/google/android/location/l/a/bi;->d:Lcom/google/android/location/l/a/bo;

    invoke-virtual {v1, v2}, Lcom/google/android/location/l/a/bo;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_5

    goto/16 :goto_0

    .line 129
    :cond_12
    iget-object v1, p0, Lcom/google/android/location/l/a/bi;->e:Lcom/google/android/location/l/a/bq;

    iget-object v2, p1, Lcom/google/android/location/l/a/bi;->e:Lcom/google/android/location/l/a/bq;

    invoke-virtual {v1, v2}, Lcom/google/android/location/l/a/bq;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_6

    goto/16 :goto_0

    .line 138
    :cond_13
    iget-object v1, p0, Lcom/google/android/location/l/a/bi;->f:Lcom/google/android/location/l/a/bb;

    iget-object v2, p1, Lcom/google/android/location/l/a/bi;->f:Lcom/google/android/location/l/a/bb;

    invoke-virtual {v1, v2}, Lcom/google/android/location/l/a/bb;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_7

    goto/16 :goto_0

    .line 147
    :cond_14
    iget-object v1, p0, Lcom/google/android/location/l/a/bi;->g:Lcom/google/android/location/l/a/bh;

    iget-object v2, p1, Lcom/google/android/location/l/a/bi;->g:Lcom/google/android/location/l/a/bh;

    invoke-virtual {v1, v2}, Lcom/google/android/location/l/a/bh;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_8

    goto/16 :goto_0

    .line 156
    :cond_15
    iget-object v1, p0, Lcom/google/android/location/l/a/bi;->h:Lcom/google/android/location/l/a/bl;

    iget-object v2, p1, Lcom/google/android/location/l/a/bi;->h:Lcom/google/android/location/l/a/bl;

    invoke-virtual {v1, v2}, Lcom/google/android/location/l/a/bl;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_9

    goto/16 :goto_0

    .line 165
    :cond_16
    iget-object v1, p0, Lcom/google/android/location/l/a/bi;->i:Lcom/google/android/location/l/a/at;

    iget-object v2, p1, Lcom/google/android/location/l/a/bi;->i:Lcom/google/android/location/l/a/at;

    invoke-virtual {v1, v2}, Lcom/google/android/location/l/a/at;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_a

    goto/16 :goto_0

    .line 174
    :cond_17
    iget-object v1, p0, Lcom/google/android/location/l/a/bi;->j:Lcom/google/android/location/l/a/bs;

    iget-object v2, p1, Lcom/google/android/location/l/a/bi;->j:Lcom/google/android/location/l/a/bs;

    invoke-virtual {v1, v2}, Lcom/google/android/location/l/a/bs;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_b

    goto/16 :goto_0

    .line 183
    :cond_18
    iget-object v1, p0, Lcom/google/android/location/l/a/bi;->k:Lcom/google/android/location/l/a/ax;

    iget-object v2, p1, Lcom/google/android/location/l/a/bi;->k:Lcom/google/android/location/l/a/ax;

    invoke-virtual {v1, v2}, Lcom/google/android/location/l/a/ax;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_c

    goto/16 :goto_0

    .line 192
    :cond_19
    iget-object v1, p0, Lcom/google/android/location/l/a/bi;->l:Lcom/google/android/location/l/a/d;

    iget-object v2, p1, Lcom/google/android/location/l/a/bi;->l:Lcom/google/android/location/l/a/d;

    invoke-virtual {v1, v2}, Lcom/google/android/location/l/a/d;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_d

    goto/16 :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 201
    iget-object v0, p0, Lcom/google/android/location/l/a/bi;->a:Ljava/lang/String;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    .line 204
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/location/l/a/bi;->b:Lcom/google/android/location/l/a/az;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    .line 206
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/location/l/a/bi;->c:Lcom/google/android/location/l/a/bn;

    if-nez v0, :cond_2

    move v0, v1

    :goto_2
    add-int/2addr v0, v2

    .line 208
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/location/l/a/bi;->d:Lcom/google/android/location/l/a/bo;

    if-nez v0, :cond_3

    move v0, v1

    :goto_3
    add-int/2addr v0, v2

    .line 210
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/location/l/a/bi;->e:Lcom/google/android/location/l/a/bq;

    if-nez v0, :cond_4

    move v0, v1

    :goto_4
    add-int/2addr v0, v2

    .line 212
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/location/l/a/bi;->f:Lcom/google/android/location/l/a/bb;

    if-nez v0, :cond_5

    move v0, v1

    :goto_5
    add-int/2addr v0, v2

    .line 214
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/location/l/a/bi;->g:Lcom/google/android/location/l/a/bh;

    if-nez v0, :cond_6

    move v0, v1

    :goto_6
    add-int/2addr v0, v2

    .line 216
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/location/l/a/bi;->h:Lcom/google/android/location/l/a/bl;

    if-nez v0, :cond_7

    move v0, v1

    :goto_7
    add-int/2addr v0, v2

    .line 218
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/location/l/a/bi;->i:Lcom/google/android/location/l/a/at;

    if-nez v0, :cond_8

    move v0, v1

    :goto_8
    add-int/2addr v0, v2

    .line 220
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/location/l/a/bi;->j:Lcom/google/android/location/l/a/bs;

    if-nez v0, :cond_9

    move v0, v1

    :goto_9
    add-int/2addr v0, v2

    .line 222
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/location/l/a/bi;->k:Lcom/google/android/location/l/a/ax;

    if-nez v0, :cond_a

    move v0, v1

    :goto_a
    add-int/2addr v0, v2

    .line 224
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/android/location/l/a/bi;->l:Lcom/google/android/location/l/a/d;

    if-nez v2, :cond_b

    :goto_b
    add-int/2addr v0, v1

    .line 226
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/google/android/location/l/a/bi;->unknownFieldDataHashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 227
    return v0

    .line 201
    :cond_0
    iget-object v0, p0, Lcom/google/android/location/l/a/bi;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_0

    .line 204
    :cond_1
    iget-object v0, p0, Lcom/google/android/location/l/a/bi;->b:Lcom/google/android/location/l/a/az;

    invoke-virtual {v0}, Lcom/google/android/location/l/a/az;->hashCode()I

    move-result v0

    goto :goto_1

    .line 206
    :cond_2
    iget-object v0, p0, Lcom/google/android/location/l/a/bi;->c:Lcom/google/android/location/l/a/bn;

    invoke-virtual {v0}, Lcom/google/android/location/l/a/bn;->hashCode()I

    move-result v0

    goto :goto_2

    .line 208
    :cond_3
    iget-object v0, p0, Lcom/google/android/location/l/a/bi;->d:Lcom/google/android/location/l/a/bo;

    invoke-virtual {v0}, Lcom/google/android/location/l/a/bo;->hashCode()I

    move-result v0

    goto :goto_3

    .line 210
    :cond_4
    iget-object v0, p0, Lcom/google/android/location/l/a/bi;->e:Lcom/google/android/location/l/a/bq;

    invoke-virtual {v0}, Lcom/google/android/location/l/a/bq;->hashCode()I

    move-result v0

    goto :goto_4

    .line 212
    :cond_5
    iget-object v0, p0, Lcom/google/android/location/l/a/bi;->f:Lcom/google/android/location/l/a/bb;

    invoke-virtual {v0}, Lcom/google/android/location/l/a/bb;->hashCode()I

    move-result v0

    goto :goto_5

    .line 214
    :cond_6
    iget-object v0, p0, Lcom/google/android/location/l/a/bi;->g:Lcom/google/android/location/l/a/bh;

    invoke-virtual {v0}, Lcom/google/android/location/l/a/bh;->hashCode()I

    move-result v0

    goto :goto_6

    .line 216
    :cond_7
    iget-object v0, p0, Lcom/google/android/location/l/a/bi;->h:Lcom/google/android/location/l/a/bl;

    invoke-virtual {v0}, Lcom/google/android/location/l/a/bl;->hashCode()I

    move-result v0

    goto :goto_7

    .line 218
    :cond_8
    iget-object v0, p0, Lcom/google/android/location/l/a/bi;->i:Lcom/google/android/location/l/a/at;

    invoke-virtual {v0}, Lcom/google/android/location/l/a/at;->hashCode()I

    move-result v0

    goto :goto_8

    .line 220
    :cond_9
    iget-object v0, p0, Lcom/google/android/location/l/a/bi;->j:Lcom/google/android/location/l/a/bs;

    invoke-virtual {v0}, Lcom/google/android/location/l/a/bs;->hashCode()I

    move-result v0

    goto :goto_9

    .line 222
    :cond_a
    iget-object v0, p0, Lcom/google/android/location/l/a/bi;->k:Lcom/google/android/location/l/a/ax;

    invoke-virtual {v0}, Lcom/google/android/location/l/a/ax;->hashCode()I

    move-result v0

    goto :goto_a

    .line 224
    :cond_b
    iget-object v1, p0, Lcom/google/android/location/l/a/bi;->l:Lcom/google/android/location/l/a/d;

    invoke-virtual {v1}, Lcom/google/android/location/l/a/d;->hashCode()I

    move-result v1

    goto :goto_b
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 5
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/android/location/l/a/bi;->storeUnknownField(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/l/a/bi;->a:Ljava/lang/String;

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Lcom/google/android/location/l/a/bi;->b:Lcom/google/android/location/l/a/az;

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/android/location/l/a/az;

    invoke-direct {v0}, Lcom/google/android/location/l/a/az;-><init>()V

    iput-object v0, p0, Lcom/google/android/location/l/a/bi;->b:Lcom/google/android/location/l/a/az;

    :cond_1
    iget-object v0, p0, Lcom/google/android/location/l/a/bi;->b:Lcom/google/android/location/l/a/az;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Lcom/google/android/location/l/a/bi;->c:Lcom/google/android/location/l/a/bn;

    if-nez v0, :cond_2

    new-instance v0, Lcom/google/android/location/l/a/bn;

    invoke-direct {v0}, Lcom/google/android/location/l/a/bn;-><init>()V

    iput-object v0, p0, Lcom/google/android/location/l/a/bi;->c:Lcom/google/android/location/l/a/bn;

    :cond_2
    iget-object v0, p0, Lcom/google/android/location/l/a/bi;->c:Lcom/google/android/location/l/a/bn;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_4
    iget-object v0, p0, Lcom/google/android/location/l/a/bi;->d:Lcom/google/android/location/l/a/bo;

    if-nez v0, :cond_3

    new-instance v0, Lcom/google/android/location/l/a/bo;

    invoke-direct {v0}, Lcom/google/android/location/l/a/bo;-><init>()V

    iput-object v0, p0, Lcom/google/android/location/l/a/bi;->d:Lcom/google/android/location/l/a/bo;

    :cond_3
    iget-object v0, p0, Lcom/google/android/location/l/a/bi;->d:Lcom/google/android/location/l/a/bo;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_5
    iget-object v0, p0, Lcom/google/android/location/l/a/bi;->e:Lcom/google/android/location/l/a/bq;

    if-nez v0, :cond_4

    new-instance v0, Lcom/google/android/location/l/a/bq;

    invoke-direct {v0}, Lcom/google/android/location/l/a/bq;-><init>()V

    iput-object v0, p0, Lcom/google/android/location/l/a/bi;->e:Lcom/google/android/location/l/a/bq;

    :cond_4
    iget-object v0, p0, Lcom/google/android/location/l/a/bi;->e:Lcom/google/android/location/l/a/bq;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_6
    iget-object v0, p0, Lcom/google/android/location/l/a/bi;->f:Lcom/google/android/location/l/a/bb;

    if-nez v0, :cond_5

    new-instance v0, Lcom/google/android/location/l/a/bb;

    invoke-direct {v0}, Lcom/google/android/location/l/a/bb;-><init>()V

    iput-object v0, p0, Lcom/google/android/location/l/a/bi;->f:Lcom/google/android/location/l/a/bb;

    :cond_5
    iget-object v0, p0, Lcom/google/android/location/l/a/bi;->f:Lcom/google/android/location/l/a/bb;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_7
    iget-object v0, p0, Lcom/google/android/location/l/a/bi;->g:Lcom/google/android/location/l/a/bh;

    if-nez v0, :cond_6

    new-instance v0, Lcom/google/android/location/l/a/bh;

    invoke-direct {v0}, Lcom/google/android/location/l/a/bh;-><init>()V

    iput-object v0, p0, Lcom/google/android/location/l/a/bi;->g:Lcom/google/android/location/l/a/bh;

    :cond_6
    iget-object v0, p0, Lcom/google/android/location/l/a/bi;->g:Lcom/google/android/location/l/a/bh;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_8
    iget-object v0, p0, Lcom/google/android/location/l/a/bi;->h:Lcom/google/android/location/l/a/bl;

    if-nez v0, :cond_7

    new-instance v0, Lcom/google/android/location/l/a/bl;

    invoke-direct {v0}, Lcom/google/android/location/l/a/bl;-><init>()V

    iput-object v0, p0, Lcom/google/android/location/l/a/bi;->h:Lcom/google/android/location/l/a/bl;

    :cond_7
    iget-object v0, p0, Lcom/google/android/location/l/a/bi;->h:Lcom/google/android/location/l/a/bl;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto/16 :goto_0

    :sswitch_9
    iget-object v0, p0, Lcom/google/android/location/l/a/bi;->l:Lcom/google/android/location/l/a/d;

    if-nez v0, :cond_8

    new-instance v0, Lcom/google/android/location/l/a/d;

    invoke-direct {v0}, Lcom/google/android/location/l/a/d;-><init>()V

    iput-object v0, p0, Lcom/google/android/location/l/a/bi;->l:Lcom/google/android/location/l/a/d;

    :cond_8
    iget-object v0, p0, Lcom/google/android/location/l/a/bi;->l:Lcom/google/android/location/l/a/d;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto/16 :goto_0

    :sswitch_a
    iget-object v0, p0, Lcom/google/android/location/l/a/bi;->i:Lcom/google/android/location/l/a/at;

    if-nez v0, :cond_9

    new-instance v0, Lcom/google/android/location/l/a/at;

    invoke-direct {v0}, Lcom/google/android/location/l/a/at;-><init>()V

    iput-object v0, p0, Lcom/google/android/location/l/a/bi;->i:Lcom/google/android/location/l/a/at;

    :cond_9
    iget-object v0, p0, Lcom/google/android/location/l/a/bi;->i:Lcom/google/android/location/l/a/at;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto/16 :goto_0

    :sswitch_b
    iget-object v0, p0, Lcom/google/android/location/l/a/bi;->j:Lcom/google/android/location/l/a/bs;

    if-nez v0, :cond_a

    new-instance v0, Lcom/google/android/location/l/a/bs;

    invoke-direct {v0}, Lcom/google/android/location/l/a/bs;-><init>()V

    iput-object v0, p0, Lcom/google/android/location/l/a/bi;->j:Lcom/google/android/location/l/a/bs;

    :cond_a
    iget-object v0, p0, Lcom/google/android/location/l/a/bi;->j:Lcom/google/android/location/l/a/bs;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto/16 :goto_0

    :sswitch_c
    iget-object v0, p0, Lcom/google/android/location/l/a/bi;->k:Lcom/google/android/location/l/a/ax;

    if-nez v0, :cond_b

    new-instance v0, Lcom/google/android/location/l/a/ax;

    invoke-direct {v0}, Lcom/google/android/location/l/a/ax;-><init>()V

    iput-object v0, p0, Lcom/google/android/location/l/a/bi;->k:Lcom/google/android/location/l/a/ax;

    :cond_b
    iget-object v0, p0, Lcom/google/android/location/l/a/bi;->k:Lcom/google/android/location/l/a/ax;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x42 -> :sswitch_8
        0x4a -> :sswitch_9
        0x52 -> :sswitch_a
        0x5a -> :sswitch_b
        0x62 -> :sswitch_c
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 233
    iget-object v0, p0, Lcom/google/android/location/l/a/bi;->a:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 234
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/location/l/a/bi;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 236
    :cond_0
    iget-object v0, p0, Lcom/google/android/location/l/a/bi;->b:Lcom/google/android/location/l/a/az;

    if-eqz v0, :cond_1

    .line 237
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/android/location/l/a/bi;->b:Lcom/google/android/location/l/a/az;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 239
    :cond_1
    iget-object v0, p0, Lcom/google/android/location/l/a/bi;->c:Lcom/google/android/location/l/a/bn;

    if-eqz v0, :cond_2

    .line 240
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/android/location/l/a/bi;->c:Lcom/google/android/location/l/a/bn;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 242
    :cond_2
    iget-object v0, p0, Lcom/google/android/location/l/a/bi;->d:Lcom/google/android/location/l/a/bo;

    if-eqz v0, :cond_3

    .line 243
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/android/location/l/a/bi;->d:Lcom/google/android/location/l/a/bo;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 245
    :cond_3
    iget-object v0, p0, Lcom/google/android/location/l/a/bi;->e:Lcom/google/android/location/l/a/bq;

    if-eqz v0, :cond_4

    .line 246
    const/4 v0, 0x5

    iget-object v1, p0, Lcom/google/android/location/l/a/bi;->e:Lcom/google/android/location/l/a/bq;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 248
    :cond_4
    iget-object v0, p0, Lcom/google/android/location/l/a/bi;->f:Lcom/google/android/location/l/a/bb;

    if-eqz v0, :cond_5

    .line 249
    const/4 v0, 0x6

    iget-object v1, p0, Lcom/google/android/location/l/a/bi;->f:Lcom/google/android/location/l/a/bb;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 251
    :cond_5
    iget-object v0, p0, Lcom/google/android/location/l/a/bi;->g:Lcom/google/android/location/l/a/bh;

    if-eqz v0, :cond_6

    .line 252
    const/4 v0, 0x7

    iget-object v1, p0, Lcom/google/android/location/l/a/bi;->g:Lcom/google/android/location/l/a/bh;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 254
    :cond_6
    iget-object v0, p0, Lcom/google/android/location/l/a/bi;->h:Lcom/google/android/location/l/a/bl;

    if-eqz v0, :cond_7

    .line 255
    const/16 v0, 0x8

    iget-object v1, p0, Lcom/google/android/location/l/a/bi;->h:Lcom/google/android/location/l/a/bl;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 257
    :cond_7
    iget-object v0, p0, Lcom/google/android/location/l/a/bi;->l:Lcom/google/android/location/l/a/d;

    if-eqz v0, :cond_8

    .line 258
    const/16 v0, 0x9

    iget-object v1, p0, Lcom/google/android/location/l/a/bi;->l:Lcom/google/android/location/l/a/d;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 260
    :cond_8
    iget-object v0, p0, Lcom/google/android/location/l/a/bi;->i:Lcom/google/android/location/l/a/at;

    if-eqz v0, :cond_9

    .line 261
    const/16 v0, 0xa

    iget-object v1, p0, Lcom/google/android/location/l/a/bi;->i:Lcom/google/android/location/l/a/at;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 263
    :cond_9
    iget-object v0, p0, Lcom/google/android/location/l/a/bi;->j:Lcom/google/android/location/l/a/bs;

    if-eqz v0, :cond_a

    .line 264
    const/16 v0, 0xb

    iget-object v1, p0, Lcom/google/android/location/l/a/bi;->j:Lcom/google/android/location/l/a/bs;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 266
    :cond_a
    iget-object v0, p0, Lcom/google/android/location/l/a/bi;->k:Lcom/google/android/location/l/a/ax;

    if-eqz v0, :cond_b

    .line 267
    const/16 v0, 0xc

    iget-object v1, p0, Lcom/google/android/location/l/a/bi;->k:Lcom/google/android/location/l/a/ax;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 269
    :cond_b
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/d;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 270
    return-void
.end method
