.class public final Lcom/google/android/location/copresence/l/q;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method public static a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/ac/b/c/av;
    .locals 1

    .prologue
    .line 111
    if-nez p0, :cond_0

    if-nez p1, :cond_0

    .line 112
    const/4 v0, 0x0

    .line 117
    :goto_0
    return-object v0

    .line 114
    :cond_0
    new-instance v0, Lcom/google/ac/b/c/av;

    invoke-direct {v0}, Lcom/google/ac/b/c/av;-><init>()V

    .line 115
    iput-object p0, v0, Lcom/google/ac/b/c/av;->a:Ljava/lang/String;

    .line 116
    iput-object p1, v0, Lcom/google/ac/b/c/av;->b:Ljava/lang/String;

    goto :goto_0
.end method

.method public static a(Lcom/google/android/gms/location/copresence/internal/StrategyImpl;Lcom/google/android/gms/location/copresence/MessageFilter$FilterPrimitive;)Lcom/google/ac/b/c/bo;
    .locals 10

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 233
    if-nez p1, :cond_0

    .line 247
    :goto_0
    return-object v1

    .line 236
    :cond_0
    new-instance v4, Lcom/google/ac/b/c/bo;

    invoke-direct {v4}, Lcom/google/ac/b/c/bo;-><init>()V

    .line 237
    iget-object v0, p1, Lcom/google/android/gms/location/copresence/MessageFilter$FilterPrimitive;->f:Ljava/lang/String;

    iput-object v0, v4, Lcom/google/ac/b/c/bo;->a:Ljava/lang/String;

    .line 238
    iget-object v3, p1, Lcom/google/android/gms/location/copresence/MessageFilter$FilterPrimitive;->c:Lcom/google/android/gms/location/copresence/AccessKey;

    if-nez v3, :cond_2

    move-object v0, v1

    :goto_1
    iput-object v0, v4, Lcom/google/ac/b/c/bo;->e:Lcom/google/ac/b/c/r;

    .line 239
    iget-object v0, p1, Lcom/google/android/gms/location/copresence/MessageFilter$FilterPrimitive;->a:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/android/gms/location/copresence/MessageFilter$FilterPrimitive;->b:Ljava/lang/String;

    invoke-static {v0, v3}, Lcom/google/android/location/copresence/l/q;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/ac/b/c/av;

    move-result-object v0

    iput-object v0, v4, Lcom/google/ac/b/c/bo;->d:Lcom/google/ac/b/c/av;

    .line 240
    iget-wide v6, p1, Lcom/google/android/gms/location/copresence/MessageFilter$FilterPrimitive;->d:J

    const-wide/16 v8, -0x1

    cmp-long v0, v6, v8

    if-eqz v0, :cond_1

    .line 241
    iget-wide v6, p1, Lcom/google/android/gms/location/copresence/MessageFilter$FilterPrimitive;->d:J

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, v4, Lcom/google/ac/b/c/bo;->c:Ljava/lang/Long;

    .line 243
    :cond_1
    invoke-static {p0}, Lcom/google/android/location/copresence/l/q;->c(Lcom/google/android/gms/location/copresence/internal/StrategyImpl;)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, v4, Lcom/google/ac/b/c/bo;->b:Ljava/lang/Integer;

    .line 244
    iget-object v5, p1, Lcom/google/android/gms/location/copresence/MessageFilter$FilterPrimitive;->e:Lcom/google/android/gms/location/copresence/People;

    if-nez v5, :cond_3

    :goto_2
    iput-object v1, v4, Lcom/google/ac/b/c/bo;->f:Lcom/google/ac/b/c/bc;

    .line 245
    invoke-static {p0}, Lcom/google/android/location/copresence/l/q;->a(Lcom/google/android/gms/location/copresence/internal/StrategyImpl;)Lcom/google/ac/b/c/bs;

    move-result-object v0

    iput-object v0, v4, Lcom/google/ac/b/c/bo;->g:Lcom/google/ac/b/c/bs;

    .line 246
    invoke-static {p0}, Lcom/google/android/location/copresence/l/q;->b(Lcom/google/android/gms/location/copresence/internal/StrategyImpl;)Lcom/google/ac/b/c/bb;

    move-result-object v0

    iput-object v0, v4, Lcom/google/ac/b/c/bo;->h:Lcom/google/ac/b/c/bb;

    move-object v1, v4

    .line 247
    goto :goto_0

    .line 238
    :cond_2
    new-instance v0, Lcom/google/ac/b/c/r;

    invoke-direct {v0}, Lcom/google/ac/b/c/r;-><init>()V

    invoke-virtual {v3}, Lcom/google/android/gms/location/copresence/AccessKey;->b()Ljava/lang/String;

    move-result-object v3

    iput-object v3, v0, Lcom/google/ac/b/c/r;->a:Ljava/lang/String;

    goto :goto_1

    .line 244
    :cond_3
    new-instance v3, Lcom/google/ac/b/c/bc;

    invoke-direct {v3}, Lcom/google/ac/b/c/bc;-><init>()V

    invoke-virtual {v5}, Lcom/google/android/gms/location/copresence/People;->b()Ljava/util/List;

    move-result-object v0

    if-nez v0, :cond_4

    move v1, v2

    :goto_3
    new-array v6, v1, [Lcom/google/ac/b/c/cl;

    :goto_4
    if-ge v2, v1, :cond_5

    invoke-virtual {v5}, Lcom/google/android/gms/location/copresence/People;->b()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    new-instance v7, Lcom/google/ac/b/c/cl;

    invoke-direct {v7}, Lcom/google/ac/b/c/cl;-><init>()V

    const/4 v8, 0x1

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    iput-object v8, v7, Lcom/google/ac/b/c/cl;->a:Ljava/lang/Integer;

    iput-object v0, v7, Lcom/google/ac/b/c/cl;->b:Ljava/lang/String;

    aput-object v7, v6, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_4

    :cond_4
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    move v1, v0

    goto :goto_3

    :cond_5
    iput-object v6, v3, Lcom/google/ac/b/c/bc;->a:[Lcom/google/ac/b/c/cl;

    invoke-virtual {v5}, Lcom/google/android/gms/location/copresence/People;->c()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, v3, Lcom/google/ac/b/c/bc;->b:Ljava/lang/Integer;

    move-object v1, v3

    goto :goto_2
.end method

.method public static a(Lcom/google/android/gms/location/copresence/internal/StrategyImpl;)Lcom/google/ac/b/c/bs;
    .locals 5

    .prologue
    const/4 v0, 0x0

    const/4 v4, 0x2

    const/4 v3, 0x1

    .line 55
    if-nez p0, :cond_0

    .line 96
    :goto_0
    return-object v0

    .line 58
    :cond_0
    new-instance v1, Lcom/google/ac/b/c/bs;

    invoke-direct {v1}, Lcom/google/ac/b/c/bs;-><init>()V

    .line 60
    invoke-virtual {p0}, Lcom/google/android/gms/location/copresence/internal/StrategyImpl;->e()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 61
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iput-object v2, v1, Lcom/google/ac/b/c/bs;->a:Ljava/lang/Integer;

    .line 63
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/gms/location/copresence/internal/StrategyImpl;->d()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    .line 77
    const/4 v2, 0x6

    invoke-static {v2}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 78
    const-string v2, "Failed to convert illegal BroadcastScanConfiguration."

    invoke-static {v2}, Lcom/google/android/location/copresence/ag;->e(Ljava/lang/String;)I

    .line 85
    :cond_2
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iput-object v2, v1, Lcom/google/ac/b/c/bs;->b:Ljava/lang/Integer;

    .line 89
    :goto_1
    invoke-virtual {p0}, Lcom/google/android/gms/location/copresence/internal/StrategyImpl;->b()Z

    move-result v2

    if-nez v2, :cond_3

    .line 90
    iput-object v0, v1, Lcom/google/ac/b/c/bs;->b:Ljava/lang/Integer;

    .line 92
    :cond_3
    invoke-virtual {p0}, Lcom/google/android/gms/location/copresence/internal/StrategyImpl;->c()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 93
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, v1, Lcom/google/ac/b/c/bs;->c:Ljava/lang/Integer;

    :cond_4
    move-object v0, v1

    .line 96
    goto :goto_0

    .line 65
    :pswitch_0
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iput-object v2, v1, Lcom/google/ac/b/c/bs;->b:Ljava/lang/Integer;

    goto :goto_1

    .line 69
    :pswitch_1
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iput-object v2, v1, Lcom/google/ac/b/c/bs;->b:Ljava/lang/Integer;

    goto :goto_1

    .line 73
    :pswitch_2
    const/4 v2, 0x3

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iput-object v2, v1, Lcom/google/ac/b/c/bs;->b:Ljava/lang/Integer;

    goto :goto_1

    .line 63
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static a(Lcom/google/android/gms/common/people/data/Audience;Lcom/google/android/gms/location/copresence/internal/StrategyImpl;)Lcom/google/ac/b/c/u;
    .locals 3

    .prologue
    .line 184
    new-instance v1, Lcom/google/ac/b/c/u;

    invoke-direct {v1}, Lcom/google/ac/b/c/u;-><init>()V

    .line 186
    invoke-virtual {p1}, Lcom/google/android/gms/location/copresence/internal/StrategyImpl;->f()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 187
    const/4 v0, 0x7

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, v1, Lcom/google/ac/b/c/u;->a:Ljava/lang/Integer;

    .line 192
    :goto_0
    :try_start_0
    invoke-static {p0}, Lcom/google/android/location/copresence/l/a;->a(Lcom/google/android/gms/common/people/data/Audience;)Lcom/google/ac/b/c/dm;

    move-result-object v0

    iput-object v0, v1, Lcom/google/ac/b/c/u;->c:Lcom/google/ac/b/c/dm;
    :try_end_0
    .catch Lcom/google/protobuf/nano/i; {:try_start_0 .. :try_end_0} :catch_0

    .line 200
    :cond_0
    :goto_1
    return-object v1

    .line 189
    :cond_1
    const/4 v0, 0x5

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, v1, Lcom/google/ac/b/c/u;->a:Ljava/lang/Integer;

    goto :goto_0

    .line 193
    :catch_0
    move-exception v0

    .line 194
    const/4 v2, 0x6

    invoke-static {v2}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 195
    const-string v2, "Failed to convert audience to renderedsharingrosters"

    invoke-static {v2, v0}, Lcom/google/android/location/copresence/ag;->a(Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1
.end method

.method public static b(Lcom/google/android/gms/location/copresence/internal/StrategyImpl;)Lcom/google/ac/b/c/bb;
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 260
    new-instance v0, Lcom/google/ac/b/c/bb;

    invoke-direct {v0}, Lcom/google/ac/b/c/bb;-><init>()V

    .line 261
    invoke-virtual {p0}, Lcom/google/android/gms/location/copresence/internal/StrategyImpl;->f()Z

    move-result v1

    if-nez v1, :cond_0

    .line 262
    new-array v1, v3, [I

    const/4 v2, 0x0

    aput v3, v1, v2

    iput-object v1, v0, Lcom/google/ac/b/c/bb;->a:[I

    .line 267
    :goto_0
    return-object v0

    .line 264
    :cond_0
    const/4 v1, 0x2

    new-array v1, v1, [I

    fill-array-data v1, :array_0

    iput-object v1, v0, Lcom/google/ac/b/c/bb;->a:[I

    goto :goto_0

    nop

    :array_0
    .array-data 4
        0x1
        0x2
    .end array-data
.end method

.method public static c(Lcom/google/android/gms/location/copresence/internal/StrategyImpl;)Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 282
    if-nez p0, :cond_0

    .line 283
    const/4 v0, 0x0

    .line 294
    :goto_0
    return-object v0

    .line 285
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/location/copresence/internal/StrategyImpl;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/google/android/gms/location/copresence/internal/StrategyImpl;->c()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 286
    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0

    .line 288
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/gms/location/copresence/internal/StrategyImpl;->b()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 289
    const/4 v0, 0x2

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0

    .line 291
    :cond_2
    invoke-virtual {p0}, Lcom/google/android/gms/location/copresence/internal/StrategyImpl;->b()Z

    move-result v0

    if-nez v0, :cond_3

    invoke-virtual {p0}, Lcom/google/android/gms/location/copresence/internal/StrategyImpl;->c()Z

    move-result v0

    if-nez v0, :cond_3

    .line 292
    const/4 v0, 0x3

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0

    .line 294
    :cond_3
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0
.end method
