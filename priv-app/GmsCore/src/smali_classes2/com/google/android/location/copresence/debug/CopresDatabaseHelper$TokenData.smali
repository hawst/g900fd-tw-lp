.class public Lcom/google/android/location/copresence/debug/CopresDatabaseHelper$TokenData;
.super Lcom/google/android/location/copresence/debug/a;
.source "SourceFile"


# instance fields
.field public broadcastAudio:Ljava/lang/Boolean;
    .annotation runtime Lcom/google/android/location/copresence/debug/b;
        a = true
        c = "0"
    .end annotation
.end field

.field public broadcastBle:Ljava/lang/Boolean;
    .annotation runtime Lcom/google/android/location/copresence/debug/b;
        a = true
        c = "0"
    .end annotation
.end field

.field public broadcastBt:Ljava/lang/Boolean;
    .annotation runtime Lcom/google/android/location/copresence/debug/b;
        a = true
        c = "0"
    .end annotation
.end field

.field public broadcastWifi:Ljava/lang/Boolean;
    .annotation runtime Lcom/google/android/location/copresence/debug/b;
        a = true
        c = "0"
    .end annotation
.end field

.field public fromAudio:Ljava/lang/Boolean;
    .annotation runtime Lcom/google/android/location/copresence/debug/b;
        a = true
        c = "0"
    .end annotation
.end field

.field public fromBle:Ljava/lang/Boolean;
    .annotation runtime Lcom/google/android/location/copresence/debug/b;
        a = true
        c = "0"
    .end annotation
.end field

.field public fromBt:Ljava/lang/Boolean;
    .annotation runtime Lcom/google/android/location/copresence/debug/b;
        a = true
        c = "0"
    .end annotation
.end field

.field public fromWifi:Ljava/lang/Boolean;
    .annotation runtime Lcom/google/android/location/copresence/debug/b;
        a = true
        c = "0"
    .end annotation
.end field

.field public isActiveToken:Ljava/lang/Boolean;
    .annotation runtime Lcom/google/android/location/copresence/debug/b;
        a = true
        c = "0"
    .end annotation
.end field

.field public isExpired:Ljava/lang/Boolean;
    .annotation runtime Lcom/google/android/location/copresence/debug/b;
        a = true
        c = "0"
    .end annotation
.end field

.field public networkState:Ljava/lang/Integer;
    .annotation runtime Lcom/google/android/location/copresence/debug/b;
        a = true
        c = "0"
    .end annotation
.end field

.field public timestamp:Ljava/lang/Long;
    .annotation runtime Lcom/google/android/location/copresence/debug/b;
        a = true
        d = 0x2
    .end annotation
.end field

.field public tokenId:Ljava/lang/String;
    .annotation runtime Lcom/google/android/location/copresence/debug/b;
        a = true
        b = true
    .end annotation
.end field

.field public wasInitiated:Ljava/lang/Boolean;
    .annotation runtime Lcom/google/android/location/copresence/debug/b;
        a = true
        c = "0"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 147
    invoke-direct {p0}, Lcom/google/android/location/copresence/debug/a;-><init>()V

    return-void
.end method
