.class final Lcom/google/android/location/copresence/c/i;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/audiomodem/bm;


# instance fields
.field final synthetic a:Lcom/google/android/location/copresence/c/h;


# direct methods
.method constructor <init>(Lcom/google/android/location/copresence/c/h;)V
    .locals 0

    .prologue
    .line 78
    iput-object p1, p0, Lcom/google/android/location/copresence/c/i;->a:Lcom/google/android/location/copresence/c/h;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(I)V
    .locals 2

    .prologue
    .line 95
    const/4 v0, 0x3

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 96
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Audio Stack: AudioStateMachine: Receiving status changed: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->b(Ljava/lang/String;)I

    .line 98
    :cond_0
    return-void
.end method

.method public final a(Ljava/util/List;)V
    .locals 2

    .prologue
    .line 81
    iget-object v0, p0, Lcom/google/android/location/copresence/c/i;->a:Lcom/google/android/location/copresence/c/h;

    iget-object v0, v0, Lcom/google/android/location/copresence/c/h;->e:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/location/copresence/c/j;

    invoke-direct {v1, p0, p1}, Lcom/google/android/location/copresence/c/j;-><init>(Lcom/google/android/location/copresence/c/i;Ljava/util/List;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 91
    return-void
.end method
