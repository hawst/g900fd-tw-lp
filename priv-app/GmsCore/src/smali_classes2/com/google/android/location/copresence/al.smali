.class final Lcom/google/android/location/copresence/al;
.super Ljava/lang/Thread;
.source "SourceFile"


# instance fields
.field final synthetic a:Ljava/lang/Runnable;

.field final synthetic b:Lcom/google/android/location/copresence/ah;


# direct methods
.method constructor <init>(Lcom/google/android/location/copresence/ah;Ljava/lang/Runnable;)V
    .locals 0

    .prologue
    .line 275
    iput-object p1, p0, Lcom/google/android/location/copresence/al;->b:Lcom/google/android/location/copresence/ah;

    iput-object p2, p0, Lcom/google/android/location/copresence/al;->a:Ljava/lang/Runnable;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 5

    .prologue
    const/4 v4, 0x3

    .line 279
    :try_start_0
    iget-object v0, p0, Lcom/google/android/location/copresence/al;->b:Lcom/google/android/location/copresence/ah;

    iget-object v0, v0, Lcom/google/android/location/copresence/ah;->a:Lcom/google/android/gms/gcm/ag;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "939806748588"

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Lcom/google/android/gms/gcm/ag;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 280
    const/4 v1, 0x3

    invoke-static {v1}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 281
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "DeviceRegistrationHelper: Device registered, registration ID="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/location/copresence/ag;->b(Ljava/lang/String;)I

    .line 284
    :cond_0
    iget-object v1, p0, Lcom/google/android/location/copresence/al;->b:Lcom/google/android/location/copresence/ah;

    iget-object v1, v1, Lcom/google/android/location/copresence/ah;->c:Lcom/google/android/location/copresence/ap;

    new-instance v2, Lcom/google/android/location/copresence/am;

    invoke-direct {v2, p0, v0}, Lcom/google/android/location/copresence/am;-><init>(Lcom/google/android/location/copresence/al;Ljava/lang/String;)V

    const/4 v0, 0x0

    invoke-virtual {v1, v2, v0}, Lcom/google/android/location/copresence/ap;->a(Ljava/lang/Runnable;Landroid/os/WorkSource;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 301
    :cond_1
    :goto_0
    return-void

    .line 295
    :catch_0
    move-exception v0

    .line 297
    invoke-static {v4}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 298
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "DeviceRegistrationHelper: Error :"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->b(Ljava/lang/String;)I

    goto :goto_0
.end method
