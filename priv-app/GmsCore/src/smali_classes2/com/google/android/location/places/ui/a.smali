.class public final Lcom/google/android/location/places/ui/a;
.super Landroid/support/v4/app/Fragment;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/location/places/ui/ab;
.implements Lcom/google/android/location/places/ui/as;
.implements Lcom/google/android/location/places/ui/ay;
.implements Lcom/google/android/location/places/ui/w;


# instance fields
.field private a:Lcom/google/android/location/places/ui/j;

.field private b:Lcom/google/android/location/places/ui/av;

.field private c:Lcom/google/android/location/places/ui/s;

.field private d:Lcom/google/android/location/places/ui/MarkerMapFragment;

.field private e:Lcom/google/android/location/places/ui/t;

.field private f:Lcom/google/android/location/places/ui/an;

.field private g:Landroid/app/ProgressDialog;

.field private h:Landroid/app/AlertDialog;

.field private i:Z

.field private j:Z

.field private k:Z

.field private l:Landroid/view/ViewGroup;

.field private m:Z

.field private n:Z

.field private o:Lcom/google/android/gms/maps/model/LatLng;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    .line 38
    return-void
.end method

.method static synthetic a(Lcom/google/android/location/places/ui/a;Landroid/app/AlertDialog;)Landroid/app/AlertDialog;
    .locals 0

    .prologue
    .line 31
    iput-object p1, p0, Lcom/google/android/location/places/ui/a;->h:Landroid/app/AlertDialog;

    return-object p1
.end method

.method public static a()Lcom/google/android/location/places/ui/a;
    .locals 1

    .prologue
    .line 69
    new-instance v0, Lcom/google/android/location/places/ui/a;

    invoke-direct {v0}, Lcom/google/android/location/places/ui/a;-><init>()V

    .line 70
    return-object v0
.end method

.method private a(Z)V
    .locals 2

    .prologue
    .line 463
    if-eqz p1, :cond_0

    .line 464
    iget-object v0, p0, Lcom/google/android/location/places/ui/a;->d:Lcom/google/android/location/places/ui/MarkerMapFragment;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/location/places/ui/MarkerMapFragment;->b(Z)V

    .line 465
    iget-object v0, p0, Lcom/google/android/location/places/ui/a;->d:Lcom/google/android/location/places/ui/MarkerMapFragment;

    invoke-virtual {v0}, Lcom/google/android/location/places/ui/MarkerMapFragment;->i()V

    .line 471
    :goto_0
    iput-boolean p1, p0, Lcom/google/android/location/places/ui/a;->k:Z

    .line 472
    return-void

    .line 467
    :cond_0
    iget-object v0, p0, Lcom/google/android/location/places/ui/a;->d:Lcom/google/android/location/places/ui/MarkerMapFragment;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/location/places/ui/MarkerMapFragment;->b(Z)V

    .line 468
    iget-object v0, p0, Lcom/google/android/location/places/ui/a;->d:Lcom/google/android/location/places/ui/MarkerMapFragment;

    invoke-virtual {v0}, Lcom/google/android/location/places/ui/MarkerMapFragment;->h()V

    goto :goto_0
.end method

.method static synthetic a(Lcom/google/android/location/places/ui/a;)Z
    .locals 1

    .prologue
    .line 31
    iget-boolean v0, p0, Lcom/google/android/location/places/ui/a;->i:Z

    return v0
.end method

.method static synthetic b(Lcom/google/android/location/places/ui/a;)Lcom/google/android/location/places/ui/t;
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, Lcom/google/android/location/places/ui/a;->e:Lcom/google/android/location/places/ui/t;

    return-object v0
.end method

.method static synthetic c(Lcom/google/android/location/places/ui/a;)Z
    .locals 1

    .prologue
    .line 31
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/location/places/ui/a;->i:Z

    return v0
.end method

.method static synthetic d(Lcom/google/android/location/places/ui/a;)Landroid/app/ProgressDialog;
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, Lcom/google/android/location/places/ui/a;->g:Landroid/app/ProgressDialog;

    return-object v0
.end method

.method static synthetic e(Lcom/google/android/location/places/ui/a;)Z
    .locals 1

    .prologue
    .line 31
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/location/places/ui/a;->n:Z

    return v0
.end method

.method static synthetic f(Lcom/google/android/location/places/ui/a;)Lcom/google/android/location/places/ui/j;
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, Lcom/google/android/location/places/ui/a;->a:Lcom/google/android/location/places/ui/j;

    return-object v0
.end method

.method static synthetic g(Lcom/google/android/location/places/ui/a;)Landroid/app/AlertDialog;
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, Lcom/google/android/location/places/ui/a;->h:Landroid/app/AlertDialog;

    return-object v0
.end method

.method static synthetic h(Lcom/google/android/location/places/ui/a;)Lcom/google/android/location/places/ui/av;
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, Lcom/google/android/location/places/ui/a;->b:Lcom/google/android/location/places/ui/av;

    return-object v0
.end method

.method static synthetic i(Lcom/google/android/location/places/ui/a;)V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0}, Lcom/google/android/location/places/ui/a;->n()V

    return-void
.end method

.method private n()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 475
    iget-object v0, p0, Lcom/google/android/location/places/ui/a;->g:Landroid/app/ProgressDialog;

    if-nez v0, :cond_0

    .line 476
    new-instance v0, Landroid/app/ProgressDialog;

    invoke-virtual {p0}, Lcom/google/android/location/places/ui/a;->getActivity()Landroid/support/v4/app/q;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/location/places/ui/a;->g:Landroid/app/ProgressDialog;

    .line 477
    iget-object v0, p0, Lcom/google/android/location/places/ui/a;->g:Landroid/app/ProgressDialog;

    sget v1, Lcom/google/android/gms/p;->ss:I

    invoke-virtual {p0, v1}, Lcom/google/android/location/places/ui/a;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 478
    iget-object v0, p0, Lcom/google/android/location/places/ui/a;->g:Landroid/app/ProgressDialog;

    invoke-virtual {v0, v2}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    .line 479
    iget-object v0, p0, Lcom/google/android/location/places/ui/a;->g:Landroid/app/ProgressDialog;

    new-instance v1, Lcom/google/android/location/places/ui/i;

    invoke-direct {v1, p0}, Lcom/google/android/location/places/ui/i;-><init>(Lcom/google/android/location/places/ui/a;)V

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)V

    .line 487
    iget-object v0, p0, Lcom/google/android/location/places/ui/a;->g:Landroid/app/ProgressDialog;

    invoke-virtual {v0, v2}, Landroid/app/ProgressDialog;->setIndeterminate(Z)V

    .line 490
    :cond_0
    iget-object v0, p0, Lcom/google/android/location/places/ui/a;->g:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->show()V

    .line 491
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/location/places/UserAddedPlace;)V
    .locals 2

    .prologue
    .line 371
    invoke-virtual {p0}, Lcom/google/android/location/places/ui/a;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    new-instance v1, Lcom/google/android/location/places/ui/e;

    invoke-direct {v1, p0, p1}, Lcom/google/android/location/places/ui/e;-><init>(Lcom/google/android/location/places/ui/a;Lcom/google/android/gms/location/places/UserAddedPlace;)V

    invoke-virtual {v0, v1}, Landroid/support/v4/app/q;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 418
    return-void
.end method

.method public final a(Lcom/google/android/gms/location/places/f;)V
    .locals 0

    .prologue
    .line 271
    return-void
.end method

.method public final a(Lcom/google/android/gms/maps/model/LatLng;)V
    .locals 1

    .prologue
    .line 280
    iget-boolean v0, p0, Lcom/google/android/location/places/ui/a;->k:Z

    if-nez v0, :cond_0

    .line 281
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/location/places/ui/a;->a(Z)V

    .line 284
    :cond_0
    iget-boolean v0, p0, Lcom/google/android/location/places/ui/a;->n:Z

    if-nez v0, :cond_1

    .line 285
    iget-boolean v0, p0, Lcom/google/android/location/places/ui/a;->m:Z

    if-eqz v0, :cond_2

    .line 286
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/location/places/ui/a;->m:Z

    .line 287
    iget-object v0, p0, Lcom/google/android/location/places/ui/a;->d:Lcom/google/android/location/places/ui/MarkerMapFragment;

    invoke-virtual {v0, p1}, Lcom/google/android/location/places/ui/MarkerMapFragment;->a(Lcom/google/android/gms/maps/model/LatLng;)V

    .line 292
    :cond_1
    :goto_0
    return-void

    .line 289
    :cond_2
    iget-object v0, p0, Lcom/google/android/location/places/ui/a;->d:Lcom/google/android/location/places/ui/MarkerMapFragment;

    invoke-virtual {v0, p1}, Lcom/google/android/location/places/ui/MarkerMapFragment;->b(Lcom/google/android/gms/maps/model/LatLng;)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/location/places/PlaceType;)V
    .locals 7

    .prologue
    .line 458
    iget-object v0, p0, Lcom/google/android/location/places/ui/a;->b:Lcom/google/android/location/places/ui/av;

    iget-object v6, p0, Lcom/google/android/location/places/ui/a;->o:Lcom/google/android/gms/maps/model/LatLng;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/location/places/ui/av;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/location/places/PlaceType;Lcom/google/android/gms/maps/model/LatLng;)V

    .line 459
    invoke-direct {p0}, Lcom/google/android/location/places/ui/a;->n()V

    .line 460
    return-void
.end method

.method public final a([Lcom/google/android/gms/location/places/AutocompletePrediction;)V
    .locals 0

    .prologue
    .line 338
    return-void
.end method

.method public final a([Lcom/google/android/gms/location/places/f;)V
    .locals 0

    .prologue
    .line 333
    return-void
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 231
    iget-object v0, p0, Lcom/google/android/location/places/ui/a;->d:Lcom/google/android/location/places/ui/MarkerMapFragment;

    invoke-virtual {v0}, Lcom/google/android/location/places/ui/MarkerMapFragment;->d()V

    .line 232
    return-void
.end method

.method public final b(Lcom/google/android/gms/location/places/f;)V
    .locals 2

    .prologue
    .line 317
    invoke-virtual {p0}, Lcom/google/android/location/places/ui/a;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    new-instance v1, Lcom/google/android/location/places/ui/c;

    invoke-direct {v1, p0, p1}, Lcom/google/android/location/places/ui/c;-><init>(Lcom/google/android/location/places/ui/a;Lcom/google/android/gms/location/places/f;)V

    invoke-virtual {v0, v1}, Landroid/support/v4/app/q;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 328
    return-void
.end method

.method public final b([Lcom/google/android/gms/location/places/f;)V
    .locals 0

    .prologue
    .line 343
    return-void
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 236
    iget-object v0, p0, Lcom/google/android/location/places/ui/a;->d:Lcom/google/android/location/places/ui/MarkerMapFragment;

    invoke-virtual {v0}, Lcom/google/android/location/places/ui/MarkerMapFragment;->e()V

    .line 237
    return-void
.end method

.method public final c(Lcom/google/android/gms/location/places/f;)V
    .locals 2

    .prologue
    .line 353
    invoke-virtual {p0}, Lcom/google/android/location/places/ui/a;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    new-instance v1, Lcom/google/android/location/places/ui/d;

    invoke-direct {v1, p0, p1}, Lcom/google/android/location/places/ui/d;-><init>(Lcom/google/android/location/places/ui/a;Lcom/google/android/gms/location/places/f;)V

    invoke-virtual {v0, v1}, Landroid/support/v4/app/q;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 366
    return-void
.end method

.method public final d()V
    .locals 6

    .prologue
    .line 242
    iget-object v0, p0, Lcom/google/android/location/places/ui/a;->d:Lcom/google/android/location/places/ui/MarkerMapFragment;

    invoke-virtual {v0}, Lcom/google/android/location/places/ui/MarkerMapFragment;->f()Lcom/google/android/gms/maps/model/LatLng;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/places/ui/a;->o:Lcom/google/android/gms/maps/model/LatLng;

    .line 244
    iget-boolean v0, p0, Lcom/google/android/location/places/ui/a;->k:Z

    if-nez v0, :cond_0

    .line 266
    :goto_0
    return-void

    .line 248
    :cond_0
    new-instance v1, Lcom/google/android/location/places/ui/b;

    invoke-direct {v1, p0}, Lcom/google/android/location/places/ui/b;-><init>(Lcom/google/android/location/places/ui/a;)V

    .line 262
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/location/places/ui/a;->i:Z

    .line 263
    new-instance v2, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-direct {v2, v0}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    sget-object v0, Lcom/google/android/location/x;->S:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    int-to-long v4, v0

    invoke-virtual {v2, v1, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 265
    iget-object v0, p0, Lcom/google/android/location/places/ui/a;->b:Lcom/google/android/location/places/ui/av;

    iget-object v1, p0, Lcom/google/android/location/places/ui/a;->o:Lcom/google/android/gms/maps/model/LatLng;

    invoke-virtual {v0, v1}, Lcom/google/android/location/places/ui/av;->a(Lcom/google/android/gms/maps/model/LatLng;)V

    goto :goto_0
.end method

.method public final d(Lcom/google/android/gms/location/places/f;)V
    .locals 4

    .prologue
    .line 427
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/location/places/ui/a;->n:Z

    .line 429
    iget-object v0, p0, Lcom/google/android/location/places/ui/a;->f:Lcom/google/android/location/places/ui/an;

    if-nez v0, :cond_0

    .line 430
    invoke-static {}, Lcom/google/android/location/places/ui/an;->a()Lcom/google/android/location/places/ui/an;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/places/ui/a;->f:Lcom/google/android/location/places/ui/an;

    .line 433
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/location/places/ui/a;->getChildFragmentManager()Landroid/support/v4/app/v;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/v;->a()Landroid/support/v4/app/aj;

    move-result-object v0

    sget v1, Lcom/google/android/gms/j;->of:I

    iget-object v2, p0, Lcom/google/android/location/places/ui/a;->f:Lcom/google/android/location/places/ui/an;

    const-string v3, "place_editor_fragment"

    invoke-virtual {v0, v1, v2, v3}, Landroid/support/v4/app/aj;->b(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/aj;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v4/app/aj;->a(Ljava/lang/String;)Landroid/support/v4/app/aj;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/aj;->a()I

    .line 439
    iget-object v0, p0, Lcom/google/android/location/places/ui/a;->f:Lcom/google/android/location/places/ui/an;

    invoke-virtual {v0, p0}, Lcom/google/android/location/places/ui/an;->a(Lcom/google/android/location/places/ui/as;)Lcom/google/android/location/places/ui/an;

    .line 440
    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lcom/google/android/gms/location/places/f;->c()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 441
    iget-object v0, p0, Lcom/google/android/location/places/ui/a;->f:Lcom/google/android/location/places/ui/an;

    invoke-virtual {p1}, Lcom/google/android/gms/location/places/f;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/location/places/ui/an;->a(Ljava/lang/String;)Lcom/google/android/location/places/ui/an;

    .line 446
    :goto_0
    return-void

    .line 443
    :cond_1
    iget-object v0, p0, Lcom/google/android/location/places/ui/a;->f:Lcom/google/android/location/places/ui/an;

    const-string v1, ""

    invoke-virtual {v0, v1}, Lcom/google/android/location/places/ui/an;->a(Ljava/lang/String;)Lcom/google/android/location/places/ui/an;

    goto :goto_0
.end method

.method public final e()V
    .locals 0

    .prologue
    .line 276
    return-void
.end method

.method public final f()V
    .locals 1

    .prologue
    .line 296
    iget-boolean v0, p0, Lcom/google/android/location/places/ui/a;->k:Z

    if-nez v0, :cond_0

    .line 297
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/location/places/ui/a;->a(Z)V

    .line 300
    :cond_0
    iget-boolean v0, p0, Lcom/google/android/location/places/ui/a;->n:Z

    if-nez v0, :cond_1

    .line 301
    iget-boolean v0, p0, Lcom/google/android/location/places/ui/a;->m:Z

    if-eqz v0, :cond_1

    .line 302
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/location/places/ui/a;->m:Z

    .line 303
    invoke-virtual {p0}, Lcom/google/android/location/places/ui/a;->d()V

    .line 306
    :cond_1
    return-void
.end method

.method public final g()V
    .locals 0

    .prologue
    .line 311
    return-void
.end method

.method public final h()V
    .locals 0

    .prologue
    .line 348
    return-void
.end method

.method public final i()V
    .locals 1

    .prologue
    .line 422
    iget-object v0, p0, Lcom/google/android/location/places/ui/a;->a:Lcom/google/android/location/places/ui/j;

    invoke-interface {v0}, Lcom/google/android/location/places/ui/j;->a()V

    .line 423
    return-void
.end method

.method public final j()V
    .locals 1

    .prologue
    .line 450
    iget-boolean v0, p0, Lcom/google/android/location/places/ui/a;->k:Z

    if-eqz v0, :cond_0

    .line 451
    invoke-virtual {p0}, Lcom/google/android/location/places/ui/a;->d()V

    .line 453
    :cond_0
    return-void
.end method

.method public final k()V
    .locals 1

    .prologue
    .line 495
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/location/places/ui/a;->n:Z

    .line 496
    invoke-virtual {p0}, Lcom/google/android/location/places/ui/a;->getChildFragmentManager()Landroid/support/v4/app/v;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/v;->c()V

    .line 497
    iget-object v0, p0, Lcom/google/android/location/places/ui/a;->a:Lcom/google/android/location/places/ui/j;

    invoke-interface {v0}, Lcom/google/android/location/places/ui/j;->a()V

    .line 498
    return-void
.end method

.method public final l()V
    .locals 1

    .prologue
    .line 502
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/location/places/ui/a;->n:Z

    .line 503
    invoke-virtual {p0}, Lcom/google/android/location/places/ui/a;->getChildFragmentManager()Landroid/support/v4/app/v;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/v;->c()V

    .line 504
    invoke-virtual {p0}, Lcom/google/android/location/places/ui/a;->d()V

    .line 505
    return-void
.end method

.method public final m()V
    .locals 1

    .prologue
    .line 508
    iget-boolean v0, p0, Lcom/google/android/location/places/ui/a;->n:Z

    if-eqz v0, :cond_0

    .line 509
    invoke-virtual {p0}, Lcom/google/android/location/places/ui/a;->l()V

    .line 513
    :goto_0
    return-void

    .line 511
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/location/places/ui/a;->i()V

    goto :goto_0
.end method

.method public final onActivityCreated(Landroid/os/Bundle;)V
    .locals 7

    .prologue
    const/high16 v4, 0x42400000    # 48.0f

    const/4 v6, 0x0

    const/4 v5, 0x1

    .line 94
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 96
    invoke-virtual {p0}, Lcom/google/android/location/places/ui/a;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/places/ui/PlacePickerActivity;

    invoke-virtual {v0}, Lcom/google/android/location/places/ui/PlacePickerActivity;->e()Lcom/google/android/location/places/ui/av;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/places/ui/a;->b:Lcom/google/android/location/places/ui/av;

    .line 97
    invoke-virtual {p0}, Lcom/google/android/location/places/ui/a;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/places/ui/PlacePickerActivity;

    invoke-virtual {v0}, Lcom/google/android/location/places/ui/PlacePickerActivity;->c()Lcom/google/android/location/places/ui/s;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/places/ui/a;->c:Lcom/google/android/location/places/ui/s;

    .line 100
    invoke-virtual {p0}, Lcom/google/android/location/places/ui/a;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    sget v1, Lcom/google/android/gms/j;->om:I

    invoke-virtual {v0, v1}, Landroid/support/v4/app/q;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 101
    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    .line 102
    iget v2, v1, Landroid/view/ViewGroup$LayoutParams;->width:I

    invoke-virtual {v0}, Landroid/view/View;->getPaddingLeft()I

    move-result v3

    sub-int/2addr v2, v3

    invoke-virtual {v0}, Landroid/view/View;->getPaddingRight()I

    move-result v3

    sub-int/2addr v2, v3

    .line 103
    iget v1, v1, Landroid/view/ViewGroup$LayoutParams;->height:I

    invoke-virtual {v0}, Landroid/view/View;->getPaddingTop()I

    move-result v3

    sub-int/2addr v1, v3

    invoke-virtual {v0}, Landroid/view/View;->getPaddingBottom()I

    move-result v0

    sub-int/2addr v1, v0

    .line 105
    invoke-virtual {p0}, Lcom/google/android/location/places/ui/a;->getFragmentManager()Landroid/support/v4/app/v;

    move-result-object v0

    sget v3, Lcom/google/android/gms/j;->os:I

    invoke-virtual {v0, v3}, Landroid/support/v4/app/v;->a(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/places/ui/MarkerMapFragment;

    iput-object v0, p0, Lcom/google/android/location/places/ui/a;->d:Lcom/google/android/location/places/ui/MarkerMapFragment;

    .line 107
    invoke-virtual {p0}, Lcom/google/android/location/places/ui/a;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    invoke-static {v4, v0}, Lcom/google/android/location/places/ui/bm;->a(FLandroid/content/Context;)I

    move-result v0

    .line 108
    invoke-virtual {p0}, Lcom/google/android/location/places/ui/a;->getActivity()Landroid/support/v4/app/q;

    move-result-object v3

    invoke-static {v4, v3}, Lcom/google/android/location/places/ui/bm;->a(FLandroid/content/Context;)I

    move-result v3

    .line 109
    iget-object v4, p0, Lcom/google/android/location/places/ui/a;->d:Lcom/google/android/location/places/ui/MarkerMapFragment;

    invoke-virtual {v4, v2, v1, v0, v3}, Lcom/google/android/location/places/ui/MarkerMapFragment;->a(IIII)Lcom/google/android/location/places/ui/MarkerMapFragment;

    .line 111
    if-eqz p1, :cond_0

    .line 112
    const-string v0, "editing_place"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/location/places/ui/a;->n:Z

    .line 113
    const-string v0, "waiting_for_location"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/location/places/ui/a;->m:Z

    .line 114
    const-string v0, "last_center_position"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/maps/model/LatLng;

    iput-object v0, p0, Lcom/google/android/location/places/ui/a;->o:Lcom/google/android/gms/maps/model/LatLng;

    .line 115
    invoke-virtual {p0}, Lcom/google/android/location/places/ui/a;->getChildFragmentManager()Landroid/support/v4/app/v;

    move-result-object v0

    const-string v1, "place_editor_fragment"

    invoke-virtual {v0, v1}, Landroid/support/v4/app/v;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/places/ui/an;

    iput-object v0, p0, Lcom/google/android/location/places/ui/a;->f:Lcom/google/android/location/places/ui/an;

    .line 117
    invoke-virtual {p0}, Lcom/google/android/location/places/ui/a;->getChildFragmentManager()Landroid/support/v4/app/v;

    move-result-object v0

    const-string v1, "location_selector_fragment"

    invoke-virtual {v0, v1}, Landroid/support/v4/app/v;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/places/ui/t;

    iput-object v0, p0, Lcom/google/android/location/places/ui/a;->e:Lcom/google/android/location/places/ui/t;

    .line 121
    :cond_0
    iget-boolean v0, p0, Lcom/google/android/location/places/ui/a;->n:Z

    if-eqz v0, :cond_5

    .line 122
    iget-object v0, p0, Lcom/google/android/location/places/ui/a;->f:Lcom/google/android/location/places/ui/an;

    if-nez v0, :cond_1

    .line 123
    invoke-static {}, Lcom/google/android/location/places/ui/an;->a()Lcom/google/android/location/places/ui/an;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/places/ui/a;->f:Lcom/google/android/location/places/ui/an;

    .line 125
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/location/places/ui/a;->getChildFragmentManager()Landroid/support/v4/app/v;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/v;->a()Landroid/support/v4/app/aj;

    move-result-object v0

    sget v1, Lcom/google/android/gms/j;->of:I

    iget-object v2, p0, Lcom/google/android/location/places/ui/a;->f:Lcom/google/android/location/places/ui/an;

    const-string v3, "place_editor_fragment"

    invoke-virtual {v0, v1, v2, v3}, Landroid/support/v4/app/aj;->b(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/aj;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v4/app/aj;->a(Ljava/lang/String;)Landroid/support/v4/app/aj;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/aj;->a()I

    .line 140
    :goto_0
    iget-object v0, p0, Lcom/google/android/location/places/ui/a;->f:Lcom/google/android/location/places/ui/an;

    if-eqz v0, :cond_2

    .line 141
    iget-object v0, p0, Lcom/google/android/location/places/ui/a;->f:Lcom/google/android/location/places/ui/an;

    invoke-virtual {v0, p0}, Lcom/google/android/location/places/ui/an;->a(Lcom/google/android/location/places/ui/as;)Lcom/google/android/location/places/ui/an;

    .line 144
    :cond_2
    iget-object v0, p0, Lcom/google/android/location/places/ui/a;->e:Lcom/google/android/location/places/ui/t;

    if-eqz v0, :cond_3

    .line 145
    iget-object v0, p0, Lcom/google/android/location/places/ui/a;->e:Lcom/google/android/location/places/ui/t;

    iput-object p0, v0, Lcom/google/android/location/places/ui/t;->a:Lcom/google/android/location/places/ui/w;

    .line 148
    :cond_3
    iget-object v0, p0, Lcom/google/android/location/places/ui/a;->d:Lcom/google/android/location/places/ui/MarkerMapFragment;

    invoke-virtual {v0}, Lcom/google/android/location/places/ui/MarkerMapFragment;->j()V

    .line 149
    iget-object v0, p0, Lcom/google/android/location/places/ui/a;->d:Lcom/google/android/location/places/ui/MarkerMapFragment;

    invoke-virtual {v0}, Lcom/google/android/location/places/ui/MarkerMapFragment;->k()V

    .line 150
    invoke-direct {p0, v5}, Lcom/google/android/location/places/ui/a;->a(Z)V

    .line 152
    if-nez p1, :cond_4

    .line 153
    iget-boolean v0, p0, Lcom/google/android/location/places/ui/a;->n:Z

    if-nez v0, :cond_4

    .line 154
    iget-object v0, p0, Lcom/google/android/location/places/ui/a;->c:Lcom/google/android/location/places/ui/s;

    iget v0, v0, Lcom/google/android/location/places/ui/s;->e:I

    if-ne v0, v5, :cond_8

    .line 155
    iget-object v0, p0, Lcom/google/android/location/places/ui/a;->c:Lcom/google/android/location/places/ui/s;

    iget-object v0, v0, Lcom/google/android/location/places/ui/s;->j:Lcom/google/android/gms/maps/model/LatLngBounds;

    if-eqz v0, :cond_7

    .line 156
    iput-boolean v5, p0, Lcom/google/android/location/places/ui/a;->j:Z

    .line 157
    iput-boolean v6, p0, Lcom/google/android/location/places/ui/a;->m:Z

    .line 158
    invoke-direct {p0, v6}, Lcom/google/android/location/places/ui/a;->a(Z)V

    .line 168
    :cond_4
    :goto_1
    return-void

    .line 131
    :cond_5
    iget-object v0, p0, Lcom/google/android/location/places/ui/a;->e:Lcom/google/android/location/places/ui/t;

    if-nez v0, :cond_6

    .line 132
    new-instance v0, Lcom/google/android/location/places/ui/t;

    invoke-direct {v0}, Lcom/google/android/location/places/ui/t;-><init>()V

    iput-object v0, p0, Lcom/google/android/location/places/ui/a;->e:Lcom/google/android/location/places/ui/t;

    .line 134
    :cond_6
    invoke-virtual {p0}, Lcom/google/android/location/places/ui/a;->getChildFragmentManager()Landroid/support/v4/app/v;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/v;->a()Landroid/support/v4/app/aj;

    move-result-object v0

    sget v1, Lcom/google/android/gms/j;->of:I

    iget-object v2, p0, Lcom/google/android/location/places/ui/a;->e:Lcom/google/android/location/places/ui/t;

    const-string v3, "location_selector_fragment"

    invoke-virtual {v0, v1, v2, v3}, Landroid/support/v4/app/aj;->b(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/aj;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/aj;->a()I

    goto :goto_0

    .line 160
    :cond_7
    iput-boolean v5, p0, Lcom/google/android/location/places/ui/a;->m:Z

    .line 161
    invoke-direct {p0, v6}, Lcom/google/android/location/places/ui/a;->a(Z)V

    goto :goto_1

    .line 164
    :cond_8
    iput-boolean v5, p0, Lcom/google/android/location/places/ui/a;->m:Z

    goto :goto_1
.end method

.method public final onAttach(Landroid/app/Activity;)V
    .locals 4

    .prologue
    .line 75
    instance-of v1, p1, Lcom/google/android/location/places/ui/PlacePickerActivity;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " must be an instance of PlacePickerActivity."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/gms/common/internal/bx;->b(ZLjava/lang/Object;)V

    .line 77
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onAttach(Landroid/app/Activity;)V

    .line 79
    :try_start_0
    move-object v0, p1

    check-cast v0, Lcom/google/android/location/places/ui/j;

    move-object v1, v0

    iput-object v1, p0, Lcom/google/android/location/places/ui/a;->a:Lcom/google/android/location/places/ui/j;
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    .line 83
    return-void

    .line 81
    :catch_0
    move-exception v1

    new-instance v1, Ljava/lang/ClassCastException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " must implement AddAPlaceFragment.Listener"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/ClassCastException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2

    .prologue
    .line 88
    sget v0, Lcom/google/android/gms/l;->cS:I

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/google/android/location/places/ui/a;->l:Landroid/view/ViewGroup;

    .line 89
    iget-object v0, p0, Lcom/google/android/location/places/ui/a;->l:Landroid/view/ViewGroup;

    return-object v0
.end method

.method public final onDestroyView()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 206
    iget-object v0, p0, Lcom/google/android/location/places/ui/a;->b:Lcom/google/android/location/places/ui/av;

    if-eqz v0, :cond_0

    .line 207
    iget-object v0, p0, Lcom/google/android/location/places/ui/a;->b:Lcom/google/android/location/places/ui/av;

    invoke-virtual {v0}, Lcom/google/android/location/places/ui/av;->a()V

    .line 208
    iget-object v0, p0, Lcom/google/android/location/places/ui/a;->b:Lcom/google/android/location/places/ui/av;

    invoke-virtual {v0, v1}, Lcom/google/android/location/places/ui/av;->a(Lcom/google/android/location/places/ui/ay;)V

    .line 211
    :cond_0
    iget-object v0, p0, Lcom/google/android/location/places/ui/a;->d:Lcom/google/android/location/places/ui/MarkerMapFragment;

    if-eqz v0, :cond_1

    .line 212
    iget-object v0, p0, Lcom/google/android/location/places/ui/a;->d:Lcom/google/android/location/places/ui/MarkerMapFragment;

    invoke-virtual {v0, v1}, Lcom/google/android/location/places/ui/MarkerMapFragment;->a(Lcom/google/android/location/places/ui/ab;)Lcom/google/android/location/places/ui/MarkerMapFragment;

    .line 215
    :cond_1
    iget-object v0, p0, Lcom/google/android/location/places/ui/a;->g:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_2

    .line 216
    iget-object v0, p0, Lcom/google/android/location/places/ui/a;->g:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 217
    iput-object v1, p0, Lcom/google/android/location/places/ui/a;->g:Landroid/app/ProgressDialog;

    .line 221
    :cond_2
    iget-object v0, p0, Lcom/google/android/location/places/ui/a;->h:Landroid/app/AlertDialog;

    if-eqz v0, :cond_3

    .line 222
    iget-object v0, p0, Lcom/google/android/location/places/ui/a;->h:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    .line 223
    iput-object v1, p0, Lcom/google/android/location/places/ui/a;->h:Landroid/app/AlertDialog;

    .line 226
    :cond_3
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onDestroyView()V

    .line 227
    return-void
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 198
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 199
    const-string v0, "editing_place"

    iget-boolean v1, p0, Lcom/google/android/location/places/ui/a;->n:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 200
    const-string v0, "waiting_for_location"

    iget-boolean v1, p0, Lcom/google/android/location/places/ui/a;->m:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 201
    const-string v0, "last_center_position"

    iget-object v1, p0, Lcom/google/android/location/places/ui/a;->o:Lcom/google/android/gms/maps/model/LatLng;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 202
    return-void
.end method

.method public final onStart()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 173
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onStart()V

    .line 175
    iget-object v0, p0, Lcom/google/android/location/places/ui/a;->b:Lcom/google/android/location/places/ui/av;

    invoke-virtual {v0, p0}, Lcom/google/android/location/places/ui/av;->a(Lcom/google/android/location/places/ui/ay;)V

    .line 176
    iget-boolean v0, p0, Lcom/google/android/location/places/ui/a;->m:Z

    if-eqz v0, :cond_1

    .line 177
    iget-object v0, p0, Lcom/google/android/location/places/ui/a;->c:Lcom/google/android/location/places/ui/s;

    iget v0, v0, Lcom/google/android/location/places/ui/s;->e:I

    if-ne v0, v1, :cond_0

    .line 178
    invoke-direct {p0, v2}, Lcom/google/android/location/places/ui/a;->a(Z)V

    .line 182
    :goto_0
    iget-object v0, p0, Lcom/google/android/location/places/ui/a;->d:Lcom/google/android/location/places/ui/MarkerMapFragment;

    invoke-virtual {v0, p0}, Lcom/google/android/location/places/ui/MarkerMapFragment;->a(Lcom/google/android/location/places/ui/ab;)Lcom/google/android/location/places/ui/MarkerMapFragment;

    .line 183
    iget-object v0, p0, Lcom/google/android/location/places/ui/a;->d:Lcom/google/android/location/places/ui/MarkerMapFragment;

    invoke-virtual {v0}, Lcom/google/android/location/places/ui/MarkerMapFragment;->l()V

    .line 184
    iput-boolean v2, p0, Lcom/google/android/location/places/ui/a;->j:Z

    .line 194
    :goto_1
    return-void

    .line 180
    :cond_0
    invoke-direct {p0, v1}, Lcom/google/android/location/places/ui/a;->a(Z)V

    goto :goto_0

    .line 185
    :cond_1
    iget-boolean v0, p0, Lcom/google/android/location/places/ui/a;->j:Z

    if-eqz v0, :cond_2

    .line 186
    invoke-direct {p0, v1}, Lcom/google/android/location/places/ui/a;->a(Z)V

    .line 187
    iget-object v0, p0, Lcom/google/android/location/places/ui/a;->d:Lcom/google/android/location/places/ui/MarkerMapFragment;

    invoke-virtual {v0, p0}, Lcom/google/android/location/places/ui/MarkerMapFragment;->a(Lcom/google/android/location/places/ui/ab;)Lcom/google/android/location/places/ui/MarkerMapFragment;

    .line 188
    iget-object v0, p0, Lcom/google/android/location/places/ui/a;->d:Lcom/google/android/location/places/ui/MarkerMapFragment;

    iget-object v1, p0, Lcom/google/android/location/places/ui/a;->c:Lcom/google/android/location/places/ui/s;

    iget-object v1, v1, Lcom/google/android/location/places/ui/s;->j:Lcom/google/android/gms/maps/model/LatLngBounds;

    invoke-virtual {v0, v1}, Lcom/google/android/location/places/ui/MarkerMapFragment;->a(Lcom/google/android/gms/maps/model/LatLngBounds;)V

    .line 189
    iput-boolean v2, p0, Lcom/google/android/location/places/ui/a;->j:Z

    goto :goto_1

    .line 191
    :cond_2
    invoke-direct {p0, v1}, Lcom/google/android/location/places/ui/a;->a(Z)V

    .line 192
    iget-object v0, p0, Lcom/google/android/location/places/ui/a;->d:Lcom/google/android/location/places/ui/MarkerMapFragment;

    invoke-virtual {v0, p0}, Lcom/google/android/location/places/ui/MarkerMapFragment;->a(Lcom/google/android/location/places/ui/ab;)Lcom/google/android/location/places/ui/MarkerMapFragment;

    goto :goto_1
.end method
