.class public final enum Lcom/google/android/location/e/ab;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field public static final enum a:Lcom/google/android/location/e/ab;

.field public static final enum b:Lcom/google/android/location/e/ab;

.field public static final enum c:Lcom/google/android/location/e/ab;

.field public static final enum d:Lcom/google/android/location/e/ab;

.field private static final synthetic e:[Lcom/google/android/location/e/ab;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 20
    new-instance v0, Lcom/google/android/location/e/ab;

    const-string v1, "OK"

    invoke-direct {v0, v1, v2}, Lcom/google/android/location/e/ab;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/location/e/ab;->a:Lcom/google/android/location/e/ab;

    .line 21
    new-instance v0, Lcom/google/android/location/e/ab;

    const-string v1, "NO_LOCATION"

    invoke-direct {v0, v1, v3}, Lcom/google/android/location/e/ab;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/location/e/ab;->b:Lcom/google/android/location/e/ab;

    .line 22
    new-instance v0, Lcom/google/android/location/e/ab;

    const-string v1, "CACHE_MISS"

    invoke-direct {v0, v1, v4}, Lcom/google/android/location/e/ab;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/location/e/ab;->c:Lcom/google/android/location/e/ab;

    .line 23
    new-instance v0, Lcom/google/android/location/e/ab;

    const-string v1, "BATCH_MODE_DEFERRED"

    invoke-direct {v0, v1, v5}, Lcom/google/android/location/e/ab;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/location/e/ab;->d:Lcom/google/android/location/e/ab;

    .line 19
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/google/android/location/e/ab;

    sget-object v1, Lcom/google/android/location/e/ab;->a:Lcom/google/android/location/e/ab;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/android/location/e/ab;->b:Lcom/google/android/location/e/ab;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/android/location/e/ab;->c:Lcom/google/android/location/e/ab;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/android/location/e/ab;->d:Lcom/google/android/location/e/ab;

    aput-object v1, v0, v5

    sput-object v0, Lcom/google/android/location/e/ab;->e:[Lcom/google/android/location/e/ab;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/location/e/ab;
    .locals 1

    .prologue
    .line 19
    const-class v0, Lcom/google/android/location/e/ab;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/e/ab;

    return-object v0
.end method

.method public static values()[Lcom/google/android/location/e/ab;
    .locals 1

    .prologue
    .line 19
    sget-object v0, Lcom/google/android/location/e/ab;->e:[Lcom/google/android/location/e/ab;

    invoke-virtual {v0}, [Lcom/google/android/location/e/ab;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/location/e/ab;

    return-object v0
.end method
