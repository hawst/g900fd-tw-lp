.class public final Lcom/google/android/location/copresence/h/a;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/location/copresence/l/x;
.implements Lcom/google/android/location/copresence/o/o;


# static fields
.field public static b:Lcom/google/android/location/copresence/h/a;


# instance fields
.field a:Ljava/lang/Long;

.field private final c:Landroid/content/Context;

.field private final d:Lcom/google/android/location/copresence/n/f;

.field private final e:Lcom/google/android/location/copresence/ap;

.field private final f:Lcom/google/android/location/copresence/an;

.field private final g:Lcom/google/android/gms/common/util/p;

.field private final h:Ljava/lang/Runnable;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 6

    .prologue
    .line 76
    invoke-static {}, Lcom/google/android/gms/common/util/r;->c()Lcom/google/android/gms/common/util/p;

    move-result-object v2

    invoke-static {p1}, Lcom/google/android/location/copresence/ap;->a(Landroid/content/Context;)Lcom/google/android/location/copresence/ap;

    move-result-object v3

    invoke-static {p1}, Lcom/google/android/location/copresence/n/f;->a(Landroid/content/Context;)Lcom/google/android/location/copresence/n/f;

    move-result-object v4

    invoke-static {p1}, Lcom/google/android/location/copresence/an;->a(Landroid/content/Context;)Lcom/google/android/location/copresence/an;

    move-result-object v5

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Lcom/google/android/location/copresence/h/a;-><init>(Landroid/content/Context;Lcom/google/android/gms/common/util/p;Lcom/google/android/location/copresence/ap;Lcom/google/android/location/copresence/n/f;Lcom/google/android/location/copresence/an;)V

    .line 81
    invoke-static {p1}, Lcom/google/android/location/copresence/l/m;->a(Landroid/content/Context;)Lcom/google/android/location/copresence/l/m;

    move-result-object v0

    iput-object p0, v0, Lcom/google/android/location/copresence/l/m;->c:Lcom/google/android/location/copresence/l/x;

    .line 82
    invoke-static {p1}, Lcom/google/android/location/copresence/o/k;->a(Landroid/content/Context;)Lcom/google/android/location/copresence/o/k;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/android/location/copresence/o/k;->a(Lcom/google/android/location/copresence/o/o;)V

    .line 83
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Lcom/google/android/gms/common/util/p;Lcom/google/android/location/copresence/ap;Lcom/google/android/location/copresence/n/f;Lcom/google/android/location/copresence/an;)V
    .locals 1

    .prologue
    .line 88
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 53
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/location/copresence/h/a;->a:Ljava/lang/Long;

    .line 55
    new-instance v0, Lcom/google/android/location/copresence/h/b;

    invoke-direct {v0, p0}, Lcom/google/android/location/copresence/h/b;-><init>(Lcom/google/android/location/copresence/h/a;)V

    iput-object v0, p0, Lcom/google/android/location/copresence/h/a;->h:Ljava/lang/Runnable;

    .line 89
    iput-object p1, p0, Lcom/google/android/location/copresence/h/a;->c:Landroid/content/Context;

    .line 90
    iput-object p2, p0, Lcom/google/android/location/copresence/h/a;->g:Lcom/google/android/gms/common/util/p;

    .line 91
    iput-object p3, p0, Lcom/google/android/location/copresence/h/a;->e:Lcom/google/android/location/copresence/ap;

    .line 92
    iput-object p4, p0, Lcom/google/android/location/copresence/h/a;->d:Lcom/google/android/location/copresence/n/f;

    .line 93
    iput-object p5, p0, Lcom/google/android/location/copresence/h/a;->f:Lcom/google/android/location/copresence/an;

    .line 94
    return-void
.end method


# virtual methods
.method final a()V
    .locals 9

    .prologue
    const/4 v1, 0x0

    const/4 v8, 0x3

    const/4 v4, 0x1

    .line 144
    iget-object v0, p0, Lcom/google/android/location/copresence/h/a;->f:Lcom/google/android/location/copresence/an;

    invoke-virtual {v0, v4, v4}, Lcom/google/android/location/copresence/an;->b(II)Ljava/util/List;

    move-result-object v0

    .line 146
    iget-object v2, p0, Lcom/google/android/location/copresence/h/a;->f:Lcom/google/android/location/copresence/an;

    const/4 v3, 0x2

    invoke-virtual {v2, v4, v3}, Lcom/google/android/location/copresence/an;->b(II)Ljava/util/List;

    move-result-object v2

    .line 148
    invoke-static {v8}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 149
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "DirectiveHandler: updateDirectiveState: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/location/copresence/ag;->b(Ljava/lang/String;)I

    .line 153
    :cond_0
    iget-object v3, p0, Lcom/google/android/location/copresence/h/a;->e:Lcom/google/android/location/copresence/ap;

    invoke-virtual {v3}, Lcom/google/android/location/copresence/ap;->a()Lcom/google/android/location/copresence/j;

    move-result-object v3

    invoke-virtual {v3, v2}, Lcom/google/android/location/copresence/j;->a(Ljava/util/List;)V

    .line 154
    iget-object v2, p0, Lcom/google/android/location/copresence/h/a;->e:Lcom/google/android/location/copresence/ap;

    invoke-virtual {v2}, Lcom/google/android/location/copresence/ap;->a()Lcom/google/android/location/copresence/j;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/google/android/location/copresence/j;->b(Ljava/util/List;)V

    .line 158
    iget-object v0, p0, Lcom/google/android/location/copresence/h/a;->f:Lcom/google/android/location/copresence/an;

    invoke-virtual {v0}, Lcom/google/android/location/copresence/an;->c()Ljava/lang/Long;

    move-result-object v0

    .line 159
    iget-object v2, p0, Lcom/google/android/location/copresence/h/a;->g:Lcom/google/android/gms/common/util/p;

    invoke-interface {v2}, Lcom/google/android/gms/common/util/p;->b()J

    move-result-wide v2

    .line 160
    if-nez v0, :cond_3

    move-object v0, v1

    .line 162
    :goto_0
    if-eqz v0, :cond_5

    .line 163
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    const-wide/16 v4, 0xbb8

    add-long/2addr v0, v4

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    .line 165
    iget-object v1, p0, Lcom/google/android/location/copresence/h/a;->a:Ljava/lang/Long;

    if-eqz v1, :cond_1

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    iget-object v1, p0, Lcom/google/android/location/copresence/h/a;->a:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    cmp-long v1, v4, v6

    if-gez v1, :cond_4

    .line 167
    :cond_1
    iput-object v0, p0, Lcom/google/android/location/copresence/h/a;->a:Ljava/lang/Long;

    .line 168
    iget-object v1, p0, Lcom/google/android/location/copresence/h/a;->e:Lcom/google/android/location/copresence/ap;

    invoke-virtual {v1}, Lcom/google/android/location/copresence/ap;->b()Lcom/google/android/location/copresence/c;

    move-result-object v1

    iget-object v4, p0, Lcom/google/android/location/copresence/h/a;->h:Ljava/lang/Runnable;

    iget-object v5, p0, Lcom/google/android/location/copresence/h/a;->a:Ljava/lang/Long;

    invoke-virtual {v5}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    sub-long/2addr v6, v2

    invoke-virtual {v1, v4, v6, v7}, Lcom/google/android/location/copresence/c;->a(Ljava/lang/Runnable;J)V

    .line 170
    invoke-static {v8}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 171
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v4, "DirectiveHandler: Setting alarm: mNextAlarmSinceBootMillis="

    invoke-direct {v1, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/google/android/location/copresence/h/a;->a:Ljava/lang/Long;

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, ", desiredAlarmSinceBoot="

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " now="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->b(Ljava/lang/String;)I

    .line 185
    :cond_2
    :goto_1
    return-void

    .line 160
    :cond_3
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    add-long/2addr v4, v2

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    goto :goto_0

    .line 175
    :cond_4
    invoke-static {v8}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 176
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v4, "DirectiveHandler: Not setting alarm: mNextAlarmSinceBootMillis="

    invoke-direct {v1, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/google/android/location/copresence/h/a;->a:Ljava/lang/Long;

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, ", desiredAlarmSinceBoot="

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " now="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->b(Ljava/lang/String;)I

    goto :goto_1

    .line 182
    :cond_5
    iget-object v0, p0, Lcom/google/android/location/copresence/h/a;->e:Lcom/google/android/location/copresence/ap;

    invoke-virtual {v0}, Lcom/google/android/location/copresence/ap;->b()Lcom/google/android/location/copresence/c;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/location/copresence/h/a;->h:Ljava/lang/Runnable;

    invoke-virtual {v0, v2}, Lcom/google/android/location/copresence/c;->a(Ljava/lang/Runnable;)V

    .line 183
    iput-object v1, p0, Lcom/google/android/location/copresence/h/a;->a:Ljava/lang/Long;

    goto :goto_1
.end method

.method public final a(JII)V
    .locals 0

    .prologue
    .line 235
    return-void
.end method

.method public final a(JILjava/lang/String;I)V
    .locals 1

    .prologue
    .line 213
    packed-switch p3, :pswitch_data_0

    .line 221
    :goto_0
    :pswitch_0
    return-void

    .line 215
    :pswitch_1
    iget-object v0, p0, Lcom/google/android/location/copresence/h/a;->f:Lcom/google/android/location/copresence/an;

    invoke-virtual {v0, p4, p1, p2}, Lcom/google/android/location/copresence/an;->b(Ljava/lang/String;J)V

    .line 216
    invoke-virtual {p0}, Lcom/google/android/location/copresence/h/a;->a()V

    goto :goto_0

    .line 219
    :pswitch_2
    iget-object v0, p0, Lcom/google/android/location/copresence/h/a;->f:Lcom/google/android/location/copresence/an;

    invoke-virtual {v0, p4, p1, p2}, Lcom/google/android/location/copresence/an;->a(Ljava/lang/String;J)V

    .line 220
    invoke-virtual {p0}, Lcom/google/android/location/copresence/h/a;->a()V

    goto :goto_0

    .line 213
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public final a(Lcom/google/android/gms/common/api/Status;[Lcom/google/ac/b/c/ah;ZLjava/util/HashSet;)V
    .locals 9

    .prologue
    const/4 v8, 0x1

    .line 100
    iget-object v0, p0, Lcom/google/android/location/copresence/h/a;->e:Lcom/google/android/location/copresence/ap;

    invoke-virtual {v0}, Lcom/google/android/location/copresence/ap;->c()V

    .line 101
    if-eqz p2, :cond_0

    array-length v0, p2

    if-eqz v0, :cond_0

    sget-object v0, Lcom/google/android/gms/common/api/Status;->a:Lcom/google/android/gms/common/api/Status;

    if-eq p1, v0, :cond_1

    .line 141
    :cond_0
    :goto_0
    return-void

    .line 105
    :cond_1
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    .line 107
    array-length v3, p2

    const/4 v0, 0x0

    move v1, v0

    :goto_1
    if-ge v1, v3, :cond_7

    aget-object v4, p2, v1

    .line 108
    iget-object v0, v4, Lcom/google/ac/b/c/ah;->c:Ljava/lang/Long;

    if-nez v0, :cond_2

    const-wide/16 v6, 0x0

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, v4, Lcom/google/ac/b/c/ah;->c:Ljava/lang/Long;

    .line 109
    :cond_2
    iget-object v0, v4, Lcom/google/ac/b/c/ah;->a:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-ne v0, v8, :cond_4

    .line 110
    iget-object v0, p0, Lcom/google/android/location/copresence/h/a;->f:Lcom/google/android/location/copresence/an;

    invoke-virtual {v0, v4, p3, p4}, Lcom/google/android/location/copresence/an;->a(Lcom/google/ac/b/c/ah;ZLjava/util/HashSet;)V

    .line 111
    iget-object v0, v4, Lcom/google/ac/b/c/ah;->b:Lcom/google/ac/b/c/bt;

    iget-object v0, v0, Lcom/google/ac/b/c/bt;->a:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-ne v0, v8, :cond_4

    .line 113
    const/4 v0, 0x3

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 114
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v5, "DirectiveHandler: Transmit token "

    invoke-direct {v0, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v5, v4, Lcom/google/ac/b/c/ah;->b:Lcom/google/ac/b/c/bt;

    iget-object v5, v5, Lcom/google/ac/b/c/bt;->c:Ljava/lang/String;

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v5, " over medium "

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v5, v4, Lcom/google/ac/b/c/ah;->b:Lcom/google/ac/b/c/bt;

    iget-object v5, v5, Lcom/google/ac/b/c/bt;->b:Ljava/lang/Integer;

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v5, " for "

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v0, v4, Lcom/google/ac/b/c/ah;->d:Ljava/lang/Long;

    if-nez v0, :cond_5

    const-string v0, "infinite TTL"

    :goto_2
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->b(Ljava/lang/String;)I

    .line 119
    :cond_3
    iget-object v0, v4, Lcom/google/ac/b/c/ah;->b:Lcom/google/ac/b/c/bt;

    iget-object v0, v0, Lcom/google/ac/b/c/bt;->c:Ljava/lang/String;

    invoke-virtual {v2, v0}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 120
    iget-object v0, v4, Lcom/google/ac/b/c/ah;->b:Lcom/google/ac/b/c/bt;

    iget-object v0, v0, Lcom/google/ac/b/c/bt;->c:Ljava/lang/String;

    invoke-virtual {v2, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    iget-object v4, v4, Lcom/google/ac/b/c/ah;->b:Lcom/google/ac/b/c/bt;

    iget-object v4, v4, Lcom/google/ac/b/c/bt;->b:Ljava/lang/Integer;

    invoke-interface {v0, v4}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 107
    :cond_4
    :goto_3
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 114
    :cond_5
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v6, v4, Lcom/google/ac/b/c/ah;->d:Ljava/lang/Long;

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v6, "ms."

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    .line 123
    :cond_6
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    .line 124
    iget-object v5, v4, Lcom/google/ac/b/c/ah;->b:Lcom/google/ac/b/c/bt;

    iget-object v5, v5, Lcom/google/ac/b/c/bt;->b:Ljava/lang/Integer;

    invoke-interface {v0, v5}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 125
    iget-object v4, v4, Lcom/google/ac/b/c/ah;->b:Lcom/google/ac/b/c/bt;

    iget-object v4, v4, Lcom/google/ac/b/c/bt;->c:Ljava/lang/String;

    invoke-virtual {v2, v4, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_3

    .line 132
    :cond_7
    iget-object v0, p0, Lcom/google/android/location/copresence/h/a;->d:Lcom/google/android/location/copresence/n/f;

    invoke-virtual {v0}, Lcom/google/android/location/copresence/n/f;->b()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_8
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_9

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/copresence/a/a;

    .line 133
    invoke-static {v0}, Lcom/google/android/location/copresence/p/a;->a(Lcom/google/android/location/copresence/a/a;)Lcom/google/android/location/copresence/p/a;

    move-result-object v4

    .line 134
    invoke-virtual {v2}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_4
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 135
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-static {v1}, Lcom/google/android/gms/location/copresence/x;->a(Ljava/lang/String;)Lcom/google/android/gms/location/copresence/x;

    move-result-object v1

    .line 136
    iget-object v6, p0, Lcom/google/android/location/copresence/h/a;->c:Landroid/content/Context;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    invoke-virtual {v4, v6, v1, v0}, Lcom/google/android/location/copresence/p/a;->a(Landroid/content/Context;Lcom/google/android/gms/location/copresence/x;Ljava/util/Set;)Z

    goto :goto_4

    .line 140
    :cond_9
    invoke-virtual {p0}, Lcom/google/android/location/copresence/h/a;->a()V

    goto/16 :goto_0
.end method
