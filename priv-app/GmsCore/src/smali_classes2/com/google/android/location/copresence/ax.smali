.class public final Lcom/google/android/location/copresence/ax;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static final a:Ljava/util/Set;

.field private static b:Lcom/google/android/location/copresence/q/g;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    .line 28
    new-instance v0, Ljava/util/HashSet;

    const/4 v1, 0x6

    new-array v1, v1, [Ljava/lang/Integer;

    const/4 v2, 0x0

    const/16 v3, 0x8

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v6

    const/4 v2, 0x7

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v7

    const/4 v2, 0x5

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    sput-object v0, Lcom/google/android/location/copresence/ax;->a:Ljava/util/Set;

    return-void
.end method

.method public static declared-synchronized a(Landroid/content/Context;)Lcom/google/android/location/copresence/q/g;
    .locals 3

    .prologue
    .line 39
    const-class v1, Lcom/google/android/location/copresence/ax;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/google/android/location/copresence/ax;->b:Lcom/google/android/location/copresence/q/g;

    if-nez v0, :cond_0

    .line 40
    new-instance v0, Lcom/google/android/location/copresence/q/g;

    invoke-static {p0}, Lcom/google/android/location/copresence/ap;->a(Landroid/content/Context;)Lcom/google/android/location/copresence/ap;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/location/copresence/ap;->d()Landroid/os/Handler;

    move-result-object v2

    invoke-direct {v0, v2}, Lcom/google/android/location/copresence/q/g;-><init>(Landroid/os/Handler;)V

    sput-object v0, Lcom/google/android/location/copresence/ax;->b:Lcom/google/android/location/copresence/q/g;

    .line 42
    :cond_0
    sget-object v0, Lcom/google/android/location/copresence/ax;->b:Lcom/google/android/location/copresence/q/g;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 39
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static a(Landroid/content/Context;I)Lcom/google/android/location/copresence/w;
    .locals 2

    .prologue
    .line 46
    packed-switch p1, :pswitch_data_0

    .line 60
    :pswitch_0
    const/4 v0, 0x6

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 61
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Unknown Medium requested: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1}, Ljava/lang/IllegalStateException;-><init>()V

    invoke-static {v0, v1}, Lcom/google/android/location/copresence/ag;->a(Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 63
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    .line 48
    :pswitch_1
    invoke-static {p0}, Lcom/google/android/location/copresence/c/a;->a(Landroid/content/Context;)Lcom/google/android/location/copresence/c/a;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/location/copresence/c/a;->c:Lcom/google/android/location/copresence/w;

    .line 58
    :goto_0
    return-object v0

    .line 50
    :pswitch_2
    invoke-static {p0}, Lcom/google/android/location/copresence/c/a;->a(Landroid/content/Context;)Lcom/google/android/location/copresence/c/a;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/location/copresence/c/a;->a:Lcom/google/android/location/copresence/w;

    goto :goto_0

    .line 52
    :pswitch_3
    invoke-static {p0}, Lcom/google/android/location/copresence/d/j;->a(Landroid/content/Context;)Lcom/google/android/location/copresence/d/j;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/location/copresence/d/j;->j()Lcom/google/android/location/copresence/w;

    move-result-object v0

    goto :goto_0

    .line 54
    :pswitch_4
    invoke-static {p0}, Lcom/google/android/location/copresence/d/j;->a(Landroid/content/Context;)Lcom/google/android/location/copresence/d/j;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/location/copresence/d/j;->l()Lcom/google/android/location/copresence/w;

    move-result-object v0

    goto :goto_0

    .line 56
    :pswitch_5
    invoke-static {p0}, Lcom/google/android/location/copresence/r/ag;->a(Landroid/content/Context;)Lcom/google/android/location/copresence/r/ag;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/location/copresence/r/ag;->b:Lcom/google/android/location/copresence/w;

    goto :goto_0

    .line 58
    :pswitch_6
    invoke-static {p0}, Lcom/google/android/location/copresence/r/ag;->a(Landroid/content/Context;)Lcom/google/android/location/copresence/r/ag;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/location/copresence/r/ag;->a:Lcom/google/android/location/copresence/w;

    goto :goto_0

    .line 46
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_0
        :pswitch_0
        :pswitch_6
        :pswitch_2
    .end packed-switch
.end method

.method public static b(Landroid/content/Context;I)Lcom/google/android/location/copresence/z;
    .locals 2

    .prologue
    .line 68
    packed-switch p1, :pswitch_data_0

    .line 81
    :pswitch_0
    const/4 v0, 0x6

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 82
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Unknown Medium requested: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1}, Ljava/lang/IllegalStateException;-><init>()V

    invoke-static {v0, v1}, Lcom/google/android/location/copresence/ag;->b(Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 84
    :cond_0
    sget-object v0, Lcom/google/android/location/copresence/z;->a:Lcom/google/android/location/copresence/z;

    :goto_0
    return-object v0

    .line 70
    :pswitch_1
    invoke-static {p0}, Lcom/google/android/location/copresence/c/a;->a(Landroid/content/Context;)Lcom/google/android/location/copresence/c/a;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/location/copresence/c/a;->d:Lcom/google/android/location/copresence/z;

    goto :goto_0

    .line 72
    :pswitch_2
    invoke-static {p0}, Lcom/google/android/location/copresence/c/a;->a(Landroid/content/Context;)Lcom/google/android/location/copresence/c/a;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/location/copresence/c/a;->b:Lcom/google/android/location/copresence/z;

    goto :goto_0

    .line 74
    :pswitch_3
    invoke-static {p0}, Lcom/google/android/location/copresence/d/j;->a(Landroid/content/Context;)Lcom/google/android/location/copresence/d/j;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/location/copresence/d/j;->k()Lcom/google/android/location/copresence/z;

    move-result-object v0

    goto :goto_0

    .line 76
    :pswitch_4
    invoke-static {p0}, Lcom/google/android/location/copresence/d/j;->a(Landroid/content/Context;)Lcom/google/android/location/copresence/d/j;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/location/copresence/d/j;->m()Lcom/google/android/location/copresence/z;

    move-result-object v0

    goto :goto_0

    .line 79
    :pswitch_5
    invoke-static {p0}, Lcom/google/android/location/copresence/r/ag;->a(Landroid/content/Context;)Lcom/google/android/location/copresence/r/ag;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/location/copresence/r/ag;->c:Lcom/google/android/location/copresence/z;

    goto :goto_0

    .line 68
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_0
        :pswitch_0
        :pswitch_5
        :pswitch_2
    .end packed-switch
.end method
