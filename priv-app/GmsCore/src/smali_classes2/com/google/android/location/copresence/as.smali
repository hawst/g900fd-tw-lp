.class public final Lcom/google/android/location/copresence/as;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public final a:I

.field public final b:Lcom/google/ac/b/c/o;


# direct methods
.method public constructor <init>(ILcom/google/ac/b/c/o;)V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    iput p1, p0, Lcom/google/android/location/copresence/as;->a:I

    .line 17
    iput-object p2, p0, Lcom/google/android/location/copresence/as;->b:Lcom/google/ac/b/c/o;

    .line 18
    return-void
.end method


# virtual methods
.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 31
    if-ne p0, p1, :cond_1

    .line 39
    :cond_0
    :goto_0
    return v0

    .line 34
    :cond_1
    instance-of v2, p1, Lcom/google/android/location/copresence/as;

    if-eqz v2, :cond_3

    .line 35
    check-cast p1, Lcom/google/android/location/copresence/as;

    .line 36
    iget v2, p0, Lcom/google/android/location/copresence/as;->a:I

    iget v3, p1, Lcom/google/android/location/copresence/as;->a:I

    if-ne v2, v3, :cond_2

    iget-object v2, p0, Lcom/google/android/location/copresence/as;->b:Lcom/google/ac/b/c/o;

    iget-object v3, p1, Lcom/google/android/location/copresence/as;->b:Lcom/google/ac/b/c/o;

    invoke-static {v2, v3}, Lcom/google/android/gms/common/internal/bu;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_2
    move v0, v1

    goto :goto_0

    :cond_3
    move v0, v1

    .line 39
    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    .line 44
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget v2, p0, Lcom/google/android/location/copresence/as;->a:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/location/copresence/as;->b:Lcom/google/ac/b/c/o;

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method
