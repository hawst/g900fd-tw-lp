.class public final Lcom/google/android/location/ay;
.super Lcom/google/android/location/a;
.source "SourceFile"


# static fields
.field public static final i:Ljava/util/Set;

.field public static final k:Ljava/util/List;


# instance fields
.field private final A:Lcom/google/android/location/at;

.field private final B:Ljava/util/Random;

.field private C:Z

.field private D:Ljava/lang/Integer;

.field private E:Lcom/google/android/location/o/f;

.field private F:Z

.field private G:Z

.field public final j:Ljava/util/Set;

.field public l:J

.field public m:Lcom/google/android/location/g;

.field public n:Z

.field public o:J

.field public p:Lcom/google/android/location/collectionlib/be;

.field public q:Z

.field r:Lcom/google/p/a/b/b/a;

.field public s:Z

.field public t:Lcom/google/android/location/collectionlib/cy;

.field u:J

.field public v:J

.field private final w:Lcom/google/android/location/v;

.field private final x:Lcom/google/android/location/av;

.field private final y:Lcom/google/android/location/s;

.field private final z:Lcom/google/android/location/activity/k;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/16 v6, 0x10

    const/16 v5, 0xb

    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 67
    new-array v0, v3, [Lcom/google/android/location/collectionlib/cg;

    sget-object v1, Lcom/google/android/location/collectionlib/cg;->d:Lcom/google/android/location/collectionlib/cg;

    aput-object v1, v0, v4

    invoke-static {v0}, Lcom/google/android/location/collectionlib/cg;->a([Lcom/google/android/location/collectionlib/cg;)Ljava/util/Set;

    move-result-object v0

    sput-object v0, Lcom/google/android/location/ay;->i:Ljava/util/Set;

    .line 91
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 93
    sput-object v0, Lcom/google/android/location/ay;->k:Ljava/util/List;

    new-instance v1, Lcom/google/android/location/o/f;

    const/4 v2, 0x6

    invoke-direct {v1, v2, v4, v5}, Lcom/google/android/location/o/f;-><init>(III)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 95
    sget-object v0, Lcom/google/android/location/ay;->k:Ljava/util/List;

    new-instance v1, Lcom/google/android/location/o/f;

    invoke-direct {v1, v5, v3, v6}, Lcom/google/android/location/o/f;-><init>(III)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 97
    sget-object v0, Lcom/google/android/location/ay;->k:Ljava/util/List;

    new-instance v1, Lcom/google/android/location/o/f;

    const/16 v2, 0x17

    invoke-direct {v1, v6, v3, v2}, Lcom/google/android/location/o/f;-><init>(III)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 98
    return-void
.end method

.method public constructor <init>(Lcom/google/android/location/os/bi;Lcom/google/android/location/b/ak;Lcom/google/android/location/k/a;Lcom/google/android/location/be;Lcom/google/android/location/d;Lcom/google/android/location/v;Lcom/google/android/location/s;Lcom/google/android/location/activity/k;Ljava/util/Random;)V
    .locals 13

    .prologue
    .line 173
    invoke-static {}, Lcom/google/android/location/av;->b()Lcom/google/android/location/av;

    move-result-object v9

    new-instance v12, Lcom/google/android/location/at;

    sget-object v1, Lcom/google/android/location/ay;->k:Ljava/util/List;

    move-object/from16 v0, p6

    invoke-direct {v12, v1, p1, v0}, Lcom/google/android/location/at;-><init>(Ljava/util/List;Lcom/google/android/location/os/bi;Lcom/google/android/location/v;)V

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v10, p8

    move-object/from16 v11, p9

    invoke-direct/range {v1 .. v12}, Lcom/google/android/location/ay;-><init>(Lcom/google/android/location/os/bi;Lcom/google/android/location/b/ak;Lcom/google/android/location/k/a;Lcom/google/android/location/be;Lcom/google/android/location/d;Lcom/google/android/location/v;Lcom/google/android/location/s;Lcom/google/android/location/av;Lcom/google/android/location/activity/k;Ljava/util/Random;Lcom/google/android/location/at;)V

    .line 184
    return-void
.end method

.method private constructor <init>(Lcom/google/android/location/os/bi;Lcom/google/android/location/b/ak;Lcom/google/android/location/k/a;Lcom/google/android/location/be;Lcom/google/android/location/d;Lcom/google/android/location/v;Lcom/google/android/location/s;Lcom/google/android/location/av;Lcom/google/android/location/activity/k;Ljava/util/Random;Lcom/google/android/location/at;)V
    .locals 10

    .prologue
    .line 198
    const-string v3, "SCollector"

    sget-object v9, Lcom/google/android/location/e;->b:Lcom/google/android/location/e;

    move-object v2, p0

    move-object v4, p1

    move-object v5, p2

    move-object v6, p3

    move-object v7, p4

    move-object v8, p5

    invoke-direct/range {v2 .. v9}, Lcom/google/android/location/a;-><init>(Ljava/lang/String;Lcom/google/android/location/os/bi;Lcom/google/android/location/b/ak;Lcom/google/android/location/k/a;Lcom/google/android/location/be;Lcom/google/android/location/d;Lcom/google/android/location/e;)V

    .line 70
    const/4 v2, 0x4

    new-array v2, v2, [Lcom/google/android/location/collectionlib/cg;

    const/4 v3, 0x0

    sget-object v4, Lcom/google/android/location/collectionlib/cg;->g:Lcom/google/android/location/collectionlib/cg;

    aput-object v4, v2, v3

    const/4 v3, 0x1

    sget-object v4, Lcom/google/android/location/collectionlib/cg;->a:Lcom/google/android/location/collectionlib/cg;

    aput-object v4, v2, v3

    const/4 v3, 0x2

    sget-object v4, Lcom/google/android/location/collectionlib/cg;->e:Lcom/google/android/location/collectionlib/cg;

    aput-object v4, v2, v3

    const/4 v3, 0x3

    sget-object v4, Lcom/google/android/location/collectionlib/cg;->d:Lcom/google/android/location/collectionlib/cg;

    aput-object v4, v2, v3

    invoke-static {v2}, Lcom/google/android/location/collectionlib/cg;->a([Lcom/google/android/location/collectionlib/cg;)Ljava/util/Set;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/location/ay;->j:Ljava/util/Set;

    .line 120
    const-wide/16 v2, -0x1

    iput-wide v2, p0, Lcom/google/android/location/ay;->l:J

    .line 122
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/google/android/location/ay;->m:Lcom/google/android/location/g;

    .line 123
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/google/android/location/ay;->C:Z

    .line 125
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/google/android/location/ay;->n:Z

    .line 133
    const-wide/16 v2, -0x1

    iput-wide v2, p0, Lcom/google/android/location/ay;->o:J

    .line 135
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/google/android/location/ay;->p:Lcom/google/android/location/collectionlib/be;

    .line 141
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/google/android/location/ay;->q:Z

    .line 142
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/google/android/location/ay;->r:Lcom/google/p/a/b/b/a;

    .line 149
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/google/android/location/ay;->s:Z

    .line 151
    new-instance v2, Lcom/google/android/location/az;

    const/4 v3, 0x0

    invoke-direct {v2, p0, v3}, Lcom/google/android/location/az;-><init>(Lcom/google/android/location/ay;B)V

    iput-object v2, p0, Lcom/google/android/location/ay;->t:Lcom/google/android/location/collectionlib/cy;

    .line 153
    const-wide/16 v2, -0x1

    iput-wide v2, p0, Lcom/google/android/location/ay;->u:J

    .line 156
    const-wide/16 v2, -0x1

    iput-wide v2, p0, Lcom/google/android/location/ay;->v:J

    .line 159
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/google/android/location/ay;->F:Z

    .line 162
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/google/android/location/ay;->G:Z

    .line 199
    move-object/from16 v0, p6

    iput-object v0, p0, Lcom/google/android/location/ay;->w:Lcom/google/android/location/v;

    .line 200
    move-object/from16 v0, p8

    iput-object v0, p0, Lcom/google/android/location/ay;->x:Lcom/google/android/location/av;

    .line 201
    move-object/from16 v0, p7

    iput-object v0, p0, Lcom/google/android/location/ay;->y:Lcom/google/android/location/s;

    .line 202
    move-object/from16 v0, p9

    iput-object v0, p0, Lcom/google/android/location/ay;->z:Lcom/google/android/location/activity/k;

    .line 203
    move-object/from16 v0, p11

    iput-object v0, p0, Lcom/google/android/location/ay;->A:Lcom/google/android/location/at;

    .line 204
    move-object/from16 v0, p10

    iput-object v0, p0, Lcom/google/android/location/ay;->B:Ljava/util/Random;

    .line 207
    sget-object v2, Lcom/google/android/location/collectionlib/cg;->j:Lcom/google/android/location/collectionlib/cg;

    invoke-interface {p1, v2}, Lcom/google/android/location/os/bi;->a(Lcom/google/android/location/collectionlib/cg;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 208
    iget-object v2, p0, Lcom/google/android/location/ay;->j:Ljava/util/Set;

    sget-object v3, Lcom/google/android/location/collectionlib/cg;->j:Lcom/google/android/location/collectionlib/cg;

    invoke-interface {v2, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 214
    :cond_0
    :goto_0
    sget-object v2, Lcom/google/android/location/collectionlib/cg;->i:Lcom/google/android/location/collectionlib/cg;

    invoke-interface {p1, v2}, Lcom/google/android/location/os/bi;->a(Lcom/google/android/location/collectionlib/cg;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 215
    iget-object v2, p0, Lcom/google/android/location/ay;->j:Ljava/util/Set;

    sget-object v3, Lcom/google/android/location/collectionlib/cg;->i:Lcom/google/android/location/collectionlib/cg;

    invoke-interface {v2, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 219
    :cond_1
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x16

    if-lt v2, v3, :cond_2

    .line 220
    iget-object v2, p0, Lcom/google/android/location/ay;->j:Ljava/util/Set;

    sget-object v3, Lcom/google/android/location/collectionlib/cg;->p:Lcom/google/android/location/collectionlib/cg;

    invoke-interface {v2, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 221
    iget-object v2, p0, Lcom/google/android/location/ay;->j:Ljava/util/Set;

    sget-object v3, Lcom/google/android/location/collectionlib/cg;->q:Lcom/google/android/location/collectionlib/cg;

    invoke-interface {v2, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 223
    :cond_2
    return-void

    .line 209
    :cond_3
    sget-object v2, Lcom/google/android/location/collectionlib/cg;->f:Lcom/google/android/location/collectionlib/cg;

    invoke-interface {p1, v2}, Lcom/google/android/location/os/bi;->a(Lcom/google/android/location/collectionlib/cg;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 210
    iget-object v2, p0, Lcom/google/android/location/ay;->j:Ljava/util/Set;

    sget-object v3, Lcom/google/android/location/collectionlib/cg;->f:Lcom/google/android/location/collectionlib/cg;

    invoke-interface {v2, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method private a(JJJIZ)V
    .locals 15

    .prologue
    .line 621
    iget-object v2, p0, Lcom/google/android/location/ay;->b:Lcom/google/android/location/os/bi;

    invoke-interface {v2}, Lcom/google/android/location/os/bi;->c()Lcom/google/android/location/j/b;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/location/j/b;->d()J

    move-result-wide v2

    .line 622
    add-long v4, p1, v2

    .line 623
    add-long v6, p5, v2

    .line 624
    iget-object v3, p0, Lcom/google/android/location/ay;->w:Lcom/google/android/location/v;

    iget-object v2, p0, Lcom/google/android/location/ay;->m:Lcom/google/android/location/g;

    iget-object v8, v3, Lcom/google/android/location/v;->a:Ljava/lang/Object;

    monitor-enter v8

    :try_start_0
    invoke-static {v4, v5, v6, v7}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v10

    invoke-static {v4, v5, v6, v7}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v12

    new-instance v9, Lcom/google/p/a/b/b/a;

    sget-object v14, Lcom/google/android/location/m/a;->bC:Lcom/google/p/a/b/b/c;

    invoke-direct {v9, v14}, Lcom/google/p/a/b/b/a;-><init>(Lcom/google/p/a/b/b/c;)V

    const/4 v14, 0x1

    invoke-virtual {v9, v14, v10, v11}, Lcom/google/p/a/b/b/a;->b(IJ)Lcom/google/p/a/b/b/a;

    const/4 v10, 0x2

    invoke-virtual {v9, v10, v12, v13}, Lcom/google/p/a/b/b/a;->b(IJ)Lcom/google/p/a/b/b/a;

    const/4 v10, 0x4

    move/from16 v0, p7

    invoke-virtual {v9, v10, v0}, Lcom/google/p/a/b/b/a;->f(II)Lcom/google/p/a/b/b/a;

    if-eqz v2, :cond_0

    const/4 v10, 0x5

    sget-object v11, Lcom/google/android/location/g;->b:Lcom/google/android/location/g;

    if-ne v2, v11, :cond_4

    const/4 v2, 0x2

    :goto_0
    invoke-virtual {v9, v10, v2}, Lcom/google/p/a/b/b/a;->f(II)Lcom/google/p/a/b/b/a;

    :cond_0
    iget-object v2, v3, Lcom/google/android/location/v;->c:Lcom/google/p/a/b/b/a;

    const/4 v10, 0x1

    invoke-virtual {v2, v10, v9}, Lcom/google/p/a/b/b/a;->a(ILcom/google/p/a/b/b/a;)V

    invoke-virtual {v3}, Lcom/google/android/location/v;->h()V

    iget-object v9, v3, Lcom/google/android/location/v;->a:Ljava/lang/Object;

    monitor-enter v9
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    :try_start_1
    iget-object v2, v3, Lcom/google/android/location/v;->c:Lcom/google/p/a/b/b/a;

    const/4 v10, 0x2

    invoke-static {v2, v10}, Lcom/google/android/location/o/j;->a(Lcom/google/p/a/b/b/a;I)V

    iget-object v2, v3, Lcom/google/android/location/v;->c:Lcom/google/p/a/b/b/a;

    const/16 v3, 0xb

    invoke-static {v2, v3}, Lcom/google/android/location/o/j;->a(Lcom/google/p/a/b/b/a;I)V

    monitor-exit v9
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    monitor-exit v8
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 625
    iget-object v2, p0, Lcom/google/android/location/ay;->w:Lcom/google/android/location/v;

    invoke-virtual {v2}, Lcom/google/android/location/v;->a()V

    .line 626
    sget-boolean v2, Lcom/google/android/location/i/a;->b:Z

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/google/android/location/ay;->a:Ljava/lang/String;

    const-string v3, "Collector exits with [%d], time [%s, %s]"

    const/4 v8, 0x3

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    invoke-static/range {p7 .. p7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v8, v9

    const/4 v9, 0x1

    new-instance v10, Ljava/util/Date;

    invoke-direct {v10, v4, v5}, Ljava/util/Date;-><init>(J)V

    aput-object v10, v8, v9

    const/4 v4, 0x2

    new-instance v5, Ljava/util/Date;

    invoke-direct {v5, v6, v7}, Ljava/util/Date;-><init>(J)V

    aput-object v5, v8, v4

    invoke-static {v3, v8}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/location/o/a/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 628
    :cond_1
    const-wide/16 v2, -0x1

    cmp-long v2, p3, v2

    if-eqz v2, :cond_3

    .line 629
    sub-long v2, p5, p3

    .line 630
    const-wide/16 v4, 0x0

    cmp-long v4, v2, v4

    if-lez v4, :cond_3

    .line 631
    iget-object v4, p0, Lcom/google/android/location/ay;->e:Lcom/google/android/location/k/a;

    iget-object v5, v4, Lcom/google/android/location/k/a;->f:Lcom/google/android/location/j/b;

    invoke-interface {v5}, Lcom/google/android/location/j/b;->c()J

    move-result-wide v6

    iget-object v5, v4, Lcom/google/android/location/k/a;->a:Lcom/google/android/location/k/b;

    iget-object v5, v5, Lcom/google/android/location/k/b;->d:Lcom/google/android/location/k/d;

    invoke-virtual {v5, v2, v3, v6, v7}, Lcom/google/android/location/k/d;->a(JJ)Z

    sget-boolean v2, Lcom/google/android/location/i/a;->b:Z

    if-eqz v2, :cond_2

    const-string v2, "CollectionPolicy"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v5, "gps tokens left for sensor collection: "

    invoke-direct {v3, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v5, v4, Lcom/google/android/location/k/a;->a:Lcom/google/android/location/k/b;

    iget-object v5, v5, Lcom/google/android/location/k/b;->d:Lcom/google/android/location/k/d;

    iget-wide v8, v5, Lcom/google/android/location/k/d;->e:J

    invoke-virtual {v3, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/location/o/a/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_2
    iget-object v2, v4, Lcom/google/android/location/k/a;->a:Lcom/google/android/location/k/b;

    invoke-virtual {v2, v6, v7}, Lcom/google/android/location/k/b;->a(J)V

    .line 634
    :cond_3
    move/from16 v0, p8

    invoke-direct {p0, v0}, Lcom/google/android/location/ay;->g(Z)V

    .line 635
    return-void

    .line 624
    :cond_4
    const/4 v2, 0x1

    goto/16 :goto_0

    :catchall_0
    move-exception v2

    :try_start_3
    monitor-exit v9

    throw v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    :catchall_1
    move-exception v2

    monitor-exit v8

    throw v2
.end method

.method static synthetic a(Lcom/google/android/location/ay;JJJI)V
    .locals 11

    .prologue
    .line 63
    const/4 v9, 0x1

    move-object v1, p0

    move-wide v2, p1

    move-wide v4, p3

    move-wide/from16 v6, p5

    move/from16 v8, p7

    invoke-direct/range {v1 .. v9}, Lcom/google/android/location/ay;->a(JJJIZ)V

    return-void
.end method

.method private a(Ljava/util/Calendar;J)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 503
    invoke-direct {p0, p2, p3}, Lcom/google/android/location/ay;->h(J)Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/google/android/location/ay;->d(Z)Lcom/google/android/location/e/aj;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/location/e/aj;->a:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Boolean;

    .line 504
    iget-object v1, p0, Lcom/google/android/location/ay;->A:Lcom/google/android/location/at;

    iget-wide v2, p0, Lcom/google/android/location/ay;->v:J

    invoke-virtual {v1, p1, v2, v3}, Lcom/google/android/location/at;->a(Ljava/util/Calendar;J)Lcom/google/android/location/au;

    move-result-object v1

    iget-boolean v1, v1, Lcom/google/android/location/au;->a:Z

    .line 506
    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1

    if-eqz v1, :cond_1

    iget-object v0, p0, Lcom/google/android/location/ay;->h:Lcom/google/android/location/e;

    sget-object v2, Lcom/google/android/location/e;->b:Lcom/google/android/location/e;

    if-ne v0, v2, :cond_1

    .line 507
    invoke-direct {p0, v5}, Lcom/google/android/location/ay;->e(Z)V

    .line 508
    iget-object v0, p0, Lcom/google/android/location/ay;->A:Lcom/google/android/location/at;

    iget-wide v2, p0, Lcom/google/android/location/ay;->v:J

    invoke-virtual {v0, p1, v2, v3, v4}, Lcom/google/android/location/at;->a(Ljava/util/Calendar;JZ)J

    move-result-wide v0

    invoke-direct {p0, v0, v1}, Lcom/google/android/location/ay;->i(J)V

    .line 524
    :cond_0
    :goto_0
    return-void

    .line 510
    :cond_1
    invoke-direct {p0, v4}, Lcom/google/android/location/ay;->e(Z)V

    .line 512
    invoke-direct {p0}, Lcom/google/android/location/ay;->i()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 515
    if-eqz v1, :cond_2

    .line 516
    iget-object v0, p0, Lcom/google/android/location/ay;->A:Lcom/google/android/location/at;

    iget-wide v2, p0, Lcom/google/android/location/ay;->v:J

    invoke-virtual {v0, p1, v2, v3, v4}, Lcom/google/android/location/at;->a(Ljava/util/Calendar;JZ)J

    move-result-wide v0

    invoke-direct {p0, v0, v1}, Lcom/google/android/location/ay;->i(J)V

    goto :goto_0

    .line 519
    :cond_2
    iget-object v0, p0, Lcom/google/android/location/ay;->A:Lcom/google/android/location/at;

    iget-wide v2, p0, Lcom/google/android/location/ay;->v:J

    invoke-virtual {v0, p1, v2, v3, v5}, Lcom/google/android/location/at;->a(Ljava/util/Calendar;JZ)J

    move-result-wide v0

    invoke-direct {p0, v0, v1}, Lcom/google/android/location/ay;->i(J)V

    goto :goto_0
.end method

.method private a(JZ)Z
    .locals 19

    .prologue
    .line 446
    new-instance v7, Ljava/util/HashMap;

    invoke-direct {v7}, Ljava/util/HashMap;-><init>()V

    .line 447
    sget-object v4, Lcom/google/android/location/collectionlib/cg;->e:Lcom/google/android/location/collectionlib/cg;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/location/ay;->w:Lcom/google/android/location/v;

    invoke-virtual {v5}, Lcom/google/android/location/v;->d()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v7, v4, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 448
    sget-object v4, Lcom/google/android/location/collectionlib/cg;->d:Lcom/google/android/location/collectionlib/cg;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/location/ay;->w:Lcom/google/android/location/v;

    invoke-virtual {v5}, Lcom/google/android/location/v;->e()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v7, v4, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 449
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/location/ay;->b:Lcom/google/android/location/os/bi;

    sget-object v5, Lcom/google/android/location/collectionlib/cg;->j:Lcom/google/android/location/collectionlib/cg;

    invoke-interface {v4, v5}, Lcom/google/android/location/os/bi;->a(Lcom/google/android/location/collectionlib/cg;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 450
    sget-object v4, Lcom/google/android/location/collectionlib/cg;->j:Lcom/google/android/location/collectionlib/cg;

    const/4 v5, 0x1

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v7, v4, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 456
    :cond_0
    :goto_0
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/location/ay;->b:Lcom/google/android/location/os/bi;

    sget-object v5, Lcom/google/android/location/collectionlib/cg;->i:Lcom/google/android/location/collectionlib/cg;

    invoke-interface {v4, v5}, Lcom/google/android/location/os/bi;->a(Lcom/google/android/location/collectionlib/cg;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 457
    sget-object v4, Lcom/google/android/location/collectionlib/cg;->i:Lcom/google/android/location/collectionlib/cg;

    const/4 v5, 0x3

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v7, v4, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 461
    :cond_1
    sget v4, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v5, 0x16

    if-ge v4, v5, :cond_5

    .line 462
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/location/ay;->j:Ljava/util/Set;

    .line 472
    :cond_2
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/location/ay;->b:Lcom/google/android/location/os/bi;

    const-wide/32 v8, 0x493e0

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/location/ay;->b:Lcom/google/android/location/os/bi;

    invoke-interface {v4}, Lcom/google/android/location/os/bi;->n()Ljava/io/File;

    move-result-object v4

    invoke-virtual {v4}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v10

    if-eqz p3, :cond_7

    const/16 v4, 0x12

    :goto_1
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/location/ay;->y:Lcom/google/android/location/s;

    iget-object v13, v4, Lcom/google/android/location/s;->n:Lcom/google/p/a/b/b/a;

    const/4 v14, 0x0

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/location/ay;->t:Lcom/google/android/location/collectionlib/cy;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/location/ay;->a:Ljava/lang/String;

    move-object/from16 v16, v0

    const/16 v17, 0x0

    move/from16 v12, p3

    invoke-interface/range {v5 .. v17}, Lcom/google/android/location/os/bi;->a(Ljava/util/Set;Ljava/util/Map;JLjava/lang/String;Ljava/lang/Integer;ZLcom/google/p/a/b/b/a;ZLcom/google/android/location/collectionlib/ar;Ljava/lang/String;Lcom/google/android/location/o/n;)Lcom/google/android/location/collectionlib/be;

    move-result-object v4

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/google/android/location/ay;->p:Lcom/google/android/location/collectionlib/be;

    .line 488
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/location/ay;->p:Lcom/google/android/location/collectionlib/be;

    if-eqz v4, :cond_8

    .line 489
    move-wide/from16 v0, p1

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/google/android/location/ay;->o:J

    .line 490
    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput-boolean v4, v0, Lcom/google/android/location/ay;->s:Z

    .line 491
    move/from16 v0, p3

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/google/android/location/ay;->q:Z

    .line 492
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/location/ay;->p:Lcom/google/android/location/collectionlib/be;

    invoke-interface {v4}, Lcom/google/android/location/collectionlib/be;->a()V

    .line 493
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/google/android/location/ay;->F:Z

    move-object/from16 v0, p0

    invoke-direct {v0, v4}, Lcom/google/android/location/ay;->f(Z)V

    .line 494
    sget-boolean v4, Lcom/google/android/location/i/a;->b:Z

    if-eqz v4, :cond_3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/location/ay;->a:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Signal collector started, isInsightCollection: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-boolean v6, v0, Lcom/google/android/location/ay;->q:Z

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/android/location/o/a/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 499
    :cond_3
    :goto_2
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/location/ay;->p:Lcom/google/android/location/collectionlib/be;

    if-eqz v4, :cond_9

    const/4 v4, 0x1

    :goto_3
    return v4

    .line 451
    :cond_4
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/location/ay;->b:Lcom/google/android/location/os/bi;

    sget-object v5, Lcom/google/android/location/collectionlib/cg;->f:Lcom/google/android/location/collectionlib/cg;

    invoke-interface {v4, v5}, Lcom/google/android/location/os/bi;->a(Lcom/google/android/location/collectionlib/cg;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 452
    sget-object v4, Lcom/google/android/location/collectionlib/cg;->f:Lcom/google/android/location/collectionlib/cg;

    const/4 v5, 0x1

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v7, v4, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_0

    .line 464
    :cond_5
    new-instance v6, Lcom/google/android/gms/common/util/g;

    invoke-direct {v6}, Lcom/google/android/gms/common/util/g;-><init>()V

    .line 465
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/location/ay;->j:Ljava/util/Set;

    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_6
    :goto_4
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/location/collectionlib/cg;

    .line 466
    invoke-static {v4}, Lcom/google/android/location/ay;->b(Lcom/google/android/location/collectionlib/cg;)Z

    move-result v8

    if-eqz v8, :cond_6

    .line 467
    invoke-interface {v6, v4}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_4

    .line 472
    :cond_7
    const/16 v4, 0xe

    goto/16 :goto_1

    .line 497
    :cond_8
    sget-boolean v4, Lcom/google/android/location/i/a;->b:Z

    if-eqz v4, :cond_3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/location/ay;->a:Ljava/lang/String;

    const-string v5, "Failed to create signal collector."

    invoke-static {v4, v5}, Lcom/google/android/location/o/a/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    .line 499
    :cond_9
    const/4 v4, 0x0

    goto :goto_3
.end method

.method static synthetic a(Lcom/google/android/location/collectionlib/cg;)Z
    .locals 1

    .prologue
    .line 63
    invoke-static {p0}, Lcom/google/android/location/ay;->b(Lcom/google/android/location/collectionlib/cg;)Z

    move-result v0

    return v0
.end method

.method private static b(Lcom/google/android/location/collectionlib/cg;)Z
    .locals 1

    .prologue
    .line 815
    sget-object v0, Lcom/google/android/location/collectionlib/cg;->p:Lcom/google/android/location/collectionlib/cg;

    if-ne p0, v0, :cond_0

    .line 816
    sget-object v0, Lcom/google/android/location/d/a;->z:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    .line 821
    :goto_0
    return v0

    .line 818
    :cond_0
    sget-object v0, Lcom/google/android/location/collectionlib/cg;->q:Lcom/google/android/location/collectionlib/cg;

    if-ne p0, v0, :cond_1

    .line 819
    sget-object v0, Lcom/google/android/location/d/a;->A:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    goto :goto_0

    .line 821
    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private d(Z)Lcom/google/android/location/e/aj;
    .locals 12

    .prologue
    const/4 v6, 0x1

    const/4 v8, 0x0

    .line 374
    iget-object v0, p0, Lcom/google/android/location/ay;->x:Lcom/google/android/location/av;

    invoke-virtual {v0, p1}, Lcom/google/android/location/av;->a(Z)Lcom/google/android/location/e/aj;

    move-result-object v9

    .line 375
    iget-object v0, p0, Lcom/google/android/location/ay;->b:Lcom/google/android/location/os/bi;

    invoke-interface {v0}, Lcom/google/android/location/os/bi;->h()Z

    move-result v10

    .line 376
    iget-object v0, p0, Lcom/google/android/location/ay;->y:Lcom/google/android/location/s;

    iget-object v0, v0, Lcom/google/android/location/s;->n:Lcom/google/p/a/b/b/a;

    if-eqz v0, :cond_1

    move v7, v6

    .line 377
    :goto_0
    iget-object v0, p0, Lcom/google/android/location/ay;->e:Lcom/google/android/location/k/a;

    iget-object v1, v0, Lcom/google/android/location/k/a;->a:Lcom/google/android/location/k/b;

    iget-object v1, v1, Lcom/google/android/location/k/b;->d:Lcom/google/android/location/k/d;

    const-wide/32 v2, 0x493e0

    iget-object v0, v0, Lcom/google/android/location/k/a;->f:Lcom/google/android/location/j/b;

    invoke-interface {v0}, Lcom/google/android/location/j/b;->c()J

    move-result-wide v4

    invoke-virtual/range {v1 .. v6}, Lcom/google/android/location/k/d;->a(JJZ)Z

    move-result v3

    .line 380
    iget-object v0, v9, Lcom/google/android/location/e/aj;->a:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-boolean v0, p0, Lcom/google/android/location/ay;->C:Z

    if-eqz v0, :cond_2

    if-eqz v10, :cond_2

    if-eqz v7, :cond_2

    if-eqz v3, :cond_2

    move v1, v6

    .line 384
    :goto_1
    if-eqz v1, :cond_d

    .line 386
    iget-object v0, p0, Lcom/google/android/location/ay;->b:Lcom/google/android/location/os/bi;

    invoke-interface {v0}, Lcom/google/android/location/os/bi;->n()Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v4

    if-eqz v4, :cond_3

    array-length v5, v4

    move v2, v8

    move v0, v8

    :goto_2
    if-ge v2, v5, :cond_4

    aget-object v11, v4, v2

    invoke-virtual {v11}, Ljava/io/File;->isDirectory()Z

    move-result v11

    if-eqz v11, :cond_0

    add-int/lit8 v0, v0, 0x1

    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    :cond_1
    move v7, v8

    .line 376
    goto :goto_0

    :cond_2
    move v1, v8

    .line 380
    goto :goto_1

    :cond_3
    move v0, v8

    .line 387
    :cond_4
    const/4 v2, 0x3

    if-lt v0, v2, :cond_5

    move v0, v6

    .line 388
    :goto_3
    if-eqz v1, :cond_6

    if-nez v0, :cond_6

    :goto_4
    move v8, v0

    .line 391
    :goto_5
    if-nez v6, :cond_c

    .line 402
    iget-object v0, v9, Lcom/google/android/location/e/aj;->a:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_7

    .line 403
    iget-object v0, v9, Lcom/google/android/location/e/aj;->b:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    .line 418
    :goto_6
    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/google/android/location/e/aj;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/android/location/e/aj;

    move-result-object v0

    return-object v0

    :cond_5
    move v0, v8

    .line 387
    goto :goto_3

    :cond_6
    move v6, v8

    .line 388
    goto :goto_4

    .line 404
    :cond_7
    if-nez v10, :cond_8

    .line 405
    const/16 v0, 0x18

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_6

    .line 406
    :cond_8
    if-nez v7, :cond_9

    .line 407
    const/16 v0, 0x1a

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_6

    .line 408
    :cond_9
    if-nez v3, :cond_a

    .line 409
    const/16 v0, 0x19

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_6

    .line 410
    :cond_a
    if-eqz v8, :cond_b

    .line 411
    const/16 v0, 0x1b

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_6

    .line 413
    :cond_b
    const/16 v0, 0x63

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_6

    .line 416
    :cond_c
    const/4 v0, 0x0

    goto :goto_6

    :cond_d
    move v6, v1

    goto :goto_5
.end method

.method private e(Z)V
    .locals 3

    .prologue
    const/16 v2, 0xb4

    .line 527
    iget-boolean v0, p0, Lcom/google/android/location/ay;->n:Z

    if-eq p1, v0, :cond_0

    .line 528
    if-eqz p1, :cond_1

    .line 529
    iget-object v0, p0, Lcom/google/android/location/ay;->z:Lcom/google/android/location/activity/k;

    const/4 v1, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/google/android/location/activity/k;->a(IZ)V

    .line 535
    :goto_0
    iput-boolean p1, p0, Lcom/google/android/location/ay;->n:Z

    .line 537
    :cond_0
    return-void

    .line 532
    :cond_1
    iget-object v0, p0, Lcom/google/android/location/ay;->z:Lcom/google/android/location/activity/k;

    invoke-virtual {v0, v2}, Lcom/google/android/location/activity/k;->a(I)V

    goto :goto_0
.end method

.method private f(Z)V
    .locals 7

    .prologue
    const/4 v6, 0x1

    .line 558
    iget-object v0, p0, Lcom/google/android/location/ay;->p:Lcom/google/android/location/collectionlib/be;

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/google/android/location/ay;->q:Z

    if-eqz v0, :cond_1

    .line 559
    iget-object v0, p0, Lcom/google/android/location/ay;->r:Lcom/google/p/a/b/b/a;

    if-nez v0, :cond_0

    .line 560
    new-instance v0, Lcom/google/p/a/b/b/a;

    sget-object v1, Lcom/google/android/location/m/a;->e:Lcom/google/p/a/b/b/c;

    invoke-direct {v0, v1}, Lcom/google/p/a/b/b/a;-><init>(Lcom/google/p/a/b/b/c;)V

    iput-object v0, p0, Lcom/google/android/location/ay;->r:Lcom/google/p/a/b/b/a;

    .line 562
    :cond_0
    new-instance v0, Lcom/google/p/a/b/b/a;

    sget-object v1, Lcom/google/android/location/m/a;->d:Lcom/google/p/a/b/b/c;

    invoke-direct {v0, v1}, Lcom/google/p/a/b/b/a;-><init>(Lcom/google/p/a/b/b/c;)V

    .line 563
    invoke-virtual {v0, v6, p1}, Lcom/google/p/a/b/b/a;->a(IZ)Lcom/google/p/a/b/b/a;

    .line 564
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/location/ay;->b:Lcom/google/android/location/os/bi;

    invoke-interface {v2}, Lcom/google/android/location/os/bi;->c()Lcom/google/android/location/j/b;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/location/j/b;->c()J

    move-result-wide v2

    iget-wide v4, p0, Lcom/google/android/location/ay;->o:J

    sub-long/2addr v2, v4

    long-to-int v2, v2

    invoke-virtual {v0, v1, v2}, Lcom/google/p/a/b/b/a;->f(II)Lcom/google/p/a/b/b/a;

    .line 566
    iget-object v1, p0, Lcom/google/android/location/ay;->r:Lcom/google/p/a/b/b/a;

    invoke-virtual {v1, v6, v0}, Lcom/google/p/a/b/b/a;->a(ILcom/google/p/a/b/b/a;)V

    .line 568
    :cond_1
    return-void
.end method

.method private g(Z)V
    .locals 5

    .prologue
    const/4 v2, 0x0

    const/4 v4, 0x0

    .line 639
    iput-object v4, p0, Lcom/google/android/location/ay;->p:Lcom/google/android/location/collectionlib/be;

    .line 640
    iput-boolean v2, p0, Lcom/google/android/location/ay;->s:Z

    .line 641
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/google/android/location/ay;->o:J

    .line 642
    iput-boolean v2, p0, Lcom/google/android/location/ay;->q:Z

    .line 643
    iput-object v4, p0, Lcom/google/android/location/ay;->r:Lcom/google/p/a/b/b/a;

    .line 644
    iput-object v4, p0, Lcom/google/android/location/ay;->D:Ljava/lang/Integer;

    .line 645
    iput-object v4, p0, Lcom/google/android/location/ay;->E:Lcom/google/android/location/o/f;

    .line 646
    if-eqz p1, :cond_0

    .line 647
    iget-object v0, p0, Lcom/google/android/location/ay;->b:Lcom/google/android/location/os/bi;

    invoke-interface {v0}, Lcom/google/android/location/os/bi;->f()Lcom/google/android/location/j/j;

    move-result-object v0

    sget-object v1, Lcom/google/android/location/j/k;->g:Lcom/google/android/location/j/k;

    const-wide/16 v2, 0x0

    invoke-interface {v0, v1, v2, v3, v4}, Lcom/google/android/location/j/j;->a(Lcom/google/android/location/j/k;JLcom/google/android/location/o/n;)V

    .line 649
    :cond_0
    return-void
.end method

.method private h(J)Z
    .locals 5

    .prologue
    .line 291
    iget-wide v0, p0, Lcom/google/android/location/ay;->v:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    iget-wide v0, p0, Lcom/google/android/location/ay;->v:J

    sub-long v0, p1, v0

    const-wide/32 v2, 0x1d4c0

    cmp-long v0, v0, v2

    if-gez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private i(J)V
    .locals 9

    .prologue
    .line 336
    iget-wide v0, p0, Lcom/google/android/location/ay;->l:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    iget-wide v0, p0, Lcom/google/android/location/ay;->l:J

    sub-long v0, p1, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->abs(J)J

    move-result-wide v0

    const-wide/16 v2, 0x3e8

    cmp-long v0, v0, v2

    if-lez v0, :cond_1

    .line 338
    :cond_0
    iput-wide p1, p0, Lcom/google/android/location/ay;->l:J

    .line 339
    iget-object v0, p0, Lcom/google/android/location/ay;->b:Lcom/google/android/location/os/bi;

    invoke-interface {v0}, Lcom/google/android/location/os/bi;->f()Lcom/google/android/location/j/j;

    move-result-object v0

    sget-object v1, Lcom/google/android/location/j/k;->g:Lcom/google/android/location/j/k;

    const/4 v2, 0x0

    invoke-interface {v0, v1, p1, p2, v2}, Lcom/google/android/location/j/j;->a(Lcom/google/android/location/j/k;JLcom/google/android/location/o/n;)V

    .line 340
    sget-boolean v0, Lcom/google/android/location/i/a;->b:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/location/ay;->a:Ljava/lang/String;

    const-string v1, "Alarm scheduled at %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    new-instance v4, Ljava/util/Date;

    iget-object v5, p0, Lcom/google/android/location/ay;->b:Lcom/google/android/location/os/bi;

    invoke-interface {v5}, Lcom/google/android/location/os/bi;->c()Lcom/google/android/location/j/b;

    move-result-object v5

    invoke-interface {v5}, Lcom/google/android/location/j/b;->d()J

    move-result-wide v6

    add-long/2addr v6, p1

    invoke-direct {v4, v6, v7}, Ljava/util/Date;-><init>(J)V

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/location/o/a/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 343
    :cond_1
    return-void
.end method

.method private i()Z
    .locals 4

    .prologue
    .line 356
    iget-object v0, p0, Lcom/google/android/location/ay;->x:Lcom/google/android/location/av;

    invoke-virtual {v0}, Lcom/google/android/location/av;->c()Z

    move-result v0

    .line 357
    if-nez v0, :cond_0

    .line 358
    sget-boolean v1, Lcom/google/android/location/i/a;->b:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/location/ay;->a:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "cantSchedule: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/google/android/location/ay;->x:Lcom/google/android/location/av;

    invoke-virtual {v3}, Lcom/google/android/location/av;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/location/o/a/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 360
    :cond_0
    return v0
.end method

.method private j()Ljava/util/Calendar;
    .locals 4

    .prologue
    .line 540
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 542
    iget-object v1, p0, Lcom/google/android/location/ay;->b:Lcom/google/android/location/os/bi;

    invoke-interface {v1}, Lcom/google/android/location/os/bi;->c()Lcom/google/android/location/j/b;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/location/j/b;->b()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 543
    return-object v0
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/location/ActivityRecognitionResult;)V
    .locals 3

    .prologue
    .line 774
    sget-boolean v0, Lcom/google/android/location/i/a;->b:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/location/ay;->a:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Detected activity: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/location/o/a/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 775
    :cond_0
    invoke-virtual {p1}, Lcom/google/android/gms/location/ActivityRecognitionResult;->a()Lcom/google/android/gms/location/DetectedActivity;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/location/DetectedActivity;->a()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    .line 776
    iget-object v0, p0, Lcom/google/android/location/ay;->b:Lcom/google/android/location/os/bi;

    invoke-interface {v0}, Lcom/google/android/location/os/bi;->c()Lcom/google/android/location/j/b;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/location/j/b;->c()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/location/ay;->u:J

    .line 778
    :cond_1
    return-void
.end method

.method public final a(Lcom/google/android/location/e/bi;)V
    .locals 8

    .prologue
    const/4 v4, 0x1

    const/4 v2, 0x0

    .line 783
    invoke-super {p0, p1}, Lcom/google/android/location/a;->a(Lcom/google/android/location/e/bi;)V

    .line 785
    iget-object v0, p0, Lcom/google/android/location/ay;->h:Lcom/google/android/location/e;

    sget-object v1, Lcom/google/android/location/e;->b:Lcom/google/android/location/e;

    if-ne v0, v1, :cond_0

    if-nez p1, :cond_1

    .line 799
    :cond_0
    :goto_0
    return-void

    .line 789
    :cond_1
    iget-object v0, p0, Lcom/google/android/location/ay;->g:Lcom/google/android/location/d;

    iget-object v5, v0, Lcom/google/android/location/d;->d:Lcom/google/android/location/b/ao;

    move v1, v2

    move v3, v2

    .line 790
    :goto_1
    invoke-virtual {p1}, Lcom/google/android/location/e/bi;->a()I

    move-result v0

    if-ge v1, v0, :cond_5

    .line 791
    invoke-virtual {p1, v1}, Lcom/google/android/location/e/bi;->a(I)Lcom/google/android/location/e/bc;

    move-result-object v0

    iget-wide v6, v0, Lcom/google/android/location/e/bc;->b:J

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iget-object v6, v5, Lcom/google/android/location/b/ao;->b:Lcom/google/android/location/b/ap;

    invoke-virtual {v6, v0}, Lcom/google/android/location/b/ap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/b/aq;

    if-nez v0, :cond_3

    const/high16 v0, -0x40800000    # -1.0f

    :goto_2
    const/4 v6, 0x0

    cmpl-float v6, v0, v6

    if-lez v6, :cond_4

    iget-object v6, p0, Lcom/google/android/location/ay;->B:Ljava/util/Random;

    invoke-virtual {v6}, Ljava/util/Random;->nextFloat()F

    move-result v6

    cmpg-float v0, v6, v0

    if-gez v0, :cond_4

    move v0, v4

    :goto_3
    if-eqz v0, :cond_2

    .line 792
    add-int/lit8 v3, v3, 0x1

    .line 790
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 791
    :cond_3
    iget v0, v0, Lcom/google/android/location/b/aq;->c:F

    goto :goto_2

    :cond_4
    move v0, v2

    goto :goto_3

    .line 795
    :cond_5
    int-to-long v0, v3

    const-wide/16 v6, 0x3

    cmp-long v0, v0, v6

    if-ltz v0, :cond_0

    .line 796
    sget-boolean v0, Lcom/google/android/location/i/a;->b:Z

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/google/android/location/ay;->a:Ljava/lang/String;

    const-string v1, "Got %d votes for sensor collection."

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v4, v2

    invoke-static {v1, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/location/o/a/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 797
    :cond_6
    iget-wide v0, p1, Lcom/google/android/location/e/bi;->a:J

    iput-wide v0, p0, Lcom/google/android/location/ay;->v:J

    goto :goto_0
.end method

.method final a(Lcom/google/android/location/e/h;)V
    .locals 1

    .prologue
    .line 827
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/location/ay;->G:Z

    .line 828
    return-void
.end method

.method public final a(Lcom/google/android/location/j/k;)V
    .locals 4

    .prologue
    .line 322
    sget-object v0, Lcom/google/android/location/j/k;->g:Lcom/google/android/location/j/k;

    if-ne p1, v0, :cond_0

    .line 323
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/google/android/location/ay;->l:J

    .line 324
    invoke-direct {p0}, Lcom/google/android/location/ay;->j()Ljava/util/Calendar;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/location/ay;->b:Lcom/google/android/location/os/bi;

    invoke-interface {v1}, Lcom/google/android/location/os/bi;->c()Lcom/google/android/location/j/b;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/location/j/b;->c()J

    move-result-wide v2

    invoke-direct {p0, v0, v2, v3}, Lcom/google/android/location/ay;->a(Ljava/util/Calendar;J)V

    .line 326
    :cond_0
    return-void
.end method

.method public final a(Z)V
    .locals 0

    .prologue
    .line 228
    iput-boolean p1, p0, Lcom/google/android/location/ay;->C:Z

    .line 229
    return-void
.end method

.method final b(Z)V
    .locals 0

    .prologue
    .line 548
    invoke-direct {p0, p1}, Lcom/google/android/location/ay;->f(Z)V

    .line 549
    iput-boolean p1, p0, Lcom/google/android/location/ay;->F:Z

    .line 550
    return-void
.end method

.method public final b(J)Z
    .locals 13

    .prologue
    const/4 v10, 0x1

    const-wide/16 v4, -0x1

    const/4 v9, 0x0

    .line 238
    iget-boolean v0, p0, Lcom/google/android/location/ay;->G:Z

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/location/ay;->i()Z

    move-result v0

    if-nez v0, :cond_2

    .line 239
    :cond_0
    iput-boolean v9, p0, Lcom/google/android/location/ay;->G:Z

    .line 284
    :cond_1
    :goto_0
    return v9

    .line 243
    :cond_2
    invoke-direct {p0}, Lcom/google/android/location/ay;->j()Ljava/util/Calendar;

    move-result-object v0

    .line 247
    invoke-direct {p0, p1, p2}, Lcom/google/android/location/ay;->h(J)Z

    move-result v11

    .line 248
    iget-wide v2, p0, Lcom/google/android/location/ay;->l:J

    cmp-long v1, v2, v4

    if-eqz v1, :cond_3

    if-eqz v11, :cond_4

    iget-boolean v1, p0, Lcom/google/android/location/ay;->n:Z

    if-nez v1, :cond_4

    .line 250
    :cond_3
    invoke-direct {p0, v0, p1, p2}, Lcom/google/android/location/ay;->a(Ljava/util/Calendar;J)V

    .line 252
    :cond_4
    iget-object v1, p0, Lcom/google/android/location/ay;->A:Lcom/google/android/location/at;

    iget-wide v2, p0, Lcom/google/android/location/ay;->v:J

    invoke-virtual {v1, v0, v2, v3}, Lcom/google/android/location/at;->a(Ljava/util/Calendar;J)Lcom/google/android/location/au;

    move-result-object v12

    .line 254
    iget-object v0, v12, Lcom/google/android/location/au;->c:Lcom/google/android/location/o/f;

    if-nez v0, :cond_b

    const/4 v0, 0x0

    :goto_1
    iget-object v1, p0, Lcom/google/android/location/ay;->E:Lcom/google/android/location/o/f;

    if-eqz v1, :cond_6

    iget-object v1, p0, Lcom/google/android/location/ay;->E:Lcom/google/android/location/o/f;

    invoke-virtual {v1, v0}, Lcom/google/android/location/o/f;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_6

    iget-object v1, p0, Lcom/google/android/location/ay;->D:Ljava/lang/Integer;

    if-eqz v1, :cond_e

    sget-boolean v1, Lcom/google/android/location/i/a;->b:Z

    if-eqz v1, :cond_5

    iget-object v1, p0, Lcom/google/android/location/ay;->a:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unable to do collection in the last timespan. Reporting the failure: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/google/android/location/ay;->D:Ljava/lang/Integer;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/location/o/a/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_5
    iget-object v1, p0, Lcom/google/android/location/ay;->D:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v8

    move-object v1, p0

    move-wide v2, p1

    move-wide v6, p1

    invoke-direct/range {v1 .. v9}, Lcom/google/android/location/ay;->a(JJJIZ)V

    :cond_6
    :goto_2
    iput-object v0, p0, Lcom/google/android/location/ay;->E:Lcom/google/android/location/o/f;

    .line 255
    iget-boolean v0, v12, Lcom/google/android/location/au;->a:Z

    if-eqz v0, :cond_a

    .line 257
    iget-object v0, v12, Lcom/google/android/location/au;->b:Lcom/google/android/location/g;

    iput-object v0, p0, Lcom/google/android/location/ay;->m:Lcom/google/android/location/g;

    .line 260
    invoke-direct {p0, v11}, Lcom/google/android/location/ay;->d(Z)Lcom/google/android/location/e/aj;

    move-result-object v1

    .line 261
    iget-object v0, v1, Lcom/google/android/location/e/aj;->a:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_11

    .line 263
    iget-wide v0, p0, Lcom/google/android/location/ay;->u:J

    cmp-long v0, v0, v4

    if-eqz v0, :cond_f

    iget-wide v0, p0, Lcom/google/android/location/ay;->u:J

    sub-long v0, p1, v0

    const-wide/16 v2, 0x7530

    cmp-long v0, v0, v2

    if-gez v0, :cond_f

    move v0, v10

    .line 265
    :goto_3
    if-eqz v0, :cond_10

    .line 267
    sget-boolean v0, Lcom/google/android/location/i/a;->b:Z

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/google/android/location/ay;->a:Ljava/lang/String;

    const-string v1, "On foot detected. Starting collection."

    invoke-static {v0, v1}, Lcom/google/android/location/o/a/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 268
    :cond_7
    iget-wide v0, p0, Lcom/google/android/location/ay;->l:J

    cmp-long v0, v0, v4

    if-eqz v0, :cond_8

    iput-wide v4, p0, Lcom/google/android/location/ay;->l:J

    iget-object v0, p0, Lcom/google/android/location/ay;->b:Lcom/google/android/location/os/bi;

    invoke-interface {v0}, Lcom/google/android/location/os/bi;->f()Lcom/google/android/location/j/j;

    move-result-object v0

    sget-object v1, Lcom/google/android/location/j/k;->g:Lcom/google/android/location/j/k;

    invoke-interface {v0, v1}, Lcom/google/android/location/j/j;->b(Lcom/google/android/location/j/k;)V

    .line 269
    :cond_8
    sget-object v0, Lcom/google/android/location/e;->h:Lcom/google/android/location/e;

    iput-object v0, p0, Lcom/google/android/location/ay;->h:Lcom/google/android/location/e;

    .line 270
    invoke-direct {p0, v9}, Lcom/google/android/location/ay;->e(Z)V

    .line 271
    invoke-direct {p0, p1, p2, v11}, Lcom/google/android/location/ay;->a(JZ)Z

    move-result v0

    if-nez v0, :cond_a

    .line 272
    sget-boolean v0, Lcom/google/android/location/i/a;->b:Z

    if-eqz v0, :cond_9

    iget-object v0, p0, Lcom/google/android/location/ay;->a:Ljava/lang/String;

    const-string v1, "Unable to start collection"

    invoke-static {v0, v1}, Lcom/google/android/location/o/a/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 273
    :cond_9
    sget-object v0, Lcom/google/android/location/e;->b:Lcom/google/android/location/e;

    iput-object v0, p0, Lcom/google/android/location/ay;->h:Lcom/google/android/location/e;

    .line 274
    const/16 v8, 0x1d

    move-object v1, p0

    move-wide v2, p1

    move-wide v6, p1

    invoke-direct/range {v1 .. v9}, Lcom/google/android/location/ay;->a(JJJIZ)V

    .line 284
    :cond_a
    :goto_4
    iget-object v0, p0, Lcom/google/android/location/ay;->h:Lcom/google/android/location/e;

    sget-object v1, Lcom/google/android/location/e;->b:Lcom/google/android/location/e;

    if-eq v0, v1, :cond_1

    move v9, v10

    goto/16 :goto_0

    .line 254
    :cond_b
    iget-object v0, p0, Lcom/google/android/location/ay;->A:Lcom/google/android/location/at;

    iget-object v1, v12, Lcom/google/android/location/au;->c:Lcom/google/android/location/o/f;

    iget-object v0, v0, Lcom/google/android/location/at;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_c
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_d

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/o/f;

    iget-wide v6, v1, Lcom/google/android/location/o/f;->a:J

    invoke-virtual {v0, v6, v7}, Lcom/google/android/location/o/f;->b(J)Z

    move-result v3

    if-eqz v3, :cond_c

    goto/16 :goto_1

    :cond_d
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Did not find parent of subtimespan: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_e
    sget-boolean v1, Lcom/google/android/location/i/a;->b:Z

    if-eqz v1, :cond_6

    iget-object v1, p0, Lcom/google/android/location/ay;->a:Ljava/lang/String;

    const-string v2, "Unable to collect in the last timespan, but the failure reason is null"

    invoke-static {v1, v2}, Lcom/google/android/location/o/a/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2

    :cond_f
    move v0, v9

    .line 263
    goto/16 :goto_3

    .line 278
    :cond_10
    const/16 v0, 0x15

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/ay;->D:Ljava/lang/Integer;

    goto :goto_4

    .line 281
    :cond_11
    iget-object v0, v1, Lcom/google/android/location/e/aj;->b:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    iput-object v0, p0, Lcom/google/android/location/ay;->D:Ljava/lang/Integer;

    goto :goto_4
.end method

.method public final c()Z
    .locals 4

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 573
    iget-object v0, p0, Lcom/google/android/location/ay;->p:Lcom/google/android/location/collectionlib/be;

    if-nez v0, :cond_0

    .line 576
    sget-object v0, Lcom/google/android/location/e;->b:Lcom/google/android/location/e;

    iput-object v0, p0, Lcom/google/android/location/ay;->h:Lcom/google/android/location/e;

    move v0, v1

    .line 604
    :goto_0
    return v0

    .line 580
    :cond_0
    iget-object v0, p0, Lcom/google/android/location/ay;->x:Lcom/google/android/location/av;

    iget-boolean v3, p0, Lcom/google/android/location/ay;->q:Z

    invoke-virtual {v0, v3}, Lcom/google/android/location/av;->a(Z)Lcom/google/android/location/e/aj;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/location/e/aj;->a:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_5

    .line 582
    sget-boolean v0, Lcom/google/android/location/i/a;->b:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/location/ay;->a:Ljava/lang/String;

    const-string v3, "Bad device conditions, terminated early."

    invoke-static {v0, v3}, Lcom/google/android/location/o/a/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 583
    :cond_1
    iput-boolean v1, p0, Lcom/google/android/location/ay;->s:Z

    .line 586
    iget-object v0, p0, Lcom/google/android/location/ay;->x:Lcom/google/android/location/av;

    invoke-virtual {v0}, Lcom/google/android/location/av;->d()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-boolean v0, p0, Lcom/google/android/location/ay;->f:Z

    if-nez v0, :cond_3

    :cond_2
    move v0, v1

    .line 587
    :goto_1
    if-eqz v0, :cond_4

    .line 590
    iget-object v0, p0, Lcom/google/android/location/ay;->p:Lcom/google/android/location/collectionlib/be;

    invoke-interface {v0}, Lcom/google/android/location/collectionlib/be;->c()V

    .line 594
    invoke-direct {p0, v2}, Lcom/google/android/location/ay;->g(Z)V

    .line 595
    sget-object v0, Lcom/google/android/location/e;->b:Lcom/google/android/location/e;

    iput-object v0, p0, Lcom/google/android/location/ay;->h:Lcom/google/android/location/e;

    move v0, v1

    .line 596
    goto :goto_0

    :cond_3
    move v0, v2

    .line 586
    goto :goto_1

    .line 601
    :cond_4
    iget-object v0, p0, Lcom/google/android/location/ay;->p:Lcom/google/android/location/collectionlib/be;

    invoke-interface {v0}, Lcom/google/android/location/collectionlib/be;->b()V

    :cond_5
    move v0, v2

    .line 604
    goto :goto_0
.end method

.method public final g()V
    .locals 4

    .prologue
    .line 330
    invoke-super {p0}, Lcom/google/android/location/a;->g()V

    .line 331
    sget-boolean v0, Lcom/google/android/location/i/a;->b:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/location/ay;->a:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Sensor policy changed: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/location/ay;->x:Lcom/google/android/location/av;

    invoke-virtual {v2}, Lcom/google/android/location/av;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/location/o/a/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 332
    :cond_0
    invoke-direct {p0}, Lcom/google/android/location/ay;->j()Ljava/util/Calendar;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/location/ay;->b:Lcom/google/android/location/os/bi;

    invoke-interface {v1}, Lcom/google/android/location/os/bi;->c()Lcom/google/android/location/j/b;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/location/j/b;->c()J

    move-result-wide v2

    invoke-direct {p0, v0, v2, v3}, Lcom/google/android/location/ay;->a(Ljava/util/Calendar;J)V

    .line 333
    return-void
.end method
