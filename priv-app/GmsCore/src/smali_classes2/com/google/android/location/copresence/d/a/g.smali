.class final Lcom/google/android/location/copresence/d/a/g;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/location/copresence/d/a/k;


# instance fields
.field final synthetic a:Landroid/bluetooth/BluetoothDevice;

.field final synthetic b:I

.field final synthetic c:Lcom/google/android/location/copresence/d/a/d;

.field final synthetic d:Lcom/google/android/location/copresence/d/a/f;


# direct methods
.method constructor <init>(Lcom/google/android/location/copresence/d/a/f;Landroid/bluetooth/BluetoothDevice;ILcom/google/android/location/copresence/d/a/d;)V
    .locals 0

    .prologue
    .line 39
    iput-object p1, p0, Lcom/google/android/location/copresence/d/a/g;->d:Lcom/google/android/location/copresence/d/a/f;

    iput-object p2, p0, Lcom/google/android/location/copresence/d/a/g;->a:Landroid/bluetooth/BluetoothDevice;

    iput p3, p0, Lcom/google/android/location/copresence/d/a/g;->b:I

    iput-object p4, p0, Lcom/google/android/location/copresence/d/a/g;->c:Lcom/google/android/location/copresence/d/a/d;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Landroid/bluetooth/BluetoothDevice;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lcom/google/android/location/copresence/d/a/g;->a:Landroid/bluetooth/BluetoothDevice;

    return-object v0
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 48
    iget v0, p0, Lcom/google/android/location/copresence/d/a/g;->b:I

    return v0
.end method

.method public final c()Ljava/util/UUID;
    .locals 7

    .prologue
    const/4 v1, 0x0

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 53
    iget-object v0, p0, Lcom/google/android/location/copresence/d/a/g;->c:Lcom/google/android/location/copresence/d/a/d;

    iget-object v0, v0, Lcom/google/android/location/copresence/d/a/d;->a:Ljava/util/List;

    .line 54
    if-eqz v0, :cond_0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_1

    :cond_0
    move-object v0, v1

    .line 69
    :goto_0
    return-object v0

    .line 57
    :cond_1
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    if-le v2, v6, :cond_2

    .line 58
    const/4 v2, 0x3

    invoke-static {v2}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 59
    const-string v2, "BleScanImplJBMR2"

    new-array v3, v6, [Ljava/lang/Object;

    const-string v4, "Found more than one UUID."

    aput-object v4, v3, v5

    invoke-static {v2, v3}, Lcom/google/android/location/copresence/ag;->a(Ljava/lang/String;[Ljava/lang/Object;)I

    .line 62
    :cond_2
    invoke-interface {v0, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B

    .line 64
    const/4 v2, 0x2

    new-array v2, v2, [B

    aget-byte v3, v0, v6

    aput-byte v3, v2, v5

    aget-byte v0, v0, v5

    aput-byte v0, v2, v6

    invoke-static {v2}, Lcom/google/android/gms/blescanner/compat/b;->a([B)Landroid/os/ParcelUuid;

    move-result-object v0

    .line 66
    if-nez v0, :cond_3

    move-object v0, v1

    .line 67
    goto :goto_0

    .line 69
    :cond_3
    invoke-virtual {v0}, Landroid/os/ParcelUuid;->getUuid()Ljava/util/UUID;

    move-result-object v0

    goto :goto_0
.end method

.method public final d()Lcom/google/android/gms/location/copresence/x;
    .locals 14

    .prologue
    const-wide/16 v12, 0xff

    const/4 v2, 0x0

    const/4 v6, 0x1

    const/4 v7, 0x0

    .line 92
    invoke-virtual {p0}, Lcom/google/android/location/copresence/d/a/g;->c()Ljava/util/UUID;

    move-result-object v8

    if-nez v8, :cond_1

    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/google/android/location/copresence/d/a/g;->c:Lcom/google/android/location/copresence/d/a/d;

    iget-object v1, v0, Lcom/google/android/location/copresence/d/a/d;->d:[B

    if-nez v1, :cond_9

    iget-object v0, v0, Lcom/google/android/location/copresence/d/a/d;->c:[B

    :goto_1
    invoke-static {v2, v0}, Lcom/google/android/location/copresence/d/a/l;->a([B[B)Lcom/google/android/gms/location/copresence/x;

    move-result-object v0

    return-object v0

    :cond_1
    iget-object v0, p0, Lcom/google/android/location/copresence/d/a/g;->c:Lcom/google/android/location/copresence/d/a/d;

    iget-object v1, v0, Lcom/google/android/location/copresence/d/a/d;->b:Ljava/util/List;

    if-nez v1, :cond_4

    move-object v1, v2

    :cond_2
    if-eqz v1, :cond_0

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v0

    if-le v0, v6, :cond_3

    const/4 v0, 0x3

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v0

    if-eqz v0, :cond_3

    const-string v0, "BleScanImplJBMR2"

    new-array v2, v6, [Ljava/lang/Object;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Found more than one service data for uuid: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v7

    invoke-static {v0, v2}, Lcom/google/android/location/copresence/ag;->a(Ljava/lang/String;[Ljava/lang/Object;)I

    :cond_3
    invoke-interface {v1, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B

    move-object v2, v0

    goto :goto_0

    :cond_4
    iget-object v0, v0, Lcom/google/android/location/copresence/d/a/d;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v9

    move-object v1, v2

    :cond_5
    :goto_2
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/copresence/d/a/p;

    new-instance v3, Landroid/os/ParcelUuid;

    invoke-direct {v3, v8}, Landroid/os/ParcelUuid;-><init>(Ljava/util/UUID;)V

    invoke-static {v3}, Lcom/google/android/gms/blescanner/compat/b;->b(Landroid/os/ParcelUuid;)Z

    move-result v4

    if-eqz v4, :cond_7

    invoke-static {v3}, Lcom/google/android/gms/blescanner/compat/b;->a(Landroid/os/ParcelUuid;)I

    move-result v3

    int-to-long v4, v3

    :goto_3
    and-long v10, v4, v12

    long-to-int v3, v10

    int-to-byte v3, v3

    iget-object v10, v0, Lcom/google/android/location/copresence/d/a/p;->a:[B

    aget-byte v10, v10, v7

    if-ne v3, v10, :cond_8

    const/16 v3, 0x8

    shr-long/2addr v4, v3

    and-long/2addr v4, v12

    long-to-int v3, v4

    int-to-byte v3, v3

    iget-object v4, v0, Lcom/google/android/location/copresence/d/a/p;->a:[B

    aget-byte v4, v4, v6

    if-ne v3, v4, :cond_8

    move v3, v6

    :goto_4
    if-eqz v3, :cond_5

    if-nez v1, :cond_6

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    :cond_6
    iget-object v0, v0, Lcom/google/android/location/copresence/d/a/p;->b:[B

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    :cond_7
    invoke-virtual {v8}, Ljava/util/UUID;->getLeastSignificantBits()J

    move-result-wide v4

    goto :goto_3

    :cond_8
    move v3, v7

    goto :goto_4

    :cond_9
    iget-object v0, v0, Lcom/google/android/location/copresence/d/a/d;->d:[B

    goto/16 :goto_1
.end method
