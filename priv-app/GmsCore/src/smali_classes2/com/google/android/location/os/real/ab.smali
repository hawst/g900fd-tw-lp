.class final Lcom/google/android/location/os/real/ab;
.super Landroid/os/Handler;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/location/os/real/x;

.field private final b:Landroid/telephony/TelephonyManager;

.field private final c:Landroid/net/ConnectivityManager;

.field private final d:Landroid/net/wifi/WifiManager;

.field private final e:Landroid/os/PowerManager;

.field private f:I

.field private g:Lcom/google/android/location/e/h;

.field private h:Lcom/google/android/location/u;


# direct methods
.method private constructor <init>(Lcom/google/android/location/os/real/x;Landroid/os/Looper;)V
    .locals 2

    .prologue
    .line 185
    iput-object p1, p0, Lcom/google/android/location/os/real/ab;->a:Lcom/google/android/location/os/real/x;

    .line 186
    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 173
    iget-object v0, p0, Lcom/google/android/location/os/real/ab;->a:Lcom/google/android/location/os/real/x;

    iget-object v0, v0, Lcom/google/android/location/os/real/x;->n:Landroid/content/Context;

    const-string v1, "phone"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    iput-object v0, p0, Lcom/google/android/location/os/real/ab;->b:Landroid/telephony/TelephonyManager;

    .line 175
    iget-object v0, p0, Lcom/google/android/location/os/real/ab;->a:Lcom/google/android/location/os/real/x;

    iget-object v0, v0, Lcom/google/android/location/os/real/x;->n:Landroid/content/Context;

    const-string v1, "connectivity"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    iput-object v0, p0, Lcom/google/android/location/os/real/ab;->c:Landroid/net/ConnectivityManager;

    .line 177
    iget-object v0, p0, Lcom/google/android/location/os/real/ab;->a:Lcom/google/android/location/os/real/x;

    iget-object v0, v0, Lcom/google/android/location/os/real/x;->n:Landroid/content/Context;

    const-string v1, "wifi"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/wifi/WifiManager;

    iput-object v0, p0, Lcom/google/android/location/os/real/ab;->d:Landroid/net/wifi/WifiManager;

    .line 179
    iget-object v0, p0, Lcom/google/android/location/os/real/ab;->a:Lcom/google/android/location/os/real/x;

    iget-object v0, v0, Lcom/google/android/location/os/real/x;->n:Landroid/content/Context;

    const-string v1, "power"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    iput-object v0, p0, Lcom/google/android/location/os/real/ab;->e:Landroid/os/PowerManager;

    .line 181
    const/16 v0, -0x270f

    iput v0, p0, Lcom/google/android/location/os/real/ab;->f:I

    .line 182
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/location/os/real/ab;->g:Lcom/google/android/location/e/h;

    .line 187
    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/location/os/real/x;Landroid/os/Looper;B)V
    .locals 0

    .prologue
    .line 172
    invoke-direct {p0, p1, p2}, Lcom/google/android/location/os/real/ab;-><init>(Lcom/google/android/location/os/real/x;Landroid/os/Looper;)V

    return-void
.end method


# virtual methods
.method public final handleMessage(Landroid/os/Message;)V
    .locals 12

    .prologue
    const/4 v5, 0x0

    const/4 v0, 0x1

    const/4 v7, 0x0

    .line 193
    iget v1, p1, Landroid/os/Message;->arg2:I

    const/16 v2, 0x10e1

    if-ne v1, v2, :cond_0

    move v11, v0

    .line 205
    :goto_0
    iget v1, p1, Landroid/os/Message;->what:I

    packed-switch v1, :pswitch_data_0

    .line 498
    :pswitch_0
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "unexpected message "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p1, Landroid/os/Message;->what:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 195
    :cond_0
    iget v1, p1, Landroid/os/Message;->arg2:I

    const/16 v2, 0x2156

    if-ne v1, v2, :cond_1

    move v11, v7

    .line 196
    goto :goto_0

    .line 198
    :cond_1
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "No wakelock mode specified for command "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p1, Landroid/os/Message;->what:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 207
    :pswitch_1
    new-instance v0, Lcom/google/android/location/u;

    new-instance v1, Lcom/google/android/location/k;

    iget-object v2, p0, Lcom/google/android/location/os/real/ab;->a:Lcom/google/android/location/os/real/x;

    iget-object v2, v2, Lcom/google/android/location/os/real/x;->v:Lcom/google/android/location/os/real/aw;

    invoke-direct {v1, v2}, Lcom/google/android/location/k;-><init>(Lcom/google/android/location/os/bi;)V

    invoke-direct {v0, v1}, Lcom/google/android/location/u;-><init>(Lcom/google/android/location/k;)V

    iput-object v0, p0, Lcom/google/android/location/os/real/ab;->h:Lcom/google/android/location/u;

    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    const-string v1, "android.intent.action.AIRPLANE_MODE"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v1, "android.intent.action.BATTERY_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v1, "android.intent.action.SCREEN_OFF"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v1, "android.intent.action.SCREEN_ON"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/location/os/real/ab;->a:Lcom/google/android/location/os/real/x;

    iget-object v1, v1, Lcom/google/android/location/os/real/x;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/location/os/real/ab;->a:Lcom/google/android/location/os/real/x;

    iget-object v1, v1, Lcom/google/android/location/os/real/x;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/location/os/real/ab;->a:Lcom/google/android/location/os/real/x;

    iget-object v1, v1, Lcom/google/android/location/os/real/x;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/location/os/real/ab;->a:Lcom/google/android/location/os/real/x;

    iget-object v1, v1, Lcom/google/android/location/os/real/x;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/location/os/real/ab;->a:Lcom/google/android/location/os/real/x;

    iget-object v1, v1, Lcom/google/android/location/os/real/x;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/location/os/real/ab;->a:Lcom/google/android/location/os/real/x;

    iget-object v1, v1, Lcom/google/android/location/os/real/x;->f:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/location/os/real/ab;->a:Lcom/google/android/location/os/real/x;

    iget-object v1, v1, Lcom/google/android/location/os/real/x;->g:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/location/os/real/ab;->a:Lcom/google/android/location/os/real/x;

    iget-object v1, v1, Lcom/google/android/location/os/real/x;->h:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/location/os/real/ab;->a:Lcom/google/android/location/os/real/x;

    iget-object v1, v1, Lcom/google/android/location/os/real/x;->i:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/location/os/real/ab;->a:Lcom/google/android/location/os/real/x;

    iget-object v1, v1, Lcom/google/android/location/os/real/x;->j:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/location/os/real/ab;->a:Lcom/google/android/location/os/real/x;

    iget-object v1, v1, Lcom/google/android/location/os/real/x;->k:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/location/os/real/ab;->a:Lcom/google/android/location/os/real/x;

    iget-object v1, v1, Lcom/google/android/location/os/real/x;->l:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/location/os/real/ab;->a:Lcom/google/android/location/os/real/x;

    iget-object v1, v1, Lcom/google/android/location/os/real/x;->m:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v1, "android.net.wifi.SCAN_RESULTS"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v1, "android.net.wifi.WIFI_STATE_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v1, "android.net.conn.CONNECTIVITY_CHANGE"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v1, "android.intent.action.USER_FOREGROUND"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v1, "android.intent.action.USER_BACKGROUND"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v1, "android.os.action.POWER_SAVE_MODE_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/location/os/real/ab;->a:Lcom/google/android/location/os/real/x;

    iget-object v1, v1, Lcom/google/android/location/os/real/x;->w:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v2, p0, Lcom/google/android/location/os/real/ab;->a:Lcom/google/android/location/os/real/x;

    iget-boolean v2, v2, Lcom/google/android/location/os/real/x;->x:Z

    if-nez v2, :cond_2

    iget-object v2, p0, Lcom/google/android/location/os/real/ab;->a:Lcom/google/android/location/os/real/x;

    new-instance v3, Lcom/google/android/location/os/real/z;

    iget-object v4, p0, Lcom/google/android/location/os/real/ab;->a:Lcom/google/android/location/os/real/x;

    const/4 v5, 0x0

    invoke-direct {v3, v4, v5}, Lcom/google/android/location/os/real/z;-><init>(Lcom/google/android/location/os/real/x;B)V

    iput-object v3, v2, Lcom/google/android/location/os/real/x;->z:Lcom/google/android/location/os/real/z;

    iget-object v2, p0, Lcom/google/android/location/os/real/ab;->a:Lcom/google/android/location/os/real/x;

    iget-object v2, v2, Lcom/google/android/location/os/real/x;->n:Landroid/content/Context;

    iget-object v3, p0, Lcom/google/android/location/os/real/ab;->a:Lcom/google/android/location/os/real/x;

    iget-object v3, v3, Lcom/google/android/location/os/real/x;->z:Lcom/google/android/location/os/real/z;

    invoke-virtual {v2, v3, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    :cond_2
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Lcom/google/android/location/os/real/ab;->a:Lcom/google/android/location/os/real/x;

    iget-object v0, v0, Lcom/google/android/location/os/real/x;->n:Landroid/content/Context;

    const-string v1, "phone"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    iget-object v1, p0, Lcom/google/android/location/os/real/ab;->a:Lcom/google/android/location/os/real/x;

    iget-object v1, v1, Lcom/google/android/location/os/real/x;->r:Lcom/google/android/location/os/real/ad;

    const/16 v2, 0x450

    invoke-virtual {v0, v1, v2}, Landroid/telephony/TelephonyManager;->listen(Landroid/telephony/PhoneStateListener;I)V

    iget-object v0, p0, Lcom/google/android/location/os/real/ab;->a:Lcom/google/android/location/os/real/x;

    iget-object v0, v0, Lcom/google/android/location/os/real/x;->n:Landroid/content/Context;

    const-string v1, "location"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/location/LocationManager;

    iget-object v1, p0, Lcom/google/android/location/os/real/ab;->a:Lcom/google/android/location/os/real/x;

    iget-object v1, v1, Lcom/google/android/location/os/real/x;->s:Lcom/google/android/location/os/real/aa;

    invoke-virtual {v0, v1}, Landroid/location/LocationManager;->addGpsStatusListener(Landroid/location/GpsStatus$Listener;)Z

    const-string v1, "passive"

    invoke-virtual {v0, v1}, Landroid/location/LocationManager;->getProvider(Ljava/lang/String;)Landroid/location/LocationProvider;

    move-result-object v0

    if-eqz v0, :cond_3

    invoke-static {}, Lcom/google/android/location/os/real/bf;->a()Lcom/google/android/location/os/real/bf;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/location/os/real/ab;->a:Lcom/google/android/location/os/real/x;

    iget-object v1, v1, Lcom/google/android/location/os/real/x;->n:Landroid/content/Context;

    const-string v2, "passive"

    iget-object v3, p0, Lcom/google/android/location/os/real/ab;->a:Lcom/google/android/location/os/real/x;

    iget-object v3, v3, Lcom/google/android/location/os/real/x;->p:Lcom/google/android/location/os/real/ac;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v4

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/google/android/location/os/real/bf;->a(Landroid/content/Context;Ljava/lang/String;Landroid/location/LocationListener;Landroid/os/Looper;)V

    :cond_3
    iget-object v0, p0, Lcom/google/android/location/os/real/ab;->a:Lcom/google/android/location/os/real/x;

    iget-object v0, v0, Lcom/google/android/location/os/real/x;->o:Lcom/google/android/location/os/j;

    sget-object v1, Lcom/google/android/location/os/au;->a:Lcom/google/android/location/os/au;

    invoke-virtual {v0, v1}, Lcom/google/android/location/os/j;->a(Lcom/google/android/location/os/au;)V

    iget-object v0, p0, Lcom/google/android/location/os/real/ab;->h:Lcom/google/android/location/u;

    invoke-virtual {v0}, Lcom/google/android/location/u;->d()V

    iget-object v0, p0, Lcom/google/android/location/os/real/ab;->a:Lcom/google/android/location/os/real/x;

    iget-object v1, p0, Lcom/google/android/location/os/real/ab;->h:Lcom/google/android/location/u;

    invoke-static {v0, v1, v7}, Lcom/google/android/location/os/real/x;->a(Lcom/google/android/location/os/real/x;Lcom/google/android/location/os/a;Z)V

    .line 208
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    .line 210
    sget-boolean v0, Lcom/google/android/location/i/a;->b:Z

    if-eqz v0, :cond_4

    const-string v0, "NetworkLocationCallbackRunner"

    const-string v1, "CallbackRunner, CallbacksDispatcher and ActivityProvider initialized."

    invoke-static {v0, v1}, Lcom/google/android/location/o/a/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 501
    :cond_4
    :goto_1
    if-eqz v11, :cond_5

    .line 502
    iget-object v0, p0, Lcom/google/android/location/os/real/ab;->a:Lcom/google/android/location/os/real/x;

    iget-object v0, v0, Lcom/google/android/location/os/real/x;->u:Lcom/google/android/location/o/h;

    invoke-virtual {v0}, Lcom/google/android/location/o/h;->b()V

    .line 504
    :cond_5
    return-void

    .line 207
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 214
    :pswitch_2
    iget-object v1, p0, Lcom/google/android/location/os/real/ab;->a:Lcom/google/android/location/os/real/x;

    iget-object v1, v1, Lcom/google/android/location/os/real/x;->o:Lcom/google/android/location/os/j;

    sget-object v2, Lcom/google/android/location/os/au;->b:Lcom/google/android/location/os/au;

    invoke-virtual {v1, v2}, Lcom/google/android/location/os/j;->a(Lcom/google/android/location/os/au;)V

    .line 215
    iget-object v1, p0, Lcom/google/android/location/os/real/ab;->h:Lcom/google/android/location/u;

    iget v2, p1, Landroid/os/Message;->arg1:I

    if-eqz v2, :cond_7

    :goto_2
    invoke-virtual {v1, v0}, Lcom/google/android/location/u;->c(Z)V

    .line 216
    iget-object v0, p0, Lcom/google/android/location/os/real/ab;->b:Landroid/telephony/TelephonyManager;

    iget-object v1, p0, Lcom/google/android/location/os/real/ab;->a:Lcom/google/android/location/os/real/x;

    iget-object v1, v1, Lcom/google/android/location/os/real/x;->r:Lcom/google/android/location/os/real/ad;

    invoke-virtual {v0, v1, v7}, Landroid/telephony/TelephonyManager;->listen(Landroid/telephony/PhoneStateListener;I)V

    .line 217
    iget-object v0, p0, Lcom/google/android/location/os/real/ab;->a:Lcom/google/android/location/os/real/x;

    iget-object v0, v0, Lcom/google/android/location/os/real/x;->n:Landroid/content/Context;

    const-string v1, "location"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/location/LocationManager;

    .line 219
    iget-object v1, p0, Lcom/google/android/location/os/real/ab;->a:Lcom/google/android/location/os/real/x;

    iget-boolean v1, v1, Lcom/google/android/location/os/real/x;->A:Z

    if-eqz v1, :cond_8

    iget-object v1, p0, Lcom/google/android/location/os/real/ab;->a:Lcom/google/android/location/os/real/x;

    iget-object v1, v1, Lcom/google/android/location/os/real/x;->q:Lcom/google/android/location/os/real/ac;

    :goto_3
    invoke-virtual {v0, v1}, Landroid/location/LocationManager;->removeUpdates(Landroid/location/LocationListener;)V

    .line 220
    iget-object v1, p0, Lcom/google/android/location/os/real/ab;->a:Lcom/google/android/location/os/real/x;

    iget-object v1, v1, Lcom/google/android/location/os/real/x;->s:Lcom/google/android/location/os/real/aa;

    invoke-virtual {v0, v1}, Landroid/location/LocationManager;->removeGpsStatusListener(Landroid/location/GpsStatus$Listener;)V

    .line 221
    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Looper;->quit()V

    .line 222
    iget-object v0, p0, Lcom/google/android/location/os/real/ab;->a:Lcom/google/android/location/os/real/x;

    iget-object v0, v0, Lcom/google/android/location/os/real/x;->u:Lcom/google/android/location/o/h;

    invoke-virtual {v0}, Lcom/google/android/location/o/h;->c()Z

    move-result v0

    if-eqz v0, :cond_a

    .line 223
    sget-boolean v0, Lcom/google/android/location/i/a;->e:Z

    if-eqz v0, :cond_6

    const-string v0, "NetworkLocationCallbackRunner"

    const-string v1, "Wake lock is held after the callback runner quit."

    invoke-static {v0, v1}, Lcom/google/android/location/o/a/a;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 224
    :cond_6
    sget-boolean v0, Lcom/google/android/location/i/a;->a:Z

    if-eqz v0, :cond_9

    .line 226
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Wake lock is still held. Check for program errors."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_7
    move v0, v7

    .line 215
    goto :goto_2

    .line 219
    :cond_8
    iget-object v1, p0, Lcom/google/android/location/os/real/ab;->a:Lcom/google/android/location/os/real/x;

    iget-object v1, v1, Lcom/google/android/location/os/real/x;->p:Lcom/google/android/location/os/real/ac;

    goto :goto_3

    .line 230
    :cond_9
    :goto_4
    iget-object v0, p0, Lcom/google/android/location/os/real/ab;->a:Lcom/google/android/location/os/real/x;

    iget-object v0, v0, Lcom/google/android/location/os/real/x;->u:Lcom/google/android/location/o/h;

    invoke-virtual {v0}, Lcom/google/android/location/o/h;->c()Z

    move-result v0

    if-eqz v0, :cond_a

    .line 231
    iget-object v0, p0, Lcom/google/android/location/os/real/ab;->a:Lcom/google/android/location/os/real/x;

    iget-object v0, v0, Lcom/google/android/location/os/real/x;->u:Lcom/google/android/location/o/h;

    invoke-virtual {v0}, Lcom/google/android/location/o/h;->b()V

    goto :goto_4

    .line 235
    :cond_a
    iput-object v5, p0, Lcom/google/android/location/os/real/ab;->h:Lcom/google/android/location/u;

    goto/16 :goto_1

    .line 239
    :pswitch_3
    iget-object v1, p0, Lcom/google/android/location/os/real/ab;->h:Lcom/google/android/location/u;

    invoke-virtual {v1}, Lcom/google/android/location/u;->f()Z

    move-result v1

    .line 240
    if-eqz v1, :cond_e

    .line 241
    iget-object v1, p0, Lcom/google/android/location/os/real/ab;->a:Lcom/google/android/location/os/real/x;

    iget-object v1, v1, Lcom/google/android/location/os/real/x;->o:Lcom/google/android/location/os/j;

    sget-object v2, Lcom/google/android/location/os/au;->v:Lcom/google/android/location/os/au;

    invoke-virtual {v1, v2}, Lcom/google/android/location/os/j;->a(Lcom/google/android/location/os/au;)V

    .line 242
    iget-object v1, p0, Lcom/google/android/location/os/real/ab;->h:Lcom/google/android/location/u;

    iget v2, p1, Landroid/os/Message;->arg1:I

    if-eqz v2, :cond_b

    :goto_5
    invoke-virtual {v1, v0}, Lcom/google/android/location/u;->f(Z)V

    .line 243
    sget-boolean v0, Lcom/google/android/location/i/a;->a:Z

    if-eqz v0, :cond_c

    .line 244
    iget-object v0, p0, Lcom/google/android/location/os/real/ab;->a:Lcom/google/android/location/os/real/x;

    iget-boolean v0, v0, Lcom/google/android/location/os/real/x;->A:Z

    if-eqz v0, :cond_d

    .line 245
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Stopping NetworkProvider when GPS is left on."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_b
    move v0, v7

    .line 242
    goto :goto_5

    .line 248
    :cond_c
    iget-object v0, p0, Lcom/google/android/location/os/real/ab;->a:Lcom/google/android/location/os/real/x;

    iget-boolean v0, v0, Lcom/google/android/location/os/real/x;->A:Z

    if-eqz v0, :cond_d

    .line 249
    sget-boolean v0, Lcom/google/android/location/i/a;->e:Z

    if-eqz v0, :cond_d

    const-string v0, "NetworkLocationCallbackRunner"

    const-string v1, "Stopping NetworkProvider when GPS is left on."

    invoke-static {v0, v1}, Lcom/google/android/location/o/a/a;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 252
    :cond_d
    sget-boolean v0, Lcom/google/android/location/i/a;->b:Z

    if-eqz v0, :cond_e

    const-string v0, "NetworkLocationCallbackRunner"

    const-string v1, "NetworkProvider quit."

    invoke-static {v0, v1}, Lcom/google/android/location/o/a/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 255
    :cond_e
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    goto/16 :goto_1

    .line 259
    :pswitch_4
    iget-object v0, p0, Lcom/google/android/location/os/real/ab;->a:Lcom/google/android/location/os/real/x;

    iget-object v0, v0, Lcom/google/android/location/os/real/x;->o:Lcom/google/android/location/os/j;

    sget-object v1, Lcom/google/android/location/os/au;->n:Lcom/google/android/location/os/au;

    invoke-virtual {v0, v1}, Lcom/google/android/location/os/j;->a(Lcom/google/android/location/os/au;)V

    .line 260
    iget-object v1, p0, Lcom/google/android/location/os/real/ab;->h:Lcom/google/android/location/u;

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/google/p/a/b/b/a;

    invoke-virtual {v1, v0}, Lcom/google/android/location/u;->b(Lcom/google/p/a/b/b/a;)V

    goto/16 :goto_1

    .line 264
    :pswitch_5
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    move-object v10, v0

    check-cast v10, Lcom/google/android/location/os/real/af;

    .line 265
    iget-object v0, p0, Lcom/google/android/location/os/real/ab;->a:Lcom/google/android/location/os/real/x;

    iget-object v2, v0, Lcom/google/android/location/os/real/x;->o:Lcom/google/android/location/os/j;

    iget v6, v10, Lcom/google/android/location/os/real/af;->a:I

    iget v7, v10, Lcom/google/android/location/os/real/af;->b:I

    iget v8, v10, Lcom/google/android/location/os/real/af;->c:I

    iget-boolean v9, v10, Lcom/google/android/location/os/real/af;->d:Z

    new-instance v1, Lcom/google/android/location/os/v;

    sget-object v3, Lcom/google/android/location/os/au;->d:Lcom/google/android/location/os/au;

    iget-object v0, v2, Lcom/google/android/location/os/j;->a:Lcom/google/android/location/os/at;

    invoke-interface {v0}, Lcom/google/android/location/os/at;->a()J

    move-result-wide v4

    invoke-direct/range {v1 .. v9}, Lcom/google/android/location/os/v;-><init>(Lcom/google/android/location/os/j;Lcom/google/android/location/os/au;JIIIZ)V

    if-eqz v9, :cond_f

    const-string v9, "1"

    :goto_6
    move-object v4, v2

    move-object v5, v1

    invoke-virtual/range {v4 .. v9}, Lcom/google/android/location/os/j;->a(Lcom/google/android/location/os/as;IIILjava/lang/String;)V

    .line 267
    iget-object v0, p0, Lcom/google/android/location/os/real/ab;->h:Lcom/google/android/location/u;

    iget v1, v10, Lcom/google/android/location/os/real/af;->a:I

    iget v2, v10, Lcom/google/android/location/os/real/af;->b:I

    iget v3, v10, Lcom/google/android/location/os/real/af;->c:I

    iget-boolean v4, v10, Lcom/google/android/location/os/real/af;->d:Z

    iget-object v5, v10, Lcom/google/android/location/os/real/af;->e:Lcom/google/android/location/o/n;

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/location/u;->a(IIIZLcom/google/android/location/o/n;)V

    goto/16 :goto_1

    .line 265
    :cond_f
    const-string v9, "0"

    goto :goto_6

    .line 272
    :pswitch_6
    iget v1, p1, Landroid/os/Message;->arg1:I

    if-ne v1, v0, :cond_10

    .line 273
    const/16 v0, -0x270f

    iput v0, p0, Lcom/google/android/location/os/real/ab;->f:I

    .line 276
    :cond_10
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    .line 278
    invoke-static {}, Lcom/google/android/location/os/real/bf;->a()Lcom/google/android/location/os/real/bf;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/location/os/real/ab;->b:Landroid/telephony/TelephonyManager;

    iget v4, p0, Lcom/google/android/location/os/real/ab;->f:I

    invoke-virtual {v2, v3, v4, v0, v1}, Lcom/google/android/location/os/real/bf;->a(Landroid/telephony/TelephonyManager;IJ)[Lcom/google/android/location/e/h;

    move-result-object v0

    .line 281
    if-eqz v0, :cond_11

    array-length v1, v0

    if-nez v1, :cond_12

    .line 287
    :cond_11
    iget-object v0, p0, Lcom/google/android/location/os/real/ab;->a:Lcom/google/android/location/os/real/x;

    iget-object v0, v0, Lcom/google/android/location/os/real/x;->o:Lcom/google/android/location/os/j;

    invoke-virtual {v0, v5}, Lcom/google/android/location/os/j;->a(Lcom/google/android/location/e/h;)V

    .line 288
    iget-object v0, p0, Lcom/google/android/location/os/real/ab;->h:Lcom/google/android/location/u;

    invoke-virtual {v0, v5}, Lcom/google/android/location/u;->a(Lcom/google/android/location/e/h;)V

    .line 289
    iput-object v5, p0, Lcom/google/android/location/os/real/ab;->g:Lcom/google/android/location/e/h;

    goto/16 :goto_1

    .line 292
    :cond_12
    :goto_7
    array-length v1, v0

    if-ge v7, v1, :cond_4

    .line 293
    aget-object v1, v0, v7

    iput-object v1, p0, Lcom/google/android/location/os/real/ab;->g:Lcom/google/android/location/e/h;

    .line 294
    iget-object v1, p0, Lcom/google/android/location/os/real/ab;->a:Lcom/google/android/location/os/real/x;

    iget-object v1, v1, Lcom/google/android/location/os/real/x;->o:Lcom/google/android/location/os/j;

    iget-object v2, p0, Lcom/google/android/location/os/real/ab;->g:Lcom/google/android/location/e/h;

    invoke-virtual {v1, v2}, Lcom/google/android/location/os/j;->a(Lcom/google/android/location/e/h;)V

    .line 295
    iget-object v1, p0, Lcom/google/android/location/os/real/ab;->h:Lcom/google/android/location/u;

    iget-object v2, p0, Lcom/google/android/location/os/real/ab;->g:Lcom/google/android/location/e/h;

    invoke-virtual {v1, v2}, Lcom/google/android/location/u;->a(Lcom/google/android/location/e/h;)V

    .line 292
    add-int/lit8 v7, v7, 0x1

    goto :goto_7

    .line 301
    :pswitch_7
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Landroid/telephony/SignalStrength;

    .line 304
    iget-object v1, p0, Lcom/google/android/location/os/real/ab;->g:Lcom/google/android/location/e/h;

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/google/android/location/os/real/ab;->g:Lcom/google/android/location/e/h;

    instance-of v1, v1, Lcom/google/android/location/e/m;

    if-nez v1, :cond_13

    iget-object v1, p0, Lcom/google/android/location/os/real/ab;->g:Lcom/google/android/location/e/h;

    instance-of v1, v1, Lcom/google/android/location/e/e;

    if-eqz v1, :cond_4

    .line 306
    :cond_13
    invoke-virtual {v0}, Landroid/telephony/SignalStrength;->isGsm()Z

    move-result v1

    if-eqz v1, :cond_14

    .line 307
    invoke-virtual {v0}, Landroid/telephony/SignalStrength;->getGsmSignalStrength()I

    move-result v0

    iput v0, p0, Lcom/google/android/location/os/real/ab;->f:I

    .line 311
    :goto_8
    iget-object v0, p0, Lcom/google/android/location/os/real/ab;->a:Lcom/google/android/location/os/real/x;

    iget-object v2, v0, Lcom/google/android/location/os/real/x;->o:Lcom/google/android/location/os/j;

    iget v6, p0, Lcom/google/android/location/os/real/ab;->f:I

    new-instance v1, Lcom/google/android/location/os/ap;

    sget-object v3, Lcom/google/android/location/os/au;->i:Lcom/google/android/location/os/au;

    iget-object v0, v2, Lcom/google/android/location/os/j;->a:Lcom/google/android/location/os/at;

    invoke-interface {v0}, Lcom/google/android/location/os/at;->a()J

    move-result-wide v4

    invoke-direct/range {v1 .. v6}, Lcom/google/android/location/os/ap;-><init>(Lcom/google/android/location/os/j;Lcom/google/android/location/os/au;JI)V

    invoke-virtual {v2, v1}, Lcom/google/android/location/os/j;->a(Lcom/google/android/location/os/as;)V

    .line 312
    iget-object v0, p0, Lcom/google/android/location/os/real/ab;->g:Lcom/google/android/location/e/h;

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    iget v1, p0, Lcom/google/android/location/os/real/ab;->f:I

    invoke-virtual {v0, v2, v3, v1}, Lcom/google/android/location/e/h;->a(JI)Lcom/google/android/location/e/h;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/os/real/ab;->g:Lcom/google/android/location/e/h;

    .line 314
    iget-object v0, p0, Lcom/google/android/location/os/real/ab;->h:Lcom/google/android/location/u;

    iget-object v1, p0, Lcom/google/android/location/os/real/ab;->g:Lcom/google/android/location/e/h;

    invoke-virtual {v0, v1}, Lcom/google/android/location/u;->a(Lcom/google/android/location/e/h;)V

    goto/16 :goto_1

    .line 309
    :cond_14
    invoke-virtual {v0}, Landroid/telephony/SignalStrength;->getCdmaDbm()I

    move-result v0

    iput v0, p0, Lcom/google/android/location/os/real/ab;->f:I

    goto :goto_8

    .line 319
    :pswitch_8
    iget v6, p1, Landroid/os/Message;->arg1:I

    .line 320
    invoke-static {v6}, Lcom/google/android/location/j/k;->a(I)Lcom/google/android/location/j/k;

    move-result-object v0

    .line 321
    iget-object v1, p0, Lcom/google/android/location/os/real/ab;->a:Lcom/google/android/location/os/real/x;

    iget-object v2, v1, Lcom/google/android/location/os/real/x;->o:Lcom/google/android/location/os/j;

    new-instance v1, Lcom/google/android/location/os/am;

    sget-object v3, Lcom/google/android/location/os/au;->f:Lcom/google/android/location/os/au;

    iget-object v4, v2, Lcom/google/android/location/os/j;->a:Lcom/google/android/location/os/at;

    invoke-interface {v4}, Lcom/google/android/location/os/at;->a()J

    move-result-wide v4

    invoke-direct/range {v1 .. v6}, Lcom/google/android/location/os/am;-><init>(Lcom/google/android/location/os/j;Lcom/google/android/location/os/au;JI)V

    invoke-virtual {v2, v1, v6}, Lcom/google/android/location/os/j;->a(Lcom/google/android/location/os/as;I)V

    .line 322
    iget-object v1, p0, Lcom/google/android/location/os/real/ab;->a:Lcom/google/android/location/os/real/x;

    iget-object v1, v1, Lcom/google/android/location/os/real/x;->v:Lcom/google/android/location/os/real/aw;

    invoke-virtual {v1, v0}, Lcom/google/android/location/os/real/aw;->a(Lcom/google/android/location/j/k;)V

    .line 323
    iget-object v1, p0, Lcom/google/android/location/os/real/ab;->h:Lcom/google/android/location/u;

    invoke-virtual {v1, v0}, Lcom/google/android/location/u;->a(Lcom/google/android/location/j/k;)V

    goto/16 :goto_1

    .line 327
    :pswitch_9
    iget-object v0, p0, Lcom/google/android/location/os/real/ab;->a:Lcom/google/android/location/os/real/x;

    iget-boolean v0, v0, Lcom/google/android/location/os/real/x;->A:Z

    if-nez v0, :cond_4

    .line 329
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/location/e/aj;

    .line 330
    new-instance v2, Lcom/google/android/location/os/real/av;

    iget-object v1, v0, Lcom/google/android/location/e/aj;->a:Ljava/lang/Object;

    check-cast v1, Landroid/location/Location;

    iget-object v0, v0, Lcom/google/android/location/e/aj;->b:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    iget-object v0, p0, Lcom/google/android/location/os/real/ab;->a:Lcom/google/android/location/os/real/x;

    iget-object v0, v0, Lcom/google/android/location/os/real/x;->s:Lcom/google/android/location/os/real/aa;

    invoke-virtual {v0}, Lcom/google/android/location/os/real/aa;->a()I

    move-result v0

    invoke-direct {v2, v1, v4, v5, v0}, Lcom/google/android/location/os/real/av;-><init>(Landroid/location/Location;JI)V

    .line 332
    iget-object v0, p0, Lcom/google/android/location/os/real/ab;->a:Lcom/google/android/location/os/real/x;

    iget-object v0, v0, Lcom/google/android/location/os/real/x;->o:Lcom/google/android/location/os/j;

    invoke-virtual {v0, v2}, Lcom/google/android/location/os/j;->a(Lcom/google/android/location/os/aw;)V

    .line 333
    iget-object v0, p0, Lcom/google/android/location/os/real/ab;->h:Lcom/google/android/location/u;

    invoke-virtual {v0, v2}, Lcom/google/android/location/u;->a(Lcom/google/android/location/os/aw;)V

    goto/16 :goto_1

    .line 338
    :pswitch_a
    iget-object v0, p0, Lcom/google/android/location/os/real/ab;->a:Lcom/google/android/location/os/real/x;

    iget-boolean v0, v0, Lcom/google/android/location/os/real/x;->A:Z

    if-eqz v0, :cond_4

    .line 340
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/location/e/aj;

    .line 341
    new-instance v2, Lcom/google/android/location/os/real/av;

    iget-object v1, v0, Lcom/google/android/location/e/aj;->a:Ljava/lang/Object;

    check-cast v1, Landroid/location/Location;

    iget-object v0, v0, Lcom/google/android/location/e/aj;->b:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    iget-object v0, p0, Lcom/google/android/location/os/real/ab;->a:Lcom/google/android/location/os/real/x;

    iget-object v0, v0, Lcom/google/android/location/os/real/x;->s:Lcom/google/android/location/os/real/aa;

    invoke-virtual {v0}, Lcom/google/android/location/os/real/aa;->a()I

    move-result v0

    invoke-direct {v2, v1, v4, v5, v0}, Lcom/google/android/location/os/real/av;-><init>(Landroid/location/Location;JI)V

    .line 343
    iget-object v0, p0, Lcom/google/android/location/os/real/ab;->a:Lcom/google/android/location/os/real/x;

    iget-object v0, v0, Lcom/google/android/location/os/real/x;->o:Lcom/google/android/location/os/j;

    invoke-virtual {v0, v2}, Lcom/google/android/location/os/j;->a(Lcom/google/android/location/os/aw;)V

    .line 344
    iget-object v0, p0, Lcom/google/android/location/os/real/ab;->h:Lcom/google/android/location/u;

    invoke-virtual {v0, v2}, Lcom/google/android/location/u;->a(Lcom/google/android/location/os/aw;)V

    goto/16 :goto_1

    .line 349
    :pswitch_b
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/location/os/real/ag;

    .line 350
    iget-object v8, v0, Lcom/google/android/location/os/real/ag;->a:[Lcom/google/android/location/e/bi;

    .line 352
    if-eqz v8, :cond_15

    array-length v1, v8

    if-lez v1, :cond_15

    .line 353
    iget-object v1, p0, Lcom/google/android/location/os/real/ab;->a:Lcom/google/android/location/os/real/x;

    iget-object v2, v1, Lcom/google/android/location/os/real/x;->o:Lcom/google/android/location/os/j;

    aget-object v6, v8, v7

    new-instance v1, Lcom/google/android/location/os/n;

    sget-object v3, Lcom/google/android/location/os/au;->s:Lcom/google/android/location/os/au;

    iget-object v4, v2, Lcom/google/android/location/os/j;->a:Lcom/google/android/location/os/at;

    invoke-interface {v4}, Lcom/google/android/location/os/at;->a()J

    move-result-wide v4

    invoke-direct/range {v1 .. v6}, Lcom/google/android/location/os/n;-><init>(Lcom/google/android/location/os/j;Lcom/google/android/location/os/au;JLcom/google/android/location/e/bi;)V

    invoke-virtual {v6}, Lcom/google/android/location/e/bi;->a()I

    move-result v3

    invoke-virtual {v2, v1, v3}, Lcom/google/android/location/os/j;->a(Lcom/google/android/location/os/as;I)V

    .line 356
    :cond_15
    iget-object v1, p0, Lcom/google/android/location/os/real/ab;->h:Lcom/google/android/location/u;

    iget-boolean v0, v0, Lcom/google/android/location/os/real/ag;->b:Z

    invoke-virtual {v1, v8, v0}, Lcom/google/android/location/u;->a([Lcom/google/android/location/e/bi;Z)V

    goto/16 :goto_1

    .line 360
    :pswitch_c
    iget v1, p1, Landroid/os/Message;->arg1:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_16

    move v1, v0

    .line 361
    :goto_9
    iget v2, p1, Landroid/os/Message;->arg1:I

    and-int/lit8 v2, v2, 0x2

    if-eqz v2, :cond_17

    .line 362
    :goto_a
    iget-object v2, p0, Lcom/google/android/location/os/real/ab;->a:Lcom/google/android/location/os/real/x;

    iget-object v2, v2, Lcom/google/android/location/os/real/x;->o:Lcom/google/android/location/os/j;

    invoke-virtual {v2, v1}, Lcom/google/android/location/os/j;->c(Z)V

    .line 363
    iget-object v2, p0, Lcom/google/android/location/os/real/ab;->h:Lcom/google/android/location/u;

    invoke-virtual {v2, v1, v0}, Lcom/google/android/location/u;->a(ZZ)V

    goto/16 :goto_1

    :cond_16
    move v1, v7

    .line 360
    goto :goto_9

    :cond_17
    move v0, v7

    .line 361
    goto :goto_a

    .line 367
    :pswitch_d
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Landroid/os/Bundle;

    .line 368
    iget-object v1, p0, Lcom/google/android/location/os/real/ab;->a:Lcom/google/android/location/os/real/x;

    iget-object v2, p0, Lcom/google/android/location/os/real/ab;->h:Lcom/google/android/location/u;

    invoke-virtual {v1, v0, v2}, Lcom/google/android/location/os/real/x;->a(Landroid/os/Bundle;Lcom/google/android/location/os/a;)V

    goto/16 :goto_1

    .line 372
    :pswitch_e
    iget v1, p1, Landroid/os/Message;->arg1:I

    if-eqz v1, :cond_18

    .line 373
    :goto_b
    iget-object v1, p0, Lcom/google/android/location/os/real/ab;->a:Lcom/google/android/location/os/real/x;

    iget-object v1, v1, Lcom/google/android/location/os/real/x;->o:Lcom/google/android/location/os/j;

    invoke-virtual {v1, v0}, Lcom/google/android/location/os/j;->b(Z)V

    .line 374
    iget-object v1, p0, Lcom/google/android/location/os/real/ab;->h:Lcom/google/android/location/u;

    invoke-virtual {v1, v0}, Lcom/google/android/location/u;->a(Z)V

    goto/16 :goto_1

    :cond_18
    move v0, v7

    .line 372
    goto :goto_b

    .line 378
    :pswitch_f
    iget v1, p1, Landroid/os/Message;->arg1:I

    if-eqz v1, :cond_19

    .line 379
    :goto_c
    iget-object v1, p0, Lcom/google/android/location/os/real/ab;->a:Lcom/google/android/location/os/real/x;

    iget-object v1, v1, Lcom/google/android/location/os/real/x;->o:Lcom/google/android/location/os/j;

    invoke-virtual {v1, v0}, Lcom/google/android/location/os/j;->a(Z)V

    .line 380
    iget-object v1, p0, Lcom/google/android/location/os/real/ab;->h:Lcom/google/android/location/u;

    invoke-virtual {v1, v0}, Lcom/google/android/location/u;->d(Z)V

    goto/16 :goto_1

    :cond_19
    move v0, v7

    .line 378
    goto :goto_c

    .line 384
    :pswitch_10
    iget v1, p1, Landroid/os/Message;->arg1:I

    if-eqz v1, :cond_1a

    move v6, v0

    .line 385
    :goto_d
    iget-object v1, p0, Lcom/google/android/location/os/real/ab;->a:Lcom/google/android/location/os/real/x;

    iget-object v2, v1, Lcom/google/android/location/os/real/x;->o:Lcom/google/android/location/os/j;

    new-instance v1, Lcom/google/android/location/os/aq;

    sget-object v3, Lcom/google/android/location/os/au;->j:Lcom/google/android/location/os/au;

    iget-object v4, v2, Lcom/google/android/location/os/j;->a:Lcom/google/android/location/os/at;

    invoke-interface {v4}, Lcom/google/android/location/os/at;->a()J

    move-result-wide v4

    invoke-direct/range {v1 .. v6}, Lcom/google/android/location/os/aq;-><init>(Lcom/google/android/location/os/j;Lcom/google/android/location/os/au;JZ)V

    if-eqz v6, :cond_1b

    :goto_e
    invoke-virtual {v2, v1, v0}, Lcom/google/android/location/os/j;->a(Lcom/google/android/location/os/as;I)V

    .line 386
    iget-object v0, p0, Lcom/google/android/location/os/real/ab;->h:Lcom/google/android/location/u;

    invoke-virtual {v0, v6}, Lcom/google/android/location/u;->e(Z)V

    goto/16 :goto_1

    :cond_1a
    move v6, v7

    .line 384
    goto :goto_d

    :cond_1b
    move v0, v7

    .line 385
    goto :goto_e

    .line 390
    :pswitch_11
    iget-object v0, p0, Lcom/google/android/location/os/real/ab;->a:Lcom/google/android/location/os/real/x;

    iget-object v0, v0, Lcom/google/android/location/os/real/x;->o:Lcom/google/android/location/os/j;

    sget-object v1, Lcom/google/android/location/os/au;->k:Lcom/google/android/location/os/au;

    invoke-virtual {v0, v1}, Lcom/google/android/location/os/j;->a(Lcom/google/android/location/os/au;)V

    .line 391
    iget-object v1, p0, Lcom/google/android/location/os/real/ab;->h:Lcom/google/android/location/u;

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/google/p/a/b/b/a;

    invoke-virtual {v1, v0}, Lcom/google/android/location/u;->a(Lcom/google/p/a/b/b/a;)V

    goto/16 :goto_1

    .line 396
    :pswitch_12
    iget-object v0, p0, Lcom/google/android/location/os/real/ab;->a:Lcom/google/android/location/os/real/x;

    iget-object v1, p0, Lcom/google/android/location/os/real/ab;->c:Landroid/net/ConnectivityManager;

    iget-object v2, p0, Lcom/google/android/location/os/real/ab;->d:Landroid/net/wifi/WifiManager;

    iget-object v3, p0, Lcom/google/android/location/os/real/ab;->h:Lcom/google/android/location/u;

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/location/os/real/x;->a(Landroid/net/ConnectivityManager;Landroid/net/wifi/WifiManager;Lcom/google/android/location/os/a;)V

    goto/16 :goto_1

    .line 400
    :pswitch_13
    iget-object v0, p0, Lcom/google/android/location/os/real/ab;->a:Lcom/google/android/location/os/real/x;

    iget-object v0, v0, Lcom/google/android/location/os/real/x;->o:Lcom/google/android/location/os/j;

    sget-object v1, Lcom/google/android/location/os/au;->q:Lcom/google/android/location/os/au;

    invoke-virtual {v0, v1}, Lcom/google/android/location/os/j;->a(Lcom/google/android/location/os/au;)V

    .line 401
    iget-object v1, p0, Lcom/google/android/location/os/real/ab;->h:Lcom/google/android/location/u;

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/location/e/ah;

    invoke-virtual {v1, v0}, Lcom/google/android/location/u;->a(Lcom/google/android/location/e/ah;)V

    goto/16 :goto_1

    .line 405
    :pswitch_14
    iget-object v1, p0, Lcom/google/android/location/os/real/ab;->h:Lcom/google/android/location/u;

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/location/activity/bd;

    invoke-virtual {v1, v0}, Lcom/google/android/location/u;->a(Lcom/google/android/location/activity/bd;)V

    goto/16 :goto_1

    .line 410
    :pswitch_15
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/location/e/aj;

    .line 411
    iget-object v2, p0, Lcom/google/android/location/os/real/ab;->h:Lcom/google/android/location/u;

    iget-object v1, v0, Lcom/google/android/location/e/aj;->a:Ljava/lang/Object;

    check-cast v1, Lcom/google/android/location/j/k;

    iget-object v0, v0, Lcom/google/android/location/e/aj;->b:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/location/e/b;

    invoke-virtual {v2, v1, v0}, Lcom/google/android/location/u;->a(Lcom/google/android/location/j/k;Lcom/google/android/location/e/b;)V

    goto/16 :goto_1

    .line 415
    :pswitch_16
    iget-object v1, p0, Lcom/google/android/location/os/real/ab;->h:Lcom/google/android/location/u;

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/gms/location/ActivityRecognitionResult;

    invoke-virtual {v1, v0}, Lcom/google/android/location/u;->a(Lcom/google/android/gms/location/ActivityRecognitionResult;)V

    goto/16 :goto_1

    .line 419
    :pswitch_17
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    move-object v6, v0

    check-cast v6, Lcom/google/android/location/os/real/y;

    .line 420
    iget-object v0, p0, Lcom/google/android/location/os/real/ab;->h:Lcom/google/android/location/u;

    iget v1, v6, Lcom/google/android/location/os/real/y;->a:I

    iget v2, v6, Lcom/google/android/location/os/real/y;->b:I

    iget-boolean v3, v6, Lcom/google/android/location/os/real/y;->c:Z

    iget-boolean v4, v6, Lcom/google/android/location/os/real/y;->d:Z

    iget-object v5, v6, Lcom/google/android/location/os/real/y;->e:Lcom/google/android/location/o/n;

    iget-object v6, v6, Lcom/google/android/location/os/real/y;->f:Ljava/util/Set;

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/location/u;->a(IIZZLcom/google/android/location/o/n;Ljava/util/Set;)V

    goto/16 :goto_1

    .line 427
    :pswitch_18
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/location/e/aj;

    .line 428
    iget-object v2, p0, Lcom/google/android/location/os/real/ab;->h:Lcom/google/android/location/u;

    iget-object v1, v0, Lcom/google/android/location/e/aj;->a:Ljava/lang/Object;

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    iget-object v0, v0, Lcom/google/android/location/e/aj;->b:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v2, v1, v0}, Lcom/google/android/location/u;->a(ZLjava/lang/String;)V

    goto/16 :goto_1

    .line 433
    :pswitch_19
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/location/e/aj;

    .line 434
    iget-object v2, p0, Lcom/google/android/location/os/real/ab;->h:Lcom/google/android/location/u;

    iget-object v1, v0, Lcom/google/android/location/e/aj;->a:Ljava/lang/Object;

    check-cast v1, Lcom/google/android/location/os/bk;

    iget-object v0, v0, Lcom/google/android/location/e/aj;->b:Ljava/lang/Object;

    invoke-virtual {v2, v1, v0}, Lcom/google/android/location/u;->a(Lcom/google/android/location/os/bk;Ljava/lang/Object;)V

    goto/16 :goto_1

    .line 438
    :pswitch_1a
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Runnable;

    .line 439
    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    goto/16 :goto_1

    .line 443
    :pswitch_1b
    iget-object v0, p0, Lcom/google/android/location/os/real/ab;->h:Lcom/google/android/location/u;

    invoke-virtual {v0}, Lcom/google/android/location/u;->a()V

    goto/16 :goto_1

    .line 447
    :pswitch_1c
    iget-object v0, p0, Lcom/google/android/location/os/real/ab;->h:Lcom/google/android/location/u;

    invoke-virtual {v0}, Lcom/google/android/location/u;->b()V

    goto/16 :goto_1

    .line 451
    :pswitch_1d
    iget-object v1, p0, Lcom/google/android/location/os/real/ab;->a:Lcom/google/android/location/os/real/x;

    iget-object v1, v1, Lcom/google/android/location/os/real/x;->o:Lcom/google/android/location/os/j;

    sget-object v2, Lcom/google/android/location/os/au;->u:Lcom/google/android/location/os/au;

    invoke-virtual {v1, v2}, Lcom/google/android/location/os/j;->a(Lcom/google/android/location/os/au;)V

    .line 453
    iget-object v1, p0, Lcom/google/android/location/os/real/ab;->h:Lcom/google/android/location/u;

    invoke-virtual {v1}, Lcom/google/android/location/u;->f()Z

    move-result v1

    if-nez v1, :cond_1d

    .line 457
    iget v2, p1, Landroid/os/Message;->arg1:I

    .line 458
    and-int/lit8 v1, v2, 0x1

    if-eqz v1, :cond_1e

    move v1, v0

    .line 459
    :goto_f
    and-int/lit8 v2, v2, 0x2

    if-eqz v2, :cond_1c

    move v7, v0

    .line 460
    :cond_1c
    iget-object v2, p0, Lcom/google/android/location/os/real/ab;->h:Lcom/google/android/location/u;

    iget-object v3, p0, Lcom/google/android/location/os/real/ab;->a:Lcom/google/android/location/os/real/x;

    iget-object v3, v3, Lcom/google/android/location/os/real/x;->v:Lcom/google/android/location/os/real/aw;

    iget-object v4, p0, Lcom/google/android/location/os/real/ab;->a:Lcom/google/android/location/os/real/x;

    iget-object v4, v4, Lcom/google/android/location/os/real/x;->v:Lcom/google/android/location/os/real/aw;

    invoke-virtual {v4}, Lcom/google/android/location/os/real/aw;->a()Lcom/google/android/location/e/ah;

    move-result-object v4

    invoke-virtual {v2, v3, v4, v1}, Lcom/google/android/location/u;->a(Lcom/google/android/location/os/bi;Lcom/google/android/location/e/ah;Z)Lcom/google/android/location/ap;

    move-result-object v1

    .line 465
    if-eqz v1, :cond_1d

    .line 468
    iget-object v2, p0, Lcom/google/android/location/os/real/ab;->a:Lcom/google/android/location/os/real/x;

    invoke-static {v2, v1, v0}, Lcom/google/android/location/os/real/x;->a(Lcom/google/android/location/os/real/x;Lcom/google/android/location/os/a;Z)V

    .line 469
    invoke-virtual {v1, v7}, Lcom/google/android/location/ap;->e(Z)V

    .line 472
    :cond_1d
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    .line 473
    sget-boolean v0, Lcom/google/android/location/i/a;->b:Z

    if-eqz v0, :cond_4

    const-string v0, "NetworkLocationCallbackRunner"

    const-string v1, "NetworkProvider initialized."

    invoke-static {v0, v1}, Lcom/google/android/location/o/a/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    :cond_1e
    move v1, v7

    .line 458
    goto :goto_f

    .line 478
    :pswitch_1e
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/location/e/aj;

    .line 479
    iget-object v2, p0, Lcom/google/android/location/os/real/ab;->h:Lcom/google/android/location/u;

    iget-object v1, v0, Lcom/google/android/location/e/aj;->a:Ljava/lang/Object;

    check-cast v1, Ljava/io/PrintWriter;

    invoke-virtual {v2}, Lcom/google/android/location/u;->g()V

    iget-object v2, v2, Lcom/google/android/location/u;->a:Lcom/google/android/location/k;

    iget-object v3, v2, Lcom/google/android/location/k;->a:Lcom/google/android/location/activity/k;

    invoke-virtual {v3, v1}, Lcom/google/android/location/activity/k;->a(Ljava/io/PrintWriter;)V

    iget-object v3, v2, Lcom/google/android/location/k;->b:Lcom/google/android/location/activity/be;

    if-eqz v3, :cond_1f

    iget-object v2, v2, Lcom/google/android/location/k;->b:Lcom/google/android/location/activity/be;

    const-string v3, "####VehicleExitDetectorStats Start"

    invoke-virtual {v1, v3}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    iget-object v3, v2, Lcom/google/android/location/activity/be;->c:Lcom/google/android/location/activity/bl;

    const-string v4, "\n"

    iget-object v2, v2, Lcom/google/android/location/activity/be;->a:Lcom/google/android/location/os/bi;

    invoke-interface {v2}, Lcom/google/android/location/os/bi;->c()Lcom/google/android/location/j/b;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/location/j/b;->c()J

    move-result-wide v6

    invoke-virtual {v3, v1, v4, v6, v7}, Lcom/google/android/location/activity/bl;->a(Ljava/io/PrintWriter;Ljava/lang/String;J)V

    const-string v2, "####VehicleExitDetectorStats End"

    invoke-virtual {v1, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 480
    :cond_1f
    iget-object v0, v0, Lcom/google/android/location/e/aj;->b:Ljava/lang/Object;

    check-cast v0, Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    goto/16 :goto_1

    .line 484
    :pswitch_1f
    iget-object v1, p0, Lcom/google/android/location/os/real/ab;->h:Lcom/google/android/location/u;

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/location/e/bh;

    invoke-virtual {v1, v0}, Lcom/google/android/location/u;->a(Lcom/google/android/location/e/bh;)V

    goto/16 :goto_1

    .line 488
    :pswitch_20
    iget-object v0, p0, Lcom/google/android/location/os/real/ab;->h:Lcom/google/android/location/u;

    invoke-virtual {v0}, Lcom/google/android/location/u;->e()V

    goto/16 :goto_1

    .line 492
    :pswitch_21
    invoke-static {}, Lcom/google/android/location/os/real/bf;->a()Lcom/google/android/location/os/real/bf;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/location/os/real/ab;->e:Landroid/os/PowerManager;

    invoke-virtual {v0, v1}, Lcom/google/android/location/os/real/bf;->a(Landroid/os/PowerManager;)Z

    move-result v0

    .line 493
    iget-object v1, p0, Lcom/google/android/location/os/real/ab;->a:Lcom/google/android/location/os/real/x;

    iget-object v1, v1, Lcom/google/android/location/os/real/x;->o:Lcom/google/android/location/os/j;

    invoke-virtual {v1, v0}, Lcom/google/android/location/os/j;->d(Z)V

    .line 494
    iget-object v1, p0, Lcom/google/android/location/os/real/ab;->h:Lcom/google/android/location/u;

    invoke-virtual {v1, v0}, Lcom/google/android/location/u;->b(Z)V

    goto/16 :goto_1

    .line 205
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_0
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_a
        :pswitch_e
        :pswitch_4
        :pswitch_f
        :pswitch_10
        :pswitch_12
        :pswitch_11
        :pswitch_13
        :pswitch_16
        :pswitch_17
        :pswitch_14
        :pswitch_18
        :pswitch_19
        :pswitch_1a
        :pswitch_1b
        :pswitch_1c
        :pswitch_15
        :pswitch_1
        :pswitch_1d
        :pswitch_3
        :pswitch_1e
        :pswitch_1f
        :pswitch_20
        :pswitch_21
    .end packed-switch
.end method
