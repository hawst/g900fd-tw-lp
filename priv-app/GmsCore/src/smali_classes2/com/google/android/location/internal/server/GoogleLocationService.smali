.class public Lcom/google/android/location/internal/server/GoogleLocationService;
.super Landroid/app/Service;
.source "SourceFile"


# static fields
.field public static final a:Lcom/google/android/location/os/at;


# instance fields
.field private b:Lcom/google/android/location/os/j;

.field private final c:Landroid/os/HandlerThread;

.field private d:Lcom/google/android/location/b/at;

.field private e:Lcom/google/android/location/internal/server/m;

.field private f:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 63
    new-instance v0, Lcom/google/android/location/internal/server/e;

    invoke-direct {v0}, Lcom/google/android/location/internal/server/e;-><init>()V

    sput-object v0, Lcom/google/android/location/internal/server/GoogleLocationService;->a:Lcom/google/android/location/os/at;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    .line 52
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 71
    new-instance v0, Landroid/os/HandlerThread;

    const-string v1, "GoogleLocationService"

    const/4 v2, -0x4

    invoke-direct {v0, v1, v2}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;I)V

    iput-object v0, p0, Lcom/google/android/location/internal/server/GoogleLocationService;->c:Landroid/os/HandlerThread;

    .line 74
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/location/internal/server/GoogleLocationService;->f:Z

    return-void
.end method

.method private static a(Landroid/content/Intent;Ljava/lang/String;I)J
    .locals 6

    .prologue
    const-wide/16 v4, -0x1

    .line 338
    invoke-virtual {p0, p1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 339
    int-to-long v0, p2

    .line 350
    :goto_0
    return-wide v0

    .line 341
    :cond_0
    invoke-virtual {p0, p1, v4, v5}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v0

    .line 343
    cmp-long v2, v0, v4

    if-nez v2, :cond_1

    .line 344
    const/4 v0, -0x1

    invoke-virtual {p0, p1, v0}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    int-to-long v0, v0

    .line 346
    :cond_1
    cmp-long v2, v0, v4

    if-nez v2, :cond_2

    .line 347
    int-to-long v0, p2

    .line 349
    :cond_2
    const-wide/32 v2, 0x7fffffff

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v0

    goto :goto_0
.end method

.method private a(Landroid/content/Intent;Z)V
    .locals 18

    .prologue
    .line 198
    const-string v2, "com.google.android.location.internal.EXTRA_PENDING_INTENT"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 199
    const-string v2, "com.google.android.location.internal.EXTRA_PENDING_INTENT"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v3

    check-cast v3, Landroid/app/PendingIntent;

    .line 201
    const-string v2, "com.google.android.location.internal.EXTRA_LOCATION_LOW_POWER"

    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v4}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v8

    .line 202
    const-string v2, "com.google.android.location.internal.EXTRA_LOCATION_REMOVE"

    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v4}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v2

    .line 203
    const-string v4, "com.google.android.location.internal.EXTRA_DEBUG_INFO"

    const/4 v5, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v5}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v6

    .line 204
    const-string v4, "com.google.android.location.internal.EXTRA_PERIOD_MILLIS"

    const/4 v5, -0x1

    move-object/from16 v0, p1

    invoke-static {v0, v4, v5}, Lcom/google/android/location/internal/server/GoogleLocationService;->a(Landroid/content/Intent;Ljava/lang/String;I)J

    move-result-wide v4

    .line 205
    const-string v7, "com.google.android.location.internal.EXTRA_BATCH_DURATION_MILLIS"

    const/4 v9, 0x0

    move-object/from16 v0, p1

    invoke-static {v0, v7, v9}, Lcom/google/android/location/internal/server/GoogleLocationService;->a(Landroid/content/Intent;Ljava/lang/String;I)J

    move-result-wide v12

    .line 206
    const-string v7, "com.google.android.location.internal.EXTRA_LOCATION_FORCE_NOW"

    const/4 v9, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v7, v9}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v7

    .line 208
    const-string v9, "com.google.android.location.internal.EXTRA_LOCATION_TAG"

    move-object/from16 v0, p1

    invoke-virtual {v0, v9}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 209
    if-eqz v3, :cond_1

    .line 210
    if-eqz v2, :cond_6

    .line 211
    sget-boolean v2, Lcom/google/android/location/i/a;->c:Z

    if-eqz v2, :cond_0

    const-string v2, "GoogleLocationService"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Client canceled location update "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Lcom/google/android/location/o/a/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 212
    :cond_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/location/internal/server/GoogleLocationService;->e:Lcom/google/android/location/internal/server/m;

    invoke-virtual {v2, v3}, Lcom/google/android/location/internal/server/m;->a(Landroid/app/PendingIntent;)V

    .line 249
    :cond_1
    :goto_0
    const-string v2, "com.google.android.location.internal.EXTRA_ACTIVITY_PENDING_INTENT"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 250
    const-string v2, "com.google.android.location.internal.EXTRA_ACTIVITY_PENDING_INTENT"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v3

    check-cast v3, Landroid/app/PendingIntent;

    .line 252
    if-eqz v3, :cond_5

    .line 253
    invoke-virtual {v3}, Landroid/app/PendingIntent;->getTargetPackage()Ljava/lang/String;

    move-result-object v2

    .line 254
    sget-boolean v4, Lcom/google/android/location/i/a;->c:Z

    if-eqz v4, :cond_2

    const-string v4, "GoogleLocationService"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "received activity pending intent from "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/android/location/o/a/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 256
    :cond_2
    sget-boolean v4, Lcom/google/android/location/i/a;->f:Z

    if-nez v4, :cond_3

    invoke-static {v2}, Lcom/google/android/location/internal/server/a;->a(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_13

    .line 258
    :cond_3
    const-string v4, "com.google.android.location.internal.EXTRA_ACTIVITY_REMOVE"

    const/4 v5, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v5}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v4

    if-eqz v4, :cond_e

    .line 259
    sget-boolean v2, Lcom/google/android/location/i/a;->c:Z

    if-eqz v2, :cond_4

    const-string v2, "GoogleLocationService"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Client canceled activity detection "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Lcom/google/android/location/o/a/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 260
    :cond_4
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/location/internal/server/GoogleLocationService;->e:Lcom/google/android/location/internal/server/m;

    invoke-virtual {v2, v3}, Lcom/google/android/location/internal/server/m;->b(Landroid/app/PendingIntent;)V

    .line 261
    if-eqz p2, :cond_5

    .line 262
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/location/internal/server/GoogleLocationService;->d:Lcom/google/android/location/b/at;

    move-object/from16 v0, p1

    invoke-virtual {v2, v0}, Lcom/google/android/location/b/at;->b(Landroid/os/Parcelable;)V

    .line 302
    :cond_5
    :goto_1
    return-void

    .line 214
    :cond_6
    const-wide/16 v14, 0x0

    cmp-long v2, v4, v14

    if-ltz v2, :cond_1

    .line 215
    invoke-virtual {v3}, Landroid/app/PendingIntent;->getTargetPackage()Ljava/lang/String;

    move-result-object v11

    .line 224
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/location/internal/server/GoogleLocationService;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v14

    .line 225
    if-eqz v6, :cond_8

    .line 226
    const-string v2, "android.permission.ACCESS_WIFI_STATE"

    invoke-virtual {v14, v2, v11}, Landroid/content/pm/PackageManager;->checkPermission(Ljava/lang/String;Ljava/lang/String;)I

    move-result v2

    if-nez v2, :cond_c

    const/4 v2, 0x1

    .line 228
    :goto_2
    sget-boolean v9, Lcom/google/android/location/i/a;->c:Z

    if-eqz v9, :cond_7

    const-string v9, "GoogleLocationService"

    new-instance v15, Ljava/lang/StringBuilder;

    const-string v16, "package "

    invoke-direct/range {v15 .. v16}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v15, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, " hasWifi? "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v9, v15}, Lcom/google/android/location/o/a/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 229
    :cond_7
    if-eqz v6, :cond_d

    if-eqz v2, :cond_d

    const/4 v2, 0x1

    :goto_3
    move v6, v2

    .line 232
    :cond_8
    sget-boolean v2, Lcom/google/android/location/i/a;->c:Z

    if-eqz v2, :cond_9

    const-string v2, "GoogleLocationService"

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v15, "received pending intent. pendingIntent="

    invoke-direct {v9, v15}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v15, ", packageName="

    invoke-virtual {v9, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v15, ", tag="

    invoke-virtual {v9, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v2, v9}, Lcom/google/android/location/o/a/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 234
    :cond_9
    const-wide/16 v16, 0x3e8

    div-long v4, v4, v16

    long-to-int v4, v4

    .line 235
    const-wide/16 v16, 0x3e8

    div-long v12, v12, v16

    long-to-int v5, v12

    .line 236
    const-string v2, "com.google.android.location.internal.EXTRA_LOCATION_WORK_SOURCE"

    move-object/from16 v0, p1

    invoke-static {v0, v2}, Lcom/google/android/location/o/n;->a(Landroid/content/Intent;Ljava/lang/String;)Lcom/google/android/location/o/n;

    move-result-object v9

    .line 238
    if-eqz v9, :cond_b

    const-string v2, "android.permission.UPDATE_DEVICE_STATS"

    invoke-virtual {v14, v2, v11}, Landroid/content/pm/PackageManager;->checkPermission(Ljava/lang/String;Ljava/lang/String;)I

    move-result v2

    if-eqz v2, :cond_b

    .line 240
    sget-boolean v2, Lcom/google/android/location/i/a;->c:Z

    if-eqz v2, :cond_a

    const-string v2, "GoogleLocationService"

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v12, "package "

    invoke-direct {v9, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v11, " doesn\'t have permission for WorkSource"

    invoke-virtual {v9, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v2, v9}, Lcom/google/android/location/o/a/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 241
    :cond_a
    const/4 v9, 0x0

    .line 243
    :cond_b
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/location/internal/server/GoogleLocationService;->e:Lcom/google/android/location/internal/server/m;

    invoke-virtual/range {v2 .. v10}, Lcom/google/android/location/internal/server/m;->a(Landroid/app/PendingIntent;IIZZZLcom/google/android/location/o/n;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 226
    :cond_c
    const/4 v2, 0x0

    goto/16 :goto_2

    .line 229
    :cond_d
    const/4 v2, 0x0

    goto :goto_3

    .line 266
    :cond_e
    const-string v4, "com.google.android.location.internal.EXTRA_ACTIVITY_PERIOD_MILLIS"

    const v5, 0x2bf20

    move-object/from16 v0, p1

    invoke-static {v0, v4, v5}, Lcom/google/android/location/internal/server/GoogleLocationService;->a(Landroid/content/Intent;Ljava/lang/String;I)J

    move-result-wide v10

    .line 269
    const-string v4, "com.google.android.location.internal.EXTRA_ACTIVITY_FORCE_DETECTION_NOW"

    const/4 v5, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v5}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v5

    .line 271
    sget-boolean v4, Lcom/google/android/location/i/a;->c:Z

    if-eqz v4, :cond_f

    const-string v4, "GoogleLocationService"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "Adding/updating activity detection client: periodMillis: "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " force: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v6}, Lcom/google/android/location/o/a/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 273
    :cond_f
    const-string v4, "com.google.android.location.internal.EXTRA_IS_FROM_FIRST_PARTY"

    const/4 v6, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v6}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v6

    .line 275
    const-string v4, "com.google.android.location.internal.EXTRA_LOCATION_WORK_SOURCE"

    move-object/from16 v0, p1

    invoke-static {v0, v4}, Lcom/google/android/location/o/n;->a(Landroid/content/Intent;Ljava/lang/String;)Lcom/google/android/location/o/n;

    move-result-object v7

    .line 277
    if-eqz v7, :cond_11

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/location/internal/server/GoogleLocationService;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v4

    const-string v8, "android.permission.UPDATE_DEVICE_STATS"

    invoke-virtual {v3}, Landroid/app/PendingIntent;->getTargetPackage()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v4, v8, v9}, Landroid/content/pm/PackageManager;->checkPermission(Ljava/lang/String;Ljava/lang/String;)I

    move-result v4

    if-eqz v4, :cond_11

    .line 281
    sget-boolean v4, Lcom/google/android/location/i/a;->c:Z

    if-eqz v4, :cond_10

    const-string v4, "GoogleLocationService"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "package "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v7, " doesn\'t have permission for WorkSource"

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v4, v2}, Lcom/google/android/location/o/a/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 282
    :cond_10
    const/4 v7, 0x0

    .line 284
    :cond_11
    const-string v2, "com.google.android.location.internal.EXTRA_ACTIVITY_TAG"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 285
    if-nez v8, :cond_12

    .line 286
    const-string v8, ""

    .line 288
    :cond_12
    const-string v2, "com.google.android.location.internal.EXTRA_NONDEFAULT_ACTIVITY_TYPES"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getIntArrayExtra(Ljava/lang/String;)[I

    move-result-object v9

    .line 290
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/location/internal/server/GoogleLocationService;->e:Lcom/google/android/location/internal/server/m;

    const-wide/16 v12, 0x3e8

    div-long/2addr v10, v12

    long-to-int v4, v10

    invoke-virtual/range {v2 .. v9}, Lcom/google/android/location/internal/server/m;->a(Landroid/app/PendingIntent;IZZLcom/google/android/location/o/n;Ljava/lang/String;[I)V

    .line 292
    if-eqz p2, :cond_5

    .line 293
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/location/internal/server/GoogleLocationService;->d:Lcom/google/android/location/b/at;

    move-object/from16 v0, p1

    invoke-virtual {v2, v0}, Lcom/google/android/location/b/at;->a(Landroid/os/Parcelable;)V

    goto/16 :goto_1

    .line 297
    :cond_13
    sget-boolean v3, Lcom/google/android/location/i/a;->c:Z

    if-eqz v3, :cond_5

    const-string v3, "GoogleLocationService"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Rejecting request for activity detection. Application not in whitelist. Please contact lbs-team. "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v2}, Lcom/google/android/location/o/a/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1
.end method

.method private declared-synchronized b()Z
    .locals 1

    .prologue
    .line 371
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/location/internal/server/GoogleLocationService;->f:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public final declared-synchronized a()V
    .locals 1

    .prologue
    .line 363
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/location/internal/server/GoogleLocationService;->d:Lcom/google/android/location/b/at;

    invoke-virtual {v0}, Lcom/google/android/location/b/at;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 364
    invoke-virtual {p0}, Lcom/google/android/location/internal/server/GoogleLocationService;->stopSelf()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 368
    :goto_0
    monitor-exit p0

    return-void

    .line 366
    :cond_0
    const/4 v0, 0x1

    :try_start_1
    iput-boolean v0, p0, Lcom/google/android/location/internal/server/GoogleLocationService;->f:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 363
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected declared-synchronized dump(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    .locals 7

    .prologue
    .line 315
    monitor-enter p0

    :try_start_0
    sget-boolean v0, Lcom/google/android/location/i/a;->c:Z

    if-eqz v0, :cond_0

    const-string v0, "GoogleLocationService"

    const-string v1, "dump"

    invoke-static {v0, v1}, Lcom/google/android/location/o/a/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 316
    :cond_0
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    .line 317
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    .line 318
    new-instance v4, Ljava/text/SimpleDateFormat;

    const-string v5, "MM-dd HH:mm:ss.SSS"

    sget-object v6, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-direct {v4, v5, v6}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    .line 319
    new-instance v5, Ljava/util/Date;

    invoke-direct {v5, v2, v3}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v4, v5}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v5

    .line 320
    const-string v6, "elapsedRealtime "

    invoke-virtual {p2, v6}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 321
    invoke-virtual {p2, v0, v1}, Ljava/io/PrintWriter;->print(J)V

    .line 322
    const-string v6, " is time "

    invoke-virtual {p2, v6}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 323
    invoke-virtual {p2, v5}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 324
    invoke-static {p2}, Lcom/google/android/location/d/a;->a(Ljava/io/PrintWriter;)V

    .line 325
    iget-object v5, p0, Lcom/google/android/location/internal/server/GoogleLocationService;->e:Lcom/google/android/location/internal/server/m;

    invoke-virtual {v5, p2}, Lcom/google/android/location/internal/server/m;->b(Ljava/io/PrintWriter;)V

    .line 326
    iget-object v5, p0, Lcom/google/android/location/internal/server/GoogleLocationService;->e:Lcom/google/android/location/internal/server/m;

    invoke-virtual {v5, p2}, Lcom/google/android/location/internal/server/m;->a(Ljava/io/PrintWriter;)V

    .line 327
    iget-object v5, p0, Lcom/google/android/location/internal/server/GoogleLocationService;->e:Lcom/google/android/location/internal/server/m;

    invoke-virtual {v5, v4, p2}, Lcom/google/android/location/internal/server/m;->a(Ljava/text/Format;Ljava/io/PrintWriter;)V

    .line 328
    iget-object v5, p0, Lcom/google/android/location/internal/server/GoogleLocationService;->e:Lcom/google/android/location/internal/server/m;

    invoke-virtual {v5, p2}, Lcom/google/android/location/internal/server/m;->c(Ljava/io/PrintWriter;)V

    .line 331
    invoke-virtual {p2}, Ljava/io/PrintWriter;->flush()V

    .line 332
    iget-object v5, p0, Lcom/google/android/location/internal/server/GoogleLocationService;->b:Lcom/google/android/location/os/j;

    if-eqz v5, :cond_1

    .line 333
    iget-object v5, p0, Lcom/google/android/location/internal/server/GoogleLocationService;->b:Lcom/google/android/location/os/j;

    sub-long v0, v2, v0

    invoke-virtual {v5, v4, v0, v1, p2}, Lcom/google/android/location/os/j;->a(Ljava/text/Format;JLjava/io/PrintWriter;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 335
    :cond_1
    monitor-exit p0

    return-void

    .line 315
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1

    .prologue
    .line 172
    monitor-enter p0

    const/4 v0, 0x0

    monitor-exit p0

    return-object v0
.end method

.method public declared-synchronized onCreate()V
    .locals 8

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 91
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/location/internal/server/GoogleLocationService;->b:Lcom/google/android/location/os/j;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    .line 168
    :goto_0
    monitor-exit p0

    return-void

    .line 95
    :cond_0
    :try_start_1
    invoke-static {}, Lcom/google/android/location/os/real/bf;->a()Lcom/google/android/location/os/real/bf;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/location/os/real/bf;->c()V

    .line 96
    invoke-static {p0}, Lcom/google/android/gms/common/a/d;->a(Landroid/content/Context;)V

    .line 97
    iget-object v0, p0, Lcom/google/android/location/internal/server/GoogleLocationService;->c:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    .line 102
    new-instance v0, Lcom/google/android/location/b/at;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    new-instance v4, Lcom/google/android/location/internal/server/f;

    invoke-direct {v4, p0}, Lcom/google/android/location/internal/server/f;-><init>(Lcom/google/android/location/internal/server/GoogleLocationService;)V

    invoke-direct {v0, p0, v3, v4}, Lcom/google/android/location/b/at;-><init>(Landroid/content/Context;Ljava/lang/Class;Lcom/google/android/location/b/ax;)V

    iput-object v0, p0, Lcom/google/android/location/internal/server/GoogleLocationService;->d:Lcom/google/android/location/b/at;

    .line 130
    new-instance v0, Lcom/google/android/location/internal/server/m;

    iget-object v3, p0, Lcom/google/android/location/internal/server/GoogleLocationService;->c:Landroid/os/HandlerThread;

    invoke-virtual {v3}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/location/internal/server/GoogleLocationService;->d:Lcom/google/android/location/b/at;

    invoke-direct {v0, p0, v3, v4}, Lcom/google/android/location/internal/server/m;-><init>(Lcom/google/android/location/internal/server/GoogleLocationService;Landroid/os/Looper;Lcom/google/android/location/b/at;)V

    iput-object v0, p0, Lcom/google/android/location/internal/server/GoogleLocationService;->e:Lcom/google/android/location/internal/server/m;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 134
    :try_start_2
    const-string v0, "nlp_debug_log"

    invoke-virtual {p0, v0}, Lcom/google/android/location/internal/server/GoogleLocationService;->openFileInput(Ljava/lang/String;)Ljava/io/FileInputStream;

    move-result-object v3

    .line 137
    const/4 v0, 0x1

    .line 138
    invoke-virtual {v3}, Ljava/io/FileInputStream;->close()V
    :try_end_2
    .catch Ljava/io/FileNotFoundException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 145
    :goto_1
    if-eqz v0, :cond_2

    .line 147
    :try_start_3
    new-instance v2, Ljava/io/BufferedOutputStream;

    const-string v0, "nlp_debug_log"

    const v3, 0x8000

    invoke-virtual {p0, v0, v3}, Lcom/google/android/location/internal/server/GoogleLocationService;->openFileOutput(Ljava/lang/String;I)Ljava/io/FileOutputStream;

    move-result-object v0

    invoke-direct {v2, v0}, Ljava/io/BufferedOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 148
    new-instance v0, Ljava/io/PrintWriter;

    invoke-direct {v0, v2}, Ljava/io/PrintWriter;-><init>(Ljava/io/OutputStream;)V
    :try_end_3
    .catch Ljava/io/FileNotFoundException; {:try_start_3 .. :try_end_3} :catch_2
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-object v3, v0

    .line 154
    :goto_2
    :try_start_4
    sget-object v0, Lcom/google/android/location/d/a;->s:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 155
    new-instance v2, Ljava/io/File;

    invoke-virtual {p0}, Lcom/google/android/location/internal/server/GoogleLocationService;->getCacheDir()Ljava/io/File;

    move-result-object v0

    const-string v4, "compactlog"

    invoke-direct {v2, v0, v4}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 156
    invoke-virtual {v2}, Ljava/io/File;->mkdir()Z

    .line 157
    new-instance v0, Lcom/google/android/location/os/b;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v6

    sub-long/2addr v4, v6

    invoke-direct {v0, v2, v4, v5}, Lcom/google/android/location/os/b;-><init>(Ljava/io/File;J)V

    move-object v2, v0

    .line 159
    :goto_3
    new-instance v4, Lcom/google/android/location/os/j;

    sget-object v5, Lcom/google/android/location/internal/server/GoogleLocationService;->a:Lcom/google/android/location/os/at;

    invoke-static {}, Lcom/google/android/location/os/real/bf;->a()Lcom/google/android/location/os/real/bf;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/location/os/real/bf;->b()Z

    move-result v0

    if-eqz v0, :cond_3

    move-object v0, v1

    :goto_4
    invoke-direct {v4, v5, v0, v3, v2}, Lcom/google/android/location/os/j;-><init>(Lcom/google/android/location/os/at;Lcom/google/android/location/o/a/b;Ljava/io/PrintWriter;Lcom/google/android/location/os/b;)V

    iput-object v4, p0, Lcom/google/android/location/internal/server/GoogleLocationService;->b:Lcom/google/android/location/os/j;

    .line 164
    iget-object v0, p0, Lcom/google/android/location/internal/server/GoogleLocationService;->e:Lcom/google/android/location/internal/server/m;

    iget-object v1, p0, Lcom/google/android/location/internal/server/GoogleLocationService;->b:Lcom/google/android/location/os/j;

    invoke-virtual {v0, v1}, Lcom/google/android/location/internal/server/m;->a(Lcom/google/android/location/os/j;)V

    .line 165
    new-instance v0, Lcom/google/android/location/os/real/a;

    iget-object v1, p0, Lcom/google/android/location/internal/server/GoogleLocationService;->b:Lcom/google/android/location/os/j;

    invoke-direct {v0, v1}, Lcom/google/android/location/os/real/a;-><init>(Lcom/google/android/location/os/j;)V

    invoke-static {v0}, Lcom/google/android/location/o/a/a;->a(Lcom/google/android/location/o/a/b;)V

    .line 166
    sget-boolean v0, Lcom/google/android/location/i/a;->c:Z

    if-eqz v0, :cond_1

    const-string v0, "GoogleLocationService"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "onCreate "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/location/o/a/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 167
    :cond_1
    iget-object v0, p0, Lcom/google/android/location/internal/server/GoogleLocationService;->e:Lcom/google/android/location/internal/server/m;

    invoke-virtual {v0}, Lcom/google/android/location/internal/server/m;->b()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_0

    .line 91
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 140
    :catch_0
    move-exception v0

    move v0, v2

    .line 143
    goto/16 :goto_1

    .line 142
    :catch_1
    move-exception v0

    move v0, v2

    goto/16 :goto_1

    .line 150
    :catch_2
    move-exception v0

    :try_start_5
    sget-boolean v0, Lcom/google/android/location/i/a;->e:Z

    if-eqz v0, :cond_2

    const-string v0, "GoogleLocationService"

    const-string v2, "debug log file missing for output!?"

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    move-object v3, v1

    goto/16 :goto_2

    .line 159
    :cond_3
    new-instance v0, Lcom/google/android/location/os/real/a;

    invoke-direct {v0}, Lcom/google/android/location/os/real/a;-><init>()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto :goto_4

    :cond_4
    move-object v2, v1

    goto :goto_3
.end method

.method public declared-synchronized onDestroy()V
    .locals 2

    .prologue
    .line 306
    monitor-enter p0

    :try_start_0
    sget-boolean v0, Lcom/google/android/location/i/a;->c:Z

    if-eqz v0, :cond_0

    const-string v0, "GoogleLocationService"

    const-string v1, "onDestroy"

    invoke-static {v0, v1}, Lcom/google/android/location/o/a/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 307
    :cond_0
    iget-object v0, p0, Lcom/google/android/location/internal/server/GoogleLocationService;->e:Lcom/google/android/location/internal/server/m;

    invoke-virtual {v0}, Lcom/google/android/location/internal/server/m;->c()V

    .line 309
    sget-boolean v0, Lcom/google/android/location/i/a;->b:Z

    if-eqz v0, :cond_1

    const-string v0, "GoogleLocationService"

    const-string v1, "unregistering logger"

    invoke-static {v0, v1}, Lcom/google/android/location/o/a/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 310
    :cond_1
    const/4 v0, 0x0

    invoke-static {v0}, Lcom/google/android/location/o/a/a;->a(Lcom/google/android/location/o/a/b;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 311
    monitor-exit p0

    return-void

    .line 306
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized onStart(Landroid/content/Intent;I)V
    .locals 4

    .prologue
    .line 177
    monitor-enter p0

    if-nez p1, :cond_1

    .line 195
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 180
    :cond_1
    :try_start_0
    invoke-static {p1}, Lcom/google/android/location/b/at;->a(Landroid/content/Intent;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 181
    sget-boolean v0, Lcom/google/android/location/i/a;->c:Z

    if-eqz v0, :cond_2

    const-string v0, "GoogleLocationService"

    const-string v1, "Received SystemMemoryCache init intent."

    invoke-static {v0, v1}, Lcom/google/android/location/o/a/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 182
    :cond_2
    iget-object v0, p0, Lcom/google/android/location/internal/server/GoogleLocationService;->d:Lcom/google/android/location/b/at;

    invoke-virtual {v0, p1}, Lcom/google/android/location/b/at;->c(Landroid/content/Intent;)V

    .line 183
    invoke-direct {p0}, Lcom/google/android/location/internal/server/GoogleLocationService;->b()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 184
    invoke-virtual {p0}, Lcom/google/android/location/internal/server/GoogleLocationService;->a()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 177
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 187
    :cond_3
    :try_start_1
    iget-object v0, p0, Lcom/google/android/location/internal/server/GoogleLocationService;->d:Lcom/google/android/location/b/at;

    invoke-virtual {v0}, Lcom/google/android/location/b/at;->c()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Intent;

    .line 188
    sget-boolean v2, Lcom/google/android/location/i/a;->c:Z

    if-eqz v2, :cond_4

    const-string v2, "GoogleLocationService"

    const-string v3, "Re-adding cached client."

    invoke-static {v2, v3}, Lcom/google/android/location/o/a/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 189
    :cond_4
    const/4 v2, 0x0

    invoke-direct {p0, v0, v2}, Lcom/google/android/location/internal/server/GoogleLocationService;->a(Landroid/content/Intent;Z)V

    goto :goto_1

    .line 191
    :cond_5
    sget-boolean v0, Lcom/google/android/location/i/a;->c:Z

    if-eqz v0, :cond_0

    const-string v0, "GoogleLocationService"

    const-string v1, "Finished initializing clients from pending intent cache."

    invoke-static {v0, v1}, Lcom/google/android/location/o/a/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 193
    :cond_6
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lcom/google/android/location/internal/server/GoogleLocationService;->a(Landroid/content/Intent;Z)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0
.end method
