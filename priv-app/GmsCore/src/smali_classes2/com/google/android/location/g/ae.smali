.class public final Lcom/google/android/location/g/ae;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public final a:Lcom/google/android/location/d/k;

.field public final b:Lcom/google/android/location/g/q;

.field public c:Lcom/google/android/location/g/ag;

.field public final d:Lcom/google/t/b/b/a/d;

.field public e:Z

.field public f:Z

.field private final g:Lcom/google/android/location/e/bd;

.field private final h:Lcom/google/android/location/e/bd;

.field private final i:Lcom/google/android/location/b/ba;

.field private final j:Lcom/google/android/location/b/ai;

.field private final k:Lcom/google/android/location/g/ah;

.field private final l:Lcom/google/android/location/j/b;


# direct methods
.method private constructor <init>(Lcom/google/android/location/g/aa;Lcom/google/android/location/g/g;Lcom/google/android/location/b/ai;Lcom/google/android/location/b/ba;Lcom/google/android/location/g/q;Lcom/google/t/b/b/a/d;Lcom/google/android/location/g/ag;Lcom/google/android/location/g/ah;Lcom/google/android/location/j/b;)V
    .locals 1

    .prologue
    .line 165
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 73
    invoke-static {}, Lcom/google/android/location/d/k;->a()Lcom/google/android/location/d/k;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/g/ae;->a:Lcom/google/android/location/d/k;

    .line 85
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/location/g/ae;->e:Z

    .line 91
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/location/g/ae;->f:Z

    .line 166
    iput-object p1, p0, Lcom/google/android/location/g/ae;->g:Lcom/google/android/location/e/bd;

    .line 167
    iput-object p2, p0, Lcom/google/android/location/g/ae;->h:Lcom/google/android/location/e/bd;

    .line 168
    iput-object p3, p0, Lcom/google/android/location/g/ae;->j:Lcom/google/android/location/b/ai;

    .line 169
    iput-object p5, p0, Lcom/google/android/location/g/ae;->b:Lcom/google/android/location/g/q;

    .line 170
    iput-object p4, p0, Lcom/google/android/location/g/ae;->i:Lcom/google/android/location/b/ba;

    .line 171
    iput-object p6, p0, Lcom/google/android/location/g/ae;->d:Lcom/google/t/b/b/a/d;

    .line 172
    iput-object p7, p0, Lcom/google/android/location/g/ae;->c:Lcom/google/android/location/g/ag;

    .line 173
    iput-object p8, p0, Lcom/google/android/location/g/ae;->k:Lcom/google/android/location/g/ah;

    .line 174
    iput-object p9, p0, Lcom/google/android/location/g/ae;->l:Lcom/google/android/location/j/b;

    .line 175
    return-void
.end method

.method private static a(Ljava/util/Map;)Lcom/google/android/location/e/bb;
    .locals 5

    .prologue
    .line 400
    const/4 v1, -0x1

    .line 401
    sget-object v0, Lcom/google/android/location/e/bb;->a:Lcom/google/android/location/e/bb;

    .line 402
    invoke-interface {p0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v2, v1

    move-object v1, v0

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 403
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/e/az;

    iget-object v0, v0, Lcom/google/android/location/e/az;->b:Lcom/google/android/location/e/bb;

    .line 404
    invoke-virtual {v0}, Lcom/google/android/location/e/bb;->ordinal()I

    move-result v4

    if-le v4, v2, :cond_2

    .line 405
    invoke-static {}, Lcom/google/android/location/e/bb;->a()Lcom/google/android/location/e/bb;

    move-result-object v1

    if-ne v0, v1, :cond_0

    .line 413
    :goto_1
    return-object v0

    .line 409
    :cond_0
    invoke-virtual {v0}, Lcom/google/android/location/e/bb;->ordinal()I

    move-result v1

    :goto_2
    move v2, v1

    move-object v1, v0

    .line 412
    goto :goto_0

    :cond_1
    move-object v0, v1

    .line 413
    goto :goto_1

    :cond_2
    move-object v0, v1

    move v1, v2

    goto :goto_2
.end method

.method private static a(Lcom/google/android/location/e/bf;Lcom/google/android/location/e/bf;)Lcom/google/android/location/e/bf;
    .locals 2

    .prologue
    .line 331
    if-eqz p0, :cond_0

    iget-object v0, p0, Lcom/google/android/location/e/bf;->c:Lcom/google/android/location/e/al;

    if-nez v0, :cond_2

    :cond_0
    move-object p0, p1

    .line 340
    :cond_1
    :goto_0
    return-object p0

    .line 334
    :cond_2
    if-eqz p1, :cond_1

    iget-object v0, p1, Lcom/google/android/location/e/bf;->c:Lcom/google/android/location/e/al;

    if-eqz v0, :cond_1

    .line 337
    iget-object v0, p0, Lcom/google/android/location/e/bf;->c:Lcom/google/android/location/e/al;

    iget v0, v0, Lcom/google/android/location/e/al;->f:I

    iget-object v1, p1, Lcom/google/android/location/e/bf;->c:Lcom/google/android/location/e/al;

    iget v1, v1, Lcom/google/android/location/e/al;->f:I

    if-le v0, v1, :cond_1

    move-object p0, p1

    .line 340
    goto :goto_0
.end method

.method public static a(Lcom/google/android/location/g/w;Lcom/google/android/location/b/ba;Lcom/google/android/location/b/ai;Lcom/google/android/location/os/bi;)Lcom/google/android/location/g/ae;
    .locals 10

    .prologue
    .line 104
    new-instance v1, Lcom/google/android/location/g/aa;

    invoke-direct {v1}, Lcom/google/android/location/g/aa;-><init>()V

    .line 109
    sget-object v0, Lcom/google/android/location/d/a;->k:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 118
    invoke-interface {p3}, Lcom/google/android/location/os/bi;->c()Lcom/google/android/location/j/b;

    move-result-object v0

    invoke-interface {p3}, Lcom/google/android/location/os/bi;->e()Lcom/google/android/location/j/e;

    move-result-object v2

    invoke-interface {p3}, Lcom/google/android/location/os/bi;->f()Lcom/google/android/location/j/j;

    move-result-object v3

    invoke-interface {p3}, Lcom/google/android/location/os/bi;->B()Lcom/google/android/location/j/f;

    move-result-object v4

    invoke-interface {p3}, Lcom/google/android/location/os/bi;->d()Lcom/google/android/location/j/d;

    move-result-object v5

    invoke-interface {v5}, Lcom/google/android/location/j/d;->c()[B

    move-result-object v5

    invoke-static {v0, v2, v3, v4, v5}, Lcom/google/t/b/b/a/b;->a(Lcom/google/android/location/j/b;Lcom/google/android/location/j/e;Lcom/google/android/location/j/j;Lcom/google/android/location/j/f;[B)Lcom/google/t/b/b/a/b;

    move-result-object v0

    .line 121
    new-instance v6, Lcom/google/t/b/b/a/d;

    invoke-direct {v6, v0}, Lcom/google/t/b/b/a/d;-><init>(Lcom/google/t/b/b/a/b;)V

    .line 127
    :goto_0
    :try_start_0
    const-string v0, "nlp_metricmodel.bin"

    invoke-interface {p3, v0}, Lcom/google/android/location/os/bi;->a(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/location/g/p;->a(Ljava/io/InputStream;)Lcom/google/android/location/g/p;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/location/g/p;->a()Ljava/util/List;

    move-result-object v0

    .line 129
    sget-boolean v2, Lcom/google/android/location/i/a;->b:Z

    if-eqz v2, :cond_0

    const-string v2, "WifiLocator"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Metric-model profile has "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " entries."

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/location/o/a/a;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    .line 137
    :cond_0
    :goto_1
    new-instance v2, Lcom/google/android/location/g/g;

    invoke-direct {v2, v0}, Lcom/google/android/location/g/g;-><init>(Ljava/util/List;)V

    .line 140
    new-instance v5, Lcom/google/android/location/g/q;

    invoke-direct {v5, p0}, Lcom/google/android/location/g/q;-><init>(Lcom/google/android/location/g/s;)V

    .line 141
    invoke-static {p3}, Lcom/google/android/location/g/ag;->a(Lcom/google/android/location/os/bi;)Lcom/google/android/location/g/ag;

    move-result-object v7

    .line 142
    new-instance v8, Lcom/google/android/location/g/ah;

    invoke-interface {p3}, Lcom/google/android/location/os/bi;->c()Lcom/google/android/location/j/b;

    move-result-object v0

    invoke-direct {v8, v0}, Lcom/google/android/location/g/ah;-><init>(Lcom/google/android/location/j/b;)V

    .line 144
    new-instance v0, Lcom/google/android/location/g/ae;

    invoke-interface {p3}, Lcom/google/android/location/os/bi;->c()Lcom/google/android/location/j/b;

    move-result-object v9

    move-object v3, p2

    move-object v4, p1

    invoke-direct/range {v0 .. v9}, Lcom/google/android/location/g/ae;-><init>(Lcom/google/android/location/g/aa;Lcom/google/android/location/g/g;Lcom/google/android/location/b/ai;Lcom/google/android/location/b/ba;Lcom/google/android/location/g/q;Lcom/google/t/b/b/a/d;Lcom/google/android/location/g/ag;Lcom/google/android/location/g/ah;Lcom/google/android/location/j/b;)V

    return-object v0

    .line 123
    :cond_1
    const/4 v6, 0x0

    goto :goto_0

    .line 130
    :catch_0
    move-exception v0

    .line 133
    sget-boolean v2, Lcom/google/android/location/i/a;->e:Z

    if-eqz v2, :cond_2

    const-string v2, "WifiLocator"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Could not load metric model: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/google/android/location/o/a/a;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 134
    :cond_2
    new-instance v0, Ljava/util/ArrayList;

    const/4 v2, 0x0

    invoke-direct {v0, v2}, Ljava/util/ArrayList;-><init>(I)V

    goto :goto_1
.end method

.method private a(Ljava/util/Set;Lcom/google/android/location/e/al;)Lcom/google/android/location/g/af;
    .locals 22

    .prologue
    .line 460
    new-instance v13, Ljava/util/HashMap;

    invoke-direct {v13}, Ljava/util/HashMap;-><init>()V

    .line 461
    new-instance v14, Ljava/util/HashMap;

    invoke-direct {v14}, Ljava/util/HashMap;-><init>()V

    .line 462
    const/4 v2, 0x0

    .line 464
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/location/g/ae;->l:Lcom/google/android/location/j/b;

    invoke-interface {v3}, Lcom/google/android/location/j/b;->b()J

    move-result-wide v16

    .line 465
    invoke-interface/range {p1 .. p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v15

    move v11, v2

    :cond_0
    :goto_0
    invoke-interface {v15}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-interface {v15}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    move-object v10, v2

    check-cast v10, Ljava/lang/Long;

    .line 466
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/location/g/ae;->i:Lcom/google/android/location/b/ba;

    move-wide/from16 v0, v16

    invoke-virtual {v2, v10, v0, v1}, Lcom/google/android/location/b/ba;->a(Ljava/lang/Object;J)Lcom/google/android/location/b/a;

    move-result-object v18

    .line 468
    if-eqz v18, :cond_3

    .line 469
    move-object/from16 v0, v18

    iget-object v2, v0, Lcom/google/android/location/b/a;->d:Ljava/lang/Object;

    move-object v4, v2

    check-cast v4, Lcom/google/android/location/e/az;

    .line 470
    invoke-virtual {v4}, Lcom/google/android/location/e/az;->b()Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, v4, Lcom/google/android/location/e/az;->b:Lcom/google/android/location/e/bb;

    sget-object v3, Lcom/google/android/location/e/bb;->a:Lcom/google/android/location/e/bb;

    if-eq v2, v3, :cond_2

    .line 471
    const/4 v12, 0x1

    .line 473
    if-eqz p2, :cond_e

    .line 474
    move-object/from16 v0, p2

    iget v2, v0, Lcom/google/android/location/e/al;->f:I

    int-to-double v2, v2

    const-wide v6, 0x408f400000000000L    # 1000.0

    div-double v20, v2, v6

    .line 475
    iget v2, v4, Lcom/google/android/location/e/az;->d:I

    int-to-double v2, v2

    iget v4, v4, Lcom/google/android/location/e/az;->e:I

    int-to-double v4, v4

    move-object/from16 v0, p2

    iget v6, v0, Lcom/google/android/location/e/al;->d:I

    int-to-double v6, v6

    move-object/from16 v0, p2

    iget v8, v0, Lcom/google/android/location/e/al;->e:I

    int-to-double v8, v8

    invoke-static/range {v2 .. v9}, Lcom/google/android/location/e/p;->a(DDDD)D

    move-result-wide v2

    .line 478
    cmpl-double v2, v2, v20

    if-lez v2, :cond_e

    .line 479
    const/4 v2, 0x0

    .line 491
    :goto_1
    if-eqz v2, :cond_1

    .line 492
    move-object/from16 v0, v18

    iget-object v2, v0, Lcom/google/android/location/b/a;->d:Ljava/lang/Object;

    invoke-interface {v13, v10, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 494
    :cond_1
    move-object/from16 v0, v18

    iget-object v2, v0, Lcom/google/android/location/b/a;->d:Ljava/lang/Object;

    invoke-interface {v14, v10, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 496
    :cond_2
    invoke-virtual {v4}, Lcom/google/android/location/e/az;->e()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 502
    invoke-virtual {v4}, Lcom/google/android/location/e/az;->c()V

    goto :goto_0

    .line 505
    :cond_3
    add-int/lit8 v2, v11, 0x1

    move v11, v2

    .line 507
    goto :goto_0

    .line 516
    :cond_4
    if-nez p2, :cond_6

    invoke-interface {v13}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/location/g/ae;->a(Ljava/util/Collection;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 517
    sget-boolean v2, Lcom/google/android/location/i/a;->b:Z

    if-eqz v2, :cond_5

    const-string v2, "WifiLocator"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "AP set of size "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v13}, Ljava/util/Map;->size()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " has no overlap"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/location/o/a/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 518
    :cond_5
    new-instance v2, Lcom/google/android/location/g/af;

    sget-object v3, Lcom/google/android/location/e/ab;->b:Lcom/google/android/location/e/ab;

    const/4 v4, 0x0

    move-object/from16 v0, p0

    invoke-direct {v2, v0, v3, v4, v14}, Lcom/google/android/location/g/af;-><init>(Lcom/google/android/location/g/ae;Lcom/google/android/location/e/ab;Ljava/util/Map;Ljava/util/Map;)V

    .line 591
    :goto_2
    return-object v2

    .line 530
    :cond_6
    invoke-interface {v13}, Ljava/util/Map;->size()I

    move-result v2

    if-nez v2, :cond_a

    invoke-interface {v14}, Ljava/util/Map;->size()I

    move-result v2

    const/4 v3, 0x1

    if-le v2, v3, :cond_a

    .line 531
    invoke-interface {v14}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/location/g/ae;->a(Ljava/util/Set;)Z

    move-result v2

    if-eqz v2, :cond_9

    .line 533
    sget-boolean v2, Lcom/google/android/location/i/a;->b:Z

    if-eqz v2, :cond_7

    const-string v2, "WifiLocator"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Diversity, rehabilitating ap list of size "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v14}, Ljava/util/Map;->size()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/location/o/a/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 539
    :cond_7
    invoke-interface {v14}, Ljava/util/Map;->size()I

    move-result v2

    const/4 v3, 0x1

    if-le v2, v3, :cond_8

    invoke-interface {v14}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/location/g/ae;->a(Ljava/util/Collection;)Z

    move-result v2

    if-eqz v2, :cond_8

    .line 541
    new-instance v2, Lcom/google/android/location/g/af;

    sget-object v3, Lcom/google/android/location/e/ab;->b:Lcom/google/android/location/e/ab;

    const/4 v4, 0x0

    move-object/from16 v0, p0

    invoke-direct {v2, v0, v3, v4, v14}, Lcom/google/android/location/g/af;-><init>(Lcom/google/android/location/g/ae;Lcom/google/android/location/e/ab;Ljava/util/Map;Ljava/util/Map;)V

    goto :goto_2

    .line 544
    :cond_8
    invoke-interface {v13, v14}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    .line 545
    invoke-interface {v14}, Ljava/util/Map;->clear()V

    .line 561
    :cond_9
    :goto_3
    invoke-interface {v13}, Ljava/util/Map;->size()I

    move-result v2

    .line 565
    if-lez v2, :cond_c

    .line 568
    const/4 v3, 0x5

    invoke-static {v3, v11}, Ljava/lang/Math;->min(II)I

    move-result v3

    if-lt v2, v3, :cond_b

    .line 570
    const-string v3, "Good cache hits. Computing WiFi location locally"

    invoke-interface/range {p1 .. p1}, Ljava/util/Set;->size()I

    move-result v4

    invoke-static {v3, v2, v11, v4}, Lcom/google/android/location/g/ae;->a(Ljava/lang/String;III)V

    .line 572
    new-instance v2, Lcom/google/android/location/g/af;

    sget-object v3, Lcom/google/android/location/e/ab;->a:Lcom/google/android/location/e/ab;

    move-object/from16 v0, p0

    invoke-direct {v2, v0, v3, v13, v14}, Lcom/google/android/location/g/af;-><init>(Lcom/google/android/location/g/ae;Lcom/google/android/location/e/ab;Ljava/util/Map;Ljava/util/Map;)V

    goto :goto_2

    .line 547
    :cond_a
    invoke-interface {v13}, Ljava/util/Map;->size()I

    move-result v2

    const/4 v3, 0x1

    if-ne v2, v3, :cond_9

    invoke-interface {v14}, Ljava/util/Map;->size()I

    move-result v2

    if-lez v2, :cond_9

    .line 557
    invoke-interface {v13, v14}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    .line 558
    invoke-interface {v14}, Ljava/util/Map;->clear()V

    goto :goto_3

    .line 579
    :cond_b
    const-string v3, "Not enough positive cache hits compared to misses. Need server request."

    invoke-interface/range {p1 .. p1}, Ljava/util/Set;->size()I

    move-result v4

    invoke-static {v3, v2, v11, v4}, Lcom/google/android/location/g/ae;->a(Ljava/lang/String;III)V

    .line 581
    new-instance v2, Lcom/google/android/location/g/af;

    sget-object v3, Lcom/google/android/location/e/ab;->c:Lcom/google/android/location/e/ab;

    const/4 v4, 0x0

    move-object/from16 v0, p0

    invoke-direct {v2, v0, v3, v4, v14}, Lcom/google/android/location/g/af;-><init>(Lcom/google/android/location/g/ae;Lcom/google/android/location/e/ab;Ljava/util/Map;Ljava/util/Map;)V

    goto/16 :goto_2

    .line 583
    :cond_c
    if-lez v11, :cond_d

    .line 585
    const-string v3, "Too many cache  misses. Need server request."

    invoke-interface/range {p1 .. p1}, Ljava/util/Set;->size()I

    move-result v4

    invoke-static {v3, v2, v11, v4}, Lcom/google/android/location/g/ae;->a(Ljava/lang/String;III)V

    .line 586
    new-instance v2, Lcom/google/android/location/g/af;

    sget-object v3, Lcom/google/android/location/e/ab;->c:Lcom/google/android/location/e/ab;

    const/4 v4, 0x0

    move-object/from16 v0, p0

    invoke-direct {v2, v0, v3, v4, v14}, Lcom/google/android/location/g/af;-><init>(Lcom/google/android/location/g/ae;Lcom/google/android/location/e/ab;Ljava/util/Map;Ljava/util/Map;)V

    goto/16 :goto_2

    .line 589
    :cond_d
    const-string v3, "Too many no-location APs. Will not compute a location nor go to the server."

    invoke-interface/range {p1 .. p1}, Ljava/util/Set;->size()I

    move-result v4

    invoke-static {v3, v2, v11, v4}, Lcom/google/android/location/g/ae;->a(Ljava/lang/String;III)V

    .line 591
    new-instance v2, Lcom/google/android/location/g/af;

    sget-object v3, Lcom/google/android/location/e/ab;->b:Lcom/google/android/location/e/ab;

    const/4 v4, 0x0

    move-object/from16 v0, p0

    invoke-direct {v2, v0, v3, v4, v14}, Lcom/google/android/location/g/af;-><init>(Lcom/google/android/location/g/ae;Lcom/google/android/location/e/ab;Ljava/util/Map;Ljava/util/Map;)V

    goto/16 :goto_2

    :cond_e
    move v2, v12

    goto/16 :goto_1
.end method

.method private static a(Ljava/lang/String;III)V
    .locals 3

    .prologue
    .line 672
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 673
    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 674
    const-string v1, " hasLocation="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 675
    add-int v1, p1, p2

    sub-int v1, p3, v1

    .line 676
    const-string v2, " noLocation="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 677
    const-string v1, " cacheMiss="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 678
    sget-boolean v1, Lcom/google/android/location/i/a;->b:Z

    if-eqz v1, :cond_0

    const-string v1, "WifiLocator"

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/google/android/location/o/a/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 679
    :cond_0
    return-void
.end method

.method private static a(Ljava/util/Collection;)Z
    .locals 12

    .prologue
    const/4 v9, 0x0

    .line 603
    invoke-interface {p0}, Ljava/util/Collection;->size()I

    move-result v0

    const/4 v1, 0x2

    if-ge v0, v1, :cond_0

    move v0, v9

    .line 618
    :goto_0
    return v0

    .line 607
    :cond_0
    invoke-interface {p0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :cond_1
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v8, v0

    check-cast v8, Lcom/google/android/location/e/al;

    .line 608
    invoke-interface {p0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v11

    :cond_2
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/google/android/location/e/al;

    .line 609
    if-eq v8, v6, :cond_2

    .line 610
    iget v0, v8, Lcom/google/android/location/e/al;->d:I

    int-to-double v0, v0

    iget v2, v8, Lcom/google/android/location/e/al;->e:I

    int-to-double v2, v2

    iget v4, v6, Lcom/google/android/location/e/al;->d:I

    int-to-double v4, v4

    iget v6, v6, Lcom/google/android/location/e/al;->e:I

    int-to-double v6, v6

    invoke-static/range {v0 .. v7}, Lcom/google/android/location/e/p;->a(DDDD)D

    move-result-wide v0

    .line 612
    const-wide v2, 0x40b3880000000000L    # 5000.0

    cmpg-double v0, v0, v2

    if-gez v0, :cond_2

    move v0, v9

    .line 613
    goto :goto_0

    .line 618
    :cond_3
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private static a(Ljava/util/Set;)Z
    .locals 10

    .prologue
    const/4 v5, 0x1

    const/4 v6, 0x0

    .line 628
    .line 630
    const-wide/16 v0, 0x0

    .line 632
    invoke-interface {p0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v7

    move-wide v2, v0

    move v4, v5

    :cond_0
    :goto_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    .line 634
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    const-wide v8, 0xfcffff000000L

    and-long/2addr v0, v8

    .line 636
    if-eqz v4, :cond_1

    move-wide v2, v0

    move v4, v6

    .line 638
    goto :goto_0

    .line 640
    :cond_1
    cmp-long v0, v0, v2

    if-eqz v0, :cond_2

    move v0, v5

    .line 642
    :goto_1
    if-eqz v0, :cond_0

    .line 648
    :goto_2
    return v5

    :cond_2
    move v0, v6

    .line 640
    goto :goto_1

    :cond_3
    move v5, v6

    .line 648
    goto :goto_2
.end method


# virtual methods
.method public final a(Lcom/google/android/location/e/bi;Lcom/google/android/location/e/al;)Lcom/google/android/location/e/bf;
    .locals 19

    .prologue
    .line 217
    new-instance v4, Ljava/util/HashMap;

    invoke-direct {v4}, Ljava/util/HashMap;-><init>()V

    if-eqz p1, :cond_1

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/location/e/bi;->a()I

    move-result v5

    const/4 v2, 0x0

    move v3, v2

    :goto_0
    if-ge v3, v5, :cond_1

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Lcom/google/android/location/e/bi;->a(I)Lcom/google/android/location/e/bc;

    move-result-object v6

    iget-wide v8, v6, Lcom/google/android/location/e/bc;->b:J

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v4, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/List;

    if-nez v2, :cond_0

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iget-wide v8, v6, Lcom/google/android/location/e/bc;->b:J

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-interface {v4, v7, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    iget v6, v6, Lcom/google/android/location/e/bc;->d:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-interface {v2, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_0

    :cond_1
    new-instance v10, Ljava/util/HashMap;

    invoke-direct {v10}, Ljava/util/HashMap;-><init>()V

    invoke-interface {v4}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map$Entry;

    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/List;

    invoke-static {v3}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v4

    div-int/lit8 v6, v4, 0x2

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v4

    rem-int/lit8 v4, v4, 0x2

    if-nez v4, :cond_2

    add-int/lit8 v4, v6, -0x1

    invoke-interface {v3, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-interface {v3, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    add-int/2addr v3, v4

    div-int/lit8 v3, v3, 0x2

    :goto_2
    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v10, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    :cond_2
    invoke-interface {v3, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    goto :goto_2

    .line 220
    :cond_3
    invoke-interface {v10}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v2

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-direct {v0, v2, v1}, Lcom/google/android/location/g/ae;->a(Ljava/util/Set;Lcom/google/android/location/e/al;)Lcom/google/android/location/g/af;

    move-result-object v2

    .line 222
    const/16 v16, 0x0

    .line 223
    const/high16 v17, -0x80000000

    .line 224
    iget-object v9, v2, Lcom/google/android/location/g/af;->b:Ljava/util/Map;

    .line 225
    iget-object v11, v2, Lcom/google/android/location/g/af;->a:Lcom/google/android/location/e/ab;

    .line 228
    iget-object v2, v2, Lcom/google/android/location/g/af;->c:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_3
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Long;

    .line 229
    invoke-interface {v10, v2}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_3

    .line 232
    :cond_4
    const/4 v3, 0x0

    .line 236
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/location/g/ae;->d:Lcom/google/t/b/b/a/d;

    if-eqz v2, :cond_8

    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/google/android/location/g/ae;->f:Z

    if-eqz v2, :cond_8

    .line 238
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/google/android/location/g/ae;->e:Z

    if-eqz v2, :cond_1b

    .line 239
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/location/g/ae;->d:Lcom/google/t/b/b/a/d;

    move-object/from16 v0, p1

    invoke-virtual {v2, v0}, Lcom/google/t/b/b/a/d;->a(Lcom/google/android/location/e/bi;)Lcom/google/android/location/e/bf;

    move-result-object v2

    .line 241
    sget-boolean v4, Lcom/google/android/location/i/a;->b:Z

    if-eqz v4, :cond_5

    .line 242
    const-string v4, "WifiLocator"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "GPWLE result: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/android/location/o/a/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 244
    :cond_5
    if-eqz v2, :cond_7

    .line 322
    :cond_6
    :goto_4
    return-object v2

    :cond_7
    move-object/from16 v18, v3

    .line 293
    :goto_5
    sget-object v2, Lcom/google/android/location/e/ab;->a:Lcom/google/android/location/e/ab;

    if-eq v11, v2, :cond_e

    .line 296
    new-instance v2, Lcom/google/android/location/e/bf;

    const/4 v3, 0x0

    const/4 v4, 0x0

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/location/g/ae;->l:Lcom/google/android/location/j/b;

    invoke-interface {v5}, Lcom/google/android/location/j/b;->c()J

    move-result-wide v6

    move-object v5, v11

    move-object/from16 v8, p1

    invoke-direct/range {v2 .. v9}, Lcom/google/android/location/e/bf;-><init>(ILcom/google/android/location/e/al;Lcom/google/android/location/e/ab;JLcom/google/android/location/e/bi;Ljava/util/Map;)V

    .line 298
    move-object/from16 v0, v18

    invoke-static {v0, v2}, Lcom/google/android/location/g/ae;->a(Lcom/google/android/location/e/bf;Lcom/google/android/location/e/bf;)Lcom/google/android/location/e/bf;

    move-result-object v2

    goto :goto_4

    .line 250
    :cond_8
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/location/g/ae;->c:Lcom/google/android/location/g/ag;

    invoke-virtual {v2, v10}, Lcom/google/android/location/g/ag;->a(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v4

    .line 251
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/location/g/ae;->k:Lcom/google/android/location/g/ah;

    if-nez v4, :cond_b

    const/4 v2, 0x0

    .line 256
    :goto_6
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/location/g/ae;->b:Lcom/google/android/location/g/q;

    const/4 v5, 0x0

    invoke-virtual {v4, v5, v2}, Lcom/google/android/location/g/q;->a(Ljava/util/Map;Ljava/util/Map;)Lcom/google/android/location/e/be;

    move-result-object v12

    .line 258
    if-eqz v12, :cond_1b

    .line 259
    invoke-interface {v10}, Ljava/util/Map;->size()I

    move-result v2

    const/4 v4, 0x2

    if-lt v2, v4, :cond_1b

    .line 260
    sget-boolean v2, Lcom/google/android/location/i/a;->b:Z

    if-eqz v2, :cond_9

    const-string v2, "WifiLocator"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Indoor localizer returned: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/location/o/a/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 261
    :cond_9
    new-instance v2, Lcom/google/android/location/e/bf;

    const/4 v3, 0x3

    invoke-virtual {v12}, Lcom/google/android/location/e/be;->a()Lcom/google/android/location/e/al;

    move-result-object v4

    sget-object v5, Lcom/google/android/location/e/ab;->a:Lcom/google/android/location/e/ab;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/location/g/ae;->l:Lcom/google/android/location/j/b;

    invoke-interface {v6}, Lcom/google/android/location/j/b;->c()J

    move-result-wide v6

    move-object/from16 v8, p1

    invoke-direct/range {v2 .. v9}, Lcom/google/android/location/e/bf;-><init>(ILcom/google/android/location/e/al;Lcom/google/android/location/e/ab;JLcom/google/android/location/e/bi;Ljava/util/Map;)V

    .line 267
    invoke-virtual {v12}, Lcom/google/android/location/e/be;->a()Lcom/google/android/location/e/al;

    move-result-object v3

    check-cast v3, Lcom/google/android/location/e/n;

    .line 269
    if-eqz v3, :cond_1a

    .line 270
    iget-object v0, v3, Lcom/google/android/location/e/n;->b:Ljava/lang/String;

    move-object/from16 v16, v0

    .line 271
    if-eqz v16, :cond_a

    .line 272
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/location/g/ae;->j:Lcom/google/android/location/b/ai;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/location/g/ae;->l:Lcom/google/android/location/j/b;

    invoke-interface {v5}, Lcom/google/android/location/j/b;->b()J

    move-result-wide v6

    move-object/from16 v0, v16

    invoke-interface {v4, v0, v6, v7}, Lcom/google/android/location/b/ai;->a(Ljava/lang/String;J)I

    .line 274
    sget-boolean v4, Lcom/google/android/location/i/a;->b:Z

    if-eqz v4, :cond_a

    const-string v4, "WifiLocator"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Incremented executions for level "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v16

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/android/location/o/a/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 276
    :cond_a
    iget v4, v3, Lcom/google/android/location/e/n;->f:I

    const/16 v5, 0x7530

    if-lt v4, v5, :cond_6

    .line 287
    iget v0, v3, Lcom/google/android/location/e/n;->c:I

    move/from16 v17, v0

    move-object/from16 v18, v2

    goto/16 :goto_5

    .line 251
    :cond_b
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    iget-object v6, v5, Lcom/google/android/location/g/ah;->a:Lcom/google/android/location/j/b;

    invoke-interface {v6}, Lcom/google/android/location/j/b;->c()J

    move-result-wide v6

    iget-wide v12, v5, Lcom/google/android/location/g/ah;->b:J

    sub-long v12, v6, v12

    const-wide/16 v14, 0x1f40

    cmp-long v8, v12, v14

    if-gez v8, :cond_c

    iget-object v8, v5, Lcom/google/android/location/g/ah;->c:Ljava/util/Map;

    if-eqz v8, :cond_c

    iget-object v8, v5, Lcom/google/android/location/g/ah;->c:Ljava/util/Map;

    invoke-interface {v2, v8}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    :cond_c
    invoke-interface {v2, v4}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    sget-boolean v8, Lcom/google/android/location/i/a;->b:Z

    if-eqz v8, :cond_d

    const-string v8, "WifiSignalAugmenter"

    new-instance v12, Ljava/lang/StringBuilder;

    const-string v13, "Orig AP count: "

    invoke-direct {v12, v13}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v4}, Ljava/util/Map;->size()I

    move-result v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, " Augmented AP count: "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-interface {v2}, Ljava/util/Map;->size()I

    move-result v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v8, v12}, Lcom/google/android/location/o/a/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_d
    iput-wide v6, v5, Lcom/google/android/location/g/ah;->b:J

    iput-object v4, v5, Lcom/google/android/location/g/ah;->c:Ljava/util/Map;

    goto/16 :goto_6

    .line 301
    :cond_e
    invoke-static {v9}, Lcom/google/android/location/g/ae;->a(Ljava/util/Map;)Lcom/google/android/location/e/bb;

    move-result-object v2

    sget-object v3, Lcom/google/android/location/e/bb;->a:Lcom/google/android/location/e/bb;

    if-ne v2, v3, :cond_12

    sget-boolean v2, Lcom/google/android/location/i/a;->b:Z

    if-eqz v2, :cond_f

    const-string v2, "WifiLocator"

    const-string v3, "No APs found with known confidence values. Not computing a location"

    invoke-static {v2, v3}, Lcom/google/android/location/o/a/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_f
    const/4 v2, 0x0

    move-object v3, v2

    .line 303
    :goto_7
    if-eqz v3, :cond_10

    invoke-virtual {v3}, Lcom/google/android/location/e/be;->a()Lcom/google/android/location/e/al;

    move-result-object v2

    if-nez v2, :cond_16

    .line 304
    :cond_10
    sget-boolean v2, Lcom/google/android/location/i/a;->b:Z

    if-eqz v2, :cond_11

    const-string v2, "WifiLocator"

    const-string v3, "Locator did not find a location"

    invoke-static {v2, v3}, Lcom/google/android/location/o/a/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 305
    :cond_11
    new-instance v2, Lcom/google/android/location/e/bf;

    const/4 v3, 0x0

    const/4 v4, 0x0

    sget-object v5, Lcom/google/android/location/e/ab;->b:Lcom/google/android/location/e/ab;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/location/g/ae;->l:Lcom/google/android/location/j/b;

    invoke-interface {v6}, Lcom/google/android/location/j/b;->c()J

    move-result-wide v6

    move-object/from16 v8, p1

    invoke-direct/range {v2 .. v9}, Lcom/google/android/location/e/bf;-><init>(ILcom/google/android/location/e/al;Lcom/google/android/location/e/ab;JLcom/google/android/location/e/bi;Ljava/util/Map;)V

    .line 307
    move-object/from16 v0, v18

    invoke-static {v0, v2}, Lcom/google/android/location/g/ae;->a(Lcom/google/android/location/e/bf;Lcom/google/android/location/e/bf;)Lcom/google/android/location/e/bf;

    move-result-object v2

    goto/16 :goto_4

    .line 301
    :cond_12
    sget-object v3, Lcom/google/android/location/e/bb;->b:Lcom/google/android/location/e/bb;

    if-ne v2, v3, :cond_14

    sget-boolean v2, Lcom/google/android/location/i/a;->b:Z

    if-eqz v2, :cond_13

    const-string v2, "WifiLocator"

    const-string v3, "Computing location using circle intersection."

    invoke-static {v2, v3}, Lcom/google/android/location/o/a/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_13
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/location/g/ae;->g:Lcom/google/android/location/e/bd;

    invoke-interface {v2, v9, v10}, Lcom/google/android/location/e/bd;->a(Ljava/util/Map;Ljava/util/Map;)Lcom/google/android/location/e/be;

    move-result-object v2

    :goto_8
    move-object v3, v2

    goto :goto_7

    :cond_14
    sget-boolean v2, Lcom/google/android/location/i/a;->b:Z

    if-eqz v2, :cond_15

    const-string v2, "WifiLocator"

    const-string v3, "Computing location using MaxLre."

    invoke-static {v2, v3}, Lcom/google/android/location/o/a/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_15
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/location/g/ae;->h:Lcom/google/android/location/e/bd;

    invoke-interface {v2, v9, v10}, Lcom/google/android/location/e/bd;->a(Ljava/util/Map;Ljava/util/Map;)Lcom/google/android/location/e/be;

    move-result-object v2

    goto :goto_8

    .line 308
    :cond_16
    invoke-virtual {v3}, Lcom/google/android/location/e/be;->a()Lcom/google/android/location/e/al;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/location/g/d;->c(Lcom/google/android/location/e/al;)Z

    move-result v2

    if-nez v2, :cond_18

    .line 309
    sget-boolean v2, Lcom/google/android/location/i/a;->d:Z

    if-eqz v2, :cond_17

    const-string v2, "WifiLocator"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Locator found a location that did not have sane values: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/location/o/a/a;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 310
    :cond_17
    new-instance v2, Lcom/google/android/location/e/bf;

    const/4 v3, 0x0

    const/4 v4, 0x0

    sget-object v5, Lcom/google/android/location/e/ab;->b:Lcom/google/android/location/e/ab;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/location/g/ae;->l:Lcom/google/android/location/j/b;

    invoke-interface {v6}, Lcom/google/android/location/j/b;->c()J

    move-result-wide v6

    move-object/from16 v8, p1

    invoke-direct/range {v2 .. v9}, Lcom/google/android/location/e/bf;-><init>(ILcom/google/android/location/e/al;Lcom/google/android/location/e/ab;JLcom/google/android/location/e/bi;Ljava/util/Map;)V

    .line 312
    move-object/from16 v0, v18

    invoke-static {v0, v2}, Lcom/google/android/location/g/ae;->a(Lcom/google/android/location/e/bf;Lcom/google/android/location/e/bf;)Lcom/google/android/location/e/bf;

    move-result-object v2

    goto/16 :goto_4

    .line 315
    :cond_18
    invoke-virtual {v3}, Lcom/google/android/location/e/be;->a()Lcom/google/android/location/e/al;

    move-result-object v2

    .line 316
    new-instance v10, Lcom/google/android/location/e/n;

    iget v11, v2, Lcom/google/android/location/e/al;->d:I

    iget v12, v2, Lcom/google/android/location/e/al;->e:I

    iget v13, v2, Lcom/google/android/location/e/al;->f:I

    invoke-virtual {v3}, Lcom/google/android/location/e/be;->b()I

    move-result v14

    const/4 v15, 0x0

    invoke-direct/range {v10 .. v17}, Lcom/google/android/location/e/n;-><init>(IIIILjava/lang/String;Ljava/lang/String;I)V

    .line 319
    new-instance v2, Lcom/google/android/location/e/bf;

    iget v3, v3, Lcom/google/android/location/e/be;->b:I

    sget-object v5, Lcom/google/android/location/e/ab;->a:Lcom/google/android/location/e/ab;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/location/g/ae;->l:Lcom/google/android/location/j/b;

    invoke-interface {v4}, Lcom/google/android/location/j/b;->c()J

    move-result-wide v6

    move-object v4, v10

    move-object/from16 v8, p1

    invoke-direct/range {v2 .. v9}, Lcom/google/android/location/e/bf;-><init>(ILcom/google/android/location/e/al;Lcom/google/android/location/e/ab;JLcom/google/android/location/e/bi;Ljava/util/Map;)V

    .line 321
    sget-boolean v3, Lcom/google/android/location/i/a;->b:Z

    if-eqz v3, :cond_19

    const-string v3, "WifiLocator"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Finished computing WiFi location: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/android/location/o/a/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 322
    :cond_19
    move-object/from16 v0, v18

    invoke-static {v0, v2}, Lcom/google/android/location/g/ae;->a(Lcom/google/android/location/e/bf;Lcom/google/android/location/e/bf;)Lcom/google/android/location/e/bf;

    move-result-object v2

    goto/16 :goto_4

    :cond_1a
    move-object/from16 v18, v2

    goto/16 :goto_5

    :cond_1b
    move-object/from16 v18, v3

    goto/16 :goto_5
.end method
