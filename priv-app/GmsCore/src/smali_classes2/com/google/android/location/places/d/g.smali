.class final Lcom/google/android/location/places/d/g;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lcom/google/android/location/places/d/e;

.field private b:J

.field private final c:Lcom/google/android/location/places/Subscription;


# direct methods
.method public constructor <init>(Lcom/google/android/location/places/d/e;Lcom/google/android/location/places/Subscription;)V
    .locals 2

    .prologue
    .line 164
    iput-object p1, p0, Lcom/google/android/location/places/d/g;->a:Lcom/google/android/location/places/d/e;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 165
    iput-object p2, p0, Lcom/google/android/location/places/d/g;->c:Lcom/google/android/location/places/Subscription;

    .line 166
    sget-wide v0, Lcom/google/android/location/places/d/e;->a:J

    iput-wide v0, p0, Lcom/google/android/location/places/d/g;->b:J

    .line 167
    return-void
.end method

.method static synthetic a(Lcom/google/android/location/places/d/g;)Lcom/google/android/location/places/Subscription;
    .locals 1

    .prologue
    .line 159
    iget-object v0, p0, Lcom/google/android/location/places/d/g;->c:Lcom/google/android/location/places/Subscription;

    return-object v0
.end method


# virtual methods
.method public final a(Lcom/google/android/location/places/Subscription;Lcom/google/android/location/places/c/i;)V
    .locals 10

    .prologue
    const/4 v1, 0x4

    .line 223
    iget-object v0, p0, Lcom/google/android/location/places/d/g;->a:Lcom/google/android/location/places/d/e;

    invoke-static {v0}, Lcom/google/android/location/places/d/e;->d(Lcom/google/android/location/places/d/e;)Lcom/google/android/location/places/d/b;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/location/places/d/b;->g:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 224
    const-string v0, "Places"

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 225
    const-string v0, "Places"

    const-string v1, "personal places received, but subscription has been removed."

    invoke-static {v0, v1}, Lcom/google/android/location/n/aa;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 263
    :cond_0
    :goto_0
    return-void

    .line 230
    :cond_1
    const-string v0, "Places"

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 231
    const-string v0, "Places"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "response.placeUserDataResults.size(): "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p2, Lcom/google/android/location/places/c/i;->a:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/location/n/aa;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 235
    :cond_2
    new-instance v3, Ljava/util/ArrayList;

    iget-object v0, p2, Lcom/google/android/location/places/c/i;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    invoke-direct {v3, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 238
    invoke-virtual {p1}, Lcom/google/android/location/places/Subscription;->a()Lcom/google/android/gms/location/places/internal/PlacesParams;

    move-result-object v0

    iget-object v2, v0, Lcom/google/android/gms/location/places/internal/PlacesParams;->e:Ljava/lang/String;

    .line 239
    iget-object v0, p2, Lcom/google/android/location/places/c/i;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_3
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/google/android/location/places/d/a;

    .line 240
    iget-object v0, p0, Lcom/google/android/location/places/d/g;->a:Lcom/google/android/location/places/d/e;

    invoke-static {v0}, Lcom/google/android/location/places/d/e;->f(Lcom/google/android/location/places/d/e;)Lcom/google/android/location/places/d/i;

    move-result-object v0

    invoke-static {v2, v1}, Lcom/google/android/location/places/d/a;->a(Ljava/lang/String;Lcom/google/android/location/places/d/a;)Lcom/google/android/gms/location/places/personalized/PlaceUserData;

    move-result-object v5

    iget-object v0, v0, Lcom/google/android/location/places/d/i;->a:Lcom/google/k/c/dn;

    invoke-virtual {v5}, Lcom/google/android/gms/location/places/personalized/PlaceUserData;->b()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v0, v6, v5}, Lcom/google/k/c/dn;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 242
    invoke-virtual {p1}, Lcom/google/android/location/places/Subscription;->d()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 243
    iget-object v0, v1, Lcom/google/android/location/places/d/a;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/places/b/j;

    .line 244
    iget-object v6, v1, Lcom/google/android/location/places/d/a;->a:Ljava/lang/String;

    invoke-static {v0, v6}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v0

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 251
    :cond_4
    const v0, 0x7fffffff

    .line 252
    iget-object v1, p2, Lcom/google/android/location/places/c/i;->c:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 253
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v4

    if-ge v4, v1, :cond_9

    .line 254
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    :goto_3
    move v1, v0

    .line 256
    goto :goto_2

    .line 257
    :cond_5
    iget-object v0, p0, Lcom/google/android/location/places/d/g;->a:Lcom/google/android/location/places/d/e;

    invoke-static {v0}, Lcom/google/android/location/places/d/e;->d(Lcom/google/android/location/places/d/e;)Lcom/google/android/location/places/d/b;

    move-result-object v4

    iget-object v0, v4, Lcom/google/android/location/places/d/b;->g:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_7

    const/4 v0, 0x0

    move-object v2, v0

    :goto_4
    if-nez v2, :cond_8

    const-string v0, "Places"

    const/4 v1, 0x5

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_6

    const-string v0, "Places"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unable to schedule a refresh for subscription "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 258
    :cond_6
    :goto_5
    iget-object v0, p0, Lcom/google/android/location/places/d/g;->a:Lcom/google/android/location/places/d/e;

    invoke-static {v0}, Lcom/google/android/location/places/d/e;->g(Lcom/google/android/location/places/d/e;)Lcom/google/android/location/places/b/a;

    move-result-object v0

    iget-object v1, p2, Lcom/google/android/location/places/c/i;->b:Lcom/google/android/location/places/b/j;

    iget-object v2, p0, Lcom/google/android/location/places/d/g;->a:Lcom/google/android/location/places/d/e;

    invoke-virtual {v0, v3, v1, p1, v2}, Lcom/google/android/location/places/b/a;->a(Ljava/util/Collection;Lcom/google/android/location/places/b/j;Lcom/google/android/location/places/Subscription;Lcom/google/android/location/places/b/c;)V

    goto/16 :goto_0

    .line 257
    :cond_7
    new-instance v2, Landroid/content/Intent;

    iget-object v0, v4, Lcom/google/android/location/places/d/b;->b:Landroid/content/Context;

    const-class v5, Lcom/google/android/location/internal/PendingIntentCallbackService;

    invoke-direct {v2, v0, v5}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v0, "com.google.android.location.internal.action.PLACES_REFRESH_USER_DATA_SUBSCRIPTION"

    invoke-virtual {v2, v0}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    iget-object v0, v4, Lcom/google/android/location/places/d/b;->b:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    const-string v5, "extra_subscription_code"

    iget-object v0, v4, Lcom/google/android/location/places/d/b;->g:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/io/Serializable;

    invoke-virtual {v2, v5, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    goto :goto_4

    :cond_8
    iget-object v0, v4, Lcom/google/android/location/places/d/b;->g:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iget-object v5, v4, Lcom/google/android/location/places/d/b;->e:Lcom/google/android/location/geofencer/service/ah;

    iget-object v5, v4, Lcom/google/android/location/places/d/b;->b:Landroid/content/Context;

    const/high16 v6, 0x10000000

    invoke-static {v5, v0, v2, v6}, Landroid/app/PendingIntent;->getService(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    iget-object v2, v4, Lcom/google/android/location/places/d/b;->c:Lcom/google/android/gms/common/util/p;

    invoke-interface {v2}, Lcom/google/android/gms/common/util/p;->a()J

    move-result-wide v6

    sget-object v2, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    int-to-long v8, v1

    invoke-virtual {v2, v8, v9}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v8

    add-long/2addr v6, v8

    iget-object v1, v4, Lcom/google/android/location/places/d/b;->d:Landroid/app/AlarmManager;

    const/4 v2, 0x0

    invoke-virtual {v1, v2, v6, v7, v0}, Landroid/app/AlarmManager;->set(IJLandroid/app/PendingIntent;)V

    goto :goto_5

    :cond_9
    move v0, v1

    goto/16 :goto_3
.end method

.method public final a(Ljava/lang/Throwable;)V
    .locals 6

    .prologue
    const/4 v4, 0x5

    .line 266
    const-string v0, "Places"

    invoke-static {v0, v4}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 267
    const-string v0, "Places"

    const-string v1, "onPersonalPlacesFailure"

    invoke-static {v0, v1, p1}, Lcom/google/android/location/n/aa;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 269
    :cond_0
    iget-wide v0, p0, Lcom/google/android/location/places/d/g;->b:J

    sget-wide v2, Lcom/google/android/location/places/d/e;->b:J

    cmp-long v0, v0, v2

    if-gtz v0, :cond_2

    const/4 v0, 0x1

    :goto_0
    if-eqz v0, :cond_3

    .line 270
    iget-wide v0, p0, Lcom/google/android/location/places/d/g;->b:J

    iget-wide v2, p0, Lcom/google/android/location/places/d/g;->b:J

    const-wide/16 v4, 0x3

    mul-long/2addr v2, v4

    iput-wide v2, p0, Lcom/google/android/location/places/d/g;->b:J

    .line 271
    iget-object v2, p0, Lcom/google/android/location/places/d/g;->a:Lcom/google/android/location/places/d/e;

    invoke-static {v2}, Lcom/google/android/location/places/d/e;->b(Lcom/google/android/location/places/d/e;)Lcom/google/android/location/places/bz;

    move-result-object v2

    invoke-virtual {v2, p0, v0, v1}, Lcom/google/android/location/places/bz;->a(Ljava/lang/Runnable;J)Z

    .line 272
    const-string v2, "Places"

    const/4 v3, 0x3

    invoke-static {v2, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 273
    const-string v2, "Places"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Scheduling a GetPlaces retry after "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " ms"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/google/android/location/n/aa;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 278
    :cond_1
    :goto_1
    return-void

    .line 269
    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    .line 275
    :cond_3
    const-string v0, "Places"

    invoke-static {v0, v4}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 276
    const-string v0, "Places"

    const-string v1, "Can\'t retry GetPlaces anymore - failing."

    invoke-static {v0, v1}, Lcom/google/android/location/n/aa;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method public final run()V
    .locals 7

    .prologue
    const/4 v6, 0x1

    .line 181
    iget-object v0, p0, Lcom/google/android/location/places/d/g;->a:Lcom/google/android/location/places/d/e;

    invoke-static {v0}, Lcom/google/android/location/places/d/e;->a(Lcom/google/android/location/places/d/e;)Lcom/google/android/location/fused/g;

    move-result-object v0

    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v1

    iget-object v2, p0, Lcom/google/android/location/places/d/g;->c:Lcom/google/android/location/places/Subscription;

    invoke-virtual {v2}, Lcom/google/android/location/places/Subscription;->a()Lcom/google/android/gms/location/places/internal/PlacesParams;

    move-result-object v2

    iget-object v2, v2, Lcom/google/android/gms/location/places/internal/PlacesParams;->c:Ljava/lang/String;

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v6, v3}, Lcom/google/android/location/fused/g;->a(ILjava/lang/String;ZZ)Landroid/location/Location;

    move-result-object v0

    .line 189
    if-nez v0, :cond_1

    .line 192
    const-string v0, "Places"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 193
    const-string v0, "Places"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Location is missing, scheduling a GetPlaces retry after "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-wide v2, Lcom/google/android/location/places/d/e;->c:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " ms"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/location/n/aa;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 196
    :cond_0
    iget-object v0, p0, Lcom/google/android/location/places/d/g;->a:Lcom/google/android/location/places/d/e;

    invoke-static {v0}, Lcom/google/android/location/places/d/e;->b(Lcom/google/android/location/places/d/e;)Lcom/google/android/location/places/bz;

    move-result-object v0

    sget-wide v2, Lcom/google/android/location/places/d/e;->c:J

    invoke-virtual {v0, p0, v2, v3}, Lcom/google/android/location/places/bz;->a(Ljava/lang/Runnable;J)Z

    .line 219
    :goto_0
    return-void

    .line 200
    :cond_1
    new-instance v1, Lcom/google/android/gms/maps/model/LatLng;

    invoke-virtual {v0}, Landroid/location/Location;->getLatitude()D

    move-result-wide v2

    invoke-virtual {v0}, Landroid/location/Location;->getLongitude()D

    move-result-wide v4

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/google/android/gms/maps/model/LatLng;-><init>(DD)V

    .line 201
    new-instance v0, Lcom/google/android/location/places/c/h;

    iget-object v2, p0, Lcom/google/android/location/places/d/g;->a:Lcom/google/android/location/places/d/e;

    invoke-static {v2}, Lcom/google/android/location/places/d/e;->c(Lcom/google/android/location/places/d/e;)Lcom/google/android/location/places/c/j;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/location/places/d/g;->c:Lcom/google/android/location/places/Subscription;

    invoke-direct {v0, v2, v3, v1}, Lcom/google/android/location/places/c/h;-><init>(Lcom/google/android/location/places/c/j;Lcom/google/android/location/places/Subscription;Lcom/google/android/gms/maps/model/LatLng;)V

    .line 203
    iget-object v1, p0, Lcom/google/android/location/places/d/g;->a:Lcom/google/android/location/places/d/e;

    invoke-static {v1}, Lcom/google/android/location/places/d/e;->d(Lcom/google/android/location/places/d/e;)Lcom/google/android/location/places/d/b;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/location/places/d/g;->c:Lcom/google/android/location/places/Subscription;

    iget-object v3, v1, Lcom/google/android/location/places/d/b;->g:Ljava/util/Map;

    invoke-interface {v3, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_2

    iget-object v3, v1, Lcom/google/android/location/places/d/b;->a:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v3, v6}, Ljava/util/concurrent/atomic/AtomicInteger;->addAndGet(I)I

    move-result v3

    iget-object v4, v1, Lcom/google/android/location/places/d/b;->g:Ljava/util/Map;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v4, v2, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v1, v1, Lcom/google/android/location/places/d/b;->h:Ljava/util/Map;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v1, v3, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 204
    :cond_2
    iget-object v1, p0, Lcom/google/android/location/places/d/g;->a:Lcom/google/android/location/places/d/e;

    invoke-static {v1}, Lcom/google/android/location/places/d/e;->e(Lcom/google/android/location/places/d/e;)Lcom/google/android/location/places/c/d;

    move-result-object v1

    new-instance v2, Lcom/google/android/location/places/d/h;

    invoke-direct {v2, p0}, Lcom/google/android/location/places/d/h;-><init>(Lcom/google/android/location/places/d/g;)V

    invoke-virtual {v1, v0, v2}, Lcom/google/android/location/places/c/d;->a(Ljava/util/concurrent/Callable;Lcom/google/android/location/places/c/m;)V

    goto :goto_0
.end method
