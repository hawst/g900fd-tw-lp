.class public final Lcom/google/android/location/internal/g;
.super Lcom/google/android/gms/location/places/internal/b;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/android/location/internal/e;


# direct methods
.method public constructor <init>(Lcom/google/android/location/internal/e;)V
    .locals 0

    .prologue
    .line 42
    invoke-direct {p0}, Lcom/google/android/gms/location/places/internal/b;-><init>()V

    .line 43
    iput-object p1, p0, Lcom/google/android/location/internal/g;->a:Lcom/google/android/location/internal/e;

    .line 44
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/location/places/NearbyAlertRequest;Lcom/google/android/gms/location/places/internal/PlacesParams;Landroid/app/PendingIntent;)V
    .locals 1

    .prologue
    .line 101
    iget-object v0, p0, Lcom/google/android/location/internal/g;->a:Lcom/google/android/location/internal/e;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/location/internal/e;->a(Lcom/google/android/gms/location/places/NearbyAlertRequest;Lcom/google/android/gms/location/places/internal/PlacesParams;Landroid/app/PendingIntent;)V

    .line 102
    return-void
.end method

.method public final a(Lcom/google/android/gms/location/places/PlaceFilter;Lcom/google/android/gms/location/places/internal/PlacesParams;Lcom/google/android/gms/location/places/internal/d;)V
    .locals 1

    .prologue
    .line 95
    iget-object v0, p0, Lcom/google/android/location/internal/g;->a:Lcom/google/android/location/internal/e;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/location/internal/e;->a(Lcom/google/android/gms/location/places/PlaceFilter;Lcom/google/android/gms/location/places/internal/PlacesParams;Lcom/google/android/gms/location/places/internal/d;)V

    .line 96
    return-void
.end method

.method public final a(Lcom/google/android/gms/location/places/PlaceReport;Lcom/google/android/gms/location/places/internal/PlacesParams;)V
    .locals 1

    .prologue
    .line 122
    iget-object v0, p0, Lcom/google/android/location/internal/g;->a:Lcom/google/android/location/internal/e;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/location/internal/e;->a(Lcom/google/android/gms/location/places/PlaceReport;Lcom/google/android/gms/location/places/internal/PlacesParams;)V

    .line 123
    return-void
.end method

.method public final a(Lcom/google/android/gms/location/places/PlaceRequest;Lcom/google/android/gms/location/places/internal/PlacesParams;Landroid/app/PendingIntent;)V
    .locals 1

    .prologue
    .line 112
    iget-object v0, p0, Lcom/google/android/location/internal/g;->a:Lcom/google/android/location/internal/e;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/location/internal/e;->a(Lcom/google/android/gms/location/places/PlaceRequest;Lcom/google/android/gms/location/places/internal/PlacesParams;Landroid/app/PendingIntent;)V

    .line 113
    return-void
.end method

.method public final a(Lcom/google/android/gms/location/places/UserAddedPlace;Lcom/google/android/gms/location/places/internal/PlacesParams;Lcom/google/android/gms/location/places/internal/d;)V
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lcom/google/android/location/internal/g;->a:Lcom/google/android/location/internal/e;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/location/internal/e;->a(Lcom/google/android/gms/location/places/UserAddedPlace;Lcom/google/android/gms/location/places/internal/PlacesParams;Lcom/google/android/gms/location/places/internal/d;)V

    .line 67
    return-void
.end method

.method public final a(Lcom/google/android/gms/location/places/UserDataType;Lcom/google/android/gms/maps/model/LatLngBounds;Ljava/util/List;Lcom/google/android/gms/location/places/internal/PlacesParams;Lcom/google/android/gms/location/places/internal/d;)V
    .locals 6

    .prologue
    .line 60
    iget-object v0, p0, Lcom/google/android/location/internal/g;->a:Lcom/google/android/location/internal/e;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/location/internal/e;->a(Lcom/google/android/gms/location/places/UserDataType;Lcom/google/android/gms/maps/model/LatLngBounds;Ljava/util/List;Lcom/google/android/gms/location/places/internal/PlacesParams;Lcom/google/android/gms/location/places/internal/d;)V

    .line 61
    return-void
.end method

.method public final a(Lcom/google/android/gms/location/places/internal/PlacesParams;Landroid/app/PendingIntent;)V
    .locals 1

    .prologue
    .line 117
    iget-object v0, p0, Lcom/google/android/location/internal/g;->a:Lcom/google/android/location/internal/e;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/location/internal/e;->b(Lcom/google/android/gms/location/places/internal/PlacesParams;Landroid/app/PendingIntent;)V

    .line 118
    return-void
.end method

.method public final a(Lcom/google/android/gms/maps/model/LatLng;Lcom/google/android/gms/location/places/PlaceFilter;Lcom/google/android/gms/location/places/internal/PlacesParams;Lcom/google/android/gms/location/places/internal/d;)V
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lcom/google/android/location/internal/g;->a:Lcom/google/android/location/internal/e;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/google/android/location/internal/e;->a(Lcom/google/android/gms/maps/model/LatLng;Lcom/google/android/gms/location/places/PlaceFilter;Lcom/google/android/gms/location/places/internal/PlacesParams;Lcom/google/android/gms/location/places/internal/d;)V

    .line 73
    return-void
.end method

.method public final a(Lcom/google/android/gms/maps/model/LatLngBounds;ILjava/lang/String;Lcom/google/android/gms/location/places/PlaceFilter;Lcom/google/android/gms/location/places/internal/PlacesParams;Lcom/google/android/gms/location/places/internal/d;)V
    .locals 7

    .prologue
    .line 84
    iget-object v0, p0, Lcom/google/android/location/internal/g;->a:Lcom/google/android/location/internal/e;

    move-object v1, p1

    move v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object v6, p6

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/location/internal/e;->a(Lcom/google/android/gms/maps/model/LatLngBounds;ILjava/lang/String;Lcom/google/android/gms/location/places/PlaceFilter;Lcom/google/android/gms/location/places/internal/PlacesParams;Lcom/google/android/gms/location/places/internal/d;)V

    .line 85
    return-void
.end method

.method public final a(Ljava/lang/String;Lcom/google/android/gms/location/places/internal/PlacesParams;Lcom/google/android/gms/location/places/internal/d;)V
    .locals 1

    .prologue
    .line 90
    iget-object v0, p0, Lcom/google/android/location/internal/g;->a:Lcom/google/android/location/internal/e;

    invoke-virtual {v0, p2}, Lcom/google/android/location/internal/e;->a(Lcom/google/android/gms/location/places/internal/PlacesParams;)V

    .line 91
    return-void
.end method

.method public final a(Ljava/lang/String;Lcom/google/android/gms/maps/model/LatLngBounds;Lcom/google/android/gms/location/places/AutocompleteFilter;Lcom/google/android/gms/location/places/internal/PlacesParams;Lcom/google/android/gms/location/places/internal/d;)V
    .locals 6

    .prologue
    .line 78
    iget-object v0, p0, Lcom/google/android/location/internal/g;->a:Lcom/google/android/location/internal/e;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/location/internal/e;->a(Ljava/lang/String;Lcom/google/android/gms/maps/model/LatLngBounds;Lcom/google/android/gms/location/places/AutocompleteFilter;Lcom/google/android/gms/location/places/internal/PlacesParams;Lcom/google/android/gms/location/places/internal/d;)V

    .line 79
    return-void
.end method

.method public final a(Ljava/util/List;Lcom/google/android/gms/location/places/internal/PlacesParams;Lcom/google/android/gms/location/places/internal/d;)V
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lcom/google/android/location/internal/g;->a:Lcom/google/android/location/internal/e;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/location/internal/e;->a(Ljava/util/List;Lcom/google/android/gms/location/places/internal/PlacesParams;Lcom/google/android/gms/location/places/internal/d;)V

    .line 55
    return-void
.end method

.method public final b(Lcom/google/android/gms/location/places/internal/PlacesParams;Landroid/app/PendingIntent;)V
    .locals 1

    .prologue
    .line 106
    iget-object v0, p0, Lcom/google/android/location/internal/g;->a:Lcom/google/android/location/internal/e;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/location/internal/e;->a(Lcom/google/android/gms/location/places/internal/PlacesParams;Landroid/app/PendingIntent;)V

    .line 107
    return-void
.end method

.method public final b(Ljava/lang/String;Lcom/google/android/gms/location/places/internal/PlacesParams;Lcom/google/android/gms/location/places/internal/d;)V
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lcom/google/android/location/internal/g;->a:Lcom/google/android/location/internal/e;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/location/internal/e;->a(Ljava/lang/String;Lcom/google/android/gms/location/places/internal/PlacesParams;Lcom/google/android/gms/location/places/internal/d;)V

    .line 49
    return-void
.end method
