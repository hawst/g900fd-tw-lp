.class final Lcom/google/android/location/os/real/z;
.super Landroid/content/BroadcastReceiver;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/location/os/real/x;

.field private final b:Landroid/net/wifi/WifiManager;


# direct methods
.method private constructor <init>(Lcom/google/android/location/os/real/x;)V
    .locals 2

    .prologue
    .line 796
    iput-object p1, p0, Lcom/google/android/location/os/real/z;->a:Lcom/google/android/location/os/real/x;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    .line 798
    iget-object v0, p0, Lcom/google/android/location/os/real/z;->a:Lcom/google/android/location/os/real/x;

    iget-object v0, v0, Lcom/google/android/location/os/real/x;->n:Landroid/content/Context;

    const-string v1, "wifi"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/wifi/WifiManager;

    iput-object v0, p0, Lcom/google/android/location/os/real/z;->b:Landroid/net/wifi/WifiManager;

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/location/os/real/x;B)V
    .locals 0

    .prologue
    .line 796
    invoke-direct {p0, p1}, Lcom/google/android/location/os/real/z;-><init>(Lcom/google/android/location/os/real/x;)V

    return-void
.end method


# virtual methods
.method public final onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 7

    .prologue
    const/4 v2, 0x2

    const/16 v6, 0xc

    const/4 v5, 0x6

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 806
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v3

    .line 807
    const-string v4, "android.intent.action.SCREEN_OFF"

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 808
    iget-object v0, p0, Lcom/google/android/location/os/real/z;->a:Lcom/google/android/location/os/real/x;

    invoke-virtual {v0, v6, v1, v1}, Lcom/google/android/location/os/real/x;->a(IIZ)Z

    .line 918
    :cond_0
    :goto_0
    return-void

    .line 809
    :cond_1
    const-string v4, "android.intent.action.SCREEN_ON"

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 810
    iget-object v2, p0, Lcom/google/android/location/os/real/z;->a:Lcom/google/android/location/os/real/x;

    invoke-virtual {v2, v6, v0, v1}, Lcom/google/android/location/os/real/x;->a(IIZ)Z

    goto :goto_0

    .line 811
    :cond_2
    iget-object v4, p0, Lcom/google/android/location/os/real/z;->a:Lcom/google/android/location/os/real/x;

    iget-object v4, v4, Lcom/google/android/location/os/real/x;->a:Ljava/lang/String;

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 815
    iget-object v2, p0, Lcom/google/android/location/os/real/z;->a:Lcom/google/android/location/os/real/x;

    invoke-virtual {v2, v5, v1, v0}, Lcom/google/android/location/os/real/x;->a(IIZ)Z

    goto :goto_0

    .line 816
    :cond_3
    iget-object v4, p0, Lcom/google/android/location/os/real/z;->a:Lcom/google/android/location/os/real/x;

    iget-object v4, v4, Lcom/google/android/location/os/real/x;->b:Ljava/lang/String;

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 820
    iget-object v1, p0, Lcom/google/android/location/os/real/z;->a:Lcom/google/android/location/os/real/x;

    invoke-virtual {v1, v5, v0, v0}, Lcom/google/android/location/os/real/x;->a(IIZ)Z

    goto :goto_0

    .line 821
    :cond_4
    iget-object v4, p0, Lcom/google/android/location/os/real/z;->a:Lcom/google/android/location/os/real/x;

    iget-object v4, v4, Lcom/google/android/location/os/real/x;->c:Ljava/lang/String;

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 825
    iget-object v1, p0, Lcom/google/android/location/os/real/z;->a:Lcom/google/android/location/os/real/x;

    invoke-virtual {v1, v5, v2, v0}, Lcom/google/android/location/os/real/x;->a(IIZ)Z

    goto :goto_0

    .line 826
    :cond_5
    iget-object v4, p0, Lcom/google/android/location/os/real/z;->a:Lcom/google/android/location/os/real/x;

    iget-object v4, v4, Lcom/google/android/location/os/real/x;->d:Ljava/lang/String;

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_6

    .line 830
    iget-object v1, p0, Lcom/google/android/location/os/real/z;->a:Lcom/google/android/location/os/real/x;

    const/4 v2, 0x3

    invoke-virtual {v1, v5, v2, v0}, Lcom/google/android/location/os/real/x;->a(IIZ)Z

    goto :goto_0

    .line 831
    :cond_6
    iget-object v4, p0, Lcom/google/android/location/os/real/z;->a:Lcom/google/android/location/os/real/x;

    iget-object v4, v4, Lcom/google/android/location/os/real/x;->e:Ljava/lang/String;

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_7

    .line 835
    iget-object v1, p0, Lcom/google/android/location/os/real/z;->a:Lcom/google/android/location/os/real/x;

    const/4 v2, 0x4

    invoke-virtual {v1, v5, v2, v0}, Lcom/google/android/location/os/real/x;->a(IIZ)Z

    goto :goto_0

    .line 836
    :cond_7
    iget-object v4, p0, Lcom/google/android/location/os/real/z;->a:Lcom/google/android/location/os/real/x;

    iget-object v4, v4, Lcom/google/android/location/os/real/x;->f:Ljava/lang/String;

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_8

    .line 840
    iget-object v1, p0, Lcom/google/android/location/os/real/z;->a:Lcom/google/android/location/os/real/x;

    const/4 v2, 0x5

    invoke-virtual {v1, v5, v2, v0}, Lcom/google/android/location/os/real/x;->a(IIZ)Z

    goto :goto_0

    .line 841
    :cond_8
    iget-object v4, p0, Lcom/google/android/location/os/real/z;->a:Lcom/google/android/location/os/real/x;

    iget-object v4, v4, Lcom/google/android/location/os/real/x;->g:Ljava/lang/String;

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_9

    .line 845
    iget-object v1, p0, Lcom/google/android/location/os/real/z;->a:Lcom/google/android/location/os/real/x;

    invoke-virtual {v1, v5, v5, v0}, Lcom/google/android/location/os/real/x;->a(IIZ)Z

    goto/16 :goto_0

    .line 846
    :cond_9
    iget-object v4, p0, Lcom/google/android/location/os/real/z;->a:Lcom/google/android/location/os/real/x;

    iget-object v4, v4, Lcom/google/android/location/os/real/x;->h:Ljava/lang/String;

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_a

    .line 850
    iget-object v1, p0, Lcom/google/android/location/os/real/z;->a:Lcom/google/android/location/os/real/x;

    const/4 v2, 0x7

    invoke-virtual {v1, v5, v2, v0}, Lcom/google/android/location/os/real/x;->a(IIZ)Z

    goto/16 :goto_0

    .line 851
    :cond_a
    iget-object v4, p0, Lcom/google/android/location/os/real/z;->a:Lcom/google/android/location/os/real/x;

    iget-object v4, v4, Lcom/google/android/location/os/real/x;->i:Ljava/lang/String;

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_b

    .line 855
    iget-object v1, p0, Lcom/google/android/location/os/real/z;->a:Lcom/google/android/location/os/real/x;

    const/16 v2, 0x8

    invoke-virtual {v1, v5, v2, v0}, Lcom/google/android/location/os/real/x;->a(IIZ)Z

    goto/16 :goto_0

    .line 856
    :cond_b
    iget-object v4, p0, Lcom/google/android/location/os/real/z;->a:Lcom/google/android/location/os/real/x;

    iget-object v4, v4, Lcom/google/android/location/os/real/x;->j:Ljava/lang/String;

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_c

    .line 860
    iget-object v1, p0, Lcom/google/android/location/os/real/z;->a:Lcom/google/android/location/os/real/x;

    const/16 v2, 0x9

    invoke-virtual {v1, v5, v2, v0}, Lcom/google/android/location/os/real/x;->a(IIZ)Z

    goto/16 :goto_0

    .line 861
    :cond_c
    iget-object v4, p0, Lcom/google/android/location/os/real/z;->a:Lcom/google/android/location/os/real/x;

    iget-object v4, v4, Lcom/google/android/location/os/real/x;->k:Ljava/lang/String;

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_d

    .line 865
    iget-object v1, p0, Lcom/google/android/location/os/real/z;->a:Lcom/google/android/location/os/real/x;

    const/16 v2, 0xa

    invoke-virtual {v1, v5, v2, v0}, Lcom/google/android/location/os/real/x;->a(IIZ)Z

    goto/16 :goto_0

    .line 866
    :cond_d
    iget-object v4, p0, Lcom/google/android/location/os/real/z;->a:Lcom/google/android/location/os/real/x;

    iget-object v4, v4, Lcom/google/android/location/os/real/x;->l:Ljava/lang/String;

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_e

    .line 870
    iget-object v1, p0, Lcom/google/android/location/os/real/z;->a:Lcom/google/android/location/os/real/x;

    const/16 v2, 0xb

    invoke-virtual {v1, v5, v2, v0}, Lcom/google/android/location/os/real/x;->a(IIZ)Z

    goto/16 :goto_0

    .line 871
    :cond_e
    iget-object v4, p0, Lcom/google/android/location/os/real/z;->a:Lcom/google/android/location/os/real/x;

    iget-object v4, v4, Lcom/google/android/location/os/real/x;->m:Ljava/lang/String;

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_f

    .line 875
    iget-object v1, p0, Lcom/google/android/location/os/real/z;->a:Lcom/google/android/location/os/real/x;

    invoke-virtual {v1, v5, v6, v0}, Lcom/google/android/location/os/real/x;->a(IIZ)Z

    goto/16 :goto_0

    .line 876
    :cond_f
    const-string v4, "android.net.wifi.SCAN_RESULTS"

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_10

    .line 877
    iget-object v0, p0, Lcom/google/android/location/os/real/z;->a:Lcom/google/android/location/os/real/x;

    iget-object v2, p0, Lcom/google/android/location/os/real/z;->b:Landroid/net/wifi/WifiManager;

    invoke-virtual {v2}, Landroid/net/wifi/WifiManager;->getScanResults()Ljava/util/List;

    move-result-object v2

    invoke-virtual {v0, v2, v1}, Lcom/google/android/location/os/real/x;->a(Ljava/util/List;Z)V

    goto/16 :goto_0

    .line 878
    :cond_10
    const-string v4, "android.net.wifi.WIFI_STATE_CHANGED"

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_16

    .line 879
    const-string v3, "wifi_state"

    const/4 v4, 0x4

    invoke-virtual {p2, v3, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v3

    .line 882
    const/4 v4, 0x3

    if-eq v3, v4, :cond_11

    if-ne v3, v0, :cond_0

    .line 887
    :cond_11
    iget-object v3, p0, Lcom/google/android/location/os/real/z;->b:Landroid/net/wifi/WifiManager;

    invoke-virtual {v3}, Landroid/net/wifi/WifiManager;->isWifiEnabled()Z

    move-result v4

    .line 888
    if-nez v4, :cond_12

    invoke-static {}, Lcom/google/android/location/os/real/bf;->a()Lcom/google/android/location/os/real/bf;

    move-result-object v3

    iget-object v5, p0, Lcom/google/android/location/os/real/z;->b:Landroid/net/wifi/WifiManager;

    invoke-virtual {v3, v5, p1}, Lcom/google/android/location/os/real/bf;->a(Landroid/net/wifi/WifiManager;Landroid/content/Context;)Z

    move-result v3

    if-eqz v3, :cond_13

    :cond_12
    move v3, v0

    .line 890
    :goto_1
    if-eqz v4, :cond_14

    :goto_2
    if-eqz v3, :cond_15

    :goto_3
    or-int/2addr v0, v2

    .line 893
    iget-object v2, p0, Lcom/google/android/location/os/real/z;->a:Lcom/google/android/location/os/real/x;

    const/16 v3, 0x9

    invoke-virtual {v2, v3, v0, v1}, Lcom/google/android/location/os/real/x;->a(IIZ)Z

    goto/16 :goto_0

    :cond_13
    move v3, v1

    .line 888
    goto :goto_1

    :cond_14
    move v2, v1

    .line 890
    goto :goto_2

    :cond_15
    move v0, v1

    goto :goto_3

    .line 895
    :cond_16
    const-string v2, "android.intent.action.BATTERY_CHANGED"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_17

    .line 896
    iget-object v0, p0, Lcom/google/android/location/os/real/z;->a:Lcom/google/android/location/os/real/x;

    invoke-virtual {p2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    const/16 v3, 0xa

    invoke-virtual {v0, v3, v2, v1}, Lcom/google/android/location/os/real/x;->a(ILjava/lang/Object;Z)Z

    goto/16 :goto_0

    .line 897
    :cond_17
    const-string v2, "android.intent.action.AIRPLANE_MODE"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_19

    .line 898
    iget-object v2, p0, Lcom/google/android/location/os/real/z;->a:Lcom/google/android/location/os/real/x;

    invoke-static {p1}, Lcom/google/android/location/os/real/x;->a(Landroid/content/Context;)Z

    move-result v3

    if-eqz v3, :cond_18

    :goto_4
    const/16 v3, 0xe

    invoke-virtual {v2, v3, v0, v1}, Lcom/google/android/location/os/real/x;->a(IIZ)Z

    goto/16 :goto_0

    :cond_18
    move v0, v1

    goto :goto_4

    .line 899
    :cond_19
    const-string v0, "android.net.conn.CONNECTIVITY_CHANGE"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1a

    .line 900
    iget-object v0, p0, Lcom/google/android/location/os/real/z;->a:Lcom/google/android/location/os/real/x;

    const/16 v2, 0x10

    invoke-virtual {v0, v2, v1, v1}, Lcom/google/android/location/os/real/x;->a(IIZ)Z

    goto/16 :goto_0

    .line 901
    :cond_1a
    const-string v0, "android.os.action.POWER_SAVE_MODE_CHANGED"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1b

    .line 902
    iget-object v0, p0, Lcom/google/android/location/os/real/z;->a:Lcom/google/android/location/os/real/x;

    const/16 v2, 0x22

    invoke-virtual {v0, v2, v1, v1}, Lcom/google/android/location/os/real/x;->a(IIZ)Z

    goto/16 :goto_0

    .line 904
    :cond_1b
    invoke-static {}, Lcom/google/android/location/os/real/bf;->a()Lcom/google/android/location/os/real/bf;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/google/android/location/os/real/bf;->a(Ljava/lang/String;)I

    move-result v0

    .line 906
    packed-switch v0, :pswitch_data_0

    .line 914
    sget-boolean v0, Lcom/google/android/location/i/a;->e:Z

    if-eqz v0, :cond_0

    const-string v0, "NetworkLocationCallbackRunner"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unexpected action "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/location/o/a/a;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 908
    :pswitch_0
    iget-object v0, p0, Lcom/google/android/location/os/real/z;->a:Lcom/google/android/location/os/real/x;

    const/16 v2, 0x1a

    invoke-virtual {v0, v2, v1, v1}, Lcom/google/android/location/os/real/x;->a(IIZ)Z

    goto/16 :goto_0

    .line 911
    :pswitch_1
    iget-object v0, p0, Lcom/google/android/location/os/real/z;->a:Lcom/google/android/location/os/real/x;

    const/16 v2, 0x19

    invoke-virtual {v0, v2, v1, v1}, Lcom/google/android/location/os/real/x;->a(IIZ)Z

    goto/16 :goto_0

    .line 906
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
