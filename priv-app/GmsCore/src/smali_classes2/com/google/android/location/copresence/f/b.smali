.class public final Lcom/google/android/location/copresence/f/b;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final a:Lcom/google/ac/b/c/g;

.field private static b:Lcom/google/android/location/copresence/f/b;


# instance fields
.field private final c:Landroid/content/SharedPreferences;

.field private final d:Lcom/google/ac/b/c/g;

.field private final e:Lcom/google/ac/b/c/g;

.field private f:Lcom/google/ac/b/c/g;

.field private g:Lcom/google/ac/b/c/g;

.field private final h:Ljava/lang/Object;

.field private i:Ljava/lang/String;

.field private j:Lcom/google/ac/b/c/g;

.field private final k:Ljava/util/ArrayList;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 66
    invoke-static {}, Lcom/google/android/location/copresence/f/b;->f()Lcom/google/ac/b/c/g;

    move-result-object v0

    sput-object v0, Lcom/google/android/location/copresence/f/b;->a:Lcom/google/ac/b/c/g;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .locals 6

    .prologue
    .line 113
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 84
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/location/copresence/f/b;->h:Ljava/lang/Object;

    .line 114
    const/4 v0, 0x3

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 115
    const-string v0, "ConfigurationManager:  Creating configuration manager"

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->b(Ljava/lang/String;)I

    .line 118
    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p1, p2, v0}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/copresence/f/b;->c:Landroid/content/SharedPreferences;

    .line 121
    invoke-static {}, Lcom/google/android/location/copresence/f/b;->f()Lcom/google/ac/b/c/g;

    move-result-object v0

    iget-object v1, v0, Lcom/google/ac/b/c/g;->a:Lcom/google/ac/b/c/l;

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    iput-object v2, v1, Lcom/google/ac/b/c/l;->a:Ljava/lang/Boolean;

    iget-object v1, v0, Lcom/google/ac/b/c/g;->a:Lcom/google/ac/b/c/l;

    const-wide/high16 v2, 0x4069000000000000L    # 200.0

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    iput-object v2, v1, Lcom/google/ac/b/c/l;->b:Ljava/lang/Double;

    iget-object v1, v0, Lcom/google/ac/b/c/g;->b:Lcom/google/ac/b/c/o;

    const-string v2, "GCS"

    iput-object v2, v1, Lcom/google/ac/b/c/o;->b:Ljava/lang/String;

    iget-object v1, v0, Lcom/google/ac/b/c/g;->b:Lcom/google/ac/b/c/o;

    const/4 v2, 0x6

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iput-object v2, v1, Lcom/google/ac/b/c/o;->a:Ljava/lang/Integer;

    const-string v1, ""

    iput-object v1, v0, Lcom/google/ac/b/c/g;->c:Ljava/lang/String;

    iget-object v1, v0, Lcom/google/ac/b/c/g;->d:Lcom/google/ac/b/c/h;

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    iput-object v2, v1, Lcom/google/ac/b/c/h;->a:Ljava/lang/Boolean;

    iget-object v1, v0, Lcom/google/ac/b/c/g;->d:Lcom/google/ac/b/c/h;

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    iput-object v2, v1, Lcom/google/ac/b/c/h;->b:Ljava/lang/Boolean;

    iget-object v1, v0, Lcom/google/ac/b/c/g;->d:Lcom/google/ac/b/c/h;

    const-string v2, ""

    iput-object v2, v1, Lcom/google/ac/b/c/h;->c:Ljava/lang/String;

    iget-object v1, v0, Lcom/google/ac/b/c/g;->e:Lcom/google/ac/b/c/m;

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    iput-object v2, v1, Lcom/google/ac/b/c/m;->a:Ljava/lang/Boolean;

    iget-object v1, v0, Lcom/google/ac/b/c/g;->e:Lcom/google/ac/b/c/m;

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    iput-object v2, v1, Lcom/google/ac/b/c/m;->b:Ljava/lang/Boolean;

    iget-object v1, v0, Lcom/google/ac/b/c/g;->e:Lcom/google/ac/b/c/m;

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    iput-object v2, v1, Lcom/google/ac/b/c/m;->c:Ljava/lang/Boolean;

    iget-object v1, v0, Lcom/google/ac/b/c/g;->e:Lcom/google/ac/b/c/m;

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    iput-object v2, v1, Lcom/google/ac/b/c/m;->A:Ljava/lang/Boolean;

    iget-object v1, v0, Lcom/google/ac/b/c/g;->e:Lcom/google/ac/b/c/m;

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    iput-object v2, v1, Lcom/google/ac/b/c/m;->B:Ljava/lang/Boolean;

    iget-object v1, v0, Lcom/google/ac/b/c/g;->e:Lcom/google/ac/b/c/m;

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    iput-object v2, v1, Lcom/google/ac/b/c/m;->d:Ljava/lang/Boolean;

    iget-object v1, v0, Lcom/google/ac/b/c/g;->e:Lcom/google/ac/b/c/m;

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    iput-object v2, v1, Lcom/google/ac/b/c/m;->e:Ljava/lang/Boolean;

    iget-object v1, v0, Lcom/google/ac/b/c/g;->e:Lcom/google/ac/b/c/m;

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    iput-object v2, v1, Lcom/google/ac/b/c/m;->f:Ljava/lang/Boolean;

    iget-object v1, v0, Lcom/google/ac/b/c/g;->e:Lcom/google/ac/b/c/m;

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    iput-object v2, v1, Lcom/google/ac/b/c/m;->g:Ljava/lang/Boolean;

    iget-object v1, v0, Lcom/google/ac/b/c/g;->e:Lcom/google/ac/b/c/m;

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    iput-object v2, v1, Lcom/google/ac/b/c/m;->h:Ljava/lang/Boolean;

    iget-object v1, v0, Lcom/google/ac/b/c/g;->e:Lcom/google/ac/b/c/m;

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    iput-object v2, v1, Lcom/google/ac/b/c/m;->i:Ljava/lang/Boolean;

    iget-object v1, v0, Lcom/google/ac/b/c/g;->e:Lcom/google/ac/b/c/m;

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    iput-object v2, v1, Lcom/google/ac/b/c/m;->j:Ljava/lang/Boolean;

    iget-object v1, v0, Lcom/google/ac/b/c/g;->e:Lcom/google/ac/b/c/m;

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    iput-object v2, v1, Lcom/google/ac/b/c/m;->k:Ljava/lang/Boolean;

    iget-object v1, v0, Lcom/google/ac/b/c/g;->e:Lcom/google/ac/b/c/m;

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    iput-object v2, v1, Lcom/google/ac/b/c/m;->l:Ljava/lang/Boolean;

    iget-object v1, v0, Lcom/google/ac/b/c/g;->e:Lcom/google/ac/b/c/m;

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    iput-object v2, v1, Lcom/google/ac/b/c/m;->m:Ljava/lang/Boolean;

    iget-object v1, v0, Lcom/google/ac/b/c/g;->e:Lcom/google/ac/b/c/m;

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    iput-object v2, v1, Lcom/google/ac/b/c/m;->n:Ljava/lang/Boolean;

    iget-object v1, v0, Lcom/google/ac/b/c/g;->e:Lcom/google/ac/b/c/m;

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    iput-object v2, v1, Lcom/google/ac/b/c/m;->o:Ljava/lang/Boolean;

    iget-object v1, v0, Lcom/google/ac/b/c/g;->e:Lcom/google/ac/b/c/m;

    const-wide/16 v2, 0x7530

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    iput-object v2, v1, Lcom/google/ac/b/c/m;->p:Ljava/lang/Long;

    iget-object v1, v0, Lcom/google/ac/b/c/g;->e:Lcom/google/ac/b/c/m;

    const-wide/32 v2, 0x240c8400

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    iput-object v2, v1, Lcom/google/ac/b/c/m;->q:Ljava/lang/Long;

    iget-object v1, v0, Lcom/google/ac/b/c/g;->e:Lcom/google/ac/b/c/m;

    const/4 v2, 0x3

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iput-object v2, v1, Lcom/google/ac/b/c/m;->r:Ljava/lang/Integer;

    iget-object v1, v0, Lcom/google/ac/b/c/g;->e:Lcom/google/ac/b/c/m;

    const-wide/16 v2, 0x3e8

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    iput-object v2, v1, Lcom/google/ac/b/c/m;->s:Ljava/lang/Long;

    iget-object v1, v0, Lcom/google/ac/b/c/g;->e:Lcom/google/ac/b/c/m;

    sget-object v2, Ljava/util/concurrent/TimeUnit;->DAYS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v4, 0x1

    invoke-virtual {v2, v4, v5}, Ljava/util/concurrent/TimeUnit;->toSeconds(J)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    iput-object v2, v1, Lcom/google/ac/b/c/m;->t:Ljava/lang/Long;

    iget-object v1, v0, Lcom/google/ac/b/c/g;->e:Lcom/google/ac/b/c/m;

    const-wide/16 v2, 0x1388

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    iput-object v2, v1, Lcom/google/ac/b/c/m;->u:Ljava/lang/Long;

    iget-object v1, v0, Lcom/google/ac/b/c/g;->e:Lcom/google/ac/b/c/m;

    const-wide/32 v2, 0x927c0

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    iput-object v2, v1, Lcom/google/ac/b/c/m;->v:Ljava/lang/Long;

    iget-object v1, v0, Lcom/google/ac/b/c/g;->e:Lcom/google/ac/b/c/m;

    const-string v2, ""

    iput-object v2, v1, Lcom/google/ac/b/c/m;->w:Ljava/lang/String;

    iget-object v1, v0, Lcom/google/ac/b/c/g;->e:Lcom/google/ac/b/c/m;

    const-wide/16 v2, 0x7d0

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    iput-object v2, v1, Lcom/google/ac/b/c/m;->x:Ljava/lang/Long;

    iget-object v1, v0, Lcom/google/ac/b/c/g;->e:Lcom/google/ac/b/c/m;

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    iput-object v2, v1, Lcom/google/ac/b/c/m;->y:Ljava/lang/Boolean;

    iget-object v1, v0, Lcom/google/ac/b/c/g;->e:Lcom/google/ac/b/c/m;

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    iput-object v2, v1, Lcom/google/ac/b/c/m;->z:Ljava/lang/Boolean;

    iget-object v1, v0, Lcom/google/ac/b/c/g;->e:Lcom/google/ac/b/c/m;

    const-wide/16 v2, 0x1388

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    iput-object v2, v1, Lcom/google/ac/b/c/m;->C:Ljava/lang/Long;

    iget-object v1, v0, Lcom/google/ac/b/c/g;->h:Lcom/google/ac/b/c/e;

    const-wide/16 v2, 0x1388

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    iput-object v2, v1, Lcom/google/ac/b/c/e;->a:Ljava/lang/Long;

    iget-object v1, v0, Lcom/google/ac/b/c/g;->h:Lcom/google/ac/b/c/e;

    const-wide/16 v2, 0x12c

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    iput-object v2, v1, Lcom/google/ac/b/c/e;->b:Ljava/lang/Long;

    iget-object v1, v0, Lcom/google/ac/b/c/g;->h:Lcom/google/ac/b/c/e;

    const-wide/16 v2, 0x1f40

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    iput-object v2, v1, Lcom/google/ac/b/c/e;->c:Ljava/lang/Long;

    iget-object v1, v0, Lcom/google/ac/b/c/g;->h:Lcom/google/ac/b/c/e;

    const-wide/16 v2, 0xbb8

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    iput-object v2, v1, Lcom/google/ac/b/c/e;->d:Ljava/lang/Long;

    iget-object v1, v0, Lcom/google/ac/b/c/g;->h:Lcom/google/ac/b/c/e;

    const-wide/16 v2, 0x2710

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    iput-object v2, v1, Lcom/google/ac/b/c/e;->e:Ljava/lang/Long;

    iget-object v1, v0, Lcom/google/ac/b/c/g;->h:Lcom/google/ac/b/c/e;

    const-wide/16 v2, 0x1388

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    iput-object v2, v1, Lcom/google/ac/b/c/e;->f:Ljava/lang/Long;

    iget-object v1, v0, Lcom/google/ac/b/c/g;->h:Lcom/google/ac/b/c/e;

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    iput-object v2, v1, Lcom/google/ac/b/c/e;->g:Ljava/lang/Boolean;

    iget-object v1, v0, Lcom/google/ac/b/c/g;->h:Lcom/google/ac/b/c/e;

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    iput-object v2, v1, Lcom/google/ac/b/c/e;->h:Ljava/lang/Boolean;

    iget-object v1, v0, Lcom/google/ac/b/c/g;->h:Lcom/google/ac/b/c/e;

    const-wide/32 v2, 0x1b7740

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    iput-object v2, v1, Lcom/google/ac/b/c/e;->i:Ljava/lang/Long;

    iget-object v1, v0, Lcom/google/ac/b/c/g;->h:Lcom/google/ac/b/c/e;

    const-wide/16 v2, 0x3a98

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    iput-object v2, v1, Lcom/google/ac/b/c/e;->j:Ljava/lang/Long;

    iget-object v1, v0, Lcom/google/ac/b/c/g;->h:Lcom/google/ac/b/c/e;

    const-wide/16 v2, 0x0

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    iput-object v2, v1, Lcom/google/ac/b/c/e;->k:Ljava/lang/Long;

    iget-object v1, v0, Lcom/google/ac/b/c/g;->h:Lcom/google/ac/b/c/e;

    const-wide/16 v2, 0x3e8

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    iput-object v2, v1, Lcom/google/ac/b/c/e;->l:Ljava/lang/Long;

    iget-object v1, v0, Lcom/google/ac/b/c/g;->h:Lcom/google/ac/b/c/e;

    const-wide/16 v2, 0x0

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    iput-object v2, v1, Lcom/google/ac/b/c/e;->m:Ljava/lang/Long;

    iget-object v1, v0, Lcom/google/ac/b/c/g;->i:Lcom/google/ac/b/c/d;

    const-wide/16 v2, 0xbb8

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    iput-object v2, v1, Lcom/google/ac/b/c/d;->a:Ljava/lang/Long;

    iget-object v1, v0, Lcom/google/ac/b/c/g;->i:Lcom/google/ac/b/c/d;

    const-wide/16 v2, 0x3e8

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    iput-object v2, v1, Lcom/google/ac/b/c/d;->b:Ljava/lang/Long;

    iget-object v1, v0, Lcom/google/ac/b/c/g;->i:Lcom/google/ac/b/c/d;

    const-wide/16 v2, 0x1388

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    iput-object v2, v1, Lcom/google/ac/b/c/d;->c:Ljava/lang/Long;

    iget-object v1, v0, Lcom/google/ac/b/c/g;->j:Lcom/google/ac/b/c/p;

    const-wide/16 v2, 0xbb8

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    iput-object v2, v1, Lcom/google/ac/b/c/p;->a:Ljava/lang/Long;

    iget-object v1, v0, Lcom/google/ac/b/c/g;->j:Lcom/google/ac/b/c/p;

    const-wide/16 v2, 0x7d0

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    iput-object v2, v1, Lcom/google/ac/b/c/p;->b:Ljava/lang/Long;

    iget-object v1, v0, Lcom/google/ac/b/c/g;->j:Lcom/google/ac/b/c/p;

    const-wide/16 v2, 0x0

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    iput-object v2, v1, Lcom/google/ac/b/c/p;->c:Ljava/lang/Long;

    iget-object v1, v0, Lcom/google/ac/b/c/g;->j:Lcom/google/ac/b/c/p;

    const-wide/16 v2, 0x7d0

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    iput-object v2, v1, Lcom/google/ac/b/c/p;->e:Ljava/lang/Long;

    iget-object v1, v0, Lcom/google/ac/b/c/g;->j:Lcom/google/ac/b/c/p;

    const-wide/16 v2, 0x0

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    iput-object v2, v1, Lcom/google/ac/b/c/p;->d:Ljava/lang/Long;

    iget-object v1, v0, Lcom/google/ac/b/c/g;->k:Lcom/google/ac/b/c/n;

    const-wide/16 v2, 0x3a98

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    iput-object v2, v1, Lcom/google/ac/b/c/n;->a:Ljava/lang/Long;

    iget-object v1, v0, Lcom/google/ac/b/c/g;->k:Lcom/google/ac/b/c/n;

    const-wide/16 v2, 0x2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    iput-object v2, v1, Lcom/google/ac/b/c/n;->b:Ljava/lang/Long;

    iget-object v1, v0, Lcom/google/ac/b/c/g;->k:Lcom/google/ac/b/c/n;

    const-wide/32 v2, 0x927c0

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    iput-object v2, v1, Lcom/google/ac/b/c/n;->c:Ljava/lang/Long;

    iget-object v1, v0, Lcom/google/ac/b/c/g;->l:Lcom/google/ac/b/c/j;

    const-wide/high16 v2, 0x403e000000000000L    # 30.0

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    iput-object v2, v1, Lcom/google/ac/b/c/j;->a:Ljava/lang/Double;

    iget-object v1, v0, Lcom/google/ac/b/c/g;->l:Lcom/google/ac/b/c/j;

    const-wide/16 v2, 0x258

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    iput-object v2, v1, Lcom/google/ac/b/c/j;->b:Ljava/lang/Long;

    iget-object v1, v0, Lcom/google/ac/b/c/g;->l:Lcom/google/ac/b/c/j;

    const-wide/high16 v2, 0x4024000000000000L    # 10.0

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    iput-object v2, v1, Lcom/google/ac/b/c/j;->c:Ljava/lang/Double;

    iget-object v1, v0, Lcom/google/ac/b/c/g;->l:Lcom/google/ac/b/c/j;

    const-wide/16 v2, 0x4e20

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    iput-object v2, v1, Lcom/google/ac/b/c/j;->d:Ljava/lang/Long;

    iget-object v1, v0, Lcom/google/ac/b/c/g;->l:Lcom/google/ac/b/c/j;

    const-wide/16 v2, 0x3e8

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    iput-object v2, v1, Lcom/google/ac/b/c/j;->e:Ljava/lang/Long;

    iget-object v1, v0, Lcom/google/ac/b/c/g;->f:Lcom/google/ac/b/c/f;

    const-wide/16 v2, 0x0

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    iput-object v2, v1, Lcom/google/ac/b/c/f;->a:Ljava/lang/Long;

    iget-object v1, v0, Lcom/google/ac/b/c/g;->f:Lcom/google/ac/b/c/f;

    const-wide/16 v2, 0x12c

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    iput-object v2, v1, Lcom/google/ac/b/c/f;->b:Ljava/lang/Long;

    iget-object v1, v0, Lcom/google/ac/b/c/g;->f:Lcom/google/ac/b/c/f;

    const-wide/16 v2, 0x960

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    iput-object v2, v1, Lcom/google/ac/b/c/f;->c:Ljava/lang/Long;

    iget-object v1, v0, Lcom/google/ac/b/c/g;->f:Lcom/google/ac/b/c/f;

    const-wide/16 v2, 0x1f4

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    iput-object v2, v1, Lcom/google/ac/b/c/f;->d:Ljava/lang/Long;

    iget-object v1, v0, Lcom/google/ac/b/c/g;->f:Lcom/google/ac/b/c/f;

    const-wide/16 v2, 0x44c

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    iput-object v2, v1, Lcom/google/ac/b/c/f;->e:Ljava/lang/Long;

    iget-object v1, v0, Lcom/google/ac/b/c/g;->f:Lcom/google/ac/b/c/f;

    const-wide/16 v2, 0x5dc

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    iput-object v2, v1, Lcom/google/ac/b/c/f;->f:Ljava/lang/Long;

    iget-object v1, v0, Lcom/google/ac/b/c/g;->f:Lcom/google/ac/b/c/f;

    const-wide/16 v2, 0xfa0

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    iput-object v2, v1, Lcom/google/ac/b/c/f;->g:Ljava/lang/Long;

    iget-object v1, v0, Lcom/google/ac/b/c/g;->f:Lcom/google/ac/b/c/f;

    const-wide/16 v2, 0x12c

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    iput-object v2, v1, Lcom/google/ac/b/c/f;->h:Ljava/lang/Long;

    iget-object v1, v0, Lcom/google/ac/b/c/g;->f:Lcom/google/ac/b/c/f;

    const-wide/16 v2, 0x96

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    iput-object v2, v1, Lcom/google/ac/b/c/f;->i:Ljava/lang/Long;

    iget-object v1, v0, Lcom/google/ac/b/c/g;->g:Lcom/google/ac/b/c/b;

    const/4 v2, 0x5

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iput-object v2, v1, Lcom/google/ac/b/c/b;->a:Ljava/lang/Integer;

    iget-object v1, v0, Lcom/google/ac/b/c/g;->g:Lcom/google/ac/b/c/b;

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    iput-object v2, v1, Lcom/google/ac/b/c/b;->b:Ljava/lang/Boolean;

    iget-object v1, v0, Lcom/google/ac/b/c/g;->g:Lcom/google/ac/b/c/b;

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    iput-object v2, v1, Lcom/google/ac/b/c/b;->c:Ljava/lang/Boolean;

    iget-object v1, v0, Lcom/google/ac/b/c/g;->g:Lcom/google/ac/b/c/b;

    const-wide v2, 0x40d2110000000000L    # 18500.0

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    iput-object v2, v1, Lcom/google/ac/b/c/b;->d:Ljava/lang/Double;

    iget-object v1, v0, Lcom/google/ac/b/c/g;->g:Lcom/google/ac/b/c/b;

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    iput-object v2, v1, Lcom/google/ac/b/c/b;->e:Ljava/lang/Boolean;

    iget-object v1, v0, Lcom/google/ac/b/c/g;->g:Lcom/google/ac/b/c/b;

    const/4 v2, 0x7

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iput-object v2, v1, Lcom/google/ac/b/c/b;->f:Ljava/lang/Integer;

    iget-object v1, v0, Lcom/google/ac/b/c/g;->g:Lcom/google/ac/b/c/b;

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iput-object v2, v1, Lcom/google/ac/b/c/b;->g:Ljava/lang/Integer;

    iget-object v1, v0, Lcom/google/ac/b/c/g;->g:Lcom/google/ac/b/c/b;

    const-wide v2, 0x40e7700000000000L    # 48000.0

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    iput-object v2, v1, Lcom/google/ac/b/c/b;->h:Ljava/lang/Double;

    iget-object v1, v0, Lcom/google/ac/b/c/g;->g:Lcom/google/ac/b/c/b;

    const/4 v2, 0x4

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iput-object v2, v1, Lcom/google/ac/b/c/b;->i:Ljava/lang/Integer;

    iget-object v1, v0, Lcom/google/ac/b/c/g;->g:Lcom/google/ac/b/c/b;

    const/4 v2, 0x4

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iput-object v2, v1, Lcom/google/ac/b/c/b;->j:Ljava/lang/Integer;

    iget-object v1, v0, Lcom/google/ac/b/c/g;->g:Lcom/google/ac/b/c/b;

    const/4 v2, 0x4

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iput-object v2, v1, Lcom/google/ac/b/c/b;->k:Ljava/lang/Integer;

    iget-object v1, v0, Lcom/google/ac/b/c/g;->g:Lcom/google/ac/b/c/b;

    const/16 v2, 0x10

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iput-object v2, v1, Lcom/google/ac/b/c/b;->l:Ljava/lang/Integer;

    iget-object v1, v0, Lcom/google/ac/b/c/g;->n:Lcom/google/ac/b/c/c;

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    iput-object v2, v1, Lcom/google/ac/b/c/c;->a:Ljava/lang/Boolean;

    iget-object v1, v0, Lcom/google/ac/b/c/g;->n:Lcom/google/ac/b/c/c;

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    iput-object v2, v1, Lcom/google/ac/b/c/c;->b:Ljava/lang/Boolean;

    iget-object v1, v0, Lcom/google/ac/b/c/g;->n:Lcom/google/ac/b/c/c;

    const-wide v2, 0x40e5888000000000L    # 44100.0

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    iput-object v2, v1, Lcom/google/ac/b/c/c;->c:Ljava/lang/Double;

    iget-object v1, v0, Lcom/google/ac/b/c/g;->n:Lcom/google/ac/b/c/c;

    const/16 v2, 0x10

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iput-object v2, v1, Lcom/google/ac/b/c/c;->d:Ljava/lang/Integer;

    iget-object v1, v0, Lcom/google/ac/b/c/g;->n:Lcom/google/ac/b/c/c;

    const/4 v2, 0x2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iput-object v2, v1, Lcom/google/ac/b/c/c;->e:Ljava/lang/Integer;

    iget-object v1, v0, Lcom/google/ac/b/c/g;->n:Lcom/google/ac/b/c/c;

    const/16 v2, 0x32

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iput-object v2, v1, Lcom/google/ac/b/c/c;->f:Ljava/lang/Integer;

    iget-object v1, v0, Lcom/google/ac/b/c/g;->m:Lcom/google/ac/b/c/k;

    const-wide/32 v2, 0xa4cb800

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    iput-object v2, v1, Lcom/google/ac/b/c/k;->a:Ljava/lang/Long;

    iget-object v1, v0, Lcom/google/ac/b/c/g;->m:Lcom/google/ac/b/c/k;

    const-wide/32 v2, 0x5265c00

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    iput-object v2, v1, Lcom/google/ac/b/c/k;->b:Ljava/lang/Long;

    iget-object v1, v0, Lcom/google/ac/b/c/g;->m:Lcom/google/ac/b/c/k;

    const/16 v2, 0xc8

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iput-object v2, v1, Lcom/google/ac/b/c/k;->c:Ljava/lang/Integer;

    iput-object v0, p0, Lcom/google/android/location/copresence/f/b;->d:Lcom/google/ac/b/c/g;

    .line 122
    const-string v0, "server"

    invoke-direct {p0, v0}, Lcom/google/android/location/copresence/f/b;->a(Ljava/lang/String;)Lcom/google/ac/b/c/g;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/copresence/f/b;->e:Lcom/google/ac/b/c/g;

    .line 123
    const-string v0, "overrides"

    invoke-direct {p0, v0}, Lcom/google/android/location/copresence/f/b;->a(Ljava/lang/String;)Lcom/google/ac/b/c/g;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/copresence/f/b;->f:Lcom/google/ac/b/c/g;

    .line 124
    invoke-direct {p0}, Lcom/google/android/location/copresence/f/b;->g()Lcom/google/ac/b/c/g;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/copresence/f/b;->g:Lcom/google/ac/b/c/g;

    .line 125
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/location/copresence/f/b;->k:Ljava/util/ArrayList;

    .line 127
    invoke-direct {p0}, Lcom/google/android/location/copresence/f/b;->e()V

    .line 129
    iget-object v0, p0, Lcom/google/android/location/copresence/f/b;->f:Lcom/google/ac/b/c/g;

    invoke-static {}, Lcom/google/android/location/copresence/f/b;->f()Lcom/google/ac/b/c/g;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/ac/b/c/g;->equals(Ljava/lang/Object;)Z

    .line 130
    return-void
.end method

.method private a(Ljava/lang/String;)Lcom/google/ac/b/c/g;
    .locals 3

    .prologue
    const/4 v2, 0x6

    .line 218
    iget-object v0, p0, Lcom/google/android/location/copresence/f/b;->c:Landroid/content/SharedPreferences;

    const/4 v1, 0x0

    invoke-interface {v0, p1, v1}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 219
    if-nez v0, :cond_0

    .line 220
    invoke-static {}, Lcom/google/android/location/copresence/f/b;->f()Lcom/google/ac/b/c/g;

    move-result-object v0

    .line 244
    :goto_0
    return-object v0

    .line 226
    :cond_0
    const/4 v1, 0x0

    :try_start_0
    invoke-static {v0, v1}, Landroid/util/Base64;->decode(Ljava/lang/String;I)[B

    move-result-object v1

    .line 228
    invoke-static {}, Lcom/google/android/location/copresence/f/b;->f()Lcom/google/ac/b/c/g;

    move-result-object v0

    .line 229
    invoke-static {v0, v1}, Lcom/google/protobuf/nano/j;->mergeFrom(Lcom/google/protobuf/nano/j;[B)Lcom/google/protobuf/nano/j;
    :try_end_0
    .catch Lcom/google/protobuf/nano/i; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    .line 233
    :catch_0
    move-exception v0

    .line 234
    invoke-static {v2}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 235
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "ConfigurationManager: reading stored configuration error: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/google/protobuf/nano/i;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->e(Ljava/lang/String;)I

    .line 238
    :cond_1
    invoke-direct {p0, p1}, Lcom/google/android/location/copresence/f/b;->b(Ljava/lang/String;)Lcom/google/ac/b/c/g;

    move-result-object v0

    goto :goto_0

    .line 239
    :catch_1
    move-exception v0

    .line 240
    invoke-static {v2}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 241
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "ConfigurationManager: reading stored configuration error: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->e(Ljava/lang/String;)I

    .line 244
    :cond_2
    invoke-direct {p0, p1}, Lcom/google/android/location/copresence/f/b;->b(Ljava/lang/String;)Lcom/google/ac/b/c/g;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(Lcom/google/ac/b/c/i;)Lcom/google/ac/b/c/o;
    .locals 2

    .prologue
    .line 430
    if-eqz p0, :cond_0

    iget-object v0, p0, Lcom/google/ac/b/c/i;->a:Lcom/google/ac/b/c/o;

    if-eqz v0, :cond_0

    .line 431
    iget-object v0, p0, Lcom/google/ac/b/c/i;->a:Lcom/google/ac/b/c/o;

    .line 434
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/google/ac/b/c/o;

    invoke-direct {v0}, Lcom/google/ac/b/c/o;-><init>()V

    const/4 v1, 0x6

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, v0, Lcom/google/ac/b/c/o;->a:Ljava/lang/Integer;

    const-string v1, "GCS"

    iput-object v1, v0, Lcom/google/ac/b/c/o;->b:Ljava/lang/String;

    goto :goto_0
.end method

.method public static declared-synchronized a()Lcom/google/android/location/copresence/f/b;
    .locals 4

    .prologue
    .line 99
    const-class v1, Lcom/google/android/location/copresence/f/b;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/google/android/location/copresence/f/b;->b:Lcom/google/android/location/copresence/f/b;

    if-nez v0, :cond_0

    .line 100
    new-instance v0, Lcom/google/android/location/copresence/f/b;

    invoke-static {}, Lcom/google/android/gms/common/app/GmsApplication;->b()Lcom/google/android/gms/common/app/GmsApplication;

    move-result-object v2

    const-string v3, "copresence_configuration"

    invoke-direct {v0, v2, v3}, Lcom/google/android/location/copresence/f/b;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/location/copresence/f/b;->b:Lcom/google/android/location/copresence/f/b;

    .line 102
    :cond_0
    sget-object v0, Lcom/google/android/location/copresence/f/b;->b:Lcom/google/android/location/copresence/f/b;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 99
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private a(Ljava/lang/String;Lcom/google/ac/b/c/g;)V
    .locals 2

    .prologue
    .line 255
    invoke-static {p2}, Lcom/google/ac/b/c/g;->toByteArray(Lcom/google/protobuf/nano/j;)[B

    move-result-object v0

    .line 256
    const/4 v1, 0x0

    invoke-static {v0, v1}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;

    move-result-object v0

    .line 258
    iget-object v1, p0, Lcom/google/android/location/copresence/f/b;->c:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    .line 259
    invoke-interface {v1, p1, v0}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 260
    invoke-static {v1}, Lcom/android/a/c;->a(Landroid/content/SharedPreferences$Editor;)V

    .line 261
    invoke-direct {p0}, Lcom/google/android/location/copresence/f/b;->e()V

    .line 262
    return-void
.end method

.method public static b()Lcom/google/ac/b/c/g;
    .locals 1

    .prologue
    .line 109
    invoke-static {}, Lcom/google/android/location/copresence/f/b;->a()Lcom/google/android/location/copresence/f/b;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/location/copresence/f/b;->j:Lcom/google/ac/b/c/g;

    return-object v0
.end method

.method private b(Ljava/lang/String;)Lcom/google/ac/b/c/g;
    .locals 1

    .prologue
    .line 249
    invoke-static {}, Lcom/google/android/location/copresence/f/b;->f()Lcom/google/ac/b/c/g;

    move-result-object v0

    .line 250
    invoke-direct {p0, p1, v0}, Lcom/google/android/location/copresence/f/b;->a(Ljava/lang/String;Lcom/google/ac/b/c/g;)V

    .line 251
    return-object v0
.end method

.method private e()V
    .locals 4

    .prologue
    .line 265
    invoke-static {}, Lcom/google/android/location/copresence/f/b;->f()Lcom/google/ac/b/c/g;

    move-result-object v1

    .line 268
    :try_start_0
    iget-object v0, p0, Lcom/google/android/location/copresence/f/b;->d:Lcom/google/ac/b/c/g;

    invoke-static {v0}, Lcom/google/ac/b/c/g;->toByteArray(Lcom/google/protobuf/nano/j;)[B

    move-result-object v0

    invoke-static {v1, v0}, Lcom/google/protobuf/nano/j;->mergeFrom(Lcom/google/protobuf/nano/j;[B)Lcom/google/protobuf/nano/j;

    .line 269
    iget-object v0, p0, Lcom/google/android/location/copresence/f/b;->e:Lcom/google/ac/b/c/g;

    invoke-static {v0}, Lcom/google/ac/b/c/g;->toByteArray(Lcom/google/protobuf/nano/j;)[B

    move-result-object v0

    invoke-static {v1, v0}, Lcom/google/protobuf/nano/j;->mergeFrom(Lcom/google/protobuf/nano/j;[B)Lcom/google/protobuf/nano/j;

    .line 270
    iget-object v0, p0, Lcom/google/android/location/copresence/f/b;->g:Lcom/google/ac/b/c/g;

    invoke-static {v0}, Lcom/google/ac/b/c/g;->toByteArray(Lcom/google/protobuf/nano/j;)[B

    move-result-object v0

    invoke-static {v1, v0}, Lcom/google/protobuf/nano/j;->mergeFrom(Lcom/google/protobuf/nano/j;[B)Lcom/google/protobuf/nano/j;

    .line 271
    iget-object v0, p0, Lcom/google/android/location/copresence/f/b;->f:Lcom/google/ac/b/c/g;

    invoke-static {v0}, Lcom/google/ac/b/c/g;->toByteArray(Lcom/google/protobuf/nano/j;)[B

    move-result-object v0

    invoke-static {v1, v0}, Lcom/google/protobuf/nano/j;->mergeFrom(Lcom/google/protobuf/nano/j;[B)Lcom/google/protobuf/nano/j;
    :try_end_0
    .catch Lcom/google/protobuf/nano/i; {:try_start_0 .. :try_end_0} :catch_0

    .line 278
    :cond_0
    :goto_0
    iput-object v1, p0, Lcom/google/android/location/copresence/f/b;->j:Lcom/google/ac/b/c/g;

    .line 279
    iget-object v0, p0, Lcom/google/android/location/copresence/f/b;->k:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    goto :goto_1

    .line 272
    :catch_0
    move-exception v0

    .line 273
    const/4 v2, 0x6

    invoke-static {v2}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 274
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "ConfigurationManager: merging configuration error: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/google/protobuf/nano/i;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->e(Ljava/lang/String;)I

    goto :goto_0

    .line 282
    :cond_1
    return-void
.end method

.method private static f()Lcom/google/ac/b/c/g;
    .locals 2

    .prologue
    .line 411
    new-instance v0, Lcom/google/ac/b/c/g;

    invoke-direct {v0}, Lcom/google/ac/b/c/g;-><init>()V

    .line 412
    new-instance v1, Lcom/google/ac/b/c/l;

    invoke-direct {v1}, Lcom/google/ac/b/c/l;-><init>()V

    iput-object v1, v0, Lcom/google/ac/b/c/g;->a:Lcom/google/ac/b/c/l;

    .line 413
    new-instance v1, Lcom/google/ac/b/c/o;

    invoke-direct {v1}, Lcom/google/ac/b/c/o;-><init>()V

    iput-object v1, v0, Lcom/google/ac/b/c/g;->b:Lcom/google/ac/b/c/o;

    .line 414
    new-instance v1, Lcom/google/ac/b/c/h;

    invoke-direct {v1}, Lcom/google/ac/b/c/h;-><init>()V

    iput-object v1, v0, Lcom/google/ac/b/c/g;->d:Lcom/google/ac/b/c/h;

    .line 415
    new-instance v1, Lcom/google/ac/b/c/m;

    invoke-direct {v1}, Lcom/google/ac/b/c/m;-><init>()V

    iput-object v1, v0, Lcom/google/ac/b/c/g;->e:Lcom/google/ac/b/c/m;

    .line 416
    new-instance v1, Lcom/google/ac/b/c/b;

    invoke-direct {v1}, Lcom/google/ac/b/c/b;-><init>()V

    iput-object v1, v0, Lcom/google/ac/b/c/g;->g:Lcom/google/ac/b/c/b;

    .line 417
    new-instance v1, Lcom/google/ac/b/c/f;

    invoke-direct {v1}, Lcom/google/ac/b/c/f;-><init>()V

    iput-object v1, v0, Lcom/google/ac/b/c/g;->f:Lcom/google/ac/b/c/f;

    .line 418
    new-instance v1, Lcom/google/ac/b/c/e;

    invoke-direct {v1}, Lcom/google/ac/b/c/e;-><init>()V

    iput-object v1, v0, Lcom/google/ac/b/c/g;->h:Lcom/google/ac/b/c/e;

    .line 419
    new-instance v1, Lcom/google/ac/b/c/d;

    invoke-direct {v1}, Lcom/google/ac/b/c/d;-><init>()V

    iput-object v1, v0, Lcom/google/ac/b/c/g;->i:Lcom/google/ac/b/c/d;

    .line 420
    new-instance v1, Lcom/google/ac/b/c/p;

    invoke-direct {v1}, Lcom/google/ac/b/c/p;-><init>()V

    iput-object v1, v0, Lcom/google/ac/b/c/g;->j:Lcom/google/ac/b/c/p;

    .line 421
    new-instance v1, Lcom/google/ac/b/c/n;

    invoke-direct {v1}, Lcom/google/ac/b/c/n;-><init>()V

    iput-object v1, v0, Lcom/google/ac/b/c/g;->k:Lcom/google/ac/b/c/n;

    .line 422
    new-instance v1, Lcom/google/ac/b/c/j;

    invoke-direct {v1}, Lcom/google/ac/b/c/j;-><init>()V

    iput-object v1, v0, Lcom/google/ac/b/c/g;->l:Lcom/google/ac/b/c/j;

    .line 423
    new-instance v1, Lcom/google/ac/b/c/k;

    invoke-direct {v1}, Lcom/google/ac/b/c/k;-><init>()V

    iput-object v1, v0, Lcom/google/ac/b/c/g;->m:Lcom/google/ac/b/c/k;

    .line 424
    new-instance v1, Lcom/google/ac/b/c/c;

    invoke-direct {v1}, Lcom/google/ac/b/c/c;-><init>()V

    iput-object v1, v0, Lcom/google/ac/b/c/g;->n:Lcom/google/ac/b/c/c;

    .line 426
    return-object v0
.end method

.method private g()Lcom/google/ac/b/c/g;
    .locals 4

    .prologue
    const/4 v3, 0x6

    .line 542
    sget-object v0, Lcom/google/android/location/copresence/k;->F:Lcom/google/android/location/copresence/l;

    invoke-virtual {v0}, Lcom/google/android/location/copresence/l;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 544
    iget-object v1, p0, Lcom/google/android/location/copresence/f/b;->h:Ljava/lang/Object;

    monitor-enter v1

    .line 545
    :try_start_0
    iget-object v2, p0, Lcom/google/android/location/copresence/f/b;->i:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 546
    const/4 v0, 0x0

    monitor-exit v1

    .line 568
    :goto_0
    return-object v0

    .line 548
    :cond_0
    iput-object v0, p0, Lcom/google/android/location/copresence/f/b;->i:Ljava/lang/String;

    .line 549
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 551
    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 552
    sget-object v0, Lcom/google/android/location/copresence/f/b;->a:Lcom/google/ac/b/c/g;

    goto :goto_0

    .line 549
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 555
    :cond_1
    invoke-static {}, Lcom/google/android/location/copresence/f/b;->f()Lcom/google/ac/b/c/g;

    move-result-object v1

    .line 557
    const/16 v2, 0x8

    :try_start_1
    invoke-static {v0, v2}, Landroid/util/Base64;->decode(Ljava/lang/String;I)[B

    move-result-object v0

    .line 558
    invoke-static {v1, v0}, Lcom/google/protobuf/nano/j;->mergeFrom(Lcom/google/protobuf/nano/j;[B)Lcom/google/protobuf/nano/j;
    :try_end_1
    .catch Lcom/google/protobuf/nano/i; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_1

    :cond_2
    :goto_1
    move-object v0, v1

    .line 568
    goto :goto_0

    .line 559
    :catch_0
    move-exception v0

    .line 560
    invoke-static {v3}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 561
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "ConfigurationManager: error reading gservices overrides configuration: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/google/protobuf/nano/i;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->e(Ljava/lang/String;)I

    goto :goto_1

    .line 563
    :catch_1
    move-exception v0

    .line 564
    invoke-static {v3}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 565
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "ConfigurationManager: error reading gservices overrides configuration: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->e(Ljava/lang/String;)I

    goto :goto_1
.end method


# virtual methods
.method public final a(Lcom/google/ac/b/c/g;)V
    .locals 4

    .prologue
    .line 157
    iget-object v0, p0, Lcom/google/android/location/copresence/f/b;->e:Lcom/google/ac/b/c/g;

    const-string v1, "server"

    invoke-static {p1}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    const/4 v2, 0x3

    invoke-static {v2}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v2

    if-eqz v2, :cond_0

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "ConfigurationManager:  Updating "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/location/copresence/ag;->b(Ljava/lang/String;)I

    :cond_0
    :try_start_0
    invoke-static {p1}, Lcom/google/ac/b/c/g;->toByteArray(Lcom/google/protobuf/nano/j;)[B

    move-result-object v2

    invoke-static {v0, v2}, Lcom/google/protobuf/nano/j;->mergeFrom(Lcom/google/protobuf/nano/j;[B)Lcom/google/protobuf/nano/j;
    :try_end_0
    .catch Lcom/google/protobuf/nano/i; {:try_start_0 .. :try_end_0} :catch_0

    const-string v2, "overrides"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-static {}, Lcom/google/android/location/copresence/f/b;->f()Lcom/google/ac/b/c/g;

    move-result-object v2

    invoke-virtual {p1, v2}, Lcom/google/ac/b/c/g;->equals(Ljava/lang/Object;)Z

    :cond_1
    invoke-direct {p0, v1, v0}, Lcom/google/android/location/copresence/f/b;->a(Ljava/lang/String;Lcom/google/ac/b/c/g;)V

    invoke-direct {p0}, Lcom/google/android/location/copresence/f/b;->e()V

    .line 158
    :cond_2
    :goto_0
    return-void

    .line 157
    :catch_0
    move-exception v0

    const/4 v2, 0x6

    invoke-static {v2}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v2

    if-eqz v2, :cond_2

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "ConfigurationManager: update "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " config error: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Lcom/google/protobuf/nano/i;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->e(Ljava/lang/String;)I

    goto :goto_0
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 164
    iget-object v0, p0, Lcom/google/android/location/copresence/f/b;->e:Lcom/google/ac/b/c/g;

    iget-object v0, v0, Lcom/google/ac/b/c/g;->c:Ljava/lang/String;

    return-object v0
.end method

.method public final d()V
    .locals 1

    .prologue
    .line 572
    const/4 v0, 0x3

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 573
    const-string v0, "ConfigurationManager: Updating Gservices Overrides:"

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->b(Ljava/lang/String;)I

    .line 575
    :cond_0
    invoke-direct {p0}, Lcom/google/android/location/copresence/f/b;->g()Lcom/google/ac/b/c/g;

    move-result-object v0

    if-eqz v0, :cond_1

    iput-object v0, p0, Lcom/google/android/location/copresence/f/b;->g:Lcom/google/ac/b/c/g;

    invoke-direct {p0}, Lcom/google/android/location/copresence/f/b;->e()V

    .line 576
    :cond_1
    return-void
.end method
