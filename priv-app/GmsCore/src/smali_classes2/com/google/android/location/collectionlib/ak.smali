.class final Lcom/google/android/location/collectionlib/ak;
.super Landroid/os/Handler;
.source "SourceFile"


# instance fields
.field protected final a:Lcom/google/android/location/o/a/c;

.field private final b:Lcom/google/android/location/collectionlib/ca;

.field private final c:Landroid/os/Handler;

.field private final d:I

.field private e:Z

.field private final f:Ljava/lang/Runnable;


# direct methods
.method constructor <init>(Lcom/google/android/location/collectionlib/ca;Landroid/os/Handler;ILcom/google/android/location/o/a/c;)V
    .locals 1

    .prologue
    .line 54
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    .line 40
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/location/collectionlib/ak;->e:Z

    .line 42
    new-instance v0, Lcom/google/android/location/collectionlib/al;

    invoke-direct {v0, p0}, Lcom/google/android/location/collectionlib/al;-><init>(Lcom/google/android/location/collectionlib/ak;)V

    iput-object v0, p0, Lcom/google/android/location/collectionlib/ak;->f:Ljava/lang/Runnable;

    .line 55
    iput-object p1, p0, Lcom/google/android/location/collectionlib/ak;->b:Lcom/google/android/location/collectionlib/ca;

    .line 56
    iput-object p2, p0, Lcom/google/android/location/collectionlib/ak;->c:Landroid/os/Handler;

    .line 57
    iput p3, p0, Lcom/google/android/location/collectionlib/ak;->d:I

    .line 58
    invoke-static {p4}, Lcom/google/android/location/collectionlib/df;->a(Lcom/google/android/location/o/a/c;)Lcom/google/android/location/o/a/c;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/collectionlib/ak;->a:Lcom/google/android/location/o/a/c;

    .line 59
    return-void
.end method

.method static synthetic a(Lcom/google/android/location/collectionlib/ak;)Z
    .locals 1

    .prologue
    .line 26
    iget-boolean v0, p0, Lcom/google/android/location/collectionlib/ak;->e:Z

    return v0
.end method

.method static synthetic b(Lcom/google/android/location/collectionlib/ak;)Lcom/google/android/location/collectionlib/ca;
    .locals 1

    .prologue
    .line 26
    iget-object v0, p0, Lcom/google/android/location/collectionlib/ak;->b:Lcom/google/android/location/collectionlib/ca;

    return-object v0
.end method

.method static synthetic c(Lcom/google/android/location/collectionlib/ak;)I
    .locals 1

    .prologue
    .line 26
    iget v0, p0, Lcom/google/android/location/collectionlib/ak;->d:I

    return v0
.end method

.method static synthetic d(Lcom/google/android/location/collectionlib/ak;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 26
    iget-object v0, p0, Lcom/google/android/location/collectionlib/ak;->c:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic e(Lcom/google/android/location/collectionlib/ak;)Z
    .locals 1

    .prologue
    .line 26
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/location/collectionlib/ak;->e:Z

    return v0
.end method


# virtual methods
.method final a()V
    .locals 1

    .prologue
    .line 77
    iget-object v0, p0, Lcom/google/android/location/collectionlib/ak;->f:Ljava/lang/Runnable;

    invoke-virtual {p0, v0}, Lcom/google/android/location/collectionlib/ak;->post(Ljava/lang/Runnable;)Z

    .line 78
    return-void
.end method

.method final a(FFFF)V
    .locals 1

    .prologue
    .line 176
    iget-object v0, p0, Lcom/google/android/location/collectionlib/ak;->b:Lcom/google/android/location/collectionlib/ca;

    invoke-interface {v0, p1, p2, p3, p4}, Lcom/google/android/location/collectionlib/ca;->a(FFFF)V

    .line 177
    return-void
.end method

.method final a(IFFFFFFIJJ)V
    .locals 13

    .prologue
    .line 150
    const/16 v0, 0xe

    if-ne p1, v0, :cond_0

    .line 151
    iget-object v0, p0, Lcom/google/android/location/collectionlib/ak;->b:Lcom/google/android/location/collectionlib/ca;

    move v1, p2

    move/from16 v2, p3

    move/from16 v3, p4

    move/from16 v4, p5

    move/from16 v5, p6

    move/from16 v6, p7

    move/from16 v7, p8

    move-wide/from16 v8, p9

    move-wide/from16 v10, p11

    invoke-interface/range {v0 .. v11}, Lcom/google/android/location/collectionlib/ca;->a(FFFFFFIJJ)V

    .line 154
    :cond_0
    return-void
.end method

.method final a(IFFFIJJ)V
    .locals 10

    .prologue
    .line 119
    const/4 v0, 0x3

    if-ne p1, v0, :cond_1

    .line 120
    iget-object v1, p0, Lcom/google/android/location/collectionlib/ak;->b:Lcom/google/android/location/collectionlib/ca;

    move v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    move-wide/from16 v6, p6

    move-wide/from16 v8, p8

    invoke-interface/range {v1 .. v9}, Lcom/google/android/location/collectionlib/ca;->a(FFFIJJ)V

    .line 146
    :cond_0
    :goto_0
    return-void

    .line 122
    :cond_1
    const/4 v0, 0x1

    if-ne p1, v0, :cond_2

    .line 123
    iget-object v1, p0, Lcom/google/android/location/collectionlib/ak;->b:Lcom/google/android/location/collectionlib/ca;

    move v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    move-wide/from16 v6, p6

    move-wide/from16 v8, p8

    invoke-interface/range {v1 .. v9}, Lcom/google/android/location/collectionlib/ca;->b(FFFIJJ)V

    goto :goto_0

    .line 125
    :cond_2
    const/4 v0, 0x2

    if-ne p1, v0, :cond_3

    .line 126
    iget-object v1, p0, Lcom/google/android/location/collectionlib/ak;->b:Lcom/google/android/location/collectionlib/ca;

    move v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    move-wide/from16 v6, p6

    move-wide/from16 v8, p8

    invoke-interface/range {v1 .. v9}, Lcom/google/android/location/collectionlib/ca;->c(FFFIJJ)V

    goto :goto_0

    .line 128
    :cond_3
    const/4 v0, 0x4

    if-ne p1, v0, :cond_4

    .line 129
    iget-object v1, p0, Lcom/google/android/location/collectionlib/ak;->b:Lcom/google/android/location/collectionlib/ca;

    move v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    move-wide/from16 v6, p6

    move-wide/from16 v8, p8

    invoke-interface/range {v1 .. v9}, Lcom/google/android/location/collectionlib/ca;->d(FFFIJJ)V

    goto :goto_0

    .line 131
    :cond_4
    const/4 v0, 0x6

    if-ne p1, v0, :cond_5

    .line 132
    iget-object v1, p0, Lcom/google/android/location/collectionlib/ak;->b:Lcom/google/android/location/collectionlib/ca;

    move v2, p2

    move v3, p5

    move-wide/from16 v4, p6

    move-wide/from16 v6, p8

    invoke-interface/range {v1 .. v7}, Lcom/google/android/location/collectionlib/ca;->a(FIJJ)V

    goto :goto_0

    .line 133
    :cond_5
    const/4 v0, 0x5

    if-ne p1, v0, :cond_6

    .line 134
    iget-object v1, p0, Lcom/google/android/location/collectionlib/ak;->b:Lcom/google/android/location/collectionlib/ca;

    move v2, p2

    move v3, p5

    move-wide/from16 v4, p6

    move-wide/from16 v6, p8

    invoke-interface/range {v1 .. v7}, Lcom/google/android/location/collectionlib/ca;->b(FIJJ)V

    goto :goto_0

    .line 135
    :cond_6
    const/16 v0, 0x8

    if-ne p1, v0, :cond_7

    .line 136
    iget-object v1, p0, Lcom/google/android/location/collectionlib/ak;->b:Lcom/google/android/location/collectionlib/ca;

    move v2, p2

    move v3, p5

    move-wide/from16 v4, p6

    move-wide/from16 v6, p8

    invoke-interface/range {v1 .. v7}, Lcom/google/android/location/collectionlib/ca;->c(FIJJ)V

    goto :goto_0

    .line 139
    :cond_7
    const/16 v0, 0x15

    if-ne p1, v0, :cond_8

    .line 140
    iget-object v1, p0, Lcom/google/android/location/collectionlib/ak;->b:Lcom/google/android/location/collectionlib/ca;

    move v2, p2

    move v3, p5

    move-wide/from16 v4, p6

    move-wide/from16 v6, p8

    invoke-interface/range {v1 .. v7}, Lcom/google/android/location/collectionlib/ca;->d(FIJJ)V

    goto :goto_0

    .line 142
    :cond_8
    const/16 v0, 0x13

    if-ne p1, v0, :cond_0

    .line 143
    iget-object v1, p0, Lcom/google/android/location/collectionlib/ak;->b:Lcom/google/android/location/collectionlib/ca;

    move v2, p2

    move v3, p5

    move-wide/from16 v4, p6

    move-wide/from16 v6, p8

    invoke-interface/range {v1 .. v7}, Lcom/google/android/location/collectionlib/ca;->e(FIJJ)V

    goto/16 :goto_0
.end method

.method final a(ILjava/lang/String;Landroid/telephony/CellLocation;ILjava/util/List;J)V
    .locals 10

    .prologue
    .line 106
    new-instance v1, Lcom/google/android/location/collectionlib/ao;

    move-object v2, p0

    move v3, p1

    move-object v4, p2

    move-object v5, p3

    move v6, p4

    move-object v7, p5

    move-wide/from16 v8, p6

    invoke-direct/range {v1 .. v9}, Lcom/google/android/location/collectionlib/ao;-><init>(Lcom/google/android/location/collectionlib/ak;ILjava/lang/String;Landroid/telephony/CellLocation;ILjava/util/List;J)V

    invoke-virtual {p0, v1}, Lcom/google/android/location/collectionlib/ak;->post(Ljava/lang/Runnable;)Z

    .line 113
    return-void
.end method

.method final a(Landroid/location/GpsMeasurementsEvent;J)V
    .locals 2

    .prologue
    .line 180
    new-instance v0, Lcom/google/android/location/collectionlib/ap;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/google/android/location/collectionlib/ap;-><init>(Lcom/google/android/location/collectionlib/ak;Landroid/location/GpsMeasurementsEvent;J)V

    invoke-virtual {p0, v0}, Lcom/google/android/location/collectionlib/ak;->post(Ljava/lang/Runnable;)Z

    .line 186
    return-void
.end method

.method final a(Landroid/location/GpsNavigationMessageEvent;J)V
    .locals 2

    .prologue
    .line 191
    new-instance v0, Lcom/google/android/location/collectionlib/aq;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/google/android/location/collectionlib/aq;-><init>(Lcom/google/android/location/collectionlib/ak;Landroid/location/GpsNavigationMessageEvent;J)V

    invoke-virtual {p0, v0}, Lcom/google/android/location/collectionlib/ak;->post(Ljava/lang/Runnable;)Z

    .line 197
    return-void
.end method

.method public final a(Landroid/location/GpsStatus;J)V
    .locals 2

    .prologue
    .line 171
    invoke-static {p1}, Lcom/google/android/location/collectionlib/df;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 172
    iget-object v0, p0, Lcom/google/android/location/collectionlib/ak;->b:Lcom/google/android/location/collectionlib/ca;

    invoke-interface {v0, p1, p2, p3}, Lcom/google/android/location/collectionlib/ca;->a(Landroid/location/GpsStatus;J)V

    .line 173
    return-void
.end method

.method final a(Landroid/location/Location;J)V
    .locals 2

    .prologue
    .line 166
    invoke-static {p1}, Lcom/google/android/location/collectionlib/df;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 167
    iget-object v0, p0, Lcom/google/android/location/collectionlib/ak;->b:Lcom/google/android/location/collectionlib/ca;

    invoke-interface {v0, p1, p2, p3}, Lcom/google/android/location/collectionlib/ca;->a(Landroid/location/Location;J)Z

    .line 168
    return-void
.end method

.method final a(Lcom/google/android/location/collectionlib/ce;)V
    .locals 1

    .prologue
    .line 62
    new-instance v0, Lcom/google/android/location/collectionlib/am;

    invoke-direct {v0, p0, p1}, Lcom/google/android/location/collectionlib/am;-><init>(Lcom/google/android/location/collectionlib/ak;Lcom/google/android/location/collectionlib/ce;)V

    invoke-virtual {p0, v0}, Lcom/google/android/location/collectionlib/ak;->post(Ljava/lang/Runnable;)Z

    .line 70
    return-void
.end method

.method final a(Ljava/util/List;J)V
    .locals 2

    .prologue
    .line 88
    invoke-static {p1}, Lcom/google/android/location/collectionlib/df;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 89
    new-instance v0, Lcom/google/android/location/collectionlib/an;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/google/android/location/collectionlib/an;-><init>(Lcom/google/android/location/collectionlib/ak;Ljava/util/List;J)V

    invoke-virtual {p0, v0}, Lcom/google/android/location/collectionlib/ak;->post(Ljava/lang/Runnable;)Z

    .line 95
    return-void
.end method

.method final a([D[D[D[D[DJ)V
    .locals 8

    .prologue
    .line 162
    iget-object v0, p0, Lcom/google/android/location/collectionlib/ak;->b:Lcom/google/android/location/collectionlib/ca;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-wide v6, p6

    invoke-interface/range {v0 .. v7}, Lcom/google/android/location/collectionlib/ca;->a([D[D[D[D[DJ)V

    .line 163
    return-void
.end method
