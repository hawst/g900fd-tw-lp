.class public final Lcom/google/android/location/places/t;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final a:J


# instance fields
.field private final b:Lcom/google/android/location/b/p;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 51
    sget-object v0, Lcom/google/android/location/x;->aa:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    sput-wide v0, Lcom/google/android/location/places/t;->a:J

    return-void
.end method

.method public constructor <init>(Lcom/google/android/location/n/ai;Landroid/os/Handler;)V
    .locals 17

    .prologue
    .line 65
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    .line 66
    sget-object v3, Lcom/google/android/location/x;->W:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v3}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v4

    .line 67
    new-instance v15, Lcom/google/android/location/b/w;

    invoke-direct {v15}, Lcom/google/android/location/b/w;-><init>()V

    const/4 v3, 0x1

    const-string v5, "Version must be non-negaive."

    invoke-static {v3, v5}, Lcom/google/android/location/o/j;->a(ZLjava/lang/Object;)V

    const/4 v3, 0x1

    iput-short v3, v15, Lcom/google/android/location/b/w;->a:S

    if-lez v4, :cond_4

    const/4 v3, 0x1

    :goto_0
    const-string v5, "Memory capacity must be positive."

    invoke-static {v3, v5}, Lcom/google/android/location/o/j;->a(ZLjava/lang/Object;)V

    iput v4, v15, Lcom/google/android/location/b/w;->b:I

    iget-short v3, v15, Lcom/google/android/location/b/w;->a:S

    if-ltz v3, :cond_5

    const/4 v3, 0x1

    :goto_1
    const-string v4, "Version must be non-negaive."

    invoke-static {v3, v4}, Lcom/google/android/location/o/j;->a(ZLjava/lang/Object;)V

    iget v3, v15, Lcom/google/android/location/b/w;->b:I

    if-lez v3, :cond_6

    const/4 v3, 0x1

    :goto_2
    const-string v4, "Memory capacity must be positive."

    invoke-static {v3, v4}, Lcom/google/android/location/o/j;->a(ZLjava/lang/Object;)V

    const/4 v14, 0x1

    iget v3, v15, Lcom/google/android/location/b/w;->c:I

    if-lez v3, :cond_0

    const/4 v14, 0x0

    :cond_0
    if-eqz v14, :cond_1

    iget-object v3, v15, Lcom/google/android/location/b/w;->j:Lcom/google/android/location/b/z;

    if-eqz v3, :cond_2

    :cond_1
    iget-object v3, v15, Lcom/google/android/location/b/w;->h:Lcom/google/android/location/j/i;

    const-string v4, "Executor cannot be null. Did you forget to call setExecutor()?"

    invoke-static {v3, v4}, Lcom/google/android/location/o/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_2
    new-instance v3, Lcom/google/android/location/b/p;

    iget-short v4, v15, Lcom/google/android/location/b/w;->a:S

    iget v5, v15, Lcom/google/android/location/b/w;->b:I

    iget v6, v15, Lcom/google/android/location/b/w;->c:I

    iget v7, v15, Lcom/google/android/location/b/w;->d:I

    iget-object v8, v15, Lcom/google/android/location/b/w;->e:Ljava/io/File;

    iget-object v9, v15, Lcom/google/android/location/b/w;->f:Lcom/google/android/location/e/a;

    iget-object v10, v15, Lcom/google/android/location/b/w;->g:Lcom/google/android/location/b/y;

    iget-object v11, v15, Lcom/google/android/location/b/w;->j:Lcom/google/android/location/b/z;

    iget-object v12, v15, Lcom/google/android/location/b/w;->i:Lcom/google/android/location/b/ac;

    iget-object v13, v15, Lcom/google/android/location/b/w;->h:Lcom/google/android/location/j/i;

    iget-boolean v15, v15, Lcom/google/android/location/b/w;->k:Z

    const/16 v16, 0x0

    invoke-direct/range {v3 .. v16}, Lcom/google/android/location/b/p;-><init>(SIIILjava/io/File;Lcom/google/android/location/e/a;Lcom/google/android/location/b/y;Lcom/google/android/location/b/z;Lcom/google/android/location/b/ac;Lcom/google/android/location/j/i;ZZB)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/location/places/t;->b:Lcom/google/android/location/b/p;

    .line 72
    if-eqz p1, :cond_3

    if-eqz p2, :cond_3

    .line 73
    new-instance v3, Lcom/google/android/location/places/u;

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    move-object/from16 v2, p1

    invoke-direct {v3, v0, v1, v2}, Lcom/google/android/location/places/u;-><init>(Lcom/google/android/location/places/t;Landroid/os/Handler;Lcom/google/android/location/n/ai;)V

    .line 85
    move-object/from16 v0, p2

    invoke-virtual {v0, v3}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 87
    :cond_3
    return-void

    .line 67
    :cond_4
    const/4 v3, 0x0

    goto :goto_0

    :cond_5
    const/4 v3, 0x0

    goto :goto_1

    :cond_6
    const/4 v3, 0x0

    goto :goto_2
.end method

.method static synthetic a(Lcom/google/android/location/places/t;)Lcom/google/android/location/b/p;
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lcom/google/android/location/places/t;->b:Lcom/google/android/location/b/p;

    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;J)Lcom/google/android/gms/location/places/internal/PlaceImpl;
    .locals 2

    .prologue
    .line 94
    iget-object v0, p0, Lcom/google/android/location/places/t;->b:Lcom/google/android/location/b/p;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/location/b/p;->a(Ljava/lang/Object;J)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/location/places/internal/PlaceImpl;

    return-object v0
.end method

.method public final a()V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 103
    iget-object v2, p0, Lcom/google/android/location/places/t;->b:Lcom/google/android/location/b/p;

    iget-object v3, v2, Lcom/google/android/location/b/p;->a:Lcom/google/android/location/b/bd;

    monitor-enter v3

    :try_start_0
    iget-object v0, v2, Lcom/google/android/location/b/p;->a:Lcom/google/android/location/b/bd;

    invoke-virtual {v0}, Lcom/google/android/location/b/bd;->clear()V

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-boolean v0, v2, Lcom/google/android/location/b/p;->y:Z

    if-nez v0, :cond_2

    iget-object v0, v2, Lcom/google/android/location/b/p;->v:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v3

    if-eqz v3, :cond_0

    array-length v4, v3

    move v0, v1

    :goto_0
    if-ge v0, v4, :cond_0

    aget-object v5, v3, v0

    invoke-virtual {v5}, Ljava/io/File;->delete()Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v3

    throw v0

    :cond_0
    iget-object v0, v2, Lcom/google/android/location/b/p;->v:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    iget-object v0, v2, Lcom/google/android/location/b/p;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentHashMap;->clear()V

    goto :goto_1

    :cond_1
    const-wide v4, 0x7fffffffffffffffL

    iput-wide v4, v2, Lcom/google/android/location/b/p;->e:J

    move v0, v1

    :goto_2
    iget v1, v2, Lcom/google/android/location/b/p;->t:I

    if-ge v0, v1, :cond_2

    iget-object v1, v2, Lcom/google/android/location/b/p;->f:Ljava/util/concurrent/atomic/AtomicIntegerArray;

    const/4 v3, 0x1

    invoke-virtual {v1, v0, v3}, Ljava/util/concurrent/atomic/AtomicIntegerArray;->set(II)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 104
    :cond_2
    return-void
.end method

.method public final a(Lcom/google/android/gms/location/places/internal/PlaceImpl;J)V
    .locals 2

    .prologue
    .line 90
    iget-object v0, p0, Lcom/google/android/location/places/t;->b:Lcom/google/android/location/b/p;

    invoke-virtual {p1}, Lcom/google/android/gms/location/places/internal/PlaceImpl;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, p1, p2, p3}, Lcom/google/android/location/b/p;->a(Ljava/lang/Object;Ljava/lang/Object;J)V

    .line 91
    return-void
.end method
