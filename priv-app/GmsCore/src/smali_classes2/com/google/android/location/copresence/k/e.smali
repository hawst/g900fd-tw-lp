.class public final Lcom/google/android/location/copresence/k/e;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/location/copresence/k/c;


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Ljava/lang/String;

.field private final c:Lcom/google/android/location/copresence/k/c;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/location/copresence/k/c;)V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    iput-object p1, p0, Lcom/google/android/location/copresence/k/e;->a:Landroid/content/Context;

    .line 29
    iput-object p2, p0, Lcom/google/android/location/copresence/k/e;->b:Ljava/lang/String;

    .line 30
    iput-object p3, p0, Lcom/google/android/location/copresence/k/e;->c:Lcom/google/android/location/copresence/k/c;

    .line 31
    return-void
.end method

.method private a(Lcom/google/android/location/copresence/k/f;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 68
    iget-object v0, p0, Lcom/google/android/location/copresence/k/e;->a:Landroid/content/Context;

    if-eqz v0, :cond_1

    .line 69
    const/4 v0, 0x3

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 70
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "StateTransitionLogger: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/location/copresence/k/e;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ": "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p1, Lcom/google/android/location/copresence/k/f;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->b(Ljava/lang/String;)I

    .line 72
    :cond_0
    new-instance v0, Lcom/google/android/location/copresence/debug/CopresDatabaseHelper$StateTransitionRecord;

    invoke-direct {v0}, Lcom/google/android/location/copresence/debug/CopresDatabaseHelper$StateTransitionRecord;-><init>()V

    .line 73
    iget-object v1, p0, Lcom/google/android/location/copresence/k/e;->b:Ljava/lang/String;

    iput-object v1, v0, Lcom/google/android/location/copresence/debug/CopresDatabaseHelper$StateTransitionRecord;->stateMachine:Ljava/lang/String;

    .line 74
    iget-object v1, p1, Lcom/google/android/location/copresence/k/f;->e:Ljava/lang/String;

    iput-object v1, v0, Lcom/google/android/location/copresence/debug/CopresDatabaseHelper$StateTransitionRecord;->transitionType:Ljava/lang/String;

    .line 75
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    iput-object v1, v0, Lcom/google/android/location/copresence/debug/CopresDatabaseHelper$StateTransitionRecord;->transitionTimestamp:Ljava/lang/Long;

    .line 76
    iput-object p2, v0, Lcom/google/android/location/copresence/debug/CopresDatabaseHelper$StateTransitionRecord;->transitionPhase:Ljava/lang/String;

    .line 77
    iget-object v1, p0, Lcom/google/android/location/copresence/k/e;->a:Landroid/content/Context;

    invoke-static {v1, v0}, Lcom/google/android/location/copresence/debug/CopresDatabaseHelper;->a(Landroid/content/Context;Lcom/google/android/location/copresence/debug/a;)V

    .line 79
    :cond_1
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 59
    const/4 v0, 0x3

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 60
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "StateTransitionLogger: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/location/copresence/k/e;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " completed all state transitions in queue."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->b(Ljava/lang/String;)I

    .line 62
    :cond_0
    iget-object v0, p0, Lcom/google/android/location/copresence/k/e;->c:Lcom/google/android/location/copresence/k/c;

    if-eqz v0, :cond_1

    .line 63
    iget-object v0, p0, Lcom/google/android/location/copresence/k/e;->c:Lcom/google/android/location/copresence/k/c;

    invoke-interface {v0}, Lcom/google/android/location/copresence/k/c;->a()V

    .line 65
    :cond_1
    return-void
.end method

.method public final a(Lcom/google/android/location/copresence/k/f;)V
    .locals 1

    .prologue
    .line 51
    const-string v0, "Failed transition"

    invoke-direct {p0, p1, v0}, Lcom/google/android/location/copresence/k/e;->a(Lcom/google/android/location/copresence/k/f;Ljava/lang/String;)V

    .line 52
    iget-object v0, p0, Lcom/google/android/location/copresence/k/e;->c:Lcom/google/android/location/copresence/k/c;

    if-eqz v0, :cond_0

    .line 53
    iget-object v0, p0, Lcom/google/android/location/copresence/k/e;->c:Lcom/google/android/location/copresence/k/c;

    invoke-interface {v0, p1}, Lcom/google/android/location/copresence/k/c;->a(Lcom/google/android/location/copresence/k/f;)V

    .line 55
    :cond_0
    return-void
.end method

.method public final b(Lcom/google/android/location/copresence/k/f;)V
    .locals 1

    .prologue
    .line 43
    const-string v0, "In state"

    invoke-direct {p0, p1, v0}, Lcom/google/android/location/copresence/k/e;->a(Lcom/google/android/location/copresence/k/f;Ljava/lang/String;)V

    .line 44
    iget-object v0, p0, Lcom/google/android/location/copresence/k/e;->c:Lcom/google/android/location/copresence/k/c;

    if-eqz v0, :cond_0

    .line 45
    iget-object v0, p0, Lcom/google/android/location/copresence/k/e;->c:Lcom/google/android/location/copresence/k/c;

    invoke-interface {v0, p1}, Lcom/google/android/location/copresence/k/c;->b(Lcom/google/android/location/copresence/k/f;)V

    .line 47
    :cond_0
    return-void
.end method

.method public final c(Lcom/google/android/location/copresence/k/f;)V
    .locals 1

    .prologue
    .line 35
    const-string v0, "Attempting transition"

    invoke-direct {p0, p1, v0}, Lcom/google/android/location/copresence/k/e;->a(Lcom/google/android/location/copresence/k/f;Ljava/lang/String;)V

    .line 36
    iget-object v0, p0, Lcom/google/android/location/copresence/k/e;->c:Lcom/google/android/location/copresence/k/c;

    if-eqz v0, :cond_0

    .line 37
    iget-object v0, p0, Lcom/google/android/location/copresence/k/e;->c:Lcom/google/android/location/copresence/k/c;

    invoke-interface {v0, p1}, Lcom/google/android/location/copresence/k/c;->c(Lcom/google/android/location/copresence/k/f;)V

    .line 39
    :cond_0
    return-void
.end method
