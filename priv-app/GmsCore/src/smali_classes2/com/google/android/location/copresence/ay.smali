.class final Lcom/google/android/location/copresence/ay;
.super Landroid/content/BroadcastReceiver;
.source "SourceFile"


# static fields
.field private static final a:Lcom/google/android/location/copresence/ba;

.field private static final b:Lcom/google/android/location/copresence/bc;

.field private static final c:Lcom/google/android/location/copresence/bb;

.field private static g:Lcom/google/android/location/copresence/ay;


# instance fields
.field private final d:Landroid/content/Context;

.field private final e:Landroid/content/pm/PackageManager;

.field private final f:Lcom/google/android/location/copresence/q/w;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 27
    new-instance v0, Lcom/google/android/location/copresence/ba;

    invoke-direct {v0, v1}, Lcom/google/android/location/copresence/ba;-><init>(B)V

    sput-object v0, Lcom/google/android/location/copresence/ay;->a:Lcom/google/android/location/copresence/ba;

    .line 29
    new-instance v0, Lcom/google/android/location/copresence/bc;

    invoke-direct {v0, v1}, Lcom/google/android/location/copresence/bc;-><init>(B)V

    sput-object v0, Lcom/google/android/location/copresence/ay;->b:Lcom/google/android/location/copresence/bc;

    .line 31
    new-instance v0, Lcom/google/android/location/copresence/bb;

    invoke-direct {v0, v1}, Lcom/google/android/location/copresence/bb;-><init>(B)V

    sput-object v0, Lcom/google/android/location/copresence/ay;->c:Lcom/google/android/location/copresence/bb;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 59
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    .line 60
    iput-object p1, p0, Lcom/google/android/location/copresence/ay;->d:Landroid/content/Context;

    .line 61
    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/copresence/ay;->e:Landroid/content/pm/PackageManager;

    .line 62
    new-instance v0, Lcom/google/android/location/copresence/q/w;

    invoke-static {p1}, Lcom/google/android/location/copresence/ap;->a(Landroid/content/Context;)Lcom/google/android/location/copresence/ap;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/location/copresence/q/w;-><init>(Lcom/google/android/location/copresence/ap;)V

    iput-object v0, p0, Lcom/google/android/location/copresence/ay;->f:Lcom/google/android/location/copresence/q/w;

    .line 63
    return-void
.end method

.method public static declared-synchronized a(Landroid/content/Context;)Lcom/google/android/location/copresence/ay;
    .locals 2

    .prologue
    .line 53
    const-class v1, Lcom/google/android/location/copresence/ay;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/google/android/location/copresence/ay;->g:Lcom/google/android/location/copresence/ay;

    if-nez v0, :cond_0

    .line 54
    new-instance v0, Lcom/google/android/location/copresence/ay;

    invoke-direct {v0, p0}, Lcom/google/android/location/copresence/ay;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/google/android/location/copresence/ay;->g:Lcom/google/android/location/copresence/ay;

    .line 56
    :cond_0
    sget-object v0, Lcom/google/android/location/copresence/ay;->g:Lcom/google/android/location/copresence/ay;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 53
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private a(Landroid/content/Intent;)Ljava/util/Collection;
    .locals 4

    .prologue
    const/4 v0, 0x0

    const/4 v3, 0x2

    .line 146
    const-string v1, "android.intent.extra.UID"

    const/4 v2, -0x1

    invoke-virtual {p1, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    .line 147
    if-gez v1, :cond_1

    .line 148
    invoke-static {v3}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 149
    const-string v1, "PackageChangesObserver: Invalid UID for action."

    invoke-static {v1}, Lcom/google/android/location/copresence/ag;->a(Ljava/lang/String;)I

    .line 162
    :cond_0
    :goto_0
    return-object v0

    .line 154
    :cond_1
    iget-object v2, p0, Lcom/google/android/location/copresence/ay;->e:Landroid/content/pm/PackageManager;

    invoke-virtual {v2, v1}, Landroid/content/pm/PackageManager;->getPackagesForUid(I)[Ljava/lang/String;

    move-result-object v1

    .line 155
    if-nez v1, :cond_2

    .line 156
    invoke-static {v3}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 157
    const-string v1, "PackageChangesObserver: No packages found for UID."

    invoke-static {v1}, Lcom/google/android/location/copresence/ag;->a(Ljava/lang/String;)I

    goto :goto_0

    .line 162
    :cond_2
    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    goto :goto_0
.end method

.method private a(Ljava/lang/String;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 137
    :try_start_0
    iget-object v1, p0, Lcom/google/android/location/copresence/ay;->e:Landroid/content/pm/PackageManager;

    const/4 v2, 0x0

    invoke-virtual {v1, p1, v2}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object v1

    .line 139
    iget-boolean v0, v1, Landroid/content/pm/ApplicationInfo;->enabled:Z
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 141
    :goto_0
    return v0

    :catch_0
    move-exception v1

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    .line 66
    iget-object v0, p0, Lcom/google/android/location/copresence/ay;->d:Landroid/content/Context;

    new-instance v1, Landroid/content/IntentFilter;

    invoke-direct {v1}, Landroid/content/IntentFilter;-><init>()V

    const-string v2, "android.intent.action.PACKAGE_FULLY_REMOVED"

    invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v2, "android.intent.action.PACKAGE_DATA_CLEARED"

    invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v2, "android.intent.action.PACKAGE_CHANGED"

    invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v2, "package"

    invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->addDataScheme(Ljava/lang/String;)V

    invoke-virtual {v0, p0, v1}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 67
    return-void
.end method

.method public final a(Lcom/google/android/location/copresence/az;)V
    .locals 1

    .prologue
    .line 128
    iget-object v0, p0, Lcom/google/android/location/copresence/ay;->f:Lcom/google/android/location/copresence/q/w;

    invoke-virtual {v0, p1}, Lcom/google/android/location/copresence/q/w;->a(Ljava/lang/Object;)V

    .line 129
    return-void
.end method

.method public final onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 7

    .prologue
    const/4 v4, 0x2

    const/4 v6, 0x0

    .line 75
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 77
    const-string v1, "android.intent.action.PACKAGE_DATA_CLEARED"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 78
    invoke-static {v4}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 79
    const-string v1, "PackageChangesObserver: Package Data cleared."

    invoke-static {v1}, Lcom/google/android/location/copresence/ag;->a(Ljava/lang/String;)I

    .line 81
    :cond_0
    invoke-direct {p0, p2}, Lcom/google/android/location/copresence/ay;->a(Landroid/content/Intent;)Ljava/util/Collection;

    move-result-object v1

    .line 82
    if-eqz v1, :cond_1

    .line 83
    iget-object v2, p0, Lcom/google/android/location/copresence/ay;->f:Lcom/google/android/location/copresence/q/w;

    sget-object v3, Lcom/google/android/location/copresence/ay;->a:Lcom/google/android/location/copresence/ba;

    invoke-virtual {v2, v3, v1}, Lcom/google/android/location/copresence/q/w;->a(Lcom/google/android/location/copresence/q/ac;Ljava/lang/Object;)V

    .line 88
    :cond_1
    const-string v1, "android.intent.action.PACKAGE_REMOVED"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 89
    const-string v1, "android.intent.extra.REPLACING"

    invoke-virtual {p2, v1, v6}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    .line 90
    invoke-direct {p0, p2}, Lcom/google/android/location/copresence/ay;->a(Landroid/content/Intent;)Ljava/util/Collection;

    move-result-object v2

    .line 92
    invoke-static {v4}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 93
    const-string v3, "Package removed. DataRemoved=%b, Replacing=%b"

    new-array v4, v4, [Ljava/lang/Object;

    const-string v5, "android.intent.extra.DATA_REMOVED"

    invoke-virtual {p2, v5, v6}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v5

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    aput-object v5, v4, v6

    const/4 v5, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 97
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "PackageChangesObserver: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/location/copresence/ag;->a(Ljava/lang/String;)I

    .line 102
    :cond_2
    if-nez v1, :cond_3

    if-eqz v2, :cond_3

    .line 103
    iget-object v1, p0, Lcom/google/android/location/copresence/ay;->f:Lcom/google/android/location/copresence/q/w;

    sget-object v3, Lcom/google/android/location/copresence/ay;->b:Lcom/google/android/location/copresence/bc;

    invoke-virtual {v1, v3, v2}, Lcom/google/android/location/copresence/q/w;->a(Lcom/google/android/location/copresence/q/ac;Ljava/lang/Object;)V

    .line 108
    :cond_3
    const-string v1, "android.intent.action.PACKAGE_CHANGED"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 109
    invoke-direct {p0, p2}, Lcom/google/android/location/copresence/ay;->a(Landroid/content/Intent;)Ljava/util/Collection;

    move-result-object v0

    .line 111
    if-eqz v0, :cond_6

    .line 112
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 113
    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_4
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 114
    invoke-direct {p0, v0}, Lcom/google/android/location/copresence/ay;->a(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_4

    .line 115
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 119
    :cond_5
    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_6

    .line 120
    iget-object v0, p0, Lcom/google/android/location/copresence/ay;->f:Lcom/google/android/location/copresence/q/w;

    sget-object v2, Lcom/google/android/location/copresence/ay;->c:Lcom/google/android/location/copresence/bb;

    invoke-virtual {v0, v2, v1}, Lcom/google/android/location/copresence/q/w;->a(Lcom/google/android/location/copresence/q/ac;Ljava/lang/Object;)V

    .line 125
    :cond_6
    return-void
.end method
