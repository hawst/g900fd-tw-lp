.class public final Lcom/google/android/location/os/real/aw;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/location/o/e;
.implements Lcom/google/android/location/os/bi;


# instance fields
.field public final a:Landroid/content/Context;

.field private final b:Lcom/google/android/location/os/j;

.field private final c:Lcom/google/android/location/os/real/x;

.field private final d:Lcom/google/android/location/os/real/ax;

.field private final e:Landroid/support/v4/a/m;

.field private final f:Lcom/google/android/location/e/ah;

.field private final g:Lcom/google/android/location/j/b;

.field private final h:Lcom/google/android/location/j/e;

.field private final i:Lcom/google/android/location/os/bn;

.field private final j:Lcom/google/android/location/j/f;

.field private final k:Lcom/google/android/location/os/real/az;

.field private final m:Landroid/hardware/SensorManager;

.field private final n:Landroid/location/LocationManager;

.field private final o:Lcom/google/android/location/d/b;

.field private final p:J

.field private final q:Lcom/google/android/location/j/d;

.field private final r:Lcom/google/android/location/a/a;

.field private final s:Z

.field private final t:Lcom/google/android/location/os/bm;

.field private final u:Lcom/google/android/location/os/real/bi;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/location/os/j;Lcom/google/android/location/os/real/ax;)V
    .locals 7

    .prologue
    const/4 v6, 0x1

    .line 142
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 109
    new-instance v0, Lcom/google/android/location/os/real/ap;

    invoke-direct {v0}, Lcom/google/android/location/os/real/ap;-><init>()V

    iput-object v0, p0, Lcom/google/android/location/os/real/aw;->g:Lcom/google/android/location/j/b;

    .line 143
    iput-object p1, p0, Lcom/google/android/location/os/real/aw;->a:Landroid/content/Context;

    .line 144
    iput-object p2, p0, Lcom/google/android/location/os/real/aw;->b:Lcom/google/android/location/os/j;

    .line 145
    iput-object p3, p0, Lcom/google/android/location/os/real/aw;->d:Lcom/google/android/location/os/real/ax;

    .line 146
    new-instance v0, Lcom/google/android/location/d/b;

    invoke-direct {v0, p1, v6}, Lcom/google/android/location/d/b;-><init>(Landroid/content/Context;Z)V

    iput-object v0, p0, Lcom/google/android/location/os/real/aw;->o:Lcom/google/android/location/d/b;

    .line 147
    invoke-static {p1}, Landroid/support/v4/a/m;->a(Landroid/content/Context;)Landroid/support/v4/a/m;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/os/real/aw;->e:Landroid/support/v4/a/m;

    .line 148
    new-instance v0, Lcom/google/android/location/os/real/ar;

    invoke-direct {v0, p1, p2}, Lcom/google/android/location/os/real/ar;-><init>(Landroid/content/Context;Lcom/google/android/location/os/j;)V

    iput-object v0, p0, Lcom/google/android/location/os/real/aw;->h:Lcom/google/android/location/j/e;

    .line 149
    new-instance v0, Lcom/google/android/location/os/real/bi;

    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    invoke-direct {v0, v1}, Lcom/google/android/location/os/real/bi;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/location/os/real/aw;->u:Lcom/google/android/location/os/real/bi;

    .line 151
    new-instance v0, Lcom/google/android/location/e/ah;

    iget-object v1, p0, Lcom/google/android/location/os/real/aw;->g:Lcom/google/android/location/j/b;

    iget-object v2, p0, Lcom/google/android/location/os/real/aw;->h:Lcom/google/android/location/j/e;

    invoke-direct {v0, v1, v2, p0}, Lcom/google/android/location/e/ah;-><init>(Lcom/google/android/location/j/b;Lcom/google/android/location/j/e;Lcom/google/android/location/o/e;)V

    iput-object v0, p0, Lcom/google/android/location/os/real/aw;->f:Lcom/google/android/location/e/ah;

    .line 152
    iget-object v0, p0, Lcom/google/android/location/os/real/aw;->f:Lcom/google/android/location/e/ah;

    invoke-virtual {v0}, Lcom/google/android/location/e/ah;->a()V

    .line 153
    new-instance v0, Lcom/google/android/location/os/real/x;

    iget-object v3, p0, Lcom/google/android/location/os/real/aw;->f:Lcom/google/android/location/e/ah;

    iget-object v5, p0, Lcom/google/android/location/os/real/aw;->u:Lcom/google/android/location/os/real/bi;

    move-object v1, p1

    move-object v2, p0

    move-object v4, p2

    invoke-direct/range {v0 .. v5}, Lcom/google/android/location/os/real/x;-><init>(Landroid/content/Context;Lcom/google/android/location/os/real/aw;Lcom/google/android/location/e/ah;Lcom/google/android/location/os/j;Lcom/google/android/location/os/real/bi;)V

    iput-object v0, p0, Lcom/google/android/location/os/real/aw;->c:Lcom/google/android/location/os/real/x;

    .line 154
    new-instance v0, Lcom/google/android/location/os/real/az;

    iget-object v1, p0, Lcom/google/android/location/os/real/aw;->c:Lcom/google/android/location/os/real/x;

    invoke-direct {v0, p1, v1, p2}, Lcom/google/android/location/os/real/az;-><init>(Landroid/content/Context;Lcom/google/android/location/os/real/x;Lcom/google/android/location/os/j;)V

    iput-object v0, p0, Lcom/google/android/location/os/real/aw;->k:Lcom/google/android/location/os/real/az;

    .line 156
    const-string v0, "wifi"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/net/wifi/WifiManager;

    .line 157
    new-instance v0, Lcom/google/android/location/os/real/bd;

    new-instance v3, Lcom/google/android/location/os/real/ay;

    invoke-direct {v3, p0, v6}, Lcom/google/android/location/os/real/ay;-><init>(Lcom/google/android/location/os/real/aw;Z)V

    new-instance v4, Lcom/google/android/location/os/real/ay;

    const/4 v1, 0x0

    invoke-direct {v4, p0, v1}, Lcom/google/android/location/os/real/ay;-><init>(Lcom/google/android/location/os/real/aw;Z)V

    move-object v1, p1

    move-object v2, p2

    invoke-direct/range {v0 .. v5}, Lcom/google/android/location/os/real/bd;-><init>(Landroid/content/Context;Lcom/google/android/location/os/j;Lcom/google/android/location/os/real/be;Lcom/google/android/location/os/real/be;Landroid/net/wifi/WifiManager;)V

    iput-object v0, p0, Lcom/google/android/location/os/real/aw;->i:Lcom/google/android/location/os/bn;

    .line 160
    new-instance v0, Lcom/google/android/location/os/real/at;

    iget-object v2, p0, Lcom/google/android/location/os/real/aw;->g:Lcom/google/android/location/j/b;

    iget-object v3, p0, Lcom/google/android/location/os/real/aw;->f:Lcom/google/android/location/e/ah;

    iget-object v4, p0, Lcom/google/android/location/os/real/aw;->c:Lcom/google/android/location/os/real/x;

    move-object v1, p1

    move-object v5, p2

    invoke-direct/range {v0 .. v5}, Lcom/google/android/location/os/real/at;-><init>(Landroid/content/Context;Lcom/google/android/location/j/b;Lcom/google/android/location/e/ah;Lcom/google/android/location/os/real/x;Lcom/google/android/location/os/j;)V

    iput-object v0, p0, Lcom/google/android/location/os/real/aw;->j:Lcom/google/android/location/j/f;

    .line 162
    const-string v0, "sensor"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/SensorManager;

    iput-object v0, p0, Lcom/google/android/location/os/real/aw;->m:Landroid/hardware/SensorManager;

    .line 163
    const-string v0, "location"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/location/LocationManager;

    iput-object v0, p0, Lcom/google/android/location/os/real/aw;->n:Landroid/location/LocationManager;

    .line 165
    sget-object v0, Lcom/google/android/location/d/a;->t:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/location/os/real/aw;->p:J

    .line 171
    new-instance v0, Lcom/google/android/location/os/real/aq;

    iget-wide v2, p0, Lcom/google/android/location/os/real/aw;->p:J

    new-instance v1, Ljava/io/File;

    invoke-virtual {p1}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v4

    const-string v5, "nlp_ck"

    invoke-direct {v1, v4, v5}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-direct {v0, p1, v2, v3, v1}, Lcom/google/android/location/os/real/aq;-><init>(Landroid/content/Context;JLjava/io/File;)V

    iput-object v0, p0, Lcom/google/android/location/os/real/aw;->q:Lcom/google/android/location/j/d;

    .line 174
    invoke-static {p1}, Lcom/google/android/location/os/real/aw;->a(Landroid/content/Context;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/location/os/real/aw;->s:Z

    .line 175
    invoke-static {}, Lcom/google/android/location/os/real/bf;->a()Lcom/google/android/location/os/real/bf;

    move-result-object v0

    invoke-virtual {v0, p1, p0}, Lcom/google/android/location/os/real/bf;->a(Landroid/content/Context;Lcom/google/android/location/os/bi;)Lcom/google/android/location/os/bm;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/os/real/aw;->t:Lcom/google/android/location/os/bm;

    .line 176
    new-instance v0, Lcom/google/android/location/a/a/b;

    invoke-direct {v0}, Lcom/google/android/location/a/a/b;-><init>()V

    iput-object v0, p0, Lcom/google/android/location/os/real/aw;->r:Lcom/google/android/location/a/a;

    .line 177
    return-void
.end method

.method static synthetic a(Lcom/google/android/location/os/real/aw;)Lcom/google/android/location/os/real/x;
    .locals 1

    .prologue
    .line 89
    iget-object v0, p0, Lcom/google/android/location/os/real/aw;->c:Lcom/google/android/location/os/real/x;

    return-object v0
.end method

.method public static a(Landroid/content/Context;)Z
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 183
    invoke-static {}, Lcom/google/android/gms/common/util/ay;->e()I

    move-result v0

    const/16 v3, 0xa

    if-ne v0, v3, :cond_0

    move v0, v1

    .line 184
    :goto_0
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v3

    .line 185
    iget v3, v3, Landroid/content/res/Configuration;->uiMode:I

    and-int/lit8 v3, v3, 0xf

    const/4 v4, 0x6

    if-ne v3, v4, :cond_1

    move v3, v1

    .line 186
    :goto_1
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    :goto_2
    return v1

    :cond_0
    move v0, v2

    .line 183
    goto :goto_0

    :cond_1
    move v3, v2

    .line 185
    goto :goto_1

    :cond_2
    move v1, v2

    .line 186
    goto :goto_2
.end method

.method public static b(Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 196
    :try_start_0
    new-instance v0, Ljava/io/File;

    invoke-virtual {p0}, Landroid/content/Context;->getCacheDir()Ljava/io/File;

    move-result-object v1

    const-string v2, "nlp_state"

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    .line 197
    new-instance v0, Ljava/io/File;

    invoke-virtual {p0}, Landroid/content/Context;->getCacheDir()Ljava/io/File;

    move-result-object v1

    const-string v2, "nlp_devices"

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    .line 198
    invoke-static {p0}, Lcom/google/android/location/os/real/aw;->e(Landroid/content/Context;)Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->delete()Z
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0

    .line 205
    :cond_0
    :goto_0
    :try_start_1
    invoke-static {p0}, Lcom/google/android/location/os/real/aw;->d(Landroid/content/Context;)Ljava/io/File;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/location/o/j;->a(Ljava/io/File;)Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 210
    :cond_1
    :goto_1
    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/location/os/real/ah;->a(Landroid/content/Context;)V

    .line 211
    return-void

    .line 199
    :catch_0
    move-exception v0

    .line 200
    sget-boolean v1, Lcom/google/android/location/i/a;->e:Z

    if-eqz v1, :cond_0

    const-string v1, "NetworkLocationRealOs"

    const-string v2, "Unable to delete nlp state file"

    invoke-static {v1, v2, v0}, Lcom/google/android/location/o/a/a;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 206
    :catch_1
    move-exception v0

    .line 207
    sget-boolean v1, Lcom/google/android/location/i/a;->e:Z

    if-eqz v1, :cond_1

    const-string v1, "NetworkLocationRealOs"

    const-string v2, "Unable to delete scache dir"

    invoke-static {v1, v2, v0}, Lcom/google/android/location/o/a/a;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1
.end method

.method public static c(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 222
    const-string v0, "cache.cell"

    invoke-virtual {p0, v0}, Landroid/content/Context;->deleteFile(Ljava/lang/String;)Z

    .line 223
    const-string v0, "cache.wifi"

    invoke-virtual {p0, v0}, Landroid/content/Context;->deleteFile(Ljava/lang/String;)Z

    .line 224
    const-string v0, "gls.platform.key"

    invoke-virtual {p0, v0}, Landroid/content/Context;->deleteFile(Ljava/lang/String;)Z

    .line 227
    const-string v0, "nlp_GlsPlatformKey"

    invoke-virtual {p0, v0}, Landroid/content/Context;->deleteFile(Ljava/lang/String;)Z

    .line 228
    return-void
.end method

.method private static d(Landroid/content/Context;)Ljava/io/File;
    .locals 3

    .prologue
    .line 603
    new-instance v0, Ljava/io/File;

    invoke-virtual {p0}, Landroid/content/Context;->getCacheDir()Ljava/io/File;

    move-result-object v1

    const-string v2, "nlp_s"

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    return-object v0
.end method

.method private static e(Landroid/content/Context;)Ljava/io/File;
    .locals 3

    .prologue
    .line 612
    new-instance v0, Ljava/io/File;

    invoke-virtual {p0}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v1

    const-string v2, "nlp_ioh"

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    return-object v0
.end method


# virtual methods
.method public final A()Lcom/google/android/location/os/bm;
    .locals 1

    .prologue
    .line 792
    iget-object v0, p0, Lcom/google/android/location/os/real/aw;->t:Lcom/google/android/location/os/bm;

    return-object v0
.end method

.method public final B()Lcom/google/android/location/j/f;
    .locals 1

    .prologue
    .line 368
    iget-object v0, p0, Lcom/google/android/location/os/real/aw;->j:Lcom/google/android/location/j/f;

    return-object v0
.end method

.method public final C()V
    .locals 2

    .prologue
    .line 275
    iget-object v0, p0, Lcom/google/android/location/os/real/aw;->c:Lcom/google/android/location/os/real/x;

    iget-object v1, v0, Lcom/google/android/location/os/real/x;->z:Lcom/google/android/location/os/real/z;

    if-eqz v1, :cond_0

    :try_start_0
    iget-object v1, v0, Lcom/google/android/location/os/real/x;->n:Landroid/content/Context;

    iget-object v0, v0, Lcom/google/android/location/os/real/x;->z:Lcom/google/android/location/os/real/z;

    invoke-virtual {v1, v0}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 276
    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public final D()V
    .locals 4

    .prologue
    .line 280
    iget-object v0, p0, Lcom/google/android/location/os/real/aw;->c:Lcom/google/android/location/os/real/x;

    iget-object v1, v0, Lcom/google/android/location/os/real/x;->t:Ljava/lang/Thread;

    if-eqz v1, :cond_0

    :goto_0
    :try_start_0
    iget-object v1, v0, Lcom/google/android/location/os/real/x;->t:Ljava/lang/Thread;

    invoke-virtual {v1}, Ljava/lang/Thread;->join()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_1

    :cond_0
    :goto_1
    iget-object v1, v0, Lcom/google/android/location/os/real/x;->u:Lcom/google/android/location/o/h;

    invoke-virtual {v1}, Lcom/google/android/location/o/h;->c()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, v0, Lcom/google/android/location/os/real/x;->u:Lcom/google/android/location/o/h;

    invoke-virtual {v1}, Lcom/google/android/location/o/h;->b()V

    goto :goto_1

    .line 284
    :cond_1
    iget-object v0, p0, Lcom/google/android/location/os/real/aw;->h:Lcom/google/android/location/j/e;

    invoke-interface {v0}, Lcom/google/android/location/j/e;->d()Lcom/google/android/location/j/i;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/location/j/i;->shutdown()V

    .line 286
    :try_start_1
    iget-object v0, p0, Lcom/google/android/location/os/real/aw;->h:Lcom/google/android/location/j/e;

    invoke-interface {v0}, Lcom/google/android/location/j/e;->d()Lcom/google/android/location/j/i;

    move-result-object v0

    const-wide/16 v2, 0xa

    sget-object v1, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v2, v3, v1}, Lcom/google/android/location/j/i;->awaitTermination(JLjava/util/concurrent/TimeUnit;)Z
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0

    .line 290
    :goto_2
    iget-object v0, p0, Lcom/google/android/location/os/real/aw;->k:Lcom/google/android/location/os/real/az;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/location/os/real/az;->a(Z)V

    .line 291
    return-void

    :catch_0
    move-exception v0

    goto :goto_2

    .line 280
    :catch_1
    move-exception v1

    goto :goto_0
.end method

.method public final E()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 298
    iget-object v0, p0, Lcom/google/android/location/os/real/aw;->c:Lcom/google/android/location/os/real/x;

    new-instance v1, Lcom/google/android/location/os/real/ae;

    invoke-direct {v1}, Lcom/google/android/location/os/real/ae;-><init>()V

    new-instance v2, Ljava/lang/Thread;

    const/4 v3, 0x0

    const-string v4, "NetworkLocationCallbackRunner"

    invoke-direct {v2, v3, v1, v4}, Ljava/lang/Thread;-><init>(Ljava/lang/ThreadGroup;Ljava/lang/Runnable;Ljava/lang/String;)V

    iput-object v2, v0, Lcom/google/android/location/os/real/x;->t:Ljava/lang/Thread;

    iget-object v2, v0, Lcom/google/android/location/os/real/x;->t:Ljava/lang/Thread;

    invoke-virtual {v2}, Ljava/lang/Thread;->start()V

    new-instance v2, Lcom/google/android/location/os/real/ab;

    invoke-static {v1}, Lcom/google/android/location/os/real/ae;->a(Lcom/google/android/location/os/real/ae;)Landroid/os/Looper;

    move-result-object v1

    const/4 v3, 0x0

    invoke-direct {v2, v0, v1, v3}, Lcom/google/android/location/os/real/ab;-><init>(Lcom/google/android/location/os/real/x;Landroid/os/Looper;B)V

    iput-object v2, v0, Lcom/google/android/location/os/real/x;->y:Landroid/os/Handler;

    new-instance v1, Ljava/util/concurrent/CountDownLatch;

    invoke-direct {v1, v5}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V

    const/16 v2, 0x1c

    invoke-virtual {v0, v2, v1, v5}, Lcom/google/android/location/os/real/x;->a(ILjava/lang/Object;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {v1}, Lcom/google/android/location/os/real/x;->a(Ljava/util/concurrent/CountDownLatch;)V

    .line 299
    :cond_0
    return-void
.end method

.method public final a(Ljava/lang/String;Lcom/google/android/location/collectionlib/ab;Ljava/lang/String;Lcom/google/android/location/o/n;)Lcom/google/android/location/collectionlib/bd;
    .locals 11

    .prologue
    const/4 v10, 0x0

    .line 641
    iget-object v0, p0, Lcom/google/android/location/os/real/aw;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/location/os/real/ah;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    .line 642
    if-nez v3, :cond_1

    .line 643
    sget-boolean v0, Lcom/google/android/location/i/a;->b:Z

    if-eqz v0, :cond_0

    const-string v0, "NetworkLocationRealOs"

    const-string v1, "client ID not assigned yet."

    invoke-static {v0, v1}, Lcom/google/android/location/o/a/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    move-object v0, v10

    .line 654
    :goto_0
    return-object v0

    .line 647
    :cond_1
    :try_start_0
    new-instance v0, Lcom/google/android/location/collectionlib/i;

    iget-object v1, p0, Lcom/google/android/location/os/real/aw;->a:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/location/os/real/aw;->q:Lcom/google/android/location/j/d;

    invoke-interface {v2}, Lcom/google/android/location/j/d;->b()Ljavax/crypto/SecretKey;

    move-result-object v2

    invoke-interface {v2}, Ljavax/crypto/SecretKey;->getEncoded()[B

    move-result-object v4

    iget-object v2, p0, Lcom/google/android/location/os/real/aw;->q:Lcom/google/android/location/j/d;

    invoke-interface {v2}, Lcom/google/android/location/j/d;->c()[B

    move-result-object v5

    iget-object v2, p0, Lcom/google/android/location/os/real/aw;->c:Lcom/google/android/location/os/real/x;

    iget-object v2, v2, Lcom/google/android/location/os/real/x;->y:Landroid/os/Handler;

    invoke-virtual {v2}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;

    move-result-object v7

    new-instance v8, Lcom/google/android/location/o/a/c;

    sget-object v2, Lcom/google/android/location/o/a/a;->a:Lcom/google/android/location/o/a/b;

    invoke-direct {v8, p3, v2}, Lcom/google/android/location/o/a/c;-><init>(Ljava/lang/String;Lcom/google/android/location/o/a/b;)V

    move-object v2, p1

    move-object v6, p2

    move-object v9, p4

    invoke-direct/range {v0 .. v9}, Lcom/google/android/location/collectionlib/i;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;[B[BLcom/google/android/location/collectionlib/ab;Landroid/os/Looper;Lcom/google/android/location/o/a/c;Lcom/google/android/location/o/n;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 651
    :catch_0
    move-exception v0

    .line 652
    sget-boolean v1, Lcom/google/android/location/i/a;->e:Z

    if-eqz v1, :cond_2

    const-string v1, "NetworkLocationRealOs"

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lcom/google/android/location/o/a/a;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    :cond_2
    move-object v0, v10

    .line 654
    goto :goto_0
.end method

.method public final a(Ljava/util/Set;Ljava/util/Map;JLjava/lang/String;Ljava/lang/Integer;ZLcom/google/p/a/b/b/a;ZLcom/google/android/location/collectionlib/ar;Ljava/lang/String;Lcom/google/android/location/o/n;)Lcom/google/android/location/collectionlib/be;
    .locals 11

    .prologue
    .line 528
    new-instance v8, Lcom/google/android/location/os/real/b;

    move-object/from16 v0, p10

    invoke-direct {v8, v0, p0}, Lcom/google/android/location/os/real/b;-><init>(Lcom/google/android/location/collectionlib/ar;Lcom/google/android/location/os/bi;)V

    .line 529
    new-instance v1, Lcom/google/android/location/collectionlib/bt;

    invoke-direct {v1}, Lcom/google/android/location/collectionlib/bt;-><init>()V

    .line 531
    iput-object p1, v1, Lcom/google/android/location/collectionlib/bt;->a:Ljava/util/Set;

    invoke-virtual {v1, p3, p4}, Lcom/google/android/location/collectionlib/bt;->a(J)Lcom/google/android/location/collectionlib/bt;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/location/os/real/aw;->q:Lcom/google/android/location/j/d;

    invoke-interface {v3}, Lcom/google/android/location/j/d;->c()[B

    move-result-object v3

    sget-object v4, Lcom/google/android/location/collectionlib/aj;->b:Lcom/google/android/location/collectionlib/aj;

    iput-object v4, v2, Lcom/google/android/location/collectionlib/bt;->b:Lcom/google/android/location/collectionlib/aj;

    move-object/from16 v0, p5

    iput-object v0, v2, Lcom/google/android/location/collectionlib/bt;->c:Ljava/lang/String;

    const/4 v4, 0x0

    iput-object v4, v2, Lcom/google/android/location/collectionlib/bt;->d:Ljava/lang/String;

    iput-object v3, v2, Lcom/google/android/location/collectionlib/bt;->e:[B

    move/from16 v0, p9

    iput-boolean v0, v2, Lcom/google/android/location/collectionlib/bt;->i:Z

    move-object/from16 v0, p12

    iput-object v0, v2, Lcom/google/android/location/collectionlib/bt;->j:Lcom/google/android/location/o/n;

    .line 536
    invoke-virtual {v1}, Lcom/google/android/location/collectionlib/bt;->a()Lcom/google/android/location/collectionlib/RealCollectorConfig;

    move-result-object v3

    .line 537
    move/from16 v0, p7

    invoke-virtual {v3, v0}, Lcom/google/android/location/collectionlib/RealCollectorConfig;->a(Z)V

    .line 538
    if-eqz p2, :cond_0

    .line 539
    invoke-interface {p2}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    .line 540
    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/location/collectionlib/cg;

    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v3, v2, v1}, Lcom/google/android/location/collectionlib/RealCollectorConfig;->a(Lcom/google/android/location/collectionlib/cg;I)V

    goto :goto_0

    .line 543
    :cond_0
    new-instance v1, Lcom/google/android/location/collectionlib/cq;

    iget-object v2, p0, Lcom/google/android/location/os/real/aw;->a:Landroid/content/Context;

    iget-object v4, p0, Lcom/google/android/location/os/real/aw;->o:Lcom/google/android/location/d/b;

    iget-object v5, p0, Lcom/google/android/location/os/real/aw;->i:Lcom/google/android/location/os/bn;

    new-instance v9, Lcom/google/android/location/o/a/c;

    sget-object v6, Lcom/google/android/location/o/a/a;->a:Lcom/google/android/location/o/a/b;

    move-object/from16 v0, p11

    invoke-direct {v9, v0, v6}, Lcom/google/android/location/o/a/c;-><init>(Ljava/lang/String;Lcom/google/android/location/o/a/b;)V

    move-object/from16 v6, p6

    move-object/from16 v7, p8

    invoke-direct/range {v1 .. v9}, Lcom/google/android/location/collectionlib/cq;-><init>(Landroid/content/Context;Lcom/google/android/location/collectionlib/ai;Lcom/google/android/location/d/b;Lcom/google/android/location/os/bn;Ljava/lang/Integer;Lcom/google/p/a/b/b/a;Lcom/google/android/location/collectionlib/ar;Lcom/google/android/location/o/a/c;)V

    return-object v1
.end method

.method public final a(ZLjava/util/Set;Ljava/util/Map;JLcom/google/android/location/collectionlib/SensorScannerConfig;Lcom/google/android/location/collectionlib/ar;Ljava/lang/String;Lcom/google/android/location/o/n;)Lcom/google/android/location/collectionlib/be;
    .locals 12

    .prologue
    .line 490
    new-instance v9, Lcom/google/android/location/os/real/b;

    move-object/from16 v0, p7

    invoke-direct {v9, v0, p0}, Lcom/google/android/location/os/real/b;-><init>(Lcom/google/android/location/collectionlib/ar;Lcom/google/android/location/os/bi;)V

    .line 491
    new-instance v3, Lcom/google/android/location/collectionlib/bt;

    invoke-direct {v3}, Lcom/google/android/location/collectionlib/bt;-><init>()V

    .line 492
    iput-object p2, v3, Lcom/google/android/location/collectionlib/bt;->a:Ljava/util/Set;

    if-eqz p1, :cond_1

    sget-object v2, Lcom/google/android/location/collectionlib/aj;->d:Lcom/google/android/location/collectionlib/aj;

    :goto_0
    iput-object v2, v3, Lcom/google/android/location/collectionlib/bt;->b:Lcom/google/android/location/collectionlib/aj;

    const/4 v2, 0x0

    iput-object v2, v3, Lcom/google/android/location/collectionlib/bt;->c:Ljava/lang/String;

    const/4 v2, 0x0

    iput-object v2, v3, Lcom/google/android/location/collectionlib/bt;->d:Ljava/lang/String;

    const/4 v2, 0x0

    iput-object v2, v3, Lcom/google/android/location/collectionlib/bt;->e:[B

    const/4 v2, 0x1

    iput-boolean v2, v3, Lcom/google/android/location/collectionlib/bt;->i:Z

    move-object/from16 v0, p9

    iput-object v0, v3, Lcom/google/android/location/collectionlib/bt;->j:Lcom/google/android/location/o/n;

    .line 496
    const-wide/16 v4, 0x0

    cmp-long v2, p4, v4

    if-ltz v2, :cond_2

    .line 497
    move-wide/from16 v0, p4

    invoke-virtual {v3, v0, v1}, Lcom/google/android/location/collectionlib/bt;->a(J)Lcom/google/android/location/collectionlib/bt;

    .line 501
    :goto_1
    if-eqz p6, :cond_0

    .line 502
    move-object/from16 v0, p6

    iput-object v0, v3, Lcom/google/android/location/collectionlib/bt;->h:Lcom/google/android/location/collectionlib/SensorScannerConfig;

    const/4 v2, 0x0

    iput-boolean v2, v3, Lcom/google/android/location/collectionlib/bt;->g:Z

    .line 504
    :cond_0
    invoke-virtual {v3}, Lcom/google/android/location/collectionlib/bt;->a()Lcom/google/android/location/collectionlib/RealCollectorConfig;

    move-result-object v4

    .line 505
    if-eqz p3, :cond_4

    .line 506
    invoke-interface {p3}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_2
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map$Entry;

    .line 507
    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/location/collectionlib/cg;

    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {v4, v3, v2}, Lcom/google/android/location/collectionlib/RealCollectorConfig;->a(Lcom/google/android/location/collectionlib/cg;I)V

    goto :goto_2

    .line 492
    :cond_1
    sget-object v2, Lcom/google/android/location/collectionlib/aj;->a:Lcom/google/android/location/collectionlib/aj;

    goto :goto_0

    .line 499
    :cond_2
    move-wide/from16 v0, p4

    neg-long v4, v0

    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v6, 0x13

    if-ge v2, v6, :cond_3

    new-instance v2, Ljava/lang/UnsupportedOperationException;

    const-string v3, "Scanning past data in supported starting from KitKat."

    invoke-direct {v2, v3}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_3
    iput-wide v4, v3, Lcom/google/android/location/collectionlib/bt;->f:J

    const/4 v2, 0x1

    iput-boolean v2, v3, Lcom/google/android/location/collectionlib/bt;->g:Z

    const/4 v2, 0x0

    iput-object v2, v3, Lcom/google/android/location/collectionlib/bt;->h:Lcom/google/android/location/collectionlib/SensorScannerConfig;

    goto :goto_1

    .line 510
    :cond_4
    new-instance v2, Lcom/google/android/location/collectionlib/cq;

    iget-object v3, p0, Lcom/google/android/location/os/real/aw;->a:Landroid/content/Context;

    iget-object v5, p0, Lcom/google/android/location/os/real/aw;->o:Lcom/google/android/location/d/b;

    iget-object v6, p0, Lcom/google/android/location/os/real/aw;->i:Lcom/google/android/location/os/bn;

    const/4 v7, 0x0

    const/4 v8, 0x0

    new-instance v10, Lcom/google/android/location/o/a/c;

    sget-object v11, Lcom/google/android/location/o/a/a;->a:Lcom/google/android/location/o/a/b;

    move-object/from16 v0, p8

    invoke-direct {v10, v0, v11}, Lcom/google/android/location/o/a/c;-><init>(Ljava/lang/String;Lcom/google/android/location/o/a/b;)V

    invoke-direct/range {v2 .. v10}, Lcom/google/android/location/collectionlib/cq;-><init>(Landroid/content/Context;Lcom/google/android/location/collectionlib/ai;Lcom/google/android/location/d/b;Lcom/google/android/location/os/bn;Ljava/lang/Integer;Lcom/google/p/a/b/b/a;Lcom/google/android/location/collectionlib/ar;Lcom/google/android/location/o/a/c;)V

    return-object v2
.end method

.method public final a()Lcom/google/android/location/e/ah;
    .locals 1

    .prologue
    .line 214
    iget-object v0, p0, Lcom/google/android/location/os/real/aw;->f:Lcom/google/android/location/e/ah;

    return-object v0
.end method

.method public final a(Ljava/lang/String;)Ljava/io/InputStream;
    .locals 3

    .prologue
    .line 463
    sget-boolean v0, Lcom/google/android/location/i/a;->f:Z

    if-eqz v0, :cond_0

    const-string v0, "location/"

    .line 466
    :goto_0
    :try_start_0
    iget-object v1, p0, Lcom/google/android/location/os/real/aw;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v2, 0x3

    invoke-virtual {v1, v0, v2}, Landroid/content/res/AssetManager;->open(Ljava/lang/String;I)Ljava/io/InputStream;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 469
    :goto_1
    return-object v0

    .line 463
    :cond_0
    const-string v0, ""

    goto :goto_0

    .line 468
    :catch_0
    move-exception v0

    sget-boolean v0, Lcom/google/android/location/i/a;->d:Z

    if-eqz v0, :cond_1

    const-string v0, "NetworkLocationRealOs"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Could not load asset: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/location/o/a/a;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 469
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public final a(II)V
    .locals 8

    .prologue
    .line 451
    iget-object v2, p0, Lcom/google/android/location/os/real/aw;->b:Lcom/google/android/location/os/j;

    new-instance v1, Lcom/google/android/location/os/t;

    sget-object v3, Lcom/google/android/location/os/au;->P:Lcom/google/android/location/os/au;

    iget-object v0, v2, Lcom/google/android/location/os/j;->a:Lcom/google/android/location/os/at;

    invoke-interface {v0}, Lcom/google/android/location/os/at;->a()J

    move-result-wide v4

    move v6, p1

    move v7, p2

    invoke-direct/range {v1 .. v7}, Lcom/google/android/location/os/t;-><init>(Lcom/google/android/location/os/j;Lcom/google/android/location/os/au;JII)V

    invoke-virtual {v2, v1, p1, p2}, Lcom/google/android/location/os/j;->a(Lcom/google/android/location/os/as;II)V

    .line 453
    iget-object v0, p0, Lcom/google/android/location/os/real/aw;->d:Lcom/google/android/location/os/real/ax;

    invoke-interface {v0, p1, p2}, Lcom/google/android/location/os/real/ax;->a(II)V

    .line 454
    return-void
.end method

.method public final a(IIIZLcom/google/android/location/o/n;)V
    .locals 8

    .prologue
    .line 261
    iget-object v6, p0, Lcom/google/android/location/os/real/aw;->c:Lcom/google/android/location/os/real/x;

    const/4 v7, 0x3

    new-instance v0, Lcom/google/android/location/os/real/af;

    move v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move-object v5, p5

    invoke-direct/range {v0 .. v5}, Lcom/google/android/location/os/real/af;-><init>(IIIZLcom/google/android/location/o/n;)V

    const/4 v1, 0x0

    invoke-virtual {v6, v7, v0, v1}, Lcom/google/android/location/os/real/x;->a(ILjava/lang/Object;Z)Z

    .line 263
    return-void
.end method

.method public final a(IIZZLcom/google/android/location/o/n;Ljava/util/Set;)V
    .locals 8

    .prologue
    .line 703
    iget-object v7, p0, Lcom/google/android/location/os/real/aw;->c:Lcom/google/android/location/os/real/x;

    new-instance v0, Lcom/google/android/location/os/real/y;

    move v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move-object v5, p5

    move-object v6, p6

    invoke-direct/range {v0 .. v6}, Lcom/google/android/location/os/real/y;-><init>(IIZZLcom/google/android/location/o/n;Ljava/util/Set;)V

    const/16 v1, 0x14

    const/4 v2, 0x1

    invoke-virtual {v7, v1, v0, v2}, Lcom/google/android/location/os/real/x;->a(ILjava/lang/Object;Z)Z

    .line 705
    return-void
.end method

.method public final a(J)V
    .locals 1

    .prologue
    .line 360
    iget-object v0, p0, Lcom/google/android/location/os/real/aw;->r:Lcom/google/android/location/a/a;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/location/a/a;->a(J)V

    .line 361
    return-void
.end method

.method public final a(Lcom/google/android/gms/location/ActivityRecognitionResult;)V
    .locals 3

    .prologue
    .line 569
    iget-object v0, p0, Lcom/google/android/location/os/real/aw;->c:Lcom/google/android/location/os/real/x;

    const/16 v1, 0x13

    const/4 v2, 0x1

    invoke-virtual {v0, v1, p1, v2}, Lcom/google/android/location/os/real/x;->a(ILjava/lang/Object;Z)Z

    .line 570
    iget-object v0, p0, Lcom/google/android/location/os/real/aw;->d:Lcom/google/android/location/os/real/ax;

    invoke-interface {v0, p1}, Lcom/google/android/location/os/real/ax;->a(Lcom/google/android/gms/location/ActivityRecognitionResult;)V

    .line 571
    return-void
.end method

.method public final a(Lcom/google/android/location/activity/bd;)V
    .locals 3

    .prologue
    .line 564
    iget-object v0, p0, Lcom/google/android/location/os/real/aw;->c:Lcom/google/android/location/os/real/x;

    const/16 v1, 0x15

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, v2}, Lcom/google/android/location/os/real/x;->a(ILjava/lang/Object;Z)Z

    .line 565
    return-void
.end method

.method public final a(Lcom/google/android/location/e/ag;Lcom/google/android/location/e/ay;)V
    .locals 12

    .prologue
    const/4 v1, -0x1

    const/4 v10, 0x0

    const/4 v9, 0x1

    .line 439
    iget-object v2, p0, Lcom/google/android/location/os/real/aw;->b:Lcom/google/android/location/os/j;

    iget-object v0, p1, Lcom/google/android/location/e/ag;->b:Lcom/google/android/location/e/bf;

    if-nez v0, :cond_1

    move v0, v1

    :goto_0
    iget-object v3, p1, Lcom/google/android/location/e/ag;->c:Lcom/google/android/location/e/f;

    if-nez v3, :cond_2

    move v7, v1

    :goto_1
    iget-boolean v1, p1, Lcom/google/android/location/e/ag;->d:Z

    if-eqz v1, :cond_3

    move v8, v9

    :goto_2
    iget-object v1, p1, Lcom/google/android/location/e/ag;->a:Lcom/google/android/location/e/z;

    iget-object v3, p1, Lcom/google/android/location/e/ag;->b:Lcom/google/android/location/e/bf;

    if-ne v1, v3, :cond_4

    const/4 v1, 0x2

    move v11, v1

    :goto_3
    new-instance v1, Lcom/google/android/location/os/s;

    sget-object v3, Lcom/google/android/location/os/au;->O:Lcom/google/android/location/os/au;

    iget-object v4, v2, Lcom/google/android/location/os/j;->a:Lcom/google/android/location/os/at;

    invoke-interface {v4}, Lcom/google/android/location/os/at;->a()J

    move-result-wide v4

    move-object v6, p1

    invoke-direct/range {v1 .. v6}, Lcom/google/android/location/os/s;-><init>(Lcom/google/android/location/os/j;Lcom/google/android/location/os/au;JLcom/google/android/location/e/ag;)V

    or-int v3, v8, v11

    invoke-virtual {v2, v1, v0, v7, v3}, Lcom/google/android/location/os/j;->a(Lcom/google/android/location/os/as;III)V

    .line 440
    if-eqz p1, :cond_0

    iget-object v0, p1, Lcom/google/android/location/e/ag;->a:Lcom/google/android/location/e/z;

    if-eqz v0, :cond_0

    iget-object v0, p1, Lcom/google/android/location/e/ag;->a:Lcom/google/android/location/e/z;

    iget-object v0, v0, Lcom/google/android/location/e/z;->c:Lcom/google/android/location/e/al;

    if-eqz v0, :cond_0

    iget-object v0, p1, Lcom/google/android/location/e/ag;->a:Lcom/google/android/location/e/z;

    iget-object v0, v0, Lcom/google/android/location/e/z;->d:Lcom/google/android/location/e/ab;

    sget-object v1, Lcom/google/android/location/e/ab;->a:Lcom/google/android/location/e/ab;

    if-eq v0, v1, :cond_5

    .line 447
    :cond_0
    :goto_4
    return-void

    .line 439
    :cond_1
    iget-object v0, p1, Lcom/google/android/location/e/ag;->b:Lcom/google/android/location/e/bf;

    iget-object v0, v0, Lcom/google/android/location/e/bf;->d:Lcom/google/android/location/e/ab;

    invoke-virtual {v0}, Lcom/google/android/location/e/ab;->ordinal()I

    move-result v0

    goto :goto_0

    :cond_2
    iget-object v1, p1, Lcom/google/android/location/e/ag;->c:Lcom/google/android/location/e/f;

    iget-object v1, v1, Lcom/google/android/location/e/f;->d:Lcom/google/android/location/e/ab;

    invoke-virtual {v1}, Lcom/google/android/location/e/ab;->ordinal()I

    move-result v1

    move v7, v1

    goto :goto_1

    :cond_3
    move v8, v10

    goto :goto_2

    :cond_4
    move v11, v10

    goto :goto_3

    .line 445
    :cond_5
    sget-object v0, Lcom/google/android/location/d/a;->d:Lcom/google/android/gms/common/a/d;

    invoke-static {v0}, Lcom/google/android/location/d/a;->a(Lcom/google/android/gms/common/a/d;)Z

    move-result v0

    if-eqz v0, :cond_7

    const-string v1, "nlp"

    const-string v2, "diags"

    const-string v3, "wifitimes"

    iget-object v0, p0, Lcom/google/android/location/os/real/aw;->u:Lcom/google/android/location/os/real/bi;

    iget v0, v0, Lcom/google/android/location/os/real/bi;->a:I

    if-eq v0, v9, :cond_6

    move v10, v9

    :cond_6
    if-eqz v10, :cond_8

    const-wide/16 v4, 0x1

    :goto_5
    move-object v0, p0

    move v6, v9

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/location/os/real/aw;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JZ)V

    .line 446
    :cond_7
    iget-object v0, p0, Lcom/google/android/location/os/real/aw;->d:Lcom/google/android/location/os/real/ax;

    invoke-interface {v0, p1, p2}, Lcom/google/android/location/os/real/ax;->a(Lcom/google/android/location/e/ag;Lcom/google/android/location/e/ay;)V

    goto :goto_4

    .line 445
    :cond_8
    const-wide/16 v4, 0x0

    goto :goto_5
.end method

.method public final a(Lcom/google/android/location/e/bh;)V
    .locals 3

    .prologue
    .line 575
    iget-object v0, p0, Lcom/google/android/location/os/real/aw;->c:Lcom/google/android/location/os/real/x;

    const/16 v1, 0x20

    const/4 v2, 0x1

    invoke-virtual {v0, v1, p1, v2}, Lcom/google/android/location/os/real/x;->a(ILjava/lang/Object;Z)Z

    .line 576
    return-void
.end method

.method final a(Lcom/google/android/location/j/k;)V
    .locals 2

    .prologue
    .line 234
    iget-object v0, p0, Lcom/google/android/location/os/real/aw;->k:Lcom/google/android/location/os/real/az;

    invoke-virtual {p1}, Lcom/google/android/location/j/k;->ordinal()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/location/os/real/az;->a(I)V

    .line 235
    return-void
.end method

.method public final a(Lcom/google/android/location/j/k;Z)V
    .locals 10

    .prologue
    const/4 v8, 0x1

    const/4 v9, 0x0

    .line 418
    iget-object v2, p0, Lcom/google/android/location/os/real/aw;->b:Lcom/google/android/location/os/j;

    invoke-virtual {p1}, Lcom/google/android/location/j/k;->ordinal()I

    move-result v7

    new-instance v1, Lcom/google/android/location/os/u;

    sget-object v3, Lcom/google/android/location/os/au;->M:Lcom/google/android/location/os/au;

    iget-object v0, v2, Lcom/google/android/location/os/j;->a:Lcom/google/android/location/os/at;

    invoke-interface {v0}, Lcom/google/android/location/os/at;->a()J

    move-result-wide v4

    move v6, p2

    invoke-direct/range {v1 .. v7}, Lcom/google/android/location/os/u;-><init>(Lcom/google/android/location/os/j;Lcom/google/android/location/os/au;JZI)V

    if-eqz p2, :cond_1

    move v0, v8

    :goto_0
    invoke-virtual {v2, v1, v0, v7}, Lcom/google/android/location/os/j;->a(Lcom/google/android/location/os/as;II)V

    .line 419
    iget-object v0, p0, Lcom/google/android/location/os/real/aw;->c:Lcom/google/android/location/os/real/x;

    iget-object v1, p0, Lcom/google/android/location/os/real/aw;->o:Lcom/google/android/location/d/b;

    invoke-virtual {p1}, Lcom/google/android/location/j/k;->ordinal()I

    move-result v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    iget-boolean v3, v0, Lcom/google/android/location/os/real/x;->A:Z

    if-eq v3, p2, :cond_0

    iput-boolean p2, v0, Lcom/google/android/location/os/real/x;->A:Z

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v3

    if-eqz p2, :cond_2

    iget-object v4, v0, Lcom/google/android/location/os/real/x;->p:Lcom/google/android/location/os/real/ac;

    invoke-virtual {v1, v2, v9, v4}, Lcom/google/android/location/d/b;->a(Ljava/lang/String;ZLandroid/location/LocationListener;)V

    const-string v4, "gps"

    iget-object v0, v0, Lcom/google/android/location/os/real/x;->q:Lcom/google/android/location/os/real/ac;

    invoke-virtual {v1, v2, v4, v0, v3}, Lcom/google/android/location/d/b;->a(Ljava/lang/String;Ljava/lang/String;Landroid/location/LocationListener;Landroid/os/Looper;)V

    .line 420
    :cond_0
    :goto_1
    return-void

    :cond_1
    move v0, v9

    .line 418
    goto :goto_0

    .line 419
    :cond_2
    iget-object v4, v0, Lcom/google/android/location/os/real/x;->q:Lcom/google/android/location/os/real/ac;

    invoke-virtual {v1, v2, v8, v4}, Lcom/google/android/location/d/b;->a(Ljava/lang/String;ZLandroid/location/LocationListener;)V

    const-string v4, "passive"

    iget-object v0, v0, Lcom/google/android/location/os/real/x;->p:Lcom/google/android/location/os/real/ac;

    invoke-virtual {v1, v2, v4, v0, v3}, Lcom/google/android/location/d/b;->a(Ljava/lang/String;Ljava/lang/String;Landroid/location/LocationListener;Landroid/os/Looper;)V

    goto :goto_1
.end method

.method public final a(Ljava/io/PrintWriter;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 766
    iget-object v0, p0, Lcom/google/android/location/os/real/aw;->c:Lcom/google/android/location/os/real/x;

    new-instance v1, Ljava/util/concurrent/CountDownLatch;

    invoke-direct {v1, v4}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V

    const/16 v2, 0x1f

    invoke-static {p1, v1}, Lcom/google/android/location/e/aj;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/android/location/e/aj;

    move-result-object v3

    invoke-virtual {v0, v2, v3, v4}, Lcom/google/android/location/os/real/x;->a(ILjava/lang/Object;Z)Z

    invoke-static {v1}, Lcom/google/android/location/os/real/x;->a(Ljava/util/concurrent/CountDownLatch;)V

    .line 767
    return-void
.end method

.method public final bridge synthetic a(Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 89
    check-cast p1, Lcom/google/android/location/e/ah;

    iget-object v0, p0, Lcom/google/android/location/os/real/aw;->c:Lcom/google/android/location/os/real/x;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/location/os/real/aw;->c:Lcom/google/android/location/os/real/x;

    const/16 v1, 0x12

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, v2}, Lcom/google/android/location/os/real/x;->a(ILjava/lang/Object;Z)Z

    :cond_0
    return-void
.end method

.method public final a(Ljava/lang/String;Lcom/google/android/location/collectionlib/cg;)V
    .locals 6

    .prologue
    .line 694
    iget-object v0, p0, Lcom/google/android/location/os/real/aw;->a:Landroid/content/Context;

    new-instance v1, Lcom/google/android/location/o/a/c;

    const-string v2, "NetworkLocationRealOs"

    sget-object v3, Lcom/google/android/location/o/a/a;->a:Lcom/google/android/location/o/a/b;

    invoke-direct {v1, v2, v3}, Lcom/google/android/location/o/a/c;-><init>(Ljava/lang/String;Lcom/google/android/location/o/a/b;)V

    invoke-static {v0, v1}, Lcom/google/android/location/collectionlib/u;->a(Landroid/content/Context;Lcom/google/android/location/o/a/c;)Lcom/google/android/location/collectionlib/u;

    move-result-object v2

    .line 696
    invoke-virtual {v2, p2}, Lcom/google/android/location/collectionlib/u;->b(Lcom/google/android/location/collectionlib/cg;)V

    iget-object v3, v2, Lcom/google/android/location/collectionlib/u;->e:Ljava/lang/Object;

    monitor-enter v3

    :try_start_0
    invoke-virtual {p2}, Lcom/google/android/location/collectionlib/cg;->c()Z

    move-result v0

    if-nez v0, :cond_1

    sget-boolean v0, Lcom/google/android/location/i/a;->d:Z

    if-eqz v0, :cond_0

    iget-object v0, v2, Lcom/google/android/location/collectionlib/u;->a:Lcom/google/android/location/o/a/c;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v4, "ScannerType "

    invoke-direct {v1, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p2}, Lcom/google/android/location/collectionlib/cg;->b()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, " not supported"

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/location/o/a/c;->c(Ljava/lang/String;)V

    :cond_0
    :goto_0
    invoke-virtual {v2}, Lcom/google/android/location/collectionlib/u;->a()V

    monitor-exit v3

    return-void

    :cond_1
    invoke-virtual {p2}, Lcom/google/android/location/collectionlib/cg;->a()I

    move-result v4

    invoke-virtual {v2, p2}, Lcom/google/android/location/collectionlib/u;->a(Lcom/google/android/location/collectionlib/cg;)Landroid/hardware/Sensor;

    move-result-object v5

    iget-object v0, v2, Lcom/google/android/location/collectionlib/u;->h:Landroid/util/SparseArray;

    invoke-virtual {v0, v4}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/collectionlib/z;

    if-eqz v0, :cond_2

    iget-object v1, v0, Lcom/google/android/location/collectionlib/z;->b:Ljava/util/Set;

    invoke-interface {v1, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    move-result v1

    iget-object v0, v0, Lcom/google/android/location/collectionlib/z;->b:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, v2, Lcom/google/android/location/collectionlib/u;->h:Landroid/util/SparseArray;

    invoke-virtual {v0, v4}, Landroid/util/SparseArray;->remove(I)V

    iget-object v0, v2, Lcom/google/android/location/collectionlib/u;->b:Landroid/hardware/SensorManager;

    iget-object v4, v2, Lcom/google/android/location/collectionlib/u;->c:Landroid/hardware/SensorEventListener;

    invoke-virtual {v0, v4, v5}, Landroid/hardware/SensorManager;->unregisterListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;)V

    sget-boolean v0, Lcom/google/android/location/i/a;->b:Z

    if-eqz v0, :cond_3

    iget-object v0, v2, Lcom/google/android/location/collectionlib/u;->a:Lcom/google/android/location/o/a/c;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Canceling batch for scanner type "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " because no client requests it."

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/google/android/location/o/a/c;->a(Ljava/lang/String;)V

    move v0, v1

    :goto_1
    if-nez v0, :cond_0

    sget-boolean v0, Lcom/google/android/location/i/a;->b:Z

    if-eqz v0, :cond_0

    iget-object v0, v2, Lcom/google/android/location/collectionlib/u;->a:Lcom/google/android/location/o/a/c;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v4, "ClientId "

    invoke-direct {v1, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, " for scanner type "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, " not exists."

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/location/o/a/c;->a(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v3

    throw v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_1

    :cond_3
    move v0, v1

    goto :goto_1
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JZ)V
    .locals 8

    .prologue
    .line 355
    iget-object v0, p0, Lcom/google/android/location/os/real/aw;->r:Lcom/google/android/location/a/a;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-wide v4, p4

    move v6, p6

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/location/a/a;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JZ)V

    .line 356
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/util/Map;)V
    .locals 4

    .prologue
    .line 757
    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    .line 758
    invoke-virtual {v2, p1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 759
    invoke-interface {p2}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 760
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/io/Serializable;

    invoke-virtual {v2, v1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    goto :goto_0

    .line 762
    :cond_0
    iget-object v0, p0, Lcom/google/android/location/os/real/aw;->e:Landroid/support/v4/a/m;

    invoke-virtual {v0, v2}, Landroid/support/v4/a/m;->a(Landroid/content/Intent;)Z

    .line 763
    return-void
.end method

.method public final a(Ljava/text/Format;Ljava/io/PrintWriter;)V
    .locals 4

    .prologue
    .line 711
    iget-object v0, p0, Lcom/google/android/location/os/real/aw;->o:Lcom/google/android/location/d/b;

    iget-object v1, p0, Lcom/google/android/location/os/real/aw;->g:Lcom/google/android/location/j/b;

    invoke-interface {v1}, Lcom/google/android/location/j/b;->d()J

    move-result-wide v2

    invoke-virtual {v0, p1, v2, v3, p2}, Lcom/google/android/location/d/b;->a(Ljava/text/Format;JLjava/io/PrintWriter;)V

    .line 712
    return-void
.end method

.method public final a(Z)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 271
    iget-object v2, p0, Lcom/google/android/location/os/real/aw;->c:Lcom/google/android/location/os/real/x;

    iget-object v3, v2, Lcom/google/android/location/os/real/x;->w:Ljava/lang/Object;

    monitor-enter v3

    :try_start_0
    iget-boolean v4, v2, Lcom/google/android/location/os/real/x;->x:Z

    if-eqz v4, :cond_0

    monitor-exit v3

    :goto_0
    return-void

    :cond_0
    sget-boolean v4, Lcom/google/android/location/i/a;->b:Z

    if-eqz v4, :cond_1

    const-string v4, "NetworkLocationCallbackRunner"

    const-string v5, "quit"

    invoke-static {v4, v5}, Lcom/google/android/location/o/a/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    iget-object v4, v2, Lcom/google/android/location/os/real/x;->y:Landroid/os/Handler;

    if-eqz v4, :cond_2

    const/4 v4, 0x1

    if-eqz p1, :cond_3

    :goto_1
    const/4 v1, 0x0

    invoke-virtual {v2, v4, v0, v1}, Lcom/google/android/location/os/real/x;->a(IIZ)Z

    :cond_2
    const/4 v0, 0x1

    iput-boolean v0, v2, Lcom/google/android/location/os/real/x;->x:Z

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v3

    throw v0

    :cond_3
    move v0, v1

    goto :goto_1
.end method

.method public final a(ZLjava/lang/String;)V
    .locals 4

    .prologue
    .line 722
    iget-object v0, p0, Lcom/google/android/location/os/real/aw;->c:Lcom/google/android/location/os/real/x;

    const/16 v1, 0x16

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-static {v2, p2}, Lcom/google/android/location/e/aj;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/android/location/e/aj;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/location/os/real/x;->a(ILjava/lang/Object;Z)Z

    .line 723
    return-void
.end method

.method public final a(ZZ)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    .line 303
    iget-object v2, p0, Lcom/google/android/location/os/real/aw;->c:Lcom/google/android/location/os/real/x;

    new-instance v3, Ljava/util/concurrent/CountDownLatch;

    invoke-direct {v3, v1}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V

    const/4 v0, 0x0

    if-eqz p1, :cond_0

    move v0, v1

    :cond_0
    if-eqz p2, :cond_1

    or-int/lit8 v0, v0, 0x2

    :cond_1
    const/16 v4, 0x1d

    invoke-virtual {v2, v4, v0, v3, v1}, Lcom/google/android/location/os/real/x;->a(IILjava/lang/Object;Z)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-static {v3}, Lcom/google/android/location/os/real/x;->a(Ljava/util/concurrent/CountDownLatch;)V

    .line 304
    :cond_2
    return-void
.end method

.method public final a(I)Z
    .locals 2

    .prologue
    .line 680
    sget-object v0, Lcom/google/android/location/d/a;->m:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/google/android/location/os/real/bf;->a()Lcom/google/android/location/os/real/bf;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/location/os/real/aw;->m:Landroid/hardware/SensorManager;

    invoke-virtual {v0, v1, p1}, Lcom/google/android/location/os/real/bf;->a(Landroid/hardware/SensorManager;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Lcom/google/android/location/collectionlib/cg;)Z
    .locals 2

    .prologue
    .line 631
    sget-object v0, Lcom/google/android/location/collectionlib/RealCollectorConfig;->b:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 632
    if-nez v0, :cond_0

    .line 633
    const/4 v0, 0x0

    .line 635
    :goto_0
    return v0

    :cond_0
    iget-object v1, p0, Lcom/google/android/location/os/real/aw;->m:Landroid/hardware/SensorManager;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v1, v0}, Lcom/google/android/location/os/real/bg;->a(Landroid/hardware/SensorManager;I)Z

    move-result v0

    goto :goto_0
.end method

.method public final a(Lcom/google/android/location/os/bk;Ljava/lang/Object;)Z
    .locals 4

    .prologue
    .line 727
    iget-object v0, p0, Lcom/google/android/location/os/real/aw;->c:Lcom/google/android/location/os/real/x;

    const/16 v1, 0x17

    invoke-static {p1, p2}, Lcom/google/android/location/e/aj;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/android/location/e/aj;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/location/os/real/x;->a(ILjava/lang/Object;Z)Z

    move-result v0

    return v0
.end method

.method public final a(Ljava/lang/String;Lcom/google/android/location/collectionlib/cg;ILjava/lang/String;)Z
    .locals 3

    .prologue
    .line 687
    iget-object v0, p0, Lcom/google/android/location/os/real/aw;->a:Landroid/content/Context;

    new-instance v1, Lcom/google/android/location/o/a/c;

    sget-object v2, Lcom/google/android/location/o/a/a;->a:Lcom/google/android/location/o/a/b;

    invoke-direct {v1, p4, v2}, Lcom/google/android/location/o/a/c;-><init>(Ljava/lang/String;Lcom/google/android/location/o/a/b;)V

    invoke-static {v0, v1}, Lcom/google/android/location/collectionlib/u;->a(Landroid/content/Context;Lcom/google/android/location/o/a/c;)Lcom/google/android/location/collectionlib/u;

    move-result-object v0

    .line 689
    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/location/collectionlib/u;->a(Ljava/lang/String;Lcom/google/android/location/collectionlib/cg;I)Z

    move-result v0

    return v0
.end method

.method public final b()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 347
    iget-object v0, p0, Lcom/google/android/location/os/real/aw;->b:Lcom/google/android/location/os/j;

    sget-object v1, Lcom/google/android/location/os/au;->A:Lcom/google/android/location/os/au;

    invoke-virtual {v0, v1}, Lcom/google/android/location/os/j;->a(Lcom/google/android/location/os/au;)V

    .line 348
    iget-object v0, p0, Lcom/google/android/location/os/real/aw;->c:Lcom/google/android/location/os/real/x;

    const/4 v1, 0x4

    invoke-virtual {v0, v1, v2, v2}, Lcom/google/android/location/os/real/x;->a(IIZ)Z

    .line 349
    return-void
.end method

.method public final b(J)V
    .locals 1

    .prologue
    .line 797
    invoke-static {}, Lcom/google/android/location/os/real/bf;->a()Lcom/google/android/location/os/real/bf;

    move-result-object v0

    invoke-virtual {v0, p0, p1, p2}, Lcom/google/android/location/os/real/bf;->a(Lcom/google/android/location/os/bi;J)V

    .line 798
    return-void
.end method

.method public final b(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 752
    iget-object v0, p0, Lcom/google/android/location/os/real/aw;->e:Landroid/support/v4/a/m;

    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1, p1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/support/v4/a/m;->a(Landroid/content/Intent;)Z

    .line 753
    return-void
.end method

.method public final b(Z)V
    .locals 6

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 308
    iget-object v3, p0, Lcom/google/android/location/os/real/aw;->c:Lcom/google/android/location/os/real/x;

    new-instance v4, Ljava/util/concurrent/CountDownLatch;

    invoke-direct {v4, v1}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V

    const/16 v5, 0x1e

    if-eqz p1, :cond_1

    move v0, v1

    :goto_0
    invoke-virtual {v3, v5, v0, v4, v2}, Lcom/google/android/location/os/real/x;->a(IILjava/lang/Object;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {v4}, Lcom/google/android/location/os/real/x;->a(Ljava/util/concurrent/CountDownLatch;)V

    .line 312
    :cond_0
    iget-object v0, p0, Lcom/google/android/location/os/real/aw;->k:Lcom/google/android/location/os/real/az;

    invoke-virtual {v0, v1}, Lcom/google/android/location/os/real/az;->a(Z)V

    .line 313
    return-void

    :cond_1
    move v0, v2

    .line 308
    goto :goto_0
.end method

.method public final c()Lcom/google/android/location/j/b;
    .locals 1

    .prologue
    .line 392
    iget-object v0, p0, Lcom/google/android/location/os/real/aw;->g:Lcom/google/android/location/j/b;

    return-object v0
.end method

.method public final d()Lcom/google/android/location/j/d;
    .locals 1

    .prologue
    .line 376
    iget-object v0, p0, Lcom/google/android/location/os/real/aw;->q:Lcom/google/android/location/j/d;

    return-object v0
.end method

.method public final e()Lcom/google/android/location/j/e;
    .locals 1

    .prologue
    .line 400
    iget-object v0, p0, Lcom/google/android/location/os/real/aw;->h:Lcom/google/android/location/j/e;

    return-object v0
.end method

.method public final f()Lcom/google/android/location/j/j;
    .locals 1

    .prologue
    .line 384
    iget-object v0, p0, Lcom/google/android/location/os/real/aw;->k:Lcom/google/android/location/os/real/az;

    return-object v0
.end method

.method public final g()Lcom/google/android/location/os/bn;
    .locals 1

    .prologue
    .line 408
    iget-object v0, p0, Lcom/google/android/location/os/real/aw;->i:Lcom/google/android/location/os/bn;

    return-object v0
.end method

.method public final h()Z
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 424
    iget-object v0, p0, Lcom/google/android/location/os/real/aw;->a:Landroid/content/Context;

    const-string v2, "location"

    invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/location/LocationManager;

    .line 426
    :try_start_0
    const-string v2, "gps"

    invoke-virtual {v0, v2}, Landroid/location/LocationManager;->isProviderEnabled(Ljava/lang/String;)Z
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_1

    move-result v0

    .line 431
    :goto_0
    return v0

    .line 427
    :catch_0
    move-exception v0

    .line 428
    sget-boolean v2, Lcom/google/android/location/i/a;->b:Z

    if-eqz v2, :cond_0

    const-string v2, "NetworkLocationRealOs"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "can\'t check GPS "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/SecurityException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/google/android/location/o/a/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    move v0, v1

    .line 429
    goto :goto_0

    .line 431
    :catch_1
    move-exception v0

    move v0, v1

    goto :goto_0
.end method

.method public final i()Lcom/google/android/location/os/aw;
    .locals 1

    .prologue
    .line 458
    iget-object v0, p0, Lcom/google/android/location/os/real/aw;->d:Lcom/google/android/location/os/real/ax;

    invoke-interface {v0}, Lcom/google/android/location/os/real/ax;->d()Lcom/google/android/location/os/aw;

    move-result-object v0

    return-object v0
.end method

.method public final j()Lcom/google/android/location/os/bj;
    .locals 5

    .prologue
    .line 475
    new-instance v0, Lcom/google/android/location/os/bj;

    sget-object v1, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    sget-object v2, Landroid/os/Build;->MODEL:Ljava/lang/String;

    sget-object v3, Landroid/os/Build;->FINGERPRINT:Ljava/lang/String;

    sget v4, Landroid/os/Build$VERSION;->SDK_INT:I

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/location/os/bj;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    return-object v0
.end method

.method public final k()Ljava/lang/String;
    .locals 2

    .prologue
    .line 585
    iget-object v0, p0, Lcom/google/android/location/os/real/aw;->a:Landroid/content/Context;

    const-string v1, "phone"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    .line 587
    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getNetworkCountryIso()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final l()Ljava/lang/String;
    .locals 2

    .prologue
    .line 592
    iget-object v0, p0, Lcom/google/android/location/os/real/aw;->a:Landroid/content/Context;

    const-string v1, "phone"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    .line 594
    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getSimCountryIso()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final m()Z
    .locals 2

    .prologue
    .line 626
    iget-object v0, p0, Lcom/google/android/location/os/real/aw;->m:Landroid/hardware/SensorManager;

    iget-object v1, p0, Lcom/google/android/location/os/real/aw;->n:Landroid/location/LocationManager;

    invoke-static {v0, v1}, Lcom/google/android/location/os/real/bg;->a(Landroid/hardware/SensorManager;Landroid/location/LocationManager;)Z

    move-result v0

    return v0
.end method

.method public final n()Ljava/io/File;
    .locals 1

    .prologue
    .line 599
    iget-object v0, p0, Lcom/google/android/location/os/real/aw;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/location/os/real/aw;->d(Landroid/content/Context;)Ljava/io/File;

    move-result-object v0

    return-object v0
.end method

.method public final o()Ljava/io/File;
    .locals 1

    .prologue
    .line 608
    iget-object v0, p0, Lcom/google/android/location/os/real/aw;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/location/os/real/aw;->e(Landroid/content/Context;)Ljava/io/File;

    move-result-object v0

    return-object v0
.end method

.method public final p()Ljava/io/File;
    .locals 3

    .prologue
    .line 617
    iget-object v0, p0, Lcom/google/android/location/os/real/aw;->a:Landroid/content/Context;

    new-instance v1, Ljava/io/File;

    invoke-virtual {v0}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v0

    const-string v2, "nlp_acd"

    invoke-direct {v1, v0, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    return-object v1
.end method

.method public final q()I
    .locals 1

    .prologue
    .line 659
    new-instance v0, Landroid/os/Debug$MemoryInfo;

    invoke-direct {v0}, Landroid/os/Debug$MemoryInfo;-><init>()V

    .line 660
    invoke-static {v0}, Landroid/os/Debug;->getMemoryInfo(Landroid/os/Debug$MemoryInfo;)V

    .line 661
    invoke-virtual {v0}, Landroid/os/Debug$MemoryInfo;->getTotalPss()I

    move-result v0

    return v0
.end method

.method public final r()I
    .locals 4

    .prologue
    .line 666
    new-instance v1, Landroid/app/ActivityManager$MemoryInfo;

    invoke-direct {v1}, Landroid/app/ActivityManager$MemoryInfo;-><init>()V

    .line 667
    iget-object v0, p0, Lcom/google/android/location/os/real/aw;->a:Landroid/content/Context;

    const-string v2, "activity"

    invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    .line 668
    invoke-virtual {v0, v1}, Landroid/app/ActivityManager;->getMemoryInfo(Landroid/app/ActivityManager$MemoryInfo;)V

    .line 669
    iget-wide v0, v1, Landroid/app/ActivityManager$MemoryInfo;->availMem:J

    const-wide/16 v2, 0x400

    div-long/2addr v0, v2

    long-to-int v0, v0

    return v0
.end method

.method public final s()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 674
    iget-object v1, p0, Lcom/google/android/location/os/real/aw;->m:Landroid/hardware/SensorManager;

    invoke-virtual {v1, v0}, Landroid/hardware/SensorManager;->getSensorList(I)Ljava/util/List;

    move-result-object v1

    .line 675
    if-eqz v1, :cond_0

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final t()J
    .locals 2

    .prologue
    .line 191
    iget-wide v0, p0, Lcom/google/android/location/os/real/aw;->p:J

    return-wide v0
.end method

.method public final u()Z
    .locals 1

    .prologue
    .line 732
    invoke-static {}, Lcom/google/android/location/os/real/bf;->a()Lcom/google/android/location/os/real/bf;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/location/os/real/bf;->d()Z

    move-result v0

    return v0
.end method

.method public final v()Lcom/google/android/location/os/j;
    .locals 1

    .prologue
    .line 737
    iget-object v0, p0, Lcom/google/android/location/os/real/aw;->b:Lcom/google/android/location/os/j;

    return-object v0
.end method

.method public final w()Lcom/google/android/location/activity/at;
    .locals 2

    .prologue
    .line 742
    invoke-static {}, Lcom/google/android/location/os/real/bf;->a()Lcom/google/android/location/os/real/bf;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/location/os/real/aw;->m:Landroid/hardware/SensorManager;

    invoke-virtual {v0, v1, p0}, Lcom/google/android/location/os/real/bf;->a(Landroid/hardware/SensorManager;Lcom/google/android/location/os/bi;)Lcom/google/android/location/activity/at;

    move-result-object v0

    return-object v0
.end method

.method public final x()Lcom/google/android/location/activity/bn;
    .locals 2

    .prologue
    .line 747
    invoke-static {}, Lcom/google/android/location/os/real/bf;->a()Lcom/google/android/location/os/real/bf;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/location/os/real/aw;->m:Landroid/hardware/SensorManager;

    invoke-virtual {v0, v1, p0}, Lcom/google/android/location/os/real/bf;->b(Landroid/hardware/SensorManager;Lcom/google/android/location/os/bi;)Lcom/google/android/location/activity/bn;

    move-result-object v0

    return-object v0
.end method

.method public final y()Z
    .locals 1

    .prologue
    .line 771
    iget-boolean v0, p0, Lcom/google/android/location/os/real/aw;->s:Z

    return v0
.end method

.method public final z()V
    .locals 4

    .prologue
    .line 580
    iget-object v0, p0, Lcom/google/android/location/os/real/aw;->c:Lcom/google/android/location/os/real/x;

    const/16 v1, 0x21

    const/4 v2, 0x0

    const/4 v3, 0x1

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/location/os/real/x;->a(IIZ)Z

    .line 581
    return-void
.end method
