.class public final Lcom/google/android/location/os/ak;
.super Lcom/google/android/location/os/as;
.source "SourceFile"


# instance fields
.field final synthetic a:J

.field final synthetic b:J

.field final synthetic c:Z

.field final synthetic d:Lcom/google/android/location/os/j;


# direct methods
.method public constructor <init>(Lcom/google/android/location/os/j;Lcom/google/android/location/os/au;JJJZ)V
    .locals 1

    .prologue
    .line 644
    iput-object p1, p0, Lcom/google/android/location/os/ak;->d:Lcom/google/android/location/os/j;

    iput-wide p5, p0, Lcom/google/android/location/os/ak;->a:J

    iput-wide p7, p0, Lcom/google/android/location/os/ak;->b:J

    iput-boolean p9, p0, Lcom/google/android/location/os/ak;->c:Z

    invoke-direct {p0, p2, p3, p4}, Lcom/google/android/location/os/as;-><init>(Lcom/google/android/location/os/au;J)V

    return-void
.end method


# virtual methods
.method protected final a(Ljava/io/PrintWriter;)V
    .locals 4

    .prologue
    .line 647
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "minPeriodMillis="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v2, p0, Lcom/google/android/location/os/ak;->a:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 648
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, ", minPeriodTiltDetectorMillis="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v2, p0, Lcom/google/android/location/os/ak;->b:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 649
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, ", forceDetectionNow="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v1, p0, Lcom/google/android/location/os/ak;->c:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 650
    return-void
.end method
