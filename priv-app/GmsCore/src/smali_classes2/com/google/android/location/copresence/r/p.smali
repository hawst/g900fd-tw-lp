.class public final Lcom/google/android/location/copresence/r/p;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x10
.end annotation


# static fields
.field static final a:Ljava/util/Set;


# instance fields
.field b:Lcom/google/android/location/copresence/r/ad;

.field final c:Lcom/google/android/location/copresence/r/ad;

.field private final d:Landroid/content/Context;

.field private final e:Lcom/google/android/location/copresence/r/ah;

.field private final f:Lcom/google/android/location/copresence/k/a;

.field private final g:Landroid/content/SharedPreferences;

.field private final h:Lcom/google/android/location/copresence/r/ad;

.field private final i:Lcom/google/android/location/copresence/r/ad;

.field private final j:Landroid/net/wifi/p2p/WifiP2pManager$ActionListener;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 44
    const-string v0, "deviceName"

    const-string v1, "groupInterface"

    const-string v2, "updated"

    invoke-static {v0, v1, v2}, Lcom/google/k/c/cd;->a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/k/c/cd;

    move-result-object v0

    sput-object v0, Lcom/google/android/location/copresence/r/p;->a:Ljava/util/Set;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/android/location/copresence/r/ah;Lcom/google/android/location/copresence/k/a;Landroid/content/SharedPreferences;)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 58
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 110
    new-instance v0, Lcom/google/android/location/copresence/r/q;

    const-string v1, "DeviceNameReverted"

    new-array v2, v4, [Lcom/google/android/location/copresence/k/f;

    invoke-direct {v0, p0, v1, v2}, Lcom/google/android/location/copresence/r/q;-><init>(Lcom/google/android/location/copresence/r/p;Ljava/lang/String;[Lcom/google/android/location/copresence/k/f;)V

    iput-object v0, p0, Lcom/google/android/location/copresence/r/p;->b:Lcom/google/android/location/copresence/r/ad;

    .line 137
    new-instance v0, Lcom/google/android/location/copresence/r/r;

    const-string v1, "UserDeviceNameSaved"

    new-array v2, v5, [Lcom/google/android/location/copresence/k/f;

    iget-object v3, p0, Lcom/google/android/location/copresence/r/p;->b:Lcom/google/android/location/copresence/r/ad;

    aput-object v3, v2, v4

    invoke-direct {v0, p0, v1, v2}, Lcom/google/android/location/copresence/r/r;-><init>(Lcom/google/android/location/copresence/r/p;Ljava/lang/String;[Lcom/google/android/location/copresence/k/f;)V

    iput-object v0, p0, Lcom/google/android/location/copresence/r/p;->h:Lcom/google/android/location/copresence/r/ad;

    .line 298
    new-instance v0, Lcom/google/android/location/copresence/r/t;

    const-string v1, "NoGroup"

    new-array v2, v4, [Lcom/google/android/location/copresence/k/f;

    invoke-direct {v0, p0, v1, v2}, Lcom/google/android/location/copresence/r/t;-><init>(Lcom/google/android/location/copresence/r/p;Ljava/lang/String;[Lcom/google/android/location/copresence/k/f;)V

    iput-object v0, p0, Lcom/google/android/location/copresence/r/p;->i:Lcom/google/android/location/copresence/r/ad;

    .line 368
    new-instance v0, Lcom/google/android/location/copresence/r/w;

    invoke-direct {v0, p0}, Lcom/google/android/location/copresence/r/w;-><init>(Lcom/google/android/location/copresence/r/p;)V

    iput-object v0, p0, Lcom/google/android/location/copresence/r/p;->j:Landroid/net/wifi/p2p/WifiP2pManager$ActionListener;

    .line 384
    new-instance v0, Lcom/google/android/location/copresence/r/x;

    const-string v1, "Unmodified"

    const/4 v2, 0x2

    new-array v2, v2, [Lcom/google/android/location/copresence/k/f;

    iget-object v3, p0, Lcom/google/android/location/copresence/r/p;->b:Lcom/google/android/location/copresence/r/ad;

    aput-object v3, v2, v4

    iget-object v3, p0, Lcom/google/android/location/copresence/r/p;->i:Lcom/google/android/location/copresence/r/ad;

    aput-object v3, v2, v5

    invoke-direct {v0, p0, v1, v2}, Lcom/google/android/location/copresence/r/x;-><init>(Lcom/google/android/location/copresence/r/p;Ljava/lang/String;[Lcom/google/android/location/copresence/k/f;)V

    iput-object v0, p0, Lcom/google/android/location/copresence/r/p;->c:Lcom/google/android/location/copresence/r/ad;

    .line 59
    iput-object p1, p0, Lcom/google/android/location/copresence/r/p;->d:Landroid/content/Context;

    .line 60
    iput-object p2, p0, Lcom/google/android/location/copresence/r/p;->e:Lcom/google/android/location/copresence/r/ah;

    .line 61
    iput-object p3, p0, Lcom/google/android/location/copresence/r/p;->f:Lcom/google/android/location/copresence/k/a;

    .line 62
    iput-object p4, p0, Lcom/google/android/location/copresence/r/p;->g:Landroid/content/SharedPreferences;

    .line 63
    return-void
.end method

.method static synthetic a(Lcom/google/android/location/copresence/r/p;)Lcom/google/android/location/copresence/r/ad;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lcom/google/android/location/copresence/r/p;->h:Lcom/google/android/location/copresence/r/ad;

    return-object v0
.end method

.method static synthetic a(Landroid/content/SharedPreferences$Editor;Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 39
    invoke-static {p0, p1}, Lcom/google/android/location/copresence/r/p;->b(Landroid/content/SharedPreferences$Editor;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method static synthetic b(Lcom/google/android/location/copresence/r/p;)Lcom/google/android/location/copresence/r/ah;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lcom/google/android/location/copresence/r/p;->e:Lcom/google/android/location/copresence/r/ah;

    return-object v0
.end method

.method private static b(Landroid/content/SharedPreferences$Editor;Ljava/lang/String;)Z
    .locals 4

    .prologue
    .line 404
    const-string v0, "updated"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-interface {p0, v0, v2, v3}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    move-result v0

    .line 405
    if-eqz v0, :cond_1

    .line 406
    const/4 v1, 0x3

    invoke-static {v1}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 407
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "WifiDirectStates2: Completed store to disk and "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/location/copresence/ag;->b(Ljava/lang/String;)I

    .line 414
    :cond_0
    :goto_0
    return v0

    .line 410
    :cond_1
    const/4 v1, 0x6

    invoke-static {v1}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 411
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "WifiDirectStates2: Failed to store to disk and "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/location/copresence/ag;->e(Ljava/lang/String;)I

    goto :goto_0
.end method

.method static synthetic c(Lcom/google/android/location/copresence/r/p;)Landroid/net/wifi/p2p/WifiP2pManager$ActionListener;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lcom/google/android/location/copresence/r/p;->j:Landroid/net/wifi/p2p/WifiP2pManager$ActionListener;

    return-object v0
.end method

.method static synthetic d(Lcom/google/android/location/copresence/r/p;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 39
    iget-object v0, p0, Lcom/google/android/location/copresence/r/p;->g:Landroid/content/SharedPreferences;

    const-string v1, "deviceName"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic e(Lcom/google/android/location/copresence/r/p;)Landroid/content/SharedPreferences;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lcom/google/android/location/copresence/r/p;->g:Landroid/content/SharedPreferences;

    return-object v0
.end method

.method static synthetic f(Lcom/google/android/location/copresence/r/p;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lcom/google/android/location/copresence/r/p;->d:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic g(Lcom/google/android/location/copresence/r/p;)Lcom/google/android/location/copresence/k/a;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lcom/google/android/location/copresence/r/p;->f:Lcom/google/android/location/copresence/k/a;

    return-object v0
.end method

.method static synthetic h(Lcom/google/android/location/copresence/r/p;)Lcom/google/android/location/copresence/r/ad;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lcom/google/android/location/copresence/r/p;->i:Lcom/google/android/location/copresence/r/ad;

    return-object v0
.end method

.method static synthetic i(Lcom/google/android/location/copresence/r/p;)V
    .locals 3

    .prologue
    .line 39
    iget-object v0, p0, Lcom/google/android/location/copresence/r/p;->g:Landroid/content/SharedPreferences;

    const-string v1, "groupInterface"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    const/4 v1, 0x3

    invoke-static {v1}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "WifiDirectStates2: Ensuring unconnectable group is removed"

    invoke-static {v1}, Lcom/google/android/location/copresence/ag;->b(Ljava/lang/String;)I

    :cond_0
    iget-object v1, p0, Lcom/google/android/location/copresence/r/p;->e:Lcom/google/android/location/copresence/r/ah;

    invoke-virtual {v1, v0}, Lcom/google/android/location/copresence/r/ah;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/location/copresence/r/p;->g:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "groupInterface"

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "removed stored interface"

    invoke-static {v0, v1}, Lcom/google/android/location/copresence/r/p;->b(Landroid/content/SharedPreferences$Editor;Ljava/lang/String;)Z

    :cond_1
    return-void
.end method
