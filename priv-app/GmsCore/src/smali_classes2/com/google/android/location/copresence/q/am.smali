.class public final Lcom/google/android/location/copresence/q/am;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final a:Landroid/os/Handler;

.field final b:Lcom/google/android/location/copresence/q/aq;

.field final c:Ljava/lang/Runnable;

.field final d:Ljava/lang/Object;

.field e:Z

.field f:Z

.field private g:Z


# direct methods
.method public constructor <init>(Lcom/google/android/location/copresence/q/aq;Landroid/os/Handler;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 54
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 55
    invoke-static {p2}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Handler;

    iput-object v0, p0, Lcom/google/android/location/copresence/q/am;->a:Landroid/os/Handler;

    .line 56
    invoke-static {p1}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/copresence/q/aq;

    iput-object v0, p0, Lcom/google/android/location/copresence/q/am;->b:Lcom/google/android/location/copresence/q/aq;

    .line 57
    new-instance v0, Lcom/google/android/location/copresence/q/an;

    invoke-direct {v0, p0}, Lcom/google/android/location/copresence/q/an;-><init>(Lcom/google/android/location/copresence/q/am;)V

    iput-object v0, p0, Lcom/google/android/location/copresence/q/am;->c:Ljava/lang/Runnable;

    .line 63
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/location/copresence/q/am;->d:Ljava/lang/Object;

    .line 65
    iput-boolean v1, p0, Lcom/google/android/location/copresence/q/am;->e:Z

    .line 66
    iput-boolean v1, p0, Lcom/google/android/location/copresence/q/am;->f:Z

    .line 67
    iput-boolean v1, p0, Lcom/google/android/location/copresence/q/am;->g:Z

    .line 68
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    .line 85
    iget-object v1, p0, Lcom/google/android/location/copresence/q/am;->d:Ljava/lang/Object;

    monitor-enter v1

    .line 86
    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/location/copresence/q/am;->f:Z

    if-eqz v0, :cond_1

    .line 87
    iget-boolean v0, p0, Lcom/google/android/location/copresence/q/am;->g:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x6

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 88
    const-string v0, "TimeoutHandler: notifyFinished() was called more than once."

    new-instance v2, Ljava/lang/IllegalStateException;

    invoke-direct {v2}, Ljava/lang/IllegalStateException;-><init>()V

    invoke-static {v0, v2}, Lcom/google/android/location/copresence/ag;->a(Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 91
    :cond_0
    monitor-exit v1

    .line 103
    :goto_0
    return-void

    .line 93
    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/location/copresence/q/am;->f:Z

    .line 94
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/location/copresence/q/am;->g:Z

    .line 95
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 96
    iget-object v0, p0, Lcom/google/android/location/copresence/q/am;->a:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/location/copresence/q/am;->c:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 97
    iget-object v0, p0, Lcom/google/android/location/copresence/q/am;->a:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/location/copresence/q/ao;

    invoke-direct {v1, p0}, Lcom/google/android/location/copresence/q/ao;-><init>(Lcom/google/android/location/copresence/q/am;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0

    .line 95
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method
