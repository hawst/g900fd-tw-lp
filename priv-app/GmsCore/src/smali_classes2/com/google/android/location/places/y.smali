.class public final Lcom/google/android/location/places/y;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final a:Lcom/google/android/location/places/aa;

.field final b:Ljava/util/Set;

.field final c:Lcom/google/android/location/places/bv;

.field final d:Lcom/google/android/location/places/f/a;

.field final e:Lcom/google/android/location/places/a/a;

.field f:Lcom/google/android/location/places/ae;

.field g:Ljava/lang/Object;

.field h:Lcom/google/android/location/places/x;

.field final i:Lcom/google/android/location/places/ac;


# direct methods
.method public constructor <init>(Lcom/google/android/location/j/b;Lcom/google/android/location/places/af;Lcom/google/android/location/places/f/a;Lcom/google/android/location/places/bv;)V
    .locals 1

    .prologue
    .line 46
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/location/places/y;->g:Ljava/lang/Object;

    .line 155
    new-instance v0, Lcom/google/android/location/places/z;

    invoke-direct {v0, p0}, Lcom/google/android/location/places/z;-><init>(Lcom/google/android/location/places/y;)V

    iput-object v0, p0, Lcom/google/android/location/places/y;->i:Lcom/google/android/location/places/ac;

    .line 47
    iput-object p3, p0, Lcom/google/android/location/places/y;->d:Lcom/google/android/location/places/f/a;

    .line 48
    iput-object p4, p0, Lcom/google/android/location/places/y;->c:Lcom/google/android/location/places/bv;

    .line 49
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/location/places/y;->e:Lcom/google/android/location/places/a/a;

    .line 50
    new-instance v0, Lcom/google/android/location/places/aa;

    invoke-direct {v0, p1, p2}, Lcom/google/android/location/places/aa;-><init>(Lcom/google/android/location/j/b;Lcom/google/android/location/places/af;)V

    iput-object v0, p0, Lcom/google/android/location/places/y;->a:Lcom/google/android/location/places/aa;

    .line 51
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/location/places/y;->b:Ljava/util/Set;

    .line 52
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/location/e/al;)V
    .locals 2

    .prologue
    .line 94
    invoke-static {}, Lcom/google/android/location/places/p;->a()Lcom/google/android/location/o/a/c;

    move-result-object v0

    const-string v1, "New position available"

    invoke-virtual {v0, v1}, Lcom/google/android/location/o/a/c;->a(Ljava/lang/String;)V

    .line 95
    iget-object v0, p0, Lcom/google/android/location/places/y;->b:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/places/ab;

    .line 96
    invoke-virtual {v0, p1}, Lcom/google/android/location/places/ab;->a(Lcom/google/android/location/e/al;)V

    goto :goto_0

    .line 98
    :cond_0
    return-void
.end method

.method public final a(Lcom/google/android/location/e/bi;)V
    .locals 2

    .prologue
    .line 114
    invoke-static {}, Lcom/google/android/location/places/p;->a()Lcom/google/android/location/o/a/c;

    move-result-object v0

    const-string v1, "New WiFi scan available"

    invoke-virtual {v0, v1}, Lcom/google/android/location/o/a/c;->a(Ljava/lang/String;)V

    .line 115
    iget-object v0, p0, Lcom/google/android/location/places/y;->b:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/places/ab;

    .line 116
    invoke-virtual {v0, p1}, Lcom/google/android/location/places/ab;->a(Lcom/google/android/location/e/bi;)V

    goto :goto_0

    .line 118
    :cond_0
    return-void
.end method
