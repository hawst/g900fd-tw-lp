.class public final Lcom/google/android/location/copresence/c/h;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final a:Ljava/lang/String;

.field b:I

.field c:I

.field final d:Z

.field final e:Landroid/os/Handler;

.field f:Lcom/google/android/location/copresence/c/l;

.field g:Lcom/google/android/gms/audiomodem/TokenReceiver$Params;

.field h:Lcom/google/android/gms/audiomodem/TokenBroadcaster$Params;

.field private final i:Lcom/google/android/location/copresence/c/b;

.field private final j:Lcom/google/android/gms/audiomodem/bm;

.field private final k:Lcom/google/android/gms/audiomodem/bf;

.field private final l:Lcom/google/android/location/copresence/c/q;

.field private final m:Lcom/google/android/location/copresence/c/z;

.field private final n:Lcom/google/android/location/copresence/c/y;


# direct methods
.method public constructor <init>(Landroid/content/Context;ZLjava/lang/String;)V
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 115
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 58
    iput v0, p0, Lcom/google/android/location/copresence/c/h;->b:I

    .line 67
    iput v0, p0, Lcom/google/android/location/copresence/c/h;->c:I

    .line 78
    new-instance v0, Lcom/google/android/location/copresence/c/i;

    invoke-direct {v0, p0}, Lcom/google/android/location/copresence/c/i;-><init>(Lcom/google/android/location/copresence/c/h;)V

    iput-object v0, p0, Lcom/google/android/location/copresence/c/h;->j:Lcom/google/android/gms/audiomodem/bm;

    .line 101
    new-instance v0, Lcom/google/android/location/copresence/c/k;

    invoke-direct {v0, p0}, Lcom/google/android/location/copresence/c/k;-><init>(Lcom/google/android/location/copresence/c/h;)V

    iput-object v0, p0, Lcom/google/android/location/copresence/c/h;->k:Lcom/google/android/gms/audiomodem/bf;

    .line 116
    invoke-static {p1}, Lcom/google/android/location/copresence/ap;->a(Landroid/content/Context;)Lcom/google/android/location/copresence/ap;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/location/copresence/ap;->d()Landroid/os/Handler;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/copresence/c/h;->e:Landroid/os/Handler;

    .line 117
    new-instance v0, Lcom/google/android/location/copresence/c/b;

    invoke-direct {v0, p1, p3}, Lcom/google/android/location/copresence/c/b;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/location/copresence/c/h;->i:Lcom/google/android/location/copresence/c/b;

    .line 118
    iput-boolean p2, p0, Lcom/google/android/location/copresence/c/h;->d:Z

    .line 119
    iput-object p3, p0, Lcom/google/android/location/copresence/c/h;->a:Ljava/lang/String;

    .line 121
    new-instance v0, Lcom/google/android/location/copresence/c/q;

    iget-object v1, p0, Lcom/google/android/location/copresence/c/h;->i:Lcom/google/android/location/copresence/c/b;

    iget-object v2, p0, Lcom/google/android/location/copresence/c/h;->j:Lcom/google/android/gms/audiomodem/bm;

    iget-object v3, p0, Lcom/google/android/location/copresence/c/h;->k:Lcom/google/android/gms/audiomodem/bf;

    invoke-direct {v0, p1, v1, v2, v3}, Lcom/google/android/location/copresence/c/q;-><init>(Landroid/content/Context;Lcom/google/android/location/copresence/c/b;Lcom/google/android/gms/audiomodem/bm;Lcom/google/android/gms/audiomodem/bf;)V

    iput-object v0, p0, Lcom/google/android/location/copresence/c/h;->l:Lcom/google/android/location/copresence/c/q;

    .line 123
    new-instance v0, Lcom/google/android/location/copresence/c/y;

    iget-object v1, p0, Lcom/google/android/location/copresence/c/h;->i:Lcom/google/android/location/copresence/c/b;

    iget-object v2, p0, Lcom/google/android/location/copresence/c/h;->k:Lcom/google/android/gms/audiomodem/bf;

    invoke-direct {v0, v1, v2}, Lcom/google/android/location/copresence/c/y;-><init>(Lcom/google/android/location/copresence/c/b;Lcom/google/android/gms/audiomodem/bf;)V

    iput-object v0, p0, Lcom/google/android/location/copresence/c/h;->n:Lcom/google/android/location/copresence/c/y;

    .line 125
    new-instance v0, Lcom/google/android/location/copresence/c/z;

    iget-object v1, p0, Lcom/google/android/location/copresence/c/h;->i:Lcom/google/android/location/copresence/c/b;

    iget-object v2, p0, Lcom/google/android/location/copresence/c/h;->j:Lcom/google/android/gms/audiomodem/bm;

    invoke-direct {v0, p1, v1, v2}, Lcom/google/android/location/copresence/c/z;-><init>(Landroid/content/Context;Lcom/google/android/location/copresence/c/b;Lcom/google/android/gms/audiomodem/bm;)V

    iput-object v0, p0, Lcom/google/android/location/copresence/c/h;->m:Lcom/google/android/location/copresence/c/z;

    .line 127
    return-void
.end method

.method private b()V
    .locals 3

    .prologue
    const/4 v2, 0x3

    .line 290
    iget-boolean v0, p0, Lcom/google/android/location/copresence/c/h;->d:Z

    if-eqz v0, :cond_0

    .line 291
    iget-object v0, p0, Lcom/google/android/location/copresence/c/h;->l:Lcom/google/android/location/copresence/c/q;

    iget v1, v0, Lcom/google/android/location/copresence/c/q;->a:I

    if-nez v1, :cond_1

    invoke-static {v2}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "Audio Stack: CarrierSenseManager: already stopped"

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->b(Ljava/lang/String;)I

    .line 293
    :cond_0
    :goto_0
    return-void

    .line 291
    :cond_1
    invoke-static {v2}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v1

    if-eqz v1, :cond_2

    const-string v1, "Audio Stack: CarrierSenseManager: stopping"

    invoke-static {v1}, Lcom/google/android/location/copresence/ag;->b(Ljava/lang/String;)I

    :cond_2
    iget-object v1, v0, Lcom/google/android/location/copresence/c/q;->b:Landroid/os/Handler;

    iget-object v2, v0, Lcom/google/android/location/copresence/c/q;->h:Ljava/lang/Runnable;

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    iget-object v1, v0, Lcom/google/android/location/copresence/c/q;->b:Landroid/os/Handler;

    iget-object v2, v0, Lcom/google/android/location/copresence/c/q;->i:Ljava/lang/Runnable;

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    iget-object v1, v0, Lcom/google/android/location/copresence/c/q;->g:Lcom/google/android/location/copresence/c/b;

    iget-object v2, v0, Lcom/google/android/location/copresence/c/q;->d:Lcom/google/android/gms/audiomodem/bf;

    invoke-virtual {v1, v2}, Lcom/google/android/location/copresence/c/b;->a(Lcom/google/android/gms/audiomodem/bf;)V

    iget-object v1, v0, Lcom/google/android/location/copresence/c/q;->g:Lcom/google/android/location/copresence/c/b;

    iget-object v2, v0, Lcom/google/android/location/copresence/c/q;->c:Lcom/google/android/gms/audiomodem/bm;

    invoke-virtual {v1, v2}, Lcom/google/android/location/copresence/c/b;->a(Lcom/google/android/gms/audiomodem/bm;)V

    const/4 v1, 0x0

    iput v1, v0, Lcom/google/android/location/copresence/c/q;->a:I

    goto :goto_0
.end method


# virtual methods
.method final a()V
    .locals 4

    .prologue
    const/4 v3, 0x3

    const/4 v2, 0x0

    .line 228
    invoke-static {v3}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 229
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Audio Stack: AudioStateMachine: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/location/copresence/c/h;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "Requesting to stop listening for tokens."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->b(Ljava/lang/String;)I

    .line 231
    :cond_0
    iput v2, p0, Lcom/google/android/location/copresence/c/h;->b:I

    .line 232
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/location/copresence/c/h;->g:Lcom/google/android/gms/audiomodem/TokenReceiver$Params;

    .line 233
    iget v0, p0, Lcom/google/android/location/copresence/c/h;->c:I

    packed-switch v0, :pswitch_data_0

    .line 247
    :cond_1
    :goto_0
    iget v0, p0, Lcom/google/android/location/copresence/c/h;->c:I

    invoke-virtual {p0, v0}, Lcom/google/android/location/copresence/c/h;->a(I)V

    .line 248
    iget-object v0, p0, Lcom/google/android/location/copresence/c/h;->f:Lcom/google/android/location/copresence/c/l;

    if-eqz v0, :cond_2

    .line 249
    iget-object v0, p0, Lcom/google/android/location/copresence/c/h;->f:Lcom/google/android/location/copresence/c/l;

    invoke-interface {v0}, Lcom/google/android/location/copresence/c/l;->a()V

    .line 251
    :cond_2
    return-void

    .line 235
    :pswitch_0
    const/4 v0, 0x2

    iput v0, p0, Lcom/google/android/location/copresence/c/h;->c:I

    goto :goto_0

    .line 238
    :pswitch_1
    iput v2, p0, Lcom/google/android/location/copresence/c/h;->c:I

    goto :goto_0

    .line 242
    :pswitch_2
    invoke-static {v3}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 243
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Audio Stack: AudioStateMachine: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/location/copresence/c/h;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "Remaining in state "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/location/copresence/c/h;->c:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->b(Ljava/lang/String;)I

    goto :goto_0

    .line 233
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method final a(I)V
    .locals 10

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    const/4 v7, 0x3

    .line 254
    invoke-static {v7}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 255
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Audio Stack: AudioStateMachine: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/location/copresence/c/h;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "Updating to state "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->b(Ljava/lang/String;)I

    .line 257
    :cond_0
    packed-switch p1, :pswitch_data_0

    .line 287
    :cond_1
    :goto_0
    return-void

    .line 259
    :pswitch_0
    invoke-direct {p0}, Lcom/google/android/location/copresence/c/h;->b()V

    .line 260
    iget-object v0, p0, Lcom/google/android/location/copresence/c/h;->n:Lcom/google/android/location/copresence/c/y;

    invoke-virtual {v0}, Lcom/google/android/location/copresence/c/y;->a()V

    .line 261
    iget-object v0, p0, Lcom/google/android/location/copresence/c/h;->m:Lcom/google/android/location/copresence/c/z;

    invoke-virtual {v0}, Lcom/google/android/location/copresence/c/z;->a()V

    .line 262
    iget-object v0, p0, Lcom/google/android/location/copresence/c/h;->i:Lcom/google/android/location/copresence/c/b;

    invoke-static {v7}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v1

    if-eqz v1, :cond_2

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Audio Stack: AudioModemHelper: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, v0, Lcom/google/android/location/copresence/c/b;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "disconnecting client"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/location/copresence/ag;->b(Ljava/lang/String;)I

    :cond_2
    iget-object v0, v0, Lcom/google/android/location/copresence/c/b;->b:Lcom/google/android/gms/common/api/v;

    invoke-interface {v0}, Lcom/google/android/gms/common/api/v;->d()V

    goto :goto_0

    .line 265
    :pswitch_1
    iget-object v0, p0, Lcom/google/android/location/copresence/c/h;->i:Lcom/google/android/location/copresence/c/b;

    invoke-virtual {v0}, Lcom/google/android/location/copresence/c/b;->a()V

    .line 266
    invoke-direct {p0}, Lcom/google/android/location/copresence/c/h;->b()V

    .line 267
    iget-object v0, p0, Lcom/google/android/location/copresence/c/h;->m:Lcom/google/android/location/copresence/c/z;

    invoke-virtual {v0}, Lcom/google/android/location/copresence/c/z;->a()V

    .line 268
    iget-object v0, p0, Lcom/google/android/location/copresence/c/h;->n:Lcom/google/android/location/copresence/c/y;

    iget-object v1, p0, Lcom/google/android/location/copresence/c/h;->h:Lcom/google/android/gms/audiomodem/TokenBroadcaster$Params;

    iput-object v1, v0, Lcom/google/android/location/copresence/c/y;->b:Lcom/google/android/gms/audiomodem/TokenBroadcaster$Params;

    invoke-static {v7}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v1

    if-eqz v1, :cond_3

    const-string v1, "Audio Stack: JustBroadcastManager: Entering broadcast"

    invoke-static {v1}, Lcom/google/android/location/copresence/ag;->b(Ljava/lang/String;)I

    :cond_3
    iget-object v1, v0, Lcom/google/android/location/copresence/c/y;->d:Lcom/google/android/location/copresence/c/b;

    iget-object v2, v0, Lcom/google/android/location/copresence/c/y;->a:Lcom/google/android/gms/audiomodem/bf;

    iget-object v3, v0, Lcom/google/android/location/copresence/c/y;->b:Lcom/google/android/gms/audiomodem/TokenBroadcaster$Params;

    invoke-virtual {v1, v2, v3}, Lcom/google/android/location/copresence/c/b;->a(Lcom/google/android/gms/audiomodem/bf;Lcom/google/android/gms/audiomodem/TokenBroadcaster$Params;)V

    iput-boolean v4, v0, Lcom/google/android/location/copresence/c/y;->c:Z

    goto :goto_0

    .line 271
    :pswitch_2
    iget-object v0, p0, Lcom/google/android/location/copresence/c/h;->i:Lcom/google/android/location/copresence/c/b;

    invoke-virtual {v0}, Lcom/google/android/location/copresence/c/b;->a()V

    .line 272
    invoke-direct {p0}, Lcom/google/android/location/copresence/c/h;->b()V

    .line 273
    iget-object v0, p0, Lcom/google/android/location/copresence/c/h;->n:Lcom/google/android/location/copresence/c/y;

    invoke-virtual {v0}, Lcom/google/android/location/copresence/c/y;->a()V

    .line 274
    iget-object v6, p0, Lcom/google/android/location/copresence/c/h;->m:Lcom/google/android/location/copresence/c/z;

    iget-object v0, p0, Lcom/google/android/location/copresence/c/h;->g:Lcom/google/android/gms/audiomodem/TokenReceiver$Params;

    iget v1, p0, Lcom/google/android/location/copresence/c/h;->b:I

    iget-object v2, p0, Lcom/google/android/location/copresence/c/h;->f:Lcom/google/android/location/copresence/c/l;

    iput-object v0, v6, Lcom/google/android/location/copresence/c/z;->d:Lcom/google/android/gms/audiomodem/TokenReceiver$Params;

    iput-object v2, v6, Lcom/google/android/location/copresence/c/z;->f:Lcom/google/android/location/copresence/c/l;

    const/4 v0, 0x2

    if-ne v1, v0, :cond_6

    iget v0, v6, Lcom/google/android/location/copresence/c/z;->a:I

    if-ne v0, v4, :cond_6

    invoke-static {v7}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v0

    if-eqz v0, :cond_4

    const-string v0, "Audio Stack: JustListenManager: continuous listening already in progress, ignoring request to listen while active broadcaster"

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->b(Ljava/lang/String;)I

    :cond_4
    :goto_1
    iget v0, v6, Lcom/google/android/location/copresence/c/z;->a:I

    packed-switch v0, :pswitch_data_1

    goto/16 :goto_0

    :pswitch_3
    invoke-static {v7}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v0

    if-eqz v0, :cond_5

    const-string v0, "Audio Stack: JustListenManager: Entering listenContinuously"

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->b(Ljava/lang/String;)I

    :cond_5
    iget-object v0, v6, Lcom/google/android/location/copresence/c/z;->e:Lcom/google/android/location/copresence/c/b;

    iget-object v1, v6, Lcom/google/android/location/copresence/c/z;->c:Lcom/google/android/gms/audiomodem/bm;

    iget-object v2, v6, Lcom/google/android/location/copresence/c/z;->d:Lcom/google/android/gms/audiomodem/TokenReceiver$Params;

    const-wide/16 v4, -0x1

    const/4 v6, 0x0

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/location/copresence/c/b;->a(Lcom/google/android/gms/audiomodem/bm;Lcom/google/android/gms/audiomodem/TokenReceiver$Params;ZJLcom/google/android/gms/audiomodem/bc;)V

    invoke-static {v7}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "Audio Stack: JustListenManager: state: listen continuously"

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->b(Ljava/lang/String;)I

    goto/16 :goto_0

    :cond_6
    iput v1, v6, Lcom/google/android/location/copresence/c/z;->a:I

    goto :goto_1

    :pswitch_4
    invoke-static {v7}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v0

    if-eqz v0, :cond_7

    const-string v0, "Audio Stack: JustListenManager: Entering listenWhileActiveBroadcaster"

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->b(Ljava/lang/String;)I

    :cond_7
    invoke-static {}, Lcom/google/android/location/copresence/f/a;->f()Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    long-to-double v0, v0

    invoke-static {}, Lcom/google/android/location/copresence/f/a;->f()Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    long-to-double v4, v4

    invoke-static {}, Ljava/lang/Math;->random()D

    move-result-wide v8

    mul-double/2addr v4, v8

    add-double/2addr v0, v4

    double-to-long v4, v0

    iget-object v0, v6, Lcom/google/android/location/copresence/c/z;->e:Lcom/google/android/location/copresence/c/b;

    iget-object v1, v6, Lcom/google/android/location/copresence/c/z;->c:Lcom/google/android/gms/audiomodem/bm;

    iget-object v2, v6, Lcom/google/android/location/copresence/c/z;->d:Lcom/google/android/gms/audiomodem/TokenReceiver$Params;

    iget-object v6, v6, Lcom/google/android/location/copresence/c/z;->g:Lcom/google/android/gms/audiomodem/bc;

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/location/copresence/c/b;->a(Lcom/google/android/gms/audiomodem/bm;Lcom/google/android/gms/audiomodem/TokenReceiver$Params;ZJLcom/google/android/gms/audiomodem/bc;)V

    invoke-static {v7}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "Audio Stack: JustListenManager: state: listen while active broadcaster"

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->b(Ljava/lang/String;)I

    goto/16 :goto_0

    .line 278
    :pswitch_5
    iget-object v0, p0, Lcom/google/android/location/copresence/c/h;->i:Lcom/google/android/location/copresence/c/b;

    invoke-virtual {v0}, Lcom/google/android/location/copresence/c/b;->a()V

    .line 281
    iget-object v0, p0, Lcom/google/android/location/copresence/c/h;->m:Lcom/google/android/location/copresence/c/z;

    iput v3, v0, Lcom/google/android/location/copresence/c/z;->a:I

    .line 282
    iget-object v0, p0, Lcom/google/android/location/copresence/c/h;->n:Lcom/google/android/location/copresence/c/y;

    invoke-virtual {v0}, Lcom/google/android/location/copresence/c/y;->a()V

    .line 283
    iget-object v0, p0, Lcom/google/android/location/copresence/c/h;->l:Lcom/google/android/location/copresence/c/q;

    iget-object v1, p0, Lcom/google/android/location/copresence/c/h;->g:Lcom/google/android/gms/audiomodem/TokenReceiver$Params;

    iget-object v2, p0, Lcom/google/android/location/copresence/c/h;->h:Lcom/google/android/gms/audiomodem/TokenBroadcaster$Params;

    iput-object v1, v0, Lcom/google/android/location/copresence/c/q;->f:Lcom/google/android/gms/audiomodem/TokenReceiver$Params;

    iput-object v2, v0, Lcom/google/android/location/copresence/c/q;->e:Lcom/google/android/gms/audiomodem/TokenBroadcaster$Params;

    iget v1, v0, Lcom/google/android/location/copresence/c/q;->a:I

    if-nez v1, :cond_1

    invoke-virtual {v0}, Lcom/google/android/location/copresence/c/q;->a()V

    goto/16 :goto_0

    .line 257
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_5
        :pswitch_1
        :pswitch_2
    .end packed-switch

    .line 274
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method
