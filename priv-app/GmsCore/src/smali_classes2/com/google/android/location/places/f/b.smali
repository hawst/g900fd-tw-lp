.class final Lcom/google/android/location/places/f/b;
.super Lcom/google/android/location/places/d;
.source "SourceFile"


# instance fields
.field final synthetic b:Lcom/google/android/location/places/f/a;

.field private final c:Lcom/google/android/location/e/bi;


# direct methods
.method public constructor <init>(Lcom/google/android/location/places/f/a;Lcom/google/android/location/e/bi;)V
    .locals 0

    .prologue
    .line 66
    iput-object p1, p0, Lcom/google/android/location/places/f/b;->b:Lcom/google/android/location/places/f/a;

    invoke-direct {p0, p1}, Lcom/google/android/location/places/d;-><init>(Lcom/google/android/location/places/c;)V

    .line 67
    iput-object p2, p0, Lcom/google/android/location/places/f/b;->c:Lcom/google/android/location/e/bi;

    .line 68
    return-void
.end method


# virtual methods
.method public final run()V
    .locals 6

    .prologue
    .line 72
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 73
    const/4 v0, 0x0

    :goto_0
    iget-object v2, p0, Lcom/google/android/location/places/f/b;->c:Lcom/google/android/location/e/bi;

    invoke-virtual {v2}, Lcom/google/android/location/e/bi;->a()I

    move-result v2

    if-ge v0, v2, :cond_0

    .line 74
    iget-object v2, p0, Lcom/google/android/location/places/f/b;->c:Lcom/google/android/location/e/bi;

    invoke-virtual {v2, v0}, Lcom/google/android/location/e/bi;->a(I)Lcom/google/android/location/e/bc;

    move-result-object v2

    .line 75
    iget-wide v4, v2, Lcom/google/android/location/e/bc;->b:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    iget v2, v2, Lcom/google/android/location/e/bc;->d:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v3, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 73
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 79
    :cond_0
    iget-object v0, p0, Lcom/google/android/location/places/f/b;->b:Lcom/google/android/location/places/f/a;

    iget-object v0, v0, Lcom/google/android/location/places/f/a;->e:Lcom/google/android/location/g/q;

    invoke-virtual {v0, v1}, Lcom/google/android/location/g/q;->a(Ljava/util/Map;)Lcom/google/android/location/g/r;

    move-result-object v0

    .line 81
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lcom/google/android/location/g/r;->a()Ljava/lang/String;

    move-result-object v0

    .line 83
    :goto_1
    if-nez v0, :cond_3

    .line 84
    invoke-static {}, Lcom/google/android/location/places/p;->a()Lcom/google/android/location/o/a/c;

    move-result-object v0

    const-string v1, "No models for current WiFI data."

    invoke-virtual {v0, v1}, Lcom/google/android/location/o/a/c;->b(Ljava/lang/String;)V

    .line 99
    :cond_1
    :goto_2
    return-void

    .line 81
    :cond_2
    const/4 v0, 0x0

    goto :goto_1

    .line 88
    :cond_3
    iget-object v2, p0, Lcom/google/android/location/places/f/b;->b:Lcom/google/android/location/places/f/a;

    iget-object v2, v2, Lcom/google/android/location/places/f/a;->e:Lcom/google/android/location/g/q;

    invoke-virtual {v2, v0, v1}, Lcom/google/android/location/g/q;->a(Ljava/lang/String;Ljava/util/Map;)Lcom/google/android/location/e/u;

    move-result-object v0

    .line 89
    if-nez v0, :cond_4

    .line 90
    invoke-static {}, Lcom/google/android/location/places/p;->a()Lcom/google/android/location/o/a/c;

    move-result-object v0

    const-string v1, "No place in current models."

    invoke-virtual {v0, v1}, Lcom/google/android/location/o/a/c;->b(Ljava/lang/String;)V

    goto :goto_2

    .line 94
    :cond_4
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 95
    iget-object v0, v0, Lcom/google/android/location/e/u;->c:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_3
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 96
    new-instance v4, Lcom/google/android/location/places/w;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    invoke-direct {v4, v1, v0}, Lcom/google/android/location/places/w;-><init>(Ljava/lang/String;F)V

    invoke-interface {v2, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 98
    :cond_5
    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/location/places/d;->a:Lcom/google/android/location/places/c;

    iget-object v0, v0, Lcom/google/android/location/places/c;->b:Ljava/util/concurrent/Executor;

    new-instance v1, Lcom/google/android/location/places/e;

    invoke-direct {v1, p0, v2}, Lcom/google/android/location/places/e;-><init>(Lcom/google/android/location/places/d;Ljava/util/List;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    goto :goto_2
.end method
