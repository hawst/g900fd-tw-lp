.class public final Lcom/google/android/location/copresence/q/a;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static d:Lcom/google/android/location/copresence/q/a;


# instance fields
.field public final a:Lcom/google/android/location/fused/g;

.field public final b:Ljava/util/Set;

.field public final c:Lcom/google/android/gms/location/j;


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 50
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 51
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/location/copresence/q/a;->b:Ljava/util/Set;

    .line 52
    invoke-static {p1}, Lcom/google/android/location/fused/g;->a(Landroid/content/Context;)Lcom/google/android/location/fused/g;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/copresence/q/a;->a:Lcom/google/android/location/fused/g;

    .line 53
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    .line 56
    new-instance v1, Lcom/google/android/location/copresence/q/b;

    invoke-direct {v1, p0, v0}, Lcom/google/android/location/copresence/q/b;-><init>(Lcom/google/android/location/copresence/q/a;Landroid/os/Handler;)V

    iput-object v1, p0, Lcom/google/android/location/copresence/q/a;->c:Lcom/google/android/gms/location/j;

    .line 74
    return-void
.end method

.method public static declared-synchronized a(Landroid/content/Context;)Lcom/google/android/location/copresence/q/a;
    .locals 2

    .prologue
    .line 44
    const-class v1, Lcom/google/android/location/copresence/q/a;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/google/android/location/copresence/q/a;->d:Lcom/google/android/location/copresence/q/a;

    if-nez v0, :cond_0

    .line 45
    new-instance v0, Lcom/google/android/location/copresence/q/a;

    invoke-direct {v0, p0}, Lcom/google/android/location/copresence/q/a;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/google/android/location/copresence/q/a;->d:Lcom/google/android/location/copresence/q/a;

    .line 47
    :cond_0
    sget-object v0, Lcom/google/android/location/copresence/q/a;->d:Lcom/google/android/location/copresence/q/a;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 44
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static a(Landroid/location/Location;)Z
    .locals 6

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 100
    if-eqz p0, :cond_5

    invoke-virtual {p0}, Landroid/location/Location;->getAccuracy()F

    move-result v0

    float-to-double v0, v0

    invoke-static {}, Lcom/google/android/location/copresence/f/b;->b()Lcom/google/ac/b/c/g;

    move-result-object v4

    iget-object v4, v4, Lcom/google/ac/b/c/g;->l:Lcom/google/ac/b/c/j;

    iget-object v4, v4, Lcom/google/ac/b/c/j;->a:Ljava/lang/Double;

    invoke-virtual {v4}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v4

    cmpg-double v0, v0, v4

    if-gtz v0, :cond_1

    move v0, v2

    :goto_0
    if-eqz v0, :cond_5

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x11

    if-lt v0, v1, :cond_2

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtimeNanos()J

    move-result-wide v0

    invoke-virtual {p0}, Landroid/location/Location;->getElapsedRealtimeNanos()J

    move-result-wide v4

    sub-long/2addr v0, v4

    sget-object v4, Ljava/util/concurrent/TimeUnit;->NANOSECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v4, v0, v1}, Ljava/util/concurrent/TimeUnit;->toSeconds(J)J

    move-result-wide v0

    :goto_1
    invoke-static {}, Lcom/google/android/location/copresence/f/b;->b()Lcom/google/ac/b/c/g;

    move-result-object v4

    iget-object v4, v4, Lcom/google/ac/b/c/g;->l:Lcom/google/ac/b/c/j;

    iget-object v4, v4, Lcom/google/ac/b/c/j;->b:Ljava/lang/Long;

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    cmp-long v0, v0, v4

    if-gtz v0, :cond_3

    move v0, v2

    :goto_2
    if-eqz v0, :cond_5

    invoke-virtual {p0}, Landroid/location/Location;->hasSpeed()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Landroid/location/Location;->getSpeed()F

    move-result v0

    float-to-double v0, v0

    invoke-static {}, Lcom/google/android/location/copresence/f/b;->b()Lcom/google/ac/b/c/g;

    move-result-object v4

    iget-object v4, v4, Lcom/google/ac/b/c/g;->l:Lcom/google/ac/b/c/j;

    iget-object v4, v4, Lcom/google/ac/b/c/j;->c:Ljava/lang/Double;

    invoke-virtual {v4}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v4

    cmpg-double v0, v0, v4

    if-gtz v0, :cond_4

    :cond_0
    move v0, v2

    :goto_3
    if-eqz v0, :cond_5

    move v0, v2

    :goto_4
    return v0

    :cond_1
    move v0, v3

    goto :goto_0

    :cond_2
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    invoke-virtual {p0}, Landroid/location/Location;->getTime()J

    move-result-wide v4

    sub-long/2addr v0, v4

    sget-object v4, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v4, v0, v1}, Ljava/util/concurrent/TimeUnit;->toSeconds(J)J

    move-result-wide v0

    goto :goto_1

    :cond_3
    move v0, v3

    goto :goto_2

    :cond_4
    move v0, v3

    goto :goto_3

    :cond_5
    move v0, v3

    goto :goto_4
.end method
