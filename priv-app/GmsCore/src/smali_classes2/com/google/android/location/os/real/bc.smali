.class final Lcom/google/android/location/os/real/bc;
.super Lcom/google/android/location/e/bi;
.source "SourceFile"


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x11
.end annotation


# direct methods
.method private constructor <init>(JLjava/util/ArrayList;)V
    .locals 1

    .prologue
    .line 124
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/location/e/bi;-><init>(JLjava/util/ArrayList;)V

    .line 125
    return-void
.end method

.method private static a(JLjava/util/List;)Lcom/google/android/location/e/bi;
    .locals 12

    .prologue
    .line 92
    new-instance v9, Ljava/util/ArrayList;

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v1

    invoke-direct {v9, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 96
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :cond_0
    :goto_0
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    move-object v6, v1

    check-cast v6, Landroid/net/wifi/ScanResult;

    .line 97
    if-eqz v6, :cond_0

    .line 98
    iget-object v1, v6, Landroid/net/wifi/ScanResult;->BSSID:Ljava/lang/String;

    invoke-static {v1}, Lcom/google/android/location/e/bj;->a(Ljava/lang/String;)J

    move-result-wide v2

    .line 100
    invoke-static {v2, v3, v6}, Lcom/google/android/location/os/real/bc;->a(JLandroid/net/wifi/ScanResult;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 101
    new-instance v1, Lcom/google/android/location/e/bc;

    iget v4, v6, Landroid/net/wifi/ScanResult;->level:I

    iget-object v5, v6, Landroid/net/wifi/ScanResult;->SSID:Ljava/lang/String;

    iget v6, v6, Landroid/net/wifi/ScanResult;->frequency:I

    int-to-short v6, v6

    move-wide v7, p0

    invoke-direct/range {v1 .. v8}, Lcom/google/android/location/e/bc;-><init>(JILjava/lang/String;SJ)V

    invoke-virtual {v9, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 106
    :cond_1
    new-instance v1, Lcom/google/android/location/os/real/bc;

    invoke-direct {v1, p0, p1, v9}, Lcom/google/android/location/os/real/bc;-><init>(JLjava/util/ArrayList;)V

    return-object v1
.end method

.method private static a(JLandroid/net/wifi/ScanResult;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 117
    iget-object v2, p2, Landroid/net/wifi/ScanResult;->capabilities:Ljava/lang/String;

    if-eqz v2, :cond_0

    iget-object v2, p2, Landroid/net/wifi/ScanResult;->capabilities:Ljava/lang/String;

    const-string v3, "[IBSS]"

    invoke-virtual {v2, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    move v2, v0

    .line 120
    :goto_0
    const-wide v4, 0xffffffffffffL

    cmp-long v3, p0, v4

    if-eqz v3, :cond_1

    if-nez v2, :cond_1

    :goto_1
    return v0

    :cond_0
    move v2, v1

    .line 117
    goto :goto_0

    :cond_1
    move v0, v1

    .line 120
    goto :goto_1
.end method

.method public static a(JLcom/google/android/location/os/real/bi;Ljava/util/List;)[Lcom/google/android/location/e/bi;
    .locals 12

    .prologue
    .line 32
    move-object v0, p3

    move-wide v2, p0

    :goto_0
    iget v1, p2, Lcom/google/android/location/os/real/bi;->a:I

    packed-switch v1, :pswitch_data_0

    const/4 v0, 0x0

    .line 33
    :goto_1
    if-eqz v0, :cond_c

    const-wide/high16 v0, -0x8000000000000000L

    new-instance v2, Ljava/util/ArrayList;

    invoke-interface {p3}, Ljava/util/List;->size()I

    move-result v3

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(I)V

    invoke-interface {p3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move-wide v8, v0

    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/wifi/ScanResult;

    if-eqz v0, :cond_0

    iget-object v1, v0, Landroid/net/wifi/ScanResult;->BSSID:Ljava/lang/String;

    invoke-static {v1}, Lcom/google/android/location/e/bj;->a(Ljava/lang/String;)J

    move-result-wide v4

    invoke-static {v4, v5, v0}, Lcom/google/android/location/os/real/bc;->a(JLandroid/net/wifi/ScanResult;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-wide v4, v0, Landroid/net/wifi/ScanResult;->timestamp:J

    const-wide/16 v6, 0x3e8

    div-long/2addr v4, v6

    invoke-static {v4, v5, v8, v9}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v8

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    move-wide v0, v8

    move-wide v8, v0

    goto :goto_2

    .line 32
    :pswitch_0
    if-eqz v0, :cond_1

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    if-nez v1, :cond_2

    :cond_1
    const/4 v0, 0x1

    goto :goto_1

    :cond_2
    iget v1, p2, Lcom/google/android/location/os/real/bi;->a:I

    if-nez v1, :cond_5

    if-eqz v0, :cond_5

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_5

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_3
    :goto_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/wifi/ScanResult;

    if-eqz v0, :cond_3

    iget-object v4, p2, Lcom/google/android/location/os/real/bi;->c:Lcom/google/android/location/os/real/bf;

    invoke-virtual {v4, v0}, Lcom/google/android/location/os/real/bf;->a(Landroid/net/wifi/ScanResult;)J

    move-result-wide v4

    const-wide/16 v6, 0x0

    cmp-long v0, v4, v6

    if-eqz v0, :cond_4

    const-wide/16 v6, -0x1

    cmp-long v0, v4, v6

    if-nez v0, :cond_6

    :cond_4
    const/4 v0, 0x1

    iput v0, p2, Lcom/google/android/location/os/real/bi;->a:I

    :cond_5
    :goto_4
    const/4 v0, 0x0

    const-wide/16 v2, 0x0

    goto/16 :goto_0

    :cond_6
    sub-long v4, v2, v4

    invoke-static {v4, v5}, Ljava/lang/Math;->abs(J)J

    move-result-wide v4

    const-wide/32 v6, 0x36ee80

    cmp-long v0, v4, v6

    if-lez v0, :cond_7

    const/4 v0, 0x1

    iput v0, p2, Lcom/google/android/location/os/real/bi;->a:I

    goto :goto_4

    :cond_7
    iget v0, p2, Lcom/google/android/location/os/real/bi;->b:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p2, Lcom/google/android/location/os/real/bi;->b:I

    const/4 v4, 0x6

    if-le v0, v4, :cond_3

    const/4 v0, 0x2

    iput v0, p2, Lcom/google/android/location/os/real/bi;->a:I

    goto :goto_3

    :pswitch_1
    const/4 v0, 0x0

    goto/16 :goto_1

    :pswitch_2
    const/4 v0, 0x1

    goto/16 :goto_1

    .line 33
    :cond_8
    new-instance v10, Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v0

    invoke-direct {v10, v0}, Ljava/util/ArrayList;-><init>(I)V

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v11

    :cond_9
    :goto_5
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_b

    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Landroid/net/wifi/ScanResult;

    iget-wide v0, v5, Landroid/net/wifi/ScanResult;->timestamp:J

    const-wide/16 v2, 0x3e8

    div-long v6, v0, v2

    sub-long v0, v8, v6

    const-wide/16 v2, 0x7530

    cmp-long v0, v0, v2

    if-gtz v0, :cond_a

    iget-object v0, v5, Landroid/net/wifi/ScanResult;->BSSID:Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/location/e/bj;->a(Ljava/lang/String;)J

    move-result-wide v1

    new-instance v0, Lcom/google/android/location/e/bc;

    iget v3, v5, Landroid/net/wifi/ScanResult;->level:I

    iget-object v4, v5, Landroid/net/wifi/ScanResult;->SSID:Ljava/lang/String;

    iget v5, v5, Landroid/net/wifi/ScanResult;->frequency:I

    int-to-short v5, v5

    invoke-direct/range {v0 .. v7}, Lcom/google/android/location/e/bc;-><init>(JILjava/lang/String;SJ)V

    invoke-virtual {v10, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_5

    :cond_a
    sget-boolean v0, Lcom/google/android/location/i/a;->b:Z

    if-eqz v0, :cond_9

    const-string v0, "RealWifiScan"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Stale scan: scanTimestamp is "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " and device timestamp is "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " and diff is "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sub-long v2, v8, v6

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/location/o/a/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_5

    :cond_b
    new-instance v0, Lcom/google/android/location/os/real/bc;

    invoke-direct {v0, v8, v9, v10}, Lcom/google/android/location/os/real/bc;-><init>(JLjava/util/ArrayList;)V

    .line 35
    :goto_6
    const/4 v1, 0x1

    new-array v1, v1, [Lcom/google/android/location/e/bi;

    const/4 v2, 0x0

    aput-object v0, v1, v2

    return-object v1

    .line 33
    :cond_c
    invoke-static {p0, p1, p3}, Lcom/google/android/location/os/real/bc;->a(JLjava/util/List;)Lcom/google/android/location/e/bi;

    move-result-object v0

    goto :goto_6

    .line 32
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
