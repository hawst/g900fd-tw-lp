.class final Lcom/google/android/location/copresence/d/a/c;
.super Landroid/bluetooth/le/AdvertiseCallback;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/location/copresence/d/a/a;

.field private final b:Ljava/util/concurrent/CountDownLatch;

.field private c:Z


# direct methods
.method public constructor <init>(Lcom/google/android/location/copresence/d/a/a;)V
    .locals 2

    .prologue
    .line 170
    iput-object p1, p0, Lcom/google/android/location/copresence/d/a/c;->a:Lcom/google/android/location/copresence/d/a/a;

    invoke-direct {p0}, Landroid/bluetooth/le/AdvertiseCallback;-><init>()V

    .line 171
    new-instance v0, Ljava/util/concurrent/CountDownLatch;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/location/copresence/d/a/c;->b:Ljava/util/concurrent/CountDownLatch;

    .line 172
    return-void
.end method


# virtual methods
.method public final a(J)I
    .locals 3

    .prologue
    .line 176
    :try_start_0
    iget-object v0, p0, Lcom/google/android/location/copresence/d/a/c;->b:Ljava/util/concurrent/CountDownLatch;

    sget-object v1, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, p1, p2, v1}, Ljava/util/concurrent/CountDownLatch;->await(JLjava/util/concurrent/TimeUnit;)Z

    move-result v0

    .line 177
    if-eqz v0, :cond_1

    .line 178
    iget-boolean v0, p0, Lcom/google/android/location/copresence/d/a/c;->c:Z
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    .line 185
    :goto_0
    return v0

    .line 178
    :cond_0
    const/4 v0, 0x1

    goto :goto_0

    .line 181
    :catch_0
    move-exception v0

    const/4 v0, 0x5

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 182
    const-string v0, "BleAdvertiseImplL: timed out waiting for advertising to start"

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->d(Ljava/lang/String;)I

    .line 185
    :cond_1
    const/4 v0, 0x2

    goto :goto_0
.end method

.method public final onStartFailure(I)V
    .locals 1

    .prologue
    .line 190
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/location/copresence/d/a/c;->c:Z

    .line 191
    iget-object v0, p0, Lcom/google/android/location/copresence/d/a/c;->b:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    .line 192
    return-void
.end method

.method public final onStartSuccess(Landroid/bluetooth/le/AdvertiseSettings;)V
    .locals 1

    .prologue
    .line 196
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/location/copresence/d/a/c;->c:Z

    .line 197
    iget-object v0, p0, Lcom/google/android/location/copresence/d/a/c;->b:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    .line 198
    return-void
.end method
