.class final Lcom/google/android/location/copresence/r/a;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/location/copresence/w;


# instance fields
.field private final b:Landroid/content/Context;

.field private final c:Lcom/google/android/location/copresence/r/ae;

.field private final d:Ljava/util/Random;

.field private final e:Landroid/os/PowerManager;

.field private final f:Landroid/content/SharedPreferences;

.field private final g:Landroid/os/Handler;

.field private final h:Landroid/content/BroadcastReceiver;

.field private i:Lcom/google/android/gms/location/copresence/x;


# direct methods
.method constructor <init>(Landroid/content/Context;Lcom/google/android/location/copresence/r/ae;)V
    .locals 3

    .prologue
    .line 67
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 54
    new-instance v0, Lcom/google/android/location/copresence/r/b;

    invoke-direct {v0, p0}, Lcom/google/android/location/copresence/r/b;-><init>(Lcom/google/android/location/copresence/r/a;)V

    iput-object v0, p0, Lcom/google/android/location/copresence/r/a;->h:Landroid/content/BroadcastReceiver;

    .line 68
    iput-object p1, p0, Lcom/google/android/location/copresence/r/a;->b:Landroid/content/Context;

    .line 69
    iput-object p2, p0, Lcom/google/android/location/copresence/r/a;->c:Lcom/google/android/location/copresence/r/ae;

    .line 70
    const-string v0, "power"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    iput-object v0, p0, Lcom/google/android/location/copresence/r/a;->e:Landroid/os/PowerManager;

    .line 71
    iget-object v0, p0, Lcom/google/android/location/copresence/r/a;->b:Landroid/content/Context;

    const-string v1, "copresence_user_wifi_ap_state"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/copresence/r/a;->f:Landroid/content/SharedPreferences;

    .line 72
    new-instance v0, Ljava/util/Random;

    invoke-direct {v0}, Ljava/util/Random;-><init>()V

    iput-object v0, p0, Lcom/google/android/location/copresence/r/a;->d:Ljava/util/Random;

    .line 73
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/google/android/location/copresence/r/a;->g:Landroid/os/Handler;

    .line 74
    invoke-direct {p0}, Lcom/google/android/location/copresence/r/a;->d()Z

    move-result v0

    if-nez v0, :cond_0

    .line 75
    const/4 v0, 0x6

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 76
    const-string v0, "WifiApBeacon: Attempted to revert wifi config and failed!"

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->e(Ljava/lang/String;)I

    .line 79
    :cond_0
    return-void
.end method

.method private c()Z
    .locals 2

    .prologue
    .line 162
    iget-object v0, p0, Lcom/google/android/location/copresence/r/a;->f:Landroid/content/SharedPreferences;

    const-string v1, "ssid"

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/location/copresence/r/a;->f:Landroid/content/SharedPreferences;

    const-string v1, "shared_key"

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private d()Z
    .locals 9

    .prologue
    const/4 v8, 0x0

    const/4 v7, 0x6

    const/4 v6, 0x3

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 190
    invoke-direct {p0}, Lcom/google/android/location/copresence/r/a;->c()Z

    move-result v2

    if-nez v2, :cond_1

    .line 191
    invoke-static {v6}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 192
    const-string v1, "WifiApBeacon: User config is not saved, nothing to revert."

    invoke-static {v1}, Lcom/google/android/location/copresence/ag;->b(Ljava/lang/String;)I

    .line 230
    :cond_0
    :goto_0
    return v0

    .line 197
    :cond_1
    invoke-static {v6}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 198
    const-string v2, "WifiApBeacon: Reverting to user wifi config."

    invoke-static {v2}, Lcom/google/android/location/copresence/ag;->b(Ljava/lang/String;)I

    .line 201
    :cond_2
    iget-object v2, p0, Lcom/google/android/location/copresence/r/a;->c:Lcom/google/android/location/copresence/r/ae;

    invoke-virtual {v2}, Lcom/google/android/location/copresence/r/ae;->b()Landroid/net/wifi/WifiConfiguration;

    move-result-object v2

    .line 202
    iget-object v3, p0, Lcom/google/android/location/copresence/r/a;->f:Landroid/content/SharedPreferences;

    const-string v4, "ssid"

    invoke-interface {v3, v4, v8}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 203
    iget-object v4, p0, Lcom/google/android/location/copresence/r/a;->f:Landroid/content/SharedPreferences;

    const-string v5, "shared_key"

    invoke-interface {v4, v5, v8}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 204
    if-nez v3, :cond_4

    .line 206
    const/4 v0, 0x2

    invoke-static {v0}, Lcom/google/android/location/copresence/b/a;->b(I)V

    .line 207
    invoke-static {v7}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 208
    const-string v0, "WifiApBeacon: Wifi Ap config not correctly saved"

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->e(Ljava/lang/String;)I

    :cond_3
    move v0, v1

    .line 210
    goto :goto_0

    .line 212
    :cond_4
    iput-object v3, v2, Landroid/net/wifi/WifiConfiguration;->SSID:Ljava/lang/String;

    .line 213
    iput-object v4, v2, Landroid/net/wifi/WifiConfiguration;->preSharedKey:Ljava/lang/String;

    .line 215
    iget-object v3, p0, Lcom/google/android/location/copresence/r/a;->c:Lcom/google/android/location/copresence/r/ae;

    invoke-virtual {v3, v2, v0}, Lcom/google/android/location/copresence/r/ae;->a(Landroid/net/wifi/WifiConfiguration;Z)Z

    move-result v3

    .line 216
    iget-object v4, p0, Lcom/google/android/location/copresence/r/a;->c:Lcom/google/android/location/copresence/r/ae;

    invoke-virtual {v4, v2, v1}, Lcom/google/android/location/copresence/r/ae;->a(Landroid/net/wifi/WifiConfiguration;Z)Z

    move-result v4

    and-int/2addr v3, v4

    .line 219
    iget-object v4, p0, Lcom/google/android/location/copresence/r/a;->c:Lcom/google/android/location/copresence/r/ae;

    invoke-virtual {v4, v0}, Lcom/google/android/location/copresence/r/ae;->a(Z)Z

    move-result v0

    and-int/2addr v0, v3

    .line 221
    if-eqz v0, :cond_5

    .line 222
    iget-object v0, v2, Landroid/net/wifi/WifiConfiguration;->SSID:Ljava/lang/String;

    .line 223
    iget-object v0, p0, Lcom/google/android/location/copresence/r/a;->f:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->clear()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    move-result v0

    goto :goto_0

    .line 226
    :cond_5
    invoke-static {v7}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 227
    const-string v0, "WifiApBeacon: Wifi Ap config not correctly restored"

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->e(Ljava/lang/String;)I

    .line 229
    :cond_6
    invoke-static {v6}, Lcom/google/android/location/copresence/b/a;->b(I)V

    move v0, v1

    .line 230
    goto :goto_0
.end method


# virtual methods
.method public final a(Lcom/google/android/location/copresence/b;)V
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v5, 0x3

    .line 105
    invoke-virtual {p0}, Lcom/google/android/location/copresence/r/a;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 106
    new-instance v0, Lcom/google/android/location/copresence/y;

    invoke-direct {v0}, Lcom/google/android/location/copresence/y;-><init>()V

    throw v0

    .line 108
    :cond_0
    iget-object v0, p0, Lcom/google/android/location/copresence/r/a;->i:Lcom/google/android/gms/location/copresence/x;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/location/copresence/r/a;->i:Lcom/google/android/gms/location/copresence/x;

    iget-object v2, p1, Lcom/google/android/location/copresence/b;->a:Lcom/google/android/gms/location/copresence/x;

    invoke-virtual {v0, v2}, Lcom/google/android/gms/location/copresence/x;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_8

    .line 109
    :cond_1
    invoke-static {v5}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 110
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "WifiApBeacon: Start advertising, token="

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p1, Lcom/google/android/location/copresence/b;->a:Lcom/google/android/gms/location/copresence/x;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->b(Ljava/lang/String;)I

    .line 114
    :cond_2
    invoke-direct {p0}, Lcom/google/android/location/copresence/r/a;->c()Z

    move-result v0

    if-nez v0, :cond_6

    .line 115
    invoke-direct {p0}, Lcom/google/android/location/copresence/r/a;->c()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-static {v5}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v0

    if-eqz v0, :cond_3

    const-string v0, "WifiApBeacon: User config already saved."

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->b(Ljava/lang/String;)I

    :cond_3
    move v0, v1

    :goto_0
    if-nez v0, :cond_6

    .line 116
    new-instance v0, Lcom/google/android/location/copresence/y;

    invoke-direct {v0}, Lcom/google/android/location/copresence/y;-><init>()V

    throw v0

    .line 115
    :cond_4
    iget-object v0, p0, Lcom/google/android/location/copresence/r/a;->c:Lcom/google/android/location/copresence/r/ae;

    invoke-virtual {v0}, Lcom/google/android/location/copresence/r/ae;->b()Landroid/net/wifi/WifiConfiguration;

    move-result-object v0

    invoke-static {v5}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v2

    if-eqz v2, :cond_5

    const-string v2, "WifiApBeacon: Saving existing wifi network config."

    invoke-static {v2}, Lcom/google/android/location/copresence/ag;->b(Ljava/lang/String;)I

    :cond_5
    iget-object v2, p0, Lcom/google/android/location/copresence/r/a;->f:Landroid/content/SharedPreferences;

    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    const-string v3, "ssid"

    iget-object v4, v0, Landroid/net/wifi/WifiConfiguration;->SSID:Ljava/lang/String;

    invoke-interface {v2, v3, v4}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    const-string v3, "shared_key"

    iget-object v0, v0, Landroid/net/wifi/WifiConfiguration;->preSharedKey:Ljava/lang/String;

    invoke-interface {v2, v3, v0}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    move-result v0

    goto :goto_0

    .line 120
    :cond_6
    iget-object v0, p1, Lcom/google/android/location/copresence/b;->a:Lcom/google/android/gms/location/copresence/x;

    iput-object v0, p0, Lcom/google/android/location/copresence/r/a;->i:Lcom/google/android/gms/location/copresence/x;

    .line 122
    iget-object v0, p0, Lcom/google/android/location/copresence/r/a;->c:Lcom/google/android/location/copresence/r/ae;

    invoke-virtual {v0}, Lcom/google/android/location/copresence/r/ae;->b()Landroid/net/wifi/WifiConfiguration;

    move-result-object v0

    .line 123
    invoke-virtual {p1}, Lcom/google/android/location/copresence/b;->a()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v0, Landroid/net/wifi/WifiConfiguration;->SSID:Ljava/lang/String;

    .line 126
    const/16 v2, 0xa

    new-array v2, v2, [B

    iget-object v3, p0, Lcom/google/android/location/copresence/r/a;->d:Ljava/util/Random;

    invoke-virtual {v3, v2}, Ljava/util/Random;->nextBytes([B)V

    const/16 v3, 0xb

    invoke-static {v2, v3}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v0, Landroid/net/wifi/WifiConfiguration;->preSharedKey:Ljava/lang/String;

    .line 129
    iget-object v2, p0, Lcom/google/android/location/copresence/r/a;->c:Lcom/google/android/location/copresence/r/ae;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/google/android/location/copresence/r/ae;->a(Z)Z

    .line 130
    iget-object v2, p0, Lcom/google/android/location/copresence/r/a;->c:Lcom/google/android/location/copresence/r/ae;

    invoke-virtual {v2, v0, v1}, Lcom/google/android/location/copresence/r/ae;->a(Landroid/net/wifi/WifiConfiguration;Z)Z

    move-result v1

    .line 131
    if-eqz v1, :cond_9

    .line 132
    invoke-static {v5}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 133
    const-string v1, "E2E Advertise: step 2d) Advertise on Wifi AP started."

    invoke-static {v1}, Lcom/google/android/location/copresence/ag;->b(Ljava/lang/String;)I

    .line 135
    :cond_7
    iget-object v0, v0, Landroid/net/wifi/WifiConfiguration;->SSID:Ljava/lang/String;

    .line 136
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    const-string v1, "android.intent.action.SCREEN_OFF"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v1, "android.intent.action.SCREEN_ON"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/location/copresence/r/a;->b:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/location/copresence/r/a;->h:Landroid/content/BroadcastReceiver;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/android/location/copresence/r/a;->g:Landroid/os/Handler;

    invoke-virtual {v1, v2, v0, v3, v4}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;Ljava/lang/String;Landroid/os/Handler;)Landroid/content/Intent;

    .line 145
    :cond_8
    :goto_1
    return-void

    .line 139
    :cond_9
    const/4 v0, 0x5

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 140
    const-string v0, "WifiApBeacon: Unable to start wifi access point."

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->d(Ljava/lang/String;)I

    .line 142
    :cond_a
    invoke-direct {p0}, Lcom/google/android/location/copresence/r/a;->d()Z

    goto :goto_1
.end method

.method public final a()Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 88
    invoke-static {}, Lcom/google/android/location/copresence/f/b;->b()Lcom/google/ac/b/c/g;

    move-result-object v2

    iget-object v2, v2, Lcom/google/ac/b/c/g;->e:Lcom/google/ac/b/c/m;

    iget-object v2, v2, Lcom/google/ac/b/c/m;->k:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    .line 91
    invoke-static {}, Lcom/google/android/location/copresence/f/a;->c()Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/google/android/location/copresence/r/a;->c:Lcom/google/android/location/copresence/r/ae;

    invoke-virtual {v3}, Lcom/google/android/location/copresence/r/ae;->a()Z

    move-result v3

    if-nez v3, :cond_3

    iget-object v3, p0, Lcom/google/android/location/copresence/r/a;->c:Lcom/google/android/location/copresence/r/ae;

    iget-object v3, v3, Lcom/google/android/location/copresence/r/ae;->a:Landroid/net/wifi/WifiManager;

    invoke-virtual {v3}, Landroid/net/wifi/WifiManager;->isWifiEnabled()Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/google/android/location/copresence/r/a;->c:Lcom/google/android/location/copresence/r/ae;

    invoke-virtual {v3}, Lcom/google/android/location/copresence/r/ae;->d()Z

    move-result v3

    if-nez v3, :cond_3

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/google/android/location/copresence/r/a;->e:Landroid/os/PowerManager;

    invoke-virtual {v2}, Landroid/os/PowerManager;->isScreenOn()Z

    move-result v2

    if-nez v2, :cond_3

    :cond_0
    iget-object v2, p0, Lcom/google/android/location/copresence/r/a;->i:Lcom/google/android/gms/location/copresence/x;

    if-eqz v2, :cond_2

    move v2, v0

    :goto_0
    if-nez v2, :cond_1

    iget-object v2, p0, Lcom/google/android/location/copresence/r/a;->c:Lcom/google/android/location/copresence/r/ae;

    invoke-virtual {v2}, Lcom/google/android/location/copresence/r/ae;->c()Z

    move-result v2

    if-nez v2, :cond_3

    :cond_1
    :goto_1
    return v0

    :cond_2
    move v2, v1

    goto :goto_0

    :cond_3
    move v0, v1

    goto :goto_1
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 149
    iget-object v0, p0, Lcom/google/android/location/copresence/r/a;->i:Lcom/google/android/gms/location/copresence/x;

    if-eqz v0, :cond_0

    .line 150
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/location/copresence/r/a;->i:Lcom/google/android/gms/location/copresence/x;

    .line 151
    :try_start_0
    iget-object v0, p0, Lcom/google/android/location/copresence/r/a;->b:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/location/copresence/r/a;->h:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 152
    :goto_0
    invoke-direct {p0}, Lcom/google/android/location/copresence/r/a;->d()Z

    .line 154
    :cond_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method
