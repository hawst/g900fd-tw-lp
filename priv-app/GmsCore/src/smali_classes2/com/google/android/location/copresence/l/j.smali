.class public final Lcom/google/android/location/copresence/l/j;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/location/copresence/e/h;
.implements Lcom/google/android/location/copresence/i;


# static fields
.field private static e:Lcom/google/android/location/copresence/l/j;


# instance fields
.field final a:Lcom/google/android/location/copresence/ap;

.field final b:Lcom/google/android/location/copresence/l/m;

.field c:I

.field final d:Ljava/lang/Runnable;

.field private f:Z


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 70
    new-instance v0, Lcom/google/android/location/copresence/l/k;

    invoke-direct {v0, p0}, Lcom/google/android/location/copresence/l/k;-><init>(Lcom/google/android/location/copresence/l/j;)V

    iput-object v0, p0, Lcom/google/android/location/copresence/l/j;->d:Ljava/lang/Runnable;

    .line 40
    invoke-static {p1}, Lcom/google/android/location/copresence/ap;->a(Landroid/content/Context;)Lcom/google/android/location/copresence/ap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/copresence/l/j;->a:Lcom/google/android/location/copresence/ap;

    .line 41
    invoke-static {p1}, Lcom/google/android/location/copresence/l/m;->a(Landroid/content/Context;)Lcom/google/android/location/copresence/l/m;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/copresence/l/j;->b:Lcom/google/android/location/copresence/l/m;

    .line 43
    invoke-static {p1}, Lcom/google/android/location/copresence/e/d;->a(Landroid/content/Context;)Lcom/google/android/location/copresence/e/d;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/android/location/copresence/e/d;->a(Lcom/google/android/location/copresence/e/h;)V

    .line 44
    return-void
.end method

.method public static declared-synchronized a(Landroid/content/Context;)Lcom/google/android/location/copresence/l/j;
    .locals 2

    .prologue
    .line 33
    const-class v1, Lcom/google/android/location/copresence/l/j;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/google/android/location/copresence/l/j;->e:Lcom/google/android/location/copresence/l/j;

    if-nez v0, :cond_0

    .line 34
    new-instance v0, Lcom/google/android/location/copresence/l/j;

    invoke-direct {v0, p0}, Lcom/google/android/location/copresence/l/j;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/google/android/location/copresence/l/j;->e:Lcom/google/android/location/copresence/l/j;

    .line 36
    :cond_0
    sget-object v0, Lcom/google/android/location/copresence/l/j;->e:Lcom/google/android/location/copresence/l/j;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 33
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method static c()J
    .locals 2

    .prologue
    .line 96
    invoke-static {}, Lcom/google/android/location/copresence/f/b;->b()Lcom/google/ac/b/c/g;

    move-result-object v0

    iget-object v0, v0, Lcom/google/ac/b/c/g;->e:Lcom/google/ac/b/c/m;

    iget-object v0, v0, Lcom/google/ac/b/c/m;->u:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    return-wide v0
.end method

.method private d()V
    .locals 4

    .prologue
    .line 85
    iget v0, p0, Lcom/google/android/location/copresence/l/j;->c:I

    if-lez v0, :cond_1

    iget-boolean v0, p0, Lcom/google/android/location/copresence/l/j;->f:Z

    if-nez v0, :cond_1

    .line 86
    iget-object v0, p0, Lcom/google/android/location/copresence/l/j;->a:Lcom/google/android/location/copresence/ap;

    invoke-virtual {v0}, Lcom/google/android/location/copresence/ap;->b()Lcom/google/android/location/copresence/c;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/location/copresence/l/j;->d:Ljava/lang/Runnable;

    invoke-static {}, Lcom/google/android/location/copresence/l/j;->c()J

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/location/copresence/c;->a(Ljava/lang/Runnable;J)V

    .line 88
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/location/copresence/l/j;->f:Z

    .line 93
    :cond_0
    :goto_0
    return-void

    .line 89
    :cond_1
    iget v0, p0, Lcom/google/android/location/copresence/l/j;->c:I

    if-nez v0, :cond_0

    .line 90
    iget-object v0, p0, Lcom/google/android/location/copresence/l/j;->a:Lcom/google/android/location/copresence/ap;

    invoke-virtual {v0}, Lcom/google/android/location/copresence/ap;->b()Lcom/google/android/location/copresence/c;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/location/copresence/l/j;->d:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Lcom/google/android/location/copresence/c;->a(Ljava/lang/Runnable;)V

    .line 91
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/location/copresence/l/j;->f:Z

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 0

    .prologue
    .line 101
    invoke-direct {p0}, Lcom/google/android/location/copresence/l/j;->d()V

    .line 102
    return-void
.end method

.method public final a(Lcom/google/android/location/copresence/e/f;)V
    .locals 1

    .prologue
    .line 48
    iget-object v0, p1, Lcom/google/android/location/copresence/e/f;->c:Lcom/google/android/gms/location/copresence/u;

    .line 49
    check-cast v0, Lcom/google/android/gms/location/copresence/internal/StrategyImpl;

    invoke-virtual {v0}, Lcom/google/android/gms/location/copresence/internal/StrategyImpl;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 50
    const/4 v0, 0x3

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 51
    const-string v0, "HeartbeatManager: Adding opportunistic/aggressive entry to HeartbeatManager"

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->b(Ljava/lang/String;)I

    .line 53
    :cond_0
    iget v0, p0, Lcom/google/android/location/copresence/l/j;->c:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/location/copresence/l/j;->c:I

    .line 54
    invoke-direct {p0}, Lcom/google/android/location/copresence/l/j;->d()V

    .line 56
    :cond_1
    return-void
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 106
    const/4 v0, 0x3

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 107
    const-string v0, "HeartbeatManager: Pausing heartbeat due to no internet connectivity."

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->b(Ljava/lang/String;)I

    .line 109
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/location/copresence/l/j;->f:Z

    .line 110
    iget-object v0, p0, Lcom/google/android/location/copresence/l/j;->a:Lcom/google/android/location/copresence/ap;

    invoke-virtual {v0}, Lcom/google/android/location/copresence/ap;->b()Lcom/google/android/location/copresence/c;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/location/copresence/l/j;->d:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Lcom/google/android/location/copresence/c;->a(Ljava/lang/Runnable;)V

    .line 111
    return-void
.end method

.method public final b(Lcom/google/android/location/copresence/e/f;)V
    .locals 1

    .prologue
    .line 60
    iget-object v0, p1, Lcom/google/android/location/copresence/e/f;->c:Lcom/google/android/gms/location/copresence/u;

    .line 61
    check-cast v0, Lcom/google/android/gms/location/copresence/internal/StrategyImpl;

    invoke-virtual {v0}, Lcom/google/android/gms/location/copresence/internal/StrategyImpl;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 62
    const/4 v0, 0x3

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 63
    const-string v0, "HeartbeatManager: Removing opportunistic/aggressive entry from HeartbeatManager"

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->b(Ljava/lang/String;)I

    .line 65
    :cond_0
    iget v0, p0, Lcom/google/android/location/copresence/l/j;->c:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/google/android/location/copresence/l/j;->c:I

    .line 66
    invoke-direct {p0}, Lcom/google/android/location/copresence/l/j;->d()V

    .line 68
    :cond_1
    return-void
.end method
