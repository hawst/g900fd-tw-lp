.class public final Lcom/google/android/location/copresence/l/i;
.super Lcom/google/android/location/copresence/l/ac;
.source "SourceFile"


# direct methods
.method static a(Lcom/google/ac/b/c/bi;)Lcom/google/android/gms/location/copresence/CopresenceSettings;
    .locals 3

    .prologue
    .line 71
    iget-object v0, p0, Lcom/google/ac/b/c/bi;->c:[Lcom/google/ac/b/c/aw;

    invoke-static {v0}, Lcom/google/android/location/copresence/l/l;->a([Lcom/google/ac/b/c/aw;)[Lcom/google/android/gms/location/copresence/internal/CopresenceNamedAcl;

    move-result-object v0

    .line 73
    iget-object v1, p0, Lcom/google/ac/b/c/bi;->d:[Lcom/google/ac/b/c/am;

    invoke-static {v1}, Lcom/google/android/location/copresence/l/g;->a([Lcom/google/ac/b/c/am;)[Lcom/google/android/gms/location/copresence/internal/CopresenceFeatureOptIn;

    move-result-object v1

    .line 76
    iget-object v2, p0, Lcom/google/ac/b/c/bi;->a:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-static {v2, v0, v1}, Lcom/google/android/gms/location/copresence/CopresenceSettings;->a(Z[Lcom/google/android/gms/location/copresence/internal/CopresenceNamedAcl;[Lcom/google/android/gms/location/copresence/internal/CopresenceFeatureOptIn;)Lcom/google/android/gms/location/copresence/CopresenceSettings;

    move-result-object v0

    return-object v0
.end method

.method static aw_()Lcom/google/ac/b/c/bj;
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 58
    new-instance v0, Lcom/google/ac/b/c/bj;

    invoke-direct {v0}, Lcom/google/ac/b/c/bj;-><init>()V

    .line 59
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, v0, Lcom/google/ac/b/c/bj;->a:Ljava/lang/Boolean;

    .line 60
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, v0, Lcom/google/ac/b/c/bj;->b:Ljava/lang/Boolean;

    .line 61
    new-instance v1, Lcom/google/ac/b/c/ay;

    invoke-direct {v1}, Lcom/google/ac/b/c/ay;-><init>()V

    iput-object v1, v0, Lcom/google/ac/b/c/bj;->c:Lcom/google/ac/b/c/ay;

    .line 62
    iget-object v1, v0, Lcom/google/ac/b/c/bj;->c:Lcom/google/ac/b/c/ay;

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    iput-object v2, v1, Lcom/google/ac/b/c/ay;->b:Ljava/lang/Boolean;

    .line 63
    new-instance v1, Lcom/google/ac/b/c/an;

    invoke-direct {v1}, Lcom/google/ac/b/c/an;-><init>()V

    iput-object v1, v0, Lcom/google/ac/b/c/bj;->d:Lcom/google/ac/b/c/an;

    .line 64
    iget-object v1, v0, Lcom/google/ac/b/c/bj;->d:Lcom/google/ac/b/c/an;

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    iput-object v2, v1, Lcom/google/ac/b/c/an;->b:Ljava/lang/Boolean;

    .line 66
    return-object v0
.end method


# virtual methods
.method protected final bridge synthetic a(Lcom/google/protobuf/nano/j;)Lcom/google/ac/b/c/dc;
    .locals 1

    .prologue
    .line 25
    check-cast p1, Lcom/google/ac/b/c/cs;

    if-eqz p1, :cond_0

    iget-object v0, p1, Lcom/google/ac/b/c/cs;->a:Lcom/google/ac/b/c/dc;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected final bridge synthetic a(Lcom/google/protobuf/nano/j;Lcom/google/protobuf/nano/j;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 25
    check-cast p2, Lcom/google/ac/b/c/cs;

    iget-object v0, p2, Lcom/google/ac/b/c/cs;->b:Lcom/google/ac/b/c/bi;

    invoke-static {v0}, Lcom/google/android/location/copresence/l/i;->a(Lcom/google/ac/b/c/bi;)Lcom/google/android/gms/location/copresence/CopresenceSettings;

    move-result-object v0

    return-object v0
.end method

.method protected final synthetic b(Lcom/google/protobuf/nano/j;)I
    .locals 1

    .prologue
    .line 25
    check-cast p1, Lcom/google/ac/b/c/cs;

    if-eqz p1, :cond_0

    iget-object v0, p1, Lcom/google/ac/b/c/cs;->a:Lcom/google/ac/b/c/dc;

    if-eqz v0, :cond_0

    iget-object v0, p1, Lcom/google/ac/b/c/cs;->a:Lcom/google/ac/b/c/dc;

    iget-object v0, v0, Lcom/google/ac/b/c/dc;->c:Lcom/google/ac/b/c/bl;

    if-eqz v0, :cond_0

    iget-object v0, p1, Lcom/google/ac/b/c/cs;->a:Lcom/google/ac/b/c/dc;

    iget-object v0, v0, Lcom/google/ac/b/c/dc;->c:Lcom/google/ac/b/c/bl;

    iget-object v0, v0, Lcom/google/ac/b/c/bl;->a:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    iget-object v0, p1, Lcom/google/ac/b/c/cs;->a:Lcom/google/ac/b/c/dc;

    iget-object v0, v0, Lcom/google/ac/b/c/dc;->c:Lcom/google/ac/b/c/bl;

    iget-object v0, v0, Lcom/google/ac/b/c/bl;->a:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method protected final synthetic b()Lcom/google/protobuf/nano/j;
    .locals 2

    .prologue
    .line 25
    new-instance v0, Lcom/google/ac/b/c/cr;

    invoke-direct {v0}, Lcom/google/ac/b/c/cr;-><init>()V

    invoke-virtual {p0}, Lcom/google/android/location/copresence/l/i;->e()Lcom/google/ac/b/c/db;

    move-result-object v1

    iput-object v1, v0, Lcom/google/ac/b/c/cr;->a:Lcom/google/ac/b/c/db;

    invoke-static {}, Lcom/google/android/location/copresence/l/i;->aw_()Lcom/google/ac/b/c/bj;

    move-result-object v1

    iput-object v1, v0, Lcom/google/ac/b/c/cr;->b:Lcom/google/ac/b/c/bj;

    return-object v0
.end method

.method public final bridge synthetic c()Lcom/google/android/location/copresence/a/a;
    .locals 1

    .prologue
    .line 25
    invoke-super {p0}, Lcom/google/android/location/copresence/l/ac;->c()Lcom/google/android/location/copresence/a/a;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic d()V
    .locals 0

    .prologue
    .line 25
    invoke-super {p0}, Lcom/google/android/location/copresence/l/ac;->d()V

    return-void
.end method
