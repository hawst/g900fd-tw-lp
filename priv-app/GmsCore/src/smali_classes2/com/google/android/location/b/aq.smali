.class public final Lcom/google/android/location/b/aq;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field a:S

.field public b:F

.field public c:F

.field d:S

.field e:I

.field public f:I

.field public g:J


# direct methods
.method public constructor <init>(Lcom/google/p/a/b/b/a;)V
    .locals 7

    .prologue
    const/4 v6, 0x5

    const/4 v5, 0x4

    const/4 v4, 0x3

    const/4 v3, 0x2

    const/high16 v2, -0x40800000    # -1.0f

    .line 541
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 539
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/google/android/location/b/aq;->g:J

    .line 542
    invoke-virtual {p1, v4}, Lcom/google/p/a/b/b/a;->i(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 543
    invoke-virtual {p1, v4}, Lcom/google/p/a/b/b/a;->c(I)I

    move-result v0

    int-to-short v0, v0

    iput-short v0, p0, Lcom/google/android/location/b/aq;->a:S

    .line 549
    :goto_0
    invoke-virtual {p1, v5}, Lcom/google/p/a/b/b/a;->i(I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 550
    invoke-virtual {p1, v5}, Lcom/google/p/a/b/b/a;->e(I)F

    move-result v0

    iput v0, p0, Lcom/google/android/location/b/aq;->b:F

    .line 555
    :goto_1
    const/4 v0, 0x7

    invoke-virtual {p1, v0}, Lcom/google/p/a/b/b/a;->i(I)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 556
    const/4 v0, 0x7

    invoke-virtual {p1, v0}, Lcom/google/p/a/b/b/a;->e(I)F

    move-result v0

    iput v0, p0, Lcom/google/android/location/b/aq;->c:F

    .line 561
    :goto_2
    invoke-virtual {p1, v3}, Lcom/google/p/a/b/b/a;->i(I)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 562
    invoke-virtual {p1, v3}, Lcom/google/p/a/b/b/a;->c(I)I

    move-result v0

    int-to-short v0, v0

    iput-short v0, p0, Lcom/google/android/location/b/aq;->d:S

    .line 568
    :goto_3
    invoke-virtual {p1, v6}, Lcom/google/p/a/b/b/a;->i(I)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 569
    invoke-virtual {p1, v6}, Lcom/google/p/a/b/b/a;->c(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/location/b/aq;->e:I

    .line 574
    :goto_4
    const/4 v0, 0x6

    invoke-virtual {p1, v0}, Lcom/google/p/a/b/b/a;->i(I)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 575
    const/4 v0, 0x6

    invoke-virtual {p1, v0}, Lcom/google/p/a/b/b/a;->c(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/location/b/aq;->f:I

    .line 579
    :goto_5
    return-void

    .line 546
    :cond_0
    const/16 v0, 0x7fff

    iput-short v0, p0, Lcom/google/android/location/b/aq;->a:S

    goto :goto_0

    .line 552
    :cond_1
    iput v2, p0, Lcom/google/android/location/b/aq;->b:F

    goto :goto_1

    .line 558
    :cond_2
    iput v2, p0, Lcom/google/android/location/b/aq;->c:F

    goto :goto_2

    .line 565
    :cond_3
    const/16 v0, 0x7fff

    iput-short v0, p0, Lcom/google/android/location/b/aq;->d:S

    goto :goto_3

    .line 571
    :cond_4
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/location/b/aq;->e:I

    goto :goto_4

    .line 577
    :cond_5
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/location/b/aq;->f:I

    goto :goto_5
.end method

.method static a(JJJJ)J
    .locals 2

    .prologue
    .line 762
    add-long v0, p2, p0

    sub-long/2addr v0, p4

    invoke-static {p6, p7, v0, v1}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v0

    return-wide v0
.end method

.method static b(J)S
    .locals 6

    .prologue
    .line 770
    long-to-double v0, p0

    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    mul-double/2addr v0, v2

    const-wide v2, 0x4194997000000000L    # 8.64E7

    div-double/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->floor(D)D

    move-result-wide v0

    double-to-long v0, v0

    .line 773
    const-wide/32 v2, 0x5265c00

    mul-long/2addr v2, v0

    sub-long/2addr v2, p0

    invoke-static {v2, v3}, Ljava/lang/Math;->abs(J)J

    move-result-wide v2

    .line 774
    const-wide/32 v4, 0x51d3440

    cmp-long v2, v2, v4

    if-lez v2, :cond_0

    .line 775
    const-wide/16 v2, 0x1

    add-long/2addr v0, v2

    .line 777
    :cond_0
    long-to-int v0, v0

    int-to-short v0, v0

    return v0
.end method


# virtual methods
.method final a(J)V
    .locals 1

    .prologue
    .line 706
    invoke-static {p1, p2}, Lcom/google/android/location/b/aq;->b(J)S

    move-result v0

    iput-short v0, p0, Lcom/google/android/location/b/aq;->d:S

    .line 707
    return-void
.end method
