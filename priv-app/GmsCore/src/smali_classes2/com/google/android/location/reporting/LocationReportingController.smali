.class public Lcom/google/android/location/reporting/LocationReportingController;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation build Lcom/google/android/gms/common/util/RetainForClient;
.end annotation


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Lcom/google/android/gms/common/util/p;

.field private final c:Lcom/google/android/location/reporting/e;

.field private final d:Lcom/google/android/location/reporting/e;

.field private final e:Lcom/google/android/location/reporting/e;

.field private final f:Lcom/google/android/location/reporting/s;

.field private final g:Lcom/google/android/location/reporting/config/h;

.field private final h:Lcom/google/android/location/reporting/service/q;

.field private final i:Lcom/google/android/location/fused/g;

.field private j:Lcom/google/android/location/reporting/ble/d;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/gms/common/util/p;Lcom/google/android/location/reporting/e;Lcom/google/android/location/reporting/e;Lcom/google/android/location/reporting/e;Lcom/google/android/location/reporting/s;Lcom/google/android/location/reporting/config/h;Lcom/google/android/location/reporting/service/q;Lcom/google/android/location/fused/g;)V
    .locals 0

    .prologue
    .line 114
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 115
    iput-object p1, p0, Lcom/google/android/location/reporting/LocationReportingController;->a:Landroid/content/Context;

    .line 116
    iput-object p2, p0, Lcom/google/android/location/reporting/LocationReportingController;->b:Lcom/google/android/gms/common/util/p;

    .line 117
    iput-object p3, p0, Lcom/google/android/location/reporting/LocationReportingController;->c:Lcom/google/android/location/reporting/e;

    .line 118
    iput-object p4, p0, Lcom/google/android/location/reporting/LocationReportingController;->d:Lcom/google/android/location/reporting/e;

    .line 119
    iput-object p5, p0, Lcom/google/android/location/reporting/LocationReportingController;->e:Lcom/google/android/location/reporting/e;

    .line 120
    iput-object p6, p0, Lcom/google/android/location/reporting/LocationReportingController;->f:Lcom/google/android/location/reporting/s;

    .line 121
    iput-object p7, p0, Lcom/google/android/location/reporting/LocationReportingController;->g:Lcom/google/android/location/reporting/config/h;

    .line 122
    iput-object p8, p0, Lcom/google/android/location/reporting/LocationReportingController;->h:Lcom/google/android/location/reporting/service/q;

    .line 123
    iput-object p9, p0, Lcom/google/android/location/reporting/LocationReportingController;->i:Lcom/google/android/location/fused/g;

    .line 124
    return-void
.end method

.method public static a(Landroid/content/Context;Landroid/content/Intent;)Landroid/app/PendingIntent;
    .locals 2

    .prologue
    .line 424
    const/4 v0, 0x0

    const/high16 v1, 0x8000000

    invoke-static {p0, v0, p1, v1}, Landroid/app/PendingIntent;->getService(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    return-object v0
.end method

.method private a(Ljava/lang/String;)Landroid/app/PendingIntent;
    .locals 2

    .prologue
    .line 419
    iget-object v0, p0, Lcom/google/android/location/reporting/LocationReportingController;->a:Landroid/content/Context;

    invoke-static {v0, p1}, Lcom/google/android/location/reporting/service/DispatchingService;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 420
    iget-object v1, p0, Lcom/google/android/location/reporting/LocationReportingController;->a:Landroid/content/Context;

    invoke-static {v1, v0}, Lcom/google/android/location/reporting/LocationReportingController;->a(Landroid/content/Context;Landroid/content/Intent;)Landroid/app/PendingIntent;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/location/reporting/LocationReportingController;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, Lcom/google/android/location/reporting/LocationReportingController;->a:Landroid/content/Context;

    return-object v0
.end method

.method private a(Lcom/google/android/location/reporting/config/ReportingConfig;J)V
    .locals 10

    .prologue
    const/4 v2, 0x0

    .line 182
    iget-object v0, p0, Lcom/google/android/location/reporting/LocationReportingController;->g:Lcom/google/android/location/reporting/config/h;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/location/reporting/config/h;->a(Lcom/google/android/location/reporting/config/ReportingConfig;J)Ljava/util/List;

    move-result-object v0

    .line 183
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :cond_0
    :goto_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/google/android/location/reporting/config/c;

    .line 184
    iget-object v8, v4, Lcom/google/android/location/reporting/config/c;->a:Lcom/google/android/location/reporting/config/AccountConfig;

    .line 185
    invoke-virtual {v8}, Lcom/google/android/location/reporting/config/AccountConfig;->z()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 186
    iget-object v0, p0, Lcom/google/android/location/reporting/LocationReportingController;->i:Lcom/google/android/location/fused/g;

    invoke-virtual {v0}, Lcom/google/android/location/fused/g;->d()Lcom/google/android/gms/location/LocationStatus;

    move-result-object v3

    .line 188
    iget-object v0, v4, Lcom/google/android/location/reporting/config/c;->b:Lcom/google/android/location/reporting/config/d;

    sget-object v1, Lcom/google/android/location/reporting/d;->a:[I

    invoke-virtual {v0}, Lcom/google/android/location/reporting/config/d;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    move-object v0, v2

    :goto_1
    new-instance v1, Lcom/google/android/ulr/ApiActivationChange;

    invoke-direct {v1, v0}, Lcom/google/android/ulr/ApiActivationChange;-><init>(Ljava/lang/String;)V

    invoke-static {v3}, Lcom/google/android/location/reporting/c;->a(Lcom/google/android/gms/location/LocationStatus;)Lcom/google/android/ulr/ApiLocationStatus;

    move-result-object v3

    new-instance v0, Lcom/google/android/ulr/ApiMetadata;

    iget-wide v4, v4, Lcom/google/android/location/reporting/config/c;->c:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    move-object v4, v2

    move-object v5, v2

    invoke-direct/range {v0 .. v6}, Lcom/google/android/ulr/ApiMetadata;-><init>(Lcom/google/android/ulr/ApiActivationChange;Lcom/google/android/ulr/ApiBleScanReport;Lcom/google/android/ulr/ApiLocationStatus;Lcom/google/android/ulr/ApiPlaceReport;Lcom/google/android/ulr/ApiRate;Ljava/lang/Long;)V

    .line 190
    const/4 v1, 0x1

    invoke-static {v1}, Lcom/google/android/location/reporting/b/l;->a(I)V

    .line 191
    iget-object v1, p0, Lcom/google/android/location/reporting/LocationReportingController;->e:Lcom/google/android/location/reporting/e;

    const-string v3, "ApiActivationChange"

    invoke-virtual {v1, v8, v0, v3}, Lcom/google/android/location/reporting/e;->a(Lcom/google/android/location/reporting/config/AccountConfig;Ljava/lang/Object;Ljava/lang/String;)Z

    goto :goto_0

    .line 188
    :pswitch_0
    const-string v0, "inactivated"

    goto :goto_1

    :pswitch_1
    const-string v0, "activated"

    goto :goto_1

    .line 195
    :cond_1
    return-void

    .line 188
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private static a(Ljava/lang/RuntimeException;)V
    .locals 2

    .prologue
    .line 431
    const-string v0, "GCoreUlr"

    const-string v1, "Unexpected exception thrown by BleScanReporter"

    invoke-static {v0, v1, p0}, Lcom/google/android/location/reporting/b/d;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 432
    return-void
.end method

.method private b()V
    .locals 2

    .prologue
    .line 324
    iget-object v0, p0, Lcom/google/android/location/reporting/LocationReportingController;->j:Lcom/google/android/location/reporting/ble/d;

    if-eqz v0, :cond_0

    .line 326
    :try_start_0
    iget-object v0, p0, Lcom/google/android/location/reporting/LocationReportingController;->j:Lcom/google/android/location/reporting/ble/d;

    invoke-interface {v0}, Lcom/google/android/location/reporting/ble/d;->g()V

    .line 327
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/location/reporting/LocationReportingController;->j:Lcom/google/android/location/reporting/ble/d;
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    .line 331
    :goto_0
    const-string v0, "GCoreUlr"

    const/4 v1, 0x4

    invoke-static {v0, v1}, Lcom/google/android/location/reporting/b/d;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 332
    const-string v0, "GCoreUlr"

    const-string v1, "GMS BLE scans disabled."

    invoke-static {v0, v1}, Lcom/google/android/location/reporting/b/d;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 335
    :cond_0
    return-void

    .line 328
    :catch_0
    move-exception v0

    invoke-static {v0}, Lcom/google/android/location/reporting/LocationReportingController;->a(Ljava/lang/RuntimeException;)V

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 202
    const-string v0, "GCoreUlr"

    const/4 v1, 0x2

    invoke-static {v0, v1}, Lcom/google/android/location/reporting/b/d;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 203
    const-string v0, "GCoreUlr"

    const-string v1, "Removing Location Reporting requests."

    invoke-static {v0, v1}, Lcom/google/android/location/reporting/b/d;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 205
    :cond_0
    iget-object v0, p0, Lcom/google/android/location/reporting/LocationReportingController;->i:Lcom/google/android/location/fused/g;

    invoke-virtual {v0, v2}, Lcom/google/android/location/fused/g;->a(Lcom/google/android/location/fused/t;)V

    .line 206
    const-string v0, "com.google.android.location.reporting.ACTION_LOCATION"

    invoke-direct {p0, v0}, Lcom/google/android/location/reporting/LocationReportingController;->a(Ljava/lang/String;)Landroid/app/PendingIntent;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/location/reporting/LocationReportingController;->i:Lcom/google/android/location/fused/g;

    invoke-virtual {v1, v0}, Lcom/google/android/location/fused/g;->b(Landroid/app/PendingIntent;)V

    iget-object v1, p0, Lcom/google/android/location/reporting/LocationReportingController;->i:Lcom/google/android/location/fused/g;

    invoke-virtual {v1, v2}, Lcom/google/android/location/fused/g;->a(Lcom/google/android/location/fused/t;)V

    invoke-virtual {v0}, Landroid/app/PendingIntent;->cancel()V

    const-string v0, "com.google.android.location.reporting.ACTION_ACTIVITY"

    invoke-direct {p0, v0}, Lcom/google/android/location/reporting/LocationReportingController;->a(Ljava/lang/String;)Landroid/app/PendingIntent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/PendingIntent;->cancel()V

    const-string v0, "GCoreUlr"

    const/4 v1, 0x4

    invoke-static {v0, v1}, Lcom/google/android/location/reporting/b/d;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "GCoreUlr"

    const-string v1, "Unbound from all location providers"

    invoke-static {v0, v1}, Lcom/google/android/location/reporting/b/d;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 207
    :cond_1
    invoke-direct {p0}, Lcom/google/android/location/reporting/LocationReportingController;->b()V

    .line 208
    return-void
.end method

.method public final a(Lcom/google/android/location/reporting/config/ReportingConfig;Ljava/lang/String;)V
    .locals 16

    .prologue
    .line 137
    invoke-static {}, Lcom/google/android/location/reporting/b/a;->a()V

    .line 139
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/location/reporting/LocationReportingController;->b:Lcom/google/android/gms/common/util/p;

    invoke-interface {v2}, Lcom/google/android/gms/common/util/p;->a()J

    move-result-wide v12

    .line 142
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/location/reporting/LocationReportingController;->g:Lcom/google/android/location/reporting/config/h;

    invoke-virtual {v2}, Lcom/google/android/location/reporting/config/h;->c()Lcom/google/android/location/reporting/config/ReportingConfig;

    move-result-object v2

    .line 143
    move-object/from16 v0, p1

    invoke-static {v2, v0}, Lcom/google/android/location/reporting/b/l;->a(Lcom/google/android/location/reporting/config/ReportingConfig;Lcom/google/android/location/reporting/config/ReportingConfig;)V

    .line 145
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/location/reporting/config/ReportingConfig;->getActiveAccounts()Ljava/util/List;

    move-result-object v2

    .line 146
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/location/reporting/LocationReportingController;->c:Lcom/google/android/location/reporting/e;

    invoke-static {v3, v2}, Lcom/google/android/location/reporting/b/n;->a(Lcom/google/android/location/reporting/e;Ljava/util/List;)V

    .line 147
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/location/reporting/LocationReportingController;->d:Lcom/google/android/location/reporting/e;

    invoke-static {v3, v2}, Lcom/google/android/location/reporting/b/n;->a(Lcom/google/android/location/reporting/e;Ljava/util/List;)V

    .line 148
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/location/reporting/LocationReportingController;->e:Lcom/google/android/location/reporting/e;

    invoke-static {v3, v2}, Lcom/google/android/location/reporting/b/n;->a(Lcom/google/android/location/reporting/e;Ljava/util/List;)V

    .line 150
    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_17

    .line 151
    const-string v3, "GCoreUlr"

    const/4 v4, 0x4

    invoke-static {v3, v4}, Lcom/google/android/location/reporting/b/d;->a(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 152
    const-string v3, "GCoreUlr"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p2

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ": Ensuring that reporting is active for "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {v2}, Lcom/google/android/gms/location/reporting/a/d;->a(Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v2}, Lcom/google/android/location/reporting/b/d;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 155
    :cond_0
    invoke-static {}, Lcom/google/android/location/activity/k;->a()Z

    move-result v2

    if-eqz v2, :cond_9

    const-string v6, "internal"

    new-instance v2, Lcom/google/android/ulr/ApiRate;

    const-string v3, "stationary"

    sget-object v4, Lcom/google/android/location/reporting/service/ab;->g:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v4}, Lcom/google/android/gms/common/a/d;->c()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Long;

    const-string v5, "stationary"

    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    invoke-direct/range {v2 .. v10}, Lcom/google/android/ulr/ApiRate;-><init>(Ljava/lang/String;Ljava/lang/Long;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/String;Ljava/lang/String;)V

    move-object v11, v2

    :goto_0
    sget-object v2, Lcom/google/android/location/reporting/service/ab;->h:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v2}, Lcom/google/android/gms/common/a/d;->c()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    sget-object v2, Lcom/google/android/location/reporting/service/ab;->k:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v2}, Lcom/google/android/gms/common/a/d;->c()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v8

    sget-object v2, Lcom/google/android/location/reporting/service/ab;->i:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v2}, Lcom/google/android/gms/common/a/d;->c()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v9

    new-instance v3, Lcom/google/android/location/reporting/y;

    invoke-virtual {v11}, Lcom/google/android/ulr/ApiRate;->b()Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-direct/range {v3 .. v10}, Lcom/google/android/location/reporting/y;-><init>(JJZJ)V

    const-string v2, "com.google.android.location.reporting.ACTION_LOCATION"

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/google/android/location/reporting/LocationReportingController;->a(Ljava/lang/String;)Landroid/app/PendingIntent;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/location/reporting/LocationReportingController;->h:Lcom/google/android/location/reporting/service/q;

    invoke-virtual {v2}, Lcom/google/android/location/reporting/service/q;->l()Lcom/google/android/location/reporting/config/ReportingConfig;

    move-result-object v4

    sget-object v2, Lcom/google/android/location/reporting/service/ab;->T:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v2}, Lcom/google/android/gms/common/a/d;->c()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_a

    new-instance v2, Lcom/google/android/location/reporting/l;

    move-object/from16 v0, p0

    invoke-direct {v2, v0}, Lcom/google/android/location/reporting/l;-><init>(Lcom/google/android/location/reporting/LocationReportingController;)V

    :goto_1
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/location/reporting/LocationReportingController;->i:Lcom/google/android/location/fused/g;

    invoke-virtual {v6, v2}, Lcom/google/android/location/fused/g;->a(Lcom/google/android/location/fused/t;)V

    if-nez v4, :cond_b

    const-string v2, "GCoreUlr"

    const/4 v4, 0x3

    invoke-static {v2, v4}, Lcom/google/android/location/reporting/b/d;->a(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_1

    const-string v2, "GCoreUlr"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v6, "Skipping FLP amnesia test: no oldConfig in "

    invoke-direct {v4, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/location/reporting/LocationReportingController;->h:Lcom/google/android/location/reporting/service/q;

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Lcom/google/android/location/reporting/b/d;->b(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    :goto_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/location/reporting/LocationReportingController;->i:Lcom/google/android/location/fused/g;

    invoke-static {}, Lcom/google/android/gms/location/LocationRequest;->a()Lcom/google/android/gms/location/LocationRequest;

    move-result-object v4

    iget-wide v6, v3, Lcom/google/android/location/reporting/y;->a:J

    invoke-virtual {v4, v6, v7}, Lcom/google/android/gms/location/LocationRequest;->a(J)Lcom/google/android/gms/location/LocationRequest;

    move-result-object v4

    const/16 v6, 0x66

    invoke-virtual {v4, v6}, Lcom/google/android/gms/location/LocationRequest;->a(I)Lcom/google/android/gms/location/LocationRequest;

    move-result-object v4

    iget-wide v6, v3, Lcom/google/android/location/reporting/y;->b:J

    invoke-virtual {v4, v6, v7}, Lcom/google/android/gms/location/LocationRequest;->b(J)Lcom/google/android/gms/location/LocationRequest;

    move-result-object v4

    iget-boolean v6, v3, Lcom/google/android/location/reporting/y;->d:Z

    invoke-virtual {v2, v4, v5, v6}, Lcom/google/android/location/fused/g;->a(Lcom/google/android/gms/location/LocationRequest;Landroid/app/PendingIntent;Z)V

    const-string v2, "GCoreUlr"

    const/4 v4, 0x3

    invoke-static {v2, v4}, Lcom/google/android/location/reporting/b/d;->a(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_2

    const-string v2, "GCoreUlr"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "GMS FLP location updates requested: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3}, Lcom/google/android/location/reporting/y;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "; triggerImmediate=false"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Lcom/google/android/location/reporting/b/d;->b(Ljava/lang/String;Ljava/lang/String;)V

    :cond_2
    const-string v2, "com.google.android.location.reporting.ACTION_ACTIVITY"

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/google/android/location/reporting/LocationReportingController;->a(Ljava/lang/String;)Landroid/app/PendingIntent;

    move-result-object v9

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/location/reporting/LocationReportingController;->a:Landroid/content/Context;

    new-instance v5, Lcom/google/android/location/internal/j;

    invoke-direct {v5}, Lcom/google/android/location/internal/j;-><init>()V

    iget-wide v6, v3, Lcom/google/android/location/reporting/y;->c:J

    const/4 v8, 0x0

    const-string v10, "UlrSampleSpec"

    invoke-virtual/range {v5 .. v10}, Lcom/google/android/location/internal/j;->a(JZLandroid/app/PendingIntent;Ljava/lang/String;)Lcom/google/android/location/internal/j;

    const/4 v4, 0x1

    invoke-virtual {v5, v4}, Lcom/google/android/location/internal/j;->c(Z)Lcom/google/android/location/internal/j;

    invoke-virtual {v5, v2}, Lcom/google/android/location/internal/j;->a(Landroid/content/Context;)Landroid/content/ComponentName;

    move-result-object v2

    if-nez v2, :cond_11

    const-string v2, "GCoreUlr"

    const/4 v4, 0x5

    invoke-static {v2, v4}, Lcom/google/android/location/reporting/b/d;->a(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_3

    const-string v2, "GCoreUlr"

    const-string v4, "Unable to bind to GMS NLP"

    invoke-static {v2, v4}, Lcom/google/android/location/reporting/b/d;->d(Ljava/lang/String;Ljava/lang/String;)V

    :cond_3
    :goto_3
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/location/reporting/LocationReportingController;->b:Lcom/google/android/gms/common/util/p;

    invoke-interface {v2}, Lcom/google/android/gms/common/util/p;->b()J

    move-result-wide v4

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/location/reporting/LocationReportingController;->h:Lcom/google/android/location/reporting/service/q;

    invoke-virtual {v2, v11, v3, v4, v5}, Lcom/google/android/location/reporting/service/q;->a(Lcom/google/android/ulr/ApiRate;Lcom/google/android/location/reporting/y;J)V

    invoke-static {v11}, Lcom/google/android/location/reporting/c;->a(Lcom/google/android/ulr/ApiRate;)Lcom/google/android/ulr/ApiMetadata;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/location/reporting/LocationReportingController;->e:Lcom/google/android/location/reporting/e;

    const-string v4, "ApiRate"

    move-object/from16 v0, p1

    invoke-static {v3, v0, v2, v4}, Lcom/google/android/location/reporting/c;->a(Lcom/google/android/location/reporting/e;Lcom/google/android/location/reporting/config/ReportingConfig;Lcom/google/android/ulr/ApiMetadata;Ljava/lang/String;)V

    const-string v2, "GCoreUlr"

    const/4 v3, 0x4

    invoke-static {v2, v3}, Lcom/google/android/location/reporting/b/d;->a(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_4

    const-string v2, "GCoreUlr"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "GMS FLP location and AR updates requested: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/location/reporting/b/d;->c(Ljava/lang/String;Ljava/lang/String;)V

    :cond_4
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/location/reporting/LocationReportingController;->a:Landroid/content/Context;

    invoke-static {v2}, Lcom/google/android/location/reporting/service/ac;->a(Landroid/content/Context;)V

    .line 157
    :cond_5
    :goto_4
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x13

    if-lt v2, v3, :cond_13

    sget-object v2, Lcom/google/android/location/reporting/service/ab;->o:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v2}, Lcom/google/android/gms/common/a/d;->c()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_13

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/location/reporting/LocationReportingController;->a:Landroid/content/Context;

    const-string v3, "activity"

    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/app/ActivityManager;

    sget v3, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v4, 0x13

    if-lt v3, v4, :cond_12

    if-eqz v2, :cond_12

    invoke-virtual {v2}, Landroid/app/ActivityManager;->isLowRamDevice()Z

    move-result v2

    if-eqz v2, :cond_12

    const/4 v2, 0x1

    :goto_5
    if-eqz v2, :cond_6

    sget-object v2, Lcom/google/android/location/reporting/service/ab;->p:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v2}, Lcom/google/android/gms/common/a/d;->c()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_13

    :cond_6
    invoke-static {}, Landroid/bluetooth/BluetoothAdapter;->getDefaultAdapter()Landroid/bluetooth/BluetoothAdapter;

    move-result-object v2

    if-eqz v2, :cond_13

    const/4 v2, 0x1

    :goto_6
    if-eqz v2, :cond_16

    .line 158
    :try_start_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/location/reporting/LocationReportingController;->j:Lcom/google/android/location/reporting/ble/d;

    if-nez v2, :cond_8

    sget-object v2, Lcom/google/android/location/reporting/service/ab;->C:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v2}, Lcom/google/android/gms/common/a/d;->c()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_14

    const-string v2, "GCoreUlr"

    const/4 v3, 0x4

    invoke-static {v2, v3}, Lcom/google/android/location/reporting/b/d;->a(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_7

    const-string v2, "GCoreUlr"

    const-string v3, "GMS BLE building event-driven scanner"

    invoke-static {v2, v3}, Lcom/google/android/location/reporting/b/d;->c(Ljava/lang/String;Ljava/lang/String;)V

    :cond_7
    new-instance v2, Lcom/google/android/location/reporting/ble/a;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/location/reporting/LocationReportingController;->a:Landroid/content/Context;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/location/reporting/LocationReportingController;->b:Lcom/google/android/gms/common/util/p;

    invoke-direct {v2, v3, v4}, Lcom/google/android/location/reporting/ble/a;-><init>(Landroid/content/Context;Lcom/google/android/gms/common/util/p;)V

    :goto_7
    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/location/reporting/LocationReportingController;->j:Lcom/google/android/location/reporting/ble/d;

    :cond_8
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/location/reporting/LocationReportingController;->j:Lcom/google/android/location/reporting/ble/d;

    invoke-interface {v2}, Lcom/google/android/location/reporting/ble/d;->f()V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    .line 177
    :goto_8
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/location/reporting/LocationReportingController;->h:Lcom/google/android/location/reporting/service/q;

    move-object/from16 v0, p1

    invoke-virtual {v2, v0}, Lcom/google/android/location/reporting/service/q;->a(Lcom/google/android/location/reporting/config/ReportingConfig;)V

    .line 178
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v12, v13}, Lcom/google/android/location/reporting/LocationReportingController;->a(Lcom/google/android/location/reporting/config/ReportingConfig;J)V

    .line 179
    return-void

    .line 155
    :cond_9
    const-string v6, "internal"

    new-instance v2, Lcom/google/android/ulr/ApiRate;

    const-string v3, "default"

    sget-object v4, Lcom/google/android/location/reporting/service/ab;->f:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v4}, Lcom/google/android/gms/common/a/d;->c()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Long;

    const-string v5, "default"

    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    invoke-direct/range {v2 .. v10}, Lcom/google/android/ulr/ApiRate;-><init>(Ljava/lang/String;Ljava/lang/Long;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/String;Ljava/lang/String;)V

    move-object v11, v2

    goto/16 :goto_0

    :cond_a
    const/4 v2, 0x0

    goto/16 :goto_1

    :cond_b
    invoke-virtual {v4}, Lcom/google/android/location/reporting/config/ReportingConfig;->isReportingActive()Z

    move-result v2

    if-eqz v2, :cond_1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/location/reporting/LocationReportingController;->i:Lcom/google/android/location/fused/g;

    invoke-virtual {v2, v5}, Lcom/google/android/location/fused/g;->a(Landroid/app/PendingIntent;)Lcom/google/android/gms/location/LocationRequest;

    move-result-object v6

    if-nez v6, :cond_c

    const-string v2, "flp_request_dropped"

    invoke-static {v2}, Lcom/google/android/location/reporting/b/l;->a(Ljava/lang/String;)V

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v6, "FLP dropped ULR location request: "

    invoke-direct {v2, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v4, "GCoreUlr"

    new-instance v6, Ljava/lang/IllegalStateException;

    invoke-direct {v6, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    invoke-static {v4, v6}, Lcom/google/android/location/reporting/b/d;->b(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_2

    :cond_c
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/location/reporting/LocationReportingController;->h:Lcom/google/android/location/reporting/service/q;

    iget-object v2, v4, Lcom/google/android/location/reporting/service/q;->c:Lcom/google/android/location/reporting/y;

    invoke-virtual {v4}, Lcom/google/android/location/reporting/service/q;->l()Lcom/google/android/location/reporting/config/ReportingConfig;

    move-result-object v4

    if-eqz v4, :cond_d

    invoke-virtual {v4}, Lcom/google/android/location/reporting/config/ReportingConfig;->isReportingActive()Z

    move-result v4

    if-eqz v4, :cond_d

    const/4 v4, 0x1

    :goto_9
    if-eqz v6, :cond_f

    if-eqz v2, :cond_f

    if-eqz v4, :cond_f

    iget-wide v8, v2, Lcom/google/android/location/reporting/y;->a:J

    invoke-virtual {v6}, Lcom/google/android/gms/location/LocationRequest;->c()J

    move-result-wide v14

    cmp-long v4, v8, v14

    if-nez v4, :cond_e

    :goto_a
    invoke-virtual {v3, v2}, Lcom/google/android/location/reporting/y;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_10

    const-string v4, "GCoreUlr"

    const/4 v5, 0x3

    invoke-static {v4, v5}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_5

    const-string v4, "GCoreUlr"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "current request "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v5, " matches "

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", no need to request a new one"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v4, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_4

    :cond_d
    const/4 v4, 0x0

    goto :goto_9

    :cond_e
    const-string v4, "GCoreUlr"

    new-instance v7, Ljava/lang/IllegalStateException;

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "FLP request "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v8, " incompatible with "

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v7, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    invoke-static {v4, v7}, Lcom/google/android/location/reporting/b/d;->b(Ljava/lang/String;Ljava/lang/Throwable;)V

    const-string v2, "flp_request_inconsistent"

    invoke-static {v2}, Lcom/google/android/location/reporting/b/l;->a(Ljava/lang/String;)V

    :cond_f
    const/4 v2, 0x0

    goto :goto_a

    :cond_10
    const-string v4, "GCoreUlr"

    const/4 v6, 0x3

    invoke-static {v4, v6}, Lcom/google/android/location/reporting/b/d;->a(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_1

    const-string v4, "GCoreUlr"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "FLP request: "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v6, " -> "

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v4, v2}, Lcom/google/android/location/reporting/b/d;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2

    :cond_11
    const-string v2, "GCoreUlr"

    const/4 v4, 0x3

    invoke-static {v2, v4}, Lcom/google/android/location/reporting/b/d;->a(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_3

    const-string v2, "GCoreUlr"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "GMS NLP is bound with activity detection polling interval "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v6, v3, Lcom/google/android/location/reporting/y;->c:J

    invoke-virtual {v4, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " ms"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Lcom/google/android/location/reporting/b/d;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_3

    .line 157
    :cond_12
    const/4 v2, 0x0

    goto/16 :goto_5

    :cond_13
    const/4 v2, 0x0

    goto/16 :goto_6

    .line 158
    :cond_14
    :try_start_1
    const-string v2, "GCoreUlr"

    const/4 v3, 0x4

    invoke-static {v2, v3}, Lcom/google/android/location/reporting/b/d;->a(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_15

    const-string v2, "GCoreUlr"

    const-string v3, "GMS BLE building self-scheduled scanner"

    invoke-static {v2, v3}, Lcom/google/android/location/reporting/b/d;->c(Ljava/lang/String;Ljava/lang/String;)V

    :cond_15
    new-instance v2, Lcom/google/android/location/reporting/ble/h;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/location/reporting/LocationReportingController;->a:Landroid/content/Context;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/location/reporting/LocationReportingController;->b:Lcom/google/android/gms/common/util/p;

    invoke-direct {v2, v3, v4}, Lcom/google/android/location/reporting/ble/h;-><init>(Landroid/content/Context;Lcom/google/android/gms/common/util/p;)V
    :try_end_1
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_7

    :catch_0
    move-exception v2

    invoke-static {v2}, Lcom/google/android/location/reporting/LocationReportingController;->a(Ljava/lang/RuntimeException;)V

    goto/16 :goto_8

    .line 160
    :cond_16
    invoke-direct/range {p0 .. p0}, Lcom/google/android/location/reporting/LocationReportingController;->b()V

    goto/16 :goto_8

    .line 163
    :cond_17
    const-string v2, "GCoreUlr"

    const/4 v3, 0x4

    invoke-static {v2, v3}, Lcom/google/android/location/reporting/b/d;->a(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_18

    .line 164
    const-string v2, "GCoreUlr"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p2

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ": Ensuring that reporting is stopped because of reasons: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/location/reporting/config/ReportingConfig;->getInactiveReasonsString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/location/reporting/b/d;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 167
    :cond_18
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/location/reporting/LocationReportingController;->a()V

    .line 171
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/location/reporting/LocationReportingController;->f:Lcom/google/android/location/reporting/s;

    invoke-virtual {v2}, Lcom/google/android/location/reporting/s;->a()V

    .line 172
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/location/reporting/LocationReportingController;->h:Lcom/google/android/location/reporting/service/q;

    invoke-virtual {v2}, Lcom/google/android/location/reporting/service/q;->a()V

    goto/16 :goto_8
.end method
