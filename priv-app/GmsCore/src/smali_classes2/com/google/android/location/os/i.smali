.class public final Lcom/google/android/location/os/i;
.super Lcom/google/android/location/o/b;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/location/os/bi;


# instance fields
.field private final a:Lcom/google/android/location/j/c;


# direct methods
.method public constructor <init>(Lcom/google/android/location/os/bi;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 45
    new-instance v0, Lcom/google/android/location/os/ax;

    invoke-direct {v0}, Lcom/google/android/location/os/ax;-><init>()V

    invoke-direct {p0, p1, v0, v2}, Lcom/google/android/location/o/b;-><init>(Ljava/lang/Object;Ljava/lang/Object;Z)V

    .line 46
    new-instance v0, Lcom/google/android/location/j/c;

    invoke-interface {p1}, Lcom/google/android/location/os/bi;->B()Lcom/google/android/location/j/f;

    move-result-object v1

    invoke-direct {v0, v1, v2}, Lcom/google/android/location/j/c;-><init>(Lcom/google/android/location/j/f;Z)V

    iput-object v0, p0, Lcom/google/android/location/os/i;->a:Lcom/google/android/location/j/c;

    .line 47
    return-void
.end method


# virtual methods
.method public final A()Lcom/google/android/location/os/bm;
    .locals 1

    .prologue
    .line 328
    invoke-virtual {p0}, Lcom/google/android/location/os/i;->C()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/os/bi;

    invoke-interface {v0}, Lcom/google/android/location/os/bi;->A()Lcom/google/android/location/os/bm;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic B()Lcom/google/android/location/j/f;
    .locals 2

    .prologue
    .line 39
    invoke-virtual {p0}, Lcom/google/android/location/os/i;->C()Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/location/os/i;->a:Lcom/google/android/location/j/c;

    invoke-virtual {v0}, Lcom/google/android/location/j/c;->C()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/location/os/i;->C()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/os/bi;

    invoke-interface {v0}, Lcom/google/android/location/os/bi;->B()Lcom/google/android/location/j/f;

    move-result-object v0

    if-eq v1, v0, :cond_0

    const-string v0, "DetachableOs"

    const-string v1, "getGls"

    invoke-static {v0, v1}, Lcom/google/android/location/o/a/a;->e(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/location/os/i;->a:Lcom/google/android/location/j/c;

    return-object v0
.end method

.method public final a(Ljava/lang/String;Lcom/google/android/location/collectionlib/ab;Ljava/lang/String;Lcom/google/android/location/o/n;)Lcom/google/android/location/collectionlib/bd;
    .locals 1

    .prologue
    .line 220
    invoke-virtual {p0}, Lcom/google/android/location/os/i;->C()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/os/bi;

    invoke-interface {v0, p1, p2, p3, p4}, Lcom/google/android/location/os/bi;->a(Ljava/lang/String;Lcom/google/android/location/collectionlib/ab;Ljava/lang/String;Lcom/google/android/location/o/n;)Lcom/google/android/location/collectionlib/bd;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/util/Set;Ljava/util/Map;JLjava/lang/String;Ljava/lang/Integer;ZLcom/google/p/a/b/b/a;ZLcom/google/android/location/collectionlib/ar;Ljava/lang/String;Lcom/google/android/location/o/n;)Lcom/google/android/location/collectionlib/be;
    .locals 15

    .prologue
    .line 171
    invoke-virtual {p0}, Lcom/google/android/location/os/i;->C()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/location/os/bi;

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move-wide/from16 v4, p3

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move/from16 v8, p7

    move-object/from16 v9, p8

    move/from16 v10, p9

    move-object/from16 v11, p10

    move-object/from16 v12, p11

    move-object/from16 v13, p12

    invoke-interface/range {v1 .. v13}, Lcom/google/android/location/os/bi;->a(Ljava/util/Set;Ljava/util/Map;JLjava/lang/String;Ljava/lang/Integer;ZLcom/google/p/a/b/b/a;ZLcom/google/android/location/collectionlib/ar;Ljava/lang/String;Lcom/google/android/location/o/n;)Lcom/google/android/location/collectionlib/be;

    move-result-object v0

    return-object v0
.end method

.method public final a(ZLjava/util/Set;Ljava/util/Map;JLcom/google/android/location/collectionlib/SensorScannerConfig;Lcom/google/android/location/collectionlib/ar;Ljava/lang/String;Lcom/google/android/location/o/n;)Lcom/google/android/location/collectionlib/be;
    .locals 10

    .prologue
    .line 161
    invoke-virtual {p0}, Lcom/google/android/location/os/i;->C()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/os/bi;

    move v1, p1

    move-object v2, p2

    move-object v3, p3

    move-wide v4, p4

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    move-object/from16 v8, p8

    move-object/from16 v9, p9

    invoke-interface/range {v0 .. v9}, Lcom/google/android/location/os/bi;->a(ZLjava/util/Set;Ljava/util/Map;JLcom/google/android/location/collectionlib/SensorScannerConfig;Lcom/google/android/location/collectionlib/ar;Ljava/lang/String;Lcom/google/android/location/o/n;)Lcom/google/android/location/collectionlib/be;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;)Ljava/io/InputStream;
    .locals 1

    .prologue
    .line 138
    invoke-virtual {p0}, Lcom/google/android/location/os/i;->C()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/os/bi;

    invoke-interface {v0, p1}, Lcom/google/android/location/os/bi;->a(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v0

    return-object v0
.end method

.method public final a()V
    .locals 1

    .prologue
    .line 57
    invoke-super {p0}, Lcom/google/android/location/o/b;->a()V

    .line 58
    iget-object v0, p0, Lcom/google/android/location/os/i;->a:Lcom/google/android/location/j/c;

    invoke-virtual {v0}, Lcom/google/android/location/j/c;->a()V

    .line 59
    return-void
.end method

.method public final a(II)V
    .locals 1

    .prologue
    .line 128
    invoke-virtual {p0}, Lcom/google/android/location/os/i;->C()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/os/bi;

    invoke-interface {v0, p1, p2}, Lcom/google/android/location/os/bi;->a(II)V

    .line 129
    return-void
.end method

.method public final a(J)V
    .locals 1

    .prologue
    .line 74
    invoke-virtual {p0}, Lcom/google/android/location/os/i;->C()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/os/bi;

    invoke-interface {v0, p1, p2}, Lcom/google/android/location/os/bi;->a(J)V

    .line 75
    return-void
.end method

.method public final a(Lcom/google/android/gms/location/ActivityRecognitionResult;)V
    .locals 1

    .prologue
    .line 240
    invoke-virtual {p0}, Lcom/google/android/location/os/i;->C()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/os/bi;

    invoke-interface {v0, p1}, Lcom/google/android/location/os/bi;->a(Lcom/google/android/gms/location/ActivityRecognitionResult;)V

    .line 241
    return-void
.end method

.method public final a(Lcom/google/android/location/activity/bd;)V
    .locals 1

    .prologue
    .line 235
    invoke-virtual {p0}, Lcom/google/android/location/os/i;->C()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/os/bi;

    invoke-interface {v0, p1}, Lcom/google/android/location/os/bi;->a(Lcom/google/android/location/activity/bd;)V

    .line 236
    return-void
.end method

.method public final a(Lcom/google/android/location/e/ag;Lcom/google/android/location/e/ay;)V
    .locals 1

    .prologue
    .line 123
    invoke-virtual {p0}, Lcom/google/android/location/os/i;->C()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/os/bi;

    invoke-interface {v0, p1, p2}, Lcom/google/android/location/os/bi;->a(Lcom/google/android/location/e/ag;Lcom/google/android/location/e/ay;)V

    .line 124
    return-void
.end method

.method public final a(Lcom/google/android/location/e/bh;)V
    .locals 1

    .prologue
    .line 318
    invoke-virtual {p0}, Lcom/google/android/location/os/i;->C()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/os/bi;

    invoke-interface {v0, p1}, Lcom/google/android/location/os/bi;->a(Lcom/google/android/location/e/bh;)V

    .line 319
    return-void
.end method

.method public final a(Lcom/google/android/location/j/k;Z)V
    .locals 1

    .prologue
    .line 113
    invoke-virtual {p0}, Lcom/google/android/location/os/i;->C()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/os/bi;

    invoke-interface {v0, p1, p2}, Lcom/google/android/location/os/bi;->a(Lcom/google/android/location/j/k;Z)V

    .line 114
    return-void
.end method

.method public final a(Ljava/lang/String;Lcom/google/android/location/collectionlib/cg;)V
    .locals 1

    .prologue
    .line 199
    invoke-virtual {p0}, Lcom/google/android/location/os/i;->C()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/os/bi;

    invoke-interface {v0, p1, p2}, Lcom/google/android/location/os/bi;->a(Ljava/lang/String;Lcom/google/android/location/collectionlib/cg;)V

    .line 200
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JZ)V
    .locals 8

    .prologue
    .line 69
    invoke-virtual {p0}, Lcom/google/android/location/os/i;->C()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/os/bi;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-wide v4, p4

    move v6, p6

    invoke-interface/range {v0 .. v6}, Lcom/google/android/location/os/bi;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JZ)V

    .line 70
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/util/Map;)V
    .locals 1

    .prologue
    .line 293
    invoke-virtual {p0}, Lcom/google/android/location/os/i;->C()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/os/bi;

    invoke-interface {v0, p1, p2}, Lcom/google/android/location/os/bi;->a(Ljava/lang/String;Ljava/util/Map;)V

    .line 294
    return-void
.end method

.method public final a(I)Z
    .locals 1

    .prologue
    .line 188
    invoke-virtual {p0}, Lcom/google/android/location/os/i;->C()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/os/bi;

    invoke-interface {v0, p1}, Lcom/google/android/location/os/bi;->a(I)Z

    move-result v0

    return v0
.end method

.method public final a(Lcom/google/android/location/collectionlib/cg;)Z
    .locals 1

    .prologue
    .line 183
    invoke-virtual {p0}, Lcom/google/android/location/os/i;->C()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/os/bi;

    invoke-interface {v0, p1}, Lcom/google/android/location/os/bi;->a(Lcom/google/android/location/collectionlib/cg;)Z

    move-result v0

    return v0
.end method

.method public final a(Lcom/google/android/location/os/bk;Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 263
    invoke-virtual {p0}, Lcom/google/android/location/os/i;->C()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/os/bi;

    invoke-interface {v0, p1, p2}, Lcom/google/android/location/os/bi;->a(Lcom/google/android/location/os/bk;Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final a(Ljava/lang/String;Lcom/google/android/location/collectionlib/cg;ILjava/lang/String;)Z
    .locals 1

    .prologue
    .line 194
    invoke-virtual {p0}, Lcom/google/android/location/os/i;->C()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/os/bi;

    invoke-interface {v0, p1, p2, p3, p4}, Lcom/google/android/location/os/bi;->a(Ljava/lang/String;Lcom/google/android/location/collectionlib/cg;ILjava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 63
    invoke-virtual {p0}, Lcom/google/android/location/os/i;->C()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/os/bi;

    invoke-interface {v0}, Lcom/google/android/location/os/bi;->b()V

    .line 64
    return-void
.end method

.method public final b(J)V
    .locals 1

    .prologue
    .line 333
    invoke-virtual {p0}, Lcom/google/android/location/os/i;->C()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/os/bi;

    invoke-interface {v0, p1, p2}, Lcom/google/android/location/os/bi;->b(J)V

    .line 334
    return-void
.end method

.method public final b(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 288
    invoke-virtual {p0}, Lcom/google/android/location/os/i;->C()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/os/bi;

    invoke-interface {v0, p1}, Lcom/google/android/location/os/bi;->b(Ljava/lang/String;)V

    .line 289
    return-void
.end method

.method public final c()Lcom/google/android/location/j/b;
    .locals 1

    .prologue
    .line 79
    invoke-virtual {p0}, Lcom/google/android/location/os/i;->C()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/os/bi;

    invoke-interface {v0}, Lcom/google/android/location/os/bi;->c()Lcom/google/android/location/j/b;

    move-result-object v0

    return-object v0
.end method

.method public final d()Lcom/google/android/location/j/d;
    .locals 1

    .prologue
    .line 84
    invoke-virtual {p0}, Lcom/google/android/location/os/i;->C()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/os/bi;

    invoke-interface {v0}, Lcom/google/android/location/os/bi;->d()Lcom/google/android/location/j/d;

    move-result-object v0

    return-object v0
.end method

.method public final e()Lcom/google/android/location/j/e;
    .locals 1

    .prologue
    .line 89
    invoke-virtual {p0}, Lcom/google/android/location/os/i;->C()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/os/bi;

    invoke-interface {v0}, Lcom/google/android/location/os/bi;->e()Lcom/google/android/location/j/e;

    move-result-object v0

    return-object v0
.end method

.method public final f()Lcom/google/android/location/j/j;
    .locals 1

    .prologue
    .line 94
    invoke-virtual {p0}, Lcom/google/android/location/os/i;->C()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/os/bi;

    invoke-interface {v0}, Lcom/google/android/location/os/bi;->f()Lcom/google/android/location/j/j;

    move-result-object v0

    return-object v0
.end method

.method public final g()Lcom/google/android/location/os/bn;
    .locals 1

    .prologue
    .line 108
    invoke-virtual {p0}, Lcom/google/android/location/os/i;->C()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/os/bi;

    invoke-interface {v0}, Lcom/google/android/location/os/bi;->g()Lcom/google/android/location/os/bn;

    move-result-object v0

    return-object v0
.end method

.method public final h()Z
    .locals 1

    .prologue
    .line 118
    invoke-virtual {p0}, Lcom/google/android/location/os/i;->C()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/os/bi;

    invoke-interface {v0}, Lcom/google/android/location/os/bi;->h()Z

    move-result v0

    return v0
.end method

.method public final i()Lcom/google/android/location/os/aw;
    .locals 1

    .prologue
    .line 133
    invoke-virtual {p0}, Lcom/google/android/location/os/i;->C()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/os/bi;

    invoke-interface {v0}, Lcom/google/android/location/os/bi;->i()Lcom/google/android/location/os/aw;

    move-result-object v0

    return-object v0
.end method

.method public final j()Lcom/google/android/location/os/bj;
    .locals 1

    .prologue
    .line 143
    invoke-virtual {p0}, Lcom/google/android/location/os/i;->C()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/os/bi;

    invoke-interface {v0}, Lcom/google/android/location/os/bi;->j()Lcom/google/android/location/os/bj;

    move-result-object v0

    return-object v0
.end method

.method public final k()Ljava/lang/String;
    .locals 1

    .prologue
    .line 148
    invoke-virtual {p0}, Lcom/google/android/location/os/i;->C()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/os/bi;

    invoke-interface {v0}, Lcom/google/android/location/os/bi;->k()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final l()Ljava/lang/String;
    .locals 1

    .prologue
    .line 153
    invoke-virtual {p0}, Lcom/google/android/location/os/i;->C()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/os/bi;

    invoke-interface {v0}, Lcom/google/android/location/os/bi;->l()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final m()Z
    .locals 1

    .prologue
    .line 178
    invoke-virtual {p0}, Lcom/google/android/location/os/i;->C()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/os/bi;

    invoke-interface {v0}, Lcom/google/android/location/os/bi;->m()Z

    move-result v0

    return v0
.end method

.method public final n()Ljava/io/File;
    .locals 1

    .prologue
    .line 204
    invoke-virtual {p0}, Lcom/google/android/location/os/i;->C()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/os/bi;

    invoke-interface {v0}, Lcom/google/android/location/os/bi;->n()Ljava/io/File;

    move-result-object v0

    return-object v0
.end method

.method public final o()Ljava/io/File;
    .locals 1

    .prologue
    .line 209
    invoke-virtual {p0}, Lcom/google/android/location/os/i;->C()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/os/bi;

    invoke-interface {v0}, Lcom/google/android/location/os/bi;->o()Ljava/io/File;

    move-result-object v0

    return-object v0
.end method

.method public final p()Ljava/io/File;
    .locals 1

    .prologue
    .line 214
    invoke-virtual {p0}, Lcom/google/android/location/os/i;->C()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/os/bi;

    invoke-interface {v0}, Lcom/google/android/location/os/bi;->p()Ljava/io/File;

    move-result-object v0

    return-object v0
.end method

.method public final q()I
    .locals 1

    .prologue
    .line 225
    invoke-virtual {p0}, Lcom/google/android/location/os/i;->C()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/os/bi;

    invoke-interface {v0}, Lcom/google/android/location/os/bi;->q()I

    move-result v0

    return v0
.end method

.method public final r()I
    .locals 1

    .prologue
    .line 230
    invoke-virtual {p0}, Lcom/google/android/location/os/i;->C()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/os/bi;

    invoke-interface {v0}, Lcom/google/android/location/os/bi;->r()I

    move-result v0

    return v0
.end method

.method public final s()Z
    .locals 1

    .prologue
    .line 245
    invoke-virtual {p0}, Lcom/google/android/location/os/i;->C()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/os/bi;

    invoke-interface {v0}, Lcom/google/android/location/os/bi;->s()Z

    move-result v0

    return v0
.end method

.method public final t()J
    .locals 2

    .prologue
    .line 258
    invoke-virtual {p0}, Lcom/google/android/location/os/i;->C()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/os/bi;

    invoke-interface {v0}, Lcom/google/android/location/os/bi;->t()J

    move-result-wide v0

    return-wide v0
.end method

.method public final u()Z
    .locals 1

    .prologue
    .line 268
    invoke-virtual {p0}, Lcom/google/android/location/os/i;->C()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/os/bi;

    invoke-interface {v0}, Lcom/google/android/location/os/bi;->u()Z

    move-result v0

    return v0
.end method

.method public final v()Lcom/google/android/location/os/j;
    .locals 1

    .prologue
    .line 273
    invoke-virtual {p0}, Lcom/google/android/location/os/i;->C()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/os/bi;

    invoke-interface {v0}, Lcom/google/android/location/os/bi;->v()Lcom/google/android/location/os/j;

    move-result-object v0

    return-object v0
.end method

.method public final w()Lcom/google/android/location/activity/at;
    .locals 1

    .prologue
    .line 278
    invoke-virtual {p0}, Lcom/google/android/location/os/i;->C()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/os/bi;

    invoke-interface {v0}, Lcom/google/android/location/os/bi;->w()Lcom/google/android/location/activity/at;

    move-result-object v0

    return-object v0
.end method

.method public final x()Lcom/google/android/location/activity/bn;
    .locals 1

    .prologue
    .line 283
    invoke-virtual {p0}, Lcom/google/android/location/os/i;->C()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/os/bi;

    invoke-interface {v0}, Lcom/google/android/location/os/bi;->x()Lcom/google/android/location/activity/bn;

    move-result-object v0

    return-object v0
.end method

.method public final y()Z
    .locals 1

    .prologue
    .line 313
    invoke-virtual {p0}, Lcom/google/android/location/os/i;->C()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/os/bi;

    invoke-interface {v0}, Lcom/google/android/location/os/bi;->y()Z

    move-result v0

    return v0
.end method

.method public final z()V
    .locals 1

    .prologue
    .line 323
    invoke-virtual {p0}, Lcom/google/android/location/os/i;->C()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/os/bi;

    invoke-interface {v0}, Lcom/google/android/location/os/bi;->z()V

    .line 324
    return-void
.end method
