.class public final Lcom/google/android/location/b/f;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final a:Ljava/io/File;

.field final b:Lcom/google/android/location/e/a;

.field final c:Lcom/google/android/location/j/e;

.field final d:S

.field final e:Lcom/google/android/location/b/bd;

.field final f:Lcom/google/android/location/b/j;

.field final g:Lcom/google/android/location/b/i;

.field private h:Lcom/google/android/location/j/i;

.field private final i:Ljava/lang/String;

.field private final j:Lcom/google/android/location/b/bi;

.field private final k:Ljava/util/Random;


# direct methods
.method public constructor <init>(IILcom/google/android/location/j/i;Lcom/google/android/location/b/i;Lcom/google/android/location/b/bi;Ljava/io/File;Ljava/lang/String;[BLcom/google/android/location/j/e;Ljava/util/Random;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 159
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 160
    iput-object p10, p0, Lcom/google/android/location/b/f;->k:Ljava/util/Random;

    .line 161
    iput-object p4, p0, Lcom/google/android/location/b/f;->g:Lcom/google/android/location/b/i;

    .line 163
    iput-object p6, p0, Lcom/google/android/location/b/f;->a:Ljava/io/File;

    .line 164
    if-nez p8, :cond_0

    .line 165
    iput-object v1, p0, Lcom/google/android/location/b/f;->b:Lcom/google/android/location/e/a;

    .line 169
    :goto_0
    iput-object p3, p0, Lcom/google/android/location/b/f;->h:Lcom/google/android/location/j/i;

    .line 171
    new-instance v0, Lcom/google/android/location/b/bd;

    invoke-direct {v0, p1}, Lcom/google/android/location/b/bd;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/location/b/f;->e:Lcom/google/android/location/b/bd;

    .line 173
    iget-object v0, p0, Lcom/google/android/location/b/f;->b:Lcom/google/android/location/e/a;

    if-eqz v0, :cond_1

    .line 174
    new-instance v0, Lcom/google/android/location/b/j;

    iget-object v1, p0, Lcom/google/android/location/b/f;->h:Lcom/google/android/location/j/i;

    invoke-direct {v0, p2, v1, p6}, Lcom/google/android/location/b/j;-><init>(ILjava/util/concurrent/ExecutorService;Ljava/io/File;)V

    iput-object v0, p0, Lcom/google/android/location/b/f;->f:Lcom/google/android/location/b/j;

    .line 179
    :goto_1
    iput-object p5, p0, Lcom/google/android/location/b/f;->j:Lcom/google/android/location/b/bi;

    .line 180
    iput-object p9, p0, Lcom/google/android/location/b/f;->c:Lcom/google/android/location/j/e;

    .line 181
    iput-object p7, p0, Lcom/google/android/location/b/f;->i:Ljava/lang/String;

    .line 182
    const/4 v0, 0x1

    iput-short v0, p0, Lcom/google/android/location/b/f;->d:S

    .line 183
    return-void

    .line 167
    :cond_0
    invoke-static {p8, v1}, Lcom/google/android/location/e/a;->a([BLcom/google/android/location/o/a/c;)Lcom/google/android/location/e/a;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/b/f;->b:Lcom/google/android/location/e/a;

    goto :goto_0

    .line 176
    :cond_1
    iput-object v1, p0, Lcom/google/android/location/b/f;->f:Lcom/google/android/location/b/j;

    goto :goto_1
.end method

.method static a(Ljava/io/Closeable;)V
    .locals 1

    .prologue
    .line 625
    if-eqz p0, :cond_0

    .line 627
    :try_start_0
    invoke-interface {p0}, Ljava/io/Closeable;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 632
    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method private d()V
    .locals 4

    .prologue
    .line 613
    iget-object v0, p0, Lcom/google/android/location/b/f;->a:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v1

    .line 614
    if-eqz v1, :cond_0

    .line 615
    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    .line 616
    invoke-virtual {v3}, Ljava/io/File;->delete()Z

    .line 615
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 619
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;J)Ljava/lang/Object;
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 287
    monitor-enter p0

    .line 288
    :try_start_0
    iget-object v0, p0, Lcom/google/android/location/b/f;->e:Lcom/google/android/location/b/bd;

    invoke-virtual {v0, p1}, Lcom/google/android/location/b/bd;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/e/ax;

    .line 289
    iget-object v1, p0, Lcom/google/android/location/b/f;->b:Lcom/google/android/location/e/a;

    if-eqz v1, :cond_1

    .line 290
    iget-object v1, p0, Lcom/google/android/location/b/f;->f:Lcom/google/android/location/b/j;

    invoke-virtual {v1, p1}, Lcom/google/android/location/b/j;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/location/e/ax;

    .line 294
    :goto_0
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 296
    if-eqz v0, :cond_2

    .line 299
    iget-object v2, p0, Lcom/google/android/location/b/f;->b:Lcom/google/android/location/e/a;

    if-eqz v2, :cond_0

    .line 301
    iput-wide p2, v1, Lcom/google/android/location/e/ax;->c:J

    .line 303
    :cond_0
    invoke-virtual {v0, p2, p3}, Lcom/google/android/location/e/ax;->a(J)Ljava/lang/Object;

    move-result-object v0

    .line 371
    :goto_1
    return-object v0

    :cond_1
    move-object v1, v2

    .line 292
    goto :goto_0

    .line 294
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 304
    :cond_2
    if-eqz v1, :cond_4

    .line 308
    const-string v0, ""

    iget-object v3, v1, Lcom/google/android/location/e/ax;->a:Ljava/lang/Object;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 309
    new-instance v0, Lcom/google/android/location/e/ax;

    iget-wide v4, v1, Lcom/google/android/location/e/ax;->b:J

    invoke-direct {v0, v2, v4, v5}, Lcom/google/android/location/e/ax;-><init>(Ljava/lang/Object;J)V

    .line 312
    monitor-enter p0

    .line 313
    :try_start_1
    iget-object v1, p0, Lcom/google/android/location/b/f;->e:Lcom/google/android/location/b/bd;

    invoke-virtual {v1, p1, v0}, Lcom/google/android/location/b/bd;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 314
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-object v0, v2

    .line 316
    goto :goto_1

    .line 314
    :catchall_1
    move-exception v0

    monitor-exit p0

    throw v0

    .line 320
    :cond_3
    iget-object v0, p0, Lcom/google/android/location/b/f;->h:Lcom/google/android/location/j/i;

    new-instance v3, Lcom/google/android/location/b/g;

    invoke-direct {v3, p0, p1, v1}, Lcom/google/android/location/b/g;-><init>(Lcom/google/android/location/b/f;Ljava/lang/Object;Lcom/google/android/location/e/ax;)V

    invoke-virtual {v0, v3}, Lcom/google/android/location/j/i;->submit(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    move-object v0, v2

    .line 369
    goto :goto_1

    :cond_4
    move-object v0, v2

    .line 371
    goto :goto_1
.end method

.method public final declared-synchronized a()V
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 488
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/location/b/f;->b:Lcom/google/android/location/e/a;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_1

    .line 546
    :cond_0
    monitor-exit p0

    return-void

    .line 493
    :cond_1
    :try_start_1
    new-instance v3, Ljava/io/File;

    iget-object v0, p0, Lcom/google/android/location/b/f;->a:Ljava/io/File;

    iget-object v1, p0, Lcom/google/android/location/b/f;->i:Ljava/lang/String;

    invoke-direct {v3, v0, v1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 494
    const/4 v0, 0x0

    .line 497
    :try_start_2
    new-instance v1, Ljava/io/DataInputStream;

    new-instance v4, Ljava/io/FileInputStream;

    invoke-direct {v4, v3}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    invoke-direct {v1, v4}, Ljava/io/DataInputStream;-><init>(Ljava/io/InputStream;)V
    :try_end_2
    .catch Ljava/io/FileNotFoundException; {:try_start_2 .. :try_end_2} :catch_3
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 498
    :try_start_3
    invoke-virtual {v1}, Ljava/io/DataInputStream;->readUnsignedShort()I

    move-result v0

    .line 500
    iget-short v3, p0, Lcom/google/android/location/b/f;->d:S

    if-eq v0, v3, :cond_2

    .line 502
    invoke-direct {p0}, Lcom/google/android/location/b/f;->d()V
    :try_end_3
    .catch Ljava/io/FileNotFoundException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    .line 517
    :goto_0
    :try_start_4
    invoke-static {v1}, Lcom/google/android/location/b/f;->a(Ljava/io/Closeable;)V

    .line 522
    :goto_1
    iget-object v0, p0, Lcom/google/android/location/b/f;->e:Lcom/google/android/location/b/bd;

    invoke-virtual {v0}, Lcom/google/android/location/b/bd;->size()I

    move-result v0

    iget-object v1, p0, Lcom/google/android/location/b/f;->f:Lcom/google/android/location/b/j;

    invoke-virtual {v1}, Lcom/google/android/location/b/j;->size()I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 523
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 525
    const/4 v0, 0x0

    iget-object v3, p0, Lcom/google/android/location/b/f;->f:Lcom/google/android/location/b/j;

    invoke-virtual {v3}, Lcom/google/android/location/b/j;->size()I

    move-result v3

    iget-object v4, p0, Lcom/google/android/location/b/f;->e:Lcom/google/android/location/b/bd;

    invoke-virtual {v4}, Lcom/google/android/location/b/bd;->a()I

    move-result v4

    sub-int/2addr v3, v4

    invoke-static {v0, v3}, Ljava/lang/Math;->max(II)I

    move-result v3

    .line 532
    iget-object v0, p0, Lcom/google/android/location/b/f;->f:Lcom/google/android/location/b/j;

    invoke-virtual {v0}, Lcom/google/android/location/b/j;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move v0, v2

    .line 533
    :goto_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 534
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    move-result-object v2

    .line 535
    if-ge v0, v3, :cond_3

    .line 536
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 504
    :cond_2
    :try_start_5
    iget-object v0, p0, Lcom/google/android/location/b/f;->b:Lcom/google/android/location/e/a;

    invoke-virtual {v0, v1}, Lcom/google/android/location/e/a;->a(Ljava/io/DataInputStream;)Lcom/google/android/location/e/aj;

    move-result-object v0

    .line 505
    iget-object v3, p0, Lcom/google/android/location/b/f;->j:Lcom/google/android/location/b/bi;

    iget-object v4, p0, Lcom/google/android/location/b/f;->f:Lcom/google/android/location/b/j;

    new-instance v5, Ljava/io/ByteArrayInputStream;

    iget-object v0, v0, Lcom/google/android/location/e/aj;->b:Ljava/lang/Object;

    check-cast v0, [B

    invoke-direct {v5, v0}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    invoke-interface {v3, v4, v5}, Lcom/google/android/location/b/bi;->a(Lcom/google/android/location/b/bd;Ljava/io/InputStream;)V
    :try_end_5
    .catch Ljava/io/FileNotFoundException; {:try_start_5 .. :try_end_5} :catch_0
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_2
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    goto :goto_0

    .line 510
    :catch_0
    move-exception v0

    move-object v0, v1

    :goto_3
    :try_start_6
    invoke-direct {p0}, Lcom/google/android/location/b/f;->d()V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_3

    .line 517
    :try_start_7
    invoke-static {v0}, Lcom/google/android/location/b/f;->a(Ljava/io/Closeable;)V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    goto :goto_1

    .line 488
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 513
    :catch_1
    move-exception v1

    move-object v1, v0

    :goto_4
    :try_start_8
    iget-object v0, p0, Lcom/google/android/location/b/f;->f:Lcom/google/android/location/b/j;

    invoke-virtual {v0}, Lcom/google/android/location/b/j;->clear()V

    .line 515
    invoke-direct {p0}, Lcom/google/android/location/b/f;->d()V
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_2

    .line 517
    :try_start_9
    invoke-static {v1}, Lcom/google/android/location/b/f;->a(Ljava/io/Closeable;)V

    goto :goto_1

    :catchall_1
    move-exception v1

    move-object v6, v1

    move-object v1, v0

    move-object v0, v6

    :goto_5
    invoke-static {v1}, Lcom/google/android/location/b/f;->a(Ljava/io/Closeable;)V

    throw v0

    .line 538
    :cond_3
    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 543
    :cond_4
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_6
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 544
    const-wide/16 v2, 0x0

    invoke-virtual {p0, v1, v2, v3}, Lcom/google/android/location/b/f;->a(Ljava/lang/Object;J)Ljava/lang/Object;
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    goto :goto_6

    .line 517
    :catchall_2
    move-exception v0

    goto :goto_5

    :catchall_3
    move-exception v1

    move-object v6, v1

    move-object v1, v0

    move-object v0, v6

    goto :goto_5

    .line 513
    :catch_2
    move-exception v0

    goto :goto_4

    .line 510
    :catch_3
    move-exception v1

    goto :goto_3
.end method

.method public final declared-synchronized a(JJ)V
    .locals 1

    .prologue
    .line 475
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/location/b/f;->e:Lcom/google/android/location/b/bd;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/google/android/location/b/bd;->a(JJ)V

    .line 477
    iget-object v0, p0, Lcom/google/android/location/b/f;->b:Lcom/google/android/location/e/a;

    if-eqz v0, :cond_0

    .line 480
    iget-object v0, p0, Lcom/google/android/location/b/f;->f:Lcom/google/android/location/b/j;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/google/android/location/b/j;->a(JJ)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 482
    :cond_0
    monitor-exit p0

    return-void

    .line 475
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final a(Ljava/lang/Object;Ljava/lang/Object;J)V
    .locals 7

    .prologue
    .line 384
    iget-object v0, p0, Lcom/google/android/location/b/f;->b:Lcom/google/android/location/e/a;

    if-nez v0, :cond_0

    .line 385
    new-instance v0, Lcom/google/android/location/e/ax;

    invoke-direct {v0, p2, p3, p4}, Lcom/google/android/location/e/ax;-><init>(Ljava/lang/Object;J)V

    .line 386
    iget-object v1, p0, Lcom/google/android/location/b/f;->e:Lcom/google/android/location/b/bd;

    invoke-virtual {v1, p1, v0}, Lcom/google/android/location/b/bd;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 464
    :goto_0
    return-void

    .line 391
    :cond_0
    new-instance v1, Lcom/google/android/location/e/ax;

    invoke-direct {v1, p2, p3, p4}, Lcom/google/android/location/e/ax;-><init>(Ljava/lang/Object;J)V

    .line 394
    new-instance v4, Lcom/google/android/location/e/ax;

    if-nez p2, :cond_1

    const-string v0, ""

    :goto_1
    invoke-direct {v4, v0, p3, p4}, Lcom/google/android/location/e/ax;-><init>(Ljava/lang/Object;J)V

    .line 401
    monitor-enter p0

    .line 402
    :try_start_0
    iget-object v0, p0, Lcom/google/android/location/b/f;->f:Lcom/google/android/location/b/j;

    invoke-virtual {v0, p1}, Lcom/google/android/location/b/j;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/location/e/ax;

    .line 404
    iget-object v0, p0, Lcom/google/android/location/b/f;->e:Lcom/google/android/location/b/bd;

    invoke-virtual {v0, p1, v1}, Lcom/google/android/location/b/bd;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 405
    iget-object v0, p0, Lcom/google/android/location/b/f;->f:Lcom/google/android/location/b/j;

    invoke-virtual {v0, p1, v4}, Lcom/google/android/location/b/j;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 408
    if-nez p2, :cond_2

    if-nez v2, :cond_2

    .line 409
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 464
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 394
    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {p3, p4}, Ljava/lang/Long;->toHexString(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/location/b/f;->k:Ljava/util/Random;

    invoke-virtual {v2}, Ljava/util/Random;->nextLong()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->toHexString(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ".cache"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 418
    :cond_2
    :try_start_1
    iget-object v6, p0, Lcom/google/android/location/b/f;->h:Lcom/google/android/location/j/i;

    new-instance v0, Lcom/google/android/location/b/h;

    move-object v1, p0

    move-object v3, p2

    move-object v5, p1

    invoke-direct/range {v0 .. v5}, Lcom/google/android/location/b/h;-><init>(Lcom/google/android/location/b/f;Lcom/google/android/location/e/ax;Ljava/lang/Object;Lcom/google/android/location/e/ax;Ljava/lang/Object;)V

    invoke-virtual {v6, v0}, Lcom/google/android/location/j/i;->submit(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    .line 464
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0
.end method

.method public final declared-synchronized a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 260
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/location/b/f;->b:Lcom/google/android/location/e/a;

    if-eqz v0, :cond_0

    .line 261
    iget-object v0, p0, Lcom/google/android/location/b/f;->f:Lcom/google/android/location/b/j;

    invoke-virtual {v0, p1}, Lcom/google/android/location/b/j;->containsKey(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    .line 263
    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/google/android/location/b/f;->e:Lcom/google/android/location/b/bd;

    invoke-virtual {v0, p1}, Lcom/google/android/location/b/bd;->containsKey(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v0

    goto :goto_0

    .line 260
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final b()V
    .locals 6

    .prologue
    .line 552
    iget-object v0, p0, Lcom/google/android/location/b/f;->b:Lcom/google/android/location/e/a;

    if-nez v0, :cond_0

    .line 587
    :goto_0
    return-void

    .line 556
    :cond_0
    new-instance v2, Ljava/io/File;

    iget-object v0, p0, Lcom/google/android/location/b/f;->a:Ljava/io/File;

    iget-object v1, p0, Lcom/google/android/location/b/f;->i:Ljava/lang/String;

    invoke-direct {v2, v0, v1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 557
    const/4 v0, 0x0

    .line 559
    :try_start_0
    invoke-virtual {v2}, Ljava/io/File;->createNewFile()Z

    .line 560
    iget-object v1, p0, Lcom/google/android/location/b/f;->c:Lcom/google/android/location/j/e;

    invoke-interface {v1, v2}, Lcom/google/android/location/j/e;->a(Ljava/io/File;)V

    .line 562
    new-instance v1, Ljava/io/DataOutputStream;

    new-instance v3, Ljava/io/FileOutputStream;

    invoke-direct {v3, v2}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    invoke-direct {v1, v3}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 564
    :try_start_1
    iget-short v0, p0, Lcom/google/android/location/b/f;->d:S

    invoke-virtual {v1, v0}, Ljava/io/DataOutputStream;->writeShort(I)V

    .line 567
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 568
    monitor-enter p0
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    .line 569
    :try_start_2
    iget-object v3, p0, Lcom/google/android/location/b/f;->j:Lcom/google/android/location/b/bi;

    iget-object v4, p0, Lcom/google/android/location/b/f;->f:Lcom/google/android/location/b/j;

    invoke-interface {v3, v4, v0}, Lcom/google/android/location/b/bi;->a(Lcom/google/android/location/b/bd;Ljava/io/OutputStream;)V

    .line 570
    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 571
    :try_start_3
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->close()V

    .line 574
    iget-object v3, p0, Lcom/google/android/location/b/f;->b:Lcom/google/android/location/e/a;

    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v0

    invoke-virtual {v3, v1, v0}, Lcom/google/android/location/e/a;->a(Ljava/io/DataOutputStream;[B)V
    :try_end_3
    .catch Ljava/io/FileNotFoundException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    .line 586
    invoke-static {v1}, Lcom/google/android/location/b/f;->a(Ljava/io/Closeable;)V

    goto :goto_0

    .line 570
    :catchall_0
    move-exception v0

    :try_start_4
    monitor-exit p0

    throw v0
    :try_end_4
    .catch Ljava/io/FileNotFoundException; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    .line 586
    :catch_0
    move-exception v0

    move-object v0, v1

    :goto_1
    invoke-static {v0}, Lcom/google/android/location/b/f;->a(Ljava/io/Closeable;)V

    goto :goto_0

    .line 581
    :catch_1
    move-exception v1

    move-object v1, v0

    :goto_2
    :try_start_5
    invoke-virtual {v2}, Ljava/io/File;->delete()Z
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 586
    invoke-static {v1}, Lcom/google/android/location/b/f;->a(Ljava/io/Closeable;)V

    goto :goto_0

    :catchall_1
    move-exception v1

    move-object v5, v1

    move-object v1, v0

    move-object v0, v5

    :goto_3
    invoke-static {v1}, Lcom/google/android/location/b/f;->a(Ljava/io/Closeable;)V

    throw v0

    :catchall_2
    move-exception v0

    goto :goto_3

    .line 581
    :catch_2
    move-exception v0

    goto :goto_2

    .line 586
    :catch_3
    move-exception v1

    goto :goto_1
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 604
    invoke-direct {p0}, Lcom/google/android/location/b/f;->d()V

    .line 605
    iget-object v0, p0, Lcom/google/android/location/b/f;->a:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    .line 606
    return-void
.end method
