.class public final Lcom/google/android/location/copresence/k;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static final A:Lcom/google/android/location/copresence/l;

.field public static final B:Lcom/google/android/location/copresence/l;

.field public static final C:Lcom/google/android/location/copresence/l;

.field public static final D:Lcom/google/android/location/copresence/l;

.field public static final E:Lcom/google/android/location/copresence/l;

.field public static final F:Lcom/google/android/location/copresence/l;

.field public static final G:Lcom/google/android/location/copresence/l;

.field public static final H:Lcom/google/android/location/copresence/l;

.field public static final I:Lcom/google/android/location/copresence/l;

.field public static final a:Lcom/google/android/location/copresence/l;

.field public static final b:Lcom/google/android/location/copresence/l;

.field public static final c:Lcom/google/android/location/copresence/l;

.field public static final d:Lcom/google/android/location/copresence/l;

.field public static final e:Lcom/google/android/location/copresence/l;

.field public static final f:Lcom/google/android/location/copresence/l;

.field public static final g:Lcom/google/android/location/copresence/l;

.field public static final h:Lcom/google/android/location/copresence/l;

.field public static final i:Lcom/google/android/location/copresence/l;

.field public static final j:Lcom/google/android/location/copresence/l;

.field public static final k:Lcom/google/android/location/copresence/l;

.field public static final l:Lcom/google/android/location/copresence/l;

.field public static final m:Lcom/google/android/location/copresence/l;

.field public static final n:Lcom/google/android/location/copresence/l;

.field public static final o:Lcom/google/android/location/copresence/l;

.field public static final p:Lcom/google/android/location/copresence/l;

.field public static final q:Lcom/google/android/location/copresence/l;

.field public static final r:Lcom/google/android/location/copresence/l;

.field public static final s:Lcom/google/android/location/copresence/l;

.field public static final t:Lcom/google/android/location/copresence/l;

.field public static final u:Lcom/google/android/location/copresence/l;

.field public static final v:Lcom/google/android/location/copresence/l;

.field public static final w:Lcom/google/android/location/copresence/l;

.field public static final x:Lcom/google/android/location/copresence/l;

.field public static final y:Lcom/google/android/location/copresence/l;

.field public static final z:Lcom/google/android/location/copresence/l;


# direct methods
.method static constructor <clinit>()V
    .locals 10

    .prologue
    const-wide v8, 0x7fffffffffffffffL

    const-wide/16 v6, 0x64

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 30
    const-string v0, "copresence:server_url"

    const-string v1, "https://www.googleapis.com"

    invoke-static {v0, v1}, Lcom/google/android/location/copresence/l;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/location/copresence/l;

    move-result-object v0

    sput-object v0, Lcom/google/android/location/copresence/k;->a:Lcom/google/android/location/copresence/l;

    .line 38
    const-string v0, "copresence:server_api_path"

    const-string v1, "/copresence/v2/copresence/"

    invoke-static {v0, v1}, Lcom/google/android/location/copresence/l;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/location/copresence/l;

    move-result-object v0

    sput-object v0, Lcom/google/android/location/copresence/k;->b:Lcom/google/android/location/copresence/l;

    .line 46
    const-string v0, "copresence:apiary_trace"

    const-string v1, ""

    invoke-static {v0, v1}, Lcom/google/android/location/copresence/l;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/location/copresence/l;

    move-result-object v0

    sput-object v0, Lcom/google/android/location/copresence/k;->c:Lcom/google/android/location/copresence/l;

    .line 52
    const-string v0, "copresence:server_api_scope"

    const-string v1, "https://www.googleapis.com/auth/social.copresence"

    invoke-static {v0, v1}, Lcom/google/android/location/copresence/l;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/location/copresence/l;

    move-result-object v0

    sput-object v0, Lcom/google/android/location/copresence/k;->d:Lcom/google/android/location/copresence/l;

    .line 61
    const-string v0, "copresence:audio_broadcaster_detection_upper_bound_millis"

    const-wide/16 v2, 0x3e8

    invoke-static {v0, v2, v3}, Lcom/google/android/location/copresence/l;->a(Ljava/lang/String;J)Lcom/google/android/location/copresence/l;

    move-result-object v0

    sput-object v0, Lcom/google/android/location/copresence/k;->e:Lcom/google/android/location/copresence/l;

    .line 69
    const-string v0, "copresence:audio_max_token_guesses"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/location/copresence/l;->a(Ljava/lang/String;I)Lcom/google/android/location/copresence/l;

    move-result-object v0

    sput-object v0, Lcom/google/android/location/copresence/k;->f:Lcom/google/android/location/copresence/l;

    .line 76
    const-string v0, "copresence:audio_best_decoding_group_size_threshold"

    invoke-static {v0, v4}, Lcom/google/android/location/copresence/l;->a(Ljava/lang/String;I)Lcom/google/android/location/copresence/l;

    move-result-object v0

    sput-object v0, Lcom/google/android/location/copresence/k;->g:Lcom/google/android/location/copresence/l;

    .line 83
    const-string v0, "copresence:requires_korean_tos_consent"

    invoke-static {v0, v4}, Lcom/google/android/location/copresence/l;->b(Ljava/lang/String;Z)Lcom/google/android/location/copresence/l;

    move-result-object v0

    sput-object v0, Lcom/google/android/location/copresence/k;->h:Lcom/google/android/location/copresence/l;

    .line 89
    const-string v0, "copresence:korean_location_tos_url"

    const-string v1, "http://www.google.com/intl/ko/policies/terms/location/"

    invoke-static {v0, v1}, Lcom/google/android/location/copresence/l;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/location/copresence/l;

    move-result-object v0

    sput-object v0, Lcom/google/android/location/copresence/k;->i:Lcom/google/android/location/copresence/l;

    .line 101
    const-string v0, "copresence:is_supported_geo"

    invoke-static {v0, v5}, Lcom/google/android/location/copresence/l;->b(Ljava/lang/String;Z)Lcom/google/android/location/copresence/l;

    move-result-object v0

    sput-object v0, Lcom/google/android/location/copresence/k;->j:Lcom/google/android/location/copresence/l;

    .line 107
    const-string v0, "copresence:audio_decoding_buffer_size_seconds"

    invoke-static {v0}, Lcom/google/android/location/copresence/l;->c(Ljava/lang/String;)Lcom/google/android/location/copresence/l;

    move-result-object v0

    sput-object v0, Lcom/google/android/location/copresence/k;->k:Lcom/google/android/location/copresence/l;

    .line 114
    const-string v0, "copresence:token_length_bytes"

    const/4 v1, 0x6

    invoke-static {v0, v1}, Lcom/google/android/location/copresence/l;->a(Ljava/lang/String;I)Lcom/google/android/location/copresence/l;

    move-result-object v0

    sput-object v0, Lcom/google/android/location/copresence/k;->l:Lcom/google/android/location/copresence/l;

    .line 120
    const-string v0, "copresence:audio_decoding_period_millis"

    const-wide/16 v2, 0x1f4

    invoke-static {v0, v2, v3}, Lcom/google/android/location/copresence/l;->a(Ljava/lang/String;J)Lcom/google/android/location/copresence/l;

    move-result-object v0

    sput-object v0, Lcom/google/android/location/copresence/k;->m:Lcom/google/android/location/copresence/l;

    .line 127
    const-string v0, "copresence:audio_detect_broadcaster_period_millis"

    invoke-static {v0, v6, v7}, Lcom/google/android/location/copresence/l;->a(Ljava/lang/String;J)Lcom/google/android/location/copresence/l;

    move-result-object v0

    sput-object v0, Lcom/google/android/location/copresence/k;->n:Lcom/google/android/location/copresence/l;

    .line 134
    const-string v0, "copresence:audio_processing_period_millis"

    invoke-static {v0, v6, v7}, Lcom/google/android/location/copresence/l;->a(Ljava/lang/String;J)Lcom/google/android/location/copresence/l;

    move-result-object v0

    sput-object v0, Lcom/google/android/location/copresence/k;->o:Lcom/google/android/location/copresence/l;

    .line 141
    const-string v0, "copresence:token_listener_record_stereo"

    invoke-static {v0, v4}, Lcom/google/android/location/copresence/l;->b(Ljava/lang/String;Z)Lcom/google/android/location/copresence/l;

    move-result-object v0

    sput-object v0, Lcom/google/android/location/copresence/k;->p:Lcom/google/android/location/copresence/l;

    .line 147
    const-string v0, "copresence:token_listener_sample_rate"

    const v1, 0xac44

    invoke-static {v0, v1}, Lcom/google/android/location/copresence/l;->a(Ljava/lang/String;I)Lcom/google/android/location/copresence/l;

    move-result-object v0

    sput-object v0, Lcom/google/android/location/copresence/k;->q:Lcom/google/android/location/copresence/l;

    .line 153
    const-string v0, "copresence:audio_broadcaster_stopped_threshold_millis"

    const/16 v1, 0x96

    invoke-static {v0, v1}, Lcom/google/android/location/copresence/l;->a(Ljava/lang/String;I)Lcom/google/android/location/copresence/l;

    move-result-object v0

    sput-object v0, Lcom/google/android/location/copresence/k;->r:Lcom/google/android/location/copresence/l;

    .line 157
    const-string v0, "copresence:token_listener_audio_source"

    const-string v1, "DEFAULT"

    invoke-static {v0, v1}, Lcom/google/android/location/copresence/l;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/location/copresence/l;

    move-result-object v0

    sput-object v0, Lcom/google/android/location/copresence/k;->s:Lcom/google/android/location/copresence/l;

    .line 160
    const-string v0, "copresence:token_listener_channel_config"

    const-string v1, "CHANNEL_IN_DEFAULT"

    invoke-static {v0, v1}, Lcom/google/android/location/copresence/l;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/location/copresence/l;

    move-result-object v0

    sput-object v0, Lcom/google/android/location/copresence/k;->t:Lcom/google/android/location/copresence/l;

    .line 167
    const-string v0, "copresence:should_log_token_audio"

    invoke-static {v0, v4}, Lcom/google/android/location/copresence/l;->b(Ljava/lang/String;Z)Lcom/google/android/location/copresence/l;

    move-result-object v0

    sput-object v0, Lcom/google/android/location/copresence/k;->u:Lcom/google/android/location/copresence/l;

    .line 174
    const-string v0, "copresence:should_log_recorded_audio"

    invoke-static {v0, v4}, Lcom/google/android/location/copresence/l;->b(Ljava/lang/String;Z)Lcom/google/android/location/copresence/l;

    move-result-object v0

    sput-object v0, Lcom/google/android/location/copresence/k;->v:Lcom/google/android/location/copresence/l;

    .line 181
    const-string v0, "copresence:should_log_decoded_tokens"

    invoke-static {v0, v4}, Lcom/google/android/location/copresence/l;->b(Ljava/lang/String;Z)Lcom/google/android/location/copresence/l;

    move-result-object v0

    sput-object v0, Lcom/google/android/location/copresence/k;->w:Lcom/google/android/location/copresence/l;

    .line 185
    const-string v0, "copresence:audio_diagnostic_storage_directory"

    const-string v1, "whispernet"

    invoke-static {v0, v1}, Lcom/google/android/location/copresence/l;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/location/copresence/l;

    move-result-object v0

    sput-object v0, Lcom/google/android/location/copresence/k;->x:Lcom/google/android/location/copresence/l;

    .line 194
    const-string v0, "copresence:audio_max_diagnostic_storage_bytes"

    const/high16 v1, 0xa00000

    invoke-static {v0, v1}, Lcom/google/android/location/copresence/l;->a(Ljava/lang/String;I)Lcom/google/android/location/copresence/l;

    move-result-object v0

    sput-object v0, Lcom/google/android/location/copresence/k;->y:Lcom/google/android/location/copresence/l;

    .line 202
    const-string v0, "copresence:audio_broadcast_repeats"

    const/4 v1, 0x5

    invoke-static {v0, v1}, Lcom/google/android/location/copresence/l;->a(Ljava/lang/String;I)Lcom/google/android/location/copresence/l;

    move-result-object v0

    sput-object v0, Lcom/google/android/location/copresence/k;->z:Lcom/google/android/location/copresence/l;

    .line 209
    const-string v0, "copresence:audio_broadcast_until_stopped"

    invoke-static {v0, v5}, Lcom/google/android/location/copresence/l;->b(Ljava/lang/String;Z)Lcom/google/android/location/copresence/l;

    move-result-object v0

    sput-object v0, Lcom/google/android/location/copresence/k;->A:Lcom/google/android/location/copresence/l;

    .line 215
    const-string v0, "copresence:audio_broadcast_force_volume"

    invoke-static {v0, v5}, Lcom/google/android/location/copresence/l;->b(Ljava/lang/String;Z)Lcom/google/android/location/copresence/l;

    move-result-object v0

    sput-object v0, Lcom/google/android/location/copresence/k;->B:Lcom/google/android/location/copresence/l;

    .line 221
    const-string v0, "copresence:audio_broadcast_until_stopped_timeout_millis"

    invoke-static {v0, v8, v9}, Lcom/google/android/location/copresence/l;->a(Ljava/lang/String;J)Lcom/google/android/location/copresence/l;

    move-result-object v0

    sput-object v0, Lcom/google/android/location/copresence/k;->C:Lcom/google/android/location/copresence/l;

    .line 228
    const-string v0, "copresence:audio_listen_timeout_millis"

    invoke-static {v0, v8, v9}, Lcom/google/android/location/copresence/l;->a(Ljava/lang/String;J)Lcom/google/android/location/copresence/l;

    move-result-object v0

    sput-object v0, Lcom/google/android/location/copresence/k;->D:Lcom/google/android/location/copresence/l;

    .line 232
    const-string v0, "copresence:audio_piped_input_stream_buffer_size"

    const/16 v1, 0x2000

    invoke-static {v0, v1}, Lcom/google/android/location/copresence/l;->a(Ljava/lang/String;I)Lcom/google/android/location/copresence/l;

    move-result-object v0

    sput-object v0, Lcom/google/android/location/copresence/k;->E:Lcom/google/android/location/copresence/l;

    .line 239
    const-string v0, "copresence:config_override_proto"

    const-string v1, ""

    invoke-static {v0, v1}, Lcom/google/android/location/copresence/l;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/location/copresence/l;

    move-result-object v0

    sput-object v0, Lcom/google/android/location/copresence/k;->F:Lcom/google/android/location/copresence/l;

    .line 243
    const-string v0, "copresence:allow_gcm_wakeup_pings"

    invoke-static {v0, v4}, Lcom/google/android/location/copresence/l;->b(Ljava/lang/String;Z)Lcom/google/android/location/copresence/l;

    move-result-object v0

    sput-object v0, Lcom/google/android/location/copresence/k;->G:Lcom/google/android/location/copresence/l;

    .line 247
    const-string v0, "copresence:allow_register_device_calls"

    invoke-static {v0, v4}, Lcom/google/android/location/copresence/l;->b(Ljava/lang/String;Z)Lcom/google/android/location/copresence/l;

    move-result-object v0

    sput-object v0, Lcom/google/android/location/copresence/k;->H:Lcom/google/android/location/copresence/l;

    .line 254
    const-string v0, "copresence:enable_dump_to_bug_report"

    invoke-static {v0, v4}, Lcom/google/android/location/copresence/l;->b(Ljava/lang/String;Z)Lcom/google/android/location/copresence/l;

    move-result-object v0

    sput-object v0, Lcom/google/android/location/copresence/k;->I:Lcom/google/android/location/copresence/l;

    return-void
.end method
