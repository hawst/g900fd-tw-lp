.class final Lcom/google/android/location/copresence/r/ai;
.super Landroid/content/BroadcastReceiver;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/location/copresence/r/ah;


# direct methods
.method constructor <init>(Lcom/google/android/location/copresence/r/ah;)V
    .locals 0

    .prologue
    .line 57
    iput-object p1, p0, Lcom/google/android/location/copresence/r/ai;->a:Lcom/google/android/location/copresence/r/ah;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public final onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 3

    .prologue
    const/4 v2, 0x2

    .line 60
    if-nez p2, :cond_2

    const/4 v0, 0x0

    .line 61
    :goto_0
    const-string v1, "android.net.wifi.p2p.THIS_DEVICE_CHANGED"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 62
    iget-object v1, p0, Lcom/google/android/location/copresence/r/ai;->a:Lcom/google/android/location/copresence/r/ah;

    const-string v0, "wifiP2pDevice"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/wifi/p2p/WifiP2pDevice;

    iput-object v0, v1, Lcom/google/android/location/copresence/r/ah;->b:Landroid/net/wifi/p2p/WifiP2pDevice;

    .line 64
    invoke-static {v2}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 65
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "WifiP2pManager: Wifi P2p this device changed action: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/location/copresence/r/ai;->a:Lcom/google/android/location/copresence/r/ah;

    iget-object v1, v1, Lcom/google/android/location/copresence/r/ah;->b:Landroid/net/wifi/p2p/WifiP2pDevice;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->a(Ljava/lang/String;)I

    .line 67
    :cond_0
    iget-object v0, p0, Lcom/google/android/location/copresence/r/ai;->a:Lcom/google/android/location/copresence/r/ah;

    iget-object v0, v0, Lcom/google/android/location/copresence/r/ah;->f:Lcom/google/android/location/copresence/r/ak;

    if-eqz v0, :cond_1

    .line 68
    iget-object v0, p0, Lcom/google/android/location/copresence/r/ai;->a:Lcom/google/android/location/copresence/r/ah;

    iget-object v0, v0, Lcom/google/android/location/copresence/r/ah;->f:Lcom/google/android/location/copresence/r/ak;

    iget-object v1, p0, Lcom/google/android/location/copresence/r/ai;->a:Lcom/google/android/location/copresence/r/ah;

    iget-object v1, v1, Lcom/google/android/location/copresence/r/ah;->b:Landroid/net/wifi/p2p/WifiP2pDevice;

    invoke-virtual {v0, v1}, Lcom/google/android/location/copresence/r/ak;->a(Landroid/net/wifi/p2p/WifiP2pDevice;)V

    .line 83
    :cond_1
    :goto_1
    return-void

    .line 60
    :cond_2
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 70
    :cond_3
    const-string v1, "android.net.wifi.p2p.CONNECTION_STATE_CHANGE"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 71
    iget-object v1, p0, Lcom/google/android/location/copresence/r/ai;->a:Lcom/google/android/location/copresence/r/ah;

    const-string v0, "wifiP2pInfo"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/wifi/p2p/WifiP2pInfo;

    iput-object v0, v1, Lcom/google/android/location/copresence/r/ah;->c:Landroid/net/wifi/p2p/WifiP2pInfo;

    .line 73
    iget-object v1, p0, Lcom/google/android/location/copresence/r/ai;->a:Lcom/google/android/location/copresence/r/ah;

    const-string v0, "p2pGroupInfo"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/wifi/p2p/WifiP2pGroup;

    iput-object v0, v1, Lcom/google/android/location/copresence/r/ah;->d:Landroid/net/wifi/p2p/WifiP2pGroup;

    .line 75
    invoke-static {v2}, Lcom/google/android/location/copresence/ag;->a(I)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 76
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "WifiP2pManager: Wifi P2p Connection changed action. Info: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/location/copresence/r/ai;->a:Lcom/google/android/location/copresence/r/ah;

    iget-object v1, v1, Lcom/google/android/location/copresence/r/ah;->c:Landroid/net/wifi/p2p/WifiP2pInfo;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " Group: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/location/copresence/r/ai;->a:Lcom/google/android/location/copresence/r/ah;

    iget-object v1, v1, Lcom/google/android/location/copresence/r/ah;->d:Landroid/net/wifi/p2p/WifiP2pGroup;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/location/copresence/ag;->a(Ljava/lang/String;)I

    .line 79
    :cond_4
    iget-object v0, p0, Lcom/google/android/location/copresence/r/ai;->a:Lcom/google/android/location/copresence/r/ah;

    iget-object v0, v0, Lcom/google/android/location/copresence/r/ah;->f:Lcom/google/android/location/copresence/r/ak;

    if-eqz v0, :cond_1

    .line 80
    iget-object v0, p0, Lcom/google/android/location/copresence/r/ai;->a:Lcom/google/android/location/copresence/r/ah;

    iget-object v0, v0, Lcom/google/android/location/copresence/r/ah;->f:Lcom/google/android/location/copresence/r/ak;

    iget-object v1, p0, Lcom/google/android/location/copresence/r/ai;->a:Lcom/google/android/location/copresence/r/ah;

    iget-object v1, v1, Lcom/google/android/location/copresence/r/ah;->c:Landroid/net/wifi/p2p/WifiP2pInfo;

    invoke-virtual {v0, v1}, Lcom/google/android/location/copresence/r/ak;->a(Landroid/net/wifi/p2p/WifiP2pInfo;)V

    goto :goto_1
.end method
