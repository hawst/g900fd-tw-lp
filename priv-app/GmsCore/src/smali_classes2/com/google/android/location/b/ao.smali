.class public final Lcom/google/android/location/b/ao;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/location/e/aw;


# instance fields
.field public final a:Lcom/google/android/location/b/as;

.field public final b:Lcom/google/android/location/b/ap;

.field final c:Lcom/google/android/location/e/ah;

.field public final d:Lcom/google/android/location/j/b;

.field public e:J

.field f:Z

.field public final g:Lcom/google/android/location/e/au;


# direct methods
.method public constructor <init>(Lcom/google/android/location/j/b;Lcom/google/android/location/j/e;Lcom/google/android/location/j/d;Lcom/google/android/location/e/ah;)V
    .locals 6

    .prologue
    .line 116
    invoke-interface {p2}, Lcom/google/android/location/j/e;->a()Ljava/io/File;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v3, 0x0

    :goto_0
    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/location/b/ao;-><init>(Lcom/google/android/location/j/b;Lcom/google/android/location/j/e;Ljava/io/File;Lcom/google/android/location/j/d;Lcom/google/android/location/e/ah;)V

    .line 119
    return-void

    .line 116
    :cond_0
    new-instance v3, Ljava/io/File;

    invoke-interface {p2}, Lcom/google/android/location/j/e;->a()Ljava/io/File;

    move-result-object v0

    const-string v1, "nlp_devices"

    invoke-direct {v3, v0, v1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private constructor <init>(Lcom/google/android/location/j/b;Lcom/google/android/location/j/e;Ljava/io/File;Lcom/google/android/location/j/d;Lcom/google/android/location/e/ah;)V
    .locals 9

    .prologue
    .line 123
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 95
    new-instance v0, Lcom/google/android/location/b/as;

    invoke-direct {v0}, Lcom/google/android/location/b/as;-><init>()V

    iput-object v0, p0, Lcom/google/android/location/b/ao;->a:Lcom/google/android/location/b/as;

    .line 96
    new-instance v0, Lcom/google/android/location/b/ap;

    iget-object v1, p0, Lcom/google/android/location/b/ao;->a:Lcom/google/android/location/b/as;

    invoke-direct {v0, v1}, Lcom/google/android/location/b/ap;-><init>(Lcom/google/android/location/b/as;)V

    iput-object v0, p0, Lcom/google/android/location/b/ao;->b:Lcom/google/android/location/b/ap;

    .line 124
    iput-object p1, p0, Lcom/google/android/location/b/ao;->d:Lcom/google/android/location/j/b;

    .line 125
    iput-object p5, p0, Lcom/google/android/location/b/ao;->c:Lcom/google/android/location/e/ah;

    .line 126
    new-instance v0, Lcom/google/android/location/e/au;

    const/4 v1, 0x2

    invoke-interface {p4}, Lcom/google/android/location/j/d;->a()Ljavax/crypto/SecretKey;

    move-result-object v2

    const/4 v3, 0x3

    invoke-interface {p4}, Lcom/google/android/location/j/d;->c()[B

    move-result-object v4

    sget-object v5, Lcom/google/android/location/m/a;->bs:Lcom/google/p/a/b/b/c;

    move-object v6, p3

    move-object v7, p0

    move-object v8, p2

    invoke-direct/range {v0 .. v8}, Lcom/google/android/location/e/au;-><init>(ILjavax/crypto/SecretKey;I[BLcom/google/p/a/b/b/c;Ljava/io/File;Lcom/google/android/location/e/aw;Lcom/google/android/location/j/e;)V

    iput-object v0, p0, Lcom/google/android/location/b/ao;->g:Lcom/google/android/location/e/au;

    .line 133
    invoke-virtual {p0}, Lcom/google/android/location/b/ao;->b()V

    .line 134
    return-void
.end method


# virtual methods
.method public final a(JJ)Lcom/google/android/location/b/aq;
    .locals 3

    .prologue
    .line 276
    iget-object v0, p0, Lcom/google/android/location/b/ao;->b:Lcom/google/android/location/b/ap;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/location/b/ap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/b/aq;

    .line 277
    if-nez v0, :cond_1

    .line 278
    new-instance v0, Lcom/google/p/a/b/b/a;

    sget-object v1, Lcom/google/android/location/m/a;->bt:Lcom/google/p/a/b/b/c;

    invoke-direct {v0, v1}, Lcom/google/p/a/b/b/a;-><init>(Lcom/google/p/a/b/b/c;)V

    .line 279
    const/4 v1, 0x1

    invoke-virtual {v0, v1, p1, p2}, Lcom/google/p/a/b/b/a;->b(IJ)Lcom/google/p/a/b/b/a;

    .line 280
    new-instance v1, Lcom/google/android/location/b/aq;

    invoke-direct {v1, v0}, Lcom/google/android/location/b/aq;-><init>(Lcom/google/p/a/b/b/a;)V

    .line 281
    iget-object v0, p0, Lcom/google/android/location/b/ao;->b:Lcom/google/android/location/b/ap;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v2, v1}, Lcom/google/android/location/b/ap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/b/aq;

    .line 282
    if-nez v0, :cond_0

    .line 284
    iget-object v0, p0, Lcom/google/android/location/b/ao;->a:Lcom/google/android/location/b/as;

    const/4 v2, 0x5

    invoke-virtual {v0, v2}, Lcom/google/android/location/b/as;->a(I)V

    :cond_0
    move-object v0, v1

    .line 287
    :cond_1
    invoke-static {p3, p4}, Lcom/google/android/location/b/aq;->b(J)S

    move-result v1

    iput-short v1, v0, Lcom/google/android/location/b/aq;->a:S

    .line 288
    return-object v0
.end method

.method public final a()Lcom/google/p/a/b/b/a;
    .locals 12

    .prologue
    const/4 v11, 0x4

    const/4 v10, 0x3

    const/4 v9, 0x2

    const/4 v8, 0x1

    const/high16 v7, -0x40800000    # -1.0f

    .line 418
    new-instance v2, Lcom/google/p/a/b/b/a;

    sget-object v0, Lcom/google/android/location/m/a;->bs:Lcom/google/p/a/b/b/c;

    invoke-direct {v2, v0}, Lcom/google/p/a/b/b/a;-><init>(Lcom/google/p/a/b/b/c;)V

    .line 419
    iget-object v0, p0, Lcom/google/android/location/b/ao;->b:Lcom/google/android/location/b/ap;

    invoke-virtual {v0}, Lcom/google/android/location/b/ap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .line 420
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 421
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 422
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    .line 423
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/b/aq;

    .line 425
    new-instance v1, Lcom/google/p/a/b/b/a;

    sget-object v6, Lcom/google/android/location/m/a;->bt:Lcom/google/p/a/b/b/c;

    invoke-direct {v1, v6}, Lcom/google/p/a/b/b/a;-><init>(Lcom/google/p/a/b/b/c;)V

    invoke-virtual {v1, v8, v4, v5}, Lcom/google/p/a/b/b/a;->b(IJ)Lcom/google/p/a/b/b/a;

    iget-short v4, v0, Lcom/google/android/location/b/aq;->a:S

    const/16 v5, 0x7fff

    if-eq v4, v5, :cond_0

    iget-short v4, v0, Lcom/google/android/location/b/aq;->a:S

    invoke-virtual {v1, v10, v4}, Lcom/google/p/a/b/b/a;->f(II)Lcom/google/p/a/b/b/a;

    :cond_0
    iget v4, v0, Lcom/google/android/location/b/aq;->b:F

    cmpl-float v4, v4, v7

    if-eqz v4, :cond_1

    iget v4, v0, Lcom/google/android/location/b/aq;->b:F

    invoke-virtual {v1, v11, v4}, Lcom/google/p/a/b/b/a;->a(IF)Lcom/google/p/a/b/b/a;

    :cond_1
    iget v4, v0, Lcom/google/android/location/b/aq;->c:F

    cmpl-float v4, v4, v7

    if-eqz v4, :cond_2

    const/4 v4, 0x7

    iget v5, v0, Lcom/google/android/location/b/aq;->c:F

    invoke-virtual {v1, v4, v5}, Lcom/google/p/a/b/b/a;->a(IF)Lcom/google/p/a/b/b/a;

    :cond_2
    iget-short v4, v0, Lcom/google/android/location/b/aq;->d:S

    const/16 v5, 0x7fff

    if-eq v4, v5, :cond_3

    iget-short v4, v0, Lcom/google/android/location/b/aq;->d:S

    invoke-virtual {v1, v9, v4}, Lcom/google/p/a/b/b/a;->f(II)Lcom/google/p/a/b/b/a;

    :cond_3
    iget v4, v0, Lcom/google/android/location/b/aq;->e:I

    const/4 v5, -0x1

    if-eq v4, v5, :cond_4

    const/4 v4, 0x5

    iget v5, v0, Lcom/google/android/location/b/aq;->e:I

    invoke-virtual {v1, v4, v5}, Lcom/google/p/a/b/b/a;->f(II)Lcom/google/p/a/b/b/a;

    :cond_4
    iget v4, v0, Lcom/google/android/location/b/aq;->f:I

    if-eqz v4, :cond_5

    const/4 v4, 0x6

    iget v0, v0, Lcom/google/android/location/b/aq;->f:I

    invoke-virtual {v1, v4, v0}, Lcom/google/p/a/b/b/a;->f(II)Lcom/google/p/a/b/b/a;

    :cond_5
    invoke-virtual {v2, v10, v1}, Lcom/google/p/a/b/b/a;->a(ILcom/google/p/a/b/b/a;)V

    goto :goto_0

    .line 427
    :cond_6
    iget-object v0, p0, Lcom/google/android/location/b/ao;->d:Lcom/google/android/location/j/b;

    invoke-interface {v0}, Lcom/google/android/location/j/b;->d()J

    move-result-wide v0

    invoke-virtual {v2, v8, v0, v1}, Lcom/google/p/a/b/b/a;->b(IJ)Lcom/google/p/a/b/b/a;

    .line 428
    iget-wide v0, p0, Lcom/google/android/location/b/ao;->e:J

    invoke-virtual {v2, v9, v0, v1}, Lcom/google/p/a/b/b/a;->b(IJ)Lcom/google/p/a/b/b/a;

    .line 430
    iget-boolean v0, p0, Lcom/google/android/location/b/ao;->f:Z

    invoke-virtual {v2, v11, v0}, Lcom/google/p/a/b/b/a;->a(IZ)Lcom/google/p/a/b/b/a;

    .line 431
    return-object v2
.end method

.method public final a(JZ)V
    .locals 1

    .prologue
    .line 266
    iput-wide p1, p0, Lcom/google/android/location/b/ao;->e:J

    .line 267
    iput-boolean p3, p0, Lcom/google/android/location/b/ao;->f:Z

    .line 268
    return-void
.end method

.method public final a(Lcom/google/p/a/b/b/a;)Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 220
    invoke-virtual {p1, v0}, Lcom/google/p/a/b/b/a;->i(I)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x2

    invoke-virtual {p1, v1}, Lcom/google/p/a/b/b/a;->i(I)Z

    move-result v1

    if-eqz v1, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()V
    .locals 6

    .prologue
    const/4 v4, 0x1

    .line 441
    invoke-static {}, Ljava/lang/Math;->random()D

    move-result-wide v0

    const-wide v2, 0x4194997000000000L    # 8.64E7

    mul-double/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->floor(D)D

    move-result-wide v0

    double-to-long v0, v0

    .line 442
    iget-object v2, p0, Lcom/google/android/location/b/ao;->d:Lcom/google/android/location/j/b;

    invoke-interface {v2}, Lcom/google/android/location/j/b;->c()J

    move-result-wide v2

    sub-long v0, v2, v0

    invoke-virtual {p0, v0, v1, v4}, Lcom/google/android/location/b/ao;->a(JZ)V

    .line 443
    iget-object v0, p0, Lcom/google/android/location/b/ao;->b:Lcom/google/android/location/b/ap;

    invoke-virtual {v0}, Lcom/google/android/location/b/ap;->clear()V

    .line 444
    sget-boolean v0, Lcom/google/android/location/i/a;->b:Z

    if-eqz v0, :cond_0

    const-string v0, "SeenDevicesCache"

    const-string v1, "Clearing device cache, last refresh: %d"

    new-array v2, v4, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-wide v4, p0, Lcom/google/android/location/b/ao;->e:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/location/o/a/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 446
    :cond_0
    return-void
.end method

.method public final b(Lcom/google/p/a/b/b/a;)V
    .locals 14

    .prologue
    .line 384
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Lcom/google/p/a/b/b/a;->d(I)J

    move-result-wide v0

    .line 385
    const/4 v2, 0x2

    invoke-virtual {p1, v2}, Lcom/google/p/a/b/b/a;->d(I)J

    move-result-wide v2

    add-long v8, v2, v0

    .line 387
    iget-object v2, p0, Lcom/google/android/location/b/ao;->d:Lcom/google/android/location/j/b;

    invoke-interface {v2}, Lcom/google/android/location/j/b;->b()J

    move-result-wide v2

    .line 388
    iget-object v4, p0, Lcom/google/android/location/b/ao;->d:Lcom/google/android/location/j/b;

    invoke-interface {v4}, Lcom/google/android/location/j/b;->c()J

    move-result-wide v6

    .line 391
    sub-long v4, v2, v6

    .line 394
    cmp-long v10, v8, v2

    if-lez v10, :cond_3

    .line 396
    :goto_0
    sub-long/2addr v2, v4

    const/4 v8, 0x4

    invoke-virtual {p1, v8}, Lcom/google/p/a/b/b/a;->b(I)Z

    move-result v8

    invoke-virtual {p0, v2, v3, v8}, Lcom/google/android/location/b/ao;->a(JZ)V

    .line 398
    const/4 v2, 0x0

    move v8, v2

    :goto_1
    const/4 v2, 0x3

    invoke-virtual {p1, v2}, Lcom/google/p/a/b/b/a;->k(I)I

    move-result v2

    if-ge v8, v2, :cond_5

    .line 401
    const/4 v2, 0x3

    invoke-virtual {p1, v2, v8}, Lcom/google/p/a/b/b/a;->d(II)Lcom/google/p/a/b/b/a;

    move-result-object v9

    .line 402
    if-eqz v9, :cond_4

    invoke-virtual {v9}, Lcom/google/p/a/b/b/a;->d()Z

    move-result v2

    if-eqz v2, :cond_4

    const/4 v2, 0x1

    invoke-virtual {v9, v2}, Lcom/google/p/a/b/b/a;->i(I)Z

    move-result v2

    if-eqz v2, :cond_4

    const/4 v2, 0x1

    :goto_2
    if-eqz v2, :cond_2

    .line 406
    new-instance v10, Lcom/google/android/location/b/aq;

    invoke-direct {v10, v9}, Lcom/google/android/location/b/aq;-><init>(Lcom/google/p/a/b/b/a;)V

    .line 410
    iget-short v2, v10, Lcom/google/android/location/b/aq;->d:S

    const/16 v3, 0x7fff

    if-eq v2, v3, :cond_0

    iget-short v2, v10, Lcom/google/android/location/b/aq;->d:S

    int-to-long v2, v2

    const-wide/32 v12, 0x5265c00

    mul-long/2addr v2, v12

    invoke-static/range {v0 .. v7}, Lcom/google/android/location/b/aq;->a(JJJJ)J

    move-result-wide v2

    invoke-static {v2, v3}, Lcom/google/android/location/b/aq;->b(J)S

    move-result v2

    iput-short v2, v10, Lcom/google/android/location/b/aq;->d:S

    :cond_0
    iget-short v2, v10, Lcom/google/android/location/b/aq;->a:S

    const/16 v3, 0x7fff

    if-eq v2, v3, :cond_1

    iget-short v2, v10, Lcom/google/android/location/b/aq;->a:S

    int-to-long v2, v2

    const-wide/32 v12, 0x5265c00

    mul-long/2addr v2, v12

    invoke-static/range {v0 .. v7}, Lcom/google/android/location/b/aq;->a(JJJJ)J

    move-result-wide v2

    invoke-static {v2, v3}, Lcom/google/android/location/b/aq;->b(J)S

    move-result v2

    iput-short v2, v10, Lcom/google/android/location/b/aq;->a:S

    .line 411
    :cond_1
    const/4 v2, 0x1

    invoke-virtual {v9, v2}, Lcom/google/p/a/b/b/a;->d(I)J

    move-result-wide v2

    .line 412
    iget-object v9, p0, Lcom/google/android/location/b/ao;->b:Lcom/google/android/location/b/ap;

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v9, v2, v10}, Lcom/google/android/location/b/ap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 398
    :cond_2
    add-int/lit8 v2, v8, 0x1

    move v8, v2

    goto :goto_1

    :cond_3
    move-wide v2, v8

    .line 394
    goto :goto_0

    .line 402
    :cond_4
    const/4 v2, 0x0

    goto :goto_2

    .line 415
    :cond_5
    return-void
.end method

.method public final c()V
    .locals 12

    .prologue
    .line 454
    iget-object v0, p0, Lcom/google/android/location/b/ao;->d:Lcom/google/android/location/j/b;

    invoke-interface {v0}, Lcom/google/android/location/j/b;->c()J

    move-result-wide v0

    .line 455
    const-wide/32 v2, 0xa4cb800

    sub-long v2, v0, v2

    .line 456
    const-wide/32 v4, 0x240c8400

    sub-long v4, v0, v4

    .line 457
    iget-object v0, p0, Lcom/google/android/location/b/ao;->b:Lcom/google/android/location/b/ap;

    invoke-virtual {v0}, Lcom/google/android/location/b/ap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v6

    .line 458
    :cond_0
    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 459
    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 460
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/location/b/aq;

    iget-short v7, v1, Lcom/google/android/location/b/aq;->a:S

    .line 461
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/location/b/aq;

    iget-short v1, v1, Lcom/google/android/location/b/aq;->d:S

    .line 462
    const/16 v8, 0x7fff

    if-ne v7, v8, :cond_1

    .line 463
    invoke-interface {v6}, Ljava/util/Iterator;->remove()V

    goto :goto_0

    .line 466
    :cond_1
    int-to-long v8, v7

    const-wide/32 v10, 0x5265c00

    mul-long/2addr v8, v10

    cmp-long v7, v8, v4

    if-gez v7, :cond_2

    .line 467
    invoke-interface {v6}, Ljava/util/Iterator;->remove()V

    .line 468
    iget-object v1, p0, Lcom/google/android/location/b/ao;->a:Lcom/google/android/location/b/as;

    const/4 v7, 0x4

    invoke-virtual {v1, v7}, Lcom/google/android/location/b/as;->a(I)V

    .line 469
    sget-boolean v1, Lcom/google/android/location/i/a;->b:Z

    if-eqz v1, :cond_0

    const-string v1, "SeenDevicesCache"

    const-string v7, "Discarding %d because never seen for a long time."

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    aput-object v0, v8, v9

    invoke-static {v7, v8}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/google/android/location/o/a/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 473
    :cond_2
    const/16 v7, 0x7fff

    if-eq v1, v7, :cond_0

    int-to-long v8, v1

    const-wide/32 v10, 0x5265c00

    mul-long/2addr v8, v10

    cmp-long v1, v8, v2

    if-gez v1, :cond_0

    .line 477
    iget-object v1, p0, Lcom/google/android/location/b/ao;->a:Lcom/google/android/location/b/as;

    const/4 v7, 0x4

    invoke-virtual {v1, v7}, Lcom/google/android/location/b/as;->a(I)V

    .line 478
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/location/b/aq;

    const/16 v7, 0x7fff

    iput-short v7, v1, Lcom/google/android/location/b/aq;->d:S

    const/high16 v7, -0x40800000    # -1.0f

    iput v7, v1, Lcom/google/android/location/b/aq;->b:F

    const/high16 v7, -0x40800000    # -1.0f

    iput v7, v1, Lcom/google/android/location/b/aq;->c:F

    const/4 v7, -0x1

    iput v7, v1, Lcom/google/android/location/b/aq;->e:I

    .line 479
    sget-boolean v1, Lcom/google/android/location/i/a;->b:Z

    if-eqz v1, :cond_0

    const-string v1, "SeenDevicesCache"

    const-string v7, "Removing cache result of %d because it\'s too old."

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    aput-object v0, v8, v9

    invoke-static {v7, v8}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/google/android/location/o/a/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 483
    :cond_3
    return-void
.end method
