.class public Lcom/google/android/gms/icing/proxy/SmsContentProvider;
.super Lcom/google/android/gms/appdatasearch/f;
.source "SourceFile"


# static fields
.field static final a:Ljava/util/List;

.field static b:Z

.field private static final c:Ljava/lang/String;

.field private static final d:[Ljava/lang/String;

.field private static final e:[Ljava/lang/String;


# instance fields
.field private f:J

.field private g:Ljava/util/List;

.field private h:Ljava/util/List;

.field private final i:Ljava/lang/StringBuilder;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 48
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v3, " LIMIT 1000"

    invoke-direct {v0, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/icing/proxy/SmsContentProvider;->c:Ljava/lang/String;

    .line 49
    new-array v0, v1, [Ljava/lang/String;

    const-string v3, "_id"

    aput-object v3, v0, v2

    sput-object v0, Lcom/google/android/gms/icing/proxy/SmsContentProvider;->d:[Ljava/lang/String;

    .line 52
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/String;

    const-string v3, "date"

    aput-object v3, v0, v2

    const-string v3, "subject"

    aput-object v3, v0, v1

    const/4 v3, 0x2

    const-string v4, "body"

    aput-object v4, v0, v3

    const/4 v3, 0x3

    const-string v4, "address"

    aput-object v4, v0, v3

    const/4 v3, 0x4

    const-string v4, "type"

    aput-object v4, v0, v3

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    .line 58
    sput-object v0, Lcom/google/android/gms/icing/proxy/SmsContentProvider;->a:Ljava/util/List;

    invoke-static {v0}, Lcom/google/android/gms/appdatasearch/bd;->a(Ljava/lang/Iterable;)Ljava/util/List;

    move-result-object v0

    sget-object v3, Lcom/google/android/gms/icing/proxy/SmsContentProvider;->a:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    new-array v3, v3, [Ljava/lang/String;

    invoke-interface {v0, v3}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    sput-object v0, Lcom/google/android/gms/icing/proxy/SmsContentProvider;->e:[Ljava/lang/String;

    .line 63
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x13

    if-lt v0, v3, :cond_0

    move v0, v1

    :goto_0
    sput-boolean v0, Lcom/google/android/gms/icing/proxy/SmsContentProvider;->b:Z

    return-void

    :cond_0
    move v0, v2

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 71
    invoke-direct {p0}, Lcom/google/android/gms/appdatasearch/f;-><init>()V

    .line 72
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/icing/proxy/SmsContentProvider;->g:Ljava/util/List;

    .line 73
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/icing/proxy/SmsContentProvider;->h:Ljava/util/List;

    .line 74
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/icing/proxy/SmsContentProvider;->i:Ljava/lang/StringBuilder;

    .line 75
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/gms/icing/proxy/SmsContentProvider;->f:J

    .line 76
    return-void
.end method

.method private a(Landroid/database/MatrixCursor;Ljava/util/List;J)J
    .locals 9

    .prologue
    const-wide/16 v6, 0x0

    const/4 v8, 0x0

    .line 298
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v0

    if-gtz v0, :cond_1

    .line 299
    const-string v0, "SmsContentProvider: Size of ids is 0 in addRows"

    invoke-static {v0}, Lcom/google/android/gms/icing/c;->d(Ljava/lang/String;)I

    move-wide v0, v6

    .line 328
    :cond_0
    :goto_0
    return-wide v0

    .line 305
    :cond_1
    :try_start_0
    const-string v0, "_id IN (%s)"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-direct {p0, p2}, Lcom/google/android/gms/icing/proxy/SmsContentProvider;->a(Ljava/util/List;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    const-string v0, "_id"

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/google/android/gms/icing/proxy/SmsContentProvider;->a:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    invoke-virtual {p0}, Lcom/google/android/gms/icing/proxy/SmsContentProvider;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Landroid/provider/Telephony$Sms;->CONTENT_URI:Landroid/net/Uri;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v4

    new-array v4, v4, [Ljava/lang/String;

    invoke-interface {v2, v4}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string v5, "_id ASC "

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 306
    if-nez v2, :cond_4

    .line 307
    :try_start_1
    const-string v0, "SmsContentProvider: Getting null result when query SMS content provider."

    invoke-static {v0}, Lcom/google/android/gms/icing/c;->d(Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 308
    if-eqz v2, :cond_2

    .line 328
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    :cond_2
    move-wide v0, v6

    goto :goto_0

    :cond_3
    move-wide p3, v4

    .line 311
    :cond_4
    :try_start_2
    invoke-interface {v2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 312
    const-string v0, "_id"

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    .line 315
    invoke-virtual {p1}, Landroid/database/MatrixCursor;->newRow()Landroid/database/MatrixCursor$RowBuilder;

    move-result-object v3

    const-wide/16 v4, 0x1

    add-long/2addr v4, p3

    invoke-static {p3, p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v3, v6}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    move-result-object v3

    const-string v6, "add"

    invoke-virtual {v3, v6}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    move-result-object v3

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v3, v6}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    move-result-object v3

    const-wide/32 v6, 0x7fffffff

    sub-long v0, v6, v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v3, v0}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    move-result-object v1

    .line 321
    sget-object v0, Lcom/google/android/gms/icing/proxy/SmsContentProvider;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 322
    invoke-interface {v2, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    .line 327
    :catchall_0
    move-exception v0

    move-object v1, v2

    :goto_2
    if-eqz v1, :cond_5

    .line 328
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_5
    throw v0

    .line 325
    :cond_6
    :try_start_3
    invoke-interface {v2}, Landroid/database/Cursor;->getCount()I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result v0

    int-to-long v0, v0

    .line 327
    if-eqz v2, :cond_0

    .line 328
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    goto/16 :goto_0

    .line 327
    :catchall_1
    move-exception v0

    move-object v1, v8

    goto :goto_2
.end method

.method private a(Ljava/util/List;)Ljava/lang/String;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 345
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-gtz v0, :cond_0

    .line 346
    const-string v0, ""

    .line 354
    :goto_0
    return-object v0

    .line 348
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/icing/proxy/SmsContentProvider;->i:Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 349
    iget-object v0, p0, Lcom/google/android/gms/icing/proxy/SmsContentProvider;->i:Ljava/lang/StringBuilder;

    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 350
    const/4 v0, 0x1

    :goto_1
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_1

    .line 351
    iget-object v1, p0, Lcom/google/android/gms/icing/proxy/SmsContentProvider;->i:Ljava/lang/StringBuilder;

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 352
    iget-object v1, p0, Lcom/google/android/gms/icing/proxy/SmsContentProvider;->i:Ljava/lang/StringBuilder;

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 350
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 354
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/icing/proxy/SmsContentProvider;->i:Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 85
    const-string v0, "proxy-content-provider"

    invoke-virtual {p0, v0, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 87
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "diff-computed"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 88
    return-void
.end method

.method private static b(Landroid/database/MatrixCursor;Ljava/util/List;J)J
    .locals 6

    .prologue
    .line 334
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    .line 335
    invoke-virtual {p0}, Landroid/database/MatrixCursor;->newRow()Landroid/database/MatrixCursor$RowBuilder;

    move-result-object v4

    const-wide/16 v2, 0x1

    add-long/2addr v2, p2

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    move-result-object v4

    const-string v5, "del"

    invoke-virtual {v4, v5}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    move-result-object v0

    const/4 v4, 0x0

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v0, v4}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    move-wide p2, v2

    .line 340
    goto :goto_0

    .line 341
    :cond_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    int-to-long v0, v0

    return-wide v0
.end method

.method private b()Ljava/util/List;
    .locals 10
    .annotation build Landroid/annotation/TargetApi;
        value = 0x13
    .end annotation

    .prologue
    .line 210
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 211
    const/4 v6, 0x0

    .line 212
    const-wide/16 v0, -0x1

    move-wide v8, v0

    .line 216
    :goto_0
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/icing/proxy/SmsContentProvider;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Landroid/provider/Telephony$Sms;->CONTENT_URI:Landroid/net/Uri;

    sget-object v2, Lcom/google/android/gms/icing/proxy/SmsContentProvider;->d:[Ljava/lang/String;

    const-string v3, "_id>?"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    invoke-static {v8, v9}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v4, v5

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v8, "_id ASC "

    invoke-direct {v5, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v8, Lcom/google/android/gms/icing/proxy/SmsContentProvider;->c:Ljava/lang/String;

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v1

    .line 224
    if-eqz v1, :cond_0

    :try_start_1
    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v0

    if-nez v0, :cond_2

    .line 233
    :cond_0
    if-eqz v1, :cond_1

    .line 234
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 239
    :cond_1
    return-object v7

    .line 228
    :cond_2
    :goto_1
    :try_start_2
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 229
    const/4 v0, 0x0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-interface {v7, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    .line 233
    :catchall_0
    move-exception v0

    :goto_2
    if-eqz v1, :cond_3

    .line 234
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_3
    throw v0

    .line 231
    :cond_4
    :try_start_3
    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result v0

    int-to-long v4, v0

    .line 233
    if-eqz v1, :cond_5

    .line 234
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 237
    :cond_5
    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-interface {v7, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    .line 238
    const-wide/16 v8, 0x3e8

    cmp-long v0, v4, v8

    if-ltz v0, :cond_1

    move-wide v8, v2

    move-object v6, v1

    goto :goto_0

    .line 233
    :catchall_1
    move-exception v0

    move-object v1, v6

    goto :goto_2
.end method

.method private c()Ljava/util/List;
    .locals 16

    .prologue
    .line 244
    new-instance v10, Ljava/util/ArrayList;

    invoke-direct {v10}, Ljava/util/ArrayList;-><init>()V

    .line 245
    new-instance v12, Lcom/google/android/gms/appdatasearch/c;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/icing/proxy/SmsContentProvider;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {v12, v0}, Lcom/google/android/gms/appdatasearch/c;-><init>(Landroid/content/Context;)V

    .line 246
    const-wide/16 v0, 0x1388

    invoke-virtual {v12, v0, v1}, Lcom/google/android/gms/appdatasearch/c;->a(J)Lcom/google/android/gms/common/c;

    move-result-object v0

    .line 247
    sget-object v1, Lcom/google/android/gms/common/c;->a:Lcom/google/android/gms/common/c;

    if-eq v0, v1, :cond_0

    .line 248
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Error connecting to Icing: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/google/android/gms/common/c;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/icing/c;->d(Ljava/lang/String;)I

    .line 249
    const/4 v0, 0x0

    .line 278
    :goto_0
    return-object v0

    .line 252
    :cond_0
    const/4 v0, 0x0

    .line 253
    :try_start_0
    new-instance v13, Lcom/google/android/gms/appdatasearch/af;

    invoke-direct {v13}, Lcom/google/android/gms/appdatasearch/af;-><init>()V

    .line 254
    const/4 v1, 0x1

    iput-boolean v1, v13, Lcom/google/android/gms/appdatasearch/af;->a:Z

    const/4 v1, 0x0

    iput-boolean v1, v13, Lcom/google/android/gms/appdatasearch/af;->d:Z

    .line 256
    const/4 v1, 0x0

    iput v1, v13, Lcom/google/android/gms/appdatasearch/af;->f:I

    :cond_1
    move v11, v0

    .line 260
    const-string v14, ""

    const/4 v0, 0x1

    new-array v15, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v1, "icing.proxy.sms"

    aput-object v1, v15, v0

    new-instance v0, Lcom/google/android/gms/appdatasearch/QuerySpecification;

    const/4 v1, 0x2

    iget-boolean v2, v13, Lcom/google/android/gms/appdatasearch/af;->a:Z

    iget-object v3, v13, Lcom/google/android/gms/appdatasearch/af;->b:Ljava/util/List;

    iget-object v4, v13, Lcom/google/android/gms/appdatasearch/af;->c:Ljava/util/List;

    iget-boolean v5, v13, Lcom/google/android/gms/appdatasearch/af;->d:Z

    iget v6, v13, Lcom/google/android/gms/appdatasearch/af;->e:I

    iget v7, v13, Lcom/google/android/gms/appdatasearch/af;->f:I

    iget-boolean v8, v13, Lcom/google/android/gms/appdatasearch/af;->g:Z

    iget v9, v13, Lcom/google/android/gms/appdatasearch/af;->h:I

    invoke-direct/range {v0 .. v9}, Lcom/google/android/gms/appdatasearch/QuerySpecification;-><init>(IZLjava/util/List;Ljava/util/List;ZIIZI)V

    invoke-virtual {v12, v14, v15, v11, v0}, Lcom/google/android/gms/appdatasearch/c;->a(Ljava/lang/String;[Ljava/lang/String;ILcom/google/android/gms/appdatasearch/QuerySpecification;)Lcom/google/android/gms/appdatasearch/SearchResults;

    move-result-object v1

    .line 265
    if-nez v1, :cond_2

    .line 266
    const-string v0, "SmsContentProvider: SMS query result is null while querying Icing."

    invoke-static {v0}, Lcom/google/android/gms/icing/c;->d(Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 276
    :goto_1
    invoke-virtual {v12}, Lcom/google/android/gms/appdatasearch/c;->b()V

    move-object v0, v10

    .line 278
    goto :goto_0

    .line 269
    :cond_2
    :try_start_1
    invoke-virtual {v1}, Lcom/google/android/gms/appdatasearch/SearchResults;->d()Lcom/google/android/gms/appdatasearch/at;

    move-result-object v2

    .line 270
    :goto_2
    invoke-virtual {v2}, Lcom/google/android/gms/appdatasearch/at;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 271
    invoke-virtual {v2}, Lcom/google/android/gms/appdatasearch/at;->a()Lcom/google/android/gms/appdatasearch/as;

    move-result-object v0

    iget-object v3, v0, Lcom/google/android/gms/appdatasearch/as;->a:Lcom/google/android/gms/appdatasearch/at;

    invoke-static {v3}, Lcom/google/android/gms/appdatasearch/at;->a(Lcom/google/android/gms/appdatasearch/at;)Lcom/google/android/gms/appdatasearch/ar;

    move-result-object v3

    if-nez v3, :cond_5

    iget-object v3, v0, Lcom/google/android/gms/appdatasearch/as;->c:Lcom/google/android/gms/appdatasearch/SearchResults;

    iget-object v3, v3, Lcom/google/android/gms/appdatasearch/SearchResults;->c:[I

    if-eqz v3, :cond_3

    iget-object v3, v0, Lcom/google/android/gms/appdatasearch/as;->c:Lcom/google/android/gms/appdatasearch/SearchResults;

    iget-object v3, v3, Lcom/google/android/gms/appdatasearch/SearchResults;->d:[B

    if-nez v3, :cond_4

    :cond_3
    const/4 v0, 0x0

    :goto_3
    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-interface {v10, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_2

    .line 276
    :catchall_0
    move-exception v0

    invoke-virtual {v12}, Lcom/google/android/gms/appdatasearch/c;->b()V

    throw v0

    .line 271
    :cond_4
    :try_start_2
    iget-object v3, v0, Lcom/google/android/gms/appdatasearch/as;->a:Lcom/google/android/gms/appdatasearch/at;

    new-instance v4, Lcom/google/android/gms/appdatasearch/ar;

    iget-object v5, v0, Lcom/google/android/gms/appdatasearch/as;->c:Lcom/google/android/gms/appdatasearch/SearchResults;

    iget-object v5, v5, Lcom/google/android/gms/appdatasearch/SearchResults;->c:[I

    iget-object v6, v0, Lcom/google/android/gms/appdatasearch/as;->c:Lcom/google/android/gms/appdatasearch/SearchResults;

    iget-object v6, v6, Lcom/google/android/gms/appdatasearch/SearchResults;->d:[B

    invoke-direct {v4, v5, v6}, Lcom/google/android/gms/appdatasearch/ar;-><init>([I[B)V

    invoke-static {v3, v4}, Lcom/google/android/gms/appdatasearch/at;->a(Lcom/google/android/gms/appdatasearch/at;Lcom/google/android/gms/appdatasearch/ar;)Lcom/google/android/gms/appdatasearch/ar;

    :cond_5
    iget-object v3, v0, Lcom/google/android/gms/appdatasearch/as;->a:Lcom/google/android/gms/appdatasearch/at;

    invoke-static {v3}, Lcom/google/android/gms/appdatasearch/at;->a(Lcom/google/android/gms/appdatasearch/at;)Lcom/google/android/gms/appdatasearch/ar;

    move-result-object v3

    iget v0, v0, Lcom/google/android/gms/appdatasearch/as;->b:I

    invoke-virtual {v3, v0}, Lcom/google/android/gms/appdatasearch/ar;->a(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_3

    .line 273
    :cond_6
    add-int/lit8 v0, v11, 0x64

    .line 274
    invoke-virtual {v1}, Lcom/google/android/gms/appdatasearch/SearchResults;->c()I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result v1

    const/16 v2, 0x64

    if-ge v1, v2, :cond_1

    goto :goto_1
.end method


# virtual methods
.method public final a([Ljava/lang/String;)Landroid/database/Cursor;
    .locals 14

    .prologue
    .line 107
    sget-boolean v0, Lcom/google/android/gms/icing/proxy/SmsContentProvider;->b:Z

    if-nez v0, :cond_0

    .line 108
    const/4 v0, 0x0

    .line 153
    :goto_0
    return-object v0

    .line 111
    :cond_0
    invoke-static {p1}, Lcom/google/android/gms/appdatasearch/be;->a([Ljava/lang/String;)Lcom/google/android/gms/appdatasearch/be;

    move-result-object v0

    .line 113
    invoke-virtual {v0}, Lcom/google/android/gms/appdatasearch/be;->a()Z

    move-result v1

    if-nez v1, :cond_1

    .line 114
    const/4 v0, 0x0

    goto :goto_0

    .line 116
    :cond_1
    invoke-virtual {v0}, Lcom/google/android/gms/appdatasearch/be;->c()J

    move-result-wide v6

    .line 117
    invoke-virtual {v0}, Lcom/google/android/gms/appdatasearch/be;->b()J

    move-result-wide v4

    .line 120
    invoke-virtual {p0}, Lcom/google/android/gms/icing/proxy/SmsContentProvider;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "proxy-content-provider"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "diff-computed"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-nez v0, :cond_4

    const/4 v0, 0x1

    :goto_1
    if-nez v0, :cond_2

    iget-wide v0, p0, Lcom/google/android/gms/icing/proxy/SmsContentProvider;->f:J

    cmp-long v0, v0, v4

    if-eqz v0, :cond_d

    .line 121
    :cond_2
    iget-wide v0, p0, Lcom/google/android/gms/icing/proxy/SmsContentProvider;->f:J

    cmp-long v0, v0, v4

    if-eqz v0, :cond_3

    .line 122
    const-string v0, "SmsContentProvider: mLastSeqno (%d) != lastSeqno (%d)"

    iget-wide v2, p0, Lcom/google/android/gms/icing/proxy/SmsContentProvider;->f:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/icing/c;->c(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)I

    .line 125
    :cond_3
    const-string v0, "SmsContentProvider: About to compute new diff."

    invoke-static {v0}, Lcom/google/android/gms/icing/c;->b(Ljava/lang/String;)I

    .line 126
    invoke-direct {p0}, Lcom/google/android/gms/icing/proxy/SmsContentProvider;->c()Ljava/util/List;

    move-result-object v3

    if-nez v3, :cond_5

    const/4 v0, 0x0

    :goto_2
    if-nez v0, :cond_c

    .line 127
    const/4 v0, 0x0

    goto :goto_0

    .line 120
    :cond_4
    const/4 v0, 0x0

    goto :goto_1

    .line 126
    :cond_5
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "SmsContentProvider: Number of SMS indexed by icing is: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/icing/c;->b(Ljava/lang/String;)I

    invoke-direct {p0}, Lcom/google/android/gms/icing/proxy/SmsContentProvider;->b()Ljava/util/List;

    move-result-object v8

    if-nez v8, :cond_6

    const/4 v0, 0x0

    goto :goto_2

    :cond_6
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "SmsContentProvider: Number of SMS from content provider is: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/icing/c;->b(Ljava/lang/String;)I

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/icing/proxy/SmsContentProvider;->g:Ljava/util/List;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/icing/proxy/SmsContentProvider;->h:Ljava/util/List;

    const/4 v1, 0x0

    const/4 v0, 0x0

    move v2, v1

    move v1, v0

    :goto_3
    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_9

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_9

    invoke-interface {v8, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v10

    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v12

    cmp-long v0, v10, v12

    if-nez v0, :cond_7

    add-int/lit8 v2, v2, 0x1

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_3

    :cond_7
    cmp-long v0, v10, v12

    if-lez v0, :cond_8

    iget-object v0, p0, Lcom/google/android/gms/icing/proxy/SmsContentProvider;->h:Ljava/util/List;

    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    invoke-interface {v0, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_3

    :cond_8
    cmp-long v0, v10, v12

    if-gez v0, :cond_11

    iget-object v0, p0, Lcom/google/android/gms/icing/proxy/SmsContentProvider;->g:Ljava/util/List;

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    invoke-interface {v0, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v2, 0x1

    :goto_4
    move v2, v0

    goto :goto_3

    :cond_9
    :goto_5
    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_a

    iget-object v9, p0, Lcom/google/android/gms/icing/proxy/SmsContentProvider;->g:Ljava/util/List;

    add-int/lit8 v0, v2, 0x1

    invoke-interface {v8, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    invoke-interface {v9, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move v2, v0

    goto :goto_5

    :cond_a
    :goto_6
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_b

    iget-object v2, p0, Lcom/google/android/gms/icing/proxy/SmsContentProvider;->h:Ljava/util/List;

    add-int/lit8 v0, v1, 0x1

    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move v1, v0

    goto :goto_6

    :cond_b
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "SmsContentProvider: To Add: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/gms/icing/proxy/SmsContentProvider;->g:Ljava/util/List;

    invoke-direct {p0, v1}, Lcom/google/android/gms/icing/proxy/SmsContentProvider;->a(Ljava/util/List;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/icing/c;->b(Ljava/lang/String;)I

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "SmsContentProvider: To Del: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/gms/icing/proxy/SmsContentProvider;->h:Ljava/util/List;

    invoke-direct {p0, v1}, Lcom/google/android/gms/icing/proxy/SmsContentProvider;->a(Ljava/util/List;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/icing/c;->b(Ljava/lang/String;)I

    const-string v0, "SmsContentProvider: Finish computing diff in SmsContentProvider."

    invoke-static {v0}, Lcom/google/android/gms/icing/c;->b(Ljava/lang/String;)I

    const/4 v0, 0x1

    goto/16 :goto_2

    .line 129
    :cond_c
    invoke-virtual {p0}, Lcom/google/android/gms/icing/proxy/SmsContentProvider;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "proxy-content-provider"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "diff-computed"

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 131
    :cond_d
    iget-object v0, p0, Lcom/google/android/gms/icing/proxy/SmsContentProvider;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_e

    iget-object v0, p0, Lcom/google/android/gms/icing/proxy/SmsContentProvider;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_e

    .line 132
    iput-wide v4, p0, Lcom/google/android/gms/icing/proxy/SmsContentProvider;->f:J

    .line 133
    const/4 v0, 0x0

    goto/16 :goto_0

    .line 136
    :cond_e
    new-instance v2, Landroid/database/MatrixCursor;

    sget-object v0, Lcom/google/android/gms/icing/proxy/SmsContentProvider;->e:[Ljava/lang/String;

    invoke-direct {v2, v0}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;)V

    .line 138
    iget-object v0, p0, Lcom/google/android/gms/icing/proxy/SmsContentProvider;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_10

    .line 139
    iget-object v0, p0, Lcom/google/android/gms/icing/proxy/SmsContentProvider;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    int-to-long v0, v0

    invoke-static {v0, v1, v6, v7}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v0

    long-to-int v3, v0

    .line 140
    iget-object v0, p0, Lcom/google/android/gms/icing/proxy/SmsContentProvider;->g:Ljava/util/List;

    iget-object v1, p0, Lcom/google/android/gms/icing/proxy/SmsContentProvider;->g:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    sub-int/2addr v1, v3

    iget-object v8, p0, Lcom/google/android/gms/icing/proxy/SmsContentProvider;->g:Ljava/util/List;

    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v8

    invoke-interface {v0, v1, v8}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v8

    .line 141
    const-wide/16 v0, 0x1

    add-long/2addr v0, v4

    invoke-direct {p0, v2, v8, v0, v1}, Lcom/google/android/gms/icing/proxy/SmsContentProvider;->a(Landroid/database/MatrixCursor;Ljava/util/List;J)J

    move-result-wide v0

    add-long/2addr v0, v4

    .line 142
    invoke-interface {v8}, Ljava/util/List;->clear()V

    .line 143
    int-to-long v4, v3

    sub-long v4, v6, v4

    .line 145
    :goto_7
    const-wide/16 v6, 0x0

    cmp-long v3, v4, v6

    if-lez v3, :cond_f

    iget-object v3, p0, Lcom/google/android/gms/icing/proxy/SmsContentProvider;->h:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-lez v3, :cond_f

    .line 146
    iget-object v3, p0, Lcom/google/android/gms/icing/proxy/SmsContentProvider;->h:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    int-to-long v6, v3

    invoke-static {v6, v7, v4, v5}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v4

    long-to-int v3, v4

    .line 147
    iget-object v4, p0, Lcom/google/android/gms/icing/proxy/SmsContentProvider;->h:Ljava/util/List;

    iget-object v5, p0, Lcom/google/android/gms/icing/proxy/SmsContentProvider;->h:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    sub-int v3, v5, v3

    iget-object v5, p0, Lcom/google/android/gms/icing/proxy/SmsContentProvider;->h:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    invoke-interface {v4, v3, v5}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v3

    .line 148
    const-wide/16 v4, 0x1

    add-long/2addr v4, v0

    invoke-static {v2, v3, v4, v5}, Lcom/google/android/gms/icing/proxy/SmsContentProvider;->b(Landroid/database/MatrixCursor;Ljava/util/List;J)J

    move-result-wide v4

    add-long/2addr v0, v4

    .line 149
    invoke-interface {v3}, Ljava/util/List;->clear()V

    .line 151
    :cond_f
    iput-wide v0, p0, Lcom/google/android/gms/icing/proxy/SmsContentProvider;->f:J

    move-object v0, v2

    .line 153
    goto/16 :goto_0

    :cond_10
    move-wide v0, v4

    move-wide v4, v6

    goto :goto_7

    :cond_11
    move v0, v2

    goto/16 :goto_4
.end method

.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 80
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method
