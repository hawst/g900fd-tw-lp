.class public abstract Lcom/google/android/gms/common/internal/br;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field protected a:Ljava/lang/String;

.field protected b:I

.field protected c:Z


# direct methods
.method protected constructor <init>(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 51
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 48
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/common/internal/br;->b:I

    .line 49
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/common/internal/br;->c:Z

    .line 52
    iput-object p1, p0, Lcom/google/android/gms/common/internal/br;->a:Ljava/lang/String;

    .line 53
    return-void
.end method


# virtual methods
.method public final a(I)Lcom/google/android/gms/common/internal/br;
    .locals 0

    .prologue
    .line 74
    iput p1, p0, Lcom/google/android/gms/common/internal/br;->b:I

    .line 75
    return-object p0
.end method

.method public final a(Landroid/content/Context;I)Lcom/google/android/gms/common/internal/br;
    .locals 1

    .prologue
    .line 63
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/common/internal/br;->b:I

    .line 64
    return-object p0
.end method

.method public abstract a()Ljava/lang/String;
.end method

.method public final b()Lcom/google/android/gms/common/internal/br;
    .locals 1

    .prologue
    .line 85
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/common/internal/br;->c:Z

    .line 86
    return-object p0
.end method
