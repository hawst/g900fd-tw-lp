.class public final Lcom/google/android/gms/f/a/f;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# instance fields
.field public a:Ljava/lang/Boolean;

.field public b:Ljava/lang/Boolean;

.field public c:Lcom/google/android/gms/f/a/e;

.field public d:Ljava/lang/Boolean;

.field public e:Lcom/google/android/gms/f/a/b;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 878
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 879
    iput-object v0, p0, Lcom/google/android/gms/f/a/f;->a:Ljava/lang/Boolean;

    iput-object v0, p0, Lcom/google/android/gms/f/a/f;->b:Ljava/lang/Boolean;

    iput-object v0, p0, Lcom/google/android/gms/f/a/f;->c:Lcom/google/android/gms/f/a/e;

    iput-object v0, p0, Lcom/google/android/gms/f/a/f;->d:Ljava/lang/Boolean;

    iput-object v0, p0, Lcom/google/android/gms/f/a/f;->e:Lcom/google/android/gms/f/a/b;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/f/a/f;->cachedSize:I

    .line 880
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 915
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->computeSerializedSize()I

    move-result v0

    .line 916
    iget-object v1, p0, Lcom/google/android/gms/f/a/f;->a:Ljava/lang/Boolean;

    if-eqz v1, :cond_0

    .line 917
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/gms/f/a/f;->a:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 920
    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/f/a/f;->b:Ljava/lang/Boolean;

    if-eqz v1, :cond_1

    .line 921
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/gms/f/a/f;->b:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 924
    :cond_1
    iget-object v1, p0, Lcom/google/android/gms/f/a/f;->c:Lcom/google/android/gms/f/a/e;

    if-eqz v1, :cond_2

    .line 925
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/android/gms/f/a/f;->c:Lcom/google/android/gms/f/a/e;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 928
    :cond_2
    iget-object v1, p0, Lcom/google/android/gms/f/a/f;->d:Ljava/lang/Boolean;

    if-eqz v1, :cond_3

    .line 929
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/android/gms/f/a/f;->d:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 932
    :cond_3
    iget-object v1, p0, Lcom/google/android/gms/f/a/f;->e:Lcom/google/android/gms/f/a/b;

    if-eqz v1, :cond_4

    .line 933
    const/4 v1, 0x5

    iget-object v2, p0, Lcom/google/android/gms/f/a/f;->e:Lcom/google/android/gms/f/a/b;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 936
    :cond_4
    return v0
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 846
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/f/a/f;->a:Ljava/lang/Boolean;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/f/a/f;->b:Ljava/lang/Boolean;

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Lcom/google/android/gms/f/a/f;->c:Lcom/google/android/gms/f/a/e;

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/android/gms/f/a/e;

    invoke-direct {v0}, Lcom/google/android/gms/f/a/e;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/f/a/f;->c:Lcom/google/android/gms/f/a/e;

    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/f/a/f;->c:Lcom/google/android/gms/f/a/e;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/f/a/f;->d:Ljava/lang/Boolean;

    goto :goto_0

    :sswitch_5
    iget-object v0, p0, Lcom/google/android/gms/f/a/f;->e:Lcom/google/android/gms/f/a/b;

    if-nez v0, :cond_2

    new-instance v0, Lcom/google/android/gms/f/a/b;

    invoke-direct {v0}, Lcom/google/android/gms/f/a/b;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/f/a/f;->e:Lcom/google/android/gms/f/a/b;

    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/f/a/f;->e:Lcom/google/android/gms/f/a/b;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
        0x2a -> :sswitch_5
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 895
    iget-object v0, p0, Lcom/google/android/gms/f/a/f;->a:Ljava/lang/Boolean;

    if-eqz v0, :cond_0

    .line 896
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/gms/f/a/f;->a:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 898
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/f/a/f;->b:Ljava/lang/Boolean;

    if-eqz v0, :cond_1

    .line 899
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/android/gms/f/a/f;->b:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 901
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/f/a/f;->c:Lcom/google/android/gms/f/a/e;

    if-eqz v0, :cond_2

    .line 902
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/android/gms/f/a/f;->c:Lcom/google/android/gms/f/a/e;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 904
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/f/a/f;->d:Ljava/lang/Boolean;

    if-eqz v0, :cond_3

    .line 905
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/android/gms/f/a/f;->d:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 907
    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/f/a/f;->e:Lcom/google/android/gms/f/a/b;

    if-eqz v0, :cond_4

    .line 908
    const/4 v0, 0x5

    iget-object v1, p0, Lcom/google/android/gms/f/a/f;->e:Lcom/google/android/gms/f/a/b;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 910
    :cond_4
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 911
    return-void
.end method
