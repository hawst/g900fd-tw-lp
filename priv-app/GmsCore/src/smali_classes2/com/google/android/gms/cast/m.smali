.class public final Lcom/google/android/gms/cast/m;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field a:F

.field b:I

.field c:I

.field d:I

.field e:I

.field f:I

.field g:I

.field h:I

.field i:Ljava/lang/String;

.field j:I

.field k:I

.field l:Lorg/json/JSONObject;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 140
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 141
    invoke-virtual {p0}, Lcom/google/android/gms/cast/m;->a()V

    .line 142
    return-void
.end method

.method static a(Ljava/lang/String;)I
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 646
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v1

    const/16 v2, 0x9

    if-ne v1, v2, :cond_0

    invoke-virtual {p0, v0}, Ljava/lang/String;->charAt(I)C

    move-result v1

    const/16 v2, 0x23

    if-ne v1, v2, :cond_0

    .line 649
    const/4 v1, 0x1

    const/4 v2, 0x3

    :try_start_0
    invoke-virtual {p0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    const/16 v2, 0x10

    invoke-static {v1, v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    move-result v1

    .line 650
    const/4 v2, 0x3

    const/4 v3, 0x5

    invoke-virtual {p0, v2, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    const/16 v3, 0x10

    invoke-static {v2, v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    move-result v2

    .line 651
    const/4 v3, 0x5

    const/4 v4, 0x7

    invoke-virtual {p0, v3, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    const/16 v4, 0x10

    invoke-static {v3, v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    move-result v3

    .line 652
    const/4 v4, 0x7

    const/16 v5, 0x9

    invoke-virtual {p0, v4, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    const/16 v5, 0x10

    invoke-static {v4, v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    move-result v4

    .line 653
    invoke-static {v4, v1, v2, v3}, Landroid/graphics/Color;->argb(IIII)I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 657
    :cond_0
    :goto_0
    return v0

    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method private static a(I)Ljava/lang/String;
    .locals 4

    .prologue
    .line 640
    const-string v0, "#%02X%02X%02X%02X"

    const/4 v1, 0x4

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-static {p0}, Landroid/graphics/Color;->red(I)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    invoke-static {p0}, Landroid/graphics/Color;->green(I)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x2

    invoke-static {p0}, Landroid/graphics/Color;->blue(I)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x3

    invoke-static {p0}, Landroid/graphics/Color;->alpha(I)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method final a()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, -0x1

    const/4 v1, 0x0

    .line 352
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/google/android/gms/cast/m;->a:F

    .line 353
    iput v1, p0, Lcom/google/android/gms/cast/m;->b:I

    .line 354
    iput v1, p0, Lcom/google/android/gms/cast/m;->c:I

    .line 355
    iput v2, p0, Lcom/google/android/gms/cast/m;->d:I

    .line 356
    iput v1, p0, Lcom/google/android/gms/cast/m;->e:I

    .line 357
    iput v2, p0, Lcom/google/android/gms/cast/m;->f:I

    .line 358
    iput v1, p0, Lcom/google/android/gms/cast/m;->g:I

    .line 359
    iput v1, p0, Lcom/google/android/gms/cast/m;->h:I

    .line 360
    iput-object v3, p0, Lcom/google/android/gms/cast/m;->i:Ljava/lang/String;

    .line 361
    iput v2, p0, Lcom/google/android/gms/cast/m;->j:I

    .line 362
    iput v2, p0, Lcom/google/android/gms/cast/m;->k:I

    .line 363
    iput-object v3, p0, Lcom/google/android/gms/cast/m;->l:Lorg/json/JSONObject;

    .line 364
    return-void
.end method

.method public final b()Lorg/json/JSONObject;
    .locals 4

    .prologue
    .line 524
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    .line 526
    :try_start_0
    const-string v1, "fontScale"

    iget v2, p0, Lcom/google/android/gms/cast/m;->a:F

    float-to-double v2, v2

    invoke-virtual {v0, v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;D)Lorg/json/JSONObject;

    .line 527
    iget v1, p0, Lcom/google/android/gms/cast/m;->b:I

    if-eqz v1, :cond_0

    .line 528
    const-string v1, "foregroundColor"

    iget v2, p0, Lcom/google/android/gms/cast/m;->b:I

    invoke-static {v2}, Lcom/google/android/gms/cast/m;->a(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 531
    :cond_0
    iget v1, p0, Lcom/google/android/gms/cast/m;->c:I

    if-eqz v1, :cond_1

    .line 532
    const-string v1, "backgroundColor"

    iget v2, p0, Lcom/google/android/gms/cast/m;->c:I

    invoke-static {v2}, Lcom/google/android/gms/cast/m;->a(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 535
    :cond_1
    iget v1, p0, Lcom/google/android/gms/cast/m;->d:I

    packed-switch v1, :pswitch_data_0

    .line 550
    :goto_0
    iget v1, p0, Lcom/google/android/gms/cast/m;->e:I

    if-eqz v1, :cond_2

    .line 556
    const-string v1, "edgeColor"

    iget v2, p0, Lcom/google/android/gms/cast/m;->e:I

    invoke-static {v2}, Lcom/google/android/gms/cast/m;->a(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 559
    :cond_2
    iget v1, p0, Lcom/google/android/gms/cast/m;->f:I

    packed-switch v1, :pswitch_data_1

    .line 568
    :goto_1
    iget v1, p0, Lcom/google/android/gms/cast/m;->g:I

    if-eqz v1, :cond_3

    .line 574
    const-string v1, "windowColor"

    iget v2, p0, Lcom/google/android/gms/cast/m;->g:I

    invoke-static {v2}, Lcom/google/android/gms/cast/m;->a(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 577
    :cond_3
    iget v1, p0, Lcom/google/android/gms/cast/m;->f:I

    const/4 v2, 0x2

    if-ne v1, v2, :cond_4

    .line 578
    const-string v1, "windowRoundedCornerRadius"

    iget v2, p0, Lcom/google/android/gms/cast/m;->h:I

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 581
    :cond_4
    iget-object v1, p0, Lcom/google/android/gms/cast/m;->i:Ljava/lang/String;

    if-eqz v1, :cond_5

    .line 582
    const-string v1, "fontFamily"

    iget-object v2, p0, Lcom/google/android/gms/cast/m;->i:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 585
    :cond_5
    iget v1, p0, Lcom/google/android/gms/cast/m;->j:I

    packed-switch v1, :pswitch_data_2

    .line 606
    :goto_2
    iget v1, p0, Lcom/google/android/gms/cast/m;->k:I

    packed-switch v1, :pswitch_data_3

    .line 623
    :goto_3
    iget-object v1, p0, Lcom/google/android/gms/cast/m;->l:Lorg/json/JSONObject;

    if-eqz v1, :cond_6

    .line 629
    const-string v1, "customData"

    iget-object v2, p0, Lcom/google/android/gms/cast/m;->l:Lorg/json/JSONObject;

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 635
    :cond_6
    :goto_4
    return-object v0

    .line 537
    :pswitch_0
    const-string v1, "edgeType"

    const-string v2, "NONE"

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    goto :goto_0

    :catch_0
    move-exception v1

    goto :goto_4

    .line 540
    :pswitch_1
    const-string v1, "edgeType"

    const-string v2, "OUTLINE"

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    goto :goto_0

    .line 543
    :pswitch_2
    const-string v1, "edgeType"

    const-string v2, "DROP_SHADOW"

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    goto :goto_0

    .line 546
    :pswitch_3
    const-string v1, "edgeType"

    const-string v2, "RAISED"

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    goto :goto_0

    .line 549
    :pswitch_4
    const-string v1, "edgeType"

    const-string v2, "DEPRESSED"

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    goto :goto_0

    .line 561
    :pswitch_5
    const-string v1, "windowType"

    const-string v2, "NONE"

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    goto :goto_1

    .line 564
    :pswitch_6
    const-string v1, "windowType"

    const-string v2, "NORMAL"

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    goto :goto_1

    .line 567
    :pswitch_7
    const-string v1, "windowType"

    const-string v2, "ROUNDED_CORNERS"

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    goto :goto_1

    .line 587
    :pswitch_8
    const-string v1, "fontGenericFamily"

    const-string v2, "SANS_SERIF"

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    goto :goto_2

    .line 590
    :pswitch_9
    const-string v1, "fontGenericFamily"

    const-string v2, "MONOSPACED_SANS_SERIF"

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    goto :goto_2

    .line 593
    :pswitch_a
    const-string v1, "fontGenericFamily"

    const-string v2, "SERIF"

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    goto :goto_2

    .line 596
    :pswitch_b
    const-string v1, "fontGenericFamily"

    const-string v2, "MONOSPACED_SERIF"

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    goto :goto_2

    .line 599
    :pswitch_c
    const-string v1, "fontGenericFamily"

    const-string v2, "CASUAL"

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    goto :goto_2

    .line 602
    :pswitch_d
    const-string v1, "fontGenericFamily"

    const-string v2, "CURSIVE"

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    goto/16 :goto_2

    .line 605
    :pswitch_e
    const-string v1, "fontGenericFamily"

    const-string v2, "SMALL_CAPITALS"

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    goto/16 :goto_2

    .line 613
    :pswitch_f
    const-string v1, "fontStyle"

    const-string v2, "NORMAL"

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    goto/16 :goto_3

    .line 616
    :pswitch_10
    const-string v1, "fontStyle"

    const-string v2, "BOLD"

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    goto/16 :goto_3

    .line 619
    :pswitch_11
    const-string v1, "fontStyle"

    const-string v2, "ITALIC"

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    goto/16 :goto_3

    .line 622
    :pswitch_12
    const-string v1, "fontStyle"

    const-string v2, "BOLD_ITALIC"

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_3

    .line 535
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch

    .line 559
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch

    .line 585
    :pswitch_data_2
    .packed-switch 0x0
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
    .end packed-switch

    .line 606
    :pswitch_data_3
    .packed-switch 0x0
        :pswitch_f
        :pswitch_10
        :pswitch_11
        :pswitch_12
    .end packed-switch
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 662
    if-ne p0, p1, :cond_1

    move v2, v1

    .line 680
    :cond_0
    :goto_0
    return v2

    .line 665
    :cond_1
    instance-of v0, p1, Lcom/google/android/gms/cast/m;

    if-eqz v0, :cond_0

    .line 669
    check-cast p1, Lcom/google/android/gms/cast/m;

    .line 671
    iget-object v0, p0, Lcom/google/android/gms/cast/m;->l:Lorg/json/JSONObject;

    if-nez v0, :cond_3

    move v0, v1

    :goto_1
    iget-object v3, p1, Lcom/google/android/gms/cast/m;->l:Lorg/json/JSONObject;

    if-nez v3, :cond_4

    move v3, v1

    :goto_2
    if-ne v0, v3, :cond_0

    .line 674
    iget-object v0, p0, Lcom/google/android/gms/cast/m;->l:Lorg/json/JSONObject;

    if-eqz v0, :cond_2

    iget-object v0, p1, Lcom/google/android/gms/cast/m;->l:Lorg/json/JSONObject;

    if-eqz v0, :cond_2

    .line 675
    iget-object v0, p0, Lcom/google/android/gms/cast/m;->l:Lorg/json/JSONObject;

    iget-object v3, p1, Lcom/google/android/gms/cast/m;->l:Lorg/json/JSONObject;

    invoke-static {v0, v3}, Lcom/google/android/gms/common/util/af;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 680
    :cond_2
    iget v0, p0, Lcom/google/android/gms/cast/m;->a:F

    iget v3, p1, Lcom/google/android/gms/cast/m;->a:F

    cmpl-float v0, v0, v3

    if-nez v0, :cond_0

    iget v0, p0, Lcom/google/android/gms/cast/m;->b:I

    iget v3, p1, Lcom/google/android/gms/cast/m;->b:I

    if-ne v0, v3, :cond_0

    iget v0, p0, Lcom/google/android/gms/cast/m;->c:I

    iget v3, p1, Lcom/google/android/gms/cast/m;->c:I

    if-ne v0, v3, :cond_0

    iget v0, p0, Lcom/google/android/gms/cast/m;->d:I

    iget v3, p1, Lcom/google/android/gms/cast/m;->d:I

    if-ne v0, v3, :cond_0

    iget v0, p0, Lcom/google/android/gms/cast/m;->e:I

    iget v3, p1, Lcom/google/android/gms/cast/m;->e:I

    if-ne v0, v3, :cond_0

    iget v0, p0, Lcom/google/android/gms/cast/m;->f:I

    iget v3, p1, Lcom/google/android/gms/cast/m;->f:I

    if-ne v0, v3, :cond_0

    iget v0, p0, Lcom/google/android/gms/cast/m;->h:I

    iget v3, p1, Lcom/google/android/gms/cast/m;->h:I

    if-ne v0, v3, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/cast/m;->i:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/android/gms/cast/m;->i:Ljava/lang/String;

    invoke-static {v0, v3}, Lcom/google/android/gms/cast/internal/c;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/google/android/gms/cast/m;->j:I

    iget v3, p1, Lcom/google/android/gms/cast/m;->j:I

    if-ne v0, v3, :cond_0

    iget v0, p0, Lcom/google/android/gms/cast/m;->k:I

    iget v3, p1, Lcom/google/android/gms/cast/m;->k:I

    if-ne v0, v3, :cond_0

    move v2, v1

    goto :goto_0

    :cond_3
    move v0, v2

    .line 671
    goto :goto_1

    :cond_4
    move v3, v2

    goto :goto_2
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    .line 694
    const/16 v0, 0xc

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget v2, p0, Lcom/google/android/gms/cast/m;->a:F

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget v2, p0, Lcom/google/android/gms/cast/m;->b:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget v2, p0, Lcom/google/android/gms/cast/m;->c:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget v2, p0, Lcom/google/android/gms/cast/m;->d:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget v2, p0, Lcom/google/android/gms/cast/m;->e:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x5

    iget v2, p0, Lcom/google/android/gms/cast/m;->f:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x6

    iget v2, p0, Lcom/google/android/gms/cast/m;->g:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x7

    iget v2, p0, Lcom/google/android/gms/cast/m;->h:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x8

    iget-object v2, p0, Lcom/google/android/gms/cast/m;->i:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    iget v2, p0, Lcom/google/android/gms/cast/m;->j:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0xa

    iget v2, p0, Lcom/google/android/gms/cast/m;->k:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0xb

    iget-object v2, p0, Lcom/google/android/gms/cast/m;->l:Lorg/json/JSONObject;

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method
