.class public final Lcom/google/android/gms/games/service/statemachine/roomclient/e;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field a:Ljava/lang/String;

.field b:Lcom/google/android/gms/games/service/statemachine/roomclient/q;

.field c:Lcom/google/android/gms/games/service/statemachine/roomclient/q;

.field final d:Lcom/google/android/gms/games/service/statemachine/roomclient/c;

.field e:Ljava/util/List;

.field final synthetic f:Lcom/google/android/gms/games/service/statemachine/roomclient/InRoomState;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/games/service/statemachine/roomclient/InRoomState;)V
    .locals 1

    .prologue
    .line 635
    iput-object p1, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/e;->f:Lcom/google/android/gms/games/service/statemachine/roomclient/InRoomState;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 636
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/e;->a:Ljava/lang/String;

    .line 640
    new-instance v0, Lcom/google/android/gms/games/service/statemachine/roomclient/f;

    invoke-direct {v0, p0}, Lcom/google/android/gms/games/service/statemachine/roomclient/f;-><init>(Lcom/google/android/gms/games/service/statemachine/roomclient/e;)V

    iput-object v0, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/e;->d:Lcom/google/android/gms/games/service/statemachine/roomclient/c;

    .line 656
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/e;->e:Ljava/util/List;

    .line 869
    return-void
.end method

.method static a(Ljava/lang/String;Landroid/os/RemoteException;)V
    .locals 3

    .prologue
    .line 696
    const-string v0, "RoomServiceClientStateMachine"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "RoomCallbacksHelper."

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, p1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 697
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 659
    iget-object v0, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/e;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 660
    return-void
.end method

.method final a(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/games/service/statemachine/roomclient/d;Lcom/google/android/gms/games/service/statemachine/roomclient/c;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 715
    iget-object v0, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/e;->a:Ljava/lang/String;

    invoke-static {p2, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    if-nez v0, :cond_1

    .line 735
    :goto_1
    return-void

    .line 715
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/e;->f:Lcom/google/android/gms/games/service/statemachine/roomclient/InRoomState;

    iget-object v0, v0, Lcom/google/android/gms/games/service/statemachine/roomclient/InRoomState;->c:Lcom/google/android/gms/games/service/statemachine/d;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Current room is "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/e;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " but got a message for "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " in "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v2}, Lcom/google/android/gms/games/service/statemachine/d;->a(Ljava/lang/String;)V

    move v0, v1

    goto :goto_0

    .line 718
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/e;->f:Lcom/google/android/gms/games/service/statemachine/roomclient/InRoomState;

    invoke-static {v0}, Lcom/google/android/gms/games/service/statemachine/roomclient/InRoomState;->b(Lcom/google/android/gms/games/service/statemachine/roomclient/InRoomState;)Lcom/google/android/gms/games/service/statemachine/roomclient/by;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/google/android/gms/games/service/statemachine/roomclient/by;->a(Ljava/lang/String;)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v2

    .line 719
    if-eqz p4, :cond_2

    .line 720
    new-instance v0, Lcom/google/android/gms/games/multiplayer/realtime/c;

    invoke-direct {v0, v2}, Lcom/google/android/gms/games/multiplayer/realtime/c;-><init>(Lcom/google/android/gms/common/data/DataHolder;)V

    .line 721
    invoke-virtual {v0}, Lcom/google/android/gms/games/multiplayer/realtime/c;->c()I

    move-result v3

    if-lez v3, :cond_2

    .line 722
    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/multiplayer/realtime/c;->a(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/multiplayer/realtime/Room;

    invoke-interface {p4, v0}, Lcom/google/android/gms/games/service/statemachine/roomclient/c;->a(Lcom/google/android/gms/games/multiplayer/realtime/Room;)V

    .line 726
    :cond_2
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/e;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/service/statemachine/roomclient/q;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 728
    :try_start_1
    iget-object v0, v0, Lcom/google/android/gms/games/service/statemachine/roomclient/q;->a:Lcom/google/android/gms/games/internal/dr;

    invoke-interface {p3, v2, v0}, Lcom/google/android/gms/games/service/statemachine/roomclient/d;->a(Lcom/google/android/gms/common/data/DataHolder;Lcom/google/android/gms/games/internal/dr;)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_2

    .line 729
    :catch_0
    move-exception v0

    .line 730
    :try_start_2
    invoke-static {p1, v0}, Lcom/google/android/gms/games/service/statemachine/roomclient/e;->a(Ljava/lang/String;Landroid/os/RemoteException;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_2

    .line 734
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Lcom/google/android/gms/common/data/DataHolder;->j()V

    throw v0

    :cond_3
    invoke-virtual {v2}, Lcom/google/android/gms/common/data/DataHolder;->j()V

    goto :goto_1
.end method

.method final b()V
    .locals 2

    .prologue
    .line 671
    iget-object v0, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/e;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 672
    iget-object v0, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/e;->b:Lcom/google/android/gms/games/service/statemachine/roomclient/q;

    if-eqz v0, :cond_0

    .line 673
    iget-object v0, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/e;->e:Ljava/util/List;

    iget-object v1, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/e;->b:Lcom/google/android/gms/games/service/statemachine/roomclient/q;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 675
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/e;->c:Lcom/google/android/gms/games/service/statemachine/roomclient/q;

    if-eqz v0, :cond_1

    .line 676
    iget-object v0, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/e;->e:Ljava/util/List;

    iget-object v1, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/e;->c:Lcom/google/android/gms/games/service/statemachine/roomclient/q;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 678
    :cond_1
    return-void
.end method
