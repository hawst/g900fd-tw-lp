.class public final Lcom/google/android/gms/games/service/statemachine/roomclient/by;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final a:Landroid/content/Context;

.field final b:Lcom/google/android/gms/common/server/ClientContext;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;)V
    .locals 0

    .prologue
    .line 145
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 146
    invoke-static {}, Lcom/google/android/gms/common/app/GmsApplication;->a()V

    .line 147
    iput-object p1, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/by;->a:Landroid/content/Context;

    .line 148
    iput-object p2, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/by;->b:Lcom/google/android/gms/common/server/ClientContext;

    .line 149
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Lcom/google/android/gms/common/data/DataHolder;
    .locals 3

    .prologue
    .line 160
    iget-object v0, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/by;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/gms/games/a/t;->a(Landroid/content/Context;)Lcom/google/android/gms/games/a/t;

    move-result-object v1

    .line 162
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/by;->a:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/by;->b:Lcom/google/android/gms/common/server/ClientContext;

    invoke-virtual {v1, v0, v2, p1}, Lcom/google/android/gms/games/a/t;->b(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)Lcom/google/android/gms/common/data/DataHolder;
    :try_end_0
    .catch Lcom/google/android/gms/auth/q; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 166
    invoke-virtual {v1}, Lcom/google/android/gms/games/a/t;->a()V

    :goto_0
    return-object v0

    .line 164
    :catch_0
    move-exception v0

    const/4 v0, 0x2

    :try_start_1
    invoke-static {v0}, Lcom/google/android/gms/common/data/DataHolder;->b(I)Lcom/google/android/gms/common/data/DataHolder;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    .line 166
    invoke-virtual {v1}, Lcom/google/android/gms/games/a/t;->a()V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Lcom/google/android/gms/games/a/t;->a()V

    throw v0
.end method

.method public final a(Lcom/google/android/gms/games/h/a/ev;)Z
    .locals 5

    .prologue
    .line 183
    iget-object v0, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/by;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/gms/games/a/t;->a(Landroid/content/Context;)Lcom/google/android/gms/games/a/t;

    move-result-object v1

    .line 184
    iget-object v0, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/by;->a:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/by;->b:Lcom/google/android/gms/common/server/ClientContext;

    invoke-virtual {v1, v0, v2, p1}, Lcom/google/android/gms/games/a/t;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Lcom/google/android/gms/games/h/a/ev;)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v2

    .line 186
    :try_start_0
    invoke-virtual {v2}, Lcom/google/android/gms/common/data/DataHolder;->h()I

    move-result v0

    if-nez v0, :cond_0

    .line 187
    const-string v0, "RealTimeDataManager"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Empty data holder with status "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Lcom/google/android/gms/common/data/DataHolder;->f()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " returned for status update to room "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p1}, Lcom/google/android/gms/games/h/a/ev;->b()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Lcom/google/android/gms/games/internal/dq;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 189
    invoke-virtual {v2}, Lcom/google/android/gms/common/data/DataHolder;->j()V

    .line 195
    invoke-virtual {v1}, Lcom/google/android/gms/games/a/t;->a()V

    const/4 v0, 0x0

    :goto_0
    return v0

    .line 191
    :cond_0
    invoke-virtual {v2}, Lcom/google/android/gms/common/data/DataHolder;->j()V

    .line 195
    invoke-virtual {v1}, Lcom/google/android/gms/games/a/t;->a()V

    const/4 v0, 0x1

    goto :goto_0

    .line 194
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Lcom/google/android/gms/common/data/DataHolder;->j()V

    .line 195
    invoke-virtual {v1}, Lcom/google/android/gms/games/a/t;->a()V

    throw v0
.end method
