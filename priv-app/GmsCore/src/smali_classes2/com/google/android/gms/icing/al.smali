.class public final Lcom/google/android/gms/icing/al;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# instance fields
.field public a:I

.field public b:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 5166
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 5167
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/gms/icing/al;->a:I

    const/16 v0, 0x4e20

    iput v0, p0, Lcom/google/android/gms/icing/al;->b:I

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/icing/al;->cachedSize:I

    .line 5168
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 5217
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->computeSerializedSize()I

    move-result v0

    .line 5218
    iget v1, p0, Lcom/google/android/gms/icing/al;->a:I

    if-eqz v1, :cond_0

    .line 5219
    const/4 v1, 0x1

    iget v2, p0, Lcom/google/android/gms/icing/al;->a:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 5222
    :cond_0
    iget v1, p0, Lcom/google/android/gms/icing/al;->b:I

    const/16 v2, 0x4e20

    if-eq v1, v2, :cond_1

    .line 5223
    const/4 v1, 0x2

    iget v2, p0, Lcom/google/android/gms/icing/al;->b:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->g(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 5226
    :cond_1
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 5179
    if-ne p1, p0, :cond_1

    .line 5192
    :cond_0
    :goto_0
    return v0

    .line 5182
    :cond_1
    instance-of v2, p1, Lcom/google/android/gms/icing/al;

    if-nez v2, :cond_2

    move v0, v1

    .line 5183
    goto :goto_0

    .line 5185
    :cond_2
    check-cast p1, Lcom/google/android/gms/icing/al;

    .line 5186
    iget v2, p0, Lcom/google/android/gms/icing/al;->a:I

    iget v3, p1, Lcom/google/android/gms/icing/al;->a:I

    if-eq v2, v3, :cond_3

    move v0, v1

    .line 5187
    goto :goto_0

    .line 5189
    :cond_3
    iget v2, p0, Lcom/google/android/gms/icing/al;->b:I

    iget v3, p1, Lcom/google/android/gms/icing/al;->b:I

    if-eq v2, v3, :cond_0

    move v0, v1

    .line 5190
    goto :goto_0
.end method

.method public final hashCode()I
    .locals 2

    .prologue
    .line 5197
    iget v0, p0, Lcom/google/android/gms/icing/al;->a:I

    add-int/lit16 v0, v0, 0x20f

    .line 5199
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/google/android/gms/icing/al;->b:I

    add-int/2addr v0, v1

    .line 5200
    return v0
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 5139
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    iput v0, p0, Lcom/google/android/gms/icing/al;->a:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/icing/al;->b:I

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 5206
    iget v0, p0, Lcom/google/android/gms/icing/al;->a:I

    if-eqz v0, :cond_0

    .line 5207
    const/4 v0, 0x1

    iget v1, p0, Lcom/google/android/gms/icing/al;->a:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 5209
    :cond_0
    iget v0, p0, Lcom/google/android/gms/icing/al;->b:I

    const/16 v1, 0x4e20

    if-eq v0, v1, :cond_1

    .line 5210
    const/4 v0, 0x2

    iget v1, p0, Lcom/google/android/gms/icing/al;->b:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->c(II)V

    .line 5212
    :cond_1
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 5213
    return-void
.end method
