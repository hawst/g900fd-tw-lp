.class public Lcom/google/android/gms/drive/internal/CancelPendingActionsRequest;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field final a:I

.field final b:Ljava/util/List;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 20
    new-instance v0, Lcom/google/android/gms/drive/internal/d;

    invoke-direct {v0}, Lcom/google/android/gms/drive/internal/d;-><init>()V

    sput-object v0, Lcom/google/android/gms/drive/internal/CancelPendingActionsRequest;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method constructor <init>(ILjava/util/List;)V
    .locals 0

    .prologue
    .line 48
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 49
    iput p1, p0, Lcom/google/android/gms/drive/internal/CancelPendingActionsRequest;->a:I

    .line 50
    iput-object p2, p0, Lcom/google/android/gms/drive/internal/CancelPendingActionsRequest;->b:Ljava/util/List;

    .line 51
    return-void
.end method


# virtual methods
.method public final a()Ljava/util/List;
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Lcom/google/android/gms/drive/internal/CancelPendingActionsRequest;->b:Ljava/util/List;

    return-object v0
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 32
    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 0

    .prologue
    .line 37
    invoke-static {p0, p1}, Lcom/google/android/gms/drive/internal/d;->a(Lcom/google/android/gms/drive/internal/CancelPendingActionsRequest;Landroid/os/Parcel;)V

    .line 38
    return-void
.end method
