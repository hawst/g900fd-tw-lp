.class public Lcom/google/android/gms/car/nn;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/car/au;


# instance fields
.field protected a:I

.field protected b:I

.field protected final c:F


# direct methods
.method public constructor <init>(II)V
    .locals 3

    .prologue
    const/high16 v2, 0x3f800000    # 1.0f

    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 11
    const/16 v0, 0x64

    iput v0, p0, Lcom/google/android/gms/car/nn;->a:I

    .line 12
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/gms/car/nn;->b:I

    .line 13
    iget v0, p0, Lcom/google/android/gms/car/nn;->a:I

    iget v1, p0, Lcom/google/android/gms/car/nn;->b:I

    invoke-static {v2, v0, v1}, Lcom/google/android/gms/car/nn;->a(FII)F

    move-result v0

    div-float v0, v2, v0

    iput v0, p0, Lcom/google/android/gms/car/nn;->c:F

    .line 14
    return-void
.end method

.method static a(FII)F
    .locals 4

    .prologue
    .line 17
    int-to-double v0, p1

    neg-float v2, p0

    float-to-double v2, v2

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v0

    neg-double v0, v0

    double-to-float v0, v0

    const/high16 v1, 0x3f800000    # 1.0f

    add-float/2addr v0, v1

    int-to-float v1, p2

    mul-float/2addr v1, p0

    add-float/2addr v0, v1

    return v0
.end method


# virtual methods
.method public a(FFF)F
    .locals 3

    .prologue
    .line 22
    cmpg-float v0, p1, p2

    if-gtz v0, :cond_0

    .line 23
    const/4 v0, 0x0

    .line 28
    :goto_0
    return v0

    .line 24
    :cond_0
    cmpl-float v0, p1, p3

    if-ltz v0, :cond_1

    .line 25
    const/high16 v0, 0x3f800000    # 1.0f

    goto :goto_0

    .line 27
    :cond_1
    sub-float v0, p1, p2

    sub-float v1, p3, p2

    div-float/2addr v0, v1

    .line 28
    iget v1, p0, Lcom/google/android/gms/car/nn;->a:I

    iget v2, p0, Lcom/google/android/gms/car/nn;->b:I

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/car/nn;->a(FII)F

    move-result v0

    iget v1, p0, Lcom/google/android/gms/car/nn;->c:F

    mul-float/2addr v0, v1

    goto :goto_0
.end method
