.class final Lcom/google/android/gms/drive/events/h;
.super Ljava/lang/Thread;
.source "SourceFile"


# instance fields
.field final synthetic a:Ljava/util/concurrent/CountDownLatch;

.field final synthetic b:Lcom/google/android/gms/drive/events/g;


# direct methods
.method constructor <init>(Lcom/google/android/gms/drive/events/g;Ljava/util/concurrent/CountDownLatch;)V
    .locals 0

    .prologue
    .line 197
    iput-object p1, p0, Lcom/google/android/gms/drive/events/h;->b:Lcom/google/android/gms/drive/events/g;

    iput-object p2, p0, Lcom/google/android/gms/drive/events/h;->a:Ljava/util/concurrent/CountDownLatch;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 3

    .prologue
    .line 201
    :try_start_0
    invoke-static {}, Landroid/os/Looper;->prepare()V

    .line 208
    iget-object v0, p0, Lcom/google/android/gms/drive/events/h;->b:Lcom/google/android/gms/drive/events/g;

    new-instance v1, Lcom/google/android/gms/drive/events/i;

    iget-object v2, p0, Lcom/google/android/gms/drive/events/h;->b:Lcom/google/android/gms/drive/events/g;

    invoke-direct {v1, v2}, Lcom/google/android/gms/drive/events/i;-><init>(Lcom/google/android/gms/drive/events/g;)V

    iput-object v1, v0, Lcom/google/android/gms/drive/events/g;->a:Lcom/google/android/gms/drive/events/i;

    .line 209
    iget-object v0, p0, Lcom/google/android/gms/drive/events/h;->b:Lcom/google/android/gms/drive/events/g;

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/google/android/gms/drive/events/g;->b:Z

    .line 210
    iget-object v0, p0, Lcom/google/android/gms/drive/events/h;->a:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    .line 211
    const-string v0, "DriveEventService"

    const-string v1, "Bound and starting loop"

    invoke-static {v0, v1}, Lcom/google/android/gms/drive/internal/be;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 212
    invoke-static {}, Landroid/os/Looper;->loop()V

    .line 213
    const-string v0, "DriveEventService"

    const-string v1, "Finished loop"

    invoke-static {v0, v1}, Lcom/google/android/gms/drive/internal/be;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 217
    iget-object v0, p0, Lcom/google/android/gms/drive/events/h;->b:Lcom/google/android/gms/drive/events/g;

    invoke-static {v0}, Lcom/google/android/gms/drive/events/g;->b(Lcom/google/android/gms/drive/events/g;)Ljava/util/concurrent/CountDownLatch;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 218
    iget-object v0, p0, Lcom/google/android/gms/drive/events/h;->b:Lcom/google/android/gms/drive/events/g;

    invoke-static {v0}, Lcom/google/android/gms/drive/events/g;->b(Lcom/google/android/gms/drive/events/g;)Ljava/util/concurrent/CountDownLatch;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    .line 221
    :cond_0
    return-void

    .line 217
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/google/android/gms/drive/events/h;->b:Lcom/google/android/gms/drive/events/g;

    invoke-static {v1}, Lcom/google/android/gms/drive/events/g;->b(Lcom/google/android/gms/drive/events/g;)Ljava/util/concurrent/CountDownLatch;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 218
    iget-object v1, p0, Lcom/google/android/gms/drive/events/h;->b:Lcom/google/android/gms/drive/events/g;

    invoke-static {v1}, Lcom/google/android/gms/drive/events/g;->b(Lcom/google/android/gms/drive/events/g;)Ljava/util/concurrent/CountDownLatch;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    :cond_1
    throw v0
.end method
