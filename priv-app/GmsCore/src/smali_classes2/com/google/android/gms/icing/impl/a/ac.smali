.class public final Lcom/google/android/gms/icing/impl/a/ac;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static final a:Lcom/google/android/gms/icing/impl/a/ac;


# instance fields
.field private b:Ljava/lang/Object;

.field private c:I

.field private d:J


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    .line 28
    new-instance v0, Lcom/google/android/gms/icing/impl/a/ac;

    const/4 v1, 0x0

    const/4 v2, 0x0

    const-wide/16 v4, 0x0

    invoke-direct {v0, v1, v2, v4, v5}, Lcom/google/android/gms/icing/impl/a/ac;-><init>(Ljava/lang/Object;IJ)V

    sput-object v0, Lcom/google/android/gms/icing/impl/a/ac;->a:Lcom/google/android/gms/icing/impl/a/ac;

    return-void
.end method

.method public constructor <init>(Ljava/lang/Object;IJ)V
    .locals 1

    .prologue
    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    iput-object p1, p0, Lcom/google/android/gms/icing/impl/a/ac;->b:Ljava/lang/Object;

    .line 36
    iput p2, p0, Lcom/google/android/gms/icing/impl/a/ac;->c:I

    .line 37
    iput-wide p3, p0, Lcom/google/android/gms/icing/impl/a/ac;->d:J

    .line 38
    return-void
.end method

.method public static a()Lcom/google/android/gms/icing/impl/a/ac;
    .locals 1

    .prologue
    .line 42
    sget-object v0, Lcom/google/android/gms/icing/impl/a/ac;->a:Lcom/google/android/gms/icing/impl/a/ac;

    return-object v0
.end method

.method public static a(ILjava/lang/Object;J)Lcom/google/android/gms/icing/impl/a/ac;
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 54
    if-eqz p0, :cond_0

    if-eq p0, v0, :cond_0

    const/4 v1, 0x2

    if-ne p0, v1, :cond_1

    :cond_0
    :goto_0
    invoke-static {v0}, Lcom/google/android/gms/common/internal/bx;->b(Z)V

    .line 56
    new-instance v0, Lcom/google/android/gms/icing/impl/a/ac;

    invoke-direct {v0, p1, p0, p2, p3}, Lcom/google/android/gms/icing/impl/a/ac;-><init>(Ljava/lang/Object;IJ)V

    return-object v0

    .line 54
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Ljava/lang/Object;J)Lcom/google/android/gms/icing/impl/a/ac;
    .locals 3

    .prologue
    .line 46
    new-instance v0, Lcom/google/android/gms/icing/impl/a/ac;

    const/4 v1, 0x1

    invoke-direct {v0, p0, v1, p1, p2}, Lcom/google/android/gms/icing/impl/a/ac;-><init>(Ljava/lang/Object;IJ)V

    return-object v0
.end method

.method private declared-synchronized a(Ljava/lang/Object;IJ)V
    .locals 1

    .prologue
    .line 87
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0, p2}, Lcom/google/android/gms/icing/impl/a/ac;->a(I)Z

    move-result v0

    invoke-static {v0}, Lcom/google/android/gms/common/internal/bx;->a(Z)V

    .line 88
    iput p2, p0, Lcom/google/android/gms/icing/impl/a/ac;->c:I

    .line 89
    iput-object p1, p0, Lcom/google/android/gms/icing/impl/a/ac;->b:Ljava/lang/Object;

    .line 90
    iput-wide p3, p0, Lcom/google/android/gms/icing/impl/a/ac;->d:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 91
    monitor-exit p0

    return-void

    .line 87
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public static b(Ljava/lang/Object;J)Lcom/google/android/gms/icing/impl/a/ac;
    .locals 3

    .prologue
    .line 50
    new-instance v0, Lcom/google/android/gms/icing/impl/a/ac;

    const/4 v1, 0x2

    invoke-direct {v0, p0, v1, p1, p2}, Lcom/google/android/gms/icing/impl/a/ac;-><init>(Ljava/lang/Object;IJ)V

    return-object v0
.end method

.method public static b(I)Ljava/lang/String;
    .locals 2

    .prologue
    .line 127
    packed-switch p0, :pswitch_data_0

    .line 135
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "unknown ("

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    .line 129
    :pswitch_0
    const-string v0, "unset"

    goto :goto_0

    .line 131
    :pswitch_1
    const-string v0, "set from runtime API"

    goto :goto_0

    .line 133
    :pswitch_2
    const-string v0, "set from resources"

    goto :goto_0

    .line 127
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)Lcom/google/android/gms/icing/impl/a/ac;
    .locals 4

    .prologue
    .line 119
    new-instance v0, Lcom/google/android/gms/icing/impl/a/ac;

    invoke-virtual {p0}, Lcom/google/android/gms/icing/impl/a/ac;->b()I

    move-result v1

    iget-wide v2, p0, Lcom/google/android/gms/icing/impl/a/ac;->d:J

    invoke-direct {v0, p1, v1, v2, v3}, Lcom/google/android/gms/icing/impl/a/ac;-><init>(Ljava/lang/Object;IJ)V

    return-object v0
.end method

.method public final declared-synchronized a(J)V
    .locals 1

    .prologue
    .line 94
    monitor-enter p0

    :try_start_0
    iput-wide p1, p0, Lcom/google/android/gms/icing/impl/a/ac;->d:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 95
    monitor-exit p0

    return-void

    .line 94
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(I)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 64
    monitor-enter p0

    :try_start_0
    iget v2, p0, Lcom/google/android/gms/icing/impl/a/ac;->c:I

    if-eqz v2, :cond_0

    iget v2, p0, Lcom/google/android/gms/icing/impl/a/ac;->c:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-ne v2, v1, :cond_2

    :cond_0
    move v0, v1

    .line 78
    :cond_1
    :goto_0
    monitor-exit p0

    return v0

    .line 68
    :cond_2
    :try_start_1
    iget v2, p0, Lcom/google/android/gms/icing/impl/a/ac;->c:I

    const/4 v3, 0x2

    if-ne v2, v3, :cond_3

    .line 69
    if-eq p1, v1, :cond_1

    move v0, v1

    .line 74
    goto :goto_0

    .line 77
    :cond_3
    const/4 v1, 0x0

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Invalid source value "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/gms/common/internal/bx;->b(ZLjava/lang/Object;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 64
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final a(Lcom/google/android/gms/icing/impl/a/ac;)Z
    .locals 1

    .prologue
    .line 60
    invoke-virtual {p1}, Lcom/google/android/gms/icing/impl/a/ac;->b()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/icing/impl/a/ac;->a(I)Z

    move-result v0

    return v0
.end method

.method public final declared-synchronized b()I
    .locals 1

    .prologue
    .line 102
    monitor-enter p0

    :try_start_0
    iget v0, p0, Lcom/google/android/gms/icing/impl/a/ac;->c:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final b(Lcom/google/android/gms/icing/impl/a/ac;)V
    .locals 4

    .prologue
    .line 98
    invoke-virtual {p1}, Lcom/google/android/gms/icing/impl/a/ac;->d()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/android/gms/icing/impl/a/ac;->b()I

    move-result v1

    invoke-virtual {p1}, Lcom/google/android/gms/icing/impl/a/ac;->c()J

    move-result-wide v2

    invoke-direct {p0, v0, v1, v2, v3}, Lcom/google/android/gms/icing/impl/a/ac;->a(Ljava/lang/Object;IJ)V

    .line 99
    return-void
.end method

.method public final declared-synchronized c()J
    .locals 2

    .prologue
    .line 111
    monitor-enter p0

    :try_start_0
    iget-wide v0, p0, Lcom/google/android/gms/icing/impl/a/ac;->d:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-wide v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized d()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 115
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/a/ac;->b:Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final e()Ljava/lang/String;
    .locals 1

    .prologue
    .line 123
    invoke-virtual {p0}, Lcom/google/android/gms/icing/impl/a/ac;->b()I

    move-result v0

    invoke-static {v0}, Lcom/google/android/gms/icing/impl/a/ac;->b(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 141
    if-ne p1, p0, :cond_1

    .line 146
    :cond_0
    :goto_0
    return v0

    .line 142
    :cond_1
    instance-of v2, p1, Lcom/google/android/gms/icing/impl/a/ac;

    if-eqz v2, :cond_3

    .line 143
    check-cast p1, Lcom/google/android/gms/icing/impl/a/ac;

    .line 144
    iget v2, p1, Lcom/google/android/gms/icing/impl/a/ac;->c:I

    iget v3, p0, Lcom/google/android/gms/icing/impl/a/ac;->c:I

    if-ne v2, v3, :cond_2

    iget-object v2, p0, Lcom/google/android/gms/icing/impl/a/ac;->b:Ljava/lang/Object;

    iget-object v3, p1, Lcom/google/android/gms/icing/impl/a/ac;->b:Ljava/lang/Object;

    invoke-static {v2, v3}, Lcom/google/android/gms/common/internal/bu;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_2
    move v0, v1

    goto :goto_0

    :cond_3
    move v0, v1

    .line 146
    goto :goto_0
.end method

.method public final hashCode()I
    .locals 2

    .prologue
    .line 152
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/a/ac;->b:Ljava/lang/Object;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    add-int/lit16 v0, v0, 0x353

    .line 154
    mul-int/lit8 v0, v0, 0x25

    iget v1, p0, Lcom/google/android/gms/icing/impl/a/ac;->c:I

    add-int/2addr v0, v1

    .line 155
    return v0

    .line 152
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/a/ac;->b:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0
.end method
