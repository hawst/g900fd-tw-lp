.class public final Lcom/google/android/gms/games/d;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field static final a:Lcom/google/android/gms/common/api/j;

.field public static final b:Lcom/google/android/gms/common/api/Scope;

.field public static final c:Lcom/google/android/gms/common/api/c;

.field public static final d:Lcom/google/android/gms/common/api/Scope;

.field public static final e:Lcom/google/android/gms/common/api/c;

.field public static final f:Lcom/google/android/gms/games/k;

.field public static final g:Lcom/google/android/gms/games/achievement/c;

.field public static final h:Lcom/google/android/gms/games/appcontent/m;

.field public static final i:Lcom/google/android/gms/games/event/b;

.field public static final j:Lcom/google/android/gms/games/e/n;

.field public static final k:Lcom/google/android/gms/games/multiplayer/d;

.field public static final l:Lcom/google/android/gms/games/multiplayer/turnbased/e;

.field public static final m:Lcom/google/android/gms/games/multiplayer/realtime/b;

.field public static final n:Lcom/google/android/gms/games/multiplayer/f;

.field public static final o:Lcom/google/android/gms/games/y;

.field public static final p:Lcom/google/android/gms/games/p;

.field public static final q:Lcom/google/android/gms/games/quest/e;

.field public static final r:Lcom/google/android/gms/games/request/d;

.field public static final s:Lcom/google/android/gms/games/snapshot/h;

.field public static final t:Lcom/google/android/gms/games/internal/game/a;

.field private static final u:Lcom/google/android/gms/common/api/i;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 320
    new-instance v0, Lcom/google/android/gms/common/api/j;

    invoke-direct {v0}, Lcom/google/android/gms/common/api/j;-><init>()V

    sput-object v0, Lcom/google/android/gms/games/d;->a:Lcom/google/android/gms/common/api/j;

    .line 327
    new-instance v0, Lcom/google/android/gms/games/e;

    invoke-direct {v0}, Lcom/google/android/gms/games/e;-><init>()V

    sput-object v0, Lcom/google/android/gms/games/d;->u:Lcom/google/android/gms/common/api/i;

    .line 356
    new-instance v0, Lcom/google/android/gms/common/api/Scope;

    const-string v1, "https://www.googleapis.com/auth/games"

    invoke-direct {v0, v1}, Lcom/google/android/gms/common/api/Scope;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/gms/games/d;->b:Lcom/google/android/gms/common/api/Scope;

    .line 364
    new-instance v0, Lcom/google/android/gms/common/api/c;

    sget-object v1, Lcom/google/android/gms/games/d;->u:Lcom/google/android/gms/common/api/i;

    sget-object v2, Lcom/google/android/gms/games/d;->a:Lcom/google/android/gms/common/api/j;

    new-array v3, v6, [Lcom/google/android/gms/common/api/Scope;

    sget-object v4, Lcom/google/android/gms/games/d;->b:Lcom/google/android/gms/common/api/Scope;

    aput-object v4, v3, v5

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/gms/common/api/c;-><init>(Lcom/google/android/gms/common/api/i;Lcom/google/android/gms/common/api/j;[Lcom/google/android/gms/common/api/Scope;)V

    sput-object v0, Lcom/google/android/gms/games/d;->c:Lcom/google/android/gms/common/api/c;

    .line 373
    new-instance v0, Lcom/google/android/gms/common/api/Scope;

    const-string v1, "https://www.googleapis.com/auth/games.firstparty"

    invoke-direct {v0, v1}, Lcom/google/android/gms/common/api/Scope;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/gms/games/d;->d:Lcom/google/android/gms/common/api/Scope;

    .line 384
    new-instance v0, Lcom/google/android/gms/common/api/c;

    sget-object v1, Lcom/google/android/gms/games/d;->u:Lcom/google/android/gms/common/api/i;

    sget-object v2, Lcom/google/android/gms/games/d;->a:Lcom/google/android/gms/common/api/j;

    new-array v3, v6, [Lcom/google/android/gms/common/api/Scope;

    sget-object v4, Lcom/google/android/gms/games/d;->d:Lcom/google/android/gms/common/api/Scope;

    aput-object v4, v3, v5

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/gms/common/api/c;-><init>(Lcom/google/android/gms/common/api/i;Lcom/google/android/gms/common/api/j;[Lcom/google/android/gms/common/api/Scope;)V

    sput-object v0, Lcom/google/android/gms/games/d;->e:Lcom/google/android/gms/common/api/c;

    .line 390
    new-instance v0, Lcom/google/android/gms/games/internal/a/n;

    invoke-direct {v0}, Lcom/google/android/gms/games/internal/a/n;-><init>()V

    sput-object v0, Lcom/google/android/gms/games/d;->f:Lcom/google/android/gms/games/k;

    .line 395
    new-instance v0, Lcom/google/android/gms/games/internal/a/a;

    invoke-direct {v0}, Lcom/google/android/gms/games/internal/a/a;-><init>()V

    sput-object v0, Lcom/google/android/gms/games/d;->g:Lcom/google/android/gms/games/achievement/c;

    .line 402
    new-instance v0, Lcom/google/android/gms/games/internal/a/l;

    invoke-direct {v0}, Lcom/google/android/gms/games/internal/a/l;-><init>()V

    sput-object v0, Lcom/google/android/gms/games/d;->h:Lcom/google/android/gms/games/appcontent/m;

    .line 407
    new-instance v0, Lcom/google/android/gms/games/internal/a/m;

    invoke-direct {v0}, Lcom/google/android/gms/games/internal/a/m;-><init>()V

    sput-object v0, Lcom/google/android/gms/games/d;->i:Lcom/google/android/gms/games/event/b;

    .line 412
    new-instance v0, Lcom/google/android/gms/games/internal/a/z;

    invoke-direct {v0}, Lcom/google/android/gms/games/internal/a/z;-><init>()V

    sput-object v0, Lcom/google/android/gms/games/d;->j:Lcom/google/android/gms/games/e/n;

    .line 417
    new-instance v0, Lcom/google/android/gms/games/internal/a/u;

    invoke-direct {v0}, Lcom/google/android/gms/games/internal/a/u;-><init>()V

    sput-object v0, Lcom/google/android/gms/games/d;->k:Lcom/google/android/gms/games/multiplayer/d;

    .line 422
    new-instance v0, Lcom/google/android/gms/games/internal/a/cr;

    invoke-direct {v0}, Lcom/google/android/gms/games/internal/a/cr;-><init>()V

    sput-object v0, Lcom/google/android/gms/games/d;->l:Lcom/google/android/gms/games/multiplayer/turnbased/e;

    .line 427
    new-instance v0, Lcom/google/android/gms/games/internal/a/bx;

    invoke-direct {v0}, Lcom/google/android/gms/games/internal/a/bx;-><init>()V

    sput-object v0, Lcom/google/android/gms/games/d;->m:Lcom/google/android/gms/games/multiplayer/realtime/b;

    .line 434
    new-instance v0, Lcom/google/android/gms/games/internal/a/aj;

    invoke-direct {v0}, Lcom/google/android/gms/games/internal/a/aj;-><init>()V

    sput-object v0, Lcom/google/android/gms/games/d;->n:Lcom/google/android/gms/games/multiplayer/f;

    .line 439
    new-instance v0, Lcom/google/android/gms/games/internal/a/ax;

    invoke-direct {v0}, Lcom/google/android/gms/games/internal/a/ax;-><init>()V

    sput-object v0, Lcom/google/android/gms/games/d;->o:Lcom/google/android/gms/games/y;

    .line 444
    new-instance v0, Lcom/google/android/gms/games/internal/a/ak;

    invoke-direct {v0}, Lcom/google/android/gms/games/internal/a/ak;-><init>()V

    sput-object v0, Lcom/google/android/gms/games/d;->p:Lcom/google/android/gms/games/p;

    .line 449
    new-instance v0, Lcom/google/android/gms/games/internal/a/bn;

    invoke-direct {v0}, Lcom/google/android/gms/games/internal/a/bn;-><init>()V

    sput-object v0, Lcom/google/android/gms/games/d;->q:Lcom/google/android/gms/games/quest/e;

    .line 454
    new-instance v0, Lcom/google/android/gms/games/internal/a/by;

    invoke-direct {v0}, Lcom/google/android/gms/games/internal/a/by;-><init>()V

    sput-object v0, Lcom/google/android/gms/games/d;->r:Lcom/google/android/gms/games/request/d;

    .line 459
    new-instance v0, Lcom/google/android/gms/games/internal/a/ck;

    invoke-direct {v0}, Lcom/google/android/gms/games/internal/a/ck;-><init>()V

    sput-object v0, Lcom/google/android/gms/games/d;->s:Lcom/google/android/gms/games/snapshot/h;

    .line 466
    new-instance v0, Lcom/google/android/gms/games/internal/a/f;

    invoke-direct {v0}, Lcom/google/android/gms/games/internal/a/f;-><init>()V

    sput-object v0, Lcom/google/android/gms/games/d;->t:Lcom/google/android/gms/games/internal/game/a;

    return-void
.end method

.method public static a(Lcom/google/android/gms/common/api/v;)Lcom/google/android/gms/games/internal/c;
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 475
    if-eqz p0, :cond_0

    move v0, v1

    :goto_0
    const-string v3, "GoogleApiClient parameter is required."

    invoke-static {v0, v3}, Lcom/google/android/gms/common/internal/bx;->b(ZLjava/lang/Object;)V

    .line 477
    invoke-interface {p0}, Lcom/google/android/gms/common/api/v;->f()Z

    move-result v0

    const-string v3, "GoogleApiClient must be connected."

    invoke-static {v0, v3}, Lcom/google/android/gms/common/internal/bx;->a(ZLjava/lang/Object;)V

    .line 479
    sget-object v0, Lcom/google/android/gms/games/d;->a:Lcom/google/android/gms/common/api/j;

    invoke-interface {p0, v0}, Lcom/google/android/gms/common/api/v;->a(Lcom/google/android/gms/common/api/j;)Lcom/google/android/gms/common/api/h;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/internal/c;

    if-eqz v0, :cond_1

    :goto_1
    const-string v2, "GoogleApiClient is not configured to use the Games Api. Pass Games.API into GoogleApiClient.Builder#addApi() to use this feature."

    invoke-static {v1, v2}, Lcom/google/android/gms/common/internal/bx;->a(ZLjava/lang/Object;)V

    return-object v0

    :cond_0
    move v0, v2

    .line 475
    goto :goto_0

    :cond_1
    move v1, v2

    .line 479
    goto :goto_1
.end method

.method public static a(Lcom/google/android/gms/common/api/v;Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 699
    invoke-static {p0}, Lcom/google/android/gms/games/d;->a(Lcom/google/android/gms/common/api/v;)Lcom/google/android/gms/games/internal/c;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/gms/games/internal/c;->g(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lcom/google/android/gms/common/api/v;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 722
    invoke-static {p0}, Lcom/google/android/gms/games/d;->a(Lcom/google/android/gms/common/api/v;)Lcom/google/android/gms/games/internal/c;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/google/android/gms/games/internal/c;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 724
    return-void
.end method

.method public static b(Lcom/google/android/gms/common/api/v;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 561
    invoke-static {p0}, Lcom/google/android/gms/games/d;->a(Lcom/google/android/gms/common/api/v;)Lcom/google/android/gms/games/internal/c;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/games/internal/c;->f()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static c(Lcom/google/android/gms/common/api/v;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 574
    invoke-static {p0}, Lcom/google/android/gms/games/d;->a(Lcom/google/android/gms/common/api/v;)Lcom/google/android/gms/games/internal/c;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/games/internal/c;->t()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static d(Lcom/google/android/gms/common/api/v;)Landroid/content/Intent;
    .locals 1

    .prologue
    .line 598
    invoke-static {p0}, Lcom/google/android/gms/games/d;->a(Lcom/google/android/gms/common/api/v;)Lcom/google/android/gms/games/internal/c;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/games/internal/c;->s()Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public static e(Lcom/google/android/gms/common/api/v;)Lcom/google/android/gms/common/api/am;
    .locals 1

    .prologue
    .line 626
    new-instance v0, Lcom/google/android/gms/games/f;

    invoke-direct {v0, p0}, Lcom/google/android/gms/games/f;-><init>(Lcom/google/android/gms/common/api/v;)V

    invoke-interface {p0, v0}, Lcom/google/android/gms/common/api/v;->b(Lcom/google/android/gms/common/api/l;)Lcom/google/android/gms/common/api/l;

    move-result-object v0

    return-object v0
.end method

.method public static f(Lcom/google/android/gms/common/api/v;)V
    .locals 1

    .prologue
    .line 771
    invoke-static {p0}, Lcom/google/android/gms/games/d;->a(Lcom/google/android/gms/common/api/v;)Lcom/google/android/gms/games/internal/c;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/games/internal/c;->y()V

    .line 772
    return-void
.end method
