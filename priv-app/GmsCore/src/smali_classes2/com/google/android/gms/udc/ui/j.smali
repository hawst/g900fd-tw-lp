.class public abstract Lcom/google/android/gms/udc/ui/j;
.super Landroid/support/v4/app/m;
.source "SourceFile"


# instance fields
.field protected j:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 16
    invoke-direct {p0}, Landroid/support/v4/app/m;-><init>()V

    .line 24
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/udc/ui/j;->j:Z

    .line 93
    return-void
.end method

.method public static a(Landroid/support/v4/app/v;)Lcom/google/android/gms/udc/ui/j;
    .locals 4

    .prologue
    .line 27
    const/4 v1, 0x0

    .line 29
    :try_start_0
    const-string v0, "UdcDialog"

    invoke-virtual {p0, v0}, Landroid/support/v4/app/v;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/udc/ui/j;
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    .line 33
    :goto_0
    return-object v0

    .line 30
    :catch_0
    move-exception v0

    .line 31
    const-string v2, "UdcMessageFragment"

    const-string v3, "Error finding dialog fragment"

    invoke-static {v2, v3, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    move-object v0, v1

    goto :goto_0
.end method

.method protected static a(Landroid/view/View;ILjava/lang/CharSequence;)V
    .locals 2

    .prologue
    .line 37
    invoke-virtual {p0, p1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 38
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 39
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 43
    :goto_0
    return-void

    .line 41
    :cond_0
    invoke-virtual {v0, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method


# virtual methods
.method public abstract a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Landroid/view/View;
.end method

.method public final a(Landroid/support/v4/app/v;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 83
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/udc/ui/j;->j:Z

    .line 84
    invoke-super {p0, p1, p2}, Landroid/support/v4/app/m;->a(Landroid/support/v4/app/v;Ljava/lang/String;)V

    .line 85
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 47
    invoke-super {p0, p1}, Landroid/support/v4/app/m;->onCreate(Landroid/os/Bundle;)V

    .line 49
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/udc/ui/j;->a(Z)V

    .line 51
    if-eqz p1, :cond_0

    .line 52
    const-string v0, "UdcShownAsDialog"

    iget-boolean v1, p0, Lcom/google/android/gms/udc/ui/j;->j:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/udc/ui/j;->j:Z

    .line 54
    :cond_0
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 1

    .prologue
    .line 61
    iget-boolean v0, p0, Lcom/google/android/gms/udc/ui/j;->j:Z

    if-eqz v0, :cond_0

    .line 62
    const/4 v0, 0x0

    .line 65
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0, p1, p2}, Lcom/google/android/gms/udc/ui/j;->a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    goto :goto_0
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 74
    invoke-super {p0, p1}, Landroid/support/v4/app/m;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 76
    iget-boolean v0, p0, Lcom/google/android/gms/udc/ui/j;->j:Z

    if-eqz v0, :cond_0

    .line 77
    const-string v0, "UdcShownAsDialog"

    const/4 v1, 0x1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 79
    :cond_0
    return-void
.end method
