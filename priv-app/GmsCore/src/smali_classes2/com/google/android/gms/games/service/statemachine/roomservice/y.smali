.class public final Lcom/google/android/gms/games/service/statemachine/roomservice/y;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field protected a:Lcom/google/android/gms/games/service/statemachine/roomservice/DefaultHandlerState;

.field protected b:Lcom/google/android/gms/games/service/statemachine/roomservice/InRoomState;

.field protected c:Lcom/google/android/gms/games/service/statemachine/roomservice/LeavingRoomState;

.field protected d:Lcom/google/android/gms/games/service/statemachine/roomservice/LibrariesLoadedState;

.field protected e:Lcom/google/android/gms/games/service/statemachine/roomservice/LibrariesUnloadedState;

.field protected f:Lcom/google/android/gms/games/service/statemachine/roomservice/NetworkConnectedState;

.field protected g:Lcom/google/android/gms/games/service/statemachine/roomservice/NetworkConnectingState;

.field protected h:Lcom/google/android/gms/games/service/statemachine/roomservice/WaitNetworkDisconnectState;


# direct methods
.method protected constructor <init>()V
    .locals 0

    .prologue
    .line 113
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/games/service/statemachine/roomservice/RoomServiceStateMachine;)V
    .locals 1

    .prologue
    .line 124
    new-instance v0, Lcom/google/android/gms/games/service/statemachine/roomservice/DefaultHandlerState;

    invoke-direct {v0, p1}, Lcom/google/android/gms/games/service/statemachine/roomservice/DefaultHandlerState;-><init>(Lcom/google/android/gms/games/service/statemachine/roomservice/RoomServiceStateMachine;)V

    iput-object v0, p0, Lcom/google/android/gms/games/service/statemachine/roomservice/y;->a:Lcom/google/android/gms/games/service/statemachine/roomservice/DefaultHandlerState;

    .line 125
    new-instance v0, Lcom/google/android/gms/games/service/statemachine/roomservice/InRoomState;

    invoke-direct {v0, p1}, Lcom/google/android/gms/games/service/statemachine/roomservice/InRoomState;-><init>(Lcom/google/android/gms/games/service/statemachine/roomservice/RoomServiceStateMachine;)V

    iput-object v0, p0, Lcom/google/android/gms/games/service/statemachine/roomservice/y;->b:Lcom/google/android/gms/games/service/statemachine/roomservice/InRoomState;

    .line 126
    new-instance v0, Lcom/google/android/gms/games/service/statemachine/roomservice/LeavingRoomState;

    invoke-direct {v0, p1}, Lcom/google/android/gms/games/service/statemachine/roomservice/LeavingRoomState;-><init>(Lcom/google/android/gms/games/service/statemachine/roomservice/RoomServiceStateMachine;)V

    iput-object v0, p0, Lcom/google/android/gms/games/service/statemachine/roomservice/y;->c:Lcom/google/android/gms/games/service/statemachine/roomservice/LeavingRoomState;

    .line 127
    new-instance v0, Lcom/google/android/gms/games/service/statemachine/roomservice/LibrariesLoadedState;

    invoke-direct {v0, p1}, Lcom/google/android/gms/games/service/statemachine/roomservice/LibrariesLoadedState;-><init>(Lcom/google/android/gms/games/service/statemachine/roomservice/RoomServiceStateMachine;)V

    iput-object v0, p0, Lcom/google/android/gms/games/service/statemachine/roomservice/y;->d:Lcom/google/android/gms/games/service/statemachine/roomservice/LibrariesLoadedState;

    .line 128
    new-instance v0, Lcom/google/android/gms/games/service/statemachine/roomservice/LibrariesUnloadedState;

    invoke-direct {v0, p1}, Lcom/google/android/gms/games/service/statemachine/roomservice/LibrariesUnloadedState;-><init>(Lcom/google/android/gms/games/service/statemachine/roomservice/RoomServiceStateMachine;)V

    iput-object v0, p0, Lcom/google/android/gms/games/service/statemachine/roomservice/y;->e:Lcom/google/android/gms/games/service/statemachine/roomservice/LibrariesUnloadedState;

    .line 129
    new-instance v0, Lcom/google/android/gms/games/service/statemachine/roomservice/NetworkConnectedState;

    invoke-direct {v0, p1}, Lcom/google/android/gms/games/service/statemachine/roomservice/NetworkConnectedState;-><init>(Lcom/google/android/gms/games/service/statemachine/roomservice/RoomServiceStateMachine;)V

    iput-object v0, p0, Lcom/google/android/gms/games/service/statemachine/roomservice/y;->f:Lcom/google/android/gms/games/service/statemachine/roomservice/NetworkConnectedState;

    .line 130
    new-instance v0, Lcom/google/android/gms/games/service/statemachine/roomservice/NetworkConnectingState;

    invoke-direct {v0, p1}, Lcom/google/android/gms/games/service/statemachine/roomservice/NetworkConnectingState;-><init>(Lcom/google/android/gms/games/service/statemachine/roomservice/RoomServiceStateMachine;)V

    iput-object v0, p0, Lcom/google/android/gms/games/service/statemachine/roomservice/y;->g:Lcom/google/android/gms/games/service/statemachine/roomservice/NetworkConnectingState;

    .line 131
    new-instance v0, Lcom/google/android/gms/games/service/statemachine/roomservice/WaitNetworkDisconnectState;

    invoke-direct {v0, p1}, Lcom/google/android/gms/games/service/statemachine/roomservice/WaitNetworkDisconnectState;-><init>(Lcom/google/android/gms/games/service/statemachine/roomservice/RoomServiceStateMachine;)V

    iput-object v0, p0, Lcom/google/android/gms/games/service/statemachine/roomservice/y;->h:Lcom/google/android/gms/games/service/statemachine/roomservice/WaitNetworkDisconnectState;

    .line 132
    return-void
.end method
