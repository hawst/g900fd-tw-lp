.class final Lcom/google/android/gms/games/realtime/network/PeerStateMachine$EntryState;
.super Lcom/google/android/gms/games/realtime/network/PeerStateMachine$ContextState;
.source "SourceFile"


# annotations
.annotation build Lcom/google/android/gms/common/util/RetainForClient;
.end annotation


# instance fields
.field final synthetic a:Lcom/google/android/gms/games/realtime/network/PeerStateMachine;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/games/realtime/network/PeerStateMachine;Ljava/lang/String;Lcom/google/android/gms/games/service/statemachine/roomclient/ch;)V
    .locals 6

    .prologue
    .line 343
    iput-object p1, p0, Lcom/google/android/gms/games/realtime/network/PeerStateMachine$EntryState;->a:Lcom/google/android/gms/games/realtime/network/PeerStateMachine;

    .line 344
    const/4 v3, 0x0

    const/4 v4, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v5, p3

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/games/realtime/network/PeerStateMachine$ContextState;-><init>(Lcom/google/android/gms/games/realtime/network/PeerStateMachine;Ljava/lang/String;Ljava/lang/String;ILcom/google/android/gms/games/service/statemachine/roomclient/ch;)V

    .line 345
    iget-object v0, p0, Lcom/google/android/gms/games/realtime/network/PeerStateMachine$EntryState;->d:Lcom/google/android/gms/games/service/statemachine/roomclient/ch;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/service/statemachine/roomclient/ch;->a(I)V

    .line 346
    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Message;)Z
    .locals 8

    .prologue
    const/4 v6, 0x1

    .line 350
    iget v0, p1, Landroid/os/Message;->what:I

    sparse-switch v0, :sswitch_data_0

    .line 384
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 354
    :sswitch_0
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    move-object v3, v0

    check-cast v3, Lcom/google/android/gms/games/jingle/d;

    .line 356
    iget v0, v3, Lcom/google/android/gms/games/jingle/d;->d:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 357
    iget-object v0, p0, Lcom/google/android/gms/games/realtime/network/PeerStateMachine$EntryState;->a:Lcom/google/android/gms/games/realtime/network/PeerStateMachine;

    iget-object v0, v0, Lcom/google/android/gms/games/realtime/network/PeerStateMachine;->a:Lcom/google/android/gms/games/jingle/Libjingle;

    iget-object v1, v3, Lcom/google/android/gms/games/jingle/d;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/jingle/Libjingle;->a(Ljava/lang/String;)V

    .line 358
    iget-object v7, p0, Lcom/google/android/gms/games/realtime/network/PeerStateMachine$EntryState;->a:Lcom/google/android/gms/games/realtime/network/PeerStateMachine;

    new-instance v0, Lcom/google/android/gms/games/realtime/network/PeerStateMachine$AcceptingUnsolicitedRemoteConnection;

    iget-object v1, p0, Lcom/google/android/gms/games/realtime/network/PeerStateMachine$EntryState;->a:Lcom/google/android/gms/games/realtime/network/PeerStateMachine;

    iget-object v2, v3, Lcom/google/android/gms/games/jingle/d;->b:Ljava/lang/String;

    iget-object v3, v3, Lcom/google/android/gms/games/jingle/d;->c:Ljava/lang/String;

    iget v4, p0, Lcom/google/android/gms/games/realtime/network/PeerStateMachine$EntryState;->c:I

    iget-object v5, p0, Lcom/google/android/gms/games/realtime/network/PeerStateMachine$EntryState;->d:Lcom/google/android/gms/games/service/statemachine/roomclient/ch;

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/games/realtime/network/PeerStateMachine$AcceptingUnsolicitedRemoteConnection;-><init>(Lcom/google/android/gms/games/realtime/network/PeerStateMachine;Ljava/lang/String;Ljava/lang/String;ILcom/google/android/gms/games/service/statemachine/roomclient/ch;)V

    iput-object v0, v7, Lcom/google/android/gms/games/realtime/network/PeerStateMachine;->d:Lcom/google/android/gms/games/realtime/network/PeerStateMachine$ContextState;

    :cond_0
    move v0, v6

    .line 361
    goto :goto_0

    .line 365
    :sswitch_1
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    move-object v3, v0

    check-cast v3, Lcom/google/android/gms/games/realtime/network/f;

    .line 366
    iget-object v0, p0, Lcom/google/android/gms/games/realtime/network/PeerStateMachine$EntryState;->a:Lcom/google/android/gms/games/realtime/network/PeerStateMachine;

    iget-object v0, v0, Lcom/google/android/gms/games/realtime/network/PeerStateMachine;->c:Lcom/google/android/gms/games/service/statemachine/d;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Setting peer "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, v3, Lcom/google/android/gms/games/realtime/network/f;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " capabilities to "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, v3, Lcom/google/android/gms/games/realtime/network/f;->c:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/gms/games/service/statemachine/d;->a(Ljava/lang/String;)V

    .line 368
    iget-object v0, p0, Lcom/google/android/gms/games/realtime/network/PeerStateMachine$EntryState;->a:Lcom/google/android/gms/games/realtime/network/PeerStateMachine;

    iget-object v0, v0, Lcom/google/android/gms/games/realtime/network/PeerStateMachine;->a:Lcom/google/android/gms/games/jingle/Libjingle;

    iget-object v1, v3, Lcom/google/android/gms/games/realtime/network/f;->a:Ljava/lang/String;

    iget v2, v3, Lcom/google/android/gms/games/realtime/network/f;->c:I

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/jingle/Libjingle;->b(Ljava/lang/String;I)V

    .line 369
    iget-boolean v0, v3, Lcom/google/android/gms/games/realtime/network/f;->b:Z

    if-eqz v0, :cond_1

    .line 371
    iget-object v7, p0, Lcom/google/android/gms/games/realtime/network/PeerStateMachine$EntryState;->a:Lcom/google/android/gms/games/realtime/network/PeerStateMachine;

    new-instance v0, Lcom/google/android/gms/games/realtime/network/PeerStateMachine$WaitingForExpectedConnection;

    iget-object v1, p0, Lcom/google/android/gms/games/realtime/network/PeerStateMachine$EntryState;->a:Lcom/google/android/gms/games/realtime/network/PeerStateMachine;

    iget-object v2, p0, Lcom/google/android/gms/games/realtime/network/PeerStateMachine$EntryState;->b:Ljava/lang/String;

    iget v3, v3, Lcom/google/android/gms/games/realtime/network/f;->c:I

    iget v4, p0, Lcom/google/android/gms/games/realtime/network/PeerStateMachine$EntryState;->c:I

    iget-object v5, p0, Lcom/google/android/gms/games/realtime/network/PeerStateMachine$EntryState;->d:Lcom/google/android/gms/games/service/statemachine/roomclient/ch;

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/games/realtime/network/PeerStateMachine$WaitingForExpectedConnection;-><init>(Lcom/google/android/gms/games/realtime/network/PeerStateMachine;Ljava/lang/String;IILcom/google/android/gms/games/service/statemachine/roomclient/ch;)V

    iput-object v0, v7, Lcom/google/android/gms/games/realtime/network/PeerStateMachine;->d:Lcom/google/android/gms/games/realtime/network/PeerStateMachine$ContextState;

    :goto_1
    move v0, v6

    .line 380
    goto :goto_0

    .line 375
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/games/realtime/network/PeerStateMachine$EntryState;->a:Lcom/google/android/gms/games/realtime/network/PeerStateMachine;

    iget-object v0, v0, Lcom/google/android/gms/games/realtime/network/PeerStateMachine;->a:Lcom/google/android/gms/games/jingle/Libjingle;

    const-string v1, ""

    iget-object v2, v3, Lcom/google/android/gms/games/realtime/network/f;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/jingle/Libjingle;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 377
    iget-object v7, p0, Lcom/google/android/gms/games/realtime/network/PeerStateMachine$EntryState;->a:Lcom/google/android/gms/games/realtime/network/PeerStateMachine;

    new-instance v0, Lcom/google/android/gms/games/realtime/network/PeerStateMachine$WaitingForOutgoingRemoteConnectionNoSessionId;

    iget-object v1, p0, Lcom/google/android/gms/games/realtime/network/PeerStateMachine$EntryState;->a:Lcom/google/android/gms/games/realtime/network/PeerStateMachine;

    iget-object v2, p0, Lcom/google/android/gms/games/realtime/network/PeerStateMachine$EntryState;->b:Ljava/lang/String;

    iget v3, v3, Lcom/google/android/gms/games/realtime/network/f;->c:I

    iget v4, p0, Lcom/google/android/gms/games/realtime/network/PeerStateMachine$EntryState;->c:I

    iget-object v5, p0, Lcom/google/android/gms/games/realtime/network/PeerStateMachine$EntryState;->d:Lcom/google/android/gms/games/service/statemachine/roomclient/ch;

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/games/realtime/network/PeerStateMachine$WaitingForOutgoingRemoteConnectionNoSessionId;-><init>(Lcom/google/android/gms/games/realtime/network/PeerStateMachine;Ljava/lang/String;IILcom/google/android/gms/games/service/statemachine/roomclient/ch;)V

    iput-object v0, v7, Lcom/google/android/gms/games/realtime/network/PeerStateMachine;->d:Lcom/google/android/gms/games/realtime/network/PeerStateMachine$ContextState;

    goto :goto_1

    .line 350
    :sswitch_data_0
    .sparse-switch
        0x2 -> :sswitch_0
        0x1779 -> :sswitch_1
    .end sparse-switch
.end method
