.class final Lcom/google/android/gms/games/a/bj;
.super Lcom/google/android/gms/games/a/bb;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/games/a/ce;


# static fields
.field private static final A:Lcom/google/android/gms/games/provider/a;

.field private static final B:Lcom/google/android/gms/games/provider/a;

.field private static final C:Lcom/google/android/gms/games/provider/a;

.field private static final D:Landroid/net/Uri;

.field private static final R:Lcom/google/android/gms/games/provider/a;

.field private static final m:Ljava/util/concurrent/locks/ReentrantLock;

.field private static final n:[Ljava/lang/String;

.field private static final o:Ljava/util/concurrent/locks/ReentrantLock;

.field private static final p:Ljava/util/concurrent/locks/ReentrantLock;

.field private static final q:Ljava/util/concurrent/locks/ReentrantLock;

.field private static final r:Ljava/util/concurrent/locks/ReentrantLock;

.field private static final s:Ljava/util/concurrent/locks/ReentrantLock;

.field private static final t:Ljava/util/concurrent/locks/ReentrantLock;

.field private static final u:Ljava/util/concurrent/locks/ReentrantLock;

.field private static final v:Ljava/util/concurrent/locks/ReentrantLock;

.field private static final w:Ljava/util/concurrent/locks/ReentrantLock;

.field private static final x:Ljava/util/concurrent/locks/ReentrantLock;

.field private static final y:Ljava/util/ArrayList;

.field private static final z:Lcom/google/android/gms/games/provider/a;


# instance fields
.field private final E:Lcom/google/android/gms/games/h/a/di;

.field private final F:Lcom/google/android/gms/games/h/a/dj;

.field private final G:Lcom/google/android/gms/plus/service/v1/c;

.field private final H:Lcom/google/android/gms/plus/service/v1whitelisted/i;

.field private final I:Lcom/google/android/gms/games/h/a/ci;

.field private final J:Lcom/google/android/gms/games/h/a/bf;

.field private final K:Lcom/google/android/gms/common/server/w;

.field private final L:Lcom/google/android/gms/games/b/p;

.field private final M:Lcom/google/android/gms/games/b/p;

.field private final N:Lcom/google/android/gms/games/b/q;

.field private final O:Ljava/util/HashMap;

.field private final P:Lcom/google/android/gms/games/b/i;

.field private final Q:Lcom/google/android/gms/games/b/r;

.field private final S:Lcom/google/android/gms/games/b/w;

.field private T:J

.field final a:Lcom/google/android/gms/games/a/bb;

.field final b:Lcom/google/android/gms/games/a/bb;

.field final c:Lcom/google/android/gms/games/a/bb;

.field final d:Lcom/google/android/gms/games/a/bb;

.field final e:Lcom/google/android/gms/games/a/bb;

.field final f:Lcom/google/android/gms/games/a/bb;

.field final g:Lcom/google/android/gms/games/a/bb;

.field final h:Lcom/google/android/gms/games/a/bb;

.field final i:Lcom/google/android/gms/games/a/bb;

.field final j:Lcom/google/android/gms/games/a/bb;

.field final k:[Lcom/google/android/gms/games/a/bb;

.field l:J


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 123
    new-instance v0, Ljava/util/concurrent/locks/ReentrantLock;

    invoke-direct {v0}, Ljava/util/concurrent/locks/ReentrantLock;-><init>()V

    sput-object v0, Lcom/google/android/gms/games/a/bj;->m:Ljava/util/concurrent/locks/ReentrantLock;

    .line 140
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "xp_sync_token"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/gms/games/a/bj;->n:[Ljava/lang/String;

    .line 144
    new-instance v0, Ljava/util/concurrent/locks/ReentrantLock;

    invoke-direct {v0}, Ljava/util/concurrent/locks/ReentrantLock;-><init>()V

    sput-object v0, Lcom/google/android/gms/games/a/bj;->o:Ljava/util/concurrent/locks/ReentrantLock;

    .line 145
    new-instance v0, Ljava/util/concurrent/locks/ReentrantLock;

    invoke-direct {v0}, Ljava/util/concurrent/locks/ReentrantLock;-><init>()V

    sput-object v0, Lcom/google/android/gms/games/a/bj;->p:Ljava/util/concurrent/locks/ReentrantLock;

    .line 146
    new-instance v0, Ljava/util/concurrent/locks/ReentrantLock;

    invoke-direct {v0}, Ljava/util/concurrent/locks/ReentrantLock;-><init>()V

    sput-object v0, Lcom/google/android/gms/games/a/bj;->q:Ljava/util/concurrent/locks/ReentrantLock;

    .line 147
    new-instance v0, Ljava/util/concurrent/locks/ReentrantLock;

    invoke-direct {v0}, Ljava/util/concurrent/locks/ReentrantLock;-><init>()V

    sput-object v0, Lcom/google/android/gms/games/a/bj;->r:Ljava/util/concurrent/locks/ReentrantLock;

    .line 148
    new-instance v0, Ljava/util/concurrent/locks/ReentrantLock;

    invoke-direct {v0}, Ljava/util/concurrent/locks/ReentrantLock;-><init>()V

    sput-object v0, Lcom/google/android/gms/games/a/bj;->s:Ljava/util/concurrent/locks/ReentrantLock;

    .line 149
    new-instance v0, Ljava/util/concurrent/locks/ReentrantLock;

    invoke-direct {v0}, Ljava/util/concurrent/locks/ReentrantLock;-><init>()V

    sput-object v0, Lcom/google/android/gms/games/a/bj;->t:Ljava/util/concurrent/locks/ReentrantLock;

    .line 150
    new-instance v0, Ljava/util/concurrent/locks/ReentrantLock;

    invoke-direct {v0}, Ljava/util/concurrent/locks/ReentrantLock;-><init>()V

    sput-object v0, Lcom/google/android/gms/games/a/bj;->u:Ljava/util/concurrent/locks/ReentrantLock;

    .line 151
    new-instance v0, Ljava/util/concurrent/locks/ReentrantLock;

    invoke-direct {v0}, Ljava/util/concurrent/locks/ReentrantLock;-><init>()V

    sput-object v0, Lcom/google/android/gms/games/a/bj;->v:Ljava/util/concurrent/locks/ReentrantLock;

    .line 152
    new-instance v0, Ljava/util/concurrent/locks/ReentrantLock;

    invoke-direct {v0}, Ljava/util/concurrent/locks/ReentrantLock;-><init>()V

    sput-object v0, Lcom/google/android/gms/games/a/bj;->w:Ljava/util/concurrent/locks/ReentrantLock;

    .line 153
    new-instance v0, Ljava/util/concurrent/locks/ReentrantLock;

    invoke-direct {v0}, Ljava/util/concurrent/locks/ReentrantLock;-><init>()V

    sput-object v0, Lcom/google/android/gms/games/a/bj;->x:Ljava/util/concurrent/locks/ReentrantLock;

    .line 225
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 227
    sput-object v0, Lcom/google/android/gms/games/a/bj;->y:Ljava/util/ArrayList;

    const-string v1, "person"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 269
    invoke-static {}, Lcom/google/android/gms/games/provider/a;->a()Lcom/google/android/gms/games/provider/c;

    move-result-object v0

    sget-object v1, Lcom/google/android/gms/games/k/a;->e:Lcom/google/android/gms/games/provider/a;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/provider/c;->a(Lcom/google/android/gms/games/provider/a;)Lcom/google/android/gms/games/provider/c;

    move-result-object v0

    const-string v1, "game_icon_image_uri"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->a:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/c;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/c;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/games/provider/c;->a()Lcom/google/android/gms/games/provider/a;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/games/a/bj;->z:Lcom/google/android/gms/games/provider/a;

    .line 277
    invoke-static {}, Lcom/google/android/gms/games/provider/a;->a()Lcom/google/android/gms/games/provider/c;

    move-result-object v0

    const-string v1, "external_game_id"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->a:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/c;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/c;

    move-result-object v0

    const-string v1, "notification_id"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->a:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/c;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/c;

    move-result-object v0

    const-string v1, "external_sub_id"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->a:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/c;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/c;

    move-result-object v0

    const-string v1, "type"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->c:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/c;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/c;

    move-result-object v0

    const-string v1, "image_uri"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->a:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/c;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/c;

    move-result-object v0

    const-string v1, "ticker"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->a:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/c;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/c;

    move-result-object v0

    const-string v1, "title"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->a:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/c;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/c;

    move-result-object v0

    const-string v1, "text"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->a:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/c;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/c;

    move-result-object v0

    const-string v1, "coalesced_text"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->a:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/c;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/c;

    move-result-object v0

    const-string v1, "acknowledged"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->c:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/c;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/c;

    move-result-object v0

    const-string v1, "alert_level"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->c:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/c;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/c;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/games/provider/c;->a()Lcom/google/android/gms/games/provider/a;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/games/a/bj;->A:Lcom/google/android/gms/games/provider/a;

    .line 294
    invoke-static {}, Lcom/google/android/gms/games/provider/a;->a()Lcom/google/android/gms/games/provider/c;

    move-result-object v0

    const-string v1, "level_notifications_enabled"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->b:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/c;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/c;

    move-result-object v0

    const-string v1, "match_notifications_enabled"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->b:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/c;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/c;

    move-result-object v0

    const-string v1, "mobile_notifications_enabled"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->b:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/c;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/c;

    move-result-object v0

    const-string v1, "quest_notifications_enabled"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->b:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/c;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/c;

    move-result-object v0

    const-string v1, "request_notifications_enabled"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->b:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/c;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/c;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/games/provider/c;->a()Lcom/google/android/gms/games/provider/a;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/games/a/bj;->B:Lcom/google/android/gms/games/provider/a;

    .line 305
    invoke-static {}, Lcom/google/android/gms/games/provider/a;->a()Lcom/google/android/gms/games/provider/c;

    move-result-object v0

    const-string v1, "profile_visible"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->b:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/c;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/c;

    move-result-object v0

    const-string v1, "profile_visibility_explicitly_set"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->b:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/c;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/c;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/games/provider/c;->a()Lcom/google/android/gms/games/provider/a;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/games/a/bj;->C:Lcom/google/android/gms/games/provider/a;

    .line 348
    new-instance v0, Landroid/net/Uri$Builder;

    invoke-direct {v0}, Landroid/net/Uri$Builder;-><init>()V

    const-string v1, "android.resource"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->scheme(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "com.google.android.gms"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->authority(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "drawable"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "games_ic_levelup_shade"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/games/a/bj;->D:Landroid/net/Uri;

    .line 430
    invoke-static {}, Lcom/google/android/gms/games/provider/a;->a()Lcom/google/android/gms/games/provider/c;

    move-result-object v0

    const-string v1, "external_experience_id"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->a:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/c;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/c;

    move-result-object v0

    const-string v1, "game_id"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->c:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/c;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/c;

    move-result-object v0

    const-string v1, "created_timestamp"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->d:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/c;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/c;

    move-result-object v0

    const-string v1, "current_xp"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->d:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/c;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/c;

    move-result-object v0

    const-string v1, "display_title"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->a:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/c;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/c;

    move-result-object v0

    const-string v1, "display_description"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->a:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/c;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/c;

    move-result-object v0

    const-string v1, "display_string"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->a:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/c;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/c;

    move-result-object v0

    const-string v1, "xp_earned"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->d:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/c;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/c;

    move-result-object v0

    const-string v1, "type"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->c:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/c;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/c;

    move-result-object v0

    const-string v1, "icon_id"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->c:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/c;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/c;

    move-result-object v0

    const-string v1, "icon_url"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->a:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/c;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/c;

    move-result-object v0

    const-string v1, "icon_uri"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->a:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/c;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/c;

    move-result-object v0

    const-string v1, "newLevel"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->c:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/c;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/c;

    move-result-object v0

    sget-object v1, Lcom/google/android/gms/games/k/a;->a:Lcom/google/android/gms/games/provider/a;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/provider/c;->a(Lcom/google/android/gms/games/provider/a;)Lcom/google/android/gms/games/provider/c;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/games/provider/c;->a()Lcom/google/android/gms/games/provider/a;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/games/a/bj;->R:Lcom/google/android/gms/games/provider/a;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/gms/games/a/bb;Lcom/google/android/gms/common/server/n;Lcom/google/android/gms/common/server/n;Lcom/google/android/gms/common/server/n;Lcom/google/android/gms/common/server/n;)V
    .locals 3

    .prologue
    .line 461
    const-string v0, "PlayerAgent"

    sget-object v1, Lcom/google/android/gms/games/a/bj;->m:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-direct {p0, v0, v1, p1}, Lcom/google/android/gms/games/a/bb;-><init>(Ljava/lang/String;Ljava/util/concurrent/locks/ReentrantLock;Lcom/google/android/gms/games/a/bb;)V

    .line 388
    new-instance v0, Lcom/google/android/gms/common/server/w;

    invoke-direct {v0}, Lcom/google/android/gms/common/server/w;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/games/a/bj;->K:Lcom/google/android/gms/common/server/w;

    .line 456
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/gms/games/a/bj;->T:J

    .line 462
    new-instance v0, Lcom/google/android/gms/games/h/a/di;

    invoke-direct {v0, p2}, Lcom/google/android/gms/games/h/a/di;-><init>(Lcom/google/android/gms/common/server/n;)V

    iput-object v0, p0, Lcom/google/android/gms/games/a/bj;->E:Lcom/google/android/gms/games/h/a/di;

    .line 463
    new-instance v0, Lcom/google/android/gms/games/h/a/dj;

    invoke-direct {v0, p3}, Lcom/google/android/gms/games/h/a/dj;-><init>(Lcom/google/android/gms/common/server/n;)V

    iput-object v0, p0, Lcom/google/android/gms/games/a/bj;->F:Lcom/google/android/gms/games/h/a/dj;

    .line 464
    new-instance v0, Lcom/google/android/gms/plus/service/v1/c;

    invoke-direct {v0, p4}, Lcom/google/android/gms/plus/service/v1/c;-><init>(Lcom/google/android/gms/common/server/n;)V

    iput-object v0, p0, Lcom/google/android/gms/games/a/bj;->G:Lcom/google/android/gms/plus/service/v1/c;

    .line 465
    new-instance v0, Lcom/google/android/gms/plus/service/v1whitelisted/i;

    invoke-direct {v0, p5}, Lcom/google/android/gms/plus/service/v1whitelisted/i;-><init>(Lcom/google/android/gms/common/server/n;)V

    iput-object v0, p0, Lcom/google/android/gms/games/a/bj;->H:Lcom/google/android/gms/plus/service/v1whitelisted/i;

    .line 466
    new-instance v0, Lcom/google/android/gms/games/h/a/ci;

    invoke-direct {v0, p3}, Lcom/google/android/gms/games/h/a/ci;-><init>(Lcom/google/android/gms/common/server/n;)V

    iput-object v0, p0, Lcom/google/android/gms/games/a/bj;->I:Lcom/google/android/gms/games/h/a/ci;

    .line 467
    new-instance v0, Lcom/google/android/gms/games/h/a/bf;

    invoke-direct {v0, p3}, Lcom/google/android/gms/games/h/a/bf;-><init>(Lcom/google/android/gms/common/server/n;)V

    iput-object v0, p0, Lcom/google/android/gms/games/a/bj;->J:Lcom/google/android/gms/games/h/a/bf;

    .line 468
    new-instance v0, Lcom/google/android/gms/games/b/p;

    sget-object v1, Lcom/google/android/gms/games/a/bj;->z:Lcom/google/android/gms/games/provider/a;

    iget-object v1, v1, Lcom/google/android/gms/games/provider/a;->b:[Ljava/lang/String;

    invoke-direct {v0, v1}, Lcom/google/android/gms/games/b/p;-><init>([Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/gms/games/a/bj;->L:Lcom/google/android/gms/games/b/p;

    .line 469
    new-instance v0, Lcom/google/android/gms/games/b/p;

    sget-object v1, Lcom/google/android/gms/games/a/bj;->z:Lcom/google/android/gms/games/provider/a;

    iget-object v1, v1, Lcom/google/android/gms/games/provider/a;->b:[Ljava/lang/String;

    invoke-direct {v0, v1}, Lcom/google/android/gms/games/b/p;-><init>([Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/gms/games/a/bj;->M:Lcom/google/android/gms/games/b/p;

    .line 470
    new-instance v0, Lcom/google/android/gms/games/b/q;

    sget-object v1, Lcom/google/android/gms/games/a/bj;->z:Lcom/google/android/gms/games/provider/a;

    iget-object v1, v1, Lcom/google/android/gms/games/provider/a;->b:[Ljava/lang/String;

    invoke-direct {v0, v1}, Lcom/google/android/gms/games/b/q;-><init>([Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/gms/games/a/bj;->N:Lcom/google/android/gms/games/b/q;

    .line 471
    new-instance v0, Lcom/google/android/gms/games/b/i;

    sget-object v1, Lcom/google/android/gms/games/a/bj;->B:Lcom/google/android/gms/games/provider/a;

    iget-object v1, v1, Lcom/google/android/gms/games/provider/a;->b:[Ljava/lang/String;

    invoke-direct {v0, v1}, Lcom/google/android/gms/games/b/i;-><init>([Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/gms/games/a/bj;->P:Lcom/google/android/gms/games/b/i;

    .line 472
    new-instance v0, Lcom/google/android/gms/games/b/r;

    sget-object v1, Lcom/google/android/gms/games/a/bj;->C:Lcom/google/android/gms/games/provider/a;

    iget-object v1, v1, Lcom/google/android/gms/games/provider/a;->b:[Ljava/lang/String;

    invoke-direct {v0, v1}, Lcom/google/android/gms/games/b/r;-><init>([Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/gms/games/a/bj;->Q:Lcom/google/android/gms/games/b/r;

    .line 473
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/games/a/bj;->O:Ljava/util/HashMap;

    .line 474
    new-instance v0, Lcom/google/android/gms/games/b/w;

    sget-object v1, Lcom/google/android/gms/games/a/bj;->R:Lcom/google/android/gms/games/provider/a;

    iget-object v1, v1, Lcom/google/android/gms/games/provider/a;->b:[Ljava/lang/String;

    invoke-direct {v0, v1}, Lcom/google/android/gms/games/b/w;-><init>([Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/gms/games/a/bj;->S:Lcom/google/android/gms/games/b/w;

    .line 477
    new-instance v0, Lcom/google/android/gms/games/a/bb;

    const-string v1, "PlayerCoverPhotoUris"

    sget-object v2, Lcom/google/android/gms/games/a/bj;->o:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-direct {v0, v1, v2, p1}, Lcom/google/android/gms/games/a/bb;-><init>(Ljava/lang/String;Ljava/util/concurrent/locks/ReentrantLock;Lcom/google/android/gms/games/a/bb;)V

    iput-object v0, p0, Lcom/google/android/gms/games/a/bj;->a:Lcom/google/android/gms/games/a/bb;

    .line 479
    new-instance v0, Lcom/google/android/gms/games/a/bb;

    const-string v1, "RecentPlayersInCircles"

    sget-object v2, Lcom/google/android/gms/games/a/bj;->p:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-direct {v0, v1, v2, p1}, Lcom/google/android/gms/games/a/bb;-><init>(Ljava/lang/String;Ljava/util/concurrent/locks/ReentrantLock;Lcom/google/android/gms/games/a/bb;)V

    iput-object v0, p0, Lcom/google/android/gms/games/a/bj;->b:Lcom/google/android/gms/games/a/bb;

    .line 481
    new-instance v0, Lcom/google/android/gms/games/a/bb;

    const-string v1, "RecentlyPlayedWith"

    sget-object v2, Lcom/google/android/gms/games/a/bj;->q:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-direct {v0, v1, v2, p1}, Lcom/google/android/gms/games/a/bb;-><init>(Ljava/lang/String;Ljava/util/concurrent/locks/ReentrantLock;Lcom/google/android/gms/games/a/bb;)V

    iput-object v0, p0, Lcom/google/android/gms/games/a/bj;->c:Lcom/google/android/gms/games/a/bb;

    .line 483
    new-instance v0, Lcom/google/android/gms/games/a/bb;

    const-string v1, "PlayersYouMayKnow"

    sget-object v2, Lcom/google/android/gms/games/a/bj;->r:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-direct {v0, v1, v2, p1}, Lcom/google/android/gms/games/a/bb;-><init>(Ljava/lang/String;Ljava/util/concurrent/locks/ReentrantLock;Lcom/google/android/gms/games/a/bb;)V

    iput-object v0, p0, Lcom/google/android/gms/games/a/bj;->d:Lcom/google/android/gms/games/a/bb;

    .line 485
    new-instance v0, Lcom/google/android/gms/games/a/bb;

    const-string v1, "CircledPlayers"

    sget-object v2, Lcom/google/android/gms/games/a/bj;->s:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-direct {v0, v1, v2, p1}, Lcom/google/android/gms/games/a/bb;-><init>(Ljava/lang/String;Ljava/util/concurrent/locks/ReentrantLock;Lcom/google/android/gms/games/a/bb;)V

    iput-object v0, p0, Lcom/google/android/gms/games/a/bj;->e:Lcom/google/android/gms/games/a/bb;

    .line 486
    new-instance v0, Lcom/google/android/gms/games/a/bb;

    const-string v1, "VisiblePlayers"

    sget-object v2, Lcom/google/android/gms/games/a/bj;->t:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-direct {v0, v1, v2, p1}, Lcom/google/android/gms/games/a/bb;-><init>(Ljava/lang/String;Ljava/util/concurrent/locks/ReentrantLock;Lcom/google/android/gms/games/a/bb;)V

    iput-object v0, p0, Lcom/google/android/gms/games/a/bj;->f:Lcom/google/android/gms/games/a/bb;

    .line 487
    new-instance v0, Lcom/google/android/gms/games/a/bb;

    const-string v1, "SuggestedPlayers"

    sget-object v2, Lcom/google/android/gms/games/a/bj;->u:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-direct {v0, v1, v2, p1}, Lcom/google/android/gms/games/a/bb;-><init>(Ljava/lang/String;Ljava/util/concurrent/locks/ReentrantLock;Lcom/google/android/gms/games/a/bb;)V

    iput-object v0, p0, Lcom/google/android/gms/games/a/bj;->g:Lcom/google/android/gms/games/a/bb;

    .line 489
    new-instance v0, Lcom/google/android/gms/games/a/bb;

    const-string v1, "ConnectedPlayers"

    sget-object v2, Lcom/google/android/gms/games/a/bj;->v:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-direct {v0, v1, v2, p1}, Lcom/google/android/gms/games/a/bb;-><init>(Ljava/lang/String;Ljava/util/concurrent/locks/ReentrantLock;Lcom/google/android/gms/games/a/bb;)V

    iput-object v0, p0, Lcom/google/android/gms/games/a/bj;->h:Lcom/google/android/gms/games/a/bb;

    .line 491
    new-instance v0, Lcom/google/android/gms/games/a/bb;

    const-string v1, "SearchPlayers"

    sget-object v2, Lcom/google/android/gms/games/a/bj;->w:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-direct {v0, v1, v2, p1}, Lcom/google/android/gms/games/a/bb;-><init>(Ljava/lang/String;Ljava/util/concurrent/locks/ReentrantLock;Lcom/google/android/gms/games/a/bb;)V

    iput-object v0, p0, Lcom/google/android/gms/games/a/bj;->i:Lcom/google/android/gms/games/a/bb;

    .line 492
    new-instance v0, Lcom/google/android/gms/games/a/bb;

    const-string v1, "NearbyPlayers"

    sget-object v2, Lcom/google/android/gms/games/a/bj;->x:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-direct {v0, v1, v2, p1}, Lcom/google/android/gms/games/a/bb;-><init>(Ljava/lang/String;Ljava/util/concurrent/locks/ReentrantLock;Lcom/google/android/gms/games/a/bb;)V

    iput-object v0, p0, Lcom/google/android/gms/games/a/bj;->j:Lcom/google/android/gms/games/a/bb;

    .line 495
    const/16 v0, 0x8

    new-array v0, v0, [Lcom/google/android/gms/games/a/bb;

    const/4 v1, 0x0

    aput-object p0, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/gms/games/a/bj;->b:Lcom/google/android/gms/games/a/bb;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/gms/games/a/bj;->c:Lcom/google/android/gms/games/a/bb;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/android/gms/games/a/bj;->e:Lcom/google/android/gms/games/a/bb;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/android/gms/games/a/bj;->f:Lcom/google/android/gms/games/a/bb;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    iget-object v2, p0, Lcom/google/android/gms/games/a/bj;->g:Lcom/google/android/gms/games/a/bb;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    iget-object v2, p0, Lcom/google/android/gms/games/a/bj;->h:Lcom/google/android/gms/games/a/bb;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    iget-object v2, p0, Lcom/google/android/gms/games/a/bj;->j:Lcom/google/android/gms/games/a/bb;

    aput-object v2, v0, v1

    iput-object v0, p0, Lcom/google/android/gms/games/a/bj;->k:[Lcom/google/android/gms/games/a/bb;

    .line 505
    return-void
.end method

.method private a(Lcom/google/android/gms/games/a/au;J)Landroid/util/Pair;
    .locals 6

    .prologue
    const/4 v3, 0x0

    .line 2282
    iget-object v1, p1, Lcom/google/android/gms/games/a/au;->b:Lcom/google/android/gms/common/server/ClientContext;

    .line 2283
    new-instance v2, Lcom/google/android/gms/common/e/b;

    invoke-static {v1}, Lcom/google/android/gms/games/provider/ap;->a(Lcom/google/android/gms/common/server/ClientContext;)Landroid/net/Uri;

    move-result-object v0

    invoke-direct {v2, v0}, Lcom/google/android/gms/common/e/b;-><init>(Landroid/net/Uri;)V

    .line 2284
    const-string v0, "level_max_xp"

    const-string v4, ">?"

    invoke-static {p2, p3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v0, v5, v4}, Lcom/google/android/gms/common/e/b;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2287
    new-instance v0, Lcom/google/android/gms/games/a/o;

    invoke-direct {v0, p1}, Lcom/google/android/gms/games/a/o;-><init>(Lcom/google/android/gms/games/a/au;)V

    iput-object v2, v0, Lcom/google/android/gms/games/a/o;->a:Lcom/google/android/gms/common/e/b;

    sget-object v4, Lcom/google/android/gms/games/a/bl;->a:[Ljava/lang/String;

    iput-object v4, v0, Lcom/google/android/gms/games/a/o;->b:[Ljava/lang/String;

    const-string v4, "level_value ASC LIMIT 2"

    iput-object v4, v0, Lcom/google/android/gms/games/a/o;->c:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/google/android/gms/games/a/o;->b()Landroid/database/AbstractWindowedCursor;

    move-result-object v0

    .line 2294
    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v4

    if-nez v4, :cond_2

    .line 2295
    const-string v4, "PlayerAgent"

    const-string v5, "Refreshing level table, new current level missing."

    invoke-static {v4, v5}, Lcom/google/android/gms/games/internal/dq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2296
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 2298
    iget-object v0, p1, Lcom/google/android/gms/games/a/au;->a:Landroid/content/Context;

    invoke-static {v1}, Lcom/google/android/gms/games/a/l;->b(Lcom/google/android/gms/common/server/ClientContext;)Lcom/google/android/gms/common/server/ClientContext;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/games/a/bj;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;)I

    move-result v0

    .line 2300
    if-eqz v0, :cond_1

    .line 2301
    const-string v1, "PlayerAgent"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v4, "Failed to refresh level table: "

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Lcom/google/android/gms/games/o;->b(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/google/android/gms/games/internal/dq;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2339
    :cond_0
    :goto_0
    return-object v3

    .line 2307
    :cond_1
    new-instance v0, Lcom/google/android/gms/games/a/o;

    invoke-direct {v0, p1}, Lcom/google/android/gms/games/a/o;-><init>(Lcom/google/android/gms/games/a/au;)V

    iput-object v2, v0, Lcom/google/android/gms/games/a/o;->a:Lcom/google/android/gms/common/e/b;

    sget-object v1, Lcom/google/android/gms/games/a/bl;->a:[Ljava/lang/String;

    iput-object v1, v0, Lcom/google/android/gms/games/a/o;->b:[Ljava/lang/String;

    const-string v1, "level_value ASC LIMIT 2"

    iput-object v1, v0, Lcom/google/android/gms/games/a/o;->c:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/google/android/gms/games/a/o;->b()Landroid/database/AbstractWindowedCursor;

    move-result-object v0

    .line 2318
    :cond_2
    :try_start_0
    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 2319
    invoke-static {v0}, Lcom/google/android/gms/games/a/bj;->a(Landroid/database/Cursor;)Lcom/google/android/gms/games/PlayerLevel;

    move-result-object v2

    .line 2322
    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 2324
    invoke-static {v0}, Lcom/google/android/gms/games/a/bj;->a(Landroid/database/Cursor;)Lcom/google/android/gms/games/PlayerLevel;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 2331
    :goto_1
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 2335
    if-eqz v2, :cond_0

    .line 2339
    new-instance v3, Landroid/util/Pair;

    invoke-direct {v3, v2, v1}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_0

    :cond_3
    move-object v1, v2

    .line 2327
    goto :goto_1

    .line 2331
    :catchall_0
    move-exception v1

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    throw v1

    :cond_4
    move-object v1, v3

    move-object v2, v3

    goto :goto_1
.end method

.method private static a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Lcom/google/android/gms/games/a/bm;Ljava/lang/String;JJLcom/google/android/gms/games/b/p;)Lcom/google/android/gms/common/data/DataHolder;
    .locals 18

    .prologue
    .line 2506
    move-object/from16 v0, p2

    iget-object v2, v0, Lcom/google/android/gms/games/a/bm;->a:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v5

    .line 2507
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4, v5}, Ljava/util/ArrayList;-><init>(I)V

    .line 2509
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6, v5}, Ljava/util/ArrayList;-><init>(I)V

    .line 2510
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7, v5}, Ljava/util/ArrayList;-><init>(I)V

    .line 2511
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8, v5}, Ljava/util/ArrayList;-><init>(I)V

    .line 2512
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9, v5}, Ljava/util/ArrayList;-><init>(I)V

    .line 2513
    new-instance v10, Ljava/util/ArrayList;

    invoke-direct {v10, v5}, Ljava/util/ArrayList;-><init>(I)V

    .line 2514
    invoke-static/range {p1 .. p1}, Lcom/google/android/gms/games/provider/ad;->a(Lcom/google/android/gms/common/server/ClientContext;)Landroid/net/Uri;

    move-result-object v11

    .line 2515
    const/4 v2, 0x0

    move v3, v2

    :goto_0
    if-ge v3, v5, :cond_0

    .line 2516
    move-object/from16 v0, p2

    iget-object v2, v0, Lcom/google/android/gms/games/a/bm;->a:Ljava/util/ArrayList;

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/content/ContentValues;

    .line 2517
    const-string v12, "profile_icon_image_url"

    invoke-virtual {v2, v12}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    .line 2518
    const-string v13, "profile_hi_res_image_url"

    invoke-virtual {v2, v13}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    .line 2519
    const-string v14, "most_recent_game_icon_url"

    invoke-virtual {v2, v14}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    .line 2520
    const-string v15, "most_recent_game_hi_res_url"

    invoke-virtual {v2, v15}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    .line 2521
    const-string v16, "most_recent_game_featured_url"

    move-object/from16 v0, v16

    invoke-virtual {v2, v0}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 2522
    invoke-static {v11, v12, v4}, Lcom/google/android/gms/games/a/l;->a(Landroid/net/Uri;Ljava/lang/String;Ljava/util/ArrayList;)Ljava/lang/Integer;

    move-result-object v12

    invoke-virtual {v6, v12}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2523
    invoke-static {v11, v13, v4}, Lcom/google/android/gms/games/a/l;->a(Landroid/net/Uri;Ljava/lang/String;Ljava/util/ArrayList;)Ljava/lang/Integer;

    move-result-object v12

    invoke-virtual {v7, v12}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2524
    invoke-static {v11, v14, v4}, Lcom/google/android/gms/games/a/l;->a(Landroid/net/Uri;Ljava/lang/String;Ljava/util/ArrayList;)Ljava/lang/Integer;

    move-result-object v12

    invoke-virtual {v8, v12}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2525
    invoke-static {v11, v15, v4}, Lcom/google/android/gms/games/a/l;->a(Landroid/net/Uri;Ljava/lang/String;Ljava/util/ArrayList;)Ljava/lang/Integer;

    move-result-object v12

    invoke-virtual {v9, v12}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2526
    invoke-static {v11, v2, v4}, Lcom/google/android/gms/games/a/l;->a(Landroid/net/Uri;Ljava/lang/String;Ljava/util/ArrayList;)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v10, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2515
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_0

    .line 2529
    :cond_0
    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ne v2, v5, :cond_1

    const/4 v2, 0x1

    :goto_1
    invoke-static {v2}, Lcom/google/android/gms/common/internal/e;->a(Z)V

    .line 2530
    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ne v2, v5, :cond_2

    const/4 v2, 0x1

    :goto_2
    invoke-static {v2}, Lcom/google/android/gms/common/internal/e;->a(Z)V

    .line 2531
    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ne v2, v5, :cond_3

    const/4 v2, 0x1

    :goto_3
    invoke-static {v2}, Lcom/google/android/gms/common/internal/e;->a(Z)V

    .line 2532
    invoke-virtual {v9}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ne v2, v5, :cond_4

    const/4 v2, 0x1

    :goto_4
    invoke-static {v2}, Lcom/google/android/gms/common/internal/e;->a(Z)V

    .line 2533
    invoke-virtual {v10}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ne v2, v5, :cond_5

    const/4 v2, 0x1

    :goto_5
    invoke-static {v2}, Lcom/google/android/gms/common/internal/e;->a(Z)V

    .line 2538
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "PlayerAgent"

    invoke-static {v2, v4, v3}, Lcom/google/android/gms/games/a/l;->b(Landroid/content/ContentResolver;Ljava/util/ArrayList;Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v11

    .line 2540
    const/4 v2, 0x0

    move v4, v2

    :goto_6
    if-ge v4, v5, :cond_6

    .line 2541
    move-object/from16 v0, p2

    iget-object v2, v0, Lcom/google/android/gms/games/a/bm;->a:Ljava/util/ArrayList;

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/content/ContentValues;

    .line 2542
    const-string v12, "profile_icon_image_url"

    const-string v13, "profile_icon_image_uri"

    invoke-virtual {v6, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-static {v2, v12, v13, v11, v3}, Lcom/google/android/gms/games/a/l;->b(Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;Ljava/lang/Integer;)V

    .line 2544
    const-string v12, "profile_hi_res_image_url"

    const-string v13, "profile_hi_res_image_uri"

    invoke-virtual {v7, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-static {v2, v12, v13, v11, v3}, Lcom/google/android/gms/games/a/l;->b(Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;Ljava/lang/Integer;)V

    .line 2546
    const-string v12, "most_recent_game_icon_url"

    const-string v13, "most_recent_game_icon_uri"

    invoke-virtual {v8, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-static {v2, v12, v13, v11, v3}, Lcom/google/android/gms/games/a/l;->b(Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;Ljava/lang/Integer;)V

    .line 2548
    const-string v12, "most_recent_game_hi_res_url"

    const-string v13, "most_recent_game_hi_res_uri"

    invoke-virtual {v9, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-static {v2, v12, v13, v11, v3}, Lcom/google/android/gms/games/a/l;->b(Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;Ljava/lang/Integer;)V

    .line 2550
    const-string v12, "most_recent_game_featured_url"

    const-string v13, "most_recent_game_featured_uri"

    invoke-virtual {v10, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-static {v2, v12, v13, v11, v3}, Lcom/google/android/gms/games/a/l;->b(Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;Ljava/lang/Integer;)V

    .line 2555
    const-string v3, "most_recent_game_icon_uri"

    invoke-virtual {v2, v3}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 2556
    const-string v12, "game_icon_image_uri"

    invoke-virtual {v2, v12, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2540
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    goto :goto_6

    .line 2529
    :cond_1
    const/4 v2, 0x0

    goto/16 :goto_1

    .line 2530
    :cond_2
    const/4 v2, 0x0

    goto/16 :goto_2

    .line 2531
    :cond_3
    const/4 v2, 0x0

    goto/16 :goto_3

    .line 2532
    :cond_4
    const/4 v2, 0x0

    goto :goto_4

    .line 2533
    :cond_5
    const/4 v2, 0x0

    goto :goto_5

    .line 2560
    :cond_6
    move-object/from16 v0, p2

    iget-object v5, v0, Lcom/google/android/gms/games/a/bm;->a:Ljava/util/ArrayList;

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object/from16 v0, p2

    iget-object v8, v0, Lcom/google/android/gms/games/a/bm;->b:Ljava/lang/String;

    const/4 v9, 0x0

    move-object/from16 v3, p8

    move-object/from16 v4, p3

    move-wide/from16 v10, p4

    move-wide/from16 v12, p6

    invoke-virtual/range {v3 .. v13}, Lcom/google/android/gms/games/b/p;->a(Ljava/lang/Object;Ljava/util/ArrayList;ILjava/lang/String;Ljava/lang/String;IJJ)V

    .line 2563
    const-string v2, "total_count"

    move-object/from16 v0, p2

    iget v3, v0, Lcom/google/android/gms/games/a/bm;->c:I

    move-object/from16 v0, p8

    move-object/from16 v1, p3

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/gms/games/b/p;->a(Ljava/lang/Object;Ljava/lang/String;I)V

    .line 2564
    const/4 v2, 0x0

    const/4 v3, -0x1

    move-object/from16 v0, p8

    move-object/from16 v1, p3

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/gms/games/b/u;->a(Ljava/lang/Object;Landroid/os/Bundle;I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v2

    return-object v2
.end method

.method private a(Lcom/google/android/gms/games/a/au;Ljava/lang/String;Ljava/lang/String;IZ)Lcom/google/android/gms/common/data/DataHolder;
    .locals 22

    .prologue
    .line 2448
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/games/a/bj;->L:Lcom/google/android/gms/games/b/p;

    move-object/from16 v0, p1

    iget-object v3, v0, Lcom/google/android/gms/games/a/au;->c:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lcom/google/android/gms/games/b/p;->a(Ljava/lang/Object;)V

    .line 2449
    invoke-static {}, Lcom/google/android/gms/common/util/r;->c()Lcom/google/android/gms/common/util/p;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/gms/common/util/p;->a()J

    move-result-wide v4

    .line 2452
    move-object/from16 v0, p1

    iget-boolean v2, v0, Lcom/google/android/gms/games/a/au;->g:Z

    if-eqz v2, :cond_0

    .line 2453
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/games/a/bj;->L:Lcom/google/android/gms/games/b/p;

    move-object/from16 v0, p2

    invoke-virtual {v2, v0}, Lcom/google/android/gms/games/b/p;->c(Ljava/lang/Object;)V

    .line 2457
    :cond_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/games/a/bj;->L:Lcom/google/android/gms/games/b/p;

    move-object/from16 v3, p2

    move/from16 v6, p4

    move/from16 v7, p5

    invoke-virtual/range {v2 .. v7}, Lcom/google/android/gms/games/b/p;->a(Ljava/lang/Object;JIZ)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 2458
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/games/a/bj;->L:Lcom/google/android/gms/games/b/p;

    const/4 v3, 0x0

    const/4 v4, -0x1

    move-object/from16 v0, p2

    invoke-virtual {v2, v0, v3, v4}, Lcom/google/android/gms/games/b/u;->a(Ljava/lang/Object;Landroid/os/Bundle;I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v2

    .line 2497
    :goto_0
    return-object v2

    .line 2462
    :cond_1
    const/4 v11, 0x0

    .line 2468
    if-nez p5, :cond_2

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/games/a/bj;->L:Lcom/google/android/gms/games/b/p;

    move-object/from16 v0, p2

    invoke-virtual {v2, v0, v4, v5}, Lcom/google/android/gms/games/b/p;->a(Ljava/lang/Object;J)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 2469
    :cond_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/games/a/bj;->L:Lcom/google/android/gms/games/b/p;

    move-object/from16 v0, p2

    invoke-virtual {v2, v0, v4, v5}, Lcom/google/android/gms/games/b/p;->b(Ljava/lang/Object;J)Ljava/lang/String;

    move-result-object v11

    .line 2473
    :cond_3
    :try_start_0
    invoke-static {}, Lcom/google/android/gms/common/util/r;->c()Lcom/google/android/gms/common/util/p;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/gms/common/util/p;->a()J

    move-result-wide v12

    const-string v2, "circled"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    move-object/from16 v0, p1

    iget-boolean v2, v0, Lcom/google/android/gms/games/a/au;->h:Z

    invoke-static {v2}, Lcom/google/android/gms/common/internal/e;->a(Z)V

    const/4 v14, 0x1

    move-object/from16 v7, p0

    move-object/from16 v8, p1

    move-object/from16 v9, p3

    move/from16 v10, p4

    invoke-direct/range {v7 .. v14}, Lcom/google/android/gms/games/a/bj;->a(Lcom/google/android/gms/games/a/au;Ljava/lang/String;ILjava/lang/String;JI)Lcom/google/android/gms/games/a/bm;
    :try_end_0
    .catch Lcom/android/volley/ac; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    :goto_1
    move-object v8, v2

    .line 2483
    :goto_2
    if-nez v8, :cond_16

    .line 2484
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/games/a/bj;->L:Lcom/google/android/gms/games/b/p;

    move-object/from16 v0, p2

    invoke-virtual {v2, v0, v4, v5}, Lcom/google/android/gms/games/b/p;->a(Ljava/lang/Object;J)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 2486
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/games/a/bj;->L:Lcom/google/android/gms/games/b/p;

    const/4 v3, 0x3

    move-object/from16 v0, p2

    invoke-virtual {v2, v0, v3}, Lcom/google/android/gms/games/b/p;->a(Ljava/lang/Object;I)V

    .line 2488
    :cond_4
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/games/a/bj;->L:Lcom/google/android/gms/games/b/p;

    const/4 v3, 0x0

    const/4 v4, -0x1

    move-object/from16 v0, p2

    invoke-virtual {v2, v0, v3, v4}, Lcom/google/android/gms/games/b/u;->a(Ljava/lang/Object;Landroid/os/Bundle;I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v2

    goto :goto_0

    .line 2473
    :cond_5
    :try_start_1
    const-string v2, "suggested"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_6

    move-object/from16 v0, p1

    iget-boolean v2, v0, Lcom/google/android/gms/games/a/au;->h:Z

    invoke-static {v2}, Lcom/google/android/gms/common/internal/e;->a(Z)V

    const/4 v14, 0x0

    move-object/from16 v7, p0

    move-object/from16 v8, p1

    move-object/from16 v9, p3

    move/from16 v10, p4

    invoke-direct/range {v7 .. v14}, Lcom/google/android/gms/games/a/bj;->a(Lcom/google/android/gms/games/a/au;Ljava/lang/String;ILjava/lang/String;JI)Lcom/google/android/gms/games/a/bm;

    move-result-object v2

    goto :goto_1

    :cond_6
    const-string v2, "visible:"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_8

    move-object/from16 v0, p1

    iget-boolean v2, v0, Lcom/google/android/gms/games/a/au;->h:Z

    if-nez v2, :cond_7

    const/4 v2, 0x1

    :goto_3
    invoke-static {v2}, Lcom/google/android/gms/common/internal/e;->a(Z)V

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/gms/games/a/bj;->G:Lcom/google/android/gms/plus/service/v1/c;

    move-object/from16 v0, p1

    iget-object v15, v0, Lcom/google/android/gms/games/a/au;->b:Lcom/google/android/gms/common/server/ClientContext;

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/google/android/gms/games/a/au;->c:Ljava/lang/String;

    move-object/from16 v16, v0

    sget-object v18, Lcom/google/android/gms/games/a/bj;->y:Ljava/util/ArrayList;

    invoke-static/range {p4 .. p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v19

    const/16 v20, 0x0

    move-object/from16 v17, p3

    move-object/from16 v21, v11

    invoke-virtual/range {v14 .. v21}, Lcom/google/android/gms/plus/service/v1/c;->a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/plus/service/v1/PeopleFeed;

    move-result-object v2

    move-object/from16 v0, p1

    iget-object v3, v0, Lcom/google/android/gms/games/a/au;->a:Landroid/content/Context;

    const/4 v6, 0x1

    invoke-static {v3, v2, v12, v13, v6}, Lcom/google/android/gms/games/a/bj;->a(Landroid/content/Context;Lcom/google/android/gms/plus/service/v1/PeopleFeed;JI)Lcom/google/android/gms/games/a/bm;

    move-result-object v2

    goto :goto_1

    :cond_7
    const/4 v2, 0x0

    goto :goto_3

    :cond_8
    const-string v2, "players1p:"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_f

    move-object/from16 v0, p1

    iget-boolean v2, v0, Lcom/google/android/gms/games/a/au;->h:Z

    invoke-static {v2}, Lcom/google/android/gms/common/internal/e;->a(Z)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/games/a/bj;->F:Lcom/google/android/gms/games/h/a/dj;

    move-object/from16 v0, p1

    iget-object v7, v0, Lcom/google/android/gms/games/a/au;->b:Lcom/google/android/gms/common/server/ClientContext;

    move-object/from16 v0, p1

    iget-object v3, v0, Lcom/google/android/gms/games/a/au;->e:Ljava/lang/String;

    move-object/from16 v0, p1

    iget-object v6, v0, Lcom/google/android/gms/games/a/au;->a:Landroid/content/Context;

    invoke-static {v6}, Lcom/google/android/gms/games/a/l;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v6

    const-string v8, "players/me/players/%1$s"

    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    invoke-static/range {p3 .. p3}, Lcom/google/android/gms/games/h/a/dj;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    aput-object v14, v9, v10

    invoke-static {v8, v9}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    if-eqz v3, :cond_9

    const-string v8, "applicationId"

    invoke-static {v3}, Lcom/google/android/gms/games/h/a/dj;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v9, v8, v3}, Lcom/google/android/gms/games/h/a/dj;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    :cond_9
    if-eqz v6, :cond_a

    const-string v3, "language"

    invoke-static {v6}, Lcom/google/android/gms/games/h/a/dj;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v9, v3, v6}, Lcom/google/android/gms/games/h/a/dj;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    :cond_a
    if-eqz v11, :cond_b

    const-string v3, "pageToken"

    invoke-static {v11}, Lcom/google/android/gms/games/h/a/dj;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v9, v3, v6}, Lcom/google/android/gms/games/h/a/dj;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    :cond_b
    iget-object v6, v2, Lcom/google/android/gms/games/h/a/dj;->a:Lcom/google/android/gms/common/server/n;

    const/4 v8, 0x0

    const/4 v10, 0x0

    const-class v11, Lcom/google/android/gms/games/h/a/dc;

    invoke-virtual/range {v6 .. v11}, Lcom/google/android/gms/common/server/n;->a(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;Ljava/lang/Object;Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse;

    move-result-object v2

    check-cast v2, Lcom/google/android/gms/games/h/a/dc;

    invoke-virtual {v2}, Lcom/google/android/gms/games/h/a/dc;->getItems()Ljava/util/ArrayList;

    move-result-object v7

    if-nez v7, :cond_c

    const/4 v2, 0x0

    goto/16 :goto_1

    :cond_c
    move-object/from16 v0, p1

    iget-object v3, v0, Lcom/google/android/gms/games/a/au;->a:Landroid/content/Context;

    move-object/from16 v0, p1

    iget-object v3, v0, Lcom/google/android/gms/games/a/au;->b:Lcom/google/android/gms/common/server/ClientContext;

    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v10

    new-instance v11, Ljava/util/ArrayList;

    invoke-direct {v11, v10}, Ljava/util/ArrayList;-><init>(I)V

    const/4 v3, 0x0

    move v6, v3

    :goto_4
    if-ge v6, v10, :cond_e

    invoke-virtual {v7, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/gms/games/h/a/bk;

    invoke-virtual {v3}, Lcom/google/android/gms/games/h/a/bk;->getDisplayPlayer()Lcom/google/android/gms/games/h/a/cs;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/gms/games/h/a/cs;->getLastPlayedWith()Lcom/google/android/gms/games/h/a/cq;

    move-result-object v8

    iget-object v3, v3, Lcom/google/android/gms/common/server/response/a;->a:Landroid/content/ContentValues;

    if-nez v8, :cond_d

    const-wide/16 v8, -0x1

    :goto_5
    invoke-static {v8, v9, v12, v13, v3}, Lcom/google/android/gms/games/a/bj;->a(JJLandroid/content/ContentValues;)V

    invoke-virtual {v11, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v3, v6, 0x1

    move v6, v3

    goto :goto_4

    :cond_d
    invoke-virtual {v8}, Lcom/google/android/gms/games/h/a/cq;->b()Ljava/lang/Long;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    goto :goto_5

    :cond_e
    new-instance v3, Lcom/google/android/gms/games/a/bm;

    invoke-virtual {v2}, Lcom/google/android/gms/games/h/a/dc;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v6

    invoke-direct {v3, v11, v2, v6}, Lcom/google/android/gms/games/a/bm;-><init>(Ljava/util/ArrayList;Ljava/lang/String;I)V

    move-object v2, v3

    goto/16 :goto_1

    :cond_f
    const-string v2, "players:"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_14

    move-object/from16 v0, p1

    iget-boolean v2, v0, Lcom/google/android/gms/games/a/au;->h:Z

    if-nez v2, :cond_12

    const/4 v2, 0x1

    :goto_6
    invoke-static {v2}, Lcom/google/android/gms/common/internal/e;->a(Z)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/games/a/bj;->E:Lcom/google/android/gms/games/h/a/di;

    move-object/from16 v0, p1

    iget-object v7, v0, Lcom/google/android/gms/games/a/au;->b:Lcom/google/android/gms/common/server/ClientContext;

    move-object/from16 v0, p1

    iget-object v3, v0, Lcom/google/android/gms/games/a/au;->a:Landroid/content/Context;

    invoke-static {v3}, Lcom/google/android/gms/games/a/l;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    const-string v6, "players/me/players/%1$s"

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    invoke-static/range {p3 .. p3}, Lcom/google/android/gms/games/h/a/di;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    aput-object v10, v8, v9

    invoke-static {v6, v8}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    if-eqz v3, :cond_10

    const-string v6, "language"

    invoke-static {v3}, Lcom/google/android/gms/games/h/a/di;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v9, v6, v3}, Lcom/google/android/gms/games/h/a/di;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    :cond_10
    if-eqz v11, :cond_11

    const-string v3, "pageToken"

    invoke-static {v11}, Lcom/google/android/gms/games/h/a/di;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v9, v3, v6}, Lcom/google/android/gms/games/h/a/di;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    :cond_11
    iget-object v6, v2, Lcom/google/android/gms/games/h/a/di;->a:Lcom/google/android/gms/common/server/n;

    const/4 v8, 0x0

    const/4 v10, 0x0

    const-class v11, Lcom/google/android/gms/games/h/a/dd;

    invoke-virtual/range {v6 .. v11}, Lcom/google/android/gms/common/server/n;->a(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;Ljava/lang/Object;Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse;

    move-result-object v2

    check-cast v2, Lcom/google/android/gms/games/h/a/dd;

    invoke-virtual {v2}, Lcom/google/android/gms/games/h/a/dd;->getItems()Ljava/util/ArrayList;

    move-result-object v6

    if-nez v6, :cond_13

    const/4 v2, 0x0

    goto/16 :goto_1

    :cond_12
    const/4 v2, 0x0

    goto :goto_6

    :cond_13
    move-object/from16 v0, p1

    iget-object v3, v0, Lcom/google/android/gms/games/a/au;->a:Landroid/content/Context;

    move-object/from16 v0, p1

    iget-object v3, v0, Lcom/google/android/gms/games/a/au;->b:Lcom/google/android/gms/common/server/ClientContext;

    invoke-static {v6, v12, v13}, Lcom/google/android/gms/games/a/bj;->a(Ljava/util/ArrayList;J)Ljava/util/ArrayList;

    move-result-object v7

    new-instance v3, Lcom/google/android/gms/games/a/bm;

    invoke-virtual {v2}, Lcom/google/android/gms/games/h/a/dd;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v6

    invoke-direct {v3, v7, v2, v6}, Lcom/google/android/gms/games/a/bm;-><init>(Ljava/util/ArrayList;Ljava/lang/String;I)V

    move-object v2, v3

    goto/16 :goto_1

    :cond_14
    new-instance v2, Ljava/lang/IllegalArgumentException;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v6, "Unrecognized cache key "

    invoke-direct {v3, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p2

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2
    :try_end_1
    .catch Lcom/android/volley/ac; {:try_start_1 .. :try_end_1} :catch_0

    .line 2475
    :catch_0
    move-exception v2

    .line 2476
    invoke-static {}, Lcom/google/android/gms/games/internal/dq;->a()Z

    move-result v3

    if-eqz v3, :cond_15

    .line 2477
    const-string v3, "PlayerAgent"

    invoke-static {v2, v3}, Lcom/google/android/gms/common/server/b/c;->a(Lcom/android/volley/ac;Ljava/lang/String;)Lcom/google/android/gms/common/server/b/b;

    .line 2479
    :cond_15
    const/4 v8, 0x0

    goto/16 :goto_2

    .line 2493
    :cond_16
    const-string v2, "played_with"

    move-object/from16 v0, p3

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_17

    const-wide/32 v12, 0x1d4c0

    .line 2497
    :goto_7
    move-object/from16 v0, p1

    iget-object v6, v0, Lcom/google/android/gms/games/a/au;->a:Landroid/content/Context;

    move-object/from16 v0, p1

    iget-object v7, v0, Lcom/google/android/gms/games/a/au;->b:Lcom/google/android/gms/common/server/ClientContext;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/gms/games/a/bj;->L:Lcom/google/android/gms/games/b/p;

    move-object/from16 v9, p2

    move-wide v10, v4

    invoke-static/range {v6 .. v14}, Lcom/google/android/gms/games/a/bj;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Lcom/google/android/gms/games/a/bm;Ljava/lang/String;JJLcom/google/android/gms/games/b/p;)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v2

    goto/16 :goto_0

    .line 2493
    :cond_17
    const-wide/32 v12, 0x1b7740

    goto :goto_7
.end method

.method private static a(Landroid/database/Cursor;)Lcom/google/android/gms/games/PlayerLevel;
    .locals 6

    .prologue
    .line 2343
    new-instance v0, Lcom/google/android/gms/games/PlayerLevel;

    const/4 v1, 0x0

    invoke-interface {p0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    const/4 v2, 0x1

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    const/4 v4, 0x2

    invoke-interface {p0, v4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/games/PlayerLevel;-><init>(IJJ)V

    return-object v0
.end method

.method private a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;IJLjava/lang/String;)Lcom/google/android/gms/games/a/bm;
    .locals 11

    .prologue
    .line 1319
    const/4 v0, 0x0

    move v9, v0

    move-object/from16 v6, p7

    move-object v2, p3

    move-object v1, p2

    :goto_0
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/games/a/bj;->H:Lcom/google/android/gms/plus/service/v1whitelisted/i;

    invoke-static {p1}, Lcom/google/android/gms/games/a/l;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/gms/plus/service/v1whitelisted/i;->a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/plus/service/v1whitelisted/models/PeopleFeed;

    move-result-object v5

    const/4 v8, -0x1

    move-object v3, p0

    move-object v4, p1

    move-wide/from16 v6, p5

    invoke-direct/range {v3 .. v8}, Lcom/google/android/gms/games/a/bj;->a(Landroid/content/Context;Lcom/google/android/gms/plus/service/v1whitelisted/models/PeopleFeed;JI)Lcom/google/android/gms/games/a/bm;

    move-result-object v0

    iget-object v3, v0, Lcom/google/android/gms/games/a/bm;->a:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_1

    iget-object v3, v0, Lcom/google/android/gms/games/a/bm;->b:Ljava/lang/String;

    if-eqz v3, :cond_1

    const/4 v3, 0x3

    if-ge v9, v3, :cond_1

    iget-object v6, v0, Lcom/google/android/gms/games/a/bm;->b:Ljava/lang/String;
    :try_end_0
    .catch Lcom/android/volley/ac; {:try_start_0 .. :try_end_0} :catch_0

    add-int/lit8 v0, v9, 0x1

    move v9, v0

    goto :goto_0

    .line 1321
    :catch_0
    move-exception v0

    .line 1322
    invoke-static {}, Lcom/google/android/gms/games/internal/dq;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1323
    const-string v1, "PlayerAgent"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/server/b/c;->a(Lcom/android/volley/ac;Ljava/lang/String;)Lcom/google/android/gms/common/server/b/b;

    .line 1325
    :cond_0
    const/4 v0, 0x0

    .line 1327
    :cond_1
    return-object v0
.end method

.method private a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;J)Lcom/google/android/gms/games/a/bm;
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 1298
    :try_start_0
    iget-object v1, p0, Lcom/google/android/gms/games/a/bj;->H:Lcom/google/android/gms/plus/service/v1whitelisted/i;

    invoke-virtual {v1, p2, p3}, Lcom/google/android/gms/plus/service/v1whitelisted/i;->a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)Lcom/google/android/gms/plus/service/v1whitelisted/models/PersonEntity;

    move-result-object v1

    .line 1301
    if-nez v1, :cond_1

    .line 1311
    :cond_0
    :goto_0
    return-object v0

    .line 1301
    :cond_1
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    const-string v3, "external_player_id"

    invoke-virtual {v1}, Lcom/google/android/gms/plus/service/v1whitelisted/models/PersonEntity;->g()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v3, "profile_name"

    invoke-virtual {v1}, Lcom/google/android/gms/plus/service/v1whitelisted/models/PersonEntity;->d()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v3, "last_updated"

    invoke-static {p4, p5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v3, "is_in_circles"

    const/4 v4, -0x1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    invoke-virtual {v1}, Lcom/google/android/gms/plus/service/v1whitelisted/models/PersonEntity;->h()Lcom/google/android/gms/plus/service/v1whitelisted/models/ep;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/gms/plus/service/v1whitelisted/models/ep;->d()Ljava/lang/String;

    move-result-object v1

    new-instance v3, Lcom/google/android/gms/common/internal/bs;

    invoke-direct {v3, v1}, Lcom/google/android/gms/common/internal/bs;-><init>(Ljava/lang/String;)V

    const/4 v1, 0x0

    iput-boolean v1, v3, Lcom/google/android/gms/common/internal/bs;->d:Z

    sget v1, Lcom/google/android/gms/g;->N:I

    invoke-virtual {v3, p1, v1}, Lcom/google/android/gms/common/internal/bs;->a(Landroid/content/Context;I)Lcom/google/android/gms/common/internal/br;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/common/internal/br;->a()Ljava/lang/String;

    move-result-object v3

    if-nez v3, :cond_2

    move-object v1, v0

    :goto_1
    const-string v4, "profile_icon_image_url"

    invoke-virtual {v2, v4, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v3, "profile_hi_res_image_url"

    invoke-virtual {v2, v3, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v1, Lcom/google/android/gms/games/a/bm;

    invoke-direct {v1, v2}, Lcom/google/android/gms/games/a/bm;-><init>(Landroid/content/ContentValues;)V

    move-object v0, v1

    goto :goto_0

    :cond_2
    new-instance v1, Lcom/google/android/gms/common/internal/bs;

    invoke-direct {v1, v3}, Lcom/google/android/gms/common/internal/bs;-><init>(Ljava/lang/String;)V

    const/4 v4, 0x0

    iput-boolean v4, v1, Lcom/google/android/gms/common/internal/bs;->d:Z

    sget v4, Lcom/google/android/gms/g;->M:I

    invoke-virtual {v1, p1, v4}, Lcom/google/android/gms/common/internal/bs;->a(Landroid/content/Context;I)Lcom/google/android/gms/common/internal/br;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/common/internal/br;->a()Ljava/lang/String;
    :try_end_0
    .catch Lcom/android/volley/ac; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    goto :goto_1

    .line 1302
    :catch_0
    move-exception v1

    .line 1305
    const/16 v2, 0x194

    invoke-static {v1, v2}, Lcom/google/android/gms/common/server/b/c;->a(Lcom/android/volley/ac;I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 1306
    invoke-static {}, Lcom/google/android/gms/games/internal/dq;->a()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1307
    const-string v2, "PlayerAgent"

    invoke-static {v1, v2}, Lcom/google/android/gms/common/server/b/c;->a(Lcom/android/volley/ac;Ljava/lang/String;)Lcom/google/android/gms/common/server/b/b;

    goto :goto_0
.end method

.method private static a(Landroid/content/Context;Lcom/google/android/gms/plus/service/v1/PeopleFeed;JI)Lcom/google/android/gms/games/a/bm;
    .locals 8

    .prologue
    const/4 v1, 0x0

    const/4 v7, 0x0

    .line 2879
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 2880
    if-nez p1, :cond_0

    .line 2910
    :goto_0
    return-object v1

    .line 2885
    :cond_0
    invoke-virtual {p1}, Lcom/google/android/gms/plus/service/v1/PeopleFeed;->e()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/model/c/a;

    .line 2886
    new-instance v4, Landroid/content/ContentValues;

    invoke-direct {v4}, Landroid/content/ContentValues;-><init>()V

    .line 2887
    const-string v5, "external_player_id"

    invoke-interface {v0}, Lcom/google/android/gms/plus/model/c/a;->e()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2888
    const-string v5, "profile_name"

    invoke-interface {v0}, Lcom/google/android/gms/plus/model/c/a;->d()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2889
    const-string v5, "last_updated"

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 2891
    const-string v5, "is_in_circles"

    const/4 v6, 0x1

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2892
    invoke-interface {v0}, Lcom/google/android/gms/plus/model/c/a;->f()Lcom/google/android/gms/plus/model/c/f;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/plus/model/c/f;->d()Ljava/lang/String;

    move-result-object v0

    .line 2893
    new-instance v5, Lcom/google/android/gms/common/internal/bs;

    invoke-direct {v5, v0}, Lcom/google/android/gms/common/internal/bs;-><init>(Ljava/lang/String;)V

    iput-boolean v7, v5, Lcom/google/android/gms/common/internal/bs;->d:Z

    sget v0, Lcom/google/android/gms/g;->N:I

    invoke-virtual {v5, p0, v0}, Lcom/google/android/gms/common/internal/bs;->a(Landroid/content/Context;I)Lcom/google/android/gms/common/internal/br;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/common/internal/br;->a()Ljava/lang/String;

    move-result-object v5

    .line 2898
    if-nez v5, :cond_1

    move-object v0, v1

    .line 2906
    :goto_2
    const-string v6, "profile_icon_image_url"

    invoke-virtual {v4, v6, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2907
    const-string v5, "profile_hi_res_image_url"

    invoke-virtual {v4, v5, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2908
    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 2901
    :cond_1
    new-instance v0, Lcom/google/android/gms/common/internal/bs;

    invoke-direct {v0, v5}, Lcom/google/android/gms/common/internal/bs;-><init>(Ljava/lang/String;)V

    iput-boolean v7, v0, Lcom/google/android/gms/common/internal/bs;->d:Z

    sget v6, Lcom/google/android/gms/g;->M:I

    invoke-virtual {v0, p0, v6}, Lcom/google/android/gms/common/internal/bs;->a(Landroid/content/Context;I)Lcom/google/android/gms/common/internal/br;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/common/internal/br;->a()Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    .line 2910
    :cond_2
    new-instance v1, Lcom/google/android/gms/games/a/bm;

    invoke-virtual {p1}, Lcom/google/android/gms/plus/service/v1/PeopleFeed;->g()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/android/gms/plus/service/v1/PeopleFeed;->l()I

    move-result v3

    invoke-direct {v1, v2, v0, v3}, Lcom/google/android/gms/games/a/bm;-><init>(Ljava/util/ArrayList;Ljava/lang/String;I)V

    goto/16 :goto_0
.end method

.method private a(Landroid/content/Context;Lcom/google/android/gms/plus/service/v1whitelisted/models/PeopleFeed;JI)Lcom/google/android/gms/games/a/bm;
    .locals 11

    .prologue
    const/4 v2, 0x0

    const/4 v9, 0x0

    .line 2795
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 2796
    if-nez p2, :cond_0

    .line 2840
    :goto_0
    return-object v2

    .line 2801
    :cond_0
    invoke-virtual {p2}, Lcom/google/android/gms/plus/service/v1whitelisted/models/PeopleFeed;->c()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_1
    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/service/v1whitelisted/models/em;

    .line 2804
    invoke-interface {v0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/em;->j()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_2

    sget-object v1, Lcom/google/android/gms/games/a/bj;->y:Ljava/util/ArrayList;

    invoke-interface {v0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/em;->j()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2806
    :cond_2
    new-instance v5, Landroid/content/ContentValues;

    invoke-direct {v5}, Landroid/content/ContentValues;-><init>()V

    .line 2809
    invoke-interface {v0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/em;->g()Ljava/lang/String;

    move-result-object v6

    .line 2810
    const-string v1, "external_player_id"

    invoke-virtual {v5, v1, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2811
    const-string v1, "profile_name"

    invoke-interface {v0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/em;->d()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v1, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2812
    const-string v1, "last_updated"

    invoke-static {p3, p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-virtual {v5, v1, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 2813
    const-string v1, "is_in_circles"

    invoke-static/range {p5 .. p5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v5, v1, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2814
    invoke-interface {v0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/em;->h()Lcom/google/android/gms/plus/service/v1whitelisted/models/ep;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/gms/plus/service/v1whitelisted/models/ep;->d()Ljava/lang/String;

    move-result-object v1

    .line 2815
    new-instance v7, Lcom/google/android/gms/common/internal/bs;

    invoke-direct {v7, v1}, Lcom/google/android/gms/common/internal/bs;-><init>(Ljava/lang/String;)V

    iput-boolean v9, v7, Lcom/google/android/gms/common/internal/bs;->d:Z

    sget v1, Lcom/google/android/gms/g;->N:I

    invoke-virtual {v7, p1, v1}, Lcom/google/android/gms/common/internal/bs;->a(Landroid/content/Context;I)Lcom/google/android/gms/common/internal/br;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/common/internal/br;->a()Ljava/lang/String;

    move-result-object v7

    .line 2820
    if-nez v7, :cond_3

    move-object v1, v2

    .line 2828
    :goto_2
    const-string v8, "profile_icon_image_url"

    invoke-virtual {v5, v8, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2829
    const-string v7, "profile_hi_res_image_url"

    invoke-virtual {v5, v7, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2830
    invoke-virtual {v3, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2835
    invoke-interface {v0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/em;->n()Ljava/lang/String;

    move-result-object v0

    .line 2836
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/google/android/gms/games/a/bj;->O:Ljava/util/HashMap;

    invoke-virtual {v1, v6}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 2837
    iget-object v1, p0, Lcom/google/android/gms/games/a/bj;->O:Ljava/util/HashMap;

    invoke-virtual {v1, v6, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_1

    .line 2823
    :cond_3
    new-instance v1, Lcom/google/android/gms/common/internal/bs;

    invoke-direct {v1, v7}, Lcom/google/android/gms/common/internal/bs;-><init>(Ljava/lang/String;)V

    iput-boolean v9, v1, Lcom/google/android/gms/common/internal/bs;->d:Z

    sget v8, Lcom/google/android/gms/g;->M:I

    invoke-virtual {v1, p1, v8}, Lcom/google/android/gms/common/internal/bs;->a(Landroid/content/Context;I)Lcom/google/android/gms/common/internal/br;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/common/internal/br;->a()Ljava/lang/String;

    move-result-object v1

    goto :goto_2

    .line 2840
    :cond_4
    new-instance v2, Lcom/google/android/gms/games/a/bm;

    invoke-virtual {p2}, Lcom/google/android/gms/plus/service/v1whitelisted/models/PeopleFeed;->d()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v1

    invoke-direct {v2, v3, v0, v1}, Lcom/google/android/gms/games/a/bm;-><init>(Ljava/util/ArrayList;Ljava/lang/String;I)V

    goto/16 :goto_0
.end method

.method private a(Lcom/google/android/gms/games/a/au;Ljava/lang/String;ILjava/lang/String;JI)Lcom/google/android/gms/games/a/bm;
    .locals 9

    .prologue
    .line 2722
    iget-object v0, p0, Lcom/google/android/gms/games/a/bj;->H:Lcom/google/android/gms/plus/service/v1whitelisted/i;

    iget-object v1, p1, Lcom/google/android/gms/games/a/au;->b:Lcom/google/android/gms/common/server/ClientContext;

    iget-object v2, p1, Lcom/google/android/gms/games/a/au;->c:Ljava/lang/String;

    sget-object v4, Lcom/google/android/gms/games/a/bj;->y:Ljava/util/ArrayList;

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    const/4 v6, 0x0

    move-object v3, p2

    move-object v7, p4

    invoke-virtual/range {v0 .. v7}, Lcom/google/android/gms/plus/service/v1whitelisted/i;->a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/plus/service/v1whitelisted/models/PeopleFeed;

    move-result-object v3

    .line 2725
    iget-object v2, p1, Lcom/google/android/gms/games/a/au;->a:Landroid/content/Context;

    move-object v1, p0

    move-wide v4, p5

    move/from16 v6, p7

    invoke-direct/range {v1 .. v6}, Lcom/google/android/gms/games/a/bj;->a(Landroid/content/Context;Lcom/google/android/gms/plus/service/v1whitelisted/models/PeopleFeed;JI)Lcom/google/android/gms/games/a/bm;

    move-result-object v0

    return-object v0
.end method

.method private a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)Lcom/google/android/gms/games/a/bn;
    .locals 8

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x0

    .line 1992
    move-object v1, p2

    :goto_0
    :try_start_0
    invoke-static {p1}, Lcom/google/android/gms/games/a/l;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 1993
    iget-object v2, p0, Lcom/google/android/gms/games/a/bj;->J:Lcom/google/android/gms/games/h/a/bf;

    const-string v3, "experiences/sync"

    if-eqz v0, :cond_0

    const-string v4, "language"

    invoke-static {v0}, Lcom/google/android/gms/games/h/a/bf;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v4, v0}, Lcom/google/android/gms/games/h/a/bf;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    :cond_0
    if-eqz p3, :cond_1

    const-string v0, "syncToken"

    invoke-static {p3}, Lcom/google/android/gms/games/h/a/bf;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v0, v4}, Lcom/google/android/gms/games/h/a/bf;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    :cond_1
    iget-object v0, v2, Lcom/google/android/gms/games/h/a/bf;->a:Lcom/google/android/gms/common/server/n;

    const/4 v2, 0x0

    const/4 v4, 0x0

    const-class v5, Lcom/google/android/gms/games/h/a/be;

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/common/server/n;->a(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;Ljava/lang/Object;Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/h/a/be;
    :try_end_0
    .catch Lcom/android/volley/ac; {:try_start_0 .. :try_end_0} :catch_0

    .line 2010
    invoke-virtual {v0}, Lcom/google/android/gms/games/h/a/be;->getItems()Ljava/util/ArrayList;

    move-result-object v2

    .line 2011
    sget-object v3, Lcom/google/android/gms/games/a/bj;->n:[Ljava/lang/String;

    invoke-static {p1, v1, v3}, Lcom/google/android/gms/games/a/l;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 2012
    invoke-virtual {v0}, Lcom/google/android/gms/games/h/a/be;->b()Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2013
    invoke-static {p3, v3}, Lcom/google/android/gms/common/internal/bu;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_5

    const/4 v0, 0x1

    :goto_1
    const-string v4, "Server claims to have more data, yet sync tokens match!"

    invoke-static {v0, v4}, Lcom/google/android/gms/common/internal/e;->a(ZLjava/lang/Object;)V

    .line 2015
    invoke-direct {p0, p1, v1, v3}, Lcom/google/android/gms/games/a/bj;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)Lcom/google/android/gms/games/a/bn;

    move-result-object v0

    .line 2017
    iget v6, v0, Lcom/google/android/gms/games/a/bn;->c:I

    .line 2018
    if-nez v6, :cond_2

    .line 2019
    iget-object v1, v0, Lcom/google/android/gms/games/a/bn;->a:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 2020
    iget-object v0, v0, Lcom/google/android/gms/games/a/bn;->b:Ljava/lang/String;

    move-object v3, v0

    .line 2024
    :cond_2
    if-nez v2, :cond_6

    .line 2026
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 2028
    :goto_2
    new-instance v1, Lcom/google/android/gms/games/a/bn;

    invoke-direct {v1, v0, v3, v6}, Lcom/google/android/gms/games/a/bn;-><init>(Ljava/util/ArrayList;Ljava/lang/String;I)V

    move-object v0, v1

    :goto_3
    return-object v0

    .line 1995
    :catch_0
    move-exception v0

    .line 1996
    const/16 v2, 0x19a

    invoke-static {v0, v2}, Lcom/google/android/gms/common/server/b/c;->a(Lcom/android/volley/ac;I)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 1999
    new-instance v0, Ljava/util/ArrayList;

    const/4 v2, 0x2

    invoke-direct {v0, v2}, Ljava/util/ArrayList;-><init>(I)V

    invoke-static {v1}, Lcom/google/android/gms/games/provider/m;->a(Lcom/google/android/gms/common/server/ClientContext;)Landroid/net/Uri;

    move-result-object v2

    invoke-static {v2}, Landroid/content/ContentProviderOperation;->newUpdate(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    const-string v3, "request_sync_token"

    invoke-virtual {v2, v3, v7}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-static {v1}, Lcom/google/android/gms/games/provider/x;->a(Lcom/google/android/gms/common/server/ClientContext;)Landroid/net/Uri;

    move-result-object v2

    invoke-static {v2}, Landroid/content/ContentProviderOperation;->newDelete(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "PlayerAgent"

    invoke-static {v2, v0, v3}, Lcom/google/android/gms/games/a/l;->a(Landroid/content/ContentResolver;Ljava/util/ArrayList;Ljava/lang/String;)Z

    .line 2000
    const-string v0, "PlayerAgent"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Token "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " is invalid. Retrying with no token."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/google/android/gms/games/internal/dq;->a(Ljava/lang/String;Ljava/lang/String;)V

    move-object p3, v7

    .line 2001
    goto/16 :goto_0

    .line 2004
    :cond_3
    invoke-static {}, Lcom/google/android/gms/games/internal/dq;->a()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 2005
    const-string v1, "PlayerAgent"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/server/b/c;->a(Lcom/android/volley/ac;Ljava/lang/String;)Lcom/google/android/gms/common/server/b/b;

    .line 2007
    :cond_4
    new-instance v0, Lcom/google/android/gms/games/a/bn;

    invoke-direct {v0}, Lcom/google/android/gms/games/a/bn;-><init>()V

    goto :goto_3

    :cond_5
    move v0, v6

    .line 2013
    goto/16 :goto_1

    :cond_6
    move-object v0, v2

    goto :goto_2
.end method

.method private static a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/util/ArrayList;Z)Ljava/util/ArrayList;
    .locals 12

    .prologue
    const/4 v2, 0x0

    .line 1794
    invoke-static {p1}, Lcom/google/android/gms/games/provider/ad;->a(Lcom/google/android/gms/common/server/ClientContext;)Landroid/net/Uri;

    move-result-object v4

    .line 1795
    invoke-virtual {p2}, Ljava/util/ArrayList;->size()I

    move-result v5

    .line 1796
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6, v5}, Ljava/util/ArrayList;-><init>(I)V

    .line 1797
    invoke-static {p0, p1, p2}, Lcom/google/android/gms/games/a/bj;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/util/ArrayList;)Ljava/util/HashMap;

    move-result-object v7

    .line 1799
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8, v5}, Ljava/util/ArrayList;-><init>(I)V

    .line 1800
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9, v5}, Ljava/util/ArrayList;-><init>(I)V

    move v3, v2

    .line 1803
    :goto_0
    if-ge v3, v5, :cond_1

    .line 1804
    invoke-virtual {p2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/h/a/bc;

    .line 1805
    invoke-virtual {v0}, Lcom/google/android/gms/games/h/a/bc;->b()Ljava/lang/String;

    move-result-object v10

    .line 1806
    invoke-virtual {v7, v10}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Long;

    .line 1807
    if-nez v1, :cond_0

    .line 1808
    const-string v0, "PlayerAgent"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v11, "No game found matching external game ID "

    invoke-direct {v1, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1803
    :goto_1
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_0

    .line 1811
    :cond_0
    new-instance v10, Landroid/content/ContentValues;

    iget-object v11, v0, Lcom/google/android/gms/common/server/response/a;->a:Landroid/content/ContentValues;

    invoke-direct {v10, v11}, Landroid/content/ContentValues;-><init>(Landroid/content/ContentValues;)V

    .line 1812
    const-string v11, "game_id"

    invoke-virtual {v10, v11, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1813
    invoke-virtual {v0}, Lcom/google/android/gms/games/h/a/bc;->c()Ljava/lang/String;

    move-result-object v0

    .line 1814
    invoke-static {v4, v0, v8}, Lcom/google/android/gms/games/a/l;->a(Landroid/net/Uri;Ljava/lang/String;Ljava/util/ArrayList;)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v9, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1815
    invoke-virtual {v6, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 1819
    :cond_1
    if-eqz p3, :cond_4

    .line 1820
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "PlayerAgent"

    invoke-static {v0, v8, v1}, Lcom/google/android/gms/games/a/l;->b(Landroid/content/ContentResolver;Ljava/util/ArrayList;Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v3

    .line 1822
    :goto_2
    if-ge v2, v5, :cond_4

    .line 1823
    invoke-virtual {v6, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/ContentValues;

    .line 1824
    const-string v4, "icon_url"

    const-string v7, "icon_uri"

    invoke-virtual {v9, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-static {v0, v4, v7, v3, v1}, Lcom/google/android/gms/games/a/l;->b(Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;Ljava/lang/Integer;)V

    .line 1827
    const-string v1, "game_id"

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->getAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v1

    .line 1828
    if-eqz v1, :cond_3

    .line 1829
    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v10

    invoke-static {p1, v10, v11}, Lcom/google/android/gms/games/provider/ab;->a(Lcom/google/android/gms/common/server/ClientContext;J)Landroid/net/Uri;

    move-result-object v1

    .line 1832
    new-instance v4, Lcom/google/android/gms/games/a/o;

    invoke-direct {v4, p0}, Lcom/google/android/gms/games/a/o;-><init>(Landroid/content/Context;)V

    invoke-virtual {v4, v1}, Lcom/google/android/gms/games/a/o;->a(Landroid/net/Uri;)Lcom/google/android/gms/games/a/o;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/games/a/o;->b()Landroid/database/AbstractWindowedCursor;

    move-result-object v1

    .line 1836
    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 1837
    sget-object v4, Lcom/google/android/gms/games/k/a;->a:Lcom/google/android/gms/games/provider/a;

    invoke-virtual {v4, v1}, Lcom/google/android/gms/games/provider/a;->a(Landroid/database/Cursor;)Landroid/content/ContentValues;

    move-result-object v4

    invoke-virtual {v0, v4}, Landroid/content/ContentValues;->putAll(Landroid/content/ContentValues;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1840
    :cond_2
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 1822
    :cond_3
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_2

    .line 1840
    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0

    .line 1846
    :cond_4
    return-object v6
.end method

.method private static a(Ljava/util/ArrayList;J)Ljava/util/ArrayList;
    .locals 7

    .prologue
    .line 2668
    invoke-virtual {p0}, Ljava/util/ArrayList;->size()I

    move-result v3

    .line 2669
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4, v3}, Ljava/util/ArrayList;-><init>(I)V

    .line 2670
    const/4 v0, 0x0

    move v2, v0

    :goto_0
    if-ge v2, v3, :cond_1

    .line 2671
    invoke-virtual {p0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/h/a/cs;

    .line 2672
    iget-object v5, v0, Lcom/google/android/gms/common/server/response/a;->a:Landroid/content/ContentValues;

    .line 2673
    invoke-virtual {v0}, Lcom/google/android/gms/games/h/a/cs;->getLastPlayedWith()Lcom/google/android/gms/games/h/a/cq;

    move-result-object v0

    .line 2674
    if-nez v0, :cond_0

    const-wide/16 v0, -0x1

    .line 2676
    :goto_1
    invoke-static {v0, v1, p1, p2, v5}, Lcom/google/android/gms/games/a/bj;->a(JJLandroid/content/ContentValues;)V

    .line 2677
    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2670
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 2674
    :cond_0
    invoke-virtual {v0}, Lcom/google/android/gms/games/h/a/cq;->b()Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    goto :goto_1

    .line 2679
    :cond_1
    return-object v4
.end method

.method private static a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/util/ArrayList;)Ljava/util/HashMap;
    .locals 4

    .prologue
    .line 2131
    invoke-virtual {p2}, Ljava/util/ArrayList;->size()I

    move-result v2

    .line 2132
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3, v2}, Ljava/util/ArrayList;-><init>(I)V

    .line 2133
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_1

    .line 2134
    invoke-virtual {p2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/h/a/bc;

    .line 2135
    invoke-virtual {v0}, Lcom/google/android/gms/games/h/a/bc;->b()Ljava/lang/String;

    move-result-object v0

    .line 2136
    if-eqz v0, :cond_0

    .line 2137
    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2133
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2140
    :cond_1
    invoke-static {p0, p1, v3}, Lcom/google/android/gms/games/a/l;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/util/Collection;)Ljava/util/HashMap;

    move-result-object v0

    return-object v0
.end method

.method private static a(Lcom/google/android/gms/games/b/p;)Ljava/util/Set;
    .locals 5

    .prologue
    .line 1384
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    .line 1385
    invoke-virtual {p0}, Lcom/google/android/gms/games/b/p;->c()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1386
    const/4 v3, 0x0

    const/4 v4, -0x1

    invoke-virtual {p0, v0, v3, v4}, Lcom/google/android/gms/games/b/u;->a(Ljava/lang/Object;Landroid/os/Bundle;I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v3

    .line 1388
    :try_start_0
    new-instance v0, Lcom/google/android/gms/games/t;

    invoke-direct {v0, v3}, Lcom/google/android/gms/games/t;-><init>(Lcom/google/android/gms/common/data/DataHolder;)V

    .line 1389
    invoke-virtual {v0}, Lcom/google/android/gms/games/t;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/Player;

    .line 1390
    invoke-interface {v0}, Lcom/google/android/gms/games/Player;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    .line 1393
    :catchall_0
    move-exception v0

    invoke-virtual {v3}, Lcom/google/android/gms/common/data/DataHolder;->j()V

    throw v0

    :cond_0
    invoke-virtual {v3}, Lcom/google/android/gms/common/data/DataHolder;->j()V

    goto :goto_0

    .line 1395
    :cond_1
    return-object v1
.end method

.method private static a(JJLandroid/content/ContentValues;)V
    .locals 2

    .prologue
    .line 2713
    const-string v0, "played_with_timestamp"

    invoke-static {p0, p1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {p4, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 2714
    const-string v0, "last_updated"

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {p4, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 2715
    return-void
.end method

.method public static a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 682
    invoke-virtual {p1}, Lcom/google/android/gms/common/server/ClientContext;->c()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/google/android/gms/people/pub/b;->b(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 684
    if-eqz v0, :cond_0

    .line 687
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 688
    sget v2, Lcom/google/android/gms/g;->H:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 692
    new-instance v2, Lcom/google/android/gms/common/internal/bs;

    invoke-direct {v2, v0}, Lcom/google/android/gms/common/internal/bs;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Lcom/google/android/gms/common/internal/bs;->a(I)Lcom/google/android/gms/common/internal/br;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/common/internal/br;->a()Ljava/lang/String;

    move-result-object v0

    .line 695
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 696
    const-string v2, "cover_photo_image_url"

    invoke-virtual {v1, v2, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 697
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v2, "account_metadata"

    invoke-static {p1, v2}, Lcom/google/android/gms/games/provider/ac;->a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    const-string v3, "cover_image"

    invoke-virtual {v2, v3}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v0, v2, v1}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    move-result-object v0

    .line 699
    if-eqz v0, :cond_0

    .line 700
    invoke-virtual {p1}, Lcom/google/android/gms/common/server/ClientContext;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v1, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 703
    :cond_0
    return-void
.end method

.method private a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/util/ArrayList;Ljava/util/ArrayList;)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 2085
    invoke-static {p2}, Lcom/google/android/gms/games/provider/x;->a(Lcom/google/android/gms/common/server/ClientContext;)Landroid/net/Uri;

    move-result-object v2

    .line 2086
    invoke-static {p1, p2, p3, v0}, Lcom/google/android/gms/games/a/bj;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/util/ArrayList;Z)Ljava/util/ArrayList;

    move-result-object v3

    .line 2088
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v4

    move v1, v0

    :goto_0
    if-ge v1, v4, :cond_0

    .line 2089
    invoke-static {v2}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v5

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/ContentValues;

    invoke-virtual {v5, v0}, Landroid/content/ContentProviderOperation$Builder;->withValues(Landroid/content/ContentValues;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v0

    invoke-virtual {p4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2088
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2093
    :cond_0
    return-void
.end method

.method private a(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/h/a/dk;)V
    .locals 10

    .prologue
    const/4 v4, 0x0

    const/4 v5, 0x0

    .line 1645
    iget-object v2, p1, Lcom/google/android/gms/games/a/au;->b:Lcom/google/android/gms/common/server/ClientContext;

    .line 1646
    iget-object v0, p1, Lcom/google/android/gms/games/a/au;->c:Ljava/lang/String;

    .line 1647
    if-eqz v0, :cond_0

    .line 1648
    iget-object v0, p1, Lcom/google/android/gms/games/a/au;->c:Ljava/lang/String;

    invoke-static {v2, v0}, Lcom/google/android/gms/games/provider/aq;->a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 1650
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 1651
    const-string v3, "is_profile_visible"

    invoke-virtual {p2}, Lcom/google/android/gms/games/h/a/dk;->b()Ljava/lang/Boolean;

    move-result-object v6

    invoke-virtual {v1, v3, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 1652
    iget-object v3, p1, Lcom/google/android/gms/games/a/au;->a:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    invoke-virtual {v3, v0, v1, v5, v5}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 1656
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/games/a/bj;->Q:Lcom/google/android/gms/games/b/r;

    iget-object v0, v0, Lcom/google/android/gms/games/b/u;->a:Landroid/support/v4/g/h;

    invoke-virtual {v0}, Landroid/support/v4/g/h;->b()V

    .line 1657
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 1658
    iget-object v0, p2, Lcom/google/android/gms/common/server/response/a;->a:Landroid/content/ContentValues;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1659
    iget-object v1, p0, Lcom/google/android/gms/games/a/bj;->Q:Lcom/google/android/gms/games/b/r;

    invoke-static {}, Lcom/google/android/gms/common/util/r;->c()Lcom/google/android/gms/common/util/p;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/common/util/p;->a()J

    move-result-wide v8

    move-object v6, v5

    move v7, v4

    invoke-virtual/range {v1 .. v9}, Lcom/google/android/gms/games/b/r;->a(Ljava/lang/Object;Ljava/util/ArrayList;ILjava/lang/String;Ljava/lang/String;IJ)V

    .line 1662
    return-void
.end method

.method private a(Lcom/google/android/gms/games/a/au;Ljava/util/ArrayList;Ljava/util/HashMap;Ljava/util/ArrayList;)V
    .locals 15

    .prologue
    .line 1401
    move-object/from16 v0, p1

    iget-object v5, v0, Lcom/google/android/gms/games/a/au;->b:Lcom/google/android/gms/common/server/ClientContext;

    .line 1402
    iget-object v1, p0, Lcom/google/android/gms/games/a/bj;->M:Lcom/google/android/gms/games/b/p;

    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/google/android/gms/games/a/au;->c:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/google/android/gms/games/b/p;->b(Ljava/lang/Object;)Z

    move-result v6

    .line 1404
    invoke-virtual/range {p2 .. p2}, Ljava/util/ArrayList;->size()I

    move-result v1

    new-array v1, v1, [Ljava/lang/String;

    move-object/from16 v0, p2

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Ljava/lang/String;

    .line 1405
    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/google/android/gms/games/a/au;->a:Landroid/content/Context;

    .line 1406
    invoke-static {v2, v5, v1}, Lcom/google/android/gms/people/pub/b;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;[Ljava/lang/String;)Ljava/util/HashMap;

    move-result-object v7

    .line 1408
    const/4 v1, 0x0

    invoke-virtual/range {p2 .. p2}, Ljava/util/ArrayList;->size()I

    move-result v8

    move v4, v1

    :goto_0
    if-ge v4, v8, :cond_3

    .line 1410
    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 1411
    invoke-virtual {v7, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Boolean;

    .line 1412
    if-eqz v2, :cond_2

    .line 1413
    if-eqz v6, :cond_1

    .line 1418
    iget-object v9, p0, Lcom/google/android/gms/games/a/bj;->M:Lcom/google/android/gms/games/b/p;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v10

    invoke-virtual {v9}, Lcom/google/android/gms/games/b/p;->c()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v11

    :cond_0
    :goto_1
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    const-string v12, "external_player_id"

    invoke-virtual {v9, v3, v12, v1}, Lcom/google/android/gms/games/b/p;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_0

    const-string v12, "is_in_circles"

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v13

    iget-object v14, v9, Lcom/google/android/gms/games/b/u;->a:Landroid/support/v4/g/h;

    invoke-virtual {v14, v3}, Landroid/support/v4/g/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/gms/games/b/v;

    if-eqz v3, :cond_0

    iget-object v3, v3, Lcom/google/android/gms/games/b/v;->a:Lcom/google/android/gms/common/data/ai;

    iget-object v14, v3, Lcom/google/android/gms/common/data/ai;->c:Lcom/google/android/gms/common/data/m;

    iget-object v3, v14, Lcom/google/android/gms/common/data/m;->c:Ljava/lang/String;

    if-eqz v3, :cond_0

    iget-object v3, v14, Lcom/google/android/gms/common/data/m;->d:Ljava/util/HashMap;

    invoke-virtual {v3, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    if-eqz v3, :cond_0

    iget-object v14, v14, Lcom/google/android/gms/common/data/m;->b:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-virtual {v14, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/HashMap;

    invoke-virtual {v3, v12, v13}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 1422
    :cond_1
    move-object/from16 v0, p3

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_2

    .line 1423
    invoke-static {v5, v1}, Lcom/google/android/gms/games/provider/aq;->a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 1428
    invoke-static {v1}, Landroid/content/ContentProviderOperation;->newUpdate(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v1

    const-string v3, "is_in_circles"

    invoke-virtual {v1, v3, v2}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v1

    invoke-virtual/range {p4 .. p4}, Ljava/util/ArrayList;->size()I

    move-result v2

    invoke-static {v2}, Lcom/google/android/gms/games/a/l;->a(I)Z

    move-result v2

    invoke-virtual {v1, v2}, Landroid/content/ContentProviderOperation$Builder;->withYieldAllowed(Z)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v1

    move-object/from16 v0, p4

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1408
    :cond_2
    add-int/lit8 v1, v4, 0x1

    move v4, v1

    goto/16 :goto_0

    .line 1433
    :cond_3
    return-void
.end method

.method private b(Lcom/google/android/gms/games/a/au;Z)Lcom/google/android/gms/common/data/DataHolder;
    .locals 5

    .prologue
    .line 590
    if-eqz p2, :cond_0

    iget-object v0, p1, Lcom/google/android/gms/games/a/au;->c:Ljava/lang/String;

    .line 592
    :goto_0
    iget-boolean v1, p1, Lcom/google/android/gms/games/a/au;->g:Z

    if-nez v1, :cond_2

    if-eqz v0, :cond_2

    .line 594
    iget-object v1, p1, Lcom/google/android/gms/games/a/au;->b:Lcom/google/android/gms/common/server/ClientContext;

    invoke-static {v1, v0}, Lcom/google/android/gms/games/provider/aq;->a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 596
    new-instance v2, Lcom/google/android/gms/games/a/o;

    invoke-direct {v2, p1}, Lcom/google/android/gms/games/a/o;-><init>(Lcom/google/android/gms/games/a/au;)V

    invoke-virtual {v2, v1}, Lcom/google/android/gms/games/a/o;->a(Landroid/net/Uri;)Lcom/google/android/gms/games/a/o;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/games/a/o;->a()Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v1

    .line 599
    invoke-virtual {v1}, Lcom/google/android/gms/common/data/DataHolder;->h()I

    move-result v2

    if-lez v2, :cond_1

    move-object v0, v1

    .line 635
    :goto_1
    return-object v0

    .line 590
    :cond_0
    iget-object v0, p1, Lcom/google/android/gms/games/a/au;->f:Ljava/lang/String;

    goto :goto_0

    .line 603
    :cond_1
    invoke-virtual {v1}, Lcom/google/android/gms/common/data/DataHolder;->j()V

    .line 608
    :cond_2
    if-nez p2, :cond_3

    if-nez v0, :cond_4

    :cond_3
    :try_start_0
    const-string v1, "me"

    .line 610
    :goto_2
    invoke-direct {p0, p1, v1}, Lcom/google/android/gms/games/a/bj;->d(Lcom/google/android/gms/games/a/au;Ljava/lang/String;)Lcom/google/android/gms/games/h/a/cs;
    :try_end_0
    .catch Lcom/android/volley/ac; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 620
    iget-object v1, v0, Lcom/google/android/gms/common/server/response/a;->a:Landroid/content/ContentValues;

    .line 621
    const-string v0, "last_updated"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 622
    iget-object v0, p1, Lcom/google/android/gms/games/a/au;->b:Lcom/google/android/gms/common/server/ClientContext;

    invoke-static {v0}, Lcom/google/android/gms/games/provider/aq;->a(Lcom/google/android/gms/common/server/ClientContext;)Landroid/net/Uri;

    move-result-object v0

    .line 623
    iget-object v2, p1, Lcom/google/android/gms/games/a/au;->a:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    invoke-virtual {v2, v0, v1}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    move-result-object v0

    .line 624
    new-instance v2, Lcom/google/android/gms/games/a/o;

    invoke-direct {v2, p1}, Lcom/google/android/gms/games/a/o;-><init>(Lcom/google/android/gms/games/a/au;)V

    invoke-virtual {v2, v0}, Lcom/google/android/gms/games/a/o;->a(Landroid/net/Uri;)Lcom/google/android/gms/games/a/o;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/games/a/o;->a()Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    .line 629
    new-instance v2, Ljava/util/ArrayList;

    const/4 v3, 0x2

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(I)V

    .line 630
    const-string v3, "profile_icon_image_url"

    invoke-virtual {v1, v3}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 631
    const-string v3, "profile_hi_res_image_url"

    invoke-virtual {v1, v3}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 632
    iget-object v1, p1, Lcom/google/android/gms/games/a/au;->a:Landroid/content/Context;

    iget-object v3, p1, Lcom/google/android/gms/games/a/au;->b:Lcom/google/android/gms/common/server/ClientContext;

    invoke-static {v3}, Lcom/google/android/gms/games/provider/ad;->a(Lcom/google/android/gms/common/server/ClientContext;)Landroid/net/Uri;

    move-result-object v3

    invoke-static {v1, v3, v2}, Lcom/google/android/gms/common/images/ImageMultiThreadedIntentService;->a(Landroid/content/Context;Landroid/net/Uri;Ljava/util/ArrayList;)V

    goto :goto_1

    :cond_4
    move-object v1, v0

    .line 608
    goto :goto_2

    .line 611
    :catch_0
    move-exception v1

    .line 612
    const-string v2, "PlayerAgent"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Unable to load player "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/google/android/gms/games/internal/dq;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 613
    invoke-static {}, Lcom/google/android/gms/games/internal/dq;->a()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 614
    const-string v0, "PlayerAgent"

    invoke-static {v1, v0}, Lcom/google/android/gms/common/server/b/c;->a(Lcom/android/volley/ac;Ljava/lang/String;)Lcom/google/android/gms/common/server/b/b;

    .line 616
    :cond_5
    const/4 v0, 0x4

    invoke-static {v0}, Lcom/google/android/gms/common/data/DataHolder;->b(I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    goto/16 :goto_1
.end method

.method private c(Lcom/google/android/gms/games/a/au;Ljava/lang/String;)Lcom/google/android/gms/common/data/DataHolder;
    .locals 18

    .prologue
    .line 922
    invoke-static {}, Lcom/google/android/gms/common/util/r;->c()Lcom/google/android/gms/common/util/p;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/gms/common/util/p;->a()J

    move-result-wide v4

    .line 923
    move-object/from16 v0, p1

    iget-object v8, v0, Lcom/google/android/gms/games/a/au;->b:Lcom/google/android/gms/common/server/ClientContext;

    .line 924
    move-object/from16 v0, p1

    iget-object v9, v0, Lcom/google/android/gms/games/a/au;->a:Landroid/content/Context;

    .line 926
    move-object/from16 v0, p1

    iget-boolean v2, v0, Lcom/google/android/gms/games/a/au;->h:Z

    if-eqz v2, :cond_1

    .line 927
    invoke-static {v9, v8}, Lcom/google/android/gms/people/pub/b;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v2

    move-object/from16 v16, v2

    .line 934
    :goto_0
    if-eqz v16, :cond_0

    invoke-virtual/range {v16 .. v16}, Lcom/google/android/gms/common/data/DataHolder;->h()I

    move-result v2

    if-gtz v2, :cond_2

    .line 936
    :cond_0
    const/4 v2, 0x0

    .line 1023
    :goto_1
    return-object v2

    .line 930
    :cond_1
    invoke-virtual {v8}, Lcom/google/android/gms/common/server/ClientContext;->c()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    const/4 v6, 0x0

    invoke-static {v9, v8, v2, v3, v6}, Lcom/google/android/gms/people/pub/b;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;ZI)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v2

    move-object/from16 v16, v2

    goto :goto_0

    .line 939
    :cond_2
    :try_start_0
    new-instance v10, Lcom/google/android/gms/plus/model/c/k;

    move-object/from16 v0, v16

    invoke-direct {v10, v0}, Lcom/google/android/gms/plus/model/c/k;-><init>(Lcom/google/android/gms/common/data/DataHolder;)V

    .line 942
    invoke-virtual {v10}, Lcom/google/android/gms/plus/model/c/k;->c()I

    move-result v6

    .line 945
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/games/a/bj;->L:Lcom/google/android/gms/games/b/p;

    move-object/from16 v0, p1

    iget-object v3, v0, Lcom/google/android/gms/games/a/au;->c:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lcom/google/android/gms/games/b/p;->a(Ljava/lang/Object;)V

    .line 946
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/games/a/bj;->L:Lcom/google/android/gms/games/b/p;

    const/4 v7, 0x0

    move-object/from16 v3, p2

    invoke-virtual/range {v2 .. v7}, Lcom/google/android/gms/games/b/p;->a(Ljava/lang/Object;JIZ)Z

    move-result v2

    if-nez v2, :cond_b

    .line 948
    new-instance v11, Ljava/util/ArrayList;

    invoke-direct {v11, v6}, Ljava/util/ArrayList;-><init>(I)V

    .line 950
    new-instance v12, Ljava/util/ArrayList;

    invoke-direct {v12, v6}, Ljava/util/ArrayList;-><init>(I)V

    .line 951
    const/4 v7, 0x0

    .line 952
    new-instance v13, Ljava/util/ArrayList;

    invoke-direct {v13, v6}, Ljava/util/ArrayList;-><init>(I)V

    .line 953
    const/4 v3, 0x0

    .line 954
    invoke-static {v8}, Lcom/google/android/gms/games/provider/ad;->a(Lcom/google/android/gms/common/server/ClientContext;)Landroid/net/Uri;

    move-result-object v14

    .line 955
    const/4 v2, 0x0

    move v8, v2

    :goto_2
    if-ge v8, v6, :cond_4

    .line 956
    invoke-virtual {v10, v8}, Lcom/google/android/gms/plus/model/c/k;->b(I)Lcom/google/android/gms/plus/model/c/a;

    move-result-object v2

    .line 957
    invoke-interface {v2}, Lcom/google/android/gms/plus/model/c/a;->f()Lcom/google/android/gms/plus/model/c/f;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/gms/plus/model/c/f;->d()Ljava/lang/String;

    move-result-object v2

    .line 958
    if-eqz v2, :cond_3

    .line 959
    new-instance v3, Lcom/google/android/gms/common/internal/bs;

    invoke-direct {v3, v2}, Lcom/google/android/gms/common/internal/bs;-><init>(Ljava/lang/String;)V

    const/4 v7, 0x0

    iput-boolean v7, v3, Lcom/google/android/gms/common/internal/bs;->d:Z

    sget v7, Lcom/google/android/gms/g;->N:I

    invoke-virtual {v3, v9, v7}, Lcom/google/android/gms/common/internal/bs;->a(Landroid/content/Context;I)Lcom/google/android/gms/common/internal/br;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/gms/common/internal/br;->a()Ljava/lang/String;

    move-result-object v3

    .line 964
    new-instance v7, Lcom/google/android/gms/common/internal/bs;

    invoke-direct {v7, v2}, Lcom/google/android/gms/common/internal/bs;-><init>(Ljava/lang/String;)V

    const/4 v2, 0x0

    iput-boolean v2, v7, Lcom/google/android/gms/common/internal/bs;->d:Z

    sget v2, Lcom/google/android/gms/g;->M:I

    invoke-virtual {v7, v9, v2}, Lcom/google/android/gms/common/internal/bs;->a(Landroid/content/Context;I)Lcom/google/android/gms/common/internal/br;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/gms/common/internal/br;->a()Ljava/lang/String;

    move-result-object v2

    .line 969
    invoke-static {v14, v3, v11}, Lcom/google/android/gms/games/a/l;->a(Landroid/net/Uri;Ljava/lang/String;Ljava/util/ArrayList;)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v12, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 970
    invoke-static {v14, v2, v11}, Lcom/google/android/gms/games/a/l;->a(Landroid/net/Uri;Ljava/lang/String;Ljava/util/ArrayList;)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v13, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 955
    :goto_3
    add-int/lit8 v7, v8, 0x1

    move v8, v7

    move-object v7, v3

    move-object v3, v2

    goto :goto_2

    .line 972
    :cond_3
    const/4 v2, 0x0

    invoke-virtual {v12, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 973
    const/4 v2, 0x0

    invoke-virtual {v13, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move-object v2, v3

    move-object v3, v7

    goto :goto_3

    .line 976
    :cond_4
    invoke-virtual {v12}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ne v2, v6, :cond_6

    const/4 v2, 0x1

    :goto_4
    invoke-static {v2}, Lcom/google/android/gms/common/internal/e;->a(Z)V

    .line 977
    invoke-virtual {v13}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ne v2, v6, :cond_7

    const/4 v2, 0x1

    :goto_5
    invoke-static {v2}, Lcom/google/android/gms/common/internal/e;->a(Z)V

    .line 980
    invoke-virtual {v9}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v8, "PlayerAgent"

    invoke-static {v2, v11, v8}, Lcom/google/android/gms/games/a/l;->b(Landroid/content/ContentResolver;Ljava/util/ArrayList;Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v11

    .line 982
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    .line 983
    const/4 v2, 0x0

    move v8, v2

    :goto_6
    if-ge v8, v6, :cond_a

    .line 987
    invoke-virtual {v10, v8}, Lcom/google/android/gms/plus/model/c/k;->b(I)Lcom/google/android/gms/plus/model/c/a;

    move-result-object v2

    .line 988
    invoke-interface {v2}, Lcom/google/android/gms/plus/model/c/a;->d()Ljava/lang/String;

    move-result-object v14

    .line 989
    invoke-static {v14}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v15

    if-nez v15, :cond_5

    .line 991
    new-instance v15, Landroid/content/ContentValues;

    invoke-direct {v15}, Landroid/content/ContentValues;-><init>()V

    .line 996
    const-string v17, "external_player_id"

    invoke-interface {v2}, Lcom/google/android/gms/plus/model/c/a;->e()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v17

    invoke-virtual {v15, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 997
    const-string v2, "profile_name"

    invoke-virtual {v15, v2, v14}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 998
    const-string v2, "last_updated"

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v14

    invoke-virtual {v15, v2, v14}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 999
    const-string v2, "is_in_circles"

    const/4 v14, 0x1

    invoke-static {v14}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v14

    invoke-virtual {v15, v2, v14}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1000
    const-string v14, "profile_icon_image_uri"

    invoke-virtual {v12, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    if-nez v2, :cond_8

    const/4 v2, 0x0

    :goto_7
    invoke-virtual {v15, v14, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1004
    const-string v2, "profile_icon_image_url"

    invoke-virtual {v15, v2, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1005
    const-string v14, "profile_hi_res_image_uri"

    invoke-virtual {v13, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    if-nez v2, :cond_9

    const/4 v2, 0x0

    :goto_8
    invoke-virtual {v15, v14, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1009
    const-string v2, "profile_hi_res_image_url"

    invoke-virtual {v15, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1010
    invoke-virtual {v9, v15}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 983
    :cond_5
    add-int/lit8 v2, v8, 0x1

    move v8, v2

    goto :goto_6

    .line 976
    :cond_6
    const/4 v2, 0x0

    goto :goto_4

    .line 977
    :cond_7
    const/4 v2, 0x0

    goto :goto_5

    .line 1000
    :cond_8
    invoke-virtual {v12, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {v11, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/content/ContentProviderResult;

    iget-object v2, v2, Landroid/content/ContentProviderResult;->uri:Landroid/net/Uri;

    invoke-virtual {v2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    goto :goto_7

    .line 1005
    :cond_9
    invoke-virtual {v13, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {v11, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/content/ContentProviderResult;

    iget-object v2, v2, Landroid/content/ContentProviderResult;->uri:Landroid/net/Uri;

    invoke-virtual {v2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    goto :goto_8

    .line 1014
    :cond_a
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/games/a/bj;->L:Lcom/google/android/gms/games/b/p;

    move-object/from16 v0, p2

    invoke-virtual {v2, v0}, Lcom/google/android/gms/games/b/p;->c(Ljava/lang/Object;)V

    .line 1015
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/gms/games/a/bj;->L:Lcom/google/android/gms/games/b/p;

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    move-object/from16 v8, p2

    move-wide v14, v4

    invoke-virtual/range {v7 .. v15}, Lcom/google/android/gms/games/b/p;->a(Ljava/lang/Object;Ljava/util/ArrayList;ILjava/lang/String;Ljava/lang/String;IJ)V

    .line 1017
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/games/a/bj;->L:Lcom/google/android/gms/games/b/p;

    const-string v3, "total_count"

    move-object/from16 v0, p2

    invoke-virtual {v2, v0, v3, v6}, Lcom/google/android/gms/games/b/p;->a(Ljava/lang/Object;Ljava/lang/String;I)V

    .line 1019
    :cond_b
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/games/a/bj;->L:Lcom/google/android/gms/games/b/p;

    const/4 v3, 0x0

    const/4 v4, -0x1

    move-object/from16 v0, p2

    invoke-virtual {v2, v0, v3, v4}, Lcom/google/android/gms/games/b/u;->a(Ljava/lang/Object;Landroid/os/Bundle;I)Lcom/google/android/gms/common/data/DataHolder;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v2

    .line 1021
    invoke-virtual/range {v16 .. v16}, Lcom/google/android/gms/common/data/DataHolder;->j()V

    goto/16 :goto_1

    :catchall_0
    move-exception v2

    invoke-virtual/range {v16 .. v16}, Lcom/google/android/gms/common/data/DataHolder;->j()V

    throw v2
.end method

.method private static c(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;)Ljava/util/HashMap;
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 1449
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 1450
    new-instance v0, Lcom/google/android/gms/games/a/o;

    invoke-direct {v0, p0}, Lcom/google/android/gms/games/a/o;-><init>(Landroid/content/Context;)V

    invoke-static {p1}, Lcom/google/android/gms/games/provider/aq;->a(Lcom/google/android/gms/common/server/ClientContext;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/android/gms/games/a/o;->a(Landroid/net/Uri;)Lcom/google/android/gms/games/a/o;

    move-result-object v0

    sget-object v2, Lcom/google/android/gms/games/a/bk;->a:[Ljava/lang/String;

    iput-object v2, v0, Lcom/google/android/gms/games/a/o;->b:[Ljava/lang/String;

    invoke-virtual {v0}, Lcom/google/android/gms/games/a/o;->b()Landroid/database/AbstractWindowedCursor;

    move-result-object v2

    .line 1455
    :goto_0
    :try_start_0
    invoke-interface {v2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1456
    const/4 v0, 0x0

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 1457
    const/4 v0, 0x1

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    .line 1460
    const/4 v0, 0x0

    .line 1461
    if-ne v4, v5, :cond_1

    .line 1462
    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    .line 1466
    :cond_0
    :goto_1
    invoke-virtual {v1, v3, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 1469
    :catchall_0
    move-exception v0

    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    throw v0

    .line 1463
    :cond_1
    if-nez v4, :cond_0

    .line 1464
    const/4 v0, 0x0

    :try_start_1
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    goto :goto_1

    .line 1469
    :cond_2
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    .line 1471
    return-object v1
.end method

.method private d(Lcom/google/android/gms/games/a/au;Ljava/lang/String;)Lcom/google/android/gms/games/h/a/cs;
    .locals 8

    .prologue
    const/4 v4, 0x0

    const/4 v6, 0x1

    const/4 v2, 0x0

    .line 1677
    iget-boolean v0, p1, Lcom/google/android/gms/games/a/au;->h:Z

    if-eqz v0, :cond_1

    .line 1678
    iget-object v0, p0, Lcom/google/android/gms/games/a/bj;->F:Lcom/google/android/gms/games/h/a/dj;

    iget-object v1, p1, Lcom/google/android/gms/games/a/au;->b:Lcom/google/android/gms/common/server/ClientContext;

    iget-object v3, p1, Lcom/google/android/gms/games/a/au;->a:Landroid/content/Context;

    invoke-static {v3}, Lcom/google/android/gms/games/a/l;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v5

    const-string v3, "players/%1$s"

    new-array v6, v6, [Ljava/lang/Object;

    invoke-static {p2}, Lcom/google/android/gms/games/h/a/dj;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v2

    invoke-static {v3, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    if-eqz v5, :cond_0

    const-string v6, "language"

    invoke-static {v5}, Lcom/google/android/gms/games/h/a/dj;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v6, v5}, Lcom/google/android/gms/games/h/a/dj;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    :cond_0
    iget-object v0, v0, Lcom/google/android/gms/games/h/a/dj;->a:Lcom/google/android/gms/common/server/n;

    const-class v5, Lcom/google/android/gms/games/h/a/bk;

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/common/server/n;->a(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;Ljava/lang/Object;Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/h/a/bk;

    .line 1681
    invoke-virtual {v0}, Lcom/google/android/gms/games/h/a/bk;->getDisplayPlayer()Lcom/google/android/gms/games/h/a/cs;

    move-result-object v0

    .line 1683
    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/games/a/bj;->E:Lcom/google/android/gms/games/h/a/di;

    iget-object v1, p1, Lcom/google/android/gms/games/a/au;->b:Lcom/google/android/gms/common/server/ClientContext;

    iget-object v3, p1, Lcom/google/android/gms/games/a/au;->a:Landroid/content/Context;

    invoke-static {v3}, Lcom/google/android/gms/games/a/l;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v5

    const-string v3, "players/%1$s"

    new-array v6, v6, [Ljava/lang/Object;

    invoke-static {p2}, Lcom/google/android/gms/games/h/a/di;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v2

    invoke-static {v3, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    if-eqz v5, :cond_2

    const-string v6, "language"

    invoke-static {v5}, Lcom/google/android/gms/games/h/a/di;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v6, v5}, Lcom/google/android/gms/games/h/a/di;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    :cond_2
    iget-object v0, v0, Lcom/google/android/gms/games/h/a/di;->a:Lcom/google/android/gms/common/server/n;

    const-class v5, Lcom/google/android/gms/games/h/a/cs;

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/common/server/n;->a(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;Ljava/lang/Object;Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/h/a/cs;

    goto :goto_0
.end method

.method private static d(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;)V
    .locals 9

    .prologue
    const-wide/16 v2, -0x1

    .line 2096
    sget-object v0, Lcom/google/android/gms/games/c/a;->m:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v5

    .line 2097
    invoke-static {p1}, Lcom/google/android/gms/games/provider/x;->a(Lcom/google/android/gms/common/server/ClientContext;)Landroid/net/Uri;

    move-result-object v6

    .line 2098
    new-instance v0, Lcom/google/android/gms/games/a/o;

    invoke-direct {v0, p0}, Lcom/google/android/gms/games/a/o;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v6}, Lcom/google/android/gms/games/a/o;->a(Landroid/net/Uri;)Lcom/google/android/gms/games/a/o;

    move-result-object v0

    sget-object v1, Lcom/google/android/gms/games/a/bo;->a:[Ljava/lang/String;

    iput-object v1, v0, Lcom/google/android/gms/games/a/o;->b:[Ljava/lang/String;

    const-string v1, "created_timestamp DESC"

    iput-object v1, v0, Lcom/google/android/gms/games/a/o;->c:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/google/android/gms/games/a/o;->b()Landroid/database/AbstractWindowedCursor;

    move-result-object v7

    .line 2103
    const/4 v0, 0x0

    move v4, v0

    move-wide v0, v2

    .line 2106
    :goto_0
    :try_start_0
    invoke-interface {v7}, Landroid/database/Cursor;->moveToNext()Z

    move-result v8

    if-eqz v8, :cond_0

    .line 2107
    if-ge v4, v5, :cond_0

    .line 2108
    add-int/lit8 v4, v4, 0x1

    .line 2109
    const/4 v0, 0x2

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getLong(I)J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-wide v0

    goto :goto_0

    .line 2115
    :cond_0
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 2118
    cmp-long v2, v0, v2

    if-eqz v2, :cond_1

    if-ge v4, v5, :cond_2

    .line 2127
    :cond_1
    :goto_1
    return-void

    .line 2115
    :catchall_0
    move-exception v0

    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    throw v0

    .line 2122
    :cond_2
    new-instance v2, Lcom/google/android/gms/common/e/b;

    invoke-direct {v2, v6}, Lcom/google/android/gms/common/e/b;-><init>(Landroid/net/Uri;)V

    .line 2123
    const-string v3, "created_timestamp"

    invoke-static {v0, v1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    const-string v1, "<?"

    invoke-virtual {v2, v3, v0, v1}, Lcom/google/android/gms/common/e/b;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2125
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-virtual {v2}, Lcom/google/android/gms/common/e/b;->a()Ljava/lang/String;

    move-result-object v1

    iget-object v2, v2, Lcom/google/android/gms/common/e/b;->c:[Ljava/lang/String;

    invoke-virtual {v0, v6, v1, v2}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    goto :goto_1
.end method

.method private g(Lcom/google/android/gms/games/a/au;)Lcom/google/android/gms/games/Player;
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 2234
    const/4 v0, 0x0

    .line 2235
    iget-boolean v1, p1, Lcom/google/android/gms/games/a/au;->g:Z

    if-eqz v1, :cond_0

    .line 2236
    invoke-virtual {p1}, Lcom/google/android/gms/games/a/au;->c()Lcom/google/android/gms/games/a/av;

    move-result-object v1

    iput-boolean v2, v1, Lcom/google/android/gms/games/a/av;->g:Z

    invoke-virtual {v1}, Lcom/google/android/gms/games/a/av;->a()Lcom/google/android/gms/games/a/au;

    move-result-object p1

    .line 2240
    :cond_0
    const/4 v1, 0x1

    invoke-direct {p0, p1, v1}, Lcom/google/android/gms/games/a/bj;->b(Lcom/google/android/gms/games/a/au;Z)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v1

    .line 2241
    new-instance v2, Lcom/google/android/gms/games/t;

    invoke-direct {v2, v1}, Lcom/google/android/gms/games/t;-><init>(Lcom/google/android/gms/common/data/DataHolder;)V

    .line 2243
    :try_start_0
    invoke-virtual {v2}, Lcom/google/android/gms/games/t;->c()I

    move-result v3

    if-lez v3, :cond_1

    .line 2244
    const/4 v0, 0x0

    invoke-virtual {v2, v0}, Lcom/google/android/gms/games/t;->b(I)Lcom/google/android/gms/games/Player;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/games/Player;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/Player;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2247
    :cond_1
    invoke-virtual {v1}, Lcom/google/android/gms/common/data/DataHolder;->j()V

    .line 2249
    return-object v0

    .line 2247
    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Lcom/google/android/gms/common/data/DataHolder;->j()V

    throw v0
.end method


# virtual methods
.method public final a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;)I
    .locals 10

    .prologue
    const/4 v6, 0x0

    .line 1904
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/games/a/bj;->I:Lcom/google/android/gms/games/h/a/ci;

    const-string v3, "metagameConfig"

    iget-object v0, v0, Lcom/google/android/gms/games/h/a/ci;->a:Lcom/google/android/gms/common/server/n;

    const/4 v2, 0x0

    const/4 v4, 0x0

    const-class v5, Lcom/google/android/gms/games/h/a/cj;

    move-object v1, p2

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/common/server/n;->a(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;Ljava/lang/Object;Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/h/a/cj;
    :try_end_0
    .catch Lcom/android/volley/ac; {:try_start_0 .. :try_end_0} :catch_0

    .line 1909
    invoke-static {p2}, Lcom/google/android/gms/games/provider/ap;->a(Lcom/google/android/gms/common/server/ClientContext;)Landroid/net/Uri;

    move-result-object v1

    const-string v2, "version"

    const/4 v3, -0x1

    invoke-static {p1, v1, v2, v3}, Lcom/google/android/gms/games/a/l;->a(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;I)I

    move-result v1

    .line 1911
    invoke-virtual {v0}, Lcom/google/android/gms/games/h/a/cj;->b()Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    if-ne v1, v2, :cond_0

    move v0, v6

    .line 1938
    :goto_0
    return v0

    .line 1906
    :catch_0
    move-exception v0

    const/4 v0, 0x6

    goto :goto_0

    .line 1913
    :cond_0
    invoke-virtual {v0}, Lcom/google/android/gms/games/h/a/cj;->b()Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    if-le v1, v2, :cond_1

    .line 1914
    const-string v1, "PlayerAgent"

    const-string v2, "Local player level table version is higher than the server\'s version, replacing local data."

    invoke-static {v1, v2}, Lcom/google/android/gms/games/internal/dq;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1918
    :cond_1
    invoke-virtual {v0}, Lcom/google/android/gms/games/h/a/cj;->b()Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v2

    .line 1921
    invoke-virtual {v0}, Lcom/google/android/gms/games/h/a/cj;->getPlayerLevels()Ljava/util/ArrayList;

    move-result-object v3

    .line 1922
    new-instance v4, Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    invoke-direct {v4, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 1926
    invoke-static {p2}, Lcom/google/android/gms/games/provider/ap;->a(Lcom/google/android/gms/common/server/ClientContext;)Landroid/net/Uri;

    move-result-object v0

    invoke-static {v0}, Landroid/content/ContentProviderOperation;->newDelete(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1930
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v5

    move v1, v6

    :goto_1
    if-ge v1, v5, :cond_2

    .line 1931
    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/h/a/db;

    .line 1932
    invoke-virtual {v0}, Lcom/google/android/gms/games/h/a/db;->b()Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v7

    invoke-static {p2, v7}, Lcom/google/android/gms/games/provider/ap;->a(Lcom/google/android/gms/common/server/ClientContext;I)Landroid/net/Uri;

    move-result-object v7

    .line 1933
    iget-object v0, v0, Lcom/google/android/gms/common/server/response/a;->a:Landroid/content/ContentValues;

    .line 1934
    const-string v8, "version"

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v0, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1935
    invoke-static {v7}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v7

    invoke-virtual {v7, v0}, Landroid/content/ContentProviderOperation$Builder;->withValues(Landroid/content/ContentValues;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1930
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 1938
    :cond_2
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "PlayerAgent"

    invoke-static {v0, v4, v1}, Lcom/google/android/gms/games/a/l;->a(Landroid/content/ContentResolver;Ljava/util/ArrayList;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    move v0, v6

    goto :goto_0

    :cond_3
    const/4 v0, 0x1

    goto/16 :goto_0
.end method

.method public final a(Lcom/google/android/gms/common/server/ClientContext;ZLandroid/os/Bundle;)I
    .locals 5

    .prologue
    .line 1548
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 1549
    invoke-virtual {p3}, Landroid/os/Bundle;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1550
    new-instance v3, Lcom/google/android/gms/games/h/a/ao;

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v4

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-direct {v3, v0, v4}, Lcom/google/android/gms/games/h/a/ao;-><init>(Ljava/lang/String;Ljava/lang/Boolean;)V

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1553
    :cond_0
    new-instance v2, Lcom/google/android/gms/games/h/a/ap;

    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-direct {v2, v0, v1}, Lcom/google/android/gms/games/h/a/ap;-><init>(Ljava/lang/Boolean;Ljava/util/ArrayList;)V

    .line 1554
    iget-object v0, p0, Lcom/google/android/gms/games/a/bj;->P:Lcom/google/android/gms/games/b/i;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/games/b/i;->a(Ljava/lang/Object;)V

    .line 1555
    iget-object v0, p0, Lcom/google/android/gms/games/a/bj;->P:Lcom/google/android/gms/games/b/i;

    iget-object v0, v0, Lcom/google/android/gms/games/b/u;->a:Landroid/support/v4/g/h;

    invoke-virtual {v0}, Landroid/support/v4/g/h;->b()V

    .line 1558
    const/4 v0, 0x0

    .line 1560
    :try_start_0
    iget-object v1, p0, Lcom/google/android/gms/games/a/bj;->F:Lcom/google/android/gms/games/h/a/dj;

    const-string v3, "players/me/contactsettings"

    iget-object v1, v1, Lcom/google/android/gms/games/h/a/dj;->a:Lcom/google/android/gms/common/server/n;

    const/4 v4, 0x2

    invoke-virtual {v1, p1, v4, v3, v2}, Lcom/google/android/gms/common/server/n;->a(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;Ljava/lang/Object;)V
    :try_end_0
    .catch Lcom/android/volley/ac; {:try_start_0 .. :try_end_0} :catch_0

    .line 1567
    :goto_1
    return v0

    .line 1561
    :catch_0
    move-exception v0

    .line 1562
    invoke-static {}, Lcom/google/android/gms/games/internal/dq;->a()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1563
    const-string v1, "PlayerAgent"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/server/b/c;->a(Lcom/android/volley/ac;Ljava/lang/String;)Lcom/google/android/gms/common/server/b/b;

    .line 1565
    :cond_1
    const/4 v0, 0x6

    goto :goto_1
.end method

.method public final a(Lcom/google/android/gms/games/a/au;I)I
    .locals 7

    .prologue
    .line 1051
    iget-object v0, p0, Lcom/google/android/gms/games/a/bj;->g:Lcom/google/android/gms/games/a/bb;

    invoke-virtual {v0}, Lcom/google/android/gms/games/a/bb;->d()V

    .line 1052
    iget-object v2, p1, Lcom/google/android/gms/games/a/au;->f:Ljava/lang/String;

    .line 1055
    iget-object v0, p0, Lcom/google/android/gms/games/a/bj;->O:Ljava/util/HashMap;

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 1057
    invoke-static {p2}, Lcom/google/android/gms/games/internal/b/h;->a(I)Ljava/lang/String;

    move-result-object v3

    .line 1058
    if-eqz v4, :cond_0

    .line 1059
    iget-object v0, p0, Lcom/google/android/gms/games/a/bj;->H:Lcom/google/android/gms/plus/service/v1whitelisted/i;

    iget-object v1, p1, Lcom/google/android/gms/games/a/au;->b:Lcom/google/android/gms/common/server/ClientContext;

    iget-object v5, p0, Lcom/google/android/gms/games/a/bj;->K:Lcom/google/android/gms/common/server/w;

    iget-object v6, p0, Lcom/google/android/gms/games/a/bj;->K:Lcom/google/android/gms/common/server/w;

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/gms/plus/service/v1whitelisted/i;->a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/android/volley/x;Lcom/android/volley/w;)V

    .line 1062
    const/4 v0, 0x0

    .line 1066
    :goto_0
    return v0

    .line 1064
    :cond_0
    const-string v0, "PlayerAgent"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v4, "Unable to record action "

    invoke-direct {v1, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " for player; could not find suggestion ID for player "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1066
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/games/a/au;Z)I
    .locals 6

    .prologue
    .line 1614
    iget-object v1, p1, Lcom/google/android/gms/games/a/au;->b:Lcom/google/android/gms/common/server/ClientContext;

    .line 1615
    new-instance v2, Lcom/google/android/gms/games/h/a/dk;

    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-direct {v2, v0, v3}, Lcom/google/android/gms/games/h/a/dk;-><init>(Ljava/lang/Boolean;Ljava/lang/Boolean;)V

    .line 1617
    const/4 v0, 0x0

    .line 1619
    :try_start_0
    iget-object v3, p0, Lcom/google/android/gms/games/a/bj;->F:Lcom/google/android/gms/games/h/a/dj;

    const-string v4, "players/me/profilesettings"

    iget-object v3, v3, Lcom/google/android/gms/games/h/a/dj;->a:Lcom/google/android/gms/common/server/n;

    const/4 v5, 0x2

    invoke-virtual {v3, v1, v5, v4, v2}, Lcom/google/android/gms/common/server/n;->a(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;Ljava/lang/Object;)V
    :try_end_0
    .catch Lcom/android/volley/ac; {:try_start_0 .. :try_end_0} :catch_0

    .line 1628
    :goto_0
    if-nez v0, :cond_0

    .line 1629
    iget-object v3, p0, Lcom/google/android/gms/games/a/bj;->Q:Lcom/google/android/gms/games/b/r;

    invoke-virtual {v3, v1}, Lcom/google/android/gms/games/b/r;->a(Ljava/lang/Object;)V

    .line 1630
    invoke-direct {p0, p1, v2}, Lcom/google/android/gms/games/a/bj;->a(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/h/a/dk;)V

    .line 1632
    :cond_0
    return v0

    .line 1620
    :catch_0
    move-exception v0

    .line 1621
    invoke-static {}, Lcom/google/android/gms/games/internal/dq;->a()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 1622
    const-string v3, "PlayerAgent"

    invoke-static {v0, v3}, Lcom/google/android/gms/common/server/b/c;->a(Lcom/android/volley/ac;Ljava/lang/String;)Lcom/google/android/gms/common/server/b/b;

    .line 1624
    :cond_1
    const/4 v0, 0x6

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/common/server/ClientContext;Z)Lcom/google/android/gms/common/data/DataHolder;
    .locals 13

    .prologue
    const/4 v12, -0x1

    const/4 v10, 0x0

    const/4 v11, 0x0

    .line 1484
    invoke-static {}, Lcom/google/android/gms/common/util/r;->c()Lcom/google/android/gms/common/util/p;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/common/util/p;->a()J

    move-result-wide v8

    .line 1485
    if-nez p2, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/games/a/bj;->P:Lcom/google/android/gms/games/b/i;

    invoke-virtual {v0, p1, v8, v9}, Lcom/google/android/gms/games/b/i;->a(Ljava/lang/Object;J)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1486
    iget-object v0, p0, Lcom/google/android/gms/games/a/bj;->P:Lcom/google/android/gms/games/b/i;

    invoke-virtual {v0, p1, v11, v12}, Lcom/google/android/gms/games/b/u;->a(Ljava/lang/Object;Landroid/os/Bundle;I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    .line 1533
    :goto_0
    return-object v0

    .line 1492
    :cond_0
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/games/a/bj;->F:Lcom/google/android/gms/games/h/a/dj;

    const-string v3, "players/me/contactsettings"

    iget-object v0, v0, Lcom/google/android/gms/games/h/a/dj;->a:Lcom/google/android/gms/common/server/n;

    const/4 v2, 0x0

    const/4 v4, 0x0

    const-class v5, Lcom/google/android/gms/games/h/a/ap;

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/common/server/n;->a(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;Ljava/lang/Object;Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/h/a/ap;
    :try_end_0
    .catch Lcom/android/volley/ac; {:try_start_0 .. :try_end_0} :catch_0

    .line 1500
    :goto_1
    if-nez v0, :cond_2

    .line 1501
    const/4 v0, 0x4

    invoke-static {v0}, Lcom/google/android/gms/common/data/DataHolder;->b(I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    goto :goto_0

    .line 1493
    :catch_0
    move-exception v0

    .line 1494
    invoke-static {}, Lcom/google/android/gms/games/internal/dq;->a()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1495
    const-string v1, "PlayerAgent"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/server/b/c;->a(Lcom/android/volley/ac;Ljava/lang/String;)Lcom/google/android/gms/common/server/b/b;

    :cond_1
    move-object v0, v11

    goto :goto_1

    .line 1504
    :cond_2
    new-instance v4, Landroid/content/ContentValues;

    invoke-direct {v4}, Landroid/content/ContentValues;-><init>()V

    .line 1505
    const-string v1, "mobile_notifications_enabled"

    invoke-virtual {v0}, Lcom/google/android/gms/games/h/a/ap;->b()Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v4, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 1507
    invoke-virtual {v0}, Lcom/google/android/gms/games/h/a/ap;->getPerChannelSettings()Ljava/util/ArrayList;

    move-result-object v3

    .line 1508
    if-nez v3, :cond_3

    move v1, v10

    :goto_2
    move v2, v10

    .line 1509
    :goto_3
    if-ge v2, v1, :cond_4

    .line 1510
    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/h/a/ao;

    .line 1511
    invoke-virtual {v0}, Lcom/google/android/gms/games/h/a/ao;->b()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/google/android/gms/games/internal/b/e;->a(Ljava/lang/String;)I

    move-result v5

    .line 1512
    invoke-virtual {v0}, Lcom/google/android/gms/games/h/a/ao;->c()Ljava/lang/Boolean;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v6

    .line 1513
    packed-switch v5, :pswitch_data_0

    .line 1524
    const-string v5, "PlayerAgent"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "Ignoring unknown channel "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/google/android/gms/games/h/a/ao;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v5, v0}, Lcom/google/android/gms/games/internal/dq;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1509
    :goto_4
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_3

    .line 1508
    :cond_3
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v0

    move v1, v0

    goto :goto_2

    .line 1515
    :pswitch_0
    const-string v0, "match_notifications_enabled"

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    invoke-virtual {v4, v0, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    goto :goto_4

    .line 1518
    :pswitch_1
    const-string v0, "quest_notifications_enabled"

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    invoke-virtual {v4, v0, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    goto :goto_4

    .line 1521
    :pswitch_2
    const-string v0, "request_notifications_enabled"

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    invoke-virtual {v4, v0, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    goto :goto_4

    .line 1528
    :cond_4
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 1529
    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1530
    iget-object v1, p0, Lcom/google/android/gms/games/a/bj;->P:Lcom/google/android/gms/games/b/i;

    move-object v2, p1

    move v4, v10

    move-object v5, v11

    move-object v6, v11

    move v7, v10

    invoke-virtual/range {v1 .. v9}, Lcom/google/android/gms/games/b/i;->a(Ljava/lang/Object;Ljava/util/ArrayList;ILjava/lang/String;Ljava/lang/String;IJ)V

    .line 1533
    iget-object v0, p0, Lcom/google/android/gms/games/a/bj;->P:Lcom/google/android/gms/games/b/i;

    invoke-virtual {v0, p1, v11, v12}, Lcom/google/android/gms/games/b/u;->a(Ljava/lang/Object;Landroid/os/Bundle;I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    goto/16 :goto_0

    .line 1513
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method public final a(Lcom/google/android/gms/games/a/au;)Lcom/google/android/gms/common/data/DataHolder;
    .locals 1

    .prologue
    .line 529
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/gms/games/a/bj;->b(Lcom/google/android/gms/games/a/au;Z)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/games/a/au;IZ)Lcom/google/android/gms/common/data/DataHolder;
    .locals 6

    .prologue
    .line 762
    iget-boolean v0, p1, Lcom/google/android/gms/games/a/au;->h:Z

    const-string v1, "Calling circled from 3P context!"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/e;->a(ZLjava/lang/Object;)V

    .line 763
    iget-object v0, p0, Lcom/google/android/gms/games/a/bj;->e:Lcom/google/android/gms/games/a/bb;

    invoke-virtual {v0}, Lcom/google/android/gms/games/a/bb;->d()V

    .line 766
    const-string v0, "circled"

    invoke-direct {p0, p1, v0}, Lcom/google/android/gms/games/a/bj;->c(Lcom/google/android/gms/games/a/au;Ljava/lang/String;)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    .line 767
    if-eqz v0, :cond_0

    .line 771
    :goto_0
    return-object v0

    :cond_0
    const-string v2, "circled"

    const-string v3, "circled"

    move-object v0, p0

    move-object v1, p1

    move v4, p2

    move v5, p3

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/games/a/bj;->a(Lcom/google/android/gms/games/a/au;Ljava/lang/String;Ljava/lang/String;IZ)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/games/a/au;JLcom/google/android/gms/games/internal/el;)Lcom/google/android/gms/common/data/DataHolder;
    .locals 14

    .prologue
    .line 2168
    invoke-direct {p0, p1}, Lcom/google/android/gms/games/a/bj;->g(Lcom/google/android/gms/games/a/au;)Lcom/google/android/gms/games/Player;

    move-result-object v3

    .line 2169
    if-nez v3, :cond_0

    .line 2170
    const-string v2, "PlayerAgent"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Could not find player "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p1, Lcom/google/android/gms/games/a/au;->f:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/gms/games/internal/dq;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 2171
    const/4 v2, 0x1

    invoke-static {v2}, Lcom/google/android/gms/common/data/DataHolder;->b(I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v2

    .line 2211
    :goto_0
    return-object v2

    .line 2174
    :cond_0
    invoke-interface {v3}, Lcom/google/android/gms/games/Player;->n()Lcom/google/android/gms/games/PlayerLevelInfo;

    move-result-object v2

    if-nez v2, :cond_1

    .line 2175
    const-string v2, "PlayerAgent"

    const-string v3, "Trying to add XP values to a player with no level info!"

    invoke-static {v2, v3}, Lcom/google/android/gms/games/internal/dq;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2176
    const/4 v2, 0x1

    invoke-static {v2}, Lcom/google/android/gms/common/data/DataHolder;->b(I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v2

    goto :goto_0

    .line 2178
    :cond_1
    invoke-interface {v3}, Lcom/google/android/gms/games/Player;->n()Lcom/google/android/gms/games/PlayerLevelInfo;

    move-result-object v4

    new-instance v5, Landroid/content/ContentValues;

    invoke-direct {v5}, Landroid/content/ContentValues;-><init>()V

    invoke-virtual {v4}, Lcom/google/android/gms/games/PlayerLevelInfo;->b()J

    move-result-wide v6

    add-long v6, v6, p2

    const-string v2, "current_xp_total"

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    invoke-virtual {v5, v2, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const/4 v2, 0x0

    invoke-virtual {v4}, Lcom/google/android/gms/games/PlayerLevelInfo;->d()Lcom/google/android/gms/games/PlayerLevel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/gms/games/PlayerLevel;->d()J

    move-result-wide v8

    cmp-long v4, v6, v8

    if-ltz v4, :cond_2

    invoke-direct {p0, p1, v6, v7}, Lcom/google/android/gms/games/a/bj;->a(Lcom/google/android/gms/games/a/au;J)Landroid/util/Pair;

    move-result-object v4

    if-eqz v4, :cond_2

    const-string v6, "current_level"

    iget-object v2, v4, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v2, Lcom/google/android/gms/games/PlayerLevel;

    invoke-virtual {v2}, Lcom/google/android/gms/games/PlayerLevel;->b()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v5, v6, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v6, "current_level_min_xp"

    iget-object v2, v4, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v2, Lcom/google/android/gms/games/PlayerLevel;

    invoke-virtual {v2}, Lcom/google/android/gms/games/PlayerLevel;->c()J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v5, v6, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v6, "current_level_max_xp"

    iget-object v2, v4, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v2, Lcom/google/android/gms/games/PlayerLevel;

    invoke-virtual {v2}, Lcom/google/android/gms/games/PlayerLevel;->d()J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v5, v6, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v2, "last_level_up_timestamp"

    invoke-static {}, Lcom/google/android/gms/common/util/r;->c()Lcom/google/android/gms/common/util/p;

    move-result-object v6

    invoke-interface {v6}, Lcom/google/android/gms/common/util/p;->a()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v5, v2, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v6, "next_level"

    iget-object v2, v4, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v2, Lcom/google/android/gms/games/PlayerLevel;

    invoke-virtual {v2}, Lcom/google/android/gms/games/PlayerLevel;->b()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v5, v6, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v6, "next_level_max_xp"

    iget-object v2, v4, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v2, Lcom/google/android/gms/games/PlayerLevel;

    invoke-virtual {v2}, Lcom/google/android/gms/games/PlayerLevel;->d()J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v5, v6, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const/4 v2, 0x1

    :cond_2
    new-instance v4, Landroid/util/Pair;

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-direct {v4, v5, v2}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 2181
    iget-object v2, p1, Lcom/google/android/gms/games/a/au;->b:Lcom/google/android/gms/common/server/ClientContext;

    invoke-virtual {p1}, Lcom/google/android/gms/games/a/au;->f()Ljava/lang/String;

    move-result-object v5

    invoke-static {v2, v5}, Lcom/google/android/gms/games/provider/aq;->a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    .line 2183
    iget-object v2, p1, Lcom/google/android/gms/games/a/au;->a:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    iget-object v2, v4, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v2, Landroid/content/ContentValues;

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-virtual {v6, v5, v2, v7, v8}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 2185
    iget-object v2, p0, Lcom/google/android/gms/games/a/bj;->S:Lcom/google/android/gms/games/b/w;

    iget-object v2, v2, Lcom/google/android/gms/games/b/u;->a:Landroid/support/v4/g/h;

    invoke-virtual {v2}, Landroid/support/v4/g/h;->b()V

    .line 2188
    iget-object v2, v4, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v2, Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-nez v2, :cond_3

    .line 2189
    const/4 v2, 0x0

    invoke-static {v2}, Lcom/google/android/gms/common/data/DataHolder;->b(I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v2

    goto/16 :goto_0

    .line 2194
    :cond_3
    invoke-direct {p0, p1}, Lcom/google/android/gms/games/a/bj;->g(Lcom/google/android/gms/games/a/au;)Lcom/google/android/gms/games/Player;

    move-result-object v5

    .line 2195
    invoke-interface {v3}, Lcom/google/android/gms/games/Player;->c()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/gms/games/Player;

    invoke-interface {v5}, Lcom/google/android/gms/games/Player;->c()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/gms/games/Player;

    move-object/from16 v0, p4

    invoke-static {p1, v0, v2, v3}, Lcom/google/android/gms/games/ui/c/g;->a(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/internal/el;Lcom/google/android/gms/games/Player;Lcom/google/android/gms/games/Player;)V

    iget-object v2, p1, Lcom/google/android/gms/games/a/au;->a:Landroid/content/Context;

    iget-object v3, p1, Lcom/google/android/gms/games/a/au;->e:Ljava/lang/String;

    invoke-static {v2}, Lcom/google/android/gms/games/service/i;->b(Landroid/content/Context;)Z

    move-result v4

    if-nez v4, :cond_4

    const/4 v2, 0x0

    move-object v3, v2

    .line 2197
    :goto_1
    if-nez v3, :cond_5

    .line 2198
    const/16 v2, 0x5dd

    invoke-static {v2}, Lcom/google/android/gms/common/data/DataHolder;->b(I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v2

    goto/16 :goto_0

    .line 2195
    :cond_4
    invoke-interface {v5}, Lcom/google/android/gms/games/Player;->n()Lcom/google/android/gms/games/PlayerLevelInfo;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/gms/games/PlayerLevelInfo;->d()Lcom/google/android/gms/games/PlayerLevel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/gms/games/PlayerLevel;->b()I

    move-result v9

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v10

    new-instance v2, Lcom/google/android/gms/games/a/bi;

    invoke-static {}, Lcom/google/android/gms/games/a/bh;->a()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v5}, Lcom/google/android/gms/games/Player;->a()Ljava/lang/String;

    move-result-object v5

    const/16 v6, 0x10

    sget v7, Lcom/google/android/gms/p;->jM:I

    invoke-virtual {v10, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    sget v8, Lcom/google/android/gms/p;->jN:I

    invoke-virtual {v10, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v8

    sget v11, Lcom/google/android/gms/p;->jL:I

    const/4 v12, 0x1

    new-array v12, v12, [Ljava/lang/Object;

    const/4 v13, 0x0

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v12, v13

    invoke-virtual {v10, v11, v12}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    sget-object v10, Lcom/google/android/gms/games/a/bj;->D:Landroid/net/Uri;

    invoke-direct/range {v2 .. v10}, Lcom/google/android/gms/games/a/bi;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;)V

    move-object v3, v2

    goto :goto_1

    .line 2202
    :cond_5
    iget-object v2, p1, Lcom/google/android/gms/games/a/au;->a:Landroid/content/Context;

    iget-object v2, p1, Lcom/google/android/gms/games/a/au;->b:Lcom/google/android/gms/common/server/ClientContext;

    iget-object v2, v3, Lcom/google/android/gms/games/a/bi;->h:Landroid/net/Uri;

    if-nez v2, :cond_6

    const/4 v2, 0x0

    :goto_2
    new-instance v4, Landroid/content/ContentValues;

    invoke-direct {v4}, Landroid/content/ContentValues;-><init>()V

    const-string v5, "notification_id"

    iget-object v6, v3, Lcom/google/android/gms/games/a/bi;->b:Ljava/lang/String;

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v5, "external_game_id"

    iget-object v6, v3, Lcom/google/android/gms/games/a/bi;->a:Ljava/lang/String;

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v5, "external_sub_id"

    iget-object v6, v3, Lcom/google/android/gms/games/a/bi;->c:Ljava/lang/String;

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v5, "type"

    iget v6, v3, Lcom/google/android/gms/games/a/bi;->d:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v5, "image_uri"

    invoke-virtual {v4, v5, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "ticker"

    iget-object v5, v3, Lcom/google/android/gms/games/a/bi;->e:Ljava/lang/String;

    invoke-virtual {v4, v2, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "title"

    iget-object v5, v3, Lcom/google/android/gms/games/a/bi;->f:Ljava/lang/String;

    invoke-virtual {v4, v2, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "text"

    iget-object v5, v3, Lcom/google/android/gms/games/a/bi;->g:Ljava/lang/String;

    invoke-virtual {v4, v2, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "coalesced_text"

    iget-object v3, v3, Lcom/google/android/gms/games/a/bi;->g:Ljava/lang/String;

    invoke-virtual {v4, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "acknowledged"

    const/4 v3, 0x0

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v4, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v2, "alert_level"

    const/4 v3, 0x1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v4, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2205
    sget-object v2, Lcom/google/android/gms/games/a/bj;->A:Lcom/google/android/gms/games/provider/a;

    iget-object v2, v2, Lcom/google/android/gms/games/provider/a;->b:[Ljava/lang/String;

    invoke-static {v2}, Lcom/google/android/gms/common/data/DataHolder;->a([Ljava/lang/String;)Lcom/google/android/gms/common/data/m;

    move-result-object v2

    .line 2210
    invoke-virtual {v2, v4}, Lcom/google/android/gms/common/data/m;->a(Landroid/content/ContentValues;)Lcom/google/android/gms/common/data/m;

    .line 2211
    const/16 v3, 0x5dd

    invoke-virtual {v2, v3}, Lcom/google/android/gms/common/data/m;->a(I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v2

    goto/16 :goto_0

    .line 2202
    :cond_6
    iget-object v2, v3, Lcom/google/android/gms/games/a/bi;->h:Landroid/net/Uri;

    invoke-virtual {v2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    goto :goto_2
.end method

.method public final a(Lcom/google/android/gms/games/a/au;Ljava/lang/String;IZ)Lcom/google/android/gms/common/data/DataHolder;
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 720
    invoke-virtual {p0, p1, p2}, Lcom/google/android/gms/games/a/bj;->a(Lcom/google/android/gms/games/a/au;Ljava/lang/String;)Lcom/google/android/gms/games/a/bb;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/games/a/bb;->d()V

    .line 721
    const-string v0, "played_with"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "circled"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "you_may_know"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "nearby"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/android/gms/common/internal/e;->a(Z)V

    .line 727
    iget-object v0, p1, Lcom/google/android/gms/games/a/au;->e:Ljava/lang/String;

    invoke-static {v0, p2}, Lcom/google/android/gms/games/b/p;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 729
    const-string v0, "nearby"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 730
    iget-object v0, p0, Lcom/google/android/gms/games/a/bj;->M:Lcom/google/android/gms/games/b/p;

    iget-object v3, p1, Lcom/google/android/gms/games/a/au;->c:Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/google/android/gms/games/b/p;->a(Ljava/lang/Object;)V

    .line 731
    invoke-static {}, Lcom/google/android/gms/common/util/r;->c()Lcom/google/android/gms/common/util/p;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/common/util/p;->a()J

    move-result-wide v4

    .line 734
    iget-object v0, p0, Lcom/google/android/gms/games/a/bj;->M:Lcom/google/android/gms/games/b/p;

    invoke-virtual {v0, v2, v4, v5}, Lcom/google/android/gms/games/b/p;->a(Ljava/lang/Object;J)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 735
    iget-object v0, p0, Lcom/google/android/gms/games/a/bj;->M:Lcom/google/android/gms/games/b/p;

    const/4 v1, 0x0

    const/4 v3, -0x1

    invoke-virtual {v0, v2, v1, v3}, Lcom/google/android/gms/games/b/u;->a(Ljava/lang/Object;Landroid/os/Bundle;I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    .line 740
    :goto_1
    return-object v0

    :cond_1
    move v0, v1

    .line 721
    goto :goto_0

    .line 737
    :cond_2
    invoke-static {v1}, Lcom/google/android/gms/common/data/DataHolder;->b(I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    goto :goto_1

    :cond_3
    move-object v0, p0

    move-object v1, p1

    move-object v3, p2

    move v4, p3

    move v5, p4

    .line 740
    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/games/a/bj;->a(Lcom/google/android/gms/games/a/au;Ljava/lang/String;Ljava/lang/String;IZ)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    goto :goto_1
.end method

.method public final a(Lcom/google/android/gms/games/a/au;[Ljava/lang/String;)Lcom/google/android/gms/common/data/DataHolder;
    .locals 11

    .prologue
    const/4 v10, 0x1

    const/4 v1, 0x0

    .line 535
    new-instance v3, Lcom/google/android/gms/common/e/b;

    iget-object v0, p1, Lcom/google/android/gms/games/a/au;->b:Lcom/google/android/gms/common/server/ClientContext;

    invoke-static {v0}, Lcom/google/android/gms/games/provider/aq;->a(Lcom/google/android/gms/common/server/ClientContext;)Landroid/net/Uri;

    move-result-object v0

    invoke-direct {v3, v0}, Lcom/google/android/gms/common/e/b;-><init>(Landroid/net/Uri;)V

    move v0, v1

    .line 536
    :goto_0
    array-length v2, p2

    if-ge v0, v2, :cond_1

    .line 537
    const-string v2, "external_player_id"

    aget-object v4, p2, v0

    const-string v5, "=?"

    iget-object v6, v3, Lcom/google/android/gms/common/e/b;->b:Ljava/lang/StringBuilder;

    if-nez v6, :cond_0

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "("

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iput-object v6, v3, Lcom/google/android/gms/common/e/b;->b:Ljava/lang/StringBuilder;

    :goto_1
    iget-object v6, v3, Lcom/google/android/gms/common/e/b;->b:Ljava/lang/StringBuilder;

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v5, ")"

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, v3, Lcom/google/android/gms/common/e/b;->c:[Ljava/lang/String;

    invoke-static {v2, v4}, Lcom/google/android/gms/common/e/b;->a([Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    iput-object v2, v3, Lcom/google/android/gms/common/e/b;->c:[Ljava/lang/String;

    .line 536
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 537
    :cond_0
    iget-object v6, v3, Lcom/google/android/gms/common/e/b;->b:Ljava/lang/StringBuilder;

    const-string v7, " OR ("

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 539
    :cond_1
    iget-object v0, p1, Lcom/google/android/gms/games/a/au;->a:Landroid/content/Context;

    iget-object v2, v3, Lcom/google/android/gms/common/e/b;->a:Landroid/net/Uri;

    const-string v4, "external_player_id"

    invoke-static {p2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v5

    invoke-static {v0, v2, v4, v5}, Lcom/google/android/gms/games/a/l;->a(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;Ljava/util/Collection;)Ljava/util/HashMap;

    move-result-object v4

    move v2, v1

    .line 541
    :goto_2
    array-length v0, p2

    if-ge v2, v0, :cond_6

    .line 542
    aget-object v5, p2, v2

    .line 543
    invoke-virtual {v4, v5}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {v4, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    const-wide/16 v8, 0x0

    cmp-long v0, v6, v8

    if-gtz v0, :cond_5

    .line 545
    :cond_2
    invoke-virtual {p1}, Lcom/google/android/gms/games/a/au;->c()Lcom/google/android/gms/games/a/av;

    move-result-object v0

    iput-boolean v10, v0, Lcom/google/android/gms/games/a/av;->g:Z

    iput-object v5, v0, Lcom/google/android/gms/games/a/av;->f:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/google/android/gms/games/a/av;->a()Lcom/google/android/gms/games/a/au;

    move-result-object v0

    .line 552
    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/games/a/bj;->b(Lcom/google/android/gms/games/a/au;Z)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v5

    .line 554
    :try_start_0
    invoke-virtual {v5}, Lcom/google/android/gms/common/data/DataHolder;->f()I

    move-result v0

    if-eqz v0, :cond_4

    .line 555
    const/4 v0, 0x4

    invoke-static {v0}, Lcom/google/android/gms/common/data/DataHolder;->b(I)Lcom/google/android/gms/common/data/DataHolder;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 558
    invoke-virtual {v5}, Lcom/google/android/gms/common/data/DataHolder;->j()V

    .line 572
    :cond_3
    :goto_3
    return-object v0

    .line 558
    :cond_4
    invoke-virtual {v5}, Lcom/google/android/gms/common/data/DataHolder;->j()V

    .line 541
    :cond_5
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_2

    .line 558
    :catchall_0
    move-exception v0

    invoke-virtual {v5}, Lcom/google/android/gms/common/data/DataHolder;->j()V

    throw v0

    .line 563
    :cond_6
    new-instance v0, Lcom/google/android/gms/games/a/o;

    invoke-direct {v0, p1}, Lcom/google/android/gms/games/a/o;-><init>(Lcom/google/android/gms/games/a/au;)V

    iput-object v3, v0, Lcom/google/android/gms/games/a/o;->a:Lcom/google/android/gms/common/e/b;

    invoke-virtual {v0}, Lcom/google/android/gms/games/a/o;->a()Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    .line 568
    invoke-virtual {v0}, Lcom/google/android/gms/common/data/DataHolder;->h()I

    move-result v1

    array-length v2, p2

    if-eq v1, v2, :cond_3

    .line 572
    invoke-static {v10}, Lcom/google/android/gms/common/data/DataHolder;->b(I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    goto :goto_3
.end method

.method final a(Lcom/google/android/gms/games/a/au;Ljava/lang/String;)Lcom/google/android/gms/games/a/bb;
    .locals 3

    .prologue
    .line 647
    const/4 v0, -0x1

    invoke-virtual {p2}, Ljava/lang/String;->hashCode()I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    :cond_0
    :goto_0
    packed-switch v0, :pswitch_data_0

    .line 665
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown player collection type "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 647
    :sswitch_0
    const-string v1, "circled"

    invoke-virtual {p2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    :sswitch_1
    const-string v1, "played_with"

    invoke-virtual {p2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :sswitch_2
    const-string v1, "you_may_know"

    invoke-virtual {p2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x2

    goto :goto_0

    :sswitch_3
    const-string v1, "nearby"

    invoke-virtual {p2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x3

    goto :goto_0

    .line 649
    :pswitch_0
    iget-boolean v0, p1, Lcom/google/android/gms/games/a/au;->h:Z

    if-nez v0, :cond_1

    .line 650
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Fetching recent players from circles is only valid in a 1P context"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 653
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/games/a/bj;->b:Lcom/google/android/gms/games/a/bb;

    .line 663
    :goto_1
    return-object v0

    .line 655
    :pswitch_1
    iget-object v0, p0, Lcom/google/android/gms/games/a/bj;->c:Lcom/google/android/gms/games/a/bb;

    goto :goto_1

    .line 657
    :pswitch_2
    iget-boolean v0, p1, Lcom/google/android/gms/games/a/au;->h:Z

    if-nez v0, :cond_2

    .line 658
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Fetching gamers-you-may-know is only valid in a 1P context"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 661
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/games/a/bj;->d:Lcom/google/android/gms/games/a/bb;

    goto :goto_1

    .line 663
    :pswitch_3
    iget-object v0, p0, Lcom/google/android/gms/games/a/bj;->j:Lcom/google/android/gms/games/a/bb;

    goto :goto_1

    .line 647
    :sswitch_data_0
    .sparse-switch
        -0x3e8dd581 -> :sswitch_3
        -0x8e576bb -> :sswitch_2
        0x9529ab2 -> :sswitch_1
        0x2eaadd94 -> :sswitch_0
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public final a()V
    .locals 1

    .prologue
    .line 509
    iget-object v0, p0, Lcom/google/android/gms/games/a/bj;->L:Lcom/google/android/gms/games/b/p;

    iget-object v0, v0, Lcom/google/android/gms/games/b/u;->a:Landroid/support/v4/g/h;

    invoke-virtual {v0}, Landroid/support/v4/g/h;->b()V

    .line 510
    iget-object v0, p0, Lcom/google/android/gms/games/a/bj;->M:Lcom/google/android/gms/games/b/p;

    iget-object v0, v0, Lcom/google/android/gms/games/b/u;->a:Landroid/support/v4/g/h;

    invoke-virtual {v0}, Landroid/support/v4/g/h;->b()V

    .line 511
    iget-object v0, p0, Lcom/google/android/gms/games/a/bj;->N:Lcom/google/android/gms/games/b/q;

    iget-object v0, v0, Lcom/google/android/gms/games/b/u;->a:Landroid/support/v4/g/h;

    invoke-virtual {v0}, Landroid/support/v4/g/h;->b()V

    .line 512
    return-void
.end method

.method public final b(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;)I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 1954
    invoke-static {}, Lcom/google/android/gms/common/util/r;->c()Lcom/google/android/gms/common/util/p;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/common/util/p;->a()J

    move-result-wide v2

    .line 1955
    iget-wide v4, p0, Lcom/google/android/gms/games/a/bj;->T:J

    sub-long/2addr v2, v4

    sget-object v0, Lcom/google/android/gms/games/c/a;->v:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    cmp-long v0, v2, v4

    if-gtz v0, :cond_0

    .line 1956
    const-string v0, "PlayerAgent"

    const-string v2, "Returning cached entities"

    invoke-static {v0, v2}, Lcom/google/android/gms/games/internal/dq;->c(Ljava/lang/String;Ljava/lang/String;)V

    move v0, v1

    .line 1972
    :goto_0
    return v0

    .line 1960
    :cond_0
    sget-object v0, Lcom/google/android/gms/games/a/bj;->n:[Ljava/lang/String;

    invoke-static {p1, p2, v0}, Lcom/google/android/gms/games/a/l;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1961
    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/gms/games/a/bj;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)Lcom/google/android/gms/games/a/bn;

    move-result-object v0

    .line 1962
    const-string v2, "PlayerAgent"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Received "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, v0, Lcom/google/android/gms/games/a/bn;->a:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " requests during sync"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/gms/games/internal/dq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1963
    iget v2, v0, Lcom/google/android/gms/games/a/bn;->c:I

    if-eqz v2, :cond_1

    .line 1964
    iget v0, v0, Lcom/google/android/gms/games/a/bn;->c:I

    goto :goto_0

    .line 1968
    :cond_1
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iget-object v3, v0, Lcom/google/android/gms/games/a/bn;->b:Ljava/lang/String;

    if-eqz v3, :cond_2

    invoke-static {p2}, Lcom/google/android/gms/games/provider/m;->a(Lcom/google/android/gms/common/server/ClientContext;)Landroid/net/Uri;

    move-result-object v4

    invoke-static {v4}, Landroid/content/ContentProviderOperation;->newUpdate(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v4

    const-string v5, "request_sync_token"

    invoke-virtual {v4, v5, v3}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_2
    iget-object v0, v0, Lcom/google/android/gms/games/a/bn;->a:Ljava/util/ArrayList;

    invoke-direct {p0, p1, p2, v0, v2}, Lcom/google/android/gms/games/a/bj;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/util/ArrayList;Ljava/util/ArrayList;)V

    const/4 v0, 0x1

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-lez v3, :cond_3

    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v3, "PlayerAgent"

    invoke-static {v0, v2, v3}, Lcom/google/android/gms/games/a/l;->a(Landroid/content/ContentResolver;Ljava/util/ArrayList;Ljava/lang/String;)Z

    move-result v0

    :cond_3
    if-nez v0, :cond_4

    const-string v2, "PlayerAgent"

    const-string v3, "Failed to store experiences from sync"

    invoke-static {v2, v3}, Lcom/google/android/gms/games/internal/dq;->e(Ljava/lang/String;Ljava/lang/String;)V

    :cond_4
    invoke-static {p1, p2}, Lcom/google/android/gms/games/a/bj;->d(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;)V

    .line 1969
    if-eqz v0, :cond_5

    .line 1970
    invoke-static {}, Lcom/google/android/gms/common/util/r;->c()Lcom/google/android/gms/common/util/p;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/common/util/p;->a()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/android/gms/games/a/bj;->T:J

    :cond_5
    move v0, v1

    .line 1972
    goto :goto_0
.end method

.method public final b(Lcom/google/android/gms/games/a/au;Ljava/lang/String;)I
    .locals 10

    .prologue
    const/4 v0, 0x0

    .line 851
    iget-object v1, p0, Lcom/google/android/gms/games/a/bj;->j:Lcom/google/android/gms/games/a/bb;

    invoke-virtual {v1}, Lcom/google/android/gms/games/a/bb;->d()V

    .line 854
    invoke-virtual {p1}, Lcom/google/android/gms/games/a/au;->h()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 855
    const-string v1, "PlayerAgent"

    const-string v2, "Found self nearby - ignoring"

    invoke-static {v1, v2}, Lcom/google/android/gms/games/internal/dq;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 902
    :cond_0
    :goto_0
    return v0

    .line 861
    :cond_1
    iget-object v1, p0, Lcom/google/android/gms/games/a/bj;->M:Lcom/google/android/gms/games/b/p;

    iget-object v2, p1, Lcom/google/android/gms/games/a/au;->c:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/google/android/gms/games/b/p;->a(Ljava/lang/Object;)V

    .line 862
    invoke-static {}, Lcom/google/android/gms/common/util/r;->c()Lcom/google/android/gms/common/util/p;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/gms/common/util/p;->a()J

    move-result-wide v4

    .line 863
    iget-object v1, p1, Lcom/google/android/gms/games/a/au;->f:Ljava/lang/String;

    .line 864
    iget-object v2, p1, Lcom/google/android/gms/games/a/au;->e:Ljava/lang/String;

    const-string v3, "nearby"

    invoke-static {v2, v3}, Lcom/google/android/gms/games/b/p;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 866
    iget-object v2, p0, Lcom/google/android/gms/games/a/bj;->M:Lcom/google/android/gms/games/b/p;

    const-string v6, "external_player_id"

    invoke-virtual {v2, v3, v6, v1}, Lcom/google/android/gms/games/b/p;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/google/android/gms/games/a/bj;->M:Lcom/google/android/gms/games/b/p;

    invoke-virtual {v2, v3, v4, v5}, Lcom/google/android/gms/games/b/p;->a(Ljava/lang/Object;J)Z

    move-result v2

    if-nez v2, :cond_0

    .line 874
    :cond_2
    :try_start_0
    invoke-direct {p0, p1, v1}, Lcom/google/android/gms/games/a/bj;->d(Lcom/google/android/gms/games/a/au;Ljava/lang/String;)Lcom/google/android/gms/games/h/a/cs;
    :try_end_0
    .catch Lcom/android/volley/ac; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 884
    const/4 v9, 0x0

    .line 887
    :try_start_1
    iget-object v1, p1, Lcom/google/android/gms/games/a/au;->b:Lcom/google/android/gms/common/server/ClientContext;

    .line 888
    new-instance v2, Lcom/google/android/gms/games/a/bm;

    iget-object v0, v0, Lcom/google/android/gms/common/server/response/a;->a:Landroid/content/ContentValues;

    invoke-direct {v2, v0}, Lcom/google/android/gms/games/a/bm;-><init>(Landroid/content/ContentValues;)V

    .line 889
    iget-object v0, p1, Lcom/google/android/gms/games/a/au;->a:Landroid/content/Context;

    const-wide/32 v6, 0x1b7740

    iget-object v8, p0, Lcom/google/android/gms/games/a/bj;->M:Lcom/google/android/gms/games/b/p;

    invoke-static/range {v0 .. v8}, Lcom/google/android/gms/games/a/bj;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Lcom/google/android/gms/games/a/bm;Ljava/lang/String;JJLcom/google/android/gms/games/b/p;)Lcom/google/android/gms/common/data/DataHolder;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v1

    .line 892
    :try_start_2
    invoke-virtual {v1}, Lcom/google/android/gms/common/data/DataHolder;->f()I

    move-result v0

    .line 893
    invoke-virtual {v1}, Lcom/google/android/gms/common/data/DataHolder;->h()I

    move-result v2

    iget-object v3, p1, Lcom/google/android/gms/games/a/au;->b:Lcom/google/android/gms/common/server/ClientContext;

    invoke-virtual {v3}, Lcom/google/android/gms/common/server/ClientContext;->c()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p1, Lcom/google/android/gms/games/a/au;->a:Landroid/content/Context;

    iget-object v5, p1, Lcom/google/android/gms/games/a/au;->e:Ljava/lang/String;

    invoke-static {v4, p2, v5, v3, v2}, Lcom/google/android/gms/games/f/a;->b(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 895
    if-eqz v1, :cond_3

    .line 896
    invoke-virtual {v1}, Lcom/google/android/gms/common/data/DataHolder;->j()V

    .line 901
    :cond_3
    iget-object v1, p1, Lcom/google/android/gms/games/a/au;->a:Landroid/content/Context;

    sget-object v2, Lcom/google/android/gms/games/internal/c/a;->b:Landroid/net/Uri;

    invoke-static {v1, v2}, Lcom/google/android/gms/games/a/l;->c(Landroid/content/Context;Landroid/net/Uri;)V

    goto :goto_0

    .line 875
    :catch_0
    move-exception v0

    .line 876
    const-string v2, "PlayerAgent"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Unable to load nearby player "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Lcom/google/android/gms/games/internal/dq;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 877
    invoke-static {}, Lcom/google/android/gms/games/internal/dq;->a()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 878
    const-string v1, "PlayerAgent"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/server/b/c;->a(Lcom/android/volley/ac;Ljava/lang/String;)Lcom/google/android/gms/common/server/b/b;

    .line 880
    :cond_4
    const/4 v0, 0x6

    goto/16 :goto_0

    .line 895
    :catchall_0
    move-exception v0

    move-object v1, v9

    :goto_1
    if-eqz v1, :cond_5

    .line 896
    invoke-virtual {v1}, Lcom/google/android/gms/common/data/DataHolder;->j()V

    :cond_5
    throw v0

    .line 895
    :catchall_1
    move-exception v0

    goto :goto_1
.end method

.method public final b(Lcom/google/android/gms/games/a/au;)Lcom/google/android/gms/common/data/DataHolder;
    .locals 1

    .prologue
    .line 585
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lcom/google/android/gms/games/a/bj;->b(Lcom/google/android/gms/games/a/au;Z)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    return-object v0
.end method

.method public final b(Lcom/google/android/gms/games/a/au;I)Lcom/google/android/gms/common/data/DataHolder;
    .locals 13

    .prologue
    .line 1116
    iget-object v0, p0, Lcom/google/android/gms/games/a/bj;->h:Lcom/google/android/gms/games/a/bb;

    invoke-virtual {v0}, Lcom/google/android/gms/games/a/bb;->d()V

    .line 1117
    invoke-static {}, Lcom/google/android/gms/common/util/r;->c()Lcom/google/android/gms/common/util/p;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/common/util/p;->a()J

    move-result-wide v2

    .line 1118
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "connected"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p1, Lcom/google/android/gms/games/a/au;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1120
    iget-object v0, p0, Lcom/google/android/gms/games/a/bj;->L:Lcom/google/android/gms/games/b/p;

    iget-object v4, p1, Lcom/google/android/gms/games/a/au;->c:Ljava/lang/String;

    invoke-virtual {v0, v4}, Lcom/google/android/gms/games/b/p;->a(Ljava/lang/Object;)V

    .line 1123
    iget-boolean v0, p1, Lcom/google/android/gms/games/a/au;->g:Z

    if-eqz v0, :cond_0

    .line 1124
    iget-object v0, p0, Lcom/google/android/gms/games/a/bj;->L:Lcom/google/android/gms/games/b/p;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/b/p;->c(Ljava/lang/Object;)V

    .line 1128
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/games/a/bj;->L:Lcom/google/android/gms/games/b/p;

    const/16 v4, 0x32

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/games/b/p;->a(Ljava/lang/Object;JIZ)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1130
    iget-object v0, p0, Lcom/google/android/gms/games/a/bj;->L:Lcom/google/android/gms/games/b/p;

    const/4 v2, 0x0

    const/4 v3, -0x1

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/gms/games/b/u;->a(Ljava/lang/Object;Landroid/os/Bundle;I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    .line 1182
    :goto_0
    return-object v0

    .line 1133
    :cond_1
    const/4 v12, 0x0

    .line 1135
    :try_start_0
    iget-boolean v0, p1, Lcom/google/android/gms/games/a/au;->h:Z

    if-eqz v0, :cond_3

    .line 1136
    const/4 v0, 0x0

    .line 1139
    packed-switch p2, :pswitch_data_0

    .line 1151
    :goto_1
    iget-object v4, p0, Lcom/google/android/gms/games/a/bj;->H:Lcom/google/android/gms/plus/service/v1whitelisted/i;

    iget-object v5, p1, Lcom/google/android/gms/games/a/au;->b:Lcom/google/android/gms/common/server/ClientContext;

    iget-object v6, p1, Lcom/google/android/gms/games/a/au;->e:Ljava/lang/String;

    invoke-static {v6}, Lcom/google/android/gms/plus/j;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    const-string v7, "connected"

    const-string v8, "applications/%1$s/people/%2$s"

    const/4 v9, 0x2

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    invoke-static {v6}, Lcom/google/android/gms/plus/service/v1whitelisted/i;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v9, v10

    const/4 v6, 0x1

    invoke-static {v7}, Lcom/google/android/gms/plus/service/v1whitelisted/i;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v9, v6

    invoke-static {v8, v9}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    const-string v7, "sharedEndorsementsContext"

    invoke-static {v0}, Lcom/google/android/gms/plus/service/v1whitelisted/i;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v6, v7, v0}, Lcom/google/android/gms/plus/service/v1whitelisted/i;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    iget-object v4, v4, Lcom/google/android/gms/plus/service/v1whitelisted/i;->a:Lcom/google/android/gms/common/server/n;

    const/4 v6, 0x0

    const/4 v8, 0x0

    const-class v9, Lcom/google/android/gms/plus/service/v1whitelisted/models/PeopleFeed;

    invoke-virtual/range {v4 .. v9}, Lcom/google/android/gms/common/server/n;->a(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;Ljava/lang/Object;Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse;

    move-result-object v7

    check-cast v7, Lcom/google/android/gms/plus/service/v1whitelisted/models/PeopleFeed;

    .line 1155
    iget-object v6, p1, Lcom/google/android/gms/games/a/au;->a:Landroid/content/Context;

    const/4 v10, 0x1

    move-object v5, p0

    move-wide v8, v2

    invoke-direct/range {v5 .. v10}, Lcom/google/android/gms/games/a/bj;->a(Landroid/content/Context;Lcom/google/android/gms/plus/service/v1whitelisted/models/PeopleFeed;JI)Lcom/google/android/gms/games/a/bm;
    :try_end_0
    .catch Lcom/android/volley/ac; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v6

    .line 1173
    :goto_2
    if-nez v6, :cond_5

    .line 1174
    iget-object v0, p0, Lcom/google/android/gms/games/a/bj;->L:Lcom/google/android/gms/games/b/p;

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/gms/games/b/p;->a(Ljava/lang/Object;J)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1176
    iget-object v0, p0, Lcom/google/android/gms/games/a/bj;->L:Lcom/google/android/gms/games/b/p;

    const/4 v2, 0x3

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/b/p;->a(Ljava/lang/Object;I)V

    .line 1178
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/games/a/bj;->L:Lcom/google/android/gms/games/b/p;

    const/4 v2, 0x0

    const/4 v3, -0x1

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/gms/games/b/u;->a(Ljava/lang/Object;Landroid/os/Bundle;I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    goto :goto_0

    .line 1141
    :pswitch_0
    :try_start_1
    const-string v0, "none"

    goto :goto_1

    .line 1144
    :pswitch_1
    const-string v0, "nonAdsCommercial"

    goto :goto_1

    .line 1147
    :pswitch_2
    const-string v0, "ads"

    goto :goto_1

    .line 1158
    :cond_3
    iget-object v4, p0, Lcom/google/android/gms/games/a/bj;->G:Lcom/google/android/gms/plus/service/v1/c;

    iget-object v5, p1, Lcom/google/android/gms/games/a/au;->b:Lcom/google/android/gms/common/server/ClientContext;

    const-string v6, "me"

    const-string v7, "connected"

    sget-object v8, Lcom/google/android/gms/games/a/bj;->y:Ljava/util/ArrayList;

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    invoke-virtual/range {v4 .. v11}, Lcom/google/android/gms/plus/service/v1/c;->a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/plus/service/v1/PeopleFeed;

    move-result-object v0

    .line 1163
    iget-object v4, p1, Lcom/google/android/gms/games/a/au;->a:Landroid/content/Context;

    const/4 v5, 0x1

    invoke-static {v4, v0, v2, v3, v5}, Lcom/google/android/gms/games/a/bj;->a(Landroid/content/Context;Lcom/google/android/gms/plus/service/v1/PeopleFeed;JI)Lcom/google/android/gms/games/a/bm;
    :try_end_1
    .catch Lcom/android/volley/ac; {:try_start_1 .. :try_end_1} :catch_0

    move-result-object v6

    goto :goto_2

    .line 1166
    :catch_0
    move-exception v0

    .line 1167
    invoke-static {}, Lcom/google/android/gms/games/internal/dq;->a()Z

    move-result v4

    if-eqz v4, :cond_4

    .line 1168
    const-string v4, "PlayerAgent"

    invoke-static {v0, v4}, Lcom/google/android/gms/common/server/b/c;->a(Lcom/android/volley/ac;Ljava/lang/String;)Lcom/google/android/gms/common/server/b/b;

    :cond_4
    move-object v6, v12

    goto :goto_2

    .line 1182
    :cond_5
    iget-object v4, p1, Lcom/google/android/gms/games/a/au;->a:Landroid/content/Context;

    iget-object v5, p1, Lcom/google/android/gms/games/a/au;->b:Lcom/google/android/gms/common/server/ClientContext;

    const-wide/32 v10, 0x1b7740

    iget-object v12, p0, Lcom/google/android/gms/games/a/bj;->L:Lcom/google/android/gms/games/b/p;

    move-object v7, v1

    move-wide v8, v2

    invoke-static/range {v4 .. v12}, Lcom/google/android/gms/games/a/bj;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Lcom/google/android/gms/games/a/bm;Ljava/lang/String;JJLcom/google/android/gms/games/b/p;)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    goto/16 :goto_0

    .line 1139
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final b(Lcom/google/android/gms/games/a/au;IZ)Lcom/google/android/gms/common/data/DataHolder;
    .locals 6

    .prologue
    .line 794
    iget-boolean v0, p1, Lcom/google/android/gms/games/a/au;->h:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "Calling visible from 1P context!"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/e;->a(ZLjava/lang/Object;)V

    .line 795
    iget-object v0, p0, Lcom/google/android/gms/games/a/bj;->f:Lcom/google/android/gms/games/a/bb;

    invoke-virtual {v0}, Lcom/google/android/gms/games/a/bb;->d()V

    .line 799
    iget-object v0, p1, Lcom/google/android/gms/games/a/au;->b:Lcom/google/android/gms/common/server/ClientContext;

    invoke-static {v0}, Lcom/google/android/gms/games/b/p;->a(Lcom/google/android/gms/common/server/ClientContext;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lcom/google/android/gms/games/a/bj;->c(Lcom/google/android/gms/games/a/au;Ljava/lang/String;)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    .line 801
    if-eqz v0, :cond_1

    .line 805
    :goto_1
    return-object v0

    .line 794
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 805
    :cond_1
    iget-object v0, p1, Lcom/google/android/gms/games/a/au;->b:Lcom/google/android/gms/common/server/ClientContext;

    invoke-static {v0}, Lcom/google/android/gms/games/b/p;->a(Lcom/google/android/gms/common/server/ClientContext;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "visible"

    move-object v0, p0

    move-object v1, p1

    move v4, p2

    move v5, p3

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/games/a/bj;->a(Lcom/google/android/gms/games/a/au;Ljava/lang/String;Ljava/lang/String;IZ)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    goto :goto_1
.end method

.method public final b(Lcom/google/android/gms/games/a/au;Ljava/lang/String;IZ)Lcom/google/android/gms/common/data/DataHolder;
    .locals 6

    .prologue
    .line 831
    iget-boolean v0, p1, Lcom/google/android/gms/games/a/au;->h:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "Calling getPlayedWithPlayers from 1P context!"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/e;->a(ZLjava/lang/Object;)V

    .line 833
    invoke-virtual {p0, p1, p2}, Lcom/google/android/gms/games/a/bj;->a(Lcom/google/android/gms/games/a/au;Ljava/lang/String;)Lcom/google/android/gms/games/a/bb;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/games/a/bb;->d()V

    .line 834
    const-string v0, "played_with"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    invoke-static {v0}, Lcom/google/android/gms/common/internal/e;->a(Z)V

    .line 835
    iget-object v0, p1, Lcom/google/android/gms/games/a/au;->b:Lcom/google/android/gms/common/server/ClientContext;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "players:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const/16 v2, 0x3a

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Lcom/google/android/gms/common/server/ClientContext;->d()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    move-object v0, p0

    move-object v1, p1

    move-object v3, p2

    move v4, p3

    move v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/games/a/bj;->a(Lcom/google/android/gms/games/a/au;Ljava/lang/String;Ljava/lang/String;IZ)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    return-object v0

    .line 831
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 1334
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/google/android/gms/games/a/bj;->k:[Lcom/google/android/gms/games/a/bb;

    array-length v1, v1

    if-ge v0, v1, :cond_0

    .line 1335
    iget-object v1, p0, Lcom/google/android/gms/games/a/bj;->k:[Lcom/google/android/gms/games/a/bb;

    aget-object v1, v1, v0

    invoke-virtual {v1}, Lcom/google/android/gms/games/a/bb;->d()V

    .line 1334
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1337
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/games/a/bj;->L:Lcom/google/android/gms/games/b/p;

    iget-object v0, v0, Lcom/google/android/gms/games/b/u;->a:Landroid/support/v4/g/h;

    invoke-virtual {v0}, Landroid/support/v4/g/h;->b()V

    .line 1339
    return-void
.end method

.method public final c(Lcom/google/android/gms/games/a/au;)I
    .locals 11

    .prologue
    const/4 v3, 0x1

    const/4 v7, 0x0

    .line 1078
    iget-object v2, p1, Lcom/google/android/gms/games/a/au;->f:Ljava/lang/String;

    .line 1079
    iget-object v0, p0, Lcom/google/android/gms/games/a/bj;->g:Lcom/google/android/gms/games/a/bb;

    invoke-virtual {v0}, Lcom/google/android/gms/games/a/bb;->d()V

    .line 1080
    iget-object v0, p0, Lcom/google/android/gms/games/a/bj;->H:Lcom/google/android/gms/plus/service/v1whitelisted/i;

    iget-object v1, p1, Lcom/google/android/gms/games/a/au;->b:Lcom/google/android/gms/common/server/ClientContext;

    iget-object v4, p0, Lcom/google/android/gms/games/a/bj;->K:Lcom/google/android/gms/common/server/w;

    iget-object v5, p0, Lcom/google/android/gms/games/a/bj;->K:Lcom/google/android/gms/common/server/w;

    const-string v6, "people/%1$s/rejectSuggestion"

    new-array v8, v3, [Ljava/lang/Object;

    invoke-static {v2}, Lcom/google/android/gms/plus/service/v1whitelisted/i;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    aput-object v9, v8, v7

    invoke-static {v6, v8}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    iget-object v0, v0, Lcom/google/android/gms/plus/service/v1whitelisted/i;->a:Lcom/google/android/gms/common/server/n;

    invoke-virtual {v0, v1, v6, v4, v5}, Lcom/google/android/gms/common/server/n;->a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Lcom/android/volley/x;Lcom/android/volley/w;)V

    .line 1084
    iget-object v0, p0, Lcom/google/android/gms/games/a/bj;->O:Ljava/util/HashMap;

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 1087
    iget-object v0, p0, Lcom/google/android/gms/games/a/bj;->L:Lcom/google/android/gms/games/b/p;

    invoke-virtual {v0, v2}, Lcom/google/android/gms/games/b/p;->b(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1088
    iget-object v6, p0, Lcom/google/android/gms/games/a/bj;->L:Lcom/google/android/gms/games/b/p;

    const-string v8, "suggested"

    const-string v9, "external_player_id"

    iget-object v0, v6, Lcom/google/android/gms/games/b/u;->a:Landroid/support/v4/g/h;

    invoke-virtual {v0, v8}, Landroid/support/v4/g/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/b/v;

    if-eqz v0, :cond_2

    iget-object v1, v0, Lcom/google/android/gms/games/b/v;->a:Lcom/google/android/gms/common/data/ai;

    if-eqz v1, :cond_1

    iget-object v1, v0, Lcom/google/android/gms/games/b/v;->a:Lcom/google/android/gms/common/data/ai;

    iget-object v10, v1, Lcom/google/android/gms/common/data/ai;->c:Lcom/google/android/gms/common/data/m;

    iget-object v1, v10, Lcom/google/android/gms/common/data/m;->b:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    move v5, v1

    :goto_0
    if-ltz v5, :cond_1

    iget-object v1, v10, Lcom/google/android/gms/common/data/m;->b:Ljava/util/ArrayList;

    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/HashMap;

    invoke-virtual {v1, v9}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-static {v1, v2}, Lcom/google/android/gms/common/internal/bu;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, v10, Lcom/google/android/gms/common/data/m;->b:Ljava/util/ArrayList;

    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    :cond_0
    add-int/lit8 v1, v5, -0x1

    move v5, v1

    goto :goto_0

    :cond_1
    iget-boolean v1, v6, Lcom/google/android/gms/games/b/u;->c:Z

    if-eqz v1, :cond_2

    invoke-virtual {v8}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v6, v1, v0}, Lcom/google/android/gms/games/b/u;->a(Ljava/lang/String;Lcom/google/android/gms/games/b/v;)V

    .line 1092
    :cond_2
    if-eqz v4, :cond_3

    .line 1094
    iget-object v0, p0, Lcom/google/android/gms/games/a/bj;->H:Lcom/google/android/gms/plus/service/v1whitelisted/i;

    iget-object v1, p1, Lcom/google/android/gms/games/a/au;->b:Lcom/google/android/gms/common/server/ClientContext;

    const/4 v3, 0x2

    invoke-static {v3}, Lcom/google/android/gms/games/internal/b/h;->a(I)Ljava/lang/String;

    move-result-object v3

    iget-object v5, p0, Lcom/google/android/gms/games/a/bj;->K:Lcom/google/android/gms/common/server/w;

    iget-object v6, p0, Lcom/google/android/gms/games/a/bj;->K:Lcom/google/android/gms/common/server/w;

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/gms/plus/service/v1whitelisted/i;->a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/android/volley/x;Lcom/android/volley/w;)V

    move v0, v7

    .line 1102
    :goto_1
    return v0

    .line 1098
    :cond_3
    const-string v0, "PlayerAgent"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v4, "Unable to record rejection action for player; could not find suggestion ID for player "

    invoke-direct {v1, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    move v0, v3

    .line 1100
    goto :goto_1
.end method

.method public final c(Lcom/google/android/gms/games/a/au;I)Lcom/google/android/gms/common/data/DataHolder;
    .locals 13

    .prologue
    const/4 v9, 0x4

    const/4 v8, 0x1

    const/4 v10, 0x0

    const/4 v12, -0x1

    const/4 v11, 0x0

    .line 1700
    invoke-static {}, Lcom/google/android/gms/common/util/r;->c()Lcom/google/android/gms/common/util/p;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/common/util/p;->a()J

    move-result-wide v2

    iget-object v4, p1, Lcom/google/android/gms/games/a/au;->f:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/gms/games/a/bj;->S:Lcom/google/android/gms/games/b/w;

    iget-object v1, p1, Lcom/google/android/gms/games/a/au;->b:Lcom/google/android/gms/common/server/ClientContext;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/b/w;->b(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/google/android/gms/games/a/bj;->S:Lcom/google/android/gms/games/b/w;

    invoke-virtual {v0, v4, v2, v3}, Lcom/google/android/gms/games/b/w;->a(Ljava/lang/Object;J)Z

    move-result v5

    iget-object v0, p0, Lcom/google/android/gms/games/a/bj;->S:Lcom/google/android/gms/games/b/w;

    iget-object v0, v0, Lcom/google/android/gms/games/b/u;->a:Landroid/support/v4/g/h;

    invoke-virtual {v0, v4}, Landroid/support/v4/g/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/b/v;

    if-nez v0, :cond_1

    move v1, v9

    :goto_0
    if-nez v5, :cond_3

    iget-object v0, p0, Lcom/google/android/gms/games/a/bj;->S:Lcom/google/android/gms/games/b/w;

    iget-object v0, v0, Lcom/google/android/gms/games/b/u;->a:Landroid/support/v4/g/h;

    invoke-virtual {v0, v4}, Landroid/support/v4/g/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/b/v;

    if-nez v0, :cond_2

    move v0, v10

    :goto_1
    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/gms/games/a/bj;->S:Lcom/google/android/gms/games/b/w;

    const/4 v1, 0x3

    invoke-virtual {v0, v4, v1}, Lcom/google/android/gms/games/b/w;->a(Ljava/lang/Object;I)V

    iget-object v0, p0, Lcom/google/android/gms/games/a/bj;->S:Lcom/google/android/gms/games/b/w;

    invoke-virtual {v0, v4, v11, v12}, Lcom/google/android/gms/games/b/u;->a(Ljava/lang/Object;Landroid/os/Bundle;I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    move-object v7, v0

    .line 1701
    :goto_2
    invoke-virtual {v7}, Lcom/google/android/gms/common/data/DataHolder;->f()I

    move-result v0

    if-nez v0, :cond_7

    .line 1734
    :cond_0
    :goto_3
    return-object v7

    .line 1700
    :cond_1
    iget v0, v0, Lcom/google/android/gms/games/b/v;->b:I

    move v1, v0

    goto :goto_0

    :cond_2
    iget v0, v0, Lcom/google/android/gms/games/b/v;->d:I

    goto :goto_1

    :cond_3
    if-eqz v5, :cond_4

    if-nez v1, :cond_4

    iget-object v0, p0, Lcom/google/android/gms/games/a/bj;->S:Lcom/google/android/gms/games/b/w;

    invoke-virtual {v0, v4, v11, v12}, Lcom/google/android/gms/games/b/u;->a(Ljava/lang/Object;Landroid/os/Bundle;I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    move-object v7, v0

    goto :goto_2

    :cond_4
    iget-wide v0, p0, Lcom/google/android/gms/games/a/bj;->l:J

    sub-long/2addr v2, v0

    sget-object v0, Lcom/google/android/gms/games/c/a;->l:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    cmp-long v0, v2, v0

    if-gez v0, :cond_6

    iget-object v0, p1, Lcom/google/android/gms/games/a/au;->b:Lcom/google/android/gms/common/server/ClientContext;

    invoke-static {v0}, Lcom/google/android/gms/games/provider/x;->a(Lcom/google/android/gms/common/server/ClientContext;)Landroid/net/Uri;

    move-result-object v0

    new-instance v1, Lcom/google/android/gms/games/a/o;

    invoke-direct {v1, p1}, Lcom/google/android/gms/games/a/o;-><init>(Lcom/google/android/gms/games/a/au;)V

    invoke-virtual {v1, v0}, Lcom/google/android/gms/games/a/o;->a(Landroid/net/Uri;)Lcom/google/android/gms/games/a/o;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/games/a/o;->a()Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/common/data/DataHolder;->h()I

    move-result v1

    if-lez v1, :cond_5

    move-object v7, v0

    goto :goto_2

    :cond_5
    invoke-virtual {v0}, Lcom/google/android/gms/common/data/DataHolder;->j()V

    :cond_6
    const/4 v0, 0x6

    invoke-static {v0}, Lcom/google/android/gms/common/data/DataHolder;->b(I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    move-object v7, v0

    goto :goto_2

    .line 1705
    :cond_7
    iget-object v2, p1, Lcom/google/android/gms/games/a/au;->f:Ljava/lang/String;

    .line 1710
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/games/a/bj;->I:Lcom/google/android/gms/games/h/a/ci;

    iget-object v1, p1, Lcom/google/android/gms/games/a/au;->b:Lcom/google/android/gms/common/server/ClientContext;

    const-string v3, "experience_points"

    iget-object v4, p1, Lcom/google/android/gms/games/a/au;->a:Landroid/content/Context;

    invoke-static {v4}, Lcom/google/android/gms/games/a/l;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v4

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    const/4 v6, 0x0

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/gms/games/h/a/ci;->a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/String;)Lcom/google/android/gms/games/h/a/bd;
    :try_end_0
    .catch Lcom/android/volley/ac; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v4

    .line 1721
    iget-object v0, p1, Lcom/google/android/gms/games/a/au;->a:Landroid/content/Context;

    iget-object v1, p1, Lcom/google/android/gms/games/a/au;->b:Lcom/google/android/gms/common/server/ClientContext;

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {v4}, Lcom/google/android/gms/games/h/a/bd;->getItems()Ljava/util/ArrayList;

    move-result-object v5

    invoke-direct {p0, v0, v1, v5, v3}, Lcom/google/android/gms/games/a/bj;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/util/ArrayList;Ljava/util/ArrayList;)V

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lez v1, :cond_9

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "PlayerAgent"

    invoke-static {v0, v3, v1}, Lcom/google/android/gms/games/a/l;->a(Landroid/content/ContentResolver;Ljava/util/ArrayList;Ljava/lang/String;)Z

    move-result v0

    :goto_4
    if-nez v0, :cond_8

    const-string v0, "PlayerAgent"

    const-string v1, "Failed to store experiences from load"

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1724
    :cond_8
    iget-object v0, p0, Lcom/google/android/gms/games/a/bj;->S:Lcom/google/android/gms/games/b/w;

    iget-object v1, p1, Lcom/google/android/gms/games/a/au;->b:Lcom/google/android/gms/common/server/ClientContext;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/b/w;->a(Ljava/lang/Object;)V

    .line 1725
    iget-object v0, p0, Lcom/google/android/gms/games/a/bj;->S:Lcom/google/android/gms/games/b/w;

    iget-object v0, v0, Lcom/google/android/gms/games/b/u;->a:Landroid/support/v4/g/h;

    invoke-virtual {v0}, Landroid/support/v4/g/h;->b()V

    .line 1728
    invoke-virtual {v4}, Lcom/google/android/gms/games/h/a/bd;->getItems()Ljava/util/ArrayList;

    move-result-object v0

    .line 1729
    iget-object v1, p1, Lcom/google/android/gms/games/a/au;->a:Landroid/content/Context;

    iget-object v3, p1, Lcom/google/android/gms/games/a/au;->b:Lcom/google/android/gms/common/server/ClientContext;

    invoke-static {v1, v3, v0, v8}, Lcom/google/android/gms/games/a/bj;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/util/ArrayList;Z)Ljava/util/ArrayList;

    move-result-object v3

    .line 1731
    iget-object v1, p0, Lcom/google/android/gms/games/a/bj;->S:Lcom/google/android/gms/games/b/w;

    invoke-virtual {v4}, Lcom/google/android/gms/games/h/a/bd;->b()Ljava/lang/String;

    move-result-object v6

    invoke-static {}, Lcom/google/android/gms/common/util/r;->c()Lcom/google/android/gms/common/util/p;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/common/util/p;->a()J

    move-result-wide v8

    move v4, v10

    move-object v5, v11

    move v7, v10

    invoke-virtual/range {v1 .. v9}, Lcom/google/android/gms/games/b/w;->a(Ljava/lang/Object;Ljava/util/ArrayList;ILjava/lang/String;Ljava/lang/String;IJ)V

    .line 1734
    iget-object v0, p0, Lcom/google/android/gms/games/a/bj;->S:Lcom/google/android/gms/games/b/w;

    invoke-virtual {v0, v2, v11, v12}, Lcom/google/android/gms/games/b/u;->a(Ljava/lang/Object;Landroid/os/Bundle;I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v7

    goto/16 :goto_3

    .line 1713
    :catch_0
    move-exception v0

    const-string v1, "PlayerAgent"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/server/b/c;->a(Lcom/android/volley/ac;Ljava/lang/String;)Lcom/google/android/gms/common/server/b/b;

    .line 1715
    invoke-virtual {v7}, Lcom/google/android/gms/common/data/DataHolder;->h()I

    move-result v0

    if-nez v0, :cond_0

    .line 1716
    invoke-static {v9}, Lcom/google/android/gms/common/data/DataHolder;->b(I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v7

    goto/16 :goto_3

    :cond_9
    move v0, v8

    goto :goto_4
.end method

.method public final c(Lcom/google/android/gms/games/a/au;IZ)Lcom/google/android/gms/common/data/DataHolder;
    .locals 6

    .prologue
    .line 1038
    iget-object v0, p0, Lcom/google/android/gms/games/a/bj;->g:Lcom/google/android/gms/games/a/bb;

    invoke-virtual {v0}, Lcom/google/android/gms/games/a/bb;->d()V

    .line 1039
    const-string v2, "suggested"

    const-string v3, "suggestions"

    move-object v0, p0

    move-object v1, p1

    move v4, p2

    move v5, p3

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/games/a/bj;->a(Lcom/google/android/gms/games/a/au;Ljava/lang/String;Ljava/lang/String;IZ)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    return-object v0
.end method

.method public final c(Lcom/google/android/gms/games/a/au;Ljava/lang/String;IZ)Lcom/google/android/gms/common/data/DataHolder;
    .locals 17

    .prologue
    .line 1204
    move-object/from16 v0, p1

    iget-object v14, v0, Lcom/google/android/gms/games/a/au;->a:Landroid/content/Context;

    .line 1205
    move-object/from16 v0, p1

    iget-object v8, v0, Lcom/google/android/gms/games/a/au;->b:Lcom/google/android/gms/common/server/ClientContext;

    .line 1206
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/games/a/bj;->i:Lcom/google/android/gms/games/a/bb;

    invoke-virtual {v2}, Lcom/google/android/gms/games/a/bb;->d()V

    .line 1207
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/games/a/bj;->N:Lcom/google/android/gms/games/b/q;

    move-object/from16 v0, p1

    iget-object v3, v0, Lcom/google/android/gms/games/a/au;->c:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lcom/google/android/gms/games/b/q;->a(Ljava/lang/Object;)V

    .line 1208
    invoke-static {}, Lcom/google/android/gms/common/util/r;->c()Lcom/google/android/gms/common/util/p;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/gms/common/util/p;->a()J

    move-result-wide v4

    .line 1211
    move-object/from16 v0, p1

    iget-boolean v2, v0, Lcom/google/android/gms/games/a/au;->g:Z

    if-eqz v2, :cond_0

    .line 1212
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/games/a/bj;->N:Lcom/google/android/gms/games/b/q;

    move-object/from16 v0, p2

    invoke-virtual {v2, v0}, Lcom/google/android/gms/games/b/q;->c(Ljava/lang/Object;)V

    .line 1216
    :cond_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/games/a/bj;->N:Lcom/google/android/gms/games/b/q;

    move-object/from16 v3, p2

    move/from16 v6, p3

    move/from16 v7, p4

    invoke-virtual/range {v2 .. v7}, Lcom/google/android/gms/games/b/q;->a(Ljava/lang/Object;JIZ)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1217
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/games/a/bj;->N:Lcom/google/android/gms/games/b/q;

    const/4 v3, 0x0

    const/4 v4, -0x1

    move-object/from16 v0, p2

    invoke-virtual {v2, v0, v3, v4}, Lcom/google/android/gms/games/b/u;->a(Ljava/lang/Object;Landroid/os/Bundle;I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v2

    .line 1273
    :goto_0
    return-object v2

    .line 1221
    :cond_1
    const/4 v13, 0x0

    .line 1222
    if-nez p4, :cond_2

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/games/a/bj;->N:Lcom/google/android/gms/games/b/q;

    move-object/from16 v0, p2

    invoke-virtual {v2, v0, v4, v5}, Lcom/google/android/gms/games/b/q;->a(Ljava/lang/Object;J)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 1223
    :cond_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/games/a/bj;->N:Lcom/google/android/gms/games/b/q;

    move-object/from16 v0, p2

    invoke-virtual {v2, v0, v4, v5}, Lcom/google/android/gms/games/b/q;->b(Ljava/lang/Object;J)Ljava/lang/String;

    move-result-object v13

    .line 1225
    :cond_3
    const/4 v2, 0x0

    sget-object v3, Landroid/util/Patterns;->EMAIL_ADDRESS:Ljava/util/regex/Pattern;

    move-object/from16 v0, p2

    invoke-virtual {v3, v0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/regex/Matcher;->matches()Z

    move-result v3

    if-eqz v3, :cond_4

    move-object/from16 v6, p0

    move-object v7, v14

    move-object/from16 v9, p2

    move-wide v10, v4

    invoke-direct/range {v6 .. v11}, Lcom/google/android/gms/games/a/bj;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;J)Lcom/google/android/gms/games/a/bm;

    move-result-object v2

    :cond_4
    if-nez v2, :cond_b

    move-object/from16 v6, p0

    move-object v7, v14

    move-object/from16 v9, p2

    move/from16 v10, p3

    move-wide v11, v4

    invoke-direct/range {v6 .. v13}, Lcom/google/android/gms/games/a/bj;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;IJLjava/lang/String;)Lcom/google/android/gms/games/a/bm;

    move-result-object v2

    move-object/from16 v16, v2

    .line 1229
    :goto_1
    if-nez v16, :cond_6

    .line 1230
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/games/a/bj;->N:Lcom/google/android/gms/games/b/q;

    move-object/from16 v0, p2

    invoke-virtual {v2, v0, v4, v5}, Lcom/google/android/gms/games/b/q;->a(Ljava/lang/Object;J)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 1232
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/games/a/bj;->N:Lcom/google/android/gms/games/b/q;

    const/4 v3, 0x3

    move-object/from16 v0, p2

    invoke-virtual {v2, v0, v3}, Lcom/google/android/gms/games/b/q;->a(Ljava/lang/Object;I)V

    .line 1234
    :cond_5
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/games/a/bj;->N:Lcom/google/android/gms/games/b/q;

    const/4 v3, 0x0

    const/4 v4, -0x1

    move-object/from16 v0, p2

    invoke-virtual {v2, v0, v3, v4}, Lcom/google/android/gms/games/b/u;->a(Ljava/lang/Object;Landroid/os/Bundle;I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v2

    goto :goto_0

    .line 1238
    :cond_6
    move-object/from16 v0, v16

    iget-object v2, v0, Lcom/google/android/gms/games/a/bm;->a:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v7

    .line 1239
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6, v7}, Ljava/util/ArrayList;-><init>(I)V

    .line 1241
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9, v7}, Ljava/util/ArrayList;-><init>(I)V

    .line 1242
    new-instance v10, Ljava/util/ArrayList;

    invoke-direct {v10, v7}, Ljava/util/ArrayList;-><init>(I)V

    .line 1243
    invoke-static {v8}, Lcom/google/android/gms/games/provider/ad;->a(Lcom/google/android/gms/common/server/ClientContext;)Landroid/net/Uri;

    move-result-object v8

    .line 1244
    const/4 v2, 0x0

    move v3, v2

    :goto_2
    if-ge v3, v7, :cond_7

    .line 1245
    move-object/from16 v0, v16

    iget-object v2, v0, Lcom/google/android/gms/games/a/bm;->a:Ljava/util/ArrayList;

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/content/ContentValues;

    .line 1246
    const-string v11, "profile_icon_image_url"

    invoke-virtual {v2, v11}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    .line 1247
    const-string v12, "profile_hi_res_image_url"

    invoke-virtual {v2, v12}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1248
    invoke-static {v8, v11, v6}, Lcom/google/android/gms/games/a/l;->a(Landroid/net/Uri;Ljava/lang/String;Ljava/util/ArrayList;)Ljava/lang/Integer;

    move-result-object v11

    invoke-virtual {v9, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1249
    invoke-static {v8, v2, v6}, Lcom/google/android/gms/games/a/l;->a(Landroid/net/Uri;Ljava/lang/String;Ljava/util/ArrayList;)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v10, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1244
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_2

    .line 1251
    :cond_7
    invoke-virtual {v9}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ne v2, v7, :cond_8

    const/4 v2, 0x1

    :goto_3
    invoke-static {v2}, Lcom/google/android/gms/common/internal/e;->a(Z)V

    .line 1252
    invoke-virtual {v10}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ne v2, v7, :cond_9

    const/4 v2, 0x1

    :goto_4
    invoke-static {v2}, Lcom/google/android/gms/common/internal/e;->a(Z)V

    .line 1257
    invoke-virtual {v14}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "PlayerAgent"

    invoke-static {v2, v6, v3}, Lcom/google/android/gms/games/a/l;->b(Landroid/content/ContentResolver;Ljava/util/ArrayList;Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v8

    .line 1259
    const/4 v2, 0x0

    move v6, v2

    :goto_5
    if-ge v6, v7, :cond_a

    .line 1260
    move-object/from16 v0, v16

    iget-object v2, v0, Lcom/google/android/gms/games/a/bm;->a:Ljava/util/ArrayList;

    invoke-virtual {v2, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/content/ContentValues;

    .line 1261
    const-string v11, "profile_icon_image_url"

    const-string v12, "profile_icon_image_uri"

    invoke-virtual {v9, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-static {v2, v11, v12, v8, v3}, Lcom/google/android/gms/games/a/l;->b(Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;Ljava/lang/Integer;)V

    .line 1263
    const-string v11, "profile_hi_res_image_url"

    const-string v12, "profile_hi_res_image_uri"

    invoke-virtual {v10, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-static {v2, v11, v12, v8, v3}, Lcom/google/android/gms/games/a/l;->b(Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;Ljava/lang/Integer;)V

    .line 1259
    add-int/lit8 v2, v6, 0x1

    move v6, v2

    goto :goto_5

    .line 1251
    :cond_8
    const/4 v2, 0x0

    goto :goto_3

    .line 1252
    :cond_9
    const/4 v2, 0x0

    goto :goto_4

    .line 1268
    :cond_a
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/gms/games/a/bj;->N:Lcom/google/android/gms/games/b/q;

    move-object/from16 v0, v16

    iget-object v9, v0, Lcom/google/android/gms/games/a/bm;->a:Ljava/util/ArrayList;

    const/4 v10, 0x0

    const/4 v11, 0x0

    move-object/from16 v0, v16

    iget-object v12, v0, Lcom/google/android/gms/games/a/bm;->b:Ljava/lang/String;

    const/4 v13, 0x0

    move-object/from16 v8, p2

    move-wide v14, v4

    invoke-virtual/range {v7 .. v15}, Lcom/google/android/gms/games/b/q;->a(Ljava/lang/Object;Ljava/util/ArrayList;ILjava/lang/String;Ljava/lang/String;IJ)V

    .line 1271
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/games/a/bj;->N:Lcom/google/android/gms/games/b/q;

    const-string v3, "total_count"

    move-object/from16 v0, v16

    iget v4, v0, Lcom/google/android/gms/games/a/bm;->c:I

    move-object/from16 v0, p2

    invoke-virtual {v2, v0, v3, v4}, Lcom/google/android/gms/games/b/q;->a(Ljava/lang/Object;Ljava/lang/String;I)V

    .line 1273
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/games/a/bj;->N:Lcom/google/android/gms/games/b/q;

    const/4 v3, 0x0

    const/4 v4, -0x1

    move-object/from16 v0, p2

    invoke-virtual {v2, v0, v3, v4}, Lcom/google/android/gms/games/b/u;->a(Ljava/lang/Object;Landroid/os/Bundle;I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v2

    goto/16 :goto_0

    :cond_b
    move-object/from16 v16, v2

    goto/16 :goto_1
.end method

.method public final d(Lcom/google/android/gms/games/a/au;I)Lcom/google/android/gms/common/data/DataHolder;
    .locals 13

    .prologue
    const/4 v12, 0x0

    const/4 v11, -0x1

    const/4 v10, 0x0

    .line 1752
    invoke-static {}, Lcom/google/android/gms/common/util/r;->c()Lcom/google/android/gms/common/util/p;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/common/util/p;->a()J

    move-result-wide v0

    .line 1753
    iget-object v2, p1, Lcom/google/android/gms/games/a/au;->f:Ljava/lang/String;

    .line 1754
    iget-object v3, p0, Lcom/google/android/gms/games/a/bj;->S:Lcom/google/android/gms/games/b/w;

    iget-object v4, p1, Lcom/google/android/gms/games/a/au;->b:Lcom/google/android/gms/common/server/ClientContext;

    invoke-virtual {v3, v4}, Lcom/google/android/gms/games/b/w;->b(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/google/android/gms/games/a/bj;->S:Lcom/google/android/gms/games/b/w;

    invoke-virtual {v3, v2, v0, v1}, Lcom/google/android/gms/games/b/w;->a(Ljava/lang/Object;J)Z

    move-result v3

    if-nez v3, :cond_1

    .line 1757
    :cond_0
    invoke-virtual {p0, p1, p2}, Lcom/google/android/gms/games/a/bj;->c(Lcom/google/android/gms/games/a/au;I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    .line 1788
    :goto_0
    return-object v0

    .line 1759
    :cond_1
    iget-object v3, p0, Lcom/google/android/gms/games/a/bj;->S:Lcom/google/android/gms/games/b/w;

    invoke-virtual {v3, v2, v0, v1}, Lcom/google/android/gms/games/b/w;->b(Ljava/lang/Object;J)Ljava/lang/String;

    move-result-object v6

    .line 1761
    if-nez v6, :cond_2

    .line 1763
    iget-object v0, p0, Lcom/google/android/gms/games/a/bj;->S:Lcom/google/android/gms/games/b/w;

    invoke-virtual {v0, v2, v10, v11}, Lcom/google/android/gms/games/b/u;->a(Ljava/lang/Object;Landroid/os/Bundle;I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    goto :goto_0

    .line 1767
    :cond_2
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 1769
    :try_start_0
    iget-object v0, p1, Lcom/google/android/gms/games/a/au;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/gms/games/a/l;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v4

    .line 1770
    iget-object v0, p0, Lcom/google/android/gms/games/a/bj;->I:Lcom/google/android/gms/games/h/a/ci;

    iget-object v1, p1, Lcom/google/android/gms/games/a/au;->b:Lcom/google/android/gms/common/server/ClientContext;

    const-string v3, "experience_points"

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/gms/games/h/a/ci;->a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/String;)Lcom/google/android/gms/games/h/a/bd;

    move-result-object v0

    .line 1774
    invoke-virtual {v0}, Lcom/google/android/gms/games/h/a/bd;->getItems()Ljava/util/ArrayList;

    move-result-object v1

    if-eqz v1, :cond_3

    .line 1775
    invoke-virtual {v0}, Lcom/google/android/gms/games/h/a/bd;->b()Ljava/lang/String;

    move-result-object v6

    .line 1776
    iget-object v1, p1, Lcom/google/android/gms/games/a/au;->a:Landroid/content/Context;

    iget-object v3, p1, Lcom/google/android/gms/games/a/au;->b:Lcom/google/android/gms/common/server/ClientContext;

    invoke-virtual {v0}, Lcom/google/android/gms/games/h/a/bd;->getItems()Ljava/util/ArrayList;

    move-result-object v0

    const/4 v4, 0x1

    invoke-static {v1, v3, v0, v4}, Lcom/google/android/gms/games/a/bj;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/util/ArrayList;Z)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v7, v0}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z
    :try_end_0
    .catch Lcom/android/volley/ac; {:try_start_0 .. :try_end_0} :catch_0

    .line 1784
    :cond_3
    invoke-virtual {v7}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_4

    .line 1785
    iget-object v0, p1, Lcom/google/android/gms/games/a/au;->b:Lcom/google/android/gms/common/server/ClientContext;

    invoke-static {}, Lcom/google/android/gms/common/util/r;->c()Lcom/google/android/gms/common/util/p;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/gms/common/util/p;->a()J

    move-result-wide v8

    iget-object v1, p0, Lcom/google/android/gms/games/a/bj;->S:Lcom/google/android/gms/games/b/w;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/games/b/w;->a(Ljava/lang/Object;)V

    iget-object v1, p0, Lcom/google/android/gms/games/a/bj;->S:Lcom/google/android/gms/games/b/w;

    move-object v3, v7

    move v4, v12

    move-object v5, v10

    move v7, v12

    invoke-virtual/range {v1 .. v9}, Lcom/google/android/gms/games/b/w;->a(Ljava/lang/Object;Ljava/util/ArrayList;ILjava/lang/String;Ljava/lang/String;IJ)V

    iput-wide v8, p0, Lcom/google/android/gms/games/a/bj;->l:J

    .line 1788
    :cond_4
    iget-object v0, p0, Lcom/google/android/gms/games/a/bj;->S:Lcom/google/android/gms/games/b/w;

    invoke-virtual {v0, v2, v10, v11}, Lcom/google/android/gms/games/b/u;->a(Ljava/lang/Object;Landroid/os/Bundle;I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    goto :goto_0

    .line 1780
    :catch_0
    move-exception v0

    const-string v1, "PlayerAgent"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/server/b/c;->a(Lcom/android/volley/ac;Ljava/lang/String;)Lcom/google/android/gms/common/server/b/b;

    .line 1782
    const/4 v0, 0x6

    invoke-static {v0}, Lcom/google/android/gms/common/data/DataHolder;->b(I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    goto :goto_0
.end method

.method public final d(Lcom/google/android/gms/games/a/au;)V
    .locals 8

    .prologue
    .line 1350
    iget-object v1, p1, Lcom/google/android/gms/games/a/au;->a:Landroid/content/Context;

    .line 1351
    iget-object v0, p1, Lcom/google/android/gms/games/a/au;->b:Lcom/google/android/gms/common/server/ClientContext;

    invoke-static {v1, v0}, Lcom/google/android/gms/games/a/bj;->c(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;)Ljava/util/HashMap;

    move-result-object v2

    .line 1353
    new-instance v0, Ljava/util/HashSet;

    invoke-virtual {v2}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v3

    invoke-direct {v0, v3}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    .line 1354
    iget-object v3, p0, Lcom/google/android/gms/games/a/bj;->L:Lcom/google/android/gms/games/b/p;

    invoke-static {v3}, Lcom/google/android/gms/games/a/bj;->a(Lcom/google/android/gms/games/b/p;)Ljava/util/Set;

    move-result-object v3

    invoke-interface {v0, v3}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 1355
    iget-object v3, p0, Lcom/google/android/gms/games/a/bj;->M:Lcom/google/android/gms/games/b/p;

    invoke-static {v3}, Lcom/google/android/gms/games/a/bj;->a(Lcom/google/android/gms/games/b/p;)Ljava/util/Set;

    move-result-object v3

    invoke-interface {v0, v3}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 1359
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 1360
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 1361
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1362
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v6

    const/16 v7, 0x32

    if-lt v6, v7, :cond_0

    .line 1363
    invoke-direct {p0, p1, v4, v2, v3}, Lcom/google/android/gms/games/a/bj;->a(Lcom/google/android/gms/games/a/au;Ljava/util/ArrayList;Ljava/util/HashMap;Ljava/util/ArrayList;)V

    .line 1364
    invoke-virtual {v4}, Ljava/util/ArrayList;->clear()V

    goto :goto_0

    .line 1369
    :cond_0
    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1373
    :cond_1
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_2

    .line 1374
    invoke-direct {p0, p1, v4, v2, v3}, Lcom/google/android/gms/games/a/bj;->a(Lcom/google/android/gms/games/a/au;Ljava/util/ArrayList;Ljava/util/HashMap;Ljava/util/ArrayList;)V

    .line 1378
    :cond_2
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_3

    .line 1379
    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "PlayerAgent"

    invoke-static {v0, v3, v1}, Lcom/google/android/gms/games/a/l;->a(Landroid/content/ContentResolver;Ljava/util/ArrayList;Ljava/lang/String;)Z

    .line 1381
    :cond_3
    return-void
.end method

.method public final e(Lcom/google/android/gms/games/a/au;)Lcom/google/android/gms/common/data/DataHolder;
    .locals 8

    .prologue
    const/4 v7, -0x1

    const/4 v6, 0x0

    .line 1580
    iget-object v1, p1, Lcom/google/android/gms/games/a/au;->b:Lcom/google/android/gms/common/server/ClientContext;

    .line 1581
    invoke-static {}, Lcom/google/android/gms/common/util/r;->c()Lcom/google/android/gms/common/util/p;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/common/util/p;->a()J

    move-result-wide v2

    .line 1582
    iget-object v0, p0, Lcom/google/android/gms/games/a/bj;->Q:Lcom/google/android/gms/games/b/r;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/b/r;->a(Ljava/lang/Object;)V

    .line 1583
    iget-boolean v0, p1, Lcom/google/android/gms/games/a/au;->g:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/games/a/bj;->Q:Lcom/google/android/gms/games/b/r;

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/gms/games/b/r;->a(Ljava/lang/Object;J)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1585
    iget-object v0, p0, Lcom/google/android/gms/games/a/bj;->Q:Lcom/google/android/gms/games/b/r;

    invoke-virtual {v0, v1, v6, v7}, Lcom/google/android/gms/games/b/u;->a(Ljava/lang/Object;Landroid/os/Bundle;I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    .line 1601
    :goto_0
    return-object v0

    .line 1591
    :cond_0
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/games/a/bj;->F:Lcom/google/android/gms/games/h/a/dj;

    const-string v3, "players/me/profilesettings"

    iget-object v0, v0, Lcom/google/android/gms/games/h/a/dj;->a:Lcom/google/android/gms/common/server/n;

    const/4 v2, 0x0

    const/4 v4, 0x0

    const-class v5, Lcom/google/android/gms/games/h/a/dk;

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/common/server/n;->a(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;Ljava/lang/Object;Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/h/a/dk;
    :try_end_0
    .catch Lcom/android/volley/ac; {:try_start_0 .. :try_end_0} :catch_0

    .line 1600
    invoke-direct {p0, p1, v0}, Lcom/google/android/gms/games/a/bj;->a(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/h/a/dk;)V

    .line 1601
    iget-object v0, p0, Lcom/google/android/gms/games/a/bj;->Q:Lcom/google/android/gms/games/b/r;

    invoke-virtual {v0, v1, v6, v7}, Lcom/google/android/gms/games/b/u;->a(Ljava/lang/Object;Landroid/os/Bundle;I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    goto :goto_0

    .line 1592
    :catch_0
    move-exception v0

    .line 1593
    invoke-static {}, Lcom/google/android/gms/games/internal/dq;->a()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1594
    const-string v1, "PlayerAgent"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/server/b/c;->a(Lcom/android/volley/ac;Ljava/lang/String;)Lcom/google/android/gms/common/server/b/b;

    .line 1596
    :cond_1
    const/4 v0, 0x4

    invoke-static {v0}, Lcom/google/android/gms/common/data/DataHolder;->b(I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    goto :goto_0
.end method

.method public final f(Lcom/google/android/gms/games/a/au;)Lcom/google/android/gms/common/data/DataHolder;
    .locals 13

    .prologue
    const/4 v0, 0x0

    const/4 v12, 0x2

    const/4 v11, 0x1

    const/4 v6, 0x0

    .line 2391
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 2396
    :cond_0
    :try_start_0
    iget-object v2, p0, Lcom/google/android/gms/games/a/bj;->I:Lcom/google/android/gms/games/h/a/ci;

    iget-object v1, p1, Lcom/google/android/gms/games/a/au;->b:Lcom/google/android/gms/common/server/ClientContext;

    iget-object v3, p1, Lcom/google/android/gms/games/a/au;->f:Ljava/lang/String;

    const-string v4, "all"

    iget-object v5, p1, Lcom/google/android/gms/games/a/au;->a:Landroid/content/Context;

    invoke-static {v5}, Lcom/google/android/gms/games/a/l;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v5

    const-string v8, "players/%1$s/categories/%2$s"

    const/4 v9, 0x2

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    invoke-static {v3}, Lcom/google/android/gms/games/h/a/ci;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v9, v10

    const/4 v3, 0x1

    invoke-static {v4}, Lcom/google/android/gms/games/h/a/ci;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v9, v3

    invoke-static {v8, v9}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    if-eqz v5, :cond_1

    const-string v4, "language"

    invoke-static {v5}, Lcom/google/android/gms/games/h/a/ci;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/google/android/gms/games/h/a/ci;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    :cond_1
    if-eqz v0, :cond_2

    const-string v4, "pageToken"

    invoke-static {v0}, Lcom/google/android/gms/games/h/a/ci;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v4, v0}, Lcom/google/android/gms/games/h/a/ci;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    :cond_2
    iget-object v0, v2, Lcom/google/android/gms/games/h/a/ci;->a:Lcom/google/android/gms/common/server/n;

    const/4 v2, 0x0

    const/4 v4, 0x0

    const-class v5, Lcom/google/android/gms/games/h/a/an;

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/common/server/n;->a(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;Ljava/lang/Object;Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/h/a/an;

    .line 2402
    invoke-virtual {v0}, Lcom/google/android/gms/games/h/a/an;->getItems()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v7, v1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 2403
    invoke-virtual {v0}, Lcom/google/android/gms/games/h/a/an;->b()Ljava/lang/String;
    :try_end_0
    .catch Lcom/android/volley/ac; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 2404
    if-nez v0, :cond_0

    .line 2411
    new-array v0, v12, [Ljava/lang/String;

    const-string v1, "game_category"

    aput-object v1, v0, v6

    const-string v1, "xp_for_game"

    aput-object v1, v0, v11

    const-string v1, "game_category"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/data/DataHolder;->a([Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/data/m;

    move-result-object v2

    .line 2413
    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v3

    move v1, v6

    :goto_0
    if-ge v1, v3, :cond_3

    .line 2414
    invoke-virtual {v7, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/h/a/am;

    .line 2415
    new-instance v4, Landroid/content/ContentValues;

    invoke-direct {v4}, Landroid/content/ContentValues;-><init>()V

    .line 2416
    const-string v5, "game_category"

    invoke-virtual {v0}, Lcom/google/android/gms/games/h/a/am;->b()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v4, v5, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2417
    const-string v5, "xp_for_game"

    invoke-virtual {v0}, Lcom/google/android/gms/games/h/a/am;->c()Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v4, v5, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 2418
    invoke-virtual {v2, v4}, Lcom/google/android/gms/common/data/m;->a(Landroid/content/ContentValues;)Lcom/google/android/gms/common/data/m;

    .line 2413
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2405
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/android/volley/ac;->printStackTrace()V

    .line 2407
    const/4 v0, 0x6

    invoke-static {v0}, Lcom/google/android/gms/common/data/DataHolder;->b(I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    .line 2420
    :goto_1
    return-object v0

    :cond_3
    invoke-virtual {v2, v6}, Lcom/google/android/gms/common/data/m;->a(I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    goto :goto_1
.end method
