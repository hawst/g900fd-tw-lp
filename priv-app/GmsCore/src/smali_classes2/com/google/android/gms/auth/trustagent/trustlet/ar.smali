.class public final Lcom/google/android/gms/auth/trustagent/trustlet/ar;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static b:Ljava/lang/String;

.field private static final c:J


# instance fields
.field protected final a:Lcom/google/android/gms/auth/trustagent/trustlet/at;

.field private final d:Ljava/util/concurrent/atomic/AtomicLong;

.field private final e:Landroid/content/Context;

.field private final f:Lcom/google/android/gms/auth/trustagent/trustlet/as;

.field private g:Z

.field private h:Landroid/app/PendingIntent;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 28
    const-string v0, "com.google.android.gms.auth.trustagent.trustlet.ACTION_ACTIVITY_DETECTED"

    sput-object v0, Lcom/google/android/gms/auth/trustagent/trustlet/ar;->b:Ljava/lang/String;

    .line 40
    sget-object v0, Lcom/google/android/gms/auth/b/a;->aa:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    const-wide/16 v2, 0x3e8

    mul-long/2addr v0, v2

    sput-wide v0, Lcom/google/android/gms/auth/trustagent/trustlet/ar;->c:J

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/android/gms/auth/trustagent/trustlet/as;)V
    .locals 4

    .prologue
    .line 55
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 44
    new-instance v0, Ljava/util/concurrent/atomic/AtomicLong;

    const-wide/16 v2, -0x1

    invoke-direct {v0, v2, v3}, Ljava/util/concurrent/atomic/AtomicLong;-><init>(J)V

    iput-object v0, p0, Lcom/google/android/gms/auth/trustagent/trustlet/ar;->d:Ljava/util/concurrent/atomic/AtomicLong;

    .line 56
    iput-object p1, p0, Lcom/google/android/gms/auth/trustagent/trustlet/ar;->e:Landroid/content/Context;

    .line 57
    iput-object p2, p0, Lcom/google/android/gms/auth/trustagent/trustlet/ar;->f:Lcom/google/android/gms/auth/trustagent/trustlet/as;

    .line 58
    new-instance v0, Lcom/google/android/gms/auth/trustagent/trustlet/at;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/android/gms/auth/trustagent/trustlet/at;-><init>(Lcom/google/android/gms/auth/trustagent/trustlet/ar;B)V

    iput-object v0, p0, Lcom/google/android/gms/auth/trustagent/trustlet/ar;->a:Lcom/google/android/gms/auth/trustagent/trustlet/at;

    .line 59
    invoke-direct {p0}, Lcom/google/android/gms/auth/trustagent/trustlet/ar;->d()V

    .line 60
    const-string v0, "PhonePositionTracker"

    const-string v1, "PhonePositionTracker created"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 61
    return-void
.end method

.method private d()V
    .locals 8

    .prologue
    const/4 v7, 0x0

    const/4 v4, 0x1

    .line 67
    const-string v0, "PhonePositionTracker"

    const-string v1, "Registering to activity recognition"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 70
    new-instance v0, Landroid/content/Intent;

    sget-object v1, Lcom/google/android/gms/auth/trustagent/trustlet/ar;->b:Ljava/lang/String;

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 71
    const-string v1, "PhonePositionTracker"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "AR broadcast intent: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v3, Lcom/google/android/gms/auth/trustagent/trustlet/ar;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", package: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/gms/auth/trustagent/trustlet/ar;->e:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 73
    iget-object v1, p0, Lcom/google/android/gms/auth/trustagent/trustlet/ar;->e:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 74
    iget-object v1, p0, Lcom/google/android/gms/auth/trustagent/trustlet/ar;->e:Landroid/content/Context;

    const/high16 v2, 0x8000000

    invoke-static {v1, v7, v0, v2}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/auth/trustagent/trustlet/ar;->h:Landroid/app/PendingIntent;

    .line 76
    new-instance v1, Lcom/google/android/location/internal/j;

    invoke-direct {v1}, Lcom/google/android/location/internal/j;-><init>()V

    .line 77
    const-wide/32 v2, 0xea60

    iget-object v5, p0, Lcom/google/android/gms/auth/trustagent/trustlet/ar;->h:Landroid/app/PendingIntent;

    const-string v6, "PhonePositionTracker"

    invoke-virtual/range {v1 .. v6}, Lcom/google/android/location/internal/j;->a(JZLandroid/app/PendingIntent;Ljava/lang/String;)Lcom/google/android/location/internal/j;

    .line 79
    invoke-virtual {v1, v4}, Lcom/google/android/location/internal/j;->c(Z)Lcom/google/android/location/internal/j;

    .line 80
    new-array v0, v4, [I

    const/16 v2, 0x9

    aput v2, v0, v7

    iget-object v2, v1, Lcom/google/android/location/internal/j;->a:Landroid/content/Intent;

    const-string v3, "com.google.android.location.internal.EXTRA_NONDEFAULT_ACTIVITY_TYPES"

    invoke-virtual {v2, v3, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[I)Landroid/content/Intent;

    .line 81
    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/trustlet/ar;->e:Landroid/content/Context;

    invoke-virtual {v1, v0}, Lcom/google/android/location/internal/j;->a(Landroid/content/Context;)Landroid/content/ComponentName;

    move-result-object v0

    if-nez v0, :cond_0

    .line 82
    const-string v0, "PhonePositionTracker"

    const-string v1, "Unable to start the GMS NLP"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 83
    iput-boolean v7, p0, Lcom/google/android/gms/auth/trustagent/trustlet/ar;->g:Z

    .line 89
    :goto_0
    return-void

    .line 85
    :cond_0
    iput-boolean v4, p0, Lcom/google/android/gms/auth/trustagent/trustlet/ar;->g:Z

    .line 86
    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/trustlet/ar;->e:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/gms/auth/trustagent/trustlet/ar;->a:Lcom/google/android/gms/auth/trustagent/trustlet/at;

    new-instance v2, Landroid/content/IntentFilter;

    sget-object v3, Lcom/google/android/gms/auth/trustagent/trustlet/ar;->b:Ljava/lang/String;

    invoke-direct {v2, v3}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 95
    const-string v0, "PhonePositionTracker"

    const-string v1, "Unregistering from activity recognition"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 96
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/auth/trustagent/trustlet/ar;->g:Z

    .line 97
    new-instance v0, Lcom/google/android/location/internal/j;

    invoke-direct {v0}, Lcom/google/android/location/internal/j;-><init>()V

    .line 98
    iget-object v1, p0, Lcom/google/android/gms/auth/trustagent/trustlet/ar;->h:Landroid/app/PendingIntent;

    invoke-virtual {v0, v1}, Lcom/google/android/location/internal/j;->a(Landroid/app/PendingIntent;)Lcom/google/android/location/internal/j;

    .line 99
    iget-object v1, p0, Lcom/google/android/gms/auth/trustagent/trustlet/ar;->e:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/google/android/location/internal/j;->a(Landroid/content/Context;)Landroid/content/ComponentName;

    move-result-object v0

    if-nez v0, :cond_0

    .line 100
    const-string v0, "PhonePositionTracker"

    const-string v1, "Unable to start the GMS NLP"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 102
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/trustlet/ar;->e:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/gms/auth/trustagent/trustlet/ar;->a:Lcom/google/android/gms/auth/trustagent/trustlet/at;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 103
    return-void
.end method

.method protected final a(Lcom/google/android/gms/location/ActivityRecognitionResult;)V
    .locals 12

    .prologue
    const-wide/16 v10, -0x1

    const/16 v9, 0x32

    const/16 v8, 0x9

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 130
    invoke-virtual {p1}, Lcom/google/android/gms/location/ActivityRecognitionResult;->a()Lcom/google/android/gms/location/DetectedActivity;

    move-result-object v3

    .line 131
    const-string v0, "PhonePositionTracker"

    const-string v4, "Received AR result: %s at %s"

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    aput-object v3, v5, v2

    invoke-virtual {p1}, Lcom/google/android/gms/location/ActivityRecognitionResult;->d()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    aput-object v6, v5, v1

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 132
    invoke-virtual {v3}, Lcom/google/android/gms/location/DetectedActivity;->a()I

    move-result v0

    const/4 v4, 0x5

    if-ne v0, v4, :cond_0

    invoke-virtual {v3}, Lcom/google/android/gms/location/DetectedActivity;->b()I

    move-result v0

    int-to-long v4, v0

    const-wide/16 v6, 0x32

    cmp-long v0, v4, v6

    if-gtz v0, :cond_1

    :cond_0
    invoke-virtual {v3}, Lcom/google/android/gms/location/DetectedActivity;->a()I

    move-result v0

    if-ne v0, v8, :cond_3

    invoke-virtual {v3}, Lcom/google/android/gms/location/DetectedActivity;->b()I

    move-result v0

    if-gt v0, v9, :cond_3

    :cond_1
    move v0, v1

    .line 136
    :goto_0
    if-eqz v0, :cond_4

    .line 137
    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/trustlet/ar;->d:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v0, v10, v11}, Ljava/util/concurrent/atomic/AtomicLong;->set(J)V

    .line 154
    :cond_2
    :goto_1
    return-void

    :cond_3
    move v0, v2

    .line 132
    goto :goto_0

    .line 139
    :cond_4
    invoke-virtual {v3}, Lcom/google/android/gms/location/DetectedActivity;->a()I

    move-result v0

    if-ne v0, v8, :cond_5

    invoke-virtual {v3}, Lcom/google/android/gms/location/DetectedActivity;->b()I

    move-result v0

    if-le v0, v9, :cond_5

    move v0, v1

    .line 142
    :goto_2
    if-eqz v0, :cond_2

    .line 143
    invoke-virtual {p1}, Lcom/google/android/gms/location/ActivityRecognitionResult;->d()J

    move-result-wide v4

    .line 144
    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/trustlet/ar;->d:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v0, v10, v11, v4, v5}, Ljava/util/concurrent/atomic/AtomicLong;->compareAndSet(JJ)Z

    .line 145
    const-string v0, "PhonePositionTracker"

    const-string v3, "Off body for %d millis"

    new-array v1, v1, [Ljava/lang/Object;

    iget-object v6, p0, Lcom/google/android/gms/auth/trustagent/trustlet/ar;->d:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v6}, Ljava/util/concurrent/atomic/AtomicLong;->get()J

    move-result-wide v6

    sub-long v6, v4, v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    aput-object v6, v1, v2

    invoke-static {v3, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 147
    const-string v0, "PhonePositionTracker"

    const-string v1, "Update on person state based on new OFF_BODY result"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 148
    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/trustlet/ar;->d:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicLong;->get()J

    move-result-wide v0

    sub-long v0, v4, v0

    sget-wide v2, Lcom/google/android/gms/auth/trustagent/trustlet/ar;->c:J

    cmp-long v0, v0, v2

    if-ltz v0, :cond_2

    .line 149
    const-string v0, "PhonePositionTracker"

    const-string v1, "Switching to off person state"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 150
    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/trustlet/ar;->f:Lcom/google/android/gms/auth/trustagent/trustlet/as;

    invoke-interface {v0}, Lcom/google/android/gms/auth/trustagent/trustlet/as;->a()V

    goto :goto_1

    :cond_5
    move v0, v2

    .line 139
    goto :goto_2
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 106
    iget-boolean v0, p0, Lcom/google/android/gms/auth/trustagent/trustlet/ar;->g:Z

    return v0
.end method

.method public final c()V
    .locals 4

    .prologue
    .line 113
    const-string v0, "PhonePositionTracker"

    const-string v1, "Reset phone on-person state"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 114
    iget-boolean v0, p0, Lcom/google/android/gms/auth/trustagent/trustlet/ar;->g:Z

    if-nez v0, :cond_0

    .line 115
    invoke-direct {p0}, Lcom/google/android/gms/auth/trustagent/trustlet/ar;->d()V

    .line 117
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/trustlet/ar;->d:Ljava/util/concurrent/atomic/AtomicLong;

    const-wide/16 v2, -0x1

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/atomic/AtomicLong;->set(J)V

    .line 118
    return-void
.end method
