.class public final Lcom/google/android/gms/games/service/RoomAndroidService;
.super Landroid/app/Service;
.source "SourceFile"


# static fields
.field private static a:I


# instance fields
.field private b:Lcom/google/android/gms/games/service/af;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 61
    const/4 v0, -0x1

    sput v0, Lcom/google/android/gms/games/service/RoomAndroidService;->a:I

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 51
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 107
    return-void
.end method

.method public static a(Ljava/lang/String;)Lcom/google/android/gms/games/h/a/ev;
    .locals 2

    .prologue
    .line 69
    new-instance v0, Lcom/google/android/gms/games/h/a/ev;

    invoke-direct {v0}, Lcom/google/android/gms/games/h/a/ev;-><init>()V

    .line 70
    new-instance v1, Lcom/google/android/gms/common/server/response/d;

    invoke-direct {v1}, Lcom/google/android/gms/common/server/response/d;-><init>()V

    .line 71
    invoke-virtual {v1, p0, v0}, Lcom/google/android/gms/common/server/response/d;->a(Ljava/lang/String;Lcom/google/android/gms/common/server/response/FastJsonResponse;)V

    .line 72
    return-object v0
.end method


# virtual methods
.method protected final dump(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 99
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "[RTMP DUMP] "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 100
    iget-object v0, p0, Lcom/google/android/gms/games/service/RoomAndroidService;->b:Lcom/google/android/gms/games/service/af;

    const-string v1, "  "

    invoke-virtual {v0, p2, v1}, Lcom/google/android/gms/games/service/af;->a(Ljava/io/PrintWriter;Ljava/lang/String;)V

    .line 101
    return-void
.end method

.method public final onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 3

    .prologue
    .line 77
    monitor-enter p0

    .line 78
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/games/service/RoomAndroidService;->b:Lcom/google/android/gms/games/service/af;

    if-nez v0, :cond_0

    .line 79
    new-instance v0, Lcom/google/android/gms/games/service/af;

    invoke-direct {v0, p0}, Lcom/google/android/gms/games/service/af;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/gms/games/service/RoomAndroidService;->b:Lcom/google/android/gms/games/service/af;

    .line 81
    :cond_0
    sget-object v0, Landroid/os/Build;->TYPE:Ljava/lang/String;

    const-string v1, "user"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    sget v0, Lcom/google/android/gms/games/service/RoomAndroidService;->a:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_1

    .line 82
    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v0

    sput v0, Lcom/google/android/gms/games/service/RoomAndroidService;->a:I

    .line 83
    const-string v0, "RoomAndroidService"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Room Android Service has pid "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget v2, Lcom/google/android/gms/games/service/RoomAndroidService;->a:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 85
    :cond_1
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 86
    iget-object v0, p0, Lcom/google/android/gms/games/service/RoomAndroidService;->b:Lcom/google/android/gms/games/service/af;

    return-object v0

    .line 85
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final onDestroy()V
    .locals 1

    .prologue
    .line 91
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/games/service/RoomAndroidService;->b:Lcom/google/android/gms/games/service/af;

    .line 92
    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    .line 93
    return-void
.end method
