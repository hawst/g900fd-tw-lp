.class public final Lcom/google/android/gms/games/ui/client/snapshots/ClientSnapshotListActivity;
.super Lcom/google/android/gms/games/ui/client/a;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/api/aq;
.implements Lcom/google/android/gms/games/ui/common/b/a;
.implements Lcom/google/android/gms/games/ui/common/b/b;


# static fields
.field private static final n:I


# instance fields
.field private o:Ljava/lang/String;

.field private p:Z

.field private q:Z

.field private r:I

.field private s:Lcom/google/android/gms/games/ui/client/snapshots/SnapshotFragment;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 49
    sget v0, Lcom/google/android/gms/l;->bO:I

    sput v0, Lcom/google/android/gms/games/ui/client/snapshots/ClientSnapshotListActivity;->n:I

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 62
    invoke-direct {p0, v0, v0, v1, v1}, Lcom/google/android/gms/games/ui/client/a;-><init>(IIZZ)V

    .line 55
    iput-boolean v0, p0, Lcom/google/android/gms/games/ui/client/snapshots/ClientSnapshotListActivity;->p:Z

    .line 56
    iput-boolean v0, p0, Lcom/google/android/gms/games/ui/client/snapshots/ClientSnapshotListActivity;->q:Z

    .line 57
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/games/ui/client/snapshots/ClientSnapshotListActivity;->r:I

    .line 64
    return-void
.end method


# virtual methods
.method protected final N()I
    .locals 1

    .prologue
    .line 135
    const/16 v0, 0xf

    return v0
.end method

.method public final T()I
    .locals 1

    .prologue
    .line 141
    iget v0, p0, Lcom/google/android/gms/games/ui/client/snapshots/ClientSnapshotListActivity;->r:I

    return v0
.end method

.method public final U()Z
    .locals 1

    .prologue
    .line 146
    iget-boolean v0, p0, Lcom/google/android/gms/games/ui/client/snapshots/ClientSnapshotListActivity;->p:Z

    return v0
.end method

.method public final V()Z
    .locals 1

    .prologue
    .line 151
    iget-boolean v0, p0, Lcom/google/android/gms/games/ui/client/snapshots/ClientSnapshotListActivity;->q:Z

    return v0
.end method

.method public final W()V
    .locals 3

    .prologue
    .line 179
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 180
    const-string v1, "com.google.android.gms.games.SNAPSHOT_NEW"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 182
    const/4 v1, -0x1

    invoke-virtual {p0, v1, v0}, Lcom/google/android/gms/games/ui/client/snapshots/ClientSnapshotListActivity;->setResult(ILandroid/content/Intent;)V

    .line 183
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/client/snapshots/ClientSnapshotListActivity;->finish()V

    .line 184
    return-void
.end method

.method public final synthetic a(Lcom/google/android/gms/common/api/ap;)V
    .locals 2

    .prologue
    .line 43
    check-cast p1, Lcom/google/android/gms/games/snapshot/i;

    invoke-interface {p1}, Lcom/google/android/gms/games/snapshot/i;->C_()Lcom/google/android/gms/common/api/Status;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/common/api/Status;->h()I

    move-result v0

    if-nez v0, :cond_0

    invoke-interface {p1}, Lcom/google/android/gms/games/snapshot/i;->b()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/games/ui/client/snapshots/ClientSnapshotListActivity;->s:Lcom/google/android/gms/games/ui/client/snapshots/SnapshotFragment;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/games/ui/client/snapshots/SnapshotFragment;->a(Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    sget v0, Lcom/google/android/gms/p;->iD:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/client/snapshots/ClientSnapshotListActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0
.end method

.method protected final a(Lcom/google/android/gms/common/api/w;)V
    .locals 1

    .prologue
    .line 92
    invoke-super {p0, p1}, Lcom/google/android/gms/games/ui/client/a;->a(Lcom/google/android/gms/common/api/w;)V

    .line 93
    sget-object v0, Lcom/google/android/gms/drive/b;->c:Lcom/google/android/gms/common/api/Scope;

    invoke-virtual {p1, v0}, Lcom/google/android/gms/common/api/w;->a(Lcom/google/android/gms/common/api/Scope;)Lcom/google/android/gms/common/api/w;

    .line 94
    return-void
.end method

.method public final a(Lcom/google/android/gms/games/snapshot/SnapshotMetadata;)V
    .locals 3

    .prologue
    .line 161
    const-string v0, "SnapshotActivity"

    const-string v1, "onSnapshotClicked:"

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 162
    invoke-static {p1}, Lcom/google/android/gms/common/internal/e;->a(Ljava/lang/Object;)V

    .line 165
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 168
    const-string v2, "com.google.android.gms.games.SNAPSHOT_METADATA"

    invoke-interface {p1}, Lcom/google/android/gms/games/snapshot/SnapshotMetadata;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Parcelable;

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 170
    const/4 v0, -0x1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/games/ui/client/snapshots/ClientSnapshotListActivity;->setResult(ILandroid/content/Intent;)V

    .line 171
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/client/snapshots/ClientSnapshotListActivity;->finish()V

    .line 172
    return-void
.end method

.method public final b(Lcom/google/android/gms/games/snapshot/SnapshotMetadata;)V
    .locals 4

    .prologue
    .line 189
    new-instance v1, Lcom/google/android/gms/games/ui/client/snapshots/a;

    invoke-direct {v1}, Lcom/google/android/gms/games/ui/client/snapshots/a;-><init>()V

    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    const-string v3, "com.google.android.gms.games.SNAPSHOT_METADATA"

    invoke-interface {p1}, Lcom/google/android/gms/games/snapshot/SnapshotMetadata;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Parcelable;

    invoke-virtual {v2, v3, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    invoke-virtual {v1, v2}, Lcom/google/android/gms/games/ui/client/snapshots/a;->setArguments(Landroid/os/Bundle;)V

    const-string v0, "confirm_delete_dialog"

    invoke-static {p0, v1, v0}, Lcom/google/android/gms/games/ui/d/a;->a(Landroid/support/v4/app/q;Landroid/support/v4/app/m;Ljava/lang/String;)V

    .line 190
    return-void
.end method

.method public final c(Lcom/google/android/gms/games/snapshot/SnapshotMetadata;)V
    .locals 2

    .prologue
    .line 215
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/client/snapshots/ClientSnapshotListActivity;->w()Lcom/google/android/gms/common/api/v;

    move-result-object v0

    .line 216
    invoke-static {v0, p0}, Lcom/google/android/gms/games/ui/d/al;->a(Lcom/google/android/gms/common/api/v;Lcom/google/android/gms/games/ui/q;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 218
    const-string v0, "SnapshotActivity"

    const-string v1, "deleteSnapshot: not connected; ignoring..."

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 222
    :goto_0
    return-void

    .line 221
    :cond_0
    sget-object v1, Lcom/google/android/gms/games/d;->s:Lcom/google/android/gms/games/snapshot/h;

    invoke-interface {v1, v0, p1}, Lcom/google/android/gms/games/snapshot/h;->a(Lcom/google/android/gms/common/api/v;Lcom/google/android/gms/games/snapshot/SnapshotMetadata;)Lcom/google/android/gms/common/api/am;

    move-result-object v0

    invoke-interface {v0, p0}, Lcom/google/android/gms/common/api/am;->a(Lcom/google/android/gms/common/api/aq;)V

    goto :goto_0
.end method

.method public final f()Z
    .locals 1

    .prologue
    .line 82
    const/4 v0, 0x1

    return v0
.end method

.method public final onAttachFragment(Landroid/support/v4/app/Fragment;)V
    .locals 2

    .prologue
    .line 202
    invoke-virtual {p1}, Landroid/support/v4/app/Fragment;->getId()I

    move-result v0

    sget v1, Lcom/google/android/gms/j;->rP:I

    if-ne v0, v1, :cond_0

    .line 203
    check-cast p1, Lcom/google/android/gms/games/ui/client/snapshots/SnapshotFragment;

    iput-object p1, p0, Lcom/google/android/gms/games/ui/client/snapshots/ClientSnapshotListActivity;->s:Lcom/google/android/gms/games/ui/client/snapshots/SnapshotFragment;

    .line 205
    :cond_0
    return-void
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 68
    invoke-super {p0, p1}, Lcom/google/android/gms/games/ui/client/a;->onCreate(Landroid/os/Bundle;)V

    .line 70
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/client/snapshots/ClientSnapshotListActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v2, "com.google.android.gms.games.TITLE"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/gms/games/ui/client/snapshots/ClientSnapshotListActivity;->o:Ljava/lang/String;

    const-string v2, "com.google.android.gms.games.MAX_SNAPSHOTS"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    iput v2, p0, Lcom/google/android/gms/games/ui/client/snapshots/ClientSnapshotListActivity;->r:I

    const-string v2, "com.google.android.gms.games.ALLOW_CREATE_SNAPSHOT"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v2

    iput-boolean v2, p0, Lcom/google/android/gms/games/ui/client/snapshots/ClientSnapshotListActivity;->p:Z

    const-string v2, "com.google.android.gms.games.ALLOW_DELETE_SNAPSHOT"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/games/ui/client/snapshots/ClientSnapshotListActivity;->q:Z

    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/snapshots/ClientSnapshotListActivity;->o:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "SnapshotActivity"

    const-string v2, "com.google.android.gms.games.TITLE must be set"

    invoke-static {v0, v2}, Lcom/google/android/gms/games/internal/dq;->e(Ljava/lang/String;Ljava/lang/String;)V

    move v0, v1

    :goto_0
    if-nez v0, :cond_0

    .line 72
    const-string v0, "SnapshotActivity"

    const-string v2, "Error parsing intent; bailing out..."

    invoke-static {v0, v2}, Lcom/google/android/gms/games/internal/dq;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 73
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/client/snapshots/ClientSnapshotListActivity;->finish()V

    .line 76
    :cond_0
    iput-boolean v1, p0, Lcom/google/android/gms/games/ui/client/snapshots/ClientSnapshotListActivity;->l:Z

    .line 77
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/snapshots/ClientSnapshotListActivity;->o:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/client/snapshots/ClientSnapshotListActivity;->setTitle(Ljava/lang/CharSequence;)V

    .line 78
    return-void

    .line 70
    :cond_1
    iget v0, p0, Lcom/google/android/gms/games/ui/client/snapshots/ClientSnapshotListActivity;->r:I

    const/4 v2, -0x1

    if-eq v0, v2, :cond_2

    iget v0, p0, Lcom/google/android/gms/games/ui/client/snapshots/ClientSnapshotListActivity;->r:I

    if-gtz v0, :cond_2

    const-string v0, "SnapshotActivity"

    const-string v2, "com.google.android.gms.games.MAX_SNAPSHOTS must be specified as either Snapshots.DISPLAY_LIMIT_NONE or > 0"

    invoke-static {v0, v2}, Lcom/google/android/gms/games/internal/dq;->e(Ljava/lang/String;Ljava/lang/String;)V

    move v0, v1

    goto :goto_0

    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method protected final u()I
    .locals 1

    .prologue
    .line 87
    sget v0, Lcom/google/android/gms/games/ui/client/snapshots/ClientSnapshotListActivity;->n:I

    return v0
.end method
