.class public Lcom/google/android/gms/games/ui/client/snapshots/SnapshotFragment;
.super Lcom/google/android/gms/games/ui/s;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/api/aq;
.implements Lcom/google/android/gms/games/ui/k;


# instance fields
.field private l:Lcom/google/android/gms/games/ui/client/snapshots/f;

.field private m:Lcom/google/android/gms/games/ui/j;

.field private n:Lcom/google/android/gms/games/ui/client/snapshots/d;

.field private o:Lcom/google/android/gms/games/ui/client/snapshots/b;

.field private p:Lcom/google/android/gms/games/ui/common/b/b;

.field private q:Lcom/google/android/gms/common/data/r;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/s;-><init>()V

    return-void
.end method

.method private C()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 80
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/client/snapshots/SnapshotFragment;->b()Lcom/google/android/gms/common/api/v;

    move-result-object v0

    .line 81
    iget-object v1, p0, Lcom/google/android/gms/games/ui/client/snapshots/SnapshotFragment;->d:Lcom/google/android/gms/games/ui/q;

    invoke-static {v0, v1}, Lcom/google/android/gms/games/ui/d/al;->a(Lcom/google/android/gms/common/api/v;Lcom/google/android/gms/games/ui/q;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 89
    :goto_0
    return-void

    .line 85
    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/games/ui/client/snapshots/SnapshotFragment;->d:Lcom/google/android/gms/games/ui/q;

    invoke-virtual {v1}, Lcom/google/android/gms/games/ui/q;->z()V

    .line 87
    sget-object v1, Lcom/google/android/gms/games/d;->s:Lcom/google/android/gms/games/snapshot/h;

    invoke-interface {v1, v0, v2}, Lcom/google/android/gms/games/snapshot/h;->a(Lcom/google/android/gms/common/api/v;Z)Lcom/google/android/gms/common/api/am;

    move-result-object v0

    invoke-interface {v0, p0}, Lcom/google/android/gms/common/api/am;->a(Lcom/google/android/gms/common/api/aq;)V

    .line 88
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/snapshots/SnapshotFragment;->h:Lcom/google/android/gms/games/ui/d/p;

    invoke-virtual {v0, v2}, Lcom/google/android/gms/games/ui/d/p;->b(I)V

    goto :goto_0
.end method

.method private D()V
    .locals 2

    .prologue
    .line 135
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/snapshots/SnapshotFragment;->p:Lcom/google/android/gms/games/ui/common/b/b;

    invoke-interface {v0}, Lcom/google/android/gms/games/ui/common/b/b;->U()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 136
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/snapshots/SnapshotFragment;->o:Lcom/google/android/gms/games/ui/client/snapshots/b;

    invoke-static {v0}, Lcom/google/android/gms/common/internal/e;->a(Ljava/lang/Object;)V

    .line 137
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/snapshots/SnapshotFragment;->n:Lcom/google/android/gms/games/ui/client/snapshots/d;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/client/snapshots/d;->f()Lcom/google/android/gms/common/data/d;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/common/data/d;->c()I

    move-result v0

    iget-object v1, p0, Lcom/google/android/gms/games/ui/client/snapshots/SnapshotFragment;->p:Lcom/google/android/gms/games/ui/common/b/b;

    invoke-interface {v1}, Lcom/google/android/gms/games/ui/common/b/b;->T()I

    move-result v1

    if-gt v0, v1, :cond_1

    .line 139
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/snapshots/SnapshotFragment;->o:Lcom/google/android/gms/games/ui/client/snapshots/b;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/ui/client/snapshots/b;->c(Z)V

    .line 144
    :cond_0
    :goto_0
    return-void

    .line 141
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/snapshots/SnapshotFragment;->o:Lcom/google/android/gms/games/ui/client/snapshots/b;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/ui/client/snapshots/b;->c(Z)V

    goto :goto_0
.end method

.method private b(I)V
    .locals 5

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 148
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/snapshots/SnapshotFragment;->n:Lcom/google/android/gms/games/ui/client/snapshots/d;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/client/snapshots/d;->f()Lcom/google/android/gms/common/data/d;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/common/data/d;->c()I

    move-result v1

    .line 154
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/snapshots/SnapshotFragment;->p:Lcom/google/android/gms/games/ui/common/b/b;

    invoke-interface {v0}, Lcom/google/android/gms/games/ui/common/b/b;->U()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 155
    add-int/lit8 v0, v1, 0x1

    .line 159
    :goto_0
    iget-object v4, p0, Lcom/google/android/gms/games/ui/client/snapshots/SnapshotFragment;->h:Lcom/google/android/gms/games/ui/d/p;

    invoke-virtual {v4, p1, v0, v2}, Lcom/google/android/gms/games/ui/d/p;->a(IIZ)V

    .line 164
    iget-object v4, p0, Lcom/google/android/gms/games/ui/client/snapshots/SnapshotFragment;->l:Lcom/google/android/gms/games/ui/client/snapshots/f;

    if-nez v1, :cond_0

    if-nez p1, :cond_0

    move v1, v2

    :goto_1
    invoke-virtual {v4, v1}, Lcom/google/android/gms/games/ui/client/snapshots/f;->c(Z)V

    .line 169
    invoke-static {p1}, Lcom/google/android/gms/games/ui/d/al;->a(I)Z

    move-result v1

    if-eqz v1, :cond_1

    if-lez v0, :cond_1

    .line 170
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/snapshots/SnapshotFragment;->m:Lcom/google/android/gms/games/ui/j;

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/ui/j;->f(I)V

    .line 171
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/snapshots/SnapshotFragment;->m:Lcom/google/android/gms/games/ui/j;

    invoke-virtual {v0, v2}, Lcom/google/android/gms/games/ui/j;->c(Z)V

    .line 178
    :goto_2
    return-void

    :cond_0
    move v1, v3

    .line 164
    goto :goto_1

    .line 172
    :cond_1
    if-eqz p1, :cond_2

    if-lez v0, :cond_2

    .line 173
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/snapshots/SnapshotFragment;->m:Lcom/google/android/gms/games/ui/j;

    const/4 v1, 0x6

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/ui/j;->f(I)V

    .line 174
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/snapshots/SnapshotFragment;->m:Lcom/google/android/gms/games/ui/j;

    invoke-virtual {v0, v2}, Lcom/google/android/gms/games/ui/j;->c(Z)V

    goto :goto_2

    .line 176
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/snapshots/SnapshotFragment;->m:Lcom/google/android/gms/games/ui/j;

    invoke-virtual {v0, v3}, Lcom/google/android/gms/games/ui/j;->c(Z)V

    goto :goto_2

    :cond_3
    move v0, v1

    goto :goto_0
.end method


# virtual methods
.method public final B()V
    .locals 0

    .prologue
    .line 186
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/client/snapshots/SnapshotFragment;->C()V

    .line 187
    return-void
.end method

.method public final a()V
    .locals 0

    .prologue
    .line 191
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/client/snapshots/SnapshotFragment;->C()V

    .line 192
    return-void
.end method

.method public final synthetic a(Lcom/google/android/gms/common/api/ap;)V
    .locals 4

    .prologue
    .line 31
    check-cast p1, Lcom/google/android/gms/games/snapshot/j;

    invoke-interface {p1}, Lcom/google/android/gms/games/snapshot/j;->C_()Lcom/google/android/gms/common/api/Status;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/common/api/Status;->h()I

    move-result v0

    invoke-interface {p1}, Lcom/google/android/gms/games/snapshot/j;->c()Lcom/google/android/gms/games/snapshot/c;

    move-result-object v1

    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/client/snapshots/SnapshotFragment;->d()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v2

    if-nez v2, :cond_0

    invoke-virtual {v1}, Lcom/google/android/gms/games/snapshot/c;->w_()V

    :goto_0
    return-void

    :cond_0
    :try_start_1
    iget-object v2, p0, Lcom/google/android/gms/games/ui/client/snapshots/SnapshotFragment;->d:Lcom/google/android/gms/games/ui/q;

    invoke-virtual {v2, v0}, Lcom/google/android/gms/games/ui/q;->b(I)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v2

    if-nez v2, :cond_1

    invoke-virtual {v1}, Lcom/google/android/gms/games/snapshot/c;->w_()V

    goto :goto_0

    :cond_1
    :try_start_2
    iget-object v2, p0, Lcom/google/android/gms/games/ui/client/snapshots/SnapshotFragment;->d:Lcom/google/android/gms/games/ui/q;

    invoke-virtual {v2}, Lcom/google/android/gms/games/ui/q;->A()V

    new-instance v2, Lcom/google/android/gms/common/data/s;

    const-string v3, "external_snapshot_id"

    invoke-direct {v2, v1, v3}, Lcom/google/android/gms/common/data/s;-><init>(Lcom/google/android/gms/common/data/a;Ljava/lang/String;)V

    iget-object v3, p0, Lcom/google/android/gms/games/ui/client/snapshots/SnapshotFragment;->n:Lcom/google/android/gms/games/ui/client/snapshots/d;

    invoke-virtual {v3, v2}, Lcom/google/android/gms/games/ui/client/snapshots/d;->a(Lcom/google/android/gms/common/data/d;)V

    iput-object v2, p0, Lcom/google/android/gms/games/ui/client/snapshots/SnapshotFragment;->q:Lcom/google/android/gms/common/data/r;

    invoke-direct {p0}, Lcom/google/android/gms/games/ui/client/snapshots/SnapshotFragment;->D()V

    iget-object v2, p0, Lcom/google/android/gms/games/ui/client/snapshots/SnapshotFragment;->n:Lcom/google/android/gms/games/ui/client/snapshots/d;

    iget-object v2, v2, Landroid/support/v7/widget/bv;->a:Landroid/support/v7/widget/bw;

    invoke-virtual {v2}, Landroid/support/v7/widget/bw;->a()V

    invoke-direct {p0, v0}, Lcom/google/android/gms/games/ui/client/snapshots/SnapshotFragment;->b(I)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Lcom/google/android/gms/games/snapshot/c;->w_()V

    throw v0
.end method

.method public final a(Lcom/google/android/gms/common/api/v;)V
    .locals 2

    .prologue
    .line 76
    sget-object v0, Lcom/google/android/gms/games/d;->s:Lcom/google/android/gms/games/snapshot/h;

    const/4 v1, 0x0

    invoke-interface {v0, p1, v1}, Lcom/google/android/gms/games/snapshot/h;->a(Lcom/google/android/gms/common/api/v;Z)Lcom/google/android/gms/common/api/am;

    move-result-object v0

    invoke-interface {v0, p0}, Lcom/google/android/gms/common/api/am;->a(Lcom/google/android/gms/common/api/aq;)V

    .line 77
    return-void
.end method

.method final a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 128
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/snapshots/SnapshotFragment;->q:Lcom/google/android/gms/common/data/r;

    invoke-interface {v0, p1}, Lcom/google/android/gms/common/data/r;->a(Ljava/lang/String;)V

    .line 129
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/snapshots/SnapshotFragment;->n:Lcom/google/android/gms/games/ui/client/snapshots/d;

    iget-object v0, v0, Landroid/support/v7/widget/bv;->a:Landroid/support/v7/widget/bw;

    invoke-virtual {v0}, Landroid/support/v7/widget/bw;->a()V

    .line 130
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/client/snapshots/SnapshotFragment;->D()V

    .line 131
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/gms/games/ui/client/snapshots/SnapshotFragment;->b(I)V

    .line 132
    return-void
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 47
    invoke-super {p0, p1}, Lcom/google/android/gms/games/ui/s;->onActivityCreated(Landroid/os/Bundle;)V

    .line 49
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/snapshots/SnapshotFragment;->d:Lcom/google/android/gms/games/ui/q;

    instance-of v0, v0, Lcom/google/android/gms/games/ui/common/b/a;

    invoke-static {v0}, Lcom/google/android/gms/common/internal/e;->a(Z)V

    .line 51
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/snapshots/SnapshotFragment;->d:Lcom/google/android/gms/games/ui/q;

    instance-of v0, v0, Lcom/google/android/gms/games/ui/common/b/b;

    const-string v1, "Parent activity did not implement SnapshotListMetaDataProvider"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/e;->a(ZLjava/lang/Object;)V

    .line 53
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/snapshots/SnapshotFragment;->d:Lcom/google/android/gms/games/ui/q;

    check-cast v0, Lcom/google/android/gms/games/ui/common/b/b;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/client/snapshots/SnapshotFragment;->p:Lcom/google/android/gms/games/ui/common/b/b;

    .line 55
    new-instance v0, Lcom/google/android/gms/games/ui/client/snapshots/f;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/client/snapshots/SnapshotFragment;->d:Lcom/google/android/gms/games/ui/q;

    invoke-direct {v0, v1}, Lcom/google/android/gms/games/ui/client/snapshots/f;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/gms/games/ui/client/snapshots/SnapshotFragment;->l:Lcom/google/android/gms/games/ui/client/snapshots/f;

    .line 56
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/snapshots/SnapshotFragment;->l:Lcom/google/android/gms/games/ui/client/snapshots/f;

    invoke-virtual {v0, v2}, Lcom/google/android/gms/games/ui/client/snapshots/f;->c(Z)V

    .line 58
    new-instance v0, Lcom/google/android/gms/games/ui/j;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/client/snapshots/SnapshotFragment;->d:Lcom/google/android/gms/games/ui/q;

    invoke-direct {v0, v1}, Lcom/google/android/gms/games/ui/j;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/gms/games/ui/client/snapshots/SnapshotFragment;->m:Lcom/google/android/gms/games/ui/j;

    .line 59
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/snapshots/SnapshotFragment;->m:Lcom/google/android/gms/games/ui/j;

    invoke-virtual {v0, v2}, Lcom/google/android/gms/games/ui/j;->c(Z)V

    .line 60
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/snapshots/SnapshotFragment;->m:Lcom/google/android/gms/games/ui/j;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/games/ui/j;->a(Lcom/google/android/gms/games/ui/k;)V

    .line 62
    new-instance v1, Lcom/google/android/gms/games/ui/client/snapshots/d;

    iget-object v2, p0, Lcom/google/android/gms/games/ui/client/snapshots/SnapshotFragment;->d:Lcom/google/android/gms/games/ui/q;

    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/snapshots/SnapshotFragment;->d:Lcom/google/android/gms/games/ui/q;

    check-cast v0, Lcom/google/android/gms/games/ui/common/b/a;

    invoke-direct {v1, v2, v0}, Lcom/google/android/gms/games/ui/client/snapshots/d;-><init>(Landroid/app/Activity;Lcom/google/android/gms/games/ui/common/b/a;)V

    iput-object v1, p0, Lcom/google/android/gms/games/ui/client/snapshots/SnapshotFragment;->n:Lcom/google/android/gms/games/ui/client/snapshots/d;

    .line 63
    new-instance v1, Lcom/google/android/gms/games/ui/by;

    invoke-direct {v1}, Lcom/google/android/gms/games/ui/by;-><init>()V

    .line 64
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/snapshots/SnapshotFragment;->m:Lcom/google/android/gms/games/ui/j;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/games/ui/by;->a(Lcom/google/android/gms/games/ui/ac;)Lcom/google/android/gms/games/ui/by;

    .line 65
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/snapshots/SnapshotFragment;->n:Lcom/google/android/gms/games/ui/client/snapshots/d;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/games/ui/by;->a(Lcom/google/android/gms/games/ui/ac;)Lcom/google/android/gms/games/ui/by;

    .line 66
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/snapshots/SnapshotFragment;->l:Lcom/google/android/gms/games/ui/client/snapshots/f;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/games/ui/by;->a(Lcom/google/android/gms/games/ui/ac;)Lcom/google/android/gms/games/ui/by;

    .line 67
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/snapshots/SnapshotFragment;->p:Lcom/google/android/gms/games/ui/common/b/b;

    invoke-interface {v0}, Lcom/google/android/gms/games/ui/common/b/b;->U()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 68
    new-instance v2, Lcom/google/android/gms/games/ui/client/snapshots/b;

    iget-object v3, p0, Lcom/google/android/gms/games/ui/client/snapshots/SnapshotFragment;->d:Lcom/google/android/gms/games/ui/q;

    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/snapshots/SnapshotFragment;->d:Lcom/google/android/gms/games/ui/q;

    check-cast v0, Lcom/google/android/gms/games/ui/common/b/a;

    invoke-direct {v2, v3, v0}, Lcom/google/android/gms/games/ui/client/snapshots/b;-><init>(Landroid/content/Context;Lcom/google/android/gms/games/ui/common/b/a;)V

    iput-object v2, p0, Lcom/google/android/gms/games/ui/client/snapshots/SnapshotFragment;->o:Lcom/google/android/gms/games/ui/client/snapshots/b;

    .line 69
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/snapshots/SnapshotFragment;->o:Lcom/google/android/gms/games/ui/client/snapshots/b;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/games/ui/by;->a(Lcom/google/android/gms/games/ui/ac;)Lcom/google/android/gms/games/ui/by;

    .line 71
    :cond_0
    invoke-virtual {v1}, Lcom/google/android/gms/games/ui/by;->a()Lcom/google/android/gms/games/ui/bw;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/client/snapshots/SnapshotFragment;->a(Landroid/support/v7/widget/bv;)V

    .line 72
    return-void
.end method
