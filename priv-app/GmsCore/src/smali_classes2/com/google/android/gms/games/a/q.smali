.class final Lcom/google/android/gms/games/a/q;
.super Lcom/google/android/gms/games/a/bb;
.source "SourceFile"


# static fields
.field private static final b:Ljava/util/concurrent/locks/ReentrantLock;


# instance fields
.field final a:Lcom/google/android/gms/games/b/f;

.field private final c:Lcom/google/android/gms/games/h/a/t;

.field private final d:Lcom/google/android/gms/games/b/b;

.field private final e:Lcom/google/android/gms/games/b/c;

.field private final f:Lcom/google/android/gms/games/b/e;

.field private final g:Lcom/google/android/gms/games/b/g;

.field private h:Landroid/content/ContentValues;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 71
    new-instance v0, Ljava/util/concurrent/locks/ReentrantLock;

    invoke-direct {v0}, Ljava/util/concurrent/locks/ReentrantLock;-><init>()V

    sput-object v0, Lcom/google/android/gms/games/a/q;->b:Ljava/util/concurrent/locks/ReentrantLock;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/gms/games/a/bb;Lcom/google/android/gms/common/server/n;)V
    .locals 2

    .prologue
    .line 125
    const-string v0, "AppContentAgent"

    sget-object v1, Lcom/google/android/gms/games/a/q;->b:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-direct {p0, v0, v1, p1}, Lcom/google/android/gms/games/a/bb;-><init>(Ljava/lang/String;Ljava/util/concurrent/locks/ReentrantLock;Lcom/google/android/gms/games/a/bb;)V

    .line 126
    new-instance v0, Lcom/google/android/gms/games/h/a/t;

    invoke-direct {v0, p2}, Lcom/google/android/gms/games/h/a/t;-><init>(Lcom/google/android/gms/common/server/n;)V

    iput-object v0, p0, Lcom/google/android/gms/games/a/q;->c:Lcom/google/android/gms/games/h/a/t;

    .line 127
    new-instance v0, Lcom/google/android/gms/games/b/f;

    invoke-direct {v0}, Lcom/google/android/gms/games/b/f;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/games/a/q;->a:Lcom/google/android/gms/games/b/f;

    .line 128
    new-instance v0, Lcom/google/android/gms/games/b/b;

    invoke-direct {v0}, Lcom/google/android/gms/games/b/b;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/games/a/q;->d:Lcom/google/android/gms/games/b/b;

    .line 129
    new-instance v0, Lcom/google/android/gms/games/b/c;

    invoke-direct {v0}, Lcom/google/android/gms/games/b/c;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/games/a/q;->e:Lcom/google/android/gms/games/b/c;

    .line 130
    new-instance v0, Lcom/google/android/gms/games/b/e;

    invoke-direct {v0}, Lcom/google/android/gms/games/b/e;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/games/a/q;->f:Lcom/google/android/gms/games/b/e;

    .line 131
    new-instance v0, Lcom/google/android/gms/games/b/g;

    invoke-direct {v0}, Lcom/google/android/gms/games/b/g;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/games/a/q;->g:Lcom/google/android/gms/games/b/g;

    .line 132
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/games/a/q;->h:Landroid/content/ContentValues;

    .line 133
    return-void
.end method

.method private a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Lcom/google/android/gms/games/a/s;Ljava/lang/String;J)Lcom/google/android/gms/games/h/a/bo;
    .locals 9

    .prologue
    .line 619
    iget-object v2, p3, Lcom/google/android/gms/games/a/s;->b:Ljava/lang/String;

    .line 620
    const/4 v1, 0x0

    .line 622
    iget-object v0, p0, Lcom/google/android/gms/games/a/q;->a:Lcom/google/android/gms/games/b/f;

    invoke-virtual {v0, v2, p5, p6}, Lcom/google/android/gms/games/b/f;->a(Ljava/lang/Object;J)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 624
    iget-object v0, p0, Lcom/google/android/gms/games/a/q;->a:Lcom/google/android/gms/games/b/f;

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Lcom/google/android/gms/games/b/f;->a(Ljava/lang/Object;I)V

    move-object v0, v1

    .line 664
    :cond_0
    :goto_0
    return-object v0

    .line 626
    :cond_1
    new-instance v0, Lcom/google/android/gms/common/e/b;

    invoke-static {p2}, Lcom/google/android/gms/games/provider/r;->a(Lcom/google/android/gms/common/server/ClientContext;)Landroid/net/Uri;

    move-result-object v3

    invoke-direct {v0, v3}, Lcom/google/android/gms/common/e/b;-><init>(Landroid/net/Uri;)V

    .line 627
    const-string v3, "experiments"

    invoke-virtual {p3}, Lcom/google/android/gms/games/a/s;->a()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Lcom/google/android/gms/common/e/b;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 629
    const-string v3, "screen_name"

    invoke-virtual {v0, v3, v2}, Lcom/google/android/gms/common/e/b;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 630
    if-eqz p4, :cond_2

    .line 631
    const-string v3, "page_token"

    invoke-virtual {v0, v3, p4}, Lcom/google/android/gms/common/e/b;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 635
    :cond_2
    invoke-static {p1, v0}, Lcom/google/android/gms/games/a/l;->a(Landroid/content/Context;Lcom/google/android/gms/common/e/b;)J

    move-result-wide v4

    const-wide/16 v6, 0x0

    cmp-long v3, v4, v6

    if-lez v3, :cond_3

    .line 636
    iget-object v0, v0, Lcom/google/android/gms/common/e/b;->a:Landroid/net/Uri;

    const-string v3, "json"

    invoke-static {p1, v0, v3}, Lcom/google/android/gms/games/a/l;->d(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;)[B

    move-result-object v3

    .line 639
    if-eqz v3, :cond_3

    .line 641
    :try_start_0
    const-class v0, Lcom/google/android/gms/games/h/a/bo;

    invoke-virtual {v0}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/h/a/bo;
    :try_end_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/InstantiationException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lcom/google/android/gms/common/server/response/m; {:try_start_0 .. :try_end_0} :catch_2

    .line 642
    const/16 v1, 0xc8

    :try_start_1
    invoke-virtual {v0, v1, v3}, Lcom/google/android/gms/games/h/a/bo;->a(I[B)V

    .line 644
    iget-object v1, p0, Lcom/google/android/gms/games/a/q;->a:Lcom/google/android/gms/games/b/f;

    const/4 v3, 0x3

    invoke-virtual {v1, v2, v3}, Lcom/google/android/gms/games/b/f;->a(Ljava/lang/Object;I)V
    :try_end_1
    .catch Ljava/lang/IllegalAccessException; {:try_start_1 .. :try_end_1} :catch_5
    .catch Ljava/lang/InstantiationException; {:try_start_1 .. :try_end_1} :catch_4
    .catch Lcom/google/android/gms/common/server/response/m; {:try_start_1 .. :try_end_1} :catch_3

    .line 659
    :goto_1
    if-nez v0, :cond_0

    .line 660
    iget-object v1, p0, Lcom/google/android/gms/games/a/q;->a:Lcom/google/android/gms/games/b/f;

    const/4 v3, 0x4

    invoke-virtual {v1, v2, v3}, Lcom/google/android/gms/games/b/f;->a(Ljava/lang/Object;I)V

    goto :goto_0

    .line 646
    :catch_0
    move-exception v0

    .line 648
    :goto_2
    const-string v3, "AppContentAgent"

    const-string v4, "Unable to recreate the App Content Response"

    invoke-static {v3, v4, v0}, Lcom/google/android/gms/games/internal/dq;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    move-object v0, v1

    .line 655
    goto :goto_1

    .line 649
    :catch_1
    move-exception v0

    .line 651
    :goto_3
    const-string v3, "AppContentAgent"

    const-string v4, "Unable to recreate the App Content Response"

    invoke-static {v3, v4, v0}, Lcom/google/android/gms/games/internal/dq;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    move-object v0, v1

    .line 655
    goto :goto_1

    .line 652
    :catch_2
    move-exception v0

    .line 654
    :goto_4
    const-string v3, "AppContentAgent"

    const-string v4, "Unable to recreate the App Content Response"

    invoke-static {v3, v4, v0}, Lcom/google/android/gms/games/internal/dq;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    move-object v0, v1

    goto :goto_1

    .line 652
    :catch_3
    move-exception v1

    move-object v8, v1

    move-object v1, v0

    move-object v0, v8

    goto :goto_4

    .line 649
    :catch_4
    move-exception v1

    move-object v8, v1

    move-object v1, v0

    move-object v0, v8

    goto :goto_3

    .line 646
    :catch_5
    move-exception v1

    move-object v8, v1

    move-object v1, v0

    move-object v0, v8

    goto :goto_2

    :cond_3
    move-object v0, v1

    goto :goto_1
.end method

.method private a(Ljava/lang/String;)Ljava/util/ArrayList;
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, -0x1

    .line 557
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 558
    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/gms/games/a/q;->a:Lcom/google/android/gms/games/b/f;

    invoke-virtual {v2, p1, v4, v3}, Lcom/google/android/gms/games/b/u;->a(Ljava/lang/Object;Landroid/os/Bundle;I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 560
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/gms/games/a/q;->d:Lcom/google/android/gms/games/b/b;

    invoke-virtual {v2, p1, v4, v3}, Lcom/google/android/gms/games/b/u;->a(Ljava/lang/Object;Landroid/os/Bundle;I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 561
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/gms/games/a/q;->e:Lcom/google/android/gms/games/b/c;

    invoke-virtual {v2, p1, v4, v3}, Lcom/google/android/gms/games/b/u;->a(Ljava/lang/Object;Landroid/os/Bundle;I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 562
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/android/gms/games/a/q;->g:Lcom/google/android/gms/games/b/g;

    invoke-virtual {v2, p1, v4, v3}, Lcom/google/android/gms/games/b/u;->a(Ljava/lang/Object;Landroid/os/Bundle;I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 563
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/android/gms/games/a/q;->f:Lcom/google/android/gms/games/b/e;

    invoke-virtual {v2, p1, v4, v3}, Lcom/google/android/gms/games/b/u;->a(Ljava/lang/Object;Landroid/os/Bundle;I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 564
    return-object v0
.end method

.method private static a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 599
    new-instance v0, Lcom/google/android/gms/common/e/b;

    invoke-static {p1}, Lcom/google/android/gms/games/provider/r;->a(Lcom/google/android/gms/common/server/ClientContext;)Landroid/net/Uri;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/gms/common/e/b;-><init>(Landroid/net/Uri;)V

    .line 600
    const-string v1, "screen_name"

    invoke-virtual {v0, v1, p2}, Lcom/google/android/gms/common/e/b;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 602
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    iget-object v0, v0, Lcom/google/android/gms/common/e/b;->a:Landroid/net/Uri;

    invoke-virtual {v1, v0, v2, v2}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 604
    return-void
.end method

.method private a(Lcom/google/android/gms/games/a/au;Ljava/util/ArrayList;Ljava/util/ArrayList;)V
    .locals 34

    .prologue
    .line 258
    new-instance v19, Lcom/google/android/gms/games/a/r;

    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/google/android/gms/games/a/au;->a:Landroid/content/Context;

    move-object/from16 v0, p1

    iget-object v3, v0, Lcom/google/android/gms/games/a/au;->b:Lcom/google/android/gms/common/server/ClientContext;

    move-object/from16 v0, v19

    move-object/from16 v1, p0

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/gms/games/a/r;-><init>(Lcom/google/android/gms/games/a/q;Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;)V

    .line 261
    const/4 v2, 0x1

    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v17

    check-cast v17, Ljava/util/ArrayList;

    .line 262
    const/4 v2, 0x0

    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    move-object/from16 v30, v2

    check-cast v30, Ljava/util/ArrayList;

    .line 263
    const/4 v2, 0x3

    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/util/ArrayList;

    .line 266
    invoke-virtual/range {p2 .. p2}, Ljava/util/ArrayList;->size()I

    move-result v32

    .line 267
    const/4 v2, 0x0

    move/from16 v31, v2

    :goto_0
    move/from16 v0, v31

    move/from16 v1, v32

    if-ge v0, v1, :cond_c

    .line 268
    move-object/from16 v0, p2

    move/from16 v1, v31

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/gms/games/h/a/aq;

    invoke-virtual {v2}, Lcom/google/android/gms/games/h/a/aq;->getSection()Lcom/google/android/gms/games/h/a/fb;

    move-result-object v33

    .line 269
    const-string v2, "Top-level card entries, must be sections."

    move-object/from16 v0, v33

    invoke-static {v0, v2}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 273
    new-instance v8, Landroid/content/ContentValues;

    move-object/from16 v0, v33

    iget-object v2, v0, Lcom/google/android/gms/common/server/response/a;->a:Landroid/content/ContentValues;

    invoke-direct {v8, v2}, Landroid/content/ContentValues;-><init>(Landroid/content/ContentValues;)V

    .line 274
    const-string v2, "section_background_image_default_id"

    const/4 v3, 0x0

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v8, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 276
    const-string v2, "section_id"

    invoke-virtual {v8, v2}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 278
    const-string v2, "CONTINUES"

    invoke-virtual/range {v33 .. v33}, Lcom/google/android/gms/games/h/a/fb;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    .line 279
    if-eqz v3, :cond_0

    .line 281
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/games/a/q;->h:Landroid/content/ContentValues;

    if-eqz v2, :cond_5

    const/4 v2, 0x1

    :goto_1
    const-string v5, "Continuing a previous section, but didn\'t have a previous section cached"

    invoke-static {v2, v5}, Lcom/google/android/gms/common/internal/bx;->a(ZLjava/lang/Object;)V

    .line 283
    new-instance v8, Landroid/content/ContentValues;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/games/a/q;->h:Landroid/content/ContentValues;

    invoke-direct {v8, v2}, Landroid/content/ContentValues;-><init>(Landroid/content/ContentValues;)V

    .line 287
    :cond_0
    if-nez v3, :cond_2

    .line 289
    const-string v2, "section_id"

    invoke-virtual {v8, v2, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 290
    invoke-virtual/range {v33 .. v33}, Lcom/google/android/gms/games/h/a/fb;->getSectionData()Ljava/util/ArrayList;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 292
    const-string v3, "section_data"

    const-string v5, "tuple_id"

    const-string v6, "TUPLE"

    new-instance v7, Ljava/util/ArrayList;

    invoke-virtual/range {v33 .. v33}, Lcom/google/android/gms/games/h/a/fb;->getSectionData()Ljava/util/ArrayList;

    move-result-object v2

    invoke-direct {v7, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    move-object/from16 v2, p0

    invoke-direct/range {v2 .. v9}, Lcom/google/android/gms/games/a/q;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;Landroid/content/ContentValues;Ljava/util/ArrayList;)V

    .line 297
    :cond_1
    invoke-virtual/range {v33 .. v33}, Lcom/google/android/gms/games/h/a/fb;->getActions()Ljava/util/ArrayList;

    move-result-object v2

    if-eqz v2, :cond_2

    .line 299
    const-string v11, "section_actions"

    const-string v13, "action_id"

    const-string v14, "ACTION"

    new-instance v15, Ljava/util/ArrayList;

    invoke-virtual/range {v33 .. v33}, Lcom/google/android/gms/games/h/a/fb;->getActions()Ljava/util/ArrayList;

    move-result-object v2

    invoke-direct {v15, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    move-object/from16 v10, p0

    move-object v12, v4

    move-object/from16 v16, v8

    move-object/from16 v18, p3

    invoke-direct/range {v10 .. v19}, Lcom/google/android/gms/games/a/q;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;Landroid/content/ContentValues;Ljava/util/ArrayList;Ljava/util/ArrayList;Lcom/google/android/gms/games/a/r;)V

    .line 307
    :cond_2
    invoke-virtual/range {v33 .. v33}, Lcom/google/android/gms/games/h/a/fb;->getEntries()Ljava/util/ArrayList;

    move-result-object v7

    .line 308
    if-eqz v7, :cond_3

    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-nez v2, :cond_6

    .line 310
    :cond_3
    move-object/from16 v0, v30

    invoke-virtual {v0, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 311
    invoke-virtual/range {v33 .. v33}, Lcom/google/android/gms/games/h/a/fb;->getBackgroundImage()Lcom/google/android/gms/games/h/a/bp;

    move-result-object v22

    const-string v23, "section_background_image_uri"

    const-string v24, "section_background_image_height"

    const-string v25, "section_background_image_width"

    move-object/from16 v20, v8

    move-object/from16 v21, v4

    invoke-virtual/range {v19 .. v25}, Lcom/google/android/gms/games/a/r;->a(Landroid/content/ContentValues;Ljava/lang/String;Lcom/google/android/gms/games/h/a/bp;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 336
    :cond_4
    move-object/from16 v0, p0

    iput-object v8, v0, Lcom/google/android/gms/games/a/q;->h:Landroid/content/ContentValues;

    .line 267
    add-int/lit8 v2, v31, 0x1

    move/from16 v31, v2

    goto/16 :goto_0

    .line 281
    :cond_5
    const/4 v2, 0x0

    goto :goto_1

    .line 319
    :cond_6
    const/4 v2, 0x0

    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v10

    move v6, v2

    :goto_2
    if-ge v6, v10, :cond_4

    .line 320
    invoke-virtual {v7, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/gms/games/h/a/aq;

    invoke-virtual {v2}, Lcom/google/android/gms/games/h/a/aq;->getCard()Lcom/google/android/gms/games/h/a/ah;

    move-result-object v11

    .line 321
    const-string v2, "2nd-level card entries, must be cards."

    invoke-static {v11, v2}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 322
    new-instance v26, Landroid/content/ContentValues;

    move-object/from16 v0, v26

    invoke-direct {v0, v8}, Landroid/content/ContentValues;-><init>(Landroid/content/ContentValues;)V

    .line 323
    const/4 v2, 0x1

    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v27

    check-cast v27, Ljava/util/ArrayList;

    const/4 v2, 0x2

    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/ArrayList;

    const/4 v3, 0x4

    move-object/from16 v0, p3

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/ArrayList;

    const/4 v5, 0x3

    move-object/from16 v0, p3

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/util/ArrayList;

    iget-object v12, v11, Lcom/google/android/gms/common/server/response/a;->a:Landroid/content/ContentValues;

    const-string v13, "card_id"

    invoke-virtual {v12, v13}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v22

    const-string v12, "card_id"

    move-object/from16 v0, v26

    move-object/from16 v1, v22

    invoke-virtual {v0, v12, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v12, v11, Lcom/google/android/gms/common/server/response/a;->a:Landroid/content/ContentValues;

    move-object/from16 v0, v26

    invoke-virtual {v0, v12}, Landroid/content/ContentValues;->putAll(Landroid/content/ContentValues;)V

    invoke-virtual {v11}, Lcom/google/android/gms/games/h/a/ah;->getActions()Ljava/util/ArrayList;

    move-result-object v12

    if-eqz v12, :cond_7

    const-string v21, "card_actions"

    const-string v23, "action_id"

    const-string v24, "ACTION"

    new-instance v25, Ljava/util/ArrayList;

    invoke-virtual {v11}, Lcom/google/android/gms/games/h/a/ah;->getActions()Ljava/util/ArrayList;

    move-result-object v12

    move-object/from16 v0, v25

    invoke-direct {v0, v12}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    move-object/from16 v20, p0

    move-object/from16 v28, p3

    move-object/from16 v29, v19

    invoke-direct/range {v20 .. v29}, Lcom/google/android/gms/games/a/q;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;Landroid/content/ContentValues;Ljava/util/ArrayList;Ljava/util/ArrayList;Lcom/google/android/gms/games/a/r;)V

    :cond_7
    invoke-virtual {v11}, Lcom/google/android/gms/games/h/a/ah;->getOverflowActions()Ljava/util/ArrayList;

    move-result-object v12

    if-eqz v12, :cond_8

    const-string v21, "card_overflow_actions"

    const-string v23, "action_id"

    const-string v24, "ACTION"

    new-instance v25, Ljava/util/ArrayList;

    invoke-virtual {v11}, Lcom/google/android/gms/games/h/a/ah;->getOverflowActions()Ljava/util/ArrayList;

    move-result-object v12

    move-object/from16 v0, v25

    invoke-direct {v0, v12}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    move-object/from16 v20, p0

    move-object/from16 v28, p3

    move-object/from16 v29, v19

    invoke-direct/range {v20 .. v29}, Lcom/google/android/gms/games/a/q;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;Landroid/content/ContentValues;Ljava/util/ArrayList;Ljava/util/ArrayList;Lcom/google/android/gms/games/a/r;)V

    :cond_8
    invoke-virtual {v11}, Lcom/google/android/gms/games/h/a/ah;->getAnnotations()Ljava/util/ArrayList;

    move-result-object v12

    if-eqz v12, :cond_9

    const-string v21, "card_annotations"

    const-string v23, "annotation_id"

    const-string v24, "ANNOTATION"

    new-instance v25, Ljava/util/ArrayList;

    invoke-virtual {v11}, Lcom/google/android/gms/games/h/a/ah;->getAnnotations()Ljava/util/ArrayList;

    move-result-object v12

    move-object/from16 v0, v25

    invoke-direct {v0, v12}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    const/16 v28, 0x0

    move-object/from16 v20, p0

    move-object/from16 v27, v2

    move-object/from16 v29, v19

    invoke-direct/range {v20 .. v29}, Lcom/google/android/gms/games/a/q;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;Landroid/content/ContentValues;Ljava/util/ArrayList;Ljava/util/ArrayList;Lcom/google/android/gms/games/a/r;)V

    :cond_9
    invoke-virtual {v11}, Lcom/google/android/gms/games/h/a/ah;->getConditions()Ljava/util/ArrayList;

    move-result-object v2

    if-eqz v2, :cond_a

    const-string v21, "card_conditions"

    const-string v23, "condition_id"

    const-string v24, "CONDITION"

    new-instance v25, Ljava/util/ArrayList;

    invoke-virtual {v11}, Lcom/google/android/gms/games/h/a/ah;->getConditions()Ljava/util/ArrayList;

    move-result-object v2

    move-object/from16 v0, v25

    invoke-direct {v0, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    move-object/from16 v20, p0

    move-object/from16 v27, v3

    move-object/from16 v28, p3

    move-object/from16 v29, v19

    invoke-direct/range {v20 .. v29}, Lcom/google/android/gms/games/a/q;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;Landroid/content/ContentValues;Ljava/util/ArrayList;Ljava/util/ArrayList;Lcom/google/android/gms/games/a/r;)V

    :cond_a
    invoke-virtual {v11}, Lcom/google/android/gms/games/h/a/ah;->getCardData()Ljava/util/ArrayList;

    move-result-object v2

    if-eqz v2, :cond_b

    const-string v21, "card_data"

    const-string v23, "tuple_id"

    const-string v24, "TUPLE"

    new-instance v25, Ljava/util/ArrayList;

    invoke-virtual {v11}, Lcom/google/android/gms/games/h/a/ah;->getCardData()Ljava/util/ArrayList;

    move-result-object v2

    move-object/from16 v0, v25

    invoke-direct {v0, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    move-object/from16 v20, p0

    move-object/from16 v27, v5

    invoke-direct/range {v20 .. v27}, Lcom/google/android/gms/games/a/q;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;Landroid/content/ContentValues;Ljava/util/ArrayList;)V

    .line 327
    :cond_b
    invoke-virtual/range {v33 .. v33}, Lcom/google/android/gms/games/h/a/fb;->getBackgroundImage()Lcom/google/android/gms/games/h/a/bp;

    move-result-object v22

    const-string v23, "section_background_image_uri"

    const-string v24, "section_background_image_height"

    const-string v25, "section_background_image_width"

    move-object/from16 v20, v26

    move-object/from16 v21, v4

    invoke-virtual/range {v19 .. v25}, Lcom/google/android/gms/games/a/r;->a(Landroid/content/ContentValues;Ljava/lang/String;Lcom/google/android/gms/games/h/a/bp;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 332
    move-object/from16 v0, v30

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 319
    add-int/lit8 v2, v6, 0x1

    move v6, v2

    goto/16 :goto_2

    .line 340
    :cond_c
    invoke-virtual/range {v19 .. v19}, Lcom/google/android/gms/games/a/r;->a()V

    .line 341
    return-void
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;Landroid/content/ContentValues;Ljava/util/ArrayList;)V
    .locals 10

    .prologue
    .line 462
    const/4 v8, 0x0

    const/4 v9, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    invoke-direct/range {v0 .. v9}, Lcom/google/android/gms/games/a/q;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;Landroid/content/ContentValues;Ljava/util/ArrayList;Ljava/util/ArrayList;Lcom/google/android/gms/games/a/r;)V

    .line 465
    return-void
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;Landroid/content/ContentValues;Ljava/util/ArrayList;Ljava/util/ArrayList;Lcom/google/android/gms/games/a/r;)V
    .locals 29

    .prologue
    .line 362
    const-string v2, "condition_id"

    move-object/from16 v0, p3

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v24

    .line 363
    if-eqz v24, :cond_0

    .line 364
    if-eqz p8, :cond_6

    const/4 v2, 0x1

    :goto_0
    const-string v3, "Must provide the content value collection for conditions"

    invoke-static {v2, v3}, Lcom/google/android/gms/common/internal/bx;->b(ZLjava/lang/Object;)V

    .line 367
    :cond_0
    if-nez p8, :cond_7

    const/4 v12, 0x0

    .line 371
    :goto_1
    const-string v2, "action_id"

    move-object/from16 v0, p3

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v25

    .line 372
    if-nez p8, :cond_8

    const/16 v20, 0x0

    .line 376
    :goto_2
    if-nez v25, :cond_1

    if-eqz v24, :cond_2

    .line 377
    :cond_1
    if-eqz p8, :cond_9

    const/4 v2, 0x1

    :goto_3
    const-string v3, "Must provide the content value collection for conditions and actions"

    invoke-static {v2, v3}, Lcom/google/android/gms/common/internal/bx;->b(ZLjava/lang/Object;)V

    .line 382
    :cond_2
    const-string v2, "annotation_id"

    move-object/from16 v0, p3

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v26

    .line 385
    new-instance v27, Ljava/lang/StringBuilder;

    invoke-direct/range {v27 .. v27}, Ljava/lang/StringBuilder;-><init>()V

    .line 386
    if-eqz p5, :cond_e

    .line 387
    const/4 v2, 0x0

    invoke-virtual/range {p5 .. p5}, Ljava/util/ArrayList;->size()I

    move-result v28

    move/from16 v23, v2

    :goto_4
    move/from16 v0, v23

    move/from16 v1, v28

    if-ge v0, v1, :cond_e

    .line 388
    if-eqz v23, :cond_3

    .line 389
    const-string v2, ","

    move-object/from16 v0, v27

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 392
    :cond_3
    move-object/from16 v0, p5

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    move-object v9, v2

    check-cast v9, Lcom/google/android/gms/common/server/response/a;

    .line 393
    iget-object v3, v9, Lcom/google/android/gms/common/server/response/a;->a:Landroid/content/ContentValues;

    .line 394
    move-object/from16 v0, p7

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 397
    if-eqz v25, :cond_a

    .line 398
    const-string v2, "action_id"

    invoke-virtual {v3, v2}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 410
    :goto_5
    move-object/from16 v0, v27

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 412
    if-eqz v26, :cond_4

    move-object v2, v9

    .line 413
    check-cast v2, Lcom/google/android/gms/games/h/a/aj;

    .line 414
    invoke-virtual {v2}, Lcom/google/android/gms/games/h/a/aj;->getImage()Lcom/google/android/gms/games/h/a/bp;

    move-result-object v5

    const-string v6, "annotation_image_uri"

    const-string v7, "annotation_image_height"

    const-string v8, "annotation_image_width"

    move-object/from16 v2, p9

    invoke-virtual/range {v2 .. v8}, Lcom/google/android/gms/games/a/r;->a(Landroid/content/ContentValues;Ljava/lang/String;Lcom/google/android/gms/games/h/a/bp;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 421
    :cond_4
    if-eqz v24, :cond_c

    instance-of v2, v9, Lcom/google/android/gms/games/h/a/ak;

    if-eqz v2, :cond_c

    move-object v2, v9

    .line 424
    check-cast v2, Lcom/google/android/gms/games/h/a/ak;

    .line 425
    invoke-virtual {v2}, Lcom/google/android/gms/games/h/a/ak;->getPredicateParameters()Ljava/util/ArrayList;

    move-result-object v5

    if-eqz v5, :cond_5

    .line 426
    const-string v6, "condition_predicate_parameters"

    const-string v8, "tuple_id"

    const-string v9, "TUPLE"

    new-instance v10, Ljava/util/ArrayList;

    invoke-virtual {v2}, Lcom/google/android/gms/games/h/a/ak;->getPredicateParameters()Ljava/util/ArrayList;

    move-result-object v2

    invoke-direct {v10, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    move-object/from16 v5, p0

    move-object v7, v4

    move-object v11, v3

    invoke-direct/range {v5 .. v12}, Lcom/google/android/gms/games/a/q;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;Landroid/content/ContentValues;Ljava/util/ArrayList;)V

    .line 387
    :cond_5
    :goto_6
    add-int/lit8 v2, v23, 0x1

    move/from16 v23, v2

    goto :goto_4

    .line 364
    :cond_6
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 367
    :cond_7
    const/4 v2, 0x3

    move-object/from16 v0, p8

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/ArrayList;

    move-object v12, v2

    goto/16 :goto_1

    .line 372
    :cond_8
    const/4 v2, 0x4

    move-object/from16 v0, p8

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/ArrayList;

    move-object/from16 v20, v2

    goto/16 :goto_2

    .line 377
    :cond_9
    const/4 v2, 0x0

    goto/16 :goto_3

    .line 399
    :cond_a
    invoke-static/range {p2 .. p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_b

    const-string v2, "%s%s%d"

    const/4 v4, 0x3

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object p4, v4, v5

    const/4 v5, 0x1

    const-string v6, "/"

    aput-object v6, v4, v5

    const/4 v5, 0x2

    invoke-static/range {v23 .. v23}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v2, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 407
    :goto_7
    move-object/from16 v0, p3

    invoke-virtual {v3, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    move-object v4, v2

    goto/16 :goto_5

    .line 399
    :cond_b
    const-string v2, "%s%s%s%s%d"

    const/4 v4, 0x5

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object p2, v4, v5

    const/4 v5, 0x1

    const-string v6, "/"

    aput-object v6, v4, v5

    const/4 v5, 0x2

    aput-object p4, v4, v5

    const/4 v5, 0x3

    const-string v6, "/"

    aput-object v6, v4, v5

    const/4 v5, 0x4

    invoke-static/range {v23 .. v23}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v2, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    goto :goto_7

    .line 433
    :cond_c
    if-eqz v25, :cond_5

    instance-of v2, v9, Lcom/google/android/gms/games/h/a/ai;

    if-eqz v2, :cond_5

    move-object v2, v9

    .line 435
    check-cast v2, Lcom/google/android/gms/games/h/a/ai;

    .line 436
    invoke-virtual {v2}, Lcom/google/android/gms/games/h/a/ai;->getConditions()Ljava/util/ArrayList;

    move-result-object v5

    if-eqz v5, :cond_d

    .line 437
    const-string v14, "action_conditions"

    const-string v16, "condition_id"

    const-string v17, "CONDITION"

    new-instance v18, Ljava/util/ArrayList;

    invoke-virtual {v2}, Lcom/google/android/gms/games/h/a/ai;->getConditions()Ljava/util/ArrayList;

    move-result-object v5

    move-object/from16 v0, v18

    invoke-direct {v0, v5}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    move-object/from16 v13, p0

    move-object v15, v4

    move-object/from16 v19, v3

    move-object/from16 v21, p8

    move-object/from16 v22, p9

    invoke-direct/range {v13 .. v22}, Lcom/google/android/gms/games/a/q;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;Landroid/content/ContentValues;Ljava/util/ArrayList;Ljava/util/ArrayList;Lcom/google/android/gms/games/a/r;)V

    .line 445
    :cond_d
    invoke-virtual {v2}, Lcom/google/android/gms/games/h/a/ai;->getActionParameters()Ljava/util/ArrayList;

    move-result-object v5

    if-eqz v5, :cond_5

    .line 446
    const-string v6, "action_data"

    const-string v8, "tuple_id"

    const-string v9, "TUPLE"

    new-instance v10, Ljava/util/ArrayList;

    invoke-virtual {v2}, Lcom/google/android/gms/games/h/a/ai;->getActionParameters()Ljava/util/ArrayList;

    move-result-object v2

    invoke-direct {v10, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    move-object/from16 v5, p0

    move-object v7, v4

    move-object v11, v3

    invoke-direct/range {v5 .. v12}, Lcom/google/android/gms/games/a/q;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;Landroid/content/ContentValues;Ljava/util/ArrayList;)V

    goto/16 :goto_6

    .line 456
    :cond_e
    invoke-virtual/range {v27 .. v27}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p6

    move-object/from16 v1, p1

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 457
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/a/s;)Ljava/util/ArrayList;
    .locals 17

    .prologue
    .line 145
    move-object/from16 v0, p2

    iget-object v12, v0, Lcom/google/android/gms/games/a/s;->b:Ljava/lang/String;

    .line 146
    invoke-static {}, Lcom/google/android/gms/common/util/r;->c()Lcom/google/android/gms/common/util/p;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/gms/common/util/p;->b()J

    move-result-wide v10

    .line 147
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/games/a/q;->a:Lcom/google/android/gms/games/b/f;

    move-object/from16 v0, p1

    iget-object v3, v0, Lcom/google/android/gms/games/a/au;->c:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lcom/google/android/gms/games/b/f;->a(Ljava/lang/Object;)V

    .line 148
    move-object/from16 v0, p1

    iget-object v8, v0, Lcom/google/android/gms/games/a/au;->a:Landroid/content/Context;

    .line 149
    move-object/from16 v0, p1

    iget-object v3, v0, Lcom/google/android/gms/games/a/au;->b:Lcom/google/android/gms/common/server/ClientContext;

    .line 152
    move-object/from16 v0, p1

    iget-boolean v2, v0, Lcom/google/android/gms/games/a/au;->g:Z

    if-eqz v2, :cond_0

    .line 153
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/games/a/q;->a:Lcom/google/android/gms/games/b/f;

    invoke-virtual {v2, v12}, Lcom/google/android/gms/games/b/f;->c(Ljava/lang/Object;)V

    .line 156
    :cond_0
    const/4 v9, 0x0

    .line 157
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/games/a/q;->a:Lcom/google/android/gms/games/b/f;

    invoke-virtual {v2, v12, v10, v11}, Lcom/google/android/gms/games/b/f;->a(Ljava/lang/Object;J)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 158
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/games/a/q;->a:Lcom/google/android/gms/games/b/f;

    invoke-virtual {v2, v12, v10, v11}, Lcom/google/android/gms/games/b/f;->b(Ljava/lang/Object;J)Ljava/lang/String;

    move-result-object v9

    .line 163
    :cond_1
    :try_start_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/games/a/q;->c:Lcom/google/android/gms/games/h/a/t;

    move-object/from16 v0, p2

    iget v4, v0, Lcom/google/android/gms/games/a/s;->c:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    move-object/from16 v0, p2

    iget-object v5, v0, Lcom/google/android/gms/games/a/s;->b:Ljava/lang/String;

    invoke-static {v8}, Lcom/google/android/gms/games/d/b;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual/range {p2 .. p2}, Lcom/google/android/gms/games/a/s;->a()Ljava/lang/String;

    move-result-object v7

    invoke-static {v8}, Lcom/google/android/gms/games/a/l;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v13

    const-string v14, "appContents/%1$s/screen/%2$s"

    const/4 v15, 0x2

    new-array v15, v15, [Ljava/lang/Object;

    const/16 v16, 0x0

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v15, v16

    const/4 v4, 0x1

    invoke-static {v5}, Lcom/google/android/gms/games/h/a/t;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v15, v4

    invoke-static {v14, v15}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    if-eqz v6, :cond_2

    const-string v4, "deviceType"

    invoke-static {v6}, Lcom/google/android/gms/games/h/a/t;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v4, v6}, Lcom/google/android/gms/games/h/a/t;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    :cond_2
    if-eqz v7, :cond_3

    const-string v4, "experimentOverride"

    invoke-static {v7}, Lcom/google/android/gms/games/h/a/t;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v4, v6}, Lcom/google/android/gms/games/h/a/t;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    :cond_3
    if-eqz v13, :cond_4

    const-string v4, "language"

    invoke-static {v13}, Lcom/google/android/gms/games/h/a/t;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v4, v6}, Lcom/google/android/gms/games/h/a/t;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    :cond_4
    if-eqz v9, :cond_5

    const-string v4, "pageToken"

    invoke-static {v9}, Lcom/google/android/gms/games/h/a/t;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v4, v6}, Lcom/google/android/gms/games/h/a/t;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    :cond_5
    iget-object v2, v2, Lcom/google/android/gms/games/h/a/t;->a:Lcom/google/android/gms/common/server/n;

    const/4 v4, 0x0

    const/4 v6, 0x0

    const-class v7, Lcom/google/android/gms/games/h/a/bo;

    invoke-virtual/range {v2 .. v7}, Lcom/google/android/gms/common/server/n;->a(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;Ljava/lang/Object;Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse;

    move-result-object v2

    check-cast v2, Lcom/google/android/gms/games/h/a/bo;

    .line 167
    if-nez v9, :cond_6

    move-object/from16 v0, p2

    iget-object v4, v0, Lcom/google/android/gms/games/a/s;->b:Ljava/lang/String;

    invoke-static {v8, v3, v4}, Lcom/google/android/gms/games/a/q;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)V

    :cond_6
    new-instance v4, Landroid/content/ContentValues;

    invoke-direct {v4}, Landroid/content/ContentValues;-><init>()V

    const-string v5, "experiments"

    invoke-virtual/range {p2 .. p2}, Lcom/google/android/gms/games/a/s;->a()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v5, "json"

    invoke-virtual {v2}, Lcom/google/android/gms/games/h/a/bo;->ah_()[B

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    const-string v5, "page_token"

    invoke-virtual {v4, v5, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v5, "screen_name"

    move-object/from16 v0, p2

    iget-object v6, v0, Lcom/google/android/gms/games/a/s;->b:Ljava/lang/String;

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v8}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    invoke-static {v3}, Lcom/google/android/gms/games/provider/r;->a(Lcom/google/android/gms/common/server/ClientContext;)Landroid/net/Uri;

    move-result-object v6

    invoke-virtual {v5, v6, v4}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    :try_end_0
    .catch Lcom/android/volley/ac; {:try_start_0 .. :try_end_0} :catch_0

    .line 179
    :cond_7
    new-instance v13, Ljava/util/ArrayList;

    const/4 v3, 0x5

    invoke-direct {v13, v3}, Ljava/util/ArrayList;-><init>(I)V

    .line 180
    const/4 v3, 0x0

    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {v13, v3, v4}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 181
    const/4 v3, 0x1

    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {v13, v3, v4}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 182
    const/4 v3, 0x2

    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {v13, v3, v4}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 183
    const/4 v3, 0x3

    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {v13, v3, v4}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 184
    const/4 v3, 0x4

    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {v13, v3, v4}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 187
    invoke-virtual {v2}, Lcom/google/android/gms/games/h/a/bo;->getItems()Ljava/util/ArrayList;

    move-result-object v3

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v3, v13}, Lcom/google/android/gms/games/a/q;->a(Lcom/google/android/gms/games/a/au;Ljava/util/ArrayList;Ljava/util/ArrayList;)V

    .line 188
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/gms/games/a/q;->d:Lcom/google/android/gms/games/b/b;

    const/4 v4, 0x1

    invoke-virtual {v13, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/util/ArrayList;

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual {v2}, Lcom/google/android/gms/games/h/a/bo;->b()Ljava/lang/String;

    move-result-object v8

    const/4 v9, 0x0

    move-object v4, v12

    invoke-virtual/range {v3 .. v11}, Lcom/google/android/gms/games/b/b;->a(Ljava/lang/Object;Ljava/util/ArrayList;ILjava/lang/String;Ljava/lang/String;IJ)V

    .line 191
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/gms/games/a/q;->e:Lcom/google/android/gms/games/b/c;

    const/4 v4, 0x2

    invoke-virtual {v13, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/util/ArrayList;

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual {v2}, Lcom/google/android/gms/games/h/a/bo;->b()Ljava/lang/String;

    move-result-object v8

    const/4 v9, 0x0

    move-object v4, v12

    invoke-virtual/range {v3 .. v11}, Lcom/google/android/gms/games/b/c;->a(Ljava/lang/Object;Ljava/util/ArrayList;ILjava/lang/String;Ljava/lang/String;IJ)V

    .line 194
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/gms/games/a/q;->f:Lcom/google/android/gms/games/b/e;

    const/4 v4, 0x4

    invoke-virtual {v13, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/util/ArrayList;

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual {v2}, Lcom/google/android/gms/games/h/a/bo;->b()Ljava/lang/String;

    move-result-object v8

    const/4 v9, 0x0

    move-object v4, v12

    invoke-virtual/range {v3 .. v11}, Lcom/google/android/gms/games/b/e;->a(Ljava/lang/Object;Ljava/util/ArrayList;ILjava/lang/String;Ljava/lang/String;IJ)V

    .line 197
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/gms/games/a/q;->g:Lcom/google/android/gms/games/b/g;

    const/4 v4, 0x3

    invoke-virtual {v13, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/util/ArrayList;

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual {v2}, Lcom/google/android/gms/games/h/a/bo;->b()Ljava/lang/String;

    move-result-object v8

    const/4 v9, 0x0

    move-object v4, v12

    invoke-virtual/range {v3 .. v11}, Lcom/google/android/gms/games/b/g;->a(Ljava/lang/Object;Ljava/util/ArrayList;ILjava/lang/String;Ljava/lang/String;IJ)V

    .line 200
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/gms/games/a/q;->a:Lcom/google/android/gms/games/b/f;

    const/4 v4, 0x0

    invoke-virtual {v13, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/util/ArrayList;

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual {v2}, Lcom/google/android/gms/games/h/a/bo;->b()Ljava/lang/String;

    move-result-object v8

    const/4 v9, 0x0

    move-object v4, v12

    invoke-virtual/range {v3 .. v11}, Lcom/google/android/gms/games/b/f;->a(Ljava/lang/Object;Ljava/util/ArrayList;ILjava/lang/String;Ljava/lang/String;IJ)V

    .line 203
    move-object/from16 v0, p0

    invoke-direct {v0, v12}, Lcom/google/android/gms/games/a/q;->a(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v2

    :goto_0
    return-object v2

    .line 168
    :catch_0
    move-exception v2

    const-string v4, "AppContentAgent"

    invoke-static {v2, v4}, Lcom/google/android/gms/common/server/b/c;->a(Lcom/android/volley/ac;Ljava/lang/String;)Lcom/google/android/gms/common/server/b/b;

    move-object/from16 v5, p0

    move-object v6, v8

    move-object v7, v3

    move-object/from16 v8, p2

    .line 170
    invoke-direct/range {v5 .. v11}, Lcom/google/android/gms/games/a/q;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Lcom/google/android/gms/games/a/s;Ljava/lang/String;J)Lcom/google/android/gms/games/h/a/bo;

    move-result-object v2

    .line 173
    if-nez v2, :cond_7

    .line 174
    move-object/from16 v0, p0

    invoke-direct {v0, v12}, Lcom/google/android/gms/games/a/q;->a(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v2

    goto :goto_0
.end method

.method public final a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;[Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 215
    array-length v1, p3

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    aget-object v2, p3, v0

    .line 216
    iget-object v3, p0, Lcom/google/android/gms/games/a/q;->a:Lcom/google/android/gms/games/b/f;

    invoke-virtual {v3, v2}, Lcom/google/android/gms/games/b/f;->c(Ljava/lang/Object;)V

    .line 217
    invoke-static {p1, p2, v2}, Lcom/google/android/gms/games/a/q;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)V

    .line 215
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 219
    :cond_0
    return-void
.end method
