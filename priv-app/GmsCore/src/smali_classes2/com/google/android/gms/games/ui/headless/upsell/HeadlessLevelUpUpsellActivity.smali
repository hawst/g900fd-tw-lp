.class public Lcom/google/android/gms/games/ui/headless/upsell/HeadlessLevelUpUpsellActivity;
.super Lcom/google/android/gms/games/ui/headless/a;
.source "SourceFile"


# instance fields
.field private i:Z


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 45
    sget v0, Lcom/google/android/gms/l;->bb:I

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/games/ui/headless/a;-><init>(II)V

    .line 46
    return-void
.end method

.method public constructor <init>(Z)V
    .locals 2

    .prologue
    .line 49
    sget v0, Lcom/google/android/gms/l;->bb:I

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1, p1}, Lcom/google/android/gms/games/ui/headless/a;-><init>(IIZ)V

    .line 50
    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/games/ui/headless/upsell/HeadlessLevelUpUpsellActivity;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 31
    iget-boolean v0, p0, Lcom/google/android/gms/games/ui/q;->c:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "Cannot try to install a game while already waiting for another install"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/e;->a(ZLjava/lang/Object;)V

    const-string v0, "currentAccountName cannot be null"

    invoke-static {p1, v0}, Lcom/google/android/gms/common/internal/e;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    const-string v0, "game package to install cannot be null"

    invoke-static {p2, v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/q;->d:Ljava/lang/String;

    invoke-static {p0, p1, p2}, Lcom/google/android/gms/common/internal/ay;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const/16 v1, 0x7d0

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/games/ui/q;->startActivityForResult(Landroid/content/Intent;I)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final D()V
    .locals 1

    .prologue
    .line 100
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/games/ui/headless/upsell/HeadlessLevelUpUpsellActivity;->i:Z

    .line 101
    return-void
.end method

.method public final E()V
    .locals 3

    .prologue
    .line 105
    const-string v0, "com.google.android.gms.games.ui.dialog.progressDialogInstalling"

    invoke-static {p0, v0}, Lcom/google/android/gms/games/ui/d/a;->dismiss(Landroid/support/v4/app/q;Ljava/lang/String;)V

    .line 107
    iget-object v0, p0, Lcom/google/android/gms/games/ui/q;->b:Lcom/google/android/gms/games/ui/bh;

    .line 109
    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/bh;->g()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-static {p0, v0, v1, v2}, Lcom/google/android/gms/games/ui/a/c;->a(Landroid/app/Activity;Ljava/lang/String;ZLandroid/os/Bundle;)V

    .line 111
    return-void
.end method

.method protected final K()V
    .locals 2

    .prologue
    .line 85
    invoke-super {p0}, Lcom/google/android/gms/games/ui/headless/a;->K()V

    .line 89
    invoke-super {p0}, Landroid/support/v7/app/d;->d()Landroid/support/v7/app/e;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v7/app/e;->b()Landroid/support/v7/app/a;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v7/app/a;->f()V

    .line 90
    invoke-super {p0}, Landroid/support/v7/app/d;->d()Landroid/support/v7/app/e;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v7/app/e;->b()Landroid/support/v7/app/a;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v7/app/a;->a(Z)V

    .line 91
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 54
    invoke-super {p0, p1}, Lcom/google/android/gms/games/ui/headless/a;->onCreate(Landroid/os/Bundle;)V

    .line 56
    invoke-static {p0}, Lcom/google/android/gms/games/ui/d/al;->b(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 57
    new-instance v0, Lcom/google/android/gms/games/ui/headless/upsell/a;

    invoke-direct {v0}, Lcom/google/android/gms/games/ui/headless/upsell/a;-><init>()V

    .line 58
    const-string v1, "com.google.android.gms.games.ui.dialog.installDialog"

    invoke-static {p0, v0, v1}, Lcom/google/android/gms/games/ui/d/a;->a(Landroid/support/v4/app/q;Landroid/support/v4/app/m;Ljava/lang/String;)V

    .line 67
    :cond_0
    :goto_0
    return-void

    .line 62
    :cond_1
    invoke-static {p0}, Lcom/google/android/gms/games/ui/d/al;->c(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 63
    invoke-static {}, Lcom/google/android/gms/games/ui/b/g;->b()Lcom/google/android/gms/games/ui/b/g;

    move-result-object v0

    .line 65
    const-string v1, "com.google.android.gms.games.ui.dialog.upgradeDialog"

    invoke-static {p0, v0, v1}, Lcom/google/android/gms/games/ui/d/a;->a(Landroid/support/v4/app/q;Landroid/support/v4/app/m;Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected onResume()V
    .locals 2

    .prologue
    .line 71
    invoke-super {p0}, Lcom/google/android/gms/games/ui/headless/a;->onResume()V

    .line 73
    iget-boolean v0, p0, Lcom/google/android/gms/games/ui/headless/upsell/HeadlessLevelUpUpsellActivity;->i:Z

    if-eqz v0, :cond_0

    .line 74
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/games/ui/headless/upsell/HeadlessLevelUpUpsellActivity;->i:Z

    .line 76
    sget v0, Lcom/google/android/gms/p;->jO:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/headless/upsell/HeadlessLevelUpUpsellActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 77
    invoke-static {v0}, Lcom/google/android/gms/games/ui/b/f;->a(Ljava/lang/String;)Lcom/google/android/gms/games/ui/b/f;

    move-result-object v0

    .line 78
    const-string v1, "com.google.android.gms.games.ui.dialog.progressDialogInstalling"

    invoke-static {p0, v0, v1}, Lcom/google/android/gms/games/ui/d/a;->a(Landroid/support/v4/app/q;Landroid/support/v4/app/m;Ljava/lang/String;)V

    .line 81
    :cond_0
    return-void
.end method
