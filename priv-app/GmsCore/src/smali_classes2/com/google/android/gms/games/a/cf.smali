.class Lcom/google/android/gms/games/a/cf;
.super Lcom/google/android/gms/games/a/bb;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/games/a/ax;


# static fields
.field private static final a:Ljava/lang/String;

.field private static final b:Ljava/util/concurrent/locks/ReentrantLock;


# instance fields
.field private final c:Ljava/util/Random;

.field private final d:Lcom/google/android/gms/games/h/a/fu;

.field private final e:Lcom/google/android/gms/games/h/a/fv;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 82
    const-class v0, Lcom/google/android/gms/games/a/cf;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/games/a/cf;->a:Ljava/lang/String;

    .line 84
    new-instance v0, Ljava/util/concurrent/locks/ReentrantLock;

    invoke-direct {v0}, Ljava/util/concurrent/locks/ReentrantLock;-><init>()V

    sput-object v0, Lcom/google/android/gms/games/a/cf;->b:Ljava/util/concurrent/locks/ReentrantLock;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/gms/games/a/bb;Lcom/google/android/gms/common/server/n;Lcom/google/android/gms/common/server/n;)V
    .locals 4

    .prologue
    .line 149
    sget-object v0, Lcom/google/android/gms/games/a/cf;->a:Ljava/lang/String;

    sget-object v1, Lcom/google/android/gms/games/a/cf;->b:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-direct {p0, v0, v1, p1}, Lcom/google/android/gms/games/a/bb;-><init>(Ljava/lang/String;Ljava/util/concurrent/locks/ReentrantLock;Lcom/google/android/gms/games/a/bb;)V

    .line 150
    new-instance v0, Ljava/util/Random;

    invoke-static {}, Lcom/google/android/gms/common/util/r;->c()Lcom/google/android/gms/common/util/p;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/gms/common/util/p;->a()J

    move-result-wide v2

    invoke-direct {v0, v2, v3}, Ljava/util/Random;-><init>(J)V

    iput-object v0, p0, Lcom/google/android/gms/games/a/cf;->c:Ljava/util/Random;

    .line 151
    new-instance v0, Lcom/google/android/gms/games/h/a/fu;

    invoke-direct {v0, p2}, Lcom/google/android/gms/games/h/a/fu;-><init>(Lcom/google/android/gms/common/server/n;)V

    iput-object v0, p0, Lcom/google/android/gms/games/a/cf;->d:Lcom/google/android/gms/games/h/a/fu;

    .line 152
    new-instance v0, Lcom/google/android/gms/games/h/a/fv;

    invoke-direct {v0, p3}, Lcom/google/android/gms/games/h/a/fv;-><init>(Lcom/google/android/gms/common/server/n;)V

    iput-object v0, p0, Lcom/google/android/gms/games/a/cf;->e:Lcom/google/android/gms/games/h/a/fv;

    .line 153
    return-void
.end method

.method private a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Landroid/net/Uri;)I
    .locals 7

    .prologue
    const/4 v0, 0x0

    const/4 v6, 0x0

    .line 438
    .line 440
    :try_start_0
    invoke-virtual {p2}, Lcom/google/android/gms/common/server/ClientContext;->f()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 441
    iget-object v1, p0, Lcom/google/android/gms/games/a/cf;->e:Lcom/google/android/gms/games/h/a/fv;

    const-string v2, "turnbasedmatches/%1$s/dismiss"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {p3}, Lcom/google/android/gms/games/h/a/fv;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    iget-object v1, v1, Lcom/google/android/gms/games/h/a/fv;->a:Lcom/google/android/gms/common/server/n;

    const/4 v3, 0x2

    const/4 v4, 0x0

    invoke-virtual {v1, p2, v3, v2, v4}, Lcom/google/android/gms/common/server/n;->a(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;Ljava/lang/Object;)V
    :try_end_0
    .catch Lcom/android/volley/ac; {:try_start_0 .. :try_end_0} :catch_0

    .line 452
    :goto_0
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    invoke-virtual {v1, p4, v6, v6}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 453
    return v0

    .line 443
    :cond_0
    :try_start_1
    iget-object v1, p0, Lcom/google/android/gms/games/a/cf;->d:Lcom/google/android/gms/games/h/a/fu;

    const-string v2, "turnbasedmatches/%1$s/dismiss"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {p3}, Lcom/google/android/gms/games/h/a/fu;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    iget-object v1, v1, Lcom/google/android/gms/games/h/a/fu;->a:Lcom/google/android/gms/common/server/n;

    const/4 v3, 0x2

    const/4 v4, 0x0

    invoke-virtual {v1, p2, v3, v2, v4}, Lcom/google/android/gms/common/server/n;->a(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;Ljava/lang/Object;)V
    :try_end_1
    .catch Lcom/android/volley/ac; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 445
    :catch_0
    move-exception v0

    .line 446
    sget-object v1, Lcom/google/android/gms/games/a/cf;->a:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Failed to dismiss: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/android/volley/ac;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/gms/games/internal/dq;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 447
    sget-object v1, Lcom/google/android/gms/games/a/cf;->a:Ljava/lang/String;

    const/4 v2, 0x5

    invoke-static {v1, v0, v2}, Lcom/google/android/gms/games/a/bf;->a(Ljava/lang/String;Lcom/android/volley/ac;I)I

    move-result v0

    goto :goto_0
.end method

.method private a(Landroid/database/Cursor;Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;)I
    .locals 10

    .prologue
    const/4 v9, 0x4

    const/4 v8, 0x3

    const/4 v0, 0x2

    .line 1108
    const/4 v1, 0x1

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    .line 1109
    invoke-interface {p1, v8}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 1110
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 1111
    const/4 v4, 0x7

    invoke-interface {p1, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 1112
    invoke-interface {p1, v9}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 1115
    new-instance v6, Lcom/google/android/gms/games/h/a/fs;

    invoke-direct {v6}, Lcom/google/android/gms/games/h/a/fs;-><init>()V

    .line 1116
    new-instance v7, Lcom/google/android/gms/common/server/response/d;

    invoke-direct {v7}, Lcom/google/android/gms/common/server/response/d;-><init>()V

    .line 1118
    :try_start_0
    invoke-virtual {v7, v4, v6}, Lcom/google/android/gms/common/server/response/d;->a(Ljava/lang/String;Lcom/google/android/gms/common/server/response/FastJsonResponse;)V
    :try_end_0
    .catch Lcom/google/android/gms/common/server/response/m; {:try_start_0 .. :try_end_0} :catch_0

    .line 1126
    if-ne v1, v8, :cond_1

    .line 1127
    new-instance v0, Lcom/google/android/gms/games/h/a/ft;

    invoke-virtual {v6}, Lcom/google/android/gms/games/h/a/fs;->getData()Lcom/google/android/gms/games/h/a/fo;

    move-result-object v1

    invoke-virtual {v6}, Lcom/google/android/gms/games/h/a/fs;->b()Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v6}, Lcom/google/android/gms/games/h/a/fs;->getResults()Ljava/util/ArrayList;

    move-result-object v6

    invoke-direct {v0, v1, v4, v5, v6}, Lcom/google/android/gms/games/h/a/ft;-><init>(Lcom/google/android/gms/games/h/a/fo;Ljava/lang/Integer;Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 1129
    invoke-direct {p0, p2, p3, v2, v0}, Lcom/google/android/gms/games/a/cf;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Lcom/google/android/gms/games/h/a/ft;)Landroid/util/Pair;

    move-result-object v0

    move-object v1, v0

    .line 1137
    :goto_0
    iget-object v0, v1, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v2

    .line 1138
    iget-object v0, v1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/gms/games/h/a/fl;

    .line 1139
    if-eqz v0, :cond_0

    .line 1140
    invoke-static {p2, p3, v3}, Lcom/google/android/gms/games/a/au;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)Lcom/google/android/gms/games/a/au;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/google/android/gms/games/a/cf;->a(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/h/a/fl;)Z

    .line 1143
    :cond_0
    invoke-static {v2}, Lcom/google/android/gms/games/a/cf;->b(I)I

    move-result v0

    :goto_1
    return v0

    .line 1120
    :catch_0
    move-exception v1

    sget-object v1, Lcom/google/android/gms/games/a/cf;->a:Ljava/lang/String;

    const-string v2, "Failed to parse offline update. Aborting."

    invoke-static {v1, v2}, Lcom/google/android/gms/games/internal/dq;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 1130
    :cond_1
    if-ne v1, v9, :cond_2

    .line 1131
    invoke-direct {p0, p2, p3, v2, v6}, Lcom/google/android/gms/games/a/cf;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Lcom/google/android/gms/games/h/a/fs;)Landroid/util/Pair;

    move-result-object v0

    move-object v1, v0

    goto :goto_0

    .line 1133
    :cond_2
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unknown update type "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private a(Lcom/google/android/gms/games/a/au;Ljava/lang/String;IZLjava/lang/String;)I
    .locals 11

    .prologue
    .line 866
    iget-object v8, p1, Lcom/google/android/gms/games/a/au;->a:Landroid/content/Context;

    .line 867
    iget-object v1, p1, Lcom/google/android/gms/games/a/au;->b:Lcom/google/android/gms/common/server/ClientContext;

    .line 869
    const/4 v7, 0x0

    .line 870
    invoke-static {v8}, Lcom/google/android/gms/games/a/l;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 871
    const/4 v6, 0x0

    .line 873
    if-eqz p4, :cond_3

    .line 874
    :try_start_0
    iget-object v2, p0, Lcom/google/android/gms/games/a/cf;->d:Lcom/google/android/gms/games/h/a/fu;

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    const-string v4, "turnbasedmatches/%1$s/leaveTurn"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v9, 0x0

    invoke-static {p2}, Lcom/google/android/gms/games/h/a/fu;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    aput-object v10, v5, v9

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    const-string v5, "matchVersion"

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v4, v5, v3}, Lcom/google/android/gms/games/h/a/fu;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    if-eqz v0, :cond_0

    const-string v4, "language"

    invoke-static {v0}, Lcom/google/android/gms/games/h/a/fu;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v4, v0}, Lcom/google/android/gms/games/h/a/fu;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    :cond_0
    if-eqz p5, :cond_1

    const-string v0, "pendingParticipantId"

    invoke-static/range {p5 .. p5}, Lcom/google/android/gms/games/h/a/fu;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v0, v4}, Lcom/google/android/gms/games/h/a/fu;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    :cond_1
    iget-object v0, v2, Lcom/google/android/gms/games/h/a/fu;->a:Lcom/google/android/gms/common/server/n;

    const/4 v2, 0x2

    const/4 v4, 0x0

    const-class v5, Lcom/google/android/gms/games/h/a/fl;

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/common/server/n;->a(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;Ljava/lang/Object;Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/h/a/fl;
    :try_end_0
    .catch Lcom/android/volley/ac; {:try_start_0 .. :try_end_0} :catch_0

    move-object v2, v0

    move v0, v7

    .line 892
    :goto_0
    const/16 v3, 0x1967

    if-ne v0, v3, :cond_5

    .line 895
    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/gms/games/a/cf;->a(Lcom/google/android/gms/games/a/au;Ljava/lang/String;I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    .line 896
    invoke-virtual {v0}, Lcom/google/android/gms/common/data/DataHolder;->f()I

    move-result v0

    .line 908
    :cond_2
    :goto_1
    return v0

    .line 877
    :cond_3
    :try_start_1
    iget-object v2, p0, Lcom/google/android/gms/games/a/cf;->d:Lcom/google/android/gms/games/h/a/fu;

    const-string v3, "turnbasedmatches/%1$s/leave"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-static {p2}, Lcom/google/android/gms/games/h/a/fu;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    aput-object v9, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    if-eqz v0, :cond_4

    const-string v4, "language"

    invoke-static {v0}, Lcom/google/android/gms/games/h/a/fu;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v4, v0}, Lcom/google/android/gms/games/h/a/fu;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    :cond_4
    iget-object v0, v2, Lcom/google/android/gms/games/h/a/fu;->a:Lcom/google/android/gms/common/server/n;

    const/4 v2, 0x2

    const/4 v4, 0x0

    const-class v5, Lcom/google/android/gms/games/h/a/fl;

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/common/server/n;->a(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;Ljava/lang/Object;Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/h/a/fl;
    :try_end_1
    .catch Lcom/android/volley/ac; {:try_start_1 .. :try_end_1} :catch_0

    move-object v2, v0

    move v0, v7

    .line 890
    goto :goto_0

    .line 879
    :catch_0
    move-exception v0

    .line 880
    sget-object v2, Lcom/google/android/gms/games/a/cf;->a:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Failed to leave match: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/android/volley/ac;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/gms/games/internal/dq;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 881
    sget-object v2, Lcom/google/android/gms/games/a/cf;->a:Ljava/lang/String;

    const/4 v3, 0x5

    invoke-static {v2, v0, v3}, Lcom/google/android/gms/games/a/bf;->a(Ljava/lang/String;Lcom/android/volley/ac;I)I

    move-result v2

    .line 886
    const/16 v3, 0x194

    invoke-static {v0, v3}, Lcom/google/android/gms/common/server/b/c;->a(Lcom/android/volley/ac;I)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 887
    sget-object v0, Lcom/google/android/gms/games/a/cf;->a:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Server had no record for "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " while leaving."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/google/android/gms/games/internal/dq;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 888
    const/4 v0, 0x0

    move-object v2, v6

    goto :goto_0

    .line 899
    :cond_5
    if-eqz v2, :cond_6

    .line 901
    invoke-static {p1, v2}, Lcom/google/android/gms/games/a/cf;->a(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/h/a/fl;)Z

    goto :goto_1

    .line 902
    :cond_6
    if-eqz v0, :cond_7

    const/4 v2, 0x5

    if-ne v0, v2, :cond_2

    .line 905
    :cond_7
    invoke-static {v1, p2}, Lcom/google/android/gms/games/provider/ak;->a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 906
    invoke-virtual {v8}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-virtual {v2, v1, v3, v4}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    goto/16 :goto_1

    :cond_8
    move v0, v2

    move-object v2, v6

    goto/16 :goto_0
.end method

.method private a(Lcom/google/android/gms/games/a/au;Ljava/lang/String;Z)I
    .locals 12

    .prologue
    const/4 v11, 0x5

    const/4 v6, 0x1

    const/4 v8, 0x0

    const/4 v7, 0x0

    .line 310
    iget-object v9, p1, Lcom/google/android/gms/games/a/au;->a:Landroid/content/Context;

    .line 311
    iget-object v1, p1, Lcom/google/android/gms/games/a/au;->b:Lcom/google/android/gms/common/server/ClientContext;

    .line 316
    :try_start_0
    iget-boolean v0, p1, Lcom/google/android/gms/games/a/au;->h:Z

    if-eqz v0, :cond_1

    .line 317
    iget-object v0, p0, Lcom/google/android/gms/games/a/cf;->e:Lcom/google/android/gms/games/h/a/fv;

    invoke-static {v9}, Lcom/google/android/gms/games/a/l;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "turnbasedmatches/%1$s/decline"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-static {p2}, Lcom/google/android/gms/games/h/a/fv;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    aput-object v10, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    if-eqz v2, :cond_0

    const-string v4, "language"

    invoke-static {v2}, Lcom/google/android/gms/games/h/a/fv;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v4, v2}, Lcom/google/android/gms/games/h/a/fv;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    :cond_0
    iget-object v0, v0, Lcom/google/android/gms/games/h/a/fv;->a:Lcom/google/android/gms/common/server/n;

    const/4 v2, 0x2

    const/4 v4, 0x0

    const-class v5, Lcom/google/android/gms/games/h/a/fl;

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/common/server/n;->a(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;Ljava/lang/Object;Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/h/a/fl;
    :try_end_0
    .catch Lcom/android/volley/ac; {:try_start_0 .. :try_end_0} :catch_0

    move v2, v7

    .line 338
    :goto_0
    if-eqz v2, :cond_4

    .line 340
    sget-object v3, Lcom/google/android/gms/games/a/cf;->a:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Declining invitation failed on server: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/android/gms/games/internal/dq;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 341
    if-eq v2, v11, :cond_3

    move v0, v2

    .line 363
    :goto_1
    return v0

    .line 320
    :cond_1
    :try_start_1
    iget-object v0, p0, Lcom/google/android/gms/games/a/cf;->d:Lcom/google/android/gms/games/h/a/fu;

    invoke-static {v9}, Lcom/google/android/gms/games/a/l;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "turnbasedmatches/%1$s/decline"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-static {p2}, Lcom/google/android/gms/games/h/a/fu;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    aput-object v10, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    if-eqz v2, :cond_2

    const-string v4, "language"

    invoke-static {v2}, Lcom/google/android/gms/games/h/a/fu;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v4, v2}, Lcom/google/android/gms/games/h/a/fu;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    :cond_2
    iget-object v0, v0, Lcom/google/android/gms/games/h/a/fu;->a:Lcom/google/android/gms/common/server/n;

    const/4 v2, 0x2

    const/4 v4, 0x0

    const-class v5, Lcom/google/android/gms/games/h/a/fl;

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/common/server/n;->a(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;Ljava/lang/Object;Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/h/a/fl;
    :try_end_1
    .catch Lcom/android/volley/ac; {:try_start_1 .. :try_end_1} :catch_0

    move v2, v7

    .line 335
    goto :goto_0

    .line 323
    :catch_0
    move-exception v0

    .line 324
    sget-object v2, Lcom/google/android/gms/games/a/cf;->a:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Failed to decline invitation: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/android/volley/ac;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/gms/games/internal/dq;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 325
    sget-object v2, Lcom/google/android/gms/games/a/cf;->a:Ljava/lang/String;

    invoke-static {v2, v0, v11}, Lcom/google/android/gms/games/a/bf;->a(Ljava/lang/String;Lcom/android/volley/ac;I)I

    move-result v2

    .line 330
    const/16 v3, 0x194

    invoke-static {v0, v3}, Lcom/google/android/gms/common/server/b/c;->a(Lcom/android/volley/ac;I)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 331
    sget-object v0, Lcom/google/android/gms/games/a/cf;->a:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Server had no record for "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " while declining."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/google/android/gms/games/internal/dq;->d(Ljava/lang/String;Ljava/lang/String;)V

    move-object v0, v8

    move v2, v7

    .line 333
    goto/16 :goto_0

    .line 348
    :cond_3
    if-eqz p3, :cond_4

    .line 349
    invoke-static {p1, v7, p2}, Lcom/google/android/gms/games/a/cf;->a(Lcom/google/android/gms/games/a/au;ILjava/lang/String;)V

    .line 355
    :cond_4
    if-eqz v0, :cond_5

    .line 356
    invoke-virtual {v0}, Lcom/google/android/gms/games/h/a/fl;->f()Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v3

    .line 357
    if-ne v3, v11, :cond_6

    move v0, v6

    :goto_2
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Match was supposed to be DELETED, but instead is "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v3}, Lcom/google/android/gms/games/internal/b/j;->a(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Lcom/google/android/gms/common/internal/e;->a(ZLjava/lang/Object;)V

    .line 361
    :cond_5
    invoke-static {v1, p2}, Lcom/google/android/gms/games/provider/ae;->a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 362
    invoke-virtual {v9}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    invoke-virtual {v1, v0, v8, v8}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    move v0, v2

    .line 363
    goto/16 :goto_1

    :cond_6
    move v0, v7

    .line 357
    goto :goto_2

    :cond_7
    move-object v0, v8

    goto/16 :goto_0
.end method

.method private a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Lcom/google/android/gms/games/h/a/fs;)Landroid/util/Pair;
    .locals 8

    .prologue
    const/4 v7, 0x0

    .line 798
    .line 799
    const/4 v6, 0x0

    .line 801
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/games/a/cf;->d:Lcom/google/android/gms/games/h/a/fu;

    invoke-static {p1}, Lcom/google/android/gms/games/a/l;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "turnbasedmatches/%1$s/finish"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {p3}, Lcom/google/android/gms/games/h/a/fu;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    if-eqz v1, :cond_0

    const-string v2, "language"

    invoke-static {v1}, Lcom/google/android/gms/games/h/a/fu;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v3, v2, v1}, Lcom/google/android/gms/games/h/a/fu;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    :cond_0
    iget-object v0, v0, Lcom/google/android/gms/games/h/a/fu;->a:Lcom/google/android/gms/common/server/n;

    const/4 v2, 0x2

    const-class v5, Lcom/google/android/gms/games/h/a/fl;

    move-object v1, p2

    move-object v4, p4

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/common/server/n;->a(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;Ljava/lang/Object;Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/h/a/fl;
    :try_end_0
    .catch Lcom/android/volley/ac; {:try_start_0 .. :try_end_0} :catch_0

    move v1, v7

    .line 808
    :goto_0
    new-instance v2, Landroid/util/Pair;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-direct {v2, v1, v0}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    return-object v2

    .line 803
    :catch_0
    move-exception v0

    .line 804
    sget-object v1, Lcom/google/android/gms/games/a/cf;->a:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Failed to finish match: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/android/volley/ac;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/gms/games/internal/dq;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 805
    sget-object v1, Lcom/google/android/gms/games/a/cf;->a:Ljava/lang/String;

    const/4 v2, 0x5

    invoke-static {v1, v0, v2}, Lcom/google/android/gms/games/a/bf;->a(Ljava/lang/String;Lcom/android/volley/ac;I)I

    move-result v0

    move v1, v0

    move-object v0, v6

    goto :goto_0
.end method

.method private a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Lcom/google/android/gms/games/h/a/ft;)Landroid/util/Pair;
    .locals 8

    .prologue
    const/4 v7, 0x0

    .line 647
    .line 648
    const/4 v6, 0x0

    .line 650
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/games/a/cf;->d:Lcom/google/android/gms/games/h/a/fu;

    invoke-static {p1}, Lcom/google/android/gms/games/a/l;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "turnbasedmatches/%1$s/turn"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {p3}, Lcom/google/android/gms/games/h/a/fu;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    if-eqz v1, :cond_0

    const-string v2, "language"

    invoke-static {v1}, Lcom/google/android/gms/games/h/a/fu;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v3, v2, v1}, Lcom/google/android/gms/games/h/a/fu;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    :cond_0
    iget-object v0, v0, Lcom/google/android/gms/games/h/a/fu;->a:Lcom/google/android/gms/common/server/n;

    const/4 v2, 0x2

    const-class v5, Lcom/google/android/gms/games/h/a/fl;

    move-object v1, p2

    move-object v4, p4

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/common/server/n;->a(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;Ljava/lang/Object;Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/h/a/fl;
    :try_end_0
    .catch Lcom/android/volley/ac; {:try_start_0 .. :try_end_0} :catch_0

    move v1, v7

    .line 657
    :goto_0
    new-instance v2, Landroid/util/Pair;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-direct {v2, v1, v0}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    return-object v2

    .line 652
    :catch_0
    move-exception v0

    .line 653
    sget-object v1, Lcom/google/android/gms/games/a/cf;->a:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Failed to update match: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/android/volley/ac;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/gms/games/internal/dq;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 654
    sget-object v1, Lcom/google/android/gms/games/a/cf;->a:Ljava/lang/String;

    const/4 v2, 0x5

    invoke-static {v1, v0, v2}, Lcom/google/android/gms/games/a/bf;->a(Ljava/lang/String;Lcom/android/volley/ac;I)I

    move-result v0

    move v1, v0

    move-object v0, v6

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;I)Lcom/google/android/gms/common/data/DataHolder;
    .locals 4

    .prologue
    .line 534
    invoke-static {p1, p2}, Lcom/google/android/gms/games/provider/aj;->a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 535
    new-instance v1, Lcom/google/android/gms/games/a/o;

    invoke-direct {v1, p0}, Lcom/google/android/gms/games/a/o;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1, v0}, Lcom/google/android/gms/games/a/o;->a(Landroid/net/Uri;)Lcom/google/android/gms/games/a/o;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/games/a/o;->b()Landroid/database/AbstractWindowedCursor;

    move-result-object v0

    .line 539
    if-nez p3, :cond_0

    invoke-virtual {v0}, Landroid/database/AbstractWindowedCursor;->getCount()I

    move-result v1

    if-nez v1, :cond_0

    .line 541
    const/16 p3, 0x196a

    .line 544
    :cond_0
    new-instance v1, Lcom/google/android/gms/common/data/DataHolder;

    invoke-static {v0, p3}, Lcom/google/android/gms/games/a/l;->a(Landroid/database/Cursor;I)I

    move-result v2

    const/4 v3, 0x0

    invoke-direct {v1, v0, v2, v3}, Lcom/google/android/gms/common/data/DataHolder;-><init>(Landroid/database/AbstractWindowedCursor;ILandroid/os/Bundle;)V

    return-object v1
.end method

.method private static a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;[BIZLjava/util/ArrayList;)Lcom/google/android/gms/common/data/DataHolder;
    .locals 11

    .prologue
    .line 665
    invoke-static {p1, p2}, Lcom/google/android/gms/games/provider/ak;->a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    const-string v3, "pending_participant_external"

    invoke-static {p0, v2, v3}, Lcom/google/android/gms/games/a/l;->f(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 668
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 669
    new-instance v3, Landroid/content/ContentValues;

    invoke-direct {v3}, Landroid/content/ContentValues;-><init>()V

    .line 670
    const-string v5, "external_match_id"

    invoke-virtual {v3, v5, p2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 671
    const-string v5, "last_updater_external"

    invoke-virtual {v3, v5, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 672
    const-string v5, "last_updated_timestamp"

    invoke-static {}, Lcom/google/android/gms/common/util/r;->c()Lcom/google/android/gms/common/util/p;

    move-result-object v6

    invoke-interface {v6}, Lcom/google/android/gms/common/util/p;->a()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v3, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 673
    const-string v5, "pending_participant_external"

    invoke-virtual {v3, v5, p3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 674
    const-string v5, "data"

    invoke-virtual {v3, v5, p4}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    .line 675
    if-eqz p6, :cond_1

    .line 676
    const-string v2, "user_match_status"

    const/4 v5, 0x3

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v2, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 677
    const-string v2, "status"

    const/4 v5, 0x2

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v2, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 686
    :goto_0
    const-string v2, "version"

    add-int/lit8 v5, p5, 0x1

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v2, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 687
    const-string v2, "upsync_required"

    const/4 v5, 0x1

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v2, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 688
    invoke-static {p1}, Lcom/google/android/gms/games/provider/ak;->a(Lcom/google/android/gms/common/server/ClientContext;)Landroid/net/Uri;

    move-result-object v2

    invoke-static {v2}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    invoke-virtual {v2, v3}, Landroid/content/ContentProviderOperation$Builder;->withValues(Landroid/content/ContentValues;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v2

    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 693
    const/4 v2, 0x0

    invoke-virtual/range {p7 .. p7}, Ljava/util/ArrayList;->size()I

    move-result v5

    move v3, v2

    :goto_1
    if-ge v3, v5, :cond_4

    .line 694
    move-object/from16 v0, p7

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/gms/games/multiplayer/ParticipantResult;

    .line 695
    invoke-virtual {v2}, Lcom/google/android/gms/games/multiplayer/ParticipantResult;->b()Ljava/lang/String;

    move-result-object v6

    .line 696
    invoke-static {p1, v6}, Lcom/google/android/gms/games/provider/ao;->a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v7

    .line 698
    new-instance v8, Landroid/content/ContentValues;

    invoke-direct {v8}, Landroid/content/ContentValues;-><init>()V

    .line 699
    const-string v9, "placing"

    invoke-virtual {v2}, Lcom/google/android/gms/games/multiplayer/ParticipantResult;->d()I

    move-result v10

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {v8, v9, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 700
    const-string v9, "result_type"

    invoke-virtual {v2}, Lcom/google/android/gms/games/multiplayer/ParticipantResult;->c()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v8, v9, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 701
    if-eqz p6, :cond_0

    invoke-virtual {v6, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 702
    const-string v2, "player_status"

    const/4 v6, 0x5

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v8, v2, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 704
    :cond_0
    invoke-static {v7}, Landroid/content/ContentProviderOperation;->newUpdate(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    invoke-virtual {v2, v8}, Landroid/content/ContentProviderOperation$Builder;->withValues(Landroid/content/ContentValues;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v2

    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 693
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_1

    .line 678
    :cond_1
    if-nez p3, :cond_2

    .line 679
    const-string v2, "user_match_status"

    const/4 v5, 0x2

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v2, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 680
    const-string v2, "status"

    const/4 v5, 0x0

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v2, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    goto/16 :goto_0

    .line 681
    :cond_2
    invoke-virtual {p3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 682
    const-string v2, "user_match_status"

    const/4 v5, 0x1

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v2, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    goto/16 :goto_0

    .line 684
    :cond_3
    const-string v2, "user_match_status"

    const/4 v5, 0x2

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v2, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    goto/16 :goto_0

    .line 708
    :cond_4
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    sget-object v3, Lcom/google/android/gms/games/a/cf;->a:Ljava/lang/String;

    invoke-static {v2, v4, v3}, Lcom/google/android/gms/games/a/l;->a(Landroid/content/ContentResolver;Ljava/util/ArrayList;Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_5

    .line 709
    sget-object v2, Lcom/google/android/gms/games/a/cf;->a:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Failed to apply local match update for "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/gms/games/internal/dq;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 710
    const/4 v2, 0x1

    invoke-static {v2}, Lcom/google/android/gms/common/data/DataHolder;->b(I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v2

    .line 713
    :goto_2
    return-object v2

    .line 712
    :cond_5
    invoke-static {p1, p2}, Lcom/google/android/gms/games/provider/aj;->a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 713
    new-instance v3, Lcom/google/android/gms/games/a/o;

    invoke-direct {v3, p0}, Lcom/google/android/gms/games/a/o;-><init>(Landroid/content/Context;)V

    invoke-virtual {v3, v2}, Lcom/google/android/gms/games/a/o;->a(Landroid/net/Uri;)Lcom/google/android/gms/games/a/o;

    move-result-object v2

    const/4 v3, 0x5

    iput v3, v2, Lcom/google/android/gms/games/a/o;->d:I

    invoke-virtual {v2}, Lcom/google/android/gms/games/a/o;->a()Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v2

    goto :goto_2
.end method

.method public static a(Lcom/google/android/gms/games/a/au;II)Lcom/google/android/gms/common/data/DataHolder;
    .locals 4

    .prologue
    .line 506
    iget-object v0, p0, Lcom/google/android/gms/games/a/au;->b:Lcom/google/android/gms/common/server/ClientContext;

    invoke-static {v0}, Lcom/google/android/gms/games/provider/aj;->a(Lcom/google/android/gms/common/server/ClientContext;)Landroid/net/Uri;

    move-result-object v0

    .line 507
    new-instance v1, Lcom/google/android/gms/common/e/b;

    invoke-direct {v1, v0}, Lcom/google/android/gms/common/e/b;-><init>(Landroid/net/Uri;)V

    .line 508
    const-string v0, "metadata_version"

    const-string v2, "0"

    const-string v3, ">=?"

    invoke-virtual {v1, v0, v2, v3}, Lcom/google/android/gms/common/e/b;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 509
    const-string v0, "user_match_status"

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Lcom/google/android/gms/common/e/b;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 510
    invoke-virtual {p0}, Lcom/google/android/gms/games/a/au;->g()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 511
    const-string v0, "external_game_id"

    iget-object v2, p0, Lcom/google/android/gms/games/a/au;->e:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Lcom/google/android/gms/common/e/b;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 515
    :cond_0
    new-instance v0, Lcom/google/android/gms/games/a/o;

    invoke-direct {v0, p0}, Lcom/google/android/gms/games/a/o;-><init>(Lcom/google/android/gms/games/a/au;)V

    iput-object v1, v0, Lcom/google/android/gms/games/a/o;->a:Lcom/google/android/gms/common/e/b;

    const-string v1, "last_updated_timestamp DESC"

    iput-object v1, v0, Lcom/google/android/gms/games/a/o;->c:Ljava/lang/String;

    iput p2, v0, Lcom/google/android/gms/games/a/o;->d:I

    invoke-virtual {v0}, Lcom/google/android/gms/games/a/o;->a()Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    return-object v0
.end method

.method private a(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/h/a/fl;I)Lcom/google/android/gms/common/data/DataHolder;
    .locals 2

    .prologue
    .line 1204
    invoke-static {p1, p2}, Lcom/google/android/gms/games/a/cf;->a(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/h/a/fl;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1205
    const/4 v0, 0x1

    invoke-static {v0}, Lcom/google/android/gms/common/data/DataHolder;->b(I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    .line 1209
    :goto_0
    return-object v0

    .line 1207
    :cond_0
    iget-object v0, p1, Lcom/google/android/gms/games/a/au;->b:Lcom/google/android/gms/common/server/ClientContext;

    invoke-virtual {p2}, Lcom/google/android/gms/games/h/a/fl;->d()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/games/provider/aj;->a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 1209
    new-instance v1, Lcom/google/android/gms/games/a/o;

    invoke-direct {v1, p1}, Lcom/google/android/gms/games/a/o;-><init>(Lcom/google/android/gms/games/a/au;)V

    invoke-virtual {v1, v0}, Lcom/google/android/gms/games/a/o;->a(Landroid/net/Uri;)Lcom/google/android/gms/games/a/o;

    move-result-object v0

    iput p3, v0, Lcom/google/android/gms/games/a/o;->d:I

    invoke-virtual {v0}, Lcom/google/android/gms/games/a/o;->a()Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    goto :goto_0
.end method

.method private a(Lcom/google/android/gms/games/a/au;Ljava/lang/String;I)Lcom/google/android/gms/common/data/DataHolder;
    .locals 10

    .prologue
    const/4 v6, 0x0

    const/4 v7, 0x0

    .line 623
    .line 626
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/games/a/cf;->d:Lcom/google/android/gms/games/h/a/fu;

    iget-object v1, p1, Lcom/google/android/gms/games/a/au;->b:Lcom/google/android/gms/common/server/ClientContext;

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    iget-object v3, p1, Lcom/google/android/gms/games/a/au;->a:Landroid/content/Context;

    invoke-static {v3}, Lcom/google/android/gms/games/a/l;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v4

    const-string v3, "turnbasedmatches/%1$s"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v8, 0x0

    invoke-static {p2}, Lcom/google/android/gms/games/h/a/fu;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    aput-object v9, v5, v8

    invoke-static {v3, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    if-eqz v2, :cond_0

    const-string v5, "includeMatchData"

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v5, v2}, Lcom/google/android/gms/games/h/a/fu;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    :cond_0
    if-eqz v4, :cond_1

    const-string v2, "language"

    invoke-static {v4}, Lcom/google/android/gms/games/h/a/fu;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v2, v4}, Lcom/google/android/gms/games/h/a/fu;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    :cond_1
    iget-object v0, v0, Lcom/google/android/gms/games/h/a/fu;->a:Lcom/google/android/gms/common/server/n;

    const/4 v2, 0x0

    const/4 v4, 0x0

    const-class v5, Lcom/google/android/gms/games/h/a/fl;

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/common/server/n;->a(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;Ljava/lang/Object;Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/h/a/fl;
    :try_end_0
    .catch Lcom/android/volley/ac; {:try_start_0 .. :try_end_0} :catch_0

    move v1, v7

    .line 635
    :goto_0
    if-eqz v0, :cond_2

    .line 636
    invoke-direct {p0, p1, v0, p3}, Lcom/google/android/gms/games/a/cf;->a(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/h/a/fl;I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    .line 640
    :goto_1
    return-object v0

    .line 629
    :catch_0
    move-exception v0

    .line 630
    sget-object v1, Lcom/google/android/gms/games/a/cf;->a:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Failed to load match: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/android/volley/ac;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/gms/games/internal/dq;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 631
    sget-object v1, Lcom/google/android/gms/games/a/cf;->a:Ljava/lang/String;

    const/4 v2, 0x5

    invoke-static {v1, v0, v2}, Lcom/google/android/gms/games/a/bf;->a(Ljava/lang/String;Lcom/android/volley/ac;I)I

    move-result v0

    move v1, v0

    move-object v0, v6

    goto :goto_0

    .line 640
    :cond_2
    invoke-static {v1}, Lcom/google/android/gms/common/data/DataHolder;->b(I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    goto :goto_1
.end method

.method private static a(Ljava/util/ArrayList;)Ljava/util/ArrayList;
    .locals 8

    .prologue
    const/4 v2, 0x0

    .line 1271
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_1

    .line 1288
    :cond_0
    :goto_0
    return-object v2

    .line 1276
    :cond_1
    invoke-virtual {p0}, Ljava/util/ArrayList;->size()I

    move-result v5

    .line 1277
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3, v5}, Ljava/util/ArrayList;-><init>(I)V

    .line 1279
    const/4 v0, 0x0

    move v4, v0

    :goto_1
    if-ge v4, v5, :cond_3

    .line 1280
    invoke-virtual {p0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/multiplayer/ParticipantResult;

    .line 1281
    invoke-virtual {v0}, Lcom/google/android/gms/games/multiplayer/ParticipantResult;->d()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    .line 1282
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v6

    const/4 v7, -0x1

    if-ne v6, v7, :cond_2

    move-object v1, v2

    .line 1285
    :cond_2
    new-instance v6, Lcom/google/android/gms/games/h/a/ch;

    invoke-virtual {v0}, Lcom/google/android/gms/games/multiplayer/ParticipantResult;->b()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0}, Lcom/google/android/gms/games/multiplayer/ParticipantResult;->c()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unknown result: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    :pswitch_0
    const-string v0, "MATCH_RESULT_WIN"

    :goto_2
    invoke-direct {v6, v7, v1, v0}, Lcom/google/android/gms/games/h/a/ch;-><init>(Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/String;)V

    invoke-virtual {v3, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1279
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto :goto_1

    .line 1285
    :pswitch_1
    const-string v0, "MATCH_RESULT_LOSS"

    goto :goto_2

    :pswitch_2
    const-string v0, "MATCH_RESULT_TIE"

    goto :goto_2

    :pswitch_3
    const-string v0, "MATCH_RESULT_NONE"

    goto :goto_2

    :pswitch_4
    const-string v0, "MATCH_RESULT_DISCONNECT"

    goto :goto_2

    :pswitch_5
    const-string v0, "MATCH_RESULT_DISAGREED"

    goto :goto_2

    :cond_3
    move-object v2, v3

    .line 1288
    goto :goto_0

    .line 1285
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method private static a(Lcom/google/android/gms/games/a/au;ILjava/lang/String;)V
    .locals 7

    .prologue
    const/4 v3, 0x0

    .line 1061
    const/4 v4, 0x0

    const/4 v5, -0x1

    move-object v0, p0

    move v1, p1

    move-object v2, p2

    move-object v6, v3

    invoke-static/range {v0 .. v6}, Lcom/google/android/gms/games/a/cf;->a(Lcom/google/android/gms/games/a/au;ILjava/lang/String;Ljava/lang/String;ZILcom/google/android/gms/games/h/a/fs;)V

    .line 1063
    return-void
.end method

.method private static a(Lcom/google/android/gms/games/a/au;ILjava/lang/String;Ljava/lang/String;ZILcom/google/android/gms/games/h/a/fs;)V
    .locals 8

    .prologue
    .line 1068
    iget-object v1, p0, Lcom/google/android/gms/games/a/au;->a:Landroid/content/Context;

    .line 1069
    iget-object v2, p0, Lcom/google/android/gms/games/a/au;->b:Lcom/google/android/gms/common/server/ClientContext;

    .line 1072
    invoke-static {v2}, Lcom/google/android/gms/games/provider/al;->a(Lcom/google/android/gms/common/server/ClientContext;)Landroid/net/Uri;

    move-result-object v3

    .line 1073
    new-instance v4, Landroid/content/ContentValues;

    invoke-direct {v4}, Landroid/content/ContentValues;-><init>()V

    .line 1074
    const-string v0, "client_context_id"

    invoke-static {v1, v2}, Lcom/google/android/gms/games/a/l;->b(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;)J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v4, v0, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1076
    const-string v0, "type"

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v4, v0, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1077
    const-string v0, "external_game_id"

    iget-object v5, p0, Lcom/google/android/gms/games/a/au;->e:Ljava/lang/String;

    invoke-virtual {v4, v0, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1078
    const-string v0, "external_match_id"

    invoke-virtual {v4, v0, p2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1079
    const-string v0, "pending_participant_id"

    invoke-virtual {v4, v0, p3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1080
    const-string v0, "is_turn"

    invoke-static {p4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    invoke-virtual {v4, v0, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 1081
    const-string v0, "version"

    invoke-static {p5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v4, v0, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1082
    const-string v5, "results"

    if-nez p6, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v4, v5, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1083
    new-instance v0, Ljava/util/ArrayList;

    const/4 v5, 0x2

    invoke-direct {v0, v5}, Ljava/util/ArrayList;-><init>(I)V

    .line 1084
    invoke-static {v3}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v3

    invoke-virtual {v3, v4}, Landroid/content/ContentProviderOperation$Builder;->withValues(Landroid/content/ContentValues;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1087
    invoke-static {v2, p2}, Lcom/google/android/gms/games/provider/ak;->a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    .line 1088
    invoke-static {v3}, Landroid/content/ContentProviderOperation;->newUpdate(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v3

    const-string v4, "upsync_required"

    const/4 v5, 0x1

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1091
    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    sget-object v3, Lcom/google/android/gms/games/a/cf;->a:Ljava/lang/String;

    invoke-static {v1, v0, v3}, Lcom/google/android/gms/games/a/l;->a(Landroid/content/ContentResolver;Ljava/util/ArrayList;Ljava/lang/String;)Z

    .line 1092
    invoke-static {v2}, Lcom/google/android/gms/games/service/ac;->a(Lcom/google/android/gms/common/server/ClientContext;)V

    .line 1093
    return-void

    .line 1082
    :cond_0
    invoke-virtual {p6}, Lcom/google/android/gms/games/h/a/fs;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private static a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)Z
    .locals 4

    .prologue
    .line 295
    invoke-static {p1, p2}, Lcom/google/android/gms/games/provider/al;->a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 296
    invoke-static {p0, v0}, Lcom/google/android/gms/games/a/l;->a(Landroid/content/Context;Landroid/net/Uri;)J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static a(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/h/a/fl;)Z
    .locals 10

    .prologue
    const/4 v8, 0x1

    const/4 v9, 0x0

    .line 1216
    iget-object v1, p0, Lcom/google/android/gms/games/a/au;->a:Landroid/content/Context;

    .line 1217
    iget-object v2, p0, Lcom/google/android/gms/games/a/au;->b:Lcom/google/android/gms/common/server/ClientContext;

    .line 1220
    invoke-virtual {p0}, Lcom/google/android/gms/games/a/au;->j()J

    move-result-wide v4

    .line 1223
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 1224
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/google/android/gms/games/h/a/fl;->f()Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const/4 v3, 0x5

    if-ne v0, v3, :cond_0

    .line 1225
    invoke-virtual {p1}, Lcom/google/android/gms/games/h/a/fl;->d()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0, v7}, Lcom/google/android/gms/games/a/bf;->a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 1226
    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/google/android/gms/games/a/cf;->a:Ljava/lang/String;

    invoke-static {v0, v7, v1}, Lcom/google/android/gms/games/a/l;->a(Landroid/content/ContentResolver;Ljava/util/ArrayList;Ljava/lang/String;)Z

    move v0, v8

    .line 1252
    :goto_0
    return v0

    .line 1231
    :cond_0
    invoke-static {}, Lcom/google/android/gms/common/util/r;->c()Lcom/google/android/gms/common/util/p;

    move-result-object v6

    move-object v3, p1

    invoke-static/range {v1 .. v7}, Lcom/google/android/gms/games/a/bf;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Lcom/google/android/gms/games/h/a/fl;JLcom/google/android/gms/common/util/p;Ljava/util/ArrayList;)I

    move-result v0

    .line 1233
    const/4 v2, -0x1

    if-ne v0, v2, :cond_1

    move v0, v9

    .line 1234
    goto :goto_0

    .line 1238
    :cond_1
    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    sget-object v2, Lcom/google/android/gms/games/a/cf;->a:Ljava/lang/String;

    invoke-static {v1, v7, v2}, Lcom/google/android/gms/games/a/l;->b(Landroid/content/ContentResolver;Ljava/util/ArrayList;Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v1

    .line 1240
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/ContentProviderResult;

    iget-object v0, v0, Landroid/content/ContentProviderResult;->uri:Landroid/net/Uri;

    .line 1241
    if-nez v0, :cond_2

    .line 1242
    sget-object v0, Lcom/google/android/gms/games/a/cf;->a:Ljava/lang/String;

    const-string v1, "Failed to store data for newly created entity"

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->e(Ljava/lang/String;Ljava/lang/String;)V

    move v0, v9

    .line 1243
    goto :goto_0

    .line 1245
    :cond_2
    invoke-static {v0}, Landroid/content/ContentUris;->parseId(Landroid/net/Uri;)J

    move-result-wide v0

    .line 1246
    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-gez v0, :cond_3

    .line 1247
    sget-object v0, Lcom/google/android/gms/games/a/cf;->a:Ljava/lang/String;

    const-string v1, "Failed to store data for newly created entity"

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->e(Ljava/lang/String;Ljava/lang/String;)V

    move v0, v9

    .line 1248
    goto :goto_0

    :cond_3
    move v0, v8

    .line 1252
    goto :goto_0
.end method

.method private static b(I)I
    .locals 1

    .prologue
    .line 1188
    if-nez p0, :cond_0

    .line 1189
    const/4 v0, 0x0

    .line 1193
    :goto_0
    return v0

    .line 1190
    :cond_0
    const/4 v0, 0x5

    if-ne p0, v0, :cond_1

    .line 1191
    const/4 v0, 0x1

    goto :goto_0

    .line 1193
    :cond_1
    const/4 v0, 0x2

    goto :goto_0
.end method

.method private b(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)I
    .locals 7

    .prologue
    const/4 v0, 0x0

    const/4 v6, 0x0

    .line 943
    .line 945
    :try_start_0
    iget-object v1, p0, Lcom/google/android/gms/games/a/cf;->d:Lcom/google/android/gms/games/h/a/fu;

    const-string v2, "turnbasedmatches/%1$s/cancel"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {p3}, Lcom/google/android/gms/games/h/a/fu;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    iget-object v1, v1, Lcom/google/android/gms/games/h/a/fu;->a:Lcom/google/android/gms/common/server/n;

    const/4 v3, 0x2

    const/4 v4, 0x0

    invoke-virtual {v1, p2, v3, v2, v4}, Lcom/google/android/gms/common/server/n;->a(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;Ljava/lang/Object;)V
    :try_end_0
    .catch Lcom/android/volley/ac; {:try_start_0 .. :try_end_0} :catch_0

    .line 953
    :goto_0
    invoke-static {p2, p3}, Lcom/google/android/gms/games/provider/ak;->a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 954
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    invoke-virtual {v2, v1, v6, v6}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 955
    return v0

    .line 946
    :catch_0
    move-exception v0

    .line 947
    sget-object v1, Lcom/google/android/gms/games/a/cf;->a:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Failed to cancel match: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/android/volley/ac;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/gms/games/internal/dq;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 948
    sget-object v1, Lcom/google/android/gms/games/a/cf;->a:Ljava/lang/String;

    const/4 v2, 0x5

    invoke-static {v1, v0, v2}, Lcom/google/android/gms/games/a/bf;->a(Ljava/lang/String;Lcom/android/volley/ac;I)I

    move-result v0

    goto :goto_0
.end method

.method private b(Lcom/google/android/gms/games/a/au;)I
    .locals 14

    .prologue
    .line 978
    iget-object v7, p1, Lcom/google/android/gms/games/a/au;->a:Landroid/content/Context;

    .line 979
    iget-object v0, p1, Lcom/google/android/gms/games/a/au;->b:Lcom/google/android/gms/common/server/ClientContext;

    .line 983
    invoke-static {v0}, Lcom/google/android/gms/games/provider/al;->a(Lcom/google/android/gms/common/server/ClientContext;)Landroid/net/Uri;

    move-result-object v8

    .line 984
    invoke-virtual {v0}, Lcom/google/android/gms/common/server/ClientContext;->c()Ljava/lang/String;

    move-result-object v9

    .line 985
    const/4 v6, 0x0

    .line 986
    new-instance v10, Ljava/util/ArrayList;

    invoke-direct {v10}, Ljava/util/ArrayList;-><init>()V

    .line 987
    new-instance v0, Lcom/google/android/gms/games/a/o;

    invoke-direct {v0, v7}, Lcom/google/android/gms/games/a/o;-><init>(Landroid/content/Context;)V

    const-string v1, "account_name=?"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    aput-object v9, v2, v3

    invoke-virtual {v0, v8, v1, v2}, Lcom/google/android/gms/games/a/o;->a(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)Lcom/google/android/gms/games/a/o;

    move-result-object v0

    sget-object v1, Lcom/google/android/gms/games/a/cg;->a:[Ljava/lang/String;

    iput-object v1, v0, Lcom/google/android/gms/games/a/o;->b:[Ljava/lang/String;

    const-string v1, "package_name"

    iput-object v1, v0, Lcom/google/android/gms/games/a/o;->c:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/google/android/gms/games/a/o;->b()Landroid/database/AbstractWindowedCursor;

    move-result-object v11

    .line 993
    :goto_0
    :try_start_0
    invoke-interface {v11}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 995
    const/4 v0, 0x1

    invoke-interface {v11, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    .line 996
    const/16 v1, 0x9

    invoke-interface {v11, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    .line 997
    const/16 v2, 0x8

    invoke-interface {v11, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 998
    const/4 v3, 0x3

    invoke-interface {v11, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v12

    .line 999
    invoke-static {v1, v2, v9}, Lcom/google/android/gms/games/a/l;->a(ILjava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/server/ClientContext;

    move-result-object v13

    .line 1004
    packed-switch v0, :pswitch_data_0

    .line 1024
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unknown op type "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1052
    :catchall_0
    move-exception v0

    invoke-interface {v11}, Landroid/database/Cursor;->close()V

    throw v0

    .line 1006
    :pswitch_0
    const/4 v0, 0x3

    :try_start_1
    invoke-interface {v11, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x2

    invoke-interface {v11, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v7, v13, v1}, Lcom/google/android/gms/games/a/au;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)Lcom/google/android/gms/games/a/au;

    move-result-object v1

    const/4 v2, 0x0

    invoke-direct {p0, v1, v0, v2}, Lcom/google/android/gms/games/a/cf;->a(Lcom/google/android/gms/games/a/au;Ljava/lang/String;Z)I

    move-result v0

    invoke-static {v0}, Lcom/google/android/gms/games/a/cf;->b(I)I

    move-result v0

    .line 1030
    :goto_1
    if-eqz v0, :cond_0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    .line 1031
    :cond_0
    const/4 v1, 0x0

    invoke-interface {v11, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    .line 1032
    invoke-static {v8, v2, v3}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v1

    .line 1033
    invoke-static {v1}, Landroid/content/ContentProviderOperation;->newDelete(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v1

    invoke-virtual {v10, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1034
    if-eqz v12, :cond_1

    .line 1035
    invoke-static {v13, v12}, Lcom/google/android/gms/games/provider/ak;->a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 1037
    invoke-static {v1}, Landroid/content/ContentProviderOperation;->newUpdate(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v1

    const-string v2, "upsync_required"

    const/4 v3, 0x0

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/content/ContentProviderOperation$Builder;->withYieldAllowed(Z)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v1

    invoke-virtual {v10, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1045
    :cond_1
    const/4 v1, 0x2

    if-ne v0, v1, :cond_5

    .line 1046
    const/4 v6, 0x6

    goto/16 :goto_0

    .line 1011
    :pswitch_1
    invoke-direct {p0, v11, v7, v13}, Lcom/google/android/gms/games/a/cf;->a(Landroid/database/Cursor;Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;)I

    move-result v0

    goto :goto_1

    .line 1015
    :pswitch_2
    const/4 v0, 0x1

    invoke-interface {v11, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    const/4 v1, 0x3

    invoke-interface {v11, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    if-ne v0, v2, :cond_2

    invoke-static {v13, v1}, Lcom/google/android/gms/games/provider/ae;->a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    :goto_2
    invoke-direct {p0, v7, v13, v1, v0}, Lcom/google/android/gms/games/a/cf;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Landroid/net/Uri;)I

    move-result v0

    invoke-static {v0}, Lcom/google/android/gms/games/a/cf;->b(I)I

    move-result v0

    goto :goto_1

    :cond_2
    const/4 v2, 0x2

    if-ne v0, v2, :cond_3

    invoke-static {v13, v1}, Lcom/google/android/gms/games/provider/ak;->a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    goto :goto_2

    :cond_3
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unknown dismiss type "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 1018
    :pswitch_3
    const/4 v0, 0x3

    invoke-interface {v11, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v0, 0x2

    invoke-interface {v11, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x4

    invoke-interface {v11, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    const/4 v1, 0x5

    invoke-interface {v11, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    const/4 v1, 0x6

    invoke-interface {v11, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    if-lez v1, :cond_4

    const/4 v4, 0x1

    :goto_3
    invoke-static {v7, v13, v0}, Lcom/google/android/gms/games/a/au;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)Lcom/google/android/gms/games/a/au;

    move-result-object v1

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/games/a/cf;->a(Lcom/google/android/gms/games/a/au;Ljava/lang/String;IZLjava/lang/String;)I

    move-result v0

    invoke-static {v0}, Lcom/google/android/gms/games/a/cf;->b(I)I

    move-result v0

    goto/16 :goto_1

    :cond_4
    const/4 v4, 0x0

    goto :goto_3

    .line 1021
    :pswitch_4
    const/4 v0, 0x3

    invoke-interface {v11, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v7, v13, v0}, Lcom/google/android/gms/games/a/cf;->b(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Lcom/google/android/gms/games/a/cf;->b(I)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v0

    goto/16 :goto_1

    .line 1047
    :cond_5
    const/4 v1, 0x1

    if-ne v0, v1, :cond_7

    .line 1048
    const/4 v0, 0x5

    :goto_4
    move v6, v0

    .line 1050
    goto/16 :goto_0

    .line 1052
    :cond_6
    invoke-interface {v11}, Landroid/database/Cursor;->close()V

    .line 1056
    invoke-virtual {v7}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/google/android/gms/games/a/cf;->a:Ljava/lang/String;

    invoke-static {v0, v10, v1}, Lcom/google/android/gms/games/a/l;->a(Landroid/content/ContentResolver;Ljava/util/ArrayList;Ljava/lang/String;)Z

    .line 1057
    return v6

    :cond_7
    move v0, v6

    goto :goto_4

    .line 1004
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_2
        :pswitch_1
        :pswitch_1
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method private static c(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)I
    .locals 3

    .prologue
    .line 1293
    invoke-static {p1, p2}, Lcom/google/android/gms/games/provider/ak;->a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 1294
    const-string v1, "version"

    const/4 v2, -0x1

    invoke-static {p0, v0, v1, v2}, Lcom/google/android/gms/games/a/l;->a(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;I)I

    move-result v0

    return v0
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/games/a/au;)I
    .locals 2

    .prologue
    .line 481
    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-static {p1, v0, v1}, Lcom/google/android/gms/games/a/cf;->a(Lcom/google/android/gms/games/a/au;II)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v1

    .line 485
    :try_start_0
    new-instance v0, Lcom/google/android/gms/games/multiplayer/turnbased/c;

    invoke-direct {v0, v1}, Lcom/google/android/gms/games/multiplayer/turnbased/c;-><init>(Lcom/google/android/gms/common/data/DataHolder;)V

    .line 486
    invoke-virtual {v0}, Lcom/google/android/gms/games/multiplayer/turnbased/c;->c()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    .line 488
    invoke-virtual {v1}, Lcom/google/android/gms/common/data/DataHolder;->j()V

    .line 490
    return v0

    .line 488
    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Lcom/google/android/gms/common/data/DataHolder;->j()V

    throw v0
.end method

.method public final a(Lcom/google/android/gms/games/a/au;ILjava/util/ArrayList;Landroid/os/Bundle;)Lcom/google/android/gms/common/data/DataHolder;
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 173
    if-nez p4, :cond_1

    move-object v0, v1

    .line 176
    :goto_0
    const/4 v2, -0x1

    if-ne p2, v2, :cond_2

    .line 177
    :goto_1
    :try_start_0
    new-instance v4, Lcom/google/android/gms/games/h/a/fm;

    iget-object v2, p0, Lcom/google/android/gms/games/a/cf;->c:Ljava/util/Random;

    invoke-virtual {v2}, Ljava/util/Random;->nextLong()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-direct {v4, v0, p3, v2, v1}, Lcom/google/android/gms/games/h/a/fm;-><init>(Lcom/google/android/gms/games/h/a/fk;Ljava/util/ArrayList;Ljava/lang/Long;Ljava/lang/Integer;)V

    .line 179
    iget-object v0, p0, Lcom/google/android/gms/games/a/cf;->d:Lcom/google/android/gms/games/h/a/fu;

    iget-object v1, p1, Lcom/google/android/gms/games/a/au;->b:Lcom/google/android/gms/common/server/ClientContext;

    iget-object v2, p1, Lcom/google/android/gms/games/a/au;->a:Landroid/content/Context;

    invoke-static {v2}, Lcom/google/android/gms/games/a/l;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "turnbasedmatches/create"

    if-eqz v2, :cond_0

    const-string v5, "language"

    invoke-static {v2}, Lcom/google/android/gms/games/h/a/fu;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v5, v2}, Lcom/google/android/gms/games/h/a/fu;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    :cond_0
    iget-object v0, v0, Lcom/google/android/gms/games/h/a/fu;->a:Lcom/google/android/gms/common/server/n;

    const/4 v2, 0x1

    const-class v5, Lcom/google/android/gms/games/h/a/fl;

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/common/server/n;->a(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;Ljava/lang/Object;Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/h/a/fl;
    :try_end_0
    .catch Lcom/android/volley/ac; {:try_start_0 .. :try_end_0} :catch_0

    .line 192
    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/google/android/gms/games/a/cf;->a(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/h/a/fl;I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    :goto_2
    return-object v0

    .line 173
    :cond_1
    const-string v0, "exclusive_bit_mask"

    invoke-virtual {p4, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    const-string v0, "exclusive_bit_mask"

    invoke-virtual {p4, v0}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    :goto_3
    new-instance v2, Lcom/google/android/gms/games/h/a/fk;

    const-string v3, "max_automatch_players"

    invoke-virtual {p4, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    const-string v4, "min_automatch_players"

    invoke-virtual {p4, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v2, v0, v3, v4}, Lcom/google/android/gms/games/h/a/fk;-><init>(Ljava/lang/Long;Ljava/lang/Integer;Ljava/lang/Integer;)V

    move-object v0, v2

    goto :goto_0

    .line 176
    :cond_2
    :try_start_1
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;
    :try_end_1
    .catch Lcom/android/volley/ac; {:try_start_1 .. :try_end_1} :catch_0

    move-result-object v1

    goto :goto_1

    .line 181
    :catch_0
    move-exception v0

    .line 184
    sget-object v1, Lcom/google/android/gms/games/a/cf;->a:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Failed to create turn-based match: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/android/volley/ac;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/gms/games/internal/dq;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 185
    sget-object v1, Lcom/google/android/gms/games/a/cf;->a:Ljava/lang/String;

    const/4 v2, 0x6

    invoke-static {v1, v0, v2}, Lcom/google/android/gms/games/a/bf;->a(Ljava/lang/String;Lcom/android/volley/ac;I)I

    move-result v0

    .line 187
    invoke-static {v0}, Lcom/google/android/gms/common/data/DataHolder;->b(I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    goto :goto_2

    :cond_3
    move-object v0, v1

    goto :goto_3
.end method

.method public final a(Lcom/google/android/gms/games/a/au;Ljava/lang/String;)Lcom/google/android/gms/common/data/DataHolder;
    .locals 9

    .prologue
    const/4 v8, 0x0

    .line 206
    iget-object v1, p1, Lcom/google/android/gms/games/a/au;->b:Lcom/google/android/gms/common/server/ClientContext;

    .line 207
    iget-object v0, p1, Lcom/google/android/gms/games/a/au;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/gms/games/a/l;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 210
    :try_start_0
    iget-boolean v2, p1, Lcom/google/android/gms/games/a/au;->h:Z

    if-eqz v2, :cond_2

    .line 211
    iget-object v2, p0, Lcom/google/android/gms/games/a/cf;->e:Lcom/google/android/gms/games/h/a/fv;

    iget-object v3, p0, Lcom/google/android/gms/games/a/cf;->c:Ljava/util/Random;

    invoke-virtual {v3}, Ljava/util/Random;->nextLong()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    const-string v3, "turnbasedmatches/%1$s/rematch"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    invoke-static {p2}, Lcom/google/android/gms/games/h/a/fv;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-static {v3, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    if-eqz v0, :cond_0

    const-string v5, "language"

    invoke-static {v0}, Lcom/google/android/gms/games/h/a/fv;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v5, v0}, Lcom/google/android/gms/games/h/a/fv;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    :cond_0
    if-eqz v4, :cond_1

    const-string v0, "requestId"

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v0, v4}, Lcom/google/android/gms/games/h/a/fv;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    :cond_1
    iget-object v0, v2, Lcom/google/android/gms/games/h/a/fv;->a:Lcom/google/android/gms/common/server/n;

    const/4 v2, 0x1

    const/4 v4, 0x0

    const-class v5, Lcom/google/android/gms/games/h/a/fr;

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/common/server/n;->a(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;Ljava/lang/Object;Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/h/a/fr;
    :try_end_0
    .catch Lcom/android/volley/ac; {:try_start_0 .. :try_end_0} :catch_0

    .line 227
    :goto_0
    invoke-virtual {v0}, Lcom/google/android/gms/games/h/a/fr;->getPreviousMatch()Lcom/google/android/gms/games/h/a/fl;

    move-result-object v1

    invoke-static {p1, v1}, Lcom/google/android/gms/games/a/cf;->a(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/h/a/fl;)Z

    .line 228
    invoke-virtual {v0}, Lcom/google/android/gms/games/h/a/fr;->getRematch()Lcom/google/android/gms/games/h/a/fl;

    move-result-object v0

    invoke-direct {p0, p1, v0, v8}, Lcom/google/android/gms/games/a/cf;->a(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/h/a/fl;I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    :goto_1
    return-object v0

    .line 214
    :cond_2
    :try_start_1
    iget-object v2, p0, Lcom/google/android/gms/games/a/cf;->d:Lcom/google/android/gms/games/h/a/fu;

    iget-object v3, p0, Lcom/google/android/gms/games/a/cf;->c:Ljava/util/Random;

    invoke-virtual {v3}, Ljava/util/Random;->nextLong()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    const-string v3, "turnbasedmatches/%1$s/rematch"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    invoke-static {p2}, Lcom/google/android/gms/games/h/a/fu;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-static {v3, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    if-eqz v0, :cond_3

    const-string v5, "language"

    invoke-static {v0}, Lcom/google/android/gms/games/h/a/fu;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v5, v0}, Lcom/google/android/gms/games/h/a/fu;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    :cond_3
    if-eqz v4, :cond_4

    const-string v0, "requestId"

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v0, v4}, Lcom/google/android/gms/games/h/a/fu;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    :cond_4
    iget-object v0, v2, Lcom/google/android/gms/games/h/a/fu;->a:Lcom/google/android/gms/common/server/n;

    const/4 v2, 0x1

    const/4 v4, 0x0

    const-class v5, Lcom/google/android/gms/games/h/a/fr;

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/common/server/n;->a(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;Ljava/lang/Object;Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/h/a/fr;
    :try_end_1
    .catch Lcom/android/volley/ac; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 217
    :catch_0
    move-exception v0

    .line 219
    sget-object v1, Lcom/google/android/gms/games/a/cf;->a:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Failed to create rematch for match "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/gms/games/internal/dq;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 220
    sget-object v1, Lcom/google/android/gms/games/a/cf;->a:Ljava/lang/String;

    const/4 v2, 0x6

    invoke-static {v1, v0, v2}, Lcom/google/android/gms/games/a/bf;->a(Ljava/lang/String;Lcom/android/volley/ac;I)I

    move-result v0

    .line 222
    invoke-static {v0}, Lcom/google/android/gms/common/data/DataHolder;->b(I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    goto :goto_1
.end method

.method public final a(Lcom/google/android/gms/games/a/au;Ljava/lang/String;Ljava/lang/String;[BLjava/util/ArrayList;)Lcom/google/android/gms/common/data/DataHolder;
    .locals 9

    .prologue
    .line 570
    iget-object v7, p1, Lcom/google/android/gms/games/a/au;->a:Landroid/content/Context;

    .line 571
    iget-object v8, p1, Lcom/google/android/gms/games/a/au;->b:Lcom/google/android/gms/common/server/ClientContext;

    .line 573
    invoke-static {v7, v8, p2}, Lcom/google/android/gms/games/a/cf;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 574
    sget-object v0, Lcom/google/android/gms/games/a/cf;->a:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Cannot take turn while pending ops are present for match "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 576
    const/16 v0, 0x196b

    invoke-static {v0}, Lcom/google/android/gms/common/data/DataHolder;->b(I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    .line 617
    :goto_0
    return-object v0

    .line 580
    :cond_0
    invoke-static {v7, v8, p2}, Lcom/google/android/gms/games/a/cf;->c(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)I

    move-result v5

    .line 581
    const/4 v0, -0x1

    if-ne v5, v0, :cond_1

    .line 586
    sget-object v0, Lcom/google/android/gms/games/a/cf;->a:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "No local record found for match "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 587
    const/4 v0, 0x1

    invoke-static {v0}, Lcom/google/android/gms/common/data/DataHolder;->b(I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    goto :goto_0

    .line 591
    :cond_1
    if-nez p4, :cond_2

    const/4 v0, 0x0

    move-object v1, v0

    .line 593
    :goto_1
    invoke-static {p5}, Lcom/google/android/gms/games/a/cf;->a(Ljava/util/ArrayList;)Ljava/util/ArrayList;

    move-result-object v2

    .line 594
    new-instance v0, Lcom/google/android/gms/games/h/a/ft;

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-direct {v0, v1, v3, p3, v2}, Lcom/google/android/gms/games/h/a/ft;-><init>(Lcom/google/android/gms/games/h/a/fo;Ljava/lang/Integer;Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 596
    invoke-direct {p0, v7, v8, p2, v0}, Lcom/google/android/gms/games/a/cf;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Lcom/google/android/gms/games/h/a/ft;)Landroid/util/Pair;

    move-result-object v3

    .line 598
    iget-object v0, v3, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v4

    .line 599
    iget-object v0, v3, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/gms/games/h/a/fl;

    .line 600
    if-eqz v0, :cond_3

    .line 602
    invoke-direct {p0, p1, v0, v4}, Lcom/google/android/gms/games/a/cf;->a(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/h/a/fl;I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    goto :goto_0

    .line 591
    :cond_2
    new-instance v0, Lcom/google/android/gms/games/h/a/fo;

    invoke-static {p4}, Lcom/google/android/gms/common/util/m;->b([B)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/gms/games/h/a/fo;-><init>(Ljava/lang/String;)V

    move-object v1, v0

    goto :goto_1

    .line 603
    :cond_3
    const/16 v0, 0x1967

    if-ne v4, v0, :cond_4

    .line 606
    invoke-direct {p0, p1, p2, v4}, Lcom/google/android/gms/games/a/cf;->a(Lcom/google/android/gms/games/a/au;Ljava/lang/String;I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    goto :goto_0

    .line 607
    :cond_4
    const/4 v0, 0x5

    if-eq v4, v0, :cond_5

    .line 609
    invoke-static {v4}, Lcom/google/android/gms/common/data/DataHolder;->b(I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    goto :goto_0

    .line 613
    :cond_5
    new-instance v6, Lcom/google/android/gms/games/h/a/fs;

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-direct {v6, v1, v0, v2}, Lcom/google/android/gms/games/h/a/fs;-><init>(Lcom/google/android/gms/games/h/a/fo;Ljava/lang/Integer;Ljava/util/ArrayList;)V

    .line 615
    const/4 v1, 0x3

    const/4 v4, 0x0

    move-object v0, p1

    move-object v2, p2

    move-object v3, p3

    invoke-static/range {v0 .. v6}, Lcom/google/android/gms/games/a/cf;->a(Lcom/google/android/gms/games/a/au;ILjava/lang/String;Ljava/lang/String;ZILcom/google/android/gms/games/h/a/fs;)V

    .line 617
    const/4 v6, 0x0

    move-object v0, v7

    move-object v1, v8

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v7, p5

    invoke-static/range {v0 .. v7}, Lcom/google/android/gms/games/a/cf;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;[BIZLjava/util/ArrayList;)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    goto/16 :goto_0
.end method

.method public final a(Lcom/google/android/gms/games/a/au;Ljava/lang/String;ZLjava/lang/String;)Lcom/google/android/gms/common/data/DataHolder;
    .locals 12

    .prologue
    .line 829
    iget-object v0, p1, Lcom/google/android/gms/games/a/au;->a:Landroid/content/Context;

    .line 830
    iget-object v11, p1, Lcom/google/android/gms/games/a/au;->b:Lcom/google/android/gms/common/server/ClientContext;

    .line 832
    invoke-static {v0, v11, p2}, Lcom/google/android/gms/games/a/cf;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 833
    sget-object v0, Lcom/google/android/gms/games/a/cf;->a:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Cannot leave while pending ops are present for "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 834
    const/16 v0, 0x196b

    invoke-static {v0}, Lcom/google/android/gms/common/data/DataHolder;->b(I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    .line 856
    :goto_0
    return-object v0

    .line 838
    :cond_0
    invoke-static {v0, v11, p2}, Lcom/google/android/gms/games/a/cf;->c(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)I

    move-result v3

    .line 839
    const/4 v0, -0x1

    if-ne v3, v0, :cond_1

    .line 844
    sget-object v0, Lcom/google/android/gms/games/a/cf;->a:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "No local record found for match "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 845
    const/4 v0, 0x1

    invoke-static {v0}, Lcom/google/android/gms/common/data/DataHolder;->b(I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    goto :goto_0

    :cond_1
    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v4, p3

    move-object/from16 v5, p4

    .line 849
    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/games/a/cf;->a(Lcom/google/android/gms/games/a/au;Ljava/lang/String;IZLjava/lang/String;)I

    move-result v0

    .line 851
    const/4 v1, 0x5

    if-ne v0, v1, :cond_2

    .line 852
    const/4 v5, 0x5

    const/4 v10, 0x0

    move-object v4, p1

    move-object v6, p2

    move-object/from16 v7, p4

    move v8, p3

    move v9, v3

    invoke-static/range {v4 .. v10}, Lcom/google/android/gms/games/a/cf;->a(Lcom/google/android/gms/games/a/au;ILjava/lang/String;Ljava/lang/String;ZILcom/google/android/gms/games/h/a/fs;)V

    .line 856
    :cond_2
    new-instance v1, Lcom/google/android/gms/games/a/o;

    invoke-direct {v1, p1}, Lcom/google/android/gms/games/a/o;-><init>(Lcom/google/android/gms/games/a/au;)V

    invoke-static {v11, p2}, Lcom/google/android/gms/games/provider/aj;->a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/gms/games/a/o;->a(Landroid/net/Uri;)Lcom/google/android/gms/games/a/o;

    move-result-object v1

    iput v0, v1, Lcom/google/android/gms/games/a/o;->d:I

    invoke-virtual {v1}, Lcom/google/android/gms/games/a/o;->a()Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/games/a/au;Ljava/lang/String;[BLjava/util/ArrayList;)Lcom/google/android/gms/common/data/DataHolder;
    .locals 10

    .prologue
    const/4 v9, 0x1

    const/4 v3, 0x0

    .line 745
    iget-object v7, p1, Lcom/google/android/gms/games/a/au;->a:Landroid/content/Context;

    .line 746
    iget-object v8, p1, Lcom/google/android/gms/games/a/au;->b:Lcom/google/android/gms/common/server/ClientContext;

    .line 748
    invoke-static {v7, v8, p2}, Lcom/google/android/gms/games/a/cf;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 749
    sget-object v0, Lcom/google/android/gms/games/a/cf;->a:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Cannot finish match while pending ops are present for match "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 751
    const/16 v0, 0x196b

    invoke-static {v0}, Lcom/google/android/gms/common/data/DataHolder;->b(I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    .line 790
    :goto_0
    return-object v0

    .line 755
    :cond_0
    invoke-static {v7, v8, p2}, Lcom/google/android/gms/games/a/cf;->c(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)I

    move-result v5

    .line 756
    const/4 v0, -0x1

    if-ne v5, v0, :cond_1

    .line 761
    sget-object v0, Lcom/google/android/gms/games/a/cf;->a:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "No local record found for match "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 762
    invoke-static {v9}, Lcom/google/android/gms/common/data/DataHolder;->b(I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    goto :goto_0

    .line 766
    :cond_1
    if-nez p3, :cond_2

    move-object v0, v3

    .line 768
    :goto_1
    invoke-static {p4}, Lcom/google/android/gms/games/a/cf;->a(Ljava/util/ArrayList;)Ljava/util/ArrayList;

    move-result-object v1

    .line 769
    new-instance v6, Lcom/google/android/gms/games/h/a/fs;

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-direct {v6, v0, v2, v1}, Lcom/google/android/gms/games/h/a/fs;-><init>(Lcom/google/android/gms/games/h/a/fo;Ljava/lang/Integer;Ljava/util/ArrayList;)V

    .line 771
    invoke-direct {p0, v7, v8, p2, v6}, Lcom/google/android/gms/games/a/cf;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Lcom/google/android/gms/games/h/a/fs;)Landroid/util/Pair;

    move-result-object v1

    .line 773
    iget-object v0, v1, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v2

    .line 774
    iget-object v0, v1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/gms/games/h/a/fl;

    .line 775
    if-eqz v0, :cond_3

    .line 777
    invoke-direct {p0, p1, v0, v2}, Lcom/google/android/gms/games/a/cf;->a(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/h/a/fl;I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    goto :goto_0

    .line 766
    :cond_2
    new-instance v0, Lcom/google/android/gms/games/h/a/fo;

    invoke-static {p3}, Lcom/google/android/gms/common/util/m;->b([B)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/gms/games/h/a/fo;-><init>(Ljava/lang/String;)V

    goto :goto_1

    .line 778
    :cond_3
    const/16 v0, 0x1967

    if-ne v2, v0, :cond_4

    .line 781
    invoke-direct {p0, p1, p2, v2}, Lcom/google/android/gms/games/a/cf;->a(Lcom/google/android/gms/games/a/au;Ljava/lang/String;I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    goto :goto_0

    .line 782
    :cond_4
    const/4 v0, 0x5

    if-eq v2, v0, :cond_5

    .line 784
    invoke-static {v2}, Lcom/google/android/gms/common/data/DataHolder;->b(I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    goto :goto_0

    .line 788
    :cond_5
    const/4 v1, 0x4

    const/4 v4, 0x0

    move-object v0, p1

    move-object v2, p2

    invoke-static/range {v0 .. v6}, Lcom/google/android/gms/games/a/cf;->a(Lcom/google/android/gms/games/a/au;ILjava/lang/String;Ljava/lang/String;ZILcom/google/android/gms/games/h/a/fs;)V

    move-object v0, v7

    move-object v1, v8

    move-object v2, p2

    move-object v4, p3

    move v6, v9

    move-object v7, p4

    .line 790
    invoke-static/range {v0 .. v7}, Lcom/google/android/gms/games/a/cf;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;[BIZLjava/util/ArrayList;)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    goto :goto_0
.end method

.method public final a()Lcom/google/android/gms/games/a/bb;
    .locals 0

    .prologue
    .line 458
    return-object p0
.end method

.method public final a(I)V
    .locals 0

    .prologue
    .line 475
    return-void
.end method

.method public final a(Lcom/google/android/gms/games/a/au;Landroid/content/SyncResult;)V
    .locals 6

    .prologue
    .line 970
    invoke-direct {p0, p1}, Lcom/google/android/gms/games/a/cf;->b(Lcom/google/android/gms/games/a/au;)I

    move-result v0

    .line 971
    if-eqz v0, :cond_0

    .line 972
    iget-object v0, p2, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v2, v0, Landroid/content/SyncStats;->numIoExceptions:J

    const-wide/16 v4, 0x1

    add-long/2addr v2, v4

    iput-wide v2, v0, Landroid/content/SyncStats;->numIoExceptions:J

    .line 974
    :cond_0
    return-void
.end method

.method public final b(Lcom/google/android/gms/games/a/au;Ljava/lang/String;)Lcom/google/android/gms/common/data/DataHolder;
    .locals 8

    .prologue
    const/4 v7, 0x0

    .line 241
    iget-object v0, p1, Lcom/google/android/gms/games/a/au;->a:Landroid/content/Context;

    .line 242
    iget-object v1, p1, Lcom/google/android/gms/games/a/au;->b:Lcom/google/android/gms/common/server/ClientContext;

    .line 245
    :try_start_0
    iget-boolean v2, p1, Lcom/google/android/gms/games/a/au;->h:Z

    if-eqz v2, :cond_1

    .line 246
    iget-object v2, p0, Lcom/google/android/gms/games/a/cf;->e:Lcom/google/android/gms/games/h/a/fv;

    invoke-static {v0}, Lcom/google/android/gms/games/a/l;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    const-string v3, "turnbasedmatches/%1$s/join"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-static {p2}, Lcom/google/android/gms/games/h/a/fv;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    if-eqz v0, :cond_0

    const-string v4, "language"

    invoke-static {v0}, Lcom/google/android/gms/games/h/a/fv;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v4, v0}, Lcom/google/android/gms/games/h/a/fv;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    :cond_0
    iget-object v0, v2, Lcom/google/android/gms/games/h/a/fv;->a:Lcom/google/android/gms/common/server/n;

    const/4 v2, 0x2

    const/4 v4, 0x0

    const-class v5, Lcom/google/android/gms/games/h/a/fl;

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/common/server/n;->a(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;Ljava/lang/Object;Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/h/a/fl;
    :try_end_0
    .catch Lcom/android/volley/ac; {:try_start_0 .. :try_end_0} :catch_0

    .line 261
    :goto_0
    invoke-direct {p0, p1, v0, v7}, Lcom/google/android/gms/games/a/cf;->a(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/h/a/fl;I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    :goto_1
    return-object v0

    .line 249
    :cond_1
    :try_start_1
    iget-object v2, p0, Lcom/google/android/gms/games/a/cf;->d:Lcom/google/android/gms/games/h/a/fu;

    invoke-static {v0}, Lcom/google/android/gms/games/a/l;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    const-string v3, "turnbasedmatches/%1$s/join"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-static {p2}, Lcom/google/android/gms/games/h/a/fu;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    if-eqz v0, :cond_2

    const-string v4, "language"

    invoke-static {v0}, Lcom/google/android/gms/games/h/a/fu;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v4, v0}, Lcom/google/android/gms/games/h/a/fu;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    :cond_2
    iget-object v0, v2, Lcom/google/android/gms/games/h/a/fu;->a:Lcom/google/android/gms/common/server/n;

    const/4 v2, 0x2

    const/4 v4, 0x0

    const-class v5, Lcom/google/android/gms/games/h/a/fl;

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/common/server/n;->a(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;Ljava/lang/Object;Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/h/a/fl;
    :try_end_1
    .catch Lcom/android/volley/ac; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 252
    :catch_0
    move-exception v0

    .line 254
    sget-object v1, Lcom/google/android/gms/games/a/cf;->a:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Failed to accept invitation: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/android/volley/ac;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/gms/games/internal/dq;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 255
    sget-object v1, Lcom/google/android/gms/games/a/cf;->a:Ljava/lang/String;

    const/4 v2, 0x6

    invoke-static {v1, v0, v2}, Lcom/google/android/gms/games/a/bf;->a(Ljava/lang/String;Lcom/android/volley/ac;I)I

    move-result v0

    .line 257
    invoke-static {v0}, Lcom/google/android/gms/common/data/DataHolder;->b(I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    goto :goto_1
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 463
    const-string v0, "inbox_matches_count"

    return-object v0
.end method

.method public final c(Lcom/google/android/gms/games/a/au;Ljava/lang/String;)I
    .locals 3

    .prologue
    .line 273
    iget-object v0, p1, Lcom/google/android/gms/games/a/au;->a:Landroid/content/Context;

    iget-object v1, p1, Lcom/google/android/gms/games/a/au;->b:Lcom/google/android/gms/common/server/ClientContext;

    invoke-static {v0, v1, p2}, Lcom/google/android/gms/games/a/cf;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 275
    sget-object v0, Lcom/google/android/gms/games/a/cf;->a:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Cannot decline while pending ops are present for "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 277
    const/16 v0, 0x196b

    .line 279
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/gms/games/a/cf;->a(Lcom/google/android/gms/games/a/au;Ljava/lang/String;Z)I

    move-result v0

    goto :goto_0
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 469
    const/4 v0, 0x0

    return v0
.end method

.method public final d(Lcom/google/android/gms/games/a/au;Ljava/lang/String;)I
    .locals 3

    .prologue
    .line 378
    iget-object v0, p1, Lcom/google/android/gms/games/a/au;->a:Landroid/content/Context;

    .line 379
    iget-object v1, p1, Lcom/google/android/gms/games/a/au;->b:Lcom/google/android/gms/common/server/ClientContext;

    .line 381
    invoke-static {v0, v1, p2}, Lcom/google/android/gms/games/a/cf;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 382
    sget-object v0, Lcom/google/android/gms/games/a/cf;->a:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Cannot dismiss while pending ops are present for invitation "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 384
    const/16 v0, 0x196b

    .line 394
    :cond_0
    :goto_0
    return v0

    .line 387
    :cond_1
    invoke-static {v1, p2}, Lcom/google/android/gms/games/provider/ae;->a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 389
    invoke-direct {p0, v0, v1, p2, v2}, Lcom/google/android/gms/games/a/cf;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Landroid/net/Uri;)I

    move-result v0

    .line 390
    const/4 v1, 0x5

    if-ne v0, v1, :cond_0

    .line 391
    const/4 v1, 0x1

    invoke-static {p1, v1, p2}, Lcom/google/android/gms/games/a/cf;->a(Lcom/google/android/gms/games/a/au;ILjava/lang/String;)V

    goto :goto_0
.end method

.method public final e(Lcom/google/android/gms/games/a/au;Ljava/lang/String;)I
    .locals 3

    .prologue
    .line 408
    iget-object v0, p1, Lcom/google/android/gms/games/a/au;->a:Landroid/content/Context;

    .line 409
    iget-object v1, p1, Lcom/google/android/gms/games/a/au;->b:Lcom/google/android/gms/common/server/ClientContext;

    .line 411
    invoke-static {v0, v1, p2}, Lcom/google/android/gms/games/a/cf;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 412
    sget-object v0, Lcom/google/android/gms/games/a/cf;->a:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Cannot dismiss while pending ops are present for match "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 414
    const/16 v0, 0x196b

    .line 422
    :cond_0
    :goto_0
    return v0

    .line 417
    :cond_1
    invoke-static {v1, p2}, Lcom/google/android/gms/games/provider/ak;->a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 418
    invoke-direct {p0, v0, v1, p2, v2}, Lcom/google/android/gms/games/a/cf;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Landroid/net/Uri;)I

    move-result v0

    .line 419
    const/4 v1, 0x5

    if-ne v0, v1, :cond_0

    .line 420
    const/4 v1, 0x2

    invoke-static {p1, v1, p2}, Lcom/google/android/gms/games/a/cf;->a(Lcom/google/android/gms/games/a/au;ILjava/lang/String;)V

    goto :goto_0
.end method

.method public final f(Lcom/google/android/gms/games/a/au;Ljava/lang/String;)I
    .locals 3

    .prologue
    .line 926
    iget-object v0, p1, Lcom/google/android/gms/games/a/au;->a:Landroid/content/Context;

    .line 927
    iget-object v1, p1, Lcom/google/android/gms/games/a/au;->b:Lcom/google/android/gms/common/server/ClientContext;

    .line 929
    invoke-static {v0, v1, p2}, Lcom/google/android/gms/games/a/cf;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 930
    sget-object v0, Lcom/google/android/gms/games/a/cf;->a:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Cannot cancel while pending ops are present for "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 931
    const/16 v0, 0x196b

    .line 938
    :cond_0
    :goto_0
    return v0

    .line 934
    :cond_1
    invoke-direct {p0, v0, v1, p2}, Lcom/google/android/gms/games/a/cf;->b(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)I

    move-result v0

    .line 935
    const/4 v1, 0x5

    if-ne v0, v1, :cond_0

    .line 936
    const/4 v1, 0x6

    invoke-static {p1, v1, p2}, Lcom/google/android/gms/games/a/cf;->a(Lcom/google/android/gms/games/a/au;ILjava/lang/String;)V

    goto :goto_0
.end method
