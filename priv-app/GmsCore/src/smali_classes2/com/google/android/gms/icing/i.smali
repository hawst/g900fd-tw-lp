.class public final Lcom/google/android/gms/icing/i;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# instance fields
.field public a:Lcom/google/android/gms/icing/g;

.field public b:Lcom/google/android/gms/icing/j;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 2884
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 2885
    iput-object v0, p0, Lcom/google/android/gms/icing/i;->a:Lcom/google/android/gms/icing/g;

    iput-object v0, p0, Lcom/google/android/gms/icing/i;->b:Lcom/google/android/gms/icing/j;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/icing/i;->cachedSize:I

    .line 2886
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 2949
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->computeSerializedSize()I

    move-result v0

    .line 2950
    iget-object v1, p0, Lcom/google/android/gms/icing/i;->a:Lcom/google/android/gms/icing/g;

    if-eqz v1, :cond_0

    .line 2951
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/gms/icing/i;->a:Lcom/google/android/gms/icing/g;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2954
    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/icing/i;->b:Lcom/google/android/gms/icing/j;

    if-eqz v1, :cond_1

    .line 2955
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/gms/icing/i;->b:Lcom/google/android/gms/icing/j;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2958
    :cond_1
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2897
    if-ne p1, p0, :cond_1

    .line 2922
    :cond_0
    :goto_0
    return v0

    .line 2900
    :cond_1
    instance-of v2, p1, Lcom/google/android/gms/icing/i;

    if-nez v2, :cond_2

    move v0, v1

    .line 2901
    goto :goto_0

    .line 2903
    :cond_2
    check-cast p1, Lcom/google/android/gms/icing/i;

    .line 2904
    iget-object v2, p0, Lcom/google/android/gms/icing/i;->a:Lcom/google/android/gms/icing/g;

    if-nez v2, :cond_3

    .line 2905
    iget-object v2, p1, Lcom/google/android/gms/icing/i;->a:Lcom/google/android/gms/icing/g;

    if-eqz v2, :cond_4

    move v0, v1

    .line 2906
    goto :goto_0

    .line 2909
    :cond_3
    iget-object v2, p0, Lcom/google/android/gms/icing/i;->a:Lcom/google/android/gms/icing/g;

    iget-object v3, p1, Lcom/google/android/gms/icing/i;->a:Lcom/google/android/gms/icing/g;

    invoke-virtual {v2, v3}, Lcom/google/android/gms/icing/g;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    move v0, v1

    .line 2910
    goto :goto_0

    .line 2913
    :cond_4
    iget-object v2, p0, Lcom/google/android/gms/icing/i;->b:Lcom/google/android/gms/icing/j;

    if-nez v2, :cond_5

    .line 2914
    iget-object v2, p1, Lcom/google/android/gms/icing/i;->b:Lcom/google/android/gms/icing/j;

    if-eqz v2, :cond_0

    move v0, v1

    .line 2915
    goto :goto_0

    .line 2918
    :cond_5
    iget-object v2, p0, Lcom/google/android/gms/icing/i;->b:Lcom/google/android/gms/icing/j;

    iget-object v3, p1, Lcom/google/android/gms/icing/i;->b:Lcom/google/android/gms/icing/j;

    invoke-virtual {v2, v3}, Lcom/google/android/gms/icing/j;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    .line 2919
    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2927
    iget-object v0, p0, Lcom/google/android/gms/icing/i;->a:Lcom/google/android/gms/icing/g;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    .line 2930
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/android/gms/icing/i;->b:Lcom/google/android/gms/icing/j;

    if-nez v2, :cond_1

    :goto_1
    add-int/2addr v0, v1

    .line 2932
    return v0

    .line 2927
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/icing/i;->a:Lcom/google/android/gms/icing/g;

    invoke-virtual {v0}, Lcom/google/android/gms/icing/g;->hashCode()I

    move-result v0

    goto :goto_0

    .line 2930
    :cond_1
    iget-object v1, p0, Lcom/google/android/gms/icing/i;->b:Lcom/google/android/gms/icing/j;

    invoke-virtual {v1}, Lcom/google/android/gms/icing/j;->hashCode()I

    move-result v1

    goto :goto_1
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 2861
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Lcom/google/android/gms/icing/i;->a:Lcom/google/android/gms/icing/g;

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/android/gms/icing/g;

    invoke-direct {v0}, Lcom/google/android/gms/icing/g;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/icing/i;->a:Lcom/google/android/gms/icing/g;

    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/icing/i;->a:Lcom/google/android/gms/icing/g;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Lcom/google/android/gms/icing/i;->b:Lcom/google/android/gms/icing/j;

    if-nez v0, :cond_2

    new-instance v0, Lcom/google/android/gms/icing/j;

    invoke-direct {v0}, Lcom/google/android/gms/icing/j;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/icing/i;->b:Lcom/google/android/gms/icing/j;

    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/icing/i;->b:Lcom/google/android/gms/icing/j;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 2938
    iget-object v0, p0, Lcom/google/android/gms/icing/i;->a:Lcom/google/android/gms/icing/g;

    if-eqz v0, :cond_0

    .line 2939
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/gms/icing/i;->a:Lcom/google/android/gms/icing/g;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 2941
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/icing/i;->b:Lcom/google/android/gms/icing/j;

    if-eqz v0, :cond_1

    .line 2942
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/android/gms/icing/i;->b:Lcom/google/android/gms/icing/j;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 2944
    :cond_1
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 2945
    return-void
.end method
