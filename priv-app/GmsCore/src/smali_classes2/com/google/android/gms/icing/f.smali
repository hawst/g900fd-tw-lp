.class public final Lcom/google/android/gms/icing/f;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# instance fields
.field public a:J

.field public b:I

.field public c:I

.field public d:I

.field public e:I


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 3243
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 3244
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/gms/icing/f;->a:J

    iput v2, p0, Lcom/google/android/gms/icing/f;->b:I

    iput v2, p0, Lcom/google/android/gms/icing/f;->c:I

    iput v2, p0, Lcom/google/android/gms/icing/f;->d:I

    iput v2, p0, Lcom/google/android/gms/icing/f;->e:I

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/icing/f;->cachedSize:I

    .line 3245
    return-void
.end method

.method public static a([B)Lcom/google/android/gms/icing/f;
    .locals 1

    .prologue
    .line 3384
    new-instance v0, Lcom/google/android/gms/icing/f;

    invoke-direct {v0}, Lcom/google/android/gms/icing/f;-><init>()V

    invoke-static {v0, p0}, Lcom/google/protobuf/nano/j;->mergeFrom(Lcom/google/protobuf/nano/j;[B)Lcom/google/protobuf/nano/j;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/icing/f;

    return-object v0
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 6

    .prologue
    .line 3319
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->computeSerializedSize()I

    move-result v0

    .line 3320
    iget-wide v2, p0, Lcom/google/android/gms/icing/f;->a:J

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-eqz v1, :cond_0

    .line 3321
    const/4 v1, 0x1

    iget-wide v2, p0, Lcom/google/android/gms/icing/f;->a:J

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/b;->e(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 3324
    :cond_0
    iget v1, p0, Lcom/google/android/gms/icing/f;->b:I

    if-eqz v1, :cond_1

    .line 3325
    const/4 v1, 0x2

    iget v2, p0, Lcom/google/android/gms/icing/f;->b:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->g(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 3328
    :cond_1
    iget v1, p0, Lcom/google/android/gms/icing/f;->c:I

    if-eqz v1, :cond_2

    .line 3329
    const/4 v1, 0x3

    iget v2, p0, Lcom/google/android/gms/icing/f;->c:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->g(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 3332
    :cond_2
    iget v1, p0, Lcom/google/android/gms/icing/f;->d:I

    if-eqz v1, :cond_3

    .line 3333
    const/4 v1, 0x4

    iget v2, p0, Lcom/google/android/gms/icing/f;->d:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->g(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 3336
    :cond_3
    iget v1, p0, Lcom/google/android/gms/icing/f;->e:I

    if-eqz v1, :cond_4

    .line 3337
    const/4 v1, 0x5

    iget v2, p0, Lcom/google/android/gms/icing/f;->e:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->g(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 3340
    :cond_4
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 3259
    if-ne p1, p0, :cond_1

    .line 3281
    :cond_0
    :goto_0
    return v0

    .line 3262
    :cond_1
    instance-of v2, p1, Lcom/google/android/gms/icing/f;

    if-nez v2, :cond_2

    move v0, v1

    .line 3263
    goto :goto_0

    .line 3265
    :cond_2
    check-cast p1, Lcom/google/android/gms/icing/f;

    .line 3266
    iget-wide v2, p0, Lcom/google/android/gms/icing/f;->a:J

    iget-wide v4, p1, Lcom/google/android/gms/icing/f;->a:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_3

    move v0, v1

    .line 3267
    goto :goto_0

    .line 3269
    :cond_3
    iget v2, p0, Lcom/google/android/gms/icing/f;->b:I

    iget v3, p1, Lcom/google/android/gms/icing/f;->b:I

    if-eq v2, v3, :cond_4

    move v0, v1

    .line 3270
    goto :goto_0

    .line 3272
    :cond_4
    iget v2, p0, Lcom/google/android/gms/icing/f;->c:I

    iget v3, p1, Lcom/google/android/gms/icing/f;->c:I

    if-eq v2, v3, :cond_5

    move v0, v1

    .line 3273
    goto :goto_0

    .line 3275
    :cond_5
    iget v2, p0, Lcom/google/android/gms/icing/f;->d:I

    iget v3, p1, Lcom/google/android/gms/icing/f;->d:I

    if-eq v2, v3, :cond_6

    move v0, v1

    .line 3276
    goto :goto_0

    .line 3278
    :cond_6
    iget v2, p0, Lcom/google/android/gms/icing/f;->e:I

    iget v3, p1, Lcom/google/android/gms/icing/f;->e:I

    if-eq v2, v3, :cond_0

    move v0, v1

    .line 3279
    goto :goto_0
.end method

.method public final hashCode()I
    .locals 5

    .prologue
    .line 3286
    iget-wide v0, p0, Lcom/google/android/gms/icing/f;->a:J

    iget-wide v2, p0, Lcom/google/android/gms/icing/f;->a:J

    const/16 v4, 0x20

    ushr-long/2addr v2, v4

    xor-long/2addr v0, v2

    long-to-int v0, v0

    add-int/lit16 v0, v0, 0x20f

    .line 3289
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/google/android/gms/icing/f;->b:I

    add-int/2addr v0, v1

    .line 3290
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/google/android/gms/icing/f;->c:I

    add-int/2addr v0, v1

    .line 3291
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/google/android/gms/icing/f;->d:I

    add-int/2addr v0, v1

    .line 3292
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/google/android/gms/icing/f;->e:I

    add-int/2addr v0, v1

    .line 3293
    return v0
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 2

    .prologue
    .line 3211
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->j()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/gms/icing/f;->a:J

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/icing/f;->b:I

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/icing/f;->c:I

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/icing/f;->d:I

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/icing/f;->e:I

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 4

    .prologue
    .line 3299
    iget-wide v0, p0, Lcom/google/android/gms/icing/f;->a:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    .line 3300
    const/4 v0, 0x1

    iget-wide v2, p0, Lcom/google/android/gms/icing/f;->a:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/b;->a(IJ)V

    .line 3302
    :cond_0
    iget v0, p0, Lcom/google/android/gms/icing/f;->b:I

    if-eqz v0, :cond_1

    .line 3303
    const/4 v0, 0x2

    iget v1, p0, Lcom/google/android/gms/icing/f;->b:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->c(II)V

    .line 3305
    :cond_1
    iget v0, p0, Lcom/google/android/gms/icing/f;->c:I

    if-eqz v0, :cond_2

    .line 3306
    const/4 v0, 0x3

    iget v1, p0, Lcom/google/android/gms/icing/f;->c:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->c(II)V

    .line 3308
    :cond_2
    iget v0, p0, Lcom/google/android/gms/icing/f;->d:I

    if-eqz v0, :cond_3

    .line 3309
    const/4 v0, 0x4

    iget v1, p0, Lcom/google/android/gms/icing/f;->d:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->c(II)V

    .line 3311
    :cond_3
    iget v0, p0, Lcom/google/android/gms/icing/f;->e:I

    if-eqz v0, :cond_4

    .line 3312
    const/4 v0, 0x5

    iget v1, p0, Lcom/google/android/gms/icing/f;->e:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->c(II)V

    .line 3314
    :cond_4
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 3315
    return-void
.end method
