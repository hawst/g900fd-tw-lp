.class public abstract Lcom/google/android/gms/people/identity/internal/r;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static a(Lcom/google/android/gms/people/identity/i;)Lcom/google/android/gms/people/identity/internal/models/DefaultMetadataImpl;
    .locals 2

    .prologue
    .line 264
    new-instance v0, Lcom/google/android/gms/people/identity/internal/models/DefaultMetadataImpl;

    invoke-direct {v0}, Lcom/google/android/gms/people/identity/internal/models/DefaultMetadataImpl;-><init>()V

    iget-boolean v1, p0, Lcom/google/android/gms/people/identity/i;->e:Z

    invoke-virtual {v0, v1}, Lcom/google/android/gms/people/identity/internal/models/DefaultMetadataImpl;->a(Z)Lcom/google/android/gms/people/identity/internal/models/DefaultMetadataImpl;

    move-result-object v1

    iget-boolean v0, p0, Lcom/google/android/gms/people/identity/i;->d:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v1, v0}, Lcom/google/android/gms/people/identity/internal/models/DefaultMetadataImpl;->c(Z)Lcom/google/android/gms/people/identity/internal/models/DefaultMetadataImpl;

    move-result-object v0

    const-string v1, "cp2"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/people/identity/internal/models/DefaultMetadataImpl;->e(Ljava/lang/String;)Lcom/google/android/gms/people/identity/internal/models/DefaultMetadataImpl;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/people/identity/i;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/people/identity/internal/models/DefaultMetadataImpl;->g(Ljava/lang/String;)Lcom/google/android/gms/people/identity/internal/models/DefaultMetadataImpl;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/people/identity/i;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/people/identity/internal/models/DefaultMetadataImpl;->f(Ljava/lang/String;)Lcom/google/android/gms/people/identity/internal/models/DefaultMetadataImpl;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method protected abstract a()Lcom/google/android/gms/people/identity/internal/models/CombinedPersonImpl;
.end method

.method public b(Landroid/content/Context;Ljava/lang/Object;Lcom/google/android/gms/people/identity/s;Lcom/google/android/gms/people/identity/h;Lcom/google/android/gms/people/identity/k;)Lcom/google/android/gms/people/identity/internal/models/CombinedPersonImpl;
    .locals 7

    .prologue
    .line 56
    invoke-virtual {p0}, Lcom/google/android/gms/people/identity/internal/r;->a()Lcom/google/android/gms/people/identity/internal/models/CombinedPersonImpl;

    move-result-object v2

    .line 58
    const/4 v0, 0x0

    .line 59
    if-eqz p3, :cond_0

    iget-object v1, p3, Lcom/google/android/gms/people/identity/s;->a:[B

    if-eqz v1, :cond_0

    .line 61
    :try_start_0
    iget v1, p3, Lcom/google/android/gms/people/identity/s;->b:I

    packed-switch v1, :pswitch_data_0

    .line 82
    :pswitch_0
    const-string v0, "DefaultPersonFactory"

    const-string v1, "Unrecognized data format"

    invoke-static {v0, v1}, Lcom/google/android/gms/people/internal/as;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 83
    const/4 v0, 0x0

    .line 101
    :goto_0
    return-object v0

    .line 63
    :pswitch_1
    new-instance v0, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl;

    invoke-direct {v0}, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl;-><init>()V

    .line 64
    iget v1, p3, Lcom/google/android/gms/people/identity/s;->c:I

    iget-object v3, p3, Lcom/google/android/gms/people/identity/s;->a:[B

    invoke-virtual {v0, v1, v3}, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl;->a(I[B)V

    .line 66
    invoke-virtual {v2, v0}, Lcom/google/android/gms/people/identity/internal/models/CombinedPersonImpl;->a(Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl;)Lcom/google/android/gms/people/identity/internal/models/CombinedPersonImpl;
    :try_end_0
    .catch Lcom/google/android/gms/common/server/response/m; {:try_start_0 .. :try_end_0} :catch_0

    .line 67
    const/4 v0, 0x1

    .line 91
    :cond_0
    :goto_1
    if-nez v0, :cond_5

    .line 92
    if-eqz p5, :cond_5

    .line 94
    if-eqz p5, :cond_5

    invoke-virtual {v2}, Lcom/google/android/gms/people/identity/internal/models/CombinedPersonImpl;->t()Z

    move-result v0

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$Metadata;

    invoke-direct {v0}, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$Metadata;-><init>()V

    invoke-virtual {v2, v0}, Lcom/google/android/gms/people/identity/internal/models/CombinedPersonImpl;->a(Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$Metadata;)Lcom/google/android/gms/people/identity/internal/models/CombinedPersonImpl;

    move-object v1, v0

    :goto_2
    invoke-virtual {p5}, Lcom/google/android/gms/people/identity/k;->e()I

    move-result v0

    packed-switch v0, :pswitch_data_1

    :goto_3
    invoke-virtual {p5}, Lcom/google/android/gms/people/identity/k;->g()Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {p5}, Lcom/google/android/gms/people/identity/k;->g()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_4
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/people/identity/m;

    invoke-virtual {v0}, Lcom/google/android/gms/people/identity/m;->a()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$Metadata;->e(Ljava/lang/String;)Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$Metadata;

    new-instance v4, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$Memberships;

    invoke-direct {v4}, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$Memberships;-><init>()V

    invoke-virtual {v0}, Lcom/google/android/gms/people/identity/m;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$Memberships;->e(Ljava/lang/String;)Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$Memberships;

    move-result-object v0

    new-instance v4, Lcom/google/android/gms/people/identity/internal/models/DefaultMetadataImpl;

    invoke-direct {v4}, Lcom/google/android/gms/people/identity/internal/models/DefaultMetadataImpl;-><init>()V

    const-string v5, "profile"

    invoke-virtual {v4, v5}, Lcom/google/android/gms/people/identity/internal/models/DefaultMetadataImpl;->e(Ljava/lang/String;)Lcom/google/android/gms/people/identity/internal/models/DefaultMetadataImpl;

    move-result-object v4

    invoke-virtual {p5}, Lcom/google/android/gms/people/identity/k;->b()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/google/android/gms/people/identity/internal/models/DefaultMetadataImpl;->g(Ljava/lang/String;)Lcom/google/android/gms/people/identity/internal/models/DefaultMetadataImpl;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$Memberships;->a(Lcom/google/android/gms/people/identity/internal/models/DefaultMetadataImpl;)Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$Memberships;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/google/android/gms/people/identity/internal/models/CombinedPersonImpl;->a(Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$Memberships;)Lcom/google/android/gms/people/identity/internal/models/CombinedPersonImpl;

    goto :goto_4

    .line 72
    :pswitch_2
    :try_start_1
    new-instance v1, Lcom/google/android/gms/people/identity/internal/models/aj;

    invoke-direct {v1}, Lcom/google/android/gms/people/identity/internal/models/aj;-><init>()V

    .line 73
    iget v3, p3, Lcom/google/android/gms/people/identity/s;->c:I

    iget-object v4, p3, Lcom/google/android/gms/people/identity/s;->a:[B

    invoke-virtual {v1, v3, v4}, Lcom/google/android/gms/people/identity/internal/models/aj;->a(I[B)V

    .line 75
    invoke-virtual {v1}, Lcom/google/android/gms/people/identity/internal/models/aj;->c()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {v1}, Lcom/google/android/gms/people/identity/internal/models/aj;->b()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-lez v3, :cond_0

    .line 76
    invoke-virtual {v1}, Lcom/google/android/gms/people/identity/internal/models/aj;->b()Ljava/util/List;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl;

    invoke-virtual {v2, v0}, Lcom/google/android/gms/people/identity/internal/models/CombinedPersonImpl;->a(Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl;)Lcom/google/android/gms/people/identity/internal/models/CombinedPersonImpl;
    :try_end_1
    .catch Lcom/google/android/gms/common/server/response/m; {:try_start_1 .. :try_end_1} :catch_0

    .line 77
    const/4 v0, 0x1

    goto/16 :goto_1

    .line 85
    :catch_0
    move-exception v0

    .line 86
    const-string v1, "DefaultPersonFactory"

    const-string v2, "ParseException"

    invoke-static {v1, v2, v0}, Lcom/google/android/gms/people/internal/as;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 87
    const/4 v0, 0x0

    goto/16 :goto_0

    .line 94
    :cond_1
    invoke-virtual {v2}, Lcom/google/android/gms/people/identity/internal/models/CombinedPersonImpl;->s()Lcom/google/android/gms/people/identity/models/r;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$Metadata;

    move-object v1, v0

    goto/16 :goto_2

    :pswitch_3
    const-string v0, "page"

    invoke-virtual {v1, v0}, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$Metadata;->f(Ljava/lang/String;)Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$Metadata;

    goto/16 :goto_3

    :pswitch_4
    const-string v0, "person"

    invoke-virtual {v1, v0}, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$Metadata;->f(Ljava/lang/String;)Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$Metadata;

    goto/16 :goto_3

    :cond_2
    new-instance v0, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$Names;

    invoke-direct {v0}, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$Names;-><init>()V

    invoke-virtual {p5}, Lcom/google/android/gms/people/identity/k;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$Names;->e(Ljava/lang/String;)Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$Names;

    move-result-object v0

    new-instance v1, Lcom/google/android/gms/people/identity/internal/models/DefaultMetadataImpl;

    invoke-direct {v1}, Lcom/google/android/gms/people/identity/internal/models/DefaultMetadataImpl;-><init>()V

    const-string v3, "profile"

    invoke-virtual {v1, v3}, Lcom/google/android/gms/people/identity/internal/models/DefaultMetadataImpl;->e(Ljava/lang/String;)Lcom/google/android/gms/people/identity/internal/models/DefaultMetadataImpl;

    move-result-object v1

    const/4 v3, 0x1

    invoke-virtual {v1, v3}, Lcom/google/android/gms/people/identity/internal/models/DefaultMetadataImpl;->a(Z)Lcom/google/android/gms/people/identity/internal/models/DefaultMetadataImpl;

    move-result-object v1

    const/4 v3, 0x0

    invoke-virtual {v1, v3}, Lcom/google/android/gms/people/identity/internal/models/DefaultMetadataImpl;->c(Z)Lcom/google/android/gms/people/identity/internal/models/DefaultMetadataImpl;

    move-result-object v1

    invoke-virtual {p5}, Lcom/google/android/gms/people/identity/k;->c()Z

    move-result v3

    invoke-virtual {v1, v3}, Lcom/google/android/gms/people/identity/internal/models/DefaultMetadataImpl;->b(Z)Lcom/google/android/gms/people/identity/internal/models/DefaultMetadataImpl;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$Names;->a(Lcom/google/android/gms/people/identity/internal/models/DefaultMetadataImpl;)Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$Names;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/google/android/gms/people/identity/internal/models/CombinedPersonImpl;->a(Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$Names;)Lcom/google/android/gms/people/identity/internal/models/CombinedPersonImpl;

    move-result-object v0

    invoke-virtual {p5}, Lcom/google/android/gms/people/identity/k;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/people/identity/internal/models/CombinedPersonImpl;->a(Ljava/lang/String;)Lcom/google/android/gms/people/identity/internal/models/CombinedPersonImpl;

    move-result-object v0

    new-instance v1, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$Taglines;

    invoke-direct {v1}, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$Taglines;-><init>()V

    invoke-virtual {p5}, Lcom/google/android/gms/people/identity/k;->d()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$Taglines;->e(Ljava/lang/String;)Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$Taglines;

    move-result-object v1

    new-instance v3, Lcom/google/android/gms/people/identity/internal/models/DefaultMetadataImpl;

    invoke-direct {v3}, Lcom/google/android/gms/people/identity/internal/models/DefaultMetadataImpl;-><init>()V

    const-string v4, "profile"

    invoke-virtual {v3, v4}, Lcom/google/android/gms/people/identity/internal/models/DefaultMetadataImpl;->e(Ljava/lang/String;)Lcom/google/android/gms/people/identity/internal/models/DefaultMetadataImpl;

    move-result-object v3

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Lcom/google/android/gms/people/identity/internal/models/DefaultMetadataImpl;->a(Z)Lcom/google/android/gms/people/identity/internal/models/DefaultMetadataImpl;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Lcom/google/android/gms/people/identity/internal/models/DefaultMetadataImpl;->c(Z)Lcom/google/android/gms/people/identity/internal/models/DefaultMetadataImpl;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$Taglines;->a(Lcom/google/android/gms/people/identity/internal/models/DefaultMetadataImpl;)Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$Taglines;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/people/identity/internal/models/CombinedPersonImpl;->a(Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$Taglines;)Lcom/google/android/gms/people/identity/internal/models/CombinedPersonImpl;

    move-result-object v0

    new-instance v1, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$Images;

    invoke-direct {v1}, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$Images;-><init>()V

    invoke-virtual {p5}, Lcom/google/android/gms/people/identity/k;->f()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$Images;->e(Ljava/lang/String;)Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$Images;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$Images;->g()Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$Images;

    move-result-object v1

    new-instance v3, Lcom/google/android/gms/people/identity/internal/models/DefaultMetadataImpl;

    invoke-direct {v3}, Lcom/google/android/gms/people/identity/internal/models/DefaultMetadataImpl;-><init>()V

    const-string v4, "profile"

    invoke-virtual {v3, v4}, Lcom/google/android/gms/people/identity/internal/models/DefaultMetadataImpl;->e(Ljava/lang/String;)Lcom/google/android/gms/people/identity/internal/models/DefaultMetadataImpl;

    move-result-object v3

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Lcom/google/android/gms/people/identity/internal/models/DefaultMetadataImpl;->a(Z)Lcom/google/android/gms/people/identity/internal/models/DefaultMetadataImpl;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Lcom/google/android/gms/people/identity/internal/models/DefaultMetadataImpl;->c(Z)Lcom/google/android/gms/people/identity/internal/models/DefaultMetadataImpl;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$Images;->a(Lcom/google/android/gms/people/identity/internal/models/DefaultMetadataImpl;)Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$Images;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/people/identity/internal/models/CombinedPersonImpl;->a(Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$Images;)Lcom/google/android/gms/people/identity/internal/models/CombinedPersonImpl;

    invoke-virtual {p5}, Lcom/google/android/gms/people/identity/k;->h()Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_3

    invoke-virtual {p5}, Lcom/google/android/gms/people/identity/k;->h()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_5
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/people/identity/l;

    new-instance v3, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$Addresses;

    invoke-direct {v3}, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$Addresses;-><init>()V

    iget-object v4, v0, Lcom/google/android/gms/people/identity/l;->a:Ljava/lang/String;

    invoke-virtual {v3, v4}, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$Addresses;->l(Ljava/lang/String;)Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$Addresses;

    move-result-object v3

    iget-object v0, v0, Lcom/google/android/gms/people/identity/l;->b:Ljava/lang/String;

    invoke-virtual {v3, v0}, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$Addresses;->h(Ljava/lang/String;)Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$Addresses;

    move-result-object v0

    new-instance v3, Lcom/google/android/gms/people/identity/internal/models/DefaultMetadataImpl;

    invoke-direct {v3}, Lcom/google/android/gms/people/identity/internal/models/DefaultMetadataImpl;-><init>()V

    const-string v4, "profile"

    invoke-virtual {v3, v4}, Lcom/google/android/gms/people/identity/internal/models/DefaultMetadataImpl;->e(Ljava/lang/String;)Lcom/google/android/gms/people/identity/internal/models/DefaultMetadataImpl;

    move-result-object v3

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Lcom/google/android/gms/people/identity/internal/models/DefaultMetadataImpl;->a(Z)Lcom/google/android/gms/people/identity/internal/models/DefaultMetadataImpl;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Lcom/google/android/gms/people/identity/internal/models/DefaultMetadataImpl;->c(Z)Lcom/google/android/gms/people/identity/internal/models/DefaultMetadataImpl;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$Addresses;->a(Lcom/google/android/gms/people/identity/internal/models/DefaultMetadataImpl;)Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$Addresses;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/google/android/gms/people/identity/internal/models/CombinedPersonImpl;->a(Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$Addresses;)Lcom/google/android/gms/people/identity/internal/models/CombinedPersonImpl;

    goto :goto_5

    :cond_3
    invoke-virtual {p5}, Lcom/google/android/gms/people/identity/k;->i()Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_4

    invoke-virtual {p5}, Lcom/google/android/gms/people/identity/k;->i()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_6
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/people/identity/o;

    new-instance v3, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$PhoneNumbers;

    invoke-direct {v3}, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$PhoneNumbers;-><init>()V

    iget-object v4, v0, Lcom/google/android/gms/people/identity/o;->a:Ljava/lang/String;

    invoke-virtual {v3, v4}, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$PhoneNumbers;->g(Ljava/lang/String;)Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$PhoneNumbers;

    move-result-object v3

    iget-object v0, v0, Lcom/google/android/gms/people/identity/o;->b:Ljava/lang/String;

    invoke-virtual {v3, v0}, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$PhoneNumbers;->e(Ljava/lang/String;)Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$PhoneNumbers;

    move-result-object v0

    new-instance v3, Lcom/google/android/gms/people/identity/internal/models/DefaultMetadataImpl;

    invoke-direct {v3}, Lcom/google/android/gms/people/identity/internal/models/DefaultMetadataImpl;-><init>()V

    const-string v4, "profile"

    invoke-virtual {v3, v4}, Lcom/google/android/gms/people/identity/internal/models/DefaultMetadataImpl;->e(Ljava/lang/String;)Lcom/google/android/gms/people/identity/internal/models/DefaultMetadataImpl;

    move-result-object v3

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Lcom/google/android/gms/people/identity/internal/models/DefaultMetadataImpl;->a(Z)Lcom/google/android/gms/people/identity/internal/models/DefaultMetadataImpl;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Lcom/google/android/gms/people/identity/internal/models/DefaultMetadataImpl;->c(Z)Lcom/google/android/gms/people/identity/internal/models/DefaultMetadataImpl;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$PhoneNumbers;->a(Lcom/google/android/gms/people/identity/internal/models/DefaultMetadataImpl;)Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$PhoneNumbers;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/google/android/gms/people/identity/internal/models/CombinedPersonImpl;->a(Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$PhoneNumbers;)Lcom/google/android/gms/people/identity/internal/models/CombinedPersonImpl;

    goto :goto_6

    :cond_4
    invoke-virtual {p5}, Lcom/google/android/gms/people/identity/k;->j()Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_5

    invoke-virtual {p5}, Lcom/google/android/gms/people/identity/k;->j()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_7
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/people/identity/n;

    new-instance v3, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$Emails;

    invoke-direct {v3}, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$Emails;-><init>()V

    iget-object v4, v0, Lcom/google/android/gms/people/identity/n;->a:Ljava/lang/String;

    invoke-virtual {v3, v4}, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$Emails;->g(Ljava/lang/String;)Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$Emails;

    move-result-object v3

    iget-object v0, v0, Lcom/google/android/gms/people/identity/n;->b:Ljava/lang/String;

    invoke-virtual {v3, v0}, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$Emails;->e(Ljava/lang/String;)Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$Emails;

    move-result-object v0

    new-instance v3, Lcom/google/android/gms/people/identity/internal/models/DefaultMetadataImpl;

    invoke-direct {v3}, Lcom/google/android/gms/people/identity/internal/models/DefaultMetadataImpl;-><init>()V

    const-string v4, "profile"

    invoke-virtual {v3, v4}, Lcom/google/android/gms/people/identity/internal/models/DefaultMetadataImpl;->e(Ljava/lang/String;)Lcom/google/android/gms/people/identity/internal/models/DefaultMetadataImpl;

    move-result-object v3

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Lcom/google/android/gms/people/identity/internal/models/DefaultMetadataImpl;->a(Z)Lcom/google/android/gms/people/identity/internal/models/DefaultMetadataImpl;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Lcom/google/android/gms/people/identity/internal/models/DefaultMetadataImpl;->c(Z)Lcom/google/android/gms/people/identity/internal/models/DefaultMetadataImpl;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$Emails;->a(Lcom/google/android/gms/people/identity/internal/models/DefaultMetadataImpl;)Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$Emails;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/google/android/gms/people/identity/internal/models/CombinedPersonImpl;->a(Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$Emails;)Lcom/google/android/gms/people/identity/internal/models/CombinedPersonImpl;

    goto :goto_7

    .line 98
    :cond_5
    if-eqz p4, :cond_a

    .line 99
    new-instance v3, Ljava/util/HashSet;

    invoke-direct {v3}, Ljava/util/HashSet;-><init>()V

    iget-object v0, p4, Lcom/google/android/gms/people/identity/h;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_6
    :goto_8
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_a

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/people/identity/i;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/gms/people/identity/i;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_7

    const/16 v1, 0xd

    invoke-virtual {v0, v1}, Lcom/google/android/gms/people/identity/i;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_6

    :cond_7
    iget-object v1, v0, Lcom/google/android/gms/people/identity/i;->a:Ljava/lang/String;

    invoke-virtual {v3, v1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_8

    iget-object v1, v0, Lcom/google/android/gms/people/identity/i;->a:Ljava/lang/String;

    invoke-virtual {v3, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    new-instance v1, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$Memberships;

    invoke-direct {v1}, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$Memberships;-><init>()V

    iget-object v5, v0, Lcom/google/android/gms/people/identity/i;->a:Ljava/lang/String;

    invoke-virtual {v1, v5}, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$Memberships;->f(Ljava/lang/String;)Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$Memberships;

    move-result-object v1

    invoke-static {v0}, Lcom/google/android/gms/people/identity/internal/r;->a(Lcom/google/android/gms/people/identity/i;)Lcom/google/android/gms/people/identity/internal/models/DefaultMetadataImpl;

    move-result-object v5

    invoke-virtual {v1, v5}, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$Memberships;->a(Lcom/google/android/gms/people/identity/internal/models/DefaultMetadataImpl;)Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$Memberships;

    move-result-object v1

    invoke-virtual {v2, v1}, Lcom/google/android/gms/people/identity/internal/models/CombinedPersonImpl;->a(Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$Memberships;)Lcom/google/android/gms/people/identity/internal/models/CombinedPersonImpl;

    :cond_8
    iget-object v5, v0, Lcom/google/android/gms/people/identity/i;->c:Ljava/lang/String;

    const/4 v1, -0x1

    invoke-virtual {v5}, Ljava/lang/String;->hashCode()I

    move-result v6

    sparse-switch v6, :sswitch_data_0

    :cond_9
    :goto_9
    packed-switch v1, :pswitch_data_2

    goto :goto_8

    :pswitch_5
    new-instance v1, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$Emails;

    invoke-direct {v1}, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$Emails;-><init>()V

    invoke-static {v0}, Lcom/google/android/gms/people/identity/internal/r;->a(Lcom/google/android/gms/people/identity/i;)Lcom/google/android/gms/people/identity/internal/models/DefaultMetadataImpl;

    move-result-object v5

    invoke-virtual {v1, v5}, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$Emails;->a(Lcom/google/android/gms/people/identity/internal/models/DefaultMetadataImpl;)Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$Emails;

    move-result-object v1

    invoke-static {v0}, Lcom/google/android/gms/people/identity/internal/h;->a(Lcom/google/android/gms/people/identity/i;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$Emails;->g(Ljava/lang/String;)Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$Emails;

    move-result-object v1

    invoke-static {v0}, Lcom/google/android/gms/people/identity/internal/h;->b(Lcom/google/android/gms/people/identity/i;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$Emails;->f(Ljava/lang/String;)Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$Emails;

    move-result-object v1

    invoke-static {p1, v0}, Lcom/google/android/gms/people/identity/internal/h;->a(Landroid/content/Context;Lcom/google/android/gms/people/identity/i;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$Emails;->e(Ljava/lang/String;)Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$Emails;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/google/android/gms/people/identity/internal/models/CombinedPersonImpl;->a(Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$Emails;)Lcom/google/android/gms/people/identity/internal/models/CombinedPersonImpl;

    goto :goto_8

    :sswitch_0
    const-string v6, "vnd.android.cursor.item/email_v2"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_9

    const/4 v1, 0x0

    goto :goto_9

    :sswitch_1
    const-string v6, "vnd.android.cursor.item/contact_event"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_9

    const/4 v1, 0x1

    goto :goto_9

    :sswitch_2
    const-string v6, "vnd.android.cursor.item/im"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_9

    const/4 v1, 0x2

    goto :goto_9

    :sswitch_3
    const-string v6, "vnd.android.cursor.item/nickname"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_9

    const/4 v1, 0x3

    goto :goto_9

    :sswitch_4
    const-string v6, "vnd.android.cursor.item/organization"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_9

    const/4 v1, 0x4

    goto :goto_9

    :sswitch_5
    const-string v6, "vnd.android.cursor.item/phone_v2"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_9

    const/4 v1, 0x5

    goto :goto_9

    :sswitch_6
    const-string v6, "vnd.android.cursor.item/relation"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_9

    const/4 v1, 0x6

    goto :goto_9

    :sswitch_7
    const-string v6, "vnd.android.cursor.item/name"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_9

    const/4 v1, 0x7

    goto :goto_9

    :sswitch_8
    const-string v6, "vnd.android.cursor.item/postal-address_v2"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_9

    const/16 v1, 0x8

    goto/16 :goto_9

    :sswitch_9
    const-string v6, "vnd.android.cursor.item/website"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_9

    const/16 v1, 0x9

    goto/16 :goto_9

    :sswitch_a
    const-string v6, "vnd.android.cursor.item/photo"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_9

    const/16 v1, 0xa

    goto/16 :goto_9

    :pswitch_6
    new-instance v1, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$Events;

    invoke-direct {v1}, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$Events;-><init>()V

    invoke-static {v0}, Lcom/google/android/gms/people/identity/internal/r;->a(Lcom/google/android/gms/people/identity/i;)Lcom/google/android/gms/people/identity/internal/models/DefaultMetadataImpl;

    move-result-object v5

    invoke-virtual {v1, v5}, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$Events;->a(Lcom/google/android/gms/people/identity/internal/models/DefaultMetadataImpl;)Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$Events;

    move-result-object v1

    invoke-static {v0}, Lcom/google/android/gms/people/identity/internal/i;->a(Lcom/google/android/gms/people/identity/i;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$Events;->e(Ljava/lang/String;)Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$Events;

    move-result-object v1

    invoke-static {p1, v0}, Lcom/google/android/gms/people/identity/internal/i;->a(Landroid/content/Context;Lcom/google/android/gms/people/identity/i;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$Events;->g(Ljava/lang/String;)Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$Events;

    move-result-object v1

    invoke-static {v0}, Lcom/google/android/gms/people/identity/internal/i;->b(Lcom/google/android/gms/people/identity/i;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$Events;->f(Ljava/lang/String;)Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$Events;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/google/android/gms/people/identity/internal/models/CombinedPersonImpl;->a(Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$Events;)Lcom/google/android/gms/people/identity/internal/models/CombinedPersonImpl;

    goto/16 :goto_8

    :pswitch_7
    new-instance v1, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$InstantMessaging;

    invoke-direct {v1}, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$InstantMessaging;-><init>()V

    invoke-static {v0}, Lcom/google/android/gms/people/identity/internal/r;->a(Lcom/google/android/gms/people/identity/i;)Lcom/google/android/gms/people/identity/internal/models/DefaultMetadataImpl;

    move-result-object v5

    invoke-virtual {v1, v5}, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$InstantMessaging;->a(Lcom/google/android/gms/people/identity/internal/models/DefaultMetadataImpl;)Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$InstantMessaging;

    move-result-object v1

    invoke-static {v0}, Lcom/google/android/gms/people/identity/internal/j;->a(Lcom/google/android/gms/people/identity/i;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$InstantMessaging;->i(Ljava/lang/String;)Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$InstantMessaging;

    move-result-object v1

    invoke-static {v0}, Lcom/google/android/gms/people/identity/internal/j;->b(Lcom/google/android/gms/people/identity/i;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$InstantMessaging;->h(Ljava/lang/String;)Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$InstantMessaging;

    move-result-object v1

    invoke-static {p1, v0}, Lcom/google/android/gms/people/identity/internal/j;->a(Landroid/content/Context;Lcom/google/android/gms/people/identity/i;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$InstantMessaging;->f(Ljava/lang/String;)Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$InstantMessaging;

    move-result-object v1

    invoke-static {v0}, Lcom/google/android/gms/people/identity/internal/j;->c(Lcom/google/android/gms/people/identity/i;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$InstantMessaging;->g(Ljava/lang/String;)Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$InstantMessaging;

    move-result-object v1

    invoke-static {p1, v0}, Lcom/google/android/gms/people/identity/internal/j;->b(Landroid/content/Context;Lcom/google/android/gms/people/identity/i;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$InstantMessaging;->e(Ljava/lang/String;)Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$InstantMessaging;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/google/android/gms/people/identity/internal/models/CombinedPersonImpl;->a(Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$InstantMessaging;)Lcom/google/android/gms/people/identity/internal/models/CombinedPersonImpl;

    goto/16 :goto_8

    :pswitch_8
    new-instance v1, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$Nicknames;

    invoke-direct {v1}, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$Nicknames;-><init>()V

    invoke-static {v0}, Lcom/google/android/gms/people/identity/internal/r;->a(Lcom/google/android/gms/people/identity/i;)Lcom/google/android/gms/people/identity/internal/models/DefaultMetadataImpl;

    move-result-object v5

    invoke-virtual {v1, v5}, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$Nicknames;->a(Lcom/google/android/gms/people/identity/internal/models/DefaultMetadataImpl;)Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$Nicknames;

    move-result-object v1

    invoke-static {v0}, Lcom/google/android/gms/people/identity/internal/l;->a(Lcom/google/android/gms/people/identity/i;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$Nicknames;->f(Ljava/lang/String;)Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$Nicknames;

    move-result-object v1

    invoke-static {v0}, Lcom/google/android/gms/people/identity/internal/l;->b(Lcom/google/android/gms/people/identity/i;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$Nicknames;->e(Ljava/lang/String;)Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$Nicknames;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/google/android/gms/people/identity/internal/models/CombinedPersonImpl;->a(Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$Nicknames;)Lcom/google/android/gms/people/identity/internal/models/CombinedPersonImpl;

    goto/16 :goto_8

    :pswitch_9
    new-instance v1, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$Organizations;

    invoke-direct {v1}, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$Organizations;-><init>()V

    invoke-static {v0}, Lcom/google/android/gms/people/identity/internal/r;->a(Lcom/google/android/gms/people/identity/i;)Lcom/google/android/gms/people/identity/internal/models/DefaultMetadataImpl;

    move-result-object v5

    invoke-virtual {v1, v5}, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$Organizations;->a(Lcom/google/android/gms/people/identity/internal/models/DefaultMetadataImpl;)Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$Organizations;

    move-result-object v1

    invoke-static {v0}, Lcom/google/android/gms/people/identity/internal/m;->a(Lcom/google/android/gms/people/identity/i;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$Organizations;->h(Ljava/lang/String;)Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$Organizations;

    move-result-object v1

    invoke-static {v0}, Lcom/google/android/gms/people/identity/internal/m;->b(Lcom/google/android/gms/people/identity/i;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$Organizations;->l(Ljava/lang/String;)Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$Organizations;

    move-result-object v1

    invoke-static {v0}, Lcom/google/android/gms/people/identity/internal/m;->c(Lcom/google/android/gms/people/identity/i;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$Organizations;->k(Ljava/lang/String;)Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$Organizations;

    move-result-object v1

    invoke-static {v0}, Lcom/google/android/gms/people/identity/internal/m;->d(Lcom/google/android/gms/people/identity/i;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$Organizations;->e(Ljava/lang/String;)Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$Organizations;

    move-result-object v1

    invoke-static {v0}, Lcom/google/android/gms/people/identity/internal/m;->e(Lcom/google/android/gms/people/identity/i;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$Organizations;->f(Ljava/lang/String;)Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$Organizations;

    move-result-object v1

    invoke-static {v0}, Lcom/google/android/gms/people/identity/internal/m;->f(Lcom/google/android/gms/people/identity/i;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$Organizations;->j(Ljava/lang/String;)Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$Organizations;

    move-result-object v1

    invoke-static {v0}, Lcom/google/android/gms/people/identity/internal/m;->g(Lcom/google/android/gms/people/identity/i;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$Organizations;->i(Ljava/lang/String;)Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$Organizations;

    move-result-object v1

    invoke-static {v0}, Lcom/google/android/gms/people/identity/internal/m;->h(Lcom/google/android/gms/people/identity/i;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$Organizations;->g(Ljava/lang/String;)Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$Organizations;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/google/android/gms/people/identity/internal/models/CombinedPersonImpl;->a(Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$Organizations;)Lcom/google/android/gms/people/identity/internal/models/CombinedPersonImpl;

    goto/16 :goto_8

    :pswitch_a
    new-instance v1, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$PhoneNumbers;

    invoke-direct {v1}, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$PhoneNumbers;-><init>()V

    invoke-static {v0}, Lcom/google/android/gms/people/identity/internal/r;->a(Lcom/google/android/gms/people/identity/i;)Lcom/google/android/gms/people/identity/internal/models/DefaultMetadataImpl;

    move-result-object v5

    invoke-virtual {v1, v5}, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$PhoneNumbers;->a(Lcom/google/android/gms/people/identity/internal/models/DefaultMetadataImpl;)Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$PhoneNumbers;

    move-result-object v1

    invoke-static {v0}, Lcom/google/android/gms/people/identity/internal/n;->a(Lcom/google/android/gms/people/identity/i;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$PhoneNumbers;->g(Ljava/lang/String;)Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$PhoneNumbers;

    move-result-object v1

    invoke-static {v0}, Lcom/google/android/gms/people/identity/internal/n;->b(Lcom/google/android/gms/people/identity/i;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$PhoneNumbers;->f(Ljava/lang/String;)Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$PhoneNumbers;

    move-result-object v1

    invoke-static {p1, v0}, Lcom/google/android/gms/people/identity/internal/n;->a(Landroid/content/Context;Lcom/google/android/gms/people/identity/i;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$PhoneNumbers;->e(Ljava/lang/String;)Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$PhoneNumbers;

    move-result-object v1

    iget v0, v0, Lcom/google/android/gms/people/identity/i;->g:I

    invoke-virtual {v1, v0}, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$PhoneNumbers;->a(I)Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$PhoneNumbers;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/google/android/gms/people/identity/internal/models/CombinedPersonImpl;->a(Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$PhoneNumbers;)Lcom/google/android/gms/people/identity/internal/models/CombinedPersonImpl;

    goto/16 :goto_8

    :pswitch_b
    new-instance v1, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$Relations;

    invoke-direct {v1}, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$Relations;-><init>()V

    invoke-static {v0}, Lcom/google/android/gms/people/identity/internal/r;->a(Lcom/google/android/gms/people/identity/i;)Lcom/google/android/gms/people/identity/internal/models/DefaultMetadataImpl;

    move-result-object v5

    invoke-virtual {v1, v5}, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$Relations;->a(Lcom/google/android/gms/people/identity/internal/models/DefaultMetadataImpl;)Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$Relations;

    move-result-object v1

    invoke-static {v0}, Lcom/google/android/gms/people/identity/internal/o;->a(Lcom/google/android/gms/people/identity/i;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$Relations;->g(Ljava/lang/String;)Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$Relations;

    move-result-object v1

    invoke-static {v0}, Lcom/google/android/gms/people/identity/internal/o;->b(Lcom/google/android/gms/people/identity/i;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$Relations;->f(Ljava/lang/String;)Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$Relations;

    move-result-object v1

    invoke-static {p1, v0}, Lcom/google/android/gms/people/identity/internal/o;->a(Landroid/content/Context;Lcom/google/android/gms/people/identity/i;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$Relations;->e(Ljava/lang/String;)Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$Relations;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/google/android/gms/people/identity/internal/models/CombinedPersonImpl;->a(Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$Relations;)Lcom/google/android/gms/people/identity/internal/models/CombinedPersonImpl;

    goto/16 :goto_8

    :pswitch_c
    new-instance v1, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$Names;

    invoke-direct {v1}, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$Names;-><init>()V

    invoke-static {v0}, Lcom/google/android/gms/people/identity/internal/r;->a(Lcom/google/android/gms/people/identity/i;)Lcom/google/android/gms/people/identity/internal/models/DefaultMetadataImpl;

    move-result-object v5

    invoke-virtual {v1, v5}, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$Names;->a(Lcom/google/android/gms/people/identity/internal/models/DefaultMetadataImpl;)Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$Names;

    move-result-object v1

    const/4 v5, 0x0

    invoke-virtual {v0, v5}, Lcom/google/android/gms/people/identity/i;->a(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$Names;->e(Ljava/lang/String;)Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$Names;

    move-result-object v1

    const/4 v5, 0x1

    invoke-virtual {v0, v5}, Lcom/google/android/gms/people/identity/i;->a(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$Names;->g(Ljava/lang/String;)Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$Names;

    move-result-object v1

    const/4 v5, 0x2

    invoke-virtual {v0, v5}, Lcom/google/android/gms/people/identity/i;->a(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$Names;->f(Ljava/lang/String;)Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$Names;

    move-result-object v1

    const/4 v5, 0x3

    invoke-virtual {v0, v5}, Lcom/google/android/gms/people/identity/i;->a(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$Names;->h(Ljava/lang/String;)Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$Names;

    move-result-object v1

    const/4 v5, 0x4

    invoke-virtual {v0, v5}, Lcom/google/android/gms/people/identity/i;->a(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$Names;->j(Ljava/lang/String;)Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$Names;

    move-result-object v1

    const/4 v5, 0x5

    invoke-virtual {v0, v5}, Lcom/google/android/gms/people/identity/i;->a(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$Names;->i(Ljava/lang/String;)Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$Names;

    move-result-object v1

    const/4 v5, 0x6

    invoke-virtual {v0, v5}, Lcom/google/android/gms/people/identity/i;->a(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$Names;->l(Ljava/lang/String;)Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$Names;

    move-result-object v1

    const/16 v5, 0x8

    invoke-virtual {v0, v5}, Lcom/google/android/gms/people/identity/i;->a(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$Names;->k(Ljava/lang/String;)Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$Names;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/google/android/gms/people/identity/internal/models/CombinedPersonImpl;->a(Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$Names;)Lcom/google/android/gms/people/identity/internal/models/CombinedPersonImpl;

    goto/16 :goto_8

    :pswitch_d
    new-instance v1, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$Addresses;

    invoke-direct {v1}, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$Addresses;-><init>()V

    invoke-static {v0}, Lcom/google/android/gms/people/identity/internal/r;->a(Lcom/google/android/gms/people/identity/i;)Lcom/google/android/gms/people/identity/internal/models/DefaultMetadataImpl;

    move-result-object v5

    invoke-virtual {v1, v5}, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$Addresses;->a(Lcom/google/android/gms/people/identity/internal/models/DefaultMetadataImpl;)Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$Addresses;

    move-result-object v1

    invoke-static {v0}, Lcom/google/android/gms/people/identity/internal/g;->a(Lcom/google/android/gms/people/identity/i;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$Addresses;->m(Ljava/lang/String;)Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$Addresses;

    move-result-object v1

    invoke-static {p1, v0}, Lcom/google/android/gms/people/identity/internal/g;->a(Landroid/content/Context;Lcom/google/android/gms/people/identity/i;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$Addresses;->h(Ljava/lang/String;)Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$Addresses;

    move-result-object v1

    invoke-static {v0}, Lcom/google/android/gms/people/identity/internal/g;->b(Lcom/google/android/gms/people/identity/i;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$Addresses;->l(Ljava/lang/String;)Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$Addresses;

    move-result-object v1

    invoke-static {v0}, Lcom/google/android/gms/people/identity/internal/g;->c(Lcom/google/android/gms/people/identity/i;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$Addresses;->i(Ljava/lang/String;)Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$Addresses;

    move-result-object v1

    invoke-static {v0}, Lcom/google/android/gms/people/identity/internal/g;->d(Lcom/google/android/gms/people/identity/i;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$Addresses;->e(Ljava/lang/String;)Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$Addresses;

    move-result-object v1

    invoke-static {v0}, Lcom/google/android/gms/people/identity/internal/g;->e(Lcom/google/android/gms/people/identity/i;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$Addresses;->k(Ljava/lang/String;)Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$Addresses;

    move-result-object v1

    invoke-static {v0}, Lcom/google/android/gms/people/identity/internal/g;->f(Lcom/google/android/gms/people/identity/i;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$Addresses;->j(Ljava/lang/String;)Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$Addresses;

    move-result-object v1

    invoke-static {v0}, Lcom/google/android/gms/people/identity/internal/g;->g(Lcom/google/android/gms/people/identity/i;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$Addresses;->f(Ljava/lang/String;)Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$Addresses;

    move-result-object v1

    invoke-static {v0}, Lcom/google/android/gms/people/identity/internal/g;->h(Lcom/google/android/gms/people/identity/i;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$Addresses;->g(Ljava/lang/String;)Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$Addresses;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/google/android/gms/people/identity/internal/models/CombinedPersonImpl;->a(Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$Addresses;)Lcom/google/android/gms/people/identity/internal/models/CombinedPersonImpl;

    goto/16 :goto_8

    :pswitch_e
    new-instance v1, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$Urls;

    invoke-direct {v1}, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$Urls;-><init>()V

    invoke-static {v0}, Lcom/google/android/gms/people/identity/internal/r;->a(Lcom/google/android/gms/people/identity/i;)Lcom/google/android/gms/people/identity/internal/models/DefaultMetadataImpl;

    move-result-object v5

    invoke-virtual {v1, v5}, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$Urls;->a(Lcom/google/android/gms/people/identity/internal/models/DefaultMetadataImpl;)Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$Urls;

    move-result-object v1

    invoke-static {v0}, Lcom/google/android/gms/people/identity/internal/p;->a(Lcom/google/android/gms/people/identity/i;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$Urls;->f(Ljava/lang/String;)Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$Urls;

    move-result-object v1

    invoke-static {v0}, Lcom/google/android/gms/people/identity/internal/p;->b(Lcom/google/android/gms/people/identity/i;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$Urls;->e(Ljava/lang/String;)Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$Urls;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/google/android/gms/people/identity/internal/models/CombinedPersonImpl;->a(Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$Urls;)Lcom/google/android/gms/people/identity/internal/models/CombinedPersonImpl;

    goto/16 :goto_8

    :pswitch_f
    const/16 v1, 0xd

    invoke-virtual {v0, v1}, Lcom/google/android/gms/people/identity/i;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_6

    new-instance v1, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$Images;

    invoke-direct {v1}, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$Images;-><init>()V

    invoke-static {v0}, Lcom/google/android/gms/people/identity/internal/r;->a(Lcom/google/android/gms/people/identity/i;)Lcom/google/android/gms/people/identity/internal/models/DefaultMetadataImpl;

    move-result-object v5

    invoke-virtual {v1, v5}, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$Images;->a(Lcom/google/android/gms/people/identity/internal/models/DefaultMetadataImpl;)Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$Images;

    move-result-object v1

    invoke-static {v0}, Lcom/google/android/gms/people/identity/internal/k;->a(Lcom/google/android/gms/people/identity/i;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$Images;->e(Ljava/lang/String;)Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$Images;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/google/android/gms/people/identity/internal/models/CombinedPersonImpl;->a(Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$Images;)Lcom/google/android/gms/people/identity/internal/models/CombinedPersonImpl;

    goto/16 :goto_8

    :cond_a
    move-object v0, v2

    .line 101
    goto/16 :goto_0

    .line 61
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch

    .line 94
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_4
        :pswitch_3
    .end packed-switch

    .line 99
    :sswitch_data_0
    .sparse-switch
        -0x5d8d3afc -> :sswitch_0
        -0x4f32162a -> :sswitch_1
        -0x4053a7f0 -> :sswitch_7
        -0x23d6087c -> :sswitch_8
        0x1b3458f6 -> :sswitch_9
        0x28c7a9f2 -> :sswitch_5
        0x291e75b8 -> :sswitch_4
        0x35fe114d -> :sswitch_a
        0x38ac87e9 -> :sswitch_2
        0x54088d01 -> :sswitch_6
        0x794b3b73 -> :sswitch_3
    .end sparse-switch

    :pswitch_data_2
    .packed-switch 0x0
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
    .end packed-switch
.end method
