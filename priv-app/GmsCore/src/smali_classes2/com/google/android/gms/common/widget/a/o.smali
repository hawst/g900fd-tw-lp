.class public Lcom/google/android/gms/common/widget/a/o;
.super Lcom/google/android/gms/common/widget/a/a;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private final a:Landroid/content/Context;

.field private b:I

.field private c:Ljava/lang/CharSequence;

.field private d:Landroid/graphics/drawable/Drawable;

.field private e:Landroid/content/Intent;

.field private f:Lcom/google/android/gms/common/widget/a/p;

.field private g:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 32
    invoke-direct {p0}, Lcom/google/android/gms/common/widget/a/a;-><init>()V

    .line 30
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/common/widget/a/o;->g:Z

    .line 33
    iput-object p1, p0, Lcom/google/android/gms/common/widget/a/o;->a:Landroid/content/Context;

    .line 34
    return-void
.end method


# virtual methods
.method public final a(I)V
    .locals 0

    .prologue
    .line 73
    invoke-super {p0, p1}, Lcom/google/android/gms/common/widget/a/a;->a(I)V

    .line 74
    invoke-virtual {p0}, Lcom/google/android/gms/common/widget/a/o;->d()V

    .line 75
    return-void
.end method

.method public final a(Landroid/content/Intent;)V
    .locals 0

    .prologue
    .line 190
    iput-object p1, p0, Lcom/google/android/gms/common/widget/a/o;->e:Landroid/content/Intent;

    .line 191
    invoke-virtual {p0}, Lcom/google/android/gms/common/widget/a/o;->d()V

    .line 192
    return-void
.end method

.method public final a(Lcom/google/android/gms/common/widget/a/p;)V
    .locals 0

    .prologue
    .line 176
    iput-object p1, p0, Lcom/google/android/gms/common/widget/a/o;->f:Lcom/google/android/gms/common/widget/a/p;

    .line 177
    invoke-virtual {p0}, Lcom/google/android/gms/common/widget/a/o;->d()V

    .line 178
    return-void
.end method

.method public final a(Ljava/lang/CharSequence;)V
    .locals 0

    .prologue
    .line 57
    invoke-super {p0, p1}, Lcom/google/android/gms/common/widget/a/a;->a(Ljava/lang/CharSequence;)V

    .line 58
    invoke-virtual {p0}, Lcom/google/android/gms/common/widget/a/o;->d()V

    .line 59
    return-void
.end method

.method public final a(Z)V
    .locals 0

    .prologue
    .line 211
    iput-boolean p1, p0, Lcom/google/android/gms/common/widget/a/o;->g:Z

    .line 212
    invoke-virtual {p0}, Lcom/google/android/gms/common/widget/a/o;->d()V

    .line 213
    return-void
.end method

.method public final b(I)V
    .locals 0

    .prologue
    .line 51
    iput p1, p0, Lcom/google/android/gms/common/widget/a/o;->b:I

    .line 52
    invoke-virtual {p0}, Lcom/google/android/gms/common/widget/a/o;->d()V

    .line 53
    return-void
.end method

.method public final b(Ljava/lang/CharSequence;)V
    .locals 0

    .prologue
    .line 108
    iput-object p1, p0, Lcom/google/android/gms/common/widget/a/o;->c:Ljava/lang/CharSequence;

    .line 109
    invoke-virtual {p0}, Lcom/google/android/gms/common/widget/a/o;->d()V

    .line 110
    return-void
.end method

.method public final c(I)V
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Lcom/google/android/gms/common/widget/a/o;->a:Landroid/content/Context;

    invoke-virtual {v0, p1}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/common/widget/a/o;->a(Ljava/lang/CharSequence;)V

    .line 69
    return-void
.end method

.method public final d(I)V
    .locals 1

    .prologue
    .line 95
    iget-object v0, p0, Lcom/google/android/gms/common/widget/a/o;->a:Landroid/content/Context;

    invoke-virtual {v0, p1}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/common/widget/a/o;->b(Ljava/lang/CharSequence;)V

    .line 96
    return-void
.end method

.method public e()I
    .locals 1

    .prologue
    .line 229
    sget v0, Lcom/google/android/gms/common/widget/e;->b:I

    return v0
.end method

.method public f()Lcom/google/android/gms/common/widget/a/h;
    .locals 1

    .prologue
    .line 235
    invoke-static {}, Lcom/google/android/gms/common/widget/a/c;->a()Lcom/google/android/gms/common/widget/a/c;

    move-result-object v0

    return-object v0
.end method

.method public final g()I
    .locals 1

    .prologue
    .line 41
    iget v0, p0, Lcom/google/android/gms/common/widget/a/o;->b:I

    return v0
.end method

.method public final h()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Lcom/google/android/gms/common/widget/a/o;->c:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public final i()Landroid/graphics/drawable/Drawable;
    .locals 1

    .prologue
    .line 119
    iget-object v0, p0, Lcom/google/android/gms/common/widget/a/o;->d:Landroid/graphics/drawable/Drawable;

    return-object v0
.end method

.method public final j()Z
    .locals 1

    .prologue
    .line 163
    iget-object v0, p0, Lcom/google/android/gms/common/widget/a/o;->f:Lcom/google/android/gms/common/widget/a/p;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/common/widget/a/o;->e:Landroid/content/Intent;

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final k()Z
    .locals 1

    .prologue
    .line 198
    iget-boolean v0, p0, Lcom/google/android/gms/common/widget/a/o;->g:Z

    return v0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 218
    iget-object v0, p0, Lcom/google/android/gms/common/widget/a/o;->f:Lcom/google/android/gms/common/widget/a/p;

    if-eqz v0, :cond_0

    .line 219
    iget-object v0, p0, Lcom/google/android/gms/common/widget/a/o;->f:Lcom/google/android/gms/common/widget/a/p;

    invoke-interface {v0, p1, p0}, Lcom/google/android/gms/common/widget/a/p;->onClick(Landroid/view/View;Lcom/google/android/gms/common/widget/a/o;)V

    .line 221
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/common/widget/a/o;->e:Landroid/content/Intent;

    if-eqz v0, :cond_1

    .line 222
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/common/widget/a/o;->e:Landroid/content/Intent;

    invoke-virtual {v0, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 224
    :cond_1
    return-void
.end method
