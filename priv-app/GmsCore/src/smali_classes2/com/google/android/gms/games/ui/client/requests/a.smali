.class public final Lcom/google/android/gms/games/ui/client/requests/a;
.super Lcom/google/android/gms/games/ui/common/requests/l;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/android/gms/games/ui/client/a;

.field private final b:Lcom/google/android/gms/games/ui/client/a/a;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/games/ui/client/a;)V
    .locals 1

    .prologue
    .line 34
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/common/requests/l;-><init>()V

    .line 35
    invoke-static {p1}, Lcom/google/android/gms/common/internal/e;->a(Ljava/lang/Object;)V

    .line 36
    iput-object p1, p0, Lcom/google/android/gms/games/ui/client/requests/a;->a:Lcom/google/android/gms/games/ui/client/a;

    .line 38
    new-instance v0, Lcom/google/android/gms/games/ui/client/a/a;

    invoke-direct {v0, p1}, Lcom/google/android/gms/games/ui/client/a/a;-><init>(Lcom/google/android/gms/games/ui/client/a;)V

    iput-object v0, p0, Lcom/google/android/gms/games/ui/client/requests/a;->b:Lcom/google/android/gms/games/ui/client/a/a;

    .line 39
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/games/Game;)V
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/requests/a;->b:Lcom/google/android/gms/games/ui/client/a/a;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/games/ui/client/a/a;->a(Lcom/google/android/gms/games/Game;)V

    .line 80
    return-void
.end method

.method public final a(Lcom/google/android/gms/games/internal/request/GameRequestCluster;)V
    .locals 4

    .prologue
    .line 98
    invoke-virtual {p1}, Lcom/google/android/gms/games/internal/request/GameRequestCluster;->d()Ljava/util/ArrayList;

    move-result-object v2

    .line 99
    const/4 v0, 0x0

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v3

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    .line 100
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/request/GameRequest;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/client/requests/a;->a(Lcom/google/android/gms/games/request/GameRequest;)V

    .line 99
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 102
    :cond_0
    return-void
.end method

.method public final a(Lcom/google/android/gms/games/internal/request/GameRequestCluster;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 85
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/requests/a;->a:Lcom/google/android/gms/games/ui/client/a;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/client/a;->w()Lcom/google/android/gms/common/api/v;

    move-result-object v0

    .line 86
    iget-object v1, p0, Lcom/google/android/gms/games/ui/client/requests/a;->a:Lcom/google/android/gms/games/ui/client/a;

    invoke-static {v0, v1}, Lcom/google/android/gms/games/ui/d/al;->a(Lcom/google/android/gms/common/api/v;Lcom/google/android/gms/games/ui/q;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 87
    const-string v0, "ClientReqInboxHelper"

    const-string v1, "onRequestClusterSeeMoreClicked: not connected; ignoring..."

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 94
    :goto_0
    return-void

    .line 91
    :cond_0
    sget-object v1, Lcom/google/android/gms/games/d;->r:Lcom/google/android/gms/games/request/d;

    invoke-interface {v1, v0, p1, p2}, Lcom/google/android/gms/games/request/d;->a(Lcom/google/android/gms/common/api/v;Lcom/google/android/gms/games/internal/request/GameRequestCluster;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 93
    iget-object v1, p0, Lcom/google/android/gms/games/ui/client/requests/a;->a:Lcom/google/android/gms/games/ui/client/a;

    const/16 v2, 0x3e8

    invoke-virtual {v1, v0, v2}, Lcom/google/android/gms/games/ui/client/a;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/games/request/GameRequest;)V
    .locals 3

    .prologue
    .line 68
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/requests/a;->a:Lcom/google/android/gms/games/ui/client/a;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/client/a;->w()Lcom/google/android/gms/common/api/v;

    move-result-object v0

    .line 69
    iget-object v1, p0, Lcom/google/android/gms/games/ui/client/requests/a;->a:Lcom/google/android/gms/games/ui/client/a;

    invoke-static {v0, v1}, Lcom/google/android/gms/games/ui/d/al;->a(Lcom/google/android/gms/common/api/v;Lcom/google/android/gms/games/ui/q;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 70
    const-string v0, "ClientReqInboxHelper"

    const-string v1, "onRequestDismissed: not connected; ignoring..."

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 75
    :goto_0
    return-void

    .line 74
    :cond_0
    sget-object v1, Lcom/google/android/gms/games/d;->r:Lcom/google/android/gms/games/request/d;

    invoke-interface {p1}, Lcom/google/android/gms/games/request/GameRequest;->e()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v0, v2}, Lcom/google/android/gms/games/request/d;->a(Lcom/google/android/gms/common/api/v;Ljava/lang/String;)Lcom/google/android/gms/common/api/am;

    goto :goto_0
.end method

.method public final varargs a([Lcom/google/android/gms/games/request/GameRequest;)V
    .locals 3

    .prologue
    .line 43
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/requests/a;->a:Lcom/google/android/gms/games/ui/client/a;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/client/a;->w()Lcom/google/android/gms/common/api/v;

    move-result-object v0

    .line 44
    iget-object v1, p0, Lcom/google/android/gms/games/ui/client/requests/a;->a:Lcom/google/android/gms/games/ui/client/a;

    invoke-static {v0, v1}, Lcom/google/android/gms/games/ui/d/al;->a(Lcom/google/android/gms/common/api/v;Lcom/google/android/gms/games/ui/q;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 46
    const-string v0, "ClientReqInboxHelper"

    const-string v1, "onRequestClicked: not connected; ignoring..."

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 51
    :goto_0
    return-void

    .line 50
    :cond_0
    invoke-static {p1}, Lcom/google/android/gms/common/data/v;->a([Lcom/google/android/gms/common/data/u;)Ljava/util/ArrayList;

    move-result-object v0

    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    const-string v2, "requests"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putParcelableArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/requests/a;->a:Lcom/google/android/gms/games/ui/client/a;

    const/4 v2, -0x1

    invoke-virtual {v0, v2, v1}, Lcom/google/android/gms/games/ui/client/a;->setResult(ILandroid/content/Intent;)V

    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/requests/a;->a:Lcom/google/android/gms/games/ui/client/a;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/client/a;->finish()V

    goto :goto_0
.end method
