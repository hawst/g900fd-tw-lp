.class final Lcom/google/android/gms/car/dl;
.super Landroid/telecom/Phone$Listener;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/gms/car/dj;


# direct methods
.method constructor <init>(Lcom/google/android/gms/car/dj;)V
    .locals 0

    .prologue
    .line 593
    iput-object p1, p0, Lcom/google/android/gms/car/dl;->a:Lcom/google/android/gms/car/dj;

    invoke-direct {p0}, Landroid/telecom/Phone$Listener;-><init>()V

    return-void
.end method


# virtual methods
.method public final onAudioStateChanged(Landroid/telecom/Phone;Landroid/telecom/AudioState;)V
    .locals 4

    .prologue
    .line 597
    iget-object v0, p0, Lcom/google/android/gms/car/dl;->a:Lcom/google/android/gms/car/dj;

    invoke-static {v0}, Lcom/google/android/gms/car/dj;->a(Lcom/google/android/gms/car/dj;)Lcom/google/android/gms/car/dn;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 599
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/car/dl;->a:Lcom/google/android/gms/car/dj;

    invoke-static {v0}, Lcom/google/android/gms/car/dj;->a(Lcom/google/android/gms/car/dj;)Lcom/google/android/gms/car/dn;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/gms/car/dn;->a:Lcom/google/android/gms/car/le;

    invoke-static {p2}, Lcom/google/android/gms/car/dj;->a(Landroid/telecom/AudioState;)Z

    move-result v1

    invoke-static {p2}, Lcom/google/android/gms/car/dj;->b(Landroid/telecom/AudioState;)I

    move-result v2

    invoke-static {p2}, Lcom/google/android/gms/car/dj;->c(Landroid/telecom/AudioState;)I

    move-result v3

    invoke-interface {v0, v1, v2, v3}, Lcom/google/android/gms/car/le;->a(ZII)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 606
    :cond_0
    :goto_0
    return-void

    .line 602
    :catch_0
    move-exception v0

    .line 603
    const-string v1, "CAR.TEL.CarCallService"

    const-string v2, "RemoteException in CarCallListener."

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public final onCallAdded(Landroid/telecom/Phone;Landroid/telecom/Call;)V
    .locals 4

    .prologue
    const/4 v2, 0x3

    .line 610
    invoke-static {p2}, Lcom/google/android/gms/car/dr;->a(Landroid/telecom/Call;)Lcom/google/android/gms/car/CarCall;

    move-result-object v0

    .line 612
    const-string v1, "CAR.TEL.CarCallService"

    invoke-static {v1, v2}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 613
    const-string v1, "CAR.TEL.CarCallService"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "onCallAdded ("

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v3, v0, Lcom/google/android/gms/car/CarCall;->b:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ")"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 616
    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/car/dl;->a:Lcom/google/android/gms/car/dj;

    invoke-static {v1}, Lcom/google/android/gms/car/dj;->b(Lcom/google/android/gms/car/dj;)Landroid/telecom/Call$Listener;

    move-result-object v1

    invoke-virtual {p2, v1}, Landroid/telecom/Call;->addListener(Landroid/telecom/Call$Listener;)V

    .line 617
    iget-object v1, p0, Lcom/google/android/gms/car/dl;->a:Lcom/google/android/gms/car/dj;

    invoke-static {v1}, Lcom/google/android/gms/car/dj;->c(Lcom/google/android/gms/car/dj;)Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1

    .line 618
    :try_start_0
    iget-object v2, p0, Lcom/google/android/gms/car/dl;->a:Lcom/google/android/gms/car/dj;

    invoke-static {v2}, Lcom/google/android/gms/car/dj;->a(Lcom/google/android/gms/car/dj;)Lcom/google/android/gms/car/dn;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v2

    if-eqz v2, :cond_2

    .line 620
    :try_start_1
    iget-object v2, p0, Lcom/google/android/gms/car/dl;->a:Lcom/google/android/gms/car/dj;

    invoke-static {v2}, Lcom/google/android/gms/car/dj;->a(Lcom/google/android/gms/car/dj;)Lcom/google/android/gms/car/dn;

    move-result-object v2

    iget-object v2, v2, Lcom/google/android/gms/car/dn;->a:Lcom/google/android/gms/car/le;

    invoke-interface {v2, v0}, Lcom/google/android/gms/car/le;->a(Lcom/google/android/gms/car/CarCall;)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 629
    :cond_1
    :goto_0
    :try_start_2
    monitor-exit v1

    return-void

    .line 621
    :catch_0
    move-exception v0

    .line 622
    const-string v2, "CAR.TEL.CarCallService"

    const-string v3, "RemoteException in CarCallListener."

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 629
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 625
    :cond_2
    :try_start_3
    const-string v0, "CAR.TEL.CarCallService"

    const/4 v2, 0x3

    invoke-static {v0, v2}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 626
    const-string v0, "CAR.TEL.CarCallService"

    const-string v2, "Can\'t notify listener of onCallAdded. It is null."

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0
.end method

.method public final onCallRemoved(Landroid/telecom/Phone;Landroid/telecom/Call;)V
    .locals 4

    .prologue
    .line 634
    iget-object v0, p0, Lcom/google/android/gms/car/dl;->a:Lcom/google/android/gms/car/dj;

    invoke-static {v0}, Lcom/google/android/gms/car/dj;->b(Lcom/google/android/gms/car/dj;)Landroid/telecom/Call$Listener;

    move-result-object v0

    invoke-virtual {p2, v0}, Landroid/telecom/Call;->removeListener(Landroid/telecom/Call$Listener;)V

    .line 635
    invoke-static {p2}, Lcom/google/android/gms/car/dr;->a(Landroid/telecom/Call;)Lcom/google/android/gms/car/CarCall;

    move-result-object v0

    .line 636
    invoke-static {p2}, Lcom/google/android/gms/car/dr;->b(Landroid/telecom/Call;)V

    .line 637
    iget-object v1, p0, Lcom/google/android/gms/car/dl;->a:Lcom/google/android/gms/car/dj;

    invoke-static {v1}, Lcom/google/android/gms/car/dj;->c(Lcom/google/android/gms/car/dj;)Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1

    .line 638
    :try_start_0
    iget-object v2, p0, Lcom/google/android/gms/car/dl;->a:Lcom/google/android/gms/car/dj;

    invoke-static {v2}, Lcom/google/android/gms/car/dj;->a(Lcom/google/android/gms/car/dj;)Lcom/google/android/gms/car/dn;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v2

    if-eqz v2, :cond_0

    .line 640
    :try_start_1
    iget-object v2, p0, Lcom/google/android/gms/car/dl;->a:Lcom/google/android/gms/car/dj;

    invoke-static {v2}, Lcom/google/android/gms/car/dj;->a(Lcom/google/android/gms/car/dj;)Lcom/google/android/gms/car/dn;

    move-result-object v2

    iget-object v2, v2, Lcom/google/android/gms/car/dn;->a:Lcom/google/android/gms/car/le;

    invoke-interface {v2, v0}, Lcom/google/android/gms/car/le;->b(Lcom/google/android/gms/car/CarCall;)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 645
    :cond_0
    :goto_0
    :try_start_2
    monitor-exit v1

    return-void

    .line 641
    :catch_0
    move-exception v0

    .line 642
    const-string v2, "CAR.TEL.CarCallService"

    const-string v3, "RemoteException in CarCallListener."

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 645
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method
