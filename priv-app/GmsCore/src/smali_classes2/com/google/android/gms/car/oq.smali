.class final Lcom/google/android/gms/car/oq;
.super Lcom/google/android/gms/car/pf;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/gms/car/on;

.field private final c:[Ljava/lang/Integer;

.field private volatile d:Z

.field private final e:Ljava/util/concurrent/ConcurrentLinkedQueue;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/car/on;Landroid/os/Looper;)V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1823
    iput-object p1, p0, Lcom/google/android/gms/car/oq;->a:Lcom/google/android/gms/car/on;

    .line 1824
    const/4 v0, 0x0

    invoke-direct {p0, v0, p2}, Lcom/google/android/gms/car/pf;-><init>(Ljava/lang/Object;Landroid/os/Looper;)V

    .line 1812
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/Integer;

    iput-object v0, p0, Lcom/google/android/gms/car/oq;->c:[Ljava/lang/Integer;

    .line 1816
    iput-boolean v2, p0, Lcom/google/android/gms/car/oq;->d:Z

    .line 1819
    new-instance v0, Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/car/oq;->e:Ljava/util/concurrent/ConcurrentLinkedQueue;

    .line 1825
    iget-object v0, p0, Lcom/google/android/gms/car/oq;->c:[Ljava/lang/Integer;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v2

    .line 1827
    iget-object v0, p0, Lcom/google/android/gms/car/oq;->c:[Ljava/lang/Integer;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v3

    .line 1829
    iget-object v0, p0, Lcom/google/android/gms/car/oq;->c:[Ljava/lang/Integer;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v4

    .line 1831
    return-void
.end method

.method private a(ILcom/google/android/gms/car/oi;)V
    .locals 3

    .prologue
    .line 1888
    iget-object v0, p0, Lcom/google/android/gms/car/oq;->e:Ljava/util/concurrent/ConcurrentLinkedQueue;

    new-instance v1, Landroid/util/Pair;

    iget-object v2, p0, Lcom/google/android/gms/car/oq;->c:[Ljava/lang/Integer;

    aget-object v2, v2, p1

    invoke-direct {v1, v2, p2}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-virtual {v0, v1}, Ljava/util/concurrent/ConcurrentLinkedQueue;->add(Ljava/lang/Object;)Z

    .line 1892
    const/4 v0, 0x5

    invoke-virtual {p0, v0}, Lcom/google/android/gms/car/oq;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    .line 1893
    invoke-virtual {p0, v0}, Lcom/google/android/gms/car/oq;->sendMessageAtFrontOfQueue(Landroid/os/Message;)Z

    .line 1894
    return-void
.end method


# virtual methods
.method public final declared-synchronized a()V
    .locals 1

    .prologue
    .line 1834
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/gms/car/oq;->d:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    .line 1839
    :goto_0
    monitor-exit p0

    return-void

    .line 1837
    :cond_0
    const/4 v0, 0x1

    :try_start_1
    invoke-virtual {p0, v0}, Lcom/google/android/gms/car/oq;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    .line 1838
    invoke-virtual {p0, v0}, Lcom/google/android/gms/car/oq;->sendMessage(Landroid/os/Message;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 1834
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(JI)V
    .locals 3

    .prologue
    .line 1842
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/gms/car/oq;->d:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    .line 1847
    :goto_0
    monitor-exit p0

    return-void

    .line 1845
    :cond_0
    const/4 v0, 0x2

    const/4 v1, 0x0

    :try_start_1
    invoke-virtual {p0, v0, p3, v1}, Lcom/google/android/gms/car/oq;->obtainMessage(III)Landroid/os/Message;

    move-result-object v0

    .line 1846
    invoke-virtual {p0, v0, p1, p2}, Lcom/google/android/gms/car/oq;->sendMessageDelayed(Landroid/os/Message;J)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 1842
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Lcom/google/android/gms/car/oi;)V
    .locals 1

    .prologue
    .line 1867
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/gms/car/oq;->d:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    .line 1871
    :goto_0
    monitor-exit p0

    return-void

    .line 1870
    :cond_0
    const/4 v0, 0x0

    :try_start_1
    invoke-direct {p0, v0, p1}, Lcom/google/android/gms/car/oq;->a(ILcom/google/android/gms/car/oi;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 1867
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Lcom/google/android/gms/car/ox;)V
    .locals 1

    .prologue
    .line 1850
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/gms/car/oq;->d:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    .line 1855
    :goto_0
    monitor-exit p0

    return-void

    .line 1853
    :cond_0
    const/4 v0, 0x4

    :try_start_1
    invoke-virtual {p0, v0, p1}, Lcom/google/android/gms/car/oq;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    .line 1854
    invoke-virtual {p0, v0}, Lcom/google/android/gms/car/oq;->sendMessage(Landroid/os/Message;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 1850
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Lcom/google/android/gms/car/pa;)V
    .locals 1

    .prologue
    .line 1905
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/gms/car/oq;->d:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    .line 1910
    :goto_0
    monitor-exit p0

    return-void

    .line 1908
    :cond_0
    const/4 v0, 0x7

    :try_start_1
    invoke-virtual {p0, v0, p1}, Lcom/google/android/gms/car/oq;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    .line 1909
    invoke-virtual {p0, v0}, Lcom/google/android/gms/car/oq;->sendMessage(Landroid/os/Message;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 1905
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b()V
    .locals 1

    .prologue
    .line 1858
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/gms/car/oq;->d:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    .line 1864
    :goto_0
    monitor-exit p0

    return-void

    .line 1861
    :cond_0
    const/4 v0, 0x1

    :try_start_1
    iput-boolean v0, p0, Lcom/google/android/gms/car/oq;->d:Z

    .line 1862
    const/4 v0, 0x3

    invoke-virtual {p0, v0}, Lcom/google/android/gms/car/oq;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    .line 1863
    invoke-virtual {p0, v0}, Lcom/google/android/gms/car/oq;->sendMessage(Landroid/os/Message;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 1858
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b(Lcom/google/android/gms/car/oi;)V
    .locals 1

    .prologue
    .line 1874
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/gms/car/oq;->d:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    .line 1878
    :goto_0
    monitor-exit p0

    return-void

    .line 1877
    :cond_0
    const/4 v0, 0x1

    :try_start_1
    invoke-direct {p0, v0, p1}, Lcom/google/android/gms/car/oq;->a(ILcom/google/android/gms/car/oi;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 1874
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized c(Lcom/google/android/gms/car/oi;)V
    .locals 1

    .prologue
    .line 1881
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/gms/car/oq;->d:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    .line 1885
    :goto_0
    monitor-exit p0

    return-void

    .line 1884
    :cond_0
    const/4 v0, 0x2

    :try_start_1
    invoke-direct {p0, v0, p1}, Lcom/google/android/gms/car/oq;->a(ILcom/google/android/gms/car/oi;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 1881
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized d(Lcom/google/android/gms/car/oi;)V
    .locals 1

    .prologue
    .line 1897
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/gms/car/oq;->d:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    .line 1902
    :goto_0
    monitor-exit p0

    return-void

    .line 1900
    :cond_0
    const/4 v0, 0x6

    :try_start_1
    invoke-virtual {p0, v0, p1}, Lcom/google/android/gms/car/oq;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    .line 1901
    invoke-virtual {p0, v0}, Lcom/google/android/gms/car/oq;->sendMessage(Landroid/os/Message;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 1897
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final handleMessage(Landroid/os/Message;)V
    .locals 4

    .prologue
    .line 1915
    iget-boolean v0, p0, Lcom/google/android/gms/car/oq;->d:Z

    if-eqz v0, :cond_1

    iget v0, p1, Landroid/os/Message;->what:I

    const/4 v1, 0x3

    if-eq v0, v1, :cond_1

    .line 1972
    :cond_0
    :goto_0
    return-void

    .line 1919
    :cond_1
    :try_start_0
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 1961
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "unknown msg "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p1, Landroid/os/Message;->what:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catch Lcom/google/android/gms/car/ow; {:try_start_0 .. :try_end_0} :catch_0

    .line 1971
    :catch_0
    move-exception v0

    .line 1965
    const-string v1, "CAR.WM"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "GlException in CompositionThread "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " command "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p1, Landroid/os/Message;->what:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1966
    iget-object v1, p0, Lcom/google/android/gms/car/oq;->a:Lcom/google/android/gms/car/on;

    invoke-static {v1}, Lcom/google/android/gms/car/on;->i(Lcom/google/android/gms/car/on;)I

    move-result v1

    const/16 v2, 0xa

    if-le v1, v2, :cond_0

    .line 1967
    const-string v1, "CAR.WM"

    const-string v2, "Too many GL error, giving up"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1968
    iget-object v1, p0, Lcom/google/android/gms/car/oq;->a:Lcom/google/android/gms/car/on;

    invoke-static {v1}, Lcom/google/android/gms/car/on;->f(Lcom/google/android/gms/car/on;)Lcom/google/android/gms/car/gx;

    move-result-object v1

    const/16 v2, 0xb

    invoke-virtual {v0}, Lcom/google/android/gms/car/ow;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Lcom/google/android/gms/car/gx;->a(ILjava/lang/String;)V

    goto :goto_0

    .line 1921
    :pswitch_0
    :try_start_1
    iget-object v0, p0, Lcom/google/android/gms/car/oq;->a:Lcom/google/android/gms/car/on;

    invoke-static {v0}, Lcom/google/android/gms/car/on;->g(Lcom/google/android/gms/car/on;)V

    goto :goto_0

    .line 1924
    :pswitch_1
    iget-object v0, p0, Lcom/google/android/gms/car/oq;->a:Lcom/google/android/gms/car/on;

    iget v1, p1, Landroid/os/Message;->arg1:I

    invoke-static {v0, v1}, Lcom/google/android/gms/car/on;->a(Lcom/google/android/gms/car/on;I)V

    goto :goto_0

    .line 1927
    :pswitch_2
    iget-object v0, p0, Lcom/google/android/gms/car/oq;->a:Lcom/google/android/gms/car/on;

    invoke-static {v0}, Lcom/google/android/gms/car/on;->h(Lcom/google/android/gms/car/on;)V

    goto :goto_0

    .line 1930
    :pswitch_3
    iget-object v1, p0, Lcom/google/android/gms/car/oq;->a:Lcom/google/android/gms/car/on;

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/gms/car/ox;

    invoke-static {v1, v0}, Lcom/google/android/gms/car/on;->a(Lcom/google/android/gms/car/on;Lcom/google/android/gms/car/ox;)V

    goto :goto_0

    .line 1933
    :pswitch_4
    iget-object v0, p0, Lcom/google/android/gms/car/oq;->e:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;->poll()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/Pair;

    move-object v1, v0

    .line 1934
    :goto_1
    if-eqz v1, :cond_0

    .line 1935
    iget-object v0, v1, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    packed-switch v0, :pswitch_data_1

    .line 1944
    :goto_2
    iget-object v0, p0, Lcom/google/android/gms/car/oq;->e:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;->poll()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/Pair;

    move-object v1, v0

    goto :goto_1

    .line 1937
    :pswitch_5
    iget-object v2, p0, Lcom/google/android/gms/car/oq;->a:Lcom/google/android/gms/car/on;

    iget-object v0, v1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/gms/car/oi;

    invoke-static {v2, v0}, Lcom/google/android/gms/car/on;->a(Lcom/google/android/gms/car/on;Lcom/google/android/gms/car/oi;)V

    goto :goto_2

    .line 1940
    :pswitch_6
    iget-object v2, p0, Lcom/google/android/gms/car/oq;->a:Lcom/google/android/gms/car/on;

    iget-object v0, v1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/gms/car/oi;

    invoke-static {v2, v0}, Lcom/google/android/gms/car/on;->b(Lcom/google/android/gms/car/on;Lcom/google/android/gms/car/oi;)V

    goto :goto_2

    .line 1943
    :pswitch_7
    iget-object v2, p0, Lcom/google/android/gms/car/oq;->a:Lcom/google/android/gms/car/on;

    iget-object v0, v1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/gms/car/oi;

    invoke-static {v2, v0}, Lcom/google/android/gms/car/on;->c(Lcom/google/android/gms/car/on;Lcom/google/android/gms/car/oi;)V

    goto :goto_2

    .line 1952
    :pswitch_8
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/gms/car/oi;

    .line 1953
    iget-object v1, p0, Lcom/google/android/gms/car/oq;->a:Lcom/google/android/gms/car/on;

    invoke-static {v0}, Lcom/google/android/gms/car/on;->j(Lcom/google/android/gms/car/oi;)V

    goto/16 :goto_0

    .line 1956
    :pswitch_9
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/gms/car/pa;

    .line 1958
    invoke-virtual {v0}, Lcom/google/android/gms/car/pa;->x()V
    :try_end_1
    .catch Lcom/google/android/gms/car/ow; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_0

    .line 1919
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_8
        :pswitch_9
    .end packed-switch

    .line 1935
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method
