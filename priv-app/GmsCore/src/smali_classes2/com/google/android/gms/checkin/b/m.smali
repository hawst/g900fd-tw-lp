.class public final Lcom/google/android/gms/checkin/b/m;
.super Lcom/google/protobuf/a/f;
.source "SourceFile"


# instance fields
.field public a:Z

.field public b:Ljava/lang/String;

.field public c:Z

.field public d:Ljava/lang/String;

.field public e:Z

.field public f:Ljava/lang/String;

.field public g:Ljava/util/List;

.field private h:Z

.field private i:Ljava/lang/String;

.field private j:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1664
    invoke-direct {p0}, Lcom/google/protobuf/a/f;-><init>()V

    .line 1796
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/gms/checkin/b/m;->b:Ljava/lang/String;

    .line 1813
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/gms/checkin/b/m;->d:Ljava/lang/String;

    .line 1830
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/gms/checkin/b/m;->f:Ljava/lang/String;

    .line 1847
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/gms/checkin/b/m;->i:Ljava/lang/String;

    .line 1863
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/checkin/b/m;->g:Ljava/util/List;

    .line 1932
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/checkin/b/m;->j:I

    .line 1664
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 1935
    iget v0, p0, Lcom/google/android/gms/checkin/b/m;->j:I

    if-gez v0, :cond_0

    .line 1937
    invoke-virtual {p0}, Lcom/google/android/gms/checkin/b/m;->b()I

    .line 1939
    :cond_0
    iget v0, p0, Lcom/google/android/gms/checkin/b/m;->j:I

    return v0
.end method

.method public final synthetic a(Lcom/google/protobuf/a/b;)Lcom/google/protobuf/a/f;
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 1661
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/a/b;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/a/b;->b(I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/a/b;->e()Ljava/lang/String;

    move-result-object v0

    iput-boolean v2, p0, Lcom/google/android/gms/checkin/b/m;->a:Z

    iput-object v0, p0, Lcom/google/android/gms/checkin/b/m;->b:Ljava/lang/String;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/a/b;->e()Ljava/lang/String;

    move-result-object v0

    iput-boolean v2, p0, Lcom/google/android/gms/checkin/b/m;->c:Z

    iput-object v0, p0, Lcom/google/android/gms/checkin/b/m;->d:Ljava/lang/String;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/a/b;->e()Ljava/lang/String;

    move-result-object v0

    iput-boolean v2, p0, Lcom/google/android/gms/checkin/b/m;->e:Z

    iput-object v0, p0, Lcom/google/android/gms/checkin/b/m;->f:Ljava/lang/String;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/a/b;->e()Ljava/lang/String;

    move-result-object v0

    iput-boolean v2, p0, Lcom/google/android/gms/checkin/b/m;->h:Z

    iput-object v0, p0, Lcom/google/android/gms/checkin/b/m;->i:Ljava/lang/String;

    goto :goto_0

    :sswitch_5
    new-instance v0, Lcom/google/android/gms/checkin/b/n;

    invoke-direct {v0}, Lcom/google/android/gms/checkin/b/n;-><init>()V

    const/4 v1, 0x5

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/a/b;->a(Lcom/google/protobuf/a/f;I)V

    iget-object v1, p0, Lcom/google/android/gms/checkin/b/m;->g:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/google/android/gms/checkin/b/m;->g:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Lcom/google/android/gms/checkin/b/m;->g:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2b -> :sswitch_5
    .end sparse-switch
.end method

.method public final a(Lcom/google/protobuf/a/c;)V
    .locals 3

    .prologue
    .line 1915
    iget-boolean v0, p0, Lcom/google/android/gms/checkin/b/m;->a:Z

    if-eqz v0, :cond_0

    .line 1916
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/gms/checkin/b/m;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/a/c;->a(ILjava/lang/String;)V

    .line 1918
    :cond_0
    iget-boolean v0, p0, Lcom/google/android/gms/checkin/b/m;->c:Z

    if-eqz v0, :cond_1

    .line 1919
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/android/gms/checkin/b/m;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/a/c;->a(ILjava/lang/String;)V

    .line 1921
    :cond_1
    iget-boolean v0, p0, Lcom/google/android/gms/checkin/b/m;->e:Z

    if-eqz v0, :cond_2

    .line 1922
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/android/gms/checkin/b/m;->f:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/a/c;->a(ILjava/lang/String;)V

    .line 1924
    :cond_2
    iget-boolean v0, p0, Lcom/google/android/gms/checkin/b/m;->h:Z

    if-eqz v0, :cond_3

    .line 1925
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/android/gms/checkin/b/m;->i:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/a/c;->a(ILjava/lang/String;)V

    .line 1927
    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/checkin/b/m;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/checkin/b/n;

    .line 1928
    const/4 v2, 0x5

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/a/c;->a(ILcom/google/protobuf/a/f;)V

    goto :goto_0

    .line 1930
    :cond_4
    return-void
.end method

.method public final b()I
    .locals 4

    .prologue
    .line 1944
    const/4 v0, 0x0

    .line 1945
    iget-boolean v1, p0, Lcom/google/android/gms/checkin/b/m;->a:Z

    if-eqz v1, :cond_0

    .line 1946
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/gms/checkin/b/m;->b:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/google/protobuf/a/c;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 1949
    :cond_0
    iget-boolean v1, p0, Lcom/google/android/gms/checkin/b/m;->c:Z

    if-eqz v1, :cond_1

    .line 1950
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/gms/checkin/b/m;->d:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/a/c;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1953
    :cond_1
    iget-boolean v1, p0, Lcom/google/android/gms/checkin/b/m;->e:Z

    if-eqz v1, :cond_2

    .line 1954
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/android/gms/checkin/b/m;->f:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/a/c;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1957
    :cond_2
    iget-boolean v1, p0, Lcom/google/android/gms/checkin/b/m;->h:Z

    if-eqz v1, :cond_3

    .line 1958
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/android/gms/checkin/b/m;->i:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/a/c;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1961
    :cond_3
    iget-object v1, p0, Lcom/google/android/gms/checkin/b/m;->g:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/checkin/b/n;

    .line 1962
    const/4 v3, 0x5

    invoke-static {v3, v0}, Lcom/google/protobuf/a/c;->c(ILcom/google/protobuf/a/f;)I

    move-result v0

    add-int/2addr v0, v1

    move v1, v0

    .line 1964
    goto :goto_0

    .line 1965
    :cond_4
    iput v1, p0, Lcom/google/android/gms/checkin/b/m;->j:I

    .line 1966
    return v1
.end method
