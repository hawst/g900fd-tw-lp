.class public final Lcom/google/android/gms/icing/a/a;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static final A:Lcom/google/android/gms/common/a/d;

.field public static final B:Lcom/google/android/gms/common/a/d;

.field public static final C:Lcom/google/android/gms/common/a/d;

.field public static final D:Lcom/google/android/gms/common/a/d;

.field public static final E:Lcom/google/android/gms/common/a/d;

.field public static final F:Lcom/google/android/gms/common/a/d;

.field public static final G:Lcom/google/android/gms/common/a/d;

.field public static final H:Lcom/google/android/gms/common/a/d;

.field public static final a:Lcom/google/android/gms/common/a/d;

.field public static final b:Lcom/google/android/gms/common/a/d;

.field public static final c:Lcom/google/android/gms/common/a/d;

.field public static final d:Lcom/google/android/gms/common/a/d;

.field public static final e:Lcom/google/android/gms/common/a/d;

.field public static final f:Lcom/google/android/gms/common/a/d;

.field public static final g:Lcom/google/android/gms/common/a/d;

.field public static final h:Lcom/google/android/gms/common/a/d;

.field public static final i:Lcom/google/android/gms/common/a/d;

.field public static final j:Lcom/google/android/gms/common/a/d;

.field public static final k:Lcom/google/android/gms/common/a/d;

.field public static final l:Lcom/google/android/gms/common/a/d;

.field public static final m:Lcom/google/android/gms/common/a/d;

.field public static final n:Lcom/google/android/gms/common/a/d;

.field public static final o:Lcom/google/android/gms/common/a/d;

.field public static final p:Lcom/google/android/gms/common/a/d;

.field public static final q:Lcom/google/android/gms/common/a/d;

.field public static final r:Lcom/google/android/gms/common/a/d;

.field public static final s:Lcom/google/android/gms/common/a/d;

.field public static final t:Lcom/google/android/gms/common/a/d;

.field public static final u:Lcom/google/android/gms/common/a/d;

.field public static final v:Lcom/google/android/gms/common/a/d;

.field public static final w:Lcom/google/android/gms/common/a/d;

.field public static final x:Lcom/google/android/gms/common/a/d;

.field public static final y:Lcom/google/android/gms/common/a/d;

.field public static final z:Lcom/google/android/gms/common/a/d;


# direct methods
.method static constructor <clinit>()V
    .locals 12

    .prologue
    const-wide/16 v10, 0x1e

    const-wide/16 v8, 0x6

    const/4 v6, 0x1

    const v5, 0x3dcccccd    # 0.1f

    const/4 v4, 0x0

    .line 22
    const-string v0, "corpora_blacklist"

    invoke-static {v0}, Lcom/google/android/gms/icing/a/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "icing.proxy.sms"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/icing/a/a;->a:Lcom/google/android/gms/common/a/d;

    .line 27
    const-string v0, "maintenance_frequency_ms"

    invoke-static {v0}, Lcom/google/android/gms/icing/a/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-wide/32 v2, 0x5265c00

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/Long;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/icing/a/a;->b:Lcom/google/android/gms/common/a/d;

    .line 30
    const-string v0, "maintenance_time_of_day_ms"

    invoke-static {v0}, Lcom/google/android/gms/icing/a/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-wide/32 v2, 0x6ddd00

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/Long;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/icing/a/a;->c:Lcom/google/android/gms/common/a/d;

    .line 33
    const-string v0, "maintenance_time_fuzz_ms"

    invoke-static {v0}, Lcom/google/android/gms/icing/a/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-wide/32 v2, 0x36ee80

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/Long;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/icing/a/a;->d:Lcom/google/android/gms/common/a/d;

    .line 39
    const-string v0, "compact_interval_ms"

    invoke-static {v0}, Lcom/google/android/gms/icing/a/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-wide/32 v2, 0x240c8400

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/Long;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/icing/a/a;->e:Lcom/google/android/gms/common/a/d;

    .line 43
    const-string v0, "maintenance_force_interval_ms"

    invoke-static {v0}, Lcom/google/android/gms/icing/a/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-wide v2, 0x90321000L

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/Long;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/icing/a/a;->f:Lcom/google/android/gms/common/a/d;

    .line 48
    const-string v0, "storage_threshold_bytes"

    invoke-static {v0}, Lcom/google/android/gms/icing/a/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-wide/32 v2, 0x40000000

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/Long;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/icing/a/a;->g:Lcom/google/android/gms/common/a/d;

    .line 51
    const-string v0, "storage_threshold_percent"

    invoke-static {v0}, Lcom/google/android/gms/icing/a/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/Long;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/icing/a/a;->h:Lcom/google/android/gms/common/a/d;

    .line 54
    const-string v0, "storage_compact_threshold"

    invoke-static {v0}, Lcom/google/android/gms/icing/a/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-wide/16 v2, 0xa

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/Long;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/icing/a/a;->i:Lcom/google/android/gms/common/a/d;

    .line 59
    const-string v0, "extension_download_enabled"

    invoke-static {v0}, Lcom/google/android/gms/icing/a/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, v4}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Z)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/icing/a/a;->j:Lcom/google/android/gms/common/a/d;

    .line 66
    const-string v0, "default_stats_sample"

    invoke-static {v0}, Lcom/google/android/gms/icing/a/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v5}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/Float;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/icing/a/a;->k:Lcom/google/android/gms/common/a/d;

    .line 69
    const-string v0, "query_stats_sample"

    invoke-static {v0}, Lcom/google/android/gms/icing/a/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v5}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/Float;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/icing/a/a;->l:Lcom/google/android/gms/common/a/d;

    .line 72
    const-string v0, "timing_stats_sample"

    invoke-static {v0}, Lcom/google/android/gms/icing/a/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v5}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/Float;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/icing/a/a;->m:Lcom/google/android/gms/common/a/d;

    .line 75
    const-string v0, "use_clearcut_logger"

    invoke-static {v0}, Lcom/google/android/gms/icing/a/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, v4}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Z)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/icing/a/a;->n:Lcom/google/android/gms/common/a/d;

    .line 78
    const-string v0, "log_in_worker_thread"

    invoke-static {v0}, Lcom/google/android/gms/icing/a/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, v6}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Z)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/icing/a/a;->o:Lcom/google/android/gms/common/a/d;

    .line 83
    const-string v0, "contextual_ime_enabled"

    invoke-static {v0}, Lcom/google/android/gms/icing/a/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, v4}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Z)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/icing/a/a;->p:Lcom/google/android/gms/common/a/d;

    .line 86
    const-string v0, "context_server_url"

    invoke-static {v0}, Lcom/google/android/gms/icing/a/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, ""

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/icing/a/a;->q:Lcom/google/android/gms/common/a/d;

    .line 89
    const-string v0, "seldon_auth_scope"

    invoke-static {v0}, Lcom/google/android/gms/icing/a/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "oauth2:https://www.googleapis.com/auth/contextengine"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/icing/a/a;->r:Lcom/google/android/gms/common/a/d;

    .line 93
    const-string v0, "footprints_api_auth_scope"

    invoke-static {v0}, Lcom/google/android/gms/icing/a/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "oauth2:https://www.googleapis.com/auth/webhistory"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/icing/a/a;->s:Lcom/google/android/gms/common/a/d;

    .line 97
    const-string v0, "footprints_api_url_base"

    invoke-static {v0}, Lcom/google/android/gms/icing/a/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "https://history.google.com/history/api"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/icing/a/a;->t:Lcom/google/android/gms/common/a/d;

    .line 101
    const-string v0, "developer_key"

    invoke-static {v0}, Lcom/google/android/gms/icing/a/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "745476177629.apps.googleusercontent.com"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/icing/a/a;->u:Lcom/google/android/gms/common/a/d;

    .line 105
    const-string v0, "seldon_num_app_history_events"

    invoke-static {v0}, Lcom/google/android/gms/icing/a/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x5

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/Integer;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/icing/a/a;->v:Lcom/google/android/gms/common/a/d;

    .line 108
    const-string v0, "context_upload_enabled"

    invoke-static {v0}, Lcom/google/android/gms/icing/a/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, v4}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Z)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/icing/a/a;->w:Lcom/google/android/gms/common/a/d;

    .line 111
    const-string v0, "app_history_debug_enabled"

    invoke-static {v0}, Lcom/google/android/gms/icing/a/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, v4}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Z)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/icing/a/a;->x:Lcom/google/android/gms/common/a/d;

    .line 116
    const-string v0, "report_usage_rate_limit_interval_ms"

    invoke-static {v0}, Lcom/google/android/gms/icing/a/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-wide/32 v2, 0x5265c00

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/Long;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/icing/a/a;->y:Lcom/google/android/gms/common/a/d;

    .line 119
    const-string v0, "report_usage_rate_limit_per_corpus_bytes"

    invoke-static {v0}, Lcom/google/android/gms/icing/a/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-wide/32 v2, 0x100000

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/Long;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/icing/a/a;->z:Lcom/google/android/gms/common/a/d;

    .line 123
    const-string v0, "report_usage_num_running_tasks"

    invoke-static {v0}, Lcom/google/android/gms/icing/a/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-wide/16 v2, 0x3

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/Long;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/icing/a/a;->A:Lcom/google/android/gms/common/a/d;

    .line 128
    const-string v0, "update_app_params_start_delay_secs"

    invoke-static {v0}, Lcom/google/android/gms/icing/a/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sget-object v1, Ljava/util/concurrent/TimeUnit;->HOURS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0x1

    invoke-virtual {v1, v2, v3}, Ljava/util/concurrent/TimeUnit;->toSeconds(J)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/Long;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/icing/a/a;->B:Lcom/google/android/gms/common/a/d;

    .line 132
    const-string v0, "update_app_params_end_delay_secs"

    invoke-static {v0}, Lcom/google/android/gms/icing/a/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sget-object v1, Ljava/util/concurrent/TimeUnit;->HOURS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v1, v8, v9}, Ljava/util/concurrent/TimeUnit;->toSeconds(J)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/Long;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/icing/a/a;->C:Lcom/google/android/gms/common/a/d;

    .line 138
    const-string v0, "app_history_upload_enabled"

    invoke-static {v0}, Lcom/google/android/gms/icing/a/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, v6}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Z)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/icing/a/a;->D:Lcom/google/android/gms/common/a/d;

    .line 141
    const-string v0, "app_history_upload_immediate"

    invoke-static {v0}, Lcom/google/android/gms/icing/a/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, v6}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Z)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/icing/a/a;->E:Lcom/google/android/gms/common/a/d;

    .line 144
    const-string v0, "app_history_upload_start_delay_secs"

    invoke-static {v0}, Lcom/google/android/gms/icing/a/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sget-object v1, Ljava/util/concurrent/TimeUnit;->MINUTES:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v1, v10, v11}, Ljava/util/concurrent/TimeUnit;->toSeconds(J)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/Long;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/icing/a/a;->F:Lcom/google/android/gms/common/a/d;

    .line 148
    const-string v0, "app_history_upload_end_delay_secs"

    invoke-static {v0}, Lcom/google/android/gms/icing/a/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sget-object v1, Ljava/util/concurrent/TimeUnit;->HOURS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v1, v8, v9}, Ljava/util/concurrent/TimeUnit;->toSeconds(J)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/Long;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/icing/a/a;->G:Lcom/google/android/gms/common/a/d;

    .line 153
    const-string v0, "help_settings_url"

    invoke-static {v0}, Lcom/google/android/gms/icing/a/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "https://support.google.com/websearch?p=ws_searchdata"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/icing/a/a;->H:Lcom/google/android/gms/common/a/d;

    return-void
.end method

.method private static a(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 163
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "gms_icing_"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static a(Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 159
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, ""

    invoke-static {v2}, Lcom/google/android/gms/icing/a/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {p0, v0}, Lcom/google/android/gms/common/c/a;->a(Landroid/content/Context;[Ljava/lang/String;)V

    .line 160
    return-void
.end method
