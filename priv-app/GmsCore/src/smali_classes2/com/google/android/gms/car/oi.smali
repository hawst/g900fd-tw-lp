.class public Lcom/google/android/gms/car/oi;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/graphics/SurfaceTexture$OnFrameAvailableListener;
.implements Lcom/google/android/gms/car/ol;


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x13
.end annotation


# instance fields
.field protected final a:Lcom/google/android/gms/car/on;

.field protected volatile b:Lcom/google/android/gms/car/ok;

.field protected volatile c:Z

.field protected d:I

.field protected e:Ljava/nio/FloatBuffer;

.field protected final f:[F

.field protected final g:I

.field protected volatile h:F

.field protected volatile i:Z

.field protected final j:[F

.field private volatile k:Landroid/graphics/SurfaceTexture;

.field private volatile l:Landroid/view/Surface;

.field private m:J

.field private final n:Ljava/util/concurrent/atomic/AtomicInteger;

.field private final o:Ljava/util/concurrent/atomic/AtomicInteger;

.field private final p:Lcom/google/android/gms/car/om;

.field private final q:Lcom/google/android/gms/car/oj;

.field private final r:Ljava/lang/Object;

.field private s:I

.field private volatile t:Lcom/google/android/gms/car/qp;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/car/on;ILcom/google/android/gms/car/om;)V
    .locals 4

    .prologue
    const/16 v3, 0x10

    const/4 v2, 0x0

    .line 80
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 48
    iput-boolean v2, p0, Lcom/google/android/gms/car/oi;->c:Z

    .line 49
    iput v2, p0, Lcom/google/android/gms/car/oi;->d:I

    .line 53
    new-array v0, v3, [F

    iput-object v0, p0, Lcom/google/android/gms/car/oi;->f:[F

    .line 54
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/google/android/gms/car/oi;->m:J

    .line 55
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-direct {v0, v2}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/gms/car/oi;->n:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 56
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-direct {v0, v2}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/gms/car/oi;->o:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 62
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/car/oi;->r:Ljava/lang/Object;

    .line 74
    new-array v0, v3, [F

    iput-object v0, p0, Lcom/google/android/gms/car/oi;->j:[F

    .line 81
    iput-object p1, p0, Lcom/google/android/gms/car/oi;->a:Lcom/google/android/gms/car/on;

    .line 82
    iput p2, p0, Lcom/google/android/gms/car/oi;->g:I

    .line 83
    iput-object p3, p0, Lcom/google/android/gms/car/oi;->p:Lcom/google/android/gms/car/om;

    .line 84
    new-instance v0, Lcom/google/android/gms/car/oj;

    invoke-direct {v0, p0, v2}, Lcom/google/android/gms/car/oj;-><init>(Lcom/google/android/gms/car/oi;B)V

    iput-object v0, p0, Lcom/google/android/gms/car/oi;->q:Lcom/google/android/gms/car/oj;

    .line 85
    iput v2, p0, Lcom/google/android/gms/car/oi;->s:I

    .line 86
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/google/android/gms/car/oi;->h:F

    .line 87
    return-void
.end method

.method private declared-synchronized x()V
    .locals 6

    .prologue
    .line 206
    monitor-enter p0

    :try_start_0
    iget-object v5, p0, Lcom/google/android/gms/car/oi;->b:Lcom/google/android/gms/car/ok;

    .line 207
    new-instance v0, Lcom/google/android/gms/car/ok;

    iget v1, v5, Lcom/google/android/gms/car/ok;->a:I

    iget v2, v5, Lcom/google/android/gms/car/ok;->b:I

    iget v3, v5, Lcom/google/android/gms/car/ok;->c:I

    iget v4, v5, Lcom/google/android/gms/car/ok;->d:I

    iget v5, v5, Lcom/google/android/gms/car/ok;->g:I

    add-int/lit8 v5, v5, -0x1

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/car/ok;-><init>(IIIII)V

    iput-object v0, p0, Lcom/google/android/gms/car/oi;->b:Lcom/google/android/gms/car/ok;

    .line 209
    invoke-virtual {p0}, Lcom/google/android/gms/car/oi;->i()V

    .line 210
    iget-object v0, p0, Lcom/google/android/gms/car/oi;->a:Lcom/google/android/gms/car/on;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/car/on;->i(Lcom/google/android/gms/car/oi;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 211
    monitor-exit p0

    return-void

    .line 206
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private y()V
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 325
    iget-object v0, p0, Lcom/google/android/gms/car/oi;->k:Landroid/graphics/SurfaceTexture;

    if-eqz v0, :cond_0

    .line 326
    invoke-virtual {p0}, Lcom/google/android/gms/car/oi;->w()Z

    .line 328
    :cond_0
    iput-boolean v2, p0, Lcom/google/android/gms/car/oi;->c:Z

    .line 329
    iput-boolean v2, p0, Lcom/google/android/gms/car/oi;->i:Z

    .line 330
    iget-object v0, p0, Lcom/google/android/gms/car/oi;->n:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0, v2}, Ljava/util/concurrent/atomic/AtomicInteger;->set(I)V

    .line 331
    iget-object v0, p0, Lcom/google/android/gms/car/oi;->o:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0, v2}, Ljava/util/concurrent/atomic/AtomicInteger;->set(I)V

    .line 332
    iget-object v0, p0, Lcom/google/android/gms/car/oi;->l:Landroid/view/Surface;

    if-eqz v0, :cond_1

    .line 333
    iget-object v0, p0, Lcom/google/android/gms/car/oi;->l:Landroid/view/Surface;

    invoke-virtual {v0}, Landroid/view/Surface;->release()V

    .line 334
    iput-object v1, p0, Lcom/google/android/gms/car/oi;->l:Landroid/view/Surface;

    .line 336
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/car/oi;->k:Landroid/graphics/SurfaceTexture;

    if-eqz v0, :cond_2

    .line 337
    iget-object v0, p0, Lcom/google/android/gms/car/oi;->k:Landroid/graphics/SurfaceTexture;

    invoke-virtual {v0}, Landroid/graphics/SurfaceTexture;->release()V

    .line 338
    iput-object v1, p0, Lcom/google/android/gms/car/oi;->k:Landroid/graphics/SurfaceTexture;

    .line 340
    :cond_2
    iget v0, p0, Lcom/google/android/gms/car/oi;->d:I

    if-eqz v0, :cond_3

    .line 341
    new-array v0, v3, [I

    iget v1, p0, Lcom/google/android/gms/car/oi;->d:I

    aput v1, v0, v2

    .line 344
    invoke-static {v3, v0, v2}, Landroid/opengl/GLES20;->glDeleteTextures(I[II)V

    .line 345
    invoke-static {}, Landroid/opengl/GLES20;->glFinish()V

    .line 346
    iput v2, p0, Lcom/google/android/gms/car/oi;->d:I

    .line 348
    :cond_3
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 91
    iget v0, p0, Lcom/google/android/gms/car/oi;->g:I

    return v0
.end method

.method protected a(F[F)V
    .locals 8

    .prologue
    const/16 v4, 0x14

    const/4 v1, 0x3

    const/4 v7, 0x1

    const/high16 v2, 0x3f800000    # 1.0f

    const/4 v3, 0x0

    .line 406
    iget-object v0, p0, Lcom/google/android/gms/car/oi;->a:Lcom/google/android/gms/car/on;

    invoke-virtual {v0, v3}, Lcom/google/android/gms/car/on;->c(I)V

    .line 407
    iget-object v0, p0, Lcom/google/android/gms/car/oi;->a:Lcom/google/android/gms/car/on;

    invoke-virtual {v0, v3}, Lcom/google/android/gms/car/on;->b(I)Lcom/google/android/gms/car/ot;

    move-result-object v6

    .line 409
    cmpl-float v0, p1, v2

    if-nez v0, :cond_0

    .line 410
    const/16 v0, 0xbe2

    invoke-static {v0}, Landroid/opengl/GLES20;->glDisable(I)V

    .line 416
    :goto_0
    iget v0, v6, Lcom/google/android/gms/car/ot;->b:I

    invoke-static {v0, v7, v3, p2, v3}, Landroid/opengl/GLES20;->glUniformMatrix4fv(IIZ[FI)V

    .line 417
    const v0, 0x84c0

    invoke-static {v0}, Landroid/opengl/GLES20;->glActiveTexture(I)V

    .line 418
    const v0, 0x8d65

    iget v2, p0, Lcom/google/android/gms/car/oi;->d:I

    invoke-static {v0, v2}, Landroid/opengl/GLES20;->glBindTexture(II)V

    .line 419
    iget-object v0, p0, Lcom/google/android/gms/car/oi;->e:Ljava/nio/FloatBuffer;

    invoke-virtual {v0, v3}, Ljava/nio/FloatBuffer;->position(I)Ljava/nio/Buffer;

    .line 420
    iget v0, v6, Lcom/google/android/gms/car/ot;->d:I

    const/16 v2, 0x1406

    iget-object v5, p0, Lcom/google/android/gms/car/oi;->e:Ljava/nio/FloatBuffer;

    invoke-static/range {v0 .. v5}, Landroid/opengl/GLES20;->glVertexAttribPointer(IIIZILjava/nio/Buffer;)V

    .line 422
    iget v0, v6, Lcom/google/android/gms/car/ot;->d:I

    invoke-static {v0}, Landroid/opengl/GLES20;->glEnableVertexAttribArray(I)V

    .line 424
    iget-object v0, p0, Lcom/google/android/gms/car/oi;->e:Ljava/nio/FloatBuffer;

    invoke-virtual {v0, v1}, Ljava/nio/FloatBuffer;->position(I)Ljava/nio/Buffer;

    .line 425
    iget v0, v6, Lcom/google/android/gms/car/ot;->e:I

    const/4 v1, 0x2

    const/16 v2, 0x1406

    iget-object v5, p0, Lcom/google/android/gms/car/oi;->e:Ljava/nio/FloatBuffer;

    invoke-static/range {v0 .. v5}, Landroid/opengl/GLES20;->glVertexAttribPointer(IIIZILjava/nio/Buffer;)V

    .line 427
    iget v0, v6, Lcom/google/android/gms/car/ot;->e:I

    invoke-static {v0}, Landroid/opengl/GLES20;->glEnableVertexAttribArray(I)V

    .line 428
    iget v0, v6, Lcom/google/android/gms/car/ot;->c:I

    iget-object v1, p0, Lcom/google/android/gms/car/oi;->f:[F

    invoke-static {v0, v7, v3, v1, v3}, Landroid/opengl/GLES20;->glUniformMatrix4fv(IIZ[FI)V

    .line 429
    const/4 v0, 0x5

    const/4 v1, 0x4

    invoke-static {v0, v3, v1}, Landroid/opengl/GLES20;->glDrawArrays(III)V

    .line 430
    return-void

    .line 412
    :cond_0
    const/16 v0, 0xbe2

    invoke-static {v0}, Landroid/opengl/GLES20;->glEnable(I)V

    .line 413
    invoke-static {v2, v2, v2, p1}, Landroid/opengl/GLES20;->glBlendColor(FFFF)V

    .line 414
    const v0, 0x8003

    const v2, 0x8004

    invoke-static {v0, v2}, Landroid/opengl/GLES20;->glBlendFunc(II)V

    goto :goto_0
.end method

.method public final declared-synchronized a(IIII)V
    .locals 6

    .prologue
    .line 179
    monitor-enter p0

    :try_start_0
    new-instance v0, Lcom/google/android/gms/car/ok;

    const/4 v1, 0x0

    move v2, p1

    move v3, p2

    move v4, p3

    move v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/car/ok;-><init>(IIIII)V

    iput-object v0, p0, Lcom/google/android/gms/car/oi;->b:Lcom/google/android/gms/car/ok;

    .line 180
    invoke-virtual {p0}, Lcom/google/android/gms/car/oi;->i()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 181
    monitor-exit p0

    return-void

    .line 179
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public a(Landroid/graphics/Bitmap;)V
    .locals 0

    .prologue
    .line 165
    return-void
.end method

.method public final a(Landroid/view/KeyEvent;)V
    .locals 1

    .prologue
    .line 489
    iget-object v0, p0, Lcom/google/android/gms/car/oi;->a:Lcom/google/android/gms/car/on;

    invoke-virtual {v0, p0, p1}, Lcom/google/android/gms/car/on;->a(Lcom/google/android/gms/car/oi;Landroid/view/KeyEvent;)V

    .line 490
    return-void
.end method

.method public final a(Landroid/view/MotionEvent;)V
    .locals 1

    .prologue
    .line 494
    iget-object v0, p0, Lcom/google/android/gms/car/oi;->q:Lcom/google/android/gms/car/oj;

    invoke-static {v0, p1}, Lcom/google/android/gms/car/oj;->a(Lcom/google/android/gms/car/oj;Landroid/view/MotionEvent;)V

    .line 495
    return-void
.end method

.method public final declared-synchronized a(Lcom/google/android/gms/car/qp;Z)V
    .locals 1

    .prologue
    .line 311
    monitor-enter p0

    if-eqz p2, :cond_0

    .line 313
    :try_start_0
    invoke-direct {p0}, Lcom/google/android/gms/car/oi;->x()V

    .line 315
    :cond_0
    iput-object p1, p0, Lcom/google/android/gms/car/oi;->t:Lcom/google/android/gms/car/qp;

    .line 316
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/car/oi;->i:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 317
    monitor-exit p0

    return-void

    .line 311
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method final a(Ljava/io/PrintWriter;)V
    .locals 1

    .prologue
    .line 541
    invoke-virtual {p0}, Lcom/google/android/gms/car/oi;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 542
    return-void
.end method

.method public final a(Z)V
    .locals 1

    .prologue
    .line 509
    iget-object v0, p0, Lcom/google/android/gms/car/oi;->q:Lcom/google/android/gms/car/oj;

    invoke-static {v0, p1}, Lcom/google/android/gms/car/oj;->a(Lcom/google/android/gms/car/oj;Z)V

    .line 510
    return-void
.end method

.method public final a(II)Z
    .locals 3

    .prologue
    .line 214
    iget-object v0, p0, Lcom/google/android/gms/car/oi;->a:Lcom/google/android/gms/car/on;

    invoke-virtual {v0}, Lcom/google/android/gms/car/on;->e()I

    move-result v0

    sub-int/2addr v0, p2

    .line 215
    iget-object v1, p0, Lcom/google/android/gms/car/oi;->b:Lcom/google/android/gms/car/ok;

    iget v1, v1, Lcom/google/android/gms/car/ok;->a:I

    if-lt p1, v1, :cond_0

    iget-object v1, p0, Lcom/google/android/gms/car/oi;->b:Lcom/google/android/gms/car/ok;

    iget v1, v1, Lcom/google/android/gms/car/ok;->b:I

    if-lt v0, v1, :cond_0

    iget-object v1, p0, Lcom/google/android/gms/car/oi;->b:Lcom/google/android/gms/car/ok;

    iget v1, v1, Lcom/google/android/gms/car/ok;->a:I

    iget-object v2, p0, Lcom/google/android/gms/car/oi;->b:Lcom/google/android/gms/car/ok;

    iget v2, v2, Lcom/google/android/gms/car/ok;->c:I

    add-int/2addr v1, v2

    if-ge p1, v1, :cond_0

    iget-object v1, p0, Lcom/google/android/gms/car/oi;->b:Lcom/google/android/gms/car/ok;

    iget v1, v1, Lcom/google/android/gms/car/ok;->b:I

    iget-object v2, p0, Lcom/google/android/gms/car/oi;->b:Lcom/google/android/gms/car/ok;

    iget v2, v2, Lcom/google/android/gms/car/ok;->d:I

    add-int/2addr v1, v2

    if-ge v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(JJILjava/util/ArrayList;)Z
    .locals 11

    .prologue
    .line 472
    iget-boolean v0, p0, Lcom/google/android/gms/car/oi;->c:Z

    if-nez v0, :cond_0

    .line 473
    const/4 v0, 0x0

    .line 484
    :goto_0
    return v0

    .line 475
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/car/oi;->a:Lcom/google/android/gms/car/on;

    invoke-virtual {v0}, Lcom/google/android/gms/car/on;->e()I

    move-result v0

    neg-int v0, v0

    iget-object v1, p0, Lcom/google/android/gms/car/oi;->b:Lcom/google/android/gms/car/ok;

    iget v1, v1, Lcom/google/android/gms/car/ok;->f:I

    add-int v7, v0, v1

    .line 476
    iget-object v0, p0, Lcom/google/android/gms/car/oi;->b:Lcom/google/android/gms/car/ok;

    iget v0, v0, Lcom/google/android/gms/car/ok;->a:I

    neg-int v6, v0

    const/16 v8, 0x1002

    const/4 v9, 0x1

    move-wide v0, p1

    move-wide v2, p3

    move/from16 v4, p5

    move-object/from16 v5, p6

    invoke-static/range {v0 .. v9}, Lcom/google/android/gms/car/od;->a(JJILjava/util/ArrayList;IIII)Landroid/view/MotionEvent;

    move-result-object v0

    .line 479
    const-string v1, "CAR.WM.WIN"

    const/4 v2, 0x2

    invoke-static {v1, v2}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 480
    const-string v1, "CAR.WM.WIN"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Injecting touch event, layer = "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v3, p0, Lcom/google/android/gms/car/oi;->g:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " window = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " MotionEvent = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 483
    :cond_1
    iget-object v1, p0, Lcom/google/android/gms/car/oi;->q:Lcom/google/android/gms/car/oj;

    invoke-static {v1, v0}, Lcom/google/android/gms/car/oj;->a(Lcom/google/android/gms/car/oj;Landroid/view/MotionEvent;)V

    .line 484
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final declared-synchronized a([F)Z
    .locals 9

    .prologue
    const/4 v6, 0x1

    const/4 v0, 0x0

    .line 365
    monitor-enter p0

    :try_start_0
    iget-boolean v1, p0, Lcom/google/android/gms/car/oi;->c:Z

    if-nez v1, :cond_1

    iget-boolean v1, p0, Lcom/google/android/gms/car/oi;->i:Z

    if-nez v1, :cond_1

    .line 366
    const-string v1, "CAR.WM.WIN"

    const/4 v2, 0x2

    invoke-static {v1, v2}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 367
    const-string v1, "CAR.WM.WIN"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "draw skipping as not visible "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 397
    :cond_0
    :goto_0
    monitor-exit p0

    return v0

    .line 371
    :cond_1
    :try_start_1
    iget v7, p0, Lcom/google/android/gms/car/oi;->h:F

    .line 373
    iget-boolean v1, p0, Lcom/google/android/gms/car/oi;->i:Z

    if-nez v1, :cond_4

    .line 374
    iget v1, p0, Lcom/google/android/gms/car/oi;->h:F

    const/4 v2, 0x0

    cmpl-float v1, v1, v2

    if-eqz v1, :cond_0

    :cond_2
    move v0, v7

    .line 388
    :goto_1
    const-string v1, "CAR.WM.WIN"

    const/4 v2, 0x2

    invoke-static {v1, v2}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 389
    const-string v1, "CAR.WM.WIN"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "draw "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " alpha "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 395
    :cond_3
    iget-object v1, p0, Lcom/google/android/gms/car/oi;->o:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicInteger;->incrementAndGet()I

    .line 396
    invoke-virtual {p0, v0, p1}, Lcom/google/android/gms/car/oi;->a(F[F)V

    move v0, v6

    .line 397
    goto :goto_0

    .line 378
    :cond_4
    iget-object v1, p0, Lcom/google/android/gms/car/oi;->n:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v1

    if-lez v1, :cond_5

    move v0, v6

    .line 380
    :cond_5
    iget-object v1, p0, Lcom/google/android/gms/car/oi;->t:Lcom/google/android/gms/car/qp;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/car/qp;->a(Z)Lcom/google/android/gms/car/qq;

    move-result-object v8

    .line 381
    iget-object v0, p0, Lcom/google/android/gms/car/oi;->j:[F

    const/4 v1, 0x0

    const/4 v3, 0x0

    iget-object v4, v8, Lcom/google/android/gms/car/qq;->a:[F

    const/4 v5, 0x0

    move-object v2, p1

    invoke-static/range {v0 .. v5}, Landroid/opengl/Matrix;->multiplyMM([FI[FI[FI)V

    .line 383
    iget-object p1, p0, Lcom/google/android/gms/car/oi;->j:[F

    .line 384
    iget v0, v8, Lcom/google/android/gms/car/qq;->b:F

    const/high16 v1, 0x3f800000    # 1.0f

    cmpl-float v0, v0, v1

    if-eqz v0, :cond_2

    .line 385
    iget v0, v8, Lcom/google/android/gms/car/qq;->b:F
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 365
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 96
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/car/oi;->c:Z

    .line 97
    return-void
.end method

.method final b(II)V
    .locals 1

    .prologue
    .line 513
    iget-object v0, p0, Lcom/google/android/gms/car/oi;->p:Lcom/google/android/gms/car/om;

    invoke-interface {v0, p0, p1, p2}, Lcom/google/android/gms/car/om;->a(Lcom/google/android/gms/car/ol;II)V

    .line 514
    return-void
.end method

.method public final b(Landroid/view/KeyEvent;)V
    .locals 1

    .prologue
    .line 504
    iget-object v0, p0, Lcom/google/android/gms/car/oi;->q:Lcom/google/android/gms/car/oj;

    invoke-static {v0, p1}, Lcom/google/android/gms/car/oj;->a(Lcom/google/android/gms/car/oj;Landroid/view/KeyEvent;)V

    .line 505
    return-void
.end method

.method public final b(Landroid/view/MotionEvent;)V
    .locals 1

    .prologue
    .line 499
    iget-object v0, p0, Lcom/google/android/gms/car/oi;->q:Lcom/google/android/gms/car/oj;

    invoke-static {v0, p1}, Lcom/google/android/gms/car/oj;->a(Lcom/google/android/gms/car/oj;Landroid/view/MotionEvent;)V

    .line 500
    return-void
.end method

.method final b(Z)V
    .locals 1

    .prologue
    .line 529
    iget-object v0, p0, Lcom/google/android/gms/car/oi;->p:Lcom/google/android/gms/car/om;

    invoke-interface {v0, p1}, Lcom/google/android/gms/car/om;->a(Z)V

    .line 530
    return-void
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 101
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/car/oi;->c:Z

    .line 102
    return-void
.end method

.method final c(Landroid/view/KeyEvent;)V
    .locals 1

    .prologue
    .line 537
    iget-object v0, p0, Lcom/google/android/gms/car/oi;->p:Lcom/google/android/gms/car/om;

    invoke-interface {v0, p1}, Lcom/google/android/gms/car/om;->a(Landroid/view/KeyEvent;)V

    .line 538
    return-void
.end method

.method final c(Landroid/view/MotionEvent;)V
    .locals 1

    .prologue
    .line 533
    iget-object v0, p0, Lcom/google/android/gms/car/oi;->p:Lcom/google/android/gms/car/om;

    invoke-interface {v0, p1}, Lcom/google/android/gms/car/om;->a(Landroid/view/MotionEvent;)V

    .line 534
    return-void
.end method

.method public final d()Z
    .locals 1

    .prologue
    .line 106
    iget-boolean v0, p0, Lcom/google/android/gms/car/oi;->c:Z

    return v0
.end method

.method public final e()Landroid/view/Surface;
    .locals 1

    .prologue
    .line 111
    iget-object v0, p0, Lcom/google/android/gms/car/oi;->l:Landroid/view/Surface;

    return-object v0
.end method

.method public final f()I
    .locals 2

    .prologue
    .line 116
    iget-object v1, p0, Lcom/google/android/gms/car/oi;->r:Ljava/lang/Object;

    monitor-enter v1

    .line 117
    :try_start_0
    iget v0, p0, Lcom/google/android/gms/car/oi;->s:I

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return v0

    .line 118
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final g()I
    .locals 1

    .prologue
    .line 138
    iget-object v0, p0, Lcom/google/android/gms/car/oi;->b:Lcom/google/android/gms/car/ok;

    iget v0, v0, Lcom/google/android/gms/car/ok;->g:I

    return v0
.end method

.method public final h()V
    .locals 1

    .prologue
    .line 147
    const v0, 0x3e99999a    # 0.3f

    iput v0, p0, Lcom/google/android/gms/car/oi;->h:F

    .line 148
    return-void
.end method

.method protected i()V
    .locals 7

    .prologue
    const/4 v6, 0x3

    const/4 v5, 0x0

    const/high16 v4, 0x3f800000    # 1.0f

    const/4 v3, 0x0

    .line 184
    const/16 v0, 0x14

    new-array v0, v0, [F

    iget-object v1, p0, Lcom/google/android/gms/car/oi;->b:Lcom/google/android/gms/car/ok;

    iget v1, v1, Lcom/google/android/gms/car/ok;->a:I

    int-to-float v1, v1

    aput v1, v0, v5

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/gms/car/oi;->b:Lcom/google/android/gms/car/ok;

    iget v2, v2, Lcom/google/android/gms/car/ok;->b:I

    int-to-float v2, v2

    aput v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/gms/car/oi;->b:Lcom/google/android/gms/car/ok;

    iget v2, v2, Lcom/google/android/gms/car/ok;->g:I

    int-to-float v2, v2

    aput v2, v0, v1

    aput v3, v0, v6

    const/4 v1, 0x4

    aput v3, v0, v1

    const/4 v1, 0x5

    iget-object v2, p0, Lcom/google/android/gms/car/oi;->b:Lcom/google/android/gms/car/ok;

    iget v2, v2, Lcom/google/android/gms/car/ok;->e:I

    int-to-float v2, v2

    aput v2, v0, v1

    const/4 v1, 0x6

    iget-object v2, p0, Lcom/google/android/gms/car/oi;->b:Lcom/google/android/gms/car/ok;

    iget v2, v2, Lcom/google/android/gms/car/ok;->b:I

    int-to-float v2, v2

    aput v2, v0, v1

    const/4 v1, 0x7

    iget-object v2, p0, Lcom/google/android/gms/car/oi;->b:Lcom/google/android/gms/car/ok;

    iget v2, v2, Lcom/google/android/gms/car/ok;->g:I

    int-to-float v2, v2

    aput v2, v0, v1

    const/16 v1, 0x8

    aput v4, v0, v1

    const/16 v1, 0x9

    aput v3, v0, v1

    const/16 v1, 0xa

    iget-object v2, p0, Lcom/google/android/gms/car/oi;->b:Lcom/google/android/gms/car/ok;

    iget v2, v2, Lcom/google/android/gms/car/ok;->a:I

    int-to-float v2, v2

    aput v2, v0, v1

    const/16 v1, 0xb

    iget-object v2, p0, Lcom/google/android/gms/car/oi;->b:Lcom/google/android/gms/car/ok;

    iget v2, v2, Lcom/google/android/gms/car/ok;->f:I

    int-to-float v2, v2

    aput v2, v0, v1

    const/16 v1, 0xc

    iget-object v2, p0, Lcom/google/android/gms/car/oi;->b:Lcom/google/android/gms/car/ok;

    iget v2, v2, Lcom/google/android/gms/car/ok;->g:I

    int-to-float v2, v2

    aput v2, v0, v1

    const/16 v1, 0xd

    aput v3, v0, v1

    const/16 v1, 0xe

    aput v4, v0, v1

    const/16 v1, 0xf

    iget-object v2, p0, Lcom/google/android/gms/car/oi;->b:Lcom/google/android/gms/car/ok;

    iget v2, v2, Lcom/google/android/gms/car/ok;->e:I

    int-to-float v2, v2

    aput v2, v0, v1

    const/16 v1, 0x10

    iget-object v2, p0, Lcom/google/android/gms/car/oi;->b:Lcom/google/android/gms/car/ok;

    iget v2, v2, Lcom/google/android/gms/car/ok;->f:I

    int-to-float v2, v2

    aput v2, v0, v1

    const/16 v1, 0x11

    iget-object v2, p0, Lcom/google/android/gms/car/oi;->b:Lcom/google/android/gms/car/ok;

    iget v2, v2, Lcom/google/android/gms/car/ok;->g:I

    int-to-float v2, v2

    aput v2, v0, v1

    const/16 v1, 0x12

    aput v4, v0, v1

    const/16 v1, 0x13

    aput v4, v0, v1

    .line 191
    array-length v1, v0

    mul-int/lit8 v1, v1, 0x4

    invoke-static {v1}, Ljava/nio/ByteBuffer;->allocateDirect(I)Ljava/nio/ByteBuffer;

    move-result-object v1

    invoke-static {}, Ljava/nio/ByteOrder;->nativeOrder()Ljava/nio/ByteOrder;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->asFloatBuffer()Ljava/nio/FloatBuffer;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/gms/car/oi;->e:Ljava/nio/FloatBuffer;

    .line 194
    iget-object v1, p0, Lcom/google/android/gms/car/oi;->e:Ljava/nio/FloatBuffer;

    invoke-virtual {v1, v0}, Ljava/nio/FloatBuffer;->put([F)Ljava/nio/FloatBuffer;

    move-result-object v0

    invoke-virtual {v0, v5}, Ljava/nio/FloatBuffer;->position(I)Ljava/nio/Buffer;

    .line 195
    const-string v0, "CAR.WM.WIN"

    invoke-static {v0, v6}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 196
    const-string v0, "CAR.WM.WIN"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "setNewVerticesData "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 198
    :cond_0
    return-void
.end method

.method public final j()Lcom/google/android/gms/car/ok;
    .locals 1

    .prologue
    .line 201
    iget-object v0, p0, Lcom/google/android/gms/car/oi;->b:Lcom/google/android/gms/car/ok;

    return-object v0
.end method

.method public final declared-synchronized k()V
    .locals 4

    .prologue
    const/4 v3, 0x4

    .line 228
    monitor-enter p0

    :try_start_0
    const-string v0, "CAR.WM.WIN"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 229
    const-string v0, "CAR.WM.WIN"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "init "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 231
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/car/oi;->f()I

    move-result v0

    .line 232
    if-eqz v0, :cond_2

    if-eq v0, v3, :cond_2

    .line 234
    const-string v0, "CAR.WM.WIN"

    const/4 v1, 0x4

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 235
    const-string v0, "CAR.WM.WIN"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "window init called while not cleaned up "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 237
    :cond_1
    invoke-direct {p0}, Lcom/google/android/gms/car/oi;->y()V

    .line 239
    :cond_2
    const/4 v0, 0x1

    new-array v0, v0, [I

    .line 240
    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-static {v1, v0, v2}, Landroid/opengl/GLES20;->glGenTextures(I[II)V

    .line 241
    const/4 v1, 0x0

    aget v0, v0, v1

    iput v0, p0, Lcom/google/android/gms/car/oi;->d:I

    .line 242
    const v0, 0x8d65

    iget v1, p0, Lcom/google/android/gms/car/oi;->d:I

    invoke-static {v0, v1}, Landroid/opengl/GLES20;->glBindTexture(II)V

    .line 243
    const-string v0, "glBindTexture mTextureID"

    invoke-static {v0}, Lcom/google/android/gms/car/ov;->a(Ljava/lang/String;)V

    .line 245
    const v0, 0x8d65

    const/16 v1, 0x2801

    const/high16 v2, 0x46180000    # 9728.0f

    invoke-static {v0, v1, v2}, Landroid/opengl/GLES20;->glTexParameterf(IIF)V

    .line 247
    const v0, 0x8d65

    const/16 v1, 0x2800

    const/high16 v2, 0x46180000    # 9728.0f

    invoke-static {v0, v1, v2}, Landroid/opengl/GLES20;->glTexParameterf(IIF)V

    .line 249
    const v0, 0x8d65

    const/16 v1, 0x2802

    const v2, 0x812f

    invoke-static {v0, v1, v2}, Landroid/opengl/GLES20;->glTexParameteri(III)V

    .line 251
    const v0, 0x8d65

    const/16 v1, 0x2803

    const v2, 0x812f

    invoke-static {v0, v1, v2}, Landroid/opengl/GLES20;->glTexParameteri(III)V

    .line 253
    const-string v0, "glTexParameter"

    invoke-static {v0}, Lcom/google/android/gms/car/ov;->a(Ljava/lang/String;)V

    .line 254
    new-instance v0, Landroid/graphics/SurfaceTexture;

    iget v1, p0, Lcom/google/android/gms/car/oi;->d:I

    invoke-direct {v0, v1}, Landroid/graphics/SurfaceTexture;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/gms/car/oi;->k:Landroid/graphics/SurfaceTexture;

    .line 255
    iget-object v0, p0, Lcom/google/android/gms/car/oi;->k:Landroid/graphics/SurfaceTexture;

    iget-object v1, p0, Lcom/google/android/gms/car/oi;->b:Lcom/google/android/gms/car/ok;

    iget v1, v1, Lcom/google/android/gms/car/ok;->c:I

    iget-object v2, p0, Lcom/google/android/gms/car/oi;->b:Lcom/google/android/gms/car/ok;

    iget v2, v2, Lcom/google/android/gms/car/ok;->d:I

    invoke-virtual {v0, v1, v2}, Landroid/graphics/SurfaceTexture;->setDefaultBufferSize(II)V

    .line 256
    new-instance v0, Landroid/view/Surface;

    iget-object v1, p0, Lcom/google/android/gms/car/oi;->k:Landroid/graphics/SurfaceTexture;

    invoke-direct {v0, v1}, Landroid/view/Surface;-><init>(Landroid/graphics/SurfaceTexture;)V

    iput-object v0, p0, Lcom/google/android/gms/car/oi;->l:Landroid/view/Surface;

    .line 257
    iget-object v0, p0, Lcom/google/android/gms/car/oi;->k:Landroid/graphics/SurfaceTexture;

    invoke-virtual {v0, p0}, Landroid/graphics/SurfaceTexture;->setOnFrameAvailableListener(Landroid/graphics/SurfaceTexture$OnFrameAvailableListener;)V

    .line 259
    invoke-virtual {p0}, Lcom/google/android/gms/car/oi;->w()Z

    .line 260
    iget-object v0, p0, Lcom/google/android/gms/car/oi;->n:Ljava/util/concurrent/atomic/AtomicInteger;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;->set(I)V

    .line 261
    iget-object v0, p0, Lcom/google/android/gms/car/oi;->o:Ljava/util/concurrent/atomic/AtomicInteger;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;->set(I)V

    .line 262
    iget-object v1, p0, Lcom/google/android/gms/car/oi;->r:Ljava/lang/Object;

    monitor-enter v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 263
    const/4 v0, 0x1

    :try_start_1
    iput v0, p0, Lcom/google/android/gms/car/oi;->s:I

    .line 264
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 265
    const v0, 0x8d65

    const/4 v1, 0x0

    :try_start_2
    invoke-static {v0, v1}, Landroid/opengl/GLES20;->glBindTexture(II)V

    .line 266
    iget-object v0, p0, Lcom/google/android/gms/car/oi;->q:Lcom/google/android/gms/car/oj;

    iget-object v1, p0, Lcom/google/android/gms/car/oi;->b:Lcom/google/android/gms/car/ok;

    iget v1, v1, Lcom/google/android/gms/car/ok;->c:I

    iget-object v2, p0, Lcom/google/android/gms/car/oi;->b:Lcom/google/android/gms/car/ok;

    iget v2, v2, Lcom/google/android/gms/car/ok;->d:I

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/car/oj;->a(Lcom/google/android/gms/car/oj;II)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 267
    monitor-exit p0

    return-void

    .line 264
    :catchall_0
    move-exception v0

    :try_start_3
    monitor-exit v1

    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 228
    :catchall_1
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized l()V
    .locals 5

    .prologue
    const/4 v4, 0x4

    const/4 v3, 0x3

    .line 270
    monitor-enter p0

    :try_start_0
    const-string v0, "CAR.WM.WIN"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 271
    const-string v0, "CAR.WM.WIN"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "cleanup "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 273
    :cond_0
    const/4 v0, 0x0

    .line 274
    iget v1, p0, Lcom/google/android/gms/car/oi;->s:I

    if-ne v1, v4, :cond_2

    .line 276
    const-string v0, "CAR.WM.WIN"

    const/4 v1, 0x4

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 277
    const-string v0, "CAR.WM.WIN"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "requesting removal for already removed window "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 294
    :cond_1
    :goto_0
    monitor-exit p0

    return-void

    .line 281
    :cond_2
    :try_start_1
    iget v1, p0, Lcom/google/android/gms/car/oi;->s:I

    if-ge v1, v3, :cond_3

    .line 282
    const/4 v0, 0x1

    .line 283
    const/4 v1, 0x3

    iput v1, p0, Lcom/google/android/gms/car/oi;->s:I

    .line 285
    :cond_3
    if-eqz v0, :cond_5

    .line 286
    const-string v0, "CAR.WM.WIN"

    const/4 v1, 0x2

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 287
    const-string v0, "CAR.WM.WIN"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "notify window detach "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 289
    :cond_4
    iget-object v0, p0, Lcom/google/android/gms/car/oi;->q:Lcom/google/android/gms/car/oj;

    invoke-static {v0}, Lcom/google/android/gms/car/oj;->a(Lcom/google/android/gms/car/oj;)V

    .line 291
    :cond_5
    invoke-direct {p0}, Lcom/google/android/gms/car/oi;->y()V

    .line 292
    const/4 v0, 0x4

    iput v0, p0, Lcom/google/android/gms/car/oi;->s:I

    .line 293
    iget-object v0, p0, Lcom/google/android/gms/car/oi;->q:Lcom/google/android/gms/car/oj;

    invoke-static {v0}, Lcom/google/android/gms/car/oj;->b(Lcom/google/android/gms/car/oj;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 270
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final m()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 297
    invoke-virtual {p0}, Lcom/google/android/gms/car/oi;->f()I

    move-result v1

    .line 298
    if-eq v1, v0, :cond_0

    const/4 v2, 0x2

    if-eq v1, v2, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final declared-synchronized n()Lcom/google/android/gms/car/DrawingSpec;
    .locals 5

    .prologue
    .line 303
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/car/oi;->l:Landroid/view/Surface;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    .line 304
    const/4 v0, 0x0

    .line 306
    :goto_0
    monitor-exit p0

    return-object v0

    :cond_0
    :try_start_1
    new-instance v0, Lcom/google/android/gms/car/DrawingSpec;

    iget-object v1, p0, Lcom/google/android/gms/car/oi;->b:Lcom/google/android/gms/car/ok;

    iget v1, v1, Lcom/google/android/gms/car/ok;->c:I

    iget-object v2, p0, Lcom/google/android/gms/car/oi;->b:Lcom/google/android/gms/car/ok;

    iget v2, v2, Lcom/google/android/gms/car/ok;->d:I

    iget-object v3, p0, Lcom/google/android/gms/car/oi;->a:Lcom/google/android/gms/car/on;

    invoke-virtual {v3}, Lcom/google/android/gms/car/on;->g()I

    move-result v3

    iget-object v4, p0, Lcom/google/android/gms/car/oi;->l:Landroid/view/Surface;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/gms/car/DrawingSpec;-><init>(IIILandroid/view/Surface;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 303
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized o()V
    .locals 1

    .prologue
    .line 320
    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    iput-boolean v0, p0, Lcom/google/android/gms/car/oi;->i:Z

    .line 321
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/car/oi;->t:Lcom/google/android/gms/car/qp;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 322
    monitor-exit p0

    return-void

    .line 320
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public onFrameAvailable(Landroid/graphics/SurfaceTexture;)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    const/4 v4, 0x2

    const/4 v1, 0x1

    .line 567
    .line 569
    iget-object v2, p0, Lcom/google/android/gms/car/oi;->r:Ljava/lang/Object;

    monitor-enter v2

    .line 570
    :try_start_0
    iget v3, p0, Lcom/google/android/gms/car/oi;->s:I

    if-ne v3, v4, :cond_1

    .line 571
    iget-boolean v3, p0, Lcom/google/android/gms/car/oi;->c:Z

    if-eqz v3, :cond_6

    .line 577
    :goto_0
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 578
    if-eqz v0, :cond_2

    .line 579
    const-string v0, "CAR.WM.WIN"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 580
    const-string v0, "CAR.WM.WIN"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "onFrameAvailable for non-active window "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 596
    :cond_0
    :goto_1
    return-void

    .line 574
    :cond_1
    :try_start_1
    iget v3, p0, Lcom/google/android/gms/car/oi;->s:I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-eq v3, v1, :cond_6

    move v5, v1

    move v1, v0

    move v0, v5

    .line 575
    goto :goto_0

    .line 577
    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0

    .line 585
    :cond_2
    if-eqz v1, :cond_4

    .line 586
    const-string v0, "CAR.WM.WIN"

    invoke-static {v0, v4}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 587
    const-string v0, "CAR.WM.WIN"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "onFrameAvailable, normal rendering "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 589
    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/car/oi;->a:Lcom/google/android/gms/car/on;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/car/on;->a(Lcom/google/android/gms/car/oi;)V

    goto :goto_1

    .line 591
    :cond_4
    const-string v0, "CAR.WM.WIN"

    invoke-static {v0, v4}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 592
    const-string v0, "CAR.WM.WIN"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "onFrameAvailable, texture update only "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 594
    :cond_5
    iget-object v0, p0, Lcom/google/android/gms/car/oi;->a:Lcom/google/android/gms/car/on;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/car/on;->b(Lcom/google/android/gms/car/oi;)V

    goto :goto_1

    :cond_6
    move v1, v0

    goto :goto_0
.end method

.method public final p()Z
    .locals 2

    .prologue
    .line 356
    invoke-virtual {p0}, Lcom/google/android/gms/car/oi;->f()I

    move-result v0

    .line 357
    const/4 v1, 0x3

    if-eq v0, v1, :cond_0

    const/4 v1, 0x4

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final q()V
    .locals 2

    .prologue
    .line 434
    iget-object v1, p0, Lcom/google/android/gms/car/oi;->r:Ljava/lang/Object;

    monitor-enter v1

    .line 435
    const/4 v0, 0x2

    :try_start_0
    iput v0, p0, Lcom/google/android/gms/car/oi;->s:I

    .line 436
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 437
    iget-object v0, p0, Lcom/google/android/gms/car/oi;->q:Lcom/google/android/gms/car/oj;

    invoke-static {v0}, Lcom/google/android/gms/car/oj;->c(Lcom/google/android/gms/car/oj;)V

    .line 438
    return-void

    .line 436
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final r()V
    .locals 3

    .prologue
    const/4 v2, 0x3

    .line 442
    iget-object v1, p0, Lcom/google/android/gms/car/oi;->r:Ljava/lang/Object;

    monitor-enter v1

    .line 443
    :try_start_0
    iget v0, p0, Lcom/google/android/gms/car/oi;->s:I

    if-lt v0, v2, :cond_0

    .line 445
    monitor-exit v1

    .line 453
    :goto_0
    return-void

    .line 447
    :cond_0
    const/4 v0, 0x3

    iput v0, p0, Lcom/google/android/gms/car/oi;->s:I

    .line 448
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 449
    const-string v0, "CAR.WM.WIN"

    invoke-static {v0, v2}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 450
    const-string v0, "CAR.WM.WIN"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "notify window detach "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 452
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/car/oi;->q:Lcom/google/android/gms/car/oj;

    invoke-static {v0}, Lcom/google/android/gms/car/oj;->a(Lcom/google/android/gms/car/oj;)V

    goto :goto_0

    .line 448
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final s()V
    .locals 3

    .prologue
    const/4 v2, 0x3

    .line 457
    iget-object v1, p0, Lcom/google/android/gms/car/oi;->r:Ljava/lang/Object;

    monitor-enter v1

    .line 458
    :try_start_0
    iget v0, p0, Lcom/google/android/gms/car/oi;->s:I

    if-lt v0, v2, :cond_0

    .line 460
    monitor-exit v1

    .line 463
    :goto_0
    return-void

    .line 462
    :cond_0
    const/4 v0, 0x3

    iput v0, p0, Lcom/google/android/gms/car/oi;->s:I

    .line 463
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method final t()V
    .locals 1

    .prologue
    .line 517
    iget-object v0, p0, Lcom/google/android/gms/car/oi;->p:Lcom/google/android/gms/car/om;

    invoke-interface {v0}, Lcom/google/android/gms/car/om;->a()V

    .line 518
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 600
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 601
    invoke-super {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 602
    const/16 v1, 0x7b

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 603
    const-string v1, "layer="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/google/android/gms/car/oi;->g:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 604
    const-string v1, ",state="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/google/android/gms/car/oi;->s:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 605
    const-string v1, ",visible="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/google/android/gms/car/oi;->c:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 606
    const-string v1, ",in animation="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/google/android/gms/car/oi;->i:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 607
    const-string v1, ",alpha="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/google/android/gms/car/oi;->h:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    .line 608
    iget-object v1, p0, Lcom/google/android/gms/car/oi;->b:Lcom/google/android/gms/car/ok;

    .line 609
    if-eqz v1, :cond_0

    .line 610
    invoke-virtual {v1}, Lcom/google/android/gms/car/ok;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 612
    :cond_0
    const-string v1, ",listener="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/google/android/gms/car/oi;->p:Lcom/google/android/gms/car/om;

    invoke-interface {v1}, Lcom/google/android/gms/car/om;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 613
    const-string v1, ",num texture updated="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/google/android/gms/car/oi;->n:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 614
    const-string v1, ",num frames rendered="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/google/android/gms/car/oi;->o:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 615
    const-string v1, ",texture id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/google/android/gms/car/oi;->d:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 616
    iget-object v1, p0, Lcom/google/android/gms/car/oi;->k:Landroid/graphics/SurfaceTexture;

    .line 617
    if-eqz v1, :cond_1

    .line 618
    const-string v2, ", texture timestamp="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Landroid/graphics/SurfaceTexture;->getTimestamp()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 620
    :cond_1
    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 621
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method final u()V
    .locals 1

    .prologue
    .line 521
    iget-object v0, p0, Lcom/google/android/gms/car/oi;->p:Lcom/google/android/gms/car/om;

    invoke-interface {v0}, Lcom/google/android/gms/car/om;->b()V

    .line 522
    return-void
.end method

.method final v()V
    .locals 1

    .prologue
    .line 525
    iget-object v0, p0, Lcom/google/android/gms/car/oi;->p:Lcom/google/android/gms/car/om;

    .line 526
    return-void
.end method

.method public final declared-synchronized w()Z
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 546
    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lcom/google/android/gms/car/oi;->k:Landroid/graphics/SurfaceTexture;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v1, :cond_1

    .line 561
    :cond_0
    :goto_0
    monitor-exit p0

    return v0

    .line 549
    :cond_1
    :try_start_1
    iget v1, p0, Lcom/google/android/gms/car/oi;->s:I

    const/4 v2, 0x3

    if-ge v1, v2, :cond_0

    .line 553
    iget-object v1, p0, Lcom/google/android/gms/car/oi;->k:Landroid/graphics/SurfaceTexture;

    invoke-virtual {v1}, Landroid/graphics/SurfaceTexture;->updateTexImage()V

    .line 554
    iget-object v1, p0, Lcom/google/android/gms/car/oi;->k:Landroid/graphics/SurfaceTexture;

    iget-object v2, p0, Lcom/google/android/gms/car/oi;->f:[F

    invoke-virtual {v1, v2}, Landroid/graphics/SurfaceTexture;->getTransformMatrix([F)V

    .line 555
    iget-object v1, p0, Lcom/google/android/gms/car/oi;->k:Landroid/graphics/SurfaceTexture;

    invoke-virtual {v1}, Landroid/graphics/SurfaceTexture;->getTimestamp()J

    move-result-wide v2

    .line 556
    iget-wide v4, p0, Lcom/google/android/gms/car/oi;->m:J

    cmp-long v1, v2, v4

    if-eqz v1, :cond_0

    .line 559
    iput-wide v2, p0, Lcom/google/android/gms/car/oi;->m:J

    .line 560
    iget-object v0, p0, Lcom/google/android/gms/car/oi;->n:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->getAndIncrement()I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 561
    const/4 v0, 0x1

    goto :goto_0

    .line 546
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
