.class public final Lcom/google/android/gms/common/data/ai;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/lang/String;

.field public c:Lcom/google/android/gms/common/data/m;


# direct methods
.method public constructor <init>([Ljava/lang/String;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 22
    invoke-direct {p0, p1, v0, v0, v0}, Lcom/google/android/gms/common/data/ai;-><init>([Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 23
    return-void
.end method

.method public constructor <init>([Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    iput-object p3, p0, Lcom/google/android/gms/common/data/ai;->a:Ljava/lang/String;

    .line 28
    iput-object p4, p0, Lcom/google/android/gms/common/data/ai;->b:Ljava/lang/String;

    .line 29
    if-eqz p2, :cond_0

    .line 30
    invoke-static {p1, p2}, Lcom/google/android/gms/common/data/DataHolder;->a([Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/data/m;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/common/data/ai;->c:Lcom/google/android/gms/common/data/m;

    .line 34
    :goto_0
    return-void

    .line 32
    :cond_0
    invoke-static {p1}, Lcom/google/android/gms/common/data/DataHolder;->a([Ljava/lang/String;)Lcom/google/android/gms/common/data/m;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/common/data/ai;->c:Lcom/google/android/gms/common/data/m;

    goto :goto_0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 77
    iget-object v0, p0, Lcom/google/android/gms/common/data/ai;->c:Lcom/google/android/gms/common/data/m;

    iget-object v0, v0, Lcom/google/android/gms/common/data/m;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public final a(I)Lcom/google/android/gms/common/data/DataHolder;
    .locals 2

    .prologue
    .line 97
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const/4 v1, -0x1

    invoke-virtual {p0, p1, v0, v1}, Lcom/google/android/gms/common/data/ai;->a(ILandroid/os/Bundle;I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    return-object v0
.end method

.method public final a(ILandroid/os/Bundle;I)Lcom/google/android/gms/common/data/DataHolder;
    .locals 6

    .prologue
    .line 111
    const-string v0, "next_page_token"

    iget-object v1, p0, Lcom/google/android/gms/common/data/ai;->b:Ljava/lang/String;

    invoke-virtual {p2, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 112
    const-string v0, "prev_page_token"

    iget-object v1, p0, Lcom/google/android/gms/common/data/ai;->a:Ljava/lang/String;

    invoke-virtual {p2, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 113
    iget-object v1, p0, Lcom/google/android/gms/common/data/ai;->c:Lcom/google/android/gms/common/data/m;

    new-instance v0, Lcom/google/android/gms/common/data/DataHolder;

    const/4 v5, 0x0

    move v2, p1

    move-object v3, p2

    move v4, p3

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/common/data/DataHolder;-><init>(Lcom/google/android/gms/common/data/m;ILandroid/os/Bundle;IB)V

    return-object v0
.end method

.method public final a(Landroid/content/ContentValues;)V
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lcom/google/android/gms/common/data/ai;->c:Lcom/google/android/gms/common/data/m;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/common/data/m;->a(Landroid/content/ContentValues;)Lcom/google/android/gms/common/data/m;

    .line 55
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 81
    iget-object v0, p0, Lcom/google/android/gms/common/data/ai;->c:Lcom/google/android/gms/common/data/m;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/common/data/m;->a(Ljava/lang/String;)Lcom/google/android/gms/common/data/m;

    .line 82
    return-void
.end method

.method public final b(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 85
    iget-object v0, p0, Lcom/google/android/gms/common/data/ai;->c:Lcom/google/android/gms/common/data/m;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/common/data/m;->b(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {v0, p1}, Lcom/google/android/gms/common/data/m;->a(Ljava/lang/String;)Lcom/google/android/gms/common/data/m;

    iget-object v0, v0, Lcom/google/android/gms/common/data/m;->b:Ljava/util/ArrayList;

    invoke-static {v0}, Ljava/util/Collections;->reverse(Ljava/util/List;)V

    .line 86
    :cond_0
    return-void
.end method
