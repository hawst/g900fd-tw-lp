.class public final Lcom/google/android/gms/games/service/a/n/h;
.super Lcom/google/android/gms/games/service/a/b;
.source "SourceFile"


# instance fields
.field private final b:Lcom/google/android/gms/games/internal/dr;

.field private final c:Lcom/google/android/gms/games/a/au;

.field private final d:I

.field private final e:[I

.field private f:I


# direct methods
.method public constructor <init>(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/internal/dr;I[I)V
    .locals 2

    .prologue
    .line 33
    iget-object v0, p1, Lcom/google/android/gms/games/a/au;->b:Lcom/google/android/gms/common/server/ClientContext;

    array-length v1, p4

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/games/service/a/b;-><init>(Lcom/google/android/gms/common/server/ClientContext;I)V

    .line 34
    iput-object p2, p0, Lcom/google/android/gms/games/service/a/n/h;->b:Lcom/google/android/gms/games/internal/dr;

    .line 35
    iput-object p1, p0, Lcom/google/android/gms/games/service/a/n/h;->c:Lcom/google/android/gms/games/a/au;

    .line 36
    iput p3, p0, Lcom/google/android/gms/games/service/a/n/h;->d:I

    .line 37
    iput-object p4, p0, Lcom/google/android/gms/games/service/a/n/h;->e:[I

    .line 39
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/gms/games/service/a/n/h;->f:I

    .line 40
    return-void
.end method


# virtual methods
.method protected final a(Lcom/google/android/gms/games/a/t;I)Lcom/google/android/gms/common/data/DataHolder;
    .locals 3

    .prologue
    .line 45
    if-ltz p2, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/games/service/a/n/h;->e:[I

    array-length v0, v0

    if-ge p2, v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/android/gms/common/internal/e;->a(Z)V

    .line 48
    if-nez p2, :cond_0

    .line 49
    iget-object v0, p0, Lcom/google/android/gms/games/service/a/n/h;->c:Lcom/google/android/gms/games/a/au;

    new-instance v1, Lcom/google/android/gms/games/service/ad;

    invoke-direct {v1}, Lcom/google/android/gms/games/service/ad;-><init>()V

    invoke-virtual {p1, v0, v1}, Lcom/google/android/gms/games/a/t;->j(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/service/ad;)I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/games/service/a/n/h;->f:I

    .line 53
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/games/service/a/n/h;->e:[I

    aget v0, v0, p2

    .line 55
    if-nez v0, :cond_2

    .line 56
    iget-object v0, p0, Lcom/google/android/gms/games/service/a/n/h;->c:Lcom/google/android/gms/games/a/au;

    iget v1, p0, Lcom/google/android/gms/games/service/a/n/h;->d:I

    iget v2, p0, Lcom/google/android/gms/games/service/a/n/h;->f:I

    invoke-virtual {p1, v0, v1, v2}, Lcom/google/android/gms/games/a/t;->a(Lcom/google/android/gms/games/a/au;II)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    .line 62
    :goto_1
    return-object v0

    .line 45
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 59
    :cond_2
    iget-object v1, p0, Lcom/google/android/gms/games/service/a/n/h;->c:Lcom/google/android/gms/games/a/au;

    iget v2, p0, Lcom/google/android/gms/games/service/a/n/h;->f:I

    invoke-virtual {p1, v1, v0, v2}, Lcom/google/android/gms/games/a/t;->b(Lcom/google/android/gms/games/a/au;II)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    goto :goto_1
.end method

.method protected final a([Lcom/google/android/gms/common/data/DataHolder;)V
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 67
    iget-object v0, p0, Lcom/google/android/gms/games/service/a/n/h;->e:[I

    array-length v0, v0

    array-length v2, p1

    if-ne v0, v2, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/android/gms/common/internal/e;->a(Z)V

    .line 72
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    move v0, v1

    .line 73
    :goto_1
    iget-object v3, p0, Lcom/google/android/gms/games/service/a/n/h;->e:[I

    array-length v3, v3

    if-ge v1, v3, :cond_1

    .line 74
    iget-object v3, p0, Lcom/google/android/gms/games/service/a/n/h;->e:[I

    aget v3, v3, v1

    invoke-static {v3}, Lcom/google/android/gms/games/internal/b/k;->a(I)Ljava/lang/String;

    move-result-object v3

    .line 75
    aget-object v4, p1, v1

    invoke-virtual {v2, v3, v4}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 76
    aget-object v3, p1, v1

    invoke-virtual {v3}, Lcom/google/android/gms/common/data/DataHolder;->h()I

    move-result v3

    add-int/2addr v0, v3

    .line 73
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_0
    move v0, v1

    .line 67
    goto :goto_0

    .line 78
    :cond_1
    iget-object v1, p0, Lcom/google/android/gms/games/service/a/n/h;->b:Lcom/google/android/gms/games/internal/dr;

    iget v3, p0, Lcom/google/android/gms/games/service/a/n/h;->f:I

    invoke-static {v0, v3}, Lcom/google/android/gms/games/a/l;->a(II)I

    move-result v0

    invoke-interface {v1, v0, v2}, Lcom/google/android/gms/games/internal/dr;->a(ILandroid/os/Bundle;)V

    .line 80
    return-void
.end method
