.class final Lcom/google/android/gms/icing/ui/c;
.super Lcom/google/android/gms/icing/ui/a;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/gms/icing/ui/IcingManageSpaceActivity;

.field private final c:Z


# direct methods
.method public constructor <init>(Lcom/google/android/gms/icing/ui/IcingManageSpaceActivity;)V
    .locals 1

    .prologue
    .line 185
    iput-object p1, p0, Lcom/google/android/gms/icing/ui/c;->a:Lcom/google/android/gms/icing/ui/IcingManageSpaceActivity;

    .line 186
    invoke-direct {p0, p1}, Lcom/google/android/gms/icing/ui/a;-><init>(Landroid/content/Context;)V

    .line 187
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/icing/ui/c;->c:Z

    .line 188
    return-void
.end method


# virtual methods
.method protected final a()V
    .locals 2

    .prologue
    .line 210
    iget-object v0, p0, Lcom/google/android/gms/icing/ui/c;->a:Lcom/google/android/gms/icing/ui/IcingManageSpaceActivity;

    invoke-static {v0}, Lcom/google/android/gms/icing/ui/IcingManageSpaceActivity;->b(Lcom/google/android/gms/icing/ui/IcingManageSpaceActivity;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    .line 211
    iget-object v0, p0, Lcom/google/android/gms/icing/ui/c;->a:Lcom/google/android/gms/icing/ui/IcingManageSpaceActivity;

    invoke-static {v0}, Lcom/google/android/gms/icing/ui/IcingManageSpaceActivity;->b(Lcom/google/android/gms/icing/ui/IcingManageSpaceActivity;)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 213
    :cond_0
    return-void
.end method

.method protected final synthetic a(Ljava/lang/Object;)V
    .locals 4

    .prologue
    .line 181
    check-cast p1, Lcom/google/android/gms/icing/ui/b;

    invoke-super {p0, p1}, Lcom/google/android/gms/icing/ui/a;->a(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/gms/icing/ui/c;->a:Lcom/google/android/gms/icing/ui/IcingManageSpaceActivity;

    invoke-static {v0}, Lcom/google/android/gms/icing/ui/IcingManageSpaceActivity;->a(Lcom/google/android/gms/icing/ui/IcingManageSpaceActivity;)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/gms/icing/ui/c;->a:Lcom/google/android/gms/icing/ui/IcingManageSpaceActivity;

    invoke-static {v0}, Lcom/google/android/gms/icing/ui/IcingManageSpaceActivity;->b(Lcom/google/android/gms/icing/ui/IcingManageSpaceActivity;)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/gms/icing/ui/c;->a:Lcom/google/android/gms/icing/ui/IcingManageSpaceActivity;

    invoke-static {v0}, Lcom/google/android/gms/icing/ui/IcingManageSpaceActivity;->c(Lcom/google/android/gms/icing/ui/IcingManageSpaceActivity;)Landroid/widget/TextView;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/icing/ui/c;->a:Lcom/google/android/gms/icing/ui/IcingManageSpaceActivity;

    iget-wide v2, p1, Lcom/google/android/gms/icing/ui/b;->c:J

    invoke-static {v1, v2, v3}, Landroid/text/format/Formatter;->formatFileSize(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/android/gms/icing/ui/c;->a:Lcom/google/android/gms/icing/ui/IcingManageSpaceActivity;

    invoke-static {v0}, Lcom/google/android/gms/icing/ui/IcingManageSpaceActivity;->d(Lcom/google/android/gms/icing/ui/IcingManageSpaceActivity;)Landroid/widget/TextView;

    move-result-object v0

    sget v1, Lcom/google/android/gms/p;->pz:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    iget-object v0, p0, Lcom/google/android/gms/icing/ui/c;->a:Lcom/google/android/gms/icing/ui/IcingManageSpaceActivity;

    invoke-static {v0}, Lcom/google/android/gms/icing/ui/IcingManageSpaceActivity;->e(Lcom/google/android/gms/icing/ui/IcingManageSpaceActivity;)Landroid/widget/ListView;

    move-result-object v0

    new-instance v1, Lcom/google/android/gms/icing/ui/d;

    iget-object v2, p0, Lcom/google/android/gms/icing/ui/c;->a:Lcom/google/android/gms/icing/ui/IcingManageSpaceActivity;

    iget-object v3, p1, Lcom/google/android/gms/icing/ui/b;->a:Ljava/util/List;

    invoke-direct {v1, v2, v3}, Lcom/google/android/gms/icing/ui/d;-><init>(Landroid/content/Context;Ljava/util/List;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    return-void
.end method

.method protected final onPreExecute()V
    .locals 4

    .prologue
    const/4 v1, 0x4

    const/4 v2, 0x0

    .line 192
    iget-object v0, p0, Lcom/google/android/gms/icing/ui/c;->a:Lcom/google/android/gms/icing/ui/IcingManageSpaceActivity;

    invoke-static {v0}, Lcom/google/android/gms/icing/ui/IcingManageSpaceActivity;->a(Lcom/google/android/gms/icing/ui/IcingManageSpaceActivity;)Landroid/view/View;

    move-result-object v3

    iget-boolean v0, p0, Lcom/google/android/gms/icing/ui/c;->c:Z

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {v3, v0}, Landroid/view/View;->setVisibility(I)V

    .line 193
    iget-object v0, p0, Lcom/google/android/gms/icing/ui/c;->a:Lcom/google/android/gms/icing/ui/IcingManageSpaceActivity;

    invoke-static {v0}, Lcom/google/android/gms/icing/ui/IcingManageSpaceActivity;->b(Lcom/google/android/gms/icing/ui/IcingManageSpaceActivity;)Landroid/view/View;

    move-result-object v0

    iget-boolean v3, p0, Lcom/google/android/gms/icing/ui/c;->c:Z

    if-eqz v3, :cond_1

    :goto_1
    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 194
    return-void

    :cond_0
    move v0, v2

    .line 192
    goto :goto_0

    :cond_1
    move v2, v1

    .line 193
    goto :goto_1
.end method
