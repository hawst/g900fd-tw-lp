.class public final Lcom/google/android/gms/icing/impl/l;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/icing/impl/k;


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Lcom/google/android/gms/common/api/v;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    iput-object p1, p0, Lcom/google/android/gms/icing/impl/l;->a:Landroid/content/Context;

    .line 38
    new-instance v0, Lcom/google/android/gms/common/api/w;

    invoke-direct {v0, p1}, Lcom/google/android/gms/common/api/w;-><init>(Landroid/content/Context;)V

    sget-object v1, Lcom/google/android/gms/lockbox/a;->a:Lcom/google/android/gms/common/api/c;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/api/w;->a(Lcom/google/android/gms/common/api/c;)Lcom/google/android/gms/common/api/w;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/common/api/w;->a()Lcom/google/android/gms/common/api/v;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/icing/impl/l;->b:Lcom/google/android/gms/common/api/v;

    .line 39
    return-void
.end method

.method private d()Landroid/content/SharedPreferences;
    .locals 3

    .prologue
    .line 42
    iget-object v1, p0, Lcom/google/android/gms/icing/impl/l;->a:Landroid/content/Context;

    const-string v2, "LockboxOptInSettings"

    const/16 v0, 0xb

    invoke-static {v0}, Lcom/google/android/gms/common/util/ao;->a(I)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x4

    goto :goto_0
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 3

    .prologue
    .line 49
    invoke-direct {p0}, Lcom/google/android/gms/icing/impl/l;->d()Landroid/content/SharedPreferences;

    move-result-object v0

    .line 50
    const-string v1, "signed-in-account"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;Lcom/google/android/gms/lockbox/g;)V
    .locals 4

    .prologue
    .line 70
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 84
    :cond_0
    :goto_0
    return-void

    .line 71
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/l;->b:Lcom/google/android/gms/common/api/v;

    invoke-interface {v0}, Lcom/google/android/gms/common/api/v;->c()Lcom/google/android/gms/common/c;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/common/c;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 76
    :try_start_0
    sget-object v0, Lcom/google/android/gms/lockbox/a;->b:Lcom/google/android/gms/lockbox/d;

    iget-object v1, p0, Lcom/google/android/gms/icing/impl/l;->b:Lcom/google/android/gms/common/api/v;

    new-instance v2, Landroid/accounts/Account;

    const-string v3, "com.google"

    invoke-direct {v2, p1, v3}, Landroid/accounts/Account;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1, v2, p2}, Lcom/google/android/gms/lockbox/d;->a(Lcom/google/android/gms/common/api/v;Landroid/accounts/Account;Lcom/google/android/gms/lockbox/g;)Lcom/google/android/gms/common/api/am;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/common/api/am;->a()Lcom/google/android/gms/common/api/ap;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/api/Status;

    .line 79
    invoke-virtual {v0}, Lcom/google/android/gms/common/api/Status;->f()Z

    move-result v0

    if-nez v0, :cond_2

    .line 80
    const-string v0, "Failed to set opt-in options."

    invoke-static {v0}, Lcom/google/android/gms/icing/c;->e(Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 83
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/l;->b:Lcom/google/android/gms/common/api/v;

    invoke-interface {v0}, Lcom/google/android/gms/common/api/v;->d()V

    goto :goto_0

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/google/android/gms/icing/impl/l;->b:Lcom/google/android/gms/common/api/v;

    invoke-interface {v1}, Lcom/google/android/gms/common/api/v;->d()V

    throw v0
.end method

.method public final a(Ljava/lang/String;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 61
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 65
    :goto_0
    return v0

    .line 62
    :cond_0
    invoke-direct {p0}, Lcom/google/android/gms/icing/impl/l;->d()Landroid/content/SharedPreferences;

    move-result-object v1

    .line 63
    const/4 v2, 0x1

    invoke-static {p1, v2}, Lcom/google/android/gms/lockbox/e/a;->c(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v2

    .line 65
    invoke-interface {v1, v2, v0}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    goto :goto_0
.end method

.method public final b()J
    .locals 4

    .prologue
    .line 55
    invoke-direct {p0}, Lcom/google/android/gms/icing/impl/l;->d()Landroid/content/SharedPreferences;

    move-result-object v0

    .line 56
    const-string v1, "signed-in-timestamp"

    const-wide/16 v2, 0x0

    invoke-interface {v0, v1, v2, v3}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    return-wide v0
.end method

.method public final c()V
    .locals 2

    .prologue
    .line 89
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/l;->b:Lcom/google/android/gms/common/api/v;

    invoke-interface {v0}, Lcom/google/android/gms/common/api/v;->c()Lcom/google/android/gms/common/c;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/common/c;->b()Z

    move-result v0

    if-nez v0, :cond_0

    .line 100
    :goto_0
    return-void

    .line 94
    :cond_0
    :try_start_0
    sget-object v0, Lcom/google/android/gms/lockbox/a;->b:Lcom/google/android/gms/lockbox/d;

    iget-object v1, p0, Lcom/google/android/gms/icing/impl/l;->b:Lcom/google/android/gms/common/api/v;

    invoke-interface {v0, v1}, Lcom/google/android/gms/lockbox/d;->a(Lcom/google/android/gms/common/api/v;)Lcom/google/android/gms/common/api/am;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/common/api/am;->a()Lcom/google/android/gms/common/api/ap;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/api/Status;

    .line 95
    invoke-virtual {v0}, Lcom/google/android/gms/common/api/Status;->f()Z

    move-result v0

    if-nez v0, :cond_1

    .line 96
    const-string v0, "Failed to remove signed-in account."

    invoke-static {v0}, Lcom/google/android/gms/icing/c;->e(Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 99
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/l;->b:Lcom/google/android/gms/common/api/v;

    invoke-interface {v0}, Lcom/google/android/gms/common/api/v;->d()V

    goto :goto_0

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/google/android/gms/icing/impl/l;->b:Lcom/google/android/gms/common/api/v;

    invoke-interface {v1}, Lcom/google/android/gms/common/api/v;->d()V

    throw v0
.end method
