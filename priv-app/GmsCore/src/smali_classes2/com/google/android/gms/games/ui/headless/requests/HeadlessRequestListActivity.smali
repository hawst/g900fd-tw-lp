.class public final Lcom/google/android/gms/games/ui/headless/requests/HeadlessRequestListActivity;
.super Lcom/google/android/gms/games/ui/headless/a;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/games/ui/b/a/s;
.implements Lcom/google/android/gms/games/ui/cj;
.implements Lcom/google/android/gms/games/ui/common/requests/m;


# static fields
.field private static final i:I

.field private static final j:I


# instance fields
.field private k:Lcom/google/android/gms/games/ui/headless/requests/a;

.field private l:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 32
    sget v0, Lcom/google/android/gms/l;->aR:I

    sput v0, Lcom/google/android/gms/games/ui/headless/requests/HeadlessRequestListActivity;->i:I

    .line 35
    sget v0, Lcom/google/android/gms/m;->o:I

    sput v0, Lcom/google/android/gms/games/ui/headless/requests/HeadlessRequestListActivity;->j:I

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 47
    sget v0, Lcom/google/android/gms/games/ui/headless/requests/HeadlessRequestListActivity;->i:I

    sget v1, Lcom/google/android/gms/games/ui/headless/requests/HeadlessRequestListActivity;->j:I

    invoke-direct {p0, v0, v1, v2, v2}, Lcom/google/android/gms/games/ui/headless/a;-><init>(IIZZ)V

    .line 49
    return-void
.end method

.method private N()Landroid/support/v4/app/Fragment;
    .locals 3

    .prologue
    .line 93
    iget v0, p0, Lcom/google/android/gms/games/ui/headless/requests/HeadlessRequestListActivity;->l:I

    packed-switch v0, :pswitch_data_0

    .line 101
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "getCurrentFragment: unexpected index "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/google/android/gms/games/ui/headless/requests/HeadlessRequestListActivity;->l:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 95
    :pswitch_0
    const/4 v0, 0x1

    invoke-static {v0}, Lcom/google/android/gms/games/ui/common/requests/j;->b(I)Lcom/google/android/gms/games/ui/common/requests/j;

    move-result-object v0

    .line 98
    :goto_0
    return-object v0

    :pswitch_1
    const/4 v0, 0x2

    invoke-static {v0}, Lcom/google/android/gms/games/ui/common/requests/j;->b(I)Lcom/google/android/gms/games/ui/common/requests/j;

    move-result-object v0

    goto :goto_0

    .line 93
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method protected final K()V
    .locals 2

    .prologue
    .line 73
    invoke-super {p0}, Lcom/google/android/gms/games/ui/headless/a;->K()V

    .line 76
    invoke-super {p0}, Landroid/support/v7/app/d;->d()Landroid/support/v7/app/e;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v7/app/e;->b()Landroid/support/v7/app/a;

    move-result-object v0

    .line 77
    invoke-virtual {v0}, Landroid/support/v7/app/a;->f()V

    .line 78
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/support/v7/app/a;->a(Z)V

    .line 79
    return-void
.end method

.method public final M_()V
    .locals 2

    .prologue
    .line 132
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/headless/requests/HeadlessRequestListActivity;->N()Landroid/support/v4/app/Fragment;

    move-result-object v0

    .line 133
    instance-of v1, v0, Lcom/google/android/gms/games/ui/cj;

    if-eqz v1, :cond_0

    .line 134
    check-cast v0, Lcom/google/android/gms/games/ui/cj;

    invoke-interface {v0}, Lcom/google/android/gms/games/ui/cj;->M_()V

    .line 136
    :cond_0
    return-void
.end method

.method public final O_()Lcom/google/android/gms/games/ui/common/requests/l;
    .locals 1

    .prologue
    .line 140
    iget-object v0, p0, Lcom/google/android/gms/games/ui/headless/requests/HeadlessRequestListActivity;->k:Lcom/google/android/gms/games/ui/headless/requests/a;

    return-object v0
.end method

.method public final P_()Lcom/google/android/gms/games/ui/b/a/r;
    .locals 1

    .prologue
    .line 145
    iget-object v0, p0, Lcom/google/android/gms/games/ui/headless/requests/HeadlessRequestListActivity;->k:Lcom/google/android/gms/games/ui/headless/requests/a;

    return-object v0
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    const/4 v2, -0x1

    .line 53
    invoke-super {p0, p1}, Lcom/google/android/gms/games/ui/headless/a;->onCreate(Landroid/os/Bundle;)V

    .line 54
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/headless/requests/HeadlessRequestListActivity;->isFinishing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 69
    :goto_0
    return-void

    .line 58
    :cond_0
    new-instance v0, Lcom/google/android/gms/games/ui/headless/requests/a;

    invoke-direct {v0, p0}, Lcom/google/android/gms/games/ui/headless/requests/a;-><init>(Lcom/google/android/gms/games/ui/headless/a;)V

    iput-object v0, p0, Lcom/google/android/gms/games/ui/headless/requests/HeadlessRequestListActivity;->k:Lcom/google/android/gms/games/ui/headless/requests/a;

    .line 60
    iget-object v0, p0, Lcom/google/android/gms/games/ui/q;->b:Lcom/google/android/gms/games/ui/bh;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/bh;->g()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/headless/requests/HeadlessRequestListActivity;->a(Ljava/lang/CharSequence;)V

    .line 62
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/headless/requests/HeadlessRequestListActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "com.google.android.gms.games.FRAGMENT_INDEX"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/games/ui/headless/requests/HeadlessRequestListActivity;->l:I

    .line 63
    iget v0, p0, Lcom/google/android/gms/games/ui/headless/requests/HeadlessRequestListActivity;->l:I

    if-ne v0, v2, :cond_1

    .line 64
    const-string v0, "HeadlessRequestAct"

    const-string v1, "Fragment Index not found in the Intent! Bailing!"

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 65
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/headless/requests/HeadlessRequestListActivity;->finish()V

    goto :goto_0

    .line 68
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/headless/requests/HeadlessRequestListActivity;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v0

    invoke-direct {p0}, Lcom/google/android/gms/games/ui/headless/requests/HeadlessRequestListActivity;->N()Landroid/support/v4/app/Fragment;

    move-result-object v1

    invoke-virtual {v0}, Landroid/support/v4/app/v;->a()Landroid/support/v4/app/aj;

    move-result-object v0

    sget v2, Lcom/google/android/gms/j;->dw:I

    invoke-virtual {v0, v2, v1}, Landroid/support/v4/app/aj;->b(ILandroid/support/v4/app/Fragment;)Landroid/support/v4/app/aj;

    invoke-virtual {v0}, Landroid/support/v4/app/aj;->a()I

    iget v0, p0, Lcom/google/android/gms/games/ui/headless/requests/HeadlessRequestListActivity;->l:I

    packed-switch v0, :pswitch_data_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "setCurrentTitle: unexpected index: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/google/android/gms/games/ui/headless/requests/HeadlessRequestListActivity;->l:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_0
    sget v0, Lcom/google/android/gms/p;->kO:I

    :goto_1
    if-lez v0, :cond_2

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/headless/requests/HeadlessRequestListActivity;->setTitle(I)V

    goto :goto_0

    :pswitch_1
    sget v0, Lcom/google/android/gms/p;->kQ:I

    goto :goto_1

    :cond_2
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/headless/requests/HeadlessRequestListActivity;->setTitle(Ljava/lang/CharSequence;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
