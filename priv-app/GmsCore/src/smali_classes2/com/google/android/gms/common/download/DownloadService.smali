.class public Lcom/google/android/gms/common/download/DownloadService;
.super Landroid/app/IntentService;
.source "SourceFile"


# static fields
.field static a:Landroid/os/PowerManager$WakeLock;

.field public static b:Z

.field private static c:J

.field private static d:J

.field private static final e:Ljava/lang/Object;

.field private static final f:Ljava/lang/Object;

.field private static final g:Ljava/util/regex/Pattern;

.field private static h:Z


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 95
    const-wide/32 v0, 0x1b7740

    sput-wide v0, Lcom/google/android/gms/common/download/DownloadService;->c:J

    .line 96
    const-wide/32 v0, 0xea60

    sput-wide v0, Lcom/google/android/gms/common/download/DownloadService;->d:J

    .line 98
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/google/android/gms/common/download/DownloadService;->e:Ljava/lang/Object;

    .line 99
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/google/android/gms/common/download/DownloadService;->f:Ljava/lang/Object;

    .line 101
    const-string v0, "[a-zA-Z0-9_\\.]+"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/common/download/DownloadService;->g:Ljava/util/regex/Pattern;

    .line 106
    sput-boolean v2, Lcom/google/android/gms/common/download/DownloadService;->b:Z

    .line 109
    sput-boolean v2, Lcom/google/android/gms/common/download/DownloadService;->h:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 112
    const-string v0, "GmsDownloadService"

    invoke-direct {p0, v0}, Landroid/app/IntentService;-><init>(Ljava/lang/String;)V

    .line 113
    return-void
.end method

.method static a(Landroid/content/Context;)Landroid/content/SharedPreferences;
    .locals 2

    .prologue
    .line 629
    const-string v0, "DownloadService"

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    return-object v0
.end method

.method private static a(Landroid/content/Context;I)Lcom/google/android/gms/common/api/Status;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 637
    sparse-switch p1, :sswitch_data_0

    .line 653
    new-instance v0, Lcom/google/android/gms/common/api/Status;

    invoke-direct {v0, p1}, Lcom/google/android/gms/common/api/Status;-><init>(I)V

    :goto_0
    return-object v0

    .line 639
    :sswitch_0
    new-instance v0, Lcom/google/android/gms/common/api/Status;

    const/16 v1, 0x1b58

    sget v2, Lcom/google/android/gms/p;->fH:I

    invoke-virtual {p0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/gms/common/api/Status;-><init>(ILjava/lang/String;Landroid/app/PendingIntent;)V

    goto :goto_0

    .line 642
    :sswitch_1
    new-instance v0, Lcom/google/android/gms/common/api/Status;

    const/16 v1, 0x1b59

    sget v2, Lcom/google/android/gms/p;->fF:I

    invoke-virtual {p0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/gms/common/api/Status;-><init>(ILjava/lang/String;Landroid/app/PendingIntent;)V

    goto :goto_0

    .line 645
    :sswitch_2
    new-instance v0, Lcom/google/android/gms/common/api/Status;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/google/android/gms/common/api/Status;-><init>(I)V

    goto :goto_0

    .line 647
    :sswitch_3
    new-instance v0, Lcom/google/android/gms/common/api/Status;

    const/16 v1, 0xd

    sget v2, Lcom/google/android/gms/p;->fE:I

    invoke-virtual {p0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/gms/common/api/Status;-><init>(ILjava/lang/String;Landroid/app/PendingIntent;)V

    goto :goto_0

    .line 650
    :sswitch_4
    new-instance v0, Lcom/google/android/gms/common/api/Status;

    const/16 v1, 0x1b5a

    sget v2, Lcom/google/android/gms/p;->fG:I

    invoke-virtual {p0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/gms/common/api/Status;-><init>(ILjava/lang/String;Landroid/app/PendingIntent;)V

    goto :goto_0

    .line 637
    nop

    :sswitch_data_0
    .sparse-switch
        0x1b58 -> :sswitch_0
        0x1b59 -> :sswitch_1
        0x1b62 -> :sswitch_2
        0x1b63 -> :sswitch_3
        0x1b64 -> :sswitch_4
    .end sparse-switch
.end method

.method static declared-synchronized a(Ljava/io/File;)Ljava/lang/String;
    .locals 7

    .prologue
    .line 719
    const-class v2, Lcom/google/android/gms/common/download/DownloadService;

    monitor-enter v2

    :try_start_0
    sget-boolean v0, Lcom/google/android/gms/common/download/DownloadService;->b:Z

    if-eqz v0, :cond_0

    .line 720
    const-string v0, "testing123"
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 745
    :goto_0
    monitor-exit v2

    return-object v0

    .line 723
    :cond_0
    :try_start_1
    const-string v0, "SHA1"

    invoke-static {v0}, Lcom/google/android/gms/common/util/e;->b(Ljava/lang/String;)Ljava/security/MessageDigest;

    move-result-object v3

    .line 724
    if-nez v3, :cond_1

    .line 725
    const-string v0, ""
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 727
    :cond_1
    const/4 v1, 0x0

    .line 729
    :try_start_2
    new-instance v0, Ljava/io/FileInputStream;

    invoke-direct {v0, p0}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_4
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 730
    const/16 v1, 0x2000

    :try_start_3
    new-array v1, v1, [B

    .line 732
    :goto_1
    invoke-virtual {v0, v1}, Ljava/io/FileInputStream;->read([B)I

    move-result v4

    const/4 v5, -0x1

    if-eq v4, v5, :cond_3

    .line 733
    const/4 v5, 0x0

    invoke-virtual {v3, v1, v5, v4}, Ljava/security/MessageDigest;->update([BII)V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    goto :goto_1

    .line 738
    :catch_0
    move-exception v1

    :goto_2
    :try_start_4
    const-string v1, ""
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 740
    if-eqz v0, :cond_2

    .line 742
    :try_start_5
    invoke-virtual {v0}, Ljava/io/FileInputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_2
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    :cond_2
    :goto_3
    move-object v0, v1

    .line 745
    goto :goto_0

    .line 735
    :cond_3
    :try_start_6
    invoke-virtual {v3}, Ljava/security/MessageDigest;->digest()[B

    move-result-object v1

    const/4 v3, 0x0

    invoke-static {v1, v3}, Lcom/google/android/gms/common/util/e;->a([BZ)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_0
    .catchall {:try_start_6 .. :try_end_6} :catchall_2

    move-result-object v1

    .line 740
    :try_start_7
    invoke-virtual {v0}, Ljava/io/FileInputStream;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_1
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    :cond_4
    :goto_4
    move-object v0, v1

    .line 745
    goto :goto_0

    .line 743
    :catch_1
    move-exception v3

    .line 744
    :try_start_8
    sget-object v0, Lcom/google/android/gms/common/download/a/a;->a:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-virtual {v3}, Ljava/io/IOException;->printStackTrace()V
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    goto :goto_4

    .line 719
    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0

    .line 743
    :catch_2
    move-exception v3

    .line 744
    :try_start_9
    sget-object v0, Lcom/google/android/gms/common/download/a/a;->a:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {v3}, Ljava/io/IOException;->printStackTrace()V
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    goto :goto_3

    .line 740
    :catchall_1
    move-exception v0

    move-object v6, v0

    move-object v0, v1

    move-object v1, v6

    :goto_5
    if-eqz v0, :cond_5

    .line 742
    :try_start_a
    invoke-virtual {v0}, Ljava/io/FileInputStream;->close()V
    :try_end_a
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_3
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    .line 745
    :cond_5
    :goto_6
    :try_start_b
    throw v1

    .line 743
    :catch_3
    move-exception v3

    .line 744
    sget-object v0, Lcom/google/android/gms/common/download/a/a;->a:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-virtual {v3}, Ljava/io/IOException;->printStackTrace()V
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_0

    goto :goto_6

    .line 740
    :catchall_2
    move-exception v1

    goto :goto_5

    .line 738
    :catch_4
    move-exception v0

    move-object v0, v1

    goto :goto_2
.end method

.method private a()V
    .locals 9

    .prologue
    const/4 v1, 0x0

    .line 328
    const-string v0, "DownloadService"

    invoke-virtual {p0, v0, v1}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v2

    .line 330
    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v3

    .line 331
    new-instance v4, Lorg/json/JSONArray;

    const-string v0, "ds__downloads_json__"

    const-string v5, "[]"

    invoke-interface {v2, v0, v5}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v4, v0}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V

    move v0, v1

    .line 332
    :goto_0
    invoke-virtual {v4}, Lorg/json/JSONArray;->length()I

    move-result v5

    if-ge v0, v5, :cond_1

    .line 333
    new-instance v5, Lcom/google/android/gms/common/download/f;

    invoke-virtual {v4, v0}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v6

    invoke-direct {v5, v6}, Lcom/google/android/gms/common/download/f;-><init>(Lorg/json/JSONObject;)V

    .line 334
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v7, v5, Lcom/google/android/gms/common/download/f;->a:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "__enabled__"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v2, v6, v1}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v6

    .line 336
    iget-object v7, v5, Lcom/google/android/gms/common/download/f;->a:Ljava/lang/String;

    invoke-static {p0, v7}, Lcom/google/android/gms/common/download/r;->g(Landroid/content/Context;Ljava/lang/String;)Lcom/google/android/gms/common/download/f;

    move-result-object v7

    .line 339
    if-nez v7, :cond_0

    .line 340
    invoke-static {p0, v5, v6}, Lcom/google/android/gms/common/download/r;->a(Landroid/content/Context;Lcom/google/android/gms/common/download/f;Z)Landroid/net/Uri;

    .line 343
    :cond_0
    iget-object v6, v5, Lcom/google/android/gms/common/download/f;->a:Ljava/lang/String;

    invoke-interface {v3, v6}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v6

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v8, v5, Lcom/google/android/gms/common/download/f;->a:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "__enabled__"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-interface {v6, v7}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v6

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v8, v5, Lcom/google/android/gms/common/download/f;->a:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "__sha1__"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-interface {v6, v7}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v6

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v8, v5, Lcom/google/android/gms/common/download/f;->a:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "__dest__"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-interface {v6, v7}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v6

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v5, v5, Lcom/google/android/gms/common/download/f;->a:Ljava/lang/String;

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v7, "__failed__"

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v6, v5}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 332
    add-int/lit8 v0, v0, 0x1

    goto/16 :goto_0

    .line 349
    :cond_1
    invoke-interface {v3}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 350
    return-void
.end method

.method private a(J)V
    .locals 9

    .prologue
    const/4 v8, 0x0

    .line 392
    sget-wide v0, Lcom/google/android/gms/common/download/DownloadService;->d:J

    cmp-long v0, p1, v0

    if-gez v0, :cond_0

    .line 393
    sget-wide p1, Lcom/google/android/gms/common/download/DownloadService;->d:J

    .line 396
    :cond_0
    const-string v0, "DownloadService"

    invoke-virtual {p0, v0, v8}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 397
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    .line 398
    add-long v4, v2, p1

    .line 399
    const-string v0, "ds__next_alarm__"

    const-wide/16 v6, 0x0

    invoke-interface {v1, v0, v6, v7}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v6

    .line 401
    cmp-long v0, v6, v2

    if-ltz v0, :cond_1

    cmp-long v0, v6, v2

    if-lez v0, :cond_2

    cmp-long v0, v6, v4

    if-lez v0, :cond_2

    .line 402
    :cond_1
    invoke-static {p0}, Lcom/google/android/gms/common/download/DownloadAlarmReceiver;->a(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v0

    const/high16 v2, 0x10000000

    invoke-static {p0, v8, v0, v2}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v2

    .line 404
    const-string v0, "alarm"

    invoke-virtual {p0, v0}, Lcom/google/android/gms/common/download/DownloadService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    .line 405
    const/4 v3, 0x3

    invoke-virtual {v0, v3, v4, v5, v2}, Landroid/app/AlarmManager;->set(IJLandroid/app/PendingIntent;)V

    .line 407
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "ds__next_alarm__"

    invoke-interface {v0, v1, v4, v5}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-static {v0}, Lcom/android/a/c;->a(Landroid/content/SharedPreferences$Editor;)V

    .line 409
    :cond_2
    return-void
.end method

.method private static a(Landroid/content/Context;J)Z
    .locals 9

    .prologue
    const/4 v1, 0x0

    .line 697
    const-string v0, "DownloadService"

    invoke-virtual {p0, v0, v1}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v2, "ds__storage_low__"

    invoke-interface {v0, v2, v1}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    .line 698
    if-eqz v0, :cond_0

    move v0, v1

    .line 711
    :goto_0
    return v0

    .line 702
    :cond_0
    new-instance v0, Landroid/os/StatFs;

    invoke-virtual {p0}, Landroid/content/Context;->getCacheDir()Ljava/io/File;

    move-result-object v2

    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2}, Landroid/os/StatFs;-><init>(Ljava/lang/String;)V

    .line 704
    invoke-virtual {v0}, Landroid/os/StatFs;->getFreeBlocks()I

    move-result v2

    int-to-long v2, v2

    invoke-virtual {v0}, Landroid/os/StatFs;->getBlockSize()I

    move-result v4

    int-to-long v4, v4

    mul-long/2addr v2, v4

    .line 705
    sub-long/2addr v2, p1

    long-to-float v2, v2

    .line 706
    const/4 v3, 0x0

    invoke-static {v2, v3}, Ljava/lang/Math;->max(FF)F

    move-result v2

    .line 708
    invoke-virtual {v0}, Landroid/os/StatFs;->getBlockCount()I

    move-result v3

    int-to-long v4, v3

    invoke-virtual {v0}, Landroid/os/StatFs;->getBlockSize()I

    move-result v0

    int-to-long v6, v0

    mul-long/2addr v4, v6

    long-to-float v0, v4

    .line 709
    const/high16 v3, 0x3f800000    # 1.0f

    invoke-static {v0, v3}, Ljava/lang/Math;->max(FF)F

    move-result v0

    .line 711
    div-float/2addr v2, v0

    sget-object v0, Lcom/google/android/gms/common/download/a/a;->d:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    cmpl-float v0, v2, v0

    if-lez v0, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;Lcom/google/android/gms/common/download/f;)Z
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 525
    invoke-static {p0, p1, v0, v0}, Lcom/google/android/gms/common/download/DownloadService;->a(Landroid/content/Context;Lcom/google/android/gms/common/download/f;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public static a(Landroid/content/Context;Lcom/google/android/gms/common/download/f;Ljava/lang/String;Ljava/lang/String;)Z
    .locals 5

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 547
    const-string v2, "DownloadDetails required"

    invoke-static {p1, v2}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 548
    sget-object v2, Lcom/google/android/gms/common/download/DownloadService;->f:Ljava/lang/Object;

    monitor-enter v2

    .line 549
    :try_start_0
    iget-object v3, p1, Lcom/google/android/gms/common/download/f;->a:Ljava/lang/String;

    invoke-static {p0, v3}, Lcom/google/android/gms/common/download/r;->g(Landroid/content/Context;Ljava/lang/String;)Lcom/google/android/gms/common/download/f;

    move-result-object v3

    .line 551
    iget-object v4, p1, Lcom/google/android/gms/common/download/f;->a:Ljava/lang/String;

    invoke-static {p0, v4}, Lcom/google/android/gms/common/download/r;->d(Landroid/content/Context;Ljava/lang/String;)Z

    .line 556
    if-nez v3, :cond_2

    .line 557
    const/4 v3, 0x1

    invoke-static {p0, p1, v3}, Lcom/google/android/gms/common/download/r;->a(Landroid/content/Context;Lcom/google/android/gms/common/download/f;Z)Landroid/net/Uri;

    move-result-object v3

    .line 559
    if-nez v3, :cond_0

    .line 560
    monitor-exit v2

    .line 584
    :goto_0
    return v0

    :cond_0
    move v0, v1

    .line 574
    :goto_1
    if-nez v0, :cond_5

    iget-object v3, p1, Lcom/google/android/gms/common/download/f;->a:Ljava/lang/String;

    invoke-static {p0, v3}, Lcom/google/android/gms/common/download/DownloadService;->a(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_5

    if-eqz p2, :cond_5

    if-eqz p3, :cond_5

    .line 577
    iget-object v0, p1, Lcom/google/android/gms/common/download/f;->a:Ljava/lang/String;

    invoke-static {p0, v0, p2, p3}, Lcom/google/android/gms/common/download/r;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 579
    invoke-static {p0}, Lcom/google/android/gms/common/download/DownloadAlarmReceiver;->a(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 584
    :cond_1
    :goto_2
    monitor-exit v2

    move v0, v1

    goto :goto_0

    .line 564
    :cond_2
    invoke-virtual {v3, p1}, Lcom/google/android/gms/common/download/f;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_3

    move v0, v1

    .line 565
    :cond_3
    if-eqz v0, :cond_4

    .line 566
    const/4 v3, 0x1

    invoke-static {p0, p1, v3}, Lcom/google/android/gms/common/download/r;->b(Landroid/content/Context;Lcom/google/android/gms/common/download/f;Z)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    .line 585
    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0

    .line 569
    :cond_4
    :try_start_1
    iget-object v3, p1, Lcom/google/android/gms/common/download/f;->a:Ljava/lang/String;

    invoke-static {p0, v3}, Lcom/google/android/gms/common/download/r;->e(Landroid/content/Context;Ljava/lang/String;)V

    goto :goto_1

    .line 580
    :cond_5
    if-eqz v0, :cond_1

    .line 582
    invoke-static {p0}, Lcom/google/android/gms/common/download/DownloadAlarmReceiver;->a(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_2
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;)Z
    .locals 2

    .prologue
    .line 471
    sget-object v1, Lcom/google/android/gms/common/download/DownloadService;->f:Ljava/lang/Object;

    monitor-enter v1

    .line 472
    :try_start_0
    invoke-static {p0, p1}, Lcom/google/android/gms/common/download/r;->g(Landroid/content/Context;Ljava/lang/String;)Lcom/google/android/gms/common/download/f;

    move-result-object v0

    .line 473
    if-eqz v0, :cond_0

    .line 474
    invoke-virtual {v0, p0}, Lcom/google/android/gms/common/download/f;->b(Landroid/content/Context;)Z

    move-result v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 476
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    monitor-exit v1

    goto :goto_0

    .line 477
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static b(Landroid/content/Context;Ljava/lang/String;)Ljava/io/File;
    .locals 3

    .prologue
    .line 490
    sget-object v1, Lcom/google/android/gms/common/download/DownloadService;->f:Ljava/lang/Object;

    monitor-enter v1

    .line 491
    :try_start_0
    invoke-static {p0, p1}, Lcom/google/android/gms/common/download/r;->g(Landroid/content/Context;Ljava/lang/String;)Lcom/google/android/gms/common/download/f;

    move-result-object v0

    .line 492
    if-eqz v0, :cond_0

    invoke-virtual {v0, p0}, Lcom/google/android/gms/common/download/f;->b(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 493
    invoke-virtual {v0, p0}, Lcom/google/android/gms/common/download/f;->a(Landroid/content/Context;)Ljava/io/File;

    move-result-object v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 495
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    monitor-exit v1

    goto :goto_0

    .line 497
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method static b(Landroid/content/Context;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 633
    new-instance v0, Ljava/io/File;

    invoke-virtual {p0}, Landroid/content/Context;->getCacheDir()Ljava/io/File;

    move-result-object v1

    const-string v2, "downloadservice"

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private b()V
    .locals 2

    .prologue
    .line 388
    sget-object v0, Lcom/google/android/gms/common/download/a/a;->c:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/common/download/DownloadService;->a(J)V

    .line 389
    return-void
.end method

.method private static b(Landroid/content/Context;Lcom/google/android/gms/common/download/f;)Z
    .locals 4

    .prologue
    const v3, 0x6768a8

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 353
    iget-object v2, p1, Lcom/google/android/gms/common/download/f;->e:Ljava/lang/String;

    if-eqz v2, :cond_2

    iget-object v2, p1, Lcom/google/android/gms/common/download/f;->a:Ljava/lang/String;

    invoke-static {p0, v2}, Lcom/google/android/gms/common/download/r;->d(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 357
    sget-object v0, Lcom/google/android/gms/common/download/a/a;->a:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 358
    const-string v0, "GmsDownloadService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p1, Lcom/google/android/gms/common/download/f;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " not enabled and self managed."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    move v0, v1

    .line 384
    :cond_1
    :goto_0
    return v0

    .line 363
    :cond_2
    iget v2, p1, Lcom/google/android/gms/common/download/f;->f:I

    if-lt v3, v2, :cond_4

    iget v2, p1, Lcom/google/android/gms/common/download/f;->g:I

    if-gt v3, v2, :cond_4

    move v2, v0

    :goto_1
    if-nez v2, :cond_5

    .line 364
    sget-object v0, Lcom/google/android/gms/common/download/a/a;->a:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 365
    const-string v0, "GmsDownloadService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p1, Lcom/google/android/gms/common/download/f;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " not supported on GmsCore version."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_3
    move v0, v1

    .line 367
    goto :goto_0

    :cond_4
    move v2, v1

    .line 363
    goto :goto_1

    .line 370
    :cond_5
    sget-object v2, Lcom/google/android/gms/common/download/DownloadService;->g:Ljava/util/regex/Pattern;

    iget-object v3, p1, Lcom/google/android/gms/common/download/f;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/regex/Matcher;->matches()Z

    move-result v2

    if-nez v2, :cond_7

    .line 371
    sget-object v0, Lcom/google/android/gms/common/download/a/a;->a:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_6

    const-string v0, "GmsDownloadService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p1, Lcom/google/android/gms/common/download/f;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " invalid filename."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_6
    move v0, v1

    .line 372
    goto :goto_0

    .line 375
    :cond_7
    iget-object v2, p1, Lcom/google/android/gms/common/download/f;->b:Ljava/lang/String;

    const-string v3, "https"

    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_9

    .line 376
    sget-object v0, Lcom/google/android/gms/common/download/a/a;->a:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_8

    const-string v0, "GmsDownloadService"

    const-string v2, "Need https."

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_8
    move v0, v1

    .line 377
    goto/16 :goto_0

    .line 380
    :cond_9
    iget-object v2, p1, Lcom/google/android/gms/common/download/f;->e:Ljava/lang/String;

    if-eqz v2, :cond_1

    iget-object v2, p1, Lcom/google/android/gms/common/download/f;->e:Ljava/lang/String;

    const-string v3, ".."

    invoke-virtual {v2, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 381
    sget-object v0, Lcom/google/android/gms/common/download/a/a;->a:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_a

    const-string v0, "GmsDownloadService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p1, Lcom/google/android/gms/common/download/f;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " bad relative path."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_a
    move v0, v1

    .line 382
    goto/16 :goto_0
.end method

.method private declared-synchronized c()V
    .locals 4

    .prologue
    .line 412
    monitor-enter p0

    :try_start_0
    sget-boolean v0, Lcom/google/android/gms/common/download/DownloadService;->b:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_1

    .line 425
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 416
    :cond_1
    :try_start_1
    sget-object v0, Lcom/google/android/gms/common/download/DownloadService;->a:Landroid/os/PowerManager$WakeLock;

    if-eqz v0, :cond_2

    sget-object v0, Lcom/google/android/gms/common/download/DownloadService;->a:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v0

    if-nez v0, :cond_0

    .line 419
    :cond_2
    const-string v0, "power"

    invoke-virtual {p0, v0}, Lcom/google/android/gms/common/download/DownloadService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    .line 420
    const/4 v1, 0x1

    const-string v2, "GmsDownloadService"

    invoke-virtual {v0, v1, v2}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v0

    .line 421
    sput-object v0, Lcom/google/android/gms/common/download/DownloadService;->a:Landroid/os/PowerManager$WakeLock;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/PowerManager$WakeLock;->setReferenceCounted(Z)V

    .line 424
    sget-object v0, Lcom/google/android/gms/common/download/DownloadService;->a:Landroid/os/PowerManager$WakeLock;

    const-wide/32 v2, 0xea60

    invoke-virtual {v0, v2, v3}, Landroid/os/PowerManager$WakeLock;->acquire(J)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 412
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public static c(Landroid/content/Context;Ljava/lang/String;)Z
    .locals 2

    .prologue
    .line 507
    sget-object v1, Lcom/google/android/gms/common/download/DownloadService;->f:Ljava/lang/Object;

    monitor-enter v1

    .line 508
    :try_start_0
    invoke-static {p0, p1}, Lcom/google/android/gms/common/download/r;->d(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return v0

    .line 509
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private d()Ljava/util/Set;
    .locals 6

    .prologue
    .line 606
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    .line 607
    const-string v0, "download"

    invoke-virtual {p0, v0}, Lcom/google/android/gms/common/download/DownloadService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    .line 608
    const-string v0, "download"

    invoke-virtual {p0, v0}, Lcom/google/android/gms/common/download/DownloadService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/DownloadManager;

    .line 609
    new-instance v2, Landroid/app/DownloadManager$Query;

    invoke-direct {v2}, Landroid/app/DownloadManager$Query;-><init>()V

    const/16 v3, 0xf

    invoke-virtual {v2, v3}, Landroid/app/DownloadManager$Query;->setFilterByStatus(I)Landroid/app/DownloadManager$Query;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/app/DownloadManager;->query(Landroid/app/DownloadManager$Query;)Landroid/database/Cursor;

    move-result-object v2

    .line 610
    if-eqz v2, :cond_2

    .line 612
    :try_start_0
    const-string v0, "_id"

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    .line 613
    :cond_0
    :goto_0
    invoke-interface {v2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 614
    invoke-interface {v2, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    invoke-static {p0, v4, v5}, Lcom/google/android/gms/common/download/r;->a(Landroid/content/Context;J)Lcom/google/android/gms/common/download/f;

    move-result-object v3

    .line 616
    if-eqz v3, :cond_0

    .line 617
    iget-object v3, v3, Lcom/google/android/gms/common/download/f;->a:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 621
    :catchall_0
    move-exception v0

    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_1
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    .line 624
    :cond_2
    return-object v1
.end method

.method public static d(Landroid/content/Context;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 600
    sget-object v1, Lcom/google/android/gms/common/download/DownloadService;->f:Ljava/lang/Object;

    monitor-enter v1

    .line 601
    :try_start_0
    invoke-static {p0, p1}, Lcom/google/android/gms/common/download/r;->f(Landroid/content/Context;Ljava/lang/String;)V

    .line 602
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static e(Landroid/content/Context;Ljava/lang/String;)Lcom/google/android/gms/common/api/Status;
    .locals 2

    .prologue
    .line 664
    invoke-static {p0, p1}, Lcom/google/android/gms/common/download/r;->g(Landroid/content/Context;Ljava/lang/String;)Lcom/google/android/gms/common/download/f;

    move-result-object v0

    .line 665
    if-nez v0, :cond_0

    .line 666
    const/16 v0, 0xd

    invoke-static {p0, v0}, Lcom/google/android/gms/common/download/DownloadService;->a(Landroid/content/Context;I)Lcom/google/android/gms/common/api/Status;

    move-result-object v0

    .line 684
    :goto_0
    return-object v0

    .line 668
    :cond_0
    invoke-static {p0, v0}, Lcom/google/android/gms/common/download/DownloadService;->b(Landroid/content/Context;Lcom/google/android/gms/common/download/f;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 669
    const/16 v0, 0x1b63

    invoke-static {p0, v0}, Lcom/google/android/gms/common/download/DownloadService;->a(Landroid/content/Context;I)Lcom/google/android/gms/common/api/Status;

    move-result-object v0

    goto :goto_0

    .line 671
    :cond_1
    iget-object v1, v0, Lcom/google/android/gms/common/download/f;->a:Ljava/lang/String;

    invoke-static {p0, v1}, Lcom/google/android/gms/common/download/DownloadService;->a(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 672
    const/16 v0, 0x1b62

    invoke-static {p0, v0}, Lcom/google/android/gms/common/download/DownloadService;->a(Landroid/content/Context;I)Lcom/google/android/gms/common/api/Status;

    move-result-object v0

    goto :goto_0

    .line 674
    :cond_2
    iget-wide v0, v0, Lcom/google/android/gms/common/download/f;->c:J

    invoke-static {p0, v0, v1}, Lcom/google/android/gms/common/download/DownloadService;->a(Landroid/content/Context;J)Z

    move-result v0

    if-nez v0, :cond_3

    .line 675
    const/16 v0, 0x1b64

    invoke-static {p0, v0}, Lcom/google/android/gms/common/download/DownloadService;->a(Landroid/content/Context;I)Lcom/google/android/gms/common/api/Status;

    move-result-object v0

    goto :goto_0

    .line 678
    :cond_3
    invoke-static {p0, p1}, Lcom/google/android/gms/common/download/r;->h(Landroid/content/Context;Ljava/lang/String;)I

    move-result v0

    .line 679
    and-int/lit8 v0, v0, 0x7

    if-eqz v0, :cond_4

    .line 682
    const/16 v0, 0x1b59

    invoke-static {p0, v0}, Lcom/google/android/gms/common/download/DownloadService;->a(Landroid/content/Context;I)Lcom/google/android/gms/common/api/Status;

    move-result-object v0

    goto :goto_0

    .line 684
    :cond_4
    const/16 v0, 0x1b58

    invoke-static {p0, v0}, Lcom/google/android/gms/common/download/DownloadService;->a(Landroid/content/Context;I)Lcom/google/android/gms/common/api/Status;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method protected onHandleIntent(Landroid/content/Intent;)V
    .locals 16

    .prologue
    const-wide/16 v10, 0x0

    const/16 v2, 0xb

    const/4 v3, 0x1

    const/4 v5, 0x0

    .line 117
    invoke-static {v2}, Lcom/google/android/gms/common/util/ao;->a(I)Z

    move-result v2

    if-nez v2, :cond_1

    .line 118
    sget-object v2, Lcom/google/android/gms/common/download/a/a;->a:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v2}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_0

    const-string v2, "GmsDownloadService"

    const-string v3, "Not supported on Gingerbread or lower."

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 325
    :cond_0
    :goto_0
    return-void

    .line 123
    :cond_1
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v6

    .line 124
    sget-object v2, Lcom/google/android/gms/common/download/a/a;->a:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v2}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_2

    const-string v2, "GmsDownloadService"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v8, "onHandleIntent: "

    invoke-direct {v4, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 126
    :cond_2
    const-string v2, "DownloadService"

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v5}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v4

    .line 127
    const-string v2, "boot"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_3

    const-string v2, "ds__last_process__"

    invoke-interface {v4, v2, v10, v11}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v8

    cmp-long v2, v8, v6

    if-gtz v2, :cond_3

    const-string v2, "ds__last_reset__"

    invoke-interface {v4, v2, v10, v11}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v8

    sub-long v8, v6, v8

    sget-object v2, Lcom/google/android/gms/common/download/a/a;->f:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v2}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v10

    cmp-long v2, v8, v10

    if-lez v2, :cond_23

    .line 131
    :cond_3
    invoke-interface {v4}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    const-string v8, "ds__next_alarm__"

    invoke-interface {v2, v8}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    const-string v8, "ds__last_reset__"

    invoke-interface {v2, v8, v6, v7}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    invoke-static {v2}, Lcom/android/a/c;->a(Landroid/content/SharedPreferences$Editor;)V

    .line 137
    :goto_1
    sget-object v2, Lcom/google/android/gms/common/download/a/a;->b:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v2}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-nez v2, :cond_4

    .line 138
    invoke-direct/range {p0 .. p0}, Lcom/google/android/gms/common/download/DownloadService;->b()V

    .line 139
    sget-object v2, Lcom/google/android/gms/common/download/a/a;->a:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v2}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_0

    const-string v2, "GmsDownloadService"

    const-string v3, "Not Enabled. Ending"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 144
    :cond_4
    const-string v2, "ds__downloads_json__"

    invoke-interface {v4, v2}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 146
    :try_start_0
    invoke-direct/range {p0 .. p0}, Lcom/google/android/gms/common/download/DownloadService;->a()V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 152
    invoke-interface {v4}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    const-string v8, "ds__downloads_json__"

    invoke-interface {v2, v8}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 157
    :cond_5
    :goto_2
    invoke-static/range {p0 .. p0}, Lcom/google/android/gms/common/util/ak;->a(Landroid/content/Context;)Z

    move-result v2

    if-nez v2, :cond_6

    .line 158
    sget-wide v2, Lcom/google/android/gms/common/download/DownloadService;->c:J

    move-object/from16 v0, p0

    invoke-direct {v0, v2, v3}, Lcom/google/android/gms/common/download/DownloadService;->a(J)V

    .line 159
    sget-object v2, Lcom/google/android/gms/common/download/a/a;->a:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v2}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_0

    const-string v2, "GmsDownloadService"

    const-string v3, "No network. Retry in 30 minutes."

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 149
    :catch_0
    move-exception v2

    :try_start_1
    const-string v2, "GmsDownloadService"

    const-string v8, "Failed to migrate from SharedPreferences."

    invoke-static {v2, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 152
    invoke-interface {v4}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    const-string v8, "ds__downloads_json__"

    invoke-interface {v2, v8}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->apply()V

    goto :goto_2

    :catchall_0
    move-exception v2

    invoke-interface {v4}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v3

    const-string v4, "ds__downloads_json__"

    invoke-interface {v3, v4}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v3

    invoke-interface {v3}, Landroid/content/SharedPreferences$Editor;->apply()V

    throw v2

    .line 163
    :cond_6
    sget-object v8, Lcom/google/android/gms/common/download/DownloadService;->e:Ljava/lang/Object;

    monitor-enter v8

    .line 164
    :try_start_2
    sget-boolean v2, Lcom/google/android/gms/common/download/DownloadService;->h:Z

    if-eqz v2, :cond_8

    .line 165
    sget-object v2, Lcom/google/android/gms/common/download/a/a;->a:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v2}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_7

    const-string v2, "GmsDownloadService"

    const-string v3, "Already processing. Don\'t need another thread"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 166
    :cond_7
    monitor-exit v8
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    goto/16 :goto_0

    .line 171
    :catchall_1
    move-exception v2

    monitor-exit v8

    throw v2

    .line 168
    :cond_8
    const/4 v2, 0x1

    :try_start_3
    sput-boolean v2, Lcom/google/android/gms/common/download/DownloadService;->h:Z

    .line 169
    invoke-interface {v4}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    const-string v4, "ds__last_process__"

    invoke-interface {v2, v4, v6, v7}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    invoke-static {v2}, Lcom/android/a/c;->a(Landroid/content/SharedPreferences$Editor;)V

    .line 171
    monitor-exit v8
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 173
    sget-object v2, Lcom/google/android/gms/common/download/a/a;->a:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v2}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_9

    const-string v2, "GmsDownloadService"

    const-string v4, "processing."

    invoke-static {v2, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 176
    :cond_9
    invoke-direct/range {p0 .. p0}, Lcom/google/android/gms/common/download/DownloadService;->c()V

    .line 180
    :try_start_4
    new-instance v6, Ljava/util/TreeSet;

    invoke-direct {v6}, Ljava/util/TreeSet;-><init>()V

    .line 181
    new-instance v7, Ljava/util/LinkedList;

    invoke-direct {v7}, Ljava/util/LinkedList;-><init>()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_3

    .line 185
    :try_start_5
    new-instance v4, Lorg/json/JSONArray;

    sget-object v2, Lcom/google/android/gms/common/download/a/a;->g:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v2}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-direct {v4, v2}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V
    :try_end_5
    .catch Lorg/json/JSONException; {:try_start_5 .. :try_end_5} :catch_1
    .catchall {:try_start_5 .. :try_end_5} :catchall_3

    .line 197
    :try_start_6
    invoke-direct/range {p0 .. p0}, Lcom/google/android/gms/common/download/DownloadService;->d()Ljava/util/Set;

    move-result-object v8

    .line 204
    invoke-static/range {p0 .. p0}, Lcom/google/android/gms/common/download/r;->a(Landroid/content/Context;)[Lcom/google/android/gms/common/download/f;

    move-result-object v9

    .line 205
    array-length v10, v9

    move v4, v5

    :goto_3
    if-ge v4, v10, :cond_14

    aget-object v11, v9, v4

    .line 206
    move-object/from16 v0, p0

    invoke-static {v0, v11}, Lcom/google/android/gms/common/download/DownloadService;->b(Landroid/content/Context;Lcom/google/android/gms/common/download/f;)Z

    move-result v2

    if-nez v2, :cond_d

    .line 207
    sget-object v2, Lcom/google/android/gms/common/download/a/a;->a:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v2}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_a

    const-string v2, "GmsDownloadService"

    new-instance v12, Ljava/lang/StringBuilder;

    const-string v13, "Download invalid: "

    invoke-direct {v12, v13}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v11, v11, Lcom/google/android/gms/common/download/f;->a:Ljava/lang/String;

    invoke-virtual {v12, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v2, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 205
    :cond_a
    :goto_4
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    goto :goto_3

    .line 188
    :catch_1
    move-exception v2

    invoke-direct/range {p0 .. p0}, Lcom/google/android/gms/common/download/DownloadService;->b()V

    .line 189
    sget-object v3, Lcom/google/android/gms/common/download/DownloadService;->e:Ljava/lang/Object;

    monitor-enter v3
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_3

    .line 190
    :try_start_7
    sget-object v2, Lcom/google/android/gms/common/download/a/a;->a:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v2}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_b

    const-string v2, "GmsDownloadService"

    const-string v4, "Bad Json"

    invoke-static {v2, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 191
    :cond_b
    const/4 v2, 0x0

    sput-boolean v2, Lcom/google/android/gms/common/download/DownloadService;->h:Z

    .line 192
    monitor-exit v3
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 317
    invoke-direct/range {p0 .. p0}, Lcom/google/android/gms/common/download/DownloadService;->b()V

    .line 318
    sget-object v3, Lcom/google/android/gms/common/download/DownloadService;->e:Ljava/lang/Object;

    monitor-enter v3

    .line 319
    const/4 v2, 0x0

    :try_start_8
    sput-boolean v2, Lcom/google/android/gms/common/download/DownloadService;->h:Z

    .line 320
    monitor-exit v3
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_4

    .line 321
    sget-boolean v2, Lcom/google/android/gms/common/download/DownloadService;->b:Z

    if-nez v2, :cond_0

    .line 322
    sget-object v2, Lcom/google/android/gms/common/download/DownloadService;->a:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v2}, Landroid/os/PowerManager$WakeLock;->release()V

    goto/16 :goto_0

    .line 192
    :catchall_2
    move-exception v2

    :try_start_9
    monitor-exit v3

    throw v2
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_3

    .line 317
    :catchall_3
    move-exception v2

    invoke-direct/range {p0 .. p0}, Lcom/google/android/gms/common/download/DownloadService;->b()V

    .line 318
    sget-object v3, Lcom/google/android/gms/common/download/DownloadService;->e:Ljava/lang/Object;

    monitor-enter v3

    .line 319
    const/4 v4, 0x0

    :try_start_a
    sput-boolean v4, Lcom/google/android/gms/common/download/DownloadService;->h:Z

    .line 320
    monitor-exit v3
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_9

    .line 321
    sget-boolean v3, Lcom/google/android/gms/common/download/DownloadService;->b:Z

    if-nez v3, :cond_c

    .line 322
    sget-object v3, Lcom/google/android/gms/common/download/DownloadService;->a:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v3}, Landroid/os/PowerManager$WakeLock;->release()V

    :cond_c
    throw v2

    .line 320
    :catchall_4
    move-exception v2

    monitor-exit v3

    throw v2

    .line 211
    :cond_d
    :try_start_b
    iget-object v2, v11, Lcom/google/android/gms/common/download/f;->a:Ljava/lang/String;

    invoke-interface {v6, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 213
    iget-object v2, v11, Lcom/google/android/gms/common/download/f;->a:Ljava/lang/String;

    invoke-interface {v8, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_e

    .line 214
    sget-object v2, Lcom/google/android/gms/common/download/a/a;->a:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v2}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_a

    .line 215
    const-string v2, "GmsDownloadService"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v11, v11, Lcom/google/android/gms/common/download/f;->a:Ljava/lang/String;

    invoke-virtual {v12, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, " is already in DownloadManager."

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v2, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_4

    .line 220
    :cond_e
    iget-object v2, v11, Lcom/google/android/gms/common/download/f;->a:Ljava/lang/String;

    move-object/from16 v0, p0

    invoke-static {v0, v2}, Lcom/google/android/gms/common/download/r;->b(Landroid/content/Context;Ljava/lang/String;)I

    move-result v12

    sget-object v2, Lcom/google/android/gms/common/download/a/a;->e:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v2}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    if-le v12, v2, :cond_10

    .line 222
    sget-object v2, Lcom/google/android/gms/common/download/a/a;->a:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v2}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_f

    const-string v2, "GmsDownloadService"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v13, v11, Lcom/google/android/gms/common/download/f;->a:Ljava/lang/String;

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, " is at retry limit."

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v2, v12}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 223
    :cond_f
    if-eqz v3, :cond_a

    .line 224
    iget-object v2, v11, Lcom/google/android/gms/common/download/f;->a:Ljava/lang/String;

    move-object/from16 v0, p0

    invoke-static {v0, v2}, Lcom/google/android/gms/common/download/r;->c(Landroid/content/Context;Ljava/lang/String;)V

    .line 230
    :cond_10
    iget-object v2, v11, Lcom/google/android/gms/common/download/f;->a:Ljava/lang/String;

    move-object/from16 v0, p0

    invoke-static {v0, v2}, Lcom/google/android/gms/common/download/DownloadService;->a(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_11

    .line 231
    sget-object v2, Lcom/google/android/gms/common/download/a/a;->a:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v2}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_a

    .line 232
    const-string v2, "GmsDownloadService"

    new-instance v12, Ljava/lang/StringBuilder;

    const-string v13, "File "

    invoke-direct {v12, v13}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v11, v11, Lcom/google/android/gms/common/download/f;->a:Ljava/lang/String;

    invoke-virtual {v12, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, " is already downloaded."

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v2, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_4

    .line 235
    :cond_11
    iget-wide v12, v11, Lcom/google/android/gms/common/download/f;->c:J

    move-object/from16 v0, p0

    invoke-static {v0, v12, v13}, Lcom/google/android/gms/common/download/DownloadService;->a(Landroid/content/Context;J)Z

    move-result v2

    if-nez v2, :cond_12

    .line 236
    sget-object v2, Lcom/google/android/gms/common/download/a/a;->a:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v2}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_a

    .line 237
    const-string v2, "GmsDownloadService"

    new-instance v12, Ljava/lang/StringBuilder;

    const-string v13, "File "

    invoke-direct {v12, v13}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v13, v11, Lcom/google/android/gms/common/download/f;->a:Ljava/lang/String;

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, " is too large to download with current space constraints; file size="

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    iget-wide v14, v11, Lcom/google/android/gms/common/download/f;->c:J

    invoke-virtual {v12, v14, v15}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, " bytes"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v2, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_4

    .line 242
    :cond_12
    sget-object v2, Lcom/google/android/gms/common/download/a/a;->a:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v2}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_13

    .line 243
    const-string v2, "GmsDownloadService"

    new-instance v12, Ljava/lang/StringBuilder;

    const-string v13, "File "

    invoke-direct {v12, v13}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v13, v11, Lcom/google/android/gms/common/download/f;->a:Ljava/lang/String;

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, " needs downloading."

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v2, v12}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 245
    :cond_13
    invoke-interface {v7, v11}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_4

    .line 250
    :cond_14
    invoke-interface {v7}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_5
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_19

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    move-object v0, v2

    check-cast v0, Lcom/google/android/gms/common/download/f;

    move-object v4, v0

    .line 251
    sget-object v2, Lcom/google/android/gms/common/download/a/a;->a:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v2}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_15

    const-string v2, "GmsDownloadService"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v8, "Enqueuing "

    invoke-direct {v3, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v8, v4, Lcom/google/android/gms/common/download/f;->a:Ljava/lang/String;

    invoke-virtual {v3, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_3

    .line 253
    :cond_15
    :try_start_c
    const-string v2, "download"

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/google/android/gms/common/download/DownloadService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/app/DownloadManager;

    new-instance v3, Landroid/app/DownloadManager$Request;

    iget-object v8, v4, Lcom/google/android/gms/common/download/f;->b:Ljava/lang/String;

    invoke-static {v8}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v8

    invoke-direct {v3, v8}, Landroid/app/DownloadManager$Request;-><init>(Landroid/net/Uri;)V

    iget-object v8, v4, Lcom/google/android/gms/common/download/f;->a:Ljava/lang/String;

    invoke-virtual {v3, v8}, Landroid/app/DownloadManager$Request;->setTitle(Ljava/lang/CharSequence;)Landroid/app/DownloadManager$Request;

    move-result-object v3

    const/4 v8, 0x0

    invoke-virtual {v3, v8}, Landroid/app/DownloadManager$Request;->setVisibleInDownloadsUi(Z)Landroid/app/DownloadManager$Request;

    move-result-object v3

    const/4 v8, 0x0

    invoke-virtual {v3, v8}, Landroid/app/DownloadManager$Request;->setAllowedOverRoaming(Z)Landroid/app/DownloadManager$Request;

    move-result-object v8

    iget-object v3, v4, Lcom/google/android/gms/common/download/f;->a:Ljava/lang/String;

    move-object/from16 v0, p0

    invoke-static {v0, v3}, Lcom/google/android/gms/common/download/r;->i(Landroid/content/Context;Ljava/lang/String;)Landroid/util/Pair;

    move-result-object v9

    if-nez v9, :cond_17

    const/16 v3, 0xb

    invoke-static {v3}, Lcom/google/android/gms/common/util/ao;->a(I)Z

    move-result v3

    if-eqz v3, :cond_16

    const/4 v3, 0x2

    invoke-virtual {v8, v3}, Landroid/app/DownloadManager$Request;->setNotificationVisibility(I)Landroid/app/DownloadManager$Request;

    :goto_6
    iget-object v3, v4, Lcom/google/android/gms/common/download/f;->a:Ljava/lang/String;

    move-object/from16 v0, p0

    invoke-static {v0, v3}, Lcom/google/android/gms/common/download/r;->j(Landroid/content/Context;Ljava/lang/String;)V

    invoke-virtual {v2, v8}, Landroid/app/DownloadManager;->enqueue(Landroid/app/DownloadManager$Request;)J

    move-result-wide v2

    iget-object v4, v4, Lcom/google/android/gms/common/download/f;->a:Ljava/lang/String;

    move-object/from16 v0, p0

    invoke-static {v0, v4, v2, v3}, Lcom/google/android/gms/common/download/r;->a(Landroid/content/Context;Ljava/lang/String;J)V
    :try_end_c
    .catch Ljava/lang/IllegalArgumentException; {:try_start_c .. :try_end_c} :catch_2
    .catchall {:try_start_c .. :try_end_c} :catchall_3

    goto :goto_5

    :catch_2
    move-exception v2

    .line 317
    invoke-direct/range {p0 .. p0}, Lcom/google/android/gms/common/download/DownloadService;->b()V

    .line 318
    sget-object v3, Lcom/google/android/gms/common/download/DownloadService;->e:Ljava/lang/Object;

    monitor-enter v3

    .line 319
    const/4 v2, 0x0

    :try_start_d
    sput-boolean v2, Lcom/google/android/gms/common/download/DownloadService;->h:Z

    .line 320
    monitor-exit v3
    :try_end_d
    .catchall {:try_start_d .. :try_end_d} :catchall_5

    .line 321
    sget-boolean v2, Lcom/google/android/gms/common/download/DownloadService;->b:Z

    if-nez v2, :cond_0

    .line 322
    sget-object v2, Lcom/google/android/gms/common/download/DownloadService;->a:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v2}, Landroid/os/PowerManager$WakeLock;->release()V

    goto/16 :goto_0

    .line 253
    :cond_16
    const/4 v3, 0x0

    :try_start_e
    invoke-virtual {v8, v3}, Landroid/app/DownloadManager$Request;->setShowRunningNotification(Z)Landroid/app/DownloadManager$Request;

    goto :goto_6

    :cond_17
    iget-object v3, v9, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v3, Ljava/lang/CharSequence;

    invoke-virtual {v8, v3}, Landroid/app/DownloadManager$Request;->setTitle(Ljava/lang/CharSequence;)Landroid/app/DownloadManager$Request;

    iget-object v3, v9, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v3, Ljava/lang/CharSequence;

    invoke-virtual {v8, v3}, Landroid/app/DownloadManager$Request;->setDescription(Ljava/lang/CharSequence;)Landroid/app/DownloadManager$Request;

    const/16 v3, 0xb

    invoke-static {v3}, Lcom/google/android/gms/common/util/ao;->a(I)Z

    move-result v3

    if-eqz v3, :cond_18

    const/4 v3, 0x0

    invoke-virtual {v8, v3}, Landroid/app/DownloadManager$Request;->setNotificationVisibility(I)Landroid/app/DownloadManager$Request;

    goto :goto_6

    :cond_18
    const/4 v3, 0x1

    invoke-virtual {v8, v3}, Landroid/app/DownloadManager$Request;->setShowRunningNotification(Z)Landroid/app/DownloadManager$Request;
    :try_end_e
    .catch Ljava/lang/IllegalArgumentException; {:try_start_e .. :try_end_e} :catch_2
    .catchall {:try_start_e .. :try_end_e} :catchall_3

    goto :goto_6

    .line 320
    :catchall_5
    move-exception v2

    monitor-exit v3

    throw v2

    .line 259
    :cond_19
    :try_start_f
    new-instance v3, Ljava/io/File;

    invoke-static/range {p0 .. p0}, Lcom/google/android/gms/common/download/DownloadService;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v3, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 263
    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_1c

    invoke-virtual {v3}, Ljava/io/File;->isDirectory()Z

    move-result v2

    if-eqz v2, :cond_1c

    .line 264
    invoke-virtual {v3}, Ljava/io/File;->list()[Ljava/lang/String;

    move-result-object v4

    array-length v7, v4

    :goto_7
    if-ge v5, v7, :cond_1c

    aget-object v8, v4, v5

    .line 265
    invoke-interface {v6, v8}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1b

    .line 266
    sget-object v2, Lcom/google/android/gms/common/download/a/a;->a:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v2}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_1a

    const-string v2, "GmsDownloadService"

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "deleting unused file: "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v2, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 267
    :cond_1a
    new-instance v2, Ljava/io/File;

    invoke-direct {v2, v3, v8}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 268
    invoke-virtual {v2}, Ljava/io/File;->delete()Z

    .line 264
    :cond_1b
    add-int/lit8 v5, v5, 0x1

    goto :goto_7

    .line 274
    :cond_1c
    const-string v2, "download"

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/google/android/gms/common/download/DownloadService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/app/DownloadManager;

    .line 275
    new-instance v3, Landroid/app/DownloadManager$Query;

    invoke-direct {v3}, Landroid/app/DownloadManager$Query;-><init>()V

    const/16 v4, 0x10

    invoke-virtual {v3, v4}, Landroid/app/DownloadManager$Query;->setFilterByStatus(I)Landroid/app/DownloadManager$Query;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/app/DownloadManager;->query(Landroid/app/DownloadManager$Query;)Landroid/database/Cursor;
    :try_end_f
    .catchall {:try_start_f .. :try_end_f} :catchall_3

    move-result-object v3

    .line 277
    if-eqz v3, :cond_1e

    .line 279
    :goto_8
    :try_start_10
    invoke-interface {v3}, Landroid/database/Cursor;->moveToNext()Z

    move-result v4

    if-eqz v4, :cond_1d

    .line 280
    const-string v4, "_id"

    invoke-interface {v3, v4}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v4

    invoke-interface {v3, v4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    .line 281
    const-string v6, "title"

    invoke-interface {v3, v6}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v6

    invoke-interface {v3, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 283
    move-object/from16 v0, p0

    invoke-static {v0, v6}, Lcom/google/android/gms/common/download/r;->a(Landroid/content/Context;Ljava/lang/String;)V

    .line 284
    const/4 v6, 0x1

    new-array v6, v6, [J

    const/4 v7, 0x0

    aput-wide v4, v6, v7

    invoke-virtual {v2, v6}, Landroid/app/DownloadManager;->remove([J)I
    :try_end_10
    .catchall {:try_start_10 .. :try_end_10} :catchall_6

    goto :goto_8

    .line 287
    :catchall_6
    move-exception v2

    :try_start_11
    invoke-interface {v3}, Landroid/database/Cursor;->close()V

    throw v2

    :cond_1d
    invoke-interface {v3}, Landroid/database/Cursor;->close()V

    .line 291
    :cond_1e
    new-instance v3, Landroid/app/DownloadManager$Query;

    invoke-direct {v3}, Landroid/app/DownloadManager$Query;-><init>()V

    const/16 v4, 0x8

    invoke-virtual {v3, v4}, Landroid/app/DownloadManager$Query;->setFilterByStatus(I)Landroid/app/DownloadManager$Query;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/app/DownloadManager;->query(Landroid/app/DownloadManager$Query;)Landroid/database/Cursor;
    :try_end_11
    .catchall {:try_start_11 .. :try_end_11} :catchall_3

    move-result-object v3

    .line 293
    if-eqz v3, :cond_21

    .line 295
    :goto_9
    :try_start_12
    invoke-interface {v3}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_20

    .line 296
    const-string v2, "_id"

    invoke-interface {v3, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v3, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    .line 297
    const-string v2, "title"

    invoke-interface {v3, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v3, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 299
    move-object/from16 v0, p0

    invoke-static {v0, v2}, Lcom/google/android/gms/common/download/DownloadService;->a(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1f

    .line 301
    move-object/from16 v0, p0

    invoke-static {v0, v4, v5}, Lcom/google/android/gms/common/download/r;->b(Landroid/content/Context;J)V
    :try_end_12
    .catchall {:try_start_12 .. :try_end_12} :catchall_7

    goto :goto_9

    .line 310
    :catchall_7
    move-exception v2

    :try_start_13
    invoke-interface {v3}, Landroid/database/Cursor;->close()V

    throw v2
    :try_end_13
    .catchall {:try_start_13 .. :try_end_13} :catchall_3

    .line 304
    :cond_1f
    :try_start_14
    new-instance v2, Landroid/content/Intent;

    const-class v6, Lcom/google/android/gms/common/download/DownloadCompleteIntentService;

    move-object/from16 v0, p0

    invoke-direct {v2, v0, v6}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 305
    const-string v6, "extra_download_id"

    invoke-virtual {v2, v6, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 306
    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/google/android/gms/common/download/DownloadService;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;
    :try_end_14
    .catchall {:try_start_14 .. :try_end_14} :catchall_7

    goto :goto_9

    .line 310
    :cond_20
    :try_start_15
    invoke-interface {v3}, Landroid/database/Cursor;->close()V

    .line 315
    :cond_21
    sget-object v2, Lcom/google/android/gms/common/download/a/a;->a:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v2}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_22

    const-string v2, "GmsDownloadService"

    const-string v3, "finished."

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_15
    .catchall {:try_start_15 .. :try_end_15} :catchall_3

    .line 317
    :cond_22
    invoke-direct/range {p0 .. p0}, Lcom/google/android/gms/common/download/DownloadService;->b()V

    .line 318
    sget-object v3, Lcom/google/android/gms/common/download/DownloadService;->e:Ljava/lang/Object;

    monitor-enter v3

    .line 319
    const/4 v2, 0x0

    :try_start_16
    sput-boolean v2, Lcom/google/android/gms/common/download/DownloadService;->h:Z

    .line 320
    monitor-exit v3
    :try_end_16
    .catchall {:try_start_16 .. :try_end_16} :catchall_8

    .line 321
    sget-boolean v2, Lcom/google/android/gms/common/download/DownloadService;->b:Z

    if-nez v2, :cond_0

    .line 322
    sget-object v2, Lcom/google/android/gms/common/download/DownloadService;->a:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v2}, Landroid/os/PowerManager$WakeLock;->release()V

    goto/16 :goto_0

    .line 320
    :catchall_8
    move-exception v2

    monitor-exit v3

    throw v2

    :catchall_9
    move-exception v2

    monitor-exit v3

    throw v2

    :cond_23
    move v3, v5

    goto/16 :goto_1
.end method
