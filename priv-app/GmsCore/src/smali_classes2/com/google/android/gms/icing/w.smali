.class public final Lcom/google/android/gms/icing/w;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# static fields
.field private static volatile c:[Lcom/google/android/gms/icing/w;


# instance fields
.field public a:Ljava/lang/String;

.field public b:D


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 7505
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 7506
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/gms/icing/w;->a:Ljava/lang/String;

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/gms/icing/w;->b:D

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/icing/w;->cachedSize:I

    .line 7507
    return-void
.end method

.method public static a()[Lcom/google/android/gms/icing/w;
    .locals 2

    .prologue
    .line 7488
    sget-object v0, Lcom/google/android/gms/icing/w;->c:[Lcom/google/android/gms/icing/w;

    if-nez v0, :cond_1

    .line 7489
    sget-object v1, Lcom/google/protobuf/nano/h;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 7491
    :try_start_0
    sget-object v0, Lcom/google/android/gms/icing/w;->c:[Lcom/google/android/gms/icing/w;

    if-nez v0, :cond_0

    .line 7492
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/android/gms/icing/w;

    sput-object v0, Lcom/google/android/gms/icing/w;->c:[Lcom/google/android/gms/icing/w;

    .line 7494
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 7496
    :cond_1
    sget-object v0, Lcom/google/android/gms/icing/w;->c:[Lcom/google/android/gms/icing/w;

    return-object v0

    .line 7494
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 6

    .prologue
    .line 7568
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->computeSerializedSize()I

    move-result v0

    .line 7569
    iget-object v1, p0, Lcom/google/android/gms/icing/w;->a:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 7570
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/gms/icing/w;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 7573
    :cond_0
    iget-wide v2, p0, Lcom/google/android/gms/icing/w;->b:D

    invoke-static {v2, v3}, Ljava/lang/Double;->doubleToLongBits(D)J

    move-result-wide v2

    const-wide/16 v4, 0x0

    invoke-static {v4, v5}, Ljava/lang/Double;->doubleToLongBits(D)J

    move-result-wide v4

    cmp-long v1, v2, v4

    if-eqz v1, :cond_1

    .line 7575
    const/4 v1, 0x2

    iget-wide v2, p0, Lcom/google/android/gms/icing/w;->b:D

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x8

    add-int/2addr v0, v1

    .line 7578
    :cond_1
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 7518
    if-ne p1, p0, :cond_1

    .line 7538
    :cond_0
    :goto_0
    return v0

    .line 7521
    :cond_1
    instance-of v2, p1, Lcom/google/android/gms/icing/w;

    if-nez v2, :cond_2

    move v0, v1

    .line 7522
    goto :goto_0

    .line 7524
    :cond_2
    check-cast p1, Lcom/google/android/gms/icing/w;

    .line 7525
    iget-object v2, p0, Lcom/google/android/gms/icing/w;->a:Ljava/lang/String;

    if-nez v2, :cond_3

    .line 7526
    iget-object v2, p1, Lcom/google/android/gms/icing/w;->a:Ljava/lang/String;

    if-eqz v2, :cond_4

    move v0, v1

    .line 7527
    goto :goto_0

    .line 7529
    :cond_3
    iget-object v2, p0, Lcom/google/android/gms/icing/w;->a:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/android/gms/icing/w;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    move v0, v1

    .line 7530
    goto :goto_0

    .line 7533
    :cond_4
    iget-wide v2, p0, Lcom/google/android/gms/icing/w;->b:D

    invoke-static {v2, v3}, Ljava/lang/Double;->doubleToLongBits(D)J

    move-result-wide v2

    .line 7534
    iget-wide v4, p1, Lcom/google/android/gms/icing/w;->b:D

    invoke-static {v4, v5}, Ljava/lang/Double;->doubleToLongBits(D)J

    move-result-wide v4

    cmp-long v2, v2, v4

    if-eqz v2, :cond_0

    move v0, v1

    .line 7535
    goto :goto_0
.end method

.method public final hashCode()I
    .locals 6

    .prologue
    .line 7543
    iget-object v0, p0, Lcom/google/android/gms/icing/w;->a:Ljava/lang/String;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    .line 7547
    iget-wide v2, p0, Lcom/google/android/gms/icing/w;->b:D

    invoke-static {v2, v3}, Ljava/lang/Double;->doubleToLongBits(D)J

    move-result-wide v2

    .line 7548
    mul-int/lit8 v0, v0, 0x1f

    const/16 v1, 0x20

    ushr-long v4, v2, v1

    xor-long/2addr v2, v4

    long-to-int v1, v2

    add-int/2addr v0, v1

    .line 7550
    return v0

    .line 7543
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/icing/w;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_0
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 2

    .prologue
    .line 7482
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/icing/w;->a:Ljava/lang/String;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->l()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Double;->longBitsToDouble(J)D

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/gms/icing/w;->b:D

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x11 -> :sswitch_2
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 4

    .prologue
    .line 7556
    iget-object v0, p0, Lcom/google/android/gms/icing/w;->a:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 7557
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/gms/icing/w;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 7559
    :cond_0
    iget-wide v0, p0, Lcom/google/android/gms/icing/w;->b:D

    invoke-static {v0, v1}, Ljava/lang/Double;->doubleToLongBits(D)J

    move-result-wide v0

    const-wide/16 v2, 0x0

    invoke-static {v2, v3}, Ljava/lang/Double;->doubleToLongBits(D)J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-eqz v0, :cond_1

    .line 7561
    const/4 v0, 0x2

    iget-wide v2, p0, Lcom/google/android/gms/icing/w;->b:D

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/b;->a(ID)V

    .line 7563
    :cond_1
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 7564
    return-void
.end method
