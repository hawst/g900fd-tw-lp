.class public final Lcom/google/android/gms/games/ui/client/main/ClientSettingsActivity;
.super Lcom/google/android/gms/games/ui/client/a;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# static fields
.field private static final p:I


# instance fields
.field private n:Ljava/lang/String;

.field private o:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 65
    sget v0, Lcom/google/android/gms/l;->aF:I

    sput v0, Lcom/google/android/gms/games/ui/client/main/ClientSettingsActivity;->p:I

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 68
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/gms/games/ui/client/a;-><init>(I)V

    .line 69
    return-void
.end method

.method private U()V
    .locals 1

    .prologue
    .line 310
    sget v0, Lcom/google/android/gms/j;->jm:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/client/main/ClientSettingsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 311
    invoke-direct {p0, v0}, Lcom/google/android/gms/games/ui/client/main/ClientSettingsActivity;->b(Landroid/view/ViewGroup;)V

    .line 312
    return-void
.end method

.method private V()V
    .locals 1

    .prologue
    .line 321
    sget v0, Lcom/google/android/gms/j;->rA:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/client/main/ClientSettingsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 322
    invoke-direct {p0, v0}, Lcom/google/android/gms/games/ui/client/main/ClientSettingsActivity;->b(Landroid/view/ViewGroup;)V

    .line 323
    return-void
.end method

.method private a(Landroid/view/ViewGroup;ILjava/lang/String;)V
    .locals 3

    .prologue
    .line 257
    invoke-virtual {p1, p0}, Landroid/view/ViewGroup;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 260
    sget v0, Lcom/google/android/gms/j;->rq:I

    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 263
    sget v1, Lcom/google/android/gms/j;->rn:I

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 265
    invoke-virtual {v1, p2}, Landroid/widget/TextView;->setText(I)V

    .line 266
    sget v2, Lcom/google/android/gms/j;->rr:I

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 267
    sget v2, Lcom/google/android/gms/j;->rp:I

    invoke-virtual {p1, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .line 269
    invoke-static {v1}, Lcom/google/android/gms/games/ui/d/ap;->a(Landroid/view/View;)V

    .line 270
    invoke-static {v0}, Lcom/google/android/gms/games/ui/d/ap;->a(Landroid/view/View;)V

    .line 271
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, Landroid/view/ViewGroup;->setEnabled(Z)V

    .line 272
    invoke-virtual {v0, p3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 273
    const/16 v0, 0x8

    invoke-virtual {v2, v0}, Landroid/view/View;->setVisibility(I)V

    .line 274
    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/games/ui/client/main/ClientSettingsActivity;)V
    .locals 3

    .prologue
    .line 40
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/client/main/ClientSettingsActivity;->w()Lcom/google/android/gms/common/api/v;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/common/api/v;->f()Z

    move-result v1

    if-eqz v1, :cond_0

    sget v1, Lcom/google/android/gms/p;->hY:I

    invoke-virtual {p0, v1}, Lcom/google/android/gms/games/ui/client/main/ClientSettingsActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/gms/games/ui/b/f;->a(Ljava/lang/String;)Lcom/google/android/gms/games/ui/b/f;

    move-result-object v1

    const-string v2, "com.google.android.gms.games.ui.dialog.progressDialog"

    invoke-static {p0, v1, v2}, Lcom/google/android/gms/games/ui/d/a;->a(Landroid/support/v4/app/q;Landroid/support/v4/app/m;Ljava/lang/String;)V

    invoke-static {v0}, Lcom/google/android/gms/games/d;->e(Lcom/google/android/gms/common/api/v;)Lcom/google/android/gms/common/api/am;

    move-result-object v0

    new-instance v1, Lcom/google/android/gms/games/ui/client/main/a;

    invoke-direct {v1, p0}, Lcom/google/android/gms/games/ui/client/main/a;-><init>(Lcom/google/android/gms/games/ui/client/main/ClientSettingsActivity;)V

    invoke-interface {v0, v1}, Lcom/google/android/gms/common/api/am;->a(Lcom/google/android/gms/common/api/aq;)V

    :goto_0
    return-void

    :cond_0
    const-string v0, "ClientSettingsActivity"

    const-string v1, "Sign-out failed, GoogleApiClient not connected."

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private b(Landroid/view/ViewGroup;)V
    .locals 5

    .prologue
    const/4 v4, 0x4

    const/4 v3, 0x0

    .line 282
    invoke-virtual {p1, p0}, Landroid/view/ViewGroup;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 285
    sget v0, Lcom/google/android/gms/j;->rq:I

    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 288
    sget v1, Lcom/google/android/gms/j;->rn:I

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 290
    sget v2, Lcom/google/android/gms/j;->rr:I

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 291
    sget v2, Lcom/google/android/gms/j;->rp:I

    invoke-virtual {p1, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .line 293
    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 294
    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 295
    invoke-virtual {p1, v3}, Landroid/view/ViewGroup;->setEnabled(Z)V

    .line 296
    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    .line 297
    return-void
.end method


# virtual methods
.method protected final N()I
    .locals 1

    .prologue
    .line 189
    const/4 v0, 0x5

    return v0
.end method

.method public final T()V
    .locals 1

    .prologue
    .line 412
    const-string v0, "com.google.android.gms.games.ui.dialog.progressDialog"

    invoke-static {p0, v0}, Lcom/google/android/gms/games/ui/d/a;->dismiss(Landroid/support/v4/app/q;Ljava/lang/String;)V

    .line 413
    const/16 v0, 0x2711

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/client/main/ClientSettingsActivity;->setResult(I)V

    .line 414
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/client/main/ClientSettingsActivity;->finish()V

    .line 415
    return-void
.end method

.method public final a(Lcom/google/android/gms/common/c;)V
    .locals 2

    .prologue
    .line 196
    invoke-virtual {p1}, Lcom/google/android/gms/common/c;->c()I

    move-result v0

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    .line 197
    const/16 v0, 0x2711

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/client/main/ClientSettingsActivity;->setResult(I)V

    .line 198
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/client/main/ClientSettingsActivity;->finish()V

    .line 202
    :goto_0
    return-void

    .line 201
    :cond_0
    invoke-super {p0, p1}, Lcom/google/android/gms/games/ui/client/a;->a(Lcom/google/android/gms/common/c;)V

    goto :goto_0
.end method

.method public final b_(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 163
    invoke-super {p0, p1}, Lcom/google/android/gms/games/ui/client/a;->b_(Landroid/os/Bundle;)V

    .line 165
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/client/main/ClientSettingsActivity;->w()Lcom/google/android/gms/common/api/v;

    move-result-object v0

    .line 168
    iget-object v1, p0, Lcom/google/android/gms/games/ui/client/main/ClientSettingsActivity;->n:Ljava/lang/String;

    if-nez v1, :cond_0

    .line 169
    invoke-static {v0}, Lcom/google/android/gms/games/d;->c(Lcom/google/android/gms/common/api/v;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/gms/games/ui/client/main/ClientSettingsActivity;->n:Ljava/lang/String;

    .line 174
    :cond_0
    invoke-static {v0}, Lcom/google/android/gms/games/d;->b(Lcom/google/android/gms/common/api/v;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/client/main/ClientSettingsActivity;->o:Ljava/lang/String;

    .line 175
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/main/ClientSettingsActivity;->o:Ljava/lang/String;

    if-nez v0, :cond_1

    .line 177
    const-string v0, "ClientSettingsActivity"

    const-string v1, "We don\'t have a current account name, something went wrong. Finishing the activity"

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 179
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/client/main/ClientSettingsActivity;->finish()V

    .line 185
    :goto_0
    return-void

    .line 183
    :cond_1
    sget v0, Lcom/google/android/gms/j;->jm:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/client/main/ClientSettingsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    sget v1, Lcom/google/android/gms/p;->hS:I

    iget-object v2, p0, Lcom/google/android/gms/games/ui/client/main/ClientSettingsActivity;->o:Ljava/lang/String;

    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/gms/games/ui/client/main/ClientSettingsActivity;->a(Landroid/view/ViewGroup;ILjava/lang/String;)V

    .line 184
    sget v0, Lcom/google/android/gms/j;->rA:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/client/main/ClientSettingsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    sget v1, Lcom/google/android/gms/p;->hU:I

    const/4 v2, 0x0

    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/gms/games/ui/client/main/ClientSettingsActivity;->a(Landroid/view/ViewGroup;ILjava/lang/String;)V

    goto :goto_0
.end method

.method public final f()Z
    .locals 1

    .prologue
    .line 73
    const/4 v0, 0x1

    return v0
.end method

.method public final onActivityResult(IILandroid/content/Intent;)V
    .locals 1

    .prologue
    .line 421
    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    .line 422
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/client/main/ClientSettingsActivity;->U()V

    .line 423
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/client/main/ClientSettingsActivity;->V()V

    .line 424
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/client/main/ClientSettingsActivity;->w()Lcom/google/android/gms/common/api/v;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/common/api/v;->e()V

    .line 430
    :goto_0
    return-void

    .line 429
    :cond_0
    invoke-super {p0, p1, p2, p3}, Lcom/google/android/gms/games/ui/client/a;->onActivityResult(IILandroid/content/Intent;)V

    goto :goto_0
.end method

.method public final onClick(Landroid/view/View;)V
    .locals 8

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x0

    const/4 v5, 0x1

    .line 328
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    .line 329
    sget v1, Lcom/google/android/gms/j;->jm:I

    if-ne v0, v1, :cond_2

    .line 330
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/client/main/ClientSettingsActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/client/main/ClientSettingsActivity;->w()Lcom/google/android/gms/common/api/v;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/gms/games/d;->b(Lcom/google/android/gms/common/api/v;)Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_1

    const-string v0, "ClientSettingsActivity"

    const-string v1, "We don\'t have a current account name, something went wrong. Finishing the activity"

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->d(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/client/main/ClientSettingsActivity;->finish()V

    .line 334
    :cond_0
    :goto_0
    return-void

    .line 330
    :cond_1
    sget v2, Lcom/google/android/gms/p;->hX:I

    invoke-static {p0, v2}, Lcom/google/android/gms/games/ui/d/al;->a(Landroid/content/Context;I)Landroid/view/View;

    move-result-object v2

    sget v3, Lcom/google/android/gms/p;->hW:I

    new-array v4, v5, [Ljava/lang/Object;

    aput-object v1, v4, v6

    invoke-virtual {v0, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Lcom/google/android/gms/games/ui/client/main/b;

    invoke-direct {v1, p0, v6}, Lcom/google/android/gms/games/ui/client/main/b;-><init>(Lcom/google/android/gms/games/ui/client/main/ClientSettingsActivity;B)V

    new-instance v3, Landroid/app/AlertDialog$Builder;

    invoke-direct {v3, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v3, v2}, Landroid/app/AlertDialog$Builder;->setCustomTitle(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    const v3, 0x104000a

    invoke-virtual {v2, v3, v1}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    const/high16 v3, 0x1040000

    invoke-virtual {v2, v3, v1}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    invoke-static {p0, v0}, Lcom/google/android/gms/games/ui/d/al;->a(Landroid/content/Context;Landroid/app/Dialog;)V

    goto :goto_0

    .line 331
    :cond_2
    sget v1, Lcom/google/android/gms/j;->rA:I

    if-ne v0, v1, :cond_0

    .line 332
    new-instance v0, Landroid/accounts/Account;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/client/main/ClientSettingsActivity;->o:Ljava/lang/String;

    const-string v2, "com.google"

    invoke-direct {v0, v1, v2}, Landroid/accounts/Account;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/gms/games/ui/client/main/ClientSettingsActivity;->n:Ljava/lang/String;

    invoke-static {v1}, Lcom/google/android/gms/plus/j;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/games/ui/client/main/ClientSettingsActivity;->i:Ljava/lang/String;

    new-instance v3, Landroid/content/Intent;

    const-string v4, "com.google.android.gms.plus.action.MANAGE_APP"

    invoke-direct {v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v4, "com.google.android.gms.plus.ACCOUNT"

    invoke-virtual {v3, v4, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const-string v0, "com.google.android.gms.plus.APP_ID"

    invoke-virtual {v3, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v0, "com.google.android.gms.plus.APP_PACKAGE"

    invoke-virtual {v3, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v0, "com.google.android.gms.plus.APP_NAME"

    invoke-virtual {v3, v0, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v0, "com.google.android.gms.plus.APP_ICON_URL"

    invoke-virtual {v3, v0, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v0, "com.google.android.gms.plus.APP_IS_ASPEN"

    invoke-virtual {v3, v0, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    invoke-virtual {p0, v3, v5}, Lcom/google/android/gms/games/ui/client/main/ClientSettingsActivity;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 8

    .prologue
    const/16 v3, 0x8

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 108
    invoke-super {p0, p1}, Lcom/google/android/gms/games/ui/client/a;->onCreate(Landroid/os/Bundle;)V

    .line 109
    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/client/main/ClientSettingsActivity;->setResult(I)V

    .line 112
    sget v0, Lcom/google/android/gms/j;->oJ:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/client/main/ClientSettingsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 113
    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    .line 114
    invoke-virtual {p0, p0}, Lcom/google/android/gms/games/ui/client/main/ClientSettingsActivity;->a(Landroid/content/Context;)I

    move-result v2

    iput v2, v1, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 115
    invoke-virtual {v0, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 118
    const-string v0, "com.google.android.gms"

    invoke-static {p0, v0}, Lcom/google/android/gms/games/ui/d/aa;->b(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 120
    sget v0, Lcom/google/android/gms/j;->jk:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/client/main/ClientSettingsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    sget v1, Lcom/google/android/gms/j;->rp:I

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    sget v1, Lcom/google/android/gms/j;->ro:I

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    sget v3, Lcom/google/android/gms/p;->il:I

    new-array v4, v7, [Ljava/lang/Object;

    sget v5, Lcom/google/android/gms/p;->eh:I

    invoke-virtual {p0, v5}, Lcom/google/android/gms/games/ui/client/main/ClientSettingsActivity;->getString(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-virtual {p0, v3, v4}, Lcom/google/android/gms/games/ui/client/main/ClientSettingsActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    sget v1, Lcom/google/android/gms/j;->rn:I

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {v0, v6}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 125
    :goto_0
    sget v0, Lcom/google/android/gms/p;->hR:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/client/main/ClientSettingsActivity;->setTitle(I)V

    .line 126
    iput-boolean v6, p0, Lcom/google/android/gms/games/ui/client/main/ClientSettingsActivity;->l:Z

    .line 130
    sget v0, Lcom/google/android/gms/j;->jm:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/client/main/ClientSettingsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    sget v1, Lcom/google/android/gms/j;->rq:I

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    sget v1, Lcom/google/android/gms/j;->ro:I

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    sget v1, Lcom/google/android/gms/p;->hT:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    sget v0, Lcom/google/android/gms/j;->rA:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/client/main/ClientSettingsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    sget v1, Lcom/google/android/gms/j;->ro:I

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    sget v1, Lcom/google/android/gms/p;->hV:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 133
    iput-boolean v7, p0, Lcom/google/android/gms/games/ui/client/main/ClientSettingsActivity;->m:Z

    .line 138
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/client/main/ClientSettingsActivity;->U()V

    .line 139
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/client/main/ClientSettingsActivity;->V()V

    .line 140
    return-void

    .line 120
    :cond_0
    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->setVisibility(I)V

    goto :goto_0
.end method

.method public final onResume()V
    .locals 0

    .prologue
    .line 151
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/client/main/ClientSettingsActivity;->w()Lcom/google/android/gms/common/api/v;

    .line 153
    invoke-super {p0}, Lcom/google/android/gms/games/ui/client/a;->onResume()V

    .line 158
    return-void
.end method

.method public final onStart()V
    .locals 0

    .prologue
    .line 144
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/client/main/ClientSettingsActivity;->w()Lcom/google/android/gms/common/api/v;

    .line 146
    invoke-super {p0}, Lcom/google/android/gms/games/ui/client/a;->onStart()V

    .line 147
    return-void
.end method

.method protected final u()I
    .locals 1

    .prologue
    .line 78
    sget v0, Lcom/google/android/gms/games/ui/client/main/ClientSettingsActivity;->p:I

    return v0
.end method
