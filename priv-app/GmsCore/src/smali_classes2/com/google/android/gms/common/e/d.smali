.class public final Lcom/google/android/gms/common/e/d;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public a:[Ljava/lang/String;

.field private b:Ljava/lang/StringBuilder;


# direct methods
.method private constructor <init>(Ljava/lang/String;I)V
    .locals 3

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    new-instance v0, Ljava/lang/StringBuilder;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/gms/common/e/d;->b:Ljava/lang/StringBuilder;

    .line 25
    new-array v0, p2, [Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/gms/common/e/d;->a:[Ljava/lang/String;

    .line 26
    return-void
.end method

.method public static a(Ljava/lang/String;Ljava/util/Collection;)Lcom/google/android/gms/common/e/d;
    .locals 1

    .prologue
    .line 38
    invoke-interface {p1}, Ljava/util/Collection;->toArray()[Ljava/lang/Object;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/google/android/gms/common/e/d;->a(Ljava/lang/String;[Ljava/lang/Object;)Lcom/google/android/gms/common/e/d;

    move-result-object v0

    return-object v0
.end method

.method public static varargs a(Ljava/lang/String;[Ljava/lang/Object;)Lcom/google/android/gms/common/e/d;
    .locals 5

    .prologue
    .line 51
    new-instance v2, Lcom/google/android/gms/common/e/d;

    array-length v0, p1

    invoke-direct {v2, p0, v0}, Lcom/google/android/gms/common/e/d;-><init>(Ljava/lang/String;I)V

    .line 52
    const/4 v0, 0x0

    array-length v3, p1

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    .line 53
    iget-object v4, v2, Lcom/google/android/gms/common/e/d;->b:Ljava/lang/StringBuilder;

    if-nez v1, :cond_0

    const-string v0, "?"

    :goto_1
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 54
    iget-object v0, v2, Lcom/google/android/gms/common/e/d;->a:[Ljava/lang/String;

    aget-object v4, p1, v1

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v0, v1

    .line 52
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 53
    :cond_0
    const-string v0, ",?"

    goto :goto_1

    .line 56
    :cond_1
    return-object v2
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 2

    .prologue
    .line 125
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/google/android/gms/common/e/d;->b:Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
