.class public final Lcom/google/android/gms/wallet/shared/BrokerAndRelationships;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field a:Ljava/lang/String;

.field b:[Ljava/lang/String;

.field private final c:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 21
    new-instance v0, Lcom/google/android/gms/wallet/shared/c;

    invoke-direct {v0}, Lcom/google/android/gms/wallet/shared/c;-><init>()V

    sput-object v0, Lcom/google/android/gms/wallet/shared/BrokerAndRelationships;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 62
    const/4 v0, 0x1

    invoke-direct {p0, v0, v1, v1}, Lcom/google/android/gms/wallet/shared/BrokerAndRelationships;-><init>(ILjava/lang/String;[Ljava/lang/String;)V

    .line 63
    return-void
.end method

.method constructor <init>(ILjava/lang/String;[Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 55
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 56
    iput p1, p0, Lcom/google/android/gms/wallet/shared/BrokerAndRelationships;->c:I

    .line 57
    iput-object p2, p0, Lcom/google/android/gms/wallet/shared/BrokerAndRelationships;->a:Ljava/lang/String;

    .line 58
    if-eqz p3, :cond_0

    :goto_0
    iput-object p3, p0, Lcom/google/android/gms/wallet/shared/BrokerAndRelationships;->b:[Ljava/lang/String;

    .line 59
    return-void

    .line 58
    :cond_0
    const/4 v0, 0x0

    new-array p3, v0, [Ljava/lang/String;

    goto :goto_0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 32
    iget v0, p0, Lcom/google/android/gms/wallet/shared/BrokerAndRelationships;->c:I

    return v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 69
    iget-object v0, p0, Lcom/google/android/gms/wallet/shared/BrokerAndRelationships;->a:Ljava/lang/String;

    return-object v0
.end method

.method public final c()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 76
    iget-object v0, p0, Lcom/google/android/gms/wallet/shared/BrokerAndRelationships;->b:[Ljava/lang/String;

    return-object v0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 37
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 0

    .prologue
    .line 42
    invoke-static {p0, p1}, Lcom/google/android/gms/wallet/shared/c;->a(Lcom/google/android/gms/wallet/shared/BrokerAndRelationships;Landroid/os/Parcel;)V

    .line 43
    return-void
.end method
