.class public final Lcom/google/android/gms/games/a/aw;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method public static a(Lcom/google/android/gms/games/a/au;Ljava/lang/String;JLjava/lang/String;)Ljava/lang/String;
    .locals 4

    .prologue
    .line 36
    sget-object v0, Lcom/google/android/gms/games/c/a;->U:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/google/android/gms/games/c/a;->Q:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Double;

    invoke-virtual {v0}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmpl-double v0, v0, v2

    if-eqz v0, :cond_0

    invoke-static {}, Ljava/lang/Math;->random()D

    move-result-wide v2

    sget-object v0, Lcom/google/android/gms/games/c/a;->Q:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Double;

    invoke-virtual {v0}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v0

    cmpg-double v0, v2, v0

    if-gtz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    if-nez v0, :cond_1

    .line 37
    const-string v0, ""

    .line 51
    :goto_1
    return-object v0

    .line 36
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 39
    :cond_1
    new-instance v0, Lcom/google/android/gms/games/g/aw;

    invoke-direct {v0}, Lcom/google/android/gms/games/g/aw;-><init>()V

    .line 40
    new-instance v1, Lcom/google/android/gms/games/g/av;

    invoke-direct {v1}, Lcom/google/android/gms/games/g/av;-><init>()V

    iput-object v1, v0, Lcom/google/android/gms/games/g/aw;->a:Lcom/google/android/gms/games/g/av;

    .line 41
    iget-object v1, v0, Lcom/google/android/gms/games/g/aw;->a:Lcom/google/android/gms/games/g/av;

    iget-object v2, p0, Lcom/google/android/gms/games/a/au;->d:Ljava/lang/String;

    iput-object v2, v1, Lcom/google/android/gms/games/g/av;->c:Ljava/lang/String;

    .line 42
    iget-object v1, v0, Lcom/google/android/gms/games/g/aw;->a:Lcom/google/android/gms/games/g/av;

    iput-object p1, v1, Lcom/google/android/gms/games/g/av;->b:Ljava/lang/String;

    .line 43
    iget-object v1, v0, Lcom/google/android/gms/games/g/aw;->a:Lcom/google/android/gms/games/g/av;

    iget-object v2, p0, Lcom/google/android/gms/games/a/au;->b:Lcom/google/android/gms/common/server/ClientContext;

    invoke-virtual {v2}, Lcom/google/android/gms/common/server/ClientContext;->d()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/google/android/gms/games/g/av;->d:Ljava/lang/String;

    .line 44
    iget-object v1, v0, Lcom/google/android/gms/games/g/aw;->a:Lcom/google/android/gms/games/g/av;

    iput-wide p2, v1, Lcom/google/android/gms/games/g/av;->a:J

    .line 45
    iget-object v1, v0, Lcom/google/android/gms/games/g/aw;->a:Lcom/google/android/gms/games/g/av;

    if-nez p4, :cond_2

    const-string p4, ""

    :cond_2
    iput-object p4, v1, Lcom/google/android/gms/games/g/av;->e:Ljava/lang/String;

    .line 47
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 48
    const-string v2, "SIGNED_DATA"

    invoke-static {v0}, Lcom/google/android/gms/games/g/aw;->toByteArray(Lcom/google/protobuf/nano/j;)[B

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/common/util/m;->b([B)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 50
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/games/a/au;->a:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/gms/games/a/au;->b:Lcom/google/android/gms/common/server/ClientContext;

    invoke-virtual {v2}, Lcom/google/android/gms/common/server/ClientContext;->d()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/google/android/gms/common/util/e;->e(Landroid/content/Context;Ljava/lang/String;)[B

    move-result-object v0

    if-eqz v0, :cond_3

    const-string v2, "PACKAGE_SIGNATURE"

    invoke-static {v0}, Lcom/google/android/gms/common/util/m;->b([B)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 51
    :cond_3
    :goto_2
    iget-object v0, p0, Lcom/google/android/gms/games/a/au;->a:Landroid/content/Context;

    const-string v2, "playGamesScore"

    invoke-static {v0, v2, v1}, Lcom/google/android/gms/droidguard/a;->a(Landroid/content/Context;Ljava/lang/String;Ljava/util/Map;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 50
    :catch_0
    move-exception v0

    const-string v2, "GamesDroidGuard"

    invoke-virtual {v0}, Landroid/content/pm/PackageManager$NameNotFoundException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/google/android/gms/games/internal/dq;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2
.end method
