.class public Lcom/google/android/gms/auth/trustagent/GoogleTrustAgentFirstNotificationActivity;
.super Landroid/app/Activity;
.source "SourceFile"


# instance fields
.field private a:Landroid/os/Handler;

.field private b:Z

.field private c:Ljava/util/concurrent/FutureTask;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/auth/trustagent/GoogleTrustAgentFirstNotificationActivity;)Z
    .locals 1

    .prologue
    .line 18
    iget-boolean v0, p0, Lcom/google/android/gms/auth/trustagent/GoogleTrustAgentFirstNotificationActivity;->b:Z

    return v0
.end method

.method static synthetic b(Lcom/google/android/gms/auth/trustagent/GoogleTrustAgentFirstNotificationActivity;)Z
    .locals 1

    .prologue
    .line 18
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/auth/trustagent/GoogleTrustAgentFirstNotificationActivity;->b:Z

    return v0
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 25
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 26
    sget v0, Lcom/google/android/gms/l;->F:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/trustagent/GoogleTrustAgentFirstNotificationActivity;->setContentView(I)V

    .line 27
    iput-boolean v4, p0, Lcom/google/android/gms/auth/trustagent/GoogleTrustAgentFirstNotificationActivity;->b:Z

    .line 29
    sget v0, Lcom/google/android/gms/j;->bq:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/trustagent/GoogleTrustAgentFirstNotificationActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 31
    new-instance v1, Lcom/google/android/gms/auth/trustagent/n;

    invoke-direct {v1, p0}, Lcom/google/android/gms/auth/trustagent/n;-><init>(Lcom/google/android/gms/auth/trustagent/GoogleTrustAgentFirstNotificationActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 39
    invoke-virtual {p0}, Lcom/google/android/gms/auth/trustagent/GoogleTrustAgentFirstNotificationActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 40
    const-string v1, "type"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 41
    const-string v2, "name"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 42
    sget v0, Lcom/google/android/gms/j;->br:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/trustagent/GoogleTrustAgentFirstNotificationActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 44
    if-eqz v1, :cond_1

    const-class v3, Lcom/google/android/gms/auth/trustagent/trustlet/d;

    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 45
    sget v1, Lcom/google/android/gms/p;->cy:I

    new-array v3, v5, [Ljava/lang/Object;

    aput-object v2, v3, v4

    invoke-virtual {p0, v1, v3}, Lcom/google/android/gms/auth/trustagent/GoogleTrustAgentFirstNotificationActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 51
    :cond_0
    :goto_0
    return-void

    .line 47
    :cond_1
    if-eqz v1, :cond_0

    const-class v3, Lcom/google/android/gms/auth/trustagent/trustlet/aw;

    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 48
    sget v1, Lcom/google/android/gms/p;->cA:I

    new-array v3, v5, [Ljava/lang/Object;

    aput-object v2, v3, v4

    invoke-virtual {p0, v1, v3}, Lcom/google/android/gms/auth/trustagent/GoogleTrustAgentFirstNotificationActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method protected onPause()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 79
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    .line 80
    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/GoogleTrustAgentFirstNotificationActivity;->c:Ljava/util/concurrent/FutureTask;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/FutureTask;->cancel(Z)Z

    .line 81
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/auth/trustagent/GoogleTrustAgentFirstNotificationActivity;->c:Ljava/util/concurrent/FutureTask;

    .line 82
    iget-boolean v0, p0, Lcom/google/android/gms/auth/trustagent/GoogleTrustAgentFirstNotificationActivity;->b:Z

    if-eqz v0, :cond_0

    .line 83
    invoke-virtual {p0}, Lcom/google/android/gms/auth/trustagent/GoogleTrustAgentFirstNotificationActivity;->finish()V

    .line 84
    invoke-virtual {p0, v2, v2}, Lcom/google/android/gms/auth/trustagent/GoogleTrustAgentFirstNotificationActivity;->overridePendingTransition(II)V

    .line 86
    :cond_0
    return-void
.end method

.method protected onResume()V
    .locals 4

    .prologue
    .line 55
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 56
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/auth/trustagent/GoogleTrustAgentFirstNotificationActivity;->a:Landroid/os/Handler;

    .line 57
    new-instance v0, Ljava/util/concurrent/FutureTask;

    new-instance v1, Lcom/google/android/gms/auth/trustagent/o;

    invoke-direct {v1, p0}, Lcom/google/android/gms/auth/trustagent/o;-><init>(Lcom/google/android/gms/auth/trustagent/GoogleTrustAgentFirstNotificationActivity;)V

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Ljava/util/concurrent/FutureTask;-><init>(Ljava/lang/Runnable;Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/google/android/gms/auth/trustagent/GoogleTrustAgentFirstNotificationActivity;->c:Ljava/util/concurrent/FutureTask;

    .line 74
    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/GoogleTrustAgentFirstNotificationActivity;->a:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/gms/auth/trustagent/GoogleTrustAgentFirstNotificationActivity;->c:Ljava/util/concurrent/FutureTask;

    const-wide/16 v2, 0x1f4

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 75
    return-void
.end method
