.class public Lcom/google/android/gms/car/CarService;
.super Landroid/app/Service;
.source "SourceFile"


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x15
.end annotation


# instance fields
.field private a:Lcom/google/android/gms/car/gx;

.field private b:Lcom/google/android/gms/car/gv;

.field private c:Lcom/google/android/gms/car/gu;

.field private d:Landroid/content/BroadcastReceiver;

.field private e:Lcom/google/android/gms/car/no;

.field private f:Landroid/app/UiModeManager;

.field private g:Landroid/content/res/Configuration;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 41
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 301
    return-void
.end method

.method private static a(Landroid/content/res/Configuration;)Landroid/content/res/Configuration;
    .locals 1

    .prologue
    .line 228
    iget v0, p0, Landroid/content/res/Configuration;->uiMode:I

    and-int/lit8 v0, v0, -0x10

    iput v0, p0, Landroid/content/res/Configuration;->uiMode:I

    .line 230
    iget v0, p0, Landroid/content/res/Configuration;->uiMode:I

    add-int/lit8 v0, v0, 0x3

    iput v0, p0, Landroid/content/res/Configuration;->uiMode:I

    .line 231
    return-object p0
.end method

.method static synthetic a(Lcom/google/android/gms/car/CarService;)Lcom/google/android/gms/car/gx;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lcom/google/android/gms/car/CarService;->a:Lcom/google/android/gms/car/gx;

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/gms/common/internal/bg;ILandroid/os/IBinder;)V
    .locals 2

    .prologue
    .line 41
    const/4 v0, 0x0

    :try_start_0
    invoke-interface {p0, p1, p2, v0}, Lcom/google/android/gms/common/internal/bg;->a(ILandroid/os/IBinder;Landroid/os/Bundle;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v0, "CAR.SERVICE"

    const-string v1, "client died while brokering service"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method static synthetic a(Landroid/os/Bundle;)Z
    .locals 2

    .prologue
    .line 41
    const-string v0, "client_name"

    invoke-virtual {p0, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "car-1-0"

    const-string v1, "client_name"

    invoke-virtual {p0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic b(Lcom/google/android/gms/car/CarService;)Lcom/google/android/gms/car/no;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lcom/google/android/gms/car/CarService;->e:Lcom/google/android/gms/car/no;

    return-object v0
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 155
    const-string v0, "CAR.SERVICE"

    const/4 v1, 0x2

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 156
    const-string v0, "CAR.SERVICE"

    const-string v1, "requestStop"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 158
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/car/CarService;->stopSelf()V

    .line 159
    return-void
.end method

.method final b()V
    .locals 3

    .prologue
    .line 163
    new-instance v0, Landroid/content/IntentFilter;

    const-string v1, "android.intent.action.SCREEN_OFF"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 164
    const-string v1, "android.intent.action.SCREEN_ON"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 165
    invoke-static {}, Lcom/google/android/gms/car/ie;->a()Z

    move-result v1

    if-nez v1, :cond_0

    .line 166
    iget-object v1, p0, Lcom/google/android/gms/car/CarService;->a:Lcom/google/android/gms/car/gx;

    invoke-virtual {v1}, Lcom/google/android/gms/car/gx;->E()Landroid/content/BroadcastReceiver;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/gms/car/CarService;->d:Landroid/content/BroadcastReceiver;

    .line 167
    iget-object v1, p0, Lcom/google/android/gms/car/CarService;->d:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v1, v0}, Lcom/google/android/gms/car/CarService;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 169
    :cond_0
    const-string v0, "CAR.SERVICE"

    const/4 v1, 0x2

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "CAR.SERVICE"

    const-string v1, "make foreground service"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/gms/car/CarService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "com.google.android.gms.car.START_CAR_MODE"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    const/4 v1, 0x0

    const/high16 v2, 0x8000000

    invoke-static {p0, v1, v0, v2}, Landroid/app/PendingIntent;->getService(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/gms/car/CarService;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    new-instance v2, Landroid/app/Notification$Builder;

    invoke-direct {v2, v1}, Landroid/app/Notification$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v2, v0}, Landroid/app/Notification$Builder;->setContentIntent(Landroid/app/PendingIntent;)Landroid/app/Notification$Builder;

    move-result-object v0

    sget v2, Lcom/google/android/gms/p;->dE:I

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/app/Notification$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v0

    sget v2, Lcom/google/android/gms/p;->dG:I

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/Notification$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v0

    sget v1, Lcom/google/android/gms/h;->u:I

    invoke-virtual {v0, v1}, Landroid/app/Notification$Builder;->setSmallIcon(I)Landroid/app/Notification$Builder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/gms/car/CarService;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/google/android/gms/f;->e:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/app/Notification$Builder;->setColor(I)Landroid/app/Notification$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Notification$Builder;->build()Landroid/app/Notification;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {p0, v1, v0}, Lcom/google/android/gms/car/CarService;->startForeground(ILandroid/app/Notification;)V

    .line 170
    return-void
.end method

.method final c()V
    .locals 2

    .prologue
    .line 174
    iget-object v0, p0, Lcom/google/android/gms/car/CarService;->d:Landroid/content/BroadcastReceiver;

    if-eqz v0, :cond_1

    .line 175
    const-string v0, "CAR.SERVICE"

    const/4 v1, 0x2

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 176
    const-string v0, "CAR.SERVICE"

    const-string v1, "Unregistering ScreenOffReceiver"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 178
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/car/CarService;->d:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/car/CarService;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 179
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/car/CarService;->d:Landroid/content/BroadcastReceiver;

    .line 181
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/gms/car/CarService;->d()V

    .line 182
    return-void
.end method

.method final d()V
    .locals 2

    .prologue
    .line 206
    const-string v0, "CAR.SERVICE"

    const/4 v1, 0x2

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 207
    const-string v0, "CAR.SERVICE"

    const-string v1, "make background service"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 209
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/gms/car/CarService;->stopForeground(Z)V

    .line 210
    return-void
.end method

.method protected dump(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 214
    const-string v0, "android.permission.DUMP"

    invoke-virtual {p0, v0}, Lcom/google/android/gms/car/CarService;->checkCallingOrSelfPermission(Ljava/lang/String;)I

    move-result v0

    if-eqz v0, :cond_1

    .line 216
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Permission Denial: can\'t dump CarService from from pid="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", uid="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 224
    :cond_0
    :goto_0
    return-void

    .line 221
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/car/CarService;->a:Lcom/google/android/gms/car/gx;

    if-eqz v0, :cond_0

    .line 222
    iget-object v0, p0, Lcom/google/android/gms/car/CarService;->a:Lcom/google/android/gms/car/gx;

    invoke-virtual {v0, p2}, Lcom/google/android/gms/car/gx;->a(Ljava/io/PrintWriter;)V

    goto :goto_0
.end method

.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 2

    .prologue
    .line 135
    const-string v0, "com.google.android.gms.car.service.START"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 136
    iget-object v0, p0, Lcom/google/android/gms/car/CarService;->c:Lcom/google/android/gms/car/gu;

    invoke-virtual {v0}, Lcom/google/android/gms/car/gu;->asBinder()Landroid/os/IBinder;

    move-result-object v0

    .line 138
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 277
    const/4 v1, 0x0

    .line 278
    iget-object v2, p0, Lcom/google/android/gms/car/CarService;->f:Landroid/app/UiModeManager;

    invoke-virtual {v2}, Landroid/app/UiModeManager;->getNightMode()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    :cond_0
    move v0, v1

    .line 291
    :goto_0
    if-eqz v0, :cond_1

    .line 292
    const-string v0, "CAR.SERVICE"

    const-string v1, "New Config and UiModeManager out of sync!"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 294
    :cond_1
    invoke-super {p0, p1}, Landroid/app/Service;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 295
    new-instance v0, Landroid/content/res/Configuration;

    invoke-direct {v0, p1}, Landroid/content/res/Configuration;-><init>(Landroid/content/res/Configuration;)V

    .line 296
    invoke-static {v0}, Lcom/google/android/gms/car/CarService;->a(Landroid/content/res/Configuration;)Landroid/content/res/Configuration;

    .line 297
    iget-object v1, p0, Lcom/google/android/gms/car/CarService;->g:Landroid/content/res/Configuration;

    invoke-virtual {v1, v0}, Landroid/content/res/Configuration;->updateFrom(Landroid/content/res/Configuration;)I

    move-result v1

    .line 298
    iget-object v2, p0, Lcom/google/android/gms/car/CarService;->a:Lcom/google/android/gms/car/gx;

    invoke-virtual {v2, v0, v1}, Lcom/google/android/gms/car/gx;->a(Landroid/content/res/Configuration;I)V

    .line 299
    return-void

    .line 280
    :pswitch_0
    iget v2, p1, Landroid/content/res/Configuration;->uiMode:I

    and-int/lit8 v2, v2, 0x10

    if-nez v2, :cond_0

    goto :goto_0

    .line 286
    :pswitch_1
    iget v2, p1, Landroid/content/res/Configuration;->uiMode:I

    and-int/lit8 v2, v2, 0x20

    if-nez v2, :cond_0

    goto :goto_0

    .line 278
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onCreate()V
    .locals 2

    .prologue
    .line 61
    const-string v0, "CAR.SERVICE"

    const/4 v1, 0x2

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 62
    const-string v0, "CAR.SERVICE"

    const-string v1, "onCreate"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 64
    :cond_0
    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    .line 66
    new-instance v0, Lcom/google/android/gms/car/id;

    invoke-virtual {p0}, Lcom/google/android/gms/car/CarService;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/gms/car/id;-><init>(Landroid/content/Context;)V

    .line 67
    new-instance v1, Lcom/google/android/gms/car/gx;

    invoke-direct {v1, p0, v0}, Lcom/google/android/gms/car/gx;-><init>(Lcom/google/android/gms/car/CarService;Lcom/google/android/gms/car/id;)V

    iput-object v1, p0, Lcom/google/android/gms/car/CarService;->a:Lcom/google/android/gms/car/gx;

    .line 70
    invoke-static {p0}, Lcom/google/android/gms/car/ia;->a(Landroid/content/Context;)Lcom/google/android/gms/car/ia;

    .line 72
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 73
    const-string v1, "android.hardware.usb.action.USB_ACCESSORY_DETACHED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 74
    new-instance v1, Lcom/google/android/gms/car/gv;

    invoke-direct {v1, p0}, Lcom/google/android/gms/car/gv;-><init>(Lcom/google/android/gms/car/CarService;)V

    iput-object v1, p0, Lcom/google/android/gms/car/CarService;->b:Lcom/google/android/gms/car/gv;

    .line 75
    iget-object v1, p0, Lcom/google/android/gms/car/CarService;->b:Lcom/google/android/gms/car/gv;

    invoke-virtual {p0, v1, v0}, Lcom/google/android/gms/car/CarService;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 77
    new-instance v0, Lcom/google/android/gms/car/gu;

    invoke-direct {v0, p0, p0}, Lcom/google/android/gms/car/gu;-><init>(Lcom/google/android/gms/car/CarService;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/gms/car/CarService;->c:Lcom/google/android/gms/car/gu;

    .line 78
    new-instance v0, Lcom/google/android/gms/car/no;

    const-string v1, "brokerbg"

    invoke-direct {v0, v1}, Lcom/google/android/gms/car/no;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/gms/car/CarService;->e:Lcom/google/android/gms/car/no;

    .line 79
    iget-object v0, p0, Lcom/google/android/gms/car/CarService;->e:Lcom/google/android/gms/car/no;

    invoke-virtual {v0}, Lcom/google/android/gms/car/no;->start()V

    .line 81
    new-instance v0, Landroid/content/res/Configuration;

    invoke-virtual {p0}, Lcom/google/android/gms/car/CarService;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/content/res/Configuration;-><init>(Landroid/content/res/Configuration;)V

    iput-object v0, p0, Lcom/google/android/gms/car/CarService;->g:Landroid/content/res/Configuration;

    .line 82
    iget-object v0, p0, Lcom/google/android/gms/car/CarService;->g:Landroid/content/res/Configuration;

    invoke-static {v0}, Lcom/google/android/gms/car/CarService;->a(Landroid/content/res/Configuration;)Landroid/content/res/Configuration;

    .line 83
    const-string v0, "uimode"

    invoke-virtual {p0, v0}, Lcom/google/android/gms/car/CarService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/UiModeManager;

    iput-object v0, p0, Lcom/google/android/gms/car/CarService;->f:Landroid/app/UiModeManager;

    .line 84
    return-void
.end method

.method public onDestroy()V
    .locals 3

    .prologue
    const/4 v2, 0x2

    .line 103
    const-string v0, "CAR.SERVICE"

    invoke-static {v0, v2}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 104
    const-string v0, "CAR.SERVICE"

    const-string v1, "onDestroy"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 106
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/car/CarService;->b:Lcom/google/android/gms/car/gv;

    if-eqz v0, :cond_2

    .line 107
    const-string v0, "CAR.SERVICE"

    invoke-static {v0, v2}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 108
    const-string v0, "CAR.SERVICE"

    const-string v1, "Unregistering ConnectionStatusReceiver"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 110
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/car/CarService;->b:Lcom/google/android/gms/car/gv;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/car/CarService;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 111
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/car/CarService;->b:Lcom/google/android/gms/car/gv;

    .line 113
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/car/CarService;->e:Lcom/google/android/gms/car/no;

    invoke-virtual {v0}, Lcom/google/android/gms/car/no;->a()V

    .line 115
    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    .line 116
    iget-object v0, p0, Lcom/google/android/gms/car/CarService;->a:Lcom/google/android/gms/car/gx;

    invoke-virtual {v0}, Lcom/google/android/gms/car/gx;->B()V

    .line 118
    invoke-static {}, Lcom/google/android/gms/car/ia;->a()V

    .line 121
    new-instance v0, Lcom/google/android/gms/car/gt;

    invoke-direct {v0, p0}, Lcom/google/android/gms/car/gt;-><init>(Lcom/google/android/gms/car/CarService;)V

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/google/android/gms/car/of;->a(Landroid/os/Looper;Ljava/lang/Runnable;)V

    .line 130
    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 3

    .prologue
    .line 88
    const-string v0, "CAR.SERVICE"

    const/4 v1, 0x2

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 89
    const-string v0, "CAR.SERVICE"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "onStartCommand start id "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ": "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 92
    :cond_0
    if-eqz p1, :cond_1

    const-string v0, "com.google.android.gms.car.START_CAR_MODE"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 93
    iget-object v0, p0, Lcom/google/android/gms/car/CarService;->a:Lcom/google/android/gms/car/gx;

    invoke-virtual {v0}, Lcom/google/android/gms/car/gx;->w()V

    .line 98
    :cond_1
    const/4 v0, 0x1

    return v0
.end method

.method public onUnbind(Landroid/content/Intent;)Z
    .locals 2

    .prologue
    .line 147
    const-string v0, "CAR.SERVICE"

    const/4 v1, 0x2

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 148
    const-string v0, "CAR.SERVICE"

    const-string v1, "onUnbind"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 150
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/car/CarService;->a:Lcom/google/android/gms/car/gx;

    invoke-virtual {v0}, Lcom/google/android/gms/car/gx;->j()V

    .line 151
    invoke-super {p0, p1}, Landroid/app/Service;->onUnbind(Landroid/content/Intent;)Z

    move-result v0

    return v0
.end method
