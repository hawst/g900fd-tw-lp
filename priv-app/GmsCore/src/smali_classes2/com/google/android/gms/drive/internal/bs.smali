.class final Lcom/google/android/gms/drive/internal/bs;
.super Lcom/google/android/gms/drive/internal/c;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/android/gms/common/api/m;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/common/api/m;)V
    .locals 0

    .prologue
    .line 187
    invoke-direct {p0}, Lcom/google/android/gms/drive/internal/c;-><init>()V

    .line 188
    iput-object p1, p0, Lcom/google/android/gms/drive/internal/bs;->a:Lcom/google/android/gms/common/api/m;

    .line 189
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/common/api/Status;)V
    .locals 3

    .prologue
    .line 200
    iget-object v0, p0, Lcom/google/android/gms/drive/internal/bs;->a:Lcom/google/android/gms/common/api/m;

    new-instance v1, Lcom/google/android/gms/drive/internal/bt;

    const/4 v2, 0x0

    invoke-direct {v1, p1, v2}, Lcom/google/android/gms/drive/internal/bt;-><init>(Lcom/google/android/gms/common/api/Status;Lcom/google/android/gms/drive/DrivePreferences;)V

    invoke-interface {v0, v1}, Lcom/google/android/gms/common/api/m;->a(Ljava/lang/Object;)V

    .line 201
    return-void
.end method

.method public final a(Lcom/google/android/gms/drive/internal/OnDrivePreferencesResponse;)V
    .locals 4

    .prologue
    .line 194
    iget-object v0, p0, Lcom/google/android/gms/drive/internal/bs;->a:Lcom/google/android/gms/common/api/m;

    new-instance v1, Lcom/google/android/gms/drive/internal/bt;

    sget-object v2, Lcom/google/android/gms/common/api/Status;->a:Lcom/google/android/gms/common/api/Status;

    invoke-virtual {p1}, Lcom/google/android/gms/drive/internal/OnDrivePreferencesResponse;->a()Lcom/google/android/gms/drive/DrivePreferences;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lcom/google/android/gms/drive/internal/bt;-><init>(Lcom/google/android/gms/common/api/Status;Lcom/google/android/gms/drive/DrivePreferences;)V

    invoke-interface {v0, v1}, Lcom/google/android/gms/common/api/m;->a(Ljava/lang/Object;)V

    .line 196
    return-void
.end method
