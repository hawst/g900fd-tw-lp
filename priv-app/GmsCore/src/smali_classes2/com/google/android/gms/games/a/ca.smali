.class final Lcom/google/android/gms/games/a/ca;
.super Lcom/google/android/gms/games/a/bb;
.source "SourceFile"


# static fields
.field private static final a:Ljava/util/concurrent/locks/ReentrantLock;


# instance fields
.field private final b:Lcom/google/android/gms/games/h/a/ei;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 35
    new-instance v0, Ljava/util/concurrent/locks/ReentrantLock;

    invoke-direct {v0}, Ljava/util/concurrent/locks/ReentrantLock;-><init>()V

    sput-object v0, Lcom/google/android/gms/games/a/ca;->a:Ljava/util/concurrent/locks/ReentrantLock;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/gms/games/a/bb;Lcom/google/android/gms/common/server/n;)V
    .locals 2

    .prologue
    .line 46
    const-string v0, "RevisionAgent"

    sget-object v1, Lcom/google/android/gms/games/a/ca;->a:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-direct {p0, v0, v1, p1}, Lcom/google/android/gms/games/a/bb;-><init>(Ljava/lang/String;Ljava/util/concurrent/locks/ReentrantLock;Lcom/google/android/gms/games/a/bb;)V

    .line 47
    new-instance v0, Lcom/google/android/gms/games/h/a/ei;

    invoke-direct {v0, p2}, Lcom/google/android/gms/games/h/a/ei;-><init>(Lcom/google/android/gms/common/server/n;)V

    iput-object v0, p0, Lcom/google/android/gms/games/a/ca;->b:Lcom/google/android/gms/games/h/a/ei;

    .line 48
    return-void
.end method

.method private a(Lcom/google/android/gms/common/server/ClientContext;)Lcom/google/android/gms/games/h/a/bl;
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 128
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/games/a/ca;->b:Lcom/google/android/gms/games/h/a/ei;

    const-string v1, "android:12"

    const-string v2, "revisions/check"

    const-string v3, "clientRevision"

    invoke-static {v1}, Lcom/google/android/gms/games/h/a/ei;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v3, v1}, Lcom/google/android/gms/games/h/a/ei;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iget-object v0, v0, Lcom/google/android/gms/games/h/a/ei;->a:Lcom/google/android/gms/common/server/n;

    const/4 v2, 0x0

    const/4 v4, 0x0

    const-class v5, Lcom/google/android/gms/games/h/a/bl;

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/common/server/n;->a(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;Ljava/lang/Object;Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/h/a/bl;
    :try_end_0
    .catch Lcom/android/volley/ac; {:try_start_0 .. :try_end_0} :catch_0

    .line 135
    :goto_0
    return-object v0

    .line 133
    :catch_0
    move-exception v0

    move-object v0, v6

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 57
    invoke-static {p0}, Lcom/google/android/gms/games/n;->a(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 58
    const-string v1, "lastPassedRevisionCheckMs"

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 59
    const-string v1, "serverApiVersion3p"

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 60
    const-string v1, "serverApiVersion1p"

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 61
    invoke-static {v0}, Lcom/android/a/c;->a(Landroid/content/SharedPreferences$Editor;)V

    .line 62
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;)Z
    .locals 12

    .prologue
    const-wide/16 v10, -0x1

    const/4 v1, 0x1

    .line 77
    invoke-virtual {p2}, Lcom/google/android/gms/common/server/ClientContext;->f()Z

    move-result v0

    const-string v2, "Must use 1P context for revision check"

    invoke-static {v0, v2}, Lcom/google/android/gms/common/internal/e;->a(ZLjava/lang/Object;)V

    .line 80
    invoke-static {p1}, Lcom/google/android/gms/games/n;->a(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v2

    .line 81
    const-string v0, "lastPassedRevisionCheckMs"

    invoke-interface {v2, v0, v10, v11}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v4

    .line 82
    invoke-static {}, Lcom/google/android/gms/common/util/r;->c()Lcom/google/android/gms/common/util/p;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/common/util/p;->a()J

    move-result-wide v6

    .line 83
    sub-long v8, v6, v4

    .line 84
    cmp-long v0, v4, v10

    if-eqz v0, :cond_1

    sget-object v0, Lcom/google/android/gms/games/c/a;->i:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    cmp-long v0, v8, v4

    if-gtz v0, :cond_1

    move v0, v1

    .line 121
    :cond_0
    :goto_0
    return v0

    .line 91
    :cond_1
    :try_start_0
    invoke-direct {p0, p2}, Lcom/google/android/gms/games/a/ca;->a(Lcom/google/android/gms/common/server/ClientContext;)Lcom/google/android/gms/games/h/a/bl;
    :try_end_0
    .catch Lcom/google/android/gms/games/h/c/a; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 105
    :goto_1
    if-nez v0, :cond_3

    move v0, v1

    .line 106
    goto :goto_0

    .line 92
    :catch_0
    move-exception v0

    .line 93
    invoke-virtual {v0}, Lcom/google/android/gms/games/h/c/a;->a()I

    move-result v3

    const/16 v4, 0x3eb

    if-eq v3, v4, :cond_2

    .line 95
    throw v0

    .line 99
    :cond_2
    const-string v0, "RevisionAgent"

    const-string v3, "Cannot use restricted APIs, resetting"

    invoke-static {v0, v3}, Lcom/google/android/gms/games/internal/dq;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 100
    invoke-static {p1}, Lcom/google/android/gms/games/a/ca;->a(Landroid/content/Context;)V

    .line 101
    invoke-direct {p0, p2}, Lcom/google/android/gms/games/a/ca;->a(Lcom/google/android/gms/common/server/ClientContext;)Lcom/google/android/gms/games/h/a/bl;

    move-result-object v0

    goto :goto_1

    .line 110
    :cond_3
    invoke-virtual {v0}, Lcom/google/android/gms/games/h/a/bl;->d()Ljava/lang/String;

    move-result-object v3

    .line 111
    invoke-virtual {v0}, Lcom/google/android/gms/games/h/a/bl;->b()Ljava/lang/String;

    move-result-object v4

    .line 112
    invoke-virtual {v0}, Lcom/google/android/gms/games/h/a/bl;->c()Ljava/lang/String;

    move-result-object v5

    .line 113
    const-string v0, "INVALID"

    invoke-static {v3, v0}, Lcom/google/android/gms/common/internal/bu;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    move v0, v1

    .line 114
    :goto_2
    if-eqz v0, :cond_0

    .line 115
    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    .line 116
    const-string v2, "lastPassedRevisionCheckMs"

    invoke-interface {v1, v2, v6, v7}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    .line 117
    const-string v2, "serverApiVersion3p"

    invoke-interface {v1, v2, v4}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 118
    const-string v2, "serverApiVersion1p"

    invoke-interface {v1, v2, v5}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 119
    invoke-static {v1}, Lcom/android/a/c;->a(Landroid/content/SharedPreferences$Editor;)V

    goto :goto_0

    .line 113
    :cond_4
    const/4 v0, 0x0

    goto :goto_2
.end method
