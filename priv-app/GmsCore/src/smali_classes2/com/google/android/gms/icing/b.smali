.class public final Lcom/google/android/gms/icing/b;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# instance fields
.field public a:I

.field public b:I

.field public c:I


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 40
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 41
    iput v1, p0, Lcom/google/android/gms/icing/b;->a:I

    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/gms/icing/b;->b:I

    iput v1, p0, Lcom/google/android/gms/icing/b;->c:I

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/icing/b;->cachedSize:I

    .line 42
    return-void
.end method

.method public static a([B)Lcom/google/android/gms/icing/b;
    .locals 1

    .prologue
    .line 156
    new-instance v0, Lcom/google/android/gms/icing/b;

    invoke-direct {v0}, Lcom/google/android/gms/icing/b;-><init>()V

    invoke-static {v0, p0}, Lcom/google/protobuf/nano/j;->mergeFrom(Lcom/google/protobuf/nano/j;[B)Lcom/google/protobuf/nano/j;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/icing/b;

    return-object v0
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 99
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->computeSerializedSize()I

    move-result v0

    .line 100
    iget v1, p0, Lcom/google/android/gms/icing/b;->a:I

    if-eqz v1, :cond_0

    .line 101
    iget v1, p0, Lcom/google/android/gms/icing/b;->a:I

    invoke-static {v2, v1}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 104
    :cond_0
    iget v1, p0, Lcom/google/android/gms/icing/b;->b:I

    if-eq v1, v2, :cond_1

    .line 105
    const/4 v1, 0x2

    iget v2, p0, Lcom/google/android/gms/icing/b;->b:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 108
    :cond_1
    iget v1, p0, Lcom/google/android/gms/icing/b;->c:I

    if-eqz v1, :cond_2

    .line 109
    const/4 v1, 0x3

    iget v2, p0, Lcom/google/android/gms/icing/b;->c:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 112
    :cond_2
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 54
    if-ne p1, p0, :cond_1

    .line 70
    :cond_0
    :goto_0
    return v0

    .line 57
    :cond_1
    instance-of v2, p1, Lcom/google/android/gms/icing/b;

    if-nez v2, :cond_2

    move v0, v1

    .line 58
    goto :goto_0

    .line 60
    :cond_2
    check-cast p1, Lcom/google/android/gms/icing/b;

    .line 61
    iget v2, p0, Lcom/google/android/gms/icing/b;->a:I

    iget v3, p1, Lcom/google/android/gms/icing/b;->a:I

    if-eq v2, v3, :cond_3

    move v0, v1

    .line 62
    goto :goto_0

    .line 64
    :cond_3
    iget v2, p0, Lcom/google/android/gms/icing/b;->b:I

    iget v3, p1, Lcom/google/android/gms/icing/b;->b:I

    if-eq v2, v3, :cond_4

    move v0, v1

    .line 65
    goto :goto_0

    .line 67
    :cond_4
    iget v2, p0, Lcom/google/android/gms/icing/b;->c:I

    iget v3, p1, Lcom/google/android/gms/icing/b;->c:I

    if-eq v2, v3, :cond_0

    move v0, v1

    .line 68
    goto :goto_0
.end method

.method public final hashCode()I
    .locals 2

    .prologue
    .line 75
    iget v0, p0, Lcom/google/android/gms/icing/b;->a:I

    add-int/lit16 v0, v0, 0x20f

    .line 77
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/google/android/gms/icing/b;->b:I

    add-int/2addr v0, v1

    .line 78
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/google/android/gms/icing/b;->c:I

    add-int/2addr v0, v1

    .line 79
    return v0
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 8
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/icing/b;->a:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    iput v0, p0, Lcom/google/android/gms/icing/b;->b:I

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/icing/b;->c:I

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 85
    iget v0, p0, Lcom/google/android/gms/icing/b;->a:I

    if-eqz v0, :cond_0

    .line 86
    iget v0, p0, Lcom/google/android/gms/icing/b;->a:I

    invoke-virtual {p1, v1, v0}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 88
    :cond_0
    iget v0, p0, Lcom/google/android/gms/icing/b;->b:I

    if-eq v0, v1, :cond_1

    .line 89
    const/4 v0, 0x2

    iget v1, p0, Lcom/google/android/gms/icing/b;->b:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 91
    :cond_1
    iget v0, p0, Lcom/google/android/gms/icing/b;->c:I

    if-eqz v0, :cond_2

    .line 92
    const/4 v0, 0x3

    iget v1, p0, Lcom/google/android/gms/icing/b;->c:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 94
    :cond_2
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 95
    return-void
.end method
