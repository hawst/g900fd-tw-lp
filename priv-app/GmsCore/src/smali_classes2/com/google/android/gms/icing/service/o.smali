.class public final Lcom/google/android/gms/icing/service/o;
.super Lcom/google/android/gms/icing/b/d;
.source "SourceFile"


# instance fields
.field private final g:Landroid/content/Context;

.field private h:Landroid/os/PowerManager$WakeLock;

.field private final i:Landroid/content/Intent;

.field private final j:Ljava/lang/String;


# direct methods
.method protected constructor <init>(Landroid/content/Context;Ljava/lang/String;Landroid/content/Intent;)V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0}, Lcom/google/android/gms/icing/b/d;-><init>()V

    .line 25
    iput-object p1, p0, Lcom/google/android/gms/icing/service/o;->g:Landroid/content/Context;

    .line 26
    iput-object p3, p0, Lcom/google/android/gms/icing/service/o;->i:Landroid/content/Intent;

    .line 27
    iput-object p2, p0, Lcom/google/android/gms/icing/service/o;->j:Ljava/lang/String;

    .line 28
    return-void
.end method


# virtual methods
.method protected final b()V
    .locals 3

    .prologue
    .line 32
    iget-object v0, p0, Lcom/google/android/gms/icing/service/o;->h:Landroid/os/PowerManager$WakeLock;

    if-nez v0, :cond_0

    .line 33
    iget-object v0, p0, Lcom/google/android/gms/icing/service/o;->g:Landroid/content/Context;

    const-string v1, "power"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    .line 34
    const/4 v1, 0x1

    const-string v2, "Icing"

    invoke-virtual {v0, v1, v2}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/icing/service/o;->h:Landroid/os/PowerManager$WakeLock;

    .line 36
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/icing/service/o;->g:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/gms/icing/service/o;->i:Landroid/content/Intent;

    invoke-virtual {v0, v1}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 37
    iget-object v0, p0, Lcom/google/android/gms/icing/service/o;->h:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->acquire()V

    .line 38
    return-void
.end method

.method protected final c()V
    .locals 2

    .prologue
    .line 42
    iget-object v0, p0, Lcom/google/android/gms/icing/service/o;->h:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V

    .line 43
    const-string v0, "%s: On dead called"

    iget-object v1, p0, Lcom/google/android/gms/icing/service/o;->j:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/google/android/gms/icing/c;->b(Ljava/lang/String;Ljava/lang/Object;)I

    .line 44
    iget-object v0, p0, Lcom/google/android/gms/icing/service/o;->g:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/gms/icing/service/o;->i:Landroid/content/Intent;

    invoke-virtual {v0, v1}, Landroid/content/Context;->stopService(Landroid/content/Intent;)Z

    .line 45
    return-void
.end method
