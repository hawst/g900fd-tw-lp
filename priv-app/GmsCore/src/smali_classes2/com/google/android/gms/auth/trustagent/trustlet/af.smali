.class public final Lcom/google/android/gms/auth/trustagent/trustlet/af;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Landroid/content/SharedPreferences;

.field private final b:Lcom/google/android/gms/auth/trustagent/am;


# direct methods
.method constructor <init>(Landroid/content/SharedPreferences;Lcom/google/android/gms/auth/trustagent/am;)V
    .locals 0

    .prologue
    .line 43
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 44
    iput-object p1, p0, Lcom/google/android/gms/auth/trustagent/trustlet/af;->a:Landroid/content/SharedPreferences;

    .line 45
    iput-object p2, p0, Lcom/google/android/gms/auth/trustagent/trustlet/af;->b:Lcom/google/android/gms/auth/trustagent/am;

    .line 46
    return-void
.end method

.method public static a(Lcom/google/android/gms/auth/trustagent/am;)Lcom/google/android/gms/auth/trustagent/trustlet/af;
    .locals 2

    .prologue
    .line 39
    new-instance v0, Lcom/google/android/gms/auth/trustagent/trustlet/af;

    const/4 v1, 0x0

    invoke-direct {v0, v1, p0}, Lcom/google/android/gms/auth/trustagent/trustlet/af;-><init>(Landroid/content/SharedPreferences;Lcom/google/android/gms/auth/trustagent/am;)V

    return-object v0
.end method

.method public static d(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 164
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "auth_trust_agent_pref_trusted_nfc_"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private e(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 119
    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/trustlet/af;->a:Landroid/content/SharedPreferences;

    if-eqz v0, :cond_0

    .line 120
    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/trustlet/af;->a:Landroid/content/SharedPreferences;

    invoke-interface {v0, p1, v1}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 122
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/trustlet/af;->b:Lcom/google/android/gms/auth/trustagent/am;

    invoke-virtual {v0, p1, v1}, Lcom/google/android/gms/auth/trustagent/am;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public final a()Ljava/util/Set;
    .locals 6

    .prologue
    .line 73
    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/trustlet/af;->a:Landroid/content/SharedPreferences;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/trustlet/af;->a:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->getAll()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    .line 74
    :goto_0
    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    .line 76
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 77
    const-string v1, "auth_trust_agent_pref_trusted_nfc_"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 79
    :try_start_0
    invoke-direct {p0, v0}, Lcom/google/android/gms/auth/trustagent/trustlet/af;->e(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 80
    if-eqz v1, :cond_2

    .line 81
    new-instance v4, Lorg/json/JSONObject;

    invoke-direct {v4, v1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    invoke-static {v4}, Lcom/google/android/gms/auth/trustagent/trustlet/UnlockTag;->a(Lorg/json/JSONObject;)Lcom/google/android/gms/auth/trustagent/trustlet/UnlockTag;

    move-result-object v1

    invoke-interface {v2, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 87
    :catch_0
    move-exception v1

    .line 88
    const-string v4, "Coffee - NfcPreferences"

    const-string v5, "corrupt UnlockTag, removing"

    invoke-static {v4, v5, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 89
    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/trustagent/trustlet/af;->c(Ljava/lang/String;)Z

    goto :goto_1

    .line 73
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/trustlet/af;->b:Lcom/google/android/gms/auth/trustagent/am;

    iget-object v0, v0, Lcom/google/android/gms/auth/trustagent/am;->e:Landroid/os/Bundle;

    invoke-virtual {v0}, Landroid/os/Bundle;->keySet()Ljava/util/Set;

    move-result-object v0

    goto :goto_0

    .line 84
    :cond_2
    :try_start_1
    const-string v1, "Coffee - NfcPreferences"

    const-string v4, "null string stored as UnlockTag, removing"

    invoke-static {v1, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 85
    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/trustagent/trustlet/af;->c(Ljava/lang/String;)Z
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1

    .line 93
    :cond_3
    return-object v2
.end method

.method public final a(Lcom/google/android/gms/auth/trustagent/trustlet/UnlockTag;)Z
    .locals 3

    .prologue
    .line 98
    :try_start_0
    invoke-virtual {p1}, Lcom/google/android/gms/auth/trustagent/trustlet/UnlockTag;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/auth/trustagent/trustlet/af;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/android/gms/auth/trustagent/trustlet/UnlockTag;->e()Lorg/json/JSONObject;

    move-result-object v1

    invoke-virtual {v1}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/auth/trustagent/trustlet/af;->a:Landroid/content/SharedPreferences;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/gms/auth/trustagent/trustlet/af;->a:Landroid/content/SharedPreferences;

    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    invoke-interface {v2, v0, v1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    move-result v0

    .line 101
    :goto_0
    return v0

    .line 98
    :cond_0
    iget-object v2, p0, Lcom/google/android/gms/auth/trustagent/trustlet/af;->b:Lcom/google/android/gms/auth/trustagent/am;

    invoke-virtual {v2, v0, v1}, Lcom/google/android/gms/auth/trustagent/am;->b(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    const/4 v0, 0x1

    goto :goto_0

    .line 99
    :catch_0
    move-exception v0

    .line 100
    const-string v1, "Coffee - NfcPreferences"

    const-string v2, "failed to create JSON from tag object"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 101
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 49
    invoke-static {p1}, Lcom/google/android/gms/auth/trustagent/trustlet/af;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/gms/auth/trustagent/trustlet/af;->e(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Ljava/util/Set;)Z
    .locals 4

    .prologue
    .line 114
    const-string v0, "nfc_trustlet_tech_codes"

    iget-object v1, p0, Lcom/google/android/gms/auth/trustagent/trustlet/af;->a:Landroid/content/SharedPreferences;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/gms/auth/trustagent/trustlet/af;->a:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-interface {v1, v0, p1}, Landroid/content/SharedPreferences$Editor;->putStringSet(Ljava/lang/String;Ljava/util/Set;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/auth/trustagent/trustlet/af;->b:Lcom/google/android/gms/auth/trustagent/am;

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2, p1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iget-object v3, v1, Lcom/google/android/gms/auth/trustagent/am;->e:Landroid/os/Bundle;

    invoke-virtual {v3, v0, v2}, Landroid/os/Bundle;->putStringArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    invoke-virtual {v1, v0, p1}, Lcom/google/android/gms/auth/trustagent/am;->a(Ljava/lang/String;Ljava/lang/Object;)V

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final b(Ljava/lang/String;)Lcom/google/android/gms/auth/trustagent/trustlet/UnlockTag;
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 57
    .line 59
    :try_start_0
    invoke-static {p1}, Lcom/google/android/gms/auth/trustagent/trustlet/af;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 60
    invoke-direct {p0, v1}, Lcom/google/android/gms/auth/trustagent/trustlet/af;->e(Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 61
    if-nez v2, :cond_0

    .line 68
    :goto_0
    return-object v0

    .line 65
    :cond_0
    :try_start_1
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1, v2}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    invoke-static {v1}, Lcom/google/android/gms/auth/trustagent/trustlet/UnlockTag;->a(Lorg/json/JSONObject;)Lcom/google/android/gms/auth/trustagent/trustlet/UnlockTag;
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v0

    goto :goto_0

    .line 66
    :catch_0
    move-exception v1

    move-object v2, v0

    .line 67
    :goto_1
    const-string v3, "Coffee - NfcPreferences"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "failed to parse UnlockTag with JSON: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v2, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 66
    :catch_1
    move-exception v1

    goto :goto_1
.end method

.method public final b()Ljava/util/Set;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 110
    const-string v1, "nfc_trustlet_tech_codes"

    iget-object v2, p0, Lcom/google/android/gms/auth/trustagent/trustlet/af;->a:Landroid/content/SharedPreferences;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/google/android/gms/auth/trustagent/trustlet/af;->a:Landroid/content/SharedPreferences;

    invoke-interface {v2, v1, v0}, Landroid/content/SharedPreferences;->getStringSet(Ljava/lang/String;Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    iget-object v2, p0, Lcom/google/android/gms/auth/trustagent/trustlet/af;->b:Lcom/google/android/gms/auth/trustagent/am;

    iget-object v2, v2, Lcom/google/android/gms/auth/trustagent/am;->e:Landroid/os/Bundle;

    invoke-virtual {v2, v1}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v1

    if-eqz v1, :cond_0

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    goto :goto_0
.end method

.method final c(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 156
    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/trustlet/af;->a:Landroid/content/SharedPreferences;

    if-eqz v0, :cond_0

    .line 157
    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/trustlet/af;->a:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0, p1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    move-result v0

    .line 160
    :goto_0
    return v0

    .line 159
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/trustlet/af;->b:Lcom/google/android/gms/auth/trustagent/am;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/auth/trustagent/am;->c(Ljava/lang/String;)V

    .line 160
    const/4 v0, 0x1

    goto :goto_0
.end method
