.class public final Lcom/google/android/gms/udc/ui/g;
.super Lcom/google/android/gms/udc/ui/k;
.source "SourceFile"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 111
    invoke-direct {p0}, Lcom/google/android/gms/udc/ui/k;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/CharSequence;)Lcom/google/android/gms/udc/ui/g;
    .locals 2

    .prologue
    .line 118
    iget-object v0, p0, Lcom/google/android/gms/udc/ui/g;->a:Landroid/os/Bundle;

    const-string v1, "UdcDialogTitle"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putCharSequence(Ljava/lang/String;Ljava/lang/CharSequence;)V

    .line 119
    return-object p0
.end method

.method public final a(Z)Lcom/google/android/gms/udc/ui/g;
    .locals 2

    .prologue
    .line 138
    iget-object v0, p0, Lcom/google/android/gms/udc/ui/g;->a:Landroid/os/Bundle;

    const-string v1, "UdcDialogShowRetry"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 139
    return-object p0
.end method

.method protected final synthetic a()Lcom/google/android/gms/udc/ui/j;
    .locals 1

    .prologue
    .line 111
    new-instance v0, Lcom/google/android/gms/udc/ui/f;

    invoke-direct {v0}, Lcom/google/android/gms/udc/ui/f;-><init>()V

    return-object v0
.end method

.method public final b(Ljava/lang/CharSequence;)Lcom/google/android/gms/udc/ui/g;
    .locals 2

    .prologue
    .line 123
    iget-object v0, p0, Lcom/google/android/gms/udc/ui/g;->a:Landroid/os/Bundle;

    const-string v1, "UdcDialogMessage"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putCharSequence(Ljava/lang/String;Ljava/lang/CharSequence;)V

    .line 124
    return-object p0
.end method

.method public final c(Ljava/lang/CharSequence;)Lcom/google/android/gms/udc/ui/g;
    .locals 2

    .prologue
    .line 128
    iget-object v0, p0, Lcom/google/android/gms/udc/ui/g;->a:Landroid/os/Bundle;

    const-string v1, "UdcDialogPositive"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putCharSequence(Ljava/lang/String;Ljava/lang/CharSequence;)V

    .line 129
    return-object p0
.end method

.method public final d(Ljava/lang/CharSequence;)Lcom/google/android/gms/udc/ui/g;
    .locals 2

    .prologue
    .line 133
    iget-object v0, p0, Lcom/google/android/gms/udc/ui/g;->a:Landroid/os/Bundle;

    const-string v1, "UdcDialogNegative"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putCharSequence(Ljava/lang/String;Ljava/lang/CharSequence;)V

    .line 134
    return-object p0
.end method
