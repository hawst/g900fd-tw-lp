.class public final Lcom/google/android/gms/auth/trustagent/bo;
.super Landroid/app/DialogFragment;
.source "SourceFile"


# instance fields
.field private a:Lcom/google/android/gms/auth/trustagent/br;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 465
    invoke-direct {p0}, Landroid/app/DialogFragment;-><init>()V

    .line 467
    return-void
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/auth/trustagent/bo;
    .locals 3

    .prologue
    .line 479
    new-instance v0, Lcom/google/android/gms/auth/trustagent/bo;

    invoke-direct {v0}, Lcom/google/android/gms/auth/trustagent/bo;-><init>()V

    .line 480
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 481
    const-string v2, "auth_trust_agent_pref_trusted_places_preference_key"

    invoke-virtual {v1, v2, p0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 482
    const-string v2, "auth_trust_agent_pref_trusted_places_preference_title"

    invoke-virtual {v1, v2, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 483
    const-string v2, "auth_trust_agent_pref_trusted_places_preference_summary"

    invoke-virtual {v1, v2, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 484
    invoke-virtual {v0, v1}, Lcom/google/android/gms/auth/trustagent/bo;->setArguments(Landroid/os/Bundle;)V

    .line 485
    return-object v0
.end method

.method static synthetic a(Lcom/google/android/gms/auth/trustagent/bo;)Lcom/google/android/gms/auth/trustagent/br;
    .locals 1

    .prologue
    .line 465
    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/bo;->a:Lcom/google/android/gms/auth/trustagent/br;

    return-object v0
.end method


# virtual methods
.method public final onAttach(Landroid/app/Activity;)V
    .locals 0

    .prologue
    .line 490
    invoke-super {p0, p1}, Landroid/app/DialogFragment;->onAttach(Landroid/app/Activity;)V

    .line 491
    check-cast p1, Lcom/google/android/gms/auth/trustagent/br;

    iput-object p1, p0, Lcom/google/android/gms/auth/trustagent/bo;->a:Lcom/google/android/gms/auth/trustagent/br;

    .line 492
    return-void
.end method

.method public final onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 6

    .prologue
    .line 496
    invoke-virtual {p0}, Lcom/google/android/gms/auth/trustagent/bo;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "auth_trust_agent_pref_trusted_places_preference_key"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 497
    invoke-virtual {p0}, Lcom/google/android/gms/auth/trustagent/bo;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "auth_trust_agent_pref_trusted_places_preference_title"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 498
    invoke-virtual {p0}, Lcom/google/android/gms/auth/trustagent/bo;->getArguments()Landroid/os/Bundle;

    move-result-object v2

    const-string v3, "auth_trust_agent_pref_trusted_places_preference_summary"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 499
    new-instance v3, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/google/android/gms/auth/trustagent/bo;->getActivity()Landroid/app/Activity;

    move-result-object v4

    invoke-direct {v3, v4}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 500
    invoke-virtual {v3, v1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v4

    invoke-virtual {v4, v2}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 501
    sget v4, Lcom/google/android/gms/p;->dk:I

    invoke-virtual {p0, v4}, Lcom/google/android/gms/auth/trustagent/bo;->getString(I)Ljava/lang/String;

    move-result-object v4

    new-instance v5, Lcom/google/android/gms/auth/trustagent/bp;

    invoke-direct {v5, p0, v0, v1, v2}, Lcom/google/android/gms/auth/trustagent/bp;-><init>(Lcom/google/android/gms/auth/trustagent/bo;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v3, v4, v5}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 510
    sget v1, Lcom/google/android/gms/p;->dj:I

    invoke-virtual {p0, v1}, Lcom/google/android/gms/auth/trustagent/bo;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/google/android/gms/auth/trustagent/bq;

    invoke-direct {v2, p0, v0}, Lcom/google/android/gms/auth/trustagent/bq;-><init>(Lcom/google/android/gms/auth/trustagent/bo;Ljava/lang/String;)V

    invoke-virtual {v3, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 517
    invoke-virtual {v3}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    return-object v0
.end method
