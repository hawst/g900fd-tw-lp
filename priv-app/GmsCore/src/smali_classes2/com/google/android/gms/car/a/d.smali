.class final Lcom/google/android/gms/car/a/d;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lcom/google/android/gms/car/a/a;

.field private final b:I


# direct methods
.method constructor <init>(Lcom/google/android/gms/car/a/a;I)V
    .locals 0

    .prologue
    .line 981
    iput-object p1, p0, Lcom/google/android/gms/car/a/d;->a:Lcom/google/android/gms/car/a/a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 982
    iput p2, p0, Lcom/google/android/gms/car/a/d;->b:I

    .line 983
    return-void
.end method


# virtual methods
.method public final run()V
    .locals 3

    .prologue
    .line 987
    iget-object v0, p0, Lcom/google/android/gms/car/a/d;->a:Lcom/google/android/gms/car/a/a;

    invoke-static {v0}, Lcom/google/android/gms/car/a/a;->a(Lcom/google/android/gms/car/a/a;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 988
    const-string v0, "CAR.BT"

    const/4 v1, 0x4

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 989
    const-string v0, "CAR.BT"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Delayed transition after cleanup. nextState="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/google/android/gms/car/a/d;->b:I

    invoke-static {v2}, Lcom/google/android/gms/car/a/a;->b(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 999
    :cond_0
    :goto_0
    return-void

    .line 994
    :cond_1
    const-string v0, "CAR.BT"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 995
    const-string v0, "CAR.BT"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "DelayedTransitionHandler dispatched. Transitioning from "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/gms/car/a/d;->a:Lcom/google/android/gms/car/a/a;

    invoke-static {v2}, Lcom/google/android/gms/car/a/a;->b(Lcom/google/android/gms/car/a/a;)I

    move-result v2

    invoke-static {v2}, Lcom/google/android/gms/car/a/a;->b(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " to "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/google/android/gms/car/a/d;->b:I

    invoke-static {v2}, Lcom/google/android/gms/car/a/a;->b(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 998
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/car/a/d;->a:Lcom/google/android/gms/car/a/a;

    iget v1, p0, Lcom/google/android/gms/car/a/d;->b:I

    invoke-static {v0, v1}, Lcom/google/android/gms/car/a/a;->a(Lcom/google/android/gms/car/a/a;I)V

    goto :goto_0
.end method
