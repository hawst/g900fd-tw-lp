.class public final Lcom/google/android/gms/icing/impl/e/j;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/api/aq;
.implements Lcom/google/android/gms/common/api/x;


# instance fields
.field public final a:Lcom/google/android/gms/common/api/v;

.field public final b:Landroid/os/ConditionVariable;

.field public final c:Ljava/util/concurrent/atomic/AtomicBoolean;

.field public d:Landroid/os/ParcelFileDescriptor;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 80
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 81
    new-instance v0, Lcom/google/android/gms/common/api/w;

    invoke-direct {v0, p1}, Lcom/google/android/gms/common/api/w;-><init>(Landroid/content/Context;)V

    sget-object v1, Lcom/google/android/gms/appdatasearch/a;->b:Lcom/google/android/gms/common/api/c;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/api/w;->a(Lcom/google/android/gms/common/api/c;)Lcom/google/android/gms/common/api/w;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/common/api/w;->a()Lcom/google/android/gms/common/api/v;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/icing/impl/e/j;->a:Lcom/google/android/gms/common/api/v;

    .line 84
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/e/j;->a:Lcom/google/android/gms/common/api/v;

    invoke-interface {v0, p0}, Lcom/google/android/gms/common/api/v;->a(Lcom/google/android/gms/common/api/x;)V

    .line 86
    new-instance v0, Landroid/os/ConditionVariable;

    invoke-direct {v0}, Landroid/os/ConditionVariable;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/icing/impl/e/j;->b:Landroid/os/ConditionVariable;

    .line 87
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/icing/impl/e/j;->c:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 88
    return-void
.end method


# virtual methods
.method public final synthetic a(Lcom/google/android/gms/common/api/ap;)V
    .locals 2

    .prologue
    .line 72
    check-cast p1, Lcom/google/android/gms/appdatasearch/bh;

    invoke-interface {p1}, Lcom/google/android/gms/appdatasearch/bh;->C_()Lcom/google/android/gms/common/api/Status;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/common/api/Status;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p1}, Lcom/google/android/gms/appdatasearch/bh;->b()Landroid/os/ParcelFileDescriptor;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/icing/impl/e/j;->d:Landroid/os/ParcelFileDescriptor;

    :goto_0
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/e/j;->b:Landroid/os/ConditionVariable;

    invoke-virtual {v0}, Landroid/os/ConditionVariable;->open()V

    return-void

    :cond_0
    const-string v0, "Failed to get file descriptor. Status code: %d."

    invoke-interface {p1}, Lcom/google/android/gms/appdatasearch/bh;->C_()Lcom/google/android/gms/common/api/Status;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/common/api/Status;->h()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/icing/c;->e(Ljava/lang/String;Ljava/lang/Object;)I

    goto :goto_0
.end method

.method public final b_(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 107
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/e/j;->c:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 108
    const-string v0, "Connected after timeout expired."

    invoke-static {v0}, Lcom/google/android/gms/icing/c;->e(Ljava/lang/String;)I

    .line 113
    :goto_0
    return-void

    .line 111
    :cond_0
    sget-object v0, Lcom/google/android/gms/appdatasearch/a;->c:Lcom/google/android/gms/appdatasearch/bg;

    iget-object v1, p0, Lcom/google/android/gms/icing/impl/e/j;->a:Lcom/google/android/gms/common/api/v;

    invoke-interface {v0, v1}, Lcom/google/android/gms/appdatasearch/bg;->a(Lcom/google/android/gms/common/api/v;)Lcom/google/android/gms/common/api/am;

    move-result-object v0

    invoke-interface {v0, p0}, Lcom/google/android/gms/common/api/am;->a(Lcom/google/android/gms/common/api/aq;)V

    goto :goto_0
.end method

.method public final f_(I)V
    .locals 1

    .prologue
    .line 117
    const-string v0, "Usage report connection failed."

    invoke-static {v0}, Lcom/google/android/gms/icing/c;->e(Ljava/lang/String;)I

    .line 118
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/e/j;->b:Landroid/os/ConditionVariable;

    invoke-virtual {v0}, Landroid/os/ConditionVariable;->open()V

    .line 119
    return-void
.end method
