.class public Lcom/google/android/gms/mdm/services/SitrepService;
.super Landroid/app/IntentService;
.source "SourceFile"


# instance fields
.field private a:I

.field private b:Ljava/lang/String;

.field private c:Ljava/lang/Boolean;

.field private d:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 147
    const-class v0, Lcom/google/android/gms/mdm/services/SitrepService;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Landroid/app/IntentService;-><init>(Ljava/lang/String;)V

    .line 148
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/gms/mdm/services/SitrepService;->setIntentRedelivery(Z)V

    .line 149
    return-void
.end method

.method private static a(JLjava/lang/String;)J
    .locals 6

    .prologue
    const-wide/16 v0, 0x0

    .line 277
    :try_start_0
    invoke-static {p2}, Landroid/net/http/AndroidHttpClient;->parseDate(Ljava/lang/String;)J
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v0

    .line 290
    :cond_0
    :goto_0
    return-wide v0

    .line 281
    :catch_0
    move-exception v2

    .line 283
    :try_start_1
    invoke-static {p2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J
    :try_end_1
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_1

    move-result-wide v2

    const-wide/16 v4, 0x3e8

    mul-long/2addr v2, v4

    .line 287
    :goto_1
    cmp-long v4, v2, v0

    if-lez v4, :cond_0

    .line 290
    add-long v0, p0, v2

    goto :goto_0

    .line 285
    :catch_1
    move-exception v2

    const-string v2, "Cannot parse retry time: %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p2, v3, v4

    const-string v4, "MDM"

    invoke-static {v4, v2, v3}, Lcom/google/android/gms/mdm/f/b;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    move-wide v2, v0

    goto :goto_1
.end method

.method public static a(Landroid/content/Context;I)Landroid/content/Intent;
    .locals 3

    .prologue
    .line 52
    const/4 v1, 0x0

    sget-object v0, Lcom/google/android/gms/mdm/e/a;->g:Lcom/google/android/gms/common/a/r;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/r;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {p0}, Lcom/google/android/gms/mdm/f/c;->c(Landroid/content/Context;)Z

    move-result v2

    invoke-static {p0, v1, v0, p1, v2}, Lcom/google/android/gms/mdm/services/SitrepService;->a(Landroid/content/Context;ZIIZ)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public static a(Landroid/content/Context;ZI)Landroid/content/Intent;
    .locals 1

    .prologue
    .line 48
    invoke-static {p0}, Lcom/google/android/gms/mdm/f/c;->c(Landroid/content/Context;)Z

    move-result v0

    invoke-static {p0, p1, p2, v0}, Lcom/google/android/gms/mdm/services/SitrepService;->a(Landroid/content/Context;ZIZ)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method private static a(Landroid/content/Context;ZIIZ)Landroid/content/Intent;
    .locals 8

    .prologue
    const/4 v3, 0x1

    const/4 v7, 0x0

    .line 70
    sget-object v0, Lcom/google/android/gms/mdm/e/a;->a:Lcom/google/android/gms/common/a/r;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/r;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v4

    sget-object v0, Lcom/google/android/gms/mdm/e/a;->b:Lcom/google/android/gms/common/a/r;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/r;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    sget-object v1, Lcom/google/android/gms/mdm/e/a;->c:Lcom/google/android/gms/common/a/r;

    invoke-virtual {v1}, Lcom/google/android/gms/common/a/r;->a()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    invoke-static {p0}, Lcom/google/android/gms/gcm/ab;->d(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v5

    sget-object v2, Lcom/google/android/gms/mdm/e/a;->i:Lcom/google/android/gms/common/a/r;

    invoke-virtual {v2}, Lcom/google/android/gms/common/a/r;->a()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v6

    sget-object v2, Lcom/google/android/gms/mdm/b/a;->o:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v2}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    if-lt v6, v2, :cond_1

    const-string v0, "Exceeded maximum sitrep attempts; not trying again."

    new-array v1, v7, [Ljava/lang/Object;

    const-string v2, "MDM"

    invoke-static {v2, v0, v1}, Lcom/google/android/gms/mdm/f/b;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    invoke-static {p0}, Lcom/google/android/gms/mdm/receivers/RetryAfterAlarmReceiver;->b(Landroid/content/Context;)V

    const-class v0, Lcom/google/android/gms/mdm/receivers/ConnectivityReceiver;

    invoke-static {p0, v0, v7}, Lcom/google/android/gms/common/util/e;->a(Landroid/content/Context;Ljava/lang/Class;Z)V

    :cond_0
    :goto_0
    const/4 v0, 0x0

    :goto_1
    return-object v0

    :cond_1
    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-static {p0, v2}, Lcom/google/android/gms/common/util/a;->d(Landroid/content/Context;Ljava/lang/String;)Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_3

    const-string v0, "No Google accounts; deferring server state update."

    new-array v1, v7, [Ljava/lang/Object;

    const-string v2, "MDM"

    invoke-static {v2, v0, v1}, Lcom/google/android/gms/mdm/f/b;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    sget-object v0, Lcom/google/android/gms/mdm/b/a;->s:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_2

    const-class v0, Lcom/google/android/gms/mdm/receivers/AccountsChangedReceiver;

    invoke-static {p0, v0, v3}, Lcom/google/android/gms/common/util/e;->a(Landroid/content/Context;Ljava/lang/Class;Z)V

    :cond_2
    const-class v0, Lcom/google/android/gms/mdm/receivers/ConnectivityReceiver;

    invoke-static {p0, v0, v7}, Lcom/google/android/gms/common/util/e;->a(Landroid/content/Context;Ljava/lang/Class;Z)V

    goto :goto_0

    :cond_3
    const-class v2, Lcom/google/android/gms/mdm/receivers/AccountsChangedReceiver;

    invoke-static {p0, v2, v7}, Lcom/google/android/gms/common/util/e;->a(Landroid/content/Context;Ljava/lang/Class;Z)V

    new-instance v2, Landroid/content/Intent;

    const-class v6, Lcom/google/android/gms/mdm/services/SitrepService;

    invoke-direct {v2, p0, v6}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v6, "reason"

    invoke-virtual {v2, v6, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v6, "retry_reason"

    invoke-virtual {v2, v6, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    invoke-static {p0}, Lcom/google/android/gms/common/util/e;->f(Landroid/content/Context;)I

    move-result v6

    const-string v7, "gms_core_version"

    invoke-virtual {v2, v7, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    if-nez p1, :cond_4

    if-eq v6, v4, :cond_9

    :cond_4
    move v4, v3

    :goto_2
    if-nez p1, :cond_5

    invoke-static {v0, v5}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_8

    :cond_5
    const-string v0, "gcm_registration_id"

    invoke-virtual {v2, v0, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move v0, v3

    :goto_3
    if-nez p1, :cond_6

    if-eqz v1, :cond_6

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eq v1, p4, :cond_7

    :cond_6
    const-string v0, "is_device_admin"

    invoke-virtual {v2, v0, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move v0, v3

    :cond_7
    if-eqz v0, :cond_0

    move-object v0, v2

    goto :goto_1

    :cond_8
    move v0, v4

    goto :goto_3

    :cond_9
    move v4, p1

    goto :goto_2
.end method

.method public static a(Landroid/content/Context;ZIZ)Landroid/content/Intent;
    .locals 1

    .prologue
    .line 65
    const/4 v0, 0x0

    invoke-static {p0, p1, p2, v0, p3}, Lcom/google/android/gms/mdm/services/SitrepService;->a(Landroid/content/Context;ZIIZ)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public static a()V
    .locals 1

    .prologue
    .line 296
    sget-object v0, Lcom/google/android/gms/mdm/e/a;->i:Lcom/google/android/gms/common/a/r;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/r;->b()V

    .line 297
    sget-object v0, Lcom/google/android/gms/mdm/e/a;->a:Lcom/google/android/gms/common/a/r;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/r;->b()V

    .line 298
    sget-object v0, Lcom/google/android/gms/mdm/e/a;->b:Lcom/google/android/gms/common/a/r;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/r;->b()V

    .line 299
    sget-object v0, Lcom/google/android/gms/mdm/e/a;->c:Lcom/google/android/gms/common/a/r;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/r;->b()V

    .line 300
    return-void
.end method

.method private a(Ljava/lang/Exception;)V
    .locals 9

    .prologue
    const-wide/16 v4, 0x0

    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 217
    const-string v0, "Error sending sitrep."

    new-array v1, v7, [Ljava/lang/Object;

    const-string v2, "MDM"

    invoke-static {v2, v0, v1}, Lcom/google/android/gms/mdm/f/b;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 218
    const/4 v1, 0x0

    .line 219
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/Exception;->getCause()Ljava/lang/Throwable;

    move-result-object v0

    instance-of v0, v0, Lcom/android/volley/ac;

    if-eqz v0, :cond_0

    .line 220
    invoke-virtual {p1}, Ljava/lang/Exception;->getCause()Ljava/lang/Throwable;

    move-result-object v0

    check-cast v0, Lcom/android/volley/ac;

    move-object v1, v0

    .line 222
    :cond_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    if-eqz v1, :cond_1

    instance-of v0, v1, Lcom/google/android/gms/mdm/a/a;

    if-eqz v0, :cond_1

    move-object v0, v1

    check-cast v0, Lcom/google/android/gms/mdm/a/a;

    invoke-virtual {v0}, Lcom/google/android/gms/mdm/a/a;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    if-eqz v1, :cond_4

    iget-object v0, v1, Lcom/android/volley/ac;->a:Lcom/android/volley/m;

    if-eqz v0, :cond_4

    new-array v0, v8, [Ljava/lang/Object;

    iget-object v6, v1, Lcom/android/volley/ac;->a:Lcom/android/volley/m;

    iget v6, v6, Lcom/android/volley/m;->a:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v0, v7

    iget-object v0, v1, Lcom/android/volley/ac;->a:Lcom/android/volley/m;

    iget-object v0, v0, Lcom/android/volley/m;->b:[B

    invoke-static {v0, v7}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;

    iget-object v0, v1, Lcom/android/volley/ac;->a:Lcom/android/volley/m;

    iget v0, v0, Lcom/android/volley/m;->a:I

    const/16 v6, 0x1f7

    if-ne v0, v6, :cond_4

    iget-object v0, v1, Lcom/android/volley/ac;->a:Lcom/android/volley/m;

    iget-object v0, v0, Lcom/android/volley/m;->c:Ljava/util/Map;

    const-string v6, "Retry-After"

    invoke-interface {v0, v6}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, v1, Lcom/android/volley/ac;->a:Lcom/android/volley/m;

    iget-object v0, v0, Lcom/android/volley/m;->c:Ljava/util/Map;

    const-string v1, "Retry-After"

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v2, v3, v0}, Lcom/google/android/gms/mdm/services/SitrepService;->a(JLjava/lang/String;)J

    move-result-wide v0

    move-wide v2, v0

    :goto_0
    sget-object v0, Lcom/google/android/gms/mdm/e/a;->g:Lcom/google/android/gms/common/a/r;

    iget v1, p0, Lcom/google/android/gms/mdm/services/SitrepService;->d:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/a/r;->a(Ljava/lang/Object;)V

    sget-object v1, Lcom/google/android/gms/mdm/e/a;->i:Lcom/google/android/gms/common/a/r;

    sget-object v0, Lcom/google/android/gms/mdm/e/a;->i:Lcom/google/android/gms/common/a/r;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/r;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/google/android/gms/common/a/r;->a(Ljava/lang/Object;)V

    cmp-long v0, v2, v4

    if-lez v0, :cond_3

    invoke-static {p0, v2, v3}, Lcom/google/android/gms/mdm/receivers/RetryAfterAlarmReceiver;->a(Landroid/content/Context;J)V

    .line 223
    :cond_2
    :goto_1
    return-void

    .line 222
    :cond_3
    const-class v0, Lcom/google/android/gms/mdm/receivers/ConnectivityReceiver;

    invoke-static {p0, v0, v8}, Lcom/google/android/gms/common/util/e;->a(Landroid/content/Context;Ljava/lang/Class;Z)V

    goto :goto_1

    :cond_4
    move-wide v2, v4

    goto :goto_0
.end method


# virtual methods
.method protected final onHandleIntent(Landroid/content/Intent;)V
    .locals 14

    .prologue
    .line 153
    invoke-static {p1}, Landroid/support/v4/a/z;->a(Landroid/content/Intent;)Z

    .line 154
    const-string v0, "reason"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/mdm/services/SitrepService;->d:I

    .line 155
    const-string v0, "gms_core_version"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/mdm/services/SitrepService;->a:I

    .line 156
    const-string v0, "gcm_registration_id"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/mdm/services/SitrepService;->b:Ljava/lang/String;

    .line 157
    const-string v0, "is_device_admin"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 158
    const-string v0, "is_device_admin"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/mdm/services/SitrepService;->c:Ljava/lang/Boolean;

    .line 161
    :cond_0
    const-string v0, "retry_reason"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    invoke-static {}, Lcom/google/android/gms/common/util/e;->a()J

    move-result-wide v4

    const-wide/16 v2, 0x0

    cmp-long v0, v4, v2

    if-nez v0, :cond_1

    const-string v0, "Android ID == 0, not sending sitrep"

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    const-string v2, "MDM"

    invoke-static {v2, v0, v1}, Lcom/google/android/gms/mdm/f/b;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/gms/mdm/services/SitrepService;->a(Ljava/lang/Exception;)V

    .line 162
    :goto_0
    return-void

    .line 161
    :cond_1
    invoke-static {}, Lcom/android/volley/toolbox/aa;->a()Lcom/android/volley/toolbox/aa;

    move-result-object v3

    const-string v0, "phone"

    invoke-virtual {p0, v0}, Lcom/google/android/gms/mdm/services/SitrepService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    iget v2, p0, Lcom/google/android/gms/mdm/services/SitrepService;->a:I

    iget-object v7, p0, Lcom/google/android/gms/mdm/services/SitrepService;->b:Ljava/lang/String;

    iget-object v8, p0, Lcom/google/android/gms/mdm/services/SitrepService;->c:Ljava/lang/Boolean;

    iget v9, p0, Lcom/google/android/gms/mdm/services/SitrepService;->d:I

    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getPhoneType()I

    move-result v10

    const-string v6, "sending sitrep: [%d, %d, %s, %s]"

    const/4 v0, 0x4

    new-array v11, v0, [Ljava/lang/Object;

    const/4 v0, 0x0

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    aput-object v12, v11, v0

    const/4 v0, 0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    aput-object v12, v11, v0

    const/4 v12, 0x2

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v13

    sget-object v0, Lcom/google/android/gms/mdm/b/a;->r:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-static {v13, v0}, Lcom/google/android/gms/mdm/f/b;->a(Ljava/lang/Object;Z)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v11, v12

    const/4 v0, 0x3

    aput-object v8, v11, v0

    const-string v0, "MDM"

    invoke-static {v0, v6, v11}, Lcom/google/android/gms/mdm/f/b;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    invoke-static {}, Lcom/google/android/gms/common/app/GmsApplication;->b()Lcom/google/android/gms/common/app/GmsApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/common/app/GmsApplication;->c()Lcom/android/volley/s;

    move-result-object v11

    new-instance v6, Lcom/google/af/a/c/a/n;

    invoke-direct {v6}, Lcom/google/af/a/c/a/n;-><init>()V

    iput v9, v6, Lcom/google/af/a/c/a/n;->g:I

    iput v1, v6, Lcom/google/af/a/c/a/n;->h:I

    iput-wide v4, v6, Lcom/google/af/a/c/a/n;->a:J

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    iput v0, v6, Lcom/google/af/a/c/a/n;->c:I

    iput v10, v6, Lcom/google/af/a/c/a/n;->i:I

    if-lez v2, :cond_2

    iput v2, v6, Lcom/google/af/a/c/a/n;->b:I

    :cond_2
    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_3

    iput-object v7, v6, Lcom/google/af/a/c/a/n;->e:Ljava/lang/String;

    :cond_3
    if-eqz v8, :cond_4

    new-instance v0, Lcom/google/af/a/c/a/d;

    invoke-direct {v0}, Lcom/google/af/a/c/a/d;-><init>()V

    iput-object v0, v6, Lcom/google/af/a/c/a/n;->f:Lcom/google/af/a/c/a/d;

    iget-object v0, v6, Lcom/google/af/a/c/a/n;->f:Lcom/google/af/a/c/a/d;

    invoke-virtual {v8}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    iput-boolean v1, v0, Lcom/google/af/a/c/a/d;->a:Z

    iget-object v0, v6, Lcom/google/af/a/c/a/n;->f:Lcom/google/af/a/c/a/d;

    invoke-virtual {v8}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    iput-boolean v1, v0, Lcom/google/af/a/c/a/d;->b:Z

    iget-object v0, v6, Lcom/google/af/a/c/a/n;->f:Lcom/google/af/a/c/a/d;

    invoke-virtual {v8}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    iput-boolean v1, v0, Lcom/google/af/a/c/a/d;->c:Z

    :cond_4
    const-string v0, "sitrep"

    new-instance v1, Lcom/android/volley/t;

    invoke-direct {v1, v11, v0}, Lcom/android/volley/t;-><init>(Lcom/android/volley/s;Ljava/lang/Object;)V

    invoke-virtual {v11, v1}, Lcom/android/volley/s;->a(Lcom/android/volley/u;)V

    new-instance v0, Lcom/google/android/gms/mdm/a/c;

    sget-object v1, Lcom/google/android/gms/mdm/b/a;->b:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v1}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    const/4 v2, 0x1

    const-class v5, Lcom/google/af/a/c/a/o;

    move-object v4, v3

    invoke-direct/range {v0 .. v6}, Lcom/google/android/gms/mdm/a/c;-><init>(Ljava/lang/String;ZLcom/android/volley/x;Lcom/android/volley/w;Ljava/lang/Class;Lcom/google/protobuf/nano/j;)V

    const-string v1, "sitrep"

    invoke-virtual {v0, v1}, Lcom/android/volley/p;->a(Ljava/lang/Object;)Lcom/android/volley/p;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/android/volley/p;->a(Z)Lcom/android/volley/p;

    invoke-virtual {v11, v0}, Lcom/android/volley/s;->a(Lcom/android/volley/p;)Lcom/android/volley/p;

    :try_start_0
    invoke-virtual {v3}, Lcom/android/volley/toolbox/aa;->get()Ljava/lang/Object;

    const-string v0, "Sitrep successful"

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    const-string v2, "MDM"

    invoke-static {v2, v0, v1}, Lcom/google/android/gms/mdm/f/b;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget v2, p0, Lcom/google/android/gms/mdm/services/SitrepService;->a:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/gms/mdm/services/SitrepService;->b:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/gms/mdm/services/SitrepService;->c:Ljava/lang/Boolean;

    aput-object v2, v0, v1

    iget v0, p0, Lcom/google/android/gms/mdm/services/SitrepService;->a:I

    if-lez v0, :cond_5

    sget-object v0, Lcom/google/android/gms/mdm/e/a;->a:Lcom/google/android/gms/common/a/r;

    iget v1, p0, Lcom/google/android/gms/mdm/services/SitrepService;->a:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/a/r;->a(Ljava/lang/Object;)V

    :cond_5
    iget-object v0, p0, Lcom/google/android/gms/mdm/services/SitrepService;->b:Ljava/lang/String;

    if-eqz v0, :cond_6

    sget-object v0, Lcom/google/android/gms/mdm/e/a;->b:Lcom/google/android/gms/common/a/r;

    iget-object v1, p0, Lcom/google/android/gms/mdm/services/SitrepService;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/a/r;->a(Ljava/lang/Object;)V

    :cond_6
    iget-object v0, p0, Lcom/google/android/gms/mdm/services/SitrepService;->c:Ljava/lang/Boolean;

    if-eqz v0, :cond_7

    sget-object v0, Lcom/google/android/gms/mdm/e/a;->c:Lcom/google/android/gms/common/a/r;

    iget-object v1, p0, Lcom/google/android/gms/mdm/services/SitrepService;->c:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/a/r;->a(Ljava/lang/Object;)V

    :cond_7
    invoke-static {p0}, Lcom/google/android/gms/mdm/receivers/RetryAfterAlarmReceiver;->b(Landroid/content/Context;)V

    sget-object v0, Lcom/google/android/gms/mdm/e/a;->g:Lcom/google/android/gms/common/a/r;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/r;->b()V

    sget-object v0, Lcom/google/android/gms/mdm/e/a;->h:Lcom/google/android/gms/common/a/r;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/r;->b()V

    sget-object v0, Lcom/google/android/gms/mdm/e/a;->i:Lcom/google/android/gms/common/a/r;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/r;->b()V

    const-class v0, Lcom/google/android/gms/mdm/receivers/ConnectivityReceiver;

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Lcom/google/android/gms/common/util/e;->a(Landroid/content/Context;Ljava/lang/Class;Z)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_1

    goto/16 :goto_0

    :catch_0
    move-exception v0

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Thread;->interrupt()V

    invoke-direct {p0, v0}, Lcom/google/android/gms/mdm/services/SitrepService;->a(Ljava/lang/Exception;)V

    goto/16 :goto_0

    :catch_1
    move-exception v0

    invoke-direct {p0, v0}, Lcom/google/android/gms/mdm/services/SitrepService;->a(Ljava/lang/Exception;)V

    goto/16 :goto_0
.end method
