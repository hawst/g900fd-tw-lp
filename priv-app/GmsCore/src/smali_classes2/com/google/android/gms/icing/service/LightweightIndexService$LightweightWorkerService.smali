.class public Lcom/google/android/gms/icing/service/LightweightIndexService$LightweightWorkerService;
.super Landroid/app/IntentService;
.source "SourceFile"


# static fields
.field private static a:Ljava/util/concurrent/ConcurrentLinkedQueue;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 512
    new-instance v0, Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;-><init>()V

    sput-object v0, Lcom/google/android/gms/icing/service/LightweightIndexService$LightweightWorkerService;->a:Ljava/util/concurrent/ConcurrentLinkedQueue;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 516
    const-string v0, "LightweightWorkerService"

    invoke-direct {p0, v0}, Landroid/app/IntentService;-><init>(Ljava/lang/String;)V

    .line 517
    return-void
.end method

.method static a(Landroid/content/Context;Ljava/lang/Runnable;)V
    .locals 1

    .prologue
    .line 520
    sget-object v0, Lcom/google/android/gms/icing/service/LightweightIndexService$LightweightWorkerService;->a:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/ConcurrentLinkedQueue;->offer(Ljava/lang/Object;)Z

    .line 521
    const-string v0, "com.google.android.gms.icing.LIGHTWEIGHT_WORKER_SERVICE"

    invoke-static {v0}, Lcom/google/android/gms/common/util/e;->g(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 522
    return-void
.end method


# virtual methods
.method protected onHandleIntent(Landroid/content/Intent;)V
    .locals 1

    .prologue
    .line 526
    sget-object v0, Lcom/google/android/gms/icing/service/LightweightIndexService$LightweightWorkerService;->a:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;->poll()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Runnable;

    .line 527
    if-eqz v0, :cond_0

    .line 528
    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    .line 532
    :goto_0
    return-void

    .line 530
    :cond_0
    const-string v0, "No work to do in LightweightWorkerService."

    invoke-static {v0}, Lcom/google/android/gms/icing/c;->e(Ljava/lang/String;)I

    goto :goto_0
.end method
