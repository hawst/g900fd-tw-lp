.class public Lcom/google/android/gms/mdm/receivers/GoogleAccountsAddedReceiver;
.super Lcom/google/android/gms/mdm/receivers/a;
.source "SourceFile"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Lcom/google/android/gms/mdm/receivers/a;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 2

    .prologue
    .line 16
    const-string v0, "com.google.android.gms.auth.GOOGLE_ACCOUNT_CHANGE"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "com.google.android.gms.auth.category.ACCOUNT_ADDED"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->hasCategory(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 19
    invoke-static {}, Lcom/google/android/gms/mdm/receivers/GoogleAccountsAddedReceiver;->a()J

    move-result-wide v0

    invoke-static {p1, v0, v1}, Lcom/google/android/gms/mdm/receivers/GoogleAccountsAddedReceiver;->a(Landroid/content/Context;J)V

    .line 21
    :cond_0
    return-void
.end method
