.class public final Lcom/google/android/gms/common/internal/aq;
.super Lcom/google/android/gms/common/internal/al;
.source "SourceFile"


# instance fields
.field public final b:I

.field public final c:Landroid/os/Bundle;

.field public final d:Landroid/os/IBinder;

.field final synthetic e:Lcom/google/android/gms/common/internal/aj;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/common/internal/aj;ILandroid/os/IBinder;Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 653
    iput-object p1, p0, Lcom/google/android/gms/common/internal/aq;->e:Lcom/google/android/gms/common/internal/aj;

    .line 654
    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lcom/google/android/gms/common/internal/al;-><init>(Lcom/google/android/gms/common/internal/aj;Ljava/lang/Object;)V

    .line 655
    iput p2, p0, Lcom/google/android/gms/common/internal/aq;->b:I

    .line 656
    iput-object p3, p0, Lcom/google/android/gms/common/internal/aq;->d:Landroid/os/IBinder;

    .line 657
    iput-object p4, p0, Lcom/google/android/gms/common/internal/aq;->c:Landroid/os/Bundle;

    .line 658
    return-void
.end method

.method private a(Landroid/os/IBinder;)Landroid/os/IInterface;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 715
    :try_start_0
    invoke-interface {p1}, Landroid/os/IBinder;->getInterfaceDescriptor()Ljava/lang/String;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 720
    iget-object v2, p0, Lcom/google/android/gms/common/internal/aq;->e:Lcom/google/android/gms/common/internal/aj;

    invoke-virtual {v2}, Lcom/google/android/gms/common/internal/aj;->b_()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 726
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/common/internal/aq;->e:Lcom/google/android/gms/common/internal/aj;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/common/internal/aj;->a(Landroid/os/IBinder;)Landroid/os/IInterface;

    move-result-object v0

    goto :goto_0

    .line 717
    :catch_0
    move-exception v1

    goto :goto_0
.end method


# virtual methods
.method protected final a()V
    .locals 0

    .prologue
    .line 710
    return-void
.end method

.method protected final synthetic a(Ljava/lang/Object;)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    const/4 v5, 0x1

    .line 647
    check-cast p1, Ljava/lang/Boolean;

    if-nez p1, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/common/internal/aq;->e:Lcom/google/android/gms/common/internal/aj;

    invoke-static {v0, v5}, Lcom/google/android/gms/common/internal/aj;->a(Lcom/google/android/gms/common/internal/aj;I)V

    :goto_0
    return-void

    :cond_0
    iget v1, p0, Lcom/google/android/gms/common/internal/aq;->b:I

    sparse-switch v1, :sswitch_data_0

    iget-object v1, p0, Lcom/google/android/gms/common/internal/aq;->c:Landroid/os/Bundle;

    if-eqz v1, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/common/internal/aq;->c:Landroid/os/Bundle;

    const-string v1, "pendingIntent"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/app/PendingIntent;

    :cond_1
    iget-object v1, p0, Lcom/google/android/gms/common/internal/aq;->e:Lcom/google/android/gms/common/internal/aj;

    invoke-static {v1}, Lcom/google/android/gms/common/internal/aj;->d(Lcom/google/android/gms/common/internal/aj;)Lcom/google/android/gms/common/internal/ao;

    move-result-object v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/google/android/gms/common/internal/aq;->e:Lcom/google/android/gms/common/internal/aj;

    invoke-static {v1}, Lcom/google/android/gms/common/internal/aj;->e(Lcom/google/android/gms/common/internal/aj;)Lcom/google/android/gms/common/internal/at;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/common/internal/aq;->e:Lcom/google/android/gms/common/internal/aj;

    invoke-virtual {v2}, Lcom/google/android/gms/common/internal/aj;->a_()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/gms/common/internal/aq;->e:Lcom/google/android/gms/common/internal/aj;

    invoke-static {v3}, Lcom/google/android/gms/common/internal/aj;->d(Lcom/google/android/gms/common/internal/aj;)Lcom/google/android/gms/common/internal/ao;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/gms/common/internal/aq;->e:Lcom/google/android/gms/common/internal/aj;

    invoke-virtual {v4}, Lcom/google/android/gms/common/internal/aj;->g_()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v2, v3, v4}, Lcom/google/android/gms/common/internal/at;->b(Ljava/lang/String;Landroid/content/ServiceConnection;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/gms/common/internal/aq;->e:Lcom/google/android/gms/common/internal/aj;

    invoke-static {v1}, Lcom/google/android/gms/common/internal/aj;->f(Lcom/google/android/gms/common/internal/aj;)Lcom/google/android/gms/common/internal/ao;

    :cond_2
    iget-object v1, p0, Lcom/google/android/gms/common/internal/aq;->e:Lcom/google/android/gms/common/internal/aj;

    invoke-static {v1, v5}, Lcom/google/android/gms/common/internal/aj;->a(Lcom/google/android/gms/common/internal/aj;I)V

    iget-object v1, p0, Lcom/google/android/gms/common/internal/aq;->e:Lcom/google/android/gms/common/internal/aj;

    invoke-static {v1}, Lcom/google/android/gms/common/internal/aj;->a(Lcom/google/android/gms/common/internal/aj;)Lcom/google/android/gms/common/internal/ar;

    move-result-object v1

    new-instance v2, Lcom/google/android/gms/common/c;

    iget v3, p0, Lcom/google/android/gms/common/internal/aq;->b:I

    invoke-direct {v2, v3, v0}, Lcom/google/android/gms/common/c;-><init>(ILandroid/app/PendingIntent;)V

    invoke-virtual {v1, v2}, Lcom/google/android/gms/common/internal/ar;->a(Lcom/google/android/gms/common/c;)V

    goto :goto_0

    :sswitch_0
    iget-object v1, p0, Lcom/google/android/gms/common/internal/aq;->d:Landroid/os/IBinder;

    invoke-direct {p0, v1}, Lcom/google/android/gms/common/internal/aq;->a(Landroid/os/IBinder;)Landroid/os/IInterface;

    move-result-object v1

    if-eqz v1, :cond_3

    iget-object v2, p0, Lcom/google/android/gms/common/internal/aq;->e:Lcom/google/android/gms/common/internal/aj;

    const/4 v3, 0x2

    const/4 v4, 0x3

    invoke-static {v2, v3, v4, v1}, Lcom/google/android/gms/common/internal/aj;->a(Lcom/google/android/gms/common/internal/aj;IILandroid/os/IInterface;)Z

    move-result v1

    if-eqz v1, :cond_3

    iget-object v0, p0, Lcom/google/android/gms/common/internal/aq;->e:Lcom/google/android/gms/common/internal/aj;

    invoke-static {v0}, Lcom/google/android/gms/common/internal/aj;->a(Lcom/google/android/gms/common/internal/aj;)Lcom/google/android/gms/common/internal/ar;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/common/internal/ar;->a()V

    iget-object v0, p0, Lcom/google/android/gms/common/internal/aq;->e:Lcom/google/android/gms/common/internal/aj;

    invoke-static {v0}, Lcom/google/android/gms/common/internal/aj;->c(Lcom/google/android/gms/common/internal/aj;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/common/ew;->c(Landroid/content/Context;)V

    goto :goto_0

    :cond_3
    iget-object v1, p0, Lcom/google/android/gms/common/internal/aq;->e:Lcom/google/android/gms/common/internal/aj;

    invoke-static {v1}, Lcom/google/android/gms/common/internal/aj;->e(Lcom/google/android/gms/common/internal/aj;)Lcom/google/android/gms/common/internal/at;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/common/internal/aq;->e:Lcom/google/android/gms/common/internal/aj;

    invoke-virtual {v2}, Lcom/google/android/gms/common/internal/aj;->a_()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/gms/common/internal/aq;->e:Lcom/google/android/gms/common/internal/aj;

    invoke-static {v3}, Lcom/google/android/gms/common/internal/aj;->d(Lcom/google/android/gms/common/internal/aj;)Lcom/google/android/gms/common/internal/ao;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/gms/common/internal/aq;->e:Lcom/google/android/gms/common/internal/aj;

    invoke-virtual {v4}, Lcom/google/android/gms/common/internal/aj;->g_()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v2, v3, v4}, Lcom/google/android/gms/common/internal/at;->b(Ljava/lang/String;Landroid/content/ServiceConnection;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/gms/common/internal/aq;->e:Lcom/google/android/gms/common/internal/aj;

    invoke-static {v1}, Lcom/google/android/gms/common/internal/aj;->f(Lcom/google/android/gms/common/internal/aj;)Lcom/google/android/gms/common/internal/ao;

    iget-object v1, p0, Lcom/google/android/gms/common/internal/aq;->e:Lcom/google/android/gms/common/internal/aj;

    invoke-static {v1, v5}, Lcom/google/android/gms/common/internal/aj;->a(Lcom/google/android/gms/common/internal/aj;I)V

    iget-object v1, p0, Lcom/google/android/gms/common/internal/aq;->e:Lcom/google/android/gms/common/internal/aj;

    invoke-static {v1}, Lcom/google/android/gms/common/internal/aj;->a(Lcom/google/android/gms/common/internal/aj;)Lcom/google/android/gms/common/internal/ar;

    move-result-object v1

    new-instance v2, Lcom/google/android/gms/common/c;

    const/16 v3, 0x8

    invoke-direct {v2, v3, v0}, Lcom/google/android/gms/common/c;-><init>(ILandroid/app/PendingIntent;)V

    invoke-virtual {v1, v2}, Lcom/google/android/gms/common/internal/ar;->a(Lcom/google/android/gms/common/c;)V

    goto/16 :goto_0

    :sswitch_1
    iget-object v0, p0, Lcom/google/android/gms/common/internal/aq;->e:Lcom/google/android/gms/common/internal/aj;

    invoke-static {v0, v5}, Lcom/google/android/gms/common/internal/aj;->a(Lcom/google/android/gms/common/internal/aj;I)V

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "A fatal developer error has occurred. Check the logs for further information."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
    .end sparse-switch
.end method
