.class final Lcom/google/android/gms/icing/impl/z;
.super Lcom/google/android/gms/icing/impl/ay;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/gms/icing/impl/u;


# direct methods
.method constructor <init>(Lcom/google/android/gms/icing/impl/u;)V
    .locals 0

    .prologue
    .line 1443
    iput-object p1, p0, Lcom/google/android/gms/icing/impl/z;->a:Lcom/google/android/gms/icing/impl/u;

    invoke-direct {p0, p1}, Lcom/google/android/gms/icing/impl/ay;-><init>(Lcom/google/android/gms/icing/impl/u;)V

    return-void
.end method

.method private f()Ljava/lang/Boolean;
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 1446
    const/4 v2, 0x1

    .line 1451
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/z;->a:Lcom/google/android/gms/icing/impl/u;

    invoke-virtual {v0}, Lcom/google/android/gms/icing/impl/u;->j()Lcom/google/android/gms/icing/impl/a/j;

    move-result-object v3

    .line 1452
    invoke-virtual {v3}, Lcom/google/android/gms/icing/impl/a/j;->b()Ljava/lang/Object;

    move-result-object v4

    monitor-enter v4

    .line 1453
    :try_start_0
    invoke-virtual {v3}, Lcom/google/android/gms/icing/impl/a/j;->h()Ljava/util/Set;

    move-result-object v0

    .line 1454
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1456
    invoke-virtual {v3, v0}, Lcom/google/android/gms/icing/impl/a/j;->d(Ljava/lang/String;)Lcom/google/android/gms/icing/g;

    move-result-object v6

    .line 1457
    iget-object v7, p0, Lcom/google/android/gms/icing/impl/z;->a:Lcom/google/android/gms/icing/impl/u;

    iget-object v7, v7, Lcom/google/android/gms/icing/impl/u;->i:Lcom/google/android/gms/icing/impl/a/f;

    iget-object v6, v6, Lcom/google/android/gms/icing/g;->d:Ljava/lang/String;

    invoke-virtual {v7, v6}, Lcom/google/android/gms/icing/impl/a/f;->c(Ljava/lang/String;)Lcom/google/android/gms/icing/impl/a/e;

    move-result-object v6

    .line 1458
    iget-object v7, p0, Lcom/google/android/gms/icing/impl/z;->a:Lcom/google/android/gms/icing/impl/u;

    invoke-virtual {v7, v0, v6}, Lcom/google/android/gms/icing/impl/u;->a(Ljava/lang/String;Lcom/google/android/gms/icing/impl/a/e;)Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    move v2, v0

    .line 1461
    goto :goto_0

    .line 1462
    :cond_0
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1464
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/z;->a:Lcom/google/android/gms/icing/impl/u;

    iget-object v0, v0, Lcom/google/android/gms/icing/impl/u;->j:Lcom/google/android/gms/icing/impl/NativeIndex;

    invoke-virtual {v0}, Lcom/google/android/gms/icing/impl/NativeIndex;->q()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1467
    :goto_2
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0

    .line 1462
    :catchall_0
    move-exception v0

    monitor-exit v4

    throw v0

    :cond_1
    move v1, v2

    goto :goto_2

    :cond_2
    move v0, v2

    goto :goto_1
.end method


# virtual methods
.method protected final synthetic a()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1443
    invoke-direct {p0}, Lcom/google/android/gms/icing/impl/z;->f()Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method
