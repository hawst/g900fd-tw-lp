.class public final Lcom/google/android/gms/common/data/z;
.super Lcom/google/android/gms/common/data/e;
.source "SourceFile"


# instance fields
.field private c:Ljava/lang/Object;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/common/data/d;)V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0, p1}, Lcom/google/android/gms/common/data/e;-><init>(Lcom/google/android/gms/common/data/d;)V

    .line 29
    return-void
.end method


# virtual methods
.method public final next()Ljava/lang/Object;
    .locals 3

    .prologue
    .line 33
    invoke-virtual {p0}, Lcom/google/android/gms/common/data/z;->hasNext()Z

    move-result v0

    if-nez v0, :cond_0

    .line 34
    new-instance v0, Ljava/util/NoSuchElementException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Cannot advance the iterator beyond "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/google/android/gms/common/data/z;->b:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/NoSuchElementException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 36
    :cond_0
    iget v0, p0, Lcom/google/android/gms/common/data/z;->b:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/gms/common/data/z;->b:I

    .line 37
    iget v0, p0, Lcom/google/android/gms/common/data/z;->b:I

    if-nez v0, :cond_1

    .line 38
    iget-object v0, p0, Lcom/google/android/gms/common/data/z;->a:Lcom/google/android/gms/common/data/d;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/google/android/gms/common/data/d;->a(I)Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/common/data/z;->c:Ljava/lang/Object;

    .line 39
    iget-object v0, p0, Lcom/google/android/gms/common/data/z;->c:Ljava/lang/Object;

    instance-of v0, v0, Lcom/google/android/gms/common/data/i;

    if-nez v0, :cond_2

    .line 41
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "DataBuffer reference of type "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/gms/common/data/z;->c:Ljava/lang/Object;

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " is not movable"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 45
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/common/data/z;->c:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/gms/common/data/i;

    iget v1, p0, Lcom/google/android/gms/common/data/z;->b:I

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/data/i;->a(I)V

    .line 47
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/common/data/z;->c:Ljava/lang/Object;

    return-object v0
.end method
