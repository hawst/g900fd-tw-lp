.class public interface abstract Lcom/google/android/gms/drive/internal/ca;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/os/IInterface;


# virtual methods
.method public abstract a()V
.end method

.method public abstract a(Lcom/google/android/gms/common/api/Status;)V
.end method

.method public abstract a(Lcom/google/android/gms/drive/internal/OnContentsResponse;)V
.end method

.method public abstract a(Lcom/google/android/gms/drive/internal/OnDeviceUsagePreferenceResponse;)V
.end method

.method public abstract a(Lcom/google/android/gms/drive/internal/OnDownloadProgressResponse;)V
.end method

.method public abstract a(Lcom/google/android/gms/drive/internal/OnDriveIdResponse;)V
.end method

.method public abstract a(Lcom/google/android/gms/drive/internal/OnDrivePreferencesResponse;)V
.end method

.method public abstract a(Lcom/google/android/gms/drive/internal/OnListEntriesResponse;)V
.end method

.method public abstract a(Lcom/google/android/gms/drive/internal/OnListParentsResponse;)V
.end method

.method public abstract a(Lcom/google/android/gms/drive/internal/OnLoadRealtimeResponse;Lcom/google/android/gms/drive/realtime/internal/ag;)V
.end method

.method public abstract a(Lcom/google/android/gms/drive/internal/OnMetadataResponse;)V
.end method

.method public abstract a(Lcom/google/android/gms/drive/internal/OnResourceIdSetResponse;)V
.end method

.method public abstract a(Lcom/google/android/gms/drive/internal/OnStorageStatsResponse;)V
.end method

.method public abstract a(Lcom/google/android/gms/drive/internal/OnSyncMoreResponse;)V
.end method
