.class public final Lcom/google/android/gms/people/internal/b/af;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/people/s;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/common/api/v;Landroid/os/Bundle;)Lcom/google/android/gms/common/api/am;
    .locals 3

    .prologue
    .line 64
    const-string v0, "PeopleClientCall"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 65
    const-string v0, "internalCall"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p2, v1, v2

    invoke-static {v0, v1}, Lcom/google/android/gms/people/internal/n;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 67
    :cond_0
    new-instance v0, Lcom/google/android/gms/people/internal/b/ai;

    invoke-direct {v0, p0, p1, p2}, Lcom/google/android/gms/people/internal/b/ai;-><init>(Lcom/google/android/gms/people/internal/b/af;Lcom/google/android/gms/common/api/v;Landroid/os/Bundle;)V

    invoke-interface {p1, v0}, Lcom/google/android/gms/common/api/v;->a(Lcom/google/android/gms/common/api/l;)Lcom/google/android/gms/common/api/l;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/common/api/v;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/people/t;)Lcom/google/android/gms/common/api/am;
    .locals 6

    .prologue
    const/4 v1, 0x3

    .line 25
    const-string v0, "PeopleClientCall"

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 26
    const-string v0, "loadPeopleForAspen"

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p2, v1, v2

    const/4 v2, 0x1

    aput-object p3, v1, v2

    const/4 v2, 0x2

    aput-object p4, v1, v2

    invoke-static {v0, v1}, Lcom/google/android/gms/people/internal/n;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 28
    :cond_0
    new-instance v0, Lcom/google/android/gms/people/internal/b/ag;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/people/internal/b/ag;-><init>(Lcom/google/android/gms/people/internal/b/af;Lcom/google/android/gms/common/api/v;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/people/t;)V

    invoke-interface {p1, v0}, Lcom/google/android/gms/common/api/v;->a(Lcom/google/android/gms/common/api/l;)Lcom/google/android/gms/common/api/l;

    move-result-object v0

    return-object v0
.end method
