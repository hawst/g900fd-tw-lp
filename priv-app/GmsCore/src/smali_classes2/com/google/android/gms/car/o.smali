.class final Lcom/google/android/gms/car/o;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/car/nr;


# instance fields
.field final synthetic a:Lcom/google/android/gms/car/n;


# direct methods
.method private constructor <init>(Lcom/google/android/gms/car/n;)V
    .locals 0

    .prologue
    .line 22
    iput-object p1, p0, Lcom/google/android/gms/car/o;->a:Lcom/google/android/gms/car/n;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/gms/car/n;B)V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0, p1}, Lcom/google/android/gms/car/o;-><init>(Lcom/google/android/gms/car/n;)V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 78
    const-string v0, "CAR.AUDIO"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "AAC audio encoder returned error: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 79
    return-void
.end method

.method public final a(Ljava/nio/ByteBuffer;Landroid/media/MediaCodec$BufferInfo;)V
    .locals 2

    .prologue
    .line 25
    iget-object v0, p0, Lcom/google/android/gms/car/o;->a:Lcom/google/android/gms/car/n;

    iget-object v0, v0, Lcom/google/android/gms/car/n;->f:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->clear()Ljava/nio/Buffer;

    .line 26
    iget-object v0, p0, Lcom/google/android/gms/car/o;->a:Lcom/google/android/gms/car/n;

    iget-object v0, v0, Lcom/google/android/gms/car/n;->f:Ljava/nio/ByteBuffer;

    invoke-virtual {v0, p1}, Ljava/nio/ByteBuffer;->put(Ljava/nio/ByteBuffer;)Ljava/nio/ByteBuffer;

    .line 27
    iget-object v0, p0, Lcom/google/android/gms/car/o;->a:Lcom/google/android/gms/car/n;

    iget-object v0, v0, Lcom/google/android/gms/car/n;->f:Ljava/nio/ByteBuffer;

    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->limit()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->limit(I)Ljava/nio/Buffer;

    .line 28
    iget-object v0, p0, Lcom/google/android/gms/car/o;->a:Lcom/google/android/gms/car/n;

    iget-object v0, v0, Lcom/google/android/gms/car/n;->e:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v0}, Ljava/util/concurrent/Semaphore;->release()V

    .line 29
    return-void
.end method

.method public final b(Ljava/nio/ByteBuffer;Landroid/media/MediaCodec$BufferInfo;)V
    .locals 9
    .annotation build Landroid/annotation/TargetApi;
        value = 0x13
    .end annotation

    .prologue
    const/4 v1, 0x3

    .line 56
    iget-object v0, p0, Lcom/google/android/gms/car/o;->a:Lcom/google/android/gms/car/n;

    iget-object v0, v0, Lcom/google/android/gms/car/n;->h:Lcom/google/android/gms/car/nz;

    invoke-virtual {v0}, Lcom/google/android/gms/car/nz;->a()Lcom/google/android/gms/car/e;

    move-result-object v2

    .line 57
    iget-wide v4, p2, Landroid/media/MediaCodec$BufferInfo;->presentationTimeUs:J

    iput-wide v4, v2, Lcom/google/android/gms/car/e;->a:J

    .line 58
    iget-object v0, p0, Lcom/google/android/gms/car/o;->a:Lcom/google/android/gms/car/n;

    iget-boolean v0, v0, Lcom/google/android/gms/car/n;->d:Z

    if-eqz v0, :cond_0

    .line 59
    iget-object v3, v2, Lcom/google/android/gms/car/e;->b:Ljava/nio/ByteBuffer;

    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->limit()I

    move-result v0

    add-int/lit8 v4, v0, 0x7

    iget-object v0, p0, Lcom/google/android/gms/car/o;->a:Lcom/google/android/gms/car/n;

    iget v0, v0, Lcom/google/android/gms/car/n;->a:I

    const/16 v5, 0x3e80

    if-ne v0, v5, :cond_1

    const/16 v0, 0x8

    :goto_0
    iget-object v5, p0, Lcom/google/android/gms/car/o;->a:Lcom/google/android/gms/car/n;

    iget v5, v5, Lcom/google/android/gms/car/n;->c:I

    const/4 v6, 0x7

    new-array v6, v6, [B

    const/4 v7, 0x0

    const/4 v8, -0x1

    aput-byte v8, v6, v7

    const/4 v7, 0x1

    const/4 v8, -0x7

    aput-byte v8, v6, v7

    const/4 v7, 0x2

    shl-int/lit8 v0, v0, 0x2

    add-int/lit8 v0, v0, 0x40

    shr-int/lit8 v8, v5, 0x2

    add-int/2addr v0, v8

    int-to-byte v0, v0

    aput-byte v0, v6, v7

    and-int/lit8 v0, v5, 0x3

    shl-int/lit8 v0, v0, 0x6

    shr-int/lit8 v5, v4, 0xb

    add-int/2addr v0, v5

    int-to-byte v0, v0

    aput-byte v0, v6, v1

    const/4 v0, 0x4

    and-int/lit16 v1, v4, 0x7ff

    shr-int/lit8 v1, v1, 0x3

    int-to-byte v1, v1

    aput-byte v1, v6, v0

    const/4 v0, 0x5

    and-int/lit8 v1, v4, 0x7

    shl-int/lit8 v1, v1, 0x5

    add-int/lit8 v1, v1, 0x1f

    int-to-byte v1, v1

    aput-byte v1, v6, v0

    const/4 v0, 0x6

    const/4 v1, -0x4

    aput-byte v1, v6, v0

    invoke-virtual {v3, v6}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    .line 60
    iget-object v0, v2, Lcom/google/android/gms/car/e;->b:Ljava/nio/ByteBuffer;

    invoke-virtual {v0, p1}, Ljava/nio/ByteBuffer;->put(Ljava/nio/ByteBuffer;)Ljava/nio/ByteBuffer;

    .line 61
    iget-object v0, v2, Lcom/google/android/gms/car/e;->b:Ljava/nio/ByteBuffer;

    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->limit()I

    move-result v1

    add-int/lit8 v1, v1, 0x7

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->limit(I)Ljava/nio/Buffer;

    .line 66
    :goto_1
    iget-object v0, p0, Lcom/google/android/gms/car/o;->a:Lcom/google/android/gms/car/n;

    iget-object v0, v0, Lcom/google/android/gms/car/n;->h:Lcom/google/android/gms/car/nz;

    invoke-virtual {v0, v2}, Lcom/google/android/gms/car/nz;->a(Lcom/google/android/gms/car/e;)V

    .line 67
    iget-object v0, p0, Lcom/google/android/gms/car/o;->a:Lcom/google/android/gms/car/n;

    iget-object v0, v0, Lcom/google/android/gms/car/n;->g:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v0}, Ljava/util/concurrent/Semaphore;->release()V

    .line 68
    return-void

    .line 63
    :cond_0
    iget-object v0, v2, Lcom/google/android/gms/car/e;->b:Ljava/nio/ByteBuffer;

    invoke-virtual {v0, p1}, Ljava/nio/ByteBuffer;->put(Ljava/nio/ByteBuffer;)Ljava/nio/ByteBuffer;

    .line 64
    iget-object v0, v2, Lcom/google/android/gms/car/e;->b:Ljava/nio/ByteBuffer;

    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->limit()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->limit(I)Ljava/nio/Buffer;

    goto :goto_1

    :cond_1
    move v0, v1

    goto :goto_0
.end method
