.class public final Lcom/google/android/gms/common/people/data/g;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method public static a(Lcom/google/android/gms/common/people/data/Audience;Lcom/google/android/gms/common/people/data/AudienceMember;)Lcom/google/android/gms/common/people/data/Audience;
    .locals 2

    .prologue
    .line 32
    const-string v0, "Audience must not be null."

    invoke-static {p0, v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 33
    const-string v0, "Audience member must not be null."

    invoke-static {p1, v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 34
    new-instance v0, Ljava/util/LinkedHashSet;

    invoke-virtual {p0}, Lcom/google/android/gms/common/people/data/Audience;->a()Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/LinkedHashSet;-><init>(Ljava/util/Collection;)V

    .line 36
    invoke-virtual {v0, p1}, Ljava/util/LinkedHashSet;->add(Ljava/lang/Object;)Z

    .line 37
    new-instance v1, Lcom/google/android/gms/common/people/data/a;

    invoke-direct {v1, p0}, Lcom/google/android/gms/common/people/data/a;-><init>(Lcom/google/android/gms/common/people/data/Audience;)V

    invoke-virtual {v1, v0}, Lcom/google/android/gms/common/people/data/a;->a(Ljava/util/Collection;)Lcom/google/android/gms/common/people/data/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/common/people/data/a;->a()Lcom/google/android/gms/common/people/data/Audience;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lcom/google/android/gms/common/people/data/Audience;)Z
    .locals 1

    .prologue
    .line 67
    const-string v0, "Audience must not be null."

    invoke-static {p0, v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 68
    invoke-virtual {p0}, Lcom/google/android/gms/common/people/data/Audience;->d()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/gms/common/people/data/Audience;->a()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b(Lcom/google/android/gms/common/people/data/Audience;Lcom/google/android/gms/common/people/data/AudienceMember;)Lcom/google/android/gms/common/people/data/Audience;
    .locals 2

    .prologue
    .line 44
    const-string v0, "Audience must not be null."

    invoke-static {p0, v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 45
    const-string v0, "Audience member must not be null."

    invoke-static {p1, v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 46
    new-instance v0, Ljava/util/LinkedHashSet;

    invoke-virtual {p0}, Lcom/google/android/gms/common/people/data/Audience;->a()Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/LinkedHashSet;-><init>(Ljava/util/Collection;)V

    .line 48
    invoke-virtual {v0, p1}, Ljava/util/LinkedHashSet;->remove(Ljava/lang/Object;)Z

    .line 49
    new-instance v1, Lcom/google/android/gms/common/people/data/a;

    invoke-direct {v1, p0}, Lcom/google/android/gms/common/people/data/a;-><init>(Lcom/google/android/gms/common/people/data/Audience;)V

    invoke-virtual {v1, v0}, Lcom/google/android/gms/common/people/data/a;->a(Ljava/util/Collection;)Lcom/google/android/gms/common/people/data/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/common/people/data/a;->a()Lcom/google/android/gms/common/people/data/Audience;

    move-result-object v0

    return-object v0
.end method
