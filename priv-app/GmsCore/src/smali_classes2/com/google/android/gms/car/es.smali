.class public final Lcom/google/android/gms/car/es;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/car/gw;


# instance fields
.field a:[I

.field b:Landroid/graphics/Point;

.field private c:Lcom/google/android/gms/car/senderprotocol/ah;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/car/senderprotocol/az;)V
    .locals 2

    .prologue
    .line 21
    check-cast p1, Lcom/google/android/gms/car/senderprotocol/ah;

    iput-object p1, p0, Lcom/google/android/gms/car/es;->c:Lcom/google/android/gms/car/senderprotocol/ah;

    .line 22
    iget-object v0, p0, Lcom/google/android/gms/car/es;->c:Lcom/google/android/gms/car/senderprotocol/ah;

    iget-object v1, p0, Lcom/google/android/gms/car/es;->a:[I

    invoke-virtual {v0, v1}, Lcom/google/android/gms/car/senderprotocol/ah;->a([I)V

    .line 23
    return-void
.end method

.method public final a(Ljava/io/PrintWriter;)V
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 31
    const-string v0, "Supported keys: {"

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    move v0, v1

    .line 32
    :goto_0
    iget-object v2, p0, Lcom/google/android/gms/car/es;->a:[I

    array-length v2, v2

    if-ge v0, v2, :cond_0

    .line 33
    const-string v2, "%d, "

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/google/android/gms/car/es;->a:[I

    aget v4, v4, v0

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v1

    invoke-virtual {p1, v2, v3}, Ljava/io/PrintWriter;->printf(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/PrintWriter;

    .line 32
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 35
    :cond_0
    const-string v0, "}"

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 36
    return-void
.end method
