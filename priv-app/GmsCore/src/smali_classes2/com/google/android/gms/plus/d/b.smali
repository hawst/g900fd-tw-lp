.class public abstract Lcom/google/android/gms/plus/d/b;
.super Landroid/os/Binder;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/plus/d/a;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 19
    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    .line 20
    const-string v0, "com.google.android.gms.plus.dynamite.IAddToCirclesButton"

    invoke-virtual {p0, p0, v0}, Lcom/google/android/gms/plus/d/b;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V

    .line 21
    return-void
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .locals 0

    .prologue
    .line 39
    return-object p0
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .locals 8

    .prologue
    const/4 v0, 0x0

    const/4 v6, 0x1

    .line 43
    sparse-switch p1, :sswitch_data_0

    .line 139
    invoke-super {p0, p1, p2, p3, p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    move-result v6

    :goto_0
    return v6

    .line 47
    :sswitch_0
    const-string v0, "com.google.android.gms.plus.dynamite.IAddToCirclesButton"

    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto :goto_0

    .line 52
    :sswitch_1
    const-string v0, "com.google.android.gms.plus.dynamite.IAddToCirclesButton"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 54
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/b/m;->a(Landroid/os/IBinder;)Lcom/google/android/gms/b/l;

    move-result-object v0

    .line 56
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/gms/b/m;->a(Landroid/os/IBinder;)Lcom/google/android/gms/b/l;

    move-result-object v1

    .line 58
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/gms/b/m;->a(Landroid/os/IBinder;)Lcom/google/android/gms/b/l;

    move-result-object v2

    .line 59
    invoke-virtual {p0, v0, v1, v2}, Lcom/google/android/gms/plus/d/b;->a(Lcom/google/android/gms/b/l;Lcom/google/android/gms/b/l;Lcom/google/android/gms/b/l;)V

    .line 60
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto :goto_0

    .line 65
    :sswitch_2
    const-string v1, "com.google.android.gms.plus.dynamite.IAddToCirclesButton"

    invoke-virtual {p2, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 67
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    .line 69
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    .line 71
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v3

    if-eqz v3, :cond_0

    .line 72
    sget-object v3, Lcom/google/android/gms/common/people/data/AudienceMember;->CREATOR:Lcom/google/android/gms/common/people/data/d;

    invoke-static {p2}, Lcom/google/android/gms/common/people/data/d;->a(Landroid/os/Parcel;)Lcom/google/android/gms/common/people/data/AudienceMember;

    move-result-object v3

    .line 78
    :goto_1
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v4

    .line 80
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v5

    if-nez v5, :cond_1

    move-object v5, v0

    :goto_2
    move-object v0, p0

    .line 81
    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/plus/d/b;->a(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/common/people/data/AudienceMember;Ljava/lang/String;Lcom/google/android/gms/plus/d/c;)V

    .line 82
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto :goto_0

    :cond_0
    move-object v3, v0

    .line 75
    goto :goto_1

    .line 80
    :cond_1
    const-string v0, "com.google.android.gms.plus.dynamite.IAddToCirclesCallbacks"

    invoke-interface {v5, v0}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    move-result-object v0

    if-eqz v0, :cond_2

    instance-of v7, v0, Lcom/google/android/gms/plus/d/c;

    if-eqz v7, :cond_2

    check-cast v0, Lcom/google/android/gms/plus/d/c;

    move-object v5, v0

    goto :goto_2

    :cond_2
    new-instance v0, Lcom/google/android/gms/plus/d/e;

    invoke-direct {v0, v5}, Lcom/google/android/gms/plus/d/e;-><init>(Landroid/os/IBinder;)V

    move-object v5, v0

    goto :goto_2

    .line 87
    :sswitch_3
    const-string v1, "com.google.android.gms.plus.dynamite.IAddToCirclesButton"

    invoke-virtual {p2, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 88
    invoke-virtual {p0}, Lcom/google/android/gms/plus/d/b;->a()Lcom/google/android/gms/b/l;

    move-result-object v1

    .line 89
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 90
    if-eqz v1, :cond_3

    invoke-interface {v1}, Lcom/google/android/gms/b/l;->asBinder()Landroid/os/IBinder;

    move-result-object v0

    :cond_3
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    goto/16 :goto_0

    .line 95
    :sswitch_4
    const-string v0, "com.google.android.gms.plus.dynamite.IAddToCirclesButton"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 97
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 98
    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/d/b;->a(I)V

    .line 99
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    .line 104
    :sswitch_5
    const-string v0, "com.google.android.gms.plus.dynamite.IAddToCirclesButton"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 106
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 107
    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/d/b;->b(I)V

    .line 108
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    .line 113
    :sswitch_6
    const-string v0, "com.google.android.gms.plus.dynamite.IAddToCirclesButton"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 114
    invoke-virtual {p0}, Lcom/google/android/gms/plus/d/b;->b()V

    .line 115
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    .line 120
    :sswitch_7
    const-string v0, "com.google.android.gms.plus.dynamite.IAddToCirclesButton"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 122
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_4

    move v0, v6

    .line 123
    :goto_3
    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/d/b;->a(Z)V

    .line 124
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    .line 122
    :cond_4
    const/4 v0, 0x0

    goto :goto_3

    .line 129
    :sswitch_8
    const-string v0, "com.google.android.gms.plus.dynamite.IAddToCirclesButton"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 131
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 133
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 134
    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/plus/d/b;->a(Ljava/lang/String;I)V

    .line 135
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    .line 43
    :sswitch_data_0
    .sparse-switch
        0x2 -> :sswitch_1
        0x3 -> :sswitch_2
        0x4 -> :sswitch_3
        0x5 -> :sswitch_4
        0x6 -> :sswitch_5
        0x7 -> :sswitch_6
        0x8 -> :sswitch_7
        0x9 -> :sswitch_8
        0x5f4e5446 -> :sswitch_0
    .end sparse-switch
.end method
