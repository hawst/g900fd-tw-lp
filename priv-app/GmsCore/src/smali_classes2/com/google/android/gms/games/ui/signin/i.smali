.class public final Lcom/google/android/gms/games/ui/signin/i;
.super Lcom/google/android/gms/games/ui/signin/g;
.source "SourceFile"

# interfaces
.implements Landroid/accounts/AccountManagerCallback;
.implements Lcom/google/android/gms/common/es;
.implements Lcom/google/android/gms/common/et;
.implements Lcom/google/android/gms/plus/internal/ax;


# instance fields
.field private b:Lcom/google/android/gms/plus/internal/PlusCommonExtras;

.field private c:Lcom/google/android/gms/plus/internal/ab;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 45
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/signin/g;-><init>()V

    return-void
.end method

.method private c()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 132
    new-instance v1, Lcom/google/android/gms/common/server/ClientContext;

    invoke-static {}, Landroid/os/Process;->myUid()I

    move-result v0

    iget-object v2, p0, Lcom/google/android/gms/games/ui/signin/g;->a:Lcom/google/android/gms/games/ui/signin/SignInActivity;

    invoke-virtual {v2}, Lcom/google/android/gms/games/ui/signin/SignInActivity;->j()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/gms/games/ui/signin/g;->a:Lcom/google/android/gms/games/ui/signin/SignInActivity;

    invoke-virtual {v3}, Lcom/google/android/gms/games/ui/signin/SignInActivity;->j()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/gms/games/ui/signin/g;->a:Lcom/google/android/gms/games/ui/signin/SignInActivity;

    invoke-virtual {v4}, Lcom/google/android/gms/games/ui/signin/SignInActivity;->h()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v1, v0, v2, v3, v4}, Lcom/google/android/gms/common/server/ClientContext;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 134
    iget-object v0, p0, Lcom/google/android/gms/games/ui/signin/g;->a:Lcom/google/android/gms/games/ui/signin/SignInActivity;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/signin/SignInActivity;->i()[Ljava/lang/String;

    move-result-object v2

    .line 135
    const/4 v0, 0x0

    :goto_0
    array-length v3, v2

    if-ge v0, v3, :cond_0

    .line 136
    aget-object v3, v2, v0

    invoke-virtual {v1, v3}, Lcom/google/android/gms/common/server/ClientContext;->a(Ljava/lang/String;)V

    .line 135
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 139
    :cond_0
    new-instance v0, Lcom/google/android/gms/plus/n;

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/signin/i;->getActivity()Landroid/support/v4/app/q;

    move-result-object v2

    invoke-direct {v0, v2, v1}, Lcom/google/android/gms/plus/n;-><init>(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;)V

    iput v5, v0, Lcom/google/android/gms/plus/n;->b:I

    invoke-virtual {v0}, Lcom/google/android/gms/plus/n;->a()Landroid/content/Intent;

    move-result-object v0

    .line 142
    iget-object v1, p0, Lcom/google/android/gms/games/ui/signin/i;->b:Lcom/google/android/gms/plus/internal/PlusCommonExtras;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/plus/internal/PlusCommonExtras;->a(Landroid/content/Intent;)V

    .line 143
    const/4 v1, 0x5

    invoke-virtual {p0, v1}, Lcom/google/android/gms/games/ui/signin/i;->e(I)V

    .line 144
    invoke-virtual {p0, v0, v5}, Lcom/google/android/gms/games/ui/signin/i;->startActivityForResult(Landroid/content/Intent;I)V

    .line 145
    return-void
.end method


# virtual methods
.method public final T_()V
    .locals 3

    .prologue
    .line 212
    iget-object v0, p0, Lcom/google/android/gms/games/ui/signin/i;->c:Lcom/google/android/gms/plus/internal/ab;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/signin/i;->b:Lcom/google/android/gms/plus/internal/PlusCommonExtras;

    invoke-virtual {v1}, Lcom/google/android/gms/plus/internal/PlusCommonExtras;->b()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-interface {v0, p0, v1, v2}, Lcom/google/android/gms/plus/internal/ab;->a(Lcom/google/android/gms/plus/internal/ax;Ljava/lang/String;Lcom/google/android/gms/plus/service/v1whitelisted/models/UpgradeAccountEntity;)V

    .line 214
    return-void
.end method

.method public final W_()V
    .locals 0

    .prologue
    .line 219
    return-void
.end method

.method public final a()I
    .locals 1

    .prologue
    .line 75
    const/4 v0, 0x4

    return v0
.end method

.method public final a(Lcom/google/android/gms/common/c;)V
    .locals 2

    .prologue
    .line 224
    const-string v0, "PlusCheckFragment"

    const-string v1, "Couldn\'t connect Plus client!"

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 225
    const/16 v0, 0x2712

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/signin/i;->d(I)V

    .line 226
    return-void
.end method

.method public final a(Lcom/google/android/gms/common/c;Lcom/google/android/gms/plus/service/v1whitelisted/models/UpgradeAccountEntity;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 231
    invoke-virtual {p1}, Lcom/google/android/gms/common/c;->b()Z

    move-result v0

    if-nez v0, :cond_0

    .line 232
    const-string v0, "PlusCheckFragment"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Failed to upgrade account; status = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/google/android/gms/common/c;->c()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 233
    const/16 v0, 0x2712

    invoke-virtual {p1}, Lcom/google/android/gms/common/c;->c()I

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/games/ui/signin/i;->a(II)V

    .line 258
    :goto_0
    return-void

    .line 237
    :cond_0
    invoke-static {p2}, Lcom/google/android/gms/plus/oob/af;->a(Lcom/google/android/gms/plus/service/v1whitelisted/models/fi;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 238
    const/4 v0, 0x5

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/signin/i;->b(I)V

    goto :goto_0

    .line 239
    :cond_1
    invoke-static {p2}, Lcom/google/android/gms/plus/oob/af;->c(Lcom/google/android/gms/plus/service/v1whitelisted/models/fi;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 246
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/signin/i;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/games/ui/signin/g;->a:Lcom/google/android/gms/games/ui/signin/SignInActivity;

    invoke-virtual {v1}, Lcom/google/android/gms/games/ui/signin/SignInActivity;->j()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/games/ui/signin/g;->a:Lcom/google/android/gms/games/ui/signin/SignInActivity;

    invoke-virtual {v2}, Lcom/google/android/gms/games/ui/signin/SignInActivity;->h()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/gms/games/ui/signin/i;->b:Lcom/google/android/gms/plus/internal/PlusCommonExtras;

    invoke-static {v0, v1, v2, v3, v4}, Lcom/google/android/gms/plus/oob/UpgradeAccountActivity;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/plus/internal/PlusCommonExtras;I)Landroid/content/Intent;

    move-result-object v0

    .line 249
    invoke-virtual {v0, v4}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 250
    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/games/ui/signin/i;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0

    .line 255
    :cond_2
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/signin/i;->c()V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/games/i/b;)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    const/4 v4, 0x0

    .line 85
    iget-object v0, p0, Lcom/google/android/gms/games/ui/signin/g;->a:Lcom/google/android/gms/games/ui/signin/SignInActivity;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/signin/SignInActivity;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 87
    iget-object v0, p0, Lcom/google/android/gms/games/ui/signin/i;->b:Lcom/google/android/gms/plus/internal/PlusCommonExtras;

    new-instance v1, Lcom/google/android/gms/plus/internal/cn;

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/signin/i;->getActivity()Landroid/support/v4/app/q;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/google/android/gms/plus/internal/cn;-><init>(Landroid/content/Context;)V

    iget-object v2, p0, Lcom/google/android/gms/games/ui/signin/g;->a:Lcom/google/android/gms/games/ui/signin/SignInActivity;

    invoke-virtual {v2}, Lcom/google/android/gms/games/ui/signin/SignInActivity;->j()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/google/android/gms/plus/internal/cn;->a:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/gms/games/ui/signin/g;->a:Lcom/google/android/gms/games/ui/signin/SignInActivity;

    invoke-virtual {v2}, Lcom/google/android/gms/games/ui/signin/SignInActivity;->h()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/google/android/gms/plus/internal/cn;->c:Ljava/lang/String;

    new-array v2, v4, [Ljava/lang/String;

    iput-object v2, v1, Lcom/google/android/gms/plus/internal/cn;->d:[Ljava/lang/String;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    sget-object v3, Lcom/google/android/gms/plus/f;->e:Lcom/google/android/gms/common/api/Scope;

    invoke-virtual {v3}, Lcom/google/android/gms/common/api/Scope;->a()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-virtual {v1, v2}, Lcom/google/android/gms/plus/internal/cn;->a([Ljava/lang/String;)Lcom/google/android/gms/plus/internal/cn;

    move-result-object v1

    iput-object v0, v1, Lcom/google/android/gms/plus/internal/cn;->f:Lcom/google/android/gms/plus/internal/PlusCommonExtras;

    invoke-virtual {v1}, Lcom/google/android/gms/plus/internal/cn;->b()Lcom/google/android/gms/plus/internal/PlusSession;

    move-result-object v0

    sget-object v1, Lcom/google/android/gms/plus/internal/ab;->a:Lcom/google/android/gms/plus/internal/ad;

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/signin/i;->getActivity()Landroid/support/v4/app/q;

    move-result-object v2

    invoke-interface {v1, v2, v0, p0, p0}, Lcom/google/android/gms/plus/internal/ad;->a(Landroid/content/Context;Lcom/google/android/gms/plus/internal/PlusSession;Lcom/google/android/gms/common/es;Lcom/google/android/gms/common/et;)Lcom/google/android/gms/plus/internal/ab;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/signin/i;->c:Lcom/google/android/gms/plus/internal/ab;

    iget-object v0, p0, Lcom/google/android/gms/games/ui/signin/i;->c:Lcom/google/android/gms/plus/internal/ab;

    invoke-interface {v0}, Lcom/google/android/gms/plus/internal/ab;->a()V

    .line 92
    :goto_0
    return-void

    .line 90
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/games/ui/signin/g;->a:Lcom/google/android/gms/games/ui/signin/SignInActivity;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/signin/SignInActivity;->j()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/signin/i;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    invoke-static {v0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v3

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/signin/i;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    iget-object v4, p0, Lcom/google/android/gms/games/ui/signin/g;->a:Lcom/google/android/gms/games/ui/signin/SignInActivity;

    invoke-virtual {v4}, Lcom/google/android/gms/games/ui/signin/SignInActivity;->h()Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, v4}, Lcom/google/android/gms/common/util/a;->f(Landroid/content/Context;Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/accounts/Account;

    iget-object v5, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v5, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    :goto_1
    if-nez v0, :cond_2

    const-string v0, "PlusCheckFragment"

    const-string v1, "Couldn\'t locate selected account!"

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->e(Ljava/lang/String;Ljava/lang/String;)V

    const/16 v0, 0x2712

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/signin/i;->d(I)V

    goto :goto_0

    :cond_2
    sget-object v2, Lcom/google/android/gms/common/internal/aj;->f:[Ljava/lang/String;

    invoke-virtual {v3, v0, v2, p0, v1}, Landroid/accounts/AccountManager;->hasFeatures(Landroid/accounts/Account;[Ljava/lang/String;Landroid/accounts/AccountManagerCallback;Landroid/os/Handler;)Landroid/accounts/AccountManagerFuture;

    goto :goto_0

    :cond_3
    move-object v0, v1

    goto :goto_1
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 80
    const/4 v0, 0x4

    return v0
.end method

.method public final onActivityCreated(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 96
    invoke-super {p0, p1}, Lcom/google/android/gms/games/ui/signin/g;->onActivityCreated(Landroid/os/Bundle;)V

    .line 97
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/signin/i;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/q;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/plus/internal/PlusCommonExtras;->b(Landroid/content/Intent;)Lcom/google/android/gms/plus/internal/PlusCommonExtras;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/signin/i;->b:Lcom/google/android/gms/plus/internal/PlusCommonExtras;

    .line 98
    return-void
.end method

.method public final onActivityResult(IILandroid/content/Intent;)V
    .locals 6

    .prologue
    const/16 v5, 0x2712

    const/4 v4, 0x6

    const/4 v2, 0x5

    const/4 v3, 0x1

    const/4 v1, -0x1

    .line 149
    invoke-super {p0, p1, p2, p3}, Lcom/google/android/gms/games/ui/signin/g;->onActivityResult(IILandroid/content/Intent;)V

    .line 150
    if-ne p1, v3, :cond_4

    .line 151
    if-ne p2, v1, :cond_1

    .line 153
    invoke-virtual {p0, v4}, Lcom/google/android/gms/games/ui/signin/i;->e(I)V

    .line 154
    invoke-virtual {p0, v2}, Lcom/google/android/gms/games/ui/signin/i;->b(I)V

    .line 181
    :cond_0
    :goto_0
    return-void

    .line 155
    :cond_1
    const/16 v0, 0x2711

    if-ne p2, v0, :cond_3

    .line 160
    iget-object v0, p0, Lcom/google/android/gms/games/ui/signin/g;->a:Lcom/google/android/gms/games/ui/signin/SignInActivity;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/signin/SignInActivity;->e()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 161
    const/4 v0, 0x4

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/signin/i;->b(I)V

    goto :goto_0

    .line 164
    :cond_2
    invoke-virtual {p0, v5}, Lcom/google/android/gms/games/ui/signin/i;->d(I)V

    goto :goto_0

    .line 167
    :cond_3
    const/4 v0, 0x7

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/signin/i;->e(I)V

    .line 168
    const-string v0, "PlusCheckFragment"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "User failed to complete G+ OOB - result code "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 169
    invoke-virtual {p0, v3}, Lcom/google/android/gms/games/ui/signin/i;->b(I)V

    goto :goto_0

    .line 171
    :cond_4
    const/4 v0, 0x2

    if-ne p1, v0, :cond_0

    .line 172
    if-ne p2, v1, :cond_5

    .line 174
    invoke-virtual {p0, v4}, Lcom/google/android/gms/games/ui/signin/i;->e(I)V

    .line 175
    invoke-virtual {p0, v2}, Lcom/google/android/gms/games/ui/signin/i;->b(I)V

    goto :goto_0

    .line 178
    :cond_5
    invoke-virtual {p0, v5}, Lcom/google/android/gms/games/ui/signin/i;->d(I)V

    goto :goto_0
.end method

.method public final onStop()V
    .locals 1

    .prologue
    .line 65
    invoke-super {p0}, Lcom/google/android/gms/games/ui/signin/g;->onStop()V

    .line 68
    iget-object v0, p0, Lcom/google/android/gms/games/ui/signin/i;->c:Lcom/google/android/gms/plus/internal/ab;

    if-eqz v0, :cond_0

    .line 69
    iget-object v0, p0, Lcom/google/android/gms/games/ui/signin/i;->c:Lcom/google/android/gms/plus/internal/ab;

    invoke-interface {v0}, Lcom/google/android/gms/plus/internal/ab;->b()V

    .line 71
    :cond_0
    return-void
.end method

.method public final run(Landroid/accounts/AccountManagerFuture;)V
    .locals 4

    .prologue
    const/16 v3, 0x2712

    .line 103
    :try_start_0
    invoke-interface {p1}, Landroid/accounts/AccountManagerFuture;->getResult()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    .line 106
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/signin/i;->g()Z

    move-result v1

    if-nez v1, :cond_0

    .line 128
    :goto_0
    return-void

    .line 110
    :cond_0
    if-eqz v0, :cond_1

    .line 112
    const/4 v0, 0x5

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/signin/i;->b(I)V
    :try_end_0
    .catch Landroid/accounts/AuthenticatorException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/accounts/OperationCanceledException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2

    goto :goto_0

    .line 117
    :catch_0
    move-exception v0

    .line 118
    const-string v1, "PlusCheckFragment"

    const-string v2, "Auth error checking G+ sign-up status"

    invoke-static {v1, v2, v0}, Lcom/google/android/gms/games/internal/dq;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 119
    invoke-virtual {p0, v3}, Lcom/google/android/gms/games/ui/signin/i;->d(I)V

    goto :goto_0

    .line 115
    :cond_1
    :try_start_1
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/signin/i;->c()V
    :try_end_1
    .catch Landroid/accounts/AuthenticatorException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Landroid/accounts/OperationCanceledException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2

    goto :goto_0

    .line 120
    :catch_1
    move-exception v0

    .line 121
    const-string v1, "PlusCheckFragment"

    const-string v2, "Canceled error checking G+ sign-up status"

    invoke-static {v1, v2, v0}, Lcom/google/android/gms/games/internal/dq;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 122
    invoke-virtual {p0, v3}, Lcom/google/android/gms/games/ui/signin/i;->d(I)V

    goto :goto_0

    .line 123
    :catch_2
    move-exception v0

    .line 124
    const-string v1, "PlusCheckFragment"

    const-string v2, "Network error checking G+ sign-up status"

    invoke-static {v1, v2, v0}, Lcom/google/android/gms/games/internal/dq;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 125
    const/4 v0, 0x6

    invoke-virtual {p0, v3, v0}, Lcom/google/android/gms/games/ui/signin/i;->a(II)V

    goto :goto_0
.end method
