.class public Lcom/google/android/gms/icing/ui/IcingPrivacyActivity;
.super Landroid/support/v7/app/d;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private a:Lcom/google/android/gms/common/api/v;

.field private b:Landroid/view/View;

.field private c:Landroid/view/View;

.field private d:Landroid/view/View;

.field private e:Landroid/widget/CheckBox;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0}, Landroid/support/v7/app/d;-><init>()V

    .line 130
    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/icing/ui/IcingPrivacyActivity;)Landroid/view/View;
    .locals 1

    .prologue
    .line 25
    iget-object v0, p0, Lcom/google/android/gms/icing/ui/IcingPrivacyActivity;->b:Landroid/view/View;

    return-object v0
.end method

.method static synthetic b(Lcom/google/android/gms/icing/ui/IcingPrivacyActivity;)Landroid/view/View;
    .locals 1

    .prologue
    .line 25
    iget-object v0, p0, Lcom/google/android/gms/icing/ui/IcingPrivacyActivity;->c:Landroid/view/View;

    return-object v0
.end method

.method static synthetic c(Lcom/google/android/gms/icing/ui/IcingPrivacyActivity;)Landroid/widget/CheckBox;
    .locals 1

    .prologue
    .line 25
    iget-object v0, p0, Lcom/google/android/gms/icing/ui/IcingPrivacyActivity;->e:Landroid/widget/CheckBox;

    return-object v0
.end method

.method static synthetic d(Lcom/google/android/gms/icing/ui/IcingPrivacyActivity;)Landroid/view/View;
    .locals 1

    .prologue
    .line 25
    iget-object v0, p0, Lcom/google/android/gms/icing/ui/IcingPrivacyActivity;->d:Landroid/view/View;

    return-object v0
.end method

.method private e()V
    .locals 2

    .prologue
    .line 100
    sget v0, Lcom/google/android/gms/p;->py:I

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 102
    return-void
.end method

.method static synthetic e(Lcom/google/android/gms/icing/ui/IcingPrivacyActivity;)V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0}, Lcom/google/android/gms/icing/ui/IcingPrivacyActivity;->e()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 72
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    sget v1, Lcom/google/android/gms/j;->jO:I

    if-ne v0, v1, :cond_1

    .line 73
    invoke-static {}, Lcom/google/android/gms/icing/ui/i;->b()Lcom/google/android/gms/icing/ui/i;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/gms/icing/ui/IcingPrivacyActivity;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/icing/ui/i;->a(Landroid/support/v4/app/v;Ljava/lang/String;)V

    .line 97
    :cond_0
    :goto_0
    return-void

    .line 74
    :cond_1
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    sget v1, Lcom/google/android/gms/j;->jT:I

    if-ne v0, v1, :cond_0

    .line 75
    iget-object v0, p0, Lcom/google/android/gms/icing/ui/IcingPrivacyActivity;->a:Lcom/google/android/gms/common/api/v;

    invoke-interface {v0}, Lcom/google/android/gms/common/api/v;->f()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 76
    iget-object v0, p0, Lcom/google/android/gms/icing/ui/IcingPrivacyActivity;->e:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    .line 77
    iget-object v1, p0, Lcom/google/android/gms/icing/ui/IcingPrivacyActivity;->e:Landroid/widget/CheckBox;

    invoke-virtual {v1}, Landroid/widget/CheckBox;->toggle()V

    .line 78
    iget-object v1, p0, Lcom/google/android/gms/icing/ui/IcingPrivacyActivity;->e:Landroid/widget/CheckBox;

    invoke-virtual {v1}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v1

    .line 79
    iget-object v2, p0, Lcom/google/android/gms/icing/ui/IcingPrivacyActivity;->d:Landroid/view/View;

    invoke-virtual {v2, v3}, Landroid/view/View;->setEnabled(Z)V

    .line 80
    iget-object v2, p0, Lcom/google/android/gms/icing/ui/IcingPrivacyActivity;->e:Landroid/widget/CheckBox;

    invoke-virtual {v2, v3}, Landroid/widget/CheckBox;->setEnabled(Z)V

    .line 81
    sget-object v2, Lcom/google/android/gms/appdatasearch/a;->c:Lcom/google/android/gms/appdatasearch/bg;

    iget-object v3, p0, Lcom/google/android/gms/icing/ui/IcingPrivacyActivity;->a:Lcom/google/android/gms/common/api/v;

    invoke-interface {v2, v3, v1}, Lcom/google/android/gms/appdatasearch/bg;->a(Lcom/google/android/gms/common/api/v;Z)Lcom/google/android/gms/common/api/am;

    move-result-object v1

    new-instance v2, Lcom/google/android/gms/icing/ui/h;

    invoke-direct {v2, p0, v0}, Lcom/google/android/gms/icing/ui/h;-><init>(Lcom/google/android/gms/icing/ui/IcingPrivacyActivity;Z)V

    invoke-interface {v1, v2}, Lcom/google/android/gms/common/api/am;->a(Lcom/google/android/gms/common/api/aq;)V

    goto :goto_0

    .line 94
    :cond_2
    invoke-direct {p0}, Lcom/google/android/gms/icing/ui/IcingPrivacyActivity;->e()V

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 36
    invoke-super {p0, p1}, Landroid/support/v7/app/d;->onCreate(Landroid/os/Bundle;)V

    .line 37
    new-instance v0, Lcom/google/android/gms/common/api/w;

    invoke-direct {v0, p0}, Lcom/google/android/gms/common/api/w;-><init>(Landroid/content/Context;)V

    sget-object v1, Lcom/google/android/gms/appdatasearch/a;->b:Lcom/google/android/gms/common/api/c;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/api/w;->a(Lcom/google/android/gms/common/api/c;)Lcom/google/android/gms/common/api/w;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/common/api/w;->a()Lcom/google/android/gms/common/api/v;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/icing/ui/IcingPrivacyActivity;->a:Lcom/google/android/gms/common/api/v;

    .line 39
    sget v0, Lcom/google/android/gms/l;->ct:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/icing/ui/IcingPrivacyActivity;->setContentView(I)V

    .line 40
    sget v0, Lcom/google/android/gms/j;->jP:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/icing/ui/IcingPrivacyActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/icing/ui/IcingPrivacyActivity;->b:Landroid/view/View;

    .line 41
    sget v0, Lcom/google/android/gms/j;->jQ:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/icing/ui/IcingPrivacyActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/icing/ui/IcingPrivacyActivity;->c:Landroid/view/View;

    .line 42
    sget v0, Lcom/google/android/gms/j;->jU:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/icing/ui/IcingPrivacyActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lcom/google/android/gms/icing/ui/IcingPrivacyActivity;->e:Landroid/widget/CheckBox;

    .line 44
    sget v0, Lcom/google/android/gms/j;->jT:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/icing/ui/IcingPrivacyActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/icing/ui/IcingPrivacyActivity;->d:Landroid/view/View;

    .line 45
    iget-object v0, p0, Lcom/google/android/gms/icing/ui/IcingPrivacyActivity;->d:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 46
    sget v0, Lcom/google/android/gms/j;->jO:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/icing/ui/IcingPrivacyActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 47
    return-void
.end method

.method protected onPause()V
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lcom/google/android/gms/icing/ui/IcingPrivacyActivity;->a:Lcom/google/android/gms/common/api/v;

    invoke-interface {v0}, Lcom/google/android/gms/common/api/v;->d()V

    .line 67
    invoke-super {p0}, Landroid/support/v7/app/d;->onPause()V

    .line 68
    return-void
.end method

.method protected onResume()V
    .locals 2

    .prologue
    .line 51
    invoke-super {p0}, Landroid/support/v7/app/d;->onResume()V

    .line 52
    iget-object v0, p0, Lcom/google/android/gms/icing/ui/IcingPrivacyActivity;->a:Lcom/google/android/gms/common/api/v;

    invoke-interface {v0}, Lcom/google/android/gms/common/api/v;->b()V

    .line 53
    sget-object v0, Lcom/google/android/gms/appdatasearch/a;->c:Lcom/google/android/gms/appdatasearch/bg;

    iget-object v1, p0, Lcom/google/android/gms/icing/ui/IcingPrivacyActivity;->a:Lcom/google/android/gms/common/api/v;

    invoke-interface {v0, v1}, Lcom/google/android/gms/appdatasearch/bg;->b(Lcom/google/android/gms/common/api/v;)Lcom/google/android/gms/common/api/am;

    move-result-object v0

    new-instance v1, Lcom/google/android/gms/icing/ui/g;

    invoke-direct {v1, p0}, Lcom/google/android/gms/icing/ui/g;-><init>(Lcom/google/android/gms/icing/ui/IcingPrivacyActivity;)V

    invoke-interface {v0, v1}, Lcom/google/android/gms/common/api/am;->a(Lcom/google/android/gms/common/api/aq;)V

    .line 62
    return-void
.end method
