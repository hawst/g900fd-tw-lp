.class public final Lcom/google/android/gms/http/e;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/io/Closeable;


# static fields
.field private static final a:Landroid/content/ComponentName;

.field private static final b:Landroid/content/Intent;


# instance fields
.field private c:Landroid/content/Context;

.field private d:Landroid/content/ServiceConnection;

.field private volatile e:Lcom/google/android/gms/http/g;

.field private f:Ljava/util/concurrent/CountDownLatch;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 40
    new-instance v0, Landroid/content/ComponentName;

    const-string v1, "com.google.android.gms"

    const-string v2, "com.google.android.gms.gcm.http.GoogleHttpService"

    invoke-direct {v0, v1, v2}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/gms/http/e;->a:Landroid/content/ComponentName;

    .line 43
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string v1, "com.google.android.gms"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    sget-object v1, Lcom/google/android/gms/http/e;->a:Landroid/content/ComponentName;

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/http/e;->b:Landroid/content/Intent;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 77
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 78
    iput-object p1, p0, Lcom/google/android/gms/http/e;->c:Landroid/content/Context;

    .line 79
    new-instance v0, Ljava/util/concurrent/CountDownLatch;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/gms/http/e;->f:Ljava/util/concurrent/CountDownLatch;

    .line 80
    new-instance v0, Lcom/google/android/gms/http/f;

    invoke-direct {v0, p0}, Lcom/google/android/gms/http/f;-><init>(Lcom/google/android/gms/http/e;)V

    iput-object v0, p0, Lcom/google/android/gms/http/e;->d:Landroid/content/ServiceConnection;

    .line 94
    invoke-direct {p0}, Lcom/google/android/gms/http/e;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 95
    const-string v0, "GoogleURLConnFactory"

    const-string v1, "Failed to bind Google HTTP Service"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 97
    invoke-direct {p0}, Lcom/google/android/gms/http/e;->b()V

    .line 99
    iget-object v0, p0, Lcom/google/android/gms/http/e;->f:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    .line 101
    :cond_0
    return-void
.end method

.method private a(Ljava/lang/String;)Landroid/os/Bundle;
    .locals 4

    .prologue
    .line 190
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/http/e;->e:Lcom/google/android/gms/http/g;

    if-nez v0, :cond_0

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Looper;->getThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 197
    iget-object v0, p0, Lcom/google/android/gms/http/e;->f:Ljava/util/concurrent/CountDownLatch;

    const-wide/16 v2, 0x1f4

    sget-object v1, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v2, v3, v1}, Ljava/util/concurrent/CountDownLatch;->await(JLjava/util/concurrent/TimeUnit;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 198
    const-string v0, "GoogleURLConnFactory"

    const-string v1, "Timeout on service connection"

    new-instance v2, Ljava/lang/Throwable;

    invoke-direct {v2}, Ljava/lang/Throwable;-><init>()V

    invoke-static {v0, v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 199
    iget-object v0, p0, Lcom/google/android/gms/http/e;->f:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    .line 202
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/http/e;->e:Lcom/google/android/gms/http/g;

    if-eqz v0, :cond_1

    .line 203
    iget-object v0, p0, Lcom/google/android/gms/http/e;->e:Lcom/google/android/gms/http/g;

    invoke-interface {v0, p1}, Lcom/google/android/gms/http/g;->a(Ljava/lang/String;)Landroid/os/Bundle;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v0

    .line 216
    :goto_0
    return-object v0

    .line 205
    :catch_0
    move-exception v0

    .line 206
    const-string v1, "GoogleURLConnFactory"

    const-string v2, "Exception in Google Http Service: "

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 213
    :cond_1
    :goto_1
    const-string v0, "GoogleURLConnFactory"

    const/4 v1, 0x2

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 214
    const-string v0, "GoogleURLConnFactory"

    const-string v1, "Unable to get rewrite rules from HttpService"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 216
    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    .line 207
    :catch_1
    move-exception v0

    .line 208
    const-string v1, "GoogleURLConnFactory"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Interrupted waiting for binder: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method static synthetic a(Lcom/google/android/gms/http/e;Lcom/google/android/gms/http/g;)Lcom/google/android/gms/http/g;
    .locals 0

    .prologue
    .line 38
    iput-object p1, p0, Lcom/google/android/gms/http/e;->e:Lcom/google/android/gms/http/g;

    return-object p1
.end method

.method static synthetic a(Lcom/google/android/gms/http/e;)Ljava/util/concurrent/CountDownLatch;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lcom/google/android/gms/http/e;->f:Ljava/util/concurrent/CountDownLatch;

    return-object v0
.end method

.method private a()Z
    .locals 5

    .prologue
    .line 222
    const-string v0, "GoogleURLConnFactory"

    const-string v1, "binding HttpService"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 223
    invoke-static {}, Lcom/google/android/gms/common/stats/b;->a()Lcom/google/android/gms/common/stats/b;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/http/e;->c:Landroid/content/Context;

    sget-object v2, Lcom/google/android/gms/http/e;->b:Landroid/content/Intent;

    iget-object v3, p0, Lcom/google/android/gms/http/e;->d:Landroid/content/ServiceConnection;

    const/4 v4, 0x1

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/google/android/gms/common/stats/b;->a(Landroid/content/Context;Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    move-result v0

    return v0
.end method

.method private b()V
    .locals 4

    .prologue
    .line 229
    const-string v0, "GoogleURLConnFactory"

    const-string v1, "unbinding HttpService"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 231
    :try_start_0
    invoke-static {}, Lcom/google/android/gms/common/stats/b;->a()Lcom/google/android/gms/common/stats/b;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/http/e;->c:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/gms/http/e;->d:Landroid/content/ServiceConnection;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/common/stats/b;->a(Landroid/content/Context;Landroid/content/ServiceConnection;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 235
    :goto_0
    return-void

    .line 232
    :catch_0
    move-exception v0

    .line 233
    const-string v1, "GoogleURLConnFactory"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "error unbinding HttpService: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method static synthetic b(Lcom/google/android/gms/http/e;)Z
    .locals 1

    .prologue
    .line 38
    invoke-direct {p0}, Lcom/google/android/gms/http/e;->a()Z

    move-result v0

    return v0
.end method


# virtual methods
.method public final a(Ljava/net/URL;)Ljava/net/HttpURLConnection;
    .locals 4

    .prologue
    .line 119
    invoke-static {p1}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 120
    invoke-virtual {p1}, Ljava/net/URL;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/gms/http/e;->a(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    .line 121
    if-eqz v0, :cond_3

    .line 122
    const-string v1, "block"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 123
    const-string v1, "GoogleURLConnFactory"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Blocked by "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v3, "name"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ": "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 124
    new-instance v1, Lcom/google/android/gms/http/a;

    invoke-direct {v1, v0}, Lcom/google/android/gms/http/a;-><init>(Landroid/os/Bundle;)V

    throw v1

    .line 127
    :cond_0
    const-string v1, "rewrite"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 128
    if-eqz v0, :cond_3

    invoke-static {v0}, Landroid/webkit/URLUtil;->isHttpUrl(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    invoke-static {v0}, Landroid/webkit/URLUtil;->isHttpsUrl(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 130
    :cond_1
    const-string v1, "GoogleURLConnFactory"

    const/4 v2, 0x3

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 131
    const-string v1, "GoogleURLConnFactory"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Rewrote "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/net/URL;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " to "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 133
    :cond_2
    new-instance v1, Ljava/net/URL;

    invoke-direct {v1, v0}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/net/URL;->openConnection()Ljava/net/URLConnection;

    move-result-object v0

    check-cast v0, Ljava/net/HttpURLConnection;

    .line 136
    :goto_0
    return-object v0

    :cond_3
    invoke-virtual {p1}, Ljava/net/URL;->openConnection()Ljava/net/URLConnection;

    move-result-object v0

    check-cast v0, Ljava/net/HttpURLConnection;

    goto :goto_0
.end method

.method final a(Ljava/lang/String;I)V
    .locals 3

    .prologue
    .line 165
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/http/e;->e:Lcom/google/android/gms/http/g;

    if-eqz v0, :cond_0

    if-lez p2, :cond_0

    .line 166
    iget-object v0, p0, Lcom/google/android/gms/http/e;->e:Lcom/google/android/gms/http/g;

    invoke-interface {v0, p1, p2}, Lcom/google/android/gms/http/g;->a(Ljava/lang/String;I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 171
    :cond_0
    :goto_0
    return-void

    .line 168
    :catch_0
    move-exception v0

    .line 169
    const-string v1, "GoogleURLConnFactory"

    const-string v2, "Exception in Google Http Service: "

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public final a(Ljava/net/HttpURLConnection;I)V
    .locals 1

    .prologue
    .line 150
    if-nez p1, :cond_0

    .line 155
    :goto_0
    return-void

    .line 153
    :cond_0
    invoke-virtual {p1}, Ljava/net/HttpURLConnection;->getURL()Ljava/net/URL;

    move-result-object v0

    invoke-virtual {v0}, Ljava/net/URL;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0, p2}, Lcom/google/android/gms/http/e;->a(Ljava/lang/String;I)V

    .line 154
    invoke-virtual {p1}, Ljava/net/HttpURLConnection;->disconnect()V

    goto :goto_0
.end method

.method public final close()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 179
    iget-object v0, p0, Lcom/google/android/gms/http/e;->d:Landroid/content/ServiceConnection;

    if-eqz v0, :cond_0

    .line 180
    invoke-direct {p0}, Lcom/google/android/gms/http/e;->b()V

    .line 181
    iput-object v1, p0, Lcom/google/android/gms/http/e;->d:Landroid/content/ServiceConnection;

    .line 182
    iput-object v1, p0, Lcom/google/android/gms/http/e;->e:Lcom/google/android/gms/http/g;

    .line 184
    :cond_0
    return-void
.end method
