.class final Lcom/google/android/gms/car/a/n;
.super Landroid/content/BroadcastReceiver;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/gms/car/a/k;


# direct methods
.method private constructor <init>(Lcom/google/android/gms/car/a/k;)V
    .locals 0

    .prologue
    .line 587
    iput-object p1, p0, Lcom/google/android/gms/car/a/n;->a:Lcom/google/android/gms/car/a/k;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/gms/car/a/k;B)V
    .locals 0

    .prologue
    .line 587
    invoke-direct {p0, p1}, Lcom/google/android/gms/car/a/n;-><init>(Lcom/google/android/gms/car/a/k;)V

    return-void
.end method


# virtual methods
.method public final onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 8

    .prologue
    const/16 v7, 0xc

    const/16 v6, 0xa

    const/4 v5, 0x3

    .line 590
    const-string v0, "CAR.BT"

    invoke-static {v0, v5}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 591
    const-string v0, "CAR.BT"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "AdapterStateChangeReceiver#onReceive: intent="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 593
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/car/a/n;->a:Lcom/google/android/gms/car/a/k;

    iget v0, v0, Lcom/google/android/gms/car/a/k;->k:I

    if-eqz v0, :cond_2

    .line 594
    const-string v0, "CAR.BT"

    invoke-static {v0, v5}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 595
    const-string v0, "CAR.BT"

    const-string v1, "BluetoothUtil not initialized or being cleaned up"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 632
    :cond_1
    :goto_0
    return-void

    .line 599
    :cond_2
    const-string v0, "android.bluetooth.adapter.action.STATE_CHANGED"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 600
    const-string v0, "CAR.BT"

    const-string v1, "AdapterStateChangeReceiver: Wrong intent. This shouldn\'t happen"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 604
    :cond_3
    const-string v0, "android.bluetooth.adapter.extra.STATE"

    const/4 v1, -0x1

    invoke-virtual {p2, v0, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    .line 605
    const-string v0, "CAR.BT"

    invoke-static {v0, v5}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 606
    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    .line 607
    if-ne v1, v6, :cond_7

    .line 608
    const-string v0, "OFF"

    .line 616
    :cond_4
    :goto_1
    const-string v2, "CAR.BT"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "new state="

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 618
    :cond_5
    if-ne v1, v7, :cond_a

    .line 619
    const-string v0, "CAR.BT"

    invoke-static {v0, v5}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 620
    const-string v0, "CAR.BT"

    const-string v1, "Bluetooth has just been enabled"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 622
    :cond_6
    iget-object v0, p0, Lcom/google/android/gms/car/a/n;->a:Lcom/google/android/gms/car/a/k;

    iget-object v0, v0, Lcom/google/android/gms/car/a/k;->b:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothAdapter;->cancelDiscovery()Z

    .line 623
    iget-object v0, p0, Lcom/google/android/gms/car/a/n;->a:Lcom/google/android/gms/car/a/k;

    iget-object v0, v0, Lcom/google/android/gms/car/a/k;->e:Lcom/google/android/gms/car/a/p;

    invoke-interface {v0}, Lcom/google/android/gms/car/a/p;->a()V

    goto :goto_0

    .line 609
    :cond_7
    const/16 v2, 0xb

    if-ne v1, v2, :cond_8

    .line 610
    const-string v0, "TURNING_ON"

    goto :goto_1

    .line 611
    :cond_8
    if-ne v1, v7, :cond_9

    .line 612
    const-string v0, "ON"

    goto :goto_1

    .line 613
    :cond_9
    const/16 v2, 0xd

    if-ne v1, v2, :cond_4

    .line 614
    const-string v0, "TURNING_OFF"

    goto :goto_1

    .line 624
    :cond_a
    if-ne v1, v6, :cond_1

    .line 625
    const-string v0, "CAR.BT"

    invoke-static {v0, v5}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 626
    const-string v0, "CAR.BT"

    const-string v1, "Bluetooth disabled"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 628
    :cond_b
    iget-object v0, p0, Lcom/google/android/gms/car/a/n;->a:Lcom/google/android/gms/car/a/k;

    iget-object v0, v0, Lcom/google/android/gms/car/a/k;->l:Lcom/google/android/gms/car/a/h;

    invoke-virtual {v0}, Lcom/google/android/gms/car/a/h;->e()V

    .line 629
    iget-object v0, p0, Lcom/google/android/gms/car/a/n;->a:Lcom/google/android/gms/car/a/k;

    const/4 v1, 0x0

    iput-object v1, v0, Lcom/google/android/gms/car/a/k;->l:Lcom/google/android/gms/car/a/h;

    .line 630
    iget-object v0, p0, Lcom/google/android/gms/car/a/n;->a:Lcom/google/android/gms/car/a/k;

    iget-object v0, v0, Lcom/google/android/gms/car/a/k;->e:Lcom/google/android/gms/car/a/p;

    invoke-interface {v0}, Lcom/google/android/gms/car/a/p;->b()V

    goto/16 :goto_0
.end method
