.class public Lcom/google/android/gms/icing/ui/IcingManageSpaceActivity;
.super Landroid/support/v7/app/d;
.source "SourceFile"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# instance fields
.field private a:Landroid/view/View;

.field private b:Landroid/view/View;

.field private c:Landroid/widget/TextView;

.field private d:Landroid/widget/ListView;

.field private e:Landroid/widget/TextView;

.field private f:Lcom/google/android/gms/icing/ui/c;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0}, Landroid/support/v7/app/d;-><init>()V

    .line 181
    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/icing/ui/IcingManageSpaceActivity;)Landroid/view/View;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lcom/google/android/gms/icing/ui/IcingManageSpaceActivity;->a:Landroid/view/View;

    return-object v0
.end method

.method static synthetic b(Lcom/google/android/gms/icing/ui/IcingManageSpaceActivity;)Landroid/view/View;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lcom/google/android/gms/icing/ui/IcingManageSpaceActivity;->b:Landroid/view/View;

    return-object v0
.end method

.method static synthetic c(Lcom/google/android/gms/icing/ui/IcingManageSpaceActivity;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lcom/google/android/gms/icing/ui/IcingManageSpaceActivity;->c:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic d(Lcom/google/android/gms/icing/ui/IcingManageSpaceActivity;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lcom/google/android/gms/icing/ui/IcingManageSpaceActivity;->e:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic e(Lcom/google/android/gms/icing/ui/IcingManageSpaceActivity;)Landroid/widget/ListView;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lcom/google/android/gms/icing/ui/IcingManageSpaceActivity;->d:Landroid/widget/ListView;

    return-object v0
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 46
    invoke-super {p0, p1}, Landroid/support/v7/app/d;->onCreate(Landroid/os/Bundle;)V

    .line 47
    sget v0, Lcom/google/android/gms/l;->cs:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/icing/ui/IcingManageSpaceActivity;->setContentView(I)V

    .line 49
    sget v0, Lcom/google/android/gms/j;->lD:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/icing/ui/IcingManageSpaceActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/icing/ui/IcingManageSpaceActivity;->a:Landroid/view/View;

    .line 50
    sget v0, Lcom/google/android/gms/j;->jQ:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/icing/ui/IcingManageSpaceActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/icing/ui/IcingManageSpaceActivity;->b:Landroid/view/View;

    .line 52
    sget v0, Lcom/google/android/gms/j;->tj:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/icing/ui/IcingManageSpaceActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/gms/icing/ui/IcingManageSpaceActivity;->c:Landroid/widget/TextView;

    .line 53
    sget v0, Lcom/google/android/gms/j;->aL:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/icing/ui/IcingManageSpaceActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/google/android/gms/icing/ui/IcingManageSpaceActivity;->d:Landroid/widget/ListView;

    .line 55
    iget-object v0, p0, Lcom/google/android/gms/icing/ui/IcingManageSpaceActivity;->d:Landroid/widget/ListView;

    invoke-virtual {v0, p0}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 56
    sget v0, Lcom/google/android/gms/j;->ft:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/icing/ui/IcingManageSpaceActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/gms/icing/ui/IcingManageSpaceActivity;->e:Landroid/widget/TextView;

    .line 57
    iget-object v0, p0, Lcom/google/android/gms/icing/ui/IcingManageSpaceActivity;->d:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/google/android/gms/icing/ui/IcingManageSpaceActivity;->e:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setEmptyView(Landroid/view/View;)V

    .line 58
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 2

    .prologue
    .line 76
    invoke-virtual {p0}, Lcom/google/android/gms/icing/ui/IcingManageSpaceActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    .line 77
    sget v1, Lcom/google/android/gms/m;->x:I

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 78
    const/4 v0, 0x1

    return v0
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 4

    .prologue
    .line 94
    iget-object v0, p0, Lcom/google/android/gms/icing/ui/IcingManageSpaceActivity;->d:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/icing/ui/d;

    .line 95
    if-nez v0, :cond_1

    .line 98
    :cond_0
    :goto_0
    return-void

    .line 96
    :cond_1
    if-ltz p3, :cond_0

    invoke-virtual {v0}, Lcom/google/android/gms/icing/ui/d;->getCount()I

    move-result v1

    if-ge p3, v1, :cond_0

    .line 97
    invoke-virtual {v0, p3}, Lcom/google/android/gms/icing/ui/d;->a(I)Lcom/google/android/gms/icing/ui/m;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/gms/icing/ui/m;->a:Ljava/lang/String;

    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    const-string v2, "android.settings.APPLICATION_DETAILS_SETTINGS"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    const-string v2, "package"

    const/4 v3, 0x0

    invoke-static {v2, v0, v3}, Landroid/net/Uri;->fromParts(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    invoke-virtual {p0, v1}, Lcom/google/android/gms/icing/ui/IcingManageSpaceActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 3

    .prologue
    .line 83
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    sget v1, Lcom/google/android/gms/j;->jR:I

    if-ne v0, v1, :cond_0

    .line 84
    sget-object v0, Lcom/google/android/gms/icing/a/a;->H:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 85
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.VIEW"

    invoke-direct {v1, v2, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 86
    invoke-virtual {p0, v1}, Lcom/google/android/gms/icing/ui/IcingManageSpaceActivity;->startActivity(Landroid/content/Intent;)V

    .line 87
    const/4 v0, 0x1

    .line 89
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Landroid/support/v7/app/d;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    goto :goto_0
.end method

.method protected onPause()V
    .locals 2

    .prologue
    .line 69
    iget-object v0, p0, Lcom/google/android/gms/icing/ui/IcingManageSpaceActivity;->f:Lcom/google/android/gms/icing/ui/c;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/icing/ui/c;->cancel(Z)Z

    .line 70
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/icing/ui/IcingManageSpaceActivity;->f:Lcom/google/android/gms/icing/ui/c;

    .line 71
    invoke-super {p0}, Landroid/support/v7/app/d;->onPause()V

    .line 72
    return-void
.end method

.method protected onResume()V
    .locals 2

    .prologue
    .line 62
    invoke-super {p0}, Landroid/support/v7/app/d;->onResume()V

    .line 63
    new-instance v0, Lcom/google/android/gms/icing/ui/c;

    invoke-direct {v0, p0}, Lcom/google/android/gms/icing/ui/c;-><init>(Lcom/google/android/gms/icing/ui/IcingManageSpaceActivity;)V

    iput-object v0, p0, Lcom/google/android/gms/icing/ui/IcingManageSpaceActivity;->f:Lcom/google/android/gms/icing/ui/c;

    .line 64
    iget-object v0, p0, Lcom/google/android/gms/icing/ui/IcingManageSpaceActivity;->f:Lcom/google/android/gms/icing/ui/c;

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/icing/ui/c;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 65
    return-void
.end method
