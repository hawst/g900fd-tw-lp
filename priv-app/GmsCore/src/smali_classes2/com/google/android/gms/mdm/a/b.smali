.class public final Lcom/google/android/gms/mdm/a/b;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method public static a(Ljava/lang/String;Lcom/android/volley/x;Lcom/android/volley/w;)Lcom/android/volley/p;
    .locals 8

    .prologue
    const/4 v2, 0x1

    .line 150
    invoke-static {}, Lcom/google/android/gms/common/app/GmsApplication;->b()Lcom/google/android/gms/common/app/GmsApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/common/app/GmsApplication;->c()Lcom/android/volley/s;

    move-result-object v7

    .line 152
    new-instance v6, Lcom/google/af/a/c/a/l;

    invoke-direct {v6}, Lcom/google/af/a/c/a/l;-><init>()V

    .line 153
    invoke-static {}, Lcom/google/android/gms/common/app/GmsApplication;->b()Lcom/google/android/gms/common/app/GmsApplication;

    invoke-static {}, Lcom/google/android/gms/common/util/e;->a()J

    move-result-wide v0

    .line 154
    iput-wide v0, v6, Lcom/google/af/a/c/a/l;->a:J

    .line 155
    if-nez p0, :cond_0

    .line 156
    iput-boolean v2, v6, Lcom/google/af/a/c/a/l;->c:Z

    .line 161
    :goto_0
    new-instance v0, Lcom/google/android/gms/mdm/a/c;

    sget-object v1, Lcom/google/android/gms/mdm/b/a;->q:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v1}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    const-class v5, Lcom/google/af/a/c/a/m;

    move-object v3, p1

    move-object v4, p2

    invoke-direct/range {v0 .. v6}, Lcom/google/android/gms/mdm/a/c;-><init>(Ljava/lang/String;ZLcom/android/volley/x;Lcom/android/volley/w;Ljava/lang/Class;Lcom/google/protobuf/nano/j;)V

    .line 166
    invoke-virtual {v7, v0}, Lcom/android/volley/s;->a(Lcom/android/volley/p;)Lcom/android/volley/p;

    move-result-object v0

    return-object v0

    .line 158
    :cond_0
    iput-object p0, v6, Lcom/google/af/a/c/a/l;->b:Ljava/lang/String;

    goto :goto_0
.end method

.method public static a([ILandroid/location/Location;Lcom/google/af/a/c/a/a;Ljava/lang/String;Lcom/google/af/a/c/a/h;Lcom/android/volley/x;Lcom/android/volley/w;)Lcom/android/volley/p;
    .locals 8

    .prologue
    const/4 v2, 0x0

    .line 88
    const-string v0, "sending remote payload: [%s, %s]"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {p0}, Ljava/util/Arrays;->toString([I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v3, 0x1

    aput-object p3, v1, v3

    const-string v3, "MDM"

    invoke-static {v3, v0, v1}, Lcom/google/android/gms/mdm/f/b;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 91
    invoke-static {}, Lcom/google/android/gms/common/app/GmsApplication;->b()Lcom/google/android/gms/common/app/GmsApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/common/app/GmsApplication;->c()Lcom/android/volley/s;

    move-result-object v7

    .line 93
    new-instance v6, Lcom/google/af/a/c/a/j;

    invoke-direct {v6}, Lcom/google/af/a/c/a/j;-><init>()V

    .line 94
    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 95
    iput-object p3, v6, Lcom/google/af/a/c/a/j;->b:Ljava/lang/String;

    .line 97
    :cond_0
    iput-object p0, v6, Lcom/google/af/a/c/a/j;->c:[I

    .line 99
    if-eqz p1, :cond_1

    .line 100
    new-instance v0, Lcom/google/af/a/c/a/e;

    invoke-direct {v0}, Lcom/google/af/a/c/a/e;-><init>()V

    .line 101
    invoke-virtual {p1}, Landroid/location/Location;->getAccuracy()F

    move-result v1

    iput v1, v0, Lcom/google/af/a/c/a/e;->c:F

    .line 102
    invoke-virtual {p1}, Landroid/location/Location;->getLatitude()D

    move-result-wide v4

    iput-wide v4, v0, Lcom/google/af/a/c/a/e;->b:D

    .line 103
    invoke-virtual {p1}, Landroid/location/Location;->getLongitude()D

    move-result-wide v4

    iput-wide v4, v0, Lcom/google/af/a/c/a/e;->a:D

    .line 104
    invoke-virtual {p1}, Landroid/location/Location;->getTime()J

    move-result-wide v4

    iput-wide v4, v0, Lcom/google/af/a/c/a/e;->e:J

    .line 106
    iput-object v0, v6, Lcom/google/af/a/c/a/j;->a:Lcom/google/af/a/c/a/e;

    .line 108
    :cond_1
    if-eqz p4, :cond_2

    .line 109
    iput-object p4, v6, Lcom/google/af/a/c/a/j;->e:Lcom/google/af/a/c/a/h;

    .line 111
    :cond_2
    if-eqz p2, :cond_3

    .line 112
    iput-object p2, v6, Lcom/google/af/a/c/a/j;->f:Lcom/google/af/a/c/a/a;

    .line 115
    :cond_3
    new-instance v0, Lcom/google/android/gms/mdm/a/c;

    sget-object v1, Lcom/google/android/gms/mdm/b/a;->a:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v1}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    const-class v5, Lcom/google/af/a/c/a/k;

    move-object v3, p5

    move-object v4, p6

    invoke-direct/range {v0 .. v6}, Lcom/google/android/gms/mdm/a/c;-><init>(Ljava/lang/String;ZLcom/android/volley/x;Lcom/android/volley/w;Ljava/lang/Class;Lcom/google/protobuf/nano/j;)V

    invoke-virtual {v7, v0}, Lcom/android/volley/s;->a(Lcom/android/volley/p;)Lcom/android/volley/p;

    move-result-object v0

    return-object v0
.end method
