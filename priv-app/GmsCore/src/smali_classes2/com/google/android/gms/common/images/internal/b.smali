.class public final Lcom/google/android/gms/common/images/internal/b;
.super Landroid/graphics/drawable/Drawable;
.source "SourceFile"

# interfaces
.implements Landroid/graphics/drawable/Drawable$Callback;


# instance fields
.field private a:I

.field private b:J

.field private c:I

.field private d:I

.field private e:I

.field private f:I

.field private g:I

.field private h:Z

.field private i:Z

.field private j:Lcom/google/android/gms/common/images/internal/e;

.field private k:Landroid/graphics/drawable/Drawable;

.field private l:Landroid/graphics/drawable/Drawable;

.field private m:Z

.field private n:Z

.field private o:Z

.field private p:I


# direct methods
.method public constructor <init>(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V
    .locals 3

    .prologue
    .line 69
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/gms/common/images/internal/b;-><init>(Lcom/google/android/gms/common/images/internal/e;)V

    .line 71
    if-nez p1, :cond_0

    .line 72
    invoke-static {}, Lcom/google/android/gms/common/images/internal/c;->a()Lcom/google/android/gms/common/images/internal/c;

    move-result-object p1

    .line 74
    :cond_0
    iput-object p1, p0, Lcom/google/android/gms/common/images/internal/b;->k:Landroid/graphics/drawable/Drawable;

    .line 75
    invoke-virtual {p1, p0}, Landroid/graphics/drawable/Drawable;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    .line 76
    iget-object v0, p0, Lcom/google/android/gms/common/images/internal/b;->j:Lcom/google/android/gms/common/images/internal/e;

    iget v1, v0, Lcom/google/android/gms/common/images/internal/e;->b:I

    invoke-virtual {p1}, Landroid/graphics/drawable/Drawable;->getChangingConfigurations()I

    move-result v2

    or-int/2addr v1, v2

    iput v1, v0, Lcom/google/android/gms/common/images/internal/e;->b:I

    .line 79
    if-nez p2, :cond_1

    .line 80
    invoke-static {}, Lcom/google/android/gms/common/images/internal/c;->a()Lcom/google/android/gms/common/images/internal/c;

    move-result-object p2

    .line 82
    :cond_1
    iput-object p2, p0, Lcom/google/android/gms/common/images/internal/b;->l:Landroid/graphics/drawable/Drawable;

    .line 83
    invoke-virtual {p2, p0}, Landroid/graphics/drawable/Drawable;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    .line 84
    iget-object v0, p0, Lcom/google/android/gms/common/images/internal/b;->j:Lcom/google/android/gms/common/images/internal/e;

    iget v1, v0, Lcom/google/android/gms/common/images/internal/e;->b:I

    invoke-virtual {p2}, Landroid/graphics/drawable/Drawable;->getChangingConfigurations()I

    move-result v2

    or-int/2addr v1, v2

    iput v1, v0, Lcom/google/android/gms/common/images/internal/e;->b:I

    .line 86
    return-void
.end method

.method constructor <init>(Lcom/google/android/gms/common/images/internal/e;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 88
    invoke-direct {p0}, Landroid/graphics/drawable/Drawable;-><init>()V

    .line 42
    iput v1, p0, Lcom/google/android/gms/common/images/internal/b;->a:I

    .line 48
    const/16 v0, 0xff

    iput v0, p0, Lcom/google/android/gms/common/images/internal/b;->e:I

    .line 51
    iput v1, p0, Lcom/google/android/gms/common/images/internal/b;->g:I

    .line 52
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/common/images/internal/b;->h:Z

    .line 89
    new-instance v0, Lcom/google/android/gms/common/images/internal/e;

    invoke-direct {v0, p1}, Lcom/google/android/gms/common/images/internal/e;-><init>(Lcom/google/android/gms/common/images/internal/e;)V

    iput-object v0, p0, Lcom/google/android/gms/common/images/internal/b;->j:Lcom/google/android/gms/common/images/internal/e;

    .line 90
    return-void
.end method

.method private c()Z
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 186
    iget-boolean v0, p0, Lcom/google/android/gms/common/images/internal/b;->m:Z

    if-nez v0, :cond_0

    .line 187
    iget-object v0, p0, Lcom/google/android/gms/common/images/internal/b;->k:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getConstantState()Landroid/graphics/drawable/Drawable$ConstantState;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/common/images/internal/b;->l:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getConstantState()Landroid/graphics/drawable/Drawable$ConstantState;

    move-result-object v0

    if-eqz v0, :cond_1

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/gms/common/images/internal/b;->n:Z

    .line 189
    iput-boolean v1, p0, Lcom/google/android/gms/common/images/internal/b;->m:Z

    .line 192
    :cond_0
    iget-boolean v0, p0, Lcom/google/android/gms/common/images/internal/b;->n:Z

    return v0

    .line 187
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a()Landroid/graphics/drawable/Drawable;
    .locals 1

    .prologue
    .line 214
    iget-object v0, p0, Lcom/google/android/gms/common/images/internal/b;->l:Landroid/graphics/drawable/Drawable;

    return-object v0
.end method

.method public final b()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 223
    iput v1, p0, Lcom/google/android/gms/common/images/internal/b;->c:I

    .line 224
    iget v0, p0, Lcom/google/android/gms/common/images/internal/b;->e:I

    iput v0, p0, Lcom/google/android/gms/common/images/internal/b;->d:I

    .line 225
    iput v1, p0, Lcom/google/android/gms/common/images/internal/b;->g:I

    .line 226
    const/16 v0, 0xfa

    iput v0, p0, Lcom/google/android/gms/common/images/internal/b;->f:I

    .line 227
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/gms/common/images/internal/b;->a:I

    .line 228
    invoke-virtual {p0}, Lcom/google/android/gms/common/images/internal/b;->invalidateSelf()V

    .line 229
    return-void
.end method

.method public final draw(Landroid/graphics/Canvas;)V
    .locals 7

    .prologue
    const/4 v1, 0x1

    const/high16 v6, 0x3f800000    # 1.0f

    const/4 v0, 0x0

    .line 254
    .line 256
    iget v2, p0, Lcom/google/android/gms/common/images/internal/b;->a:I

    packed-switch v2, :pswitch_data_0

    :cond_0
    :goto_0
    move v0, v1

    .line 277
    :goto_1
    iget v1, p0, Lcom/google/android/gms/common/images/internal/b;->g:I

    .line 278
    iget-boolean v2, p0, Lcom/google/android/gms/common/images/internal/b;->h:Z

    .line 279
    iget-object v3, p0, Lcom/google/android/gms/common/images/internal/b;->k:Landroid/graphics/drawable/Drawable;

    .line 280
    iget-object v4, p0, Lcom/google/android/gms/common/images/internal/b;->l:Landroid/graphics/drawable/Drawable;

    .line 282
    if-eqz v0, :cond_6

    .line 285
    if-eqz v2, :cond_1

    if-nez v1, :cond_2

    .line 286
    :cond_1
    invoke-virtual {v3, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 288
    :cond_2
    iget v0, p0, Lcom/google/android/gms/common/images/internal/b;->e:I

    if-ne v1, v0, :cond_3

    .line 289
    iget v0, p0, Lcom/google/android/gms/common/images/internal/b;->e:I

    invoke-virtual {v4, v0}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    .line 290
    invoke-virtual {v4, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 310
    :cond_3
    :goto_2
    return-void

    .line 258
    :pswitch_0
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/android/gms/common/images/internal/b;->b:J

    .line 260
    const/4 v1, 0x2

    iput v1, p0, Lcom/google/android/gms/common/images/internal/b;->a:I

    goto :goto_1

    .line 264
    :pswitch_1
    iget-wide v2, p0, Lcom/google/android/gms/common/images/internal/b;->b:J

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-ltz v2, :cond_0

    .line 265
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    iget-wide v4, p0, Lcom/google/android/gms/common/images/internal/b;->b:J

    sub-long/2addr v2, v4

    long-to-float v2, v2

    iget v3, p0, Lcom/google/android/gms/common/images/internal/b;->f:I

    int-to-float v3, v3

    div-float/2addr v2, v3

    .line 267
    cmpl-float v3, v2, v6

    if-ltz v3, :cond_5

    .line 268
    :goto_3
    if-eqz v1, :cond_4

    .line 269
    iput v0, p0, Lcom/google/android/gms/common/images/internal/b;->a:I

    .line 271
    :cond_4
    invoke-static {v2, v6}, Ljava/lang/Math;->min(FF)F

    move-result v0

    .line 272
    iget v2, p0, Lcom/google/android/gms/common/images/internal/b;->c:I

    int-to-float v2, v2

    iget v3, p0, Lcom/google/android/gms/common/images/internal/b;->d:I

    iget v4, p0, Lcom/google/android/gms/common/images/internal/b;->c:I

    sub-int/2addr v3, v4

    int-to-float v3, v3

    mul-float/2addr v0, v3

    add-float/2addr v0, v2

    float-to-int v0, v0

    iput v0, p0, Lcom/google/android/gms/common/images/internal/b;->g:I

    goto :goto_0

    :cond_5
    move v1, v0

    .line 267
    goto :goto_3

    .line 295
    :cond_6
    if-eqz v2, :cond_7

    .line 296
    iget v0, p0, Lcom/google/android/gms/common/images/internal/b;->e:I

    sub-int/2addr v0, v1

    invoke-virtual {v3, v0}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    .line 298
    :cond_7
    invoke-virtual {v3, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 299
    if-eqz v2, :cond_8

    .line 300
    iget v0, p0, Lcom/google/android/gms/common/images/internal/b;->e:I

    invoke-virtual {v3, v0}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    .line 303
    :cond_8
    if-lez v1, :cond_9

    .line 304
    invoke-virtual {v4, v1}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    .line 305
    invoke-virtual {v4, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 306
    iget v0, p0, Lcom/google/android/gms/common/images/internal/b;->e:I

    invoke-virtual {v4, v0}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    .line 309
    :cond_9
    invoke-virtual {p0}, Lcom/google/android/gms/common/images/internal/b;->invalidateSelf()V

    goto :goto_2

    .line 256
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final getChangingConfigurations()I
    .locals 2

    .prologue
    .line 128
    invoke-super {p0}, Landroid/graphics/drawable/Drawable;->getChangingConfigurations()I

    move-result v0

    iget-object v1, p0, Lcom/google/android/gms/common/images/internal/b;->j:Lcom/google/android/gms/common/images/internal/e;

    iget v1, v1, Lcom/google/android/gms/common/images/internal/e;->a:I

    or-int/2addr v0, v1

    iget-object v1, p0, Lcom/google/android/gms/common/images/internal/b;->j:Lcom/google/android/gms/common/images/internal/e;

    iget v1, v1, Lcom/google/android/gms/common/images/internal/e;->b:I

    or-int/2addr v0, v1

    return v0
.end method

.method public final getConstantState()Landroid/graphics/drawable/Drawable$ConstantState;
    .locals 2

    .prologue
    .line 168
    invoke-direct {p0}, Lcom/google/android/gms/common/images/internal/b;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 169
    iget-object v0, p0, Lcom/google/android/gms/common/images/internal/b;->j:Lcom/google/android/gms/common/images/internal/e;

    invoke-virtual {p0}, Lcom/google/android/gms/common/images/internal/b;->getChangingConfigurations()I

    move-result v1

    iput v1, v0, Lcom/google/android/gms/common/images/internal/e;->a:I

    .line 170
    iget-object v0, p0, Lcom/google/android/gms/common/images/internal/b;->j:Lcom/google/android/gms/common/images/internal/e;

    .line 172
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final getIntrinsicHeight()I
    .locals 2

    .prologue
    .line 156
    iget-object v0, p0, Lcom/google/android/gms/common/images/internal/b;->k:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v0

    iget-object v1, p0, Lcom/google/android/gms/common/images/internal/b;->l:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    return v0
.end method

.method public final getIntrinsicWidth()I
    .locals 2

    .prologue
    .line 150
    iget-object v0, p0, Lcom/google/android/gms/common/images/internal/b;->k:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v0

    iget-object v1, p0, Lcom/google/android/gms/common/images/internal/b;->l:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    return v0
.end method

.method public final getOpacity()I
    .locals 2

    .prologue
    .line 176
    iget-boolean v0, p0, Lcom/google/android/gms/common/images/internal/b;->o:Z

    if-nez v0, :cond_0

    .line 177
    iget-object v0, p0, Lcom/google/android/gms/common/images/internal/b;->k:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getOpacity()I

    move-result v0

    iget-object v1, p0, Lcom/google/android/gms/common/images/internal/b;->l:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getOpacity()I

    move-result v1

    invoke-static {v0, v1}, Landroid/graphics/drawable/Drawable;->resolveOpacity(II)I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/common/images/internal/b;->p:I

    .line 179
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/common/images/internal/b;->o:Z

    .line 182
    :cond_0
    iget v0, p0, Lcom/google/android/gms/common/images/internal/b;->p:I

    return v0
.end method

.method public final invalidateDrawable(Landroid/graphics/drawable/Drawable;)V
    .locals 1
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    .prologue
    .line 96
    const/16 v0, 0xb

    invoke-static {v0}, Lcom/google/android/gms/common/util/ao;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 97
    invoke-virtual {p0}, Lcom/google/android/gms/common/images/internal/b;->getCallback()Landroid/graphics/drawable/Drawable$Callback;

    move-result-object v0

    .line 98
    if-eqz v0, :cond_0

    .line 99
    invoke-interface {v0, p0}, Landroid/graphics/drawable/Drawable$Callback;->invalidateDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 102
    :cond_0
    return-void
.end method

.method public final mutate()Landroid/graphics/drawable/Drawable;
    .locals 2

    .prologue
    .line 197
    iget-boolean v0, p0, Lcom/google/android/gms/common/images/internal/b;->i:Z

    if-nez v0, :cond_1

    invoke-super {p0}, Landroid/graphics/drawable/Drawable;->mutate()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    if-ne v0, p0, :cond_1

    .line 198
    invoke-direct {p0}, Lcom/google/android/gms/common/images/internal/b;->c()Z

    move-result v0

    if-nez v0, :cond_0

    .line 199
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "One or more children of this LayerDrawable does not have constant state; this drawable cannot be mutated."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 202
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/common/images/internal/b;->k:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->mutate()Landroid/graphics/drawable/Drawable;

    .line 203
    iget-object v0, p0, Lcom/google/android/gms/common/images/internal/b;->l:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->mutate()Landroid/graphics/drawable/Drawable;

    .line 204
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/common/images/internal/b;->i:Z

    .line 206
    :cond_1
    return-object p0
.end method

.method protected final onBoundsChange(Landroid/graphics/Rect;)V
    .locals 1

    .prologue
    .line 162
    iget-object v0, p0, Lcom/google/android/gms/common/images/internal/b;->k:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->setBounds(Landroid/graphics/Rect;)V

    .line 163
    iget-object v0, p0, Lcom/google/android/gms/common/images/internal/b;->l:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->setBounds(Landroid/graphics/Rect;)V

    .line 164
    return-void
.end method

.method public final scheduleDrawable(Landroid/graphics/drawable/Drawable;Ljava/lang/Runnable;J)V
    .locals 1
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    .prologue
    .line 106
    const/16 v0, 0xb

    invoke-static {v0}, Lcom/google/android/gms/common/util/ao;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 107
    invoke-virtual {p0}, Lcom/google/android/gms/common/images/internal/b;->getCallback()Landroid/graphics/drawable/Drawable$Callback;

    move-result-object v0

    .line 108
    if-eqz v0, :cond_0

    .line 109
    invoke-interface {v0, p0, p2, p3, p4}, Landroid/graphics/drawable/Drawable$Callback;->scheduleDrawable(Landroid/graphics/drawable/Drawable;Ljava/lang/Runnable;J)V

    .line 112
    :cond_0
    return-void
.end method

.method public final setAlpha(I)V
    .locals 2

    .prologue
    .line 135
    iget v0, p0, Lcom/google/android/gms/common/images/internal/b;->g:I

    iget v1, p0, Lcom/google/android/gms/common/images/internal/b;->e:I

    if-ne v0, v1, :cond_0

    .line 136
    iput p1, p0, Lcom/google/android/gms/common/images/internal/b;->g:I

    .line 138
    :cond_0
    iput p1, p0, Lcom/google/android/gms/common/images/internal/b;->e:I

    .line 139
    invoke-virtual {p0}, Lcom/google/android/gms/common/images/internal/b;->invalidateSelf()V

    .line 140
    return-void
.end method

.method public final setColorFilter(Landroid/graphics/ColorFilter;)V
    .locals 1

    .prologue
    .line 144
    iget-object v0, p0, Lcom/google/android/gms/common/images/internal/b;->k:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->setColorFilter(Landroid/graphics/ColorFilter;)V

    .line 145
    iget-object v0, p0, Lcom/google/android/gms/common/images/internal/b;->l:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->setColorFilter(Landroid/graphics/ColorFilter;)V

    .line 146
    return-void
.end method

.method public final unscheduleDrawable(Landroid/graphics/drawable/Drawable;Ljava/lang/Runnable;)V
    .locals 1
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    .prologue
    .line 116
    const/16 v0, 0xb

    invoke-static {v0}, Lcom/google/android/gms/common/util/ao;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 117
    invoke-virtual {p0}, Lcom/google/android/gms/common/images/internal/b;->getCallback()Landroid/graphics/drawable/Drawable$Callback;

    move-result-object v0

    .line 118
    if-eqz v0, :cond_0

    .line 119
    invoke-interface {v0, p0, p2}, Landroid/graphics/drawable/Drawable$Callback;->unscheduleDrawable(Landroid/graphics/drawable/Drawable;Ljava/lang/Runnable;)V

    .line 122
    :cond_0
    return-void
.end method
