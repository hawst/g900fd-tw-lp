.class public final Lcom/google/android/gms/clearcut/c;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public a:Ljava/lang/String;

.field final synthetic b:Lcom/google/android/gms/clearcut/a;

.field private c:I

.field private d:Ljava/lang/String;

.field private e:Ljava/lang/String;

.field private final f:Lcom/google/android/gms/clearcut/d;

.field private final g:Lcom/google/af/a/d/a/a/j;

.field private h:Z


# direct methods
.method private constructor <init>(Lcom/google/android/gms/clearcut/a;Lcom/google/android/gms/clearcut/d;)V
    .locals 1

    .prologue
    .line 267
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0, p2}, Lcom/google/android/gms/clearcut/c;-><init>(Lcom/google/android/gms/clearcut/a;[BLcom/google/android/gms/clearcut/d;)V

    .line 268
    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/gms/clearcut/a;Lcom/google/android/gms/clearcut/d;B)V
    .locals 0

    .prologue
    .line 252
    invoke-direct {p0, p1, p2}, Lcom/google/android/gms/clearcut/c;-><init>(Lcom/google/android/gms/clearcut/a;Lcom/google/android/gms/clearcut/d;)V

    return-void
.end method

.method private constructor <init>(Lcom/google/android/gms/clearcut/a;[B)V
    .locals 1

    .prologue
    .line 263
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/gms/clearcut/c;-><init>(Lcom/google/android/gms/clearcut/a;[BLcom/google/android/gms/clearcut/d;)V

    .line 264
    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/gms/clearcut/a;[BB)V
    .locals 0

    .prologue
    .line 252
    invoke-direct {p0, p1, p2}, Lcom/google/android/gms/clearcut/c;-><init>(Lcom/google/android/gms/clearcut/a;[B)V

    return-void
.end method

.method private constructor <init>(Lcom/google/android/gms/clearcut/a;[BLcom/google/android/gms/clearcut/d;)V
    .locals 4

    .prologue
    .line 270
    iput-object p1, p0, Lcom/google/android/gms/clearcut/c;->b:Lcom/google/android/gms/clearcut/a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 253
    iget-object v0, p0, Lcom/google/android/gms/clearcut/c;->b:Lcom/google/android/gms/clearcut/a;

    invoke-static {v0}, Lcom/google/android/gms/clearcut/a;->a(Lcom/google/android/gms/clearcut/a;)I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/clearcut/c;->c:I

    .line 254
    iget-object v0, p0, Lcom/google/android/gms/clearcut/c;->b:Lcom/google/android/gms/clearcut/a;

    invoke-static {v0}, Lcom/google/android/gms/clearcut/a;->b(Lcom/google/android/gms/clearcut/a;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/clearcut/c;->d:Ljava/lang/String;

    .line 255
    iget-object v0, p0, Lcom/google/android/gms/clearcut/c;->b:Lcom/google/android/gms/clearcut/a;

    invoke-static {v0}, Lcom/google/android/gms/clearcut/a;->c(Lcom/google/android/gms/clearcut/a;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/clearcut/c;->a:Ljava/lang/String;

    .line 256
    iget-object v0, p0, Lcom/google/android/gms/clearcut/c;->b:Lcom/google/android/gms/clearcut/a;

    invoke-static {v0}, Lcom/google/android/gms/clearcut/a;->d(Lcom/google/android/gms/clearcut/a;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/clearcut/c;->e:Ljava/lang/String;

    .line 259
    new-instance v0, Lcom/google/af/a/d/a/a/j;

    invoke-direct {v0}, Lcom/google/af/a/d/a/a/j;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/clearcut/c;->g:Lcom/google/af/a/d/a/a/j;

    .line 260
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/clearcut/c;->h:Z

    .line 271
    invoke-static {p1}, Lcom/google/android/gms/clearcut/a;->c(Lcom/google/android/gms/clearcut/a;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/clearcut/c;->a:Ljava/lang/String;

    .line 272
    invoke-static {p1}, Lcom/google/android/gms/clearcut/a;->d(Lcom/google/android/gms/clearcut/a;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/clearcut/c;->e:Ljava/lang/String;

    .line 274
    iget-object v0, p0, Lcom/google/android/gms/clearcut/c;->g:Lcom/google/af/a/d/a/a/j;

    invoke-static {p1}, Lcom/google/android/gms/clearcut/a;->e(Lcom/google/android/gms/clearcut/a;)Lcom/google/android/gms/common/util/p;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/gms/common/util/p;->a()J

    move-result-wide v2

    iput-wide v2, v0, Lcom/google/af/a/d/a/a/j;->a:J

    .line 276
    if-eqz p2, :cond_0

    .line 277
    iget-object v0, p0, Lcom/google/android/gms/clearcut/c;->g:Lcom/google/af/a/d/a/a/j;

    iput-object p2, v0, Lcom/google/af/a/d/a/a/j;->h:[B

    .line 282
    :cond_0
    iput-object p3, p0, Lcom/google/android/gms/clearcut/c;->f:Lcom/google/android/gms/clearcut/d;

    .line 283
    return-void
.end method

.method private a()Lcom/google/android/gms/clearcut/LogEventParcelable;
    .locals 8

    .prologue
    .line 355
    new-instance v7, Lcom/google/android/gms/clearcut/LogEventParcelable;

    new-instance v0, Lcom/google/android/gms/playlog/internal/PlayLoggerContext;

    iget-object v1, p0, Lcom/google/android/gms/clearcut/c;->b:Lcom/google/android/gms/clearcut/a;

    invoke-static {v1}, Lcom/google/android/gms/clearcut/a;->f(Lcom/google/android/gms/clearcut/a;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/clearcut/c;->b:Lcom/google/android/gms/clearcut/a;

    invoke-static {v2}, Lcom/google/android/gms/clearcut/a;->g(Lcom/google/android/gms/clearcut/a;)I

    move-result v2

    iget v3, p0, Lcom/google/android/gms/clearcut/c;->c:I

    iget-object v4, p0, Lcom/google/android/gms/clearcut/c;->d:Ljava/lang/String;

    iget-object v5, p0, Lcom/google/android/gms/clearcut/c;->a:Ljava/lang/String;

    iget-object v6, p0, Lcom/google/android/gms/clearcut/c;->e:Ljava/lang/String;

    invoke-direct/range {v0 .. v6}, Lcom/google/android/gms/playlog/internal/PlayLoggerContext;-><init>(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/gms/clearcut/c;->g:Lcom/google/af/a/d/a/a/j;

    iget-object v2, p0, Lcom/google/android/gms/clearcut/c;->f:Lcom/google/android/gms/clearcut/d;

    invoke-direct {v7, v0, v1, v2}, Lcom/google/android/gms/clearcut/LogEventParcelable;-><init>(Lcom/google/android/gms/playlog/internal/PlayLoggerContext;Lcom/google/af/a/d/a/a/j;Lcom/google/android/gms/clearcut/d;)V

    return-object v7
.end method


# virtual methods
.method public final a(I)Lcom/google/android/gms/clearcut/c;
    .locals 1

    .prologue
    .line 337
    iget-object v0, p0, Lcom/google/android/gms/clearcut/c;->g:Lcom/google/af/a/d/a/a/j;

    iput p1, v0, Lcom/google/af/a/d/a/a/j;->c:I

    .line 338
    return-object p0
.end method

.method public final a(Ljava/lang/String;)Lcom/google/android/gms/clearcut/c;
    .locals 1

    .prologue
    .line 325
    iget-object v0, p0, Lcom/google/android/gms/clearcut/c;->g:Lcom/google/af/a/d/a/a/j;

    iput-object p1, v0, Lcom/google/af/a/d/a/a/j;->b:Ljava/lang/String;

    .line 326
    return-object p0
.end method

.method public final a(Lcom/google/android/gms/common/api/v;)Lcom/google/android/gms/common/api/am;
    .locals 2

    .prologue
    .line 371
    iget-boolean v0, p0, Lcom/google/android/gms/clearcut/c;->h:Z

    if-eqz v0, :cond_0

    .line 372
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "do not reuse LogEventBuilder"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 375
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/clearcut/c;->h:Z

    .line 376
    iget-object v0, p0, Lcom/google/android/gms/clearcut/c;->b:Lcom/google/android/gms/clearcut/a;

    invoke-static {v0}, Lcom/google/android/gms/clearcut/a;->h(Lcom/google/android/gms/clearcut/a;)Lcom/google/android/gms/clearcut/e;

    move-result-object v0

    invoke-direct {p0}, Lcom/google/android/gms/clearcut/c;->a()Lcom/google/android/gms/clearcut/LogEventParcelable;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Lcom/google/android/gms/clearcut/e;->a(Lcom/google/android/gms/common/api/v;Lcom/google/android/gms/clearcut/LogEventParcelable;)Lcom/google/android/gms/common/api/am;

    move-result-object v0

    return-object v0
.end method

.method public final b(Lcom/google/android/gms/common/api/v;)Lcom/google/android/gms/common/api/am;
    .locals 2

    .prologue
    .line 389
    iget-boolean v0, p0, Lcom/google/android/gms/clearcut/c;->h:Z

    if-eqz v0, :cond_0

    .line 390
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "do not reuse LogEventBuilder"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 393
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/clearcut/c;->h:Z

    .line 394
    iget-object v0, p0, Lcom/google/android/gms/clearcut/c;->b:Lcom/google/android/gms/clearcut/a;

    invoke-static {v0}, Lcom/google/android/gms/clearcut/a;->h(Lcom/google/android/gms/clearcut/a;)Lcom/google/android/gms/clearcut/e;

    move-result-object v0

    invoke-direct {p0}, Lcom/google/android/gms/clearcut/c;->a()Lcom/google/android/gms/clearcut/LogEventParcelable;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Lcom/google/android/gms/clearcut/e;->b(Lcom/google/android/gms/common/api/v;Lcom/google/android/gms/clearcut/LogEventParcelable;)Lcom/google/android/gms/common/api/am;

    move-result-object v0

    return-object v0
.end method
