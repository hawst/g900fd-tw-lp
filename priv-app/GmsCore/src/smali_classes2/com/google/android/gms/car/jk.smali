.class final Lcom/google/android/gms/car/jk;
.super Landroid/content/BroadcastReceiver;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/gms/car/FirstActivity;

.field private final b:Ljava/util/HashSet;


# direct methods
.method constructor <init>(Lcom/google/android/gms/car/FirstActivity;)V
    .locals 1

    .prologue
    .line 499
    iput-object p1, p0, Lcom/google/android/gms/car/jk;->a:Lcom/google/android/gms/car/FirstActivity;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    .line 500
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/car/jk;->b:Ljava/util/HashSet;

    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/car/jk;)Ljava/util/HashSet;
    .locals 1

    .prologue
    .line 499
    iget-object v0, p0, Lcom/google/android/gms/car/jk;->b:Ljava/util/HashSet;

    return-object v0
.end method


# virtual methods
.method public final onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x3

    .line 504
    iget-object v0, p0, Lcom/google/android/gms/car/jk;->a:Lcom/google/android/gms/car/FirstActivity;

    invoke-static {v0}, Lcom/google/android/gms/car/FirstActivity;->k(Lcom/google/android/gms/car/FirstActivity;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 622
    :cond_0
    :goto_0
    return-void

    .line 508
    :cond_1
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 509
    const-string v1, "CAR.FIRST"

    invoke-static {v1, v4}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 510
    const-string v1, "CAR.FIRST"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Receive WiFi direct broadcast; action: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 513
    :cond_2
    const-string v1, "android.net.wifi.p2p.CONNECTION_STATE_CHANGE"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 514
    const-string v0, "networkInfo"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/NetworkInfo;

    .line 517
    const-string v1, "CAR.FIRST"

    invoke-static {v1, v4}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 518
    const-string v1, "CAR.FIRST"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Network connected? "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " , WiFi connected? "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/gms/car/jk;->a:Lcom/google/android/gms/car/FirstActivity;

    invoke-static {v3}, Lcom/google/android/gms/car/FirstActivity;->k(Lcom/google/android/gms/car/FirstActivity;)Z

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 521
    :cond_3
    invoke-virtual {v0}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/car/jk;->a:Lcom/google/android/gms/car/FirstActivity;

    invoke-static {v0}, Lcom/google/android/gms/car/FirstActivity;->k(Lcom/google/android/gms/car/FirstActivity;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 525
    iget-object v0, p0, Lcom/google/android/gms/car/jk;->a:Lcom/google/android/gms/car/FirstActivity;

    invoke-static {v0}, Lcom/google/android/gms/car/FirstActivity;->l(Lcom/google/android/gms/car/FirstActivity;)Z

    .line 527
    const-string v0, "wifiP2pInfo"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/wifi/p2p/WifiP2pInfo;

    .line 529
    if-eqz v0, :cond_5

    .line 530
    const-string v1, "CAR.FIRST"

    invoke-static {v1, v4}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 531
    const-string v1, "CAR.FIRST"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Connection info; group formed?  "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v3, v0, Landroid/net/wifi/p2p/WifiP2pInfo;->groupFormed:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", isGO? "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-boolean v3, v0, Landroid/net/wifi/p2p/WifiP2pInfo;->isGroupOwner:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", owner addr: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, v0, Landroid/net/wifi/p2p/WifiP2pInfo;->groupOwnerAddress:Ljava/net/InetAddress;

    invoke-virtual {v3}, Ljava/net/InetAddress;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 535
    :cond_4
    iget-object v1, p0, Lcom/google/android/gms/car/jk;->a:Lcom/google/android/gms/car/FirstActivity;

    iget-object v0, v0, Landroid/net/wifi/p2p/WifiP2pInfo;->groupOwnerAddress:Ljava/net/InetAddress;

    invoke-virtual {v0}, Ljava/net/InetAddress;->getHostAddress()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/google/android/gms/car/FirstActivity;->a(Lcom/google/android/gms/car/FirstActivity;Ljava/lang/String;)V

    .line 559
    :goto_1
    iget-object v0, p0, Lcom/google/android/gms/car/jk;->a:Lcom/google/android/gms/car/FirstActivity;

    iget-object v1, p0, Lcom/google/android/gms/car/jk;->a:Lcom/google/android/gms/car/FirstActivity;

    invoke-static {v1}, Lcom/google/android/gms/car/FirstActivity;->p(Lcom/google/android/gms/car/FirstActivity;)Lcom/google/android/gms/car/jk;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/car/FirstActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 560
    iget-object v0, p0, Lcom/google/android/gms/car/jk;->a:Lcom/google/android/gms/car/FirstActivity;

    invoke-static {v0}, Lcom/google/android/gms/car/FirstActivity;->q(Lcom/google/android/gms/car/FirstActivity;)Lcom/google/android/gms/car/jk;

    goto/16 :goto_0

    .line 539
    :cond_5
    iget-object v0, p0, Lcom/google/android/gms/car/jk;->a:Lcom/google/android/gms/car/FirstActivity;

    invoke-static {v0}, Lcom/google/android/gms/car/FirstActivity;->m(Lcom/google/android/gms/car/FirstActivity;)V

    .line 540
    iget-object v0, p0, Lcom/google/android/gms/car/jk;->a:Lcom/google/android/gms/car/FirstActivity;

    invoke-static {v0}, Lcom/google/android/gms/car/FirstActivity;->o(Lcom/google/android/gms/car/FirstActivity;)Landroid/net/wifi/p2p/WifiP2pManager;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/car/jk;->a:Lcom/google/android/gms/car/FirstActivity;

    invoke-static {v1}, Lcom/google/android/gms/car/FirstActivity;->n(Lcom/google/android/gms/car/FirstActivity;)Landroid/net/wifi/p2p/WifiP2pManager$Channel;

    move-result-object v1

    new-instance v2, Lcom/google/android/gms/car/jl;

    invoke-direct {v2, p0}, Lcom/google/android/gms/car/jl;-><init>(Lcom/google/android/gms/car/jk;)V

    invoke-virtual {v0, v1, v2}, Landroid/net/wifi/p2p/WifiP2pManager;->requestConnectionInfo(Landroid/net/wifi/p2p/WifiP2pManager$Channel;Landroid/net/wifi/p2p/WifiP2pManager$ConnectionInfoListener;)V

    goto :goto_1

    .line 561
    :cond_6
    const-string v1, "android.net.wifi.p2p.STATE_CHANGED"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_8

    .line 562
    const-string v0, "wifi_p2p_state"

    invoke-virtual {p2, v0, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 563
    const-string v1, "CAR.FIRST"

    invoke-static {v1, v4}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 564
    const-string v1, "CAR.FIRST"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "P2P state changed; new state: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 567
    :cond_7
    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 571
    iget-object v0, p0, Lcom/google/android/gms/car/jk;->a:Lcom/google/android/gms/car/FirstActivity;

    invoke-static {v0}, Lcom/google/android/gms/car/FirstActivity;->m(Lcom/google/android/gms/car/FirstActivity;)V

    .line 572
    iget-object v0, p0, Lcom/google/android/gms/car/jk;->a:Lcom/google/android/gms/car/FirstActivity;

    invoke-static {v0}, Lcom/google/android/gms/car/FirstActivity;->o(Lcom/google/android/gms/car/FirstActivity;)Landroid/net/wifi/p2p/WifiP2pManager;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/car/jk;->a:Lcom/google/android/gms/car/FirstActivity;

    invoke-static {v1}, Lcom/google/android/gms/car/FirstActivity;->n(Lcom/google/android/gms/car/FirstActivity;)Landroid/net/wifi/p2p/WifiP2pManager$Channel;

    move-result-object v1

    new-instance v2, Lcom/google/android/gms/car/jm;

    invoke-direct {v2, p0}, Lcom/google/android/gms/car/jm;-><init>(Lcom/google/android/gms/car/jk;)V

    invoke-virtual {v0, v1, v2}, Landroid/net/wifi/p2p/WifiP2pManager;->discoverPeers(Landroid/net/wifi/p2p/WifiP2pManager$Channel;Landroid/net/wifi/p2p/WifiP2pManager$ActionListener;)V

    goto/16 :goto_0

    .line 586
    :cond_8
    const-string v1, "android.net.wifi.p2p.PEERS_CHANGED"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_9

    .line 587
    iget-object v0, p0, Lcom/google/android/gms/car/jk;->a:Lcom/google/android/gms/car/FirstActivity;

    invoke-static {v0}, Lcom/google/android/gms/car/FirstActivity;->o(Lcom/google/android/gms/car/FirstActivity;)Landroid/net/wifi/p2p/WifiP2pManager;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/car/jk;->a:Lcom/google/android/gms/car/FirstActivity;

    invoke-static {v1}, Lcom/google/android/gms/car/FirstActivity;->n(Lcom/google/android/gms/car/FirstActivity;)Landroid/net/wifi/p2p/WifiP2pManager$Channel;

    move-result-object v1

    new-instance v2, Lcom/google/android/gms/car/jn;

    invoke-direct {v2, p0}, Lcom/google/android/gms/car/jn;-><init>(Lcom/google/android/gms/car/jk;)V

    invoke-virtual {v0, v1, v2}, Landroid/net/wifi/p2p/WifiP2pManager;->requestPeers(Landroid/net/wifi/p2p/WifiP2pManager$Channel;Landroid/net/wifi/p2p/WifiP2pManager$PeerListListener;)V

    goto/16 :goto_0

    .line 615
    :cond_9
    const-string v1, "android.net.wifi.WIFI_STATE_CHANGED"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 616
    const-string v0, "wifi_state"

    invoke-virtual {p2, v0, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 617
    if-ne v0, v4, :cond_0

    .line 619
    iget-object v0, p0, Lcom/google/android/gms/car/jk;->a:Lcom/google/android/gms/car/FirstActivity;

    invoke-static {v0}, Lcom/google/android/gms/car/FirstActivity;->m(Lcom/google/android/gms/car/FirstActivity;)V

    goto/16 :goto_0
.end method
