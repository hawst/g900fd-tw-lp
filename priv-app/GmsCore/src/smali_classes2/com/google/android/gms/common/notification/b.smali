.class public Lcom/google/android/gms/common/notification/b;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private final b:Landroid/content/Context;

.field private final c:Landroid/support/v4/app/bk;

.field private final d:Lorg/json/JSONObject;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 24
    const-class v0, Lcom/google/android/gms/common/notification/b;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/common/notification/b;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    iput-object p1, p0, Lcom/google/android/gms/common/notification/b;->b:Landroid/content/Context;

    .line 42
    new-instance v0, Landroid/support/v4/app/bk;

    invoke-direct {v0, p1}, Landroid/support/v4/app/bk;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/gms/common/notification/b;->c:Landroid/support/v4/app/bk;

    .line 43
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/common/notification/b;->d:Lorg/json/JSONObject;

    .line 44
    return-void
.end method

.method static a(Landroid/content/Context;Lcom/google/android/gms/common/notification/b;)Lcom/google/android/gms/common/notification/b;
    .locals 1

    .prologue
    .line 69
    iget-object v0, p1, Lcom/google/android/gms/common/notification/b;->d:Lorg/json/JSONObject;

    invoke-virtual {v0}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/google/android/gms/common/notification/b;->a(Landroid/content/Context;Ljava/lang/String;)Lcom/google/android/gms/common/notification/b;

    move-result-object v0

    return-object v0
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;)Lcom/google/android/gms/common/notification/b;
    .locals 4

    .prologue
    .line 49
    :try_start_0
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0, p1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 53
    new-instance v1, Lcom/google/android/gms/common/notification/b;

    invoke-direct {v1, p0}, Lcom/google/android/gms/common/notification/b;-><init>(Landroid/content/Context;)V

    .line 54
    const-string v2, "notify_time"

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->optLong(Ljava/lang/String;)J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lcom/google/android/gms/common/notification/b;->a(J)Lcom/google/android/gms/common/notification/b;

    .line 55
    const-string v2, "max_duration"

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->optLong(Ljava/lang/String;)J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lcom/google/android/gms/common/notification/b;->b(J)Lcom/google/android/gms/common/notification/b;

    .line 56
    const-string v2, "big_text"

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/gms/common/notification/b;->a(Ljava/lang/CharSequence;)Lcom/google/android/gms/common/notification/b;

    .line 57
    const-string v2, "content_intent_view"

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/gms/common/notification/b;->a(Landroid/net/Uri;)Lcom/google/android/gms/common/notification/b;

    .line 59
    const-string v2, "content_text"

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/gms/common/notification/b;->b(Ljava/lang/CharSequence;)Lcom/google/android/gms/common/notification/b;

    .line 60
    const-string v2, "content_title"

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/gms/common/notification/b;->c(Ljava/lang/CharSequence;)Lcom/google/android/gms/common/notification/b;

    .line 61
    const-string v2, "key_ongoing"

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;)Z

    move-result v2

    invoke-virtual {v1, v2}, Lcom/google/android/gms/common/notification/b;->a(Z)Lcom/google/android/gms/common/notification/b;

    .line 62
    const-string v2, "small_icon"

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/google/android/gms/common/notification/b;->a(I)Lcom/google/android/gms/common/notification/b;

    .line 63
    const-string v2, "ticker"

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/google/android/gms/common/notification/b;->d(Ljava/lang/CharSequence;)Lcom/google/android/gms/common/notification/b;

    .line 64
    return-object v1

    .line 50
    :catch_0
    move-exception v0

    .line 51
    new-instance v1, Ljava/lang/IllegalArgumentException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method private a(Ljava/lang/String;Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 163
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/common/notification/b;->d:Lorg/json/JSONObject;

    invoke-virtual {v0, p1, p2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 167
    :goto_0
    return-void

    .line 164
    :catch_0
    move-exception v0

    .line 165
    sget-object v1, Lcom/google/android/gms/common/notification/b;->a:Ljava/lang/String;

    const-string v2, "JSONException putting field into object!"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method


# virtual methods
.method public final a(I)Lcom/google/android/gms/common/notification/b;
    .locals 2

    .prologue
    .line 150
    iget-object v0, p0, Lcom/google/android/gms/common/notification/b;->c:Landroid/support/v4/app/bk;

    invoke-virtual {v0, p1}, Landroid/support/v4/app/bk;->a(I)Landroid/support/v4/app/bk;

    .line 151
    const-string v0, "small_icon"

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/common/notification/b;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 152
    return-object p0
.end method

.method final a(J)Lcom/google/android/gms/common/notification/b;
    .locals 3

    .prologue
    .line 93
    const-string v0, "notify_time"

    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/common/notification/b;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 94
    return-object p0
.end method

.method public final a(Landroid/net/Uri;)Lcom/google/android/gms/common/notification/b;
    .locals 5

    .prologue
    .line 120
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    invoke-direct {v0, v1, p1}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 121
    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 122
    iget-object v1, p0, Lcom/google/android/gms/common/notification/b;->c:Landroid/support/v4/app/bk;

    iget-object v2, p0, Lcom/google/android/gms/common/notification/b;->b:Landroid/content/Context;

    const/4 v3, 0x0

    const/high16 v4, 0x8000000

    invoke-static {v2, v3, v0, v4}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    iput-object v0, v1, Landroid/support/v4/app/bk;->d:Landroid/app/PendingIntent;

    .line 127
    const-string v0, "content_intent_view"

    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/common/notification/b;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 128
    return-object p0
.end method

.method public final a(Ljava/lang/CharSequence;)Lcom/google/android/gms/common/notification/b;
    .locals 2

    .prologue
    .line 114
    iget-object v0, p0, Lcom/google/android/gms/common/notification/b;->c:Landroid/support/v4/app/bk;

    new-instance v1, Landroid/support/v4/app/bj;

    invoke-direct {v1}, Landroid/support/v4/app/bj;-><init>()V

    invoke-virtual {v1, p1}, Landroid/support/v4/app/bj;->c(Ljava/lang/CharSequence;)Landroid/support/v4/app/bj;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v4/app/bk;->a(Landroid/support/v4/app/bv;)Landroid/support/v4/app/bk;

    .line 115
    const-string v0, "big_text"

    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/common/notification/b;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 116
    return-object p0
.end method

.method public final a(Z)Lcom/google/android/gms/common/notification/b;
    .locals 2

    .prologue
    .line 144
    iget-object v0, p0, Lcom/google/android/gms/common/notification/b;->c:Landroid/support/v4/app/bk;

    const/4 v1, 0x2

    invoke-virtual {v0, v1, p1}, Landroid/support/v4/app/bk;->a(IZ)V

    .line 145
    const-string v0, "key_ongoing"

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/common/notification/b;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 146
    return-object p0
.end method

.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 73
    iget-object v0, p0, Lcom/google/android/gms/common/notification/b;->d:Lorg/json/JSONObject;

    invoke-virtual {v0}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final b()Landroid/support/v4/app/bk;
    .locals 1

    .prologue
    .line 77
    iget-object v0, p0, Lcom/google/android/gms/common/notification/b;->c:Landroid/support/v4/app/bk;

    return-object v0
.end method

.method public final b(J)Lcom/google/android/gms/common/notification/b;
    .locals 3

    .prologue
    .line 109
    const-string v0, "max_duration"

    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/common/notification/b;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 110
    return-object p0
.end method

.method public final b(Ljava/lang/CharSequence;)Lcom/google/android/gms/common/notification/b;
    .locals 2

    .prologue
    .line 132
    iget-object v0, p0, Lcom/google/android/gms/common/notification/b;->c:Landroid/support/v4/app/bk;

    invoke-virtual {v0, p1}, Landroid/support/v4/app/bk;->b(Ljava/lang/CharSequence;)Landroid/support/v4/app/bk;

    .line 133
    const-string v0, "content_text"

    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/common/notification/b;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 134
    return-object p0
.end method

.method final c()J
    .locals 2

    .prologue
    .line 85
    iget-object v0, p0, Lcom/google/android/gms/common/notification/b;->d:Lorg/json/JSONObject;

    const-string v1, "notify_time"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optLong(Ljava/lang/String;)J

    move-result-wide v0

    return-wide v0
.end method

.method public final c(Ljava/lang/CharSequence;)Lcom/google/android/gms/common/notification/b;
    .locals 2

    .prologue
    .line 138
    iget-object v0, p0, Lcom/google/android/gms/common/notification/b;->c:Landroid/support/v4/app/bk;

    invoke-virtual {v0, p1}, Landroid/support/v4/app/bk;->a(Ljava/lang/CharSequence;)Landroid/support/v4/app/bk;

    .line 139
    const-string v0, "content_title"

    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/common/notification/b;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 140
    return-object p0
.end method

.method public final d()J
    .locals 2

    .prologue
    .line 101
    iget-object v0, p0, Lcom/google/android/gms/common/notification/b;->d:Lorg/json/JSONObject;

    const-string v1, "max_duration"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optLong(Ljava/lang/String;)J

    move-result-wide v0

    return-wide v0
.end method

.method public final d(Ljava/lang/CharSequence;)Lcom/google/android/gms/common/notification/b;
    .locals 2

    .prologue
    .line 156
    iget-object v0, p0, Lcom/google/android/gms/common/notification/b;->c:Landroid/support/v4/app/bk;

    invoke-virtual {v0, p1}, Landroid/support/v4/app/bk;->e(Ljava/lang/CharSequence;)Landroid/support/v4/app/bk;

    .line 157
    const-string v0, "ticker"

    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/common/notification/b;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 158
    return-object p0
.end method
