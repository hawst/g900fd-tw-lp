.class public final Lcom/google/android/gms/car/ga;
.super Lcom/google/android/gms/car/mj;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/car/gw;
.implements Lcom/google/android/gms/car/senderprotocol/ay;


# instance fields
.field final a:Ljava/lang/Object;

.field b:Lcom/google/android/gms/car/gb;

.field volatile c:Z

.field private final d:Landroid/content/Context;

.field private e:Lcom/google/android/gms/car/senderprotocol/ax;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/car/CarService;)V
    .locals 1

    .prologue
    .line 31
    invoke-direct {p0}, Lcom/google/android/gms/car/mj;-><init>()V

    .line 23
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/car/ga;->a:Ljava/lang/Object;

    .line 32
    iput-object p1, p0, Lcom/google/android/gms/car/ga;->d:Landroid/content/Context;

    .line 33
    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/car/ga;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 20
    iget-object v0, p0, Lcom/google/android/gms/car/ga;->a:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic b(Lcom/google/android/gms/car/ga;)V
    .locals 0

    .prologue
    .line 20
    invoke-virtual {p0}, Lcom/google/android/gms/car/ga;->b()V

    return-void
.end method

.method private c(Lcom/google/android/gms/car/ml;)V
    .locals 2

    .prologue
    .line 99
    iget-boolean v0, p0, Lcom/google/android/gms/car/ga;->c:Z

    if-nez v0, :cond_0

    .line 100
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "CarNotConnected"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 101
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/car/ga;->e:Lcom/google/android/gms/car/senderprotocol/ax;

    if-nez v0, :cond_1

    .line 102
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "PhoneStatusEndpoint not set."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 103
    :cond_1
    if-nez p1, :cond_2

    .line 104
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "CarPhoneStatusEventListener is null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 105
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/car/ga;->b:Lcom/google/android/gms/car/gb;

    if-eqz v0, :cond_3

    invoke-interface {p1}, Lcom/google/android/gms/car/ml;->asBinder()Landroid/os/IBinder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/car/ga;->b:Lcom/google/android/gms/car/gb;

    iget-object v1, v1, Lcom/google/android/gms/car/gb;->a:Lcom/google/android/gms/car/ml;

    invoke-interface {v1}, Lcom/google/android/gms/car/ml;->asBinder()Landroid/os/IBinder;

    move-result-object v1

    if-eq v0, v1, :cond_3

    .line 107
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Phone status service already in use."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 109
    :cond_3
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 36
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/car/ga;->c:Z

    .line 37
    return-void
.end method

.method public final a(Lcom/google/android/gms/car/ml;Lcom/google/android/gms/car/CarPhoneStatus;)V
    .locals 1

    .prologue
    .line 57
    invoke-direct {p0, p1}, Lcom/google/android/gms/car/ga;->c(Lcom/google/android/gms/car/ml;)V

    .line 58
    iget-object v0, p0, Lcom/google/android/gms/car/ga;->e:Lcom/google/android/gms/car/senderprotocol/ax;

    invoke-virtual {v0, p2}, Lcom/google/android/gms/car/senderprotocol/ax;->a(Lcom/google/android/gms/car/CarPhoneStatus;)V

    .line 59
    return-void
.end method

.method public final a(Lcom/google/android/gms/car/senderprotocol/az;)V
    .locals 0

    .prologue
    .line 41
    check-cast p1, Lcom/google/android/gms/car/senderprotocol/ax;

    iput-object p1, p0, Lcom/google/android/gms/car/ga;->e:Lcom/google/android/gms/car/senderprotocol/ax;

    .line 42
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;I)V
    .locals 4

    .prologue
    .line 138
    iget-object v1, p0, Lcom/google/android/gms/car/ga;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 139
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/car/ga;->b:Lcom/google/android/gms/car/gb;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    .line 141
    :try_start_1
    iget-object v0, p0, Lcom/google/android/gms/car/ga;->b:Lcom/google/android/gms/car/gb;

    iget-object v0, v0, Lcom/google/android/gms/car/gb;->a:Lcom/google/android/gms/car/ml;

    invoke-interface {v0, p1, p2, p3}, Lcom/google/android/gms/car/ml;->a(Ljava/lang/String;Ljava/lang/String;I)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 146
    :cond_0
    :goto_0
    :try_start_2
    monitor-exit v1

    return-void

    .line 142
    :catch_0
    move-exception v0

    .line 143
    const-string v2, "CAR.INST"

    const-string v3, "Error calling onInput()"

    invoke-static {v2, v3, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 146
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final a(Lcom/google/android/gms/car/ml;)Z
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 63
    iget-object v1, p0, Lcom/google/android/gms/car/ga;->d:Landroid/content/Context;

    invoke-static {}, Lcom/google/android/gms/car/ga;->getCallingUid()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/android/gms/car/of;->a(Landroid/content/Context;I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 64
    new-instance v0, Ljava/lang/SecurityException;

    const-string v1, "Wrong signature"

    invoke-direct {v0, v1}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 66
    :cond_0
    iget-object v2, p0, Lcom/google/android/gms/car/ga;->a:Ljava/lang/Object;

    monitor-enter v2

    .line 67
    :try_start_0
    iget-object v1, p0, Lcom/google/android/gms/car/ga;->b:Lcom/google/android/gms/car/gb;

    if-eqz v1, :cond_2

    .line 68
    const-string v1, "CAR.INST"

    const/4 v3, 0x3

    invoke-static {v1, v3}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 69
    const-string v1, "CAR.INST"

    const-string v3, "Unregistering existing listener."

    invoke-static {v1, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 71
    :cond_1
    iget-object v1, p0, Lcom/google/android/gms/car/ga;->b:Lcom/google/android/gms/car/gb;

    iget-object v1, v1, Lcom/google/android/gms/car/gb;->a:Lcom/google/android/gms/car/ml;

    invoke-virtual {p0, v1}, Lcom/google/android/gms/car/ga;->b(Lcom/google/android/gms/car/ml;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 74
    :cond_2
    :try_start_1
    new-instance v1, Lcom/google/android/gms/car/gb;

    invoke-direct {v1, p0, p1}, Lcom/google/android/gms/car/gb;-><init>(Lcom/google/android/gms/car/ga;Lcom/google/android/gms/car/ml;)V

    iput-object v1, p0, Lcom/google/android/gms/car/ga;->b:Lcom/google/android/gms/car/gb;

    .line 75
    iget-object v1, p0, Lcom/google/android/gms/car/ga;->b:Lcom/google/android/gms/car/gb;

    iget-object v1, v1, Lcom/google/android/gms/car/gb;->a:Lcom/google/android/gms/car/ml;

    invoke-interface {v1}, Lcom/google/android/gms/car/ml;->asBinder()Landroid/os/IBinder;

    move-result-object v1

    iget-object v3, p0, Lcom/google/android/gms/car/ga;->b:Lcom/google/android/gms/car/gb;

    const/4 v4, 0x0

    invoke-interface {v1, v3, v4}, Landroid/os/IBinder;->linkToDeath(Landroid/os/IBinder$DeathRecipient;I)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 80
    monitor-exit v2

    .line 81
    const/4 v0, 0x1

    :goto_0
    return v0

    .line 76
    :catch_0
    move-exception v1

    .line 77
    :try_start_2
    const-string v3, "CAR.INST"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Adding listener failed: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v3, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 78
    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 80
    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0
.end method

.method final b()V
    .locals 3

    .prologue
    .line 112
    iget-object v0, p0, Lcom/google/android/gms/car/ga;->b:Lcom/google/android/gms/car/gb;

    iget-object v0, v0, Lcom/google/android/gms/car/gb;->a:Lcom/google/android/gms/car/ml;

    invoke-interface {v0}, Lcom/google/android/gms/car/ml;->asBinder()Landroid/os/IBinder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/car/ga;->b:Lcom/google/android/gms/car/gb;

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/os/IBinder;->unlinkToDeath(Landroid/os/IBinder$DeathRecipient;I)Z

    .line 113
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/car/ga;->b:Lcom/google/android/gms/car/gb;

    .line 114
    return-void
.end method

.method public final b(Lcom/google/android/gms/car/ml;)Z
    .locals 2

    .prologue
    .line 86
    invoke-direct {p0, p1}, Lcom/google/android/gms/car/ga;->c(Lcom/google/android/gms/car/ml;)V

    .line 87
    iget-object v1, p0, Lcom/google/android/gms/car/ga;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 88
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/car/ga;->b:Lcom/google/android/gms/car/gb;

    if-nez v0, :cond_0

    .line 89
    const/4 v0, 0x0

    monitor-exit v1

    .line 92
    :goto_0
    return v0

    .line 91
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/car/ga;->b()V

    .line 92
    const/4 v0, 0x1

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 94
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method
