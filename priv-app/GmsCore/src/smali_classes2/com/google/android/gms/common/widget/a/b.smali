.class public abstract Lcom/google/android/gms/common/widget/a/b;
.super Landroid/support/v7/app/d;
.source "SourceFile"


# static fields
.field private static final c:Z


# instance fields
.field private a:Z

.field private b:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 36
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x12

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/google/android/gms/common/widget/a/b;->c:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 21
    invoke-direct {p0}, Landroid/support/v7/app/d;-><init>()V

    .line 23
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/common/widget/a/b;->a:Z

    return-void
.end method

.method private a(Landroid/content/Intent;)Z
    .locals 2

    .prologue
    .line 108
    invoke-virtual {p0}, Lcom/google/android/gms/common/widget/a/b;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    const/high16 v1, 0x10000

    invoke-virtual {v0, p1, v1}, Landroid/content/pm/PackageManager;->resolveActivity(Landroid/content/Intent;I)Landroid/content/pm/ResolveInfo;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method protected final a(Lcom/google/android/gms/common/widget/a/i;Landroid/content/Intent;I)Lcom/google/android/gms/common/widget/a/o;
    .locals 1

    .prologue
    .line 97
    invoke-direct {p0, p2}, Lcom/google/android/gms/common/widget/a/b;->a(Landroid/content/Intent;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 98
    new-instance v0, Lcom/google/android/gms/common/widget/a/o;

    invoke-direct {v0, p0}, Lcom/google/android/gms/common/widget/a/o;-><init>(Landroid/content/Context;)V

    .line 99
    invoke-virtual {v0, p3}, Lcom/google/android/gms/common/widget/a/o;->c(I)V

    .line 100
    invoke-virtual {v0, p2}, Lcom/google/android/gms/common/widget/a/o;->a(Landroid/content/Intent;)V

    .line 101
    invoke-virtual {p1, v0}, Lcom/google/android/gms/common/widget/a/i;->c(Lcom/google/android/gms/common/widget/a/e;)V

    .line 104
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected final a(Lcom/google/android/gms/common/widget/a/i;Landroid/content/Intent;Ljava/lang/CharSequence;)Lcom/google/android/gms/common/widget/a/o;
    .locals 1

    .prologue
    .line 81
    invoke-direct {p0, p2}, Lcom/google/android/gms/common/widget/a/b;->a(Landroid/content/Intent;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 82
    new-instance v0, Lcom/google/android/gms/common/widget/a/o;

    invoke-direct {v0, p0}, Lcom/google/android/gms/common/widget/a/o;-><init>(Landroid/content/Context;)V

    .line 83
    invoke-virtual {v0, p3}, Lcom/google/android/gms/common/widget/a/o;->a(Ljava/lang/CharSequence;)V

    .line 84
    invoke-virtual {v0, p2}, Lcom/google/android/gms/common/widget/a/o;->a(Landroid/content/Intent;)V

    .line 85
    invoke-virtual {p1, v0}, Lcom/google/android/gms/common/widget/a/i;->c(Lcom/google/android/gms/common/widget/a/e;)V

    .line 88
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected abstract a(Lcom/google/android/gms/common/widget/a/l;Landroid/os/Bundle;)V
.end method

.method protected final e()Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 66
    iget-boolean v0, p0, Lcom/google/android/gms/common/widget/a/b;->a:Z

    if-nez v0, :cond_0

    .line 67
    iput-boolean v1, p0, Lcom/google/android/gms/common/widget/a/b;->a:Z

    .line 68
    invoke-virtual {p0}, Lcom/google/android/gms/common/widget/a/b;->getPackageName()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v2

    sget-boolean v3, Lcom/google/android/gms/common/widget/a/b;->c:Z

    if-eqz v3, :cond_1

    const-string v3, "com.google"

    invoke-virtual {v2, v3, v0}, Landroid/accounts/AccountManager;->getAccountsByTypeForPackage(Ljava/lang/String;Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v0

    :goto_0
    array-length v0, v0

    if-lez v0, :cond_2

    move v0, v1

    :goto_1
    iput-boolean v0, p0, Lcom/google/android/gms/common/widget/a/b;->b:Z

    .line 70
    :cond_0
    iget-boolean v0, p0, Lcom/google/android/gms/common/widget/a/b;->b:Z

    return v0

    .line 68
    :cond_1
    const-string v0, "com.google"

    invoke-virtual {v2, v0}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v0

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 55
    invoke-super {p0, p1}, Landroid/support/v7/app/d;->onCreate(Landroid/os/Bundle;)V

    .line 57
    sget v0, Lcom/google/android/gms/common/widget/e;->c:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/common/widget/a/b;->setContentView(I)V

    .line 59
    new-instance v1, Lcom/google/android/gms/common/widget/a/l;

    invoke-direct {v1, p0}, Lcom/google/android/gms/common/widget/a/l;-><init>(Landroid/content/Context;)V

    .line 60
    invoke-virtual {p0, v1, p1}, Lcom/google/android/gms/common/widget/a/b;->a(Lcom/google/android/gms/common/widget/a/l;Landroid/os/Bundle;)V

    .line 61
    const v0, 0x102000a

    invoke-virtual {p0, v0}, Lcom/google/android/gms/common/widget/a/b;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/common/widget/a/l;->a(Landroid/support/v7/widget/RecyclerView;)V

    .line 63
    return-void
.end method
