.class public final Lcom/google/android/gms/drive/internal/bo;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/drive/ab;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 184
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/common/api/v;)Lcom/google/android/gms/common/api/am;
    .locals 1

    .prologue
    .line 83
    new-instance v0, Lcom/google/android/gms/drive/internal/bp;

    invoke-direct {v0, p0, p1}, Lcom/google/android/gms/drive/internal/bp;-><init>(Lcom/google/android/gms/drive/internal/bo;Lcom/google/android/gms/common/api/v;)V

    invoke-interface {p1, v0}, Lcom/google/android/gms/common/api/v;->b(Lcom/google/android/gms/common/api/l;)Lcom/google/android/gms/common/api/l;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/common/api/v;Lcom/google/android/gms/drive/DriveId;Lcom/google/android/gms/drive/am;II)Lcom/google/android/gms/common/api/am;
    .locals 7

    .prologue
    .line 113
    invoke-static {p3}, Lcom/google/android/gms/drive/am;->a(Lcom/google/android/gms/drive/am;)Lcom/google/android/gms/drive/am;

    move-result-object v3

    .line 114
    new-instance v0, Lcom/google/android/gms/drive/internal/br;

    move-object v1, p0

    move-object v2, p1

    move-object v4, p2

    move v5, p5

    move v6, p4

    invoke-direct/range {v0 .. v6}, Lcom/google/android/gms/drive/internal/br;-><init>(Lcom/google/android/gms/drive/internal/bo;Lcom/google/android/gms/common/api/v;Lcom/google/android/gms/drive/am;Lcom/google/android/gms/drive/DriveId;II)V

    invoke-interface {p1, v0}, Lcom/google/android/gms/common/api/v;->b(Lcom/google/android/gms/common/api/l;)Lcom/google/android/gms/common/api/l;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/common/api/v;Lcom/google/android/gms/drive/DrivePreferences;)Lcom/google/android/gms/common/api/am;
    .locals 1

    .prologue
    .line 100
    new-instance v0, Lcom/google/android/gms/drive/internal/bq;

    invoke-direct {v0, p0, p1, p2}, Lcom/google/android/gms/drive/internal/bq;-><init>(Lcom/google/android/gms/drive/internal/bo;Lcom/google/android/gms/common/api/v;Lcom/google/android/gms/drive/DrivePreferences;)V

    invoke-interface {p1, v0}, Lcom/google/android/gms/common/api/v;->b(Lcom/google/android/gms/common/api/l;)Lcom/google/android/gms/common/api/l;

    move-result-object v0

    return-object v0
.end method
