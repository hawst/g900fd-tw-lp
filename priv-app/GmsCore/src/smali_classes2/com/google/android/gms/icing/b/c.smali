.class public final Lcom/google/android/gms/icing/b/c;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final a:Lcom/google/android/gms/icing/b/h;

.field final synthetic b:Lcom/google/android/gms/icing/b/a;


# direct methods
.method protected constructor <init>(Lcom/google/android/gms/icing/b/a;Lcom/google/android/gms/icing/b/h;)V
    .locals 0

    .prologue
    .line 272
    iput-object p1, p0, Lcom/google/android/gms/icing/b/c;->b:Lcom/google/android/gms/icing/b/a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 273
    iput-object p2, p0, Lcom/google/android/gms/icing/b/c;->a:Lcom/google/android/gms/icing/b/h;

    .line 274
    return-void
.end method


# virtual methods
.method public final run()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x4

    const/4 v3, 0x3

    .line 278
    const-string v0, "AsyncScheduler running task %s"

    iget-object v1, p0, Lcom/google/android/gms/icing/b/c;->a:Lcom/google/android/gms/icing/b/h;

    invoke-static {v0, v1}, Lcom/google/android/gms/icing/c;->a(Ljava/lang/String;Ljava/lang/Object;)I

    .line 279
    iget-object v0, p0, Lcom/google/android/gms/icing/b/c;->b:Lcom/google/android/gms/icing/b/a;

    iget-object v0, v0, Lcom/google/android/gms/icing/b/a;->f:Ljava/lang/ThreadLocal;

    iget-object v1, p0, Lcom/google/android/gms/icing/b/c;->a:Lcom/google/android/gms/icing/b/h;

    iget v1, v1, Lcom/google/android/gms/icing/b/h;->i:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/ThreadLocal;->set(Ljava/lang/Object;)V

    .line 281
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/icing/b/c;->a:Lcom/google/android/gms/icing/b/h;

    invoke-virtual {v0}, Lcom/google/android/gms/icing/b/h;->d()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 283
    iget-object v0, p0, Lcom/google/android/gms/icing/b/c;->b:Lcom/google/android/gms/icing/b/a;

    iget-object v1, p0, Lcom/google/android/gms/icing/b/c;->a:Lcom/google/android/gms/icing/b/h;

    invoke-virtual {v0}, Lcom/google/android/gms/icing/b/a;->d()V

    .line 284
    iget-object v0, p0, Lcom/google/android/gms/icing/b/c;->b:Lcom/google/android/gms/icing/b/a;

    iget-object v0, v0, Lcom/google/android/gms/icing/b/a;->f:Ljava/lang/ThreadLocal;

    invoke-virtual {v0, v5}, Ljava/lang/ThreadLocal;->set(Ljava/lang/Object;)V

    .line 285
    iget-object v0, p0, Lcom/google/android/gms/icing/b/c;->a:Lcom/google/android/gms/icing/b/h;

    invoke-virtual {v0, v3, v4}, Lcom/google/android/gms/icing/b/h;->a(II)V

    .line 286
    return-void

    .line 283
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/google/android/gms/icing/b/c;->b:Lcom/google/android/gms/icing/b/a;

    iget-object v2, p0, Lcom/google/android/gms/icing/b/c;->a:Lcom/google/android/gms/icing/b/h;

    invoke-virtual {v1}, Lcom/google/android/gms/icing/b/a;->d()V

    .line 284
    iget-object v1, p0, Lcom/google/android/gms/icing/b/c;->b:Lcom/google/android/gms/icing/b/a;

    iget-object v1, v1, Lcom/google/android/gms/icing/b/a;->f:Ljava/lang/ThreadLocal;

    invoke-virtual {v1, v5}, Ljava/lang/ThreadLocal;->set(Ljava/lang/Object;)V

    .line 285
    iget-object v1, p0, Lcom/google/android/gms/icing/b/c;->a:Lcom/google/android/gms/icing/b/h;

    invoke-virtual {v1, v3, v4}, Lcom/google/android/gms/icing/b/h;->a(II)V

    throw v0
.end method
