.class public abstract Lcom/google/android/gms/games/service/a/b;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/games/service/e;


# instance fields
.field protected final a:Lcom/google/android/gms/common/server/ClientContext;

.field private final b:[Lcom/google/android/gms/common/data/DataHolder;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/common/server/ClientContext;I)V
    .locals 1

    .prologue
    .line 43
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 44
    invoke-static {p1}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/server/ClientContext;

    iput-object v0, p0, Lcom/google/android/gms/games/service/a/b;->a:Lcom/google/android/gms/common/server/ClientContext;

    .line 45
    if-lez p2, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/android/gms/common/internal/bx;->a(Z)V

    .line 46
    new-array v0, p2, [Lcom/google/android/gms/common/data/DataHolder;

    iput-object v0, p0, Lcom/google/android/gms/games/service/a/b;->b:[Lcom/google/android/gms/common/data/DataHolder;

    .line 47
    return-void

    .line 45
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method protected abstract a(Lcom/google/android/gms/games/a/t;I)Lcom/google/android/gms/common/data/DataHolder;
.end method

.method public final a()V
    .locals 0

    .prologue
    .line 101
    return-void
.end method

.method public final a(Landroid/content/Context;Lcom/google/android/gms/games/a/t;)V
    .locals 7

    .prologue
    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 51
    move v0, v1

    move v2, v1

    .line 52
    :goto_0
    iget-object v4, p0, Lcom/google/android/gms/games/service/a/b;->b:[Lcom/google/android/gms/common/data/DataHolder;

    array-length v4, v4

    if-ge v0, v4, :cond_2

    .line 54
    :try_start_0
    iget-object v4, p0, Lcom/google/android/gms/games/service/a/b;->b:[Lcom/google/android/gms/common/data/DataHolder;

    invoke-virtual {p0, p2, v0}, Lcom/google/android/gms/games/service/a/b;->a(Lcom/google/android/gms/games/a/t;I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v5

    aput-object v5, v4, v0
    :try_end_0
    .catch Lcom/google/android/gms/auth/q; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/google/android/gms/games/h/c/a; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_2

    .line 52
    :cond_0
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 55
    :catch_0
    move-exception v4

    .line 56
    const-string v5, "MultiDataOperation"

    const-string v6, "Auth exception while performing operation, requesting reconnect"

    invoke-static {v5, v6, v4}, Lcom/google/android/gms/games/internal/dq;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 58
    iget-object v4, p0, Lcom/google/android/gms/games/service/a/b;->b:[Lcom/google/android/gms/common/data/DataHolder;

    const/4 v5, 0x2

    invoke-static {v5}, Lcom/google/android/gms/common/data/DataHolder;->b(I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v5

    aput-object v5, v4, v0

    goto :goto_1

    .line 60
    :catch_1
    move-exception v4

    .line 61
    const-string v5, "MultiDataOperation"

    invoke-virtual {v4}, Lcom/google/android/gms/games/h/c/a;->c()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6, v4}, Lcom/google/android/gms/games/internal/dq;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 62
    iget-object v5, p0, Lcom/google/android/gms/games/service/a/b;->b:[Lcom/google/android/gms/common/data/DataHolder;

    invoke-virtual {v4}, Lcom/google/android/gms/games/h/c/a;->b()I

    move-result v6

    invoke-static {v6}, Lcom/google/android/gms/common/data/DataHolder;->b(I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v6

    aput-object v6, v5, v0

    .line 68
    invoke-virtual {v4}, Lcom/google/android/gms/games/h/c/a;->a()I

    move-result v4

    .line 69
    const/16 v5, 0x5dc

    if-ne v4, v5, :cond_1

    move v2, v3

    .line 70
    goto :goto_1

    .line 71
    :cond_1
    const/16 v5, 0x3eb

    if-ne v4, v5, :cond_0

    .line 72
    invoke-virtual {p2, p1}, Lcom/google/android/gms/games/a/t;->c(Landroid/content/Context;)V

    goto :goto_1

    .line 74
    :catch_2
    move-exception v4

    .line 75
    const-string v5, "MultiDataOperation"

    const-string v6, "Killing (on development devices) due to RuntimeException"

    invoke-static {p1, v5, v6, v4}, Lcom/google/android/gms/games/internal/dq;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 77
    iget-object v4, p0, Lcom/google/android/gms/games/service/a/b;->b:[Lcom/google/android/gms/common/data/DataHolder;

    invoke-static {v3}, Lcom/google/android/gms/common/data/DataHolder;->b(I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v5

    aput-object v5, v4, v0

    goto :goto_1

    .line 82
    :cond_2
    if-eqz v2, :cond_3

    .line 83
    iget-object v0, p0, Lcom/google/android/gms/games/service/a/b;->a:Lcom/google/android/gms/common/server/ClientContext;

    invoke-static {p1, v0, p2}, Lcom/google/android/gms/games/k/a;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Lcom/google/android/gms/games/a/t;)V

    .line 84
    iget-object v0, p0, Lcom/google/android/gms/games/service/a/b;->a:Lcom/google/android/gms/common/server/ClientContext;

    invoke-virtual {v0}, Lcom/google/android/gms/common/server/ClientContext;->d()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/google/android/gms/games/i/a;->e(Landroid/content/Context;Ljava/lang/String;)V

    .line 89
    :cond_3
    :try_start_1
    iget-object v0, p0, Lcom/google/android/gms/games/service/a/b;->b:[Lcom/google/android/gms/common/data/DataHolder;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/service/a/b;->a([Lcom/google/android/gms/common/data/DataHolder;)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_3
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 94
    :goto_2
    iget-object v0, p0, Lcom/google/android/gms/games/service/a/b;->b:[Lcom/google/android/gms/common/data/DataHolder;

    array-length v0, v0

    if-ge v1, v0, :cond_5

    .line 95
    iget-object v0, p0, Lcom/google/android/gms/games/service/a/b;->b:[Lcom/google/android/gms/common/data/DataHolder;

    aget-object v0, v0, v1

    invoke-virtual {v0}, Lcom/google/android/gms/common/data/DataHolder;->j()V

    .line 94
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 90
    :catch_3
    move-exception v0

    .line 92
    :try_start_2
    const-string v2, "MultiDataOperation"

    const-string v3, "When providing result "

    invoke-static {v2, v3, v0}, Lcom/google/android/gms/games/internal/dq;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 94
    :goto_3
    iget-object v0, p0, Lcom/google/android/gms/games/service/a/b;->b:[Lcom/google/android/gms/common/data/DataHolder;

    array-length v0, v0

    if-ge v1, v0, :cond_5

    .line 95
    iget-object v0, p0, Lcom/google/android/gms/games/service/a/b;->b:[Lcom/google/android/gms/common/data/DataHolder;

    aget-object v0, v0, v1

    invoke-virtual {v0}, Lcom/google/android/gms/common/data/DataHolder;->j()V

    .line 94
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    :goto_4
    iget-object v2, p0, Lcom/google/android/gms/games/service/a/b;->b:[Lcom/google/android/gms/common/data/DataHolder;

    array-length v2, v2

    if-ge v1, v2, :cond_4

    .line 95
    iget-object v2, p0, Lcom/google/android/gms/games/service/a/b;->b:[Lcom/google/android/gms/common/data/DataHolder;

    aget-object v2, v2, v1

    invoke-virtual {v2}, Lcom/google/android/gms/common/data/DataHolder;->j()V

    .line 94
    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    :cond_4
    throw v0

    .line 97
    :cond_5
    return-void

    .line 94
    :catchall_0
    move-exception v0

    goto :goto_4
.end method

.method protected abstract a([Lcom/google/android/gms/common/data/DataHolder;)V
.end method
