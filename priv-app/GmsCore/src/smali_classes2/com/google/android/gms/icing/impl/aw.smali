.class public final Lcom/google/android/gms/icing/impl/aw;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 406
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Ljava/lang/String;Landroid/content/Context;)Lcom/google/android/gms/icing/impl/bn;
    .locals 3

    .prologue
    .line 411
    :try_start_0
    new-instance v0, Lcom/google/android/gms/icing/impl/bn;

    invoke-virtual {p1}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v1

    invoke-direct {v0, p1, v1, p0}, Lcom/google/android/gms/icing/impl/bn;-><init>(Landroid/content/Context;Ljava/io/File;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 414
    :goto_0
    return-object v0

    .line 412
    :catch_0
    move-exception v0

    const-string v1, "Unable to create storage"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/icing/c;->b(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 414
    const/4 v0, 0x0

    goto :goto_0
.end method
