.class final Lcom/google/android/gms/games/service/statemachine/roomclient/ServiceIpcState;
.super Lcom/google/android/gms/games/service/statemachine/roomclient/ca;
.source "SourceFile"


# annotations
.annotation build Lcom/google/android/gms/common/util/RetainForClient;
.end annotation


# direct methods
.method public constructor <init>(Lcom/google/android/gms/games/service/statemachine/roomclient/cb;)V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0, p1}, Lcom/google/android/gms/games/service/statemachine/roomclient/ca;-><init>(Lcom/google/android/gms/games/service/statemachine/roomclient/cb;)V

    .line 20
    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Message;)Z
    .locals 3

    .prologue
    .line 31
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 43
    sget-boolean v0, Lcom/google/android/gms/games/service/statemachine/roomclient/ServiceIpcState;->g:Z

    :goto_0
    return v0

    .line 34
    :pswitch_0
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/ServiceIpcState;->b:Lcom/google/android/gms/games/service/statemachine/roomclient/cc;

    iget-object v0, v0, Lcom/google/android/gms/games/service/statemachine/roomclient/cc;->b:Lcom/google/android/gms/games/internal/ed;

    new-instance v1, Landroid/os/Binder;

    invoke-direct {v1}, Landroid/os/Binder;-><init>()V

    iget-object v2, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/ServiceIpcState;->b:Lcom/google/android/gms/games/service/statemachine/roomclient/cc;

    iget-object v2, v2, Lcom/google/android/gms/games/service/statemachine/roomclient/cc;->e:Lcom/google/android/gms/games/internal/eg;

    invoke-interface {v0, v1, v2}, Lcom/google/android/gms/games/internal/ed;->a(Landroid/os/IBinder;Lcom/google/android/gms/games/internal/eg;)V

    .line 35
    iget-object v0, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/ServiceIpcState;->c:Lcom/google/android/gms/games/service/statemachine/d;

    invoke-interface {v0, p1}, Lcom/google/android/gms/games/service/statemachine/d;->a(Landroid/os/Message;)V

    .line 36
    iget-object v0, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/ServiceIpcState;->a:Lcom/google/android/gms/games/service/statemachine/roomclient/cd;

    iget-object v0, v0, Lcom/google/android/gms/games/service/statemachine/roomclient/cd;->p:Lcom/google/android/gms/games/service/statemachine/roomclient/WaitServiceSetupState;

    invoke-virtual {v0}, Lcom/google/android/gms/games/service/statemachine/roomclient/WaitServiceSetupState;->b()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 40
    :goto_1
    sget-boolean v0, Lcom/google/android/gms/games/service/statemachine/roomclient/ServiceIpcState;->f:Z

    goto :goto_0

    .line 37
    :catch_0
    move-exception v0

    .line 38
    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/service/statemachine/roomclient/ServiceIpcState;->a(Landroid/os/RemoteException;)V

    goto :goto_1

    .line 31
    :pswitch_data_0
    .packed-switch 0x1d
        :pswitch_0
    .end packed-switch
.end method

.method public final b()V
    .locals 0

    .prologue
    .line 26
    invoke-virtual {p0}, Lcom/google/android/gms/games/service/statemachine/roomclient/ServiceIpcState;->c()V

    .line 27
    return-void
.end method
