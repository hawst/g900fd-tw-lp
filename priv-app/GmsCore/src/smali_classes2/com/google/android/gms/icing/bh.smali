.class public final Lcom/google/android/gms/icing/bh;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# static fields
.field private static volatile c:[Lcom/google/android/gms/icing/bh;


# instance fields
.field public a:Lcom/google/android/gms/icing/ag;

.field public b:Lcom/google/android/gms/icing/o;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 9177
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 9178
    iput-object v0, p0, Lcom/google/android/gms/icing/bh;->a:Lcom/google/android/gms/icing/ag;

    iput-object v0, p0, Lcom/google/android/gms/icing/bh;->b:Lcom/google/android/gms/icing/o;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/icing/bh;->cachedSize:I

    .line 9179
    return-void
.end method

.method public static a()[Lcom/google/android/gms/icing/bh;
    .locals 2

    .prologue
    .line 9160
    sget-object v0, Lcom/google/android/gms/icing/bh;->c:[Lcom/google/android/gms/icing/bh;

    if-nez v0, :cond_1

    .line 9161
    sget-object v1, Lcom/google/protobuf/nano/h;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 9163
    :try_start_0
    sget-object v0, Lcom/google/android/gms/icing/bh;->c:[Lcom/google/android/gms/icing/bh;

    if-nez v0, :cond_0

    .line 9164
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/android/gms/icing/bh;

    sput-object v0, Lcom/google/android/gms/icing/bh;->c:[Lcom/google/android/gms/icing/bh;

    .line 9166
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 9168
    :cond_1
    sget-object v0, Lcom/google/android/gms/icing/bh;->c:[Lcom/google/android/gms/icing/bh;

    return-object v0

    .line 9166
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 9242
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->computeSerializedSize()I

    move-result v0

    .line 9243
    iget-object v1, p0, Lcom/google/android/gms/icing/bh;->a:Lcom/google/android/gms/icing/ag;

    if-eqz v1, :cond_0

    .line 9244
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/gms/icing/bh;->a:Lcom/google/android/gms/icing/ag;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 9247
    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/icing/bh;->b:Lcom/google/android/gms/icing/o;

    if-eqz v1, :cond_1

    .line 9248
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/gms/icing/bh;->b:Lcom/google/android/gms/icing/o;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 9251
    :cond_1
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 9190
    if-ne p1, p0, :cond_1

    .line 9215
    :cond_0
    :goto_0
    return v0

    .line 9193
    :cond_1
    instance-of v2, p1, Lcom/google/android/gms/icing/bh;

    if-nez v2, :cond_2

    move v0, v1

    .line 9194
    goto :goto_0

    .line 9196
    :cond_2
    check-cast p1, Lcom/google/android/gms/icing/bh;

    .line 9197
    iget-object v2, p0, Lcom/google/android/gms/icing/bh;->a:Lcom/google/android/gms/icing/ag;

    if-nez v2, :cond_3

    .line 9198
    iget-object v2, p1, Lcom/google/android/gms/icing/bh;->a:Lcom/google/android/gms/icing/ag;

    if-eqz v2, :cond_4

    move v0, v1

    .line 9199
    goto :goto_0

    .line 9202
    :cond_3
    iget-object v2, p0, Lcom/google/android/gms/icing/bh;->a:Lcom/google/android/gms/icing/ag;

    iget-object v3, p1, Lcom/google/android/gms/icing/bh;->a:Lcom/google/android/gms/icing/ag;

    invoke-virtual {v2, v3}, Lcom/google/android/gms/icing/ag;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    move v0, v1

    .line 9203
    goto :goto_0

    .line 9206
    :cond_4
    iget-object v2, p0, Lcom/google/android/gms/icing/bh;->b:Lcom/google/android/gms/icing/o;

    if-nez v2, :cond_5

    .line 9207
    iget-object v2, p1, Lcom/google/android/gms/icing/bh;->b:Lcom/google/android/gms/icing/o;

    if-eqz v2, :cond_0

    move v0, v1

    .line 9208
    goto :goto_0

    .line 9211
    :cond_5
    iget-object v2, p0, Lcom/google/android/gms/icing/bh;->b:Lcom/google/android/gms/icing/o;

    iget-object v3, p1, Lcom/google/android/gms/icing/bh;->b:Lcom/google/android/gms/icing/o;

    invoke-virtual {v2, v3}, Lcom/google/android/gms/icing/o;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    .line 9212
    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 9220
    iget-object v0, p0, Lcom/google/android/gms/icing/bh;->a:Lcom/google/android/gms/icing/ag;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    .line 9223
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/android/gms/icing/bh;->b:Lcom/google/android/gms/icing/o;

    if-nez v2, :cond_1

    :goto_1
    add-int/2addr v0, v1

    .line 9225
    return v0

    .line 9220
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/icing/bh;->a:Lcom/google/android/gms/icing/ag;

    invoke-virtual {v0}, Lcom/google/android/gms/icing/ag;->hashCode()I

    move-result v0

    goto :goto_0

    .line 9223
    :cond_1
    iget-object v1, p0, Lcom/google/android/gms/icing/bh;->b:Lcom/google/android/gms/icing/o;

    invoke-virtual {v1}, Lcom/google/android/gms/icing/o;->hashCode()I

    move-result v1

    goto :goto_1
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 9154
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Lcom/google/android/gms/icing/bh;->a:Lcom/google/android/gms/icing/ag;

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/android/gms/icing/ag;

    invoke-direct {v0}, Lcom/google/android/gms/icing/ag;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/icing/bh;->a:Lcom/google/android/gms/icing/ag;

    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/icing/bh;->a:Lcom/google/android/gms/icing/ag;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Lcom/google/android/gms/icing/bh;->b:Lcom/google/android/gms/icing/o;

    if-nez v0, :cond_2

    new-instance v0, Lcom/google/android/gms/icing/o;

    invoke-direct {v0}, Lcom/google/android/gms/icing/o;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/icing/bh;->b:Lcom/google/android/gms/icing/o;

    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/icing/bh;->b:Lcom/google/android/gms/icing/o;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 9231
    iget-object v0, p0, Lcom/google/android/gms/icing/bh;->a:Lcom/google/android/gms/icing/ag;

    if-eqz v0, :cond_0

    .line 9232
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/gms/icing/bh;->a:Lcom/google/android/gms/icing/ag;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 9234
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/icing/bh;->b:Lcom/google/android/gms/icing/o;

    if-eqz v0, :cond_1

    .line 9235
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/android/gms/icing/bh;->b:Lcom/google/android/gms/icing/o;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 9237
    :cond_1
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 9238
    return-void
.end method
