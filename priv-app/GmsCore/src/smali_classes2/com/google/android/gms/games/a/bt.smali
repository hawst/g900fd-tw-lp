.class final Lcom/google/android/gms/games/a/bt;
.super Lcom/google/android/gms/games/a/bb;
.source "SourceFile"


# static fields
.field private static final a:Ljava/util/concurrent/locks/ReentrantLock;

.field private static final b:[Ljava/lang/String;

.field private static final c:[Ljava/lang/String;


# instance fields
.field private final d:Lcom/google/android/gms/games/h/a/ew;

.field private final e:Lcom/google/android/gms/games/h/a/ex;

.field private final f:Ljava/util/Random;

.field private g:Lcom/google/android/gms/games/a/bu;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 65
    new-instance v0, Ljava/util/concurrent/locks/ReentrantLock;

    invoke-direct {v0}, Ljava/util/concurrent/locks/ReentrantLock;-><init>()V

    sput-object v0, Lcom/google/android/gms/games/a/bt;->a:Ljava/util/concurrent/locks/ReentrantLock;

    .line 67
    const/16 v0, 0x10

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "external_match_id"

    aput-object v1, v0, v3

    const-string v1, "creator_external"

    aput-object v1, v0, v4

    const-string v1, "creation_timestamp"

    aput-object v1, v0, v5

    const/4 v1, 0x3

    const-string v2, "last_updater_external"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "last_updated_timestamp"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "pending_participant_external"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "data"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "status"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "description"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "variant"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "version"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "has_automatch_criteria"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "automatch_min_players"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "automatch_max_players"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string v2, "automatch_bit_mask"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string v2, "automatch_wait_estimate_sec"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/gms/games/a/bt;->b:[Ljava/lang/String;

    .line 86
    new-array v0, v5, [[Ljava/lang/String;

    sget-object v1, Lcom/google/android/gms/games/a/bt;->b:[Ljava/lang/String;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/android/gms/games/k/a;->g:Lcom/google/android/gms/games/provider/a;

    iget-object v1, v1, Lcom/google/android/gms/games/provider/a;->b:[Ljava/lang/String;

    aput-object v1, v0, v4

    invoke-static {v0}, Lcom/google/android/gms/common/util/h;->a([[Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    sput-object v0, Lcom/google/android/gms/games/a/bt;->c:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/gms/games/a/bb;Lcom/google/android/gms/common/server/n;Lcom/google/android/gms/common/server/n;)V
    .locals 4

    .prologue
    .line 160
    const-string v0, "RealTimeAgent"

    sget-object v1, Lcom/google/android/gms/games/a/bt;->a:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-direct {p0, v0, v1, p1}, Lcom/google/android/gms/games/a/bb;-><init>(Ljava/lang/String;Ljava/util/concurrent/locks/ReentrantLock;Lcom/google/android/gms/games/a/bb;)V

    .line 161
    new-instance v0, Lcom/google/android/gms/games/h/a/ew;

    invoke-direct {v0, p2}, Lcom/google/android/gms/games/h/a/ew;-><init>(Lcom/google/android/gms/common/server/n;)V

    iput-object v0, p0, Lcom/google/android/gms/games/a/bt;->d:Lcom/google/android/gms/games/h/a/ew;

    .line 162
    new-instance v0, Lcom/google/android/gms/games/h/a/ex;

    invoke-direct {v0, p3}, Lcom/google/android/gms/games/h/a/ex;-><init>(Lcom/google/android/gms/common/server/n;)V

    iput-object v0, p0, Lcom/google/android/gms/games/a/bt;->e:Lcom/google/android/gms/games/h/a/ex;

    .line 163
    new-instance v0, Ljava/util/Random;

    invoke-static {}, Lcom/google/android/gms/common/util/r;->c()Lcom/google/android/gms/common/util/p;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/gms/common/util/p;->a()J

    move-result-wide v2

    invoke-direct {v0, v2, v3}, Ljava/util/Random;-><init>(J)V

    iput-object v0, p0, Lcom/google/android/gms/games/a/bt;->f:Ljava/util/Random;

    .line 164
    return-void
.end method

.method private a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Lcom/google/android/gms/games/h/a/ej;Ljava/lang/String;)Lcom/google/android/gms/common/data/DataHolder;
    .locals 3

    .prologue
    .line 458
    new-instance v0, Lcom/google/android/gms/games/a/bu;

    invoke-virtual {p3}, Lcom/google/android/gms/games/h/a/ej;->e()Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, p2, p3, p4}, Lcom/google/android/gms/games/a/bf;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Lcom/google/android/gms/games/h/a/ej;Ljava/lang/String;)Ljava/util/HashMap;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/games/a/bu;-><init>(Ljava/lang/String;Ljava/util/HashMap;)V

    iput-object v0, p0, Lcom/google/android/gms/games/a/bt;->g:Lcom/google/android/gms/games/a/bu;

    .line 460
    iget-object v0, p0, Lcom/google/android/gms/games/a/bt;->g:Lcom/google/android/gms/games/a/bu;

    invoke-virtual {v0}, Lcom/google/android/gms/games/a/bu;->a()Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    return-object v0
.end method

.method private static a(Landroid/content/Context;I)Lcom/google/android/gms/games/h/a/cm;
    .locals 6

    .prologue
    .line 433
    const-string v0, "connectivity"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    .line 435
    invoke-virtual {v0}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v2

    .line 436
    if-nez v2, :cond_0

    .line 437
    const/4 v0, 0x0

    .line 442
    :goto_0
    return-object v0

    .line 440
    :cond_0
    const-string v0, "phone"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Landroid/telephony/TelephonyManager;

    .line 442
    new-instance v0, Lcom/google/android/gms/games/h/a/cm;

    invoke-virtual {v2}, Landroid/net/NetworkInfo;->getSubtype()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v2}, Landroid/net/NetworkInfo;->getType()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v4}, Landroid/telephony/TelephonyManager;->getNetworkOperator()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4}, Landroid/telephony/TelephonyManager;->getNetworkOperatorName()Ljava/lang/String;

    move-result-object v4

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/games/h/a/cm;-><init>(Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;)V

    goto :goto_0
.end method

.method static synthetic a()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 61
    sget-object v0, Lcom/google/android/gms/games/a/bt;->c:[Ljava/lang/String;

    return-object v0
.end method

.method private static b(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 509
    invoke-static {p1, p2}, Lcom/google/android/gms/games/provider/ae;->a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 510
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    invoke-virtual {v1, v0, v2, v2}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 513
    sget-object v0, Lcom/google/android/gms/games/internal/c/a;->a:Landroid/net/Uri;

    invoke-static {p0, v0}, Lcom/google/android/gms/games/a/l;->c(Landroid/content/Context;Landroid/net/Uri;)V

    .line 514
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/games/h/a/ep;)I
    .locals 8

    .prologue
    const/4 v6, 0x0

    .line 372
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/games/a/bt;->d:Lcom/google/android/gms/games/h/a/ew;

    invoke-static {p1}, Lcom/google/android/gms/games/a/l;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    new-instance v4, Lcom/google/android/gms/games/h/a/eq;

    invoke-direct {v4, p5, p4}, Lcom/google/android/gms/games/h/a/eq;-><init>(Lcom/google/android/gms/games/h/a/ep;Ljava/lang/String;)V

    const-string v2, "rooms/%1$s/leave"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-static {p3}, Lcom/google/android/gms/games/h/a/ew;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v3, v5

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    if-eqz v1, :cond_0

    const-string v2, "language"

    invoke-static {v1}, Lcom/google/android/gms/games/h/a/ew;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v3, v2, v1}, Lcom/google/android/gms/games/h/a/ew;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    :cond_0
    iget-object v0, v0, Lcom/google/android/gms/games/h/a/ew;->a:Lcom/google/android/gms/common/server/n;

    const/4 v2, 0x1

    const-class v5, Lcom/google/android/gms/games/h/a/ej;

    move-object v1, p2

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/common/server/n;->a(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;Ljava/lang/Object;Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse;
    :try_end_0
    .catch Lcom/android/volley/ac; {:try_start_0 .. :try_end_0} :catch_0

    .line 381
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/games/a/bt;->g:Lcom/google/android/gms/games/a/bu;

    move v0, v6

    .line 382
    :goto_0
    return v0

    .line 374
    :catch_0
    move-exception v0

    .line 375
    const-string v1, "RealTimeAgent"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Failed to leave match: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/android/volley/ac;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/gms/games/internal/dq;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 376
    const-string v1, "RealTimeAgent"

    const/16 v2, 0x1b58

    invoke-static {v1, v0, v2}, Lcom/google/android/gms/games/a/bf;->a(Ljava/lang/String;Lcom/android/volley/ac;I)I

    move-result v0

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/games/a/au;Ljava/lang/String;)I
    .locals 10

    .prologue
    const/16 v9, 0x1b58

    const/4 v6, 0x0

    .line 267
    iget-object v7, p1, Lcom/google/android/gms/games/a/au;->a:Landroid/content/Context;

    .line 268
    iget-object v1, p1, Lcom/google/android/gms/games/a/au;->b:Lcom/google/android/gms/common/server/ClientContext;

    .line 271
    :try_start_0
    iget-boolean v0, p1, Lcom/google/android/gms/games/a/au;->h:Z

    if-eqz v0, :cond_1

    .line 272
    iget-object v0, p0, Lcom/google/android/gms/games/a/bt;->e:Lcom/google/android/gms/games/h/a/ex;

    invoke-static {v7}, Lcom/google/android/gms/games/a/l;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "rooms/%1$s/decline"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-static {p2}, Lcom/google/android/gms/games/h/a/ex;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    if-eqz v2, :cond_0

    const-string v4, "language"

    invoke-static {v2}, Lcom/google/android/gms/games/h/a/ex;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v4, v2}, Lcom/google/android/gms/games/h/a/ex;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    :cond_0
    iget-object v0, v0, Lcom/google/android/gms/games/h/a/ex;->a:Lcom/google/android/gms/common/server/n;

    const/4 v2, 0x1

    const/4 v4, 0x0

    const-class v5, Lcom/google/android/gms/games/h/a/ej;

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/common/server/n;->a(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;Ljava/lang/Object;Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse;

    move v0, v6

    .line 286
    :goto_0
    if-ne v0, v9, :cond_3

    .line 293
    :goto_1
    return v0

    .line 275
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/games/a/bt;->d:Lcom/google/android/gms/games/h/a/ew;

    invoke-static {v7}, Lcom/google/android/gms/games/a/l;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "rooms/%1$s/decline"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-static {p2}, Lcom/google/android/gms/games/h/a/ew;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    if-eqz v2, :cond_2

    const-string v4, "language"

    invoke-static {v2}, Lcom/google/android/gms/games/h/a/ew;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v4, v2}, Lcom/google/android/gms/games/h/a/ew;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    :cond_2
    iget-object v0, v0, Lcom/google/android/gms/games/h/a/ew;->a:Lcom/google/android/gms/common/server/n;

    const/4 v2, 0x1

    const/4 v4, 0x0

    const-class v5, Lcom/google/android/gms/games/h/a/ej;

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/common/server/n;->a(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;Ljava/lang/Object;Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse;
    :try_end_0
    .catch Lcom/android/volley/ac; {:try_start_0 .. :try_end_0} :catch_0

    move v0, v6

    .line 283
    goto :goto_0

    .line 278
    :catch_0
    move-exception v0

    .line 279
    const-string v2, "RealTimeAgent"

    const-string v3, "Failed to decline invitation"

    invoke-static {v2, v3}, Lcom/google/android/gms/games/internal/dq;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 281
    const-string v2, "RealTimeAgent"

    invoke-static {v2, v0, v9}, Lcom/google/android/gms/games/a/bf;->a(Ljava/lang/String;Lcom/android/volley/ac;I)I

    move-result v0

    goto :goto_0

    .line 291
    :cond_3
    invoke-static {v7, v1, p2}, Lcom/google/android/gms/games/a/bt;->b(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)V

    goto :goto_1
.end method

.method public final a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Lcom/google/android/gms/games/h/a/ev;)Lcom/google/android/gms/common/data/DataHolder;
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 475
    iget-object v0, p0, Lcom/google/android/gms/games/a/bt;->g:Lcom/google/android/gms/games/a/bu;

    if-nez v0, :cond_0

    .line 476
    const-string v0, "RealTimeAgent"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "mRoomCache is null when receiving status update for room "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p3}, Lcom/google/android/gms/games/h/a/ev;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 478
    invoke-static {v3}, Lcom/google/android/gms/common/data/DataHolder;->b(I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    .line 488
    :goto_0
    return-object v0

    .line 479
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/games/a/bt;->g:Lcom/google/android/gms/games/a/bu;

    invoke-virtual {p3}, Lcom/google/android/gms/games/h/a/ev;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/a/bu;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 480
    const-string v0, "RealTimeAgent"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "mRoomCache.mRoomId = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/gms/games/a/bt;->g:Lcom/google/android/gms/games/a/bu;

    iget-object v2, v2, Lcom/google/android/gms/games/a/bu;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " when receiving status update for room "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p3}, Lcom/google/android/gms/games/h/a/ev;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 482
    invoke-static {v3}, Lcom/google/android/gms/common/data/DataHolder;->b(I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    goto :goto_0

    .line 484
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/games/a/bt;->g:Lcom/google/android/gms/games/a/bu;

    iget-object v1, v0, Lcom/google/android/gms/games/a/bu;->b:Ljava/util/HashMap;

    .line 485
    iget-object v0, p0, Lcom/google/android/gms/games/a/bt;->g:Lcom/google/android/gms/games/a/bu;

    iget-object v2, v0, Lcom/google/android/gms/games/a/bu;->b:Ljava/util/HashMap;

    if-eqz v2, :cond_2

    iget-object v2, v0, Lcom/google/android/gms/games/a/bu;->b:Ljava/util/HashMap;

    invoke-virtual {v2}, Ljava/util/HashMap;->size()I

    move-result v2

    if-nez v2, :cond_3

    :cond_2
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    :goto_1
    invoke-static {p1, v1, v0, p3}, Lcom/google/android/gms/games/a/bf;->a(Landroid/content/Context;Ljava/util/HashMap;Landroid/content/ContentValues;Lcom/google/android/gms/games/h/a/ev;)V

    .line 487
    iget-object v0, p0, Lcom/google/android/gms/games/a/bt;->g:Lcom/google/android/gms/games/a/bu;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/a/bu;->a(Ljava/util/HashMap;)V

    .line 488
    iget-object v0, p0, Lcom/google/android/gms/games/a/bt;->g:Lcom/google/android/gms/games/a/bu;

    invoke-virtual {v0}, Lcom/google/android/gms/games/a/bu;->a()Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    goto :goto_0

    .line 485
    :cond_3
    iget-object v0, v0, Lcom/google/android/gms/games/a/bu;->b:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    const-string v2, "No base Room entry in cache!"

    invoke-static {v0, v2}, Lcom/google/android/gms/common/internal/e;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/ContentValues;

    const-string v2, "No base Room entry values in cache!"

    invoke-static {v0, v2}, Lcom/google/android/gms/common/internal/e;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    sget-object v2, Lcom/google/android/gms/games/a/bt;->b:[Ljava/lang/String;

    invoke-static {v0, v2}, Lcom/google/android/gms/games/a/l;->a(Landroid/content/ContentValues;[Ljava/lang/String;)Landroid/content/ContentValues;

    move-result-object v0

    goto :goto_1
.end method

.method public final a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)Lcom/google/android/gms/common/data/DataHolder;
    .locals 6

    .prologue
    .line 398
    iget-object v0, p0, Lcom/google/android/gms/games/a/bt;->g:Lcom/google/android/gms/games/a/bu;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/games/a/bt;->g:Lcom/google/android/gms/games/a/bu;

    invoke-virtual {v0, p3}, Lcom/google/android/gms/games/a/bu;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 399
    iget-object v0, p0, Lcom/google/android/gms/games/a/bt;->g:Lcom/google/android/gms/games/a/bu;

    invoke-virtual {v0}, Lcom/google/android/gms/games/a/bu;->a()Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    .line 415
    :goto_0
    return-object v0

    .line 403
    :cond_0
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/games/a/bt;->d:Lcom/google/android/gms/games/h/a/ew;

    invoke-static {p1}, Lcom/google/android/gms/games/a/l;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "rooms/%1$s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {p3}, Lcom/google/android/gms/games/h/a/ew;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    if-eqz v1, :cond_1

    const-string v2, "language"

    invoke-static {v1}, Lcom/google/android/gms/games/h/a/ew;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v3, v2, v1}, Lcom/google/android/gms/games/h/a/ew;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    :cond_1
    iget-object v0, v0, Lcom/google/android/gms/games/h/a/ew;->a:Lcom/google/android/gms/common/server/n;

    const/4 v2, 0x0

    const/4 v4, 0x0

    const-class v5, Lcom/google/android/gms/games/h/a/ej;

    move-object v1, p2

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/common/server/n;->a(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;Ljava/lang/Object;Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/h/a/ej;
    :try_end_0
    .catch Lcom/android/volley/ac; {:try_start_0 .. :try_end_0} :catch_0

    .line 413
    invoke-virtual {v0}, Lcom/google/android/gms/games/h/a/ej;->b()Ljava/lang/String;

    move-result-object v1

    .line 414
    invoke-static {v1}, Lcom/google/android/gms/common/internal/e;->a(Ljava/lang/Object;)V

    .line 415
    invoke-direct {p0, p1, p2, v0, v1}, Lcom/google/android/gms/games/a/bt;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Lcom/google/android/gms/games/h/a/ej;Ljava/lang/String;)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    goto :goto_0

    .line 404
    :catch_0
    move-exception v0

    .line 405
    const-string v1, "RealTimeAgent"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Failed to retrieve room: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/android/volley/ac;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/gms/games/internal/dq;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 406
    invoke-static {}, Lcom/google/android/gms/games/internal/dq;->a()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 407
    const-string v1, "RealTimeAgent"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/server/b/c;->a(Lcom/android/volley/ac;Ljava/lang/String;)Lcom/google/android/gms/common/server/b/b;

    .line 409
    :cond_2
    const/4 v0, 0x4

    invoke-static {v0}, Lcom/google/android/gms/common/data/DataHolder;->b(I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/util/List;)Lcom/google/android/gms/common/data/DataHolder;
    .locals 7

    .prologue
    .line 342
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/games/a/bt;->d:Lcom/google/android/gms/games/h/a/ew;

    invoke-static {p1}, Lcom/google/android/gms/games/a/l;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    new-instance v4, Lcom/google/android/gms/games/h/a/et;

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2, p4}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-direct {v4, v2}, Lcom/google/android/gms/games/h/a/et;-><init>(Ljava/util/ArrayList;)V

    const-string v2, "rooms/%1$s/reportstatus"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-static {p3}, Lcom/google/android/gms/games/h/a/ew;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v3, v5

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    if-eqz v1, :cond_0

    const-string v2, "language"

    invoke-static {v1}, Lcom/google/android/gms/games/h/a/ew;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v3, v2, v1}, Lcom/google/android/gms/games/h/a/ew;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    :cond_0
    iget-object v0, v0, Lcom/google/android/gms/games/h/a/ew;->a:Lcom/google/android/gms/common/server/n;

    const/4 v2, 0x1

    const-class v5, Lcom/google/android/gms/games/h/a/ev;

    move-object v1, p2

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/common/server/n;->a(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;Ljava/lang/Object;Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/h/a/ev;

    .line 345
    invoke-static {v0}, Lcom/google/android/gms/games/a/bf;->a(Lcom/google/android/gms/games/h/a/ev;)Lcom/google/android/gms/common/data/DataHolder;
    :try_end_0
    .catch Lcom/android/volley/ac; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 352
    :goto_0
    return-object v0

    .line 346
    :catch_0
    move-exception v0

    .line 349
    const-string v1, "RealTimeAgent"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Failed to report peer connections: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/android/volley/ac;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/gms/games/internal/dq;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 350
    const-string v1, "RealTimeAgent"

    const/16 v2, 0x1b58

    invoke-static {v1, v0, v2}, Lcom/google/android/gms/games/a/bf;->a(Ljava/lang/String;Lcom/android/volley/ac;I)I

    move-result v0

    .line 352
    invoke-static {v0}, Lcom/google/android/gms/common/data/DataHolder;->b(I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/games/a/au;ILjava/util/ArrayList;Landroid/os/Bundle;Lcom/google/android/gms/games/internal/ConnectionInfo;)Lcom/google/android/gms/common/data/DataHolder;
    .locals 14

    .prologue
    .line 182
    iget-object v10, p1, Lcom/google/android/gms/games/a/au;->a:Landroid/content/Context;

    .line 183
    if-nez p4, :cond_1

    const/4 v3, 0x0

    .line 186
    :goto_0
    :try_start_0
    invoke-virtual/range {p5 .. p5}, Lcom/google/android/gms/games/internal/ConnectionInfo;->b()Ljava/lang/String;

    move-result-object v2

    .line 187
    if-nez v2, :cond_3

    const/4 v5, 0x0

    .line 189
    :goto_1
    const/4 v2, -0x1

    move/from16 v0, p2

    if-ne v0, v2, :cond_4

    const/4 v9, 0x0

    .line 190
    :goto_2
    new-instance v2, Lcom/google/android/gms/games/h/a/en;

    invoke-static {}, Lcom/google/android/gms/games/internal/b/b;->a()Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual/range {p5 .. p5}, Lcom/google/android/gms/games/internal/ConnectionInfo;->c()I

    move-result v6

    invoke-static {v10, v6}, Lcom/google/android/gms/games/a/bt;->a(Landroid/content/Context;I)Lcom/google/android/gms/games/h/a/cm;

    move-result-object v7

    iget-object v6, p0, Lcom/google/android/gms/games/a/bt;->f:Ljava/util/Random;

    invoke-virtual {v6}, Ljava/util/Random;->nextLong()J

    move-result-wide v12

    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    move-object/from16 v6, p3

    invoke-direct/range {v2 .. v9}, Lcom/google/android/gms/games/h/a/en;-><init>(Lcom/google/android/gms/games/h/a/el;Ljava/util/ArrayList;Lcom/google/android/gms/games/h/a/em;Ljava/util/ArrayList;Lcom/google/android/gms/games/h/a/cm;Ljava/lang/Long;Ljava/lang/Integer;)V

    .line 194
    iget-object v3, p0, Lcom/google/android/gms/games/a/bt;->d:Lcom/google/android/gms/games/h/a/ew;

    iget-object v4, p1, Lcom/google/android/gms/games/a/au;->b:Lcom/google/android/gms/common/server/ClientContext;

    invoke-static {v10}, Lcom/google/android/gms/games/a/l;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v5

    const-string v6, "rooms/create"

    if-eqz v5, :cond_0

    const-string v7, "language"

    invoke-static {v5}, Lcom/google/android/gms/games/h/a/ew;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v6, v7, v5}, Lcom/google/android/gms/games/h/a/ew;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    :cond_0
    iget-object v3, v3, Lcom/google/android/gms/games/h/a/ew;->a:Lcom/google/android/gms/common/server/n;

    const/4 v5, 0x1

    const-class v8, Lcom/google/android/gms/games/h/a/ej;

    move-object v7, v2

    invoke-virtual/range {v3 .. v8}, Lcom/google/android/gms/common/server/n;->a(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;Ljava/lang/Object;Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse;

    move-result-object v2

    check-cast v2, Lcom/google/android/gms/games/h/a/ej;
    :try_end_0
    .catch Lcom/android/volley/ac; {:try_start_0 .. :try_end_0} :catch_0

    .line 206
    iget-object v3, p1, Lcom/google/android/gms/games/a/au;->b:Lcom/google/android/gms/common/server/ClientContext;

    iget-object v4, p1, Lcom/google/android/gms/games/a/au;->e:Ljava/lang/String;

    invoke-direct {p0, v10, v3, v2, v4}, Lcom/google/android/gms/games/a/bt;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Lcom/google/android/gms/games/h/a/ej;Ljava/lang/String;)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v2

    :goto_3
    return-object v2

    .line 183
    :cond_1
    const/4 v2, 0x0

    const-string v3, "exclusive_bit_mask"

    move-object/from16 v0, p4

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2

    const-string v2, "exclusive_bit_mask"

    move-object/from16 v0, p4

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    :cond_2
    new-instance v3, Lcom/google/android/gms/games/h/a/el;

    const-string v4, "max_automatch_players"

    move-object/from16 v0, p4

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    const-string v5, "min_automatch_players"

    move-object/from16 v0, p4

    invoke-virtual {v0, v5}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-direct {v3, v2, v4, v5}, Lcom/google/android/gms/games/h/a/el;-><init>(Ljava/lang/Long;Ljava/lang/Integer;Ljava/lang/Integer;)V

    goto/16 :goto_0

    .line 187
    :cond_3
    :try_start_1
    new-instance v5, Lcom/google/android/gms/games/h/a/em;

    invoke-direct {v5, v2}, Lcom/google/android/gms/games/h/a/em;-><init>(Ljava/lang/String;)V
    :try_end_1
    .catch Lcom/android/volley/ac; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_1

    .line 195
    :catch_0
    move-exception v2

    .line 198
    const-string v3, "RealTimeAgent"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Failed to create room: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Lcom/android/volley/ac;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/android/gms/games/internal/dq;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 199
    const-string v3, "RealTimeAgent"

    const/16 v4, 0x1b58

    invoke-static {v3, v2, v4}, Lcom/google/android/gms/games/a/bf;->a(Ljava/lang/String;Lcom/android/volley/ac;I)I

    move-result v2

    .line 201
    invoke-static {v2}, Lcom/google/android/gms/common/data/DataHolder;->b(I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v2

    goto :goto_3

    .line 189
    :cond_4
    :try_start_2
    invoke-static/range {p2 .. p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;
    :try_end_2
    .catch Lcom/android/volley/ac; {:try_start_2 .. :try_end_2} :catch_0

    move-result-object v9

    goto/16 :goto_2
.end method

.method public final a(Lcom/google/android/gms/games/a/au;Ljava/lang/String;Lcom/google/android/gms/games/internal/ConnectionInfo;)Lcom/google/android/gms/common/data/DataHolder;
    .locals 12

    .prologue
    const/16 v11, 0x1b58

    const/4 v7, 0x0

    .line 222
    iget-object v8, p1, Lcom/google/android/gms/games/a/au;->a:Landroid/content/Context;

    .line 223
    iget-object v1, p1, Lcom/google/android/gms/games/a/au;->b:Lcom/google/android/gms/common/server/ClientContext;

    .line 226
    const/4 v6, 0x0

    .line 228
    :try_start_0
    invoke-virtual {p3}, Lcom/google/android/gms/games/internal/ConnectionInfo;->b()Ljava/lang/String;

    move-result-object v0

    .line 229
    new-instance v4, Lcom/google/android/gms/games/h/a/eo;

    invoke-static {}, Lcom/google/android/gms/games/internal/b/b;->a()Ljava/util/ArrayList;

    move-result-object v2

    new-instance v3, Lcom/google/android/gms/games/h/a/em;

    invoke-direct {v3, v0}, Lcom/google/android/gms/games/h/a/em;-><init>(Ljava/lang/String;)V

    invoke-virtual {p3}, Lcom/google/android/gms/games/internal/ConnectionInfo;->c()I

    move-result v0

    invoke-static {v8, v0}, Lcom/google/android/gms/games/a/bt;->a(Landroid/content/Context;I)Lcom/google/android/gms/games/h/a/cm;

    move-result-object v0

    invoke-direct {v4, v2, v3, v0}, Lcom/google/android/gms/games/h/a/eo;-><init>(Ljava/util/ArrayList;Lcom/google/android/gms/games/h/a/em;Lcom/google/android/gms/games/h/a/cm;)V

    .line 232
    iget-object v0, p0, Lcom/google/android/gms/games/a/bt;->d:Lcom/google/android/gms/games/h/a/ew;

    invoke-static {v8}, Lcom/google/android/gms/games/a/l;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "rooms/%1$s/join"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v9, 0x0

    invoke-static {p2}, Lcom/google/android/gms/games/h/a/ew;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    aput-object v10, v5, v9

    invoke-static {v3, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    if-eqz v2, :cond_0

    const-string v5, "language"

    invoke-static {v2}, Lcom/google/android/gms/games/h/a/ew;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v5, v2}, Lcom/google/android/gms/games/h/a/ew;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    :cond_0
    iget-object v0, v0, Lcom/google/android/gms/games/h/a/ew;->a:Lcom/google/android/gms/common/server/n;

    const/4 v2, 0x1

    const-class v5, Lcom/google/android/gms/games/h/a/ej;

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/common/server/n;->a(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;Ljava/lang/Object;Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/h/a/ej;
    :try_end_0
    .catch Lcom/android/volley/ac; {:try_start_0 .. :try_end_0} :catch_0

    move v2, v7

    .line 242
    :goto_0
    if-ne v2, v11, :cond_1

    .line 243
    invoke-static {v2}, Lcom/google/android/gms/common/data/DataHolder;->b(I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    .line 254
    :goto_1
    return-object v0

    .line 234
    :catch_0
    move-exception v0

    .line 236
    const-string v2, "RealTimeAgent"

    const-string v3, "Failed to accept invitation"

    invoke-static {v2, v3}, Lcom/google/android/gms/games/internal/dq;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 237
    const-string v2, "RealTimeAgent"

    invoke-static {v2, v0, v11}, Lcom/google/android/gms/games/a/bf;->a(Ljava/lang/String;Lcom/android/volley/ac;I)I

    move-result v0

    move v2, v0

    move-object v0, v6

    goto :goto_0

    .line 248
    :cond_1
    invoke-static {v8, v1, p2}, Lcom/google/android/gms/games/a/bt;->b(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)V

    .line 251
    if-nez v0, :cond_2

    .line 252
    invoke-static {v2}, Lcom/google/android/gms/common/data/DataHolder;->b(I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    goto :goto_1

    .line 254
    :cond_2
    iget-object v1, p1, Lcom/google/android/gms/games/a/au;->b:Lcom/google/android/gms/common/server/ClientContext;

    iget-object v2, p1, Lcom/google/android/gms/games/a/au;->e:Ljava/lang/String;

    invoke-direct {p0, v8, v1, v0, v2}, Lcom/google/android/gms/games/a/bt;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Lcom/google/android/gms/games/h/a/ej;Ljava/lang/String;)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    goto :goto_1
.end method

.method public final b(Lcom/google/android/gms/games/a/au;Ljava/lang/String;)I
    .locals 7

    .prologue
    const/4 v0, 0x0

    .line 308
    iget-object v1, p1, Lcom/google/android/gms/games/a/au;->b:Lcom/google/android/gms/common/server/ClientContext;

    .line 311
    :try_start_0
    iget-boolean v2, p1, Lcom/google/android/gms/games/a/au;->h:Z

    if-eqz v2, :cond_0

    .line 312
    iget-object v2, p0, Lcom/google/android/gms/games/a/bt;->e:Lcom/google/android/gms/games/h/a/ex;

    const-string v3, "rooms/%1$s/dismiss"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-static {p2}, Lcom/google/android/gms/games/h/a/ex;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    iget-object v2, v2, Lcom/google/android/gms/games/h/a/ex;->a:Lcom/google/android/gms/common/server/n;

    const/4 v4, 0x1

    const/4 v5, 0x0

    invoke-virtual {v2, v1, v4, v3, v5}, Lcom/google/android/gms/common/server/n;->a(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;Ljava/lang/Object;)V
    :try_end_0
    .catch Lcom/android/volley/ac; {:try_start_0 .. :try_end_0} :catch_0

    .line 324
    :goto_0
    iget-object v2, p1, Lcom/google/android/gms/games/a/au;->a:Landroid/content/Context;

    invoke-static {v2, v1, p2}, Lcom/google/android/gms/games/a/bt;->b(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)V

    .line 326
    return v0

    .line 314
    :cond_0
    :try_start_1
    iget-object v2, p0, Lcom/google/android/gms/games/a/bt;->d:Lcom/google/android/gms/games/h/a/ew;

    const-string v3, "rooms/%1$s/dismiss"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-static {p2}, Lcom/google/android/gms/games/h/a/ew;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    iget-object v2, v2, Lcom/google/android/gms/games/h/a/ew;->a:Lcom/google/android/gms/common/server/n;

    const/4 v4, 0x1

    const/4 v5, 0x0

    invoke-virtual {v2, v1, v4, v3, v5}, Lcom/google/android/gms/common/server/n;->a(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;Ljava/lang/Object;)V
    :try_end_1
    .catch Lcom/android/volley/ac; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 316
    :catch_0
    move-exception v0

    .line 317
    const-string v2, "RealTimeAgent"

    const-string v3, "Failed to dismiss invitation"

    invoke-static {v2, v3}, Lcom/google/android/gms/games/internal/dq;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 318
    const-string v2, "RealTimeAgent"

    const/4 v3, 0x5

    invoke-static {v2, v0, v3}, Lcom/google/android/gms/games/a/bf;->a(Ljava/lang/String;Lcom/android/volley/ac;I)I

    move-result v0

    goto :goto_0
.end method
