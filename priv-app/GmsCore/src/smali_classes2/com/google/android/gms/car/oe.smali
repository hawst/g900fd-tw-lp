.class public final Lcom/google/android/gms/car/oe;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final d:Ljava/lang/Object;

.field private static e:I

.field private static f:Lcom/google/android/gms/car/oe;


# instance fields
.field a:I

.field b:I

.field c:I

.field private g:Lcom/google/android/gms/car/oe;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 162
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/google/android/gms/car/oe;->d:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 160
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static a()Lcom/google/android/gms/car/oe;
    .locals 3

    .prologue
    .line 175
    sget-object v1, Lcom/google/android/gms/car/oe;->d:Ljava/lang/Object;

    monitor-enter v1

    .line 176
    :try_start_0
    sget-object v0, Lcom/google/android/gms/car/oe;->f:Lcom/google/android/gms/car/oe;

    .line 177
    if-nez v0, :cond_0

    .line 178
    new-instance v0, Lcom/google/android/gms/car/oe;

    invoke-direct {v0}, Lcom/google/android/gms/car/oe;-><init>()V

    monitor-exit v1

    .line 184
    :goto_0
    return-object v0

    .line 180
    :cond_0
    iget-object v2, v0, Lcom/google/android/gms/car/oe;->g:Lcom/google/android/gms/car/oe;

    sput-object v2, Lcom/google/android/gms/car/oe;->f:Lcom/google/android/gms/car/oe;

    .line 181
    sget v2, Lcom/google/android/gms/car/oe;->e:I

    add-int/lit8 v2, v2, -0x1

    sput v2, Lcom/google/android/gms/car/oe;->e:I

    .line 182
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 183
    const/4 v1, 0x0

    iput-object v1, v0, Lcom/google/android/gms/car/oe;->g:Lcom/google/android/gms/car/oe;

    goto :goto_0

    .line 182
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method final b()V
    .locals 3

    .prologue
    .line 187
    sget-object v1, Lcom/google/android/gms/car/oe;->d:Ljava/lang/Object;

    monitor-enter v1

    .line 188
    :try_start_0
    sget v0, Lcom/google/android/gms/car/oe;->e:I

    const/16 v2, 0xa

    if-ge v0, v2, :cond_0

    .line 189
    sget v0, Lcom/google/android/gms/car/oe;->e:I

    add-int/lit8 v0, v0, 0x1

    sput v0, Lcom/google/android/gms/car/oe;->e:I

    .line 190
    sget-object v0, Lcom/google/android/gms/car/oe;->f:Lcom/google/android/gms/car/oe;

    iput-object v0, p0, Lcom/google/android/gms/car/oe;->g:Lcom/google/android/gms/car/oe;

    .line 191
    sput-object p0, Lcom/google/android/gms/car/oe;->f:Lcom/google/android/gms/car/oe;

    .line 193
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method
