.class public Lcom/google/android/gms/maps/q;
.super Landroid/support/v4/app/Fragment;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/android/gms/maps/t;

.field private b:Lcom/google/android/gms/maps/c;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 331
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    .line 294
    new-instance v0, Lcom/google/android/gms/maps/t;

    invoke-direct {v0, p0}, Lcom/google/android/gms/maps/t;-><init>(Landroid/support/v4/app/Fragment;)V

    iput-object v0, p0, Lcom/google/android/gms/maps/q;->a:Lcom/google/android/gms/maps/t;

    .line 332
    return-void
.end method


# virtual methods
.method public final c()Lcom/google/android/gms/maps/c;
    .locals 3
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 362
    iget-object v0, p0, Lcom/google/android/gms/maps/q;->a:Lcom/google/android/gms/maps/t;

    invoke-virtual {v0}, Lcom/google/android/gms/maps/t;->b()V

    iget-object v0, p0, Lcom/google/android/gms/maps/q;->a:Lcom/google/android/gms/maps/t;

    iget-object v0, v0, Lcom/google/android/gms/b/a;->a:Lcom/google/android/gms/b/o;

    if-nez v0, :cond_0

    move-object v0, v1

    .line 363
    :goto_0
    if-nez v0, :cond_1

    move-object v0, v1

    .line 384
    :goto_1
    return-object v0

    .line 362
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/maps/q;->a:Lcom/google/android/gms/maps/t;

    iget-object v0, v0, Lcom/google/android/gms/b/a;->a:Lcom/google/android/gms/b/o;

    check-cast v0, Lcom/google/android/gms/maps/r;

    iget-object v0, v0, Lcom/google/android/gms/maps/r;->a:Lcom/google/android/gms/maps/internal/w;

    goto :goto_0

    .line 369
    :cond_1
    :try_start_0
    invoke-interface {v0}, Lcom/google/android/gms/maps/internal/w;->a()Lcom/google/android/gms/maps/internal/k;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 374
    if-nez v0, :cond_2

    move-object v0, v1

    .line 375
    goto :goto_1

    .line 370
    :catch_0
    move-exception v0

    .line 371
    new-instance v1, Lcom/google/android/gms/maps/model/n;

    invoke-direct {v1, v0}, Lcom/google/android/gms/maps/model/n;-><init>(Landroid/os/RemoteException;)V

    throw v1

    .line 380
    :cond_2
    iget-object v1, p0, Lcom/google/android/gms/maps/q;->b:Lcom/google/android/gms/maps/c;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/google/android/gms/maps/q;->b:Lcom/google/android/gms/maps/c;

    iget-object v1, v1, Lcom/google/android/gms/maps/c;->a:Lcom/google/android/gms/maps/internal/k;

    invoke-interface {v1}, Lcom/google/android/gms/maps/internal/k;->asBinder()Landroid/os/IBinder;

    move-result-object v1

    invoke-interface {v0}, Lcom/google/android/gms/maps/internal/k;->asBinder()Landroid/os/IBinder;

    move-result-object v2

    if-eq v1, v2, :cond_4

    .line 381
    :cond_3
    new-instance v1, Lcom/google/android/gms/maps/c;

    invoke-direct {v1, v0}, Lcom/google/android/gms/maps/c;-><init>(Lcom/google/android/gms/maps/internal/k;)V

    iput-object v1, p0, Lcom/google/android/gms/maps/q;->b:Lcom/google/android/gms/maps/c;

    .line 384
    :cond_4
    iget-object v0, p0, Lcom/google/android/gms/maps/q;->b:Lcom/google/android/gms/maps/c;

    goto :goto_1
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 458
    if-eqz p1, :cond_0

    .line 461
    const-class v0, Lcom/google/android/gms/maps/q;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->setClassLoader(Ljava/lang/ClassLoader;)V

    .line 463
    :cond_0
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 464
    return-void
.end method

.method public onAttach(Landroid/app/Activity;)V
    .locals 1

    .prologue
    .line 393
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onAttach(Landroid/app/Activity;)V

    .line 394
    iget-object v0, p0, Lcom/google/android/gms/maps/q;->a:Lcom/google/android/gms/maps/t;

    invoke-static {v0, p1}, Lcom/google/android/gms/maps/t;->a(Lcom/google/android/gms/maps/t;Landroid/app/Activity;)V

    .line 395
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 416
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    .line 417
    iget-object v0, p0, Lcom/google/android/gms/maps/q;->a:Lcom/google/android/gms/maps/t;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/maps/t;->a(Landroid/os/Bundle;)V

    .line 418
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 1

    .prologue
    .line 423
    iget-object v0, p0, Lcom/google/android/gms/maps/q;->a:Lcom/google/android/gms/maps/t;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/gms/maps/t;->a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 446
    iget-object v0, p0, Lcom/google/android/gms/maps/q;->a:Lcom/google/android/gms/maps/t;

    iget-object v1, v0, Lcom/google/android/gms/b/a;->a:Lcom/google/android/gms/b/o;

    if-eqz v1, :cond_0

    iget-object v0, v0, Lcom/google/android/gms/b/a;->a:Lcom/google/android/gms/b/o;

    invoke-interface {v0}, Lcom/google/android/gms/b/o;->d()V

    .line 447
    :goto_0
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onDestroy()V

    .line 448
    return-void

    .line 446
    :cond_0
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/b/a;->a(I)V

    goto :goto_0
.end method

.method public onDestroyView()V
    .locals 2

    .prologue
    .line 440
    iget-object v0, p0, Lcom/google/android/gms/maps/q;->a:Lcom/google/android/gms/maps/t;

    iget-object v1, v0, Lcom/google/android/gms/b/a;->a:Lcom/google/android/gms/b/o;

    if-eqz v1, :cond_0

    iget-object v0, v0, Lcom/google/android/gms/b/a;->a:Lcom/google/android/gms/b/o;

    invoke-interface {v0}, Lcom/google/android/gms/b/o;->c()V

    .line 441
    :goto_0
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onDestroyView()V

    .line 442
    return-void

    .line 440
    :cond_0
    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/google/android/gms/b/a;->a(I)V

    goto :goto_0
.end method

.method public onInflate(Landroid/app/Activity;Landroid/util/AttributeSet;Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 404
    invoke-super {p0, p1, p2, p3}, Landroid/support/v4/app/Fragment;->onInflate(Landroid/app/Activity;Landroid/util/AttributeSet;Landroid/os/Bundle;)V

    .line 405
    iget-object v0, p0, Lcom/google/android/gms/maps/q;->a:Lcom/google/android/gms/maps/t;

    invoke-static {v0, p1}, Lcom/google/android/gms/maps/t;->a(Lcom/google/android/gms/maps/t;Landroid/app/Activity;)V

    .line 408
    invoke-static {p1, p2}, Lcom/google/android/gms/maps/GoogleMapOptions;->a(Landroid/content/Context;Landroid/util/AttributeSet;)Lcom/google/android/gms/maps/GoogleMapOptions;

    move-result-object v0

    .line 409
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 410
    const-string v2, "MapOptions"

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 411
    iget-object v0, p0, Lcom/google/android/gms/maps/q;->a:Lcom/google/android/gms/maps/t;

    invoke-virtual {v0, p1, v1, p3}, Lcom/google/android/gms/maps/t;->a(Landroid/app/Activity;Landroid/os/Bundle;Landroid/os/Bundle;)V

    .line 412
    return-void
.end method

.method public onLowMemory()V
    .locals 2

    .prologue
    .line 452
    iget-object v0, p0, Lcom/google/android/gms/maps/q;->a:Lcom/google/android/gms/maps/t;

    iget-object v1, v0, Lcom/google/android/gms/b/a;->a:Lcom/google/android/gms/b/o;

    if-eqz v1, :cond_0

    iget-object v0, v0, Lcom/google/android/gms/b/a;->a:Lcom/google/android/gms/b/o;

    invoke-interface {v0}, Lcom/google/android/gms/b/o;->e()V

    .line 453
    :cond_0
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onLowMemory()V

    .line 454
    return-void
.end method

.method public onPause()V
    .locals 2

    .prologue
    .line 434
    iget-object v0, p0, Lcom/google/android/gms/maps/q;->a:Lcom/google/android/gms/maps/t;

    iget-object v1, v0, Lcom/google/android/gms/b/a;->a:Lcom/google/android/gms/b/o;

    if-eqz v1, :cond_0

    iget-object v0, v0, Lcom/google/android/gms/b/a;->a:Lcom/google/android/gms/b/o;

    invoke-interface {v0}, Lcom/google/android/gms/b/o;->b()V

    .line 435
    :goto_0
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onPause()V

    .line 436
    return-void

    .line 434
    :cond_0
    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Lcom/google/android/gms/b/a;->a(I)V

    goto :goto_0
.end method

.method public onResume()V
    .locals 1

    .prologue
    .line 428
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onResume()V

    .line 429
    iget-object v0, p0, Lcom/google/android/gms/maps/q;->a:Lcom/google/android/gms/maps/t;

    invoke-virtual {v0}, Lcom/google/android/gms/maps/t;->a()V

    .line 430
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 470
    if-eqz p1, :cond_0

    .line 471
    const-class v0, Lcom/google/android/gms/maps/q;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->setClassLoader(Ljava/lang/ClassLoader;)V

    .line 474
    :cond_0
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 475
    iget-object v0, p0, Lcom/google/android/gms/maps/q;->a:Lcom/google/android/gms/maps/t;

    iget-object v1, v0, Lcom/google/android/gms/b/a;->a:Lcom/google/android/gms/b/o;

    if-eqz v1, :cond_2

    iget-object v0, v0, Lcom/google/android/gms/b/a;->a:Lcom/google/android/gms/b/o;

    invoke-interface {v0, p1}, Lcom/google/android/gms/b/o;->b(Landroid/os/Bundle;)V

    .line 476
    :cond_1
    :goto_0
    return-void

    .line 475
    :cond_2
    iget-object v1, v0, Lcom/google/android/gms/b/a;->b:Landroid/os/Bundle;

    if-eqz v1, :cond_1

    iget-object v0, v0, Lcom/google/android/gms/b/a;->b:Landroid/os/Bundle;

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->putAll(Landroid/os/Bundle;)V

    goto :goto_0
.end method

.method public setArguments(Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 502
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 503
    return-void
.end method
