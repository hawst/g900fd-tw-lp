.class public final Lcom/google/android/gms/icing/x;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# instance fields
.field public a:I

.field public b:Ljava/lang/String;

.field public c:I

.field public d:[Ljava/lang/String;

.field public e:[Lcom/google/android/gms/icing/y;

.field public f:Z


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, -0x1

    .line 8057
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 8058
    iput v2, p0, Lcom/google/android/gms/icing/x;->a:I

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/gms/icing/x;->b:Ljava/lang/String;

    iput v1, p0, Lcom/google/android/gms/icing/x;->c:I

    sget-object v0, Lcom/google/protobuf/nano/m;->f:[Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/gms/icing/x;->d:[Ljava/lang/String;

    invoke-static {}, Lcom/google/android/gms/icing/y;->a()[Lcom/google/android/gms/icing/y;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/icing/x;->e:[Lcom/google/android/gms/icing/y;

    iput-boolean v2, p0, Lcom/google/android/gms/icing/x;->f:Z

    iput v1, p0, Lcom/google/android/gms/icing/x;->cachedSize:I

    .line 8059
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 8159
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->computeSerializedSize()I

    move-result v0

    .line 8160
    iget v2, p0, Lcom/google/android/gms/icing/x;->a:I

    if-eqz v2, :cond_0

    .line 8161
    const/4 v2, 0x1

    iget v3, p0, Lcom/google/android/gms/icing/x;->a:I

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 8164
    :cond_0
    iget-object v2, p0, Lcom/google/android/gms/icing/x;->b:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 8165
    const/4 v2, 0x2

    iget-object v3, p0, Lcom/google/android/gms/icing/x;->b:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 8168
    :cond_1
    iget-object v2, p0, Lcom/google/android/gms/icing/x;->e:[Lcom/google/android/gms/icing/y;

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/google/android/gms/icing/x;->e:[Lcom/google/android/gms/icing/y;

    array-length v2, v2

    if-lez v2, :cond_4

    move v2, v0

    move v0, v1

    .line 8169
    :goto_0
    iget-object v3, p0, Lcom/google/android/gms/icing/x;->e:[Lcom/google/android/gms/icing/y;

    array-length v3, v3

    if-ge v0, v3, :cond_3

    .line 8170
    iget-object v3, p0, Lcom/google/android/gms/icing/x;->e:[Lcom/google/android/gms/icing/y;

    aget-object v3, v3, v0

    .line 8171
    if-eqz v3, :cond_2

    .line 8172
    const/4 v4, 0x3

    invoke-static {v4, v3}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v3

    add-int/2addr v2, v3

    .line 8169
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_3
    move v0, v2

    .line 8177
    :cond_4
    iget-boolean v2, p0, Lcom/google/android/gms/icing/x;->f:Z

    if-eqz v2, :cond_5

    .line 8178
    const/4 v2, 0x4

    iget-boolean v3, p0, Lcom/google/android/gms/icing/x;->f:Z

    invoke-static {v2}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 8181
    :cond_5
    iget v2, p0, Lcom/google/android/gms/icing/x;->c:I

    const/4 v3, -0x1

    if-eq v2, v3, :cond_6

    .line 8182
    const/4 v2, 0x5

    iget v3, p0, Lcom/google/android/gms/icing/x;->c:I

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 8185
    :cond_6
    iget-object v2, p0, Lcom/google/android/gms/icing/x;->d:[Ljava/lang/String;

    if-eqz v2, :cond_9

    iget-object v2, p0, Lcom/google/android/gms/icing/x;->d:[Ljava/lang/String;

    array-length v2, v2

    if-lez v2, :cond_9

    move v2, v1

    move v3, v1

    .line 8188
    :goto_1
    iget-object v4, p0, Lcom/google/android/gms/icing/x;->d:[Ljava/lang/String;

    array-length v4, v4

    if-ge v1, v4, :cond_8

    .line 8189
    iget-object v4, p0, Lcom/google/android/gms/icing/x;->d:[Ljava/lang/String;

    aget-object v4, v4, v1

    .line 8190
    if-eqz v4, :cond_7

    .line 8191
    add-int/lit8 v3, v3, 0x1

    .line 8192
    invoke-static {v4}, Lcom/google/protobuf/nano/b;->a(Ljava/lang/String;)I

    move-result v4

    add-int/2addr v2, v4

    .line 8188
    :cond_7
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 8196
    :cond_8
    add-int/2addr v0, v2

    .line 8197
    mul-int/lit8 v1, v3, 0x1

    add-int/2addr v0, v1

    .line 8199
    :cond_9
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 8074
    if-ne p1, p0, :cond_1

    .line 8105
    :cond_0
    :goto_0
    return v0

    .line 8077
    :cond_1
    instance-of v2, p1, Lcom/google/android/gms/icing/x;

    if-nez v2, :cond_2

    move v0, v1

    .line 8078
    goto :goto_0

    .line 8080
    :cond_2
    check-cast p1, Lcom/google/android/gms/icing/x;

    .line 8081
    iget v2, p0, Lcom/google/android/gms/icing/x;->a:I

    iget v3, p1, Lcom/google/android/gms/icing/x;->a:I

    if-eq v2, v3, :cond_3

    move v0, v1

    .line 8082
    goto :goto_0

    .line 8084
    :cond_3
    iget-object v2, p0, Lcom/google/android/gms/icing/x;->b:Ljava/lang/String;

    if-nez v2, :cond_4

    .line 8085
    iget-object v2, p1, Lcom/google/android/gms/icing/x;->b:Ljava/lang/String;

    if-eqz v2, :cond_5

    move v0, v1

    .line 8086
    goto :goto_0

    .line 8088
    :cond_4
    iget-object v2, p0, Lcom/google/android/gms/icing/x;->b:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/android/gms/icing/x;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_5

    move v0, v1

    .line 8089
    goto :goto_0

    .line 8091
    :cond_5
    iget v2, p0, Lcom/google/android/gms/icing/x;->c:I

    iget v3, p1, Lcom/google/android/gms/icing/x;->c:I

    if-eq v2, v3, :cond_6

    move v0, v1

    .line 8092
    goto :goto_0

    .line 8094
    :cond_6
    iget-object v2, p0, Lcom/google/android/gms/icing/x;->d:[Ljava/lang/String;

    iget-object v3, p1, Lcom/google/android/gms/icing/x;->d:[Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_7

    move v0, v1

    .line 8096
    goto :goto_0

    .line 8098
    :cond_7
    iget-object v2, p0, Lcom/google/android/gms/icing/x;->e:[Lcom/google/android/gms/icing/y;

    iget-object v3, p1, Lcom/google/android/gms/icing/x;->e:[Lcom/google/android/gms/icing/y;

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_8

    move v0, v1

    .line 8100
    goto :goto_0

    .line 8102
    :cond_8
    iget-boolean v2, p0, Lcom/google/android/gms/icing/x;->f:Z

    iget-boolean v3, p1, Lcom/google/android/gms/icing/x;->f:Z

    if-eq v2, v3, :cond_0

    move v0, v1

    .line 8103
    goto :goto_0
.end method

.method public final hashCode()I
    .locals 2

    .prologue
    .line 8110
    iget v0, p0, Lcom/google/android/gms/icing/x;->a:I

    add-int/lit16 v0, v0, 0x20f

    .line 8112
    mul-int/lit8 v1, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/gms/icing/x;->b:Ljava/lang/String;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    add-int/2addr v0, v1

    .line 8114
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/google/android/gms/icing/x;->c:I

    add-int/2addr v0, v1

    .line 8115
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/google/android/gms/icing/x;->d:[Ljava/lang/String;

    invoke-static {v1}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 8117
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/google/android/gms/icing/x;->e:[Lcom/google/android/gms/icing/y;

    invoke-static {v1}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 8119
    mul-int/lit8 v1, v0, 0x1f

    iget-boolean v0, p0, Lcom/google/android/gms/icing/x;->f:Z

    if-eqz v0, :cond_1

    const/16 v0, 0x4cf

    :goto_1
    add-int/2addr v0, v1

    .line 8120
    return v0

    .line 8112
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/icing/x;->b:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_0

    .line 8119
    :cond_1
    const/16 v0, 0x4d5

    goto :goto_1
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 7867
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/icing/x;->a:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/icing/x;->b:Ljava/lang/String;

    goto :goto_0

    :sswitch_3
    const/16 v0, 0x1a

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->b(Lcom/google/protobuf/nano/a;I)I

    move-result v2

    iget-object v0, p0, Lcom/google/android/gms/icing/x;->e:[Lcom/google/android/gms/icing/y;

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lcom/google/android/gms/icing/y;

    if-eqz v0, :cond_1

    iget-object v3, p0, Lcom/google/android/gms/icing/x;->e:[Lcom/google/android/gms/icing/y;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_1
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_3

    new-instance v3, Lcom/google/android/gms/icing/y;

    invoke-direct {v3}, Lcom/google/android/gms/icing/y;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/icing/x;->e:[Lcom/google/android/gms/icing/y;

    array-length v0, v0

    goto :goto_1

    :cond_3
    new-instance v3, Lcom/google/android/gms/icing/y;

    invoke-direct {v3}, Lcom/google/android/gms/icing/y;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    iput-object v2, p0, Lcom/google/android/gms/icing/x;->e:[Lcom/google/android/gms/icing/y;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/icing/x;->f:Z

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/icing/x;->c:I

    goto :goto_0

    :sswitch_6
    const/16 v0, 0x32

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->b(Lcom/google/protobuf/nano/a;I)I

    move-result v2

    iget-object v0, p0, Lcom/google/android/gms/icing/x;->d:[Ljava/lang/String;

    if-nez v0, :cond_5

    move v0, v1

    :goto_3
    add-int/2addr v2, v0

    new-array v2, v2, [Ljava/lang/String;

    if-eqz v0, :cond_4

    iget-object v3, p0, Lcom/google/android/gms/icing/x;->d:[Ljava/lang/String;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_4
    :goto_4
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_6

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_5
    iget-object v0, p0, Lcom/google/android/gms/icing/x;->d:[Ljava/lang/String;

    array-length v0, v0

    goto :goto_3

    :cond_6
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    iput-object v2, p0, Lcom/google/android/gms/icing/x;->d:[Ljava/lang/String;

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
        0x32 -> :sswitch_6
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 8126
    iget v0, p0, Lcom/google/android/gms/icing/x;->a:I

    if-eqz v0, :cond_0

    .line 8127
    const/4 v0, 0x1

    iget v2, p0, Lcom/google/android/gms/icing/x;->a:I

    invoke-virtual {p1, v0, v2}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 8129
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/icing/x;->b:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 8130
    const/4 v0, 0x2

    iget-object v2, p0, Lcom/google/android/gms/icing/x;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v2}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 8132
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/icing/x;->e:[Lcom/google/android/gms/icing/y;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/gms/icing/x;->e:[Lcom/google/android/gms/icing/y;

    array-length v0, v0

    if-lez v0, :cond_3

    move v0, v1

    .line 8133
    :goto_0
    iget-object v2, p0, Lcom/google/android/gms/icing/x;->e:[Lcom/google/android/gms/icing/y;

    array-length v2, v2

    if-ge v0, v2, :cond_3

    .line 8134
    iget-object v2, p0, Lcom/google/android/gms/icing/x;->e:[Lcom/google/android/gms/icing/y;

    aget-object v2, v2, v0

    .line 8135
    if-eqz v2, :cond_2

    .line 8136
    const/4 v3, 0x3

    invoke-virtual {p1, v3, v2}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 8133
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 8140
    :cond_3
    iget-boolean v0, p0, Lcom/google/android/gms/icing/x;->f:Z

    if-eqz v0, :cond_4

    .line 8141
    const/4 v0, 0x4

    iget-boolean v2, p0, Lcom/google/android/gms/icing/x;->f:Z

    invoke-virtual {p1, v0, v2}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 8143
    :cond_4
    iget v0, p0, Lcom/google/android/gms/icing/x;->c:I

    const/4 v2, -0x1

    if-eq v0, v2, :cond_5

    .line 8144
    const/4 v0, 0x5

    iget v2, p0, Lcom/google/android/gms/icing/x;->c:I

    invoke-virtual {p1, v0, v2}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 8146
    :cond_5
    iget-object v0, p0, Lcom/google/android/gms/icing/x;->d:[Ljava/lang/String;

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/google/android/gms/icing/x;->d:[Ljava/lang/String;

    array-length v0, v0

    if-lez v0, :cond_7

    .line 8147
    :goto_1
    iget-object v0, p0, Lcom/google/android/gms/icing/x;->d:[Ljava/lang/String;

    array-length v0, v0

    if-ge v1, v0, :cond_7

    .line 8148
    iget-object v0, p0, Lcom/google/android/gms/icing/x;->d:[Ljava/lang/String;

    aget-object v0, v0, v1

    .line 8149
    if-eqz v0, :cond_6

    .line 8150
    const/4 v2, 0x6

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 8147
    :cond_6
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 8154
    :cond_7
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 8155
    return-void
.end method
