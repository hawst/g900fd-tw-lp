.class final Lcom/google/android/gms/car/iq;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lcom/google/android/gms/car/ip;

.field private b:Z


# direct methods
.method constructor <init>(Lcom/google/android/gms/car/ip;)V
    .locals 1

    .prologue
    .line 87
    iput-object p1, p0, Lcom/google/android/gms/car/iq;->a:Lcom/google/android/gms/car/ip;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 89
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/car/iq;->b:Z

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 92
    const/4 v0, -0x8

    invoke-static {v0}, Landroid/os/Process;->setThreadPriority(I)V

    .line 96
    :cond_0
    :goto_0
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/car/iq;->a:Lcom/google/android/gms/car/ip;

    invoke-static {v0}, Lcom/google/android/gms/car/ip;->a(Lcom/google/android/gms/car/ip;)Ljava/util/concurrent/LinkedBlockingQueue;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/LinkedBlockingQueue;->take()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/Pair;
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    move-object v2, v0

    .line 102
    :goto_1
    iget-object v0, p0, Lcom/google/android/gms/car/iq;->a:Lcom/google/android/gms/car/ip;

    invoke-static {v0}, Lcom/google/android/gms/car/ip;->a(Lcom/google/android/gms/car/ip;)Ljava/util/concurrent/LinkedBlockingQueue;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/LinkedBlockingQueue;->poll()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/Pair;

    .line 103
    iget-object v1, v2, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-eq v1, v5, :cond_1

    if-eqz v0, :cond_1

    move-object v2, v0

    .line 105
    goto :goto_1

    .line 98
    :catch_0
    move-exception v0

    new-instance v0, Landroid/util/Pair;

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    move-object v2, v0

    goto :goto_1

    .line 107
    :cond_1
    iget-object v0, v2, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 109
    :pswitch_0
    iget-boolean v0, p0, Lcom/google/android/gms/car/iq;->b:Z

    if-nez v0, :cond_0

    .line 110
    iget-object v1, p0, Lcom/google/android/gms/car/iq;->a:Lcom/google/android/gms/car/ip;

    iget-object v0, v2, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, [I

    invoke-static {v1, v0}, Lcom/google/android/gms/car/ip;->a(Lcom/google/android/gms/car/ip;[I)V

    .line 111
    iput-boolean v4, p0, Lcom/google/android/gms/car/iq;->b:Z

    goto :goto_0

    .line 115
    :pswitch_1
    iget-boolean v0, p0, Lcom/google/android/gms/car/iq;->b:Z

    if-eqz v0, :cond_0

    .line 116
    iget-object v0, p0, Lcom/google/android/gms/car/iq;->a:Lcom/google/android/gms/car/ip;

    invoke-static {v0, v4}, Lcom/google/android/gms/car/ip;->a(Lcom/google/android/gms/car/ip;Z)V

    .line 117
    iput-boolean v3, p0, Lcom/google/android/gms/car/iq;->b:Z

    goto :goto_0

    .line 121
    :pswitch_2
    iget-object v0, p0, Lcom/google/android/gms/car/iq;->a:Lcom/google/android/gms/car/ip;

    invoke-static {v0}, Lcom/google/android/gms/car/ip;->a(Lcom/google/android/gms/car/ip;)Ljava/util/concurrent/LinkedBlockingQueue;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/LinkedBlockingQueue;->clear()V

    .line 122
    iget-boolean v0, p0, Lcom/google/android/gms/car/iq;->b:Z

    if-eqz v0, :cond_2

    .line 123
    iget-object v0, p0, Lcom/google/android/gms/car/iq;->a:Lcom/google/android/gms/car/ip;

    invoke-static {v0, v3}, Lcom/google/android/gms/car/ip;->a(Lcom/google/android/gms/car/ip;Z)V

    .line 124
    iput-boolean v3, p0, Lcom/google/android/gms/car/iq;->b:Z

    .line 126
    :cond_2
    return-void

    .line 107
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
