.class public Lcom/google/android/gms/common/download/DownloadServiceSettingsActivity;
.super Landroid/support/v7/app/d;
.source "SourceFile"


# static fields
.field private static final a:Lcom/google/android/gms/common/download/f;

.field private static final b:[Ljava/lang/String;


# instance fields
.field private final c:Ljava/lang/Runnable;

.field private d:Lcom/google/android/gms/common/api/v;

.field private e:Landroid/os/Handler;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    .line 48
    new-instance v1, Lcom/google/android/gms/common/download/g;

    const-string v2, "__cats.jpg"

    const-string v3, "https://i.imgur.com/oNmqoJU.jpg"

    const-wide/32 v4, 0x11892

    const-string v6, "54cb88b9af7c5f2bd967165fe0c4c9b93a1089eb"

    invoke-direct/range {v1 .. v6}, Lcom/google/android/gms/common/download/g;-><init>(Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;)V

    const-string v0, "cats"

    iput-object v0, v1, Lcom/google/android/gms/common/download/g;->e:Ljava/lang/String;

    invoke-virtual {v1}, Lcom/google/android/gms/common/download/g;->a()Lcom/google/android/gms/common/download/f;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/common/download/DownloadServiceSettingsActivity;->a:Lcom/google/android/gms/common/download/f;

    .line 56
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "_id"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "text1"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "text2"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/gms/common/download/DownloadServiceSettingsActivity;->b:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 44
    invoke-direct {p0}, Landroid/support/v7/app/d;-><init>()V

    .line 58
    new-instance v0, Lcom/google/android/gms/common/download/j;

    invoke-direct {v0, p0}, Lcom/google/android/gms/common/download/j;-><init>(Lcom/google/android/gms/common/download/DownloadServiceSettingsActivity;)V

    iput-object v0, p0, Lcom/google/android/gms/common/download/DownloadServiceSettingsActivity;->c:Ljava/lang/Runnable;

    .line 267
    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/common/download/DownloadServiceSettingsActivity;)Lcom/google/android/gms/common/api/v;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lcom/google/android/gms/common/download/DownloadServiceSettingsActivity;->d:Lcom/google/android/gms/common/api/v;

    return-object v0
.end method

.method static synthetic a(I)Ljava/lang/String;
    .locals 1

    .prologue
    .line 44
    sparse-switch p0, :sswitch_data_0

    const-string v0, "Unknown"

    :goto_0
    return-object v0

    :sswitch_0
    const-string v0, "Failed"

    goto :goto_0

    :sswitch_1
    const-string v0, "Paused"

    goto :goto_0

    :sswitch_2
    const-string v0, "Pending"

    goto :goto_0

    :sswitch_3
    const-string v0, "Running"

    goto :goto_0

    :sswitch_4
    const-string v0, "Successful"

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_2
        0x2 -> :sswitch_3
        0x4 -> :sswitch_1
        0x8 -> :sswitch_4
        0x10 -> :sswitch_0
    .end sparse-switch
.end method

.method static synthetic b(Lcom/google/android/gms/common/download/DownloadServiceSettingsActivity;)Ljava/lang/Runnable;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lcom/google/android/gms/common/download/DownloadServiceSettingsActivity;->c:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic c(Lcom/google/android/gms/common/download/DownloadServiceSettingsActivity;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lcom/google/android/gms/common/download/DownloadServiceSettingsActivity;->e:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic e()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 44
    sget-object v0, Lcom/google/android/gms/common/download/DownloadServiceSettingsActivity;->b:[Ljava/lang/String;

    return-object v0
.end method

.method static synthetic f()Lcom/google/android/gms/common/download/f;
    .locals 1

    .prologue
    .line 44
    sget-object v0, Lcom/google/android/gms/common/download/DownloadServiceSettingsActivity;->a:Lcom/google/android/gms/common/download/f;

    return-object v0
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 152
    invoke-super {p0, p1}, Landroid/support/v7/app/d;->onCreate(Landroid/os/Bundle;)V

    .line 154
    new-instance v0, Landroid/widget/ListView;

    invoke-direct {v0, p0}, Landroid/widget/ListView;-><init>(Landroid/content/Context;)V

    .line 155
    const v1, 0x102000a

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setId(I)V

    .line 156
    invoke-virtual {p0, v0}, Lcom/google/android/gms/common/download/DownloadServiceSettingsActivity;->setContentView(Landroid/view/View;)V

    .line 158
    const-string v0, "Download Service debug"

    invoke-virtual {p0, v0}, Lcom/google/android/gms/common/download/DownloadServiceSettingsActivity;->setTitle(Ljava/lang/CharSequence;)V

    .line 160
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/common/download/DownloadServiceSettingsActivity;->e:Landroid/os/Handler;

    .line 162
    new-instance v0, Lcom/google/android/gms/common/api/w;

    invoke-direct {v0, p0}, Lcom/google/android/gms/common/api/w;-><init>(Landroid/content/Context;)V

    sget-object v1, Lcom/google/android/gms/common/download/a;->c:Lcom/google/android/gms/common/api/c;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/api/w;->a(Lcom/google/android/gms/common/api/c;)Lcom/google/android/gms/common/api/w;

    move-result-object v0

    new-instance v1, Lcom/google/android/gms/common/download/k;

    invoke-direct {v1, p0}, Lcom/google/android/gms/common/download/k;-><init>(Lcom/google/android/gms/common/download/DownloadServiceSettingsActivity;)V

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/api/w;->a(Lcom/google/android/gms/common/api/y;)Lcom/google/android/gms/common/api/w;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/common/api/w;->a()Lcom/google/android/gms/common/api/v;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/common/download/DownloadServiceSettingsActivity;->d:Lcom/google/android/gms/common/api/v;

    .line 171
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 2

    .prologue
    .line 175
    .line 176
    const-string v0, "Start DownloadService Now"

    invoke-interface {p1, v0}, Landroid/view/Menu;->add(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    move-result-object v0

    .line 177
    new-instance v1, Lcom/google/android/gms/common/download/l;

    invoke-direct {v1, p0, p0}, Lcom/google/android/gms/common/download/l;-><init>(Lcom/google/android/gms/common/download/DownloadServiceSettingsActivity;Landroid/content/Context;)V

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 186
    const-string v0, "Enable Kitty"

    invoke-interface {p1, v0}, Landroid/view/Menu;->add(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    move-result-object v0

    .line 187
    new-instance v1, Lcom/google/android/gms/common/download/m;

    invoke-direct {v1, p0, p0}, Lcom/google/android/gms/common/download/m;-><init>(Lcom/google/android/gms/common/download/DownloadServiceSettingsActivity;Landroid/content/Context;)V

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 203
    const-string v0, "Disable Kitty"

    invoke-interface {p1, v0}, Landroid/view/Menu;->add(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    move-result-object v0

    .line 204
    new-instance v1, Lcom/google/android/gms/common/download/n;

    invoke-direct {v1, p0, p0}, Lcom/google/android/gms/common/download/n;-><init>(Lcom/google/android/gms/common/download/DownloadServiceSettingsActivity;Landroid/content/Context;)V

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 220
    const-string v0, "View Kitty"

    invoke-interface {p1, v0}, Landroid/view/Menu;->add(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    move-result-object v0

    .line 221
    new-instance v1, Lcom/google/android/gms/common/download/o;

    invoke-direct {v1, p0, p0}, Lcom/google/android/gms/common/download/o;-><init>(Lcom/google/android/gms/common/download/DownloadServiceSettingsActivity;Landroid/content/Context;)V

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 238
    invoke-super {p0, p1}, Landroid/support/v7/app/d;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    move-result v0

    return v0
.end method

.method protected onStart()V
    .locals 2

    .prologue
    .line 243
    invoke-super {p0}, Landroid/support/v7/app/d;->onStart()V

    .line 244
    iget-object v0, p0, Lcom/google/android/gms/common/download/DownloadServiceSettingsActivity;->d:Lcom/google/android/gms/common/api/v;

    invoke-interface {v0}, Lcom/google/android/gms/common/api/v;->b()V

    .line 245
    new-instance v0, Lcom/google/android/gms/common/download/p;

    invoke-direct {v0, p0, p0}, Lcom/google/android/gms/common/download/p;-><init>(Lcom/google/android/gms/common/download/DownloadServiceSettingsActivity;Landroid/content/Context;)V

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/download/p;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 246
    return-void
.end method

.method protected onStop()V
    .locals 2

    .prologue
    .line 251
    invoke-super {p0}, Landroid/support/v7/app/d;->onStop()V

    .line 252
    iget-object v0, p0, Lcom/google/android/gms/common/download/DownloadServiceSettingsActivity;->d:Lcom/google/android/gms/common/api/v;

    invoke-interface {v0}, Lcom/google/android/gms/common/api/v;->d()V

    .line 253
    iget-object v0, p0, Lcom/google/android/gms/common/download/DownloadServiceSettingsActivity;->e:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/gms/common/download/DownloadServiceSettingsActivity;->c:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 254
    return-void
.end method
