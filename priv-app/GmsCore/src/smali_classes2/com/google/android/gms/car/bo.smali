.class public final Lcom/google/android/gms/car/bo;
.super Lcom/google/android/gms/car/kq;
.source "SourceFile"

# interfaces
.implements Landroid/os/IBinder$DeathRecipient;


# instance fields
.field private final a:Lcom/google/android/gms/car/bj;

.field private b:Lcom/google/android/gms/car/ks;

.field private final c:Lcom/google/android/gms/car/bg;

.field private final d:I

.field private final e:I

.field private final f:I

.field private final g:I

.field private final h:I

.field private final i:I

.field private final j:Lcom/google/android/gms/car/CarAudioConfiguration;

.field private final k:Ljava/lang/Object;

.field private l:Ljava/io/InputStream;

.field private volatile m:I

.field private n:J

.field private o:J

.field private p:I

.field private final q:Ljava/util/LinkedList;

.field private final r:J

.field private s:J

.field private t:J

.field private volatile u:Z


# direct methods
.method constructor <init>(Lcom/google/android/gms/car/bj;Lcom/google/android/gms/car/bg;IIIII)V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    const/4 v2, 0x0

    .line 64
    invoke-direct {p0}, Lcom/google/android/gms/car/kq;-><init>()V

    .line 41
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/car/bo;->k:Ljava/lang/Object;

    .line 44
    iput v2, p0, Lcom/google/android/gms/car/bo;->m:I

    .line 46
    iput-wide v4, p0, Lcom/google/android/gms/car/bo;->n:J

    .line 48
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/google/android/gms/car/bo;->o:J

    .line 50
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/car/bo;->p:I

    .line 53
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/car/bo;->q:Ljava/util/LinkedList;

    .line 58
    iput-wide v4, p0, Lcom/google/android/gms/car/bo;->t:J

    .line 59
    iput-boolean v2, p0, Lcom/google/android/gms/car/bo;->u:Z

    .line 65
    iput-object p1, p0, Lcom/google/android/gms/car/bo;->a:Lcom/google/android/gms/car/bj;

    .line 66
    iput-object p2, p0, Lcom/google/android/gms/car/bo;->c:Lcom/google/android/gms/car/bg;

    .line 67
    iput p3, p0, Lcom/google/android/gms/car/bo;->d:I

    .line 68
    iput p4, p0, Lcom/google/android/gms/car/bo;->e:I

    .line 69
    iput p5, p0, Lcom/google/android/gms/car/bo;->f:I

    .line 70
    iput p6, p0, Lcom/google/android/gms/car/bo;->g:I

    .line 71
    iput p7, p0, Lcom/google/android/gms/car/bo;->h:I

    .line 72
    iget-object v0, p0, Lcom/google/android/gms/car/bo;->c:Lcom/google/android/gms/car/bg;

    invoke-virtual {v0, p3, p4}, Lcom/google/android/gms/car/bg;->a(II)Lcom/google/android/gms/car/CarAudioConfiguration;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/car/bo;->j:Lcom/google/android/gms/car/CarAudioConfiguration;

    .line 74
    iget-object v0, p0, Lcom/google/android/gms/car/bo;->j:Lcom/google/android/gms/car/CarAudioConfiguration;

    iget v0, v0, Lcom/google/android/gms/car/CarAudioConfiguration;->c:I

    const/16 v1, 0xc

    if-ne v0, v1, :cond_0

    const/4 v0, 0x4

    :goto_0
    iput v0, p0, Lcom/google/android/gms/car/bo;->i:I

    .line 75
    iget v0, p0, Lcom/google/android/gms/car/bo;->g:I

    iget v1, p0, Lcom/google/android/gms/car/bo;->i:I

    div-int/2addr v0, v1

    mul-int/lit16 v0, v0, 0x3e8

    iget-object v1, p0, Lcom/google/android/gms/car/bo;->j:Lcom/google/android/gms/car/CarAudioConfiguration;

    iget v1, v1, Lcom/google/android/gms/car/CarAudioConfiguration;->b:I

    div-int/2addr v0, v1

    int-to-long v0, v0

    iput-wide v0, p0, Lcom/google/android/gms/car/bo;->r:J

    .line 76
    return-void

    .line 74
    :cond_0
    const/4 v0, 0x2

    goto :goto_0
.end method

.method private a([BII)V
    .locals 8

    .prologue
    const-wide/16 v6, 0x1

    const/4 v2, 0x0

    .line 431
    move v1, v2

    .line 433
    :goto_0
    iget-object v0, p0, Lcom/google/android/gms/car/bo;->q:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->size()I

    move-result v0

    if-lez v0, :cond_1

    if-ge v1, p3, :cond_1

    .line 434
    iget-object v0, p0, Lcom/google/android/gms/car/bo;->q:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->pop()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B

    .line 435
    iget v3, p0, Lcom/google/android/gms/car/bo;->g:I

    invoke-static {v0, v2, p1, p2, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 436
    iget v0, p0, Lcom/google/android/gms/car/bo;->g:I

    add-int/2addr v0, v1

    .line 437
    iget v1, p0, Lcom/google/android/gms/car/bo;->g:I

    add-int/2addr p2, v1

    .line 438
    iget-wide v4, p0, Lcom/google/android/gms/car/bo;->n:J

    add-long/2addr v4, v6

    iput-wide v4, p0, Lcom/google/android/gms/car/bo;->n:J

    move v1, v0

    .line 439
    goto :goto_0

    .line 448
    :cond_0
    iget v0, p0, Lcom/google/android/gms/car/bo;->g:I

    add-int/2addr v1, v0

    .line 449
    iget v0, p0, Lcom/google/android/gms/car/bo;->g:I

    add-int/2addr p2, v0

    .line 450
    iget-wide v4, p0, Lcom/google/android/gms/car/bo;->n:J

    add-long/2addr v4, v6

    iput-wide v4, p0, Lcom/google/android/gms/car/bo;->n:J

    .line 451
    iget v0, p0, Lcom/google/android/gms/car/bo;->m:I

    iget v3, p0, Lcom/google/android/gms/car/bo;->g:I

    sub-int/2addr v0, v3

    iput v0, p0, Lcom/google/android/gms/car/bo;->m:I

    .line 440
    :cond_1
    if-ge v1, p3, :cond_2

    .line 441
    iget-object v0, p0, Lcom/google/android/gms/car/bo;->l:Ljava/io/InputStream;

    iget v3, p0, Lcom/google/android/gms/car/bo;->g:I

    invoke-virtual {v0, p1, p2, v3}, Ljava/io/InputStream;->read([BII)I

    move-result v0

    .line 442
    iget v3, p0, Lcom/google/android/gms/car/bo;->g:I

    if-eq v0, v3, :cond_0

    .line 444
    iput v2, p0, Lcom/google/android/gms/car/bo;->m:I

    .line 445
    new-instance v1, Ljava/io/IOException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "read returned "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " while expecting "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v2, p0, Lcom/google/android/gms/car/bo;->g:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 453
    :cond_2
    return-void
.end method

.method private f()V
    .locals 2

    .prologue
    .line 101
    invoke-virtual {p0}, Lcom/google/android/gms/car/bo;->d()V

    .line 102
    iget-object v0, p0, Lcom/google/android/gms/car/bo;->b:Lcom/google/android/gms/car/ks;

    if-eqz v0, :cond_0

    :try_start_0
    invoke-interface {v0}, Lcom/google/android/gms/car/ks;->asBinder()Landroid/os/IBinder;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, p0, v1}, Landroid/os/IBinder;->unlinkToDeath(Landroid/os/IBinder$DeathRecipient;I)Z
    :try_end_0
    .catch Ljava/util/NoSuchElementException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/car/bo;->b:Lcom/google/android/gms/car/ks;

    .line 103
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/car/bo;->c:Lcom/google/android/gms/car/bg;

    invoke-static {p0}, Lcom/google/android/gms/car/bg;->a(Lcom/google/android/gms/car/bo;)V

    .line 104
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method private g()I
    .locals 2

    .prologue
    .line 426
    iget-object v0, p0, Lcom/google/android/gms/car/bo;->q:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->size()I

    move-result v0

    iget v1, p0, Lcom/google/android/gms/car/bo;->g:I

    mul-int/2addr v0, v1

    iget v1, p0, Lcom/google/android/gms/car/bo;->m:I

    add-int/2addr v0, v1

    return v0
.end method

.method private h(Lcom/google/android/gms/car/ks;)V
    .locals 2

    .prologue
    .line 107
    iget-object v0, p0, Lcom/google/android/gms/car/bo;->b:Lcom/google/android/gms/car/ks;

    invoke-interface {v0}, Lcom/google/android/gms/car/ks;->asBinder()Landroid/os/IBinder;

    move-result-object v0

    invoke-interface {p1}, Lcom/google/android/gms/car/ks;->asBinder()Landroid/os/IBinder;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 108
    new-instance v0, Ljava/lang/SecurityException;

    const-string v1, "invalid client token"

    invoke-direct {v0, v1}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 110
    :cond_0
    return-void
.end method


# virtual methods
.method final a()I
    .locals 1

    .prologue
    .line 79
    iget v0, p0, Lcom/google/android/gms/car/bo;->d:I

    return v0
.end method

.method public final a(I)V
    .locals 3

    .prologue
    .line 456
    const-string v0, "CAR.AUDIO"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 457
    const-string v0, "CAR.AUDIO"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "audio play error:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", track "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 460
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/car/bo;->c:Lcom/google/android/gms/car/bg;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/car/bg;->b(Lcom/google/android/gms/car/bo;)V

    .line 462
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/car/bo;->b:Lcom/google/android/gms/car/ks;

    .line 463
    if-eqz v0, :cond_1

    iget-boolean v1, p0, Lcom/google/android/gms/car/bo;->u:Z

    if-nez v1, :cond_1

    .line 464
    invoke-interface {v0, p1}, Lcom/google/android/gms/car/ks;->a(I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 469
    :cond_1
    :goto_0
    return-void

    .line 467
    :catch_0
    move-exception v0

    iget-object v0, p0, Lcom/google/android/gms/car/bo;->c:Lcom/google/android/gms/car/bg;

    iget-object v1, p0, Lcom/google/android/gms/car/bo;->a:Lcom/google/android/gms/car/bj;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/car/bg;->a(Lcom/google/android/gms/car/bj;)V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/car/ks;)V
    .locals 0

    .prologue
    .line 96
    invoke-direct {p0, p1}, Lcom/google/android/gms/car/bo;->h(Lcom/google/android/gms/car/ks;)V

    .line 97
    invoke-direct {p0}, Lcom/google/android/gms/car/bo;->f()V

    .line 98
    return-void
.end method

.method public final a(Lcom/google/android/gms/car/ks;I)V
    .locals 4

    .prologue
    .line 282
    invoke-direct {p0, p1}, Lcom/google/android/gms/car/bo;->h(Lcom/google/android/gms/car/ks;)V

    .line 283
    if-gez p2, :cond_0

    .line 284
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "periodInFrames is negative "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 286
    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/car/bo;->k:Ljava/lang/Object;

    monitor-enter v1

    .line 287
    :try_start_0
    iget v0, p0, Lcom/google/android/gms/car/bo;->i:I

    mul-int/2addr v0, p2

    .line 288
    iget v2, p0, Lcom/google/android/gms/car/bo;->g:I

    add-int/2addr v0, v2

    add-int/lit8 v0, v0, -0x1

    iget v2, p0, Lcom/google/android/gms/car/bo;->g:I

    div-int/2addr v0, v2

    iput v0, p0, Lcom/google/android/gms/car/bo;->p:I

    .line 290
    const-string v0, "CAR.AUDIO"

    const/4 v2, 0x3

    invoke-static {v0, v2}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 291
    const-string v0, "CAR.AUDIO"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "setPositionNotificationPeriod, set to (in min buffers) "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v3, p0, Lcom/google/android/gms/car/bo;->p:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 294
    :cond_1
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method final a(Ljava/io/PrintWriter;)V
    .locals 4

    .prologue
    .line 483
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "configIndex:"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v1, p0, Lcom/google/android/gms/car/bo;->e:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " pid:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/gms/car/bo;->h:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " data available:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/gms/car/bo;->m:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " data read in buffers:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lcom/google/android/gms/car/bo;->n:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " last notification in buffers:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lcom/google/android/gms/car/bo;->o:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " notification period in buffers:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/gms/car/bo;->p:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " play start time:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lcom/google/android/gms/car/bo;->s:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 490
    return-void
.end method

.method public final a([BIII)Z
    .locals 14

    .prologue
    .line 339
    const/4 v4, 0x0

    .line 340
    const/4 v3, 0x0

    .line 341
    const/4 v2, 0x0

    .line 342
    iget-object v5, p0, Lcom/google/android/gms/car/bo;->k:Ljava/lang/Object;

    monitor-enter v5

    .line 343
    :try_start_0
    invoke-direct {p0}, Lcom/google/android/gms/car/bo;->g()I

    move-result v6

    move/from16 v0, p3

    if-ge v6, v0, :cond_1

    .line 344
    iget-boolean v6, p0, Lcom/google/android/gms/car/bo;->u:Z

    if-eqz v6, :cond_0

    invoke-direct {p0}, Lcom/google/android/gms/car/bo;->g()I

    move-result v6

    if-nez v6, :cond_0

    .line 345
    new-instance v2, Ljava/io/IOException;

    const-string v3, "stopping"

    invoke-direct {v2, v3}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 413
    :catchall_0
    move-exception v2

    monitor-exit v5

    throw v2

    .line 349
    :cond_0
    :try_start_1
    iget-object v6, p0, Lcom/google/android/gms/car/bo;->k:Ljava/lang/Object;

    iget-wide v8, p0, Lcom/google/android/gms/car/bo;->r:J

    invoke-virtual {v6, v8, v9}, Ljava/lang/Object;->wait(J)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 355
    :cond_1
    :goto_0
    :try_start_2
    invoke-direct {p0}, Lcom/google/android/gms/car/bo;->g()I

    move-result v6

    .line 356
    const-string v7, "CAR.AUDIO"

    const/4 v8, 0x2

    invoke-static {v7, v8}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 357
    const-string v7, "CAR.AUDIO"

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "readAudioStream minBuffer "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v9, p0, Lcom/google/android/gms/car/bo;->g:I

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " read l "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    move/from16 v0, p3

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " mDataBuffer size "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p0, Lcom/google/android/gms/car/bo;->q:Ljava/util/LinkedList;

    invoke-virtual {v9}, Ljava/util/LinkedList;->size()I

    move-result v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " mDataAvailableInInputStream "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget v9, p0, Lcom/google/android/gms/car/bo;->m:I

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " mDataReadInMinBuffers "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-wide v10, p0, Lcom/google/android/gms/car/bo;->n:J

    invoke-virtual {v8, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " numQueued "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    move/from16 v0, p4

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " mLastNotificationInMinBuffers "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-wide v10, p0, Lcom/google/android/gms/car/bo;->o:J

    invoke-virtual {v8, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " mNotificationPeriodInMinBuffers "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget v9, p0, Lcom/google/android/gms/car/bo;->p:I

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 366
    :cond_2
    move/from16 v0, p3

    if-lt v6, v0, :cond_6

    .line 367
    const/4 v4, 0x0

    move/from16 v0, p3

    invoke-direct {p0, p1, v4, v0}, Lcom/google/android/gms/car/bo;->a([BII)V

    .line 368
    iget v4, p0, Lcom/google/android/gms/car/bo;->p:I

    if-lez v4, :cond_3

    iget-wide v6, p0, Lcom/google/android/gms/car/bo;->n:J

    iget-wide v8, p0, Lcom/google/android/gms/car/bo;->o:J

    iget v4, p0, Lcom/google/android/gms/car/bo;->p:I

    int-to-long v10, v4

    add-long/2addr v8, v10

    cmp-long v4, v6, v8

    if-nez v4, :cond_3

    .line 371
    iget-wide v6, p0, Lcom/google/android/gms/car/bo;->n:J

    iput-wide v6, p0, Lcom/google/android/gms/car/bo;->o:J

    .line 372
    const/4 v3, 0x1

    .line 374
    :cond_3
    const/4 v4, 0x1

    .line 375
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v6

    iput-wide v6, p0, Lcom/google/android/gms/car/bo;->t:J

    .line 413
    :cond_4
    :goto_1
    monitor-exit v5
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 415
    if-eqz v3, :cond_5

    .line 416
    :try_start_3
    iget-object v3, p0, Lcom/google/android/gms/car/bo;->b:Lcom/google/android/gms/car/ks;

    if-eqz v3, :cond_5

    invoke-interface {v3}, Lcom/google/android/gms/car/ks;->a()V
    :try_end_3
    .catch Landroid/os/RemoteException; {:try_start_3 .. :try_end_3} :catch_1

    .line 418
    :cond_5
    :goto_2
    if-eqz v2, :cond_b

    .line 419
    const/4 v2, 0x3

    invoke-virtual {p0, v2}, Lcom/google/android/gms/car/bo;->a(I)V

    .line 420
    new-instance v2, Ljava/nio/BufferUnderflowException;

    invoke-direct {v2}, Ljava/nio/BufferUnderflowException;-><init>()V

    throw v2

    .line 376
    :cond_6
    if-nez v6, :cond_4

    if-nez p4, :cond_4

    .line 377
    :try_start_4
    iget-boolean v6, p0, Lcom/google/android/gms/car/bo;->u:Z

    if-eqz v6, :cond_7

    .line 378
    new-instance v2, Ljava/io/IOException;

    const-string v3, "stopping"

    invoke-direct {v2, v3}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 380
    :cond_7
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v6

    .line 381
    iget-wide v8, p0, Lcom/google/android/gms/car/bo;->r:J

    iget-wide v10, p0, Lcom/google/android/gms/car/bo;->n:J

    mul-long/2addr v8, v10

    .line 385
    iget-wide v10, p0, Lcom/google/android/gms/car/bo;->s:J

    add-long/2addr v8, v10

    sub-long/2addr v8, v6

    .line 388
    iget-wide v10, p0, Lcom/google/android/gms/car/bo;->t:J

    iget-wide v12, p0, Lcom/google/android/gms/car/bo;->r:J

    add-long/2addr v10, v12

    sub-long v6, v10, v6

    .line 390
    const-wide/16 v10, 0x0

    cmp-long v10, v8, v10

    if-lez v10, :cond_8

    const-wide/16 v10, 0x0

    cmp-long v10, v6, v10

    if-gtz v10, :cond_9

    .line 392
    :cond_8
    const/4 v2, 0x1

    .line 393
    const-string v10, "CAR.AUDIO"

    const/4 v11, 0x2

    invoke-static {v10, v11}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v10

    if-eqz v10, :cond_4

    .line 394
    const-string v10, "CAR.AUDIO"

    new-instance v11, Ljava/lang/StringBuilder;

    const-string v12, "buffer underrun triggered, timeout in playtime "

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v11, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " timeout in buffer"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v10, v6}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 400
    :cond_9
    invoke-static {v8, v9, v6, v7}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v6

    .line 402
    const-string v8, "CAR.AUDIO"

    const/4 v9, 0x3

    invoke-static {v8, v9}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v8

    if-eqz v8, :cond_a

    .line 403
    const-string v8, "CAR.AUDIO"

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "buffer underrun approaching, will wait for data "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 407
    :cond_a
    :try_start_5
    iget-object v8, p0, Lcom/google/android/gms/car/bo;->k:Ljava/lang/Object;

    invoke-virtual {v8, v6, v7}, Ljava/lang/Object;->wait(J)V
    :try_end_5
    .catch Ljava/lang/InterruptedException; {:try_start_5 .. :try_end_5} :catch_0
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto/16 :goto_1

    :catch_0
    move-exception v6

    goto/16 :goto_1

    .line 416
    :catch_1
    move-exception v3

    iget-object v3, p0, Lcom/google/android/gms/car/bo;->c:Lcom/google/android/gms/car/bg;

    iget-object v5, p0, Lcom/google/android/gms/car/bo;->a:Lcom/google/android/gms/car/bj;

    invoke-virtual {v3, v5}, Lcom/google/android/gms/car/bg;->a(Lcom/google/android/gms/car/bj;)V

    goto/16 :goto_2

    .line 422
    :cond_b
    return v4

    :catch_2
    move-exception v6

    goto/16 :goto_0
.end method

.method final b()I
    .locals 1

    .prologue
    .line 83
    iget v0, p0, Lcom/google/android/gms/car/bo;->e:I

    return v0
.end method

.method public final b(Lcom/google/android/gms/car/ks;)Landroid/os/ParcelFileDescriptor;
    .locals 4

    .prologue
    .line 149
    iget-object v0, p0, Lcom/google/android/gms/car/bo;->c:Lcom/google/android/gms/car/bg;

    invoke-virtual {v0}, Lcom/google/android/gms/car/bg;->c()V

    .line 150
    invoke-direct {p0, p1}, Lcom/google/android/gms/car/bo;->h(Lcom/google/android/gms/car/ks;)V

    .line 153
    :try_start_0
    invoke-static {}, Landroid/os/ParcelFileDescriptor;->createPipe()[Landroid/os/ParcelFileDescriptor;

    move-result-object v0

    .line 154
    iget-object v1, p0, Lcom/google/android/gms/car/bo;->k:Ljava/lang/Object;

    monitor-enter v1
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 155
    :try_start_1
    new-instance v2, Landroid/os/ParcelFileDescriptor$AutoCloseInputStream;

    const/4 v3, 0x0

    aget-object v3, v0, v3

    invoke-direct {v2, v3}, Landroid/os/ParcelFileDescriptor$AutoCloseInputStream;-><init>(Landroid/os/ParcelFileDescriptor;)V

    iput-object v2, p0, Lcom/google/android/gms/car/bo;->l:Ljava/io/InputStream;

    .line 156
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 157
    const/4 v1, 0x1

    :try_start_2
    aget-object v0, v0, v1

    return-object v0

    .line 156
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    .line 159
    :catch_0
    move-exception v0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "cannot create pipe"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final b(Lcom/google/android/gms/car/ks;I)V
    .locals 5

    .prologue
    .line 299
    iget-object v0, p0, Lcom/google/android/gms/car/bo;->c:Lcom/google/android/gms/car/bg;

    invoke-virtual {v0}, Lcom/google/android/gms/car/bg;->c()V

    .line 300
    invoke-direct {p0, p1}, Lcom/google/android/gms/car/bo;->h(Lcom/google/android/gms/car/ks;)V

    .line 301
    iget v0, p0, Lcom/google/android/gms/car/bo;->g:I

    rem-int v0, p2, v0

    if-eqz v0, :cond_0

    .line 302
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "write size: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", should be a multiple of min buffer size: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/google/android/gms/car/bo;->g:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 305
    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/car/bo;->k:Ljava/lang/Object;

    monitor-enter v1

    .line 306
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/car/bo;->c:Lcom/google/android/gms/car/bg;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/car/bg;->c(Lcom/google/android/gms/car/bo;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 307
    iget-object v0, p0, Lcom/google/android/gms/car/bo;->q:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->size()I

    move-result v0

    iget v2, p0, Lcom/google/android/gms/car/bo;->g:I

    mul-int/2addr v0, v2

    add-int/2addr v0, p2

    iget v2, p0, Lcom/google/android/gms/car/bo;->f:I

    if-le v0, v2, :cond_1

    .line 308
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v2, "data before play exceeds buffer size"

    invoke-direct {v0, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 329
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 310
    :cond_1
    const/4 v0, 0x0

    .line 312
    :goto_0
    if-ge v0, p2, :cond_4

    .line 313
    :try_start_1
    iget v2, p0, Lcom/google/android/gms/car/bo;->g:I

    new-array v2, v2, [B

    .line 314
    iget-object v3, p0, Lcom/google/android/gms/car/bo;->l:Ljava/io/InputStream;

    invoke-virtual {v3, v2}, Ljava/io/InputStream;->read([B)I

    move-result v3

    .line 315
    iget v4, p0, Lcom/google/android/gms/car/bo;->g:I

    if-eq v3, v4, :cond_2

    .line 316
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v4, "cannot read in min buffer size, got "

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 323
    :catch_0
    move-exception v0

    :try_start_2
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v2, "cannot read from client"

    invoke-direct {v0, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 319
    :cond_2
    :try_start_3
    iget-object v3, p0, Lcom/google/android/gms/car/bo;->q:Ljava/util/LinkedList;

    invoke-virtual {v3, v2}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 320
    iget v2, p0, Lcom/google/android/gms/car/bo;->g:I
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    add-int/2addr v0, v2

    .line 321
    goto :goto_0

    .line 325
    :cond_3
    :try_start_4
    iget v0, p0, Lcom/google/android/gms/car/bo;->m:I

    add-int/2addr v0, p2

    iput v0, p0, Lcom/google/android/gms/car/bo;->m:I

    .line 327
    iget-object v0, p0, Lcom/google/android/gms/car/bo;->k:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->notifyAll()V

    .line 329
    :cond_4
    monitor-exit v1
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    return-void
.end method

.method public final binderDied()V
    .locals 0

    .prologue
    .line 334
    invoke-direct {p0}, Lcom/google/android/gms/car/bo;->f()V

    .line 335
    return-void
.end method

.method final c()Lcom/google/android/gms/car/bj;
    .locals 1

    .prologue
    .line 91
    iget-object v0, p0, Lcom/google/android/gms/car/bo;->a:Lcom/google/android/gms/car/bj;

    return-object v0
.end method

.method public final c(Lcom/google/android/gms/car/ks;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 165
    iget-object v0, p0, Lcom/google/android/gms/car/bo;->c:Lcom/google/android/gms/car/bg;

    invoke-virtual {v0}, Lcom/google/android/gms/car/bg;->c()V

    .line 166
    invoke-direct {p0, p1}, Lcom/google/android/gms/car/bo;->h(Lcom/google/android/gms/car/ks;)V

    .line 167
    iget-object v0, p0, Lcom/google/android/gms/car/bo;->c:Lcom/google/android/gms/car/bg;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/car/bg;->c(Lcom/google/android/gms/car/bo;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 168
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "already playing"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 170
    :cond_0
    iput-boolean v4, p0, Lcom/google/android/gms/car/bo;->u:Z

    .line 171
    iget-object v1, p0, Lcom/google/android/gms/car/bo;->k:Ljava/lang/Object;

    monitor-enter v1

    .line 172
    :try_start_0
    iget v0, p0, Lcom/google/android/gms/car/bo;->p:I

    if-lez v0, :cond_1

    .line 173
    const-wide/16 v2, 0x0

    iput-wide v2, p0, Lcom/google/android/gms/car/bo;->o:J

    .line 175
    :cond_1
    const-wide/16 v2, 0x0

    iput-wide v2, p0, Lcom/google/android/gms/car/bo;->n:J

    .line 176
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 177
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/gms/car/bo;->s:J

    .line 178
    iget-object v0, p0, Lcom/google/android/gms/car/bo;->c:Lcom/google/android/gms/car/bg;

    invoke-virtual {v0, p0, v4}, Lcom/google/android/gms/car/bg;->a(Lcom/google/android/gms/car/bo;Z)Lcom/google/android/gms/car/g;

    move-result-object v1

    if-nez v1, :cond_2

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "already taken"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 176
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 178
    :cond_2
    const-string v2, "CAR.AUDIO"

    const/4 v3, 0x3

    invoke-static {v2, v3}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_3

    const-string v2, "CAR.AUDIO"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "playAudioTrack for track "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_3
    invoke-virtual {v1}, Lcom/google/android/gms/car/g;->d()V

    iget v2, p0, Lcom/google/android/gms/car/bo;->e:I

    invoke-virtual {v1, v2}, Lcom/google/android/gms/car/g;->a(I)Z

    move-result v1

    if-nez v1, :cond_4

    const/4 v1, 0x1

    invoke-virtual {v0, p0, v1}, Lcom/google/android/gms/car/bg;->a(Lcom/google/android/gms/car/bo;Z)Lcom/google/android/gms/car/g;

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "no focus or wrong state"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 179
    :cond_4
    return-void
.end method

.method public final d()V
    .locals 2

    .prologue
    .line 127
    iget-object v1, p0, Lcom/google/android/gms/car/bo;->k:Ljava/lang/Object;

    monitor-enter v1

    .line 128
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/car/bo;->q:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->clear()V

    .line 129
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/gms/car/bo;->m:I

    .line 130
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 131
    iget-object v0, p0, Lcom/google/android/gms/car/bo;->c:Lcom/google/android/gms/car/bg;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/car/bg;->b(Lcom/google/android/gms/car/bo;)V

    .line 132
    iget-object v1, p0, Lcom/google/android/gms/car/bo;->k:Ljava/lang/Object;

    monitor-enter v1

    .line 133
    :try_start_1
    iget-object v0, p0, Lcom/google/android/gms/car/bo;->l:Ljava/io/InputStream;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    if-eqz v0, :cond_0

    .line 135
    :try_start_2
    iget-object v0, p0, Lcom/google/android/gms/car/bo;->l:Ljava/io/InputStream;

    invoke-virtual {v0}, Ljava/io/InputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 140
    :cond_0
    :goto_0
    :try_start_3
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    return-void

    .line 130
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 140
    :catchall_1
    move-exception v0

    monitor-exit v1

    throw v0

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public final d(Lcom/google/android/gms/car/ks;)V
    .locals 1

    .prologue
    .line 183
    iget-object v0, p0, Lcom/google/android/gms/car/bo;->c:Lcom/google/android/gms/car/bg;

    invoke-virtual {v0}, Lcom/google/android/gms/car/bg;->c()V

    .line 184
    invoke-direct {p0, p1}, Lcom/google/android/gms/car/bo;->h(Lcom/google/android/gms/car/ks;)V

    .line 185
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/car/bo;->u:Z

    .line 186
    iget-object v0, p0, Lcom/google/android/gms/car/bo;->c:Lcom/google/android/gms/car/bg;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/car/bg;->c(Lcom/google/android/gms/car/bo;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 191
    :goto_0
    return-void

    .line 190
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/car/bo;->c:Lcom/google/android/gms/car/bg;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/car/bg;->b(Lcom/google/android/gms/car/bo;)V

    goto :goto_0
.end method

.method public final e()V
    .locals 0

    .prologue
    .line 144
    invoke-virtual {p0}, Lcom/google/android/gms/car/bo;->d()V

    .line 145
    return-void
.end method

.method public final e(Lcom/google/android/gms/car/ks;)V
    .locals 5

    .prologue
    .line 195
    invoke-direct {p0, p1}, Lcom/google/android/gms/car/bo;->h(Lcom/google/android/gms/car/ks;)V

    .line 196
    iget-object v0, p0, Lcom/google/android/gms/car/bo;->c:Lcom/google/android/gms/car/bg;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/car/bg;->c(Lcom/google/android/gms/car/bo;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 221
    :goto_0
    return-void

    .line 200
    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/car/bo;->k:Ljava/lang/Object;

    monitor-enter v1

    .line 201
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/car/bo;->q:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->clear()V

    .line 202
    iget v0, p0, Lcom/google/android/gms/car/bo;->m:I

    if-gtz v0, :cond_1

    .line 203
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 221
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 205
    :cond_1
    :try_start_1
    const-string v0, "CAR.AUDIO"

    const/4 v2, 0x3

    invoke-static {v0, v2}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 206
    const-string v0, "CAR.AUDIO"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "flush requested, will throw away:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v3, p0, Lcom/google/android/gms/car/bo;->m:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 208
    :cond_2
    iget v0, p0, Lcom/google/android/gms/car/bo;->g:I

    new-array v0, v0, [B

    .line 209
    :goto_1
    iget v2, p0, Lcom/google/android/gms/car/bo;->m:I

    iget v3, p0, Lcom/google/android/gms/car/bo;->g:I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-lt v2, v3, :cond_3

    .line 211
    :try_start_2
    iget-object v2, p0, Lcom/google/android/gms/car/bo;->l:Ljava/io/InputStream;

    const/4 v3, 0x0

    iget v4, p0, Lcom/google/android/gms/car/bo;->g:I

    invoke-virtual {v2, v0, v3, v4}, Ljava/io/InputStream;->read([BII)I
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result v2

    .line 212
    if-gez v2, :cond_4

    .line 221
    :cond_3
    :try_start_3
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 216
    :cond_4
    :try_start_4
    iget v3, p0, Lcom/google/android/gms/car/bo;->m:I

    sub-int v2, v3, v2

    iput v2, p0, Lcom/google/android/gms/car/bo;->m:I
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_1

    .line 217
    :catch_0
    move-exception v0

    .line 218
    :try_start_5
    new-instance v2, Ljava/lang/IllegalStateException;

    invoke-direct {v2, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/Throwable;)V

    throw v2
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0
.end method

.method public final f(Lcom/google/android/gms/car/ks;)V
    .locals 4

    .prologue
    .line 226
    invoke-direct {p0, p1}, Lcom/google/android/gms/car/bo;->h(Lcom/google/android/gms/car/ks;)V

    .line 227
    const-string v0, "CAR.AUDIO"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 229
    iget-object v1, p0, Lcom/google/android/gms/car/bo;->k:Ljava/lang/Object;

    monitor-enter v1

    .line 230
    :try_start_0
    invoke-direct {p0}, Lcom/google/android/gms/car/bo;->g()I

    move-result v0

    .line 231
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 232
    const-string v1, "CAR.AUDIO"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "stop requested, need to wait for remaining bytes:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 237
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/car/bo;->c:Lcom/google/android/gms/car/bg;

    invoke-virtual {v0}, Lcom/google/android/gms/car/bg;->c()V

    .line 238
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/car/bo;->u:Z

    .line 239
    iget-object v0, p0, Lcom/google/android/gms/car/bo;->c:Lcom/google/android/gms/car/bg;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/car/bg;->c(Lcom/google/android/gms/car/bo;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 263
    :goto_0
    return-void

    .line 231
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 244
    :cond_1
    iget-object v1, p0, Lcom/google/android/gms/car/bo;->k:Ljava/lang/Object;

    monitor-enter v1

    .line 245
    :try_start_1
    invoke-direct {p0}, Lcom/google/android/gms/car/bo;->g()I

    move-result v0

    .line 247
    iget-object v2, p0, Lcom/google/android/gms/car/bo;->k:Ljava/lang/Object;

    invoke-virtual {v2}, Ljava/lang/Object;->notify()V

    .line 248
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 249
    iget v1, p0, Lcom/google/android/gms/car/bo;->g:I

    if-lt v0, v1, :cond_3

    .line 252
    const-wide/16 v2, 0x5

    :try_start_2
    invoke-static {v2, v3}, Ljava/lang/Thread;->sleep(J)V
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_0

    .line 261
    :cond_2
    :goto_1
    if-gtz v0, :cond_0

    .line 262
    :goto_2
    iget-object v0, p0, Lcom/google/android/gms/car/bo;->c:Lcom/google/android/gms/car/bg;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/car/bg;->b(Lcom/google/android/gms/car/bo;)V

    goto :goto_0

    .line 248
    :catchall_1
    move-exception v0

    monitor-exit v1

    throw v0

    .line 256
    :cond_3
    iget v1, p0, Lcom/google/android/gms/car/bo;->g:I

    if-ge v0, v1, :cond_2

    if-eqz v0, :cond_2

    .line 258
    const-string v1, "CAR.AUDIO"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "client wrote wrong data size, "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " left."

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 255
    :catch_0
    move-exception v1

    goto :goto_1
.end method

.method public final g(Lcom/google/android/gms/car/ks;)V
    .locals 2

    .prologue
    .line 267
    iget-object v0, p0, Lcom/google/android/gms/car/bo;->c:Lcom/google/android/gms/car/bg;

    invoke-virtual {v0}, Lcom/google/android/gms/car/bg;->c()V

    .line 268
    iget-object v0, p0, Lcom/google/android/gms/car/bo;->b:Lcom/google/android/gms/car/ks;

    if-eqz v0, :cond_0

    .line 269
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "callback already registered"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 272
    :cond_0
    :try_start_0
    invoke-interface {p1}, Lcom/google/android/gms/car/ks;->asBinder()Landroid/os/IBinder;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, p0, v1}, Landroid/os/IBinder;->linkToDeath(Landroid/os/IBinder$DeathRecipient;I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 277
    iput-object p1, p0, Lcom/google/android/gms/car/bo;->b:Lcom/google/android/gms/car/ks;

    .line 278
    :goto_0
    return-void

    .line 274
    :catch_0
    move-exception v0

    iget-object v0, p0, Lcom/google/android/gms/car/bo;->c:Lcom/google/android/gms/car/bg;

    invoke-static {p0}, Lcom/google/android/gms/car/bg;->a(Lcom/google/android/gms/car/bo;)V

    goto :goto_0
.end method
