.class public final Lcom/google/android/gms/icing/impl/c/a;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public final a:Lcom/google/android/gms/icing/impl/n;

.field public final b:Lcom/google/android/gms/icing/g;


# direct methods
.method constructor <init>(Lcom/google/android/gms/icing/impl/n;Lcom/google/android/gms/icing/g;)V
    .locals 0

    .prologue
    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    iput-object p1, p0, Lcom/google/android/gms/icing/impl/c/a;->a:Lcom/google/android/gms/icing/impl/n;

    .line 37
    iput-object p2, p0, Lcom/google/android/gms/icing/impl/c/a;->b:Lcom/google/android/gms/icing/g;

    .line 38
    return-void
.end method


# virtual methods
.method final a(Lcom/google/android/gms/icing/aq;)Landroid/util/Pair;
    .locals 5

    .prologue
    .line 110
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 111
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 112
    const/4 v0, 0x0

    .line 113
    :goto_0
    iget-object v3, p0, Lcom/google/android/gms/icing/impl/c/a;->b:Lcom/google/android/gms/icing/g;

    iget-object v3, v3, Lcom/google/android/gms/icing/g;->k:[Lcom/google/android/gms/icing/az;

    array-length v3, v3

    if-ge v0, v3, :cond_0

    .line 115
    iget-object v3, p0, Lcom/google/android/gms/icing/impl/c/a;->b:Lcom/google/android/gms/icing/g;

    iget-object v3, v3, Lcom/google/android/gms/icing/g;->k:[Lcom/google/android/gms/icing/az;

    aget-object v3, v3, v0

    .line 116
    iget v3, v3, Lcom/google/android/gms/icing/az;->a:I

    invoke-static {v3}, Lcom/google/android/gms/appdatasearch/y;->a(I)Ljava/lang/String;

    move-result-object v3

    .line 118
    iget-object v4, p1, Lcom/google/android/gms/icing/aq;->a:[Lcom/google/android/gms/icing/ar;

    aget-object v4, v4, v0

    iget-object v4, v4, Lcom/google/android/gms/icing/ar;->a:[I

    invoke-virtual {v1, v3, v4}, Landroid/os/Bundle;->putIntArray(Ljava/lang/String;[I)V

    .line 120
    iget-object v4, p1, Lcom/google/android/gms/icing/aq;->a:[Lcom/google/android/gms/icing/ar;

    aget-object v4, v4, v0

    iget-object v4, v4, Lcom/google/android/gms/icing/ar;->b:[B

    invoke-virtual {v2, v3, v4}, Landroid/os/Bundle;->putByteArray(Ljava/lang/String;[B)V

    .line 114
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 123
    :cond_0
    invoke-static {v1, v2}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/util/List;)V
    .locals 6

    .prologue
    .line 76
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/google/android/gms/icing/impl/c/a;->b:Lcom/google/android/gms/icing/g;

    iget-object v1, v1, Lcom/google/android/gms/icing/g;->j:[Lcom/google/android/gms/icing/av;

    array-length v1, v1

    if-ge v0, v1, :cond_2

    .line 77
    iget-object v1, p0, Lcom/google/android/gms/icing/impl/c/a;->b:Lcom/google/android/gms/icing/g;

    iget-object v1, v1, Lcom/google/android/gms/icing/g;->j:[Lcom/google/android/gms/icing/av;

    aget-object v1, v1, v0

    .line 79
    iget-object v2, p0, Lcom/google/android/gms/icing/impl/c/a;->a:Lcom/google/android/gms/icing/impl/n;

    new-instance v3, Lcom/google/android/gms/appdatasearch/CorpusId;

    iget-object v4, p0, Lcom/google/android/gms/icing/impl/c/a;->b:Lcom/google/android/gms/icing/g;

    iget-object v4, v4, Lcom/google/android/gms/icing/g;->d:Ljava/lang/String;

    iget-object v5, p0, Lcom/google/android/gms/icing/impl/c/a;->b:Lcom/google/android/gms/icing/g;

    iget-object v5, v5, Lcom/google/android/gms/icing/g;->b:Ljava/lang/String;

    invoke-direct {v3, v4, v5}, Lcom/google/android/gms/appdatasearch/CorpusId;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v4, v1, Lcom/google/android/gms/icing/av;->a:Ljava/lang/String;

    iget v5, v1, Lcom/google/android/gms/icing/av;->d:I

    invoke-interface {v2, v3, v4, v5}, Lcom/google/android/gms/icing/impl/n;->a(Lcom/google/android/gms/appdatasearch/CorpusId;Ljava/lang/String;I)I

    move-result v2

    iput v2, v1, Lcom/google/android/gms/icing/av;->d:I

    .line 82
    iget v2, v1, Lcom/google/android/gms/icing/av;->d:I

    const/4 v3, 0x1

    if-ne v2, v3, :cond_0

    iget v2, v1, Lcom/google/android/gms/icing/av;->l:I

    if-eqz v2, :cond_1

    .line 84
    :cond_0
    new-instance v2, Lcom/google/android/gms/icing/ao;

    invoke-direct {v2}, Lcom/google/android/gms/icing/ao;-><init>()V

    .line 85
    iput v0, v2, Lcom/google/android/gms/icing/ao;->b:I

    .line 86
    iget-object v3, p0, Lcom/google/android/gms/icing/impl/c/a;->b:Lcom/google/android/gms/icing/g;

    iget v3, v3, Lcom/google/android/gms/icing/g;->a:I

    iput v3, v2, Lcom/google/android/gms/icing/ao;->a:I

    .line 87
    iget v3, v1, Lcom/google/android/gms/icing/av;->d:I

    iput v3, v2, Lcom/google/android/gms/icing/ao;->c:I

    .line 88
    iget v1, v1, Lcom/google/android/gms/icing/av;->l:I

    iput v1, v2, Lcom/google/android/gms/icing/ao;->d:I

    .line 89
    invoke-interface {p1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 76
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 92
    :cond_2
    return-void
.end method
