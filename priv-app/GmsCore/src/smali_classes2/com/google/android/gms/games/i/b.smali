.class public final Lcom/google/android/gms/games/i/b;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public final a:Landroid/content/Context;

.field final b:Lcom/google/android/gms/games/i/r;

.field final c:Ljava/util/ArrayList;

.field public final d:Ljava/lang/String;

.field public final e:[Ljava/lang/String;

.field public f:Lcom/google/android/gms/games/internal/ea;

.field public g:Lcom/google/android/gms/games/i/x;

.field h:Lcom/google/android/gms/games/i/e;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;[Ljava/lang/String;Lcom/google/android/gms/games/i/q;Lcom/google/android/gms/games/i/r;)V
    .locals 1

    .prologue
    .line 489
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 490
    invoke-static {p1}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iput-object v0, p0, Lcom/google/android/gms/games/i/b;->a:Landroid/content/Context;

    .line 491
    invoke-static {p2}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/gms/games/i/b;->d:Ljava/lang/String;

    .line 492
    invoke-static {p3}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/gms/games/i/b;->e:[Ljava/lang/String;

    .line 493
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/games/i/b;->c:Ljava/util/ArrayList;

    .line 494
    iget-object v0, p0, Lcom/google/android/gms/games/i/b;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, p4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 495
    iput-object p5, p0, Lcom/google/android/gms/games/i/b;->b:Lcom/google/android/gms/games/i/r;

    .line 496
    new-instance v0, Lcom/google/android/gms/games/i/e;

    invoke-direct {v0, p0}, Lcom/google/android/gms/games/i/e;-><init>(Lcom/google/android/gms/games/i/b;)V

    iput-object v0, p0, Lcom/google/android/gms/games/i/b;->h:Lcom/google/android/gms/games/i/e;

    .line 497
    return-void
.end method


# virtual methods
.method public final a(Z)Ljava/lang/String;
    .locals 2

    .prologue
    .line 623
    invoke-virtual {p0}, Lcom/google/android/gms/games/i/b;->c()V

    .line 625
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/games/i/b;->f:Lcom/google/android/gms/games/internal/ea;

    iget-object v1, p0, Lcom/google/android/gms/games/i/b;->d:Ljava/lang/String;

    invoke-interface {v0, v1, p1}, Lcom/google/android/gms/games/internal/ea;->a(Ljava/lang/String;Z)Ljava/lang/String;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 628
    :goto_0
    return-object v0

    .line 627
    :catch_0
    move-exception v0

    const-string v0, "SignInClient"

    const-string v1, "service died"

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 628
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a()V
    .locals 5

    .prologue
    .line 555
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.google.android.gms.games.signin.service.START"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v1, "com.google.android.gms"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 557
    iget-object v1, p0, Lcom/google/android/gms/games/i/b;->g:Lcom/google/android/gms/games/i/x;

    if-eqz v1, :cond_0

    .line 558
    const-string v1, "SignInClient"

    const-string v2, "Calling connect() while still connected, missing disconnect()."

    invoke-static {v1, v2}, Lcom/google/android/gms/games/internal/dq;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 559
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/gms/games/i/b;->f:Lcom/google/android/gms/games/internal/ea;

    .line 560
    invoke-static {}, Lcom/google/android/gms/common/stats/b;->a()Lcom/google/android/gms/common/stats/b;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/games/i/b;->a:Landroid/content/Context;

    iget-object v3, p0, Lcom/google/android/gms/games/i/b;->g:Lcom/google/android/gms/games/i/x;

    invoke-virtual {v1, v2, v3}, Lcom/google/android/gms/common/stats/b;->a(Landroid/content/Context;Landroid/content/ServiceConnection;)V

    .line 562
    :cond_0
    new-instance v1, Lcom/google/android/gms/games/i/x;

    iget-object v2, p0, Lcom/google/android/gms/games/i/b;->a:Landroid/content/Context;

    invoke-direct {v1, p0, v2}, Lcom/google/android/gms/games/i/x;-><init>(Lcom/google/android/gms/games/i/b;Landroid/content/Context;)V

    iput-object v1, p0, Lcom/google/android/gms/games/i/b;->g:Lcom/google/android/gms/games/i/x;

    .line 563
    invoke-static {}, Lcom/google/android/gms/common/stats/b;->a()Lcom/google/android/gms/common/stats/b;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/games/i/b;->a:Landroid/content/Context;

    iget-object v3, p0, Lcom/google/android/gms/games/i/b;->g:Lcom/google/android/gms/games/i/x;

    const/16 v4, 0x81

    invoke-virtual {v1, v2, v0, v3, v4}, Lcom/google/android/gms/common/stats/b;->a(Landroid/content/Context;Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    .line 566
    return-void
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 572
    iget-object v0, p0, Lcom/google/android/gms/games/i/b;->f:Lcom/google/android/gms/games/internal/ea;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c()V
    .locals 2

    .prologue
    .line 590
    invoke-virtual {p0}, Lcom/google/android/gms/games/i/b;->b()Z

    move-result v0

    if-nez v0, :cond_0

    .line 591
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Not connected. Call connect() and wait for onConnected() to be called."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 594
    :cond_0
    return-void
.end method
