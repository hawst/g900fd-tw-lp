.class public final Lcom/google/android/gms/games/b/t;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private a:Lcom/google/android/gms/common/server/ClientContext;

.field private b:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    iput-object p1, p0, Lcom/google/android/gms/games/b/t;->a:Lcom/google/android/gms/common/server/ClientContext;

    .line 23
    iput-object p2, p0, Lcom/google/android/gms/games/b/t;->b:Ljava/lang/String;

    .line 24
    return-void
.end method


# virtual methods
.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 33
    instance-of v1, p1, Lcom/google/android/gms/games/b/t;

    if-nez v1, :cond_1

    .line 37
    :cond_0
    :goto_0
    return v0

    .line 36
    :cond_1
    check-cast p1, Lcom/google/android/gms/games/b/t;

    .line 37
    iget-object v1, p0, Lcom/google/android/gms/games/b/t;->a:Lcom/google/android/gms/common/server/ClientContext;

    iget-object v2, p1, Lcom/google/android/gms/games/b/t;->a:Lcom/google/android/gms/common/server/ClientContext;

    invoke-static {v1, v2}, Lcom/google/android/gms/common/internal/bu;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/gms/games/b/t;->b:Ljava/lang/String;

    iget-object v2, p1, Lcom/google/android/gms/games/b/t;->b:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/android/gms/common/internal/bu;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    .line 28
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/gms/games/b/t;->a:Lcom/google/android/gms/common/server/ClientContext;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/gms/games/b/t;->b:Ljava/lang/String;

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method
