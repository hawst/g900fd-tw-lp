.class public final Lcom/google/android/gms/icing/v;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# static fields
.field private static volatile h:[Lcom/google/android/gms/icing/v;


# instance fields
.field public a:I

.field public b:[B

.field public c:[B

.field public d:Z

.field public e:J

.field public f:J

.field public g:[Lcom/google/android/gms/icing/w;


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    const/4 v1, 0x0

    .line 7655
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 7656
    iput v1, p0, Lcom/google/android/gms/icing/v;->a:I

    sget-object v0, Lcom/google/protobuf/nano/m;->h:[B

    iput-object v0, p0, Lcom/google/android/gms/icing/v;->b:[B

    sget-object v0, Lcom/google/protobuf/nano/m;->h:[B

    iput-object v0, p0, Lcom/google/android/gms/icing/v;->c:[B

    iput-boolean v1, p0, Lcom/google/android/gms/icing/v;->d:Z

    iput-wide v2, p0, Lcom/google/android/gms/icing/v;->e:J

    iput-wide v2, p0, Lcom/google/android/gms/icing/v;->f:J

    invoke-static {}, Lcom/google/android/gms/icing/w;->a()[Lcom/google/android/gms/icing/w;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/icing/v;->g:[Lcom/google/android/gms/icing/w;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/icing/v;->cachedSize:I

    .line 7657
    return-void
.end method

.method public static a()[Lcom/google/android/gms/icing/v;
    .locals 2

    .prologue
    .line 7623
    sget-object v0, Lcom/google/android/gms/icing/v;->h:[Lcom/google/android/gms/icing/v;

    if-nez v0, :cond_1

    .line 7624
    sget-object v1, Lcom/google/protobuf/nano/h;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 7626
    :try_start_0
    sget-object v0, Lcom/google/android/gms/icing/v;->h:[Lcom/google/android/gms/icing/v;

    if-nez v0, :cond_0

    .line 7627
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/android/gms/icing/v;

    sput-object v0, Lcom/google/android/gms/icing/v;->h:[Lcom/google/android/gms/icing/v;

    .line 7629
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 7631
    :cond_1
    sget-object v0, Lcom/google/android/gms/icing/v;->h:[Lcom/google/android/gms/icing/v;

    return-object v0

    .line 7629
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 7

    .prologue
    const-wide/16 v4, 0x0

    .line 7755
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->computeSerializedSize()I

    move-result v0

    .line 7756
    iget v1, p0, Lcom/google/android/gms/icing/v;->a:I

    if-eqz v1, :cond_0

    .line 7757
    const/4 v1, 0x1

    iget v2, p0, Lcom/google/android/gms/icing/v;->a:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 7760
    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/icing/v;->b:[B

    sget-object v2, Lcom/google/protobuf/nano/m;->h:[B

    invoke-static {v1, v2}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v1

    if-nez v1, :cond_1

    .line 7761
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/gms/icing/v;->b:[B

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(I[B)I

    move-result v1

    add-int/2addr v0, v1

    .line 7764
    :cond_1
    iget-object v1, p0, Lcom/google/android/gms/icing/v;->c:[B

    sget-object v2, Lcom/google/protobuf/nano/m;->h:[B

    invoke-static {v1, v2}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v1

    if-nez v1, :cond_2

    .line 7765
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/android/gms/icing/v;->c:[B

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(I[B)I

    move-result v1

    add-int/2addr v0, v1

    .line 7768
    :cond_2
    iget-boolean v1, p0, Lcom/google/android/gms/icing/v;->d:Z

    if-eqz v1, :cond_3

    .line 7769
    const/4 v1, 0x4

    iget-boolean v2, p0, Lcom/google/android/gms/icing/v;->d:Z

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 7772
    :cond_3
    iget-wide v2, p0, Lcom/google/android/gms/icing/v;->e:J

    cmp-long v1, v2, v4

    if-eqz v1, :cond_4

    .line 7773
    const/4 v1, 0x5

    iget-wide v2, p0, Lcom/google/android/gms/icing/v;->e:J

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/b;->f(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 7776
    :cond_4
    iget-wide v2, p0, Lcom/google/android/gms/icing/v;->f:J

    cmp-long v1, v2, v4

    if-eqz v1, :cond_5

    .line 7777
    const/4 v1, 0x6

    iget-wide v2, p0, Lcom/google/android/gms/icing/v;->f:J

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/b;->e(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 7780
    :cond_5
    iget-object v1, p0, Lcom/google/android/gms/icing/v;->g:[Lcom/google/android/gms/icing/w;

    if-eqz v1, :cond_8

    iget-object v1, p0, Lcom/google/android/gms/icing/v;->g:[Lcom/google/android/gms/icing/w;

    array-length v1, v1

    if-lez v1, :cond_8

    .line 7781
    const/4 v1, 0x0

    move v6, v1

    move v1, v0

    move v0, v6

    :goto_0
    iget-object v2, p0, Lcom/google/android/gms/icing/v;->g:[Lcom/google/android/gms/icing/w;

    array-length v2, v2

    if-ge v0, v2, :cond_7

    .line 7782
    iget-object v2, p0, Lcom/google/android/gms/icing/v;->g:[Lcom/google/android/gms/icing/w;

    aget-object v2, v2, v0

    .line 7783
    if-eqz v2, :cond_6

    .line 7784
    const/4 v3, 0x7

    invoke-static {v3, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v2

    add-int/2addr v1, v2

    .line 7781
    :cond_6
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_7
    move v0, v1

    .line 7789
    :cond_8
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 7673
    if-ne p1, p0, :cond_1

    .line 7702
    :cond_0
    :goto_0
    return v0

    .line 7676
    :cond_1
    instance-of v2, p1, Lcom/google/android/gms/icing/v;

    if-nez v2, :cond_2

    move v0, v1

    .line 7677
    goto :goto_0

    .line 7679
    :cond_2
    check-cast p1, Lcom/google/android/gms/icing/v;

    .line 7680
    iget v2, p0, Lcom/google/android/gms/icing/v;->a:I

    iget v3, p1, Lcom/google/android/gms/icing/v;->a:I

    if-eq v2, v3, :cond_3

    move v0, v1

    .line 7681
    goto :goto_0

    .line 7683
    :cond_3
    iget-object v2, p0, Lcom/google/android/gms/icing/v;->b:[B

    iget-object v3, p1, Lcom/google/android/gms/icing/v;->b:[B

    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v2

    if-nez v2, :cond_4

    move v0, v1

    .line 7684
    goto :goto_0

    .line 7686
    :cond_4
    iget-object v2, p0, Lcom/google/android/gms/icing/v;->c:[B

    iget-object v3, p1, Lcom/google/android/gms/icing/v;->c:[B

    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v2

    if-nez v2, :cond_5

    move v0, v1

    .line 7687
    goto :goto_0

    .line 7689
    :cond_5
    iget-boolean v2, p0, Lcom/google/android/gms/icing/v;->d:Z

    iget-boolean v3, p1, Lcom/google/android/gms/icing/v;->d:Z

    if-eq v2, v3, :cond_6

    move v0, v1

    .line 7690
    goto :goto_0

    .line 7692
    :cond_6
    iget-wide v2, p0, Lcom/google/android/gms/icing/v;->e:J

    iget-wide v4, p1, Lcom/google/android/gms/icing/v;->e:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_7

    move v0, v1

    .line 7693
    goto :goto_0

    .line 7695
    :cond_7
    iget-wide v2, p0, Lcom/google/android/gms/icing/v;->f:J

    iget-wide v4, p1, Lcom/google/android/gms/icing/v;->f:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_8

    move v0, v1

    .line 7696
    goto :goto_0

    .line 7698
    :cond_8
    iget-object v2, p0, Lcom/google/android/gms/icing/v;->g:[Lcom/google/android/gms/icing/w;

    iget-object v3, p1, Lcom/google/android/gms/icing/v;->g:[Lcom/google/android/gms/icing/w;

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    .line 7700
    goto :goto_0
.end method

.method public final hashCode()I
    .locals 7

    .prologue
    const/16 v6, 0x20

    .line 7707
    iget v0, p0, Lcom/google/android/gms/icing/v;->a:I

    add-int/lit16 v0, v0, 0x20f

    .line 7709
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/google/android/gms/icing/v;->b:[B

    invoke-static {v1}, Ljava/util/Arrays;->hashCode([B)I

    move-result v1

    add-int/2addr v0, v1

    .line 7710
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/google/android/gms/icing/v;->c:[B

    invoke-static {v1}, Ljava/util/Arrays;->hashCode([B)I

    move-result v1

    add-int/2addr v0, v1

    .line 7711
    mul-int/lit8 v1, v0, 0x1f

    iget-boolean v0, p0, Lcom/google/android/gms/icing/v;->d:Z

    if-eqz v0, :cond_0

    const/16 v0, 0x4cf

    :goto_0
    add-int/2addr v0, v1

    .line 7712
    mul-int/lit8 v0, v0, 0x1f

    iget-wide v2, p0, Lcom/google/android/gms/icing/v;->e:J

    iget-wide v4, p0, Lcom/google/android/gms/icing/v;->e:J

    ushr-long/2addr v4, v6

    xor-long/2addr v2, v4

    long-to-int v1, v2

    add-int/2addr v0, v1

    .line 7714
    mul-int/lit8 v0, v0, 0x1f

    iget-wide v2, p0, Lcom/google/android/gms/icing/v;->f:J

    iget-wide v4, p0, Lcom/google/android/gms/icing/v;->f:J

    ushr-long/2addr v4, v6

    xor-long/2addr v2, v4

    long-to-int v1, v2

    add-int/2addr v0, v1

    .line 7716
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/google/android/gms/icing/v;->g:[Lcom/google/android/gms/icing/w;

    invoke-static {v1}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 7718
    return v0

    .line 7711
    :cond_0
    const/16 v0, 0x4d5

    goto :goto_0
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 7479
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/icing/v;->a:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->f()[B

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/icing/v;->b:[B

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->f()[B

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/icing/v;->c:[B

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/icing/v;->d:Z

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->j()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/android/gms/icing/v;->e:J

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->j()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/android/gms/icing/v;->f:J

    goto :goto_0

    :sswitch_7
    const/16 v0, 0x3a

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->b(Lcom/google/protobuf/nano/a;I)I

    move-result v2

    iget-object v0, p0, Lcom/google/android/gms/icing/v;->g:[Lcom/google/android/gms/icing/w;

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lcom/google/android/gms/icing/w;

    if-eqz v0, :cond_1

    iget-object v3, p0, Lcom/google/android/gms/icing/v;->g:[Lcom/google/android/gms/icing/w;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_1
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_3

    new-instance v3, Lcom/google/android/gms/icing/w;

    invoke-direct {v3}, Lcom/google/android/gms/icing/w;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/icing/v;->g:[Lcom/google/android/gms/icing/w;

    array-length v0, v0

    goto :goto_1

    :cond_3
    new-instance v3, Lcom/google/android/gms/icing/w;

    invoke-direct {v3}, Lcom/google/android/gms/icing/w;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    iput-object v2, p0, Lcom/google/android/gms/icing/v;->g:[Lcom/google/android/gms/icing/w;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
        0x30 -> :sswitch_6
        0x3a -> :sswitch_7
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    .line 7724
    iget v0, p0, Lcom/google/android/gms/icing/v;->a:I

    if-eqz v0, :cond_0

    .line 7725
    const/4 v0, 0x1

    iget v1, p0, Lcom/google/android/gms/icing/v;->a:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 7727
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/icing/v;->b:[B

    sget-object v1, Lcom/google/protobuf/nano/m;->h:[B

    invoke-static {v0, v1}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v0

    if-nez v0, :cond_1

    .line 7728
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/android/gms/icing/v;->b:[B

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(I[B)V

    .line 7730
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/icing/v;->c:[B

    sget-object v1, Lcom/google/protobuf/nano/m;->h:[B

    invoke-static {v0, v1}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v0

    if-nez v0, :cond_2

    .line 7731
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/android/gms/icing/v;->c:[B

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(I[B)V

    .line 7733
    :cond_2
    iget-boolean v0, p0, Lcom/google/android/gms/icing/v;->d:Z

    if-eqz v0, :cond_3

    .line 7734
    const/4 v0, 0x4

    iget-boolean v1, p0, Lcom/google/android/gms/icing/v;->d:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 7736
    :cond_3
    iget-wide v0, p0, Lcom/google/android/gms/icing/v;->e:J

    cmp-long v0, v0, v4

    if-eqz v0, :cond_4

    .line 7737
    const/4 v0, 0x5

    iget-wide v2, p0, Lcom/google/android/gms/icing/v;->e:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/b;->b(IJ)V

    .line 7739
    :cond_4
    iget-wide v0, p0, Lcom/google/android/gms/icing/v;->f:J

    cmp-long v0, v0, v4

    if-eqz v0, :cond_5

    .line 7740
    const/4 v0, 0x6

    iget-wide v2, p0, Lcom/google/android/gms/icing/v;->f:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/b;->a(IJ)V

    .line 7742
    :cond_5
    iget-object v0, p0, Lcom/google/android/gms/icing/v;->g:[Lcom/google/android/gms/icing/w;

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/google/android/gms/icing/v;->g:[Lcom/google/android/gms/icing/w;

    array-length v0, v0

    if-lez v0, :cond_7

    .line 7743
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/google/android/gms/icing/v;->g:[Lcom/google/android/gms/icing/w;

    array-length v1, v1

    if-ge v0, v1, :cond_7

    .line 7744
    iget-object v1, p0, Lcom/google/android/gms/icing/v;->g:[Lcom/google/android/gms/icing/w;

    aget-object v1, v1, v0

    .line 7745
    if-eqz v1, :cond_6

    .line 7746
    const/4 v2, 0x7

    invoke-virtual {p1, v2, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 7743
    :cond_6
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 7750
    :cond_7
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 7751
    return-void
.end method
