.class public final Lcom/google/android/gms/icing/bj;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# static fields
.field private static volatile f:[Lcom/google/android/gms/icing/bj;


# instance fields
.field public a:I

.field public b:I

.field public c:I

.field public d:J

.field public e:J


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    const/4 v0, 0x0

    .line 6706
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 6707
    iput v0, p0, Lcom/google/android/gms/icing/bj;->a:I

    iput v0, p0, Lcom/google/android/gms/icing/bj;->b:I

    iput v0, p0, Lcom/google/android/gms/icing/bj;->c:I

    iput-wide v2, p0, Lcom/google/android/gms/icing/bj;->d:J

    iput-wide v2, p0, Lcom/google/android/gms/icing/bj;->e:J

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/icing/bj;->cachedSize:I

    .line 6708
    return-void
.end method

.method public static a()[Lcom/google/android/gms/icing/bj;
    .locals 2

    .prologue
    .line 6680
    sget-object v0, Lcom/google/android/gms/icing/bj;->f:[Lcom/google/android/gms/icing/bj;

    if-nez v0, :cond_1

    .line 6681
    sget-object v1, Lcom/google/protobuf/nano/h;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 6683
    :try_start_0
    sget-object v0, Lcom/google/android/gms/icing/bj;->f:[Lcom/google/android/gms/icing/bj;

    if-nez v0, :cond_0

    .line 6684
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/android/gms/icing/bj;

    sput-object v0, Lcom/google/android/gms/icing/bj;->f:[Lcom/google/android/gms/icing/bj;

    .line 6686
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 6688
    :cond_1
    sget-object v0, Lcom/google/android/gms/icing/bj;->f:[Lcom/google/android/gms/icing/bj;

    return-object v0

    .line 6686
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    .line 6783
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->computeSerializedSize()I

    move-result v0

    .line 6784
    iget v1, p0, Lcom/google/android/gms/icing/bj;->a:I

    if-eqz v1, :cond_0

    .line 6785
    const/4 v1, 0x1

    iget v2, p0, Lcom/google/android/gms/icing/bj;->a:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 6788
    :cond_0
    iget v1, p0, Lcom/google/android/gms/icing/bj;->b:I

    if-eqz v1, :cond_1

    .line 6789
    const/4 v1, 0x3

    iget v2, p0, Lcom/google/android/gms/icing/bj;->b:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->g(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 6792
    :cond_1
    iget v1, p0, Lcom/google/android/gms/icing/bj;->c:I

    if-eqz v1, :cond_2

    .line 6793
    const/4 v1, 0x4

    iget v2, p0, Lcom/google/android/gms/icing/bj;->c:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->g(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 6796
    :cond_2
    iget-wide v2, p0, Lcom/google/android/gms/icing/bj;->d:J

    cmp-long v1, v2, v4

    if-eqz v1, :cond_3

    .line 6797
    const/4 v1, 0x5

    iget-wide v2, p0, Lcom/google/android/gms/icing/bj;->d:J

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/b;->e(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 6800
    :cond_3
    iget-wide v2, p0, Lcom/google/android/gms/icing/bj;->e:J

    cmp-long v1, v2, v4

    if-eqz v1, :cond_4

    .line 6801
    const/4 v1, 0x6

    iget-wide v2, p0, Lcom/google/android/gms/icing/bj;->e:J

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/b;->e(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 6804
    :cond_4
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 6722
    if-ne p1, p0, :cond_1

    .line 6744
    :cond_0
    :goto_0
    return v0

    .line 6725
    :cond_1
    instance-of v2, p1, Lcom/google/android/gms/icing/bj;

    if-nez v2, :cond_2

    move v0, v1

    .line 6726
    goto :goto_0

    .line 6728
    :cond_2
    check-cast p1, Lcom/google/android/gms/icing/bj;

    .line 6729
    iget v2, p0, Lcom/google/android/gms/icing/bj;->a:I

    iget v3, p1, Lcom/google/android/gms/icing/bj;->a:I

    if-eq v2, v3, :cond_3

    move v0, v1

    .line 6730
    goto :goto_0

    .line 6732
    :cond_3
    iget v2, p0, Lcom/google/android/gms/icing/bj;->b:I

    iget v3, p1, Lcom/google/android/gms/icing/bj;->b:I

    if-eq v2, v3, :cond_4

    move v0, v1

    .line 6733
    goto :goto_0

    .line 6735
    :cond_4
    iget v2, p0, Lcom/google/android/gms/icing/bj;->c:I

    iget v3, p1, Lcom/google/android/gms/icing/bj;->c:I

    if-eq v2, v3, :cond_5

    move v0, v1

    .line 6736
    goto :goto_0

    .line 6738
    :cond_5
    iget-wide v2, p0, Lcom/google/android/gms/icing/bj;->d:J

    iget-wide v4, p1, Lcom/google/android/gms/icing/bj;->d:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_6

    move v0, v1

    .line 6739
    goto :goto_0

    .line 6741
    :cond_6
    iget-wide v2, p0, Lcom/google/android/gms/icing/bj;->e:J

    iget-wide v4, p1, Lcom/google/android/gms/icing/bj;->e:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_0

    move v0, v1

    .line 6742
    goto :goto_0
.end method

.method public final hashCode()I
    .locals 7

    .prologue
    const/16 v6, 0x20

    .line 6749
    iget v0, p0, Lcom/google/android/gms/icing/bj;->a:I

    add-int/lit16 v0, v0, 0x20f

    .line 6751
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/google/android/gms/icing/bj;->b:I

    add-int/2addr v0, v1

    .line 6752
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/google/android/gms/icing/bj;->c:I

    add-int/2addr v0, v1

    .line 6753
    mul-int/lit8 v0, v0, 0x1f

    iget-wide v2, p0, Lcom/google/android/gms/icing/bj;->d:J

    iget-wide v4, p0, Lcom/google/android/gms/icing/bj;->d:J

    ushr-long/2addr v4, v6

    xor-long/2addr v2, v4

    long-to-int v1, v2

    add-int/2addr v0, v1

    .line 6755
    mul-int/lit8 v0, v0, 0x1f

    iget-wide v2, p0, Lcom/google/android/gms/icing/bj;->e:J

    iget-wide v4, p0, Lcom/google/android/gms/icing/bj;->e:J

    ushr-long/2addr v4, v6

    xor-long/2addr v2, v4

    long-to-int v1, v2

    add-int/2addr v0, v1

    .line 6757
    return v0
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 2

    .prologue
    .line 6674
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/icing/bj;->a:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/icing/bj;->b:I

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/icing/bj;->c:I

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->j()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/gms/icing/bj;->d:J

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->j()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/gms/icing/bj;->e:J

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x18 -> :sswitch_2
        0x20 -> :sswitch_3
        0x28 -> :sswitch_4
        0x30 -> :sswitch_5
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    .line 6763
    iget v0, p0, Lcom/google/android/gms/icing/bj;->a:I

    if-eqz v0, :cond_0

    .line 6764
    const/4 v0, 0x1

    iget v1, p0, Lcom/google/android/gms/icing/bj;->a:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 6766
    :cond_0
    iget v0, p0, Lcom/google/android/gms/icing/bj;->b:I

    if-eqz v0, :cond_1

    .line 6767
    const/4 v0, 0x3

    iget v1, p0, Lcom/google/android/gms/icing/bj;->b:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->c(II)V

    .line 6769
    :cond_1
    iget v0, p0, Lcom/google/android/gms/icing/bj;->c:I

    if-eqz v0, :cond_2

    .line 6770
    const/4 v0, 0x4

    iget v1, p0, Lcom/google/android/gms/icing/bj;->c:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->c(II)V

    .line 6772
    :cond_2
    iget-wide v0, p0, Lcom/google/android/gms/icing/bj;->d:J

    cmp-long v0, v0, v4

    if-eqz v0, :cond_3

    .line 6773
    const/4 v0, 0x5

    iget-wide v2, p0, Lcom/google/android/gms/icing/bj;->d:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/b;->a(IJ)V

    .line 6775
    :cond_3
    iget-wide v0, p0, Lcom/google/android/gms/icing/bj;->e:J

    cmp-long v0, v0, v4

    if-eqz v0, :cond_4

    .line 6776
    const/4 v0, 0x6

    iget-wide v2, p0, Lcom/google/android/gms/icing/bj;->e:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/b;->a(IJ)V

    .line 6778
    :cond_4
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 6779
    return-void
.end method
