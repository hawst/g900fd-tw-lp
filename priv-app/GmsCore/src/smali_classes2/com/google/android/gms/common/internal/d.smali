.class public final Lcom/google/android/gms/common/internal/d;
.super Lcom/google/android/gms/common/internal/bb;
.source "SourceFile"


# instance fields
.field private a:Landroid/accounts/Account;


# direct methods
.method private constructor <init>(Landroid/accounts/Account;)V
    .locals 0

    .prologue
    .line 39
    invoke-direct {p0}, Lcom/google/android/gms/common/internal/bb;-><init>()V

    .line 40
    iput-object p1, p0, Lcom/google/android/gms/common/internal/d;->a:Landroid/accounts/Account;

    .line 41
    return-void
.end method

.method public static a(Ljava/lang/String;)Lcom/google/android/gms/common/internal/d;
    .locals 2

    .prologue
    .line 34
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    .line 36
    :goto_0
    new-instance v1, Lcom/google/android/gms/common/internal/d;

    invoke-direct {v1, v0}, Lcom/google/android/gms/common/internal/d;-><init>(Landroid/accounts/Account;)V

    return-object v1

    .line 34
    :cond_0
    new-instance v0, Landroid/accounts/Account;

    const-string v1, "com.google"

    invoke-direct {v0, p0, v1}, Landroid/accounts/Account;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method public final a()Landroid/accounts/Account;
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lcom/google/android/gms/common/internal/d;->a:Landroid/accounts/Account;

    return-object v0
.end method
