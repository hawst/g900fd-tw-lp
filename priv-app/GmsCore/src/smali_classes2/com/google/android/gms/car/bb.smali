.class final Lcom/google/android/gms/car/bb;
.super Lcom/google/android/gms/car/pf;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/gms/car/aw;

.field private c:Z


# direct methods
.method private constructor <init>(Lcom/google/android/gms/car/aw;Landroid/os/Looper;)V
    .locals 1

    .prologue
    .line 1076
    iput-object p1, p0, Lcom/google/android/gms/car/bb;->a:Lcom/google/android/gms/car/aw;

    .line 1077
    const/4 v0, 0x0

    invoke-direct {p0, v0, p2}, Lcom/google/android/gms/car/pf;-><init>(Ljava/lang/Object;Landroid/os/Looper;)V

    .line 1074
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/car/bb;->c:Z

    .line 1078
    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/gms/car/aw;Landroid/os/Looper;B)V
    .locals 0

    .prologue
    .line 1063
    invoke-direct {p0, p1, p2}, Lcom/google/android/gms/car/bb;-><init>(Lcom/google/android/gms/car/aw;Landroid/os/Looper;)V

    return-void
.end method

.method private declared-synchronized a()V
    .locals 2

    .prologue
    .line 1117
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/gms/car/bb;->c:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    .line 1124
    :goto_0
    monitor-exit p0

    return-void

    .line 1120
    :cond_0
    :try_start_1
    const-string v0, "CAR.AUDIO"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1121
    const-string v0, "CAR.AUDIO"

    const-string v1, "requestToReleaseFocusToCar"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1123
    :cond_1
    const/4 v0, 0x3

    invoke-virtual {p0, v0}, Lcom/google/android/gms/car/bb;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/car/bb;->sendMessage(Landroid/os/Message;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 1117
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized a(I)V
    .locals 2

    .prologue
    .line 1081
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/gms/car/bb;->c:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    .line 1088
    :goto_0
    monitor-exit p0

    return-void

    .line 1084
    :cond_0
    :try_start_1
    const-string v0, "CAR.AUDIO"

    const/4 v1, 0x2

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1085
    const-string v0, "CAR.AUDIO"

    const-string v1, "handleAndroidFocusChangeTop"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1087
    :cond_1
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-virtual {p0, v0, p1, v1}, Lcom/google/android/gms/car/bb;->obtainMessage(III)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/car/bb;->sendMessage(Landroid/os/Message;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 1081
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized a(IZ)V
    .locals 2

    .prologue
    .line 1107
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/gms/car/bb;->c:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    .line 1114
    :goto_0
    monitor-exit p0

    return-void

    .line 1110
    :cond_0
    :try_start_1
    const-string v0, "CAR.AUDIO"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1111
    const-string v0, "CAR.AUDIO"

    const-string v1, "handleCarFocusChange"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1113
    :cond_1
    const/4 v1, 0x2

    if-eqz p2, :cond_2

    const/4 v0, 0x1

    :goto_1
    invoke-virtual {p0, v1, p1, v0}, Lcom/google/android/gms/car/bb;->obtainMessage(III)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/car/bb;->sendMessage(Landroid/os/Message;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 1107
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 1113
    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private declared-synchronized a(J)V
    .locals 4

    .prologue
    .line 1153
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/gms/car/bb;->c:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    .line 1157
    :goto_0
    monitor-exit p0

    return-void

    .line 1156
    :cond_0
    const/4 v0, 0x6

    :try_start_1
    invoke-virtual {p0, v0}, Lcom/google/android/gms/car/bb;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    const-wide/16 v2, 0xc8

    invoke-virtual {p0, v0, v2, v3}, Lcom/google/android/gms/car/bb;->sendMessageDelayed(Landroid/os/Message;J)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 1153
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method static synthetic a(Lcom/google/android/gms/car/bb;)V
    .locals 0

    .prologue
    .line 1063
    invoke-direct {p0}, Lcom/google/android/gms/car/bb;->e()V

    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/car/bb;I)V
    .locals 0

    .prologue
    .line 1063
    invoke-direct {p0, p1}, Lcom/google/android/gms/car/bb;->a(I)V

    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/car/bb;IZ)V
    .locals 0

    .prologue
    .line 1063
    invoke-direct {p0, p1, p2}, Lcom/google/android/gms/car/bb;->a(IZ)V

    return-void
.end method

.method private declared-synchronized b()V
    .locals 1

    .prologue
    .line 1127
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/gms/car/bb;->c:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    .line 1131
    :goto_0
    monitor-exit p0

    return-void

    .line 1130
    :cond_0
    const/4 v0, 0x4

    :try_start_1
    invoke-virtual {p0, v0}, Lcom/google/android/gms/car/bb;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/car/bb;->sendMessage(Landroid/os/Message;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 1127
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized b(I)V
    .locals 2

    .prologue
    .line 1091
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/gms/car/bb;->c:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    .line 1098
    :goto_0
    monitor-exit p0

    return-void

    .line 1094
    :cond_0
    :try_start_1
    const-string v0, "CAR.AUDIO"

    const/4 v1, 0x2

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1095
    const-string v0, "CAR.AUDIO"

    const-string v1, "handleAndroidFocusChangeBottom"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1097
    :cond_1
    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-virtual {p0, v0, p1, v1}, Lcom/google/android/gms/car/bb;->obtainMessage(III)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/car/bb;->sendMessage(Landroid/os/Message;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 1091
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method static synthetic b(Lcom/google/android/gms/car/bb;)V
    .locals 0

    .prologue
    .line 1063
    invoke-direct {p0}, Lcom/google/android/gms/car/bb;->d()V

    return-void
.end method

.method static synthetic b(Lcom/google/android/gms/car/bb;I)V
    .locals 0

    .prologue
    .line 1063
    invoke-direct {p0, p1}, Lcom/google/android/gms/car/bb;->b(I)V

    return-void
.end method

.method private declared-synchronized c(I)V
    .locals 4

    .prologue
    .line 1138
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/gms/car/bb;->c:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    .line 1143
    :goto_0
    monitor-exit p0

    return-void

    .line 1141
    :cond_0
    const/4 v0, 0x5

    const/4 v1, 0x0

    :try_start_1
    invoke-virtual {p0, v0, p1, v1}, Lcom/google/android/gms/car/bb;->obtainMessage(III)Landroid/os/Message;

    move-result-object v0

    const-wide/16 v2, 0x1f4

    invoke-virtual {p0, v0, v2, v3}, Lcom/google/android/gms/car/bb;->sendMessageDelayed(Landroid/os/Message;J)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 1138
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method static synthetic c(Lcom/google/android/gms/car/bb;)V
    .locals 0

    .prologue
    .line 1063
    invoke-direct {p0}, Lcom/google/android/gms/car/bb;->b()V

    return-void
.end method

.method static synthetic c(Lcom/google/android/gms/car/bb;I)V
    .locals 0

    .prologue
    .line 1063
    invoke-direct {p0, p1}, Lcom/google/android/gms/car/bb;->c(I)V

    return-void
.end method

.method private declared-synchronized c()Z
    .locals 1

    .prologue
    .line 1134
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/gms/car/bb;->c:Z

    if-nez v0, :cond_0

    const/4 v0, 0x5

    invoke-virtual {p0, v0}, Lcom/google/android/gms/car/bb;->hasMessages(I)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized d()V
    .locals 1

    .prologue
    .line 1146
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/gms/car/bb;->c:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    .line 1150
    :goto_0
    monitor-exit p0

    return-void

    .line 1149
    :cond_0
    const/4 v0, 0x5

    :try_start_1
    invoke-virtual {p0, v0}, Lcom/google/android/gms/car/bb;->removeMessages(I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 1146
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method static synthetic d(Lcom/google/android/gms/car/bb;)V
    .locals 0

    .prologue
    .line 1063
    invoke-direct {p0}, Lcom/google/android/gms/car/bb;->f()V

    return-void
.end method

.method private declared-synchronized e()V
    .locals 1

    .prologue
    .line 1160
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/gms/car/bb;->c:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    .line 1165
    :goto_0
    monitor-exit p0

    return-void

    .line 1163
    :cond_0
    const/4 v0, 0x6

    :try_start_1
    invoke-virtual {p0, v0}, Lcom/google/android/gms/car/bb;->removeMessages(I)V

    .line 1164
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lcom/google/android/gms/car/bb;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/car/bb;->sendMessage(Landroid/os/Message;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 1160
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method static synthetic e(Lcom/google/android/gms/car/bb;)V
    .locals 2

    .prologue
    .line 1063
    const-wide/16 v0, 0xc8

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/car/bb;->a(J)V

    return-void
.end method

.method private declared-synchronized f()V
    .locals 1

    .prologue
    .line 1168
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/gms/car/bb;->c:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    .line 1172
    :goto_0
    monitor-exit p0

    return-void

    .line 1171
    :cond_0
    const/4 v0, 0x7

    :try_start_1
    invoke-virtual {p0, v0}, Lcom/google/android/gms/car/bb;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/car/bb;->sendMessage(Landroid/os/Message;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 1168
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method static synthetic f(Lcom/google/android/gms/car/bb;)V
    .locals 0

    .prologue
    .line 1063
    invoke-direct {p0}, Lcom/google/android/gms/car/bb;->g()V

    return-void
.end method

.method private declared-synchronized g()V
    .locals 1

    .prologue
    .line 1175
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/gms/car/bb;->c:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    .line 1179
    :goto_0
    monitor-exit p0

    return-void

    .line 1178
    :cond_0
    const/4 v0, 0x1

    :try_start_1
    iput-boolean v0, p0, Lcom/google/android/gms/car/bb;->c:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 1175
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method static synthetic g(Lcom/google/android/gms/car/bb;)V
    .locals 0

    .prologue
    .line 1063
    invoke-direct {p0}, Lcom/google/android/gms/car/bb;->a()V

    return-void
.end method

.method static synthetic h(Lcom/google/android/gms/car/bb;)Z
    .locals 1

    .prologue
    .line 1063
    invoke-direct {p0}, Lcom/google/android/gms/car/bb;->c()Z

    move-result v0

    return v0
.end method


# virtual methods
.method public final handleMessage(Landroid/os/Message;)V
    .locals 7

    .prologue
    const/4 v3, 0x4

    const/4 v6, 0x3

    const/4 v0, 0x1

    .line 1183
    iget v1, p1, Landroid/os/Message;->what:I

    packed-switch v1, :pswitch_data_0

    .line 1210
    :cond_0
    :goto_0
    return-void

    .line 1185
    :pswitch_0
    iget-object v1, p0, Lcom/google/android/gms/car/bb;->a:Lcom/google/android/gms/car/aw;

    iget v2, p1, Landroid/os/Message;->arg1:I

    iget v3, v1, Lcom/google/android/gms/car/aw;->d:I

    if-eq v3, v2, :cond_0

    packed-switch v2, :pswitch_data_1

    :pswitch_1
    const-string v0, "CAR.AUDIO"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "unknown android focus change "

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :pswitch_2
    iget-object v0, v1, Lcom/google/android/gms/car/aw;->f:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v0

    iget-object v3, v1, Lcom/google/android/gms/car/aw;->n:Ljava/lang/Runnable;

    invoke-static {v0, v3}, Lcom/google/android/gms/car/of;->a(Landroid/os/Looper;Ljava/lang/Runnable;)V

    :goto_1
    iput v2, v1, Lcom/google/android/gms/car/aw;->d:I

    const-string v0, "CAR.AUDIO"

    invoke-static {v0, v6}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v3, "android audio focus change "

    invoke-direct {v0, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/google/android/gms/car/aw;->a(Ljava/lang/String;)V

    goto :goto_0

    :pswitch_3
    invoke-virtual {v1, v0}, Lcom/google/android/gms/car/aw;->g(I)V

    goto :goto_1

    :pswitch_4
    const/4 v0, 0x2

    invoke-virtual {v1, v0}, Lcom/google/android/gms/car/aw;->g(I)V

    goto :goto_1

    :pswitch_5
    invoke-virtual {v1, v6}, Lcom/google/android/gms/car/aw;->g(I)V

    goto :goto_1

    .line 1188
    :pswitch_6
    iget-object v0, p0, Lcom/google/android/gms/car/bb;->a:Lcom/google/android/gms/car/aw;

    iget v1, p1, Landroid/os/Message;->arg1:I

    invoke-virtual {v0, v1}, Lcom/google/android/gms/car/aw;->f(I)V

    goto :goto_0

    .line 1191
    :pswitch_7
    iget-object v1, p0, Lcom/google/android/gms/car/bb;->a:Lcom/google/android/gms/car/aw;

    iget v2, p1, Landroid/os/Message;->arg1:I

    iget v3, p1, Landroid/os/Message;->arg2:I

    if-ne v3, v0, :cond_1

    :goto_2
    invoke-virtual {v1, v2, v0}, Lcom/google/android/gms/car/aw;->a(IZ)V

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_2

    .line 1194
    :pswitch_8
    iget-object v1, p0, Lcom/google/android/gms/car/bb;->a:Lcom/google/android/gms/car/aw;

    iget v2, v1, Lcom/google/android/gms/car/aw;->d:I

    if-ne v2, v0, :cond_0

    invoke-virtual {v1, v3}, Lcom/google/android/gms/car/aw;->g(I)V

    goto :goto_0

    .line 1197
    :pswitch_9
    iget-object v0, p0, Lcom/google/android/gms/car/bb;->a:Lcom/google/android/gms/car/aw;

    invoke-static {v0}, Lcom/google/android/gms/car/aw;->a(Lcom/google/android/gms/car/aw;)V

    goto :goto_0

    .line 1200
    :pswitch_a
    iget-object v1, p0, Lcom/google/android/gms/car/bb;->a:Lcom/google/android/gms/car/aw;

    iget v2, p1, Landroid/os/Message;->arg1:I

    const-string v3, "CAR.AUDIO"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Car focus response time-out for focus request "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v1}, Lcom/google/android/gms/car/aw;->e()Z

    move-result v2

    if-nez v2, :cond_0

    iget v2, v1, Lcom/google/android/gms/car/aw;->l:I

    add-int/lit8 v2, v2, 0x1

    iput v2, v1, Lcom/google/android/gms/car/aw;->l:I

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    iput-wide v2, v1, Lcom/google/android/gms/car/aw;->m:J

    iput v6, v1, Lcom/google/android/gms/car/aw;->c:I

    iget v2, v1, Lcom/google/android/gms/car/aw;->c:I

    invoke-virtual {v1, v2, v0}, Lcom/google/android/gms/car/aw;->a(IZ)V

    goto/16 :goto_0

    .line 1203
    :pswitch_b
    iget-object v0, p0, Lcom/google/android/gms/car/bb;->a:Lcom/google/android/gms/car/aw;

    iget v1, v0, Lcom/google/android/gms/car/aw;->j:I

    if-lez v1, :cond_3

    const-string v1, "CAR.AUDIO"

    invoke-static {v1, v3}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_2

    const-string v1, "CAR.AUDIO"

    const-string v2, "regaining bottom android focus after initial failure"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    invoke-virtual {v0}, Lcom/google/android/gms/car/aw;->c()V

    goto/16 :goto_0

    :cond_3
    iget v1, v0, Lcom/google/android/gms/car/aw;->k:I

    if-lez v1, :cond_0

    const-string v1, "CAR.AUDIO"

    invoke-static {v1, v3}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_4

    const-string v1, "CAR.AUDIO"

    const-string v2, "regaining top android focus after failure"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_4
    invoke-virtual {v0}, Lcom/google/android/gms/car/aw;->d()V

    goto/16 :goto_0

    .line 1206
    :pswitch_c
    iget-object v0, p0, Lcom/google/android/gms/car/bb;->a:Lcom/google/android/gms/car/aw;

    invoke-virtual {v0}, Lcom/google/android/gms/car/aw;->c()V

    goto/16 :goto_0

    .line 1209
    :pswitch_d
    iget-object v1, p0, Lcom/google/android/gms/car/bb;->a:Lcom/google/android/gms/car/aw;

    iget v2, v1, Lcom/google/android/gms/car/aw;->j:I

    if-lez v2, :cond_5

    const-string v0, "CAR.AUDIO"

    const-string v2, "After call, bottom focus lister not registered. Re-register now"

    invoke-static {v0, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v1}, Lcom/google/android/gms/car/aw;->c()V

    goto/16 :goto_0

    :cond_5
    iget v2, v1, Lcom/google/android/gms/car/aw;->c:I

    invoke-virtual {v1, v2, v0}, Lcom/google/android/gms/car/aw;->a(IZ)V

    goto/16 :goto_0

    .line 1183
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
    .end packed-switch

    .line 1185
    :pswitch_data_1
    .packed-switch -0x3
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
