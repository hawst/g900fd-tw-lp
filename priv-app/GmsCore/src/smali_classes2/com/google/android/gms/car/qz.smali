.class Lcom/google/android/gms/car/qz;
.super Lcom/google/android/gms/car/qp;
.source "SourceFile"


# instance fields
.field private final l:Lcom/google/android/gms/car/au;

.field private final m:J

.field private final n:I

.field private final o:I

.field private final p:I

.field private final q:I


# direct methods
.method public constructor <init>(Lcom/google/android/gms/car/au;I)V
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 577
    const-wide/16 v2, 0xe1

    const/4 v0, 0x1

    invoke-direct {p0, v2, v3, v0}, Lcom/google/android/gms/car/qp;-><init>(JZ)V

    .line 578
    iput-object p1, p0, Lcom/google/android/gms/car/qz;->l:Lcom/google/android/gms/car/au;

    .line 579
    const-wide/16 v2, 0x4b

    iput-wide v2, p0, Lcom/google/android/gms/car/qz;->m:J

    .line 580
    iput v1, p0, Lcom/google/android/gms/car/qz;->n:I

    .line 581
    iput v1, p0, Lcom/google/android/gms/car/qz;->o:I

    .line 582
    iput v1, p0, Lcom/google/android/gms/car/qz;->p:I

    .line 583
    iput p2, p0, Lcom/google/android/gms/car/qz;->q:I

    .line 584
    iget v0, p0, Lcom/google/android/gms/car/qz;->n:I

    if-nez v0, :cond_0

    iget v0, p0, Lcom/google/android/gms/car/qz;->o:I

    if-eqz v0, :cond_1

    .line 585
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/car/qz;->k:Lcom/google/android/gms/car/qq;

    iget-object v0, v0, Lcom/google/android/gms/car/qq;->a:[F

    sget-object v2, Lcom/google/android/gms/car/qz;->f:[F

    iget v3, p0, Lcom/google/android/gms/car/qz;->n:I

    int-to-float v4, v3

    iget v3, p0, Lcom/google/android/gms/car/qz;->o:I

    int-to-float v5, v3

    const/4 v6, 0x0

    move v3, v1

    invoke-static/range {v0 .. v6}, Landroid/opengl/Matrix;->translateM([FI[FIFFF)V

    .line 590
    :goto_0
    return-void

    .line 588
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/car/qz;->k:Lcom/google/android/gms/car/qq;

    iget-object v0, v0, Lcom/google/android/gms/car/qq;->a:[F

    invoke-static {v0, v1}, Landroid/opengl/Matrix;->setIdentityM([FI)V

    goto :goto_0
.end method


# virtual methods
.method protected final a(ZJ)Lcom/google/android/gms/car/qq;
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 595
    iget-object v0, p0, Lcom/google/android/gms/car/qz;->k:Lcom/google/android/gms/car/qq;

    iput-wide p2, v0, Lcom/google/android/gms/car/qq;->c:J

    .line 596
    iget-wide v2, p0, Lcom/google/android/gms/car/qz;->m:J

    cmp-long v0, p2, v2

    if-lez v0, :cond_0

    .line 597
    iget-object v0, p0, Lcom/google/android/gms/car/qz;->l:Lcom/google/android/gms/car/au;

    long-to-float v2, p2

    iget-wide v4, p0, Lcom/google/android/gms/car/qz;->m:J

    long-to-float v3, v4

    iget-wide v4, p0, Lcom/google/android/gms/car/qz;->g:J

    long-to-float v4, v4

    invoke-interface {v0, v2, v3, v4}, Lcom/google/android/gms/car/au;->a(FFF)F

    move-result v0

    .line 599
    iget v2, p0, Lcom/google/android/gms/car/qz;->n:I

    int-to-float v2, v2

    iget v3, p0, Lcom/google/android/gms/car/qz;->p:I

    iget v4, p0, Lcom/google/android/gms/car/qz;->n:I

    sub-int/2addr v3, v4

    int-to-float v3, v3

    mul-float/2addr v3, v0

    add-float v4, v2, v3

    .line 600
    iget v2, p0, Lcom/google/android/gms/car/qz;->o:I

    int-to-float v2, v2

    iget v3, p0, Lcom/google/android/gms/car/qz;->q:I

    iget v5, p0, Lcom/google/android/gms/car/qz;->o:I

    sub-int/2addr v3, v5

    int-to-float v3, v3

    mul-float/2addr v0, v3

    add-float v5, v2, v0

    .line 601
    iget-object v0, p0, Lcom/google/android/gms/car/qz;->k:Lcom/google/android/gms/car/qq;

    iget-object v0, v0, Lcom/google/android/gms/car/qq;->a:[F

    sget-object v2, Lcom/google/android/gms/car/qz;->f:[F

    const/4 v6, 0x0

    move v3, v1

    invoke-static/range {v0 .. v6}, Landroid/opengl/Matrix;->translateM([FI[FIFFF)V

    .line 604
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/car/qz;->k:Lcom/google/android/gms/car/qq;

    return-object v0
.end method
