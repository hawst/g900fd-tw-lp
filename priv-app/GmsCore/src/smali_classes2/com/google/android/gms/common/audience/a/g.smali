.class public final Lcom/google/android/gms/common/audience/a/g;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method public static a(Landroid/content/Intent;)I
    .locals 2

    .prologue
    .line 204
    const-string v0, "com.google.android.gms.common.audience.EXTRA_APP_ID"

    invoke-virtual {p0, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 205
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 207
    const/16 v0, 0x50

    .line 209
    :goto_0
    return v0

    :cond_0
    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    goto :goto_0
.end method

.method public static b(Landroid/content/Intent;)Lcom/google/android/gms/common/people/data/AudienceMember;
    .locals 1

    .prologue
    .line 237
    const-string v0, "com.google.android.gms.common.audience.EXTRA_TARGET_PERSON"

    invoke-virtual {p0, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/people/data/AudienceMember;

    return-object v0
.end method
