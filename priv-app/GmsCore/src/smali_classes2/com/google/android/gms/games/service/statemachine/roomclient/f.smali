.class final Lcom/google/android/gms/games/service/statemachine/roomclient/f;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/games/service/statemachine/roomclient/c;


# instance fields
.field final synthetic a:Lcom/google/android/gms/games/service/statemachine/roomclient/e;


# direct methods
.method constructor <init>(Lcom/google/android/gms/games/service/statemachine/roomclient/e;)V
    .locals 0

    .prologue
    .line 640
    iput-object p1, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/f;->a:Lcom/google/android/gms/games/service/statemachine/roomclient/e;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/games/multiplayer/realtime/Room;)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 643
    .line 645
    invoke-interface {p1}, Lcom/google/android/gms/games/multiplayer/realtime/Room;->l()Ljava/util/ArrayList;

    move-result-object v4

    .line 646
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v5

    move v2, v0

    move v1, v0

    :goto_0
    if-ge v2, v5, :cond_0

    .line 647
    add-int/lit8 v3, v0, 0x1

    .line 648
    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/multiplayer/Participant;

    invoke-interface {v0}, Lcom/google/android/gms/games/multiplayer/Participant;->f()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 649
    add-int/lit8 v0, v1, 0x1

    .line 646
    :goto_1
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    move v1, v0

    move v0, v3

    goto :goto_0

    .line 652
    :cond_0
    iget-object v2, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/f;->a:Lcom/google/android/gms/games/service/statemachine/roomclient/e;

    iget-object v2, v2, Lcom/google/android/gms/games/service/statemachine/roomclient/e;->f:Lcom/google/android/gms/games/service/statemachine/roomclient/InRoomState;

    invoke-static {v2}, Lcom/google/android/gms/games/service/statemachine/roomclient/InRoomState;->a(Lcom/google/android/gms/games/service/statemachine/roomclient/InRoomState;)Lcom/google/android/gms/games/service/statemachine/roomclient/ce;

    move-result-object v2

    iget-object v3, v2, Lcom/google/android/gms/games/service/statemachine/roomclient/ce;->a:Lcom/google/android/gms/games/g/ag;

    iget-object v4, v2, Lcom/google/android/gms/games/service/statemachine/roomclient/ce;->a:Lcom/google/android/gms/games/g/ag;

    iget v4, v4, Lcom/google/android/gms/games/g/ag;->q:I

    invoke-static {v0, v4}, Ljava/lang/Math;->max(II)I

    move-result v0

    iput v0, v3, Lcom/google/android/gms/games/g/ag;->q:I

    iget-object v0, v2, Lcom/google/android/gms/games/service/statemachine/roomclient/ce;->a:Lcom/google/android/gms/games/g/ag;

    iget-object v2, v2, Lcom/google/android/gms/games/service/statemachine/roomclient/ce;->a:Lcom/google/android/gms/games/g/ag;

    iget v2, v2, Lcom/google/android/gms/games/g/ag;->r:I

    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v1

    iput v1, v0, Lcom/google/android/gms/games/g/ag;->r:I

    .line 653
    return-void

    :cond_1
    move v0, v1

    goto :goto_1
.end method
