.class final Lcom/google/android/gms/car/senderprotocol/ag;
.super Ljava/lang/Thread;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/gms/car/senderprotocol/y;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/car/senderprotocol/y;)V
    .locals 1

    .prologue
    .line 327
    iput-object p1, p0, Lcom/google/android/gms/car/senderprotocol/ag;->a:Lcom/google/android/gms/car/senderprotocol/y;

    .line 328
    const-string v0, "WriterThread"

    invoke-direct {p0, v0}, Ljava/lang/Thread;-><init>(Ljava/lang/String;)V

    .line 329
    return-void
.end method

.method private a()V
    .locals 2

    .prologue
    .line 332
    iget-object v0, p0, Lcom/google/android/gms/car/senderprotocol/ag;->a:Lcom/google/android/gms/car/senderprotocol/y;

    invoke-static {v0}, Lcom/google/android/gms/car/senderprotocol/y;->g(Lcom/google/android/gms/car/senderprotocol/y;)Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1

    .line 333
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/car/senderprotocol/ag;->a:Lcom/google/android/gms/car/senderprotocol/y;

    invoke-static {v0}, Lcom/google/android/gms/car/senderprotocol/y;->h(Lcom/google/android/gms/car/senderprotocol/y;)Z

    .line 334
    iget-object v0, p0, Lcom/google/android/gms/car/senderprotocol/ag;->a:Lcom/google/android/gms/car/senderprotocol/y;

    invoke-static {v0}, Lcom/google/android/gms/car/senderprotocol/y;->i(Lcom/google/android/gms/car/senderprotocol/y;)Ljava/lang/Thread;

    .line 335
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final run()V
    .locals 2

    .prologue
    .line 340
    const/16 v0, -0x13

    invoke-static {v0}, Landroid/os/Process;->setThreadPriority(I)V

    .line 342
    :goto_0
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/car/senderprotocol/ag;->a:Lcom/google/android/gms/car/senderprotocol/y;

    invoke-static {v0}, Lcom/google/android/gms/car/senderprotocol/y;->j(Lcom/google/android/gms/car/senderprotocol/y;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 343
    iget-object v0, p0, Lcom/google/android/gms/car/senderprotocol/ag;->a:Lcom/google/android/gms/car/senderprotocol/y;

    invoke-static {v0}, Lcom/google/android/gms/car/senderprotocol/y;->k(Lcom/google/android/gms/car/senderprotocol/y;)Lcom/google/android/gms/car/senderprotocol/ad;

    move-result-object v1

    sget-object v0, Lcom/google/android/gms/car/ib;->b:Lcom/google/android/gms/car/ii;

    iget-object v0, v1, Lcom/google/android/gms/car/senderprotocol/ad;->b:Ljava/util/concurrent/BlockingQueue;

    invoke-interface {v0}, Ljava/util/concurrent/BlockingQueue;->take()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/car/senderprotocol/ae;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/car/senderprotocol/ad;->a(Lcom/google/android/gms/car/senderprotocol/ae;)V

    sget-object v0, Lcom/google/android/gms/car/ib;->b:Lcom/google/android/gms/car/ii;
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    .line 347
    :catch_0
    move-exception v0

    invoke-direct {p0}, Lcom/google/android/gms/car/senderprotocol/ag;->a()V

    .line 361
    :cond_0
    :goto_1
    return-void

    .line 348
    :catch_1
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 352
    iget-object v0, p0, Lcom/google/android/gms/car/senderprotocol/ag;->a:Lcom/google/android/gms/car/senderprotocol/y;

    invoke-static {v0}, Lcom/google/android/gms/car/senderprotocol/y;->j(Lcom/google/android/gms/car/senderprotocol/y;)Z

    move-result v0

    .line 354
    invoke-direct {p0}, Lcom/google/android/gms/car/senderprotocol/ag;->a()V

    .line 356
    if-eqz v0, :cond_0

    .line 357
    const-string v0, "CAR.GAL"

    const-string v1, "WriterThread: exiting due to IO error"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 358
    iget-object v0, p0, Lcom/google/android/gms/car/senderprotocol/ag;->a:Lcom/google/android/gms/car/senderprotocol/y;

    invoke-static {v0}, Lcom/google/android/gms/car/senderprotocol/y;->f(Lcom/google/android/gms/car/senderprotocol/y;)Lcom/google/android/gms/car/senderprotocol/af;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/car/senderprotocol/af;->a()V

    goto :goto_1
.end method
