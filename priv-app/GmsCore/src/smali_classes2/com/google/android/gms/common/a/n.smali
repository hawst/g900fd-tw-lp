.class final Lcom/google/android/gms/common/a/n;
.super Lcom/google/android/gms/common/a/r;
.source "SourceFile"


# instance fields
.field final synthetic a:Ljava/lang/String;

.field final synthetic b:Ljava/lang/Long;

.field final synthetic c:Lcom/google/android/gms/common/a/m;


# direct methods
.method constructor <init>(Lcom/google/android/gms/common/a/m;Lcom/google/android/gms/common/a/m;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;)V
    .locals 0

    .prologue
    .line 94
    iput-object p1, p0, Lcom/google/android/gms/common/a/n;->c:Lcom/google/android/gms/common/a/m;

    iput-object p4, p0, Lcom/google/android/gms/common/a/n;->a:Ljava/lang/String;

    iput-object p5, p0, Lcom/google/android/gms/common/a/n;->b:Ljava/lang/Long;

    invoke-direct {p0, p2, p3}, Lcom/google/android/gms/common/a/r;-><init>(Lcom/google/android/gms/common/a/m;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method protected final synthetic a(Landroid/content/SharedPreferences;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 94
    iget-object v0, p0, Lcom/google/android/gms/common/a/n;->a:Ljava/lang/String;

    invoke-interface {p1, v0}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/common/a/n;->a:Ljava/lang/String;

    const-wide/16 v2, 0x0

    invoke-interface {p1, v0, v2, v3}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/common/a/n;->b:Ljava/lang/Long;

    goto :goto_0
.end method

.method protected final synthetic a(Landroid/content/SharedPreferences$Editor;Ljava/lang/Object;)V
    .locals 4

    .prologue
    .line 94
    check-cast p2, Ljava/lang/Long;

    if-nez p2, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "null cannot be written for Long"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/common/a/n;->a:Ljava/lang/String;

    invoke-virtual {p2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-interface {p1, v0, v2, v3}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    return-void
.end method
