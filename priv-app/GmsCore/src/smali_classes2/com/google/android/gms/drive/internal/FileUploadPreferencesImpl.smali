.class public final Lcom/google/android/gms/drive/internal/FileUploadPreferencesImpl;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;
.implements Lcom/google/android/gms/drive/ah;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field final a:I

.field b:I

.field c:I

.field d:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 19
    new-instance v0, Lcom/google/android/gms/drive/internal/bu;

    invoke-direct {v0}, Lcom/google/android/gms/drive/internal/bu;-><init>()V

    sput-object v0, Lcom/google/android/gms/drive/internal/FileUploadPreferencesImpl;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method constructor <init>(IIIZ)V
    .locals 0

    .prologue
    .line 52
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 53
    iput p1, p0, Lcom/google/android/gms/drive/internal/FileUploadPreferencesImpl;->a:I

    .line 54
    iput p2, p0, Lcom/google/android/gms/drive/internal/FileUploadPreferencesImpl;->b:I

    .line 55
    iput p3, p0, Lcom/google/android/gms/drive/internal/FileUploadPreferencesImpl;->c:I

    .line 56
    iput-boolean p4, p0, Lcom/google/android/gms/drive/internal/FileUploadPreferencesImpl;->d:Z

    .line 57
    return-void
.end method

.method public constructor <init>(IIZ)V
    .locals 1

    .prologue
    .line 61
    const/4 v0, 0x1

    invoke-direct {p0, v0, p1, p2, p3}, Lcom/google/android/gms/drive/internal/FileUploadPreferencesImpl;-><init>(IIIZ)V

    .line 62
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 66
    iget v1, p0, Lcom/google/android/gms/drive/internal/FileUploadPreferencesImpl;->b:I

    packed-switch v1, :pswitch_data_0

    move v1, v0

    :goto_0
    if-nez v1, :cond_0

    .line 69
    :goto_1
    return v0

    .line 66
    :pswitch_0
    const/4 v1, 0x1

    goto :goto_0

    .line 69
    :cond_0
    iget v0, p0, Lcom/google/android/gms/drive/internal/FileUploadPreferencesImpl;->b:I

    goto :goto_1

    .line 66
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 98
    iget-boolean v0, p0, Lcom/google/android/gms/drive/internal/FileUploadPreferencesImpl;->d:Z

    return v0
.end method

.method public final c()I
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 82
    iget v1, p0, Lcom/google/android/gms/drive/internal/FileUploadPreferencesImpl;->c:I

    packed-switch v1, :pswitch_data_0

    move v1, v0

    :goto_0
    if-nez v1, :cond_0

    .line 85
    :goto_1
    return v0

    .line 82
    :pswitch_0
    const/4 v1, 0x1

    goto :goto_0

    .line 85
    :cond_0
    iget v0, p0, Lcom/google/android/gms/drive/internal/FileUploadPreferencesImpl;->c:I

    goto :goto_1

    .line 82
    nop

    :pswitch_data_0
    .packed-switch 0x100
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 37
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 0

    .prologue
    .line 42
    invoke-static {p0, p1}, Lcom/google/android/gms/drive/internal/bu;->a(Lcom/google/android/gms/drive/internal/FileUploadPreferencesImpl;Landroid/os/Parcel;)V

    .line 43
    return-void
.end method
