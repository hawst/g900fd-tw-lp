.class public Lcom/google/android/gms/car/CarErrorDisplayActivity;
.super Landroid/app/Activity;
.source "SourceFile"


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private b:Lcom/google/android/gms/car/ea;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 21
    const-class v0, Lcom/google/android/gms/car/CarErrorDisplayActivity;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/car/CarErrorDisplayActivity;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/car/CarErrorDisplayActivity;)Lcom/google/android/gms/car/ea;
    .locals 1

    .prologue
    .line 20
    iget-object v0, p0, Lcom/google/android/gms/car/CarErrorDisplayActivity;->b:Lcom/google/android/gms/car/ea;

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/gms/car/CarErrorDisplayActivity;I)V
    .locals 7

    .prologue
    .line 20
    sget v0, Lcom/google/android/gms/l;->J:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/car/CarErrorDisplayActivity;->setContentView(I)V

    sget v0, Lcom/google/android/gms/j;->jw:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/car/CarErrorDisplayActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    sget v1, Lcom/google/android/gms/j;->fI:I

    invoke-virtual {p0, v1}, Lcom/google/android/gms/car/CarErrorDisplayActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    const/4 v2, 0x3

    if-ne p1, v2, :cond_1

    sget-object v0, Lcom/google/android/gms/car/CarErrorDisplayActivity;->a:Ljava/lang/String;

    const-string v1, "IOError from car. This can be normal disconnect."

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Lcom/google/android/gms/car/CarErrorDisplayActivity;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/gms/car/CarErrorDisplayActivity;->finish()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/gms/car/CarErrorDisplayActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/google/android/gms/c;->d:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0}, Lcom/google/android/gms/car/CarErrorDisplayActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    sget v4, Lcom/google/android/gms/p;->dF:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x1

    add-int/lit8 v6, p1, -0x1

    aget-object v2, v2, v6

    aput-object v2, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    new-instance v0, Lcom/google/android/gms/car/eh;

    invoke-direct {v0, p0}, Lcom/google/android/gms/car/eh;-><init>(Lcom/google/android/gms/car/CarErrorDisplayActivity;)V

    invoke-virtual {v1, v0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 6

    .prologue
    .line 26
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 27
    new-instance v0, Lcom/google/android/gms/car/ea;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v2

    new-instance v3, Lcom/google/android/gms/car/ee;

    invoke-direct {v3, p0}, Lcom/google/android/gms/car/ee;-><init>(Lcom/google/android/gms/car/CarErrorDisplayActivity;)V

    new-instance v4, Lcom/google/android/gms/car/ef;

    invoke-direct {v4, p0}, Lcom/google/android/gms/car/ef;-><init>(Lcom/google/android/gms/car/CarErrorDisplayActivity;)V

    new-instance v5, Lcom/google/android/gms/car/eg;

    invoke-direct {v5, p0}, Lcom/google/android/gms/car/eg;-><init>(Lcom/google/android/gms/car/CarErrorDisplayActivity;)V

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/car/ea;-><init>(Landroid/content/Context;Landroid/os/Looper;Lcom/google/android/gms/car/t;Lcom/google/android/gms/common/api/x;Lcom/google/android/gms/common/api/y;)V

    iput-object v0, p0, Lcom/google/android/gms/car/CarErrorDisplayActivity;->b:Lcom/google/android/gms/car/ea;

    .line 70
    iget-object v0, p0, Lcom/google/android/gms/car/CarErrorDisplayActivity;->b:Lcom/google/android/gms/car/ea;

    invoke-virtual {v0}, Lcom/google/android/gms/car/ea;->a()V

    .line 71
    return-void
.end method

.method protected onDestroy()V
    .locals 1

    .prologue
    .line 102
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 103
    iget-object v0, p0, Lcom/google/android/gms/car/CarErrorDisplayActivity;->b:Lcom/google/android/gms/car/ea;

    if-eqz v0, :cond_0

    .line 104
    iget-object v0, p0, Lcom/google/android/gms/car/CarErrorDisplayActivity;->b:Lcom/google/android/gms/car/ea;

    invoke-virtual {v0}, Lcom/google/android/gms/car/ea;->b()V

    .line 105
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/car/CarErrorDisplayActivity;->b:Lcom/google/android/gms/car/ea;

    .line 107
    :cond_0
    return-void
.end method
