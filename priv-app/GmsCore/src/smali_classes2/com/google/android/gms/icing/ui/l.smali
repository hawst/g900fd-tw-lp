.class public abstract Lcom/google/android/gms/icing/ui/l;
.super Landroid/os/AsyncTask;
.source "SourceFile"


# static fields
.field private static final a:J


# instance fields
.field protected final b:Landroid/content/Context;

.field private c:Z


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 19
    sget-object v0, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0x3c

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    sput-wide v0, Lcom/google/android/gms/icing/ui/l;->a:J

    return-void
.end method

.method protected constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 25
    iput-object p1, p0, Lcom/google/android/gms/icing/ui/l;->b:Landroid/content/Context;

    .line 26
    return-void
.end method


# virtual methods
.method protected varargs abstract a(Lcom/google/android/gms/appdatasearch/c;)Ljava/lang/Object;
.end method

.method protected a()V
    .locals 0

    .prologue
    .line 34
    return-void
.end method

.method protected a(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 31
    return-void
.end method

.method protected final varargs doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 38
    new-instance v1, Lcom/google/android/gms/appdatasearch/c;

    iget-object v0, p0, Lcom/google/android/gms/icing/ui/l;->b:Landroid/content/Context;

    invoke-direct {v1, v0}, Lcom/google/android/gms/appdatasearch/c;-><init>(Landroid/content/Context;)V

    .line 39
    sget-wide v2, Lcom/google/android/gms/icing/ui/l;->a:J

    invoke-virtual {v1, v2, v3}, Lcom/google/android/gms/appdatasearch/c;->a(J)Lcom/google/android/gms/common/c;

    move-result-object v0

    .line 40
    invoke-virtual {v0}, Lcom/google/android/gms/common/c;->b()Z

    move-result v2

    if-nez v2, :cond_0

    .line 41
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Can\'t connect to Icing. Error:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/google/android/gms/common/c;->c()I

    move-result v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/icing/c;->d(Ljava/lang/String;)I

    .line 42
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/icing/ui/l;->c:Z

    .line 43
    const/4 v0, 0x0

    .line 48
    :goto_0
    return-object v0

    .line 46
    :cond_0
    :try_start_0
    invoke-virtual {p0, v1}, Lcom/google/android/gms/icing/ui/l;->a(Lcom/google/android/gms/appdatasearch/c;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 48
    invoke-virtual {v1}, Lcom/google/android/gms/appdatasearch/c;->b()V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Lcom/google/android/gms/appdatasearch/c;->b()V

    throw v0
.end method

.method protected final onPostExecute(Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 54
    iget-boolean v0, p0, Lcom/google/android/gms/icing/ui/l;->c:Z

    if-eqz v0, :cond_0

    .line 55
    iget-object v0, p0, Lcom/google/android/gms/icing/ui/l;->b:Landroid/content/Context;

    sget v1, Lcom/google/android/gms/p;->py:I

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 58
    invoke-virtual {p0}, Lcom/google/android/gms/icing/ui/l;->a()V

    .line 62
    :goto_0
    return-void

    .line 60
    :cond_0
    invoke-virtual {p0, p1}, Lcom/google/android/gms/icing/ui/l;->a(Ljava/lang/Object;)V

    goto :goto_0
.end method
