.class public final Lcom/google/android/gms/games/service/a/f;
.super Lcom/google/android/gms/games/service/a/a;
.source "SourceFile"


# instance fields
.field private final c:Lcom/google/android/gms/games/internal/dr;

.field private final d:Z


# direct methods
.method public constructor <init>(Lcom/google/android/gms/common/server/ClientContext;Lcom/google/android/gms/games/internal/dr;Z)V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0, p1}, Lcom/google/android/gms/games/service/a/a;-><init>(Lcom/google/android/gms/common/server/ClientContext;)V

    .line 21
    iput-object p2, p0, Lcom/google/android/gms/games/service/a/f;->c:Lcom/google/android/gms/games/internal/dr;

    .line 22
    iput-boolean p3, p0, Lcom/google/android/gms/games/service/a/f;->d:Z

    .line 23
    return-void
.end method


# virtual methods
.method protected final a(Lcom/google/android/gms/common/data/DataHolder;)V
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/google/android/gms/games/service/a/f;->c:Lcom/google/android/gms/games/internal/dr;

    invoke-interface {v0, p1}, Lcom/google/android/gms/games/internal/dr;->B(Lcom/google/android/gms/common/data/DataHolder;)V

    .line 34
    return-void
.end method

.method protected final b(Landroid/content/Context;Lcom/google/android/gms/games/a/t;)Lcom/google/android/gms/common/data/DataHolder;
    .locals 2

    .prologue
    .line 28
    iget-object v0, p0, Lcom/google/android/gms/games/service/a/f;->a:Lcom/google/android/gms/common/server/ClientContext;

    iget-boolean v1, p0, Lcom/google/android/gms/games/service/a/f;->d:Z

    invoke-virtual {p2, v0, v1}, Lcom/google/android/gms/games/a/t;->a(Lcom/google/android/gms/common/server/ClientContext;Z)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    return-object v0
.end method
