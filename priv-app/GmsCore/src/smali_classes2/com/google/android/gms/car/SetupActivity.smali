.class public Lcom/google/android/gms/car/SetupActivity;
.super Landroid/support/v7/app/d;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/car/fsm/h;


# instance fields
.field private a:Lcom/google/android/car/fsm/FsmController;

.field private b:Landroid/app/Fragment;

.field private volatile c:Landroid/app/Fragment;

.field private d:Lcom/google/android/car/fsm/ActivityResult;

.field private e:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Landroid/support/v7/app/d;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/Class;
    .locals 1

    .prologue
    .line 97
    const-class v0, Lcom/google/android/gms/car/SetupFsm$WaitingForCarConnectionState;

    return-object v0
.end method

.method public final a(Landroid/content/Intent;)V
    .locals 1

    .prologue
    .line 188
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, Lcom/google/android/gms/car/SetupActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 189
    return-void
.end method

.method public final a(Lcom/google/android/car/fsm/FsmController;)V
    .locals 0

    .prologue
    .line 107
    iput-object p1, p0, Lcom/google/android/gms/car/SetupActivity;->a:Lcom/google/android/car/fsm/FsmController;

    .line 108
    return-void
.end method

.method public final a(Ljava/lang/Class;Landroid/os/Bundle;Z)V
    .locals 4

    .prologue
    .line 117
    if-nez p3, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/car/SetupActivity;->b:Landroid/app/Fragment;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/car/SetupActivity;->b:Landroid/app/Fragment;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 145
    :goto_0
    return-void

    .line 125
    :cond_0
    :try_start_0
    invoke-virtual {p1}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Fragment;
    :try_end_0
    .catch Ljava/lang/InstantiationException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_1

    .line 131
    invoke-virtual {v0, p2}, Landroid/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 133
    iget-boolean v1, p0, Lcom/google/android/gms/car/SetupActivity;->e:Z

    if-eqz v1, :cond_2

    .line 134
    const-string v1, "CAR.SETUP"

    const/4 v2, 0x3

    invoke-static {v1, v2}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 135
    const-string v1, "CAR.SETUP"

    const-string v2, "Paused, deferring fragment switch"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 137
    :cond_1
    iput-object v0, p0, Lcom/google/android/gms/car/SetupActivity;->c:Landroid/app/Fragment;

    goto :goto_0

    .line 126
    :catch_0
    move-exception v0

    .line 127
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 128
    :catch_1
    move-exception v0

    .line 129
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 139
    :cond_2
    iput-object v0, p0, Lcom/google/android/gms/car/SetupActivity;->b:Landroid/app/Fragment;

    .line 140
    invoke-virtual {p0}, Lcom/google/android/gms/car/SetupActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v0

    sget v1, Lcom/google/android/gms/j;->gq:I

    iget-object v2, p0, Lcom/google/android/gms/car/SetupActivity;->b:Landroid/app/Fragment;

    const-string v3, "fragment_main"

    invoke-virtual {v0, v1, v2, v3}, Landroid/app/FragmentTransaction;->replace(ILandroid/app/Fragment;Ljava/lang/String;)Landroid/app/FragmentTransaction;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentTransaction;->commit()I

    goto :goto_0
.end method

.method public final c()Lcom/google/android/car/fsm/FsmController;
    .locals 1

    .prologue
    .line 112
    iget-object v0, p0, Lcom/google/android/gms/car/SetupActivity;->a:Lcom/google/android/car/fsm/FsmController;

    return-object v0
.end method

.method public final h_()Ljava/util/List;
    .locals 1

    .prologue
    .line 102
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    return-object v0
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 3

    .prologue
    .line 193
    const-string v0, "CAR.SETUP"

    const/4 v1, 0x2

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 194
    const-string v0, "CAR.SETUP"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "onActivityResult. requestCode="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " resultCode="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 197
    :cond_0
    const/4 v0, 0x1

    if-ne p1, v0, :cond_2

    .line 201
    iget-object v0, p0, Lcom/google/android/gms/car/SetupActivity;->d:Lcom/google/android/car/fsm/ActivityResult;

    if-eqz v0, :cond_1

    .line 202
    const-string v0, "CAR.SETUP"

    const/4 v1, 0x4

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 203
    const-string v0, "CAR.SETUP"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Got an activity result when mPendingFsmActivityResult is not null. losing result with resultCode="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/gms/car/SetupActivity;->d:Lcom/google/android/car/fsm/ActivityResult;

    iget v2, v2, Lcom/google/android/car/fsm/ActivityResult;->a:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " data="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/car/SetupActivity;->d:Lcom/google/android/car/fsm/ActivityResult;

    iget-object v2, v2, Lcom/google/android/car/fsm/ActivityResult;->b:Landroid/content/Intent;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 209
    :cond_1
    iget-boolean v0, p0, Lcom/google/android/gms/car/SetupActivity;->e:Z

    if-eqz v0, :cond_3

    .line 210
    new-instance v0, Lcom/google/android/car/fsm/ActivityResult;

    invoke-direct {v0, p2, p3}, Lcom/google/android/car/fsm/ActivityResult;-><init>(ILandroid/content/Intent;)V

    iput-object v0, p0, Lcom/google/android/gms/car/SetupActivity;->d:Lcom/google/android/car/fsm/ActivityResult;

    .line 216
    :cond_2
    :goto_0
    return-void

    .line 212
    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/car/SetupActivity;->a:Lcom/google/android/car/fsm/FsmController;

    const-string v1, "EVENT_ACTIVITY_RESULT"

    new-instance v2, Lcom/google/android/car/fsm/ActivityResult;

    invoke-direct {v2, p2, p3}, Lcom/google/android/car/fsm/ActivityResult;-><init>(ILandroid/content/Intent;)V

    invoke-virtual {v0, v1, v2}, Lcom/google/android/car/fsm/FsmController;->a(Ljava/lang/String;Landroid/os/Parcelable;)V

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 44
    const-string v0, "CAR.SETUP"

    const/4 v1, 0x2

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 45
    const-string v0, "CAR.SETUP"

    const-string v1, "onCreate"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 47
    :cond_0
    invoke-super {p0, p1}, Landroid/support/v7/app/d;->onCreate(Landroid/os/Bundle;)V

    .line 49
    sget v0, Lcom/google/android/gms/l;->O:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/car/SetupActivity;->setContentView(I)V

    .line 51
    sget v0, Lcom/google/android/gms/j;->gq:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/car/SetupActivity;->findViewById(I)Landroid/view/View;

    .line 55
    invoke-virtual {p0}, Lcom/google/android/gms/car/SetupActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    const-string v1, "fragment_main"

    invoke-virtual {v0, v1}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/car/SetupActivity;->b:Landroid/app/Fragment;

    .line 56
    if-nez p1, :cond_1

    .line 57
    invoke-virtual {p0}, Lcom/google/android/gms/car/SetupActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v0

    new-instance v1, Lcom/google/android/gms/car/ph;

    invoke-direct {v1}, Lcom/google/android/gms/car/ph;-><init>()V

    const-string v2, "fragment_fsm_controller"

    invoke-virtual {v0, v1, v2}, Landroid/app/FragmentTransaction;->add(Landroid/app/Fragment;Ljava/lang/String;)Landroid/app/FragmentTransaction;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentTransaction;->commit()I

    .line 61
    :cond_1
    return-void
.end method

.method protected onPause()V
    .locals 2

    .prologue
    .line 88
    const-string v0, "CAR.SETUP"

    const/4 v1, 0x2

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 89
    const-string v0, "CAR.SETUP"

    const-string v1, "onPause"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 91
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/car/SetupActivity;->e:Z

    .line 92
    invoke-super {p0}, Landroid/support/v7/app/d;->onPause()V

    .line 93
    return-void
.end method

.method protected onResume()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 65
    const-string v0, "CAR.SETUP"

    const/4 v1, 0x2

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 66
    const-string v0, "CAR.SETUP"

    const-string v1, "onResume"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 68
    :cond_0
    invoke-super {p0}, Landroid/support/v7/app/d;->onResume()V

    .line 69
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/car/SetupActivity;->e:Z

    .line 71
    iget-object v0, p0, Lcom/google/android/gms/car/SetupActivity;->c:Landroid/app/Fragment;

    if-eqz v0, :cond_1

    .line 72
    iget-object v0, p0, Lcom/google/android/gms/car/SetupActivity;->c:Landroid/app/Fragment;

    iput-object v0, p0, Lcom/google/android/gms/car/SetupActivity;->b:Landroid/app/Fragment;

    .line 73
    iput-object v4, p0, Lcom/google/android/gms/car/SetupActivity;->c:Landroid/app/Fragment;

    .line 74
    invoke-virtual {p0}, Lcom/google/android/gms/car/SetupActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v0

    sget v1, Lcom/google/android/gms/j;->gq:I

    iget-object v2, p0, Lcom/google/android/gms/car/SetupActivity;->b:Landroid/app/Fragment;

    const-string v3, "fragment_main"

    invoke-virtual {v0, v1, v2, v3}, Landroid/app/FragmentTransaction;->replace(ILandroid/app/Fragment;Ljava/lang/String;)Landroid/app/FragmentTransaction;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentTransaction;->commit()I

    .line 80
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/car/SetupActivity;->d:Lcom/google/android/car/fsm/ActivityResult;

    if-eqz v0, :cond_2

    .line 81
    iget-object v0, p0, Lcom/google/android/gms/car/SetupActivity;->a:Lcom/google/android/car/fsm/FsmController;

    const-string v1, "EVENT_ACTIVITY_RESULT"

    iget-object v2, p0, Lcom/google/android/gms/car/SetupActivity;->d:Lcom/google/android/car/fsm/ActivityResult;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/car/fsm/FsmController;->a(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 82
    iput-object v4, p0, Lcom/google/android/gms/car/SetupActivity;->d:Lcom/google/android/car/fsm/ActivityResult;

    .line 84
    :cond_2
    return-void
.end method
