.class final Lcom/google/android/gms/games/service/statemachine/roomservice/LibrariesLoadedState;
.super Lcom/google/android/gms/games/service/statemachine/roomservice/u;
.source "SourceFile"


# annotations
.annotation build Lcom/google/android/gms/common/util/RetainForClient;
.end annotation


# direct methods
.method public constructor <init>(Lcom/google/android/gms/games/service/statemachine/roomservice/RoomServiceStateMachine;)V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0, p1}, Lcom/google/android/gms/games/service/statemachine/roomservice/u;-><init>(Lcom/google/android/gms/games/service/statemachine/roomservice/RoomServiceStateMachine;)V

    .line 25
    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Message;)Z
    .locals 4

    .prologue
    .line 36
    iget v0, p1, Landroid/os/Message;->what:I

    sparse-switch v0, :sswitch_data_0

    .line 72
    sget-boolean v0, Lcom/google/android/gms/games/service/statemachine/roomservice/LibrariesLoadedState;->g:Z

    :goto_0
    return v0

    .line 39
    :sswitch_0
    sget-boolean v0, Lcom/google/android/gms/games/service/statemachine/roomservice/LibrariesLoadedState;->f:Z

    goto :goto_0

    .line 42
    :sswitch_1
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/games/service/statemachine/roomservice/LibrariesLoadedState;->j:Lcom/google/android/gms/games/service/statemachine/roomservice/w;

    iget-object v0, v0, Lcom/google/android/gms/games/service/statemachine/roomservice/w;->d:Lcom/google/android/gms/games/internal/eg;

    invoke-interface {v0}, Lcom/google/android/gms/games/internal/eg;->b()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 46
    :goto_1
    sget-boolean v0, Lcom/google/android/gms/games/service/statemachine/roomservice/LibrariesLoadedState;->f:Z

    goto :goto_0

    .line 43
    :catch_0
    move-exception v0

    .line 44
    iget-object v1, p0, Lcom/google/android/gms/games/service/statemachine/roomservice/LibrariesLoadedState;->e:Lcom/google/android/gms/games/service/statemachine/roomservice/RoomServiceStateMachine;

    invoke-static {v0}, Lcom/google/android/gms/games/service/statemachine/roomservice/RoomServiceStateMachine;->a(Landroid/os/RemoteException;)V

    goto :goto_1

    .line 51
    :sswitch_2
    :try_start_1
    iget-object v0, p0, Lcom/google/android/gms/games/service/statemachine/roomservice/LibrariesLoadedState;->j:Lcom/google/android/gms/games/service/statemachine/roomservice/w;

    iget-object v0, v0, Lcom/google/android/gms/games/service/statemachine/roomservice/w;->d:Lcom/google/android/gms/games/internal/eg;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/google/android/gms/games/internal/eg;->a(I)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_1

    .line 55
    :goto_2
    sget-boolean v0, Lcom/google/android/gms/games/service/statemachine/roomservice/LibrariesLoadedState;->f:Z

    goto :goto_0

    .line 52
    :catch_1
    move-exception v0

    .line 53
    iget-object v1, p0, Lcom/google/android/gms/games/service/statemachine/roomservice/LibrariesLoadedState;->e:Lcom/google/android/gms/games/service/statemachine/roomservice/RoomServiceStateMachine;

    invoke-static {v0}, Lcom/google/android/gms/games/service/statemachine/roomservice/RoomServiceStateMachine;->a(Landroid/os/RemoteException;)V

    goto :goto_2

    .line 58
    :sswitch_3
    iget-object v0, p0, Lcom/google/android/gms/games/service/statemachine/roomservice/LibrariesLoadedState;->j:Lcom/google/android/gms/games/service/statemachine/roomservice/w;

    iget-object v0, v0, Lcom/google/android/gms/games/service/statemachine/roomservice/w;->b:Lcom/google/android/gms/games/realtime/network/b;

    invoke-interface {v0}, Lcom/google/android/gms/games/realtime/network/b;->a()V

    .line 59
    iget-object v0, p0, Lcom/google/android/gms/games/service/statemachine/roomservice/LibrariesLoadedState;->j:Lcom/google/android/gms/games/service/statemachine/roomservice/w;

    const/4 v1, 0x0

    iput-object v1, v0, Lcom/google/android/gms/games/service/statemachine/roomservice/w;->b:Lcom/google/android/gms/games/realtime/network/b;

    .line 60
    iget-object v0, p0, Lcom/google/android/gms/games/service/statemachine/roomservice/LibrariesLoadedState;->l:Lcom/google/android/gms/games/service/statemachine/roomservice/y;

    iget-object v0, v0, Lcom/google/android/gms/games/service/statemachine/roomservice/y;->e:Lcom/google/android/gms/games/service/statemachine/roomservice/LibrariesUnloadedState;

    invoke-virtual {v0}, Lcom/google/android/gms/games/service/statemachine/roomservice/LibrariesUnloadedState;->b()V

    .line 61
    sget-boolean v0, Lcom/google/android/gms/games/service/statemachine/roomservice/LibrariesLoadedState;->f:Z

    goto :goto_0

    .line 63
    :sswitch_4
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/gms/games/service/statemachine/roomservice/b;

    .line 68
    iget-object v1, p0, Lcom/google/android/gms/games/service/statemachine/roomservice/LibrariesLoadedState;->j:Lcom/google/android/gms/games/service/statemachine/roomservice/w;

    iget-object v1, v1, Lcom/google/android/gms/games/service/statemachine/roomservice/w;->b:Lcom/google/android/gms/games/realtime/network/b;

    iget-object v2, v0, Lcom/google/android/gms/games/service/statemachine/roomservice/b;->b:Ljava/lang/String;

    iget-object v3, v0, Lcom/google/android/gms/games/service/statemachine/roomservice/b;->a:Ljava/lang/String;

    invoke-interface {v1, v2, v3}, Lcom/google/android/gms/games/realtime/network/b;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 69
    iget-object v1, p0, Lcom/google/android/gms/games/service/statemachine/roomservice/LibrariesLoadedState;->l:Lcom/google/android/gms/games/service/statemachine/roomservice/y;

    iget-object v1, v1, Lcom/google/android/gms/games/service/statemachine/roomservice/y;->g:Lcom/google/android/gms/games/service/statemachine/roomservice/NetworkConnectingState;

    iget-object v0, v0, Lcom/google/android/gms/games/service/statemachine/roomservice/b;->a:Ljava/lang/String;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/games/service/statemachine/roomservice/NetworkConnectingState;->a(Ljava/lang/String;)V

    .line 70
    sget-boolean v0, Lcom/google/android/gms/games/service/statemachine/roomservice/LibrariesLoadedState;->f:Z

    goto :goto_0

    .line 36
    nop

    :sswitch_data_0
    .sparse-switch
        0x2 -> :sswitch_0
        0xe -> :sswitch_4
        0xf -> :sswitch_2
        0x12 -> :sswitch_3
        0x13 -> :sswitch_1
    .end sparse-switch
.end method

.method public final b()V
    .locals 0

    .prologue
    .line 31
    invoke-virtual {p0}, Lcom/google/android/gms/games/service/statemachine/roomservice/LibrariesLoadedState;->c()V

    .line 32
    return-void
.end method
