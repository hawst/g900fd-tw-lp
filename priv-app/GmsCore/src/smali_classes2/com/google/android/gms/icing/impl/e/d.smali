.class public final Lcom/google/android/gms/icing/impl/e/d;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method static a([BIILjava/lang/Class;)Lcom/google/protobuf/nano/j;
    .locals 5

    .prologue
    const/4 v3, 0x1

    const/4 v1, 0x0

    const/4 v4, 0x0

    .line 225
    :try_start_0
    invoke-virtual {p3}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protobuf/nano/j;

    .line 226
    invoke-static {v0, p0, p1, p2}, Lcom/google/protobuf/nano/j;->mergeFrom(Lcom/google/protobuf/nano/j;[BII)Lcom/google/protobuf/nano/j;
    :try_end_0
    .catch Lcom/google/protobuf/nano/i; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/InstantiationException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_2

    .line 236
    :goto_0
    return-object v0

    .line 228
    :catch_0
    move-exception v0

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Cannot deserialize message of type "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    new-array v3, v4, [Ljava/lang/Object;

    invoke-static {v0, v2, v3}, Lcom/google/android/gms/icing/c;->b(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I

    move-object v0, v1

    .line 230
    goto :goto_0

    .line 231
    :catch_1
    move-exception v0

    const-string v2, "Error constructing new message of type %s"

    new-array v3, v3, [Ljava/lang/Object;

    aput-object p3, v3, v4

    invoke-static {v0, v2, v3}, Lcom/google/android/gms/icing/c;->b(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I

    move-object v0, v1

    .line 233
    goto :goto_0

    .line 234
    :catch_2
    move-exception v0

    const-string v2, "Error constructing new message of type %s"

    new-array v3, v3, [Ljava/lang/Object;

    aput-object p3, v3, v4

    invoke-static {v0, v2, v3}, Lcom/google/android/gms/icing/c;->b(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I

    move-object v0, v1

    .line 236
    goto :goto_0
.end method

.method public static a(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 330
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 p0, 0x0

    :cond_0
    return-object p0
.end method

.method public static a([B)Ljava/lang/String;
    .locals 2

    .prologue
    .line 323
    new-instance v0, Ljava/lang/String;

    const-string v1, "UTF-8"

    invoke-static {v1}, Ljava/nio/charset/Charset;->forName(Ljava/lang/String;)Ljava/nio/charset/Charset;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Ljava/lang/String;-><init>([BLjava/nio/charset/Charset;)V

    return-object v0
.end method

.method static a(Ljava/lang/Iterable;Z)Ljava/nio/ByteBuffer;
    .locals 10

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x0

    .line 253
    .line 254
    if-eqz p1, :cond_0

    const-wide/16 v2, 0x8

    .line 256
    :goto_0
    if-eqz p1, :cond_1

    const/4 v0, 0x4

    move v1, v0

    .line 259
    :goto_1
    invoke-interface {p0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v7

    move v4, v5

    :goto_2
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protobuf/nano/j;

    .line 260
    invoke-virtual {v0}, Lcom/google/protobuf/nano/j;->getSerializedSize()I

    move-result v0

    add-int/2addr v0, v1

    int-to-long v8, v0

    add-long/2addr v2, v8

    .line 261
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    .line 262
    goto :goto_2

    .line 254
    :cond_0
    const-wide/16 v2, 0x0

    goto :goto_0

    .line 256
    :cond_1
    const/16 v0, 0xc

    move v1, v0

    goto :goto_1

    .line 263
    :cond_2
    if-nez v4, :cond_3

    .line 265
    invoke-static {v5}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 316
    :goto_3
    return-object v0

    .line 268
    :cond_3
    long-to-int v0, v2

    :try_start_0
    invoke-static {v0}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 277
    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v2

    .line 281
    invoke-interface {p0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_4
    :goto_4
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protobuf/nano/j;

    .line 283
    invoke-virtual {v0}, Lcom/google/protobuf/nano/j;->getCachedSize()I

    move-result v4

    .line 285
    :try_start_1
    invoke-virtual {v1, v4}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;
    :try_end_1
    .catch Ljava/nio/BufferOverflowException; {:try_start_1 .. :try_end_1} :catch_1

    .line 290
    add-int/lit8 v5, v5, 0x4

    .line 293
    invoke-static {v0, v2, v5, v4}, Lcom/google/protobuf/nano/j;->toByteArray(Lcom/google/protobuf/nano/j;[BII)V

    .line 296
    :try_start_2
    invoke-virtual {v1, v2, v5, v4}, Ljava/nio/ByteBuffer;->put([BII)Ljava/nio/ByteBuffer;
    :try_end_2
    .catch Ljava/nio/BufferOverflowException; {:try_start_2 .. :try_end_2} :catch_2

    .line 301
    add-int/2addr v5, v4

    .line 302
    if-nez p1, :cond_4

    .line 303
    sub-int v0, v5, v4

    invoke-static {v1, v2, v0, v4}, Lcom/google/android/gms/icing/impl/e/d;->a(Ljava/nio/ByteBuffer;[BII)V

    .line 304
    add-int/lit8 v5, v5, 0x8

    goto :goto_4

    .line 271
    :catch_0
    move-exception v0

    const-string v1, "Too big to serialize, %s"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v2, v3}, Lcom/google/android/gms/icing/impl/bp;->a(J)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v4, v5

    invoke-static {v0, v1, v4}, Lcom/google/android/gms/icing/c;->b(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I

    move-object v0, v6

    .line 273
    goto :goto_3

    .line 286
    :catch_1
    move-exception v0

    invoke-static {v0}, Lcom/google/android/gms/icing/impl/e/d;->a(Ljava/nio/BufferOverflowException;)V

    move-object v0, v6

    .line 288
    goto :goto_3

    .line 297
    :catch_2
    move-exception v0

    invoke-static {v0}, Lcom/google/android/gms/icing/impl/e/d;->a(Ljava/nio/BufferOverflowException;)V

    move-object v0, v6

    .line 299
    goto :goto_3

    .line 307
    :cond_5
    if-eqz p1, :cond_6

    .line 309
    const/4 v0, 0x0

    :try_start_3
    invoke-static {v1, v2, v0, v5}, Lcom/google/android/gms/icing/impl/e/d;->a(Ljava/nio/ByteBuffer;[BII)V
    :try_end_3
    .catch Ljava/nio/BufferOverflowException; {:try_start_3 .. :try_end_3} :catch_3

    .line 315
    :cond_6
    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->rewind()Ljava/nio/Buffer;

    move-object v0, v1

    .line 316
    goto :goto_3

    .line 310
    :catch_3
    move-exception v0

    invoke-static {v0}, Lcom/google/android/gms/icing/impl/e/d;->a(Ljava/nio/BufferOverflowException;)V

    move-object v0, v6

    .line 312
    goto :goto_3
.end method

.method static a(Ljava/nio/ByteBuffer;Ljava/lang/Class;)Ljava/util/List;
    .locals 8

    .prologue
    const/4 v7, 0x0

    const/4 v0, 0x0

    .line 70
    invoke-virtual {p1}, Ljava/lang/Class;->toString()Ljava/lang/String;

    move-result-object v2

    .line 72
    invoke-virtual {p0}, Ljava/nio/ByteBuffer;->limit()I

    move-result v1

    add-int/lit8 v1, v1, -0x8

    .line 75
    if-gez v1, :cond_1

    .line 76
    const-string v1, "Protobuf data too short to be valid"

    invoke-static {v1}, Lcom/google/android/gms/icing/c;->d(Ljava/lang/String;)I

    .line 137
    :cond_0
    :goto_0
    return-object v0

    .line 80
    :cond_1
    invoke-virtual {p0, v1}, Ljava/nio/ByteBuffer;->getLong(I)J

    move-result-wide v4

    .line 84
    invoke-virtual {p0}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v3

    invoke-static {v3, v7, v1, v4, v5}, Lcom/google/android/gms/icing/impl/e/d;->a([BIIJ)Z

    move-result v3

    if-nez v3, :cond_2

    .line 85
    const-string v1, "Ignoring corrupt protobuf data"

    invoke-static {v1}, Lcom/google/android/gms/icing/c;->d(Ljava/lang/String;)I

    goto :goto_0

    .line 88
    :cond_2
    if-nez v1, :cond_3

    .line 89
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, v7}, Ljava/util/ArrayList;-><init>(I)V

    goto :goto_0

    .line 93
    :cond_3
    invoke-virtual {p0}, Ljava/nio/ByteBuffer;->limit()I

    move-result v1

    add-int/lit8 v3, v1, -0x8

    .line 95
    new-instance v1, Ljava/util/ArrayList;

    invoke-virtual {p0}, Ljava/nio/ByteBuffer;->limit()I

    move-result v4

    div-int/lit16 v4, v4, 0x3e8

    add-int/lit8 v4, v4, 0x1

    invoke-direct {v1, v4}, Ljava/util/ArrayList;-><init>(I)V

    .line 96
    :goto_1
    invoke-virtual {p0}, Ljava/nio/ByteBuffer;->position()I

    move-result v4

    if-ge v4, v3, :cond_5

    .line 100
    :try_start_0
    invoke-virtual {p0}, Ljava/nio/ByteBuffer;->getInt()I
    :try_end_0
    .catch Ljava/nio/BufferUnderflowException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v4

    .line 105
    if-gez v4, :cond_4

    .line 109
    const-string v1, "Invalid message size: %d.May have given the wrong message type: %s"

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-static {v1, v3, v2}, Lcom/google/android/gms/icing/c;->d(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)I

    goto :goto_0

    .line 101
    :catch_0
    move-exception v1

    const-string v3, "Buffer underflow. May have given the wrong message type: %s"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    aput-object v2, v4, v7

    invoke-static {v1, v3, v4}, Lcom/google/android/gms/icing/c;->b(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I

    goto :goto_0

    .line 114
    :cond_4
    invoke-virtual {p0}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v5

    invoke-virtual {p0}, Ljava/nio/ByteBuffer;->position()I

    move-result v6

    invoke-static {v5, v6, v4, p1}, Lcom/google/android/gms/icing/impl/e/d;->a([BIILjava/lang/Class;)Lcom/google/protobuf/nano/j;

    move-result-object v5

    .line 129
    if-eqz v5, :cond_0

    .line 133
    invoke-interface {v1, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 135
    invoke-virtual {p0}, Ljava/nio/ByteBuffer;->position()I

    move-result v5

    add-int/2addr v4, v5

    add-int/lit8 v4, v4, 0x0

    invoke-virtual {p0, v4}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    goto :goto_1

    :cond_5
    move-object v0, v1

    .line 137
    goto :goto_0
.end method

.method private static a(Ljava/nio/BufferOverflowException;)V
    .locals 2

    .prologue
    .line 359
    const-string v0, "Buffer underflow. A message may have an invalid serialized form or has been concurrently modified."

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {p0, v0, v1}, Lcom/google/android/gms/icing/c;->b(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 361
    return-void
.end method

.method private static a(Ljava/nio/ByteBuffer;[BII)V
    .locals 2

    .prologue
    .line 377
    new-instance v0, Ljava/util/zip/CRC32;

    invoke-direct {v0}, Ljava/util/zip/CRC32;-><init>()V

    .line 378
    invoke-virtual {v0, p1, p2, p3}, Ljava/util/zip/CRC32;->update([BII)V

    .line 379
    invoke-virtual {v0}, Ljava/util/zip/CRC32;->getValue()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Ljava/nio/ByteBuffer;->putLong(J)Ljava/nio/ByteBuffer;

    .line 380
    return-void
.end method

.method static a([BIIJ)Z
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 383
    new-instance v1, Ljava/util/zip/CRC32;

    invoke-direct {v1}, Ljava/util/zip/CRC32;-><init>()V

    .line 384
    invoke-virtual {v1, p0, v0, p2}, Ljava/util/zip/CRC32;->update([BII)V

    .line 385
    invoke-virtual {v1}, Ljava/util/zip/CRC32;->getValue()J

    move-result-wide v2

    .line 386
    cmp-long v1, v2, p3

    if-nez v1, :cond_0

    const/4 v0, 0x1

    .line 387
    :cond_0
    if-nez v0, :cond_1

    .line 388
    const-string v1, "Corrupt protobuf data, expected CRC: %d computed CRC: %d"

    invoke-static {p3, p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-static {v1, v4, v2}, Lcom/google/android/gms/icing/c;->d(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)I

    .line 391
    :cond_1
    return v0
.end method

.method public static final a(Lcom/google/protobuf/nano/j;)[B
    .locals 1

    .prologue
    .line 351
    if-nez p0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {p0}, Lcom/google/protobuf/nano/j;->toByteArray(Lcom/google/protobuf/nano/j;)[B

    move-result-object v0

    goto :goto_0
.end method
