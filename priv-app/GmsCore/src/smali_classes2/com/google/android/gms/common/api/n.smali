.class public final Lcom/google/android/gms/common/api/n;
.super Lcom/google/android/gms/common/api/a;
.source "SourceFile"


# instance fields
.field a:I

.field b:Z

.field d:Z

.field final e:[Lcom/google/android/gms/common/api/am;

.field final f:Ljava/lang/Object;


# direct methods
.method private constructor <init>(Ljava/util/List;Landroid/os/Looper;)V
    .locals 3

    .prologue
    .line 64
    new-instance v0, Lcom/google/android/gms/common/api/b;

    invoke-direct {v0, p2}, Lcom/google/android/gms/common/api/b;-><init>(Landroid/os/Looper;)V

    invoke-direct {p0, v0}, Lcom/google/android/gms/common/api/a;-><init>(Lcom/google/android/gms/common/api/b;)V

    .line 25
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/common/api/n;->f:Ljava/lang/Object;

    .line 65
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/common/api/n;->a:I

    .line 66
    iget v0, p0, Lcom/google/android/gms/common/api/n;->a:I

    new-array v0, v0, [Lcom/google/android/gms/common/api/am;

    iput-object v0, p0, Lcom/google/android/gms/common/api/n;->e:[Lcom/google/android/gms/common/api/am;

    .line 67
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 68
    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/api/am;

    .line 69
    iget-object v2, p0, Lcom/google/android/gms/common/api/n;->e:[Lcom/google/android/gms/common/api/am;

    aput-object v0, v2, v1

    .line 70
    new-instance v2, Lcom/google/android/gms/common/api/o;

    invoke-direct {v2, p0}, Lcom/google/android/gms/common/api/o;-><init>(Lcom/google/android/gms/common/api/n;)V

    invoke-interface {v0, v2}, Lcom/google/android/gms/common/api/am;->a(Lcom/google/android/gms/common/api/an;)V

    .line 67
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 103
    :cond_0
    return-void
.end method

.method public synthetic constructor <init>(Ljava/util/List;Landroid/os/Looper;B)V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0, p1, p2}, Lcom/google/android/gms/common/api/n;-><init>(Ljava/util/List;Landroid/os/Looper;)V

    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/common/api/n;)V
    .locals 0

    .prologue
    .line 19
    invoke-super {p0}, Lcom/google/android/gms/common/api/a;->b()V

    return-void
.end method


# virtual methods
.method public final synthetic a(Lcom/google/android/gms/common/api/Status;)Lcom/google/android/gms/common/api/ap;
    .locals 2

    .prologue
    .line 19
    new-instance v0, Lcom/google/android/gms/common/api/q;

    iget-object v1, p0, Lcom/google/android/gms/common/api/n;->e:[Lcom/google/android/gms/common/api/am;

    invoke-direct {v0, p1, v1}, Lcom/google/android/gms/common/api/q;-><init>(Lcom/google/android/gms/common/api/Status;[Lcom/google/android/gms/common/api/am;)V

    return-object v0
.end method

.method public final b()V
    .locals 4

    .prologue
    .line 113
    invoke-super {p0}, Lcom/google/android/gms/common/api/a;->b()V

    .line 114
    iget-object v1, p0, Lcom/google/android/gms/common/api/n;->e:[Lcom/google/android/gms/common/api/am;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    .line 115
    invoke-interface {v3}, Lcom/google/android/gms/common/api/am;->b()V

    .line 114
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 117
    :cond_0
    return-void
.end method
