.class public final Lcom/google/android/gms/icing/c/a;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/icing/c/b;


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private final b:Landroid/content/Context;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 67
    const-string v0, "http.agent"

    invoke-static {v0}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/icing/c/a;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 78
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 79
    iput-object p1, p0, Lcom/google/android/gms/icing/c/a;->b:Landroid/content/Context;

    .line 80
    return-void
.end method

.method private static a(Lcom/google/android/gms/icing/c/a/i;)Ljava/lang/String;
    .locals 4

    .prologue
    .line 354
    :try_start_0
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    .line 355
    const-string v1, "serialized_request"

    invoke-static {p0}, Lcom/google/protobuf/nano/j;->toByteArray(Lcom/google/protobuf/nano/j;)[B

    move-result-object v2

    const/16 v3, 0x8

    invoke-static {v2, v3}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 357
    invoke-virtual {v0}, Lorg/json/JSONObject;->toString()Ljava/lang/String;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    return-object v0

    .line 358
    :catch_0
    move-exception v0

    .line 359
    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method private static a(Ljava/io/InputStream;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 364
    new-instance v1, Ljava/io/BufferedReader;

    new-instance v0, Ljava/io/InputStreamReader;

    const-string v2, "UTF-8"

    invoke-direct {v0, p0, v2}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;Ljava/lang/String;)V

    invoke-direct {v1, v0}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    .line 366
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 367
    invoke-virtual {v1}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v0

    .line 369
    :goto_0
    if-eqz v0, :cond_0

    .line 370
    :try_start_0
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 371
    invoke-virtual {v1}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    goto :goto_0

    .line 374
    :cond_0
    invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V

    .line 375
    invoke-virtual {p0}, Ljava/io/InputStream;->close()V

    .line 377
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 374
    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V

    .line 375
    invoke-virtual {p0}, Ljava/io/InputStream;->close()V

    throw v0
.end method

.method private static a(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 104
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "https"

    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 105
    :cond_0
    const/4 p0, 0x0

    .line 107
    :cond_1
    return-object p0
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 83
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 84
    const-string v0, "Account name is empty or null"

    invoke-static {v0}, Lcom/google/android/gms/icing/c;->a(Ljava/lang/String;)I

    move-object v0, v1

    .line 99
    :goto_0
    return-object v0

    .line 87
    :cond_0
    invoke-static {}, Lcom/google/android/gms/common/ew;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "<redacted>"

    .line 89
    :goto_1
    const-string v2, "Attempting to get auth token for scope/account:%s/%s"

    invoke-static {v2, p2, v0}, Lcom/google/android/gms/icing/c;->a(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)I

    .line 91
    :try_start_0
    iget-object v2, p0, Lcom/google/android/gms/icing/c/a;->b:Landroid/content/Context;

    invoke-static {v2, p1, p2}, Lcom/google/android/gms/auth/r;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Lcom/google/android/gms/auth/q; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v0

    goto :goto_0

    :cond_1
    move-object v0, p1

    .line 87
    goto :goto_1

    .line 92
    :catch_0
    move-exception v2

    .line 93
    const-string v3, "Failed to get auth token for account:%s, error:%s"

    invoke-virtual {v2}, Lcom/google/android/gms/auth/q;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v0, v2}, Lcom/google/android/gms/icing/c;->e(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)I

    move-object v0, v1

    .line 95
    goto :goto_0

    .line 96
    :catch_1
    move-exception v2

    .line 97
    const-string v3, "Failed to get auth token for account:%s, error:%s"

    invoke-virtual {v2}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v0, v2}, Lcom/google/android/gms/icing/c;->e(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)I

    move-object v0, v1

    .line 99
    goto :goto_0
.end method

.method private varargs a(Ljava/lang/String;Ljava/lang/String;[Landroid/util/Pair;)Ljava/lang/String;
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 160
    sget-object v0, Lcom/google/android/gms/icing/a/a;->s:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-direct {p0, p1, v0}, Lcom/google/android/gms/icing/c/a;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 163
    sget-object v0, Lcom/google/android/gms/icing/a/a;->t:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v5

    .line 165
    invoke-virtual {v5, p2}, Landroid/net/Uri$Builder;->appendEncodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 166
    array-length v6, p3

    const/4 v0, 0x0

    move v3, v0

    :goto_0
    if-ge v3, v6, :cond_0

    aget-object v1, p3, v3

    .line 167
    iget-object v0, v1, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    iget-object v1, v1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v5, v0, v1}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 166
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_0

    .line 169
    :cond_0
    invoke-virtual {v5}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/icing/c/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 170
    if-eqz v4, :cond_1

    if-nez v0, :cond_2

    :cond_1
    move-object v0, v2

    .line 177
    :goto_1
    return-object v0

    .line 172
    :cond_2
    :try_start_0
    invoke-static {v0, v4}, Lcom/google/android/gms/icing/c/a;->c(Ljava/lang/String;Ljava/lang/String;)Ljava/net/HttpURLConnection;

    move-result-object v1

    .line 173
    const-string v3, "X-Developer-Key"

    sget-object v0, Lcom/google/android/gms/icing/a/a;->u:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v3, v0}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 174
    const/4 v0, 0x0

    invoke-static {v1, v0}, Lcom/google/android/gms/icing/c/a;->a(Ljava/net/HttpURLConnection;Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_1

    .line 175
    :catch_0
    move-exception v0

    .line 176
    const-string v1, "Failed to send request: %s"

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/google/android/gms/icing/c;->d(Ljava/lang/String;Ljava/lang/Object;)I

    move-object v0, v2

    .line 177
    goto :goto_1
.end method

.method private static a(Ljava/net/HttpURLConnection;Ljava/lang/String;)Ljava/lang/String;
    .locals 4

    .prologue
    .line 116
    if-eqz p1, :cond_0

    .line 117
    const/4 v0, 0x1

    :try_start_0
    invoke-virtual {p0, v0}, Ljava/net/HttpURLConnection;->setDoOutput(Z)V

    .line 118
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    invoke-virtual {p0, v0}, Ljava/net/HttpURLConnection;->setFixedLengthStreamingMode(I)V

    .line 119
    new-instance v0, Ljava/io/BufferedWriter;

    new-instance v1, Ljava/io/OutputStreamWriter;

    invoke-virtual {p0}, Ljava/net/HttpURLConnection;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v2

    const-string v3, "UTF-8"

    invoke-static {v3}, Ljava/nio/charset/Charset;->forName(Ljava/lang/String;)Ljava/nio/charset/Charset;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Ljava/io/OutputStreamWriter;-><init>(Ljava/io/OutputStream;Ljava/nio/charset/Charset;)V

    invoke-direct {v0, v1}, Ljava/io/BufferedWriter;-><init>(Ljava/io/Writer;)V

    .line 121
    invoke-virtual {v0, p1}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 122
    invoke-virtual {v0}, Ljava/io/Writer;->close()V

    .line 125
    :cond_0
    invoke-virtual {p0}, Ljava/net/HttpURLConnection;->getResponseCode()I

    move-result v0

    .line 126
    const/16 v1, 0xc8

    if-eq v0, v1, :cond_3

    .line 127
    const-string v1, "Failed to get response, status code %d"

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/google/android/gms/icing/c;->d(Ljava/lang/String;Ljava/lang/Object;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 128
    if-eqz p0, :cond_1

    .line 136
    :try_start_1
    invoke-virtual {p0}, Ljava/net/HttpURLConnection;->getErrorStream()Ljava/io/InputStream;

    move-result-object v0

    .line 137
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/io/InputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2

    .line 140
    :cond_1
    :goto_0
    const/4 v0, 0x0

    :cond_2
    :goto_1
    return-object v0

    .line 131
    :cond_3
    :try_start_2
    invoke-virtual {p0}, Ljava/net/HttpURLConnection;->getInputStream()Ljava/io/InputStream;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/icing/c/a;->a(Ljava/io/InputStream;)Ljava/lang/String;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v0

    .line 133
    if-eqz p0, :cond_2

    .line 136
    :try_start_3
    invoke-virtual {p0}, Ljava/net/HttpURLConnection;->getErrorStream()Ljava/io/InputStream;

    move-result-object v1

    .line 137
    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/io/InputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0

    goto :goto_1

    :catch_0
    move-exception v1

    goto :goto_1

    .line 133
    :catchall_0
    move-exception v0

    if-eqz p0, :cond_4

    .line 136
    :try_start_4
    invoke-virtual {p0}, Ljava/net/HttpURLConnection;->getErrorStream()Ljava/io/InputStream;

    move-result-object v1

    .line 137
    if-eqz v1, :cond_4

    invoke-virtual {v1}, Ljava/io/InputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1

    .line 140
    :cond_4
    :goto_2
    throw v0

    :catch_1
    move-exception v1

    goto :goto_2

    :catch_2
    move-exception v0

    goto :goto_0
.end method

.method private static b(Ljava/lang/String;)Lcom/google/android/gms/icing/c/a/q;
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 381
    if-nez p0, :cond_0

    .line 396
    :goto_0
    return-object v0

    .line 384
    :cond_0
    :try_start_0
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1, p0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 385
    const-string v2, "serialized_response"

    const-string v3, ""

    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v1

    .line 390
    const/16 v2, 0x8

    :try_start_1
    invoke-static {v1, v2}, Landroid/util/Base64;->decode(Ljava/lang/String;I)[B

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/gms/icing/c/a/q;->a([B)Lcom/google/android/gms/icing/c/a/q;

    move-result-object v1

    .line 392
    const-string v2, "Server response: %s"

    invoke-virtual {v1}, Lcom/google/android/gms/icing/c/a/q;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/gms/icing/c;->a(Ljava/lang/String;Ljava/lang/Object;)I
    :try_end_1
    .catch Lcom/google/protobuf/nano/i; {:try_start_1 .. :try_end_1} :catch_0

    move-object v0, v1

    .line 393
    goto :goto_0

    .line 394
    :catch_0
    move-exception v1

    .line 395
    const-string v2, "Server response bad parse: %s"

    invoke-virtual {v1}, Lcom/google/protobuf/nano/i;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Lcom/google/android/gms/icing/c;->d(Ljava/lang/String;Ljava/lang/Object;)I

    goto :goto_0

    .line 387
    :catch_1
    move-exception v1

    goto :goto_0
.end method

.method private b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 146
    sget-object v0, Lcom/google/android/gms/icing/a/a;->r:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-direct {p0, p1, v0}, Lcom/google/android/gms/icing/c/a;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 147
    sget-object v0, Lcom/google/android/gms/icing/a/a;->q:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/gms/icing/c/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 148
    if-eqz v2, :cond_0

    if-nez v0, :cond_1

    :cond_0
    move-object v0, v1

    .line 154
    :goto_0
    return-object v0

    .line 150
    :cond_1
    :try_start_0
    invoke-static {v0, v2}, Lcom/google/android/gms/icing/c/a;->c(Ljava/lang/String;Ljava/lang/String;)Ljava/net/HttpURLConnection;

    move-result-object v0

    .line 151
    invoke-static {v0, p2}, Lcom/google/android/gms/icing/c/a;->a(Ljava/net/HttpURLConnection;Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_0

    .line 152
    :catch_0
    move-exception v0

    .line 153
    const-string v2, "Failed to send request: %s"

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/google/android/gms/icing/c;->d(Ljava/lang/String;Ljava/lang/Object;)I

    move-object v0, v1

    .line 154
    goto :goto_0
.end method

.method private static c(Ljava/lang/String;Ljava/lang/String;)Ljava/net/HttpURLConnection;
    .locals 4

    .prologue
    .line 341
    new-instance v0, Ljava/net/URL;

    invoke-direct {v0, p0}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    .line 342
    invoke-virtual {v0}, Ljava/net/URL;->openConnection()Ljava/net/URLConnection;

    move-result-object v0

    check-cast v0, Ljava/net/HttpURLConnection;

    .line 343
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 344
    const-string v1, "Authorization"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Bearer "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/net/HttpURLConnection;->addRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 346
    :cond_0
    const-string v1, "User-Agent"

    sget-object v2, Lcom/google/android/gms/icing/c/a;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 347
    const-string v1, "Accept"

    const-string v2, "application/json"

    invoke-virtual {v0, v1, v2}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 348
    const-string v1, "Content-Type"

    const-string v2, "application/json"

    invoke-virtual {v0, v1, v2}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 349
    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;I[Lcom/google/android/gms/icing/c/a/m;)Lcom/google/android/gms/icing/c/a/f;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 269
    if-nez p1, :cond_1

    .line 287
    :cond_0
    :goto_0
    return-object v0

    .line 271
    :cond_1
    new-instance v1, Lcom/google/android/gms/icing/c/a/l;

    invoke-direct {v1}, Lcom/google/android/gms/icing/c/a/l;-><init>()V

    .line 272
    const/4 v2, 0x1

    iput-boolean v2, v1, Lcom/google/android/gms/icing/c/a/l;->a:Z

    .line 273
    iput-object p3, v1, Lcom/google/android/gms/icing/c/a/l;->b:[Lcom/google/android/gms/icing/c/a/m;

    .line 274
    iput p2, v1, Lcom/google/android/gms/icing/c/a/l;->c:I

    .line 276
    new-instance v2, Lcom/google/android/gms/icing/c/a/k;

    invoke-direct {v2}, Lcom/google/android/gms/icing/c/a/k;-><init>()V

    .line 277
    iput-object v1, v2, Lcom/google/android/gms/icing/c/a/k;->d:Lcom/google/android/gms/icing/c/a/l;

    .line 279
    new-instance v1, Lcom/google/android/gms/icing/c/a/i;

    invoke-direct {v1}, Lcom/google/android/gms/icing/c/a/i;-><init>()V

    .line 280
    iput-object v2, v1, Lcom/google/android/gms/icing/c/a/i;->b:Lcom/google/android/gms/icing/c/a/k;

    .line 282
    invoke-static {v1}, Lcom/google/android/gms/icing/c/a;->a(Lcom/google/android/gms/icing/c/a/i;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, p1, v1}, Lcom/google/android/gms/icing/c/a;->b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/gms/icing/c/a;->b(Ljava/lang/String;)Lcom/google/android/gms/icing/c/a/q;

    move-result-object v1

    .line 284
    if-eqz v1, :cond_0

    .line 287
    iget-object v0, v1, Lcom/google/android/gms/icing/c/a/q;->b:Lcom/google/android/gms/icing/c/a/f;

    if-nez v0, :cond_2

    new-instance v0, Lcom/google/android/gms/icing/c/a/f;

    invoke-direct {v0}, Lcom/google/android/gms/icing/c/a/f;-><init>()V

    goto :goto_0

    :cond_2
    iget-object v0, v1, Lcom/google/android/gms/icing/c/a/q;->b:Lcom/google/android/gms/icing/c/a/f;

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;[Lcom/google/android/gms/icing/c/a/b;)Lcom/google/android/gms/icing/c/a/x;
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    const/4 v1, 0x0

    .line 185
    if-nez p1, :cond_1

    .line 220
    :cond_0
    :goto_0
    return-object v1

    .line 189
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/icing/c/a;->b:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/gms/common/util/e;->f(Landroid/content/Context;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 190
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v2

    const/4 v3, -0x1

    if-ne v2, v3, :cond_2

    move-object v0, v1

    .line 193
    :cond_2
    iget-object v2, p0, Lcom/google/android/gms/icing/c/a;->b:Landroid/content/Context;

    invoke-static {v2, v6, v1, v6, v0}, Lcom/google/android/gms/common/util/e;->a(Landroid/content/Context;ZLjava/lang/String;ILjava/lang/Integer;)Lcom/google/af/a/d/a/a/c;

    move-result-object v0

    .line 197
    new-instance v2, Lcom/google/android/gms/icing/c/a/c;

    invoke-direct {v2}, Lcom/google/android/gms/icing/c/a/c;-><init>()V

    .line 198
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    iput-wide v4, v2, Lcom/google/android/gms/icing/c/a/c;->a:J

    .line 199
    iput-object v0, v2, Lcom/google/android/gms/icing/c/a/c;->b:Lcom/google/af/a/d/a/a/c;

    .line 200
    iput-object p3, v2, Lcom/google/android/gms/icing/c/a/c;->c:[Lcom/google/android/gms/icing/c/a/b;

    .line 202
    new-instance v3, Lcom/google/android/gms/icing/c/a/j;

    invoke-direct {v3}, Lcom/google/android/gms/icing/c/a/j;-><init>()V

    .line 203
    iput-object v2, v3, Lcom/google/android/gms/icing/c/a/j;->a:Lcom/google/android/gms/icing/c/a/c;

    .line 205
    new-instance v2, Lcom/google/android/gms/icing/c/a/o;

    invoke-direct {v2}, Lcom/google/android/gms/icing/c/a/o;-><init>()V

    .line 206
    iput-boolean v7, v2, Lcom/google/android/gms/icing/c/a/o;->a:Z

    .line 207
    iput-object p2, v2, Lcom/google/android/gms/icing/c/a/o;->b:Ljava/lang/String;

    .line 209
    iget-object v0, p0, Lcom/google/android/gms/icing/c/a;->b:Landroid/content/Context;

    const-string v4, "location"

    invoke-virtual {v0, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/location/LocationManager;

    const-string v4, "gps"

    invoke-virtual {v0, v4}, Landroid/location/LocationManager;->getLastKnownLocation(Ljava/lang/String;)Landroid/location/Location;

    move-result-object v0

    if-eqz v0, :cond_4

    if-eqz v0, :cond_4

    const/16 v4, 0xc

    invoke-static {v0, v7, v4}, Lcom/google/android/gms/icing/c/c;->a(Landroid/location/Location;II)Ljava/lang/String;

    move-result-object v0

    const/4 v4, 0x4

    invoke-static {v1, v4, v7}, Lcom/google/android/gms/icing/c/c;->a(Landroid/location/Location;II)Ljava/lang/String;

    move-result-object v4

    const-string v5, " "

    invoke-static {v5}, Lcom/google/k/a/x;->a(Ljava/lang/String;)Lcom/google/k/a/x;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/k/a/x;->a()Lcom/google/k/a/x;

    move-result-object v5

    new-array v6, v6, [Ljava/lang/Object;

    invoke-static {v6}, Lcom/google/k/a/ah;->a(Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v7, Lcom/google/k/a/aa;

    invoke-direct {v7, v6, v0, v4}, Lcom/google/k/a/aa;-><init>([Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-virtual {v5, v7}, Lcom/google/k/a/x;->a(Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v0

    .line 210
    :goto_1
    new-instance v4, Lcom/google/android/gms/icing/c/a/k;

    invoke-direct {v4}, Lcom/google/android/gms/icing/c/a/k;-><init>()V

    .line 211
    iput-object v2, v4, Lcom/google/android/gms/icing/c/a/k;->a:Lcom/google/android/gms/icing/c/a/o;

    .line 212
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_3

    iput-object v0, v4, Lcom/google/android/gms/icing/c/a/k;->e:Ljava/lang/String;

    .line 214
    :cond_3
    new-instance v0, Lcom/google/android/gms/icing/c/a/i;

    invoke-direct {v0}, Lcom/google/android/gms/icing/c/a/i;-><init>()V

    .line 215
    iput-object v3, v0, Lcom/google/android/gms/icing/c/a/i;->a:Lcom/google/android/gms/icing/c/a/j;

    .line 216
    iput-object v4, v0, Lcom/google/android/gms/icing/c/a/i;->b:Lcom/google/android/gms/icing/c/a/k;

    .line 218
    invoke-static {v0}, Lcom/google/android/gms/icing/c/a;->a(Lcom/google/android/gms/icing/c/a/i;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lcom/google/android/gms/icing/c/a;->b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/icing/c/a;->b(Ljava/lang/String;)Lcom/google/android/gms/icing/c/a/q;

    move-result-object v0

    .line 220
    if-eqz v0, :cond_0

    iget-object v1, v0, Lcom/google/android/gms/icing/c/a/q;->a:Lcom/google/android/gms/icing/c/a/x;

    goto/16 :goto_0

    :cond_4
    move-object v0, v1

    .line 209
    goto :goto_1
.end method

.method public final a(Ljava/lang/String;I)Ljava/lang/Boolean;
    .locals 6

    .prologue
    const/4 v2, 0x1

    const/4 v5, 0x0

    const/4 v0, 0x0

    .line 296
    if-nez p1, :cond_1

    .line 323
    :cond_0
    :goto_0
    return-object v0

    .line 299
    :cond_1
    packed-switch p2, :pswitch_data_0

    .line 310
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Bad setting id "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 301
    :pswitch_0
    const-string v1, "lookup"

    new-array v2, v2, [Landroid/util/Pair;

    const-string v3, "client"

    const-string v4, "web_app"

    invoke-static {v3, v4}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v3

    aput-object v3, v2, v5

    invoke-direct {p0, p1, v1, v2}, Lcom/google/android/gms/icing/c/a;->a(Ljava/lang/String;Ljava/lang/String;[Landroid/util/Pair;)Ljava/lang/String;

    move-result-object v1

    .line 312
    :goto_1
    if-eqz v1, :cond_0

    .line 316
    :try_start_0
    new-instance v2, Lorg/json/JSONObject;

    invoke-direct {v2, v1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 317
    const-string v1, "history_recording_enabled"

    invoke-virtual {v2, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 320
    const-string v1, "history_recording_enabled"

    invoke-virtual {v2, v1}, Lorg/json/JSONObject;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_0

    .line 305
    :pswitch_1
    const-string v1, "lookup"

    new-array v2, v2, [Landroid/util/Pair;

    const-string v3, "client"

    const-string v4, "device"

    invoke-static {v3, v4}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v3

    aput-object v3, v2, v5

    invoke-direct {p0, p1, v1, v2}, Lcom/google/android/gms/icing/c/a;->a(Ljava/lang/String;Ljava/lang/String;[Landroid/util/Pair;)Ljava/lang/String;

    move-result-object v1

    goto :goto_1

    .line 323
    :catch_0
    move-exception v1

    goto :goto_0

    .line 299
    nop

    :pswitch_data_0
    .packed-switch 0x7
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public final a()Ljava/lang/String;
    .locals 7

    .prologue
    .line 330
    const/4 v1, 0x0

    .line 331
    iget-object v0, p0, Lcom/google/android/gms/icing/c/a;->b:Landroid/content/Context;

    invoke-static {v0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v0

    .line 332
    const-string v2, "com.google"

    invoke-virtual {v0, v2}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v2

    array-length v3, v2

    const/4 v0, 0x0

    move v6, v0

    move-object v0, v1

    move v1, v6

    :goto_0
    if-ge v1, v3, :cond_0

    aget-object v0, v2, v1

    .line 334
    iget-object v4, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    const-string v5, "@google.com"

    invoke-virtual {v4, v5}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 332
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 336
    :cond_0
    iget-object v0, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    return-object v0
.end method

.method public final a(Ljava/lang/String;[Lcom/google/android/gms/icing/c/a/b;)Z
    .locals 9

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 225
    if-nez p1, :cond_1

    .line 264
    :cond_0
    :goto_0
    return v0

    .line 230
    :cond_1
    iget-object v3, p0, Lcom/google/android/gms/icing/c/a;->b:Landroid/content/Context;

    invoke-static {v3}, Lcom/google/android/gms/common/util/e;->f(Landroid/content/Context;)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    .line 231
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v4

    const/4 v5, -0x1

    if-ne v4, v5, :cond_2

    .line 236
    :goto_1
    :try_start_0
    iget-object v3, p0, Lcom/google/android/gms/icing/c/a;->b:Landroid/content/Context;

    const/4 v4, 0x1

    const/4 v5, 0x0

    iget-object v6, p0, Lcom/google/android/gms/icing/c/a;->b:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v6

    iget-object v7, p0, Lcom/google/android/gms/icing/c/a;->b:Landroid/content/Context;

    invoke-virtual {v7}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v7

    const/4 v8, 0x0

    invoke-virtual {v6, v7, v8}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v6

    iget v6, v6, Landroid/content/pm/PackageInfo;->versionCode:I

    invoke-static {v3, v4, v5, v6, v2}, Lcom/google/android/gms/common/util/e;->a(Landroid/content/Context;ZLjava/lang/String;ILjava/lang/Integer;)Lcom/google/af/a/d/a/a/c;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 245
    new-instance v3, Lcom/google/android/gms/icing/c/a/c;

    invoke-direct {v3}, Lcom/google/android/gms/icing/c/a/c;-><init>()V

    .line 246
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    iput-wide v4, v3, Lcom/google/android/gms/icing/c/a/c;->a:J

    .line 247
    iput-object v2, v3, Lcom/google/android/gms/icing/c/a/c;->b:Lcom/google/af/a/d/a/a/c;

    .line 248
    iput-object p2, v3, Lcom/google/android/gms/icing/c/a/c;->c:[Lcom/google/android/gms/icing/c/a/b;

    .line 250
    new-instance v2, Lcom/google/android/gms/icing/c/a/j;

    invoke-direct {v2}, Lcom/google/android/gms/icing/c/a/j;-><init>()V

    .line 251
    iput-object v3, v2, Lcom/google/android/gms/icing/c/a/j;->a:Lcom/google/android/gms/icing/c/a/c;

    .line 253
    new-instance v3, Lcom/google/android/gms/icing/c/a/n;

    invoke-direct {v3}, Lcom/google/android/gms/icing/c/a/n;-><init>()V

    .line 254
    iput-boolean v1, v3, Lcom/google/android/gms/icing/c/a/n;->a:Z

    .line 256
    new-instance v4, Lcom/google/android/gms/icing/c/a/k;

    invoke-direct {v4}, Lcom/google/android/gms/icing/c/a/k;-><init>()V

    .line 257
    iput-object v3, v4, Lcom/google/android/gms/icing/c/a/k;->c:Lcom/google/android/gms/icing/c/a/n;

    .line 259
    new-instance v3, Lcom/google/android/gms/icing/c/a/i;

    invoke-direct {v3}, Lcom/google/android/gms/icing/c/a/i;-><init>()V

    .line 260
    iput-object v2, v3, Lcom/google/android/gms/icing/c/a/i;->a:Lcom/google/android/gms/icing/c/a/j;

    .line 261
    iput-object v4, v3, Lcom/google/android/gms/icing/c/a/i;->b:Lcom/google/android/gms/icing/c/a/k;

    .line 264
    invoke-static {v3}, Lcom/google/android/gms/icing/c/a;->a(Lcom/google/android/gms/icing/c/a/i;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, p1, v2}, Lcom/google/android/gms/icing/c/a;->b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_0

    move v0, v1

    goto :goto_0

    .line 240
    :catch_0
    move-exception v0

    .line 242
    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    :cond_2
    move-object v2, v3

    goto :goto_1
.end method
