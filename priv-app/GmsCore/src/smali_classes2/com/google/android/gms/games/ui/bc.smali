.class final Lcom/google/android/gms/games/ui/bc;
.super Lcom/google/android/gms/games/ui/ay;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/gms/games/ui/aj;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/games/ui/aj;)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 831
    iput-object p1, p0, Lcom/google/android/gms/games/ui/bc;->a:Lcom/google/android/gms/games/ui/aj;

    .line 832
    const/4 v0, 0x4

    invoke-direct {p0, p1, v0, v1, v1}, Lcom/google/android/gms/games/ui/ay;-><init>(Lcom/google/android/gms/games/ui/aj;IIZ)V

    .line 833
    return-void
.end method


# virtual methods
.method public final a(Landroid/view/View;)V
    .locals 5

    .prologue
    .line 837
    sget v0, Lcom/google/android/gms/j;->sQ:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 838
    sget v1, Lcom/google/android/gms/p;->it:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 840
    sget v0, Lcom/google/android/gms/j;->sl:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 842
    iget-object v1, p0, Lcom/google/android/gms/games/ui/bc;->a:Lcom/google/android/gms/games/ui/aj;

    invoke-static {v1}, Lcom/google/android/gms/games/ui/aj;->d(Lcom/google/android/gms/games/ui/aj;)Ljava/util/ArrayList;

    move-result-object v1

    if-nez v1, :cond_0

    .line 844
    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 857
    :goto_0
    sget v0, Lcom/google/android/gms/j;->ui:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 858
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 859
    return-void

    .line 845
    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/games/ui/bc;->a:Lcom/google/android/gms/games/ui/aj;

    invoke-static {v1}, Lcom/google/android/gms/games/ui/aj;->d(Lcom/google/android/gms/games/ui/aj;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-nez v1, :cond_1

    .line 846
    sget v1, Lcom/google/android/gms/p;->el:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0

    .line 848
    :cond_1
    iget-object v1, p0, Lcom/google/android/gms/games/ui/bc;->a:Lcom/google/android/gms/games/ui/aj;

    invoke-static {v1}, Lcom/google/android/gms/games/ui/aj;->d(Lcom/google/android/gms/games/ui/aj;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v3

    .line 849
    new-array v4, v3, [Ljava/lang/String;

    .line 850
    const/4 v1, 0x0

    move v2, v1

    :goto_1
    if-ge v2, v3, :cond_2

    .line 851
    iget-object v1, p0, Lcom/google/android/gms/games/ui/bc;->a:Lcom/google/android/gms/games/ui/aj;

    invoke-static {v1}, Lcom/google/android/gms/games/ui/aj;->d(Lcom/google/android/gms/games/ui/aj;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/common/people/data/AudienceMember;

    invoke-virtual {v1}, Lcom/google/android/gms/common/people/data/AudienceMember;->f()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v2

    .line 850
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_1

    .line 853
    :cond_2
    const-string v1, ", "

    invoke-static {v1, v4}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 854
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method
