.class final Lcom/google/android/gms/games/a/ak;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lcom/google/android/gms/games/a/ab;

.field private final b:Lcom/google/android/gms/common/server/ClientContext;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/games/a/ab;Lcom/google/android/gms/common/server/ClientContext;)V
    .locals 2

    .prologue
    .line 1057
    iput-object p1, p0, Lcom/google/android/gms/games/a/ak;->a:Lcom/google/android/gms/games/a/ab;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1059
    new-instance v0, Lcom/google/android/gms/common/server/ClientContext;

    invoke-direct {v0, p2}, Lcom/google/android/gms/common/server/ClientContext;-><init>(Lcom/google/android/gms/common/server/ClientContext;)V

    iput-object v0, p0, Lcom/google/android/gms/games/a/ak;->b:Lcom/google/android/gms/common/server/ClientContext;

    .line 1060
    iget-object v0, p0, Lcom/google/android/gms/games/a/ak;->b:Lcom/google/android/gms/common/server/ClientContext;

    const-string v1, "https://www.googleapis.com/auth/games"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/server/ClientContext;->b(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/games/a/ak;->b:Lcom/google/android/gms/common/server/ClientContext;

    const-string v1, "https://www.googleapis.com/auth/games.firstparty"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/server/ClientContext;->b(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1062
    iget-object v0, p0, Lcom/google/android/gms/games/a/ak;->b:Lcom/google/android/gms/common/server/ClientContext;

    const-string v1, "https://www.googleapis.com/auth/games"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/server/ClientContext;->a(Ljava/lang/String;)V

    .line 1064
    :cond_0
    return-void
.end method


# virtual methods
.method public final run()V
    .locals 5

    .prologue
    .line 1069
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/games/a/ak;->a:Lcom/google/android/gms/games/a/ab;

    invoke-static {v0}, Lcom/google/android/gms/games/a/ab;->a(Lcom/google/android/gms/games/a/ab;)Lcom/google/android/gms/games/h/a/af;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/games/a/ak;->b:Lcom/google/android/gms/common/server/ClientContext;

    const-string v2, "applications/played"

    iget-object v0, v0, Lcom/google/android/gms/games/h/a/af;->a:Lcom/google/android/gms/common/server/n;

    const/4 v3, 0x1

    const/4 v4, 0x0

    invoke-virtual {v0, v1, v3, v2, v4}, Lcom/google/android/gms/common/server/n;->a(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;Ljava/lang/Object;)V
    :try_end_0
    .catch Lcom/android/volley/ac; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lcom/google/android/gms/auth/q; {:try_start_0 .. :try_end_0} :catch_0

    .line 1075
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0

    .line 1074
    :catch_1
    move-exception v0

    goto :goto_0
.end method
