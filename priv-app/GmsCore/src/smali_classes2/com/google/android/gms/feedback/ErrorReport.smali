.class public Lcom/google/android/gms/feedback/ErrorReport;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field public A:I

.field public B:Ljava/lang/String;

.field public C:Ljava/lang/String;

.field public D:Ljava/lang/String;

.field public E:Landroid/os/Bundle;

.field public F:Z

.field public G:I

.field public H:I

.field public I:Z

.field public J:Ljava/lang/String;

.field public K:Ljava/lang/String;

.field public L:I

.field public M:Ljava/lang/String;

.field public N:Ljava/lang/String;

.field public O:Ljava/lang/String;

.field public P:Ljava/lang/String;

.field public Q:Ljava/lang/String;

.field public R:Ljava/lang/String;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public S:Ljava/lang/String;

.field public T:Lcom/google/android/gms/common/data/BitmapTeleporter;

.field public U:Ljava/lang/String;

.field public V:[Lcom/google/android/gms/feedback/FileTeleporter;

.field public W:[Ljava/lang/String;

.field public X:Z

.field public Y:Ljava/lang/String;

.field public Z:Lcom/google/android/gms/feedback/ThemeSettings;

.field public final a:I

.field public b:Landroid/app/ApplicationErrorReport;
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NewApi"
        }
    .end annotation
.end field

.field public c:Ljava/lang/String;

.field public d:I

.field public e:Ljava/lang/String;

.field public f:Ljava/lang/String;

.field public g:Ljava/lang/String;

.field public h:Ljava/lang/String;

.field public i:Ljava/lang/String;

.field public j:Ljava/lang/String;

.field public k:Ljava/lang/String;

.field public l:I

.field public m:Ljava/lang/String;

.field public n:Ljava/lang/String;

.field public o:Ljava/lang/String;

.field public p:Ljava/lang/String;

.field public q:Ljava/lang/String;

.field public r:[Ljava/lang/String;

.field public s:[Ljava/lang/String;

.field public t:[Ljava/lang/String;

.field public u:Ljava/lang/String;

.field public v:Ljava/lang/String;

.field public w:[B

.field public x:I

.field public y:I

.field public z:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 63
    new-instance v0, Lcom/google/android/gms/feedback/d;

    invoke-direct {v0}, Lcom/google/android/gms/feedback/d;-><init>()V

    sput-object v0, Lcom/google/android/gms/feedback/ErrorReport;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 565
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 74
    new-instance v0, Landroid/app/ApplicationErrorReport;

    invoke-direct {v0}, Landroid/app/ApplicationErrorReport;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/feedback/ErrorReport;->b:Landroid/app/ApplicationErrorReport;

    .line 566
    const/4 v0, 0x6

    iput v0, p0, Lcom/google/android/gms/feedback/ErrorReport;->a:I

    .line 567
    return-void
.end method

.method constructor <init>(ILandroid/app/ApplicationErrorReport;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;[Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[BIIIILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;ZIIZLjava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/common/data/BitmapTeleporter;Ljava/lang/String;[Lcom/google/android/gms/feedback/FileTeleporter;[Ljava/lang/String;ZLjava/lang/String;Lcom/google/android/gms/feedback/ThemeSettings;)V
    .locals 2
    .annotation build Landroid/annotation/TargetApi;
        value = 0xe
    .end annotation

    .prologue
    .line 442
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 74
    new-instance v1, Landroid/app/ApplicationErrorReport;

    invoke-direct {v1}, Landroid/app/ApplicationErrorReport;-><init>()V

    iput-object v1, p0, Lcom/google/android/gms/feedback/ErrorReport;->b:Landroid/app/ApplicationErrorReport;

    .line 443
    iput p1, p0, Lcom/google/android/gms/feedback/ErrorReport;->a:I

    .line 444
    iput-object p2, p0, Lcom/google/android/gms/feedback/ErrorReport;->b:Landroid/app/ApplicationErrorReport;

    .line 445
    iput-object p3, p0, Lcom/google/android/gms/feedback/ErrorReport;->c:Ljava/lang/String;

    .line 446
    iput p4, p0, Lcom/google/android/gms/feedback/ErrorReport;->d:I

    .line 447
    iput-object p5, p0, Lcom/google/android/gms/feedback/ErrorReport;->e:Ljava/lang/String;

    .line 448
    iput-object p6, p0, Lcom/google/android/gms/feedback/ErrorReport;->f:Ljava/lang/String;

    .line 449
    iput-object p7, p0, Lcom/google/android/gms/feedback/ErrorReport;->g:Ljava/lang/String;

    .line 450
    iput-object p8, p0, Lcom/google/android/gms/feedback/ErrorReport;->h:Ljava/lang/String;

    .line 451
    iput-object p9, p0, Lcom/google/android/gms/feedback/ErrorReport;->i:Ljava/lang/String;

    .line 452
    iput-object p10, p0, Lcom/google/android/gms/feedback/ErrorReport;->j:Ljava/lang/String;

    .line 453
    iput-object p11, p0, Lcom/google/android/gms/feedback/ErrorReport;->k:Ljava/lang/String;

    .line 454
    iput p12, p0, Lcom/google/android/gms/feedback/ErrorReport;->l:I

    .line 455
    iput-object p13, p0, Lcom/google/android/gms/feedback/ErrorReport;->m:Ljava/lang/String;

    .line 456
    move-object/from16 v0, p14

    iput-object v0, p0, Lcom/google/android/gms/feedback/ErrorReport;->n:Ljava/lang/String;

    .line 457
    move-object/from16 v0, p15

    iput-object v0, p0, Lcom/google/android/gms/feedback/ErrorReport;->o:Ljava/lang/String;

    .line 458
    move-object/from16 v0, p16

    iput-object v0, p0, Lcom/google/android/gms/feedback/ErrorReport;->p:Ljava/lang/String;

    .line 459
    move-object/from16 v0, p17

    iput-object v0, p0, Lcom/google/android/gms/feedback/ErrorReport;->q:Ljava/lang/String;

    .line 460
    move-object/from16 v0, p18

    iput-object v0, p0, Lcom/google/android/gms/feedback/ErrorReport;->r:[Ljava/lang/String;

    .line 461
    move-object/from16 v0, p19

    iput-object v0, p0, Lcom/google/android/gms/feedback/ErrorReport;->s:[Ljava/lang/String;

    .line 462
    move-object/from16 v0, p20

    iput-object v0, p0, Lcom/google/android/gms/feedback/ErrorReport;->t:[Ljava/lang/String;

    .line 463
    move-object/from16 v0, p21

    iput-object v0, p0, Lcom/google/android/gms/feedback/ErrorReport;->u:Ljava/lang/String;

    .line 464
    move-object/from16 v0, p22

    iput-object v0, p0, Lcom/google/android/gms/feedback/ErrorReport;->v:Ljava/lang/String;

    .line 465
    move-object/from16 v0, p23

    iput-object v0, p0, Lcom/google/android/gms/feedback/ErrorReport;->w:[B

    .line 466
    move/from16 v0, p24

    iput v0, p0, Lcom/google/android/gms/feedback/ErrorReport;->x:I

    .line 467
    move/from16 v0, p25

    iput v0, p0, Lcom/google/android/gms/feedback/ErrorReport;->y:I

    .line 468
    move/from16 v0, p26

    iput v0, p0, Lcom/google/android/gms/feedback/ErrorReport;->z:I

    .line 469
    move/from16 v0, p27

    iput v0, p0, Lcom/google/android/gms/feedback/ErrorReport;->A:I

    .line 470
    move-object/from16 v0, p28

    iput-object v0, p0, Lcom/google/android/gms/feedback/ErrorReport;->B:Ljava/lang/String;

    .line 471
    move-object/from16 v0, p29

    iput-object v0, p0, Lcom/google/android/gms/feedback/ErrorReport;->C:Ljava/lang/String;

    .line 472
    move-object/from16 v0, p30

    iput-object v0, p0, Lcom/google/android/gms/feedback/ErrorReport;->D:Ljava/lang/String;

    .line 473
    move-object/from16 v0, p31

    iput-object v0, p0, Lcom/google/android/gms/feedback/ErrorReport;->E:Landroid/os/Bundle;

    .line 474
    move/from16 v0, p32

    iput-boolean v0, p0, Lcom/google/android/gms/feedback/ErrorReport;->F:Z

    .line 475
    move/from16 v0, p33

    iput v0, p0, Lcom/google/android/gms/feedback/ErrorReport;->G:I

    .line 476
    move/from16 v0, p34

    iput v0, p0, Lcom/google/android/gms/feedback/ErrorReport;->H:I

    .line 477
    move/from16 v0, p35

    iput-boolean v0, p0, Lcom/google/android/gms/feedback/ErrorReport;->I:Z

    .line 478
    move-object/from16 v0, p36

    iput-object v0, p0, Lcom/google/android/gms/feedback/ErrorReport;->J:Ljava/lang/String;

    .line 479
    move-object/from16 v0, p37

    iput-object v0, p0, Lcom/google/android/gms/feedback/ErrorReport;->K:Ljava/lang/String;

    .line 480
    move/from16 v0, p38

    iput v0, p0, Lcom/google/android/gms/feedback/ErrorReport;->L:I

    .line 481
    move-object/from16 v0, p39

    iput-object v0, p0, Lcom/google/android/gms/feedback/ErrorReport;->M:Ljava/lang/String;

    .line 482
    move-object/from16 v0, p40

    iput-object v0, p0, Lcom/google/android/gms/feedback/ErrorReport;->N:Ljava/lang/String;

    .line 483
    move-object/from16 v0, p41

    iput-object v0, p0, Lcom/google/android/gms/feedback/ErrorReport;->O:Ljava/lang/String;

    .line 484
    move-object/from16 v0, p42

    iput-object v0, p0, Lcom/google/android/gms/feedback/ErrorReport;->P:Ljava/lang/String;

    .line 485
    move-object/from16 v0, p43

    iput-object v0, p0, Lcom/google/android/gms/feedback/ErrorReport;->Q:Ljava/lang/String;

    .line 486
    move-object/from16 v0, p44

    iput-object v0, p0, Lcom/google/android/gms/feedback/ErrorReport;->R:Ljava/lang/String;

    .line 487
    move-object/from16 v0, p45

    iput-object v0, p0, Lcom/google/android/gms/feedback/ErrorReport;->S:Ljava/lang/String;

    .line 488
    move-object/from16 v0, p46

    iput-object v0, p0, Lcom/google/android/gms/feedback/ErrorReport;->T:Lcom/google/android/gms/common/data/BitmapTeleporter;

    .line 489
    move-object/from16 v0, p47

    iput-object v0, p0, Lcom/google/android/gms/feedback/ErrorReport;->U:Ljava/lang/String;

    .line 490
    move-object/from16 v0, p48

    iput-object v0, p0, Lcom/google/android/gms/feedback/ErrorReport;->V:[Lcom/google/android/gms/feedback/FileTeleporter;

    .line 491
    move-object/from16 v0, p49

    iput-object v0, p0, Lcom/google/android/gms/feedback/ErrorReport;->W:[Ljava/lang/String;

    .line 492
    move/from16 v0, p50

    iput-boolean v0, p0, Lcom/google/android/gms/feedback/ErrorReport;->X:Z

    .line 493
    move-object/from16 v0, p51

    iput-object v0, p0, Lcom/google/android/gms/feedback/ErrorReport;->Y:Ljava/lang/String;

    .line 494
    move-object/from16 v0, p52

    iput-object v0, p0, Lcom/google/android/gms/feedback/ErrorReport;->Z:Lcom/google/android/gms/feedback/ThemeSettings;

    .line 495
    return-void
.end method

.method constructor <init>(Lcom/google/android/gms/feedback/ErrorReport;)V
    .locals 54
    .annotation build Landroid/annotation/TargetApi;
        value = 0xe
    .end annotation

    .prologue
    .line 574
    move-object/from16 v0, p1

    iget v2, v0, Lcom/google/android/gms/feedback/ErrorReport;->a:I

    move-object/from16 v0, p1

    iget-object v3, v0, Lcom/google/android/gms/feedback/ErrorReport;->b:Landroid/app/ApplicationErrorReport;

    move-object/from16 v0, p1

    iget-object v4, v0, Lcom/google/android/gms/feedback/ErrorReport;->c:Ljava/lang/String;

    move-object/from16 v0, p1

    iget v5, v0, Lcom/google/android/gms/feedback/ErrorReport;->d:I

    move-object/from16 v0, p1

    iget-object v6, v0, Lcom/google/android/gms/feedback/ErrorReport;->e:Ljava/lang/String;

    move-object/from16 v0, p1

    iget-object v7, v0, Lcom/google/android/gms/feedback/ErrorReport;->f:Ljava/lang/String;

    move-object/from16 v0, p1

    iget-object v8, v0, Lcom/google/android/gms/feedback/ErrorReport;->g:Ljava/lang/String;

    move-object/from16 v0, p1

    iget-object v9, v0, Lcom/google/android/gms/feedback/ErrorReport;->h:Ljava/lang/String;

    move-object/from16 v0, p1

    iget-object v10, v0, Lcom/google/android/gms/feedback/ErrorReport;->i:Ljava/lang/String;

    move-object/from16 v0, p1

    iget-object v11, v0, Lcom/google/android/gms/feedback/ErrorReport;->j:Ljava/lang/String;

    move-object/from16 v0, p1

    iget-object v12, v0, Lcom/google/android/gms/feedback/ErrorReport;->k:Ljava/lang/String;

    move-object/from16 v0, p1

    iget v13, v0, Lcom/google/android/gms/feedback/ErrorReport;->l:I

    move-object/from16 v0, p1

    iget-object v14, v0, Lcom/google/android/gms/feedback/ErrorReport;->m:Ljava/lang/String;

    move-object/from16 v0, p1

    iget-object v15, v0, Lcom/google/android/gms/feedback/ErrorReport;->n:Ljava/lang/String;

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/google/android/gms/feedback/ErrorReport;->o:Ljava/lang/String;

    move-object/from16 v16, v0

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/google/android/gms/feedback/ErrorReport;->p:Ljava/lang/String;

    move-object/from16 v17, v0

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/google/android/gms/feedback/ErrorReport;->q:Ljava/lang/String;

    move-object/from16 v18, v0

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/google/android/gms/feedback/ErrorReport;->r:[Ljava/lang/String;

    move-object/from16 v19, v0

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/google/android/gms/feedback/ErrorReport;->s:[Ljava/lang/String;

    move-object/from16 v20, v0

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/google/android/gms/feedback/ErrorReport;->t:[Ljava/lang/String;

    move-object/from16 v21, v0

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/google/android/gms/feedback/ErrorReport;->u:Ljava/lang/String;

    move-object/from16 v22, v0

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/google/android/gms/feedback/ErrorReport;->v:Ljava/lang/String;

    move-object/from16 v23, v0

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/google/android/gms/feedback/ErrorReport;->w:[B

    move-object/from16 v24, v0

    move-object/from16 v0, p1

    iget v0, v0, Lcom/google/android/gms/feedback/ErrorReport;->x:I

    move/from16 v25, v0

    move-object/from16 v0, p1

    iget v0, v0, Lcom/google/android/gms/feedback/ErrorReport;->y:I

    move/from16 v26, v0

    move-object/from16 v0, p1

    iget v0, v0, Lcom/google/android/gms/feedback/ErrorReport;->z:I

    move/from16 v27, v0

    move-object/from16 v0, p1

    iget v0, v0, Lcom/google/android/gms/feedback/ErrorReport;->A:I

    move/from16 v28, v0

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/google/android/gms/feedback/ErrorReport;->B:Ljava/lang/String;

    move-object/from16 v29, v0

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/google/android/gms/feedback/ErrorReport;->C:Ljava/lang/String;

    move-object/from16 v30, v0

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/google/android/gms/feedback/ErrorReport;->D:Ljava/lang/String;

    move-object/from16 v31, v0

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/google/android/gms/feedback/ErrorReport;->E:Landroid/os/Bundle;

    move-object/from16 v32, v0

    move-object/from16 v0, p1

    iget-boolean v0, v0, Lcom/google/android/gms/feedback/ErrorReport;->F:Z

    move/from16 v33, v0

    move-object/from16 v0, p1

    iget v0, v0, Lcom/google/android/gms/feedback/ErrorReport;->G:I

    move/from16 v34, v0

    move-object/from16 v0, p1

    iget v0, v0, Lcom/google/android/gms/feedback/ErrorReport;->H:I

    move/from16 v35, v0

    move-object/from16 v0, p1

    iget-boolean v0, v0, Lcom/google/android/gms/feedback/ErrorReport;->I:Z

    move/from16 v36, v0

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/google/android/gms/feedback/ErrorReport;->J:Ljava/lang/String;

    move-object/from16 v37, v0

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/google/android/gms/feedback/ErrorReport;->K:Ljava/lang/String;

    move-object/from16 v38, v0

    move-object/from16 v0, p1

    iget v0, v0, Lcom/google/android/gms/feedback/ErrorReport;->L:I

    move/from16 v39, v0

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/google/android/gms/feedback/ErrorReport;->M:Ljava/lang/String;

    move-object/from16 v40, v0

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/google/android/gms/feedback/ErrorReport;->N:Ljava/lang/String;

    move-object/from16 v41, v0

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/google/android/gms/feedback/ErrorReport;->O:Ljava/lang/String;

    move-object/from16 v42, v0

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/google/android/gms/feedback/ErrorReport;->P:Ljava/lang/String;

    move-object/from16 v43, v0

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/google/android/gms/feedback/ErrorReport;->Q:Ljava/lang/String;

    move-object/from16 v44, v0

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/google/android/gms/feedback/ErrorReport;->R:Ljava/lang/String;

    move-object/from16 v45, v0

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/google/android/gms/feedback/ErrorReport;->S:Ljava/lang/String;

    move-object/from16 v46, v0

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/google/android/gms/feedback/ErrorReport;->T:Lcom/google/android/gms/common/data/BitmapTeleporter;

    move-object/from16 v47, v0

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/google/android/gms/feedback/ErrorReport;->U:Ljava/lang/String;

    move-object/from16 v48, v0

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/google/android/gms/feedback/ErrorReport;->V:[Lcom/google/android/gms/feedback/FileTeleporter;

    move-object/from16 v49, v0

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/google/android/gms/feedback/ErrorReport;->W:[Ljava/lang/String;

    move-object/from16 v50, v0

    move-object/from16 v0, p1

    iget-boolean v0, v0, Lcom/google/android/gms/feedback/ErrorReport;->X:Z

    move/from16 v51, v0

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/google/android/gms/feedback/ErrorReport;->Y:Ljava/lang/String;

    move-object/from16 v52, v0

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/google/android/gms/feedback/ErrorReport;->Z:Lcom/google/android/gms/feedback/ThemeSettings;

    move-object/from16 v53, v0

    move-object/from16 v1, p0

    invoke-direct/range {v1 .. v53}, Lcom/google/android/gms/feedback/ErrorReport;-><init>(ILandroid/app/ApplicationErrorReport;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;[Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[BIIIILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;ZIIZLjava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/common/data/BitmapTeleporter;Ljava/lang/String;[Lcom/google/android/gms/feedback/FileTeleporter;[Ljava/lang/String;ZLjava/lang/String;Lcom/google/android/gms/feedback/ThemeSettings;)V

    .line 585
    return-void
.end method


# virtual methods
.method public final a(Landroid/graphics/Bitmap;)V
    .locals 3

    .prologue
    .line 640
    :goto_0
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 641
    :cond_0
    const-string v0, "ErrorReport"

    const-string v1, "Bitmap is null or recycled. Cant compress. We will not attach screenshot"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 654
    :cond_1
    return-void

    .line 644
    :cond_2
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 645
    sget-object v1, Landroid/graphics/Bitmap$CompressFormat;->JPEG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v2, 0x46

    invoke-virtual {p1, v1, v2, v0}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    .line 646
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v0, v1}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/feedback/ErrorReport;->v:Ljava/lang/String;

    .line 647
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/feedback/ErrorReport;->y:I

    .line 648
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/feedback/ErrorReport;->x:I

    .line 649
    iget-object v0, p0, Lcom/google/android/gms/feedback/ErrorReport;->v:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    array-length v0, v0

    const/high16 v1, 0x40000

    if-le v0, v1, :cond_1

    .line 650
    const-string v0, "ErrorReport"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Encountered large screenshot, size: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/gms/feedback/ErrorReport;->v:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->getBytes()[B

    move-result-object v2

    array-length v2, v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ",  compressing further."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 652
    iget v0, p0, Lcom/google/android/gms/feedback/ErrorReport;->y:I

    shr-int/lit8 v0, v0, 0x1

    iget v1, p0, Lcom/google/android/gms/feedback/ErrorReport;->x:I

    shr-int/lit8 v1, v1, 0x1

    const/4 v2, 0x1

    invoke-static {p1, v0, v1, v2}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object p1

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .annotation build Landroid/annotation/TargetApi;
        value = 0xe
    .end annotation

    .prologue
    .line 628
    iget-object v0, p0, Lcom/google/android/gms/feedback/ErrorReport;->b:Landroid/app/ApplicationErrorReport;

    iput-object p1, v0, Landroid/app/ApplicationErrorReport;->packageName:Ljava/lang/String;

    .line 629
    iget-object v0, p0, Lcom/google/android/gms/feedback/ErrorReport;->b:Landroid/app/ApplicationErrorReport;

    const/16 v1, 0xb

    iput v1, v0, Landroid/app/ApplicationErrorReport;->type:I

    .line 630
    iget-object v0, p0, Lcom/google/android/gms/feedback/ErrorReport;->b:Landroid/app/ApplicationErrorReport;

    iput-object p2, v0, Landroid/app/ApplicationErrorReport;->installerPackageName:Ljava/lang/String;

    .line 631
    return-void
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 589
    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 0

    .prologue
    .line 594
    invoke-static {p0, p1, p2}, Lcom/google/android/gms/feedback/d;->a(Lcom/google/android/gms/feedback/ErrorReport;Landroid/os/Parcel;I)V

    .line 595
    return-void
.end method
