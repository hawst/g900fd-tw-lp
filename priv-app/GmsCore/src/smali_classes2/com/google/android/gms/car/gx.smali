.class public final Lcom/google/android/gms/car/gx;
.super Lcom/google/android/gms/car/jy;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/car/eb;
.implements Lcom/google/android/gms/car/is;


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x13
.end annotation


# static fields
.field private static volatile ad:Z


# instance fields
.field private A:Lcom/google/android/gms/car/es;

.field private B:Lcom/google/android/gms/car/fx;

.field private C:Lcom/google/android/gms/car/fo;

.field private D:Lcom/google/android/gms/car/dj;

.field private E:Lcom/google/android/gms/car/pc;

.field private final F:Ljava/lang/Object;

.field private G:Lcom/google/android/gms/car/ae;

.field private final H:Lcom/google/android/gms/car/oc;

.field private final I:Landroid/content/Context;

.field private J:Lcom/google/android/gms/car/CarInfoInternal;

.field private K:Lcom/google/android/gms/car/CarUiInfo;

.field private volatile L:Z

.field private M:Ljava/util/concurrent/atomic/AtomicInteger;

.field private N:Lcom/google/android/gms/car/hn;

.field private volatile O:I

.field private volatile P:Z

.field private volatile Q:Z

.field private volatile R:I

.field private volatile S:Z

.field private final T:Lcom/google/android/gms/car/ht;

.field private volatile U:Lcom/google/android/gms/car/qc;

.field private V:Lcom/google/android/gms/car/hq;

.field private W:Lcom/google/android/gms/car/hs;

.field private volatile X:Z

.field private Y:Landroid/content/ComponentName;

.field private Z:J

.field final a:Lcom/google/android/gms/car/CarService;

.field private final aa:Lcom/google/android/gms/car/at;

.field private final ab:Landroid/content/BroadcastReceiver;

.field private final ac:Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;

.field b:Lcom/google/android/gms/car/qd;

.field c:Lcom/google/android/gms/car/no;

.field private final d:Ljava/util/Map;

.field private final e:Lcom/google/android/gms/car/id;

.field private final f:Lcom/google/android/gms/car/ic;

.field private g:Lcom/google/android/gms/car/diagnostics/i;

.field private h:Lcom/google/android/gms/car/hw;

.field private final i:Ljava/lang/Object;

.field private volatile j:Z

.field private volatile k:Z

.field private l:Z

.field private m:Ljava/io/Closeable;

.field private n:Lcom/google/android/gms/car/hx;

.field private volatile o:I

.field private volatile p:Lcom/google/android/gms/car/aw;

.field private volatile q:Lcom/google/android/gms/car/bg;

.field private r:Lcom/google/android/gms/car/gn;

.field private s:Lcom/google/android/gms/car/ip;

.field private t:Lcom/google/android/gms/car/br;

.field private u:Lcom/google/android/gms/car/fu;

.field private v:Lcom/google/android/gms/car/fh;

.field private w:Lcom/google/android/gms/car/ey;

.field private x:Lcom/google/android/gms/car/ga;

.field private final y:Ljava/util/HashMap;

.field private final z:Ljava/util/HashMap;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/car/CarService;Lcom/google/android/gms/car/id;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 241
    invoke-direct {p0}, Lcom/google/android/gms/car/jy;-><init>()V

    .line 120
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/car/gx;->d:Ljava/util/Map;

    .line 132
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/car/gx;->i:Ljava/lang/Object;

    .line 134
    iput-boolean v1, p0, Lcom/google/android/gms/car/gx;->j:Z

    .line 139
    iput-boolean v1, p0, Lcom/google/android/gms/car/gx;->k:Z

    .line 144
    iput-boolean v1, p0, Lcom/google/android/gms/car/gx;->l:Z

    .line 164
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/car/gx;->y:Ljava/util/HashMap;

    .line 166
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/car/gx;->z:Ljava/util/HashMap;

    .line 174
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/car/gx;->F:Ljava/lang/Object;

    .line 182
    iput-boolean v1, p0, Lcom/google/android/gms/car/gx;->L:Z

    .line 187
    iput v1, p0, Lcom/google/android/gms/car/gx;->O:I

    .line 188
    iput-boolean v1, p0, Lcom/google/android/gms/car/gx;->P:Z

    .line 189
    iput-boolean v1, p0, Lcom/google/android/gms/car/gx;->Q:Z

    .line 191
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/car/gx;->R:I

    .line 193
    iput-boolean v1, p0, Lcom/google/android/gms/car/gx;->S:Z

    .line 195
    new-instance v0, Lcom/google/android/gms/car/ht;

    invoke-direct {v0, p0}, Lcom/google/android/gms/car/ht;-><init>(Lcom/google/android/gms/car/gx;)V

    iput-object v0, p0, Lcom/google/android/gms/car/gx;->T:Lcom/google/android/gms/car/ht;

    .line 209
    new-instance v0, Lcom/google/android/gms/car/gy;

    invoke-direct {v0, p0}, Lcom/google/android/gms/car/gy;-><init>(Lcom/google/android/gms/car/gx;)V

    iput-object v0, p0, Lcom/google/android/gms/car/gx;->ab:Landroid/content/BroadcastReceiver;

    .line 230
    new-instance v0, Lcom/google/android/gms/car/hf;

    invoke-direct {v0, p0}, Lcom/google/android/gms/car/hf;-><init>(Lcom/google/android/gms/car/gx;)V

    iput-object v0, p0, Lcom/google/android/gms/car/gx;->ac:Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;

    .line 242
    iput-object p1, p0, Lcom/google/android/gms/car/gx;->a:Lcom/google/android/gms/car/CarService;

    .line 243
    iput-object p2, p0, Lcom/google/android/gms/car/gx;->e:Lcom/google/android/gms/car/id;

    .line 244
    iget-object v0, p0, Lcom/google/android/gms/car/gx;->e:Lcom/google/android/gms/car/id;

    iget-object v1, p0, Lcom/google/android/gms/car/gx;->ac:Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/car/id;->a(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    .line 245
    iget-object v0, p0, Lcom/google/android/gms/car/gx;->e:Lcom/google/android/gms/car/id;

    invoke-virtual {v0}, Lcom/google/android/gms/car/id;->g()Z

    move-result v0

    sput-boolean v0, Lcom/google/android/gms/car/ev;->a:Z

    .line 246
    invoke-virtual {p1}, Lcom/google/android/gms/car/CarService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/car/gx;->I:Landroid/content/Context;

    .line 247
    new-instance v0, Lcom/google/android/gms/car/oc;

    iget-object v1, p0, Lcom/google/android/gms/car/gx;->I:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/google/android/gms/car/oc;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/gms/car/gx;->H:Lcom/google/android/gms/car/oc;

    .line 248
    new-instance v0, Lcom/google/android/gms/car/ic;

    invoke-direct {v0, p1}, Lcom/google/android/gms/car/ic;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/gms/car/gx;->f:Lcom/google/android/gms/car/ic;

    .line 249
    new-instance v0, Lcom/google/android/gms/car/at;

    invoke-direct {v0, p1, p0}, Lcom/google/android/gms/car/at;-><init>(Landroid/content/Context;Lcom/google/android/gms/car/gx;)V

    iput-object v0, p0, Lcom/google/android/gms/car/gx;->aa:Lcom/google/android/gms/car/at;

    .line 250
    return-void
.end method

.method static synthetic A(Lcom/google/android/gms/car/gx;)Lcom/google/android/gms/car/fu;
    .locals 1

    .prologue
    .line 69
    iget-object v0, p0, Lcom/google/android/gms/car/gx;->u:Lcom/google/android/gms/car/fu;

    return-object v0
.end method

.method static synthetic B(Lcom/google/android/gms/car/gx;)Lcom/google/android/gms/car/fh;
    .locals 1

    .prologue
    .line 69
    iget-object v0, p0, Lcom/google/android/gms/car/gx;->v:Lcom/google/android/gms/car/fh;

    return-object v0
.end method

.method static synthetic C(Lcom/google/android/gms/car/gx;)Lcom/google/android/gms/car/ey;
    .locals 1

    .prologue
    .line 69
    iget-object v0, p0, Lcom/google/android/gms/car/gx;->w:Lcom/google/android/gms/car/ey;

    return-object v0
.end method

.method static synthetic D(Lcom/google/android/gms/car/gx;)Lcom/google/android/gms/car/ga;
    .locals 1

    .prologue
    .line 69
    iget-object v0, p0, Lcom/google/android/gms/car/gx;->x:Lcom/google/android/gms/car/ga;

    return-object v0
.end method

.method static synthetic E(Lcom/google/android/gms/car/gx;)Ljava/util/HashMap;
    .locals 1

    .prologue
    .line 69
    iget-object v0, p0, Lcom/google/android/gms/car/gx;->y:Ljava/util/HashMap;

    return-object v0
.end method

.method static synthetic F(Lcom/google/android/gms/car/gx;)Ljava/util/HashMap;
    .locals 1

    .prologue
    .line 69
    iget-object v0, p0, Lcom/google/android/gms/car/gx;->z:Ljava/util/HashMap;

    return-object v0
.end method

.method static synthetic G(Lcom/google/android/gms/car/gx;)Lcom/google/android/gms/car/fx;
    .locals 1

    .prologue
    .line 69
    iget-object v0, p0, Lcom/google/android/gms/car/gx;->B:Lcom/google/android/gms/car/fx;

    return-object v0
.end method

.method static synthetic H(Lcom/google/android/gms/car/gx;)V
    .locals 15

    .prologue
    const/4 v3, 0x2

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 69
    iget-object v5, p0, Lcom/google/android/gms/car/gx;->i:Ljava/lang/Object;

    monitor-enter v5

    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/gms/car/gx;->j:Z

    if-nez v0, :cond_1

    const-string v0, "CAR.SERVICE"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "CAR.SERVICE"

    const-string v1, "car disconnected while service discovery"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    monitor-exit v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_0
    return-void

    :cond_1
    :try_start_1
    iget-object v6, p0, Lcom/google/android/gms/car/gx;->E:Lcom/google/android/gms/car/pc;

    const/4 v0, 0x3

    new-array v7, v0, [I

    fill-array-data v7, :array_0

    iget-object v0, v6, Lcom/google/android/gms/car/pc;->d:Lcom/google/android/gms/car/senderprotocol/k;

    iget-boolean v0, v0, Lcom/google/android/gms/car/senderprotocol/k;->g:Z

    if-nez v0, :cond_3

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "startAllServices before service discovery"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :catch_0
    move-exception v0

    const/4 v1, 0x3

    :try_start_2
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v1, v0}, Lcom/google/android/gms/car/gx;->a(ILjava/lang/String;)V

    :cond_2
    :goto_1
    monitor-exit v5
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v5

    throw v0

    :cond_3
    :try_start_3
    iget-object v0, v6, Lcom/google/android/gms/car/pc;->d:Lcom/google/android/gms/car/senderprotocol/k;

    iget-object v0, v0, Lcom/google/android/gms/car/senderprotocol/k;->h:Lcom/google/android/gms/car/senderprotocol/p;

    iget-boolean v0, v0, Lcom/google/android/gms/car/senderprotocol/p;->a:Z

    if-eqz v0, :cond_4

    new-instance v0, Lcom/google/android/gms/car/senderprotocol/be;

    iget-object v4, v6, Lcom/google/android/gms/car/pc;->c:Lcom/google/android/gms/car/gx;

    iget-object v4, v4, Lcom/google/android/gms/car/gx;->s:Lcom/google/android/gms/car/ip;

    iget-object v8, v6, Lcom/google/android/gms/car/pc;->c:Lcom/google/android/gms/car/gx;

    invoke-direct {v0, v4, v8}, Lcom/google/android/gms/car/senderprotocol/be;-><init>(Lcom/google/android/gms/car/ip;Lcom/google/android/gms/car/gx;)V

    iput-object v0, v6, Lcom/google/android/gms/car/pc;->h:Lcom/google/android/gms/car/senderprotocol/be;

    iget-object v0, v6, Lcom/google/android/gms/car/pc;->d:Lcom/google/android/gms/car/senderprotocol/k;

    iget-object v4, v6, Lcom/google/android/gms/car/pc;->d:Lcom/google/android/gms/car/senderprotocol/k;

    iget-object v4, v4, Lcom/google/android/gms/car/senderprotocol/k;->h:Lcom/google/android/gms/car/senderprotocol/p;

    iget v4, v4, Lcom/google/android/gms/car/senderprotocol/p;->b:I

    iget-object v8, v6, Lcom/google/android/gms/car/pc;->h:Lcom/google/android/gms/car/senderprotocol/be;

    const/4 v9, 0x1

    invoke-virtual {v0, v4, v8, v9}, Lcom/google/android/gms/car/senderprotocol/k;->a(ILcom/google/android/gms/car/senderprotocol/az;I)Lcom/google/android/gms/car/senderprotocol/e;

    iget v0, v6, Lcom/google/android/gms/car/pc;->p:I

    add-int/lit8 v0, v0, 0x1

    iput v0, v6, Lcom/google/android/gms/car/pc;->p:I

    :cond_4
    iget-object v0, v6, Lcom/google/android/gms/car/pc;->d:Lcom/google/android/gms/car/senderprotocol/k;

    iget-object v0, v0, Lcom/google/android/gms/car/senderprotocol/k;->l:Lcom/google/android/gms/car/senderprotocol/p;

    iget-boolean v0, v0, Lcom/google/android/gms/car/senderprotocol/p;->a:Z

    if-eqz v0, :cond_5

    new-instance v0, Lcom/google/android/gms/car/senderprotocol/ah;

    iget-object v4, v6, Lcom/google/android/gms/car/pc;->c:Lcom/google/android/gms/car/gx;

    iget-object v4, v4, Lcom/google/android/gms/car/gx;->A:Lcom/google/android/gms/car/es;

    iget-object v8, v6, Lcom/google/android/gms/car/pc;->c:Lcom/google/android/gms/car/gx;

    invoke-direct {v0, v4, v8}, Lcom/google/android/gms/car/senderprotocol/ah;-><init>(Lcom/google/android/gms/car/gw;Lcom/google/android/gms/car/gx;)V

    iput-object v0, v6, Lcom/google/android/gms/car/pc;->f:Lcom/google/android/gms/car/senderprotocol/ah;

    iget-object v0, v6, Lcom/google/android/gms/car/pc;->d:Lcom/google/android/gms/car/senderprotocol/k;

    iget-object v4, v6, Lcom/google/android/gms/car/pc;->d:Lcom/google/android/gms/car/senderprotocol/k;

    iget-object v4, v4, Lcom/google/android/gms/car/senderprotocol/k;->l:Lcom/google/android/gms/car/senderprotocol/p;

    iget v4, v4, Lcom/google/android/gms/car/senderprotocol/p;->b:I

    iget-object v8, v6, Lcom/google/android/gms/car/pc;->f:Lcom/google/android/gms/car/senderprotocol/ah;

    const/4 v9, 0x1

    invoke-virtual {v0, v4, v8, v9}, Lcom/google/android/gms/car/senderprotocol/k;->a(ILcom/google/android/gms/car/senderprotocol/az;I)Lcom/google/android/gms/car/senderprotocol/e;

    iget v0, v6, Lcom/google/android/gms/car/pc;->p:I

    add-int/lit8 v0, v0, 0x1

    iput v0, v6, Lcom/google/android/gms/car/pc;->p:I

    :cond_5
    iget-object v0, v6, Lcom/google/android/gms/car/pc;->d:Lcom/google/android/gms/car/senderprotocol/k;

    iget-object v0, v0, Lcom/google/android/gms/car/senderprotocol/k;->m:Lcom/google/android/gms/car/senderprotocol/p;

    iget-boolean v0, v0, Lcom/google/android/gms/car/senderprotocol/p;->a:Z

    if-eqz v0, :cond_6

    new-instance v0, Lcom/google/android/gms/car/senderprotocol/c;

    iget-object v4, v6, Lcom/google/android/gms/car/pc;->c:Lcom/google/android/gms/car/gx;

    iget-object v4, v4, Lcom/google/android/gms/car/gx;->t:Lcom/google/android/gms/car/br;

    iget-object v8, v6, Lcom/google/android/gms/car/pc;->c:Lcom/google/android/gms/car/gx;

    invoke-direct {v0, v4, v8}, Lcom/google/android/gms/car/senderprotocol/c;-><init>(Lcom/google/android/gms/car/br;Lcom/google/android/gms/car/gx;)V

    iput-object v0, v6, Lcom/google/android/gms/car/pc;->j:Lcom/google/android/gms/car/senderprotocol/c;

    iget-object v0, v6, Lcom/google/android/gms/car/pc;->d:Lcom/google/android/gms/car/senderprotocol/k;

    iget-object v4, v6, Lcom/google/android/gms/car/pc;->d:Lcom/google/android/gms/car/senderprotocol/k;

    iget-object v4, v4, Lcom/google/android/gms/car/senderprotocol/k;->m:Lcom/google/android/gms/car/senderprotocol/p;

    iget v4, v4, Lcom/google/android/gms/car/senderprotocol/p;->b:I

    iget-object v8, v6, Lcom/google/android/gms/car/pc;->j:Lcom/google/android/gms/car/senderprotocol/c;

    const/4 v9, 0x0

    invoke-virtual {v0, v4, v8, v9}, Lcom/google/android/gms/car/senderprotocol/k;->a(ILcom/google/android/gms/car/senderprotocol/az;I)Lcom/google/android/gms/car/senderprotocol/e;

    iget v0, v6, Lcom/google/android/gms/car/pc;->p:I

    add-int/lit8 v0, v0, 0x1

    iput v0, v6, Lcom/google/android/gms/car/pc;->p:I

    :cond_6
    array-length v8, v7

    move v4, v1

    :goto_2
    if-ge v4, v8, :cond_9

    aget v9, v7, v4

    add-int/lit8 v10, v9, -0x1

    iget-object v0, v6, Lcom/google/android/gms/car/pc;->d:Lcom/google/android/gms/car/senderprotocol/k;

    invoke-virtual {v0, v9}, Lcom/google/android/gms/car/senderprotocol/k;->b(I)Lcom/google/android/gms/car/senderprotocol/p;

    move-result-object v0

    iget-boolean v0, v0, Lcom/google/android/gms/car/senderprotocol/p;->a:Z

    if-eqz v0, :cond_8

    iget-object v0, v6, Lcom/google/android/gms/car/pc;->c:Lcom/google/android/gms/car/gx;

    iget-object v0, v0, Lcom/google/android/gms/car/gx;->q:Lcom/google/android/gms/car/bg;

    iget-object v11, v0, Lcom/google/android/gms/car/bg;->d:[Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;

    packed-switch v9, :pswitch_data_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unsupported stream type "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_0
    move v0, v1

    :goto_3
    aget-object v0, v11, v0

    if-eqz v0, :cond_7

    iget-object v11, v6, Lcom/google/android/gms/car/pc;->e:[Lcom/google/android/gms/car/senderprotocol/a;

    new-instance v12, Lcom/google/android/gms/car/senderprotocol/a;

    invoke-virtual {v0}, Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;->f()I

    move-result v13

    iget-object v14, v6, Lcom/google/android/gms/car/pc;->c:Lcom/google/android/gms/car/gx;

    invoke-direct {v12, v13, v0, v0, v14}, Lcom/google/android/gms/car/senderprotocol/a;-><init>(ILcom/google/android/gms/car/gw;Lcom/google/android/gms/car/senderprotocol/b;Lcom/google/android/gms/car/gx;)V

    aput-object v12, v11, v10

    iget-object v0, v6, Lcom/google/android/gms/car/pc;->d:Lcom/google/android/gms/car/senderprotocol/k;

    iget-object v11, v6, Lcom/google/android/gms/car/pc;->d:Lcom/google/android/gms/car/senderprotocol/k;

    invoke-virtual {v11, v9}, Lcom/google/android/gms/car/senderprotocol/k;->b(I)Lcom/google/android/gms/car/senderprotocol/p;

    move-result-object v9

    iget v9, v9, Lcom/google/android/gms/car/senderprotocol/p;->b:I

    iget-object v11, v6, Lcom/google/android/gms/car/pc;->e:[Lcom/google/android/gms/car/senderprotocol/a;

    aget-object v10, v11, v10

    const/4 v11, 0x2

    invoke-virtual {v0, v9, v10, v11}, Lcom/google/android/gms/car/senderprotocol/k;->a(ILcom/google/android/gms/car/senderprotocol/az;I)Lcom/google/android/gms/car/senderprotocol/e;

    :cond_7
    iget v0, v6, Lcom/google/android/gms/car/pc;->p:I

    add-int/lit8 v0, v0, 0x1

    iput v0, v6, Lcom/google/android/gms/car/pc;->p:I

    :cond_8
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto :goto_2

    :pswitch_1
    move v0, v2

    goto :goto_3

    :pswitch_2
    move v0, v3

    goto :goto_3

    :cond_9
    iget-object v0, v6, Lcom/google/android/gms/car/pc;->d:Lcom/google/android/gms/car/senderprotocol/k;

    iget-object v0, v0, Lcom/google/android/gms/car/senderprotocol/k;->j:Lcom/google/android/gms/car/senderprotocol/p;

    iget-boolean v0, v0, Lcom/google/android/gms/car/senderprotocol/p;->a:Z

    if-eqz v0, :cond_a

    new-instance v0, Lcom/google/android/gms/car/senderprotocol/ar;

    iget-object v1, v6, Lcom/google/android/gms/car/pc;->c:Lcom/google/android/gms/car/gx;

    iget-object v1, v1, Lcom/google/android/gms/car/gx;->q:Lcom/google/android/gms/car/bg;

    iget-object v1, v1, Lcom/google/android/gms/car/bg;->e:Lcom/google/android/gms/car/nt;

    iget-object v2, v6, Lcom/google/android/gms/car/pc;->c:Lcom/google/android/gms/car/gx;

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/car/senderprotocol/ar;-><init>(Lcom/google/android/gms/car/nt;Lcom/google/android/gms/car/gx;)V

    iput-object v0, v6, Lcom/google/android/gms/car/pc;->i:Lcom/google/android/gms/car/senderprotocol/ar;

    iget-object v0, v6, Lcom/google/android/gms/car/pc;->d:Lcom/google/android/gms/car/senderprotocol/k;

    iget-object v1, v6, Lcom/google/android/gms/car/pc;->d:Lcom/google/android/gms/car/senderprotocol/k;

    iget-object v1, v1, Lcom/google/android/gms/car/senderprotocol/k;->j:Lcom/google/android/gms/car/senderprotocol/p;

    iget v1, v1, Lcom/google/android/gms/car/senderprotocol/p;->b:I

    iget-object v2, v6, Lcom/google/android/gms/car/pc;->i:Lcom/google/android/gms/car/senderprotocol/ar;

    const/4 v3, 0x4

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/gms/car/senderprotocol/k;->a(ILcom/google/android/gms/car/senderprotocol/az;I)Lcom/google/android/gms/car/senderprotocol/e;

    iget v0, v6, Lcom/google/android/gms/car/pc;->p:I

    add-int/lit8 v0, v0, 0x1

    iput v0, v6, Lcom/google/android/gms/car/pc;->p:I

    :cond_a
    iget-object v0, v6, Lcom/google/android/gms/car/pc;->d:Lcom/google/android/gms/car/senderprotocol/k;

    iget-object v0, v0, Lcom/google/android/gms/car/senderprotocol/k;->n:Lcom/google/android/gms/car/senderprotocol/p;

    iget-boolean v0, v0, Lcom/google/android/gms/car/senderprotocol/p;->a:Z

    if-eqz v0, :cond_b

    new-instance v0, Lcom/google/android/gms/car/senderprotocol/at;

    iget-object v1, v6, Lcom/google/android/gms/car/pc;->c:Lcom/google/android/gms/car/gx;

    iget-object v1, v1, Lcom/google/android/gms/car/gx;->u:Lcom/google/android/gms/car/fu;

    iget-object v2, v6, Lcom/google/android/gms/car/pc;->c:Lcom/google/android/gms/car/gx;

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/car/senderprotocol/at;-><init>(Lcom/google/android/gms/car/fu;Lcom/google/android/gms/car/gx;)V

    iput-object v0, v6, Lcom/google/android/gms/car/pc;->k:Lcom/google/android/gms/car/senderprotocol/at;

    iget-object v0, v6, Lcom/google/android/gms/car/pc;->d:Lcom/google/android/gms/car/senderprotocol/k;

    iget-object v1, v6, Lcom/google/android/gms/car/pc;->d:Lcom/google/android/gms/car/senderprotocol/k;

    iget-object v1, v1, Lcom/google/android/gms/car/senderprotocol/k;->n:Lcom/google/android/gms/car/senderprotocol/p;

    iget v1, v1, Lcom/google/android/gms/car/senderprotocol/p;->b:I

    iget-object v2, v6, Lcom/google/android/gms/car/pc;->k:Lcom/google/android/gms/car/senderprotocol/at;

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/gms/car/senderprotocol/k;->a(ILcom/google/android/gms/car/senderprotocol/az;I)Lcom/google/android/gms/car/senderprotocol/e;

    iget v0, v6, Lcom/google/android/gms/car/pc;->p:I

    add-int/lit8 v0, v0, 0x1

    iput v0, v6, Lcom/google/android/gms/car/pc;->p:I

    :cond_b
    iget-object v0, v6, Lcom/google/android/gms/car/pc;->d:Lcom/google/android/gms/car/senderprotocol/k;

    iget-object v0, v0, Lcom/google/android/gms/car/senderprotocol/k;->o:Lcom/google/android/gms/car/senderprotocol/p;

    iget-boolean v0, v0, Lcom/google/android/gms/car/senderprotocol/p;->a:Z

    if-eqz v0, :cond_c

    new-instance v0, Lcom/google/android/gms/car/senderprotocol/an;

    iget-object v1, v6, Lcom/google/android/gms/car/pc;->c:Lcom/google/android/gms/car/gx;

    iget-object v1, v1, Lcom/google/android/gms/car/gx;->v:Lcom/google/android/gms/car/fh;

    iget-object v2, v6, Lcom/google/android/gms/car/pc;->c:Lcom/google/android/gms/car/gx;

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/car/senderprotocol/an;-><init>(Lcom/google/android/gms/car/fh;Lcom/google/android/gms/car/gx;)V

    iput-object v0, v6, Lcom/google/android/gms/car/pc;->l:Lcom/google/android/gms/car/senderprotocol/an;

    iget-object v0, v6, Lcom/google/android/gms/car/pc;->d:Lcom/google/android/gms/car/senderprotocol/k;

    iget-object v1, v6, Lcom/google/android/gms/car/pc;->d:Lcom/google/android/gms/car/senderprotocol/k;

    iget-object v1, v1, Lcom/google/android/gms/car/senderprotocol/k;->o:Lcom/google/android/gms/car/senderprotocol/p;

    iget v1, v1, Lcom/google/android/gms/car/senderprotocol/p;->b:I

    iget-object v2, v6, Lcom/google/android/gms/car/pc;->l:Lcom/google/android/gms/car/senderprotocol/an;

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/gms/car/senderprotocol/k;->a(ILcom/google/android/gms/car/senderprotocol/az;I)Lcom/google/android/gms/car/senderprotocol/e;

    iget v0, v6, Lcom/google/android/gms/car/pc;->p:I

    add-int/lit8 v0, v0, 0x1

    iput v0, v6, Lcom/google/android/gms/car/pc;->p:I

    :cond_c
    iget-object v0, v6, Lcom/google/android/gms/car/pc;->d:Lcom/google/android/gms/car/senderprotocol/k;

    iget-object v0, v0, Lcom/google/android/gms/car/senderprotocol/k;->p:Lcom/google/android/gms/car/senderprotocol/p;

    iget-boolean v0, v0, Lcom/google/android/gms/car/senderprotocol/p;->a:Z

    if-eqz v0, :cond_d

    new-instance v0, Lcom/google/android/gms/car/senderprotocol/ak;

    iget-object v1, v6, Lcom/google/android/gms/car/pc;->c:Lcom/google/android/gms/car/gx;

    iget-object v1, v1, Lcom/google/android/gms/car/gx;->w:Lcom/google/android/gms/car/ey;

    iget-object v2, v6, Lcom/google/android/gms/car/pc;->c:Lcom/google/android/gms/car/gx;

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/car/senderprotocol/ak;-><init>(Lcom/google/android/gms/car/ey;Lcom/google/android/gms/car/gx;)V

    iput-object v0, v6, Lcom/google/android/gms/car/pc;->m:Lcom/google/android/gms/car/senderprotocol/ak;

    iget-object v0, v6, Lcom/google/android/gms/car/pc;->d:Lcom/google/android/gms/car/senderprotocol/k;

    iget-object v1, v6, Lcom/google/android/gms/car/pc;->d:Lcom/google/android/gms/car/senderprotocol/k;

    iget-object v1, v1, Lcom/google/android/gms/car/senderprotocol/k;->p:Lcom/google/android/gms/car/senderprotocol/p;

    iget v1, v1, Lcom/google/android/gms/car/senderprotocol/p;->b:I

    iget-object v2, v6, Lcom/google/android/gms/car/pc;->m:Lcom/google/android/gms/car/senderprotocol/ak;

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/gms/car/senderprotocol/k;->a(ILcom/google/android/gms/car/senderprotocol/az;I)Lcom/google/android/gms/car/senderprotocol/e;

    iget v0, v6, Lcom/google/android/gms/car/pc;->p:I

    add-int/lit8 v0, v0, 0x1

    iput v0, v6, Lcom/google/android/gms/car/pc;->p:I

    :cond_d
    iget-object v0, v6, Lcom/google/android/gms/car/pc;->d:Lcom/google/android/gms/car/senderprotocol/k;

    iget-object v0, v0, Lcom/google/android/gms/car/senderprotocol/k;->q:Lcom/google/android/gms/car/senderprotocol/p;

    iget-boolean v0, v0, Lcom/google/android/gms/car/senderprotocol/p;->a:Z

    if-eqz v0, :cond_e

    new-instance v0, Lcom/google/android/gms/car/senderprotocol/ax;

    iget-object v1, v6, Lcom/google/android/gms/car/pc;->c:Lcom/google/android/gms/car/gx;

    iget-object v1, v1, Lcom/google/android/gms/car/gx;->x:Lcom/google/android/gms/car/ga;

    iget-object v2, v6, Lcom/google/android/gms/car/pc;->c:Lcom/google/android/gms/car/gx;

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/car/senderprotocol/ax;-><init>(Lcom/google/android/gms/car/ga;Lcom/google/android/gms/car/gx;)V

    iput-object v0, v6, Lcom/google/android/gms/car/pc;->n:Lcom/google/android/gms/car/senderprotocol/ax;

    iget-object v0, v6, Lcom/google/android/gms/car/pc;->d:Lcom/google/android/gms/car/senderprotocol/k;

    iget-object v1, v6, Lcom/google/android/gms/car/pc;->d:Lcom/google/android/gms/car/senderprotocol/k;

    iget-object v1, v1, Lcom/google/android/gms/car/senderprotocol/k;->q:Lcom/google/android/gms/car/senderprotocol/p;

    iget v1, v1, Lcom/google/android/gms/car/senderprotocol/p;->b:I

    iget-object v2, v6, Lcom/google/android/gms/car/pc;->n:Lcom/google/android/gms/car/senderprotocol/ax;

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/gms/car/senderprotocol/k;->a(ILcom/google/android/gms/car/senderprotocol/az;I)Lcom/google/android/gms/car/senderprotocol/e;

    iget v0, v6, Lcom/google/android/gms/car/pc;->p:I

    add-int/lit8 v0, v0, 0x1

    iput v0, v6, Lcom/google/android/gms/car/pc;->p:I

    :cond_e
    iget-object v0, v6, Lcom/google/android/gms/car/pc;->d:Lcom/google/android/gms/car/senderprotocol/k;

    iget-object v0, v0, Lcom/google/android/gms/car/senderprotocol/k;->r:Lcom/google/android/gms/car/senderprotocol/p;

    iget-boolean v0, v0, Lcom/google/android/gms/car/senderprotocol/p;->a:Z

    if-eqz v0, :cond_f

    new-instance v0, Lcom/google/android/gms/car/senderprotocol/av;

    iget-object v1, v6, Lcom/google/android/gms/car/pc;->c:Lcom/google/android/gms/car/gx;

    iget-object v1, v1, Lcom/google/android/gms/car/gx;->B:Lcom/google/android/gms/car/fx;

    iget-object v2, v6, Lcom/google/android/gms/car/pc;->c:Lcom/google/android/gms/car/gx;

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/car/senderprotocol/av;-><init>(Lcom/google/android/gms/car/fx;Lcom/google/android/gms/car/gx;)V

    iput-object v0, v6, Lcom/google/android/gms/car/pc;->o:Lcom/google/android/gms/car/senderprotocol/av;

    iget-object v0, v6, Lcom/google/android/gms/car/pc;->d:Lcom/google/android/gms/car/senderprotocol/k;

    iget-object v1, v6, Lcom/google/android/gms/car/pc;->d:Lcom/google/android/gms/car/senderprotocol/k;

    iget-object v1, v1, Lcom/google/android/gms/car/senderprotocol/k;->r:Lcom/google/android/gms/car/senderprotocol/p;

    iget v1, v1, Lcom/google/android/gms/car/senderprotocol/p;->b:I

    iget-object v2, v6, Lcom/google/android/gms/car/pc;->o:Lcom/google/android/gms/car/senderprotocol/av;

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/gms/car/senderprotocol/k;->a(ILcom/google/android/gms/car/senderprotocol/az;I)Lcom/google/android/gms/car/senderprotocol/e;

    iget v0, v6, Lcom/google/android/gms/car/pc;->p:I

    add-int/lit8 v0, v0, 0x1

    iput v0, v6, Lcom/google/android/gms/car/pc;->p:I

    :cond_f
    const-string v0, "CAR.GAL"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_10

    const-string v0, "CAR.GAL"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget v2, v6, Lcom/google/android/gms/car/pc;->p:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " Car services started."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_10
    iget v0, v6, Lcom/google/android/gms/car/pc;->p:I

    iget-object v1, p0, Lcom/google/android/gms/car/gx;->M:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v1, v0}, Ljava/util/concurrent/atomic/AtomicInteger;->addAndGet(I)I

    move-result v0

    if-nez v0, :cond_2

    invoke-direct {p0}, Lcom/google/android/gms/car/gx;->P()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto/16 :goto_1

    nop

    :array_0
    .array-data 4
        0x3
        0x1
        0x2
    .end array-data

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_0
    .end packed-switch
.end method

.method static synthetic I(Lcom/google/android/gms/car/gx;)V
    .locals 9

    .prologue
    const/4 v8, 0x3

    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 69
    iget-object v0, p0, Lcom/google/android/gms/car/gx;->I:Landroid/content/Context;

    sget-object v3, Lcom/google/android/gms/car/ae;->b:Landroid/content/Intent;

    invoke-static {v0, v3}, Lcom/google/android/gms/car/ae;->a(Landroid/content/Context;Landroid/content/Intent;)Z

    move-result v4

    iget-object v0, p0, Lcom/google/android/gms/car/gx;->a:Lcom/google/android/gms/car/CarService;

    invoke-static {v0}, Lcom/google/android/gms/car/ae;->a(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v0

    sget-object v3, Lcom/google/android/gms/car/ae;->f:Landroid/content/ComponentName;

    invoke-direct {p0, v0, v3}, Lcom/google/android/gms/car/gx;->a(Landroid/content/Intent;Landroid/content/ComponentName;)Z

    move-result v5

    if-nez v5, :cond_4

    sget-object v3, Lcom/google/android/gms/car/ae;->h:Landroid/content/ComponentName;

    invoke-direct {p0, v0, v3}, Lcom/google/android/gms/car/gx;->a(Landroid/content/Intent;Landroid/content/ComponentName;)Z

    move-result v3

    if-eqz v3, :cond_4

    move v3, v1

    :goto_0
    if-nez v5, :cond_5

    if-nez v3, :cond_5

    sget-object v6, Lcom/google/android/gms/car/ae;->a:Landroid/content/ComponentName;

    invoke-direct {p0, v0, v6}, Lcom/google/android/gms/car/gx;->a(Landroid/content/Intent;Landroid/content/ComponentName;)Z

    move-result v0

    if-eqz v0, :cond_5

    move v0, v1

    :goto_1
    if-nez v4, :cond_6

    if-nez v5, :cond_0

    if-nez v3, :cond_0

    if-eqz v0, :cond_6

    :cond_0
    move v0, v1

    :goto_2
    if-eqz v0, :cond_1

    const-string v3, "CAR.SERVICE"

    invoke-static {v3, v8}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_1

    const-string v3, "CAR.SERVICE"

    const-string v4, "In plt mode. Bypassing all prompts."

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    if-eqz v0, :cond_7

    const-string v0, "CAR.SERVICE"

    invoke-static {v0, v8}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, "CAR.SERVICE"

    const-string v2, "skip car setup for standalone CarService"

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    invoke-direct {p0}, Lcom/google/android/gms/car/gx;->T()V

    iget-object v0, p0, Lcom/google/android/gms/car/gx;->U:Lcom/google/android/gms/car/qc;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/car/qc;->a(Z)V

    :cond_3
    :goto_3
    iget-object v0, p0, Lcom/google/android/gms/car/gx;->E:Lcom/google/android/gms/car/pc;

    invoke-virtual {v0}, Lcom/google/android/gms/car/pc;->a()V

    :goto_4
    return-void

    :cond_4
    move v3, v2

    goto :goto_0

    :cond_5
    move v0, v2

    goto :goto_1

    :cond_6
    move v0, v2

    goto :goto_2

    :cond_7
    iget-object v3, p0, Lcom/google/android/gms/car/gx;->U:Lcom/google/android/gms/car/qc;

    iget-object v0, v3, Lcom/google/android/gms/car/qc;->a:Landroid/content/Context;

    iget-object v4, v3, Lcom/google/android/gms/car/qc;->b:Lcom/google/android/gms/car/CarInfoInternal;

    invoke-static {v0, v4}, Lcom/google/android/gms/car/ia;->c(Landroid/content/Context;Lcom/google/android/gms/car/CarInfoInternal;)Z

    move-result v0

    if-eqz v0, :cond_9

    move v0, v2

    :goto_5
    if-nez v0, :cond_b

    const-string v0, "CAR.SERVICE"

    invoke-static {v0, v8}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_8

    const-string v0, "CAR.SERVICE"

    const-string v1, "car already rejected"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_8
    invoke-direct {p0}, Lcom/google/android/gms/car/gx;->U()V

    goto :goto_4

    :cond_9
    iget-object v0, v3, Lcom/google/android/gms/car/qc;->a:Landroid/content/Context;

    iget-object v4, v3, Lcom/google/android/gms/car/qc;->b:Lcom/google/android/gms/car/CarInfoInternal;

    invoke-static {v0, v4}, Lcom/google/android/gms/car/ia;->a(Landroid/content/Context;Lcom/google/android/gms/car/CarInfoInternal;)Z

    move-result v0

    iput-boolean v0, v3, Lcom/google/android/gms/car/qc;->e:Z

    iget-boolean v0, v3, Lcom/google/android/gms/car/qc;->e:Z

    if-nez v0, :cond_a

    move v0, v1

    :goto_6
    iput-boolean v0, v3, Lcom/google/android/gms/car/qc;->f:Z

    iget-object v0, v3, Lcom/google/android/gms/car/qc;->b:Lcom/google/android/gms/car/CarInfoInternal;

    iget-boolean v0, v0, Lcom/google/android/gms/car/CarInfoInternal;->j:Z

    iput-boolean v0, v3, Lcom/google/android/gms/car/qc;->g:Z

    move v0, v1

    goto :goto_5

    :cond_a
    move v0, v2

    goto :goto_6

    :cond_b
    iget-object v0, p0, Lcom/google/android/gms/car/gx;->e:Lcom/google/android/gms/car/id;

    invoke-virtual {v0}, Lcom/google/android/gms/car/id;->l()I

    move-result v0

    int-to-long v4, v0

    const-wide/16 v6, 0x1e

    cmp-long v3, v4, v6

    if-lez v3, :cond_d

    const-string v0, "CAR.SERVICE"

    invoke-static {v0, v8}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_c

    const-string v0, "CAR.SERVICE"

    const-string v3, "Too many incomplete connection attempts. Resetting db."

    invoke-static {v0, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_c
    iget-object v0, p0, Lcom/google/android/gms/car/gx;->a:Lcom/google/android/gms/car/CarService;

    invoke-static {v0}, Lcom/google/android/gms/car/ia;->b(Landroid/content/Context;)V

    move v0, v2

    :cond_d
    add-int/lit8 v0, v0, 0x1

    iget-object v3, p0, Lcom/google/android/gms/car/gx;->e:Lcom/google/android/gms/car/id;

    invoke-virtual {v3, v0}, Lcom/google/android/gms/car/id;->b(I)V

    const-string v3, "CAR.SERVICE"

    invoke-static {v3, v8}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_e

    const-string v3, "CAR.SERVICE"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Connection count = "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_e
    invoke-direct {p0}, Lcom/google/android/gms/car/gx;->T()V

    iget v0, p0, Lcom/google/android/gms/car/gx;->o:I

    if-nez v0, :cond_10

    const-string v0, "CAR.SERVICE"

    invoke-static {v0, v8}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_f

    const-string v0, "CAR.SERVICE"

    const-string v2, "car always allowed in emulator"

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_f
    iget-object v0, p0, Lcom/google/android/gms/car/gx;->J:Lcom/google/android/gms/car/CarInfoInternal;

    iget-object v2, p0, Lcom/google/android/gms/car/gx;->J:Lcom/google/android/gms/car/CarInfoInternal;

    iget-object v2, v2, Lcom/google/android/gms/car/CarInfoInternal;->i:Ljava/lang/String;

    iput-object v2, v0, Lcom/google/android/gms/car/CarInfoInternal;->e:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/gms/car/gx;->U:Lcom/google/android/gms/car/qc;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/car/qc;->a(Z)V

    goto/16 :goto_3

    :cond_10
    iget-object v0, p0, Lcom/google/android/gms/car/gx;->U:Lcom/google/android/gms/car/qc;

    iget-boolean v3, v0, Lcom/google/android/gms/car/qc;->e:Z

    if-eqz v3, :cond_11

    iget-boolean v3, v0, Lcom/google/android/gms/car/qc;->g:Z

    if-eqz v3, :cond_11

    iget-object v0, v0, Lcom/google/android/gms/car/qc;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/gms/car/b;->a(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_12

    :cond_11
    move v2, v1

    :cond_12
    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/google/android/gms/car/gx;->U:Lcom/google/android/gms/car/qc;

    iget-object v0, v2, Lcom/google/android/gms/car/qc;->a:Landroid/content/Context;

    const-string v3, "keyguard"

    invoke-virtual {v0, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/KeyguardManager;

    invoke-virtual {v0}, Landroid/app/KeyguardManager;->isKeyguardLocked()Z

    move-result v0

    if-eqz v0, :cond_13

    const-string v0, "CAR.SETUP"

    invoke-static {v0, v8}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_13

    const-string v0, "CAR.SETUP"

    const-string v3, "first run and screen locked"

    invoke-static {v0, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_13
    iput-boolean v1, v2, Lcom/google/android/gms/car/qc;->d:Z

    new-instance v0, Landroid/content/Intent;

    iget-object v1, v2, Lcom/google/android/gms/car/qc;->a:Landroid/content/Context;

    const-class v3, Lcom/google/android/gms/car/SetupActivity;

    invoke-direct {v0, v1, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    const/high16 v1, 0x10000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    iget-object v1, v2, Lcom/google/android/gms/car/qc;->a:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_4
.end method

.method static synthetic J(Lcom/google/android/gms/car/gx;)V
    .locals 2

    .prologue
    .line 69
    const-string v0, "CAR.SERVICE"

    const/4 v1, 0x2

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "CAR.SERVICE"

    const-string v1, "resetting system state on crash"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/car/gx;->G:Lcom/google/android/gms/car/ae;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/google/android/gms/car/ae;->e()V

    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/car/gx;->a:Lcom/google/android/gms/car/CarService;

    invoke-virtual {v0}, Lcom/google/android/gms/car/CarService;->d()V

    iget-object v0, p0, Lcom/google/android/gms/car/gx;->b:Lcom/google/android/gms/car/qd;

    invoke-virtual {v0}, Lcom/google/android/gms/car/qd;->d()V

    iget-object v0, p0, Lcom/google/android/gms/car/gx;->b:Lcom/google/android/gms/car/qd;

    invoke-virtual {v0}, Lcom/google/android/gms/car/qd;->b()V

    invoke-direct {p0}, Lcom/google/android/gms/car/gx;->V()V

    return-void
.end method

.method static synthetic K(Lcom/google/android/gms/car/gx;)V
    .locals 0

    .prologue
    .line 69
    invoke-direct {p0}, Lcom/google/android/gms/car/gx;->U()V

    return-void
.end method

.method static synthetic M()Z
    .locals 1

    .prologue
    .line 69
    sget-boolean v0, Lcom/google/android/gms/car/gx;->ad:Z

    return v0
.end method

.method static synthetic N()Z
    .locals 1

    .prologue
    .line 69
    const/4 v0, 0x1

    sput-boolean v0, Lcom/google/android/gms/car/gx;->ad:Z

    return v0
.end method

.method private O()V
    .locals 2

    .prologue
    .line 685
    iget-boolean v0, p0, Lcom/google/android/gms/car/gx;->j:Z

    if-nez v0, :cond_0

    .line 686
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "CarNotConnected"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 688
    :cond_0
    return-void
.end method

.method private P()V
    .locals 3

    .prologue
    .line 722
    const-string v0, "CAR.SERVICE"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 723
    const-string v0, "CAR.SERVICE"

    const-string v1, "completeStartup, car service ready"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 725
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/car/gx;->q:Lcom/google/android/gms/car/bg;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/car/gx;->q:Lcom/google/android/gms/car/bg;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/google/android/gms/car/bg;->f:Z

    .line 726
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/car/gx;->K:Lcom/google/android/gms/car/CarUiInfo;

    if-nez v0, :cond_2

    new-instance v0, Ljava/util/Random;

    invoke-direct {v0}, Ljava/util/Random;-><init>()V

    new-instance v1, Lcom/google/android/gms/car/CarUiInfo;

    invoke-virtual {v0}, Ljava/util/Random;->nextBoolean()Z

    move-result v2

    invoke-virtual {v0}, Ljava/util/Random;->nextBoolean()Z

    move-result v0

    invoke-direct {v1, v2, v0}, Lcom/google/android/gms/car/CarUiInfo;-><init>(ZZ)V

    iput-object v1, p0, Lcom/google/android/gms/car/gx;->K:Lcom/google/android/gms/car/CarUiInfo;

    const-string v0, "CAR.SERVICE"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Car UI info wasn\'t set. Using random values: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/gms/car/gx;->K:Lcom/google/android/gms/car/CarUiInfo;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 727
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/car/gx;->e:Lcom/google/android/gms/car/id;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/gms/car/id;->b(I)V

    new-instance v0, Lcom/google/android/gms/car/hi;

    invoke-direct {v0, p0}, Lcom/google/android/gms/car/hi;-><init>(Lcom/google/android/gms/car/gx;)V

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/google/android/gms/car/of;->a(Landroid/os/Looper;Ljava/lang/Runnable;)V

    .line 728
    invoke-virtual {p0}, Lcom/google/android/gms/car/gx;->w()V

    .line 729
    return-void
.end method

.method private Q()V
    .locals 0

    .prologue
    .line 1267
    invoke-virtual {p0}, Lcom/google/android/gms/car/gx;->u()V

    .line 1268
    invoke-direct {p0}, Lcom/google/android/gms/car/gx;->R()V

    .line 1269
    return-void
.end method

.method private R()V
    .locals 2

    .prologue
    .line 1272
    iget-object v0, p0, Lcom/google/android/gms/car/gx;->a:Lcom/google/android/gms/car/CarService;

    invoke-static {}, Lcom/google/android/gms/car/gx;->getCallingUid()I

    move-result v1

    invoke-static {v0, v1}, Lcom/google/android/gms/car/of;->a(Landroid/content/Context;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1273
    new-instance v0, Ljava/lang/SecurityException;

    const-string v1, "Wrong signature"

    invoke-direct {v0, v1}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1275
    :cond_0
    return-void
.end method

.method private S()V
    .locals 7

    .prologue
    const/4 v1, 0x0

    const/4 v6, 0x0

    .line 1558
    iget-object v0, p0, Lcom/google/android/gms/car/gx;->e:Lcom/google/android/gms/car/id;

    invoke-virtual {v0, v6}, Lcom/google/android/gms/car/id;->a(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    .line 1559
    iget-object v0, p0, Lcom/google/android/gms/car/gx;->H:Lcom/google/android/gms/car/oc;

    invoke-virtual {v0}, Lcom/google/android/gms/car/oc;->a()V

    .line 1560
    iget-object v0, p0, Lcom/google/android/gms/car/gx;->q:Lcom/google/android/gms/car/bg;

    if-eqz v0, :cond_5

    .line 1561
    iget-object v2, p0, Lcom/google/android/gms/car/gx;->q:Lcom/google/android/gms/car/bg;

    iput-boolean v1, v2, Lcom/google/android/gms/car/bg;->f:Z

    iget-object v0, v2, Lcom/google/android/gms/car/bg;->e:Lcom/google/android/gms/car/nt;

    if-eqz v0, :cond_0

    iget-object v0, v2, Lcom/google/android/gms/car/bg;->e:Lcom/google/android/gms/car/nt;

    iput-boolean v1, v0, Lcom/google/android/gms/car/nt;->f:Z

    invoke-virtual {v0}, Lcom/google/android/gms/car/nt;->b()V

    iget-object v3, v0, Lcom/google/android/gms/car/nt;->d:Ljava/util/List;

    monitor-enter v3

    :try_start_0
    iget-object v0, v0, Lcom/google/android/gms/car/nt;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iput-object v6, v2, Lcom/google/android/gms/car/bg;->e:Lcom/google/android/gms/car/nt;

    :cond_0
    move v0, v1

    :goto_0
    iget-object v3, v2, Lcom/google/android/gms/car/bg;->c:[Lcom/google/android/gms/car/g;

    array-length v3, v3

    if-ge v0, v3, :cond_3

    iget-object v3, v2, Lcom/google/android/gms/car/bg;->c:[Lcom/google/android/gms/car/g;

    aget-object v3, v3, v0

    if-eqz v3, :cond_1

    iget-object v3, v2, Lcom/google/android/gms/car/bg;->c:[Lcom/google/android/gms/car/g;

    aget-object v3, v3, v0

    invoke-virtual {v3}, Lcom/google/android/gms/car/g;->c()V

    iget-object v3, v2, Lcom/google/android/gms/car/bg;->c:[Lcom/google/android/gms/car/g;

    aput-object v6, v3, v0

    :cond_1
    iget-object v3, v2, Lcom/google/android/gms/car/bg;->d:[Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;

    aget-object v3, v3, v0

    if-eqz v3, :cond_2

    iget-object v3, v2, Lcom/google/android/gms/car/bg;->d:[Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;

    aget-object v3, v3, v0

    invoke-virtual {v3}, Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;->d()V

    iget-object v3, v2, Lcom/google/android/gms/car/bg;->d:[Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;

    aput-object v6, v3, v0

    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v3

    throw v0

    :cond_3
    iget-object v3, v2, Lcom/google/android/gms/car/bg;->b:Landroid/util/SparseArray;

    monitor-enter v3

    :try_start_1
    iget-object v0, v2, Lcom/google/android/gms/car/bg;->b:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->clear()V

    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    iget-object v3, v2, Lcom/google/android/gms/car/bg;->a:Ljava/util/List;

    monitor-enter v3

    :try_start_2
    iget-object v0, v2, Lcom/google/android/gms/car/bg;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/car/bj;

    invoke-virtual {v0}, Lcom/google/android/gms/car/bj;->b()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    goto :goto_1

    :catchall_1
    move-exception v0

    monitor-exit v3

    throw v0

    :catchall_2
    move-exception v0

    monitor-exit v3

    throw v0

    :cond_4
    :try_start_3
    iget-object v0, v2, Lcom/google/android/gms/car/bg;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    monitor-exit v3
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 1562
    iput-object v6, p0, Lcom/google/android/gms/car/gx;->q:Lcom/google/android/gms/car/bg;

    .line 1564
    :cond_5
    iget-object v0, p0, Lcom/google/android/gms/car/gx;->r:Lcom/google/android/gms/car/gn;

    if-eqz v0, :cond_6

    .line 1565
    iget-object v0, p0, Lcom/google/android/gms/car/gx;->r:Lcom/google/android/gms/car/gn;

    invoke-virtual {v0}, Lcom/google/android/gms/car/gn;->d()V

    .line 1566
    iput-object v6, p0, Lcom/google/android/gms/car/gx;->r:Lcom/google/android/gms/car/gn;

    .line 1568
    :cond_6
    iget-object v0, p0, Lcom/google/android/gms/car/gx;->s:Lcom/google/android/gms/car/ip;

    if-eqz v0, :cond_7

    .line 1569
    iget-object v0, p0, Lcom/google/android/gms/car/gx;->s:Lcom/google/android/gms/car/ip;

    invoke-virtual {v0}, Lcom/google/android/gms/car/ip;->a()V

    .line 1570
    iput-object v6, p0, Lcom/google/android/gms/car/gx;->s:Lcom/google/android/gms/car/ip;

    .line 1572
    :cond_7
    iget-object v0, p0, Lcom/google/android/gms/car/gx;->t:Lcom/google/android/gms/car/br;

    if-eqz v0, :cond_8

    .line 1573
    iget-object v0, p0, Lcom/google/android/gms/car/gx;->t:Lcom/google/android/gms/car/br;

    invoke-virtual {v0}, Lcom/google/android/gms/car/br;->a()V

    .line 1574
    iput-object v6, p0, Lcom/google/android/gms/car/gx;->t:Lcom/google/android/gms/car/br;

    .line 1576
    :cond_8
    iget-object v0, p0, Lcom/google/android/gms/car/gx;->u:Lcom/google/android/gms/car/fu;

    if-eqz v0, :cond_9

    .line 1577
    iget-object v0, p0, Lcom/google/android/gms/car/gx;->u:Lcom/google/android/gms/car/fu;

    iput-boolean v1, v0, Lcom/google/android/gms/car/fu;->b:Z

    iget-object v2, v0, Lcom/google/android/gms/car/fu;->a:Ljava/util/List;

    monitor-enter v2

    :try_start_4
    iget-object v0, v0, Lcom/google/android/gms/car/fu;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    monitor-exit v2
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_4

    .line 1578
    iput-object v6, p0, Lcom/google/android/gms/car/gx;->u:Lcom/google/android/gms/car/fu;

    .line 1580
    :cond_9
    iget-object v0, p0, Lcom/google/android/gms/car/gx;->A:Lcom/google/android/gms/car/es;

    if-eqz v0, :cond_a

    .line 1581
    iget-object v0, p0, Lcom/google/android/gms/car/gx;->A:Lcom/google/android/gms/car/es;

    .line 1582
    iput-object v6, p0, Lcom/google/android/gms/car/gx;->A:Lcom/google/android/gms/car/es;

    .line 1584
    :cond_a
    iget-object v0, p0, Lcom/google/android/gms/car/gx;->v:Lcom/google/android/gms/car/fh;

    if-eqz v0, :cond_c

    .line 1585
    iget-object v0, p0, Lcom/google/android/gms/car/gx;->v:Lcom/google/android/gms/car/fh;

    iput-boolean v1, v0, Lcom/google/android/gms/car/fh;->c:Z

    iget-object v2, v0, Lcom/google/android/gms/car/fh;->a:Ljava/lang/Object;

    monitor-enter v2

    :try_start_5
    iget-object v3, v0, Lcom/google/android/gms/car/fh;->b:Lcom/google/android/gms/car/fi;

    if-eqz v3, :cond_b

    iget-object v3, v0, Lcom/google/android/gms/car/fh;->b:Lcom/google/android/gms/car/fi;

    iget-object v3, v3, Lcom/google/android/gms/car/fi;->a:Lcom/google/android/gms/car/lt;

    invoke-interface {v3}, Lcom/google/android/gms/car/lt;->asBinder()Landroid/os/IBinder;

    move-result-object v3

    iget-object v4, v0, Lcom/google/android/gms/car/fh;->b:Lcom/google/android/gms/car/fi;

    const/4 v5, 0x0

    invoke-interface {v3, v4, v5}, Landroid/os/IBinder;->unlinkToDeath(Landroid/os/IBinder$DeathRecipient;I)Z

    const/4 v3, 0x0

    iput-object v3, v0, Lcom/google/android/gms/car/fh;->b:Lcom/google/android/gms/car/fi;

    :cond_b
    monitor-exit v2
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_5

    .line 1586
    iput-object v6, p0, Lcom/google/android/gms/car/gx;->v:Lcom/google/android/gms/car/fh;

    .line 1588
    :cond_c
    iget-object v0, p0, Lcom/google/android/gms/car/gx;->w:Lcom/google/android/gms/car/ey;

    if-eqz v0, :cond_e

    .line 1589
    iget-object v0, p0, Lcom/google/android/gms/car/gx;->w:Lcom/google/android/gms/car/ey;

    iput-boolean v1, v0, Lcom/google/android/gms/car/ey;->c:Z

    iget-object v2, v0, Lcom/google/android/gms/car/ey;->a:Ljava/lang/Object;

    monitor-enter v2

    :try_start_6
    iget-object v3, v0, Lcom/google/android/gms/car/ey;->b:Lcom/google/android/gms/car/ez;

    if-eqz v3, :cond_d

    iget-object v3, v0, Lcom/google/android/gms/car/ey;->b:Lcom/google/android/gms/car/ez;

    iget-object v3, v3, Lcom/google/android/gms/car/ez;->a:Lcom/google/android/gms/car/ln;

    invoke-interface {v3}, Lcom/google/android/gms/car/ln;->asBinder()Landroid/os/IBinder;

    move-result-object v3

    iget-object v4, v0, Lcom/google/android/gms/car/ey;->b:Lcom/google/android/gms/car/ez;

    const/4 v5, 0x0

    invoke-interface {v3, v4, v5}, Landroid/os/IBinder;->unlinkToDeath(Landroid/os/IBinder$DeathRecipient;I)Z

    const/4 v3, 0x0

    iput-object v3, v0, Lcom/google/android/gms/car/ey;->b:Lcom/google/android/gms/car/ez;

    :cond_d
    monitor-exit v2
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_6

    .line 1590
    iput-object v6, p0, Lcom/google/android/gms/car/gx;->w:Lcom/google/android/gms/car/ey;

    .line 1592
    :cond_e
    iget-object v0, p0, Lcom/google/android/gms/car/gx;->x:Lcom/google/android/gms/car/ga;

    if-eqz v0, :cond_10

    .line 1593
    iget-object v0, p0, Lcom/google/android/gms/car/gx;->x:Lcom/google/android/gms/car/ga;

    iput-boolean v1, v0, Lcom/google/android/gms/car/ga;->c:Z

    iget-object v1, v0, Lcom/google/android/gms/car/ga;->a:Ljava/lang/Object;

    monitor-enter v1

    :try_start_7
    iget-object v2, v0, Lcom/google/android/gms/car/ga;->b:Lcom/google/android/gms/car/gb;

    if-eqz v2, :cond_f

    invoke-virtual {v0}, Lcom/google/android/gms/car/ga;->b()V

    :cond_f
    monitor-exit v1
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_7

    .line 1594
    iput-object v6, p0, Lcom/google/android/gms/car/gx;->x:Lcom/google/android/gms/car/ga;

    .line 1596
    :cond_10
    iget-object v0, p0, Lcom/google/android/gms/car/gx;->D:Lcom/google/android/gms/car/dj;

    if-eqz v0, :cond_11

    .line 1597
    iget-object v0, p0, Lcom/google/android/gms/car/gx;->D:Lcom/google/android/gms/car/dj;

    invoke-virtual {v0}, Lcom/google/android/gms/car/dj;->a()V

    .line 1598
    iput-object v6, p0, Lcom/google/android/gms/car/gx;->x:Lcom/google/android/gms/car/ga;

    .line 1600
    :cond_11
    iget-object v1, p0, Lcom/google/android/gms/car/gx;->y:Ljava/util/HashMap;

    monitor-enter v1

    .line 1601
    :try_start_8
    iget-object v0, p0, Lcom/google/android/gms/car/gx;->y:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_13

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/car/ig;

    .line 1602
    const/4 v3, 0x0

    iput-boolean v3, v0, Lcom/google/android/gms/car/ig;->c:Z

    iget-object v3, v0, Lcom/google/android/gms/car/ig;->d:Ljava/lang/Object;

    monitor-enter v3
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_3

    :try_start_9
    iget-object v4, v0, Lcom/google/android/gms/car/ig;->e:Lcom/google/android/gms/car/ih;
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_8

    if-eqz v4, :cond_12

    :try_start_a
    iget-object v4, v0, Lcom/google/android/gms/car/ig;->e:Lcom/google/android/gms/car/ih;

    iget-object v4, v4, Lcom/google/android/gms/car/ih;->a:Lcom/google/android/gms/car/ng;

    invoke-interface {v4}, Lcom/google/android/gms/car/ng;->b()V
    :try_end_a
    .catch Landroid/os/RemoteException; {:try_start_a .. :try_end_a} :catch_0
    .catchall {:try_start_a .. :try_end_a} :catchall_8

    :cond_12
    :goto_3
    :try_start_b
    monitor-exit v3
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_8

    :try_start_c
    invoke-virtual {v0}, Lcom/google/android/gms/car/ig;->b()V

    invoke-virtual {v0}, Lcom/google/android/gms/car/ig;->c()V
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_3

    goto :goto_2

    .line 1606
    :catchall_3
    move-exception v0

    monitor-exit v1

    throw v0

    .line 1577
    :catchall_4
    move-exception v0

    monitor-exit v2

    throw v0

    .line 1585
    :catchall_5
    move-exception v0

    monitor-exit v2

    throw v0

    .line 1589
    :catchall_6
    move-exception v0

    monitor-exit v2

    throw v0

    .line 1593
    :catchall_7
    move-exception v0

    monitor-exit v1

    throw v0

    .line 1602
    :catch_0
    move-exception v4

    :try_start_d
    const-string v4, "CAR.VENDOR"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Error in onDisconnected for vendor extension "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v6, v0, Lcom/google/android/gms/car/ig;->a:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_d
    .catchall {:try_start_d .. :try_end_d} :catchall_8

    goto :goto_3

    :catchall_8
    move-exception v0

    :try_start_e
    monitor-exit v3

    throw v0

    .line 1604
    :cond_13
    iget-object v0, p0, Lcom/google/android/gms/car/gx;->y:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    .line 1605
    iget-object v0, p0, Lcom/google/android/gms/car/gx;->z:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    .line 1606
    monitor-exit v1
    :try_end_e
    .catchall {:try_start_e .. :try_end_e} :catchall_3

    return-void
.end method

.method private T()V
    .locals 10

    .prologue
    const/4 v9, 0x3

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1721
    :try_start_0
    iget-object v4, p0, Lcom/google/android/gms/car/gx;->E:Lcom/google/android/gms/car/pc;

    iget-object v5, v4, Lcom/google/android/gms/car/pc;->d:Lcom/google/android/gms/car/senderprotocol/k;

    iget-object v2, v5, Lcom/google/android/gms/car/senderprotocol/k;->t:Lcom/google/android/gms/car/senderprotocol/q;

    iget-object v2, v2, Lcom/google/android/gms/car/senderprotocol/q;->e:Lcom/google/android/c/b/cq;

    iget-object v6, v2, Lcom/google/android/c/b/cq;->a:[Lcom/google/android/c/b/co;

    array-length v7, v6

    move v3, v1

    :goto_0
    if-ge v3, v7, :cond_1

    aget-object v2, v6, v3

    iget-object v8, v2, Lcom/google/android/c/b/co;->b:Lcom/google/android/c/b/cm;

    if-eqz v8, :cond_0

    :goto_1
    if-nez v2, :cond_2

    move v2, v1

    :goto_2
    if-eqz v2, :cond_4

    new-instance v1, Lcom/google/android/gms/car/senderprotocol/ba;

    iget-object v2, v4, Lcom/google/android/gms/car/pc;->c:Lcom/google/android/gms/car/gx;

    iget-object v2, v2, Lcom/google/android/gms/car/gx;->r:Lcom/google/android/gms/car/gn;

    iget-object v3, v4, Lcom/google/android/gms/car/pc;->c:Lcom/google/android/gms/car/gx;

    invoke-direct {v1, v2, v3}, Lcom/google/android/gms/car/senderprotocol/ba;-><init>(Lcom/google/android/gms/car/gn;Lcom/google/android/gms/car/gx;)V

    iput-object v1, v4, Lcom/google/android/gms/car/pc;->g:Lcom/google/android/gms/car/senderprotocol/ba;

    iget-object v1, v4, Lcom/google/android/gms/car/pc;->d:Lcom/google/android/gms/car/senderprotocol/k;

    iget-object v2, v4, Lcom/google/android/gms/car/pc;->d:Lcom/google/android/gms/car/senderprotocol/k;

    iget-object v2, v2, Lcom/google/android/gms/car/senderprotocol/k;->k:Lcom/google/android/gms/car/senderprotocol/p;

    iget v2, v2, Lcom/google/android/gms/car/senderprotocol/p;->b:I

    iget-object v3, v4, Lcom/google/android/gms/car/pc;->g:Lcom/google/android/gms/car/senderprotocol/ba;

    const/4 v4, 0x3

    invoke-virtual {v1, v2, v3, v4}, Lcom/google/android/gms/car/senderprotocol/k;->a(ILcom/google/android/gms/car/senderprotocol/az;I)Lcom/google/android/gms/car/senderprotocol/e;

    :goto_3
    if-eqz v0, :cond_5

    .line 1722
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/car/gx;->L:Z

    .line 1730
    :goto_4
    return-void

    .line 1721
    :cond_0
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_0

    :cond_1
    const/4 v2, 0x0

    goto :goto_1

    :cond_2
    iget-object v3, v2, Lcom/google/android/c/b/co;->b:Lcom/google/android/c/b/cm;

    if-nez v3, :cond_3

    move v2, v1

    goto :goto_2

    :cond_3
    iget-object v6, v5, Lcom/google/android/gms/car/senderprotocol/k;->k:Lcom/google/android/gms/car/senderprotocol/p;

    const/4 v7, 0x1

    iput-boolean v7, v6, Lcom/google/android/gms/car/senderprotocol/p;->a:Z

    iget-object v6, v5, Lcom/google/android/gms/car/senderprotocol/k;->k:Lcom/google/android/gms/car/senderprotocol/p;

    iget v2, v2, Lcom/google/android/c/b/co;->a:I

    iput v2, v6, Lcom/google/android/gms/car/senderprotocol/p;->b:I

    iget-object v2, v5, Lcom/google/android/gms/car/senderprotocol/k;->a:Lcom/google/android/gms/car/senderprotocol/o;

    iget-object v3, v3, Lcom/google/android/c/b/cm;->a:[Lcom/google/android/c/b/cn;

    invoke-interface {v2, v3}, Lcom/google/android/gms/car/senderprotocol/o;->a([Lcom/google/android/c/b/cn;)V

    move v2, v0

    goto :goto_2

    :cond_4
    move v0, v1

    goto :goto_3

    .line 1725
    :cond_5
    const-string v0, "CAR.SERVICE"

    const-string v1, "car not supporting sensor"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_4

    .line 1727
    :catch_0
    move-exception v0

    .line 1728
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v9, v0}, Lcom/google/android/gms/car/gx;->a(ILjava/lang/String;)V

    goto :goto_4
.end method

.method private U()V
    .locals 2

    .prologue
    .line 1738
    iget-object v0, p0, Lcom/google/android/gms/car/gx;->N:Lcom/google/android/gms/car/hn;

    .line 1739
    if-eqz v0, :cond_0

    .line 1740
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/car/hn;->a(I)V

    .line 1743
    :cond_0
    return-void
.end method

.method private V()V
    .locals 9

    .prologue
    const/4 v8, 0x2

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 1777
    iget v0, p0, Lcom/google/android/gms/car/gx;->o:I

    if-ne v0, v7, :cond_1

    iget-boolean v0, p0, Lcom/google/android/gms/car/gx;->j:Z

    if-eqz v0, :cond_1

    .line 1778
    const-string v0, "CAR.SERVICE"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1779
    const-string v0, "CAR.SERVICE"

    const-string v1, "resetting USB current function"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1781
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/car/gx;->a:Lcom/google/android/gms/car/CarService;

    const-string v1, "usb"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/car/CarService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/usb/UsbManager;

    .line 1782
    const-class v1, Landroid/hardware/usb/UsbManager;

    const-string v2, "getDefaultFunction"

    new-array v3, v6, [Ljava/lang/Class;

    invoke-virtual {v1, v2, v3}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v1

    new-array v2, v6, [Ljava/lang/Object;

    invoke-virtual {v1, v0, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 1784
    const-class v2, Landroid/hardware/usb/UsbManager;

    const-string v3, "setCurrentFunction"

    new-array v4, v8, [Ljava/lang/Class;

    const-class v5, Ljava/lang/String;

    aput-object v5, v4, v6

    sget-object v5, Ljava/lang/Boolean;->TYPE:Ljava/lang/Class;

    aput-object v5, v4, v7

    invoke-virtual {v2, v3, v4}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v2

    new-array v3, v8, [Ljava/lang/Object;

    aput-object v1, v3, v6

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    aput-object v1, v3, v7

    invoke-virtual {v2, v0, v3}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    .line 1787
    :cond_1
    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/car/gx;I)I
    .locals 0

    .prologue
    .line 69
    iput p1, p0, Lcom/google/android/gms/car/gx;->O:I

    return p1
.end method

.method static synthetic a(Lcom/google/android/gms/car/gx;J)J
    .locals 1

    .prologue
    .line 69
    iput-wide p1, p0, Lcom/google/android/gms/car/gx;->Z:J

    return-wide p1
.end method

.method static synthetic a(Lcom/google/android/gms/car/gx;Lcom/google/android/gms/car/CarInfoInternal;)Lcom/google/android/gms/car/CarInfoInternal;
    .locals 0

    .prologue
    .line 69
    iput-object p1, p0, Lcom/google/android/gms/car/gx;->J:Lcom/google/android/gms/car/CarInfoInternal;

    return-object p1
.end method

.method static synthetic a(Lcom/google/android/gms/car/gx;Lcom/google/android/gms/car/CarUiInfo;)Lcom/google/android/gms/car/CarUiInfo;
    .locals 0

    .prologue
    .line 69
    iput-object p1, p0, Lcom/google/android/gms/car/gx;->K:Lcom/google/android/gms/car/CarUiInfo;

    return-object p1
.end method

.method static synthetic a(Lcom/google/android/gms/car/gx;Lcom/google/android/gms/car/ae;)Lcom/google/android/gms/car/ae;
    .locals 0

    .prologue
    .line 69
    iput-object p1, p0, Lcom/google/android/gms/car/gx;->G:Lcom/google/android/gms/car/ae;

    return-object p1
.end method

.method static synthetic a(Lcom/google/android/gms/car/gx;Lcom/google/android/gms/car/bg;)Lcom/google/android/gms/car/bg;
    .locals 0

    .prologue
    .line 69
    iput-object p1, p0, Lcom/google/android/gms/car/gx;->q:Lcom/google/android/gms/car/bg;

    return-object p1
.end method

.method static synthetic a(Lcom/google/android/gms/car/gx;Lcom/google/android/gms/car/br;)Lcom/google/android/gms/car/br;
    .locals 0

    .prologue
    .line 69
    iput-object p1, p0, Lcom/google/android/gms/car/gx;->t:Lcom/google/android/gms/car/br;

    return-object p1
.end method

.method static synthetic a(Lcom/google/android/gms/car/gx;Lcom/google/android/gms/car/es;)Lcom/google/android/gms/car/es;
    .locals 0

    .prologue
    .line 69
    iput-object p1, p0, Lcom/google/android/gms/car/gx;->A:Lcom/google/android/gms/car/es;

    return-object p1
.end method

.method static synthetic a(Lcom/google/android/gms/car/gx;Lcom/google/android/gms/car/ey;)Lcom/google/android/gms/car/ey;
    .locals 0

    .prologue
    .line 69
    iput-object p1, p0, Lcom/google/android/gms/car/gx;->w:Lcom/google/android/gms/car/ey;

    return-object p1
.end method

.method static synthetic a(Lcom/google/android/gms/car/gx;Lcom/google/android/gms/car/fh;)Lcom/google/android/gms/car/fh;
    .locals 0

    .prologue
    .line 69
    iput-object p1, p0, Lcom/google/android/gms/car/gx;->v:Lcom/google/android/gms/car/fh;

    return-object p1
.end method

.method static synthetic a(Lcom/google/android/gms/car/gx;Lcom/google/android/gms/car/fu;)Lcom/google/android/gms/car/fu;
    .locals 0

    .prologue
    .line 69
    iput-object p1, p0, Lcom/google/android/gms/car/gx;->u:Lcom/google/android/gms/car/fu;

    return-object p1
.end method

.method static synthetic a(Lcom/google/android/gms/car/gx;Lcom/google/android/gms/car/fx;)Lcom/google/android/gms/car/fx;
    .locals 0

    .prologue
    .line 69
    iput-object p1, p0, Lcom/google/android/gms/car/gx;->B:Lcom/google/android/gms/car/fx;

    return-object p1
.end method

.method static synthetic a(Lcom/google/android/gms/car/gx;Lcom/google/android/gms/car/ga;)Lcom/google/android/gms/car/ga;
    .locals 0

    .prologue
    .line 69
    iput-object p1, p0, Lcom/google/android/gms/car/gx;->x:Lcom/google/android/gms/car/ga;

    return-object p1
.end method

.method static synthetic a(Lcom/google/android/gms/car/gx;Lcom/google/android/gms/car/gn;)Lcom/google/android/gms/car/gn;
    .locals 0

    .prologue
    .line 69
    iput-object p1, p0, Lcom/google/android/gms/car/gx;->r:Lcom/google/android/gms/car/gn;

    return-object p1
.end method

.method static synthetic a(Lcom/google/android/gms/car/gx;Lcom/google/android/gms/car/hq;)Lcom/google/android/gms/car/hq;
    .locals 0

    .prologue
    .line 69
    iput-object p1, p0, Lcom/google/android/gms/car/gx;->V:Lcom/google/android/gms/car/hq;

    return-object p1
.end method

.method static synthetic a(Lcom/google/android/gms/car/gx;)Lcom/google/android/gms/car/ip;
    .locals 1

    .prologue
    .line 69
    iget-object v0, p0, Lcom/google/android/gms/car/gx;->s:Lcom/google/android/gms/car/ip;

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/gms/car/gx;Lcom/google/android/gms/car/ip;)Lcom/google/android/gms/car/ip;
    .locals 0

    .prologue
    .line 69
    iput-object p1, p0, Lcom/google/android/gms/car/gx;->s:Lcom/google/android/gms/car/ip;

    return-object p1
.end method

.method static synthetic a(Lcom/google/android/gms/car/gx;Lcom/google/android/gms/car/qc;)Lcom/google/android/gms/car/qc;
    .locals 0

    .prologue
    .line 69
    iput-object p1, p0, Lcom/google/android/gms/car/gx;->U:Lcom/google/android/gms/car/qc;

    return-object p1
.end method

.method static synthetic a(Lcom/google/android/gms/car/gx;Landroid/os/IBinder;)V
    .locals 0

    .prologue
    .line 69
    invoke-direct {p0, p1}, Lcom/google/android/gms/car/gx;->b(Landroid/os/IBinder;)V

    return-void
.end method

.method private a(Landroid/content/Intent;Landroid/content/ComponentName;)Z
    .locals 2

    .prologue
    .line 1621
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v0

    invoke-virtual {v0, p2}, Landroid/content/ComponentName;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/car/gx;->I:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    invoke-virtual {p2}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/common/ew;->b(Landroid/content/pm/PackageManager;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic b(Lcom/google/android/gms/car/gx;)Lcom/google/android/gms/car/id;
    .locals 1

    .prologue
    .line 69
    iget-object v0, p0, Lcom/google/android/gms/car/gx;->e:Lcom/google/android/gms/car/id;

    return-object v0
.end method

.method private b(Landroid/os/IBinder;)V
    .locals 3

    .prologue
    .line 404
    iget-object v1, p0, Lcom/google/android/gms/car/gx;->d:Ljava/util/Map;

    monitor-enter v1

    .line 405
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/car/gx;->d:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/car/hp;

    .line 406
    if-nez v0, :cond_1

    .line 407
    const-string v0, "CAR.SERVICE"

    const/4 v2, 0x4

    invoke-static {v0, v2}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 408
    const-string v0, "CAR.SERVICE"

    const-string v2, "listener not found in list"

    invoke-static {v0, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 410
    :cond_0
    monitor-exit v1

    .line 417
    :goto_0
    return-void

    .line 412
    :cond_1
    iget-object v2, p0, Lcom/google/android/gms/car/gx;->d:Ljava/util/Map;

    invoke-interface {v2, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 413
    iget-object v0, v0, Lcom/google/android/gms/car/hp;->b:Landroid/os/IBinder$DeathRecipient;

    const/4 v2, 0x0

    invoke-interface {p1, v0, v2}, Landroid/os/IBinder;->unlinkToDeath(Landroid/os/IBinder$DeathRecipient;I)Z

    .line 414
    iget-object v0, p0, Lcom/google/android/gms/car/gx;->d:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-boolean v0, p0, Lcom/google/android/gms/car/gx;->j:Z

    if-nez v0, :cond_2

    .line 415
    iget-object v0, p0, Lcom/google/android/gms/car/gx;->a:Lcom/google/android/gms/car/CarService;

    invoke-virtual {v0}, Lcom/google/android/gms/car/CarService;->a()V

    .line 417
    :cond_2
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method static synthetic b(Lcom/google/android/gms/car/gx;I)V
    .locals 4

    .prologue
    .line 69
    iget-object v1, p0, Lcom/google/android/gms/car/gx;->d:Ljava/util/Map;

    monitor-enter v1

    :try_start_0
    const-string v0, "CAR.SERVICE"

    const/4 v2, 0x3

    invoke-static {v0, v2}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "CAR.SERVICE"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "notifying car connection with type of "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " clients:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/gms/car/gx;->d:Ljava/util/Map;

    invoke-interface {v3}, Ljava/util/Map;->size()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/car/gx;->k:Z

    iget-object v0, p0, Lcom/google/android/gms/car/gx;->d:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/IBinder;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    iget-object v3, p0, Lcom/google/android/gms/car/gx;->d:Ljava/util/Map;

    invoke-interface {v3, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/car/hp;

    iget-object v0, v0, Lcom/google/android/gms/car/hp;->a:Lcom/google/android/gms/car/lh;

    invoke-interface {v0, p1}, Lcom/google/android/gms/car/lh;->a(I)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catch_0
    move-exception v0

    :try_start_2
    invoke-interface {v2}, Ljava/util/Iterator;->remove()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    :cond_1
    :try_start_3
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    iget-object v0, p0, Lcom/google/android/gms/car/gx;->a:Lcom/google/android/gms/car/CarService;

    new-instance v1, Landroid/content/Intent;

    const-string v2, "com.google.android.gms.car.CONNECTED"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const/16 v2, 0x20

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    return-void
.end method

.method static synthetic c(Lcom/google/android/gms/car/gx;)I
    .locals 1

    .prologue
    .line 69
    iget v0, p0, Lcom/google/android/gms/car/gx;->o:I

    return v0
.end method

.method static synthetic c(Lcom/google/android/gms/car/gx;I)I
    .locals 0

    .prologue
    .line 69
    iput p1, p0, Lcom/google/android/gms/car/gx;->R:I

    return p1
.end method

.method static synthetic d(Lcom/google/android/gms/car/gx;)Z
    .locals 1

    .prologue
    .line 69
    iget-boolean v0, p0, Lcom/google/android/gms/car/gx;->j:Z

    return v0
.end method

.method static synthetic e(Lcom/google/android/gms/car/gx;)Z
    .locals 1

    .prologue
    .line 69
    iget-boolean v0, p0, Lcom/google/android/gms/car/gx;->S:Z

    return v0
.end method

.method static synthetic f(Lcom/google/android/gms/car/gx;)I
    .locals 1

    .prologue
    .line 69
    iget v0, p0, Lcom/google/android/gms/car/gx;->O:I

    return v0
.end method

.method static synthetic g(Lcom/google/android/gms/car/gx;)Z
    .locals 1

    .prologue
    .line 69
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/car/gx;->S:Z

    return v0
.end method

.method static synthetic h(Lcom/google/android/gms/car/gx;)Lcom/google/android/gms/car/CarService;
    .locals 1

    .prologue
    .line 69
    iget-object v0, p0, Lcom/google/android/gms/car/gx;->a:Lcom/google/android/gms/car/CarService;

    return-object v0
.end method

.method static synthetic i(Lcom/google/android/gms/car/gx;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 69
    iget-object v0, p0, Lcom/google/android/gms/car/gx;->i:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic j(Lcom/google/android/gms/car/gx;)Z
    .locals 1

    .prologue
    .line 69
    iget-boolean v0, p0, Lcom/google/android/gms/car/gx;->l:Z

    return v0
.end method

.method static synthetic k(Lcom/google/android/gms/car/gx;)Z
    .locals 1

    .prologue
    .line 69
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/car/gx;->l:Z

    return v0
.end method

.method static synthetic l(Lcom/google/android/gms/car/gx;)Lcom/google/android/gms/car/qd;
    .locals 1

    .prologue
    .line 69
    iget-object v0, p0, Lcom/google/android/gms/car/gx;->b:Lcom/google/android/gms/car/qd;

    return-object v0
.end method

.method static synthetic m(Lcom/google/android/gms/car/gx;)Lcom/google/android/gms/car/pc;
    .locals 1

    .prologue
    .line 69
    iget-object v0, p0, Lcom/google/android/gms/car/gx;->E:Lcom/google/android/gms/car/pc;

    return-object v0
.end method

.method static synthetic n(Lcom/google/android/gms/car/gx;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 69
    iget-object v0, p0, Lcom/google/android/gms/car/gx;->F:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic o(Lcom/google/android/gms/car/gx;)Lcom/google/android/gms/car/qc;
    .locals 1

    .prologue
    .line 69
    iget-object v0, p0, Lcom/google/android/gms/car/gx;->U:Lcom/google/android/gms/car/qc;

    return-object v0
.end method

.method static synthetic p(Lcom/google/android/gms/car/gx;)Lcom/google/android/gms/car/CarInfoInternal;
    .locals 1

    .prologue
    .line 69
    iget-object v0, p0, Lcom/google/android/gms/car/gx;->J:Lcom/google/android/gms/car/CarInfoInternal;

    return-object v0
.end method

.method static synthetic q(Lcom/google/android/gms/car/gx;)Lcom/google/android/gms/car/at;
    .locals 1

    .prologue
    .line 69
    iget-object v0, p0, Lcom/google/android/gms/car/gx;->aa:Lcom/google/android/gms/car/at;

    return-object v0
.end method

.method static synthetic r(Lcom/google/android/gms/car/gx;)I
    .locals 1

    .prologue
    .line 69
    iget v0, p0, Lcom/google/android/gms/car/gx;->R:I

    return v0
.end method

.method static synthetic s(Lcom/google/android/gms/car/gx;)Lcom/google/android/gms/car/hq;
    .locals 1

    .prologue
    .line 69
    iget-object v0, p0, Lcom/google/android/gms/car/gx;->V:Lcom/google/android/gms/car/hq;

    return-object v0
.end method

.method static synthetic t(Lcom/google/android/gms/car/gx;)Landroid/graphics/Point;
    .locals 3

    .prologue
    .line 69
    iget-object v0, p0, Lcom/google/android/gms/car/gx;->a:Lcom/google/android/gms/car/CarService;

    const-string v1, "window"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/car/CarService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v1

    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0}, Landroid/graphics/Point;-><init>()V

    invoke-virtual {v1, v0}, Landroid/view/Display;->getRealSize(Landroid/graphics/Point;)V

    iget v1, v0, Landroid/graphics/Point;->x:I

    iget v2, v0, Landroid/graphics/Point;->y:I

    if-lt v1, v2, :cond_0

    :goto_0
    return-object v0

    :cond_0
    new-instance v1, Landroid/graphics/Point;

    iget v2, v0, Landroid/graphics/Point;->y:I

    iget v0, v0, Landroid/graphics/Point;->x:I

    invoke-direct {v1, v2, v0}, Landroid/graphics/Point;-><init>(II)V

    move-object v0, v1

    goto :goto_0
.end method

.method static synthetic u(Lcom/google/android/gms/car/gx;)Lcom/google/android/gms/car/bg;
    .locals 1

    .prologue
    .line 69
    iget-object v0, p0, Lcom/google/android/gms/car/gx;->q:Lcom/google/android/gms/car/bg;

    return-object v0
.end method

.method static synthetic v(Lcom/google/android/gms/car/gx;)Lcom/google/android/gms/car/aw;
    .locals 1

    .prologue
    .line 69
    iget-object v0, p0, Lcom/google/android/gms/car/gx;->p:Lcom/google/android/gms/car/aw;

    return-object v0
.end method

.method static synthetic w(Lcom/google/android/gms/car/gx;)Lcom/google/android/gms/car/gn;
    .locals 1

    .prologue
    .line 69
    iget-object v0, p0, Lcom/google/android/gms/car/gx;->r:Lcom/google/android/gms/car/gn;

    return-object v0
.end method

.method static synthetic x(Lcom/google/android/gms/car/gx;)Lcom/google/android/gms/car/CarUiInfo;
    .locals 1

    .prologue
    .line 69
    iget-object v0, p0, Lcom/google/android/gms/car/gx;->K:Lcom/google/android/gms/car/CarUiInfo;

    return-object v0
.end method

.method static synthetic y(Lcom/google/android/gms/car/gx;)Lcom/google/android/gms/car/es;
    .locals 1

    .prologue
    .line 69
    iget-object v0, p0, Lcom/google/android/gms/car/gx;->A:Lcom/google/android/gms/car/es;

    return-object v0
.end method

.method static synthetic z(Lcom/google/android/gms/car/gx;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 69
    iget-object v0, p0, Lcom/google/android/gms/car/gx;->I:Landroid/content/Context;

    return-object v0
.end method


# virtual methods
.method public final A()V
    .locals 2

    .prologue
    .line 1051
    const-string v0, "CAR.SERVICE"

    const/4 v1, 0x2

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1052
    const-string v0, "CAR.SERVICE"

    const-string v1, "onAudioCaptureThreadStuck"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1054
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/car/gx;->Q:Z

    .line 1055
    return-void
.end method

.method public final B()V
    .locals 7

    .prologue
    const/4 v1, 0x2

    const/4 v6, 0x1

    .line 1058
    const-string v0, "CAR.SERVICE"

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1059
    const-string v0, "CAR.SERVICE"

    const-string v1, "onCarDisconnected called"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1063
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/car/gx;->aa:Lcom/google/android/gms/car/at;

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    iget-wide v4, p0, Lcom/google/android/gms/car/gx;->Z:J

    sub-long/2addr v2, v4

    invoke-virtual {v0, v2, v3}, Lcom/google/android/gms/car/at;->a(J)Lcom/google/android/gms/car/at;

    .line 1067
    invoke-virtual {p0}, Lcom/google/android/gms/car/gx;->L()Lcom/google/android/gms/car/ae;

    move-result-object v0

    .line 1068
    iget-object v1, p0, Lcom/google/android/gms/car/gx;->i:Ljava/lang/Object;

    monitor-enter v1

    .line 1069
    :try_start_0
    iget-boolean v2, p0, Lcom/google/android/gms/car/gx;->j:Z

    if-nez v2, :cond_2

    .line 1070
    const-string v0, "CAR.SERVICE"

    const/4 v2, 0x3

    invoke-static {v0, v2}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1071
    const-string v0, "CAR.SERVICE"

    const-string v2, "mConnectedToCar = false, abort"

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1073
    :cond_1
    monitor-exit v1

    .line 1163
    :goto_0
    return-void

    .line 1075
    :cond_2
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/google/android/gms/car/gx;->j:Z

    .line 1077
    iget-object v2, p0, Lcom/google/android/gms/car/gx;->b:Lcom/google/android/gms/car/qd;

    invoke-virtual {v2}, Lcom/google/android/gms/car/qd;->d()V

    .line 1078
    iget-object v2, p0, Lcom/google/android/gms/car/gx;->b:Lcom/google/android/gms/car/qd;

    invoke-virtual {v2}, Lcom/google/android/gms/car/qd;->b()V

    .line 1079
    iget-object v2, p0, Lcom/google/android/gms/car/gx;->U:Lcom/google/android/gms/car/qc;

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/google/android/gms/car/gx;->U:Lcom/google/android/gms/car/qc;

    iget-boolean v2, v2, Lcom/google/android/gms/car/qc;->d:Z

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/google/android/gms/car/gx;->V:Lcom/google/android/gms/car/hq;

    if-eqz v2, :cond_3

    .line 1082
    iget-object v2, p0, Lcom/google/android/gms/car/gx;->V:Lcom/google/android/gms/car/hq;

    invoke-interface {v2}, Lcom/google/android/gms/car/hq;->a()V

    .line 1083
    iget-object v2, p0, Lcom/google/android/gms/car/gx;->U:Lcom/google/android/gms/car/qc;

    const/4 v3, 0x0

    iput-boolean v3, v2, Lcom/google/android/gms/car/qc;->d:Z

    .line 1085
    :cond_3
    if-eqz v0, :cond_4

    .line 1086
    invoke-virtual {v0}, Lcom/google/android/gms/car/ae;->d()V

    .line 1088
    :cond_4
    const/4 v0, 0x0

    invoke-static {v0}, Lcom/google/android/gms/car/diagnostics/FeedbackSenderService;->a(Lcom/google/android/gms/car/on;)V

    .line 1089
    iget-object v2, p0, Lcom/google/android/gms/car/gx;->d:Ljava/util/Map;

    monitor-enter v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    :try_start_1
    const-string v0, "CAR.SERVICE"

    const/4 v3, 0x3

    invoke-static {v0, v3}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_5

    const-string v0, "CAR.SERVICE"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "notifying car connection listeners of disconnection, clients:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/google/android/gms/car/gx;->d:Ljava/util/Map;

    invoke-interface {v4}, Ljava/util/Map;->size()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_5
    iget-boolean v0, p0, Lcom/google/android/gms/car/gx;->k:Z

    if-nez v0, :cond_e

    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 1091
    :goto_1
    :try_start_2
    iget-object v0, p0, Lcom/google/android/gms/car/gx;->E:Lcom/google/android/gms/car/pc;

    invoke-virtual {v0}, Lcom/google/android/gms/car/pc;->b()V

    .line 1092
    invoke-direct {p0}, Lcom/google/android/gms/car/gx;->S()V

    .line 1093
    iget-object v0, p0, Lcom/google/android/gms/car/gx;->E:Lcom/google/android/gms/car/pc;

    iget-object v2, v0, Lcom/google/android/gms/car/pc;->d:Lcom/google/android/gms/car/senderprotocol/k;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/google/android/gms/car/senderprotocol/k;->a(Z)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    :try_start_3
    iget-object v2, v0, Lcom/google/android/gms/car/pc;->a:Ljava/io/InputStream;

    invoke-virtual {v2}, Ljava/io/InputStream;->close()V

    iget-object v0, v0, Lcom/google/android/gms/car/pc;->b:Ljava/io/OutputStream;

    invoke-virtual {v0}, Ljava/io/OutputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    .line 1094
    :goto_2
    :try_start_4
    iget-object v0, p0, Lcom/google/android/gms/car/gx;->N:Lcom/google/android/gms/car/hn;

    invoke-virtual {v0}, Lcom/google/android/gms/car/hn;->a()V

    .line 1095
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/car/gx;->N:Lcom/google/android/gms/car/hn;

    .line 1096
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/car/gx;->E:Lcom/google/android/gms/car/pc;

    .line 1097
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/car/gx;->p:Lcom/google/android/gms/car/aw;

    .line 1098
    iget-object v0, p0, Lcom/google/android/gms/car/gx;->a:Lcom/google/android/gms/car/CarService;

    invoke-virtual {v0}, Lcom/google/android/gms/car/CarService;->c()V

    .line 1099
    iget v0, p0, Lcom/google/android/gms/car/gx;->o:I

    if-ne v0, v6, :cond_6

    .line 1100
    iget-object v0, p0, Lcom/google/android/gms/car/gx;->a:Lcom/google/android/gms/car/CarService;

    iget-object v2, p0, Lcom/google/android/gms/car/gx;->n:Lcom/google/android/gms/car/hx;

    invoke-virtual {v0, v2}, Lcom/google/android/gms/car/CarService;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    .line 1102
    :try_start_5
    invoke-direct {p0}, Lcom/google/android/gms/car/gx;->V()V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_1
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 1110
    :cond_6
    :goto_3
    :try_start_6
    iget-object v0, p0, Lcom/google/android/gms/car/gx;->m:Ljava/io/Closeable;

    if-eqz v0, :cond_8

    .line 1111
    const-string v0, "CAR.SERVICE"

    const/4 v2, 0x2

    invoke-static {v0, v2}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 1112
    const-string v0, "CAR.SERVICE"

    const-string v2, "Closing fd"

    invoke-static {v0, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1114
    :cond_7
    iget-object v0, p0, Lcom/google/android/gms/car/gx;->m:Ljava/io/Closeable;

    invoke-interface {v0}, Ljava/io/Closeable;->close()V

    .line 1115
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/car/gx;->m:Ljava/io/Closeable;
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_2
    .catchall {:try_start_6 .. :try_end_6} :catchall_2

    .line 1123
    :cond_8
    :goto_4
    const/4 v0, 0x0

    :try_start_7
    iput-object v0, p0, Lcom/google/android/gms/car/gx;->b:Lcom/google/android/gms/car/qd;

    .line 1124
    iget-object v0, p0, Lcom/google/android/gms/car/gx;->c:Lcom/google/android/gms/car/no;

    invoke-virtual {v0}, Lcom/google/android/gms/car/no;->a()V

    .line 1125
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/car/gx;->c:Lcom/google/android/gms/car/no;

    .line 1128
    iget-object v0, p0, Lcom/google/android/gms/car/gx;->h:Lcom/google/android/gms/car/hw;

    invoke-virtual {v0}, Lcom/google/android/gms/car/hw;->a()Ljava/lang/Thread$UncaughtExceptionHandler;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Thread;->setDefaultUncaughtExceptionHandler(Ljava/lang/Thread$UncaughtExceptionHandler;)V

    .line 1131
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/car/gx;->g:Lcom/google/android/gms/car/diagnostics/i;

    .line 1132
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/car/gx;->l:Z

    .line 1133
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/car/gx;->X:Z

    .line 1136
    iget v0, p0, Lcom/google/android/gms/car/gx;->o:I

    if-ne v0, v6, :cond_a

    iget-boolean v0, p0, Lcom/google/android/gms/car/gx;->P:Z

    if-eqz v0, :cond_a

    .line 1137
    const-string v0, "CAR.SERVICE"

    const/4 v2, 0x3

    invoke-static {v0, v2}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 1138
    const-string v0, "CAR.SERVICE"

    const-string v2, "reader thread stuck after cable removal. will kill process."

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1140
    :cond_9
    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v0

    invoke-static {v0}, Landroid/os/Process;->killProcess(I)V

    .line 1142
    :cond_a
    iget-boolean v0, p0, Lcom/google/android/gms/car/gx;->Q:Z

    if-eqz v0, :cond_c

    .line 1143
    const-string v0, "CAR.SERVICE"

    const/4 v2, 0x3

    invoke-static {v0, v2}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 1144
    const-string v0, "CAR.SERVICE"

    const-string v2, "audio capture thread stuck disconnect. will kill process."

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1146
    :cond_b
    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v0

    invoke-static {v0}, Landroid/os/Process;->killProcess(I)V

    .line 1148
    :cond_c
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/car/gx;->P:Z

    .line 1149
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/car/gx;->Q:Z

    .line 1152
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/car/gx;->o:I

    .line 1153
    monitor-exit v1
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 1154
    iget-object v1, p0, Lcom/google/android/gms/car/gx;->F:Ljava/lang/Object;

    monitor-enter v1

    .line 1155
    const/4 v0, 0x0

    :try_start_8
    iput-object v0, p0, Lcom/google/android/gms/car/gx;->G:Lcom/google/android/gms/car/ae;

    .line 1156
    monitor-exit v1
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_3

    .line 1158
    iget-object v1, p0, Lcom/google/android/gms/car/gx;->d:Ljava/util/Map;

    monitor-enter v1

    .line 1159
    :try_start_9
    iget-boolean v0, p0, Lcom/google/android/gms/car/gx;->j:Z

    if-nez v0, :cond_d

    iget-object v0, p0, Lcom/google/android/gms/car/gx;->d:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_d

    .line 1160
    const-string v0, "CAR.SERVICE"

    const-string v2, "No car connection listeners, stopping service"

    invoke-static {v0, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1161
    iget-object v0, p0, Lcom/google/android/gms/car/gx;->a:Lcom/google/android/gms/car/CarService;

    invoke-virtual {v0}, Lcom/google/android/gms/car/CarService;->a()V

    .line 1163
    :cond_d
    monitor-exit v1
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    goto/16 :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 1089
    :cond_e
    const/4 v0, 0x0

    :try_start_a
    iput-boolean v0, p0, Lcom/google/android/gms/car/gx;->k:Z

    iget-object v0, p0, Lcom/google/android/gms/car/gx;->d:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_5
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_f

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/IBinder;
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_1

    :try_start_b
    iget-object v4, p0, Lcom/google/android/gms/car/gx;->d:Ljava/util/Map;

    invoke-interface {v4, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/car/hp;

    iget-object v0, v0, Lcom/google/android/gms/car/hp;->a:Lcom/google/android/gms/car/lh;

    invoke-interface {v0}, Lcom/google/android/gms/car/lh;->a()V
    :try_end_b
    .catch Landroid/os/RemoteException; {:try_start_b .. :try_end_b} :catch_0
    .catchall {:try_start_b .. :try_end_b} :catchall_1

    goto :goto_5

    :catch_0
    move-exception v0

    :try_start_c
    const-string v0, "CAR.SERVICE"

    const-string v4, "remote exception calling onDisconnected, removing from list"

    invoke-static {v0, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-interface {v3}, Ljava/util/Iterator;->remove()V
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_1

    goto :goto_5

    :catchall_1
    move-exception v0

    :try_start_d
    monitor-exit v2

    throw v0
    :try_end_d
    .catchall {:try_start_d .. :try_end_d} :catchall_2

    .line 1153
    :catchall_2
    move-exception v0

    monitor-exit v1

    throw v0

    .line 1089
    :cond_f
    :try_start_e
    monitor-exit v2
    :try_end_e
    .catchall {:try_start_e .. :try_end_e} :catchall_1

    :try_start_f
    iget-object v0, p0, Lcom/google/android/gms/car/gx;->a:Lcom/google/android/gms/car/CarService;

    new-instance v2, Landroid/content/Intent;

    const-string v3, "com.google.android.gms.car.DISCONNECTED"

    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const/16 v3, 0x20

    invoke-virtual {v2, v3}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    goto/16 :goto_1

    .line 1103
    :catch_1
    move-exception v0

    .line 1104
    const-string v2, "CAR.SERVICE"

    const/4 v3, 0x3

    invoke-static {v2, v3}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 1105
    const-string v2, "CAR.SERVICE"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "endUsbMode got exception "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_3

    .line 1117
    :catch_2
    move-exception v0

    .line 1118
    const-string v2, "CAR.SERVICE"

    const/4 v3, 0x3

    invoke-static {v2, v3}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_8

    .line 1119
    const-string v2, "CAR.SERVICE"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "USB fd close failed: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_f
    .catchall {:try_start_f .. :try_end_f} :catchall_2

    goto/16 :goto_4

    .line 1156
    :catchall_3
    move-exception v0

    monitor-exit v1

    throw v0

    :catch_3
    move-exception v0

    goto/16 :goto_2
.end method

.method public final C()Lcom/google/android/gms/car/lb;
    .locals 1

    .prologue
    .line 1195
    invoke-direct {p0}, Lcom/google/android/gms/car/gx;->Q()V

    .line 1196
    iget-object v0, p0, Lcom/google/android/gms/car/gx;->D:Lcom/google/android/gms/car/dj;

    return-object v0
.end method

.method public final D()V
    .locals 1

    .prologue
    .line 1224
    invoke-direct {p0}, Lcom/google/android/gms/car/gx;->R()V

    .line 1225
    iget-object v0, p0, Lcom/google/android/gms/car/gx;->a:Lcom/google/android/gms/car/CarService;

    invoke-static {v0}, Lcom/google/android/gms/car/ia;->b(Landroid/content/Context;)V

    .line 1227
    iget-boolean v0, p0, Lcom/google/android/gms/car/gx;->k:Z

    if-eqz v0, :cond_0

    .line 1228
    new-instance v0, Lcom/google/android/gms/car/hc;

    invoke-direct {v0, p0}, Lcom/google/android/gms/car/hc;-><init>(Lcom/google/android/gms/car/gx;)V

    invoke-static {v0}, Lcom/google/android/gms/car/of;->a(Ljava/lang/Runnable;)V

    .line 1235
    :cond_0
    return-void
.end method

.method public final E()Landroid/content/BroadcastReceiver;
    .locals 1

    .prologue
    .line 1298
    iget-object v0, p0, Lcom/google/android/gms/car/gx;->ab:Landroid/content/BroadcastReceiver;

    return-object v0
.end method

.method final F()Landroid/content/Context;
    .locals 1

    .prologue
    .line 1309
    iget-object v0, p0, Lcom/google/android/gms/car/gx;->I:Landroid/content/Context;

    return-object v0
.end method

.method final G()Landroid/content/Context;
    .locals 1

    .prologue
    .line 1313
    iget-object v0, p0, Lcom/google/android/gms/car/gx;->a:Lcom/google/android/gms/car/CarService;

    return-object v0
.end method

.method final H()Lcom/google/android/gms/car/gn;
    .locals 1

    .prologue
    .line 1321
    iget-object v0, p0, Lcom/google/android/gms/car/gx;->r:Lcom/google/android/gms/car/gn;

    return-object v0
.end method

.method final I()Lcom/google/android/gms/car/es;
    .locals 1

    .prologue
    .line 1349
    iget-object v0, p0, Lcom/google/android/gms/car/gx;->A:Lcom/google/android/gms/car/es;

    return-object v0
.end method

.method final J()Lcom/google/android/gms/car/id;
    .locals 1

    .prologue
    .line 1361
    iget-object v0, p0, Lcom/google/android/gms/car/gx;->e:Lcom/google/android/gms/car/id;

    return-object v0
.end method

.method final K()Lcom/google/android/gms/car/at;
    .locals 1

    .prologue
    .line 1369
    iget-object v0, p0, Lcom/google/android/gms/car/gx;->aa:Lcom/google/android/gms/car/at;

    return-object v0
.end method

.method final L()Lcom/google/android/gms/car/ae;
    .locals 2

    .prologue
    .line 1373
    iget-object v1, p0, Lcom/google/android/gms/car/gx;->F:Ljava/lang/Object;

    monitor-enter v1

    .line 1374
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/car/gx;->G:Lcom/google/android/gms/car/ae;

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v0

    .line 1375
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final a(Ljava/lang/String;)Lcom/google/android/gms/car/nd;
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 490
    invoke-virtual {p0}, Lcom/google/android/gms/car/gx;->u()V

    .line 491
    const-string v0, "com.google"

    invoke-virtual {p1, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 492
    invoke-direct {p0}, Lcom/google/android/gms/car/gx;->R()V

    .line 502
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/car/gx;->z:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_3

    .line 503
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "CarNotSupported"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 495
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/car/gx;->z:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/Pair;

    iget-object v0, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, [Ljava/lang/String;

    .line 496
    iget-object v3, p0, Lcom/google/android/gms/car/gx;->a:Lcom/google/android/gms/car/CarService;

    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v4

    invoke-virtual {v3}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    invoke-virtual {v3, v4}, Landroid/content/pm/PackageManager;->getPackagesForUid(I)[Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/util/HashSet;

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-direct {v4, v0}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    invoke-static {v3}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/util/HashSet;->retainAll(Ljava/util/Collection;)Z

    invoke-virtual {v4}, Ljava/util/HashSet;->size()I

    move-result v0

    if-lez v0, :cond_2

    move v0, v1

    :goto_0
    if-nez v0, :cond_0

    .line 498
    new-instance v0, Ljava/lang/SecurityException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Package not permitted to access "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    move v0, v2

    .line 496
    goto :goto_0

    .line 507
    :cond_3
    iget-object v3, p0, Lcom/google/android/gms/car/gx;->y:Ljava/util/HashMap;

    monitor-enter v3

    .line 508
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/car/gx;->y:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/car/ig;

    .line 509
    if-nez v0, :cond_5

    .line 512
    new-instance v2, Lcom/google/android/gms/car/ig;

    invoke-direct {v2}, Lcom/google/android/gms/car/ig;-><init>()V

    .line 513
    iget-object v0, p0, Lcom/google/android/gms/car/gx;->z:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/Pair;

    iget-object v0, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, [B

    iput-object p1, v2, Lcom/google/android/gms/car/ig;->a:Ljava/lang/String;

    iput-object v0, v2, Lcom/google/android/gms/car/ig;->b:[B

    .line 515
    iget-object v0, p0, Lcom/google/android/gms/car/gx;->y:Ljava/util/HashMap;

    invoke-virtual {v0, p1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move v0, v1

    move-object v1, v2

    .line 518
    :goto_1
    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 519
    if-eqz v0, :cond_4

    .line 521
    :try_start_1
    iget-object v0, p0, Lcom/google/android/gms/car/gx;->E:Lcom/google/android/gms/car/pc;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/car/pc;->a(Lcom/google/android/gms/car/ig;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    .line 526
    :cond_4
    :goto_2
    return-object v1

    .line 518
    :catchall_0
    move-exception v0

    monitor-exit v3

    throw v0

    .line 522
    :catch_0
    move-exception v0

    .line 523
    const/4 v2, 0x3

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v2, v0}, Lcom/google/android/gms/car/gx;->a(ILjava/lang/String;)V

    goto :goto_2

    :cond_5
    move-object v1, v0

    move v0, v2

    goto :goto_1
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 569
    iget-object v0, p0, Lcom/google/android/gms/car/gx;->a:Lcom/google/android/gms/car/CarService;

    invoke-static {}, Lcom/google/android/gms/car/gx;->getCallingUid()I

    move-result v1

    invoke-static {v0, v1}, Lcom/google/android/gms/car/of;->a(Landroid/content/Context;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 570
    new-instance v0, Ljava/lang/SecurityException;

    const-string v1, "Wrong signature"

    invoke-direct {v0, v1}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 572
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/car/gx;->e:Lcom/google/android/gms/car/id;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/gms/car/id;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a()V
    .locals 2

    .prologue
    .line 1034
    const/4 v0, 0x3

    const-string v1, "IO error"

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/car/gx;->a(ILjava/lang/String;)V

    .line 1035
    return-void
.end method

.method public final a(I)V
    .locals 1

    .prologue
    .line 1039
    const-string v0, "Protocol error"

    invoke-virtual {p0, p1, v0}, Lcom/google/android/gms/car/gx;->a(ILjava/lang/String;)V

    .line 1040
    return-void
.end method

.method public final a(IIIIIIILandroid/view/Surface;)V
    .locals 9

    .prologue
    .line 919
    iget-object v1, p0, Lcom/google/android/gms/car/gx;->i:Ljava/lang/Object;

    monitor-enter v1

    .line 920
    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/gms/car/gx;->j:Z

    if-nez v0, :cond_1

    .line 921
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 950
    :cond_0
    :goto_0
    return-void

    .line 923
    :cond_1
    monitor-exit v1

    .line 924
    invoke-static {}, Lcom/google/android/gms/car/ie;->a()Z

    move-result v0

    if-nez v0, :cond_2

    .line 926
    iget-object v0, p0, Lcom/google/android/gms/car/gx;->a:Lcom/google/android/gms/car/CarService;

    const-string v1, "power"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/car/CarService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    .line 928
    invoke-virtual {v0}, Landroid/os/PowerManager;->isScreenOn()Z

    move-result v0

    .line 929
    if-nez v0, :cond_2

    .line 930
    iget-object v0, p0, Lcom/google/android/gms/car/gx;->s:Lcom/google/android/gms/car/ip;

    invoke-virtual {v0}, Lcom/google/android/gms/car/ip;->c()V

    goto :goto_0

    .line 923
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 934
    :cond_2
    invoke-virtual {p0}, Lcom/google/android/gms/car/gx;->L()Lcom/google/android/gms/car/ae;

    move-result-object v0

    .line 935
    if-eqz v0, :cond_0

    move v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    move v6, p6

    move/from16 v7, p7

    move-object/from16 v8, p8

    .line 938
    invoke-virtual/range {v0 .. v8}, Lcom/google/android/gms/car/ae;->a(IIIIIIILandroid/view/Surface;)V

    .line 940
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/gms/car/gx;->X:Z

    .line 942
    iget-object v1, p0, Lcom/google/android/gms/car/gx;->i:Ljava/lang/Object;

    monitor-enter v1

    .line 943
    :try_start_1
    iget-object v2, p0, Lcom/google/android/gms/car/gx;->W:Lcom/google/android/gms/car/hs;

    .line 944
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 945
    if-eqz v2, :cond_3

    .line 946
    invoke-interface {v2}, Lcom/google/android/gms/car/hs;->b()V

    .line 949
    :cond_3
    iget-object v1, p0, Lcom/google/android/gms/car/gx;->H:Lcom/google/android/gms/car/oc;

    invoke-virtual {v0}, Lcom/google/android/gms/car/ae;->f()Lcom/google/android/gms/car/on;

    move-result-object v0

    const-string v2, "CAR.POWER"

    const/4 v3, 0x3

    invoke-static {v2, v3}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_4

    const-string v2, "CAR.POWER"

    const-string v3, "starting power management"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_4
    iput-object v0, v1, Lcom/google/android/gms/car/oc;->c:Lcom/google/android/gms/car/ny;

    iget-object v0, v1, Lcom/google/android/gms/car/oc;->b:Lcom/google/android/gms/car/k;

    invoke-virtual {v0}, Lcom/google/android/gms/car/k;->a()V

    goto :goto_0

    .line 944
    :catchall_1
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final a(ILjava/lang/String;)V
    .locals 2

    .prologue
    .line 262
    new-instance v0, Lcom/google/android/gms/car/hg;

    invoke-direct {v0, p0, p1, p2}, Lcom/google/android/gms/car/hg;-><init>(Lcom/google/android/gms/car/gx;ILjava/lang/String;)V

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/google/android/gms/car/of;->a(Landroid/os/Looper;Ljava/lang/Runnable;)V

    .line 310
    return-void
.end method

.method public final a(Landroid/content/ComponentName;)V
    .locals 0

    .prologue
    .line 891
    iput-object p1, p0, Lcom/google/android/gms/car/gx;->Y:Landroid/content/ComponentName;

    .line 892
    return-void
.end method

.method public final a(Landroid/content/res/Configuration;I)V
    .locals 1

    .prologue
    .line 1302
    invoke-virtual {p0}, Lcom/google/android/gms/car/gx;->L()Lcom/google/android/gms/car/ae;

    move-result-object v0

    .line 1303
    if-eqz v0, :cond_0

    .line 1304
    invoke-virtual {v0, p1, p2}, Lcom/google/android/gms/car/ae;->a(Landroid/content/res/Configuration;I)V

    .line 1306
    :cond_0
    return-void
.end method

.method public final a(Landroid/view/MotionEvent;)V
    .locals 2

    .prologue
    .line 990
    iget-object v0, p0, Lcom/google/android/gms/car/gx;->H:Lcom/google/android/gms/car/oc;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/car/oc;->a(I)V

    .line 991
    new-instance v0, Lcom/google/android/gms/car/hl;

    invoke-direct {v0, p0, p1}, Lcom/google/android/gms/car/hl;-><init>(Lcom/google/android/gms/car/gx;Landroid/view/MotionEvent;)V

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/google/android/gms/car/of;->a(Landroid/os/Looper;Ljava/lang/Runnable;)V

    .line 1000
    return-void
.end method

.method public final a(Lcom/google/android/c/b/ar;I)V
    .locals 3

    .prologue
    .line 1016
    iget-object v0, p0, Lcom/google/android/gms/car/gx;->H:Lcom/google/android/gms/car/oc;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/car/oc;->a(I)V

    .line 1017
    const-string v0, "CAR.SERVICE"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1018
    const-string v0, "CAR.SERVICE"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "onKeyEvent code="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p1, Lcom/google/android/c/b/ar;->a:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " down="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p1, Lcom/google/android/c/b/ar;->b:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " longpress="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1021
    :cond_0
    new-instance v0, Lcom/google/android/gms/car/gz;

    invoke-direct {v0, p0, p1, p2}, Lcom/google/android/gms/car/gz;-><init>(Lcom/google/android/gms/car/gx;Lcom/google/android/c/b/ar;I)V

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/google/android/gms/car/of;->a(Landroid/os/Looper;Ljava/lang/Runnable;)V

    .line 1030
    return-void
.end method

.method public final a(Lcom/google/android/gms/car/CarFacet;)V
    .locals 1

    .prologue
    .line 591
    invoke-direct {p0}, Lcom/google/android/gms/car/gx;->R()V

    .line 592
    iget-object v0, p0, Lcom/google/android/gms/car/gx;->aa:Lcom/google/android/gms/car/at;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/car/at;->a(Lcom/google/android/gms/car/CarFacet;)Lcom/google/android/gms/car/at;

    .line 593
    return-void
.end method

.method public final a(Lcom/google/android/gms/car/hs;)V
    .locals 2

    .prologue
    .line 903
    iget-object v1, p0, Lcom/google/android/gms/car/gx;->i:Ljava/lang/Object;

    monitor-enter v1

    .line 904
    :try_start_0
    iput-object p1, p0, Lcom/google/android/gms/car/gx;->W:Lcom/google/android/gms/car/hs;

    .line 905
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final a(Lcom/google/android/gms/car/ka;)V
    .locals 2

    .prologue
    .line 1206
    invoke-direct {p0}, Lcom/google/android/gms/car/gx;->Q()V

    .line 1207
    invoke-virtual {p0}, Lcom/google/android/gms/car/gx;->L()Lcom/google/android/gms/car/ae;

    move-result-object v0

    .line 1208
    if-nez v0, :cond_0

    .line 1209
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "CarNotConnected"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1211
    :cond_0
    new-instance v0, Lcom/google/android/gms/car/hb;

    invoke-direct {v0, p0, p1}, Lcom/google/android/gms/car/hb;-><init>(Lcom/google/android/gms/car/gx;Lcom/google/android/gms/car/ka;)V

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/google/android/gms/car/of;->a(Landroid/os/Looper;Ljava/lang/Runnable;)V

    .line 1220
    return-void
.end method

.method public final a(Lcom/google/android/gms/car/lh;)V
    .locals 5

    .prologue
    .line 359
    iget-object v1, p0, Lcom/google/android/gms/car/gx;->d:Ljava/util/Map;

    monitor-enter v1

    .line 361
    :try_start_0
    new-instance v0, Lcom/google/android/gms/car/hp;

    new-instance v2, Lcom/google/android/gms/car/hh;

    invoke-direct {v2, p0, p1}, Lcom/google/android/gms/car/hh;-><init>(Lcom/google/android/gms/car/gx;Lcom/google/android/gms/car/lh;)V

    invoke-direct {v0, p0, p1, v2}, Lcom/google/android/gms/car/hp;-><init>(Lcom/google/android/gms/car/gx;Lcom/google/android/gms/car/lh;Landroid/os/IBinder$DeathRecipient;)V

    .line 370
    invoke-interface {p1}, Lcom/google/android/gms/car/lh;->asBinder()Landroid/os/IBinder;

    move-result-object v2

    iget-object v3, v0, Lcom/google/android/gms/car/hp;->b:Landroid/os/IBinder$DeathRecipient;

    const/4 v4, 0x0

    invoke-interface {v2, v3, v4}, Landroid/os/IBinder;->linkToDeath(Landroid/os/IBinder$DeathRecipient;I)V

    .line 371
    invoke-virtual {p0}, Lcom/google/android/gms/car/gx;->g()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 372
    iget v2, p0, Lcom/google/android/gms/car/gx;->o:I

    invoke-interface {p1, v2}, Lcom/google/android/gms/car/lh;->a(I)V

    .line 374
    :cond_0
    iget-object v2, p0, Lcom/google/android/gms/car/gx;->d:Ljava/util/Map;

    invoke-interface {p1}, Lcom/google/android/gms/car/lh;->asBinder()Landroid/os/IBinder;

    move-result-object v3

    invoke-interface {v2, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 375
    const-string v0, "CAR.SERVICE"

    const/4 v2, 0x3

    invoke-static {v0, v2}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 376
    const-string v0, "CAR.SERVICE"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Registering carconnectionlistener "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {p1}, Lcom/google/android/gms/car/lh;->asBinder()Landroid/os/IBinder;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " clients:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/gms/car/gx;->d:Ljava/util/Map;

    invoke-interface {v3}, Ljava/util/Map;->size()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 382
    :cond_1
    :goto_0
    :try_start_1
    monitor-exit v1

    return-void

    .line 379
    :catch_0
    move-exception v0

    .line 380
    const-string v2, "CAR.SERVICE"

    const-string v3, "ICarConnectionListener died before we could add it"

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 382
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final a(Lcom/google/android/gms/car/od;)V
    .locals 2

    .prologue
    .line 977
    iget-object v0, p0, Lcom/google/android/gms/car/gx;->H:Lcom/google/android/gms/car/oc;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/google/android/gms/car/oc;->a(I)V

    .line 978
    new-instance v0, Lcom/google/android/gms/car/hk;

    invoke-direct {v0, p0, p1}, Lcom/google/android/gms/car/hk;-><init>(Lcom/google/android/gms/car/gx;Lcom/google/android/gms/car/od;)V

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/google/android/gms/car/of;->a(Landroid/os/Looper;Ljava/lang/Runnable;)V

    .line 987
    return-void
.end method

.method public final a(Ljava/io/Closeable;Ljava/io/InputStream;Ljava/io/OutputStream;I)V
    .locals 13

    .prologue
    .line 605
    const-string v1, "CAR.SERVICE"

    const/4 v2, 0x3

    invoke-static {v1, v2}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 606
    const-string v1, "CAR.SERVICE"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "connectToCar, connectionType:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move/from16 v0, p4

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 608
    :cond_0
    const/4 v1, -0x8

    invoke-static {v1}, Landroid/os/Process;->setThreadPriority(I)V

    .line 609
    iget-object v12, p0, Lcom/google/android/gms/car/gx;->i:Ljava/lang/Object;

    monitor-enter v12

    .line 610
    :try_start_0
    iget-boolean v1, p0, Lcom/google/android/gms/car/gx;->j:Z

    if-eqz v1, :cond_2

    .line 611
    const-string v1, "CAR.SERVICE"

    const/4 v2, 0x3

    invoke-static {v1, v2}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 612
    const-string v1, "CAR.SERVICE"

    const-string v2, "connectToCar while already connected, ignore"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 614
    :cond_1
    monitor-exit v12

    .line 655
    :goto_0
    return-void

    .line 616
    :cond_2
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/google/android/gms/car/gx;->X:Z

    .line 617
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/google/android/gms/car/gx;->S:Z

    .line 618
    const/4 v1, -0x1

    iput v1, p0, Lcom/google/android/gms/car/gx;->R:I

    .line 619
    iput-object p1, p0, Lcom/google/android/gms/car/gx;->m:Ljava/io/Closeable;

    .line 620
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/google/android/gms/car/gx;->P:Z

    .line 621
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/google/android/gms/car/gx;->Q:Z

    .line 622
    new-instance v1, Lcom/google/android/gms/car/diagnostics/i;

    invoke-direct {v1}, Lcom/google/android/gms/car/diagnostics/i;-><init>()V

    iput-object v1, p0, Lcom/google/android/gms/car/gx;->g:Lcom/google/android/gms/car/diagnostics/i;

    .line 626
    new-instance v1, Lcom/google/android/gms/car/hw;

    invoke-static {}, Ljava/lang/Thread;->getDefaultUncaughtExceptionHandler()Ljava/lang/Thread$UncaughtExceptionHandler;

    move-result-object v2

    invoke-direct {v1, p0, v2}, Lcom/google/android/gms/car/hw;-><init>(Lcom/google/android/gms/car/gx;Ljava/lang/Thread$UncaughtExceptionHandler;)V

    iput-object v1, p0, Lcom/google/android/gms/car/gx;->h:Lcom/google/android/gms/car/hw;

    .line 628
    iget-object v1, p0, Lcom/google/android/gms/car/gx;->h:Lcom/google/android/gms/car/hw;

    invoke-static {v1}, Ljava/lang/Thread;->setDefaultUncaughtExceptionHandler(Ljava/lang/Thread$UncaughtExceptionHandler;)V

    .line 630
    const/4 v1, 0x1

    move/from16 v0, p4

    if-ne v0, v1, :cond_3

    .line 631
    new-instance v1, Lcom/google/android/gms/car/hx;

    invoke-direct {v1, p0}, Lcom/google/android/gms/car/hx;-><init>(Lcom/google/android/gms/car/gx;)V

    iput-object v1, p0, Lcom/google/android/gms/car/gx;->n:Lcom/google/android/gms/car/hx;

    .line 632
    new-instance v1, Landroid/content/IntentFilter;

    invoke-direct {v1}, Landroid/content/IntentFilter;-><init>()V

    .line 633
    const-string v2, "android.hardware.usb.action.USB_ACCESSORY_DETACHED"

    invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 634
    iget-object v2, p0, Lcom/google/android/gms/car/gx;->a:Lcom/google/android/gms/car/CarService;

    iget-object v3, p0, Lcom/google/android/gms/car/gx;->n:Lcom/google/android/gms/car/hx;

    invoke-virtual {v2, v3, v1}, Lcom/google/android/gms/car/CarService;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 637
    :cond_3
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/gms/car/gx;->J:Lcom/google/android/gms/car/CarInfoInternal;

    .line 638
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/gms/car/gx;->U:Lcom/google/android/gms/car/qc;

    .line 639
    const/4 v1, 0x0

    iput v1, p0, Lcom/google/android/gms/car/gx;->O:I

    .line 640
    new-instance v1, Lcom/google/android/gms/car/no;

    const-string v2, "Looper"

    invoke-direct {v1, v2}, Lcom/google/android/gms/car/no;-><init>(Ljava/lang/String;)V

    iput-object v1, p0, Lcom/google/android/gms/car/gx;->c:Lcom/google/android/gms/car/no;

    .line 641
    iget-object v1, p0, Lcom/google/android/gms/car/gx;->c:Lcom/google/android/gms/car/no;

    invoke-virtual {v1}, Lcom/google/android/gms/car/no;->start()V

    .line 642
    move/from16 v0, p4

    iput v0, p0, Lcom/google/android/gms/car/gx;->o:I

    .line 643
    new-instance v1, Lcom/google/android/gms/car/qd;

    iget v2, p0, Lcom/google/android/gms/car/gx;->o:I

    invoke-direct {v1, p0, v2}, Lcom/google/android/gms/car/qd;-><init>(Lcom/google/android/gms/car/gx;I)V

    iput-object v1, p0, Lcom/google/android/gms/car/gx;->b:Lcom/google/android/gms/car/qd;

    .line 644
    new-instance v1, Lcom/google/android/gms/car/hn;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/google/android/gms/car/hn;-><init>(Lcom/google/android/gms/car/gx;B)V

    iput-object v1, p0, Lcom/google/android/gms/car/gx;->N:Lcom/google/android/gms/car/hn;

    .line 645
    new-instance v1, Lcom/google/android/gms/car/aw;

    iget-object v2, p0, Lcom/google/android/gms/car/gx;->a:Lcom/google/android/gms/car/CarService;

    invoke-direct {v1, v2}, Lcom/google/android/gms/car/aw;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/google/android/gms/car/gx;->p:Lcom/google/android/gms/car/aw;

    .line 646
    new-instance v1, Lcom/google/android/gms/car/dj;

    iget-object v2, p0, Lcom/google/android/gms/car/gx;->a:Lcom/google/android/gms/car/CarService;

    invoke-direct {v1, v2}, Lcom/google/android/gms/car/dj;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/google/android/gms/car/gx;->D:Lcom/google/android/gms/car/dj;

    .line 647
    new-instance v1, Lcom/google/android/gms/car/fo;

    invoke-direct {v1, p0}, Lcom/google/android/gms/car/fo;-><init>(Lcom/google/android/gms/car/gx;)V

    iput-object v1, p0, Lcom/google/android/gms/car/gx;->C:Lcom/google/android/gms/car/fo;

    .line 648
    new-instance v1, Ljava/util/concurrent/atomic/AtomicInteger;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    iput-object v1, p0, Lcom/google/android/gms/car/gx;->M:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 649
    new-instance v1, Lcom/google/android/gms/car/pc;

    new-instance v2, Lcom/google/android/gms/car/hr;

    const/4 v3, 0x0

    invoke-direct {v2, p0, v3}, Lcom/google/android/gms/car/hr;-><init>(Lcom/google/android/gms/car/gx;B)V

    iget-object v6, p0, Lcom/google/android/gms/car/gx;->I:Landroid/content/Context;

    iget-object v7, p0, Lcom/google/android/gms/car/gx;->g:Lcom/google/android/gms/car/diagnostics/i;

    iget-object v8, p0, Lcom/google/android/gms/car/gx;->N:Lcom/google/android/gms/car/hn;

    iget-object v9, p0, Lcom/google/android/gms/car/gx;->p:Lcom/google/android/gms/car/aw;

    iget-object v10, p0, Lcom/google/android/gms/car/gx;->C:Lcom/google/android/gms/car/fo;

    move-object v3, p2

    move-object/from16 v4, p3

    move-object v5, p0

    move-object v11, p0

    invoke-direct/range {v1 .. v11}, Lcom/google/android/gms/car/pc;-><init>(Lcom/google/android/gms/car/senderprotocol/o;Ljava/io/InputStream;Ljava/io/OutputStream;Lcom/google/android/gms/car/gx;Landroid/content/Context;Lcom/google/android/gms/car/senderprotocol/w;Lcom/google/android/gms/car/senderprotocol/v;Lcom/google/android/gms/car/senderprotocol/u;Lcom/google/android/gms/car/senderprotocol/t;Lcom/google/android/gms/car/eb;)V

    iput-object v1, p0, Lcom/google/android/gms/car/gx;->E:Lcom/google/android/gms/car/pc;

    .line 653
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/gms/car/gx;->j:Z

    .line 654
    iget-object v1, p0, Lcom/google/android/gms/car/gx;->E:Lcom/google/android/gms/car/pc;

    const/4 v2, 0x0

    iput v2, v1, Lcom/google/android/gms/car/pc;->p:I

    iget-object v1, v1, Lcom/google/android/gms/car/pc;->d:Lcom/google/android/gms/car/senderprotocol/k;

    iget-object v1, v1, Lcom/google/android/gms/car/senderprotocol/k;->b:Lcom/google/android/gms/car/senderprotocol/y;

    invoke-virtual {v1}, Lcom/google/android/gms/car/senderprotocol/y;->a()V

    .line 655
    monitor-exit v12
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto/16 :goto_0

    :catchall_0
    move-exception v1

    monitor-exit v12

    throw v1
.end method

.method final a(Ljava/io/PrintWriter;)V
    .locals 8

    .prologue
    .line 1401
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "connected in service:"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v1, p0, Lcom/google/android/gms/car/gx;->j:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", connected in client:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/google/android/gms/car/gx;->k:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1403
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "num car connection listeners:"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/gms/car/gx;->d:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->size()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1405
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/car/gx;->d:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 1406
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1407
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/IBinder;

    .line 1408
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, " listener "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/util/ConcurrentModificationException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    .line 1413
    :cond_0
    iget-boolean v0, p0, Lcom/google/android/gms/car/gx;->j:Z

    if-nez v0, :cond_1

    .line 1487
    :goto_1
    return-void

    .line 1416
    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "connection type:"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v1, p0, Lcom/google/android/gms/car/gx;->o:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1419
    iget-object v0, p0, Lcom/google/android/gms/car/gx;->J:Lcom/google/android/gms/car/CarInfoInternal;

    .line 1420
    if-eqz v0, :cond_2

    .line 1421
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "car info:"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/gms/car/gx;->J:Lcom/google/android/gms/car/CarInfoInternal;

    invoke-virtual {v1}, Lcom/google/android/gms/car/CarInfoInternal;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1423
    :cond_2
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "setup util:"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/gms/car/gx;->U:Lcom/google/android/gms/car/qc;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1424
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "driving status:"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v1, p0, Lcom/google/android/gms/car/gx;->R:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1425
    invoke-virtual {p1}, Ljava/io/PrintWriter;->flush()V

    .line 1426
    iget-object v0, p0, Lcom/google/android/gms/car/gx;->e:Lcom/google/android/gms/car/id;

    .line 1427
    if-eqz v0, :cond_3

    .line 1428
    const-string v1, "*CarServiceSettings*"

    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1429
    invoke-virtual {v0, p1}, Lcom/google/android/gms/car/id;->a(Ljava/io/PrintWriter;)V

    .line 1431
    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/car/gx;->q:Lcom/google/android/gms/car/bg;

    .line 1432
    if-eqz v0, :cond_4

    .line 1433
    const-string v1, "*CarAudioService*"

    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1434
    invoke-virtual {v0, p1}, Lcom/google/android/gms/car/bg;->a(Ljava/io/PrintWriter;)V

    .line 1436
    :cond_4
    iget-object v0, p0, Lcom/google/android/gms/car/gx;->r:Lcom/google/android/gms/car/gn;

    .line 1437
    if-eqz v0, :cond_5

    .line 1438
    const-string v1, "*CarSensorService*"

    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1439
    invoke-virtual {v0, p1}, Lcom/google/android/gms/car/gn;->a(Ljava/io/PrintWriter;)V

    .line 1441
    :cond_5
    iget-object v0, p0, Lcom/google/android/gms/car/gx;->s:Lcom/google/android/gms/car/ip;

    .line 1442
    if-eqz v0, :cond_6

    .line 1443
    const-string v0, "*DisplaySourceService*"

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1444
    iget-object v0, p0, Lcom/google/android/gms/car/gx;->s:Lcom/google/android/gms/car/ip;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/car/ip;->a(Ljava/io/PrintWriter;)V

    .line 1446
    :cond_6
    iget-object v0, p0, Lcom/google/android/gms/car/gx;->t:Lcom/google/android/gms/car/br;

    .line 1447
    if-eqz v0, :cond_7

    .line 1448
    const-string v1, "*BluetoothService*"

    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1449
    invoke-virtual {v0, p1}, Lcom/google/android/gms/car/br;->a(Ljava/io/PrintWriter;)V

    .line 1451
    :cond_7
    iget-object v0, p0, Lcom/google/android/gms/car/gx;->u:Lcom/google/android/gms/car/fu;

    .line 1452
    if-eqz v0, :cond_8

    .line 1453
    const-string v0, "*NavigationStatusService*"

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1454
    :cond_8
    iget-object v0, p0, Lcom/google/android/gms/car/gx;->A:Lcom/google/android/gms/car/es;

    .line 1457
    iget-object v1, p0, Lcom/google/android/gms/car/gx;->A:Lcom/google/android/gms/car/es;

    if-eqz v1, :cond_9

    .line 1458
    const-string v1, "*InputService*"

    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1459
    invoke-virtual {v0, p1}, Lcom/google/android/gms/car/es;->a(Ljava/io/PrintWriter;)V

    .line 1461
    :cond_9
    iget-object v2, p0, Lcom/google/android/gms/car/gx;->z:Ljava/util/HashMap;

    .line 1462
    if-eqz v2, :cond_b

    .line 1463
    const-string v0, "*VendorExtensions*"

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1464
    invoke-virtual {v2}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_a
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_b

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1465
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v4, "Service Name: "

    invoke-direct {v1, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1466
    const-string v1, "Package White List:"

    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1467
    invoke-virtual {v2, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/Pair;

    iget-object v0, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, [Ljava/lang/String;

    array-length v4, v0

    const/4 v1, 0x0

    :goto_2
    if-ge v1, v4, :cond_a

    aget-object v5, v0, v1

    .line 1468
    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "  "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1467
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 1473
    :cond_b
    iget-object v0, p0, Lcom/google/android/gms/car/gx;->G:Lcom/google/android/gms/car/ae;

    .line 1474
    if-eqz v0, :cond_c

    .line 1475
    const-string v1, "*CarActivityManagerService*"

    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1476
    invoke-virtual {v0, p1}, Lcom/google/android/gms/car/ae;->a(Ljava/io/PrintWriter;)V

    .line 1478
    :cond_c
    iget-object v0, p0, Lcom/google/android/gms/car/gx;->E:Lcom/google/android/gms/car/pc;

    .line 1479
    if-eqz v0, :cond_d

    .line 1480
    const-string v0, "*EndPointManager*"

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1481
    :cond_d
    const-string v0, "*PowerManager*"

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1484
    iget-object v0, p0, Lcom/google/android/gms/car/gx;->H:Lcom/google/android/gms/car/oc;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "useractivity status:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, v0, Lcom/google/android/gms/car/oc;->e:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "inPowerSavingMode:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v2, v0, Lcom/google/android/gms/car/oc;->d:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ",last temp:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, v0, Lcom/google/android/gms/car/oc;->a:F

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    const-string v1, "*BatteryTemperatureMonitor*"

    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    iget-object v0, v0, Lcom/google/android/gms/car/oc;->b:Lcom/google/android/gms/car/k;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/car/k;->a(Ljava/io/PrintWriter;)V

    .line 1485
    const-string v0, "*CarMessageService*"

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1486
    iget-object v0, p0, Lcom/google/android/gms/car/gx;->C:Lcom/google/android/gms/car/fo;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/car/fo;->a(Ljava/io/PrintWriter;)V

    goto/16 :goto_1
.end method

.method public final a(Landroid/content/Intent;)Z
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 1168
    invoke-virtual {p0}, Lcom/google/android/gms/car/gx;->u()V

    .line 1169
    invoke-virtual {p0}, Lcom/google/android/gms/car/gx;->L()Lcom/google/android/gms/car/ae;

    move-result-object v2

    .line 1170
    if-nez v2, :cond_1

    .line 1190
    :cond_0
    :goto_0
    return v0

    .line 1173
    :cond_1
    invoke-virtual {p0, p1}, Lcom/google/android/gms/car/gx;->b(Landroid/content/Intent;)Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    .line 1176
    if-eqz p1, :cond_2

    invoke-virtual {p1}, Landroid/content/Intent;->hasFileDescriptors()Z

    move-result v0

    if-ne v0, v1, :cond_2

    .line 1177
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "File descriptors passed in Intent"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1180
    :cond_2
    new-instance v0, Lcom/google/android/gms/car/ha;

    invoke-direct {v0, p0, p1}, Lcom/google/android/gms/car/ha;-><init>(Lcom/google/android/gms/car/gx;Landroid/content/Intent;)V

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-static {v2, v0}, Lcom/google/android/gms/car/of;->a(Landroid/os/Looper;Ljava/lang/Runnable;)V

    move v0, v1

    .line 1190
    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Z)Z
    .locals 1

    .prologue
    .line 551
    invoke-direct {p0}, Lcom/google/android/gms/car/gx;->R()V

    .line 552
    iget-object v0, p0, Lcom/google/android/gms/car/gx;->e:Lcom/google/android/gms/car/id;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/gms/car/id;->a(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public final b(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 563
    invoke-direct {p0}, Lcom/google/android/gms/car/gx;->R()V

    .line 564
    iget-object v0, p0, Lcom/google/android/gms/car/gx;->f:Lcom/google/android/gms/car/ic;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/car/ic;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final b(Landroid/content/Intent;)Ljava/util/List;
    .locals 1

    .prologue
    .line 1201
    iget-object v0, p0, Lcom/google/android/gms/car/gx;->a:Lcom/google/android/gms/car/CarService;

    invoke-static {v0, p1}, Lcom/google/android/gms/car/nw;->a(Landroid/content/Context;Landroid/content/Intent;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 1044
    const-string v0, "CAR.SERVICE"

    const/4 v1, 0x2

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1045
    const-string v0, "CAR.SERVICE"

    const-string v1, "onReaderThreadStuck"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1047
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/car/gx;->P:Z

    .line 1048
    return-void
.end method

.method public final b(Landroid/view/MotionEvent;)V
    .locals 2

    .prologue
    .line 1003
    iget-object v0, p0, Lcom/google/android/gms/car/gx;->H:Lcom/google/android/gms/car/oc;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/google/android/gms/car/oc;->a(I)V

    .line 1004
    new-instance v0, Lcom/google/android/gms/car/hm;

    invoke-direct {v0, p0, p1}, Lcom/google/android/gms/car/hm;-><init>(Lcom/google/android/gms/car/gx;Landroid/view/MotionEvent;)V

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/google/android/gms/car/of;->a(Landroid/os/Looper;Ljava/lang/Runnable;)V

    .line 1013
    return-void
.end method

.method public final b(Lcom/google/android/gms/car/ka;)V
    .locals 2

    .prologue
    .line 1239
    iget-boolean v0, p0, Lcom/google/android/gms/car/gx;->k:Z

    if-nez v0, :cond_1

    const/4 v0, 0x0

    :goto_0
    if-nez v0, :cond_2

    .line 1256
    :cond_0
    :goto_1
    return-void

    .line 1239
    :cond_1
    invoke-direct {p0}, Lcom/google/android/gms/car/gx;->R()V

    const/4 v0, 0x1

    goto :goto_0

    .line 1243
    :cond_2
    invoke-virtual {p0}, Lcom/google/android/gms/car/gx;->L()Lcom/google/android/gms/car/ae;

    move-result-object v0

    .line 1244
    if-eqz v0, :cond_0

    .line 1247
    new-instance v0, Lcom/google/android/gms/car/hd;

    invoke-direct {v0, p0, p1}, Lcom/google/android/gms/car/hd;-><init>(Lcom/google/android/gms/car/gx;Lcom/google/android/gms/car/ka;)V

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/google/android/gms/car/of;->a(Landroid/os/Looper;Ljava/lang/Runnable;)V

    goto :goto_1
.end method

.method public final b(Lcom/google/android/gms/car/lh;)V
    .locals 3

    .prologue
    .line 387
    invoke-interface {p1}, Lcom/google/android/gms/car/lh;->asBinder()Landroid/os/IBinder;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/gms/car/gx;->b(Landroid/os/IBinder;)V

    .line 388
    const-string v0, "CAR.SERVICE"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 389
    const-string v0, "CAR.SERVICE"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unregistering car connection listener "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {p1}, Lcom/google/android/gms/car/lh;->asBinder()Landroid/os/IBinder;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " clients:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/car/gx;->d:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->size()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 392
    :cond_0
    return-void
.end method

.method public final b(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 577
    iget-object v0, p0, Lcom/google/android/gms/car/gx;->a:Lcom/google/android/gms/car/CarService;

    invoke-static {}, Lcom/google/android/gms/car/gx;->getCallingUid()I

    move-result v1

    invoke-static {v0, v1}, Lcom/google/android/gms/car/of;->a(Landroid/content/Context;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 578
    new-instance v0, Ljava/lang/SecurityException;

    const-string v1, "Wrong signature"

    invoke-direct {v0, v1}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 580
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/car/gx;->e:Lcom/google/android/gms/car/id;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/gms/car/id;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 581
    return-void
.end method

.method public final b(Ljava/lang/String;Z)V
    .locals 1

    .prologue
    .line 557
    invoke-direct {p0}, Lcom/google/android/gms/car/gx;->R()V

    .line 558
    iget-object v0, p0, Lcom/google/android/gms/car/gx;->e:Lcom/google/android/gms/car/id;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/gms/car/id;->b(Ljava/lang/String;Z)V

    .line 559
    return-void
.end method

.method public final b(I)Z
    .locals 1

    .prologue
    .line 1279
    invoke-virtual {p0}, Lcom/google/android/gms/car/gx;->L()Lcom/google/android/gms/car/ae;

    move-result-object v0

    .line 1280
    if-eqz v0, :cond_0

    .line 1281
    invoke-virtual {v0, p1}, Lcom/google/android/gms/car/ae;->a(I)Z

    move-result v0

    .line 1283
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method final c()I
    .locals 1

    .prologue
    .line 318
    iget v0, p0, Lcom/google/android/gms/car/gx;->O:I

    return v0
.end method

.method final c(I)V
    .locals 1

    .prologue
    .line 1749
    iget-object v0, p0, Lcom/google/android/gms/car/gx;->N:Lcom/google/android/gms/car/hn;

    .line 1750
    if-eqz v0, :cond_0

    .line 1751
    invoke-virtual {v0, p1}, Lcom/google/android/gms/car/hn;->a(I)V

    .line 1753
    :cond_0
    return-void
.end method

.method public final c(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 973
    const/16 v0, 0xb

    invoke-virtual {p0, v0, p1}, Lcom/google/android/gms/car/gx;->a(ILjava/lang/String;)V

    .line 974
    return-void
.end method

.method public final c(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 872
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    new-instance v1, Landroid/content/ComponentName;

    invoke-direct {v1, p1, p2}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/car/gx;->a(Landroid/content/Intent;)Z

    .line 874
    return-void
.end method

.method public final d()Lcom/google/android/gms/car/CarInfo;
    .locals 2

    .prologue
    .line 323
    iget-object v0, p0, Lcom/google/android/gms/car/gx;->a:Lcom/google/android/gms/car/CarService;

    invoke-static {}, Lcom/google/android/gms/car/gx;->getCallingUid()I

    move-result v1

    invoke-static {v0, v1}, Lcom/google/android/gms/car/of;->a(Landroid/content/Context;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 324
    invoke-direct {p0}, Lcom/google/android/gms/car/gx;->O()V

    .line 328
    :goto_0
    iget-object v0, p0, Lcom/google/android/gms/car/gx;->J:Lcom/google/android/gms/car/CarInfoInternal;

    return-object v0

    .line 326
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/car/gx;->u()V

    goto :goto_0
.end method

.method public final d(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 882
    invoke-virtual {p0}, Lcom/google/android/gms/car/gx;->u()V

    .line 883
    invoke-virtual {p0}, Lcom/google/android/gms/car/gx;->L()Lcom/google/android/gms/car/ae;

    move-result-object v0

    .line 884
    if-eqz v0, :cond_0

    .line 885
    new-instance v1, Landroid/content/ComponentName;

    invoke-direct {v1, p1, p2}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/google/android/gms/car/ae;->a(Landroid/content/ComponentName;)Z

    .line 887
    :cond_0
    return-void
.end method

.method public final e()Lcom/google/android/gms/car/CarUiInfo;
    .locals 1

    .prologue
    .line 333
    invoke-virtual {p0}, Lcom/google/android/gms/car/gx;->u()V

    .line 334
    iget-object v0, p0, Lcom/google/android/gms/car/gx;->K:Lcom/google/android/gms/car/CarUiInfo;

    return-object v0
.end method

.method public final f()Lcom/google/android/gms/car/CarUiInfo;
    .locals 1

    .prologue
    .line 343
    iget-object v0, p0, Lcom/google/android/gms/car/gx;->K:Lcom/google/android/gms/car/CarUiInfo;

    return-object v0
.end method

.method public final g()Z
    .locals 1

    .prologue
    .line 348
    iget-boolean v0, p0, Lcom/google/android/gms/car/gx;->k:Z

    return v0
.end method

.method public final h()I
    .locals 1

    .prologue
    .line 353
    invoke-direct {p0}, Lcom/google/android/gms/car/gx;->O()V

    .line 354
    iget v0, p0, Lcom/google/android/gms/car/gx;->o:I

    return v0
.end method

.method public final i()Lcom/google/android/gms/car/kd;
    .locals 2

    .prologue
    .line 396
    invoke-virtual {p0}, Lcom/google/android/gms/car/gx;->u()V

    .line 397
    iget-object v0, p0, Lcom/google/android/gms/car/gx;->q:Lcom/google/android/gms/car/bg;

    if-nez v0, :cond_0

    .line 398
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "CarNotSupported"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 400
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/car/gx;->q:Lcom/google/android/gms/car/bg;

    return-object v0
.end method

.method public final j()V
    .locals 5

    .prologue
    .line 422
    iget-boolean v0, p0, Lcom/google/android/gms/car/gx;->j:Z

    if-nez v0, :cond_2

    .line 423
    iget-object v2, p0, Lcom/google/android/gms/car/gx;->d:Ljava/util/Map;

    monitor-enter v2

    .line 424
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/car/gx;->d:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v0

    if-eqz v0, :cond_0

    .line 425
    const-string v0, "CAR.SERVICE"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "onUnbind called while "

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/google/android/gms/car/gx;->d:Ljava/util/Map;

    invoke-interface {v3}, Ljava/util/Map;->size()I

    move-result v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " connection listeners still there. Will remove all and finish"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 428
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/car/gx;->d:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .line 429
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 430
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/IBinder;

    .line 431
    iget-object v1, p0, Lcom/google/android/gms/car/gx;->d:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/car/hp;

    .line 432
    iget-object v1, v1, Lcom/google/android/gms/car/hp;->b:Landroid/os/IBinder$DeathRecipient;

    const/4 v4, 0x0

    invoke-interface {v0, v1, v4}, Landroid/os/IBinder;->unlinkToDeath(Landroid/os/IBinder$DeathRecipient;I)Z

    .line 433
    invoke-interface {v3}, Ljava/util/Iterator;->remove()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 435
    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0

    :cond_1
    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 436
    iget-object v0, p0, Lcom/google/android/gms/car/gx;->a:Lcom/google/android/gms/car/CarService;

    invoke-virtual {v0}, Lcom/google/android/gms/car/CarService;->a()V

    .line 438
    :cond_2
    return-void
.end method

.method public final k()Lcom/google/android/gms/car/mu;
    .locals 2

    .prologue
    .line 442
    invoke-direct {p0}, Lcom/google/android/gms/car/gx;->O()V

    .line 443
    iget-object v0, p0, Lcom/google/android/gms/car/gx;->r:Lcom/google/android/gms/car/gn;

    if-nez v0, :cond_0

    .line 444
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "CarNotSupported"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 446
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/car/gx;->r:Lcom/google/android/gms/car/gn;

    invoke-virtual {v0}, Lcom/google/android/gms/car/gn;->e()Z

    move-result v0

    if-nez v0, :cond_1

    .line 447
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "CarNotConnected"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 449
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/car/gx;->r:Lcom/google/android/gms/car/gn;

    return-object v0
.end method

.method public final l()Lcom/google/android/gms/car/mc;
    .locals 2

    .prologue
    .line 454
    invoke-direct {p0}, Lcom/google/android/gms/car/gx;->Q()V

    .line 455
    iget-object v0, p0, Lcom/google/android/gms/car/gx;->u:Lcom/google/android/gms/car/fu;

    if-nez v0, :cond_0

    .line 456
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "CarNotSupported"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 458
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/car/gx;->u:Lcom/google/android/gms/car/fu;

    return-object v0
.end method

.method public final m()Lcom/google/android/gms/car/lq;
    .locals 2

    .prologue
    .line 463
    invoke-direct {p0}, Lcom/google/android/gms/car/gx;->Q()V

    .line 464
    iget-object v0, p0, Lcom/google/android/gms/car/gx;->v:Lcom/google/android/gms/car/fh;

    if-nez v0, :cond_0

    .line 465
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "CarNotSupported"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 467
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/car/gx;->v:Lcom/google/android/gms/car/fh;

    return-object v0
.end method

.method public final n()Lcom/google/android/gms/car/lk;
    .locals 2

    .prologue
    .line 472
    invoke-direct {p0}, Lcom/google/android/gms/car/gx;->Q()V

    .line 473
    iget-object v0, p0, Lcom/google/android/gms/car/gx;->w:Lcom/google/android/gms/car/ey;

    if-nez v0, :cond_0

    .line 474
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "CarNotSupported"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 476
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/car/gx;->w:Lcom/google/android/gms/car/ey;

    return-object v0
.end method

.method public final o()Lcom/google/android/gms/car/mi;
    .locals 2

    .prologue
    .line 481
    invoke-direct {p0}, Lcom/google/android/gms/car/gx;->Q()V

    .line 482
    iget-object v0, p0, Lcom/google/android/gms/car/gx;->x:Lcom/google/android/gms/car/ga;

    if-nez v0, :cond_0

    .line 483
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "CarNotSupported"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 485
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/car/gx;->x:Lcom/google/android/gms/car/ga;

    return-object v0
.end method

.method public final p()Lcom/google/android/gms/car/lw;
    .locals 2

    .prologue
    .line 531
    invoke-direct {p0}, Lcom/google/android/gms/car/gx;->Q()V

    .line 532
    iget-object v0, p0, Lcom/google/android/gms/car/gx;->C:Lcom/google/android/gms/car/fo;

    if-nez v0, :cond_0

    .line 534
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "CarNotConnected"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 536
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/car/gx;->C:Lcom/google/android/gms/car/fo;

    return-object v0
.end method

.method public final q()Lcom/google/android/gms/car/kv;
    .locals 2

    .prologue
    .line 541
    invoke-direct {p0}, Lcom/google/android/gms/car/gx;->O()V

    .line 542
    invoke-direct {p0}, Lcom/google/android/gms/car/gx;->R()V

    .line 543
    iget-object v0, p0, Lcom/google/android/gms/car/gx;->t:Lcom/google/android/gms/car/br;

    if-nez v0, :cond_0

    .line 544
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "CarNotSupported"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 546
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/car/gx;->t:Lcom/google/android/gms/car/br;

    return-object v0
.end method

.method public final r()V
    .locals 1

    .prologue
    .line 585
    invoke-direct {p0}, Lcom/google/android/gms/car/gx;->R()V

    .line 586
    iget-object v0, p0, Lcom/google/android/gms/car/gx;->a:Lcom/google/android/gms/car/CarService;

    invoke-static {v0}, Lcom/google/android/gms/car/ae;->b(Landroid/content/Context;)V

    .line 587
    return-void
.end method

.method public final s()Lcom/google/android/gms/car/oc;
    .locals 1

    .prologue
    .line 659
    iget-object v0, p0, Lcom/google/android/gms/car/gx;->H:Lcom/google/android/gms/car/oc;

    return-object v0
.end method

.method final t()Lcom/google/android/gms/car/ht;
    .locals 1

    .prologue
    .line 664
    iget-object v0, p0, Lcom/google/android/gms/car/gx;->T:Lcom/google/android/gms/car/ht;

    return-object v0
.end method

.method final u()V
    .locals 2

    .prologue
    .line 674
    iget-boolean v0, p0, Lcom/google/android/gms/car/gx;->k:Z

    if-nez v0, :cond_0

    .line 675
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "CarNotConnected"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 677
    :cond_0
    return-void
.end method

.method public final v()V
    .locals 4

    .prologue
    const/4 v2, 0x3

    .line 732
    iget-boolean v0, p0, Lcom/google/android/gms/car/gx;->L:Z

    if-eqz v0, :cond_1

    .line 733
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/car/gx;->L:Z

    .line 747
    :cond_0
    :goto_0
    return-void

    .line 736
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/car/gx;->M:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->decrementAndGet()I

    move-result v0

    if-nez v0, :cond_3

    .line 738
    const-string v0, "CAR.SERVICE"

    invoke-static {v0, v2}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 739
    const-string v0, "CAR.SERVICE"

    const-string v1, "Notifying clients."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 741
    :cond_2
    invoke-direct {p0}, Lcom/google/android/gms/car/gx;->P()V

    goto :goto_0

    .line 743
    :cond_3
    const-string v1, "CAR.SERVICE"

    invoke-static {v1, v2}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 744
    const-string v1, "CAR.SERVICE"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Remaining services = "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method final w()V
    .locals 2

    .prologue
    .line 849
    new-instance v0, Lcom/google/android/gms/car/hj;

    invoke-direct {v0, p0}, Lcom/google/android/gms/car/hj;-><init>(Lcom/google/android/gms/car/gx;)V

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/google/android/gms/car/of;->a(Landroid/os/Looper;Ljava/lang/Runnable;)V

    .line 864
    return-void
.end method

.method public final x()Landroid/content/ComponentName;
    .locals 1

    .prologue
    .line 895
    iget-object v0, p0, Lcom/google/android/gms/car/gx;->Y:Landroid/content/ComponentName;

    return-object v0
.end method

.method public final y()Z
    .locals 1

    .prologue
    .line 912
    iget-boolean v0, p0, Lcom/google/android/gms/car/gx;->X:Z

    return v0
.end method

.method public final z()V
    .locals 2

    .prologue
    .line 954
    invoke-virtual {p0}, Lcom/google/android/gms/car/gx;->L()Lcom/google/android/gms/car/ae;

    move-result-object v0

    .line 955
    if-eqz v0, :cond_0

    .line 956
    invoke-virtual {v0}, Lcom/google/android/gms/car/ae;->a()V

    .line 959
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/car/gx;->X:Z

    .line 961
    iget-object v1, p0, Lcom/google/android/gms/car/gx;->i:Ljava/lang/Object;

    monitor-enter v1

    .line 962
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/car/gx;->W:Lcom/google/android/gms/car/hs;

    .line 963
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 964
    if-eqz v0, :cond_1

    .line 965
    invoke-interface {v0}, Lcom/google/android/gms/car/hs;->a()V

    .line 968
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/car/gx;->H:Lcom/google/android/gms/car/oc;

    invoke-virtual {v0}, Lcom/google/android/gms/car/oc;->a()V

    .line 969
    return-void

    .line 963
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method
