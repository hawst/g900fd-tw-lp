.class final Lcom/google/android/gms/backup/aa;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/ae/a/w;


# instance fields
.field final synthetic a:Ljava/lang/String;

.field final synthetic b:Lcom/google/android/gms/backup/b/e;

.field final synthetic c:Ljava/lang/String;

.field final synthetic d:Lcom/google/android/gms/backup/z;


# direct methods
.method constructor <init>(Lcom/google/android/gms/backup/z;Ljava/lang/String;Lcom/google/android/gms/backup/b/e;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 729
    iput-object p1, p0, Lcom/google/android/gms/backup/aa;->d:Lcom/google/android/gms/backup/z;

    iput-object p2, p0, Lcom/google/android/gms/backup/aa;->a:Ljava/lang/String;

    iput-object p3, p0, Lcom/google/android/gms/backup/aa;->b:Lcom/google/android/gms/backup/b/e;

    iput-object p4, p0, Lcom/google/android/gms/backup/aa;->c:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 737
    const-string v0, "GmsBackupTransport"

    const-string v1, "Start scotty uploading."

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 739
    iget-object v0, p0, Lcom/google/android/gms/backup/aa;->d:Lcom/google/android/gms/backup/z;

    invoke-static {v0}, Lcom/google/android/gms/backup/z;->a(Lcom/google/android/gms/backup/z;)Ljava/util/concurrent/atomic/AtomicInteger;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;->set(I)V

    .line 740
    return-void
.end method

.method public final a(Lcom/google/ae/a/d;)V
    .locals 4

    .prologue
    .line 745
    const-string v0, "GmsBackupTransport"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Http response from scotty : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p1, Lcom/google/ae/a/d;->a:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 747
    iget v0, p1, Lcom/google/ae/a/d;->a:I

    .line 748
    const/16 v1, 0x191

    if-ne v0, v1, :cond_1

    .line 750
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/backup/aa;->d:Lcom/google/android/gms/backup/z;

    iget-object v1, p0, Lcom/google/android/gms/backup/aa;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/google/android/gms/backup/z;->a(Lcom/google/android/gms/backup/z;Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/accounts/AccountsException; {:try_start_0 .. :try_end_0} :catch_0

    .line 773
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/google/android/gms/backup/aa;->d:Lcom/google/android/gms/backup/z;

    invoke-static {v0}, Lcom/google/android/gms/backup/z;->c(Lcom/google/android/gms/backup/z;)Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1

    .line 774
    :try_start_1
    iget-object v0, p0, Lcom/google/android/gms/backup/aa;->d:Lcom/google/android/gms/backup/z;

    invoke-static {v0}, Lcom/google/android/gms/backup/z;->a(Lcom/google/android/gms/backup/z;)Ljava/util/concurrent/atomic/AtomicInteger;

    move-result-object v0

    iget v2, p1, Lcom/google/ae/a/d;->a:I

    invoke-virtual {v0, v2}, Ljava/util/concurrent/atomic/AtomicInteger;->set(I)V

    .line 775
    iget-object v0, p0, Lcom/google/android/gms/backup/aa;->d:Lcom/google/android/gms/backup/z;

    invoke-static {v0}, Lcom/google/android/gms/backup/z;->c(Lcom/google/android/gms/backup/z;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->notify()V

    .line 776
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void

    .line 754
    :cond_1
    const/16 v1, 0xc8

    if-ne v0, v1, :cond_2

    .line 755
    iget-object v0, p0, Lcom/google/android/gms/backup/aa;->b:Lcom/google/android/gms/backup/b/e;

    invoke-virtual {v0}, Lcom/google/android/gms/backup/b/e;->a()Lcom/google/android/gms/drive/b/a/i;

    move-result-object v0

    .line 757
    iget-object v1, p1, Lcom/google/ae/a/d;->b:Lcom/google/ae/a/c;

    const-string v2, "X-Server-Object-Version"

    invoke-virtual {v1, v2}, Lcom/google/ae/a/c;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 759
    if-eqz v0, :cond_0

    if-eqz v1, :cond_0

    .line 760
    iget-object v2, p0, Lcom/google/android/gms/backup/aa;->d:Lcom/google/android/gms/backup/z;

    iget-object v2, v2, Lcom/google/android/gms/backup/z;->a:Lcom/google/android/gms/backup/BackupTransportService;

    iget-object v3, p0, Lcom/google/android/gms/backup/aa;->c:Ljava/lang/String;

    invoke-static {v2, v3, v1, v0}, Lcom/google/android/gms/backup/b/b;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/drive/b/a/i;)V

    goto :goto_0

    .line 768
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/backup/aa;->d:Lcom/google/android/gms/backup/z;

    invoke-static {v0}, Lcom/google/android/gms/backup/z;->b(Lcom/google/android/gms/backup/z;)Lcom/google/android/gms/backup/af;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/backup/af;->b()V

    .line 769
    iget-object v0, p0, Lcom/google/android/gms/backup/aa;->d:Lcom/google/android/gms/backup/z;

    iget-object v0, v0, Lcom/google/android/gms/backup/z;->a:Lcom/google/android/gms/backup/BackupTransportService;

    iget-object v1, p0, Lcom/google/android/gms/backup/aa;->c:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/google/android/gms/backup/b/b;->b(Landroid/content/Context;Ljava/lang/String;)V

    goto :goto_0

    .line 776
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 753
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public final a(Lcom/google/ae/a/s;)V
    .locals 4

    .prologue
    .line 732
    const-string v0, "GmsBackupTransport"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Bytes uploaded : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {p1}, Lcom/google/ae/a/s;->b()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 733
    return-void
.end method

.method public final a(Lcom/google/ae/a/t;)V
    .locals 3

    .prologue
    .line 781
    const-string v0, "GmsBackupTransport"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Scotty transfer exception. "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/google/ae/a/t;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 782
    iget-object v0, p0, Lcom/google/android/gms/backup/aa;->d:Lcom/google/android/gms/backup/z;

    invoke-static {v0}, Lcom/google/android/gms/backup/z;->b(Lcom/google/android/gms/backup/z;)Lcom/google/android/gms/backup/af;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/backup/af;->b()V

    .line 784
    iget-object v0, p0, Lcom/google/android/gms/backup/aa;->d:Lcom/google/android/gms/backup/z;

    invoke-static {v0}, Lcom/google/android/gms/backup/z;->c(Lcom/google/android/gms/backup/z;)Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1

    .line 785
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/backup/aa;->d:Lcom/google/android/gms/backup/z;

    invoke-static {v0}, Lcom/google/android/gms/backup/z;->a(Lcom/google/android/gms/backup/z;)Ljava/util/concurrent/atomic/AtomicInteger;

    move-result-object v0

    const/4 v2, -0x1

    invoke-virtual {v0, v2}, Ljava/util/concurrent/atomic/AtomicInteger;->set(I)V

    .line 786
    iget-object v0, p0, Lcom/google/android/gms/backup/aa;->d:Lcom/google/android/gms/backup/z;

    invoke-static {v0}, Lcom/google/android/gms/backup/z;->c(Lcom/google/android/gms/backup/z;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->notify()V

    .line 787
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method
