.class public final Lcom/google/android/gms/common/data/s;
.super Lcom/google/android/gms/common/data/t;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/data/g;
.implements Lcom/google/android/gms/common/data/r;


# instance fields
.field private b:Lcom/google/android/gms/common/data/a;

.field private final c:Ljava/lang/String;

.field private final d:Ljava/util/HashSet;

.field private e:Lcom/google/android/gms/common/data/h;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/common/data/a;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 37
    invoke-direct {p0, p1}, Lcom/google/android/gms/common/data/t;-><init>(Lcom/google/android/gms/common/data/d;)V

    .line 32
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/common/data/s;->d:Ljava/util/HashSet;

    .line 38
    iput-object p1, p0, Lcom/google/android/gms/common/data/s;->b:Lcom/google/android/gms/common/data/a;

    .line 39
    iput-object p2, p0, Lcom/google/android/gms/common/data/s;->c:Ljava/lang/String;

    .line 40
    new-instance v0, Lcom/google/android/gms/common/data/h;

    invoke-direct {v0}, Lcom/google/android/gms/common/data/h;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/common/data/s;->e:Lcom/google/android/gms/common/data/h;

    .line 41
    return-void
.end method

.method private a(Ljava/lang/String;Ljava/util/ArrayList;)Ljava/util/ArrayList;
    .locals 11

    .prologue
    const/4 v1, 0x0

    .line 292
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 293
    if-eqz p2, :cond_0

    .line 294
    invoke-virtual {p2}, Ljava/util/ArrayList;->clear()V

    .line 297
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/common/data/s;->b:Lcom/google/android/gms/common/data/a;

    iget-object v4, v0, Lcom/google/android/gms/common/data/a;->a:Lcom/google/android/gms/common/data/DataHolder;

    .line 298
    iget-object v5, p0, Lcom/google/android/gms/common/data/s;->c:Ljava/lang/String;

    .line 299
    iget-object v0, p0, Lcom/google/android/gms/common/data/s;->b:Lcom/google/android/gms/common/data/a;

    instance-of v6, v0, Lcom/google/android/gms/common/data/q;

    .line 301
    iget-object v0, p0, Lcom/google/android/gms/common/data/s;->b:Lcom/google/android/gms/common/data/a;

    invoke-virtual {v0}, Lcom/google/android/gms/common/data/a;->c()I

    move-result v7

    move v2, v1

    :goto_0
    if-ge v1, v7, :cond_4

    .line 303
    if-eqz v6, :cond_2

    .line 304
    iget-object v0, p0, Lcom/google/android/gms/common/data/s;->b:Lcom/google/android/gms/common/data/a;

    check-cast v0, Lcom/google/android/gms/common/data/q;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/data/q;->b(I)I

    move-result v0

    .line 308
    :goto_1
    invoke-virtual {v4, v0}, Lcom/google/android/gms/common/data/DataHolder;->a(I)I

    move-result v8

    .line 309
    invoke-virtual {v4, v5, v0, v8}, Lcom/google/android/gms/common/data/DataHolder;->c(Ljava/lang/String;II)Ljava/lang/String;

    move-result-object v8

    .line 313
    if-eqz p2, :cond_5

    .line 314
    iget-object v0, p0, Lcom/google/android/gms/common/data/s;->d:Ljava/util/HashSet;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v0, v9}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 315
    neg-int v0, v2

    add-int/lit8 v0, v0, -0x1

    .line 321
    :goto_2
    invoke-static {v8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v9

    if-nez v9, :cond_1

    .line 322
    invoke-virtual {v8, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_1

    .line 325
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v3, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 326
    if-eqz p2, :cond_1

    .line 327
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 301
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_2
    move v0, v1

    .line 306
    goto :goto_1

    .line 317
    :cond_3
    add-int/lit8 v0, v2, 0x1

    move v10, v2

    move v2, v0

    move v0, v10

    goto :goto_2

    .line 332
    :cond_4
    return-object v3

    :cond_5
    move v0, v2

    goto :goto_2
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/common/data/f;)V
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcom/google/android/gms/common/data/s;->e:Lcom/google/android/gms/common/data/h;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/common/data/h;->a(Lcom/google/android/gms/common/data/f;)V

    .line 46
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 10

    .prologue
    const/4 v2, 0x1

    const/4 v6, 0x0

    .line 107
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 163
    :cond_0
    :goto_0
    return-void

    .line 111
    :cond_1
    const/4 v0, 0x0

    .line 112
    iget-object v1, p0, Lcom/google/android/gms/common/data/s;->e:Lcom/google/android/gms/common/data/h;

    invoke-virtual {v1}, Lcom/google/android/gms/common/data/h;->a()Z

    move-result v1

    if-eqz v1, :cond_8

    .line 113
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    move-object v1, v0

    .line 115
    :goto_1
    invoke-direct {p0, p1, v1}, Lcom/google/android/gms/common/data/s;->a(Ljava/lang/String;Ljava/util/ArrayList;)Ljava/util/ArrayList;

    move-result-object v8

    .line 120
    iget-object v0, p0, Lcom/google/android/gms/common/data/s;->e:Lcom/google/android/gms/common/data/h;

    invoke-virtual {v0}, Lcom/google/android/gms/common/data/h;->a()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 128
    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v7, v0

    move v3, v6

    move v5, v6

    :goto_2
    if-ltz v7, :cond_5

    .line 129
    invoke-virtual {v1, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v4

    .line 130
    if-gez v4, :cond_2

    move v0, v2

    :goto_3
    if-nez v0, :cond_7

    .line 131
    invoke-virtual {v8, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 135
    iget-object v9, p0, Lcom/google/android/gms/common/data/s;->d:Ljava/util/HashSet;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v9, v0}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 138
    if-nez v3, :cond_3

    move v0, v2

    move v3, v4

    .line 128
    :goto_4
    add-int/lit8 v4, v7, -0x1

    move v7, v4

    move v5, v3

    move v3, v0

    goto :goto_2

    :cond_2
    move v0, v6

    .line 130
    goto :goto_3

    .line 141
    :cond_3
    add-int/lit8 v0, v5, -0x1

    if-ne v4, v0, :cond_4

    .line 144
    add-int/lit8 v4, v5, -0x1

    .line 145
    add-int/lit8 v0, v3, 0x1

    move v3, v4

    goto :goto_4

    .line 149
    :cond_4
    iget-object v0, p0, Lcom/google/android/gms/common/data/s;->e:Lcom/google/android/gms/common/data/h;

    invoke-virtual {v0, v5, v3}, Lcom/google/android/gms/common/data/h;->c_(II)V

    move v0, v2

    move v3, v4

    .line 151
    goto :goto_4

    .line 156
    :cond_5
    if-lez v3, :cond_0

    .line 157
    iget-object v0, p0, Lcom/google/android/gms/common/data/s;->e:Lcom/google/android/gms/common/data/h;

    invoke-virtual {v0, v5, v3}, Lcom/google/android/gms/common/data/h;->c_(II)V

    goto :goto_0

    .line 161
    :cond_6
    iget-object v0, p0, Lcom/google/android/gms/common/data/s;->d:Ljava/util/HashSet;

    invoke-virtual {v0, v8}, Ljava/util/HashSet;->addAll(Ljava/util/Collection;)Z

    goto :goto_0

    :cond_7
    move v0, v3

    move v3, v5

    goto :goto_4

    :cond_8
    move-object v1, v0

    goto :goto_1
.end method

.method public final b(I)I
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 60
    iget-object v0, p0, Lcom/google/android/gms/common/data/s;->d:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 87
    :goto_0
    return p1

    .line 63
    :cond_0
    if-ltz p1, :cond_1

    invoke-virtual {p0}, Lcom/google/android/gms/common/data/s;->c()I

    move-result v0

    if-lt p1, v0, :cond_2

    .line 64
    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Position "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " is out of bounds for this buffer"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 70
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/common/data/s;->a_:Lcom/google/android/gms/common/data/d;

    invoke-interface {v0}, Lcom/google/android/gms/common/data/d;->c()I

    move-result v2

    move v0, v1

    :goto_1
    if-ge v1, v2, :cond_5

    .line 72
    iget-object v3, p0, Lcom/google/android/gms/common/data/s;->d:Ljava/util/HashSet;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_4

    .line 73
    if-ne v0, p1, :cond_3

    move p1, v1

    .line 78
    goto :goto_0

    .line 82
    :cond_3
    add-int/lit8 v0, v0, 0x1

    .line 70
    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 87
    :cond_5
    const/4 p1, -0x1

    goto :goto_0
.end method

.method public final b(Lcom/google/android/gms/common/data/f;)V
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lcom/google/android/gms/common/data/s;->e:Lcom/google/android/gms/common/data/h;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/common/data/h;->b(Lcom/google/android/gms/common/data/f;)V

    .line 51
    return-void
.end method

.method public final c()I
    .locals 2

    .prologue
    .line 55
    iget-object v0, p0, Lcom/google/android/gms/common/data/s;->a_:Lcom/google/android/gms/common/data/d;

    invoke-interface {v0}, Lcom/google/android/gms/common/data/d;->c()I

    move-result v0

    iget-object v1, p0, Lcom/google/android/gms/common/data/s;->d:Ljava/util/HashSet;

    invoke-virtual {v1}, Ljava/util/HashSet;->size()I

    move-result v1

    sub-int/2addr v0, v1

    return v0
.end method

.method public final w_()V
    .locals 1

    .prologue
    .line 93
    invoke-super {p0}, Lcom/google/android/gms/common/data/t;->w_()V

    .line 94
    iget-object v0, p0, Lcom/google/android/gms/common/data/s;->e:Lcom/google/android/gms/common/data/h;

    iget-object v0, v0, Lcom/google/android/gms/common/data/h;->a:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->clear()V

    .line 95
    return-void
.end method
