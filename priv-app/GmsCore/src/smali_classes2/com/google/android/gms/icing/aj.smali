.class public final Lcom/google/android/gms/icing/aj;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# instance fields
.field public a:[Lcom/google/android/gms/icing/ak;

.field public b:Z

.field public c:[Lcom/google/android/gms/icing/am;

.field public d:Z

.field public e:[Lcom/google/android/gms/icing/ao;

.field public f:I

.field public g:Z

.field public h:Z

.field public i:Z

.field public j:I

.field public k:I

.field public l:Z

.field public m:Z

.field public n:Lcom/google/android/gms/icing/al;

.field public o:I


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 5333
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 5334
    invoke-static {}, Lcom/google/android/gms/icing/ak;->a()[Lcom/google/android/gms/icing/ak;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/icing/aj;->a:[Lcom/google/android/gms/icing/ak;

    iput-boolean v1, p0, Lcom/google/android/gms/icing/aj;->b:Z

    invoke-static {}, Lcom/google/android/gms/icing/am;->a()[Lcom/google/android/gms/icing/am;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/icing/aj;->c:[Lcom/google/android/gms/icing/am;

    iput-boolean v1, p0, Lcom/google/android/gms/icing/aj;->d:Z

    invoke-static {}, Lcom/google/android/gms/icing/ao;->a()[Lcom/google/android/gms/icing/ao;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/icing/aj;->e:[Lcom/google/android/gms/icing/ao;

    iput v1, p0, Lcom/google/android/gms/icing/aj;->f:I

    iput-boolean v1, p0, Lcom/google/android/gms/icing/aj;->g:Z

    iput-boolean v1, p0, Lcom/google/android/gms/icing/aj;->h:Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/icing/aj;->i:Z

    iput v1, p0, Lcom/google/android/gms/icing/aj;->j:I

    iput v1, p0, Lcom/google/android/gms/icing/aj;->k:I

    iput-boolean v1, p0, Lcom/google/android/gms/icing/aj;->l:Z

    iput-boolean v1, p0, Lcom/google/android/gms/icing/aj;->m:Z

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/icing/aj;->n:Lcom/google/android/gms/icing/al;

    iput v1, p0, Lcom/google/android/gms/icing/aj;->o:I

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/icing/aj;->cachedSize:I

    .line 5335
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v1, 0x0

    .line 5516
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->computeSerializedSize()I

    move-result v0

    .line 5517
    iget-object v2, p0, Lcom/google/android/gms/icing/aj;->a:[Lcom/google/android/gms/icing/ak;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/google/android/gms/icing/aj;->a:[Lcom/google/android/gms/icing/ak;

    array-length v2, v2

    if-lez v2, :cond_2

    move v2, v0

    move v0, v1

    .line 5518
    :goto_0
    iget-object v3, p0, Lcom/google/android/gms/icing/aj;->a:[Lcom/google/android/gms/icing/ak;

    array-length v3, v3

    if-ge v0, v3, :cond_1

    .line 5519
    iget-object v3, p0, Lcom/google/android/gms/icing/aj;->a:[Lcom/google/android/gms/icing/ak;

    aget-object v3, v3, v0

    .line 5520
    if-eqz v3, :cond_0

    .line 5521
    invoke-static {v5, v3}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v3

    add-int/2addr v2, v3

    .line 5518
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    move v0, v2

    .line 5526
    :cond_2
    iget-boolean v2, p0, Lcom/google/android/gms/icing/aj;->b:Z

    if-eqz v2, :cond_3

    .line 5527
    const/4 v2, 0x2

    iget-boolean v3, p0, Lcom/google/android/gms/icing/aj;->b:Z

    invoke-static {v2}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 5530
    :cond_3
    iget-object v2, p0, Lcom/google/android/gms/icing/aj;->c:[Lcom/google/android/gms/icing/am;

    if-eqz v2, :cond_6

    iget-object v2, p0, Lcom/google/android/gms/icing/aj;->c:[Lcom/google/android/gms/icing/am;

    array-length v2, v2

    if-lez v2, :cond_6

    move v2, v0

    move v0, v1

    .line 5531
    :goto_1
    iget-object v3, p0, Lcom/google/android/gms/icing/aj;->c:[Lcom/google/android/gms/icing/am;

    array-length v3, v3

    if-ge v0, v3, :cond_5

    .line 5532
    iget-object v3, p0, Lcom/google/android/gms/icing/aj;->c:[Lcom/google/android/gms/icing/am;

    aget-object v3, v3, v0

    .line 5533
    if-eqz v3, :cond_4

    .line 5534
    const/4 v4, 0x3

    invoke-static {v4, v3}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v3

    add-int/2addr v2, v3

    .line 5531
    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_5
    move v0, v2

    .line 5539
    :cond_6
    iget-boolean v2, p0, Lcom/google/android/gms/icing/aj;->d:Z

    if-eqz v2, :cond_7

    .line 5540
    const/4 v2, 0x4

    iget-boolean v3, p0, Lcom/google/android/gms/icing/aj;->d:Z

    invoke-static {v2}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 5543
    :cond_7
    iget-object v2, p0, Lcom/google/android/gms/icing/aj;->e:[Lcom/google/android/gms/icing/ao;

    if-eqz v2, :cond_9

    iget-object v2, p0, Lcom/google/android/gms/icing/aj;->e:[Lcom/google/android/gms/icing/ao;

    array-length v2, v2

    if-lez v2, :cond_9

    .line 5544
    :goto_2
    iget-object v2, p0, Lcom/google/android/gms/icing/aj;->e:[Lcom/google/android/gms/icing/ao;

    array-length v2, v2

    if-ge v1, v2, :cond_9

    .line 5545
    iget-object v2, p0, Lcom/google/android/gms/icing/aj;->e:[Lcom/google/android/gms/icing/ao;

    aget-object v2, v2, v1

    .line 5546
    if-eqz v2, :cond_8

    .line 5547
    const/4 v3, 0x5

    invoke-static {v3, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v2

    add-int/2addr v0, v2

    .line 5544
    :cond_8
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 5552
    :cond_9
    iget v1, p0, Lcom/google/android/gms/icing/aj;->f:I

    if-eqz v1, :cond_a

    .line 5553
    const/4 v1, 0x6

    iget v2, p0, Lcom/google/android/gms/icing/aj;->f:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 5556
    :cond_a
    iget-boolean v1, p0, Lcom/google/android/gms/icing/aj;->g:Z

    if-eqz v1, :cond_b

    .line 5557
    const/4 v1, 0x7

    iget-boolean v2, p0, Lcom/google/android/gms/icing/aj;->g:Z

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 5560
    :cond_b
    iget-boolean v1, p0, Lcom/google/android/gms/icing/aj;->h:Z

    if-eqz v1, :cond_c

    .line 5561
    const/16 v1, 0x8

    iget-boolean v2, p0, Lcom/google/android/gms/icing/aj;->h:Z

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 5564
    :cond_c
    iget-boolean v1, p0, Lcom/google/android/gms/icing/aj;->i:Z

    if-eq v1, v5, :cond_d

    .line 5565
    const/16 v1, 0x9

    iget-boolean v2, p0, Lcom/google/android/gms/icing/aj;->i:Z

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 5568
    :cond_d
    iget v1, p0, Lcom/google/android/gms/icing/aj;->j:I

    if-eqz v1, :cond_e

    .line 5569
    const/16 v1, 0xa

    iget v2, p0, Lcom/google/android/gms/icing/aj;->j:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 5572
    :cond_e
    iget v1, p0, Lcom/google/android/gms/icing/aj;->k:I

    if-eqz v1, :cond_f

    .line 5573
    const/16 v1, 0xb

    iget v2, p0, Lcom/google/android/gms/icing/aj;->k:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 5576
    :cond_f
    iget-boolean v1, p0, Lcom/google/android/gms/icing/aj;->l:Z

    if-eqz v1, :cond_10

    .line 5577
    const/16 v1, 0xc

    iget-boolean v2, p0, Lcom/google/android/gms/icing/aj;->l:Z

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 5580
    :cond_10
    iget-boolean v1, p0, Lcom/google/android/gms/icing/aj;->m:Z

    if-eqz v1, :cond_11

    .line 5581
    const/16 v1, 0xd

    iget-boolean v2, p0, Lcom/google/android/gms/icing/aj;->m:Z

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 5584
    :cond_11
    iget-object v1, p0, Lcom/google/android/gms/icing/aj;->n:Lcom/google/android/gms/icing/al;

    if-eqz v1, :cond_12

    .line 5585
    const/16 v1, 0xe

    iget-object v2, p0, Lcom/google/android/gms/icing/aj;->n:Lcom/google/android/gms/icing/al;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 5588
    :cond_12
    iget v1, p0, Lcom/google/android/gms/icing/aj;->o:I

    if-eqz v1, :cond_13

    .line 5589
    const/16 v1, 0xf

    iget v2, p0, Lcom/google/android/gms/icing/aj;->o:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 5592
    :cond_13
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 5359
    if-ne p1, p0, :cond_1

    .line 5420
    :cond_0
    :goto_0
    return v0

    .line 5362
    :cond_1
    instance-of v2, p1, Lcom/google/android/gms/icing/aj;

    if-nez v2, :cond_2

    move v0, v1

    .line 5363
    goto :goto_0

    .line 5365
    :cond_2
    check-cast p1, Lcom/google/android/gms/icing/aj;

    .line 5366
    iget-object v2, p0, Lcom/google/android/gms/icing/aj;->a:[Lcom/google/android/gms/icing/ak;

    iget-object v3, p1, Lcom/google/android/gms/icing/aj;->a:[Lcom/google/android/gms/icing/ak;

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    move v0, v1

    .line 5368
    goto :goto_0

    .line 5370
    :cond_3
    iget-boolean v2, p0, Lcom/google/android/gms/icing/aj;->b:Z

    iget-boolean v3, p1, Lcom/google/android/gms/icing/aj;->b:Z

    if-eq v2, v3, :cond_4

    move v0, v1

    .line 5371
    goto :goto_0

    .line 5373
    :cond_4
    iget-object v2, p0, Lcom/google/android/gms/icing/aj;->c:[Lcom/google/android/gms/icing/am;

    iget-object v3, p1, Lcom/google/android/gms/icing/aj;->c:[Lcom/google/android/gms/icing/am;

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_5

    move v0, v1

    .line 5375
    goto :goto_0

    .line 5377
    :cond_5
    iget-boolean v2, p0, Lcom/google/android/gms/icing/aj;->d:Z

    iget-boolean v3, p1, Lcom/google/android/gms/icing/aj;->d:Z

    if-eq v2, v3, :cond_6

    move v0, v1

    .line 5378
    goto :goto_0

    .line 5380
    :cond_6
    iget-object v2, p0, Lcom/google/android/gms/icing/aj;->e:[Lcom/google/android/gms/icing/ao;

    iget-object v3, p1, Lcom/google/android/gms/icing/aj;->e:[Lcom/google/android/gms/icing/ao;

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_7

    move v0, v1

    .line 5382
    goto :goto_0

    .line 5384
    :cond_7
    iget v2, p0, Lcom/google/android/gms/icing/aj;->f:I

    iget v3, p1, Lcom/google/android/gms/icing/aj;->f:I

    if-eq v2, v3, :cond_8

    move v0, v1

    .line 5385
    goto :goto_0

    .line 5387
    :cond_8
    iget-boolean v2, p0, Lcom/google/android/gms/icing/aj;->g:Z

    iget-boolean v3, p1, Lcom/google/android/gms/icing/aj;->g:Z

    if-eq v2, v3, :cond_9

    move v0, v1

    .line 5388
    goto :goto_0

    .line 5390
    :cond_9
    iget-boolean v2, p0, Lcom/google/android/gms/icing/aj;->h:Z

    iget-boolean v3, p1, Lcom/google/android/gms/icing/aj;->h:Z

    if-eq v2, v3, :cond_a

    move v0, v1

    .line 5391
    goto :goto_0

    .line 5393
    :cond_a
    iget-boolean v2, p0, Lcom/google/android/gms/icing/aj;->i:Z

    iget-boolean v3, p1, Lcom/google/android/gms/icing/aj;->i:Z

    if-eq v2, v3, :cond_b

    move v0, v1

    .line 5394
    goto :goto_0

    .line 5396
    :cond_b
    iget v2, p0, Lcom/google/android/gms/icing/aj;->j:I

    iget v3, p1, Lcom/google/android/gms/icing/aj;->j:I

    if-eq v2, v3, :cond_c

    move v0, v1

    .line 5397
    goto :goto_0

    .line 5399
    :cond_c
    iget v2, p0, Lcom/google/android/gms/icing/aj;->k:I

    iget v3, p1, Lcom/google/android/gms/icing/aj;->k:I

    if-eq v2, v3, :cond_d

    move v0, v1

    .line 5400
    goto :goto_0

    .line 5402
    :cond_d
    iget-boolean v2, p0, Lcom/google/android/gms/icing/aj;->l:Z

    iget-boolean v3, p1, Lcom/google/android/gms/icing/aj;->l:Z

    if-eq v2, v3, :cond_e

    move v0, v1

    .line 5403
    goto :goto_0

    .line 5405
    :cond_e
    iget-boolean v2, p0, Lcom/google/android/gms/icing/aj;->m:Z

    iget-boolean v3, p1, Lcom/google/android/gms/icing/aj;->m:Z

    if-eq v2, v3, :cond_f

    move v0, v1

    .line 5406
    goto :goto_0

    .line 5408
    :cond_f
    iget-object v2, p0, Lcom/google/android/gms/icing/aj;->n:Lcom/google/android/gms/icing/al;

    if-nez v2, :cond_10

    .line 5409
    iget-object v2, p1, Lcom/google/android/gms/icing/aj;->n:Lcom/google/android/gms/icing/al;

    if-eqz v2, :cond_11

    move v0, v1

    .line 5410
    goto/16 :goto_0

    .line 5413
    :cond_10
    iget-object v2, p0, Lcom/google/android/gms/icing/aj;->n:Lcom/google/android/gms/icing/al;

    iget-object v3, p1, Lcom/google/android/gms/icing/aj;->n:Lcom/google/android/gms/icing/al;

    invoke-virtual {v2, v3}, Lcom/google/android/gms/icing/al;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_11

    move v0, v1

    .line 5414
    goto/16 :goto_0

    .line 5417
    :cond_11
    iget v2, p0, Lcom/google/android/gms/icing/aj;->o:I

    iget v3, p1, Lcom/google/android/gms/icing/aj;->o:I

    if-eq v2, v3, :cond_0

    move v0, v1

    .line 5418
    goto/16 :goto_0
.end method

.method public final hashCode()I
    .locals 4

    .prologue
    const/16 v2, 0x4d5

    const/16 v1, 0x4cf

    .line 5425
    iget-object v0, p0, Lcom/google/android/gms/icing/aj;->a:[Lcom/google/android/gms/icing/ak;

    invoke-static {v0}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;)I

    move-result v0

    add-int/lit16 v0, v0, 0x20f

    .line 5428
    mul-int/lit8 v3, v0, 0x1f

    iget-boolean v0, p0, Lcom/google/android/gms/icing/aj;->b:Z

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    add-int/2addr v0, v3

    .line 5429
    mul-int/lit8 v0, v0, 0x1f

    iget-object v3, p0, Lcom/google/android/gms/icing/aj;->c:[Lcom/google/android/gms/icing/am;

    invoke-static {v3}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;)I

    move-result v3

    add-int/2addr v0, v3

    .line 5431
    mul-int/lit8 v3, v0, 0x1f

    iget-boolean v0, p0, Lcom/google/android/gms/icing/aj;->d:Z

    if-eqz v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v3

    .line 5432
    mul-int/lit8 v0, v0, 0x1f

    iget-object v3, p0, Lcom/google/android/gms/icing/aj;->e:[Lcom/google/android/gms/icing/ao;

    invoke-static {v3}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;)I

    move-result v3

    add-int/2addr v0, v3

    .line 5434
    mul-int/lit8 v0, v0, 0x1f

    iget v3, p0, Lcom/google/android/gms/icing/aj;->f:I

    add-int/2addr v0, v3

    .line 5435
    mul-int/lit8 v3, v0, 0x1f

    iget-boolean v0, p0, Lcom/google/android/gms/icing/aj;->g:Z

    if-eqz v0, :cond_2

    move v0, v1

    :goto_2
    add-int/2addr v0, v3

    .line 5436
    mul-int/lit8 v3, v0, 0x1f

    iget-boolean v0, p0, Lcom/google/android/gms/icing/aj;->h:Z

    if-eqz v0, :cond_3

    move v0, v1

    :goto_3
    add-int/2addr v0, v3

    .line 5437
    mul-int/lit8 v3, v0, 0x1f

    iget-boolean v0, p0, Lcom/google/android/gms/icing/aj;->i:Z

    if-eqz v0, :cond_4

    move v0, v1

    :goto_4
    add-int/2addr v0, v3

    .line 5438
    mul-int/lit8 v0, v0, 0x1f

    iget v3, p0, Lcom/google/android/gms/icing/aj;->j:I

    add-int/2addr v0, v3

    .line 5439
    mul-int/lit8 v0, v0, 0x1f

    iget v3, p0, Lcom/google/android/gms/icing/aj;->k:I

    add-int/2addr v0, v3

    .line 5440
    mul-int/lit8 v3, v0, 0x1f

    iget-boolean v0, p0, Lcom/google/android/gms/icing/aj;->l:Z

    if-eqz v0, :cond_5

    move v0, v1

    :goto_5
    add-int/2addr v0, v3

    .line 5441
    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v3, p0, Lcom/google/android/gms/icing/aj;->m:Z

    if-eqz v3, :cond_6

    :goto_6
    add-int/2addr v0, v1

    .line 5442
    mul-int/lit8 v1, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/gms/icing/aj;->n:Lcom/google/android/gms/icing/al;

    if-nez v0, :cond_7

    const/4 v0, 0x0

    :goto_7
    add-int/2addr v0, v1

    .line 5444
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/google/android/gms/icing/aj;->o:I

    add-int/2addr v0, v1

    .line 5445
    return v0

    :cond_0
    move v0, v2

    .line 5428
    goto :goto_0

    :cond_1
    move v0, v2

    .line 5431
    goto :goto_1

    :cond_2
    move v0, v2

    .line 5435
    goto :goto_2

    :cond_3
    move v0, v2

    .line 5436
    goto :goto_3

    :cond_4
    move v0, v2

    .line 5437
    goto :goto_4

    :cond_5
    move v0, v2

    .line 5440
    goto :goto_5

    :cond_6
    move v1, v2

    .line 5441
    goto :goto_6

    .line 5442
    :cond_7
    iget-object v0, p0, Lcom/google/android/gms/icing/aj;->n:Lcom/google/android/gms/icing/al;

    invoke-virtual {v0}, Lcom/google/android/gms/icing/al;->hashCode()I

    move-result v0

    goto :goto_7
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 4395
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    const/16 v0, 0xa

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->b(Lcom/google/protobuf/nano/a;I)I

    move-result v2

    iget-object v0, p0, Lcom/google/android/gms/icing/aj;->a:[Lcom/google/android/gms/icing/ak;

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lcom/google/android/gms/icing/ak;

    if-eqz v0, :cond_1

    iget-object v3, p0, Lcom/google/android/gms/icing/aj;->a:[Lcom/google/android/gms/icing/ak;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_1
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_3

    new-instance v3, Lcom/google/android/gms/icing/ak;

    invoke-direct {v3}, Lcom/google/android/gms/icing/ak;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/icing/aj;->a:[Lcom/google/android/gms/icing/ak;

    array-length v0, v0

    goto :goto_1

    :cond_3
    new-instance v3, Lcom/google/android/gms/icing/ak;

    invoke-direct {v3}, Lcom/google/android/gms/icing/ak;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    iput-object v2, p0, Lcom/google/android/gms/icing/aj;->a:[Lcom/google/android/gms/icing/ak;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/icing/aj;->b:Z

    goto :goto_0

    :sswitch_3
    const/16 v0, 0x1a

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->b(Lcom/google/protobuf/nano/a;I)I

    move-result v2

    iget-object v0, p0, Lcom/google/android/gms/icing/aj;->c:[Lcom/google/android/gms/icing/am;

    if-nez v0, :cond_5

    move v0, v1

    :goto_3
    add-int/2addr v2, v0

    new-array v2, v2, [Lcom/google/android/gms/icing/am;

    if-eqz v0, :cond_4

    iget-object v3, p0, Lcom/google/android/gms/icing/aj;->c:[Lcom/google/android/gms/icing/am;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_4
    :goto_4
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_6

    new-instance v3, Lcom/google/android/gms/icing/am;

    invoke-direct {v3}, Lcom/google/android/gms/icing/am;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_5
    iget-object v0, p0, Lcom/google/android/gms/icing/aj;->c:[Lcom/google/android/gms/icing/am;

    array-length v0, v0

    goto :goto_3

    :cond_6
    new-instance v3, Lcom/google/android/gms/icing/am;

    invoke-direct {v3}, Lcom/google/android/gms/icing/am;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    iput-object v2, p0, Lcom/google/android/gms/icing/aj;->c:[Lcom/google/android/gms/icing/am;

    goto/16 :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/icing/aj;->d:Z

    goto/16 :goto_0

    :sswitch_5
    const/16 v0, 0x2a

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->b(Lcom/google/protobuf/nano/a;I)I

    move-result v2

    iget-object v0, p0, Lcom/google/android/gms/icing/aj;->e:[Lcom/google/android/gms/icing/ao;

    if-nez v0, :cond_8

    move v0, v1

    :goto_5
    add-int/2addr v2, v0

    new-array v2, v2, [Lcom/google/android/gms/icing/ao;

    if-eqz v0, :cond_7

    iget-object v3, p0, Lcom/google/android/gms/icing/aj;->e:[Lcom/google/android/gms/icing/ao;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_7
    :goto_6
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_9

    new-instance v3, Lcom/google/android/gms/icing/ao;

    invoke-direct {v3}, Lcom/google/android/gms/icing/ao;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_6

    :cond_8
    iget-object v0, p0, Lcom/google/android/gms/icing/aj;->e:[Lcom/google/android/gms/icing/ao;

    array-length v0, v0

    goto :goto_5

    :cond_9
    new-instance v3, Lcom/google/android/gms/icing/ao;

    invoke-direct {v3}, Lcom/google/android/gms/icing/ao;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    iput-object v2, p0, Lcom/google/android/gms/icing/aj;->e:[Lcom/google/android/gms/icing/ao;

    goto/16 :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/icing/aj;->f:I

    goto/16 :goto_0

    :sswitch_7
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/icing/aj;->g:Z

    goto/16 :goto_0

    :sswitch_8
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/icing/aj;->h:Z

    goto/16 :goto_0

    :sswitch_9
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/icing/aj;->i:Z

    goto/16 :goto_0

    :sswitch_a
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/icing/aj;->j:I

    goto/16 :goto_0

    :sswitch_b
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto/16 :goto_0

    :pswitch_0
    iput v0, p0, Lcom/google/android/gms/icing/aj;->k:I

    goto/16 :goto_0

    :sswitch_c
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/icing/aj;->l:Z

    goto/16 :goto_0

    :sswitch_d
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/icing/aj;->m:Z

    goto/16 :goto_0

    :sswitch_e
    iget-object v0, p0, Lcom/google/android/gms/icing/aj;->n:Lcom/google/android/gms/icing/al;

    if-nez v0, :cond_a

    new-instance v0, Lcom/google/android/gms/icing/al;

    invoke-direct {v0}, Lcom/google/android/gms/icing/al;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/icing/aj;->n:Lcom/google/android/gms/icing/al;

    :cond_a
    iget-object v0, p0, Lcom/google/android/gms/icing/aj;->n:Lcom/google/android/gms/icing/al;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto/16 :goto_0

    :sswitch_f
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    packed-switch v0, :pswitch_data_1

    goto/16 :goto_0

    :pswitch_1
    iput v0, p0, Lcom/google/android/gms/icing/aj;->o:I

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
        0x2a -> :sswitch_5
        0x30 -> :sswitch_6
        0x38 -> :sswitch_7
        0x40 -> :sswitch_8
        0x48 -> :sswitch_9
        0x50 -> :sswitch_a
        0x58 -> :sswitch_b
        0x60 -> :sswitch_c
        0x68 -> :sswitch_d
        0x72 -> :sswitch_e
        0x78 -> :sswitch_f
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v1, 0x0

    .line 5451
    iget-object v0, p0, Lcom/google/android/gms/icing/aj;->a:[Lcom/google/android/gms/icing/ak;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/icing/aj;->a:[Lcom/google/android/gms/icing/ak;

    array-length v0, v0

    if-lez v0, :cond_1

    move v0, v1

    .line 5452
    :goto_0
    iget-object v2, p0, Lcom/google/android/gms/icing/aj;->a:[Lcom/google/android/gms/icing/ak;

    array-length v2, v2

    if-ge v0, v2, :cond_1

    .line 5453
    iget-object v2, p0, Lcom/google/android/gms/icing/aj;->a:[Lcom/google/android/gms/icing/ak;

    aget-object v2, v2, v0

    .line 5454
    if-eqz v2, :cond_0

    .line 5455
    invoke-virtual {p1, v4, v2}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 5452
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 5459
    :cond_1
    iget-boolean v0, p0, Lcom/google/android/gms/icing/aj;->b:Z

    if-eqz v0, :cond_2

    .line 5460
    const/4 v0, 0x2

    iget-boolean v2, p0, Lcom/google/android/gms/icing/aj;->b:Z

    invoke-virtual {p1, v0, v2}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 5462
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/icing/aj;->c:[Lcom/google/android/gms/icing/am;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/google/android/gms/icing/aj;->c:[Lcom/google/android/gms/icing/am;

    array-length v0, v0

    if-lez v0, :cond_4

    move v0, v1

    .line 5463
    :goto_1
    iget-object v2, p0, Lcom/google/android/gms/icing/aj;->c:[Lcom/google/android/gms/icing/am;

    array-length v2, v2

    if-ge v0, v2, :cond_4

    .line 5464
    iget-object v2, p0, Lcom/google/android/gms/icing/aj;->c:[Lcom/google/android/gms/icing/am;

    aget-object v2, v2, v0

    .line 5465
    if-eqz v2, :cond_3

    .line 5466
    const/4 v3, 0x3

    invoke-virtual {p1, v3, v2}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 5463
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 5470
    :cond_4
    iget-boolean v0, p0, Lcom/google/android/gms/icing/aj;->d:Z

    if-eqz v0, :cond_5

    .line 5471
    const/4 v0, 0x4

    iget-boolean v2, p0, Lcom/google/android/gms/icing/aj;->d:Z

    invoke-virtual {p1, v0, v2}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 5473
    :cond_5
    iget-object v0, p0, Lcom/google/android/gms/icing/aj;->e:[Lcom/google/android/gms/icing/ao;

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/google/android/gms/icing/aj;->e:[Lcom/google/android/gms/icing/ao;

    array-length v0, v0

    if-lez v0, :cond_7

    .line 5474
    :goto_2
    iget-object v0, p0, Lcom/google/android/gms/icing/aj;->e:[Lcom/google/android/gms/icing/ao;

    array-length v0, v0

    if-ge v1, v0, :cond_7

    .line 5475
    iget-object v0, p0, Lcom/google/android/gms/icing/aj;->e:[Lcom/google/android/gms/icing/ao;

    aget-object v0, v0, v1

    .line 5476
    if-eqz v0, :cond_6

    .line 5477
    const/4 v2, 0x5

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 5474
    :cond_6
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 5481
    :cond_7
    iget v0, p0, Lcom/google/android/gms/icing/aj;->f:I

    if-eqz v0, :cond_8

    .line 5482
    const/4 v0, 0x6

    iget v1, p0, Lcom/google/android/gms/icing/aj;->f:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 5484
    :cond_8
    iget-boolean v0, p0, Lcom/google/android/gms/icing/aj;->g:Z

    if-eqz v0, :cond_9

    .line 5485
    const/4 v0, 0x7

    iget-boolean v1, p0, Lcom/google/android/gms/icing/aj;->g:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 5487
    :cond_9
    iget-boolean v0, p0, Lcom/google/android/gms/icing/aj;->h:Z

    if-eqz v0, :cond_a

    .line 5488
    const/16 v0, 0x8

    iget-boolean v1, p0, Lcom/google/android/gms/icing/aj;->h:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 5490
    :cond_a
    iget-boolean v0, p0, Lcom/google/android/gms/icing/aj;->i:Z

    if-eq v0, v4, :cond_b

    .line 5491
    const/16 v0, 0x9

    iget-boolean v1, p0, Lcom/google/android/gms/icing/aj;->i:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 5493
    :cond_b
    iget v0, p0, Lcom/google/android/gms/icing/aj;->j:I

    if-eqz v0, :cond_c

    .line 5494
    const/16 v0, 0xa

    iget v1, p0, Lcom/google/android/gms/icing/aj;->j:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 5496
    :cond_c
    iget v0, p0, Lcom/google/android/gms/icing/aj;->k:I

    if-eqz v0, :cond_d

    .line 5497
    const/16 v0, 0xb

    iget v1, p0, Lcom/google/android/gms/icing/aj;->k:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 5499
    :cond_d
    iget-boolean v0, p0, Lcom/google/android/gms/icing/aj;->l:Z

    if-eqz v0, :cond_e

    .line 5500
    const/16 v0, 0xc

    iget-boolean v1, p0, Lcom/google/android/gms/icing/aj;->l:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 5502
    :cond_e
    iget-boolean v0, p0, Lcom/google/android/gms/icing/aj;->m:Z

    if-eqz v0, :cond_f

    .line 5503
    const/16 v0, 0xd

    iget-boolean v1, p0, Lcom/google/android/gms/icing/aj;->m:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 5505
    :cond_f
    iget-object v0, p0, Lcom/google/android/gms/icing/aj;->n:Lcom/google/android/gms/icing/al;

    if-eqz v0, :cond_10

    .line 5506
    const/16 v0, 0xe

    iget-object v1, p0, Lcom/google/android/gms/icing/aj;->n:Lcom/google/android/gms/icing/al;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 5508
    :cond_10
    iget v0, p0, Lcom/google/android/gms/icing/aj;->o:I

    if-eqz v0, :cond_11

    .line 5509
    const/16 v0, 0xf

    iget v1, p0, Lcom/google/android/gms/icing/aj;->o:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 5511
    :cond_11
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 5512
    return-void
.end method
