.class final Lcom/google/android/gms/backup/ab;
.super Landroid/content/BroadcastReceiver;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/gms/backup/BackupTransportService;


# direct methods
.method constructor <init>(Lcom/google/android/gms/backup/BackupTransportService;)V
    .locals 0

    .prologue
    .line 1209
    iput-object p1, p0, Lcom/google/android/gms/backup/ab;->a:Lcom/google/android/gms/backup/BackupTransportService;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public final onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1215
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 1216
    const-string v1, "android.accounts.LOGIN_ACCOUNTS_CHANGED"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "com.google.android.gms.backup.BackupAccountChanged"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1218
    :cond_0
    invoke-static {}, Lcom/google/android/gms/backup/BackupTransportService;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "GmsBackupTransport"

    const-string v1, "accounts changed"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1219
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/backup/ab;->a:Lcom/google/android/gms/backup/BackupTransportService;

    invoke-virtual {v0}, Lcom/google/android/gms/backup/BackupTransportService;->a()V

    .line 1220
    iget-object v0, p0, Lcom/google/android/gms/backup/ab;->a:Lcom/google/android/gms/backup/BackupTransportService;

    invoke-static {v0}, Lcom/google/android/gms/backup/BackupTransportService;->a(Lcom/google/android/gms/backup/BackupTransportService;)Landroid/accounts/Account;

    .line 1226
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/backup/ab;->a:Lcom/google/android/gms/backup/BackupTransportService;

    const-string v1, "connectivity"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/backup/BackupTransportService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    .line 1227
    if-eqz v0, :cond_3

    invoke-virtual {v0}, Landroid/net/ConnectivityManager;->getBackgroundDataSetting()Z

    move-result v1

    if-nez v1, :cond_6

    .line 1228
    :cond_3
    invoke-static {}, Lcom/google/android/gms/backup/BackupTransportService;->b()Z

    move-result v0

    if-eqz v0, :cond_4

    const-string v0, "GmsBackupTransport"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "background data off: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1229
    :cond_4
    iget-object v0, p0, Lcom/google/android/gms/backup/ab;->a:Lcom/google/android/gms/backup/BackupTransportService;

    invoke-static {v0}, Lcom/google/android/gms/backup/BackupTransportService;->b(Lcom/google/android/gms/backup/BackupTransportService;)Lcom/android/a/a;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/android/a/a;->a(Z)V

    .line 1244
    :goto_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 1245
    iget-object v2, p0, Lcom/google/android/gms/backup/ab;->a:Lcom/google/android/gms/backup/BackupTransportService;

    invoke-static {v2}, Lcom/google/android/gms/backup/BackupTransportService;->b(Lcom/google/android/gms/backup/BackupTransportService;)Lcom/android/a/a;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/gms/backup/ab;->a:Lcom/google/android/gms/backup/BackupTransportService;

    invoke-static {v3}, Lcom/google/android/gms/backup/BackupTransportService;->c(Lcom/google/android/gms/backup/BackupTransportService;)Lcom/android/a/b;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/android/a/a;->a(Lcom/android/a/b;)J

    move-result-wide v2

    cmp-long v0, v2, v0

    if-lez v0, :cond_c

    .line 1246
    invoke-static {}, Lcom/google/android/gms/backup/BackupTransportService;->b()Z

    move-result v0

    if-eqz v0, :cond_5

    const-string v0, "GmsBackupTransport"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "no backup now: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/gms/backup/ab;->a:Lcom/google/android/gms/backup/BackupTransportService;

    invoke-static {v2}, Lcom/google/android/gms/backup/BackupTransportService;->b(Lcom/google/android/gms/backup/BackupTransportService;)Lcom/android/a/a;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1257
    :cond_5
    :goto_1
    return-void

    .line 1230
    :cond_6
    invoke-static {}, Landroid/content/ContentResolver;->getMasterSyncAutomatically()Z

    move-result v1

    if-nez v1, :cond_8

    .line 1231
    invoke-static {}, Lcom/google/android/gms/backup/BackupTransportService;->b()Z

    move-result v0

    if-eqz v0, :cond_7

    const-string v0, "GmsBackupTransport"

    const-string v1, "master sync disabled"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1232
    :cond_7
    iget-object v0, p0, Lcom/google/android/gms/backup/ab;->a:Lcom/google/android/gms/backup/BackupTransportService;

    invoke-static {v0}, Lcom/google/android/gms/backup/BackupTransportService;->b(Lcom/google/android/gms/backup/BackupTransportService;)Lcom/android/a/a;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/android/a/a;->a(Z)V

    goto :goto_0

    .line 1234
    :cond_8
    invoke-virtual {v0}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v0

    .line 1235
    if-eqz v0, :cond_a

    invoke-virtual {v0}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v0

    if-eqz v0, :cond_a

    .line 1236
    invoke-static {}, Lcom/google/android/gms/backup/BackupTransportService;->b()Z

    move-result v0

    if-eqz v0, :cond_9

    const-string v0, "GmsBackupTransport"

    const-string v1, "network is up"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1237
    :cond_9
    iget-object v0, p0, Lcom/google/android/gms/backup/ab;->a:Lcom/google/android/gms/backup/BackupTransportService;

    invoke-static {v0}, Lcom/google/android/gms/backup/BackupTransportService;->b(Lcom/google/android/gms/backup/BackupTransportService;)Lcom/android/a/a;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/android/a/a;->a(Z)V

    goto :goto_0

    .line 1239
    :cond_a
    invoke-static {}, Lcom/google/android/gms/backup/BackupTransportService;->b()Z

    move-result v0

    if-eqz v0, :cond_b

    const-string v0, "GmsBackupTransport"

    const-string v1, "network is down"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1240
    :cond_b
    iget-object v0, p0, Lcom/google/android/gms/backup/ab;->a:Lcom/google/android/gms/backup/BackupTransportService;

    invoke-static {v0}, Lcom/google/android/gms/backup/BackupTransportService;->b(Lcom/google/android/gms/backup/BackupTransportService;)Lcom/android/a/a;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/android/a/a;->a(Z)V

    goto/16 :goto_0

    .line 1248
    :cond_c
    iget-object v0, p0, Lcom/google/android/gms/backup/ab;->a:Lcom/google/android/gms/backup/BackupTransportService;

    invoke-static {v0}, Lcom/google/android/gms/backup/BackupTransportService;->d(Lcom/google/android/gms/backup/BackupTransportService;)Landroid/app/backup/BackupManager;

    move-result-object v0

    .line 1249
    if-nez v0, :cond_d

    .line 1250
    const-string v0, "GmsBackupTransport"

    const-string v1, "No BackupManager service available"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 1252
    :cond_d
    invoke-static {}, Lcom/google/android/gms/backup/BackupTransportService;->b()Z

    move-result v1

    if-eqz v1, :cond_e

    const-string v1, "GmsBackupTransport"

    const-string v2, "triggering backup now"

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1253
    :cond_e
    iget-object v1, p0, Lcom/google/android/gms/backup/ab;->a:Lcom/google/android/gms/backup/BackupTransportService;

    invoke-static {v1}, Lcom/google/android/gms/backup/BackupTransportService;->b(Lcom/google/android/gms/backup/BackupTransportService;)Lcom/android/a/a;

    move-result-object v1

    const-wide v2, 0x7fffffffffffffffL

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a;->a(J)V

    .line 1254
    invoke-virtual {v0}, Landroid/app/backup/BackupManager;->backupNow()V

    goto/16 :goto_1
.end method
