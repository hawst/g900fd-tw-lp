.class public final Lcom/google/android/gms/common/account/h;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public a:I

.field public b:Landroid/widget/AdapterView$OnItemSelectedListener;

.field public c:Ljava/lang/String;

.field private final d:Landroid/content/Context;

.field private final e:Landroid/support/v7/app/a;

.field private f:Ljava/lang/String;

.field private g:[Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/support/v7/app/a;)V
    .locals 1

    .prologue
    .line 219
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 220
    iput-object p1, p0, Lcom/google/android/gms/common/account/h;->e:Landroid/support/v7/app/a;

    .line 221
    invoke-virtual {p1}, Landroid/support/v7/app/a;->g()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/common/account/h;->d:Landroid/content/Context;

    .line 222
    iget-object v0, p0, Lcom/google/android/gms/common/account/h;->d:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/common/account/h;->f:Ljava/lang/String;

    .line 223
    sget v0, Lcom/google/android/gms/p;->eP:I

    iput v0, p0, Lcom/google/android/gms/common/account/h;->a:I

    .line 224
    return-void
.end method


# virtual methods
.method public final a()Lcom/google/android/gms/common/account/g;
    .locals 6

    .prologue
    .line 248
    iget-object v0, p0, Lcom/google/android/gms/common/account/h;->g:[Ljava/lang/String;

    if-nez v0, :cond_0

    .line 249
    iget-object v0, p0, Lcom/google/android/gms/common/account/h;->d:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/gms/common/account/h;->f:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/google/android/gms/common/util/a;->d(Landroid/content/Context;Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/common/util/a;->a(Ljava/util/List;)[Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/common/account/h;->g:[Ljava/lang/String;

    .line 252
    :cond_0
    new-instance v0, Lcom/google/android/gms/common/account/g;

    iget-object v1, p0, Lcom/google/android/gms/common/account/h;->d:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/gms/common/account/h;->f:Ljava/lang/String;

    iget v3, p0, Lcom/google/android/gms/common/account/h;->a:I

    iget-object v4, p0, Lcom/google/android/gms/common/account/h;->g:[Ljava/lang/String;

    const/4 v5, 0x0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/common/account/g;-><init>(Landroid/content/Context;Ljava/lang/String;I[Ljava/lang/String;B)V

    .line 254
    iget-object v1, p0, Lcom/google/android/gms/common/account/h;->e:Landroid/support/v7/app/a;

    iget-object v2, p0, Lcom/google/android/gms/common/account/h;->b:Landroid/widget/AdapterView$OnItemSelectedListener;

    iget-object v3, p0, Lcom/google/android/gms/common/account/h;->c:Ljava/lang/String;

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/gms/common/account/g;->a(Landroid/support/v7/app/a;Landroid/widget/AdapterView$OnItemSelectedListener;Ljava/lang/String;)V

    .line 255
    return-object v0
.end method
