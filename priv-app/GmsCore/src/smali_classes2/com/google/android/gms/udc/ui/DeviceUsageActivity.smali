.class public Lcom/google/android/gms/udc/ui/DeviceUsageActivity;
.super Landroid/support/v7/app/d;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/widget/h;


# instance fields
.field private a:Lcom/google/android/gms/common/api/v;

.field private b:Landroid/view/View;

.field private c:Lcom/google/android/gms/common/widget/SwitchBar;

.field private d:Lcom/google/android/gms/udc/util/j;

.field private e:Lcom/google/android/gms/udc/util/f;

.field private f:Lcom/google/android/gms/analytics/bv;

.field private g:Ljava/lang/String;

.field private h:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 37
    invoke-direct {p0}, Landroid/support/v7/app/d;-><init>()V

    .line 225
    return-void
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 65
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/gms/udc/ui/DeviceUsageActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 66
    const-string v1, "UdcAccountName"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 67
    const-string v1, "UdcStyledTitle"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 68
    const-string v1, "UdcStyledDescription"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 69
    const-string v1, "UdcSettingId"

    invoke-virtual {v0, v1, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 70
    return-object v0
.end method

.method static synthetic a(Lcom/google/android/gms/udc/ui/DeviceUsageActivity;)Lcom/google/android/gms/common/api/v;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lcom/google/android/gms/udc/ui/DeviceUsageActivity;->a:Lcom/google/android/gms/common/api/v;

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/gms/udc/ui/DeviceUsageActivity;Lcom/google/android/gms/common/api/Status;I)V
    .locals 5

    .prologue
    .line 37
    invoke-virtual {p1}, Lcom/google/android/gms/common/api/Status;->h()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    sget v0, Lcom/google/android/gms/p;->yU:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/udc/ui/DeviceUsageActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    iget-object v1, p0, Lcom/google/android/gms/udc/ui/DeviceUsageActivity;->d:Lcom/google/android/gms/udc/util/j;

    const v2, 0x1020002

    new-instance v3, Lcom/google/android/gms/udc/ui/g;

    invoke-direct {v3}, Lcom/google/android/gms/udc/ui/g;-><init>()V

    invoke-virtual {p0, p2}, Lcom/google/android/gms/udc/ui/DeviceUsageActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/google/android/gms/udc/ui/g;->a(Ljava/lang/CharSequence;)Lcom/google/android/gms/udc/ui/g;

    move-result-object v3

    invoke-virtual {v3, v0}, Lcom/google/android/gms/udc/ui/g;->b(Ljava/lang/CharSequence;)Lcom/google/android/gms/udc/ui/g;

    move-result-object v0

    const/4 v3, 0x0

    invoke-virtual {v0, v3}, Lcom/google/android/gms/udc/ui/g;->a(Z)Lcom/google/android/gms/udc/ui/g;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/udc/ui/g;->b()Lcom/google/android/gms/udc/ui/j;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Lcom/google/android/gms/udc/util/j;->a(ILandroid/support/v4/app/Fragment;)V

    return-void

    :pswitch_0
    sget v0, Lcom/google/android/gms/p;->yY:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/udc/ui/DeviceUsageActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :pswitch_1
    sget v0, Lcom/google/android/gms/p;->zd:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/udc/ui/DeviceUsageActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x7
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method static synthetic b(Lcom/google/android/gms/udc/ui/DeviceUsageActivity;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lcom/google/android/gms/udc/ui/DeviceUsageActivity;->g:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic c(Lcom/google/android/gms/udc/ui/DeviceUsageActivity;)I
    .locals 1

    .prologue
    .line 37
    iget v0, p0, Lcom/google/android/gms/udc/ui/DeviceUsageActivity;->h:I

    return v0
.end method

.method static synthetic d(Lcom/google/android/gms/udc/ui/DeviceUsageActivity;)Lcom/google/android/gms/analytics/bv;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lcom/google/android/gms/udc/ui/DeviceUsageActivity;->f:Lcom/google/android/gms/analytics/bv;

    return-object v0
.end method

.method static synthetic e(Lcom/google/android/gms/udc/ui/DeviceUsageActivity;)Landroid/view/View;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lcom/google/android/gms/udc/ui/DeviceUsageActivity;->b:Landroid/view/View;

    return-object v0
.end method

.method static synthetic f(Lcom/google/android/gms/udc/ui/DeviceUsageActivity;)Lcom/google/android/gms/udc/util/j;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lcom/google/android/gms/udc/ui/DeviceUsageActivity;->d:Lcom/google/android/gms/udc/util/j;

    return-object v0
.end method

.method static synthetic g(Lcom/google/android/gms/udc/ui/DeviceUsageActivity;)Lcom/google/android/gms/common/widget/SwitchBar;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lcom/google/android/gms/udc/ui/DeviceUsageActivity;->c:Lcom/google/android/gms/common/widget/SwitchBar;

    return-object v0
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/common/widget/SwitchBar;Z)V
    .locals 3

    .prologue
    .line 172
    invoke-virtual {p1}, Lcom/google/android/gms/common/widget/SwitchBar;->getId()I

    move-result v0

    sget v1, Lcom/google/android/gms/j;->sn:I

    if-ne v0, v1, :cond_0

    .line 173
    iget-object v0, p0, Lcom/google/android/gms/udc/ui/DeviceUsageActivity;->d:Lcom/google/android/gms/udc/util/j;

    const/4 v1, 0x2

    new-instance v2, Lcom/google/android/gms/udc/ui/b;

    invoke-direct {v2, p0, p2}, Lcom/google/android/gms/udc/ui/b;-><init>(Lcom/google/android/gms/udc/ui/DeviceUsageActivity;Z)V

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/udc/util/j;->b(ILandroid/support/v4/app/av;)V

    .line 175
    :cond_0
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 76
    invoke-virtual {p0}, Lcom/google/android/gms/udc/ui/DeviceUsageActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 77
    const-string v1, "UdcAccountName"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/gms/udc/ui/DeviceUsageActivity;->g:Ljava/lang/String;

    .line 78
    const-string v1, "UdcStyledTitle"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 79
    const-string v2, "UdcStyledDescription"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 81
    invoke-virtual {p0, v1}, Lcom/google/android/gms/udc/ui/DeviceUsageActivity;->setTitle(Ljava/lang/CharSequence;)V

    .line 83
    invoke-super {p0, p1}, Landroid/support/v7/app/d;->onCreate(Landroid/os/Bundle;)V

    .line 84
    sget v3, Lcom/google/android/gms/l;->al:I

    invoke-virtual {p0, v3}, Lcom/google/android/gms/udc/ui/DeviceUsageActivity;->setContentView(I)V

    .line 86
    new-instance v3, Lcom/google/android/gms/udc/util/j;

    invoke-direct {v3, p0}, Lcom/google/android/gms/udc/util/j;-><init>(Landroid/support/v4/app/q;)V

    iput-object v3, p0, Lcom/google/android/gms/udc/ui/DeviceUsageActivity;->d:Lcom/google/android/gms/udc/util/j;

    .line 87
    new-instance v3, Lcom/google/android/gms/udc/util/f;

    invoke-direct {v3}, Lcom/google/android/gms/udc/util/f;-><init>()V

    iput-object v3, p0, Lcom/google/android/gms/udc/ui/DeviceUsageActivity;->e:Lcom/google/android/gms/udc/util/f;

    .line 88
    const-string v3, "AccountHistoryDeviceSetting"

    invoke-static {p0, v3}, Lcom/google/android/gms/udc/util/a;->a(Landroid/content/Context;Ljava/lang/String;)Lcom/google/android/gms/analytics/bv;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/gms/udc/ui/DeviceUsageActivity;->f:Lcom/google/android/gms/analytics/bv;

    .line 90
    iget-object v3, p0, Lcom/google/android/gms/udc/ui/DeviceUsageActivity;->g:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    const-string v3, "UdcSettingId"

    invoke-virtual {v0, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 93
    :cond_0
    const-string v3, "DeviceUsage"

    const-string v4, "The account name, setting ID, title, and description are required."

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 94
    invoke-virtual {p0, v5}, Lcom/google/android/gms/udc/ui/DeviceUsageActivity;->setResult(I)V

    .line 95
    invoke-virtual {p0}, Lcom/google/android/gms/udc/ui/DeviceUsageActivity;->finish()V

    .line 98
    :cond_1
    const-string v3, "UdcSettingId"

    invoke-virtual {v0, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/udc/ui/DeviceUsageActivity;->h:I

    .line 99
    iget v0, p0, Lcom/google/android/gms/udc/ui/DeviceUsageActivity;->h:I

    const/16 v3, 0x8

    if-eq v0, v3, :cond_2

    iget v0, p0, Lcom/google/android/gms/udc/ui/DeviceUsageActivity;->h:I

    const/4 v3, 0x7

    if-eq v0, v3, :cond_2

    .line 101
    const-string v0, "DeviceUsage"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Unsupported account setting ID: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v4, p0, Lcom/google/android/gms/udc/ui/DeviceUsageActivity;->h:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 102
    invoke-virtual {p0, v5}, Lcom/google/android/gms/udc/ui/DeviceUsageActivity;->setResult(I)V

    .line 103
    invoke-virtual {p0}, Lcom/google/android/gms/udc/ui/DeviceUsageActivity;->finish()V

    .line 106
    :cond_2
    new-instance v0, Lcom/google/android/gms/common/api/w;

    invoke-direct {v0, p0}, Lcom/google/android/gms/common/api/w;-><init>(Landroid/content/Context;)V

    sget-object v3, Lcom/google/android/gms/lockbox/a;->a:Lcom/google/android/gms/common/api/c;

    invoke-virtual {v0, v3}, Lcom/google/android/gms/common/api/w;->a(Lcom/google/android/gms/common/api/c;)Lcom/google/android/gms/common/api/w;

    move-result-object v0

    iget-object v3, p0, Lcom/google/android/gms/udc/ui/DeviceUsageActivity;->g:Ljava/lang/String;

    iput-object v3, v0, Lcom/google/android/gms/common/api/w;->a:Ljava/lang/String;

    const/4 v3, 0x0

    invoke-virtual {v0, p0, v5, v3}, Lcom/google/android/gms/common/api/w;->a(Landroid/support/v4/app/q;ILcom/google/android/gms/common/api/y;)Lcom/google/android/gms/common/api/w;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/common/api/w;->a()Lcom/google/android/gms/common/api/v;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/udc/ui/DeviceUsageActivity;->a:Lcom/google/android/gms/common/api/v;

    .line 112
    invoke-super {p0}, Landroid/support/v7/app/d;->d()Landroid/support/v7/app/e;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v7/app/e;->b()Landroid/support/v7/app/a;

    move-result-object v3

    .line 113
    invoke-static {p0}, Lcom/google/android/gms/common/util/a;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_3

    sget v0, Lcom/google/android/gms/h;->I:I

    :goto_0
    invoke-virtual {v3, v0}, Landroid/support/v7/app/a;->b(I)V

    .line 114
    invoke-virtual {v3, v6}, Landroid/support/v7/app/a;->a(Z)V

    .line 115
    invoke-virtual {v3, v6}, Landroid/support/v7/app/a;->b(Z)V

    .line 116
    invoke-virtual {v3, v1}, Landroid/support/v7/app/a;->a(Ljava/lang/CharSequence;)V

    .line 117
    iget-object v0, p0, Lcom/google/android/gms/udc/ui/DeviceUsageActivity;->g:Ljava/lang/String;

    invoke-virtual {v3, v0}, Landroid/support/v7/app/a;->b(Ljava/lang/CharSequence;)V

    .line 118
    invoke-virtual {v3}, Landroid/support/v7/app/a;->d()V

    .line 120
    sget v0, Lcom/google/android/gms/j;->tF:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/udc/ui/DeviceUsageActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/udc/ui/DeviceUsageActivity;->b:Landroid/view/View;

    .line 121
    sget v0, Lcom/google/android/gms/j;->sn:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/udc/ui/DeviceUsageActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/widget/SwitchBar;

    iput-object v0, p0, Lcom/google/android/gms/udc/ui/DeviceUsageActivity;->c:Lcom/google/android/gms/common/widget/SwitchBar;

    .line 122
    iget-object v0, p0, Lcom/google/android/gms/udc/ui/DeviceUsageActivity;->c:Lcom/google/android/gms/common/widget/SwitchBar;

    sget v1, Lcom/google/android/gms/p;->yQ:I

    invoke-virtual {p0, v1}, Lcom/google/android/gms/udc/ui/DeviceUsageActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/widget/SwitchBar;->a(Ljava/lang/CharSequence;)V

    .line 123
    iget-object v0, p0, Lcom/google/android/gms/udc/ui/DeviceUsageActivity;->c:Lcom/google/android/gms/common/widget/SwitchBar;

    sget v1, Lcom/google/android/gms/p;->yP:I

    invoke-virtual {p0, v1}, Lcom/google/android/gms/udc/ui/DeviceUsageActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/widget/SwitchBar;->b(Ljava/lang/CharSequence;)V

    .line 125
    sget v0, Lcom/google/android/gms/j;->ek:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/udc/ui/DeviceUsageActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 126
    new-instance v3, Lcom/google/android/gms/udc/e/s;

    invoke-direct {v3}, Lcom/google/android/gms/udc/e/s;-><init>()V

    .line 127
    iput-object v2, v3, Lcom/google/android/gms/udc/e/s;->b:Ljava/lang/String;

    .line 128
    iget-object v0, p0, Lcom/google/android/gms/udc/ui/DeviceUsageActivity;->e:Lcom/google/android/gms/udc/util/f;

    sget v2, Lcom/google/android/gms/j;->ek:I

    iget-object v5, p0, Lcom/google/android/gms/udc/ui/DeviceUsageActivity;->g:Ljava/lang/String;

    move-object v4, p0

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/udc/util/f;->a(Landroid/view/View;ILcom/google/android/gms/udc/e/s;Landroid/support/v4/app/q;Ljava/lang/String;)Landroid/widget/TextView;

    .line 130
    return-void

    .line 113
    :cond_3
    sget v0, Lcom/google/android/gms/h;->K:I

    goto :goto_0
.end method

.method protected onDestroy()V
    .locals 2

    .prologue
    .line 152
    iget-object v0, p0, Lcom/google/android/gms/udc/ui/DeviceUsageActivity;->d:Lcom/google/android/gms/udc/util/j;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/gms/udc/util/j;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 153
    invoke-super {p0}, Landroid/support/v7/app/d;->onDestroy()V

    .line 154
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 1

    .prologue
    .line 158
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 163
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 160
    :pswitch_0
    invoke-virtual {p0}, Lcom/google/android/gms/udc/ui/DeviceUsageActivity;->finish()V

    .line 161
    const/4 v0, 0x1

    goto :goto_0

    .line 158
    :pswitch_data_0
    .packed-switch 0x102002c
        :pswitch_0
    .end packed-switch
.end method

.method protected onResume()V
    .locals 4

    .prologue
    .line 134
    invoke-super {p0}, Landroid/support/v7/app/d;->onResume()V

    .line 135
    iget-object v0, p0, Lcom/google/android/gms/udc/ui/DeviceUsageActivity;->d:Lcom/google/android/gms/udc/util/j;

    const/4 v1, 0x1

    new-instance v2, Lcom/google/android/gms/udc/ui/a;

    const/4 v3, 0x0

    invoke-direct {v2, p0, v3}, Lcom/google/android/gms/udc/ui/a;-><init>(Lcom/google/android/gms/udc/ui/DeviceUsageActivity;B)V

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/udc/util/j;->b(ILandroid/support/v4/app/av;)V

    .line 136
    return-void
.end method

.method protected onStart()V
    .locals 1

    .prologue
    .line 140
    invoke-super {p0}, Landroid/support/v7/app/d;->onStart()V

    .line 141
    invoke-static {p0}, Lcom/google/android/gms/analytics/ax;->a(Landroid/content/Context;)Lcom/google/android/gms/analytics/ax;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/android/gms/analytics/ax;->a(Landroid/app/Activity;)V

    .line 142
    return-void
.end method

.method protected onStop()V
    .locals 1

    .prologue
    .line 146
    invoke-super {p0}, Landroid/support/v7/app/d;->onStop()V

    .line 147
    invoke-static {p0}, Lcom/google/android/gms/analytics/ax;->a(Landroid/content/Context;)Lcom/google/android/gms/analytics/ax;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/android/gms/analytics/ax;->c(Landroid/app/Activity;)V

    .line 148
    return-void
.end method
