.class public Lcom/google/android/gms/search/global/h;
.super Lcom/google/android/gms/search/a;
.source "SourceFile"


# direct methods
.method public constructor <init>(Lcom/google/android/gms/icing/impl/u;Ljava/lang/String;Lcom/google/android/gms/search/global/GetGlobalSearchSourcesCall$Request;)V
    .locals 6

    .prologue
    .line 34
    const/4 v1, 0x1

    const/4 v2, 0x2

    move-object v0, p0

    move-object v3, p1

    move-object v4, p2

    move-object v5, p3

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/search/a;-><init>(IILcom/google/android/gms/icing/impl/u;Ljava/lang/String;Ljava/lang/Object;)V

    .line 35
    return-void
.end method

.method private c()Lcom/google/android/gms/search/global/GetGlobalSearchSourcesCall$Response;
    .locals 11

    .prologue
    .line 39
    new-instance v3, Lcom/google/android/gms/search/global/GetGlobalSearchSourcesCall$Response;

    invoke-direct {v3}, Lcom/google/android/gms/search/global/GetGlobalSearchSourcesCall$Response;-><init>()V

    .line 40
    sget-object v0, Lcom/google/android/gms/common/api/Status;->a:Lcom/google/android/gms/common/api/Status;

    iput-object v0, v3, Lcom/google/android/gms/search/global/GetGlobalSearchSourcesCall$Response;->a:Lcom/google/android/gms/common/api/Status;

    .line 42
    iget-object v0, p0, Lcom/google/android/gms/search/global/h;->a:Lcom/google/android/gms/icing/impl/u;

    invoke-virtual {v0}, Lcom/google/android/gms/icing/impl/u;->A()Lcom/google/android/gms/icing/impl/a/f;

    move-result-object v4

    .line 44
    iget-object v0, p0, Lcom/google/android/gms/search/global/h;->a:Lcom/google/android/gms/icing/impl/u;

    invoke-virtual {v0}, Lcom/google/android/gms/icing/impl/u;->i()Lcom/google/android/gms/icing/impl/a/x;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/icing/impl/a/x;->b()Ljava/lang/Object;

    move-result-object v5

    monitor-enter v5

    .line 45
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/search/global/h;->a:Lcom/google/android/gms/icing/impl/u;

    iget-object v1, p0, Lcom/google/android/gms/search/global/h;->c:Lcom/google/android/gms/icing/impl/a/h;

    const/4 v2, 0x0

    const/4 v6, 0x1

    invoke-virtual {v0, v1, v2, v6}, Lcom/google/android/gms/icing/impl/u;->a(Lcom/google/android/gms/icing/impl/a/h;Lcom/google/android/gms/appdatasearch/GlobalSearchQuerySpecification;Z)Ljava/util/List;

    move-result-object v0

    .line 48
    const-string v1, "GetGlobalSearchSourcesTask: corpusConfigs=%s"

    invoke-static {v1, v0}, Lcom/google/android/gms/icing/c;->b(Ljava/lang/String;Ljava/lang/Object;)I

    .line 51
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 53
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/icing/g;

    .line 54
    iget-object v6, v0, Lcom/google/android/gms/icing/g;->d:Ljava/lang/String;

    .line 55
    const-string v7, "GetGlobalSearchSourcesTask: pkgName=%s"

    invoke-static {v7, v6}, Lcom/google/android/gms/icing/c;->b(Ljava/lang/String;Ljava/lang/Object;)I

    .line 56
    invoke-interface {v1, v6}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_0

    .line 57
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {v1, v6, v7}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 59
    :cond_0
    new-instance v7, Lcom/google/android/gms/search/global/GetGlobalSearchSourcesCall$CorpusInfo;

    invoke-direct {v7}, Lcom/google/android/gms/search/global/GetGlobalSearchSourcesCall$CorpusInfo;-><init>()V

    .line 60
    iget-object v8, v0, Lcom/google/android/gms/icing/g;->b:Ljava/lang/String;

    iput-object v8, v7, Lcom/google/android/gms/search/global/GetGlobalSearchSourcesCall$CorpusInfo;->a:Ljava/lang/String;

    .line 61
    invoke-static {v0}, Lcom/google/android/gms/icing/impl/a/j;->g(Lcom/google/android/gms/icing/g;)[Lcom/google/android/gms/appdatasearch/Feature;

    move-result-object v8

    iput-object v8, v7, Lcom/google/android/gms/search/global/GetGlobalSearchSourcesCall$CorpusInfo;->b:[Lcom/google/android/gms/appdatasearch/Feature;

    .line 62
    invoke-static {v0}, Lcom/google/android/gms/icing/impl/a/j;->f(Lcom/google/android/gms/icing/g;)Z

    move-result v0

    iput-boolean v0, v7, Lcom/google/android/gms/search/global/GetGlobalSearchSourcesCall$CorpusInfo;->c:Z

    .line 63
    invoke-interface {v1, v6}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    invoke-interface {v0, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 98
    :catchall_0
    move-exception v0

    monitor-exit v5

    throw v0

    .line 66
    :cond_1
    :try_start_1
    iget-object v0, v4, Lcom/google/android/gms/icing/impl/a/f;->a:Lcom/google/android/gms/icing/impl/a/g;

    invoke-virtual {v0}, Lcom/google/android/gms/icing/impl/a/g;->a()Z

    move-result v6

    .line 70
    iget-object v0, p0, Lcom/google/android/gms/search/global/h;->b:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/gms/search/global/GetGlobalSearchSourcesCall$Request;

    iget-boolean v0, v0, Lcom/google/android/gms/search/global/GetGlobalSearchSourcesCall$Request;->a:Z

    if-nez v0, :cond_3

    .line 71
    invoke-interface {v1}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_2
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 72
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 73
    invoke-virtual {v4, v0}, Lcom/google/android/gms/icing/impl/a/f;->c(Ljava/lang/String;)Lcom/google/android/gms/icing/impl/a/e;

    move-result-object v0

    .line 74
    invoke-virtual {v0, v6}, Lcom/google/android/gms/icing/impl/a/e;->b(Z)Z

    move-result v0

    if-nez v0, :cond_2

    .line 75
    invoke-interface {v2}, Ljava/util/Iterator;->remove()V

    goto :goto_1

    .line 80
    :cond_3
    invoke-interface {v1}, Ljava/util/Map;->size()I

    move-result v0

    new-array v0, v0, [Lcom/google/android/gms/search/global/GetGlobalSearchSourcesCall$GlobalSearchSource;

    iput-object v0, v3, Lcom/google/android/gms/search/global/GetGlobalSearchSourcesCall$Response;->b:[Lcom/google/android/gms/search/global/GetGlobalSearchSourcesCall$GlobalSearchSource;

    .line 81
    const/4 v0, 0x0

    .line 82
    invoke-interface {v1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v7

    move v2, v0

    :goto_2
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 83
    new-instance v8, Lcom/google/android/gms/search/global/GetGlobalSearchSourcesCall$GlobalSearchSource;

    invoke-direct {v8}, Lcom/google/android/gms/search/global/GetGlobalSearchSourcesCall$GlobalSearchSource;-><init>()V

    .line 84
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    iput-object v1, v8, Lcom/google/android/gms/search/global/GetGlobalSearchSourcesCall$GlobalSearchSource;->a:Ljava/lang/String;

    .line 85
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v4, v1}, Lcom/google/android/gms/icing/impl/a/f;->c(Ljava/lang/String;)Lcom/google/android/gms/icing/impl/a/e;

    move-result-object v9

    .line 86
    invoke-static {v9}, Lcom/google/android/gms/common/internal/e;->a(Ljava/lang/Object;)V

    .line 87
    invoke-virtual {v9}, Lcom/google/android/gms/icing/impl/a/e;->f()Lcom/google/android/gms/icing/impl/a/ac;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/icing/impl/a/ac;->d()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/appdatasearch/GlobalSearchApplicationInfo;

    .line 88
    iget v10, v1, Lcom/google/android/gms/appdatasearch/GlobalSearchApplicationInfo;->c:I

    iput v10, v8, Lcom/google/android/gms/search/global/GetGlobalSearchSourcesCall$GlobalSearchSource;->b:I

    .line 89
    iget v10, v1, Lcom/google/android/gms/appdatasearch/GlobalSearchApplicationInfo;->d:I

    iput v10, v8, Lcom/google/android/gms/search/global/GetGlobalSearchSourcesCall$GlobalSearchSource;->c:I

    .line 90
    iget v10, v1, Lcom/google/android/gms/appdatasearch/GlobalSearchApplicationInfo;->e:I

    iput v10, v8, Lcom/google/android/gms/search/global/GetGlobalSearchSourcesCall$GlobalSearchSource;->d:I

    .line 91
    iget-object v10, v1, Lcom/google/android/gms/appdatasearch/GlobalSearchApplicationInfo;->f:Ljava/lang/String;

    iput-object v10, v8, Lcom/google/android/gms/search/global/GetGlobalSearchSourcesCall$GlobalSearchSource;->e:Ljava/lang/String;

    .line 92
    iget-object v10, v1, Lcom/google/android/gms/appdatasearch/GlobalSearchApplicationInfo;->g:Ljava/lang/String;

    iput-object v10, v8, Lcom/google/android/gms/search/global/GetGlobalSearchSourcesCall$GlobalSearchSource;->f:Ljava/lang/String;

    .line 93
    iget-object v1, v1, Lcom/google/android/gms/appdatasearch/GlobalSearchApplicationInfo;->h:Ljava/lang/String;

    iput-object v1, v8, Lcom/google/android/gms/search/global/GetGlobalSearchSourcesCall$GlobalSearchSource;->g:Ljava/lang/String;

    .line 94
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    new-array v0, v0, [Lcom/google/android/gms/search/global/GetGlobalSearchSourcesCall$CorpusInfo;

    invoke-interface {v1, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/gms/search/global/GetGlobalSearchSourcesCall$CorpusInfo;

    iput-object v0, v8, Lcom/google/android/gms/search/global/GetGlobalSearchSourcesCall$GlobalSearchSource;->h:[Lcom/google/android/gms/search/global/GetGlobalSearchSourcesCall$CorpusInfo;

    .line 95
    invoke-virtual {v9, v6}, Lcom/google/android/gms/icing/impl/a/e;->b(Z)Z

    move-result v0

    iput-boolean v0, v8, Lcom/google/android/gms/search/global/GetGlobalSearchSourcesCall$GlobalSearchSource;->i:Z

    .line 96
    iget-object v1, v3, Lcom/google/android/gms/search/global/GetGlobalSearchSourcesCall$Response;->b:[Lcom/google/android/gms/search/global/GetGlobalSearchSourcesCall$GlobalSearchSource;

    add-int/lit8 v0, v2, 0x1

    aput-object v8, v1, v2

    move v2, v0

    .line 97
    goto :goto_2

    .line 98
    :cond_4
    monitor-exit v5
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 99
    return-object v3
.end method


# virtual methods
.method protected final synthetic a()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 30
    invoke-direct {p0}, Lcom/google/android/gms/search/global/h;->c()Lcom/google/android/gms/search/global/GetGlobalSearchSourcesCall$Response;

    move-result-object v0

    return-object v0
.end method
