.class public final Lcom/google/android/gms/icing/g;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# instance fields
.field public a:I

.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/String;

.field public e:Ljava/lang/String;

.field public f:I

.field public g:Z

.field public h:J

.field public i:J

.field public j:[Lcom/google/android/gms/icing/av;

.field public k:[Lcom/google/android/gms/icing/az;

.field public l:I

.field public m:Z

.field public n:J

.field public o:Ljava/lang/String;

.field public p:Ljava/lang/String;

.field public q:Lcom/google/android/gms/icing/h;

.field public r:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const-wide/16 v2, 0x0

    const/4 v1, 0x0

    .line 1698
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 1699
    iput v1, p0, Lcom/google/android/gms/icing/g;->a:I

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/gms/icing/g;->b:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/gms/icing/g;->c:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/gms/icing/g;->d:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/gms/icing/g;->e:Ljava/lang/String;

    iput v1, p0, Lcom/google/android/gms/icing/g;->f:I

    iput-boolean v4, p0, Lcom/google/android/gms/icing/g;->g:Z

    iput-wide v2, p0, Lcom/google/android/gms/icing/g;->h:J

    iput-wide v2, p0, Lcom/google/android/gms/icing/g;->i:J

    invoke-static {}, Lcom/google/android/gms/icing/av;->a()[Lcom/google/android/gms/icing/av;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/icing/g;->j:[Lcom/google/android/gms/icing/av;

    invoke-static {}, Lcom/google/android/gms/icing/az;->a()[Lcom/google/android/gms/icing/az;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/icing/g;->k:[Lcom/google/android/gms/icing/az;

    iput v4, p0, Lcom/google/android/gms/icing/g;->l:I

    iput-boolean v1, p0, Lcom/google/android/gms/icing/g;->m:Z

    iput-wide v2, p0, Lcom/google/android/gms/icing/g;->n:J

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/gms/icing/g;->o:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/gms/icing/g;->p:Ljava/lang/String;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/icing/g;->q:Lcom/google/android/gms/icing/h;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/gms/icing/g;->r:Ljava/lang/String;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/icing/g;->cachedSize:I

    .line 1700
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 8

    .prologue
    const/4 v1, 0x0

    const-wide/16 v6, 0x0

    const/4 v5, 0x1

    .line 1936
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->computeSerializedSize()I

    move-result v0

    .line 1937
    iget-object v2, p0, Lcom/google/android/gms/icing/g;->b:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 1938
    iget-object v2, p0, Lcom/google/android/gms/icing/g;->b:Ljava/lang/String;

    invoke-static {v5, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 1941
    :cond_0
    iget-object v2, p0, Lcom/google/android/gms/icing/g;->d:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 1942
    const/4 v2, 0x2

    iget-object v3, p0, Lcom/google/android/gms/icing/g;->d:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 1945
    :cond_1
    iget-object v2, p0, Lcom/google/android/gms/icing/g;->e:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 1946
    const/4 v2, 0x4

    iget-object v3, p0, Lcom/google/android/gms/icing/g;->e:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 1949
    :cond_2
    iget v2, p0, Lcom/google/android/gms/icing/g;->f:I

    if-eqz v2, :cond_3

    .line 1950
    const/4 v2, 0x5

    iget v3, p0, Lcom/google/android/gms/icing/g;->f:I

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 1953
    :cond_3
    iget-object v2, p0, Lcom/google/android/gms/icing/g;->j:[Lcom/google/android/gms/icing/av;

    if-eqz v2, :cond_6

    iget-object v2, p0, Lcom/google/android/gms/icing/g;->j:[Lcom/google/android/gms/icing/av;

    array-length v2, v2

    if-lez v2, :cond_6

    move v2, v0

    move v0, v1

    .line 1954
    :goto_0
    iget-object v3, p0, Lcom/google/android/gms/icing/g;->j:[Lcom/google/android/gms/icing/av;

    array-length v3, v3

    if-ge v0, v3, :cond_5

    .line 1955
    iget-object v3, p0, Lcom/google/android/gms/icing/g;->j:[Lcom/google/android/gms/icing/av;

    aget-object v3, v3, v0

    .line 1956
    if-eqz v3, :cond_4

    .line 1957
    const/4 v4, 0x6

    invoke-static {v4, v3}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v3

    add-int/2addr v2, v3

    .line 1954
    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_5
    move v0, v2

    .line 1962
    :cond_6
    iget-object v2, p0, Lcom/google/android/gms/icing/g;->k:[Lcom/google/android/gms/icing/az;

    if-eqz v2, :cond_8

    iget-object v2, p0, Lcom/google/android/gms/icing/g;->k:[Lcom/google/android/gms/icing/az;

    array-length v2, v2

    if-lez v2, :cond_8

    .line 1963
    :goto_1
    iget-object v2, p0, Lcom/google/android/gms/icing/g;->k:[Lcom/google/android/gms/icing/az;

    array-length v2, v2

    if-ge v1, v2, :cond_8

    .line 1964
    iget-object v2, p0, Lcom/google/android/gms/icing/g;->k:[Lcom/google/android/gms/icing/az;

    aget-object v2, v2, v1

    .line 1965
    if-eqz v2, :cond_7

    .line 1966
    const/4 v3, 0x7

    invoke-static {v3, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v2

    add-int/2addr v0, v2

    .line 1963
    :cond_7
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 1971
    :cond_8
    iget v1, p0, Lcom/google/android/gms/icing/g;->a:I

    if-eqz v1, :cond_9

    .line 1972
    const/16 v1, 0x8

    iget v2, p0, Lcom/google/android/gms/icing/g;->a:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1975
    :cond_9
    iget-object v1, p0, Lcom/google/android/gms/icing/g;->c:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_a

    .line 1976
    const/16 v1, 0x9

    iget-object v2, p0, Lcom/google/android/gms/icing/g;->c:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1979
    :cond_a
    iget-boolean v1, p0, Lcom/google/android/gms/icing/g;->g:Z

    if-eq v1, v5, :cond_b

    .line 1980
    const/16 v1, 0xa

    iget-boolean v2, p0, Lcom/google/android/gms/icing/g;->g:Z

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 1983
    :cond_b
    iget-wide v2, p0, Lcom/google/android/gms/icing/g;->h:J

    cmp-long v1, v2, v6

    if-eqz v1, :cond_c

    .line 1984
    const/16 v1, 0xb

    iget-wide v2, p0, Lcom/google/android/gms/icing/g;->h:J

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/b;->e(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 1987
    :cond_c
    iget v1, p0, Lcom/google/android/gms/icing/g;->l:I

    if-eq v1, v5, :cond_d

    .line 1988
    const/16 v1, 0xc

    iget v2, p0, Lcom/google/android/gms/icing/g;->l:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1991
    :cond_d
    iget-boolean v1, p0, Lcom/google/android/gms/icing/g;->m:Z

    if-eqz v1, :cond_e

    .line 1992
    const/16 v1, 0xd

    iget-boolean v2, p0, Lcom/google/android/gms/icing/g;->m:Z

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 1995
    :cond_e
    iget-wide v2, p0, Lcom/google/android/gms/icing/g;->n:J

    cmp-long v1, v2, v6

    if-eqz v1, :cond_f

    .line 1996
    const/16 v1, 0xe

    iget-wide v2, p0, Lcom/google/android/gms/icing/g;->n:J

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/b;->f(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 1999
    :cond_f
    iget-wide v2, p0, Lcom/google/android/gms/icing/g;->i:J

    cmp-long v1, v2, v6

    if-eqz v1, :cond_10

    .line 2000
    const/16 v1, 0xf

    iget-wide v2, p0, Lcom/google/android/gms/icing/g;->i:J

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/b;->e(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 2003
    :cond_10
    iget-object v1, p0, Lcom/google/android/gms/icing/g;->o:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_11

    .line 2004
    const/16 v1, 0x10

    iget-object v2, p0, Lcom/google/android/gms/icing/g;->o:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2007
    :cond_11
    iget-object v1, p0, Lcom/google/android/gms/icing/g;->p:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_12

    .line 2008
    const/16 v1, 0x11

    iget-object v2, p0, Lcom/google/android/gms/icing/g;->p:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2011
    :cond_12
    iget-object v1, p0, Lcom/google/android/gms/icing/g;->q:Lcom/google/android/gms/icing/h;

    if-eqz v1, :cond_13

    .line 2012
    const/16 v1, 0x12

    iget-object v2, p0, Lcom/google/android/gms/icing/g;->q:Lcom/google/android/gms/icing/h;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2015
    :cond_13
    iget-object v1, p0, Lcom/google/android/gms/icing/g;->r:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_14

    .line 2016
    const/16 v1, 0x13

    iget-object v2, p0, Lcom/google/android/gms/icing/g;->r:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2019
    :cond_14
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1727
    if-ne p1, p0, :cond_1

    .line 1824
    :cond_0
    :goto_0
    return v0

    .line 1730
    :cond_1
    instance-of v2, p1, Lcom/google/android/gms/icing/g;

    if-nez v2, :cond_2

    move v0, v1

    .line 1731
    goto :goto_0

    .line 1733
    :cond_2
    check-cast p1, Lcom/google/android/gms/icing/g;

    .line 1734
    iget v2, p0, Lcom/google/android/gms/icing/g;->a:I

    iget v3, p1, Lcom/google/android/gms/icing/g;->a:I

    if-eq v2, v3, :cond_3

    move v0, v1

    .line 1735
    goto :goto_0

    .line 1737
    :cond_3
    iget-object v2, p0, Lcom/google/android/gms/icing/g;->b:Ljava/lang/String;

    if-nez v2, :cond_4

    .line 1738
    iget-object v2, p1, Lcom/google/android/gms/icing/g;->b:Ljava/lang/String;

    if-eqz v2, :cond_5

    move v0, v1

    .line 1739
    goto :goto_0

    .line 1741
    :cond_4
    iget-object v2, p0, Lcom/google/android/gms/icing/g;->b:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/android/gms/icing/g;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_5

    move v0, v1

    .line 1742
    goto :goto_0

    .line 1744
    :cond_5
    iget-object v2, p0, Lcom/google/android/gms/icing/g;->c:Ljava/lang/String;

    if-nez v2, :cond_6

    .line 1745
    iget-object v2, p1, Lcom/google/android/gms/icing/g;->c:Ljava/lang/String;

    if-eqz v2, :cond_7

    move v0, v1

    .line 1746
    goto :goto_0

    .line 1748
    :cond_6
    iget-object v2, p0, Lcom/google/android/gms/icing/g;->c:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/android/gms/icing/g;->c:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_7

    move v0, v1

    .line 1749
    goto :goto_0

    .line 1751
    :cond_7
    iget-object v2, p0, Lcom/google/android/gms/icing/g;->d:Ljava/lang/String;

    if-nez v2, :cond_8

    .line 1752
    iget-object v2, p1, Lcom/google/android/gms/icing/g;->d:Ljava/lang/String;

    if-eqz v2, :cond_9

    move v0, v1

    .line 1753
    goto :goto_0

    .line 1755
    :cond_8
    iget-object v2, p0, Lcom/google/android/gms/icing/g;->d:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/android/gms/icing/g;->d:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    move v0, v1

    .line 1756
    goto :goto_0

    .line 1758
    :cond_9
    iget-object v2, p0, Lcom/google/android/gms/icing/g;->e:Ljava/lang/String;

    if-nez v2, :cond_a

    .line 1759
    iget-object v2, p1, Lcom/google/android/gms/icing/g;->e:Ljava/lang/String;

    if-eqz v2, :cond_b

    move v0, v1

    .line 1760
    goto :goto_0

    .line 1762
    :cond_a
    iget-object v2, p0, Lcom/google/android/gms/icing/g;->e:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/android/gms/icing/g;->e:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_b

    move v0, v1

    .line 1763
    goto :goto_0

    .line 1765
    :cond_b
    iget v2, p0, Lcom/google/android/gms/icing/g;->f:I

    iget v3, p1, Lcom/google/android/gms/icing/g;->f:I

    if-eq v2, v3, :cond_c

    move v0, v1

    .line 1766
    goto :goto_0

    .line 1768
    :cond_c
    iget-boolean v2, p0, Lcom/google/android/gms/icing/g;->g:Z

    iget-boolean v3, p1, Lcom/google/android/gms/icing/g;->g:Z

    if-eq v2, v3, :cond_d

    move v0, v1

    .line 1769
    goto :goto_0

    .line 1771
    :cond_d
    iget-wide v2, p0, Lcom/google/android/gms/icing/g;->h:J

    iget-wide v4, p1, Lcom/google/android/gms/icing/g;->h:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_e

    move v0, v1

    .line 1772
    goto/16 :goto_0

    .line 1774
    :cond_e
    iget-wide v2, p0, Lcom/google/android/gms/icing/g;->i:J

    iget-wide v4, p1, Lcom/google/android/gms/icing/g;->i:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_f

    move v0, v1

    .line 1775
    goto/16 :goto_0

    .line 1777
    :cond_f
    iget-object v2, p0, Lcom/google/android/gms/icing/g;->j:[Lcom/google/android/gms/icing/av;

    iget-object v3, p1, Lcom/google/android/gms/icing/g;->j:[Lcom/google/android/gms/icing/av;

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_10

    move v0, v1

    .line 1779
    goto/16 :goto_0

    .line 1781
    :cond_10
    iget-object v2, p0, Lcom/google/android/gms/icing/g;->k:[Lcom/google/android/gms/icing/az;

    iget-object v3, p1, Lcom/google/android/gms/icing/g;->k:[Lcom/google/android/gms/icing/az;

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_11

    move v0, v1

    .line 1783
    goto/16 :goto_0

    .line 1785
    :cond_11
    iget v2, p0, Lcom/google/android/gms/icing/g;->l:I

    iget v3, p1, Lcom/google/android/gms/icing/g;->l:I

    if-eq v2, v3, :cond_12

    move v0, v1

    .line 1786
    goto/16 :goto_0

    .line 1788
    :cond_12
    iget-boolean v2, p0, Lcom/google/android/gms/icing/g;->m:Z

    iget-boolean v3, p1, Lcom/google/android/gms/icing/g;->m:Z

    if-eq v2, v3, :cond_13

    move v0, v1

    .line 1789
    goto/16 :goto_0

    .line 1791
    :cond_13
    iget-wide v2, p0, Lcom/google/android/gms/icing/g;->n:J

    iget-wide v4, p1, Lcom/google/android/gms/icing/g;->n:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_14

    move v0, v1

    .line 1792
    goto/16 :goto_0

    .line 1794
    :cond_14
    iget-object v2, p0, Lcom/google/android/gms/icing/g;->o:Ljava/lang/String;

    if-nez v2, :cond_15

    .line 1795
    iget-object v2, p1, Lcom/google/android/gms/icing/g;->o:Ljava/lang/String;

    if-eqz v2, :cond_16

    move v0, v1

    .line 1796
    goto/16 :goto_0

    .line 1798
    :cond_15
    iget-object v2, p0, Lcom/google/android/gms/icing/g;->o:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/android/gms/icing/g;->o:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_16

    move v0, v1

    .line 1799
    goto/16 :goto_0

    .line 1801
    :cond_16
    iget-object v2, p0, Lcom/google/android/gms/icing/g;->p:Ljava/lang/String;

    if-nez v2, :cond_17

    .line 1802
    iget-object v2, p1, Lcom/google/android/gms/icing/g;->p:Ljava/lang/String;

    if-eqz v2, :cond_18

    move v0, v1

    .line 1803
    goto/16 :goto_0

    .line 1805
    :cond_17
    iget-object v2, p0, Lcom/google/android/gms/icing/g;->p:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/android/gms/icing/g;->p:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_18

    move v0, v1

    .line 1806
    goto/16 :goto_0

    .line 1808
    :cond_18
    iget-object v2, p0, Lcom/google/android/gms/icing/g;->q:Lcom/google/android/gms/icing/h;

    if-nez v2, :cond_19

    .line 1809
    iget-object v2, p1, Lcom/google/android/gms/icing/g;->q:Lcom/google/android/gms/icing/h;

    if-eqz v2, :cond_1a

    move v0, v1

    .line 1810
    goto/16 :goto_0

    .line 1813
    :cond_19
    iget-object v2, p0, Lcom/google/android/gms/icing/g;->q:Lcom/google/android/gms/icing/h;

    iget-object v3, p1, Lcom/google/android/gms/icing/g;->q:Lcom/google/android/gms/icing/h;

    invoke-virtual {v2, v3}, Lcom/google/android/gms/icing/h;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1a

    move v0, v1

    .line 1814
    goto/16 :goto_0

    .line 1817
    :cond_1a
    iget-object v2, p0, Lcom/google/android/gms/icing/g;->r:Ljava/lang/String;

    if-nez v2, :cond_1b

    .line 1818
    iget-object v2, p1, Lcom/google/android/gms/icing/g;->r:Ljava/lang/String;

    if-eqz v2, :cond_0

    move v0, v1

    .line 1819
    goto/16 :goto_0

    .line 1821
    :cond_1b
    iget-object v2, p0, Lcom/google/android/gms/icing/g;->r:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/android/gms/icing/g;->r:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    .line 1822
    goto/16 :goto_0
.end method

.method public final hashCode()I
    .locals 9

    .prologue
    const/16 v3, 0x4d5

    const/16 v2, 0x4cf

    const/16 v8, 0x20

    const/4 v1, 0x0

    .line 1829
    iget v0, p0, Lcom/google/android/gms/icing/g;->a:I

    add-int/lit16 v0, v0, 0x20f

    .line 1831
    mul-int/lit8 v4, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/gms/icing/g;->b:Ljava/lang/String;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/2addr v0, v4

    .line 1833
    mul-int/lit8 v4, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/gms/icing/g;->c:Ljava/lang/String;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v4

    .line 1835
    mul-int/lit8 v4, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/gms/icing/g;->d:Ljava/lang/String;

    if-nez v0, :cond_2

    move v0, v1

    :goto_2
    add-int/2addr v0, v4

    .line 1837
    mul-int/lit8 v4, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/gms/icing/g;->e:Ljava/lang/String;

    if-nez v0, :cond_3

    move v0, v1

    :goto_3
    add-int/2addr v0, v4

    .line 1839
    mul-int/lit8 v0, v0, 0x1f

    iget v4, p0, Lcom/google/android/gms/icing/g;->f:I

    add-int/2addr v0, v4

    .line 1840
    mul-int/lit8 v4, v0, 0x1f

    iget-boolean v0, p0, Lcom/google/android/gms/icing/g;->g:Z

    if-eqz v0, :cond_4

    move v0, v2

    :goto_4
    add-int/2addr v0, v4

    .line 1841
    mul-int/lit8 v0, v0, 0x1f

    iget-wide v4, p0, Lcom/google/android/gms/icing/g;->h:J

    iget-wide v6, p0, Lcom/google/android/gms/icing/g;->h:J

    ushr-long/2addr v6, v8

    xor-long/2addr v4, v6

    long-to-int v4, v4

    add-int/2addr v0, v4

    .line 1843
    mul-int/lit8 v0, v0, 0x1f

    iget-wide v4, p0, Lcom/google/android/gms/icing/g;->i:J

    iget-wide v6, p0, Lcom/google/android/gms/icing/g;->i:J

    ushr-long/2addr v6, v8

    xor-long/2addr v4, v6

    long-to-int v4, v4

    add-int/2addr v0, v4

    .line 1845
    mul-int/lit8 v0, v0, 0x1f

    iget-object v4, p0, Lcom/google/android/gms/icing/g;->j:[Lcom/google/android/gms/icing/av;

    invoke-static {v4}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;)I

    move-result v4

    add-int/2addr v0, v4

    .line 1847
    mul-int/lit8 v0, v0, 0x1f

    iget-object v4, p0, Lcom/google/android/gms/icing/g;->k:[Lcom/google/android/gms/icing/az;

    invoke-static {v4}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;)I

    move-result v4

    add-int/2addr v0, v4

    .line 1849
    mul-int/lit8 v0, v0, 0x1f

    iget v4, p0, Lcom/google/android/gms/icing/g;->l:I

    add-int/2addr v0, v4

    .line 1850
    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v4, p0, Lcom/google/android/gms/icing/g;->m:Z

    if-eqz v4, :cond_5

    :goto_5
    add-int/2addr v0, v2

    .line 1851
    mul-int/lit8 v0, v0, 0x1f

    iget-wide v2, p0, Lcom/google/android/gms/icing/g;->n:J

    iget-wide v4, p0, Lcom/google/android/gms/icing/g;->n:J

    ushr-long/2addr v4, v8

    xor-long/2addr v2, v4

    long-to-int v2, v2

    add-int/2addr v0, v2

    .line 1853
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/gms/icing/g;->o:Ljava/lang/String;

    if-nez v0, :cond_6

    move v0, v1

    :goto_6
    add-int/2addr v0, v2

    .line 1855
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/gms/icing/g;->p:Ljava/lang/String;

    if-nez v0, :cond_7

    move v0, v1

    :goto_7
    add-int/2addr v0, v2

    .line 1857
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/gms/icing/g;->q:Lcom/google/android/gms/icing/h;

    if-nez v0, :cond_8

    move v0, v1

    :goto_8
    add-int/2addr v0, v2

    .line 1859
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/android/gms/icing/g;->r:Ljava/lang/String;

    if-nez v2, :cond_9

    :goto_9
    add-int/2addr v0, v1

    .line 1861
    return v0

    .line 1831
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/icing/g;->b:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto/16 :goto_0

    .line 1833
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/icing/g;->c:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto/16 :goto_1

    .line 1835
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/icing/g;->d:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto/16 :goto_2

    .line 1837
    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/icing/g;->e:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto/16 :goto_3

    :cond_4
    move v0, v3

    .line 1840
    goto :goto_4

    :cond_5
    move v2, v3

    .line 1850
    goto :goto_5

    .line 1853
    :cond_6
    iget-object v0, p0, Lcom/google/android/gms/icing/g;->o:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_6

    .line 1855
    :cond_7
    iget-object v0, p0, Lcom/google/android/gms/icing/g;->p:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_7

    .line 1857
    :cond_8
    iget-object v0, p0, Lcom/google/android/gms/icing/g;->q:Lcom/google/android/gms/icing/h;

    invoke-virtual {v0}, Lcom/google/android/gms/icing/h;->hashCode()I

    move-result v0

    goto :goto_8

    .line 1859
    :cond_9
    iget-object v1, p0, Lcom/google/android/gms/icing/g;->r:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_9
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 1327
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/icing/g;->b:Ljava/lang/String;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/icing/g;->d:Ljava/lang/String;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/icing/g;->e:Ljava/lang/String;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/icing/g;->f:I

    goto :goto_0

    :sswitch_5
    const/16 v0, 0x32

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->b(Lcom/google/protobuf/nano/a;I)I

    move-result v2

    iget-object v0, p0, Lcom/google/android/gms/icing/g;->j:[Lcom/google/android/gms/icing/av;

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lcom/google/android/gms/icing/av;

    if-eqz v0, :cond_1

    iget-object v3, p0, Lcom/google/android/gms/icing/g;->j:[Lcom/google/android/gms/icing/av;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_1
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_3

    new-instance v3, Lcom/google/android/gms/icing/av;

    invoke-direct {v3}, Lcom/google/android/gms/icing/av;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/icing/g;->j:[Lcom/google/android/gms/icing/av;

    array-length v0, v0

    goto :goto_1

    :cond_3
    new-instance v3, Lcom/google/android/gms/icing/av;

    invoke-direct {v3}, Lcom/google/android/gms/icing/av;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    iput-object v2, p0, Lcom/google/android/gms/icing/g;->j:[Lcom/google/android/gms/icing/av;

    goto :goto_0

    :sswitch_6
    const/16 v0, 0x3a

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->b(Lcom/google/protobuf/nano/a;I)I

    move-result v2

    iget-object v0, p0, Lcom/google/android/gms/icing/g;->k:[Lcom/google/android/gms/icing/az;

    if-nez v0, :cond_5

    move v0, v1

    :goto_3
    add-int/2addr v2, v0

    new-array v2, v2, [Lcom/google/android/gms/icing/az;

    if-eqz v0, :cond_4

    iget-object v3, p0, Lcom/google/android/gms/icing/g;->k:[Lcom/google/android/gms/icing/az;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_4
    :goto_4
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_6

    new-instance v3, Lcom/google/android/gms/icing/az;

    invoke-direct {v3}, Lcom/google/android/gms/icing/az;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_5
    iget-object v0, p0, Lcom/google/android/gms/icing/g;->k:[Lcom/google/android/gms/icing/az;

    array-length v0, v0

    goto :goto_3

    :cond_6
    new-instance v3, Lcom/google/android/gms/icing/az;

    invoke-direct {v3}, Lcom/google/android/gms/icing/az;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    iput-object v2, p0, Lcom/google/android/gms/icing/g;->k:[Lcom/google/android/gms/icing/az;

    goto/16 :goto_0

    :sswitch_7
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/icing/g;->a:I

    goto/16 :goto_0

    :sswitch_8
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/icing/g;->c:Ljava/lang/String;

    goto/16 :goto_0

    :sswitch_9
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/icing/g;->g:Z

    goto/16 :goto_0

    :sswitch_a
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->j()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/android/gms/icing/g;->h:J

    goto/16 :goto_0

    :sswitch_b
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/icing/g;->l:I

    goto/16 :goto_0

    :sswitch_c
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/icing/g;->m:Z

    goto/16 :goto_0

    :sswitch_d
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->j()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/android/gms/icing/g;->n:J

    goto/16 :goto_0

    :sswitch_e
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->j()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/android/gms/icing/g;->i:J

    goto/16 :goto_0

    :sswitch_f
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/icing/g;->o:Ljava/lang/String;

    goto/16 :goto_0

    :sswitch_10
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/icing/g;->p:Ljava/lang/String;

    goto/16 :goto_0

    :sswitch_11
    iget-object v0, p0, Lcom/google/android/gms/icing/g;->q:Lcom/google/android/gms/icing/h;

    if-nez v0, :cond_7

    new-instance v0, Lcom/google/android/gms/icing/h;

    invoke-direct {v0}, Lcom/google/android/gms/icing/h;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/icing/g;->q:Lcom/google/android/gms/icing/h;

    :cond_7
    iget-object v0, p0, Lcom/google/android/gms/icing/g;->q:Lcom/google/android/gms/icing/h;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto/16 :goto_0

    :sswitch_12
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/icing/g;->r:Ljava/lang/String;

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x22 -> :sswitch_3
        0x28 -> :sswitch_4
        0x32 -> :sswitch_5
        0x3a -> :sswitch_6
        0x40 -> :sswitch_7
        0x4a -> :sswitch_8
        0x50 -> :sswitch_9
        0x58 -> :sswitch_a
        0x60 -> :sswitch_b
        0x68 -> :sswitch_c
        0x70 -> :sswitch_d
        0x78 -> :sswitch_e
        0x82 -> :sswitch_f
        0x8a -> :sswitch_10
        0x92 -> :sswitch_11
        0x9a -> :sswitch_12
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 8

    .prologue
    const/4 v1, 0x0

    const-wide/16 v6, 0x0

    const/4 v4, 0x1

    .line 1867
    iget-object v0, p0, Lcom/google/android/gms/icing/g;->b:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1868
    iget-object v0, p0, Lcom/google/android/gms/icing/g;->b:Ljava/lang/String;

    invoke-virtual {p1, v4, v0}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 1870
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/icing/g;->d:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1871
    const/4 v0, 0x2

    iget-object v2, p0, Lcom/google/android/gms/icing/g;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v2}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 1873
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/icing/g;->e:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 1874
    const/4 v0, 0x4

    iget-object v2, p0, Lcom/google/android/gms/icing/g;->e:Ljava/lang/String;

    invoke-virtual {p1, v0, v2}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 1876
    :cond_2
    iget v0, p0, Lcom/google/android/gms/icing/g;->f:I

    if-eqz v0, :cond_3

    .line 1877
    const/4 v0, 0x5

    iget v2, p0, Lcom/google/android/gms/icing/g;->f:I

    invoke-virtual {p1, v0, v2}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 1879
    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/icing/g;->j:[Lcom/google/android/gms/icing/av;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/google/android/gms/icing/g;->j:[Lcom/google/android/gms/icing/av;

    array-length v0, v0

    if-lez v0, :cond_5

    move v0, v1

    .line 1880
    :goto_0
    iget-object v2, p0, Lcom/google/android/gms/icing/g;->j:[Lcom/google/android/gms/icing/av;

    array-length v2, v2

    if-ge v0, v2, :cond_5

    .line 1881
    iget-object v2, p0, Lcom/google/android/gms/icing/g;->j:[Lcom/google/android/gms/icing/av;

    aget-object v2, v2, v0

    .line 1882
    if-eqz v2, :cond_4

    .line 1883
    const/4 v3, 0x6

    invoke-virtual {p1, v3, v2}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 1880
    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1887
    :cond_5
    iget-object v0, p0, Lcom/google/android/gms/icing/g;->k:[Lcom/google/android/gms/icing/az;

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/google/android/gms/icing/g;->k:[Lcom/google/android/gms/icing/az;

    array-length v0, v0

    if-lez v0, :cond_7

    .line 1888
    :goto_1
    iget-object v0, p0, Lcom/google/android/gms/icing/g;->k:[Lcom/google/android/gms/icing/az;

    array-length v0, v0

    if-ge v1, v0, :cond_7

    .line 1889
    iget-object v0, p0, Lcom/google/android/gms/icing/g;->k:[Lcom/google/android/gms/icing/az;

    aget-object v0, v0, v1

    .line 1890
    if-eqz v0, :cond_6

    .line 1891
    const/4 v2, 0x7

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 1888
    :cond_6
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 1895
    :cond_7
    iget v0, p0, Lcom/google/android/gms/icing/g;->a:I

    if-eqz v0, :cond_8

    .line 1896
    const/16 v0, 0x8

    iget v1, p0, Lcom/google/android/gms/icing/g;->a:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 1898
    :cond_8
    iget-object v0, p0, Lcom/google/android/gms/icing/g;->c:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_9

    .line 1899
    const/16 v0, 0x9

    iget-object v1, p0, Lcom/google/android/gms/icing/g;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 1901
    :cond_9
    iget-boolean v0, p0, Lcom/google/android/gms/icing/g;->g:Z

    if-eq v0, v4, :cond_a

    .line 1902
    const/16 v0, 0xa

    iget-boolean v1, p0, Lcom/google/android/gms/icing/g;->g:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 1904
    :cond_a
    iget-wide v0, p0, Lcom/google/android/gms/icing/g;->h:J

    cmp-long v0, v0, v6

    if-eqz v0, :cond_b

    .line 1905
    const/16 v0, 0xb

    iget-wide v2, p0, Lcom/google/android/gms/icing/g;->h:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/b;->a(IJ)V

    .line 1907
    :cond_b
    iget v0, p0, Lcom/google/android/gms/icing/g;->l:I

    if-eq v0, v4, :cond_c

    .line 1908
    const/16 v0, 0xc

    iget v1, p0, Lcom/google/android/gms/icing/g;->l:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 1910
    :cond_c
    iget-boolean v0, p0, Lcom/google/android/gms/icing/g;->m:Z

    if-eqz v0, :cond_d

    .line 1911
    const/16 v0, 0xd

    iget-boolean v1, p0, Lcom/google/android/gms/icing/g;->m:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 1913
    :cond_d
    iget-wide v0, p0, Lcom/google/android/gms/icing/g;->n:J

    cmp-long v0, v0, v6

    if-eqz v0, :cond_e

    .line 1914
    const/16 v0, 0xe

    iget-wide v2, p0, Lcom/google/android/gms/icing/g;->n:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/b;->b(IJ)V

    .line 1916
    :cond_e
    iget-wide v0, p0, Lcom/google/android/gms/icing/g;->i:J

    cmp-long v0, v0, v6

    if-eqz v0, :cond_f

    .line 1917
    const/16 v0, 0xf

    iget-wide v2, p0, Lcom/google/android/gms/icing/g;->i:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/b;->a(IJ)V

    .line 1919
    :cond_f
    iget-object v0, p0, Lcom/google/android/gms/icing/g;->o:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_10

    .line 1920
    const/16 v0, 0x10

    iget-object v1, p0, Lcom/google/android/gms/icing/g;->o:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 1922
    :cond_10
    iget-object v0, p0, Lcom/google/android/gms/icing/g;->p:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_11

    .line 1923
    const/16 v0, 0x11

    iget-object v1, p0, Lcom/google/android/gms/icing/g;->p:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 1925
    :cond_11
    iget-object v0, p0, Lcom/google/android/gms/icing/g;->q:Lcom/google/android/gms/icing/h;

    if-eqz v0, :cond_12

    .line 1926
    const/16 v0, 0x12

    iget-object v1, p0, Lcom/google/android/gms/icing/g;->q:Lcom/google/android/gms/icing/h;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 1928
    :cond_12
    iget-object v0, p0, Lcom/google/android/gms/icing/g;->r:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_13

    .line 1929
    const/16 v0, 0x13

    iget-object v1, p0, Lcom/google/android/gms/icing/g;->r:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 1931
    :cond_13
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 1932
    return-void
.end method
