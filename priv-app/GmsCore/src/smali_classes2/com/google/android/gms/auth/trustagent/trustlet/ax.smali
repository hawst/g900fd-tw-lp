.class final Lcom/google/android/gms/auth/trustagent/trustlet/ax;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;


# instance fields
.field final synthetic a:Lcom/google/android/gms/auth/trustagent/trustlet/aw;


# direct methods
.method constructor <init>(Lcom/google/android/gms/auth/trustagent/trustlet/aw;)V
    .locals 0

    .prologue
    .line 120
    iput-object p1, p0, Lcom/google/android/gms/auth/trustagent/trustlet/ax;->a:Lcom/google/android/gms/auth/trustagent/trustlet/aw;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onSharedPreferenceChanged(Landroid/content/SharedPreferences;Ljava/lang/String;)V
    .locals 5

    .prologue
    const/4 v4, 0x4

    const/4 v3, 0x0

    .line 123
    invoke-static {p2}, Lcom/google/android/gms/auth/trustagent/ak;->i(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 124
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 137
    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/trustlet/ax;->a:Lcom/google/android/gms/auth/trustagent/trustlet/aw;

    invoke-static {v0}, Lcom/google/android/gms/auth/trustagent/trustlet/aw;->a(Lcom/google/android/gms/auth/trustagent/trustlet/aw;)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0, p2, v3}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 138
    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/trustlet/ax;->a:Lcom/google/android/gms/auth/trustagent/trustlet/aw;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/auth/trustagent/trustlet/aw;->c(Ljava/lang/String;)V

    .line 139
    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/trustlet/ax;->a:Lcom/google/android/gms/auth/trustagent/trustlet/aw;

    invoke-static {v0}, Lcom/google/android/gms/auth/trustagent/trustlet/aw;->c(Lcom/google/android/gms/auth/trustagent/trustlet/aw;)Lcom/google/android/gms/auth/trustagent/trustlet/aa;

    move-result-object v2

    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/trustlet/ax;->a:Lcom/google/android/gms/auth/trustagent/trustlet/aw;

    invoke-static {v0}, Lcom/google/android/gms/auth/trustagent/trustlet/aw;->b(Lcom/google/android/gms/auth/trustagent/trustlet/aw;)Ljava/util/Set;

    move-result-object v0

    new-array v3, v3, [Ljava/lang/String;

    invoke-interface {v0, v3}, Ljava/util/Set;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    invoke-virtual {v2, v0}, Lcom/google/android/gms/auth/trustagent/trustlet/aa;->a([Ljava/lang/String;)V

    .line 141
    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/trustlet/ax;->a:Lcom/google/android/gms/auth/trustagent/trustlet/aw;

    const/4 v2, 0x1

    invoke-static {v0, v2}, Lcom/google/android/gms/auth/trustagent/trustlet/aw;->a(Lcom/google/android/gms/auth/trustagent/trustlet/aw;Z)V

    .line 142
    const-string v0, "Coffee - PlaceTrustlet"

    invoke-static {v0, v4}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 143
    const-string v0, "Coffee - PlaceTrustlet"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "onSharedPreferenceChanged, addPlaceToTrustedPlaces: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 155
    :cond_0
    :goto_0
    return-void

    .line 147
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/trustlet/ax;->a:Lcom/google/android/gms/auth/trustagent/trustlet/aw;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/auth/trustagent/trustlet/aw;->d(Ljava/lang/String;)V

    .line 148
    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/trustlet/ax;->a:Lcom/google/android/gms/auth/trustagent/trustlet/aw;

    invoke-static {v0, v3}, Lcom/google/android/gms/auth/trustagent/trustlet/aw;->a(Lcom/google/android/gms/auth/trustagent/trustlet/aw;Z)V

    .line 149
    const-string v0, "Coffee - PlaceTrustlet"

    invoke-static {v0, v4}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 150
    const-string v0, "Coffee - PlaceTrustlet"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "onSharedPreferenceChanged, removePlaceFromTrustedPlaces: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
