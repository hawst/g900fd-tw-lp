.class public final Lcom/google/android/gms/car/ey;
.super Lcom/google/android/gms/car/ll;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/car/gw;
.implements Lcom/google/android/gms/car/senderprotocol/al;


# instance fields
.field final a:Ljava/lang/Object;

.field b:Lcom/google/android/gms/car/ez;

.field volatile c:Z

.field private d:Lcom/google/android/gms/car/senderprotocol/ak;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 19
    invoke-direct {p0}, Lcom/google/android/gms/car/ll;-><init>()V

    .line 23
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/car/ey;->a:Ljava/lang/Object;

    .line 129
    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/car/ey;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 19
    iget-object v0, p0, Lcom/google/android/gms/car/ey;->a:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic b(Lcom/google/android/gms/car/ey;)Lcom/google/android/gms/car/ez;
    .locals 1

    .prologue
    .line 19
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/car/ey;->b:Lcom/google/android/gms/car/ez;

    return-object v0
.end method

.method private c(Lcom/google/android/gms/car/ln;)V
    .locals 2

    .prologue
    .line 117
    iget-boolean v0, p0, Lcom/google/android/gms/car/ey;->c:Z

    if-nez v0, :cond_0

    .line 118
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "CarNotConnected"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 119
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/car/ey;->d:Lcom/google/android/gms/car/senderprotocol/ak;

    if-nez v0, :cond_1

    .line 120
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "MediaBrowserEndpoint not set."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 121
    :cond_1
    if-nez p1, :cond_2

    .line 122
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "CarMediaBrowserEventListener is null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 123
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/car/ey;->b:Lcom/google/android/gms/car/ez;

    if-eqz v0, :cond_3

    invoke-interface {p1}, Lcom/google/android/gms/car/ln;->asBinder()Landroid/os/IBinder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/car/ey;->b:Lcom/google/android/gms/car/ez;

    iget-object v1, v1, Lcom/google/android/gms/car/ez;->a:Lcom/google/android/gms/car/ln;

    invoke-interface {v1}, Lcom/google/android/gms/car/ln;->asBinder()Landroid/os/IBinder;

    move-result-object v1

    if-eq v0, v1, :cond_3

    .line 125
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Media browser service already in use."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 127
    :cond_3
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 30
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/car/ey;->c:Z

    .line 31
    return-void
.end method

.method public final a(Lcom/google/android/gms/car/ln;Lcom/google/android/gms/car/CarMediaBrowserListNode;)V
    .locals 1

    .prologue
    .line 53
    invoke-direct {p0, p1}, Lcom/google/android/gms/car/ey;->c(Lcom/google/android/gms/car/ln;)V

    .line 54
    iget-object v0, p0, Lcom/google/android/gms/car/ey;->d:Lcom/google/android/gms/car/senderprotocol/ak;

    invoke-virtual {v0, p2}, Lcom/google/android/gms/car/senderprotocol/ak;->a(Lcom/google/android/gms/car/CarMediaBrowserListNode;)V

    .line 55
    return-void
.end method

.method public final a(Lcom/google/android/gms/car/ln;Lcom/google/android/gms/car/CarMediaBrowserRootNode;)V
    .locals 1

    .prologue
    .line 61
    invoke-direct {p0, p1}, Lcom/google/android/gms/car/ey;->c(Lcom/google/android/gms/car/ln;)V

    .line 62
    iget-object v0, p0, Lcom/google/android/gms/car/ey;->d:Lcom/google/android/gms/car/senderprotocol/ak;

    invoke-virtual {v0, p2}, Lcom/google/android/gms/car/senderprotocol/ak;->a(Lcom/google/android/gms/car/CarMediaBrowserRootNode;)V

    .line 63
    return-void
.end method

.method public final a(Lcom/google/android/gms/car/ln;Lcom/google/android/gms/car/CarMediaBrowserSongNode;)V
    .locals 1

    .prologue
    .line 69
    invoke-direct {p0, p1}, Lcom/google/android/gms/car/ey;->c(Lcom/google/android/gms/car/ln;)V

    .line 70
    iget-object v0, p0, Lcom/google/android/gms/car/ey;->d:Lcom/google/android/gms/car/senderprotocol/ak;

    invoke-virtual {v0, p2}, Lcom/google/android/gms/car/senderprotocol/ak;->a(Lcom/google/android/gms/car/CarMediaBrowserSongNode;)V

    .line 71
    return-void
.end method

.method public final a(Lcom/google/android/gms/car/ln;Lcom/google/android/gms/car/CarMediaBrowserSourceNode;)V
    .locals 1

    .prologue
    .line 77
    invoke-direct {p0, p1}, Lcom/google/android/gms/car/ey;->c(Lcom/google/android/gms/car/ln;)V

    .line 78
    iget-object v0, p0, Lcom/google/android/gms/car/ey;->d:Lcom/google/android/gms/car/senderprotocol/ak;

    invoke-virtual {v0, p2}, Lcom/google/android/gms/car/senderprotocol/ak;->a(Lcom/google/android/gms/car/CarMediaBrowserSourceNode;)V

    .line 79
    return-void
.end method

.method public final a(Lcom/google/android/gms/car/senderprotocol/az;)V
    .locals 0

    .prologue
    .line 35
    check-cast p1, Lcom/google/android/gms/car/senderprotocol/ak;

    iput-object p1, p0, Lcom/google/android/gms/car/ey;->d:Lcom/google/android/gms/car/senderprotocol/ak;

    .line 36
    return-void
.end method

.method public final a(Ljava/lang/String;I)V
    .locals 4

    .prologue
    .line 152
    iget-object v1, p0, Lcom/google/android/gms/car/ey;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 153
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/car/ey;->b:Lcom/google/android/gms/car/ez;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    .line 155
    :try_start_1
    iget-object v0, p0, Lcom/google/android/gms/car/ey;->b:Lcom/google/android/gms/car/ez;

    iget-object v0, v0, Lcom/google/android/gms/car/ez;->a:Lcom/google/android/gms/car/ln;

    invoke-interface {v0, p1, p2}, Lcom/google/android/gms/car/ln;->a(Ljava/lang/String;I)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 160
    :cond_0
    :goto_0
    :try_start_2
    monitor-exit v1

    return-void

    .line 156
    :catch_0
    move-exception v0

    .line 157
    const-string v2, "CAR.INST"

    const-string v3, "Error calling onInput."

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 160
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final a(Ljava/lang/String;IZ)V
    .locals 4

    .prologue
    .line 165
    iget-object v1, p0, Lcom/google/android/gms/car/ey;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 166
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/car/ey;->b:Lcom/google/android/gms/car/ez;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    .line 168
    :try_start_1
    iget-object v0, p0, Lcom/google/android/gms/car/ey;->b:Lcom/google/android/gms/car/ez;

    iget-object v0, v0, Lcom/google/android/gms/car/ez;->a:Lcom/google/android/gms/car/ln;

    invoke-interface {v0, p1, p2, p3}, Lcom/google/android/gms/car/ln;->a(Ljava/lang/String;IZ)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 173
    :cond_0
    :goto_0
    :try_start_2
    monitor-exit v1

    return-void

    .line 169
    :catch_0
    move-exception v0

    .line 170
    const-string v2, "CAR.INST"

    const-string v3, "Error calling onGetNode."

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 173
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final a(Lcom/google/android/gms/car/ln;)Z
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 83
    iget-object v2, p0, Lcom/google/android/gms/car/ey;->a:Ljava/lang/Object;

    monitor-enter v2

    .line 84
    :try_start_0
    iget-object v1, p0, Lcom/google/android/gms/car/ey;->b:Lcom/google/android/gms/car/ez;

    if-eqz v1, :cond_1

    .line 85
    const-string v1, "CAR.INST"

    const/4 v3, 0x3

    invoke-static {v1, v3}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 86
    const-string v1, "CAR.INST"

    const-string v3, "Unregistering existing listener."

    invoke-static {v1, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 88
    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/car/ey;->b:Lcom/google/android/gms/car/ez;

    iget-object v1, v1, Lcom/google/android/gms/car/ez;->a:Lcom/google/android/gms/car/ln;

    invoke-virtual {p0, v1}, Lcom/google/android/gms/car/ey;->b(Lcom/google/android/gms/car/ln;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 91
    :cond_1
    :try_start_1
    new-instance v1, Lcom/google/android/gms/car/ez;

    invoke-direct {v1, p0, p1}, Lcom/google/android/gms/car/ez;-><init>(Lcom/google/android/gms/car/ey;Lcom/google/android/gms/car/ln;)V

    iput-object v1, p0, Lcom/google/android/gms/car/ey;->b:Lcom/google/android/gms/car/ez;

    .line 92
    iget-object v1, p0, Lcom/google/android/gms/car/ey;->b:Lcom/google/android/gms/car/ez;

    iget-object v1, v1, Lcom/google/android/gms/car/ez;->a:Lcom/google/android/gms/car/ln;

    invoke-interface {v1}, Lcom/google/android/gms/car/ln;->asBinder()Landroid/os/IBinder;

    move-result-object v1

    iget-object v3, p0, Lcom/google/android/gms/car/ey;->b:Lcom/google/android/gms/car/ez;

    const/4 v4, 0x0

    invoke-interface {v1, v3, v4}, Landroid/os/IBinder;->linkToDeath(Landroid/os/IBinder$DeathRecipient;I)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 97
    monitor-exit v2

    .line 98
    const/4 v0, 0x1

    :goto_0
    return v0

    .line 93
    :catch_0
    move-exception v1

    .line 94
    :try_start_2
    const-string v3, "CAR.INST"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Adding listener failed: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v3, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 95
    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 97
    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0
.end method

.method public final b(Lcom/google/android/gms/car/ln;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 103
    invoke-direct {p0, p1}, Lcom/google/android/gms/car/ey;->c(Lcom/google/android/gms/car/ln;)V

    .line 104
    iget-object v1, p0, Lcom/google/android/gms/car/ey;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 105
    :try_start_0
    iget-object v2, p0, Lcom/google/android/gms/car/ey;->b:Lcom/google/android/gms/car/ez;

    if-nez v2, :cond_0

    .line 106
    monitor-exit v1

    .line 110
    :goto_0
    return v0

    .line 108
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/car/ey;->b:Lcom/google/android/gms/car/ez;

    iget-object v0, v0, Lcom/google/android/gms/car/ez;->a:Lcom/google/android/gms/car/ln;

    invoke-interface {v0}, Lcom/google/android/gms/car/ln;->asBinder()Landroid/os/IBinder;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/gms/car/ey;->b:Lcom/google/android/gms/car/ez;

    const/4 v3, 0x0

    invoke-interface {v0, v2, v3}, Landroid/os/IBinder;->unlinkToDeath(Landroid/os/IBinder$DeathRecipient;I)Z

    .line 109
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/car/ey;->b:Lcom/google/android/gms/car/ez;

    .line 110
    const/4 v0, 0x1

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 112
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method
