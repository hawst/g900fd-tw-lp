.class public final Lcom/google/android/gms/icing/impl/d;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final a:[Ljava/lang/String;

.field private static final b:[Ljava/lang/String;

.field private static final c:[Ljava/lang/String;

.field private static final d:[Ljava/lang/String;


# instance fields
.field private final e:Lcom/google/android/gms/icing/impl/c;

.field private final f:Lcom/google/android/gms/icing/impl/c;

.field private g:Landroid/content/ContentResolver;

.field private final h:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 37
    new-array v0, v6, [Ljava/lang/String;

    const-string v1, "seqno"

    aput-object v1, v0, v2

    const-string v1, "action"

    aput-object v1, v0, v3

    const-string v1, "uri"

    aput-object v1, v0, v4

    const-string v1, "doc_score"

    aput-object v1, v0, v5

    sput-object v0, Lcom/google/android/gms/icing/impl/d;->a:[Ljava/lang/String;

    .line 43
    new-array v0, v3, [Ljava/lang/String;

    const-string v1, "created_timestamp"

    aput-object v1, v0, v2

    sput-object v0, Lcom/google/android/gms/icing/impl/d;->b:[Ljava/lang/String;

    .line 46
    new-array v0, v6, [Ljava/lang/String;

    const-string v1, "seqno"

    aput-object v1, v0, v2

    const-string v1, "action"

    aput-object v1, v0, v3

    const-string v1, "uri"

    aput-object v1, v0, v4

    const-string v1, "tag"

    aput-object v1, v0, v5

    sput-object v0, Lcom/google/android/gms/icing/impl/d;->c:[Ljava/lang/String;

    .line 52
    new-array v0, v2, [Ljava/lang/String;

    sput-object v0, Lcom/google/android/gms/icing/impl/d;->d:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/ContentResolver;[Ljava/lang/String;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 76
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 59
    new-instance v0, Lcom/google/android/gms/icing/impl/c;

    invoke-direct {v0}, Lcom/google/android/gms/icing/impl/c;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/icing/impl/d;->e:Lcom/google/android/gms/icing/impl/c;

    .line 60
    new-instance v0, Lcom/google/android/gms/icing/impl/c;

    invoke-direct {v0}, Lcom/google/android/gms/icing/impl/c;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/icing/impl/d;->f:Lcom/google/android/gms/icing/impl/c;

    .line 77
    iput-object p1, p0, Lcom/google/android/gms/icing/impl/d;->g:Landroid/content/ContentResolver;

    .line 79
    sget-object v0, Lcom/google/android/gms/icing/impl/d;->a:[Ljava/lang/String;

    array-length v0, v0

    array-length v1, p2

    add-int/2addr v0, v1

    new-array v0, v0, [Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/gms/icing/impl/d;->h:[Ljava/lang/String;

    .line 81
    sget-object v0, Lcom/google/android/gms/icing/impl/d;->a:[Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/gms/icing/impl/d;->h:[Ljava/lang/String;

    sget-object v2, Lcom/google/android/gms/icing/impl/d;->a:[Ljava/lang/String;

    array-length v2, v2

    invoke-static {v0, v3, v1, v3, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 83
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/d;->h:[Ljava/lang/String;

    sget-object v1, Lcom/google/android/gms/icing/impl/d;->a:[Ljava/lang/String;

    array-length v1, v1

    array-length v2, p2

    invoke-static {p2, v3, v0, v1, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 85
    return-void
.end method


# virtual methods
.method public final a()Lcom/google/android/gms/icing/impl/c;
    .locals 1

    .prologue
    .line 65
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/d;->e:Lcom/google/android/gms/icing/impl/c;

    return-object v0
.end method

.method public final a(Landroid/net/Uri;J)V
    .locals 10

    .prologue
    .line 104
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/d;->e:Lcom/google/android/gms/icing/impl/c;

    invoke-virtual {v0}, Lcom/google/android/gms/icing/impl/c;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 105
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/d;->e:Lcom/google/android/gms/icing/impl/c;

    iget-object v1, p0, Lcom/google/android/gms/icing/impl/d;->g:Landroid/content/ContentResolver;

    const-string v3, "documents"

    iget-object v4, p0, Lcom/google/android/gms/icing/impl/d;->h:[Ljava/lang/String;

    sget-object v5, Lcom/google/android/gms/icing/impl/d;->b:[Ljava/lang/String;

    const-string v8, "20"

    move-object v2, p1

    move-wide v6, p2

    invoke-virtual/range {v0 .. v8}, Lcom/google/android/gms/icing/impl/c;->a(Landroid/content/ContentResolver;Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;[Ljava/lang/String;JLjava/lang/String;)V

    .line 107
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/d;->f:Lcom/google/android/gms/icing/impl/c;

    invoke-virtual {v0}, Lcom/google/android/gms/icing/impl/c;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 108
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/d;->f:Lcom/google/android/gms/icing/impl/c;

    iget-object v1, p0, Lcom/google/android/gms/icing/impl/d;->g:Landroid/content/ContentResolver;

    const-string v3, "tags"

    sget-object v4, Lcom/google/android/gms/icing/impl/d;->c:[Ljava/lang/String;

    sget-object v5, Lcom/google/android/gms/icing/impl/d;->d:[Ljava/lang/String;

    const-string v8, "100"

    move-object v2, p1

    move-wide v6, p2

    invoke-virtual/range {v0 .. v8}, Lcom/google/android/gms/icing/impl/c;->a(Landroid/content/ContentResolver;Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;[Ljava/lang/String;JLjava/lang/String;)V

    .line 110
    :cond_1
    return-void
.end method

.method public final b()Lcom/google/android/gms/icing/impl/c;
    .locals 1

    .prologue
    .line 69
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/d;->f:Lcom/google/android/gms/icing/impl/c;

    return-object v0
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 91
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/d;->e:Lcom/google/android/gms/icing/impl/c;

    iget-boolean v0, v0, Lcom/google/android/gms/icing/impl/c;->b:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/icing/impl/d;->f:Lcom/google/android/gms/icing/impl/c;

    iget-boolean v0, v0, Lcom/google/android/gms/icing/impl/c;->b:Z

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final d()V
    .locals 1

    .prologue
    .line 98
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/d;->e:Lcom/google/android/gms/icing/impl/c;

    invoke-virtual {v0}, Lcom/google/android/gms/icing/impl/c;->d()V

    .line 99
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/d;->f:Lcom/google/android/gms/icing/impl/c;

    invoke-virtual {v0}, Lcom/google/android/gms/icing/impl/c;->d()V

    .line 100
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/icing/impl/d;->g:Landroid/content/ContentResolver;

    .line 101
    return-void
.end method

.method protected final finalize()V
    .locals 1

    .prologue
    .line 133
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/d;->g:Landroid/content/ContentResolver;

    if-eqz v0, :cond_0

    .line 134
    const-string v0, "Content cursor disposed without a closing"

    invoke-static {v0}, Lcom/google/android/gms/icing/c;->d(Ljava/lang/String;)I

    .line 136
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/icing/impl/d;->d()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 138
    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    .line 139
    return-void

    .line 138
    :catchall_0
    move-exception v0

    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    throw v0
.end method
